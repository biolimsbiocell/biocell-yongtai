<!--
说明：
时间：2011-4-20上午09:21:32
作者：congrixu
-->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.fileList"/></title>
	<script type="text/javascript">
		window.userId="${userId}";
	</script>
	<style type="text/css">
#basic, #animated {
    border:1px solid #c3daf9;
    color:#1e4e8f;
    font:bold 14px tahoma,verdana,helvetica;
    text-align:center;
    padding-top:20px;
}
#snap {
    border:1px solid #c3daf9;
    overflow:hidden;
}
#custom {
    cursor:move;
}
#custom-rzwrap{
    z-index: 100;
}
#custom-rzwrap .x-resizable-handle{
    width:11px;
    height:11px;
    background:transparent url(${ctx}/javascript/lib/ext-3.4.0/resources/images/default/sizer/square.gif) no-repeat;
    margin:0px;
}
#custom-rzwrap .x-resizable-handle-east, #custom-rzwrap .x-resizable-handle-west{
    top:45%;
}
#custom-rzwrap .x-resizable-handle-north, #custom-rzwrap .x-resizable-handle-south{
    left:45%;
}
</style>
	
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
				<%if(request.getParameter("flag")!=null&&request.getParameter("flag").equals("pic")){%>
<script type="text/javascript">
	var ResizableExample = {
	    init : function(){
	        
	       
	        
	        var custom = new Ext.Resizable('customId', {
	            wrap:true,
	            pinned:true,
	            minWidth:50,
	            minHeight: 50,
	            preserveRatio: true,
	            handles: 'all',
	            draggable:true,
	            dynamic:true
	        });
	        var customEl = custom.getEl();
	        // move to the body to prevent overlap on my blog
	        document.body.insertBefore(customEl.dom, document.body.firstChild);
	        
	        customEl.on('dblclick', function(){
	            customEl.hide(true);
	        });
	        customEl.hide();
	        Ext.get('showMe').on('click', function(){
	       
	        	var record = gridGrid.getSelectionModel().getSelections();
	        
		        	 if(record.length>0){
		        	  var fp = record[0].data.filePath;
		        	  
		        	  fp=fp.replace(/\＼/g,"\\");
		        	  fp=fp.replace(/\．/g,".");
		        	  fp=fp.replace(/\－/g,"-");
		        	  fp=fp.replace(/\：/g,":");
		        	  fp=fp.replace(/\＿/g,"_");
		        	  
		        	  document.getElementById("customId").src= "${ctx}/image/imagelook.jpg?filePath="+fp;
	        	 
	        	
	        	 customEl.center();
	             customEl.show(true);
		        	 }
	        	   });
	       
	        
	
	    }
	};

	Ext.EventManager.onDocumentReady(ResizableExample.init, ResizableExample, true);
	
	</script>
		    <%} %>
	<script type="text/javascript" src="${ctx}/javascript/lib/swfupload.js"></script>
	<script type="text/javascript" src="${ctx}/javascript/document/uploaddialog.js"></script>
	 <script type="text/javascript" src="${ctx}/javascript/document/operDocument.js"></script>
	<link rel="stylesheet" type="text/css" href="${ctx }/css/style.css"/>
</head>
<body onload="init()" >

<input type="hidden" id="modelType" value="<%=request.getParameter("modelType")%>"></input>
<input type="hidden" id="useType" value="<%=request.getParameter("flag")%>"></input>
<input type="hidden" id="picUserId" value="<%=request.getParameter("id")%>"></input>
<input type="hidden" id="flag" value="<%=request.getParameter("flag")%>"></input>
	<%if(request.getParameter("flag")!=null&&request.getParameter("flag").equals("pic")){%>
<img id="customId" src="" width="200" height="152" style="position:absolute;left:0;top:0;"/>
    <%} %>
	<s:form  method="post" namespace="" name="operForm" theme="simple">
		<input type="hidden" name="ids" id="ids"></input>
		<table style="width:99%;">
			<tr>
				<td align="left">
					<input type="checkbox" name="all" id="all" onclick="checkAll('sel')" />
					 
					<span id="operId_selFile" style="display:none">
					<%if(request.getParameter("view")==null){%>
						<span id="uploadButton"></span>
					<%} %>
					</span>
					
				</td>
				<td align="right">
					<span id="operId_fileOper" style="display:none">
						<input type="button" class="button" value="<fmt:message key="biolims.common.download"/>" onclick="download()">
						
						<%if(request.getParameter("view")==null){%>
						<input type="button" class="button" value="<fmt:message key="biolims.common.delete"/>" onclick="delFile()">
						<%} %>
						<%if(request.getParameter("flag")!=null&&request.getParameter("flag").equals("pic")){%>
					    <input type="button" id="showMe"  class="button" value="<fmt:message key="biolims.common.checkThePicture"/>">
					    <%} %>
					</span>
				</td>
			</tr>
		</table>
	</s:form>
	<div><input type="hidden" id="id" value="" />
			<g:GridTag   
			isAutoWidth="true" col="${col}" type="${type}" title="" height="document.body.clientHeight-30" isHasNum="false"
			url="${path}" />
</div>
</body>
</html>