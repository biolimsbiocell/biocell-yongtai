<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript"	src="${ctx}/javascript/lib/jquery.fileupload.js"></script>
<script type="text/javascript"	src="${ctx}/javascript/lib/jquery.iframe-transport.js"></script>
<script type="text/javascript"	src="${ctx}/javascript/document/upload.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
</head>
<body>
<div id="uploadDiv">
	<input type="document" name="document" id="document-upload" >
</div>
</body>
</html>