<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<body>
	<input type="hidden"  id="itemId" value="${requestScope.flist}">
	<c:forEach var="fList" items="${flist}">
		<div style="float:left;padding-bottom:20px;padding-left:20px;">
			<img id="upLoadImg"  src="${ctx}/operfile/downloadById.action?id=${fList.id}">
			<p style="font-size:16px;font-weight:bold;padding-bottom:40px;">${fList.fileName}</p>
		</div>
	</c:forEach>
</body>
</html>