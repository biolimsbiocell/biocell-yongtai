﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/technology/dna/techDnaServiceTask.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString">
		<form id="searchForm">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.user.itemNo" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techDnaServiceTask_id" name="id" searchField="true" title="编号" />






					</td>
					<td class="label-title"><fmt:message
							key="biolims.common.name" /></td>
					<td align="left"><input type="text" size="20" maxlength="50"
						id="techDnaServiceTask_name" name="name" searchField="true"
						title="描述" /> <%-- 	</td>
			<g:LayOutWinTag buttonId="showprojectId" title="选择项目编号"
				hasHtmlFrame="true"
				html="${ctx}/technology/dna/projectSelect.action"
				isHasSubmit="false" functionName="ProjectFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techDnaServiceTask_projectId').value=rec.get('id');
				document.getElementById('techDnaServiceTask_projectId_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.projectId"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_projectId_name" searchField="true"  name="projectId.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_projectId" name="techDnaServiceTask.projectId.id"  value="" > 
 						<img alt='选择项目编号' id='showprojectId' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
				</tr>
				<%-- <tr>
			<g:LayOutWinTag buttonId="showcontractId" title="选择合同编号"
				hasHtmlFrame="true"
				html="${ctx}/technology/dna/contractSelect.action"
				isHasSubmit="false" functionName="ContractFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techDnaServiceTask_contractId').value=rec.get('id');
				document.getElementById('techDnaServiceTask_contractId_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.contractId"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_contractId_name" searchField="true"  name="contractId.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_contractId" name="techDnaServiceTask.contractId.id"  value="" > 
 						<img alt='选择合同编号' id='showcontractId' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showorderType" title="选择任务单类型"
				hasHtmlFrame="true"
				html="${ctx}/technology/dna/dicTypeSelect.action"
				isHasSubmit="false" functionName="DicTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techDnaServiceTask_orderType').value=rec.get('id');
				document.getElementById('techDnaServiceTask_orderType_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.orderType"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_orderType_name" searchField="true"  name="orderType.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_orderType" name="techDnaServiceTask.orderType.id"  value="" > 
 						<img alt='选择任务单类型' id='showorderType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showprojectLeader" title="选择项目负责人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun1" 
 				hasSetFun="true"
				documentId="techDnaServiceTask_projectLeader"
				documentName="techDnaServiceTask_projectLeader_name"  />
               	 	<td class="label-title" ><fmt:message key="biolims.common.projectLeader"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_projectLeader_name" searchField="true"  name="projectLeader.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_projectLeader" name="techDnaServiceTask.projectLeader.id"  value="" > 
 						<img alt='选择项目负责人' id='showprojectLeader' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showexperimentLeader" title="选择实验负责人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun2" 
 				hasSetFun="true"
				documentId="techDnaServiceTask_experimentLeader"
				documentName="techDnaServiceTask_experimentLeader_name" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.testLeader"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_experimentLeader_name" searchField="true"  name="experimentLeader.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_experimentLeader" name="techDnaServiceTask.experimentLeader.id"  value="" > 
 						<img alt='选择实验负责人' id='showexperimentLeader' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showanalysisLeader" title="选择信息负责人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun3" 
 				hasSetFun="true"
				documentId="techDnaServiceTask_analysisLeader"
				documentName="techDnaServiceTask_analysisLeader_name"  />
               	 	<td class="label-title" ><fmt:message key="biolims.common.analysisLeader"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_analysisLeader_name" searchField="true"  name="analysisLeader.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_analysisLeader" name="techDnaServiceTask.analysisLeader.id"  value="" > 
 						<img alt='选择信息负责人' id='showanalysisLeader' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showtechProjectTask" title="选择相关项目"
				hasHtmlFrame="true"
				html="${ctx}/technology/dna/techProjectTaskSelect.action"
				isHasSubmit="false" functionName="TechProjectTaskFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techDnaServiceTask_techProjectTask').value=rec.get('id');
				document.getElementById('techDnaServiceTask_techProjectTask_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.relatedMainPro"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_techProjectTask_name" searchField="true"  name="techProjectTask.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_techProjectTask" name="techDnaServiceTask.techProjectTask.id"  value="" > 
 						<img alt='选择相关项目' id='showtechProjectTask' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"><fmt:message key="biolims.common.note"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="250" id="techDnaServiceTask_note"
                   	 name="note" searchField="true" title="备注"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun4" 
 				hasSetFun="true"
				documentId="techDnaServiceTask_createUser"
				documentName="techDnaServiceTask_createUser_name" />
               	 	<td class="label-title" ><fmt:message key="biolims.sample.createUserName"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techDnaServiceTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="techDnaServiceTask_createUser" name="techDnaServiceTask.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.sample.createDate"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr> --%>
				<tr>
					<g:LayOutWinTag buttonId="showconfirmUser" title="选择审批人"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun5" hasSetFun="true"
						documentId="techDnaServiceTask_createUserOneId"
						documentName="techDnaServiceTask_createUserOneName" />
					<td class="label-title"><fmt:message
							key="biolims.common.commandPerson" /></td>
					<td align="left"><input type="text" size="20"
						id="techDnaServiceTask_createUserOneName" searchField="true"
						name="createUser" value="" class="text input" /> <input
						type="hidden" id="techDnaServiceTask_createUserOneId"
						name="techDnaServiceTask.createUserOneId" value=""> <img
						alt='选择审批人' id='showconfirmUser'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					<!-- 
               	 	<td class="label-title" ><fmt:message key="biolims.wk.approverDate"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	 -->
					<td class="label-title"><fmt:message
							key="biolims.common.stateDescribe" /></td>
					<td align="left"><input type="text" size="20"
						maxlength="25" id="techDnaServiceTask_stateName" name="stateName"
						searchField="true" title="状态描述" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="show_techDnaServiceTask_div"></div>
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value='' />
	</form>
	<div id="show_techDnaServiceTask_tree_page"></div>
</body>
</html>



