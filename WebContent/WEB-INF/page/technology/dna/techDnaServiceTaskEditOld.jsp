<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=techDnaServiceTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/technology/dna/techDnaServiceTaskEdit.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>


					<td class="label-title"><fmt:message key="biolims.common.code"/></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techDnaServiceTask_id" name="techDnaServiceTask.id" title=""
						class="text input readonlytrue" readonly="readOnly"
						value="<s:property value="techDnaServiceTask.id"/>" /></td>


					<td class="label-title"><fmt:message key="biolims.common.name"/></td>
					<td class="requiredcolumn" nowrap width="10px">
					<img	class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techDnaServiceTask_name" name="techDnaServiceTask.name"
						title="" value="<s:property value="techDnaServiceTask.name"/>" />

					</td>
					<td class="label-title"><fmt:message key="biolims.sample.createUserName"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="techDnaServiceTask_createUser_name"
						class="text input readonlytrue"
						value="<s:property value="techDnaServiceTask.createUser.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="techDnaServiceTask_createUser"
						name="techDnaServiceTask.createUser.id"
						value="<s:property value="techDnaServiceTask.createUser.id"/>">
						<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
					</td>

				</tr>

				<tr>

					<td class="label-title"><fmt:message key="biolims.common.note"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="250"
						id="techDnaServiceTask_note" name="techDnaServiceTask.note"
						title="" value="<s:property value="techDnaServiceTask.note"/>" />
					</td>
					<td class="label-title"><fmt:message key="biolims.common.stateName"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techDnaServiceTask_stateName"
						name="techDnaServiceTask.stateName" title=""
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="techDnaServiceTask.stateName"/>" /></td>

					<td class="label-title" style="display: none"><fmt:message key="biolims.common.state"/></td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="techDnaServiceTask_state"
						name="techDnaServiceTask.state" title=""
						value="<s:property value="techDnaServiceTask.state"/>"
						style="display: none" /></td>
					<td class="label-title"><fmt:message key="biolims.sample.createDate"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techDnaServiceTask_createDate"
						name="techDnaServiceTask.createDate" title=""
						readonly="readonly" class="text input readonlytrue"
						value="<s:date name="techDnaServiceTask.createDate" format="yyyy-MM-dd"/>" />
					</td>

				</tr>
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.attachment"/></td>
					<td></td>
					<td title=<fmt:message key="biolims.common.afterthepreservation"/> id="doclinks_img"><span
						class="attach-btn"></span><span class="text label">
						<fmt:message key="biolims.common.allHave"/> ${requestScope.fileNum} &nbsp;<fmt:message key="biolims.common.total"/></span>
				</tr>


			</table>
			<input type="hidden" name="techDnaServiceTaskItemJson"
				id="techDnaServiceTaskItemJson" value="" /> <input type="hidden"
				id="id_parent_hidden"
				value="<s:property value="techDnaServiceTask.id"/>" />
		</form>
		<div id="tabs">
			<ul>
				<li><a href="#techDnaServiceTaskItempage"><fmt:message key="biolims.common.claimSampleTask"/> </a></li>
			</ul>
			<div id="techDnaServiceTaskItempage" width="100%" height:10px></div>
		</div>
	</div>
</body>
</html>
