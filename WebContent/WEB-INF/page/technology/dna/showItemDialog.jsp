﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/technology/dna/showItemDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<input type="hidden" id="cid" value="${requestScope.cid}">
<table>
<tr>
			<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
                  	<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
					<input type="text" size="20" maxlength="25" id="code"
                   	 name="sampleInfoIn.code" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    />
                   	 
              	</td>
              	<td class="label-title" ><fmt:message key="biolims.common.outCode"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="50" id="sampleInfoIn_sampleInfo_idCard"
                   	 name="sampleInfoIn.sampleInfo.idCard" searchField="true" title=<fmt:message key="biolims.common.outCode"/>   />
                </td>
               <%--  <td class="label-title" ><fmt:message key="biolims.common.concentration"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="concentration"
                   	 name="sampleInfoIn.concentration" searchField="true" title="<fmt:message key="biolims.common.concentration"/>"    />
                </td>
                <td class="label-title" ><fmt:message key="biolims.common.volume"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="volume"
                   	 name="sampleInfoIn.volume" searchField="true" title="<fmt:message key="biolims.common.volume"/>"    />
                </td>
                <td class="label-title" ><fmt:message key="biolims.common.totalAmount"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="sumTotal"
                   	 name="sampleInfoIn.sumTotal" searchField="true" title="<fmt:message key="biolims.common.totalAmount"/>"    />
                </td>-%>
         </tr>
         <tr>
                
                   <%--	<td class="label-title" >合同项目</td>
                   	<td align="left"  >
                   	
                    	<input type ="hidden"  id="sampleInfo_project" name="sampleInfo.project.id"   value="<s:property value="sampleInfo.project.id"/>"> 
                   	<input type="text" searchField="true" size="20" maxlength="50"  id="sampleInfo_project_name" name="sampleInfoIn.sampleInfo.project.name"  value="<s:property value="sampleInfo.project.name"/>" />
                   	</td>--%>
                <td class="label-title" ><fmt:message key="biolims.common.sponsor"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="customer_name"
                   	 name="sampleInfoIn.customer.name" searchField="true" title="<fmt:message key="biolims.common.sponsor"/>"    />
                </td>
                <td class="label-title" ><fmt:message key="biolims.common.customerName"/></td>
                   	<td align="left"  >
                   	
<%--                    	<input type ="hidden"  id="sampleInfo_crmCustomer_id" name="sampleInfo.crmCustomer.id"   value="<s:property value="sampleInfo.crmCustomer.id"/>"> --%>
                   	<input type="text" searchField="true" size="20" maxlength="50"  id="sampleInfo_crmDoctor_name"  name="sampleInfoIn.sampleInfo.crmDoctor.name"   value="<s:property value="sampleInfo.crmDoctor.name"/>" />
                   	</td>
                   	
                   	<td colspan="2" align="center">
                   	 <input type="button" value="<fmt:message key="biolims.common.find"/>" onClick="searchGrid();">
                </td>
                   	</tr>
</table>   	
					
	<div id="showItemDialogdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
</body>
</html>


