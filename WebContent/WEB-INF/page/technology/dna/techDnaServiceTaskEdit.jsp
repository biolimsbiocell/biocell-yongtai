<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}
</style>
</head>
<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
<body style="height: 94%">
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
		<%-- <input type="hidden" id="storageOut_name"
			value="<s:property value="storageOut.name"/>"> --%> <input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.common.claimTaskList"/> <small style="color: #fff;"> <fmt:message key="biolims.common.orderId"/>: <span
								id="techDnaServiceTask_id"><s:property value="techDnaServiceTask.id" /></span>
							<fmt:message key="biolims.sample.createUserName"/>: <span
								userId="<s:property value="techDnaServiceTask.createUser.id"/>"
								id="techDnaServiceTask_createUser"><s:property
										value="techDnaServiceTask.createUser.name" /></span><fmt:message key="biolims.sample.createDate"/>: <span
								id="techDnaServiceTask_createDate"><s:date name="techDnaServiceTask.createDate" format="yyyy-MM-dd "/></span> <fmt:message key="biolims.common.state"/>: <span
								state="<s:property value="techDnaServiceTask.state"/>"
								id="headStateName"><s:property
										value="techDnaServiceTask.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.common.describe"/><img
												class='requiredimage' src='${ctx}/images/required.gif' />
											</span> <input type="text" size="20" maxlength="25"
												id="techDnaServiceTask_name" name="techDnaServiceTask.name"
												changelog="<s:property value="techDnaServiceTask.name"/>"
												class="form-control"
												value="<s:property value="techDnaServiceTask.name"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.common.note"/></span>

											<input type="text" size="20"  id="techDnaServiceTask_note" changelog="<s:property value="techDnaServiceTask.note"/>" value="<s:property value="techDnaServiceTask.note"/>" class="form-control" />

										</div>
									</div>
									<div class="col-xs-4">
											<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
									</div>
								</div>
							</form>
						</div>

	</br>






						<!--库存主数据-->
						<div id="leftDiv" class="col-md-5 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.sampleToBeClaimed" /></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageOutAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-md-7 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.claimedSample" /></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageOutItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>

		</div>
		</section>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/technology/dna/techDnaServiceAllLeft.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/technology/dna/techDnaServiceRight.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>