﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/technology/dna/techDnaServiceTaskDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="techDnaServiceTask_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">项目编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_projectId" searchField="true" name="projectId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">合同编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_contractId" searchField="true" name="contractId"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">任务单类型</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_orderType" searchField="true" name="orderType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">项目负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_projectLeader" searchField="true" name="projectLeader"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">实验负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_experimentLeader" searchField="true" name="experimentLeader"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">信息负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_analysisLeader" searchField="true" name="analysisLeader"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">相关项目</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_techProjectTask" searchField="true" name="techProjectTask"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="250" id="techDnaServiceTask_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">审批人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_confirmUser" searchField="true" name="confirmUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">审批时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_confirmDate" searchField="true" name="confirmDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态描述</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techDnaServiceTask_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_techDnaServiceTask_div"></div>
   		
</body>
</html>



