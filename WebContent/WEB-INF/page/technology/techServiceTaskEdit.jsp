﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=techServiceTask&id=${techServiceTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/technology/techServiceTaskEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="techServiceTask_id"
                   	 name="techServiceTask.id" title="编号"
                   	   
	value="<s:property value="techServiceTask.id"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="techServiceTask_name"
                   	 name="techServiceTask.name" title="描述"
                   	   
	value="<s:property value="techServiceTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techServiceTask_state"
                   	 name="techServiceTask.state" title="状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techServiceTask.state"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techServiceTask_stateName"
                   	 name="techServiceTask.stateName" title="状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techServiceTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="techServiceTaskItemJson" id="techServiceTaskItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="techServiceTask.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#techServiceTaskItempage">科技服务明细</a></li>
           	</ul> 
			<div id="techServiceTaskItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
