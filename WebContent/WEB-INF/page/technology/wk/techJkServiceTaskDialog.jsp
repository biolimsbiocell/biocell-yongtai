﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/technology/wk/techJkServiceTaskDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="techJkServiceTask_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.itemCode"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_projectId" searchField="true" name="projectId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.contractCode"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_contractId" searchField="true" name="contractId"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.taskType"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_orderType" searchField="true" name="orderType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.businessTypes"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_productId" searchField="true" name="productId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.dataType"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_dataType" searchField="true" name="dataType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.dateOfOrder"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_sequenceBillDate" searchField="true" name="sequenceBillDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.taskName"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_sequenceBillName" searchField="true" name="sequenceBillName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.sequencingMethods"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_sequenceType" searchField="true" name="sequenceType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.sequencingReadLong"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_sequenceLength" searchField="true" name="sequenceLength"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.sequencingPlatform"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_sequencePlatform" searchField="true" name="sequencePlatform"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.sampleSize"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_sampeNum" searchField="true" name="sampeNum"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.speciesType"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_species" searchField="true" name="species"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.experimentalCycle"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_experimentPeriod" searchField="true" name="experimentPeriod"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.experimentDeadline"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_experimentEndTime" searchField="true" name="experimentEndTime"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.experimentalRequirements"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_experimentRemarks" searchField="true" name="experimentRemarks"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.analysisOfTheCycle"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_analysisCycle" searchField="true" name="analysisCycle"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.informationOnTheDeadline"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_analysisEndTime" searchField="true" name="analysisEndTime"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.informationRequirements"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_anaysisRemarks" searchField="true" name="anaysisRemarks"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.projectEmergencyRecognition"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_projectDifference" searchField="true" name="projectDifference"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.mixProportion"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_mixRatio" searchField="true" name="mixRatio"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.coefficient"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_coefficient" searchField="true" name="coefficient"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">readsone</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_readsOne" searchField="true" name="readsOne"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">readstwo</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_readsTwo" searchField="true" name="readsTwo"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">index1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_index1" searchField="true" name="index1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">index2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_index2" searchField="true" name="index2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.expectedDensity"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_density" searchField="true" name="density"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.orderPeople"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_orders" searchField="true" name="orders"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.projectLeader"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_projectLeader" searchField="true" name="projectLeader"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.experiment"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_experimentLeader" searchField="true" name="experimentLeader"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.informationDirector"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_analysisLeader" searchField="true" name="analysisLeader"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.relatedProjects"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_techProjectTask" searchField="true" name="techProjectTask"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.note"/></td>
               	<td align="left">
                    		<input type="text" maxlength="250" id="techJkServiceTask_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandPerson"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.commandTime"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.auditor"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_confirmUser" searchField="true" name="confirmUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.auditDate"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_confirmDate" searchField="true" name="confirmDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.state"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.stateDescription"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="techJkServiceTask_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_techJkServiceTask_div"></div>
   		
</body>
</html>



