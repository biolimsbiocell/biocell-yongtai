<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<style type="text/css">
.hide {
	display: none;
}
</style>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.upLoadFile"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=techJkServiceTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img1 = Ext.get('doclinks_img1');
doclinks_img1.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.upLoadFile"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=techJkServiceTaskdyt&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img2 = Ext.get('doclinks_img2');
doclinks_img2.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.upLoadFile"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=techJkServiceTask2100&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/technology/wk/techJkServiceTaskEdit.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}"> <input type="hidden"
			id="type2" value="${requestScope.type}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techJkServiceTask_id" name="techJkServiceTask.id"
						title="<fmt:message key="biolims.common.serialNumber"/>"
						class="text input readonlytrue" readonly="readOnly"
						value="<s:property value="techJkServiceTask.id"/>" /></td>


					<td class="label-title"><fmt:message
							key="biolims.common.describe" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techJkServiceTask_name" name="techJkServiceTask.name"
						title="<fmt:message key="biolims.common.describe"/>"
						value="<s:property value="techJkServiceTask.name"/>" /></td>

					<td class="label-title"><fmt:message key="biolims.wk.sequenceBillName"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="hidden"
						id="techJkServiceTask_type" name="techJkServiceTask.type"
						value="${requestScope.type}"> <c:if
							test="${requestScope.type =='0'}">
							<input type=text readonly="readOnly"
								value="<fmt:message key="biolims.common.nucleicAcidTaskList"/>">
						</c:if> <c:if test="${requestScope.type =='1'}">
							<input type=text readonly="readOnly"
								value="<fmt:message key="biolims.common.taskForBuildingALibrary"/>">
						</c:if> <c:if test="${requestScope.type =='2'}">
							<input type=text readonly="readOnly"
							 value="<fmt:message key="biolims.common.sequenceTask"/>">
						</c:if> <c:if test="${requestScope.type =='3'}">
							<input type=text readonly="readOnly" 
							value=<fmt:message key="biolims.common.bioinfoTask"/>>
						</c:if></td>
				</tr>
				<tr class="jk">
					<td class="label-title"><fmt:message key="biolims.common.typePlatform"/> </td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><select id="techJkServiceTask_cxType"
						name="techJkServiceTask.cxType" class="input-20-length"
						onChange="cxTypeChange()">
							<option value=""
								<s:if test="techJkServiceTask.cxType==''">selected="selected"</s:if>>Please</option>
							<option value="1"
								<s:if test="techJkServiceTask.cxType==1">selected="selected"</s:if>>Illumina Platform</option>
							<option value="2"
								<s:if test="techJkServiceTask.cxType==2">selected="selected"</s:if>>lontorrent Platform</option>
							<option value="3"
								<s:if test="techJkServiceTask.cxType==3">selected="selected"</s:if>>Affymetrix Platform</option>
							<option value="4"
								<s:if test="techJkServiceTask.cxType==4">selected="selected"</s:if>>Q-PCR Platform</option>
							<option value="5"
								<s:if test="techJkServiceTask.cxType==5">selected="selected"</s:if>>fluidigm Platform</option>
							<option value="6"
								<s:if test="techJkServiceTask.cxType==6">selected="selected"</s:if>>MLPA Platform</option>
					</select></td>
				</tr>
				<tr style="display: none;">
					<td class="label-title"><fmt:message
							key="biolims.common.detectionItems" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="techJkServiceTask_proName"
						name="techJkServiceTask.proName"
						value="<s:property value="techJkServiceTask.proName"/>" /> <input
						type="hidden" id="techJkServiceTask_proId"
						name="techJkServiceTask.proId"
						value="<s:property value="techJkServiceTask.proId"/>"> <img
						alt='<fmt:message key="biolims.common.selectBusinessTypes"/>'
						src='${ctx}/images/img_lookup.gif' onClick="ProductFun()"
						class='detail' /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.internalProjectCode" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="20"
						id="techJkServiceTask_inwardCode"
						name="techJkServiceTask.inwardCode" title="Quote ID"
						value="<s:property value="techJkServiceTask.inwardCode"/>" /></td>
					
			</table>
			<div style="display: none;">
				<table width="100%">
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " colspan="9" width='50%' valign="top"
							align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td
													class="section_header_left text standard_section_label labelcolor"
													align="left"><span class="section_label"><fmt:message
															key="biolims.common.libraryConstructionInformation" /></span> <!-- 文库构建信息 --></td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<table class="frame-table">
					<tr>

						<td class="label-title"><fmt:message
								key="biolims.common.libraryType" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select name="techJkServiceTask.libraryType"
							id="techJkServiceTask_libraryType">
								<option value=""
									<s:if test="techJkServiceTask.libraryType == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>
								<c:forEach var="listLibraryType" items="${listLibraryType}">
									<option value="${listLibraryType.name}"
										<c:if test="${techJkServiceTask.libraryType==listLibraryType.name}">selected="selected"</c:if>>${listLibraryType.name}</option>
								</c:forEach>
						</select></td>
						<td class="label-title">Methodology</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select name="techJkServiceTask.methodology"
							id="techJkServiceTask_methodology">
								<option value=""
									<s:if test="techJkServiceTask.methodology == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>
								<c:forEach var="listMethodology" items="${listMethodology}">
									<option value="${listMethodology.name}"
										<c:if test="${techJkServiceTask.methodology==listMethodology.name}">selected="selected"</c:if>>${listMethodology.name}</option>
								</c:forEach>
						</select></td>
						<td class="label-title">Index</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select name="techJkServiceTask.lciIndex"
							id="techJkServiceTask_lciIndex">
								<option value=""
									<s:if test="techJkServiceTask.lciIndex == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>
								<c:forEach var="listLCIIndex" items="${listLCIIndex}">
									<option value="${listLCIIndex.name}"
										<c:if test="${techJkServiceTask.lciIndex==listLCIIndex.name}">selected="selected"</c:if>>${listLCIIndex.name}</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr>

						<td class="label-title"><fmt:message
								key="biolims.common.insertSize" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_insertSize"
							name="techJkServiceTask.insertSize" title="Insert Size"
							value="<s:property value="techJkServiceTask.insertSize"/>" /></td>
						<td class="label-title"><fmt:message
								key="biolims.common.targetYield" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_targetYield"
							name="techJkServiceTask.targetYield"
							title="<fmt:message key="biolims.common.targetYield"/>"
							value="<s:property value="techJkServiceTask.targetYield"/>" /></td>
						<td class="label-title"><fmt:message
								key="biolims.common.note" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_notes" name="techJkServiceTask.notes"
							title="<fmt:message key="biolims.common.note"/>"
							value="<s:property value="techJkServiceTask.notes"/>" /></td>
					</tr>
					<tr style="display: none;">
						<td class="label-title"><fmt:message
								key="biolims.common.speciesType" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="25"
							id="techJkServiceTask_species" name="techJkServiceTask.species"
							title="<fmt:message key="biolims.common.speciesType"/>"
							value="<s:property value="techJkServiceTask.species"/>" /></td>
						<td class="label-title">GC%</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_lciGc" name="techJkServiceTask.lciGc"
							title="GC" value="<s:property value="techJkServiceTask.lciGc"/>" />
						</td>

					</tr>
				</table>
			</div>
			<div class="jk">
				<table width="100%">
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " colspan="9" width='50%' valign="top"
							align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td
													class="section_header_left text standard_section_label labelcolor"
													align="left"><span class="section_label"><fmt:message
															key="biolims.common.projectInformation" /></span> <!-- 测序信息 --></td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<table class="frame-table">

					<tr>
						<td class="label-title"><fmt:message
								key="biolims.common.sequencingPlatform" /></td>
						<!-- 测序平台 -->
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select
							name="techJkServiceTask.sequencePlatform"
							id="techJkServiceTask_sequencePlatform">
								<option value=""
									<s:if test="techJkServiceTask.sequencePlatform == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>

								<c:forEach var="listPlatForm" items="${listPlatForm}">
									<option value="${listPlatForm.name}"
										<c:if test="${techJkServiceTask.sequencePlatform==listPlatForm.name}">selected="selected"</c:if>>${listPlatForm.name}</option>
								</c:forEach>

						</select></td>
						<td class="label-title"><fmt:message
								key="biolims.common.sequencingType" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select
							name="techJkServiceTask.sequenceType"
							id="techJkServiceTask_sequenceType">
								<option value=""
									<s:if test="techJkServiceTask.sequenceType == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>
								<c:forEach var="listReadType" items="${listReadType}">
									<option value="${listReadType.name}"
										<c:if test="${techJkServiceTask.sequenceType==listReadType.name}">selected="selected"</c:if>>${listReadType.name}</option>
								</c:forEach>
						</select></td>
						<td class="label-title"><fmt:message
								key="biolims.common.sequencingReadLong" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select
							name="techJkServiceTask.sequenceLength"
							id="techJkServiceTask_sequenceLength">
								<option value=""
									<s:if test="techJkServiceTask.sequenceLength == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>
								<c:forEach var="listSequencingCycle"
									items="${listSequencingCycle}">
									<option value="${listSequencingCycle.name}"
										<c:if test="${techJkServiceTask.sequenceLength==listSequencingCycle.name}">selected="selected"</c:if>>${listSequencingCycle.name}</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr style="display: none;">
						<td class="label-title">phix%</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="25"
							id="techJkServiceTask_phix" name="techJkServiceTask.phix"
							title="phix" value="<s:property value="techJkServiceTask.phix"/>" />
						</td>
						<td class="label-title">Index</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select name="techJkServiceTask.siIndex"
							id="techJkServiceTask_siIndex">
								<option value=""
									<s:if test="techJkServiceTask.siIndex == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>
								<c:forEach var="listIndex" items="${listIndex}">
									<option value="${listIndex.name}"
										<c:if test="${techJkServiceTask.siIndex==listIndex.name}">selected="selected"</c:if>>${listIndex.name}</option>
								</c:forEach>
						</select></td>
						<td class="label-title"><fmt:message
								key="biolims.common.clientIndex" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><select name="techJkServiceTask.clientIndex"
							id="techJkServiceTask_clientIndex">
								<option value=""
									<s:if test="techJkServiceTask.clientIndex == ''">selected="selected"</s:if>>
									<fmt:message key="biolims.common.pleaseSelect" />
								</option>
								<c:forEach var="listClientIndex" items="${listClientIndex}">
									<option value="${listClientIndex.name}"
										<c:if test="${techJkServiceTask.clientIndex==listClientIndex.name}">selected="selected"</c:if>>${listClientIndex.name}</option>
								</c:forEach>
						</select></td>
					</tr>
					<tr style="display: none;">
						<td class="label-title">GC%</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="25"
							id="techJkServiceTask_siGc" name="techJkServiceTask.siGc"
							title="GC" value="<s:property value="techJkServiceTask.siGc"/>" />
						</td>
						<td class="label-title">Exclusive Lane</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_exclusiveLane"
							name="techJkServiceTask.exclusiveLane" title="Exclusive Lane"
							value="<s:property value="techJkServiceTask.exclusiveLane"/>" />
						</td>
						<td class="label-title">Exclusive Flowcell</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_exclusiveFlowcell"
							name="techJkServiceTask.exclusiveFlowcell"
							title="Exclusive Flowcell"
							value="<s:property value="techJkServiceTask.exclusiveFlowcell"/>" />
						</td>
					</tr>
					<tr class="cx1">
						<td class="label-title"><fmt:message
								key="biolims.common.QCRequirements" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<!-- QC要求 -->
						<td align="left"><input type="text" size="20" maxlength="50"
							id="techJkServiceTask_qcRequirements"
							name="techJkServiceTask.qcRequirements"
							title="<fmt:message key="biolims.common.QCRequirements"/>"
							value="<s:property value="techJkServiceTask.qcRequirements"/>" />
						</td>
						<td class="label-title">芯片类型</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="50"
							id="techJkServiceTask_chipType" name="techJkServiceTask.chipType"
							title=""
							value="<s:property value="techJkServiceTask.chipType"/>" /></td>
						<td class="label-title">样本制备方法</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="100"
							id="techJkServiceTask_sampleZbff"
							name="techJkServiceTask.sampleZbff" title=""
							value="<s:property value="techJkServiceTask.sampleZbff"/>" /></td>
					</tr>
					<tr class="cx2">
						<td class="label-title">检测原辅料盒</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_testKit" name="techJkServiceTask.testKit"
							title="检测原辅料盒"
							value="<s:property value="techJkServiceTask.testKit"/>" /></td>
						<td class="label-title">基因</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_gene" name="techJkServiceTask.gene"
							title="基因" value="<s:property value="techJkServiceTask.gene"/>" />
						</td>
					</tr>
					<tr class="cx2">
						<td class="label-title">质控品</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_zkp" name="techJkServiceTask.zkp"
							title="质控品" value="<s:property value="techJkServiceTask.zkp"/>" />
						</td>
						<td class="label-title">标准品</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_bzp" name="techJkServiceTask.bzp"
							title="标准品" value="<s:property value="techJkServiceTask.bzp"/>" />
						</td>
					</tr>
					<tr class="cx3">
						<td class="label-title"><fmt:message
								key="biolims.common.machineNumber" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="hidden" size="20"
							readonly="readOnly" id="techJkServiceTask_instrument_name"
							value="<s:property value="techJkServiceTask.instrument.name"/>"
							readonly="readOnly" /> <input type="text"
							id="techJkServiceTask_instrument" readonly="readOnly"
							name="techJkServiceTask.instrument.id"
							value="<s:property value="techJkServiceTask.instrument.id"/>">
							<img
							alt='<fmt:message key="biolims.common.selectInstrumentCode"/>'
							id='showdeviceCode' src='${ctx}/images/img_lookup.gif'
							onClick="selectinstrument()" class='detail' /></td>
						<td class="label-title">检测位点数</td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="20" maxlength="20"
							id="techJkServiceTask_snp" name="techJkServiceTask.snp"
							title="检测位点数" value="<s:property value="techJkServiceTask.snp"/>" />
						</td>

					</tr>
				</table>
				<table width="100%">
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " colspan="9" width='50%' valign="top"
							align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td
													class="section_header_left text standard_section_label labelcolor"
													align="left"><span class="section_label"><fmt:message
															key="biolims.common.dataTransmission" /></span> <!-- 数据传输 --></td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<table class="frame-table">

					<tr>
						<td class="label-title"><fmt:message
								key="biolims.common.deliveryMethod" /></td>
						<td class="requiredcolumn" nowrap width="10px"></td>
						<td align="left"><input type="text" size="30"
							id="techJkServiceTask_deliveryMethod"
							name="techJkServiceTask.deliveryMethod" title="Delivery Method"
							value="<s:property value="techJkServiceTask.deliveryMethod"/>" />
						</td>

<%-- 						<td class="label-title"><fmt:message --%>
<%-- 								key="biolims.common.analysisDetails" /></td> --%>
<!-- 						<td class="requiredcolumn" nowrap width="10px"></td> --%>
<!-- 						<td align="left"><textarea class="text input false" -->
<!-- 								id="techJkServiceTask_analysisDetails" -->
<!-- 								name="techJkServiceTask.analysisDetails" tabindex="0" title="内容" -->
<!-- 								onblur="textbox_ondeactivate(event);" -->
<!-- 								onfocus="shared_onfocus(event,this)" -->
<!-- 								style="height: 150; width: 400"> -->
<%-- 								<s:property value="techJkServiceTask.analysisDetails" /> --%>
<!-- 							</textarea></td> -->
<!-- 					</tr> -->
				</table>
			</div>
			<table width="100%">
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " colspan="9" width='50%' valign="top"
						align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td
												class="section_header_left text standard_section_label labelcolor"
												align="left"><span class="section_label"><fmt:message
														key="biolims.common.other" /></span> <!-- 其他 --></td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<table class="frame-table">

				<tr>

					<td class="label-title"><fmt:message
							key="biolims.common.stateDescription" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techJkServiceTask_stateName"
						name="techJkServiceTask.stateName"
						title="<fmt:message key="biolims.common.stateDescription"/>"
						class="text input readonlytrue"
						value="<s:property value="techJkServiceTask.stateName"/>" /></td>

					<td class="label-title" style="display: none"><fmt:message
							key="biolims.common.state" /></td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="techJkServiceTask_state"
						name="techJkServiceTask.state"
						title="<fmt:message key="biolims.common.state"/>"
						value="<s:property value="techJkServiceTask.state"/>"
						style="display: none" /></td>

					<td class="label-title"><fmt:message key="biolims.common.note" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="60" maxlength="250"
						style="width: 152px" id="techJkServiceTask_note"
						name="techJkServiceTask.note"
						title="<fmt:message key="biolims.common.note"/>"
						value="<s:property value="techJkServiceTask.note"/>" />
						
						
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showAcceptUser = Ext.get('showAcceptUser');
showAcceptUser.on('click', 
 function loadAcceptUser(){
var win = Ext.getCmp('loadAcceptUser');
if (win) {win.close();}
var loadAcceptUser= new Ext.Window({
id:'loadAcceptUser',modal:true,title:'<fmt:message key="biolims.common.selectGroup"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=loadAcceptUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 loadAcceptUser.close(); }  }]  });     loadAcceptUser.show(); }
);
});
 function setloadAcceptUser(rec){
document.getElementById('techJkServiceTask_acceptUser').value=rec.get('id'); 
					document.getElementById('techJkServiceTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('loadAcceptUser')
if(win){win.close();}
}
</script>

					<td class="label-title"><fmt:message
							key="biolims.common.experimentalGroup" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left">
					
					<input type="text" size="15"
						readonly="readOnly" style="width: 152px"
						id="techJkServiceTask_acceptUser_name"
						value="<s:property value="techJkServiceTask.acceptUser.name"/>"
						readonly="readOnly" /> 
						
					<input type="hidden"
						id="techJkServiceTask_acceptUser"
						name="techJkServiceTask.acceptUser.id"
						value="<s:property value="techJkServiceTask.acceptUser.id"/>">
						<img alt='选择实验组' id='showAcceptUser'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
				</tr>
				<tr>

					<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun5(){
var win = Ext.getCmp('UserFun5');
if (win) {win.close();}
var UserFun5= new Ext.Window({
id:'UserFun5',modal:true,title:'<fmt:message key="biolims.common.selectTheCreatePerson"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun5' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun5.close(); }  }]  });     UserFun5.show(); }
);
});
 function setUserFun5(id,name){
 document.getElementById("techJkServiceTask_createUser").value = id;
document.getElementById("techJkServiceTask_createUser_name").value = name;
var win = Ext.getCmp('UserFun5')
if(win){win.close();}
}
</script>




					<td class="label-title"><fmt:message
							key="biolims.common.commandPerson" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="techJkServiceTask_createUser_name"
						class="text input readonlytrue"
						value="<s:property value="techJkServiceTask.createUser.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="techJkServiceTask_createUser"
						name="techJkServiceTask.createUser.id"
						value="<s:property value="techJkServiceTask.createUser.id"/>">
						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />     --%>
					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.commandTime" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techJkServiceTask_createDate"
						name="techJkServiceTask.createDate"
						title="<fmt:message key="biolims.common.commandTime"/>"
						class="text input readonlytrue" readonly="readonly"
						value="<s:date name="techJkServiceTask.createDate" format="yyyy-MM-dd"/>" />
					</td>


					<td class="label-title"><fmt:message key="biolims.common.testUserName"/></td>
					<td class="requiredcolumn" nowrap width="10px"><img	
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left">
					
					<input type="text" size="20"
						readonly="readOnly" id="techJkServiceTask_testUserOneName"
						name="techJkServiceTask.testUserOneName"
						value="<s:property value="techJkServiceTask.testUserOneName"/>"
						readonly="readOnly" />
						
						
					 <input type="hidden"
						id="techJkServiceTask_testUserOneId"
						name="techJkServiceTask.testUserOneId"
						value="<s:property value="techJkServiceTask.testUserOneId"/>">
						<img alt='选择实验员' id='showtestUser' onclick="testUser();"
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>




				</tr>
				<tr>


					<td class="label-title"><fmt:message key="biolims.common.endDate"/></td>
					<td class="requiredcolumn" nowrap width="10px"><img	
					class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techJkServiceTask_sequenceBillDate"
						name="techJkServiceTask.sequenceBillDate" title=""
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="techJkServiceTask.sequenceBillDate" format="yyyy-MM-dd"/>" />
					</td>

					<td class="label-title"><fmt:message key="biolims.common.labNotebook"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						id="techJkServiceTask_notebookId"
						name="techJkServiceTask.notebookId"
						title=""
						value="<s:property value="techJkServiceTask.notebookId"/>" /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.attachment" /></td>
					<td></td>
					<td
						title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>"
						id="doclinks_img"><span class="attach-btn"></span><span
						class="text label"><fmt:message
								key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
								key="biolims.common.attachment" /></span></td>
				</tr>
			</table>
			
			<div class="sx">
				<table width="100%">
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " colspan="9" width='50%' valign="top"
							align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td
													class="section_header_left text standard_section_label labelcolor"
													align="left"><span class="section_label"><fmt:message key="biolims.common.bioinformaticsAnalysis"/> </span> <!-- 测序信息 --></td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
				<table>
						<tbody>
							<tr>
								<td class="label-title" ><fmt:message key="biolims.common.analysisDetail"></fmt:message></td>
			               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
			                   	<td align="left" >
			                   		<textarea class="text input false" id="techJkServiceTask_analysisDetails" 
			                   		name="techJkServiceTask.analysisDetails" tabindex="0" title="" onblur="textbox_ondeactivate(event);" 
			                   		onfocus="shared_onfocus(event,this)" style="height:150;width:400"><s:property 
			                   		value="techJkServiceTask.analysisDetails"/></textarea>
			                   	</td>
			                
			           <script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showprojectId = Ext.get('showprojectId');
showprojectId.on('click', 
 function ProjectFun1234(){
var win = Ext.getCmp('ProjectFun1234');
if (win) {win.close();}
var ProjectFun1234= new Ext.Window({
id:'ProjectFun1234',modal:true,title:'<fmt:message key="biolims.common.selectItemNumber"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/project/showProjectSelectList.action?flag=ProjectFun1234' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 ProjectFun1234.close(); }  }]  });     ProjectFun1234.show(); }
);
});
 function setProjectFun1234(rec){
document.getElementById('techJkServiceTask_projectId').value=rec.get('id');
								document.getElementById('techJkServiceTask_projectId_name').value=rec.get('name');
var win = Ext.getCmp('ProjectFun1234')
if(win){win.close();}
}
</script>


			           <%-- <g:LayOutWinTag buttonId="showprojectId"
						title=''
						hasHtmlFrame="true"
						html="${ctx}/crm/project/showProjectSelectList.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('techJkServiceTask_projectId').value=rec.get('id');
								document.getElementById('techJkServiceTask_projectId_name').value=rec.get('name');" /> --%>

					<td class="label-title"><fmt:message key="biolims.common.projectId"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text"
						id="techJkServiceTask_projectId"
						name="techJkServiceTask.projectId.id"
						value="<s:property value="techJkServiceTask.projectId.id"/>">
						<input type="hidden" size="20" readonly="readOnly"
						id="techJkServiceTask_projectId_name"
						value="<s:property value="techJkServiceTask.projectId.name"/>"
						readonly="readOnly" /> <img
						alt='<fmt:message key="biolims.common.selectItemNumber"/>'
						id='showprojectId' src='${ctx}/images/img_lookup.gif'
						class='detail' onclick="xmCheck()"/></td>
				</tr>
			                   	
			                   	
							</tr>
						</tbody>
					</table>
					
					
				<table width="100%">
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " colspan="9" width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"><fmt:message key="biolims.common.attachment"/></span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
				</table>
				<table>
					<tbody>
						<tr>
						 	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
								<td title="" id="result_img" onclick="showResultFile()"><span 
									class="attach-btn"></span><span class="text label">
									<fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum2}&nbsp;&nbsp;
									<fmt:message key="biolims.common.attachment"/></span></td>
						</tr>
					</tbody>
				</table>
				</div>
			
			<input type="hidden" name="techJkServiceTaskItemJson"
				id="techJkServiceTaskItemJson" value="" /> <input type="hidden"
				id="id_parent_hidden"
				value="<s:property value="techJkServiceTask.id"/>" />
		</form>

		<div id="techJkServiceTaskItempage" width="100%" height:10px></div>

	</div>
</body>
</html>
