﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/technology/wk/techJkServiceTask.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString"> <input
			type="hidden" id="type" value="${requestScope.type}">
		<form id="searchForm">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techJkServiceTask_id" name="id" searchField="true"
						title="<fmt:message key="biolims.common.serialNumber"/>" /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.describe" /></td>
					<td align="left"><input type="text" size="50" maxlength="50"
						id="techJkServiceTask_name" name="name" searchField="true"
						title="<fmt:message key="biolims.common.describe"/>" /></td>
					<%-- <g:LayOutWinTag buttonId="showprojectId" title='<fmt:message key="biolims.common.selectItemNumber"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/projectSelect.action"
				isHasSubmit="false" functionName="ProjectFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_projectId').value=rec.get('id');
				document.getElementById('techJkServiceTask_projectId_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.itemCode"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_projectId_name" searchField="true"  name="projectId.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_projectId" name="techJkServiceTask.projectId.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectItemNumber"/>' id='showprojectId' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
				</tr>
				<%-- <tr>
			<g:LayOutWinTag buttonId="showcontractId" title='<fmt:message key="biolims.common.chooseTheContractCode"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/contractSelect.action"
				isHasSubmit="false" functionName="ContractFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_contractId').value=rec.get('id');
				document.getElementById('techJkServiceTask_contractId_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.contractCode"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_contractId_name" searchField="true"  name="contractId.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_contractId" name="techJkServiceTask.contractId.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheContractCode"/>' id='showcontractId' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showorderType" title='<fmt:message key="biolims.common.selectTaskType"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/dicTypeSelect.action"
				isHasSubmit="false" functionName="DicTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_orderType').value=rec.get('id');
				document.getElementById('techJkServiceTask_orderType_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.taskType"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_orderType_name" searchField="true"  name="orderType.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_orderType" name="techJkServiceTask.orderType.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTaskType"/>' id='showorderType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showproductId" title='<fmt:message key="biolims.common.selectBusinessTypes"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/productSelect.action"
				isHasSubmit="false" functionName="ProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_productId').value=rec.get('id');
				document.getElementById('techJkServiceTask_productId_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.businessTypes"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_productId_name" searchField="true"  name="productId.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_productId" name="techJkServiceTask.productId.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectBusinessTypes"/>' id='showproductId' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.dataType"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_dataType"
                   	 name="dataType" searchField="true" title="<fmt:message key="biolims.common.dataType"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.dateOfOrder"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startsequenceBillDate" name="startsequenceBillDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="sequenceBillDate1" name="sequenceBillDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endsequenceBillDate" name="endsequenceBillDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="sequenceBillDate2" name="sequenceBillDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.taskName"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_sequenceBillName"
                   	 name="sequenceBillName" searchField="true" title="<fmt:message key="biolims.common.taskName"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sequencingMethods"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_sequenceType"
                   	 name="sequenceType" searchField="true" title="<fmt:message key="biolims.common.sequencingMethods"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sequencingReadLong"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_sequenceLength"
                   	 name="sequenceLength" searchField="true" title="<fmt:message key="biolims.common.sequencingReadLong"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sequencingPlatform"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_sequencePlatform"
                   	 name="sequencePlatform" searchField="true" title="<fmt:message key="biolims.common.sequencingPlatform"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleSize"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_sampeNum"
                   	 name="sampeNum" searchField="true" title="<fmt:message key="biolims.common.sampleSize"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.speciesType"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_species"
                   	 name="species" searchField="true" title="<fmt:message key="biolims.common.speciesType"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalCycle"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_experimentPeriod"
                   	 name="experimentPeriod" searchField="true" title="<fmt:message key="biolims.common.experimentalCycle"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentDeadline"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startexperimentEndTime" name="startexperimentEndTime" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="experimentEndTime1" name="experimentEndTime##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endexperimentEndTime" name="endexperimentEndTime" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="experimentEndTime2" name="experimentEndTime##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalRequirements"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_experimentRemarks"
                   	 name="experimentRemarks" searchField="true" title="<fmt:message key="biolims.common.experimentalRequirements"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.analysisOfTheCycle"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_analysisCycle"
                   	 name="analysisCycle" searchField="true" title="<fmt:message key="biolims.common.analysisOfTheCycle"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.informationOnTheDeadline"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startanalysisEndTime" name="startanalysisEndTime" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="analysisEndTime1" name="analysisEndTime##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endanalysisEndTime" name="endanalysisEndTime" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="analysisEndTime2" name="analysisEndTime##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.informationRequirements"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_anaysisRemarks"
                   	 name="anaysisRemarks" searchField="true" title="<fmt:message key="biolims.common.informationRequirements"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.projectEmergencyRecognition"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_projectDifference"
                   	 name="projectDifference" searchField="true" title="<fmt:message key="biolims.common.projectEmergencyRecognition"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.mixProportion"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_mixRatio"
                   	 name="mixRatio" searchField="true" title="<fmt:message key="biolims.common.mixProportion"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.coefficient"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_coefficient"
                   	 name="coefficient" searchField="true" title="<fmt:message key="biolims.common.coefficient"/>"    />
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >readsone</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_readsOne"
                   	 name="readsOne" searchField="true" title="readsone"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<!-- <td class="label-title" >readstwo</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_readsTwo"
                   	 name="readsTwo" searchField="true" title="readstwo"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >index1</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_index1"
                   	 name="index1" searchField="true" title="index1"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >index2</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_index2"
                   	 name="index2" searchField="true" title="index2"    />
                   	
					
 
                  
                   	
                   	  
                   	</td> -->
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.expectedDensity"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_density"
                   	 name="density" searchField="true" title="<fmt:message key="biolims.common.expectedDensity"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showorders" title='<fmt:message key="biolims.common.selectingTheOrder"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_orders').value=rec.get('id');
				document.getElementById('techJkServiceTask_orders_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.orderPeople"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_orders_name" searchField="true"  name="orders.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_orders" name="techJkServiceTask.orders.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectingTheOrder"/>' id='showorders' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showprojectLeader" title='<fmt:message key="biolims.common.selectTheProjectDirector"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_projectLeader').value=rec.get('id');
				document.getElementById('techJkServiceTask_projectLeader_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.projectLeader"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_projectLeader_name" searchField="true"  name="projectLeader.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_projectLeader" name="techJkServiceTask.projectLeader.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheProjectDirector"/>' id='showprojectLeader' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showexperimentLeader" title='<fmt:message key="biolims.common.chooseTheExperimenter"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_experimentLeader').value=rec.get('id');
				document.getElementById('techJkServiceTask_experimentLeader_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.experiment"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_experimentLeader_name" searchField="true"  name="experimentLeader.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_experimentLeader" name="techJkServiceTask.experimentLeader.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showexperimentLeader' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showanalysisLeader" title='<fmt:message key="biolims.common.selectInformationDirector"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_analysisLeader').value=rec.get('id');
				document.getElementById('techJkServiceTask_analysisLeader_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.informationDirector"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_analysisLeader_name" searchField="true"  name="analysisLeader.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_analysisLeader" name="techJkServiceTask.analysisLeader.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectInformationDirector"/>' id='showanalysisLeader' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showtechProjectTask" title='<fmt:message key="biolims.common.selectTheRelevantProject"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/techProjectTaskSelect.action"
				isHasSubmit="false" functionName="TechProjectTaskFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_techProjectTask').value=rec.get('id');
				document.getElementById('techJkServiceTask_techProjectTask_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.relatedProjects"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_techProjectTask_name" searchField="true"  name="techProjectTask.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_techProjectTask" name="techJkServiceTask.techProjectTask.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheRelevantProject"/>' id='showtechProjectTask' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="250" id="techJkServiceTask_note"
                   	 name="note" searchField="true" title="<fmt:message key="biolims.common.note"/>"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/technology/wk/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('techJkServiceTask_createUser').value=rec.get('id');
				document.getElementById('techJkServiceTask_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="techJkServiceTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="techJkServiceTask_createUser" name="techJkServiceTask.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheCreatePerson"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr> --%>
				<tr>
					<g:LayOutWinTag buttonId="showconfirmUser"
						title='<fmt:message key="biolims.common.selectTheAuditor"/>'
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						documentId="techJkServiceTask_confirmUser"
				documentName="techJkServiceTask_confirmUser_name"/>
					<td class="label-title"><fmt:message
							key="biolims.common.auditor" /></td>
					<td align="left"><input type="text" size="20"
						id="techJkServiceTask_confirmUser_name" searchField="true"
						name="confirmUser.name" value="" class="text input" /> <input
						type="hidden" id="techJkServiceTask_confirmUser"
						name="techJkServiceTask.confirmUser.id" value=""> <img
						alt='<fmt:message key="biolims.common.selectTheAuditor"/>'
						id='showconfirmUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.auditDate" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startconfirmDate" name="startconfirmDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="confirmDate1" name="confirmDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endconfirmDate" name="endconfirmDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="confirmDate2" name="confirmDate##@@##2"
						searchField="true" /></td>

				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.state" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="techJkServiceTask_state" name="state" searchField="true"
						title="<fmt:message key="biolims.common.state"/>" /></td>
					<%-- <td class="label-title" ><fmt:message key="biolims.common.stateDescription"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="techJkServiceTask_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.stateDescription"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td> --%>
				</tr>
			</table>
		</form>
	</div>
	<div id="show_techJkServiceTask_div"></div>
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value='' />
	</form>
	<div id="show_techJkServiceTask_tree_page"></div>
</body>
</html>



