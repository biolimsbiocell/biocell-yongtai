﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/js/technology/wk/techJkServiceTaskItem.js"></script>
</head>
<body>
	<div id="techJkServiceTaskItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_lane_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>GC%</span></td>
				<td><input id="gc"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>phix%</span></td>
				<td><input id="phix"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>数据量</span></td>
				<td><input id="dataNum"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>Q30</span></td>
				<td><input id="q30"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>混合Lane</span></td>
				<td><input id="blendLane"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>混合Flowcell</span></td>
				<td><input id="blendFc"/></td>
			</tr>
		</table>
		</div>
		<div id="bat_inwardCode_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>内部项目号</span></td>
				<td><input id="inwardCode"/></td>
			</tr>
		</table>
		</div>
		<div id="bat_orderType_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>质检类型</span></td>
                <td><select id="orderType" style="width:100">
    					<option value="1" <s:if test="orderType==1">selected="selected" </s:if>>DNA抽提</option>
    					<option value="2" <s:if test="orderType==2">selected="selected" </s:if>>RNA抽提</option>
						<option value="3" <s:if test="orderType==3">selected="selected" </s:if>>DNA QC</option>
    					<option value="4" <s:if test="orderType==4">selected="selected" </s:if>>RNA QC</option>
						<option value="5" <s:if test="orderType==5">selected="selected" </s:if>>LIBRARY QC</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>
