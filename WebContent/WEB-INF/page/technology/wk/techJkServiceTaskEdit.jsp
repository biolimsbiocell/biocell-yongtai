<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
#btn_submit{
	display:none;
}
#modal-body{
	display:none;
}
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}


.zjrwd {
	display: none;
}
.jkrwd {
	display: none;
}
.sjrwd {
	display: none;
}
.sjrwd {
	display: none;
}

#box2,#box3,#box1,#box4 {
	border-radius: 10px;
	overflow: hidden;
	transition: all 1s;
	width: 80%;
	margin: 10px auto;
	color: #fff;
}

#box2 {
	background: #8FBAF3;
}

#box3 {
	background: #28CC9E;
}

#box1 {
	background: #878ECD;
}

#box4 {
	background: #4a55b4;
}

#box3:hover {
	box-shadow: 15px 15px 15px #000;
}

#box2:hover {
	box-shadow: 15px 15px 15px #000;
}

#box1:hover {
	box-shadow: 15px 15px 15px #000;
}

#box4:hover {
	box-shadow: 15px 15px 15px #000;
}

.modal-body .box-body div {
	margin: -34px 13px;
	text-align: center;
}

.modal-body .box-body div a {
	color: #fff;
	display: block;
	margin-top: 23%;
	text-decoration: none;
}

.modal-body .box-body div i {
	font-size: 100px;
}

.modal-body .box-body div p {
	font-weight: 900;
}

.chosedType {
	border: 2px solid #BAB5F6 !important;
}

.input-group {
	margin-top: 10px;
}

.scientific {
	display: none;
}

.box-title small span {
	margin-right: 20px;
}

#btn_task{
	display:none;
}
</style>
<style type="text/css">
						.row {
				margin-bottom: 5px;
			}
					.input-group .input-group-addon {
				background-color: rgb(238, 238, 238);
			}
			
			.input-group-btn>.btn {
				border-radius: 0 3px 3px 0;
				height: 34px;
			}
			
			.layui-layer-content {
				padding: 0 20px;
			}
			
			.table-responsive th,
			.table-responsive td {
				white-space: nowrap;
			}
			
			.dt-button-collection {
				font-size: 12px;
				max-height: 260px;
				overflow: auto;
			}
			
			.box-title small span {
				margin-right: 20px;
			}
		</style>
</head>

<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
<body style="height: 94%">
	<%-- <div class="modal fade" id="techJkServiceTaskModal" tabindex="-1"
		role="dialog" aria-hidden="flase" data-backdrop="false">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			
					</button>
				<!-- <div class="modal-header text-center">
					
					<button class="btn btn-warning btn-lg chosedType">
						建库任务单
					</button>
					<button class="btn btn-warning btn-lg">
						质检任务单
					</button>
					<button class="btn btn-warning btn-lg">
						上机任务单
					</button>
					<button class="btn btn-warning btn-lg">
						生信任务单
					</button>
				</div> -->
				<div class="modal-body" id="modal-body">
					<div class="row">
						<div class="col-sm-3 col-xs-12">
							<div class="box" id="box2">
								<div class="box-body" style="height: 170px;">
									<div id="zjrwd">
										<a href="####"> <i class="fa  fa-clipboard"></i>
											<p>
												<fmt:message
												key="biolims.common.qcTask" />
												
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="box" id="box1">
								<div class="box-body" style="height: 170px;">
									<div id="jkrwd">
										<a href="###"> <i class="fa  fa-clipboard"></i>
											<p>
												
												<fmt:message
												key="biolims.common.scienceServiceBuild" />
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="box" id="box3">
								<div class="box-body" style="height: 170px;">
									<div id="sjrwd">
										<a href="####"> <i class="fa fa-clipboard"></i>
											<p>
												
												<fmt:message
												key="biolims.common.sequenceTask" />
												
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12">
							<div class="box" id="box4">
								<div class="box-body" style="height: 170px;">
									<div id="sxrwd">
										<a href="####"> <i class="fa fa-clipboard"></i>
											<p>
											
												<fmt:message
												key="biolims.common.bioinfoTask" />
												
												
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> --%>
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
		<%-- <input type="hidden" id="storageOut_name"
			value="<s:property value="storageOut.name"/>"> --%> 
			<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
						<strong id="techJkServiceTaskTitle"
							type="<s:property value="techJkServiceTask.type"/>">任务单</strong><small style="color: #fff;"><fmt:message
												key="biolims.common.serialNumber" />: <span
								id="techJkServiceTask_id"><s:property value="techJkServiceTask.id" /></span>
							<fmt:message key="biolims.common.commandPerson" />: <span
								userId="<s:property value="techJkServiceTask.createUser.id"/>"
								id="techJkServiceTask_createUser"><s:property
										value="techJkServiceTask.createUser.name" /></span><fmt:message
												key="biolims.common.commandTime" />: <span
								id="techJkServiceTask_createDate"><s:date name="techJkServiceTask.createDate" format="yyyy-MM-dd "/></span> <fmt:message
												key="biolims.common.stateDescription" />: <span
								state="<s:property value="techJkServiceTask.state"/>"
								id="headStateName"><s:property
										value="techJkServiceTask.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<%-- <input type="hidden" name="techJkServiceTask.type" value="${requestScope.type}" /> --%>
								<input type="hidden" name="id" 
								changelog="<s:property value="techJkServiceTask.id"/>"
								value="<s:property value="techJkServiceTask.id" />" />
								<input type="hidden"  name="createUser.id" 
								changelog="<s:property value="techJkServiceTask.createUser.id"/>"
								value="<s:property value="techJkServiceTask.createUser.id" />" />
								<input type="hidden"  name="createDate" 
								changelog="<s:date name="techJkServiceTask.createDate" format="yyyy-MM-dd "/>"
								value="<s:date name="techJkServiceTask.createDate" format="yyyy-MM-dd "/>" />
								<input type="hidden" name="state" id="techJkServiceTask_state"
								changelog="<s:property value="techJkServiceTask.state"/>"
								value="<s:property value="techJkServiceTask.state"/>" />
								<input type="hidden"  name="stateName" 
								changelog="<s:property value="techJkServiceTask.stateName"/>"
								value="<s:property value="techJkServiceTask.stateName" />" />
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">

											<span class="input-group-addon"> <fmt:message
												key="biolims.common.describe" />
											</span> <input type="text" size="20" maxlength="25"
												id="techJkServiceTask_name" name="name"
												changelog="<s:property value="techJkServiceTask.name"/>"
												class="form-control"
												value="<s:property value="techJkServiceTask.name"/>" />

										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">人员组</span>
											<input type="hidden" size="20" id="techJkServiceTask_acceptUser_id"
												name="acceptUser-id"
												value="<s:property value=" techJkServiceTask.acceptUser.id "/>"
												class="form-control" /> 
											<input type="text" size="20" readonly="readonly"
												id="techJkServiceTask_acceptUser_name" name="acceptUser-name"
												changelog="<s:property value="techJkServiceTask.acceptUser.name"/>"
												value="<s:property value=" techJkServiceTask.acceptUser.name "/>"
												class="form-control" /> 
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" onclick="shiyanzu()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
												key="biolims.common.note" />	
											</span>
											<input type="text" size="20" maxlength="25"
											id="techJkServiceTask_note" name="note"
											class="form-control"
											changelog="<s:property value="techJkServiceTask.note"/>"
											value="<s:property value="techJkServiceTask.note"/>" />
										</div>
									</div>
									<%-- <div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
												key="biolims.common.experimenter" /></span>
											<input type="hidden" size="20" id="techJkServiceTask_testUserOneId"
												name="testUserOneId"
												value="<s:property value=" techJkServiceTask.testUserOneId "/>"
												class="form-control" /> 
											<input type="text" size="20" readonly="readonly"
												id="techJkServiceTask_testUserOneName"  name="testUserOneName"
												changelog="<s:property value="techJkServiceTask.testUserOneName"/>"
												value="<s:property value=" techJkServiceTask.testUserOneName "/>"
												class="form-control" /> 
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" onclick="shiyanyuan()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div> --%>
									<%-- <div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">

											<span class="input-group-addon"> <fmt:message
												key="biolims.wk.sequenceBillName" /></span>
												<input type="hidden" size="20" maxlength="25"
												id="techJkServiceTask_type" name="type"
												class="form-control"
												value="<s:property value="techJkServiceTask.type"/>" />
											<input type="text" size="20" readonly="readOnly" id="techJkServiceTask_typeName"  class="form-control" />

										</div>
									</div> --%>
									<%-- <div class="col-md-4 col-sm-6 col-xs-12 jkrwd">
										<div class="input-group">

											<span class="input-group-addon">
											<fmt:message
												key="biolims.common.typePlatform" />	
											</span>
									<select class="form-control" id="techJkServiceTask_cxType" name="cxType" onchange="cxTypeChange();">
											<option value=""
												<s:if test="techJkServiceTask.cxType==''">selected="selected"</s:if>>
												Please
											</option>
											<option value="1"
												<s:if test="techJkServiceTask.cxType==1">selected="selected"</s:if>>
												Illumina Platform
											</option>
											<option value="2"
												<s:if test="techJkServiceTask.cxType==2">selected="selected"</s:if>>
												lontorrent Platform
											</option>
											<option value="3"
												<s:if test="techJkServiceTask.cxType==3">selected="selected"</s:if>>
												Affymetrix Platform
											</option>
											<option value="4"
												<s:if test="techJkServiceTask.cxType==4">selected="selected"</s:if>>
												Q-PCR Platform
											</option>
											<option value="5"
												<s:if test="techJkServiceTask.cxType==5">selected="selected"</s:if>>
												fluidigm Platform
											</option>
											<option value="6"
												<s:if test="techJkServiceTask.cxType==6">selected="selected"</s:if>>
												MLPA Platform
											</option>
										</select>

										</div>
									</div> --%>
									<%-- <div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<button type="button" class="btn btn-info" onclick="fileUp2()"><fmt:message key="biolims.common.uploadAttachment"/></button>
												<span class="text"><fmt:message
													key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
													key="biolims.common.attachment" /></span>

												<button type="button" class="btn btn-info" onclick="fileView2()"><fmt:message key="biolims.report.checkFile"/></button>
											</div>
									</div> --%>
								</div>
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
												key="biolims.common.endDate" /></span>
											<input class="form-control " type="text" size="20" class="Wdate"
											maxlength="25" id="techJkServiceTask_sequenceBillDate" name="sequenceBillDate"
											changelog="<s:date name=" techJkServiceTask.sequenceBillDate " format="yyyy-MM-dd "/>"
											value="<s:date name=" techJkServiceTask.sequenceBillDate " format="yyyy-MM-dd "/>" />
										</div>
									</div>
							
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
									</div>
								</div>
							
								<div id="cxxx" class="jkrwd">
									<div class="row" style="margin-left:-10px;margin-right:-10px">
										<div class="panel"
											style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
											<div class="panel-heading text-left">
												<h3 class="panel-title" style="font-family: 黑体;">
													<i class="glyphicon glyphicon-bookmark"></i> <fmt:message
												key="biolims.common.projectInformation" />
												</h3>
											</div>
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon">
													<fmt:message
												key="biolims.common.sequencingPlatform" />
												</span>
												<%-- <input type="hidden" name="sequencePlatform " changelog="<s:property value="techJkServiceTask.sequencePlatform"/>" id="techJkServiceTask_sequencePlatform " value="<s:property value=" techJkServiceTask.sequencePlatform " />" />  --%> 
												<select class="form-control" name="sequencePlatform">
														<option value=""
															<s:if test="techJkServiceTask.sequencePlatform==''">selected="selected"</s:if>>
															Please
														</option>
														<c:forEach var="listPlatForm" items="${listPlatForm}">
															<option value="${listPlatForm.name}"
																<c:if test="${techJkServiceTask.sequencePlatform==listPlatForm.name}">selected="selected"</c:if>>${listPlatForm.name}</option>
														</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon">
													<fmt:message
												key="biolims.common.sequencingType" />
												</span>
												<%-- <input type="hidden" name="first " changelog="<s:property value="techJkServiceTask.sequenceType"/>" id="techJkServiceTask_sequenceType " value="<s:property value=" techJkServiceTask.sequenceType " />" />   --%>
												<select class="form-control" name="sequenceType">
														<option value=""
															<s:if test="techJkServiceTask.sequenceType==''">selected="selected"</s:if>>
															Please
														</option>
														<c:forEach var="listReadType" items="${listReadType}">
															<option value="${listReadType.name}"
																<c:if test="${techJkServiceTask.sequenceType==listReadType.name}">selected="selected"</c:if>>${listReadType.name}</option>
														</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon">
													<fmt:message
												key="biolims.common.sequencingReadLong" />
												</span>
												<%-- <input type="hidden" name="first " changelog="<s:property value="techJkServiceTask.sequenceLength"/>" id="techJkServiceTask_sequenceLength " value="<s:property value=" techJkServiceTask.sequenceLength " />" />   --%>
												<select class="form-control" name="sequenceLength">
														<option value=""
															<s:if test="techJkServiceTask.sequenceLength==''">selected="selected"</s:if>>
															Please
														</option>
														<c:forEach var="listSequencingCycle" items="${listSequencingCycle}">
															<option value="${listSequencingCycle.name}"
																<c:if test="${techJkServiceTask.sequenceLength==listSequencingCycle.name}">selected="selected"</c:if>>${listSequencingCycle.name}</option>
														</c:forEach>
												</select>
											</div>
										</div>
									</div>
									<div class="row cx1">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.QCRequirements" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_qcRequirements" name="qcRequirements"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.qcRequirements"/>"
												value="<s:property value="techJkServiceTask.qcRequirements"/>" />
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.chipType" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_chipType" name="chipType"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.chipType"/>"
												value="<s:property value="techJkServiceTask.chipType"/>" />
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.samplePreparation" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_sampleZbff" name="sampleZbff"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.sampleZbff"/>"
												value="<s:property value="techJkServiceTask.sampleZbff"/>" />
											</div>
										</div>
									</div>
									<div class="row cx2">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.detectionKit" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_testKit" name="testKit"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.testKit"/>"
												value="<s:property value="techJkServiceTask.testKit"/>" />
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.gene" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_gene" name="gene"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.gene"/>"
												value="<s:property value="techJkServiceTask.gene"/>" />
											</div>
										</div>
									</div>
									<div class="row cx2">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.QualityProduct" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_zkp" name="zkp"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.zkp"/>"
												value="<s:property value="techJkServiceTask.zkp"/>" />
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.standardProduct" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_bzp" name="bzp"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.bzp"/>"
												value="<s:property value="techJkServiceTask.bzp"/>" />
											</div>
										</div>
									</div>
									<div class="row cx3">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.instrument" /></span>
												<input type="hidden" size="20" id="techJkServiceTask_instrument_id"
													name="instrument.id"
													value="<s:property value=" techJkServiceTask.instrument.id "/>"
													class="form-control" /> 
												<input type="text" size="20" readonly="readonly"
													id="techJkServiceTask_instrument_name" 
													changelog="<s:property value="techJkServiceTask.instrument.name"/>"
													value="<s:property value=" techJkServiceTask.instrument.name "/>"
													class="form-control" /> 
												<span class="input-group-btn">
													<button class="btn btn-info" type="button" onclick="showInstrument()">
														<i class="glyphicon glyphicon-search"></i>
													</button>
												</span>
											</div>
										</div>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.detectionPoints" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_snp" name="snp"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.snp"/>"
												value="<s:property value="techJkServiceTask.snp"/>" />
											</div>
										</div>
									</div>
									<div class="row" style="margin-left:-10px;margin-right:-10px">
										<div class="panel"
											style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
											<div class="panel-heading text-left">
												<h3 class="panel-title" style="font-family: 黑体;">
													<i class="glyphicon glyphicon-bookmark"></i><fmt:message key="biolims.common.dataTransmission" />
												</h3>
											</div>
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.deliveryMethod" /></span>
												<input type="text" size="20" maxlength="25"
												id="techJkServiceTask_deliveryMethod" name="deliveryMethod"
												class="form-control"
												changelog="<s:property value="techJkServiceTask.deliveryMethod"/>"
												value="<s:property value="techJkServiceTask.deliveryMethod"/>" />
											</div>
										</div>
									</div>
								</div>
								<div id="sxfx" class="sjrwd">
									<div class="row" style="margin-left:-10px;margin-right:-10px">
										<div class="panel"
											style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
											<div class="panel-heading text-left">
												<h3 class="panel-title" style="font-family: 黑体;">
													<i class="glyphicon glyphicon-bookmark"></i><fmt:message key="biolims.common.bioinformaticsAnalysis" />
												</h3>
											</div>
										</div>
									</div>
									<br />
									<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.projectId" /></span>
												<input type="hidden" size="20" id="techJkServiceTask_projectId_id"
													name="projectId.id"
													value="<s:property value="techJkServiceTask.projectId.id"/>"
													class="form-control" /> 
												<input type="text" size="20" readonly="readonly"
													id="techJkServiceTask_projectId_name" 
													changelog="<s:property value="techJkServiceTask.projectId.name"/>"
													value="<s:property value="techJkServiceTask.projectId.name"/>"
													class="form-control" /> 
												<span class="input-group-btn">
													<button class="btn btn-info" type="button" onclick="xiangmu()">
														<i class="glyphicon glyphicon-search"></i>
													</button>
												</span>
											</div>
								
										</div>
						
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div class="input-group">
												<span class="input-group-addon"><fmt:message key="biolims.common.analysisDetails"  /></span>
											<textarea placeholder="<fmt:message key="biolims.common.placeholder"/>" name="analysisDetails" class="layui-textarea">${techJkServiceTask.analysisDetails}</textarea>
								
											</div>
										</div>
								
									</div>
								</div>
							</form>
						</div>

	</br>






						<!--库存主数据-->
						<div id="leftDiv" class="col-md-5 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
									待下达订单</h3>
								</div>
								<div class="box-body ipadmini" id="lefttable">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageOutAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-md-7 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
									<fmt:message
												key="biolims.common.detailedListOfTechnicalServicesAndExperimentTask" /></h3>
									<button id="btn_task" class="btn btn-info btn-sm" onclick="viewTask()">查看任务</button>
								</div>
								<div class="box-body ipadmini" id="righttable">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="storageOutItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		</section>
	</div>
	<!-- <div style="display: none" id="batch_data" class="input-group">
		<span class="input-group-addon bg-aqua">产物数量</span> <input
			type="number" id="productNum" class="form-control"
			placeholder="" value="">
	</div> -->
	<script type="text/javascript"
		src="${ctx}/js/technology/wk/techJkServiceTaskEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/technology/wk/techJkServiceAllLeft.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/technology/wk/techJkServiceRight.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>