﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/analysis/data/dateTaskCnvItem.js"></script>
</head>
<body>
	<div id="bat_mutationClass_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.mutationsInTheClassification"/></span></td>
                <td><select id="mutationClass"  style="width:100 ">
               			<option value="1"><fmt:message key="biolims.common.germLineMutations"/></option>
    					<option value="2"><fmt:message key="biolims.common.polymorphismMutation"/></option>
    					<option value="3"><fmt:message key="biolims.common.tumorMutation"/></option>
					</select>
                </td>
			</tr>
		</table>
		</div>
		<div id="bat_validState_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.mutationsInTheClassification"/></span></td>
                <td><select id="validState"  style="width:100 ">
               			<option value="0"><fmt:message key="biolims.common.invalid"/></option>
    					<option value="1"><fmt:message key="biolims.common.effective"/></option>
					</select>
                </td>
			</tr>
		</table>
		</div>
		<div id="bat_accordance_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.whetherToConsistent"/></span></td>
                <td><select id="accordance"  style="width:100 ">
               			<option value="0"><fmt:message key="biolims.common.dontAgree"/> </option>
    					<option value="1"><fmt:message key="biolims.common.consistent"/></option>
					</select>
                </td>
			</tr>
		</table>
		</div>
	<div id="bat_mutationFiction_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.mutationsInClassification"/></span></td>
                <td><select id="mutationFiction"  style="width:100 ">
               			<option value="1">level1</option>
    					<option value="2">level2</option>
    					<option value="3">level3</option>
    					<option value="4">level4</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
		<div id="bat_copyNumberVaration_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.copyNumberVariation"/></span></td>
                <td><select id="copyNumberVaration"  style="width:100 ">
               			<option value="1"><fmt:message key="biolims.common.numberOfGeneAmplification"/></option>
    					<option value="2"><fmt:message key="biolims.common.copyNumberMissing"/></option>
    					
					</select>
                </td>
			</tr>
		</table>
		</div>	
		<div id="bat_mutationType_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.mutationsInTheClassification"/></span></td>
                <td><select id="mutationType"  style="width:100 ">
               			<option value="1"><fmt:message key="biolims.common.geneFusion"/></option>
    					<option value="2"><fmt:message key="biolims.common.geneCopyNumberVariation"/></option>
    					<option value="3"><fmt:message key="biolims.common.synonymousMutations"/></option>
    					<option value="4"><fmt:message key="biolims.common.missenseMutation"/></option>
    					<option value="5"><fmt:message key="biolims.common.insertIsntFrameshiftMutations"/></option>
    					<option value="6"><fmt:message key="biolims.common.insertTheFrameshiftMutations"/></option>
    					<option value="7"><fmt:message key="biolims.common.missingIsntFrameshiftMutations"/></option>
    					<option value="8"><fmt:message key="biolims.common.missingFrameshiftMutations"/></option>
    					<option value="9"><fmt:message key="biolims.common.nonsenseMutation"/></option>
    					<option value="10"><fmt:message key="biolims.common.shearMutation"/></option>
    					<option value="11"><fmt:message key="biolims.common.otherMutation"/></option>
					</select>
                </td>
			</tr>
		</table>
		</div>
	<div id="dateTaskCnvItemdiv"></div>
	<div id="bat_uploadcsvCnv_div" style="display: none">
		<input type="file" name="file" id="file-cnvuploadcsv"><fmt:message key="biolims.common.uploadFile"/>
	</div>
</body>
</html>
