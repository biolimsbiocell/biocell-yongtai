﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>
<script type="text/javascript" src="${ctx}/js/analysis/data/mutationAuditItem.js"></script>

</head>
<body>
<input type="hidden" id="sgtid" value="${requestScope.selectId}">
	<div id="mutationItemdiv"></div>
	<div style="display:none">
		<select id="grid-method"> 
			<option value="">请选择</option>
			<option value="1">通过</option>
			<option value="0">不通过</option>
		</select>
	</div>
	<div id="bar_rna" style="display: none">
		<table>
			<tr>
				<td class="label-title">处理方式</td>
				<td>
					<select id="grid-method">
						<option value="">请选择</option>
						<option value="1">通过</option>
						<option value="0">不通过</option>
					</select>
				</td>
			</tr>
		</table>
		<div id="bat_states_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>审核状态</span></td>
                <td><select id="states"  style="width:100 ">
               			<option value="1" selected="selected">审核不通过</option>
    					<option value="2" >审核通过</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
			<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
	</div>
	 <div id="tabs">
            <ul>
			<li><a href="#mutationSvnItempage">碱基突变表</a></li>
			<li><a href="#mutationSvItempage">基因融合突变表</a></li>
			<li><a href="#mutationCnvItempage">肿瘤突变表</a></li>
			
           	</ul> 
			<div id="mutationSvnItempage" width="100%" height:10px></div>
			<div id="mutationSvItempage" width="100%" height:10px></div>
			<div id="mutationCnvItempage" width="100%" height:10px></div>
			
			</div>
        </div>
</body>
</html>


