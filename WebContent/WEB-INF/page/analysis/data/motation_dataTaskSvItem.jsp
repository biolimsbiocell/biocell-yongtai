﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/analysis/data/mutationAuditSvItem.js"></script>
</head>
<body>
<input type="hidden" id="sgtid" value="${requestScope.sgtid}">
	<div id="bat_mutationClass_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>突变分类</span></td>
                <td><select id="mutationClass"  style="width:100 ">
               			<option value="1">种系突变</option>
    					<option value="2">多态性突变</option>
    					<option value="3">肿瘤突变</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
		<div id="bat_validState_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>突变分类</span></td>
                <td><select id="validState"  style="width:100 ">
               			<option value="0">无效</option>
    					<option value="1">有效</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
	<div id="bat_mutationFiction_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>突变分级</span></td>
                <td><select id="mutationFiction"  style="width:100 ">
               			<option value="1">level1</option>
    					<option value="2">level2</option>
    					<option value="3">level3</option>
    					<option value="4">level4</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
		<div id="bat_mutationStatus_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>突变状态</span></td>
                <td><select id="mutationStatus"  style="width:100 ">
               			<option value="1">纯合多态性</option>
    					<option value="2">杂合多态性</option>
    					
					</select>
                </td>
			</tr>
		</table>
		</div><div id="bat_mutationType_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>突变类型</span></td>
                <td><select id="mutationType"  style="width:100 ">
               			<option value="1">基因融合</option>
    					<option value="2">基因拷贝数变异</option>
    					<option value="3">同义突变</option>
    					<option value="4">错义突变</option>
    					<option value="5">插入非移码突变</option>
    					<option value="6">插入移码突变</option>
    					<option value="7">缺失非移码突变</option>
    					<option value="8">缺失移码突变</option>
    					<option value="9">无义突变</option>
    					<option value="10">剪切突变</option>
    					<option value="11">其他突变</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
		
	<div id="mutationSvItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
</body>
</html>
