<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.attachment"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=dataTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/data/dataTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="dataTaskTempPage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dataTask_id"
                   	 name="dataTask.id" title="<fmt:message key="biolims.common.serialNumber"/>" 	readonly = "readOnly" class="text input readonlytrue"   
                   	   
	value="<s:property value="dataTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dataTask_name"
                   	 name="dataTask.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="dataTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<%-- <input type="text" size="20" maxlength="25" id="dataTask_createDate"
                   	 name="dataTask.createDate" title="下达时间"
                   	  value="<s:date name="dataTask.createDate" format="yyyy-MM-dd"/>"
                   	                      	  /> --%>
                   	 <input type="text" size="20" maxlength="25"  id="dataTask_createDate" 
                   	  name="dataTask.createDate"  title="出生日期"  readonly="readOnly"   class="text input readonlytrue"   
                   	  value="<s:date name="dataTask.createDate" format="yyyy-MM-dd"/>"   />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUsers = Ext.get('showcreateUsers');
showcreateUsers.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'<fmt:message key="biolims.common.selectTheCreatePerson"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(id,name){
 document.getElementById("dataTask_createUser").value = id;
document.getElementById("dataTask_createUser_name").value = name;
var win = Ext.getCmp('UserFun');
if(win){win.close();}
}
</script>


				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="dataTask_createUser_name"  value="<s:property value="dataTask.createUser.name"/>" class="text input readonlytrue"   />
 						<input type="hidden" id="dataTask_createUser" name="dataTask.createUser.id"  value="<s:property value="dataTask.createUser.id"/>" > 
<%--  						<img alt='选择下达人'  id='showcreateUsers' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
			
                   	  
                   	<td class="label-title" style="display: none"><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none"></td>            	 	
                   	<td align="left"  style="display: none">
                   	<input type="text" size="20" maxlength="25" id="dataTask_state"
                   	 name="dataTask.state" title="<fmt:message key="biolims.common.state"/>" style="display: none"
                   	   
	value="<s:property value="dataTask.state"/>"
                   	  />
                   	  
                   	</td> 
			
			
               	 	<td class="label-title"  ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"   ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dataTask_stateName"  readonly="readOnly"  class="text input readonlytrue" 
                   	 name="dataTask.stateName" title="<fmt:message key="biolims.common.stateDescription"/>"
                   	   
	value="<s:property value="dataTask.stateName"/>"
		
                   	 
                   	  />
                   	  
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.flowCellNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dataTask_fcnumber"
                   	 name="dataTask.fcnumber" title="<fmt:message key="biolims.common.flowCellNumber"/>"                	   
						value="<s:property value="dataTask.fcnumber"/>" />
                   	  <img alt='<fmt:message key="biolims.common.selectFlowCellNumber"/>' id='showFc' src='${ctx}/images/img_lookup.gif' 
                   	  	class='detail'   onClick="showfcFun()" />
                   	</td>
                   	
			</tr>
			<tr>
					
               	 	
                   	
                   	<td class="label-title" ></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="20" maxlength="25" id="dataTask_knumber"
                   	 name="dataTask.knumber" title=""
                   	   
	value="<s:property value="1"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            
            
            <input type="hidden" name="dataTaskSvnItemJson" id="dataTaskSvnItemJson" value="" />
            <input type="hidden" name="dataTaskSvItemJson" id="dataTaskSvItemJson" value="" />
            <input type="hidden" name="dateTaskCnvItemJson" id="dateTaskCnvItemJson" value="" />
            <input type="hidden" name="dateTaskItemJson" id="dateTaskItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="dataTask.id"/>" />
            </form>
            <div id="tabss">
	           <ul>
	          	 <li><a href="#dataTaskItempage"><fmt:message key="biolims.common.scheduleOfDataAnalysis"/></a></li>
				 <!-- <li><a href="#personelpage">样本信息表</a></li> -->
	           	</ul> 
	           	<div id="dataTaskItempage" style="width:100%;height:100%"></div>
				<!-- <div id="personelpage" style="width:100%;height:50%"></div> -->
			</div>
        	
            <div id="tabs">
            <ul>
			<li><a href="#dataTaskSvnItempage"><fmt:message key="biolims.common.tableBaseMutations"/></a></li>
			<li><a href="#dataTaskSvItempage"><fmt:message key="biolims.common.fusionGeneMutationsInTheTable"/></a></li>
			<li><a href="#dateTaskCnvItempage"><fmt:message key="biolims.common.copyNumberVariation"/></a></li>
			<!-- <li><a href="#personelpage">样本信息表</a></li> -->
			
           	</ul> 
			<div id="dataTaskSvnItempage" width="100%" height:10px></div>
			<div id="dataTaskSvItempage" width="100%" height:10px></div>
			<div id="dateTaskCnvItempage" width="100%" height:10px></div>
			<!-- <div id="personelpage" width="100%" height:10px></div> -->
			</div>
        	</div>
	</body>
	</html>
