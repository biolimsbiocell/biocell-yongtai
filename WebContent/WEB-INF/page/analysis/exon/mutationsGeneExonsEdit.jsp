<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=mutationsGeneExons&id=${mutationsGeneExons.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/exon/mutationsGeneExonsEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mutationsGeneExons_id"
                   	 name="mutationsGeneExons.id" title="编号"
                   	   
	value="<s:property value="mutationsGeneExons.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >突变基因</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mutationsGeneExons_mutationGenes"
                   	 name="mutationsGeneExons.mutationGenes" title="突变基因"
                   	   
	value="<s:property value="mutationsGeneExons.mutationGenes"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >转录本</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mutationsGeneExons_transcript"
                   	 name="mutationsGeneExons.transcript" title="转录本"
                   	   
	value="<s:property value="mutationsGeneExons.transcript"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >总外显子数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mutationsGeneExons_totleNumberExon"
                   	 name="mutationsGeneExons.totleNumberExon" title="总外显子数"
                   	   
	value="<s:property value="mutationsGeneExons.totleNumberExon"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="mutationsGeneExons_createDate"
                   	 name="mutationsGeneExons.createDate" title="时间"
                   	   
                   	  value="<s:date name="mutationsGeneExons.createDate" format="yyyy-MM-dd"/>"
                   	                      	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
			
               	 	<td class="label-title"  style="display:none"  >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
 						<input type="text" size="20" readonly="readOnly"  id="mutationsGeneExons_createUser_name"  value="<s:property value="mutationsGeneExons.createUser.name"/>" class="text input readonlytrue" readonly="readOnly" 	style="display:none"  />
 						<input type="hidden" id="mutationsGeneExons_createUser" name="mutationsGeneExons.createUser.id"  value="<s:property value="mutationsGeneExons.createUser.id"/>" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'   style="display:none"    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="mutationsGeneExons_state"
                   	 name="mutationsGeneExons.state" title="工作流状态"
                   	   
	value="<s:property value="mutationsGeneExons.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >状态描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="mutationsGeneExons_stateName"
                   	 name="mutationsGeneExons.stateName" title="状态描述"
                   	   
	value="<s:property value="mutationsGeneExons.stateName"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="mutationsGeneExons.id"/>" />
            </form>
            <div id="tabs">
            <ul>
           	</ul> 
			</div>
        	</div>
	</body>
	</html>
