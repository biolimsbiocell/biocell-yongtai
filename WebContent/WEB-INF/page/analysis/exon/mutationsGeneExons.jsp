﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/exon/mutationsGeneExons.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="mutationsGeneExons_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >突变基因</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="mutationsGeneExons_mutationGenes"
                   	 name="mutationGenes" searchField="true" title="突变基因"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >转录本</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="mutationsGeneExons_transcript"
                   	 name="transcript" searchField="true" title="转录本"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >总外显子数</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="mutationsGeneExons_totleNumberExon"
                   	 name="totleNumberExon" searchField="true" title="总外显子数"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >时间</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >创建人</td>
                   	<td align="left"   style="display:none">
 						<input type="text" size="20"   id="mutationsGeneExons_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" 	style="display:none" />
 						<input type="hidden" id="mutationsGeneExons_createUser" name="mutationsGeneExons.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'   style="display:none"    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >工作流状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="mutationsGeneExons_state"
                   	 name="state" searchField="true" title="工作流状态"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >状态描述</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="mutationsGeneExons_stateName"
                   	 name="stateName" searchField="true" title="状态描述"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_mutationsGeneExons_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_mutationsGeneExons_tree_page"></div>
</body>
</html>



