<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=dataTask&id=${dataTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/analysis/check/dataTaskEdit.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>


					<td class="label-title">编号</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="dataTask_id" name="dataTask.id" title="编号"
						value="<s:property value="dataTask.id"/>" />

					</td>


					<td class="label-title">描述</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="50" maxlength="50"
						id="dataTask_name" name="dataTask.name" title="描述"
						value="<s:property value="dataTask.name"/>" />

					</td>


					<td class="label-title">下达时间</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="50" maxlength="50"
						id="dataTask_createDate" name="dataTask.createDate" title="下达时间"
						value="<s:date name="dataTask.createDate" format="yyyy-MM-dd"/>" />

					</td>
				</tr>
				<tr>



					<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						documentId="dataTask_createUser"
						documentName="dataTask_createUser_name" />



					<td class="label-title">下达人</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="dataTask_createUser_name"
						value="<s:property value="dataTask.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="dataTask_createUser"
						name="dataTask.createUser.id"
						value="<s:property value="dataTask.createUser.id"/>"> <img
						alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>


					<td class="label-title">工作流状态</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="dataTask_state" name="dataTask.state" title="工作流状态"
						value="<s:property value="dataTask.state"/>" />

					</td>


					<td class="label-title" style="display: none">状态描述</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="dataTask_stateName"
						name="dataTask.stateName" title="状态描述"
						value="<s:property value="dataTask.stateName"/>"
						style="display: none" /></td>
				</tr>
				<tr>
					<td class="label-title">fc号</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="dataTask_fcnumber" name="dataTask.fcnumber" title="fc号"
						value="<s:property value="dataTask.fcnumber"/>" /></td>
				</tr>
				<tr>
					<td class="label-title">附件</td>
					<td></td>
					<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span
						class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
				</tr>


			</table>
			<input type="hidden" name="dataTaskSvnItemCheckedJson"
				id="dataTaskSvnItemCheckedJson" value="" /> <input type="hidden"
				name="dataTaskSvItemCheckedJson" id="dataTaskSvItemCheckedJson"
				value="" /> <input type="hidden" name="dateTaskCnvItemCheckedJson"
				id="dateTaskCnvItemCheckedJson" value="" /> <input type="hidden"
				name="dateTaskItemCheckedJson" id="dateTaskItemCheckedJson" value="" />
			<input type="hidden" id="id_parent_hidden"
				value="<s:property value="dataTask.id"/>" />
		</form>
		<div id="tabss">
			<ul>
				<li><a href="#dataTaskItemCheckpage">数据分析明细表</a></li>
			</ul>
			<div id="dataTaskItemCheckpage" style="width: 100%; height: 50%"></div>
		</div>
		<div id="tabs">
			<ul>
				<li><a href="#dataTaskSvnItemCheckedpage">单碱基突变表</a></li>
				<li><a href="#dataTaskSvItemCheckedpage">基因融合突变表</a></li>
				<li><a href="#dateTaskCnvItemCheckedpage">肿瘤突变表</a></li>
			</ul>
			<div id="dataTaskSvnItemCheckedpage" width="100%" height:10px></div>
			<div id="dataTaskSvItemCheckedpage" width="100%" height:10px></div>
			<div id="dateTaskCnvItemCheckedpage" width="100%" height:10px></div>
		</div>
	</div>
</body>
</html>
