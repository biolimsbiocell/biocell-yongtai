﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}

</style>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>
<script type="text/javascript" src="${ctx}/js/analysis/check/dataTaskCheckItem.js"></script>
</head>
<body>
<%-- <input type="hidden" id="sgt2" value="${requestScope.sgt2}"> --%>
	<div id="dataTaskCheckItemdiv"></div>
	<div id="bat_upload_divRna6" style="display: none">
		<input type="file" name="file" id="file-uploadRna6">上传csv文件
	</div>
	<div style="display:none">
		<select id="grid-method"> 
			<option value="">请选择</option>
			<option value="1">通过</option>
			<option value="0">不通过</option>
		</select>
	</div>
	<div id="bar_rna" style="display: none">
		<table>
			<tr>
				<td class="label-title">处理方式</td>
				<td>
					<select id="grid-method">
						<option value="">请选择</option>
						<option value="1">通过</option>
						<option value="0">不通过</option>
					</select>
				</td>
			</tr>
		</table>
		<div id="bat_states_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>审核状态</span></td>
                <td><select id="states"  style="width:100 ">
               			<option value="1" selected="selected">审核不通过</option>
    					<option value="2" >审核通过</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
			<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
	</div>
</body>
</html>


