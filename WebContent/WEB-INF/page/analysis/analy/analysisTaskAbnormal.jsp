﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/analysis/analy/analysisTaskAbnormal.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTaskAbnormal_id"
                   	 name="id" searchField="true" title="编号"    />
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTaskAbnormal_name"
                   	 name="name" searchField="true" title="描述"    />
                   	</td>
			<g:LayOutWinTag buttonId="showcode" title=""
				hasHtmlFrame="true"
				html="${ctx}/experiment/pooling/analysisTaskAbnormal/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('analysisTaskAbnormal_code').value=rec.get('id');
				document.getElementById('analysisTaskAbnormal_code_name').value=rec.get('name');" />
               	 	<td class="label-title" >pooling编号</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="analysisTaskAbnormal_code_name" searchField="true"  name="code.name"  value="" class="text input" />
 						<input type="hidden" id="analysisTaskAbnormal_code" name="analysisTaskAbnormal.code.id"  value="" > 
 						<img alt='选择pooling编号' id='showcode' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
						  
               	 	<td class="label-title" >result1</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_result1"
                   	 name="note" searchField="true" title="result1"    />
                   	</td>
                   	
                   	
               	 	<td class="label-title" >result2</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_result2"
                   	 name="note" searchField="true" title="result2"    />
                   	</td>
                   	
                   	
               	 	<td class="label-title" >cnv</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_cnv"
                   	 name="note" searchField="true" title="cnv"    />
                   	</td>
			</tr>
			<tr>
					  
                   
               	 	<td class="label-title" >reads_mb</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_reads_mb"
                   	 name="note" searchField="true" title="reads_mb"    />
                   	</td>
                   	
                   	
               	 	<td class="label-title" >gc_antent</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_gc_antent"
                   	 name="note" searchField="true" title="gc_antent"    />
                  	</td>
                   	 	  
                  
               	 	<td class="label-title" >q30_ratio</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_q30_ratio"
                   	 name="note" searchField="true" title="q30_ratio"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >align_ratio</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_align_ratio"
                   	 name="note" searchField="true" title="align_ratio"    />
                   	</td>
                   	
               	 	<td class="label-title" >ur_ratio</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_ur_ratio"
                   	 name="note" searchField="true" title="ur_ratio"    />
                   	 </td>
			
			</tr>
			<tr>
               	 	<td class="label-title" >结果判定</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTaskAbnormal_resultDecide"
                   	 name="resultDecide" searchField="true" title="结果判定"    />
                   	  
                   	</td>
               	 	<td class="label-title" >下一步流向</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTaskAbnormal_flowStep"
                   	 name="flowStep" searchField="true" title="下一步流向"    />
                   	
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >处理意见</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="125" id="analysisTaskAbnormal_opinion"
                   	 name="opinion" searchField="true" title="处理意见"    />
                   	</td>
               	 	<td class="label-title" >确认执行</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTaskAbnormal_confirm"
                   	 name="confirm" searchField="true" title="确认执行"    />
                   	</td>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="150" id="analysisTaskAbnormal_note"
                   	 name="note" searchField="true" title="备注"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_analysisTaskAbnormal_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_analysisTaskAbnormal_tree_page"></div>
		<select id="result" style="display: none">
			<option value="0">不合格</option>
			<option value="1">合格</option>
		</select>
		<select id="confirm" style="display: none">
			<option value="">请选择</option>
			<option value="1">通过</option>
			<option value="0">不通过</option>
		</select>
</body>
</html>



