﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title=""
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=analysisTask&id=${analysisTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/analy/analysisTaskEdit.js"></script>
<div id="analysisTaskEditPage" style="float:left;width:100%"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  >编号</td>
               	 	<td></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   >
                   	<input type="text" size="20" maxlength="25" id="analysisTask_id"
                   	 name="analysisTask.id" title="编号"                  	 
						value="<s:property value="analysisTask.id"/>"       	  
                   	  /> 
                   	  <input type="hidden" size="20" maxlength="25" id="analysisTask_flowCell"
                   	 name="analysisTask.flowCell" title="flowcell编号"                  	 
						value="<s:property value="analysisTask.flowCell"/>"       	  
                   	  /> 
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="analysisTask_name"
                   	 name="analysisTask.name" title="描述"
                   	   
	value="<s:property value="analysisTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
<%-- 			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人" --%>
<%-- 				hasHtmlFrame="true" --%>
<%-- 				html="${ctx}/analysis/analy/analysisTask/userSelect.action" --%>
<%-- 				isHasSubmit="false" functionName="UserFun"  --%>
<%--  				hasSetFun="true" --%>
<%-- 				extRec="rec" --%>
<%-- 				extStr="document.getElementById('analysisTask_createUser').value=rec.get('id'); --%>
<%-- 				document.getElementById('analysisTask_createUser_name').value=rec.get('name');" /> --%>
				
			
			
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="analysisTask_createUser_name"  value="<s:property value="analysisTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="analysisTask_createUser" name="analysisTask.createUser.id"  value="<s:property value="analysisTask.createUser.id"/>" > 
 						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               	</td>     
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >下达时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="analysisTask_createDate"
                   	 name="analysisTask.createDate" title="下达时间"
                   	   
                   	  value="<s:property value="analysisTask.createDate"/>"
                   	                      	  />
                   	</td> 
			
			
			
			<%-- <g:LayOutWinTag buttonId="showdesequencingTask" title="选择下机质控号"
				hasHtmlFrame="true"
				html="${ctx}/analysis/desequencing/deSequencingTask/deSequencingTaskSelect.action"
				isHasSubmit="false" functionName="DeSequencingTaskFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('analysisTask_desequencingTask').value=rec.get('id');
				document.getElementById('analysisTask_desequencingTask_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >下机质控号</td>
            	 	<td clascs="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="analysisTask_desequencingTask_name"  value="<s:property value="analysisTask.desequencingTask.name"/>" />
 						<input type="hidden" id="analysisTask_desequencingTask" name="analysisTask.desequencingTask.id"  value="<s:property value="analysisTask.desequencingTask.id"/>" > 
 						<img alt='选择下机质控号' id='showdesequencingTask' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="DeSequencingTaskFun()" />                   		
                   	</td> --%>
			
			
               	 	<td class="label-title" >任务单</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="analysisTask_jobOrder"
                   	 name="analysisTask.jobOrder" title="任务单"
                   	   
	value="<s:property value="analysisTask.jobOrder"/>"
                   	  />
                   	  
                   	</td>
                   	
                <g:LayOutWinTag buttonId="showacceptUser" title=""
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('analysisTask_acceptUser').value=rec.get('id');
				document.getElementById('analysisTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >分析员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="analysisTask_acceptUser_name"  value="<s:property value="analysisTask.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="analysisTask_acceptUser" name="analysisTask.acceptUser.id"  value="<s:property value="analysisTask.acceptUser.id"/>" > 
 						<img alt='选择分析员' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
			
			
			
			
               	 	<td class="label-title" >分析时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="analysisTask_acceptDate"
                   	 name="analysisTask.acceptDate" title="分析时间"
                   	      Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	  value="<s:date name="analysisTask.acceptDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"   style="display:none"  >工作流id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"   style="display:none"  ></td>            	 	
                   	<td align="left"    style="display:none"  >
                   	<input type="text" size="20" maxlength="25" id="analysisTask_state"
                   	 name="analysisTask.state" title="工作流id"
                   	     style="display:none"  
	value="<s:property value="analysisTask.state"/>"
                   	  />
                   	  
                   	</td>

               	 	<td class="label-title">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="analysisTask_stateName"
                   	 name="analysisTask.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="analysisTask.stateName"/>"
                   	  
                   	  />
                   	  
                   	</td>
                   	
                   	<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
            </table>
            <input type="hidden" name="analysisItemJson" id="analysisItemJson" value="" />
            <input type="hidden" name="analysisInfoJson" id="analysisInfoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="analysisTask.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
			<li><a href="#analysisItempage">样本明细</a></li>
			<li><a href="#analysisInfopage">结果明细</a></li>
           	</ul>  -->
			<div id="analysisItempage" width="100%" height:10px></div>
			<div id="analysisInfopage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
