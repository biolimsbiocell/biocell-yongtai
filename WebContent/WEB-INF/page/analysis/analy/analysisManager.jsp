﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<style>
</style>
<script type="text/javascript" src="${ctx}/js/analysis/analy/analysisManager.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="18" id="analysisManager_sampleCode"
                   	 name="sampleCode" searchField="true" title="样本编号"    />

                   	</td>
               	 	<td class="label-title" >CNV</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="analysisManager_cnv"
                   	 name="cnv" searchField="true" title="CNV"    />

                   	</td>
			</tr>
			<tr>                   	
               	 	<td class="label-title" >result1</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="analysisManager_result1"
                   	 name="result1" searchField="true" title="result1"    />

                   	</td>

               	 	<td class="label-title" >result2</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="30" id="analysisManager_result2"
                   	 name="result2" searchField="true" title="result2"    />

                   	</td>
			</tr>
            </table>          
		</form>
		</div>
	<div id="analysisManagerdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
</body>
</html>


