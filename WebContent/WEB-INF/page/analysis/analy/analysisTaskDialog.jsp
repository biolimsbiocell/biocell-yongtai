﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/analy/analysisTaskDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">下达人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">下达时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">下机质控号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_desequencingTask" searchField="true" name="desequencingTask"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">任务单</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_jobOrder" searchField="true" name="jobOrder"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">分析员</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_acceptUser" searchField="true" name="acceptUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">分析时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_acceptDate" searchField="true" name="acceptDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">工作流id</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="analysisTask_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_analysisTask_div"></div>
   		
</body>
</html>



