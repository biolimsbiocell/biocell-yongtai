﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/analy/analysisTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="analysisTask_id"
                   	 name="id" searchField="true" title="编号"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTask_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title=""
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('analysisTask_createUser').value=rec.get('id');
				document.getElementById('analysisTask_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >下达人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="analysisTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="analysisTask_createUser" name="analysisTask.createUser.id"  value="" > 
 						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >下达时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showdesequencingTask" title=""
				hasHtmlFrame="true"
				html="${ctx}/analysis/analy/analysisTask/deSequencingTaskSelect.action"
				isHasSubmit="false" functionName="DeSequencingTaskFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('analysisTask_desequencingTask').value=rec.get('id');
				document.getElementById('analysisTask_desequencingTask_name').value=rec.get('name');" />
               	 	<td class="label-title" >下机质控号</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="analysisTask_desequencingTask_name" searchField="true"  name="desequencingTask.name"  value="" class="text input" />
 						<input type="hidden" id="analysisTask_desequencingTask" name="analysisTask.desequencingTask.id"  value="" > 
 						<img alt='选择下机质控号' id='showdesequencingTask' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >任务单</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTask_jobOrder"
                   	 name="jobOrder" searchField="true" title="任务单"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showacceptUser" title=""
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('analysisTask_acceptUser').value=rec.get('id');
				document.getElementById('analysisTask_acceptUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >分析员</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="analysisTask_acceptUser_name" searchField="true"  name="acceptUser.name"  value="" class="text input" />
 						<input type="hidden" id="analysisTask_acceptUser" name="analysisTask.acceptUser.id"  value="" > 
 						<img alt='选择分析员' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >分析时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startacceptDate" name="startacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate1" name="acceptDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endacceptDate" name="endacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate2" name="acceptDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >工作流id</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="analysisTask_state"
                   	 name="state" searchField="true" title="工作流id"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >工作流状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="analysisTask_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"   style="display:none"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_analysisTask_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_analysisTask_tree_page"></div>
</body>
</html>



