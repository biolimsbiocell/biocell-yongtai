<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="/operfile/initFileList.action?modelType=dataTransfer&id=${dataTransfer.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/analysis/dataTransfer/dataTransferEdit.js"></script>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title">编号</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="127"
						id="dataTransfer_id" name="dataTransfer.id" title="编号"
						value="<s:property value="dataTransfer.id"/>"
						class="text input readonlyfalse true" /></td>
					<td class="label-title">描述</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="127"
						id="dataTransfer_name" name="dataTransfer.name" title="描述"
						value="<s:property value="dataTransfer.name"/>" /></td>
					<td class="label-title">创建人</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="dataTransfer_createUser_name"
						value="<s:property value="dataTransfer.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="dataTransfer_createUser"
						name="dataTransfer.createUser.id"
						value="<s:property value="dataTransfer.createUser.id"/>">
					</td>
				</tr>
				<tr>
					<td class="label-title">创建时间</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="127"
						id="dataTransfer_createDate" name="dataTransfer.createDate"
						title="创建时间" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="dataTransfer.createDate" format="yyyy-MM-dd HH:mm:ss"/>" />
					</td>
					<td class="label-title">状态名称</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="127"
						id="dataTransfer_stateName" name="dataTransfer.stateName"
						title="状态名称" value="<s:property value="dataTransfer.stateName"/>"
						class="text input readonlytrue" readonly="readOnly" /></td>
					<td class="label-title">任务单号</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="dataTransfer_orderId" name="dataTransfer.orderId" title="任务单号"
						value="<s:property value="dataTransfer.orderId"/>" /> <img
						alt="选择任务单号" id='orderId' src='${ctx}/images/img_lookup.gif'
						class='detail' onClick="selectOrderId()" /></td>
				</tr>
				<tr>
					<td class="label-title" style="display: none">备注</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="127" id="dataTransfer_note"
						name="dataTransfer.note" title="备注"
						value="<s:property value="dataTransfer.note"/>"
						style="display: none" /></td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.experimentalGroup" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="dataTransfer_userGroup_name"
						value="<s:property value="dataTransfer.userGroup.name"/>"
						class="text input readonlytrue true" /> <input type="hidden"
						id="dataTransfer_userGroup" name="dataTransfer.userGroup.id"
						value="<s:property value="dataTransfer.userGroup.id"/>"></td>
					<td class="label-title">实验员</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="dataTransfer_tester_name"
						value="<s:property value="dataTransfer.tester.name"/>"
						class="text input readonlytrue true" /> <input type="hidden"
						id="dataTransfer_tester" name="dataTransfer.tester.id"
						value="<s:property value="dataTransfer.tester.id"/>"></td>
					<td class="label-title">实验模板</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="dataTransfer_template_name"
						value="<s:property value="dataTransfer.template.name"/>"
						class="text input readonlytrue true" /> <input type="hidden"
						id="dataTransfer_template" name="dataTransfer.template.id"
						value="<s:property value="dataTransfer.template.id"/>"></td>
				</tr>
				<tr>
					<td class="label-title">项目名称</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="dataTransfer_project_name"
						value="<s:property value="dataTransfer.project.name"/>"
						class="text input readonlytrue true" /> <input type="hidden"
						id="dataTransfer_project" name="dataTransfer.project.id"
						value="<s:property value="dataTransfer.project.id"/>"></td>
					<td class="label-title">附件</td>
					<td></td>
					<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span
						class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
					<td class="label-title" style="display: none">状态</td>
					<td style="display: none" class="requiredcolumn" nowrap
						width="10px"></td>
					<td style="display: none" align="left"><input
						style="display: none" type="text" size="20" maxlength="127"
						id="dataTransfer_state" name="dataTransfer.state" title="状态"
						value="<s:property value="dataTransfer.state"/>"
						class="text input readonlytrue" readonly="readOnly" /></td>
				</tr>
			</table>
			<input type="hidden" name="dataTransferItemJson"
				id="dataTransferItemJson" value="" /> <input type="hidden"
				id="id_parent_hidden" value="<s:property value="dataTransfer.id"/>" />
		</form>
		<div id="tabs">
			<ul>
				<li><a href="#dataTransferItempage">数据明细</a></li>
			</ul>
			<div id="dataTransferItempage" width="100%" height:10px></div>
		</div>
	</div>
</body>
</html>
