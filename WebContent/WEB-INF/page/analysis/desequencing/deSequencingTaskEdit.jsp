﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			#box2,
			#box3,
			#box1 {
				border-radius: 10px;
				overflow: hidden;
				transition: all 1s;
				width: 80%;
				margin: 10px auto;
				color: #fff;
			}
			
			#box2 {
				background: #8FBAF3;
			}
			
			#box3 {
				background: #28CC9E;
			}
			
			#box1 {
				background: #878ECD;
			}
			
			#box3:hover {
				box-shadow: 15px 15px 15px #000;
			}
			
			#box2:hover {
				box-shadow: 15px 15px 15px #000;
			}
			
			#box1:hover {
				box-shadow: 15px 15px 15px #000;
			}
			
			.modal-body .box-body div {
				margin: -34px 13px;
				text-align: center;
			}
			
			.modal-body .box-body div a {
				color: #fff;
				display: block;
				margin-top: 23%;
				text-decoration: none;
			}
			
			.modal-body .box-body div i {
				font-size: 100px;
			}
			
			.modal-body .box-body div p {
				font-weight: 900;
			}
			
			.chosedType {
				border: 2px solid #BAB5F6 !important;
			}
			
			.input-group {
				margin-top: 10px;
			}
			.box-title small span {
				margin-right: 20px;
			}
		</style>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
			<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
		</div>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<!--选择接受类型的模态框-->
		<div class="modal fade" id="deSequencingTaskModal" tabindex="-1" role="dialog" aria-hidden="flase" data-backdrop="false">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header text-center">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<div>
						<a href="####" style="text-align: center;" onclick="downCsv()">
							<p><fmt:message key="biolims.receive.downloadCSV" /></p>
						</a>
					</div>
					<div class="modal-body" id="modal-body">
						<div class="row">
							<div class="col-sm-6 col-xs-12">
								<div class="box" id="box2">
									<div class="box-body" style="height: 170px;">
										<div id="uploadList">
											<a href="####">
												<i class="fa  fa-list"></i>
												<p><fmt:message key="biolims.desequencing.readCsv" /></p>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12">
								<div class="box" id="box3">
									<div class="box-body" style="height: 170px;">
										<div id="newList">
											<a href="####">
												<i class="fa fa-clipboard"></i>
												<p><fmt:message key="biolims.common.upLoadFile" /></p>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 58px">
			<!--订单录入的form表单-->
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
						 <strong id="deSequencingTaskTitle" type="<s:property value="deSequencingTask.type"/>"><fmt:message key="biolims.desequencing.desequencing" /></strong>
						<small style="color:#fff;">
										<fmt:message key="biolims.desequencing.id" />: <span id="deSequencingTask_id"><s:property value="deSequencingTask.id "/></span>
										<fmt:message key="biolims.sampleOut.createUser" />: <span id="deSequencingTask_createUser"><s:property value=" deSequencingTask.createUser.name "/></span>
										<fmt:message key="biolims.sampleOut.createDate" />: <span id="deSequencingTask_createDate"><s:date name="deSequencingTask.createDate" format="yyyy-MM-dd"/></span>
										<fmt:message key="biolims.sampleOut.state" />: <span id="deSequencingTask_state"><s:property value="deSequencingTask.stateName"/></span>
									</small>
					</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefresh()"><i class="glyphicon glyphicon-refresh"></i>
                </button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
								<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print" /></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData" /></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();"><fmt:message key="biolims.common.export" /></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();"><fmt:message key="biolims.common.exportCSV" /></a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message key="biolims.common.lock2Col" /></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col" /></a>
											</li>
										</ul>
							</div>
						</div>

					</div>
					<div class="box-body">
						<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter(" bpmTaskId ")%>" />
						<form name="form1" id="form1" method="post">
							<!--保存时的拼的数据-->
										<input type="hidden" name="state" value="<s:property value="deSequencingTask.state"/>"/>
										<input type="hidden" name="stateName" value="<s:property value="deSequencingTask.stateName"/>"/>
										<input type="hidden" id="deSequencingTask_id" name="id" value="<s:property value="deSequencingTask.id "/>"/>
										<input type="hidden" name="acceptUser-id" value="<s:property value=" deSequencingTask.createUser.id "/>" />
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.common.describe" />
										</span> <input type="text" size="20" maxlength="25" changelog="<s:property value=" deSequencingTask.name "/>"
										id="deSequencingTask_name" name="name" class="form-control" title="" value="<s:property value=" deSequencingTask.name "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.common.fcSerialNumber" /><img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input type="text" size="20" maxlength="25" changelog="<s:property value=" deSequencingTask.flowCode "/>"
										 id="deSequencingTask_flowCode" name="flowCode" class="form-control" title="" value="<s:property value=" deSequencingTask.flowCode "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.desequencing.machine" />
										</span> <input type="text" size="20" maxlength="25" id="machineCode" changelog="<s:property value=" deSequencingTask.machineCode "/>"
										 name="machineCode" class="form-control" title="" value="<s:property value=" deSequencingTask.machineCode "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">REF
										</span> <input type="text" size="20" maxlength="25" id="REF" changelog="<s:property value=" deSequencingTask.REF "/>"
										 name="REF" class="form-control" title="" value="<s:property value=" deSequencingTask.REF "/>" />
									</div>
								</div>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
								</div>
								</div>
							</form>
					</div>
					<div class="box-footer ipadmini" style="padding-top: 30px;min-height: 320px;">
						<table class="table table-hover table-striped table-bordered table-condensed" id="main" style="font-size:14px;"></table>
					</div>
				</div>
			</div>
		</div>

	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/analysis/desequencing/deSequencingTaskEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>
</html>