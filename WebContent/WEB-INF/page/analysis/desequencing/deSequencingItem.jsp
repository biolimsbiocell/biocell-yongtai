﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/analysis/desequencing/deSequencingItem.js"></script>
</head>
<body>
	<div id="deSequencingItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
		<div id="bat_infos_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.flow"/></td>
                <td><select id="nextFlow"  style="width:100">
    					<option value="0" ><fmt:message key="biolims.common.waitCreateReport"/></option>
    					<option value="1" ><fmt:message key="biolims.common.xjzkError"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>


