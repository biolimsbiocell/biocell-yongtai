﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript"
	src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>
<script type="text/javascript" src="${ctx}/js/analysis/desequencing/deSequencingInfo.js"></script>
</head>
<body>
	<div id="deSequencingInfodiv"></div>
	<div id="bat_upload1csv_div" style="display: none">
		<input type="file" name="file" id="file-upload1csv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
</body>
</html>


