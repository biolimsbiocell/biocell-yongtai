﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=desequencingItem&id=${deSequencingItem.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/deSequencing/deSequencingItemEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingItem_name"
                   	 name="deSequencingItem.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="deSequencingItem.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.flowCellNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingItem_flowCode"
                   	 name="deSequencingItem.flowCode" title="<fmt:message key="biolims.common.flowCellNumber"/>"
                   	   
	value="<s:property value="deSequencingItem.flowCode"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.poolingSerialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingItem_poolingCode"
                   	 name="deSequencingItem.poolingCode" title="<fmt:message key="biolims.common.poolingSerialNumber"/>"
                   	   
	value="<s:property value="deSequencingItem.poolingCode"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.libraryNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingItem_libraryCode"
                   	 name="deSequencingItem.libraryCode" title="<fmt:message key="biolims.common.libraryNumber"/>"
                   	   
	value="<s:property value="deSequencingItem.libraryCode"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingItem_sampleCode"
                   	 name="deSequencingItem.sampleCode" title="<fmt:message key="biolims.common.sampleCode"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="deSequencingItem.sampleCode"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingItem_state"
                   	 name="deSequencingItem.state" title="<fmt:message key="biolims.common.state"/>"
                   	   
	value="<s:property value="deSequencingItem.state"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingItem_note"
                   	 name="deSequencingItem.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="deSequencingItem.note"/>"
                   	  />
                   	  
                   	</td>
			
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.associatedWithTheMainTable"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
 						<input type="text" size="20" readonly="readOnly"  id="deSequencingItem_desequencingTask_name"  value="<s:property value="deSequencingItem.desequencingTask.name"/>" class="text input readonlytrue" readonly="readOnly" 	style="display:none"  />
 						<input type="hidden" id="deSequencingItem_desequencingTask" name="deSequencingItem.desequencingTask.id"  value="<s:property value="deSequencingItem.desequencingTask.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheAssociatedWithTheMainTable"/>' id='showdesequencingTask' src='${ctx}/images/img_lookup.gif' 	class='detail'   style="display:none"    />                   		
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="desequencingItem.id"/>" />
            </form>
            <div id="tabs">
            <ul>
           	</ul> 
			</div>
        	</div>
	</body>
	</html>
