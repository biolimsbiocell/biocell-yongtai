﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%-- <%@ include file="/WEB-INF/page/include/common3.jsp"%> --%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/desequencing/deSequencingItemDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
	 <input type="hidden" id="projectId" value="${requestScope.projectId}">
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.flowCellNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_flowCode" searchField="true" name="flowCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.poolingSerialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_poolingCode" searchField="true" name="poolingCode"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.libraryNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_libraryCode" searchField="true" name="libraryCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.sampleCode"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_sampleCode" searchField="true" name="sampleCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.state"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.note"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.associatedWithTheMainTable"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="deSequencingItem_desequencingTask" searchField="true" name="desequencingTask"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_deSequencingItem_div"></div>
   		
</body>
</html>



