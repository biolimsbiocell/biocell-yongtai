﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=filtrateTask&id=${filtrateTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/filt/filtrateTaskEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="filtrateTask_id"
                   	 name="filtrateTask.id" title="编号" value="<s:property value="filtrateTask.id"/>"
                   	  />
                   	  <input type="hidden" size="20" maxlength="25" id="filtrateTask_techTaskId"
                   	 name="filtrateTask.techTaskId" title="任务单编号" value="<s:property value="filtrateTask.techTaskId"/>"
                   	  />
					</td>
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="filtrateTask_name"
                   	 name="filtrateTask.name" title="描述" value="<s:property value="filtrateTask.name"/>"
                   	  />
                   	</td>
                   	<td class="label-title" >合同号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="filtrateTask_contractId"
                   	 name="filtrateTask.contractId" title="合同号" value="<s:property value="filtrateTask.contractId"/>"
                   	  />
             </tr>
             <tr>
                   	</td>
                   	<td class="label-title" >项目号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="filtrateTask_projectId"
                   	 name="filtrateTask.projectId" title="项目号" value="<s:property value="filtrateTask.projectId"/>"
                   	  />
                   	</td>
             		<td class="label-title" >负责人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="filtrateTask_headUser"
                   	 name="filtrateTask.headUser" title="负责人" value="<s:property value="filtrateTask.headUser"/>"
                   	  />
                   	</td>
                   	  <td class="label-title" >项目整体状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	   <select name="filtrateTask.projectIsGood" id="filtrateTask_projectIsGood" style="width:150px;">
                   			<option value="1" <s:if test="filtrateTask.projectIsGood == 1">selected="selected"</s:if>>合格</option>
                   			<option value="0" <s:if test="filtrateTask.projectIsGood == 0">selected="selected"</s:if>>不合格</option>
                   		</select>
                   	</td>
			</tr>
			<tr>
					<td class="label-title" >数据量是否足够</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	   <select name="filtrateTask.dataIsGood" id="filtrateTask_dataIsGood" style="width:150px;">
                   			<option value="1" <s:if test="filtrateTask.dataIsGood == 1">selected="selected"</s:if>>是</option>
                   			<option value="0" <s:if test="filtrateTask.dataIsGood == 0">selected="selected"</s:if>>否</option>
                   		</select>
                   	</td>
               	 	<td class="label-title"  style="display:none"  >状态id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="10" id="filtrateTask_state"
                   	 name="filtrateTask.state" title="状态id"
                   	   
	value="<s:property value="filtrateTask.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="10" id="filtrateTask_statename"
                   	 name="filtrateTask.statename" title="工作流状态"
                   	   
	value="<s:property value="filtrateTask.statename"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="filtrateTaskItemJson" id="filtrateTaskItemJson" value="" />
            <input type="hidden" name="sampleFiltrateInfoJson" id="sampleFiltrateInfoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="filtrateTask.id"/>" />
            </form>
            <div id="tabs">
            <!-- <ul>
			<li><a href="#filtrateTaskItempage">过滤明细</a></li>
			<li><a href="#sampleFiltrateInfopage">过滤结果</a></li>
           	</ul>  -->
			<div id="filtrateTaskItempage" width="100%" height:10px></div>
			<div id="sampleFiltrateInfopage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
