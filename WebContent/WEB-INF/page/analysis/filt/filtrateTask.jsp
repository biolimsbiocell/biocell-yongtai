﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/filt/filtrateTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="filtrateTask_id"
                   	 name="id" searchField="true" title="编号"   style="display:none"    />
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="filtrateTask_name"
                   	 name="name" searchField="true" title="描述"    />
                   	</td>
            </tr>
            <tr>
            	<td class="label-title" >合同号</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="filtrateTask_contractId"
                   	 name="contractId" searchField="true" title="合同号"    />
               	</td>
               	<td class="label-title" >项目号</td>
               	<td align="left"  >
                 
					<input type="text" size="50" maxlength="50" id="filtrateTask_projectId"
                  	 name="projectId" searchField="true" title="项目号"    />
                </td>
            </tr>
            <tr>
               	 	<td class="label-title"  style="display:none"  >状态id</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="10" id="filtrateTask_state"
                   	 name="state" searchField="true" title="状态id"   style="display:none"    />
                   	</td>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="filtrateTask_statename"
                   	 name="statename" searchField="true" title="工作流状态"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_filtrateTask_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_filtrateTask_tree_page"></div>
</body>
</html>



