﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=techAnalysisTask&id=${techAnalysisTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/techanaly/techAnalysisTaskEdit.js"></script>

  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"   >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_id"
                   	 name="techAnalysisTask.id" title="编号"
	value="<s:property value="techAnalysisTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="techAnalysisTask_name"
                   	 name="techAnalysisTask.name" title="描述"
                   	   
	value="<s:property value="techAnalysisTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_createUser"
                   	 name="techAnalysisTask.createUser" title="创建人"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techAnalysisTask.createUser"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_createDate"
                   	 name="techAnalysisTask.createDate" title="创建日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="techAnalysisTask.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >负责人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_headUser"
                   	 name="techAnalysisTask.headUser" title="负责人"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techAnalysisTask.headUser"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >项目id</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_projectId"
                   	 name="techAnalysisTask.projectId" title="项目id"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techAnalysisTask.projectId"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >合同号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_contractId"
                   	 name="techAnalysisTask.contractId" title="合同号"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techAnalysisTask.contractId"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >任务单id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_techTaskId"
                   	 name="techAnalysisTask.techTaskId" title="任务单id"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techAnalysisTask.techTaskId"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none" >状态id</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"  ></td>            	 	
                   	<td align="left"  style="display:none"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_state"
                   	 name="techAnalysisTask.state" title="状态id"
                   	   readonly = "readOnly" class="text input readonlytrue"   style="display:none" 
	value="<s:property value="techAnalysisTask.state"/>"
                   	  />
                   	  
                   	</td>
               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="techAnalysisTask_stateName"
                   	 name="techAnalysisTask.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techAnalysisTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="sampleTechAnalysisInfoJson" id="sampleTechAnalysisInfoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="techAnalysisTask.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#sampleTechAnalysisInfopage">科技服务信息分析结果</a></li>
           	</ul> 
			<div id="sampleTechAnalysisInfopage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
