﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}

</style>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>
<script type="text/javascript" src="${ctx}/js/analysis/assignment/assignmentTaskItem.js"></script>
</head>
<body>
<%-- <input type="hidden" id="sgt2" value="${requestScope.sgt2}"> --%>
	<div id="assignmentTaskItemdiv"></div>
	<div id="bat_upload_divRna6" style="display: none">
		<input type="file" name="file" id="file-uploadRna6"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div style="display:none">
		<select id="grid-method"> 
			<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
			<option value="1"><fmt:message key="biolims.common.pass"/></option>
			<option value="0"><fmt:message key="biolims.common.noPass"/></option>
		</select>
	</div>
	
	
	<div id="bar_rna" style="display: none">
		<table>
			<tr>
				<td class="label-title"><fmt:message key="biolims.common.processingMethods"/></td>
				<td>
					<select id="grid-method">
						<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
						<option value="1"><fmt:message key="biolims.common.pass"/></option>
						<option value="0"><fmt:message key="biolims.common.noPass"/></option>
					</select>
				</td>
			</tr>
		</table>
		<div id="bat_states_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.reviewTheStatus"/></span></td>
                <td><select id="states"  style="width:100 ">
               			<option value="1" ><fmt:message key="biolims.common.approved"/></option>
    					<option value="2" selected="selected"><fmt:message key="biolims.common.auditNotThrough"/></option>
					</select>
                </td>
			</tr>
		</table>
		</div>
		
		<div id="bat_sampleStage_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.installment"/></span></td>
                <td><select id="sampleStage"  style="width:100 ">
               			<option value="1" >I</option>
               			<option value="2" >II</option>
               			<option value="3" >III</option>
    					<option value="4" >IV</option>
					</select>
                </td>
			</tr>
		</table>
		</div>
<%-- 			<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" /> --%>
	</div>
</body>
</html>


