<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=assignmentTask&id=${assignmentTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/analysis/assignment/assignmentTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="assignmentTaskTempPage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="assignmentTask_id"
                   	 name="assignmentTask.id" title="<fmt:message key="biolims.common.serialNumber"/>" 	readonly = "readOnly" class="text input readonlytrue"   
                   	 value="<s:property value="assignmentTask.id"/>" />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="assignmentTask_name"
                   	 name="assignmentTask.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="assignmentTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<%-- <input type="text" size="20" maxlength="25" id="assignmentTask_createDate"
                   	 name="assignmentTask.createDate" title="下达时间"
                   	  value="<s:date name="assignmentTask.createDate" format="yyyy-MM-dd"/>"
                   	                      	  /> --%>
                   	 <input type="text" size="20" maxlength="25"  id="assignmentTask_createDate" 
                   	  name="assignmentTask.createDate"  title="出生日期"  readonly="readOnly"   class="text input readonlytrue"   
                   	  value="<s:date name="assignmentTask.createDate" format="yyyy-MM-dd"/>"   />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showcreateUsers" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="assignmentTask_createUser"
				documentName="assignmentTask_createUser_name" />
				 --%>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="assignmentTask_createUser_name"  value="<s:property value="assignmentTask.createUser.name"/>" class="text input readonlytrue"   />
 						<input type="hidden" id="assignmentTask_createUser" name="assignmentTask.createUser.id"  value="<s:property value="assignmentTask.createUser.id"/>" > 
<%--  						<img alt='选择下达人'  id='showcreateUsers' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
			
                   	  
                   	<td class="label-title" style="display: none"><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none"></td>            	 	
                   	<td align="left"  style="display: none">
                   	<input type="text" size="20" maxlength="25" id="assignmentTask_state"
                   	 name="assignmentTask.state" title="<fmt:message key="biolims.common.state"/>" 
                   	   
	value="<s:property value="assignmentTask.state"/>"
                   	  /><!-- style="display: none" -->
                   	  
                   	</td> 
			
			
               	 	<td class="label-title"  ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"   ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="assignmentTask_stateName"  readonly="readOnly"  class="text input readonlytrue" 
                   	 name="assignmentTask.stateName" title="<fmt:message key="biolims.common.stateDescription"/>"
                   	   
	value="<s:property value="assignmentTask.stateName"/>"
		
                   	 
                   	  />
                   	  
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.flowCellNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="assignmentTask_fcnumber"
                   	 name="assignmentTask.fcnumber" title="<fmt:message key="biolims.common.flowCellNumber"/>"                	   
						value="<s:property value="assignmentTask.fcnumber"/>" />
                   	  <img alt='<fmt:message key="biolims.common.selectFlowCellNumber"/>' id='showFc' src='${ctx}/images/img_lookup.gif' 
                   	  	class='detail'   onClick="showfcFun()" />
                   	</td>
                   	
			</tr>
			<tr>
					
               	 	
                   	
                   	<td class="label-title" ></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="20" maxlength="25" id="assignmentTask_knumber"
                   	 name="assignmentTask.knumber" title=""
                   	   
	value="<s:property value="1"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
             <input type="hidden" name="assignmentTaskTempJson" id="assignmentTaskTempJson" value="" />
            <input type="hidden" name="assignmentTaskItemJson" id="assignmentTaskItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="assignmentTask.id"/>" />
            </form>
            <div id="tabss">
	           <ul>
	          	 <li><a href="#assignmentTaskItempage"><fmt:message key="biolims.common.scheduleOfDataAnalysis"/></a></li>
				 <!-- <li><a href="#personelpage">样本信息表</a></li> -->
	           	</ul> 
	           	<div id="assignmentTaskItempage" style="width:100%;height:100%"></div>
				<!-- <div id="personelpage" style="width:100%;height:50%"></div> -->
			</div>
        	
        	</div>
	</body>
	</html>
