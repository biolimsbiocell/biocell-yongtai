﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

<script type="text/javascript" src="${ctx}/js/analysis/assignment/deSequencingTaskByState.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.flowCellNumber"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="deSequencingTask_flowCode"
                   	 name="deSequencingTask.flowCode" searchField="true" title="<fmt:message key="biolims.common.flowCellNumber"/>"    />
                   	</td>
                   	<td class="label-title" >REF</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="deSequencingTask_REF"
                   	 name="deSequencingTask.REF" searchField="true" title="REF号"    />
                   	</td>
            </tr>
            <tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.expectedTimeOffTheComputer"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="deSequencingTask_machineTime"
                   	 name="deSequencingTask.machineTime" searchField="true" title="<fmt:message key="biolims.common.flowCellNumber"/>"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="deSequencingTask.createDate" format="yyyy-MM-dd"/>" 
                   	      />
                   	</td>
                   	
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowId"/></td>
                   	<td align="left"   style="display:none">
					<input type="text" size="20" maxlength="25" id="deSequencingTask_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.workflowId"/>"   style="display:none"    />
                   	</td>
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="deSequencingTask_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	</td>
                   	
			</tr>
            </table>
		</form>
		</div>
		<div id="show_deSequencingTaskByState_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_deSequencingTask_tree_page"></div>
</body>
</html>



