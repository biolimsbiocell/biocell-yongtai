﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">





.chosedType {
	border: 2px solid #BAB5F6 !important;
}

.input-group {
	margin-top: 10px;
}

.scientific {
	display: none;
}

.box-title small span {
	margin-right: 20px;
}
#btn_submit{
	display: none;
}
#btn_changeState{
	display: none;
}
#hideDiv{
	display: none;
}


</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div id="tableFileLoad"></div>
		<!--选择接受类型的模态框-->
	<div class="modal fade" id="sampleReceiveModal" tabindex="-1"
		role="dialog" aria-hidden="flase" data-backdrop="false">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content" >
				<div class="modal-header text-center">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div>
						<a href="####" style="text-align: center;" >
						<p>扫描条码</p>
						</a>
					</div>
				</div>
				<div class="modal-body" id="modal-body">
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box1">
								<div class="box-body" style="height: 170px;">
									<div id="scanReceive">
										<a href="###"> <i class="fa  fa-barcode"></i>
											<p>
												<fmt:message key="biolims.receive.scanCode" />
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box2">
								<div class="box-body" style="height: 170px;">
									<div id="uploadList">
										<a href="####"> <i class="fa  fa-list"></i>
											<p>
												<fmt:message key="biolims.common.upLoadFile" />
										</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box3">
								<div class="box-body" style="height: 170px;">
									<div id="newList">
										<a href="####"> <i class="fa fa-clipboard"></i>
											<p>
												<fmt:message key="biolims.receive.newSampleReceive" />
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" id="scancode" style="display: none;">
						<input type="text" class="form-control input-lg" id="scanInput"
							autofocus>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 50px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="sampleReceiveTitle"
							type="<s:property value="sampleReceive.type"/>"><fmt:message
								key="biolims.common.transport" /></strong>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefresh()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>

				</div>
			<div class="box-body ipadmini">
				<input type="hidden" name="bpmTaskId" id="bpmTaskId"
					value="<%=request.getParameter(" bpmTaskId ")%>" />
				<!--进度条-->
				<div id="wizard" class="form_wizard wizard_horizontal">
					<ul class="wizard_steps">
						<li><a class="selected step"> <span class="step_no">1</span>
								<span class="step_descr"> Step 1<br /> <small>运输</small>
							</span>
						</a></li>
						<li><a class="done step"> <span class="step_no">2</span>
								<span class="step_descr"> Step 2<br /> <small>生产细胞运输<!-- 运输明细 --></small>
							</span>
						</a></li>
						<li onclick="transportRefush()"><a class="done step"> <span class="step_no">3</span>
								<span class="step_descr"> Step 3<br /> <small>来源样本运输<!-- 样本运输 --></small>
							</span>
						</a></li>
						<!-- <li onclick="transportStateRefush()"><a class="done step"> <span class="step_no">4</span>
								<span class="step_descr"> Step 4<br /> <small>签收信息</small>
							</span>
						</a></li> -->
					</ul>
				</div>
				
				<div class="HideShowPanel">
				<form name="form1" id="form1" method="post">
					<div id="jbxx">
						<div class="row">
							<div class="panel"
								style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
								<div class="panel-heading text-left">
									<h3 class="panel-title" style="font-family: 黑体;">
										<i class="glyphicon glyphicon-bookmark"></i>
										<fmt:message key="biolims.common.basicInformation" />
									</h3>
								</div>
							</div>
						</div>
						<br />
						<div class="row">
						<!-- 编码 -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.common.codingA" /><img
									class='requiredimage' src='${ctx}/images/required.gif' /></span><input
									type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_id" name="id" class="form-control"
									changelog="<s:property value=" transportOrder.id "/>"
									title="" value="<s:property value=" transportOrder.id "/>" />
							</div>
						</div>
						<!--运输公司-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">运输公司</span>
								<input type="hidden" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_transCompany" name="transCompany-id" class="form-control"
									value="<s:property value=" transportOrder.transCompany.id "/>" />
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_transCompany_name" name="transCompany.name" class="form-control"
									changelog="<s:property value=" transportOrder.transCompany.name "/>"
									title="" value="<s:property value=" transportOrder.transCompany.name "/>" />
								<span class="input-group-btn">
									<button class="btn btn-info" type="button"
									onClick="showexpressCompany()">
									<i class="glyphicon glyphicon-search"></i>
								</span>
							</div>
						</div>
						<%-- <!-- 运输方式 -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.common.transportType" /></span>
									<select id="transportOrder_transportType" name="transportType" class="form-control">
										<option value="0" 
											<s:if test="transportOrder.transportType==0">selected="selected"</s:if>>陆运</option>
										<option value="1"
											<s:if test="transportOrder.transportType==1">selected="selected"</s:if>>海运</option>
										<option value="2"
											<s:if test="transportOrder.transportType==2">selected="selected"</s:if>>空运</option>
									</select>
							</div>
						</div> --%>
						<!-- 样品运输方式 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.sampleTransportationMode" /></span><input
										changelog="<s:property value=" transportOrder.ysfs.id "/>"
										 type="hidden" size="20" id="transportOrder_ysfs_id"
										name="ysfs-id"
										value="<s:property value=" transportOrder.ysfs.id "/>"
										class="form-control" title="样品运输方式" /> <input
										changelog="<s:property value=" transportOrder.ysfs.name "/>"
										type="text" size="20" id="transportOrder_ysfs_name"
										name="ysfs-name" readonly="readonly"
										value="<s:property value=" transportOrder.ysfs.name "/>"
										class="form-control" title="样品运输方式" /> <span
										class="input-group-btn">
										<button class="btn btn-info" type="button" id='ypysfs'
											onclick="yunshufs()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
						
						</div>
						<div class="row">
						<!-- 运输负责人 -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">物流人员</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_transUser" name="transUser" class="form-control"
									changelog="<s:property value=" transportOrder.transUser "/>"
									title="" value="<s:property value=" transportOrder.transUser "/>" />
							</div>
						</div>
					<%-- 	<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">物流人员</span>
								<input type="hidden" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_transUser" name="transportResponsibleUser-id" class="form-control"
									value="<s:property value=" transportOrder.transportResponsibleUser.id "/>"/>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_transportResponsibleUser_name" name="transportResponsibleUser-name" class="form-control"
									changelog="<s:property value=" transportOrder.transportResponsibleUser.name "/>"
									title="" value="<s:property value=" transportOrder.transportResponsibleUser.name "/>" />
								<!-- <span class="input-group-btn">
									<button class="btn btn-info" type="button"
									onClick="selectTransportUser()">
									<i class="glyphicon glyphicon-search"></i>
								</span> -->
							</div>
						</div> --%>
					<%-- <!-- 运送单号-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">运输单号</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_transCode" name="transCode" class="form-control"
									changelog="<s:property value=" transportOrder.transCode "/>"
									title="" value="<s:property value=" transportOrder.transCode " />" />
							</div>
						</div> --%>
						<!-- 创建人 -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.tStorage.createUser" /></span>
								<input type="hidden" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_createUser" name="createUser-id" class="form-control"
									/>
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_createUser_name" name="createUser-name" class="form-control"
									changelog="<s:property value=" transportOrder.createUser.name "/>"
									title="" value="<s:property value=" transportOrder.createUser.name "/>" />
							</div>
						</div>
						<!-- 创建日期 -->
						<!-- </div>
						<div class="row"> -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date">
								<span class="input-group-addon">创建日期</span>
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_createDate" name="createDate" class="form-control"
									changelog="<s:property value=" transportOrder.createDate "/>"
									title="" value="<s:date name =" transportOrder.createDate " format="yyyy-MM-dd"/>" />
							</div>
						</div>
						<!-- 作废状态-->
					</div>
					<div class="row"> 
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">是否作废</span>
									<select id="transportOrder_zuoState" name="zuoState" class="form-control">
										<option value="1"
											<s:if test="transportOrder.zuoState==1">selected="selected"</s:if>>否</option>
										<option value="2"
											<s:if test="transportOrder.zuoState==2">selected="selected"</s:if>>是</option>
									</select>
							</div>
						</div>
						
						<!-- 产物实验单-->
						<%-- <div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">产物实验单</span>
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_outOrder" name="outOrder" class="form-control"
									changelog="<s:property value=" transportOrder.outOrder "/>"
									title="" value="<s:property value=" transportOrder.outOrder "/>" />
								<span class="input-group-btn">
									<button class="btn btn-info" type="button"
									onClick="selectProductResultOrder()">
									<i class="glyphicon glyphicon-search"></i>
								</span>
							</div>
						</div> --%>
						<!-- 审核人 -->
						<%-- <div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">审核人</span>
								<input type="hidden" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_confirmUser" name="confirmUser-id" class="form-control"
									/>
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_confirmUser_name" name="confirmUser-name" class="form-control"
									changelog="<s:property value=" transportOrder.confirmUser.name "/>"
									title="" value="<s:property value=" transportOrder.confirmUser.name "/>" />
							</div>
						</div> --%>
				<!-- 	</div> -->
					<%-- <div class="row">
						<!-- 运输类型 -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.common.typeOfTransportation" /></span>
									<select id="transportOrder_orderType" name="orderType" class="form-control">
										<option value="0" 
											<s:if test="transportOrder.transportType==0">selected="selected"</s:if>>首次终产品运输</option>
										<option value="1"
											<s:if test="transportOrder.transportType==1">selected="selected"</s:if>>中间产品运输</option>
										<option value="2"
											<s:if test="transportOrder.transportType==2">selected="selected"</s:if>>非首次终产品运输</option>
									</select>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">运输效果</span>
									<select id="transportOrder_orderType" name="orderType" class="form-control">
										<option value="1" 
											<s:if test="transportOrder.transportType==1">selected="selected"</s:if>>成功</option>
										<option value="0"
											<s:if test="transportOrder.transportType==0">selected="selected"</s:if>>失败</option>
									</select>
							</div>
						</div>
						<!-- 装箱数-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">装箱数</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_boxes" name="boxes" class="form-control"
									changelog="<s:property value=" transportOrder.boxes "/>"
									title="" value="<s:property value=" transportOrder.boxes "/>" />
							</div>
						</div>
					</div> --%>
					<!-- <div class="row"> -->
						<!-- 创建日期 -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date">
								<span class="input-group-addon">回输日期</span>
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_transportDate" name="transportDate" class="form-control"
									changelog="<s:property value=" transportOrder.transportDate "/>"
									title="" value="<s:date name =" transportOrder.transportDate " format="yyyy-MM-dd"/>" />
							</div>
						</div>
						<!-- 状态-->
						<!-- </div>
						<div class="row"> -->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.tStorage.state" /></span>
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_stateName" name="stateName" class="form-control"
									changelog="<s:property value=" transportOrder.stateName "/>"
									title="" value="<s:property value=" transportOrder.stateName "/>" />
							</div>
						</div>
						<!-- 描述-->
						 </div>
						<div class="row">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepair.name" /></span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_name" name="name" class="form-control"
									changelog="<s:property value=" transportOrder.name "/>"
									title="" value="<s:property value=" transportOrder.name "/>" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="input-group">
								<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
									<fmt:message key="biolims.common.uploadAttachment" />
								</button>&nbsp;&nbsp;
								<button type="button" class="btn btn-info btn-sm"
									onclick="fileView()">
									<fmt:message key="biolims.report.checkFile" />
								</button>
							</div>
						</div>
					</div>
					<!-- 附件 -->
					<%-- <div class="row">
						<div class="col-xs-4">
							<div class="input-group">
								<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
									<fmt:message key="biolims.common.uploadAttachment" />
								</button>&nbsp;&nbsp;
								<button type="button" class="btn btn-info btn-sm"
									onclick="fileView()">
									<fmt:message key="biolims.report.checkFile" />
								</button>
							</div>
						</div>

					</div> 
				</div>--%>
				<%-- <div id="jsxx">
					<div class="row">
						<div class="panel"
							style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
							<div class="panel-heading text-left">
								<h3 class="panel-title" style="font-family: 黑体;">
									<i class="glyphicon glyphicon-bookmark"></i>
									接收信息
								</h3>
							</div>
						</div>
					</div>
					<br />
					<div class="row">
						<!-- 联系人-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.common.linkmanName" /></span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_linkman" name="linkman" class="form-control"
									changelog="<s:property value=" transportOrder.linkman "/>"
									title="" value="<s:property value=" transportOrder.linkman "/>" />
							</div>
						</div>
						<!-- 手机-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.common.cellPhoneNumber" /></span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_mobilePhone" name="mobilePhone" class="form-control"
									changelog="<s:property value=" transportOrder.mobilePhone "/>"
									title="" value="<s:property value=" transportOrder.mobilePhone "/>" />
							</div>
						</div>
						<!-- Email-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.crmCustomer.email" /></span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_emall" name="emall" class="form-control"
									changelog="<s:property value=" transportOrder.emall "/>"
									title="" value="<s:property value=" transportOrder.emall "/>" />
							</div>
						</div>
					</div>
					<div class="row">
						<!-- 电话-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.mobile" /></span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_phone" name="phone" class="form-control"
									changelog="<s:property value=" transportOrder.phone "/>"
									title="" value="<s:property value=" transportOrder.phone "/>" />
							</div>
						</div>
						<!-- 接收单位-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">接收单位</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_receiveCompany" name="receiveCompany" class="form-control"
									changelog="<s:property value=" transportOrder.receiveCompany "/>"
									title="" value="<s:property value=" transportOrder.receiveCompany "/>" />
							</div>
						</div>
						<!-- 接收地址-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">接收地址</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_address" name="address" class="form-control"
									changelog="<s:property value=" transportOrder.address "/>"
									title="" value="<s:property value=" transportOrder.address "/>" />
							</div>
						</div>
					</div>
					<div class="row">
						<!-- 接收要求-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">接收要求</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_receiveRequest" name="receiveRequest" class="form-control"
									changelog="<s:property value=" transportOrder.receiveRequest "/>"
									title="" value="<s:property value=" transportOrder.receiveRequest "/>" />
							</div>
						</div> --%>
						<!-- 备注-->
					<%-- 	<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">接收要求</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_note" name="note" class="form-control"
									changelog="<s:property value=" transportOrder.note "/>"
									title="" value="<s:property value=" transportOrder.note "/>" />
							</div>
						</div> --%>
						
					</div>
				
				<%-- <div id="wlxx">
					<div class="row">
						<div class="panel"
							style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
							<div class="panel-heading text-left">
								<h3 class="panel-title" style="font-family: 黑体;">
									<i class="glyphicon glyphicon-bookmark"></i>
									物流信息
								</h3>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- 运送人-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">运送人</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_transUser" name="transUser" class="form-control"
									changelog="<s:property value=" transportOrder.transUser "/>"
									title="" value="<s:property value=" transportOrder.transUser "/>" />
							</div>
						</div>
						<!-- 运送人手机号-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">运送人手机</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_transUserMobilePhone" name="transUserMobilePhone" class="form-control"
									changelog="<s:property value=" transportOrder.transUserMobilePhone "/>"
									title="" value="<s:property value=" transportOrder.transUserMobilePhone "/>" />
							</div>
						</div>
						<!-- 物流公司-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">物流公司</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_transCompany" name="transCompany" class="form-control"
									changelog="<s:property value=" transportOrder.transCompany "/>"
									title="" value="<s:property value=" transportOrder.transCompany "/>" />
							</div>
						</div>
					</div>
					<div class="row">
						<!-- 预计运输日期-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date formatDate">
								<span class="input-group-addon">预计运输日期</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_expectedDeliveryDate" name="expectedDeliveryDate" class="form-control"
									changelog="<s:property value=" transportOrder.expectedDeliveryDate "/>"
									title="" value="<s:date name=" transportOrder.expectedDeliveryDate " format="yyyy-MM-dd"/>" />
							</div>
						</div>
						<!-- 实际运输日期-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date formatDate">
								<span class="input-group-addon">实际运输日期</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_deliveryDate" name="deliveryDate" class="form-control"
									changelog="<s:property value=" transportOrder.deliveryDate "/>"
									title="" value="<s:date name=" transportOrder.deliveryDate " format="yyyy-MM-dd"/>" />
							</div>
						</div>
						
					
						<!-- 预计提货时间-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date formatDate">
								<span class="input-group-addon">预计提货时间</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_ExpectedDeliveryTime" name="ExpectedDeliveryTime" class="form-control"
									changelog="<s:property value=" transportOrder.ExpectedDeliveryTime "/>"
									title="" value="<s:date name=" transportOrder.ExpectedDeliveryTime " format="yyyy-MM-dd"/>" />
							</div>
						</div>
						<!-- 预计送达日期-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date formatDate">
								<span class="input-group-addon">预计送达日期</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_willDate" name="willDate" class="form-control"
									changelog="<s:property value=" transportOrder.willDate "/>"
									title="" value="<s:date name=" transportOrder.willDate " format="yyyy-MM-dd"/>" />
							</div>
						</div>
					</div>
				</div> --%>
<%-- 				<div id="qsxx">
					<div class="row">
						<div class="panel"
								style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
								<div class="panel-heading text-left">
									<h3 class="panel-title" style="font-family: 黑体;">
										<i class="glyphicon glyphicon-bookmark"></i>
										签收信息
									</h3>
								</div>
							</div>
					</div>
					<br>
					<div class="row">
						<!-- 运送单号-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">签收人</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_receiver" name="receiver" class="form-control"
									changelog="<s:property value=" transportOrder.receiver "/>"
									title="" value="<s:property value=" transportOrder.receiver " />" />
							</div>
						</div>
						<!-- 签收日期-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date formatDate">
								<span class="input-group-addon">签收日期</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_receiveDate" name="receiveDate" class="form-control"
									changelog="<s:property value=" transportOrder.receiveDate "/>"
									title="" value="<s:date name=" transportOrder.receiveDate " format="yyyy-MM-dd" />" />
							</div>
						</div>
						<!-- 签收确认人-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">签收确认人</span>
								<input type="hidden" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_signConfirmationUser" name="signConfirmationUser-id" class="form-control"
									/>
								<input type="text" size="20" maxlength="25" readonly="readonly"
									id="transportOrder_signConfirmationUser_name" name="signConfirmationUser-name" class="form-control"
									changelog="<s:property value=" transportOrder.signConfirmationUser.name "/>"
									title="" value="<s:property value=" transportOrder.signConfirmationUser.name "/>" />
								<span class="input-group-btn">
									<button class="btn btn-info" type="button"
									onClick="selectConfirmUser()">
									<i class="glyphicon glyphicon-search"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- 确认日期-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group input-append date formatDate">
								<span class="input-group-addon">确认日期</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_signConfirmationDate" name="signConfirmationDate" class="form-control"
									changelog="<s:property value=" transportOrder.signConfirmationDate "/>"
									title="" value="<s:date name=" transportOrder.signConfirmationDate " format="yyyy-MM-dd" />" />
							</div>
						</div>
						<!-- 运输费-->
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="input-group">
								<span class="input-group-addon">运输费</span>
								<input type="text" size="20" maxlength="25" 
									id="transportOrder_transFee" name="transFee" class="form-control"
									changelog="<s:property value=" transportOrder.transFee "/>"
									title="" value="<s:property value=" transportOrder.transFee " />" />
							</div>
						</div>
					</div>
				</div> --%>
				</form>
				</div>
				<!--table表格-->
				<div class="HideShowPanel" style="display: none;">
					<div id="leftDiv" class="col-md-5 col-xs-12" style="clear:both;width:100%">
						<div class="box box-success" >
							<div class="box-header with-border">
								<i class="glyphicon glyphicon-leaf"></i>
								<h3 class="box-title">
								待运输样本</h3>
							</div>
							<div class="box-body ipadmini" id="lefttable">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="transportOrderTemp" style="font-size: 14px;">
								</table>
							</div>
						</div>
					</div>
				
					<div id="rightDiv" class="col-md-7 col-xs-12" style="clear:both;width: 100%">
						<div class="box box-success">
							<div class="box-header with-border">
								<i class="glyphicon glyphicon-leaf"></i>
								<h3 class="box-title">
								运输明细</h3>
							</div>
							<div class="box-body ipadmini" id="lefttable">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="transportOrderItem" style="font-size: 14px;">
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="transportSampleItem" style="font-size: 14px;">
						</table>
				</div>
				<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="transportSampleState" style="font-size: 14px;">
						</table>
				</div>
			</div>
			<%-- <div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">
							<fmt:message key="biolims.common.back" />
						</button>
						<button type="button" class="btn btn-primary" id="next">
							<fmt:message key="biolims.common.nextStep" />
						</button>
					</div>
				</div> --%>
		</div>
	</div>
	
	<script type="text/javascript"
		src="${ctx}/js/tra/transport/timePacker.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/tra/transport/transportOrderEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/tra/transport/transportOrderItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/tra/transport/transportOrderTemp.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/tra/transport/transportSampleItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/tra/transport/transportSampleState.js"></script>
</body>
</html>