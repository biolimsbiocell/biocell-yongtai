﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/tra/transport/transportOrderDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="transportOrder_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="transportOrder_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	</tr>
			<tr>
           	 	<td class="label-title">运输申请</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_transportApply" searchField="true" name="transportApply"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">分类</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	</tr>
			<tr>
           	 	<td class="label-title">批准人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_confirmUser" searchField="true" name="confirmUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">批准日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_confirmDate" searchField="true" name="confirmDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">运送人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_transUser" searchField="true" name="transUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">承运公司</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="transportOrder_transCompany" searchField="true" name="transCompany"  class="input-20-length"></input>
               	</td>
           	</tr>
			<tr>
           	 	<td class="label-title">运送单号</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="transportOrder_transCode" searchField="true" name="transCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">联系人</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="transportOrder_linkman" searchField="true" name="linkman"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">联系电话</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="transportOrder_phone" searchField="true" name="phone"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">接收单位</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="transportOrder_receiveCompany" searchField="true" name="receiveCompany"  class="input-20-length"></input>
               	</td>
           	</tr>
			<tr>
           	 	<td class="label-title">接收地址</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="transportOrder_address" searchField="true" name="address"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">Email</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="transportOrder_emall" searchField="true" name="emall"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">航班号</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="transportOrder_flightCode" searchField="true" name="flightCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">预计送达日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_willDate" searchField="true" name="willDate"  class="input-20-length"></input>
               	</td>
           	</tr>
			<tr>
           	 	<td class="label-title">目的地</td>
               	<td align="left">
                    		<input type="text" maxlength="40" id="transportOrder_arrived" searchField="true" name="arrived"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">出发日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_fromDate" searchField="true" name="fromDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">签收日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_receiveDate" searchField="true" name="receiveDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">运费</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="transportOrder_transFee" searchField="true" name="transFee"  class="input-20-length"></input>
               	</td>
           	</tr>
			<tr>
           	 	<td class="label-title" style="display:none">工作流状态ID</td>
               	<td align="left" style="display:none">
                    		<input type="text" maxlength="30" id="transportOrder_state" searchField="true"  name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="transportOrder_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content1" searchField="true" style="display:none" name="content1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content2" searchField="true" style="display:none" name="content2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content3" searchField="true" style="display:none" name="content3"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content4" searchField="true" style="display:none" name="content4"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content5" searchField="true" style="display:none" name="content5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content6" searchField="true" style="display:none" name="content6"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content7" searchField="true" style="display:none" name="content7"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportOrder_content8" searchField="true" style="display:none" name="content8"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_content9" searchField="true" style="display:none" name="content9"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_content10" searchField="true" style="display:none" name="content10"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_content11" searchField="true" style="display:none" name="content11"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportOrder_content12" searchField="true" style="display:none" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_transportOrder_div"></div>
   		
</body>
</html>



