﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/tra/transport/transportApply.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="transportApply_id"
                   	 name="id" searchField="true" title="编码"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="transportApply_name"
                   	 name="name" searchField="true" title="描述"    />

                   	</td>

			</tr>
			<tr>                   	

			<g:LayOutWinTag buttonId="showproject" title="选择项目" width="600" height="580"

				hasHtmlFrame="true"
				html="${ctx}/exp/project/project/projectSelect.action"
				isHasSubmit="false" functionName="ProjectFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('transportApply_project').value=rec.get('id');
				document.getElementById('transportApply_project_name').value=rec.get('name');" />
               	 	<td class="label-title" >项目</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_project_name" searchField="true"  name="project.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_project" name="transportApply.project.id"  value="" > 
 						<img alt='选择项目' id='showproject' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>

			<g:LayOutWinTag buttonId="showtype" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
				documentId="transportApply_type"
				documentName="transportApply_type_name"
			/>
               	 	<td class="label-title" >分类</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_type" name="transportApply.type.id"  value="" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>                   	
			<%-- <g:LayOutWinTag buttonId="showobjType" title="选择物品类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showObjTypeList" hasSetFun="true"
				documentId="transportApply_objType"
				documentName="transportApply_objType_name"
			/>
               	 	<td class="label-title" >物品类型</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_objType_name" searchField="true"  name="objType.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_objType" name="transportApply.objType.id"  value="" > 
 						<img alt='选择物品类型' id='showobjType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
                   	<td class="label-title" >物品类型</td>
                   	<td align="left"  >
					<select id="transportApply_objType" name="transportApply.objType" searchField="true" class="input-10-length" onChange="change()">
								<option value="0" <s:if test="transportApply.objType==0">selected="selected"</s:if>>活体</option>
								<option value="1" <s:if test="transportApply.objType==1">selected="selected"</s:if>>质粒/菌液</option>
								<option value="2" <s:if test="transportApply.objType==2">selected="selected"</s:if>>细胞</option>
								<option value="3" <s:if test="transportApply.objType==3">selected="selected"</s:if>>组织</option>								
					</select>
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择申请人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="ApplyUserFun" 
 				hasSetFun="true"
				documentId="transportApply_createUser"
				documentName="transportApply_createUser_name" />
               	 	<td class="label-title" >申请人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_createUser" name="transportApply.createUser.id"  value="" > 
 						<img alt='选择申请人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                        		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >申请部门</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="40" id="transportApply_applyDepartment"
                   	 name="applyDepartment" searchField="true" title="申请部门"    />

                   	</td>
               	 	<td class="label-title" >申请日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />

                   	</td>
			</tr>
			<tr>                   	
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择批准人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="ConUserFun" 
 				hasSetFun="true"
				documentId="transportApply_confirmUser"
				documentName="transportApply_confirmUser_name" />
               	 	<td class="label-title" >批准人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_confirmUser" name="transportApply.confirmUser.id"  value="" > 
 						<img alt='选择批准人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                        		
                   	</td>

               	 	<td class="label-title" >批准日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
           	</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >工作流状态ID</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="transportApply_state"
                   	 name="state" searchField="true" title="工作流状态ID"   style="display:none"    />

 					<input type="hidden" id="transportApply_state" name="state" searchField="true" value=""/>"
                   	</td>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="transportApply_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />

                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content1</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content1"
                   	 name="content1" searchField="true" title="content1"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content2</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content2"
                   	 name="content2" searchField="true" title="content2"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content3</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content3"
                   	 name="content3" searchField="true" title="content3"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content4</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content4"
                   	 name="content4" searchField="true" title="content4"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content5</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content5"
                   	 name="content5" searchField="true" title="content5"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content6</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content6"
                   	 name="content6" searchField="true" title="content6"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content7</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content7"
                   	 name="content7" searchField="true" title="content7"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content8</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="transportApply_content8"
                   	 name="content8" searchField="true" title="content8"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content9</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent9" name="startcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content91" name="content9##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent9" name="endcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content92" name="content9##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content10</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent10" name="startcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content101" name="content10##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent10" name="endcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content102" name="content10##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content11</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent11" name="startcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content111" name="content11##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent11" name="endcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content112" name="content11##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content12</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent12" name="startcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content121" name="content12##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent12" name="endcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content122" name="content12##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_transportApply_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_transportApply_tree_page"></div>
</body>
</html>



