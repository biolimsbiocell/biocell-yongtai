﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=transportApply&id=${transportApply.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/tra/transport/transportApplyEdit.js"></script>
<%--  <script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script> --%>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编码</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="transportApply_id"
                   	 name="transportApply.id" title="编码" readonly = "readOnly" class="text input readonlytrue" 
                   	   
	value="<s:property value="transportApply.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="50" maxlength="60" id="transportApply_name"
	                   	 name="transportApply.name" title="描述" value="<s:property value="transportApply.name"/>"/>
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showproject" title="选择项目编号" width="600" height="580"
				hasHtmlFrame="true"
				html="${ctx}/exp/project/project/projectSelect.action"
				isHasSubmit="false" functionName="ProjectFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('transportApply_project').value=rec.get('id');
				document.getElementById('transportApply_project_projectId').value=rec.get('projectId');" />
				
               	 	<td class="label-title" >项目编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="transportApply_project_projectId"  value="<s:property value="transportApply.project.projectId"/>"  readonly="readOnly"  />
 						<input type="hidden" id="transportApply_project" name="transportApply.project.id"  value="<s:property value="transportApply.project.id"/>" > 
 						<img alt='选择项目编号' id='showproject' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
			<g:LayOutWinTag buttonId="showtype" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="traApply" hasSetFun="true"
				documentId="transportApply_type"
				documentName="transportApply_type_name"
			/>
			
			
               	 	<td class="label-title" >分类</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="transportApply_type_name"  value="<s:property value="transportApply.type.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="transportApply_type" name="transportApply.type.id"  value="<s:property value="transportApply.type.id"/>" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			<%-- <g:LayOutWinTag buttonId="showobjType" title="选择物品类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showObjTypeList" hasSetFun="true"
				documentId="transportApply_objType"
				documentName="transportApply_objType_name"
			/>
			
			
               	 	<td class="label-title" >物品类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="transportApply_objType_name"  value="<s:property value="transportApply.objType.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="transportApply_objType" name="transportApply.objType.id"  value="<s:property value="transportApply.objType.id"/>" > 
 						<img alt='选择物品类型' id='showobjType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
				<td class="label-title" >物品类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>             	 	
                   	<td align="left"  >
					<select id="transportApply_objType" name="transportApply.objType" class="input-10-length" onChange="change()" style="width:150px">
						<s:if test="transportApply.id!=null && transportApply.id != 'NEW' ">
								<s:if test="transportApply.objType==0"> <option value="0" selected="selected">活体</option> </s:if> 
								<s:if test="transportApply.objType==1"> <option value="1" selected="selected">质粒/菌液</option> </s:if> 
								<s:if test="transportApply.objType==2"> <option value="2" selected="selected">细胞</option> </s:if> 
								<s:if test="transportApply.objType==3"> <option value="3" selected="selected">组织</option> </s:if> 	
						</s:if>		
						<s:if test="transportApply.id eq 'NEW'">
							<option value="0" selected="selected" >活体</option>
							<option value="1">质粒/菌液</option>
							<option value="2">细胞</option>
							<option value="3">组织</option>	
						</s:if>									
					</select>
                   	</td>
			
			<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择申请人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('transportApply_createUser').value=rec.get('id');
				document.getElementById('transportApply_createUser_name').value=rec.get('name');" />
				 --%>
			
			
               	 	<td class="label-title" >项目来源地域</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="transportApply_projectCity"
	                   	 name="transportApply.projectCity" title="项目来源地域" value="<s:property value="transportApply.projectCity"/>"/>
                   	</td>
               	 	
			<tr>
					<td class="label-title" >合同名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="transportApply_crmContractName"
	                   	 name="transportApply.crmContractName" title="描述" value="${requestScope.project.name}"/>
                   	</td>
                   	<td class="label-title" >合同编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="transportApply_crmContractNum"
	                   	 name="transportApply.crmContractNum" title="描述" value="<s:property value="transportApply.crmContractNum"/>"/>
                   	</td>
                   	</td><td class="label-title" >联系人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="transportApply_crmLinkMan"
	                   	 name="transportApply.crmLinkMan" title="描述" value="<s:property value="transportApply.crmLinkMan"/>"/>
                   	</td>
			</tr>
			<tr>
				<td class="label-title" >委托人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="transportApply_crmCustomerName"
	                   	 name="transportApply.crmCustomerName" title="描述" value="<s:property value="transportApply.crmCustomerName"/>"/>
                   	</td>
				<td class="label-title" >委托单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="transportApply_crmCustomerDept"
	                   	 name="transportApply.crmCustomerDept" title="描述" value="<s:property value="transportApply.crmCustomerDept"/>"/>
                   	<td class="label-title" >联系电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="60" id="transportApply_crmLinkMoblie"
	                   	 name="transportApply.crmLinkMoblie" title="描述" value="<s:property value="transportApply.crmLinkMoblie"/>"/>
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >申请日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="transportApply_createDate"
                   	 name="transportApply.createDate" title="申请日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="transportApply.createDate" format="yyyy-MM-dd "/>"
                   	                      	  />
                   	  
                   	</td>
			
			<%--
               	 	<td class="label-title" >申请部门</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="40" id="transportApply_applyDepartment"
                   	 name="transportApply.applyDepartment" title="申请部门"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="transportApply.applyDepartment"/>"
                   	  />
                   	  
                   	</td>
			
			
			 --%>
				<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人1"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="confirmUser" 
	 				hasSetFun="true" width="900" height="500"
					documentId="transportApply_confirmUser"
					documentName="transportApply_confirmUser_name"/> 
				
			
			
               	 	<td class="label-title" >审核人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="transportApply_confirmUser_name"  value="<s:property value="transportApply.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="transportApply_confirmUser" name="transportApply.confirmUser.id"  value="<s:property value="transportApply.confirmUser.id"/>" > 
 						<img alt='选择批准人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                        		
                   	</td>
                   	
                   	<g:LayOutWinTag buttonId="showconfirmUserTwo" title="选择审核人2"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="confirmUserTwo" 
	 				hasSetFun="true" width="900" height="500"
					documentId="transportApply_confirmUserTwo"
					documentName="transportApply_confirmUserTwo_name"/> 
					
                   	<td class="label-title" >审核人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="transportApply_confirmUserTwo_name"  value="<s:property value="transportApply.confirmUserTwo.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="transportApply_confirmUserTwo" name="transportApply.confirmUserTwo.id"  value="<s:property value="transportApply.confirmUserTwo.id"/>" > 
 						<img alt='选择批准人' id='showconfirmUserTwo' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                    		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >批准日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="transportApply_confirmDate"
                   	 name="transportApply.confirmDate" title="批准日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="transportApply.confirmDate" format="yyyy-MM-dd "/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >工作流状态ID</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="transportApply_state" name="transportApply.state" value="<s:property value="transportApply.state"/>"/>
                   	</td>
			
			
               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="30" id="transportApply_stateName"
	                   	 name="transportApply.stateName" title="工作流状态" readonly = "readOnly" class="text input readonlytrue"  
						 value="<s:property value="transportApply.stateName"/>"/>
                   	</td>
                   	<td class="label-title" >申请人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="transportApply_createUser_name"  value="<s:property value="transportApply.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="transportApply_createUser" name="transportApply.createUser.id"  value="<s:property value="transportApply.createUser.id"/>" > 
 						 <%--   <img alt='选择申请人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>                   		
                   	</td>
			</tr>
			<tr>
				<td class="label-title" >第一审核日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="transportApply_firstUserDate"
                   	 name="transportApply.firstUserDate" title="第一审核日期" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" value="<s:date name="transportApply.firstUserDate" format="yyyy-MM-dd "/>"
                   	                      	  />
                   	  
                   	</td>
                 <td class="label-title"  style="display:none"  >后续实验ID</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
 						<input type="hidden" id="transportApply_nextStepName_id" name="transportApply.nextStep" value="<s:property value="transportApply.nextStep"/>"/>
                   	</td>
                   	
               	 	<td class="label-title" >后续实验</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  	<input type="text" size="20" maxlength="30" id="transportApply_nextStepName"
                   	 		name="transportApply.nextStepName" title="后续实验" readonly="readOnly" 
                   	  		value="<s:property value="transportApply.nextStepName"/>"
                   	  />
                   	  	<img alt='选择后续实验名称' id='shownextStepName' src='${ctx}/images/img_lookup.gif' 	class='detail'   onclick="selectnextStepNameTable();"  />
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content1</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content1"
                   	 name="transportApply.content1" title="content1"
                   	   
	value="<s:property value="transportApply.content1"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content2</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content2"
                   	 name="transportApply.content2" title="content2"
                   	   
	value="<s:property value="transportApply.content2"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content3</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content3"
                   	 name="transportApply.content3" title="content3"
                   	   
	value="<s:property value="transportApply.content3"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content4</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content4"
                   	 name="transportApply.content4" title="content4"
                   	   
	value="<s:property value="transportApply.content4"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content5</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content5"
                   	 name="transportApply.content5" title="content5"
                   	   
	value="<s:property value="transportApply.content5"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content6</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content6"
                   	 name="transportApply.content6" title="content6"
                   	   
	value="<s:property value="transportApply.content6"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content7</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content7"
                   	 name="transportApply.content7" title="content7"
                   	   
	value="<s:property value="transportApply.content7"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content8</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="transportApply_content8"
                   	 name="transportApply.content8" title="content8"
                   	   
	value="<s:property value="transportApply.content8"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content9</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	  	<input type="text" size="20" maxlength="" id="transportApply_content9"
                   	 name="transportApply.content9" title="content9"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="transportApply.content9" format="yyyy-MM-dd"/>" 
                   	      style="display:none" 
                   	  />
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content10</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	  	<input type="text" size="20" maxlength="" id="transportApply_content10"
                   	 name="transportApply.content10" title="content10"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="transportApply.content10" format="yyyy-MM-dd"/>" 
                   	      style="display:none" 
                   	  />
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content11</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	  	<input type="text" size="20" maxlength="" id="transportApply_content11"
                   	 name="transportApply.content11" title="content11"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="transportApply.content11" format="yyyy-MM-dd"/>" 
                   	      style="display:none" 
                   	  />
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content12</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	  	<input type="text" size="20" maxlength="" id="transportApply_content12"
                   	 name="transportApply.content12" title="content12"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="transportApply.content12" format="yyyy-MM-dd"/>" 
                   	      style="display:none" 
                   	  />
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="transportApplyAnimalJson" id="transportApplyAnimalJson" value="" />
            <input type="hidden" name="transportApplyBacteriaJson" id="transportApplyBacteriaJson" value="" />
            <input type="hidden" name="transportApplyCellJson" id="transportApplyCellJson" value="" />
            <input type="hidden" name="transportApplyTissueJson" id="transportApplyTissueJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="transportApply.id"/>" />
            </form>
<!--        <div id="tabs">
            <ul>
			<li><a href="#transportApplyAnimalpage">运送物品(动物)情况</a></li>
			<li><a href="#transportApplyBacteriapage">运送物品(质粒/菌液)情况</a></li>
			<li><a href="#transportApplyCellpage">运送物品(细胞)情况</a></li>
			<li><a href="#transportApplyTissuepage">运送物品(组织)情况</a></li>
           	</ul>  -->
			<div id="transportApplyAnimalpage" width="100%" height:10px></div>
			<div id="transportApplyBacteriapage" width="100%" height:10px></div>
			<div id="transportApplyCellpage" width="100%" height:10px></div>
			<div id="transportApplyTissuepage" width="100%" height:10px></div>
			</div>
        	<!-- </div> -->
	</body>
	</html>
