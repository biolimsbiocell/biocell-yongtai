﻿	<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/tra/transport/transportApplyDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="transportApply_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="transportApply_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	</tr>
			<tr>
           	 	<g:LayOutWinTag buttonId="showproject" title="选择项目" width="600" height="580"

				hasHtmlFrame="true"
				html="${ctx}/exp/project/project/projectSelect.action"
				isHasSubmit="false" functionName="ProjectFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('transportApply_project').value=rec.get('id');
				document.getElementById('transportApply_project_name').value=rec.get('name');" />
               	 	<td class="label-title" >项目</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_project_name" searchField="true"  name="project.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_project" name="transportApply.project.id"  value="" > 
 						<img alt='选择项目' id='showproject' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>

			<g:LayOutWinTag buttonId="showtype" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
				documentId="transportApply_type"
				documentName="transportApply_type_name"
			/>
               	 	<td class="label-title" >分类</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_type" name="transportApply.type.id"  value="" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
           	 	<td class="label-title" >物品类型</td>
                   	<td align="left"  >
					<select id="transportApply_objType" name="transportApply.objType" searchField="true" class="input-10-length" onChange="change()">
								<option value="0" <s:if test="transportApply.objType==0">selected="selected"</s:if>>活体</option>
								<option value="1" <s:if test="transportApply.objType==1">selected="selected"</s:if>>质粒/菌液</option>
								<option value="2" <s:if test="transportApply.objType==2">selected="selected"</s:if>>细胞</option>
								<option value="3" <s:if test="transportApply.objType==3">selected="selected"</s:if>>组织</option>								
					</select>
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择申请人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="ApplyUserFun" 
 				hasSetFun="true"
				documentId="transportApply_createUser"
				documentName="transportApply_createUser_name" />
               	 	<td class="label-title" >申请人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_createUser" name="transportApply.createUser.id"  value="" > 
 						<img alt='选择申请人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                        		
                   	</td>
           	</tr>
			<tr>
           	 	<td class="label-title" >申请部门</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="40" id="transportApply_applyDepartment"
                   	 name="applyDepartment" searchField="true" title="申请部门"    />

                   	</td>
               	 	<td class="label-title" >申请日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />

                   	</td>
			</tr>
			<tr>
           	 	<g:LayOutWinTag buttonId="showconfirmUser" title="选择批准人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="ConUserFun" 
 				hasSetFun="true"
				documentId="transportApply_confirmUser"
				documentName="transportApply_confirmUser_name" />
               	 	<td class="label-title" >批准人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="transportApply_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="transportApply_confirmUser" name="transportApply.confirmUser.id"  value="" > 
 						<img alt='选择批准人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                        		
                   	</td>

               	 	<td class="label-title" >批准日期</td>
                   	<td align="left"  >
						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                   	</td>
           	</tr>
			<tr>
           	 	<td class="label-title" style="display:none">工作流状态ID</td>
               	<td align="left" style="display:none">
                    		<input type="text" maxlength="30" id="transportApply_state" searchField="true"  name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="transportApply_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content1" searchField="true" style="display:none" name="content1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content2" searchField="true" style="display:none" name="content2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content3" searchField="true" style="display:none" name="content3"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content4" searchField="true" style="display:none" name="content4"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content5" searchField="true" style="display:none" name="content5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content6" searchField="true" style="display:none" name="content6"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content7" searchField="true" style="display:none" name="content7"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="transportApply_content8" searchField="true" style="display:none" name="content8"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportApply_content9" searchField="true" style="display:none" name="content9"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportApply_content10" searchField="true" style="display:none" name="content10"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportApply_content11" searchField="true" style="display:none" name="content11"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="transportApply_content12" searchField="true" style="display:none" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_transportApply_div"></div>
   		
</body>
</html>



