﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}

</style>
<script type="text/javascript" src="${ctx}/js/tra/transport/transportOrderAnimal.js"></script>
</head>
<body>
	<div id="transportOrderAnimaldiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
	<input type="hidden" id="applyId" value="${requestScope.wid}">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="batccc_data_div" style="display: none">
		<table>
			<tr>
					<td class="label-title">生物级别</td>
					<td align="left"  >
						<select id="clean" name="clean" class="input-10-length" style="width:152px;">
									<option value="无" >无</option>
									<option value="SPF" >SPF</option>
									<option value="清洁级" >清洁级</option>
						</select>
                   	</td>
             </tr>
		</table>
	</div>
</body>
</html>


