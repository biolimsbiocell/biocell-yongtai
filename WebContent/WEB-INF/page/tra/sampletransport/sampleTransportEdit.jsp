<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
#btn_submit{
	display:none;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>样本运输
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>样本运输</small>
								</span>
							</a></li>
										<li><a class="disabled step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>样本运输明细</small></span>
								</a></li>
										<li><a class="disabled step"> <span class="step_no">3</span>
											<span class="step_descr"> Step 3<br /> <small>样本追踪情况</small></span>
								</a></li>
						</ul>
					</div>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentMaintain.id"/> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
									
										
                   						<input type="text" readonly="readonly"
											size="50" maxlength="127" id="sampleTransport_id"
											name="id"  changelog="<s:property value="sampleTransport.id.name"/>"
											  class="form-control"   
											value="<s:property value="sampleTransport.id"/>"
																 />  
					
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepair.name"/> </span> 
                   						<input type="text"
											size="50" maxlength="127" id="sampleTransport_name"
											name="name"  changelog="<s:property value="sampleTransport.name"/>"
											  class="form-control"   
											value="<s:property value="sampleTransport.name"/>"
																 />  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentMaintain.createUser"/> </span> 
										<input type="text" size="20" readonly="readOnly"
												id="sampleTransport_createUser_name"  changelog="<s:property value="sampleTransport.createUser.name"/>"
												value="<s:property value="sampleTransport.createUser.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="sampleTransport_createUser_id" name="createUser-id"
												value="<s:property value="sampleTransport.createUser.id"/>">
										
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentMaintain.state"/> </span> 
                   						<input type="text"
											size="50" maxlength="127" id="sampleTransport_stateName"
											name="stateName"  changelog="<s:property value="sampleTransport.stateName.name"/>"
											readonly = "readOnly" class="form-control readonlytrue" 
											value="<s:property value="sampleTransport.stateName"/>"
											/>  
									</div>
								</div>		
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.transportUser"/><img class='requiredimage' src='${ctx}/images/required.gif' /></span> 
									
										<input type="text" size="20" name="transportUser"
												id="sampleTransport_transportUser"  changelog="<s:property value="sampleTransport.transportUser"/>"
												value="<s:property value="sampleTransport.transportUser"/>"
												class="form-control" /> 
										<%-- <input type="hidden"
												id="sampleTransport_transportUser_id" name="transportUser-id"
												value="<s:property value="sampleTransport.transportUser.id"/>"> --%>
										
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.transportCompany"/><img class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
									
                   						<input type="text"
											size="50" maxlength="127" id="sampleTransport_transportCompany"
											name="transportCompany"  changelog="<s:property value="sampleTransport.transportCompany"/>"
											class="form-control"   
											value="<s:property value="sampleTransport.transportCompany"/>"
						 					/> 
						 				<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showTransportCompany()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>		
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.trackingNumber"/><img class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
                   						<input type="text"
											size="50" maxlength="127" id="sampleTransport_trackingNumber"
											name="trackingNumber"  changelog="<s:property value="sampleTransport.trackingNumber.name"/>"
											class="form-control"   
											value="<s:property value="sampleTransport.trackingNumber"/>"
											 />  
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group input-append date form_datetime">
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentState.startDate"/></span>
									<input type="text" size="20" maxlength="25" class="form-control"
											id="sampleTransport_beginDate" name="beginDate" class="Wdate" 
										 	value="<s:date name=" sampleTransport.beginDate " format="yyyy-MM-dd"/>" /> 
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group input-append date form_datetime">
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentState.endDate"/> </span> 
									<input type="text" size="20" maxlength="25" class="form-control"
											id="sampleTransport_endDate" name="endDate" class="Wdate" 
										 	value="<s:date name=" sampleTransport.endDate " format="yyyy-MM-dd"/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepair.note"/> </span> 
									
										
                   						<input type="text"
											size="50" maxlength="127" id="sampleTransport_note"
											name="note"  changelog="<s:property value="sampleTransport.note.name"/>"
											class="form-control"   
											value="<s:property value="sampleTransport.note"/>"
																	 />  
									</div>
								</div>				
								</div>
								<br/>
								<div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> 自定义字段
													
											</h3>
										</div>
									</div>
								</div>
							<div id="fieldItemDiv"></div>
								
							<br> 
							<input type="hidden" name="sampleTransportItemJson"
								id="sampleTransportItemJson" value="" /> 
							<input type="hidden" name="sampleTrackingConditionJson"
								id="sampleTrackingConditionJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="sampleTransport.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="sampleTransport.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="sampleTransportItemTable" style="font-size: 14px;">
						</table>
					</div>
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="sampleTrackingConditionTable" style="font-size: 14px;">
						</table>
					</div>
					</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/tra/sampletransport/sampleTransportEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/tra/sampletransport/sampleTransportItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/tra/sampletransport/sampleTrackingCondition.js"></script>
</body>

</html>	
