<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>审批历史记录</title> -->
<head>
<jsp:include page="/WEB-INF/page/workflow/include/workflow.jsp"/>
<script type="text/javascript" src="${ctx}/js/history/showTaskVariablesList.js"></script>
</head>
<body>
<div>
	<input type="hidden" id="instance_id" value="${requestScope.instanceId}">
	<div id="show_history_task_div"></div>
</div>
</body>
</html>