<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>流程实例管理</title> -->
<head>
<link rel=stylesheet type="text/css" href="${ctx}/css/css-style.css">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<jsp:include page="/WEB-INF/page/workflow/include/workflow.jsp"/>
<script type="text/javascript" src="${ctx}/js/processinstance/showProcessInstanceList.js "></script>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchdutyuser = Ext.get('searchdutyuser');
searchdutyuser.on('click', 
 function zrr(){
var win = Ext.getCmp('zrr');
if (win) {win.close();}
var zrr= new Ext.Window({
id:'zrr',modal:true,title:biolims.common.chooseInitiator,layout:'fit',width:500,height:400,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=zrr' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 zrr.close(); }  }]  });     zrr.show(); }
);
});
 function setzrr(id,name){
document.getElementById('send_user').value=id;document.getElementById('send_user_name').value=name;
var win = Ext.getCmp('zrr')
if(win){win.close();}
}
</script>

<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchForm = Ext.get('searchForm');
searchForm.on('click', 
 function searchFormFun(){
var win = Ext.getCmp('searchFormFun');
if (win) {win.close();}
var searchFormFun= new Ext.Window({
id:'searchFormFun',modal:true,title:biolims.common.selectFormType,layout:'fit',width:500,height:400,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/applicationTypeTable/applicationTypeTableSelect.action?flag=searchFormFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 searchFormFun.close(); }  }]  });     searchFormFun.show(); }
);
});
 function setsearchFormFun(id,name){
document.getElementById('form_name').value=id;document.getElementById('form_name_name').value=name;
var win = Ext.getCmp('searchFormFun')
if(win){win.close();}
}
</script>


</head>
<body>
<div>
<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
	<div class="main-content">
		<table >
			<tr>
				<td>
					<span class="label-title"><fmt:message key="biolims.common.initiator"></fmt:message></span>
					<input type='hidden' name="send_user" id="send_user">
					<input type='text' name="send_user_name" id="send_user_name">
					<img alt='<fmt:message key="biolims.common.selectInitiator"></fmt:message>' id='searchdutyuser' src='${ctx}/images/img_lookup.gif' name='searchtype' class='detail' />
				</td>
				<td>
					<span class="label-title"><fmt:message key="biolims.common.businessID"></fmt:message></span>
					<input type='text' name="form_id" id="form_id">
				</td>
				<td>
					<span class="label-title"><fmt:message key="biolims.common.formTypeID"></fmt:message></span>
					<input type='hidden' name="form_name" id="form_name">
					<input type='text' name="form_name_name" id="form_name_name">
					<img alt='<fmt:message key="biolims.common.formType"></fmt:message>' id='searchForm' src='${ctx}/images/img_lookup.gif' name='searchForm' class='detail' />
				</td>
				<td>
					<button id="search_btn"><fmt:message key="biolims.common.search"></fmt:message></button>
				</td>
			</tr>
		</table>
	</div>
	<div id="show_process_instance_div"></div>
</div>
</body>
</html>