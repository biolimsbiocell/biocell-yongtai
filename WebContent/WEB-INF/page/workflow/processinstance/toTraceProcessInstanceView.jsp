<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>流程实例追踪</title> -->
<head>
<link href="${ctx }/css/workflow-style.css" type="text/css" rel="stylesheet">
<link href="${ctx }/js/jquery-qtip/jquery.qtip.min.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="${ctx}/javascript/lib/jquery-1.7.1.js"></script>
<script type="text/javascript" src="${ctx}/javascript/common/dataTablesExtend.js"></script>
<script type="text/javascript" src="${ctx}/js/jquery-qtip/jquery.qtip.pack.js "></script>
<script type="text/javascript" src="${ctx}/js/jquery-outerhtml/jquery.outerhtml.js"></script>
<script type="text/javascript" src="${ctx}/js/processinstance/toTraceProcessInstanceView.js"></script>
</head>
<body>
<div>
	<input type="hidden" id="instance_id" value="${requestScope.instanceId}">
	<img alt='跟踪工作流' src='${ctx}/workflow/processinstance/traceProcessInstanceImage.action?instanceId=${requestScope.instanceId}' style='position:absolute; left:0px; top:0px;'>
</div>
</body>
</html>