<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>流程任务办理</title> -->
<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
</head>
<style>
		td.lbxs {
		border:1px solid #AEDEF2;
		background:ghostwhite;
		font-size:12px;
		font-family: 新宋体;
		color: #333;
		text-align:center;
		}
		
	   	.frame-table {
	   	
	   	border-style:solid;
	   		background-color: ghostwhite;
	   		border:1px solid #AEDEF2;
			font-size:12px;
			font-family: 新宋体;
		}
</style>
<body>
	<input type="hidden" id="instance_id" value="${requestScope.instanceId}">
	<table class="table">
		<tr  class="text">
			<td class="label-title" style="font-size: 16px;font-weight: 600;padding-left: 10px;width: 100px;">
			操作
			</td>
			<td align="left">
				<s:select name="oper" id="oper" class="form-control"  list="#request.operMap" theme="simple">
				</s:select>
			</td>
		</tr>
		<tr id="shift_user"  class="text" style="display:none;">
			<td class="label-title">
				转办人
			</td>
			<td align="left">
				<input type="text" id="shift_handle">
			</td>
		</tr>
		<tr id="opinion_tr"  class="text">
			<td class="label-title" style="font-size: 16px;font-weight: 600;width: 100px;padding-left: 10px;line-height: 7;">
				请填写意见
			</td>
			<td align="left">
				<textarea id="opinion"  style="width: 100%; height: 120px;"></textarea>
			</td>
		</tr>
	</table>
	<div id="cs">
	<table class="table table-bordered">
				<tr class="control textboxcontrol" height="15">
					<td valign="top" align="center" class="controlTitle lbxs" width="5%"  nowrap><label
						title="序号" class="text label" style="color: black">序号 </label></td>
					<td valign="top" align="center" class="controlTitle lbxs" width="15%" nowrap><label
						title="处理日期" class="text label" style="color: black">任务名称</label></td>
					<td valign="top" align="center" class="controlTitle lbxs" width="10%" nowrap><label
						title="模块名称" class="text label" style="color: black">办理人</label></td>
					<td valign="top" align="center" class="controlTitle lbxs" width="15%" nowrap><label
						title="办理人" class="text label" style="color: black">办理时间</label></td>
					<td valign="top" align="center" class="controlTitle lbxs" width="10%" nowrap><label
						title="意见" class="text label" style="color: black">办理结果</label></td>
					<td valign="top" align="center" class="controlTitle lbxs" width="35%" nowrap><label
						title="意见" class="text label" style="color: black">办理意见</label></td>	
					<td valign="top" align="center" class="controlTitle lbxs" width="10%"  nowrap><label
						title="意见" class="text label" style="color: black">办代/转办信息</label></td>	
				</tr>
	
	
	
	
		<s:iterator value="#request.historyTaskList" id="hisTask" status='st'>
		<tr class="text">
			<td class="lbxs">
				<s:property value='#st.index+1'/>
			</td>
			<td class="lbxs">
				<s:property value='%{#hisTask.taskName}' default="-"/>
			</td>
			<td class="lbxs">
				<s:property value='%{#hisTask.assigneeName}' default="-"/>
			</td>
			<td class="lbxs">
				<s:date name="%{#hisTask.modifyTime}" format="yyyy-MM-dd HH:mm"/>
			</td>
			<td class="lbxs">
				<s:property value='%{#hisTask.operResult}' default="-"/>
			</td>
			<td class="lbxs">
				<s:property value='%{#hisTask.info}'  default="-"/>
			</td>
			<td class="lbxs">
				<s:property value='%{#hisTask.proxyInfo}'  default="-"/>
			</td>
		</tr>
		</s:iterator>
	</table>
	</div>
	<jsp:include page="/WEB-INF/page/workflow/include/workflow.jsp"/>
<%-- 	<script type="text/javascript" src="${ctx}/js/json2.js"></script>  --%>
	<script type="text/javascript" src="${ctx}/js/processinstance/toCompleteTaskView.js"></script>
<%-- 	<script type="text/javascript" src="${ctx}/js/processinstance/toTraceProcessInstanceView.js"></script> --%>
</body>
</html>