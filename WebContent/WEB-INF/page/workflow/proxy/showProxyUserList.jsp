<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>代办人设置</title> -->
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/proxy/showProxyUserList.js"></script>
</head>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
<div>
	<div>
		<div id="proxy_user_set_div"></div>
		<div style="disply:none">
			<select id="sel_state">
				<option value="1">有效</option>
				<option value="0">无效</option>
			</select>
		</div>
		
	</div>
</div>
</body>
</html>