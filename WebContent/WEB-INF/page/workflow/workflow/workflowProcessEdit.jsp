<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="${ctx}/lims/plugins/objTree/go.js"></script>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
</head>

<body>
	<div>
				<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div class="container-fluid" style="margin-top: 56px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title"><fmt:message key="biolims.common.workflowDesign"/></h3>
					<small style="color: #fff;"> <fmt:message key="biolims.common.commandPerson"/>: <span><s:property
								value="workflowProcess.createUser.name " /></span> <fmt:message key="biolims.common.commandTime"/>: <span><s:date
								name="workflowProcess.createDate" format="yyyy-MM-dd" /></span>
								<fmt:message key="biolims.tStorage.state"/>： <span><s:property
								value="workflowProcess.stateName " /></span>
					</small>
				</div>
				<form name="form1" class="layui-form" id="form" method="post">
					<input type="hidden" id="workflowProcess_id" name="workflowProcess.id"
						value="<s:property value="workflowProcess.id" />"> <input
						type="hidden" id="contentData" name="workflowProcess.content"
						value="<s:property value="workflowProcess.content" />"> <input
						type="hidden" id="workflowProcess_createUser"
						name="workflowProcess.createUser.id" value="<s:property value="workflowProcess.createUser.id" />"> <input
						type="hidden" id="workflowProcess_createDate"
						name="workflowProcess.createDate"
						value="<s:date name="workflowProcess.createDate" format="yyyy-MM-dd" />">
					<input type="hidden" id="workflowProcess_state" name="workflowProcess.state"
						value="<s:property value="workflowProcess.state" />">
						<input type="hidden" id="workflowProcess_stateName" name="workflowProcess.stateName"
						value="<s:property value="workflowProcess.stateName" />">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.modular"/> <img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input type="hidden" id="workflowProcess_orderBlock" name="workflowProcess.orderBlock"
											value="<s:property value=" workflowProcess.orderBlock "/>" />
											<input type="text" id="workflowProcess_orderBlockName" name="workflowProcess.orderBlockName"
											class="form-control"
											value="<s:property value=" workflowProcess.orderBlockName "/>" /><span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="choseQK()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.modular"/> 
										</span> 
											<input type="text" id="workflowProcess_name" name="workflowProcess.name"
											class="form-control"
											value="<s:property value=" workflowProcess.name "/>" /><span
											class="input-group-btn">
										</span>
									</div>
								</div>
				</div>	  
				</form>
				<div>
					<div id="sample">
						<div
							style="width: 100%; display: flex; justify-content: space-between">
							<div id="myPaletteDiv"
								style="width: 100px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
							<div id="myDiagramDiv"
								style="flex-grow: 1; height: 500px; border: solid 1px black"></div>
						</div>
						<input type="hidden" value="" id="mySavedModel" />
						<textarea id="mySavedModelnew"
							style="width: 100%; height: 300px; display: none;">
{ "class": "go.GraphLinksModel",
  "linkFromPortIdProperty": "fromPort",
  "linkToPortIdProperty": "toPort",
  "nodeDataArray": [],
  "linkDataArray": []}
  </textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript"
	src="${ctx}/js/workflow/workflowProcessEdit.js"></script>

</html>