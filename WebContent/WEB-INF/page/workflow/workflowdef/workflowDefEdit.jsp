﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
				<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div class="container-fluid" style="margin-top: 56px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title"><fmt:message key="biolims.common.workflowNode" /></h3>
					<small style="color: #fff;"> <fmt:message key="biolims.common.commandPerson" />: <span><s:property
								value=" workflowDef.createUser.name " /></span> <fmt:message key="biolims.common.commandTime" />: <span><s:date
								name="workflowDef.createDate" format="yyyy-MM-dd" /></span>
					</small>
				</div>
				<div class="box-body ipadmini">
					<input type="hidden" id="handlemethod"
						value="${requestScope.handlemethod}">
					<form name="form1" class="layui-form" id="form" method="post">
						<input type="hidden" name="workflowDef.id"
							value="<s:property value=" workflowDef.id "/>" /> <input
							type="hidden" name="workflowDef.createUser.id"
							value="<s:property value=" workflowDef.createUser.id "/>" /> <input
							type="hidden" name="workflowDef.createUser.name"
							value="<s:property value=" workflowDef.createUser.name "/>" /> <input
							type="hidden" name="workflowDef.createDate"
							value="<s:date name=" workflowDef.createDate " format="yyyy-MM-dd " />" />
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.common.describe" /></span> <input type="text"
										name="workflowDef.name" class="form-control"
										value="<s:property value="workflowDef.name "/>" />
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="input-group">
									<span class="input-group-addon">  <fmt:message key="biolims.common.classPath" /></span> <input type="text"
										name="workflowDef.cls" class="form-control"
										value="<s:property value="workflowDef.cls"/>" />
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4" id ="orderss">
								<div class="input-group">
									<span class="input-group-addon"> <fmt:message key="biolims.common.selectionBlock" /> </span> <input
										type="hidden" id="orderBlockId"
										class="form-control" name="workflowDef.orderBlock"
										value="<s:property value=" workflowDef.orderBlock "/>" /><input
										type="hidden" id="orderBlockEnName" class="form-control"
										name="workflowDef.orderBlockEnName"
										value="<s:property value="workflowDef.orderBlockEnName"/>" /> <input
										type="text" id="orderBlockZhName" class="form-control"
										name="workflowDef.orderBlockName"
										value="<s:property value="workflowDef.orderBlockName"/>" /> <span
										class="input-group-btn">
										<button class="btn btn-info" type="button" onclick="choseQK()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-xs-12">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i> 
											<fmt:message key="biolims.common.optionsDefinition" />
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.common.itPassThrough" /></span><select
										class="form-control" id="workflowDef_oper"
										name="workflowDef.oper"
										value="<s:property value="workflowDef.oper"/>">
										<option value="2"><fmt:message key="biolims.common.pleaseSelect" /></option>
										<option value="1"
											<s:if test="workflowDef.oper==\"1\"">selected="selected"</s:if>>
											<fmt:message key="biolims.common.pass" /></option>
										<option value="0"
											<s:if test="workflowDef.oper==\"0\"">selected="selected"</s:if>>
											<fmt:message key="biolims.common.noPass" /></option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="input-group">
									<span class="input-group-addon"> <fmt:message key="biolims.common.stateCode" /> </span> <input type="text"
										name="workflowDef.state" class="form-control"
										value="<s:property value="workflowDef.state"/>" />
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.common.stateName" /></span> <input type="text"
										name="workflowDef.stateName" class="form-control"
										value="<s:property value="workflowDef.stateName"/>" />
								</div>
							</div>
							
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="lan" value="${sessionScope.lan}">
	<script type="text/javascript"
		src="${ctx}/js/workflowdef/workflowDefEdit.js"></script>
</body>
</html>



