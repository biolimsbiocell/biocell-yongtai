<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>流程定义管理</title> -->
<head>
<link rel=stylesheet type="text/css" href="${ctx}/css/css-style.css">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<jsp:include page="/WEB-INF/page/workflow/include/workflow.jsp"/>
<script type="text/javascript" src="${ctx}/js/processdefinition/showProcessDefinitionList.js"></script>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchForm = Ext.get('searchForm');
searchForm.on('click', 
 function searchFormFun(){
var win = Ext.getCmp('searchFormFun');
if (win) {win.close();}
var searchFormFun= new Ext.Window({
id:'searchFormFun',modal:true,title:biolims.common.selectFormType,layout:'fit',width:500,height:400,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/applicationTypeTable/applicationTypeTableSelect.action?flag=searchFormFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 searchFormFun.close(); }  }]  });     searchFormFun.show(); }
);
});
 function setsearchFormFun(id,name){
document.getElementById('key').value=id;document.getElementById('key_name').value=name;
var win = Ext.getCmp('searchFormFun')
if(win){win.close();}
}
</script>

</head>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
<div>
	<div class="main-content">
		<table>
			<tr>
				<td>
					<span class="label-title"><fmt:message key="biolims.common.inputName"></fmt:message></span>
					<input type='text' name="name" id="name">
				</td>
				<td>
					<span class="label-title"><fmt:message key="biolims.common.formType"></fmt:message></span>
					<input type='hidden' name="key" id="key">
					<input type='text' name="key_name" id="key_name">
					<img alt='<fmt:message key="biolims.common.selectFormType"></fmt:message>' id='searchForm' src='${ctx}/images/img_lookup.gif' name='searchForm' class='detail' />
				</td>
				<td>
					<button id="search_btn"><fmt:message key="biolims.common.search"></fmt:message></button>
				</td>
			</tr>
		</table>
	</div>
	<div id="show_process_definition_div"></div>
</div>
</body>
</html>