<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>流程定义配置人员</title> -->

<head>

<script type="text/javascript" src="${ctx}/js/processdefinition/showBindFormList.js"></script>
</head>
<body>
<div>
	<div>
		<input type="hidden" id="definition_id" value="${requestScope.definitionId}">

		<div id="form_set_div"></div>
	</div>
</div>
</body>
</html>