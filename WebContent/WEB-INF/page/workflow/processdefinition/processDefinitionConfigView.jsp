<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>流程定义管理</title> -->
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<head>
	
<script type="text/javascript" src="${ctx}/js/processdefinition/processDefinitionConfigView.js"></script>
<!-- g:LayOutWinTag title="选择人员"  hasHtmlFrame="true"	height="400" html="${ctx}/core/user/userSelect.action" isHasSubmit="false"  isHasButton="false" functionName="searchUser" hasSetFun="true" extRec="rec" extStr="var  gridGrid = $('#workflow_tabs_0').data('userGrid');   var record = gridGrid.getSelectionModel().getSelected();record.set('storage-id',rec.get('id'));record.set('storage-name',rec.get('name'));record.set('price',rec.get('price'));"/-->


</head>
<body>
<div>
	<div id="workflow_tabs">
		<input type="hidden" id="definition_id" value="${requestScope.definitionId}">
		<input type="hidden" id="key_id" value="<%=request.getParameter("keyId")%>">
		<ul>
			<li><a href="#workflow_tabs_0"><fmt:message key="biolims.common.setTheNode"></fmt:message></a></li>
			<li><a href="#workflow_tabs_1"><fmt:message key="biolims.common.setTheForm"></fmt:message></a></li>
		</ul>
		<div id="workflow_tabs_0">			
		</div>
		<div id="workflow_tabs_1">
		</div>
	</div>
</div>
</body>
</html>