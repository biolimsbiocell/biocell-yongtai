<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>会签规则配置</title> -->
<link rel=stylesheet type="text/css" href="${ctx}/css/css-style.css">
<body>
<div class="main-content">
	<input type="hidden" id="voteRuleId" value='<s:property value="#request.voteRule.id"/>'>
	<input type="hidden" id="activitiId" value='<s:property value="#request.activitiId"/>'>
	<input type="hidden" id="definitionId" value='<s:property value="#request.definitionId"/>'>
	<table class="fram-table">
		<tbody>
			<tr>
				<td class="label-title">一票制</td>
				<td align="center">
					<input type="radio" name="oneVote" value="1" <s:if test="#request.voteRule.oneVoteRights==1">checked="checked"</s:if> >通过
					<input type="radio" name="oneVote" value="0" <s:if test="#request.voteRule.oneVoteRights==0">checked="checked"</s:if> >否决
				</td>
			</tr>
			<tr>
				<td class="label-title">决策方式</td>
				<td align="center">
					<input type="radio" name="voteMethod" value="1" <s:if test="#request.voteRule.ruleMethod==1">checked="checked"</s:if> >通过
					<input type="radio" name="voteMethod" value="0" <s:if test="#request.voteRule.ruleMethod==0">checked="checked"</s:if> >否决
				</td>
			</tr>
			<tr>
				<td class="label-title">投票类型</td>
				<td align="center">
					<input type="radio" name="voteType" value="1" <s:if test="#request.voteRule.voteType==1">checked="checked"</s:if> >绝对票数
					<input type="radio" name="voteType" value="2" <s:if test="#request.voteRule.voteType==2">checked="checked"</s:if> >百分比
				</td>
			</tr>
			<tr>
				<td class="label-title">票数/百分比</td>
				<td align="center">
					<input type="text" id="voteNum" value='<s:property value="#request.voteRule.voteNum"/>'>
				</td>
			</tr>
			<tr>
				<td class="label-title">一票制授权人</td>
				<td align="center">
					<input type="text" id="oneVoteUser" value='<s:property value="#request.voteRule.userIds"/>'>
				</td>
			</tr>
		</tbody>
	</table>
</div>
</body>
</html>