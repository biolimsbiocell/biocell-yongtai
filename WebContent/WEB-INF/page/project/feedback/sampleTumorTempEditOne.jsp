
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleTumorTemp&id=${sampleTumorTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/sampleTumorTempEditOne.js"></script>
<s:if test="sampleTumorTemp.sampleInfo.upLoadAccessory.id != ''">
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleTumorTemp.upLoadAccessory.id}"></div>
</s:if>
<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="path" value="${requestScope.path}">
            <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleTumorTemp.upLoadAccessory.id" value="<s:property value="sampleTumorTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleTumorTemp.upLoadAccessory.fileName" value="<s:property value="sampleTumorTemp.upLoadAccessory.fileName"/>">
					<input type="hidden" id="saveType" value="zl" >
				</td>
			</tr>
				<tr>
               	 	<td class="label-title">样本编号</td>
                     	<td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
<%-- 	                   	<input type="text" 	 id="sampleTumorTemp_code" name="sampleTumorTemp.code" title="样本编号" value="<s:property value="sampleTumorTemp.code"/>" class="text input readonlytrue" readonly="readonly" /> --%>
<%-- 	                   	<input type="hidden" id="sampleTumorTemp_sampleInfo_code" name="sampleTumorTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleTumorTemp.sampleInfo.code"/>" class="text input readonlytrue" readonly="readonly" /> --%>
<%-- 						<input type="hidden" id="sampleTumorTemp_id" name="sampleTumorTemp.id" value="<s:property value="sampleTumorTemp.id"/>" /> --%>
<%-- 						<input type="hidden" id="upload_imga_name" name="sampleTumorTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleTumorTemp.sampleInfo.upLoadAccessory.fileName"/>"> --%>
<%-- 						<input type="hidden" id="upload_imga_id10" name="sampleTumorTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleTumorTemp.sampleInfo.upLoadAccessory.id"/>"> --%>
						<input type="text" 	  id="sampleTumorTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleTumorTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleTumorTemp.sampleInfo.code"/>" />
						<input type="hidden"  id="sampleTumorTemp_id"  name="sampleTumorTemp.sampleInfo.id"  value="<s:property value="sampleTumorTemp.sampleInfo.id"/>" />
						<input type="hidden"  name="sampleTumorTemp.id" value="<s:property value="sampleTumorTemp.id"/>" />
						<input type="hidden"  id="upload_imga_name" name="sampleTumorTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleTumorTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleTumorTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleTumorTemp.sampleInfo.upLoadAccessory.id"/>">
					</td>
                   
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_name"
                   	 	name="sampleTumorTemp.name" title="描述"   
						value="<s:property value="sampleTumorTemp.name"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleTumorTemp_product_name" searchField="true"  
 							name="sampleTumorTemp.productName"  value="<s:property value="sampleTumorTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleTumorTemp_product_id" name="sampleTumorTemp.productId"  
 							value="<s:property value="sampleTumorTemp.productId"/>" /> 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   	
                   	
                    </tr>
                  <tr>  	
                   	
                   	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleTumorTemp_sampleType_name"  value="<s:property value="sampleTumorTemp.sampleType.name"/>"/>
						<s:hidden id="sampleTumorTemp_sampleType_id" name="sampleTumorTemp.sampleType.id"></s:hidden>
						<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>
                   	
                 
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleTumorTemp_acceptDate"
                   	 		name="sampleTumorTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleTumorTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
                   	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_area"
                   	 	name="sampleTumorTemp.area" title="地区"  onblur="checkAdd()" 
						value="<s:property value="sampleTumorTemp.area"/>"
                   	 />
                   	</td>
                   	 </tr>	
                  <tr>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_hospital"
                   	 	name="sampleTumorTemp.hospital" title="送检医院"   
						value="<s:property value="sampleTumorTemp.hospital"/>"
                   	 />
                   	</td>
                  
                   		<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleTumorTemp_sendDate"
                   	 		name="sampleTumorTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleTumorTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleTumorTemp_reportDate"
                   	 		name="sampleTumorTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleTumorTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                   
                   	</tr>
                   	<tr>
                   		<td class="label-title" >送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_doctor"
                   	 	name="sampleTumorTemp.doctor" title="送检医生"   
						value="<s:property value="sampleTumorTemp.doctor"/>"
                   	 />
                   	</td>
                   		<td class="label-title" >检测内容</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_detectionContent"
                   	 		name="sampleTumorTemp.detectionContent" title="检测内容"   
                   	 		value="<s:property value="sampleTumorTemp.detectionContent"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>	
                   		<td class="label-title" >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_sampleNum"
                   	 		name="sampleTumorTemp.sampleNum" title="序号"   
							value="<s:property value="sampleTumorTemp.sampleNum"/>"
                   	 	/>
                    </td>
                   	</tr>
                   	<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                   	<tr>
                	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_patientName"
                   	 		name="sampleTumorTemp.patientName" title="姓名"   
							value="<s:property value="sampleTumorTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_patientNameSpell"
                   	 		name="sampleTumorTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleTumorTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.gender" id="sampleTumorTemp_gender" >
						<option value="" <s:if test="sampleTumorTemp.gender==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.gender==0">selected="selected" </s:if>>女</option>
    					<option value="1" <s:if test="sampleTumorTemp.gender==1">selected="selected" </s:if>>男</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleTumorTemp_age" name="sampleTumorTemp.age"  value="<s:property value="sampleTumorTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" >籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_nativePlace"
                   	 		name="sampleTumorTemp.nativePlace" title="籍贯"   
							value="<s:property value="sampleTumorTemp.nativePlace"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_nationality"
                   	 		name="sampleTumorTemp.nationality" title="民族"   
							value="<s:property value="sampleTumorTemp.nationality"/>"
                   	 	/>
                   	</td>
                </tr>
                   	<tr>
                   		<td class="label-title" >婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.isInsure" id="sampleTumorTemp_isInsure" >
						<option value="" <s:if test="sampleTumorTemp.isInsure==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.isInsure==0">selected="selected" </s:if>>已婚</option>
    					<option value="1" <s:if test="sampleTumorTemp.isInsure==1">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >身高</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_heights"
                   	 		name="sampleTumorTemp.heights" title="身高" 
							value="<s:property value="sampleTumorTemp.heights"/>"
                   		 />
                   	</td>
                   	<td class="label-title" >体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_weight"
                   	 		name="sampleTumorTemp.weight" title="体重" 
							value="<s:property value="sampleTumorTemp.weight"/>"
                   	 	/>
                   	</td>
                   	</tr>
                   	<tr>
                   		<td class="label-title" >血压</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_bloodPressure"
                   	 		name="sampleTumorTemp.bloodPressure" title="血压" 
							value="<s:property value="sampleTumorTemp.bloodPressure"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >心电图</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_electrocardiogram"
                   	 		name="sampleTumorTemp.electrocardiogram" title="心电图" 
							value="<s:property value="sampleTumorTemp.electrocardiogram"/>"
                   	 	/>
                   	</td>
                   	</tr>
                   	<tr>
                   			<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleTumorTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleTumorTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleTumorTemp_voucherType" name="sampleTumorTemp.voucherType.id"  value="<s:property value="sampleTumorTemp.voucherType.id"/>" > 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_voucherCode"
                   	 	name="sampleTumorTemp.voucherCode" title="证件号码"   onblur="checkFun()"
						value="<s:property value="sampleTumorTemp.voucherCode"/>"
                   	 />
                   	</td>
                   		<td class="label-title" >联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleTumorTemp_phone"
	                   		 name="sampleTumorTemp.phone" title="联系方式"  value="<s:property value="sampleTumorTemp.phone"/>"
	                   	 />
                   	</td>
                   	</tr>
                
                   	<tr>
                   	<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_emailAddress"
                  	 		name="sampleTumorTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleTumorTemp.emailAddress"/>" />
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   		<td class="label-title" >通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_address"
                   	 		name="sampleTumorTemp.address" title="通讯地址"   
							value="<s:property value="sampleTumorTemp.address"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人病史</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_diagnosis"
                  	 		name="sampleTumorTemp.diagnosis" title="临床诊断"   
							value="<s:property value="sampleTumorTemp.diagnosis"/>" />
                   	</td>
                   	
                   	<td class="label-title" >用药史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_pharmacy"
                  	 		name="sampleTumorTemp.pharmacy" title="用药史"   
							value="<s:property value="sampleTumorTemp.pharmacy"/>" />
                   	</td>
                   	
                   	<td class="label-title" >病例描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_caseDescribe"
                  	 		name="sampleTumorTemp.caseDescribe" title="病例描述"   
							value="<s:property value="sampleTumorTemp.caseDescribe"/>" />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人医疗信息</label>
						</div>
					</td>
				</tr>
                   	<tr>
                   		<td class="label-title" >个人疾病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_bloodHistory"
                   	 		name="sampleTumorTemp.bloodHistory" title="个人疾病史"   
							value="<s:property value="sampleTumorTemp.bloodHistory"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note1"
                   	 	name="sampleTumorTemp.note1" title="备注"   
						value="<s:property value="sampleTumorTemp.note1"/>"
                   	 />
                   	</td>
                
                   	<td class="label-title" >个人生育史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_parturitionTime"
                   	 	name="sampleTumorTemp.parturitionTime" title="个人生育史"   
						value="<s:property value="sampleTumorTemp.parturitionTime"/>"
                   	 />
                   	</td>
                   	   	</tr>
                   	<tr>	
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note2"
                   	 	name="sampleTumorTemp.note2" title="备注"   
						value="<s:property value="sampleTumorTemp.note2"/>"
                   	 />
                   	</td>
                 
                	<td class="label-title" >烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_cigarette"
                   	 	name="sampleTumorTemp.cigarette" title="烟"   
						value="<s:property value="sampleTumorTemp.cigarette"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_wine"
                   	 	name="sampleTumorTemp.wine" title="酒"   
						value="<s:property value="sampleTumorTemp.wine"/>"
                   	 />
                   	</td>
                   	
                </tr>
                   	<tr>
                   	<td class="label-title" >药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_medicine"
                   	 	name="sampleTumorTemp.medicine" title="药物"   
						value="<s:property value="sampleTumorTemp.medicine"/>"
                   	 />
                   	</td>
                   		<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note3"
                   	 	name="sampleTumorTemp.note3" title="备注"   
						value="<s:property value="sampleTumorTemp.note3"/>"
                   	 />
                   	</td>
                  
                   	<td class="label-title" >放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_radioactiveRays"
                   	 	name="sampleTumorTemp.radioactiveRays" title="放射线"   
						value="<s:property value="sampleTumorTemp.radioactiveRays"/>"
                   	 />
                   	</td>
                   	   	</tr>
                   	<tr>
                   	<td class="label-title" >农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_pesticide"
                   	 	name="sampleTumorTemp.pesticide" title="农药"   
						value="<s:property value="sampleTumorTemp.pesticide"/>"
                   	 />
                   	</td>
                  
                   		<td class="label-title" >铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_plumbane"
                   	 	name="sampleTumorTemp.plumbane" title="铅"   
						value="<s:property value="sampleTumorTemp.plumbane"/>"
                   	 />
                   	</td>
                  
                   	<td class="label-title" >汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_mercury"
                   	 	name="sampleTumorTemp.mercury" title="汞"   
						value="<s:property value="sampleTumorTemp.mercury"/>"
                   	 />
                   	</td>
                   	   	</tr>
                   	<tr>
                   	<td class="label-title" >镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_cadmium"
                   	 	name="sampleTumorTemp.cadmium" title="镉"   
						value="<s:property value="sampleTumorTemp.cadmium"/>"
                   	 />
                   	</td>

                   		<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note4"
                   	 	name="sampleTumorTemp.note4" title="备注"   
						value="<s:property value="sampleTumorTemp.note4"/>"
                   	 />
                   	</td>
                   
                   		<td class="label-title" >外源输血史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.medicalHistory" id="sampleTumorTemp_medicalHistory" >
						<option value="" <s:if test="sampleTumorTemp.medicalHistory==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.medicalHistory==0">selected="selected" </s:if>>有</option>
    					<option value="1" <s:if test="sampleTumorTemp.medicalHistory==1">selected="selected" </s:if>>无</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		</tr>
                   	<tr>
                   		<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note5"
                   	 	name="sampleTumorTemp.note5" title="备注"   
						value="<s:property value="sampleTumorTemp.note5"/>"
                   	 />
                   	</td>
                   	
                   		<td class="label-title" >近期是否做过检查</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.gestationIVF" id="sampleTumorTemp_gestationIVF" >
						<option value="" <s:if test="sampleTumorTemp.gestationIVF==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.gestationIVF==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.gestationIVF==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note6"
                   	 	name="sampleTumorTemp.note6" title="备注"   
						value="<s:property value="sampleTumorTemp.note6"/>"
                   	 />
                   	</td>
                   	</tr>
                   	<tr>
                   		<td class="label-title" >是否有过手术或治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.isSurgery" id="sampleTumorTemp_isSurgery" >
						<option value="" <s:if test="sampleTumorTemp.isSurgery==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.isSurgery==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.isSurgery==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		<td class="label-title" >其他</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_other"
                   	 	name="sampleTumorTemp.other" title="其他"   
						value="<s:property value="sampleTumorTemp.other"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >是否用过药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.isPharmacy" id="sampleTumorTemp_isPharmacy" >
						<option value="" <s:if test="sampleTumorTemp.isPharmacy==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.isPharmacy==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.isPharmacy==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	</tr>
                   	<tr>
                   		<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note7"
                   	 	name="sampleTumorTemp.note7" title="备注"   
						value="<s:property value="sampleTumorTemp.note7"/>"
                   	 />
                   	</td>
                   	</tr>
                   		<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人健康状况及家族信息</label>
						</div>
					</td>
				</tr>
                   	<tr>
                   		<td class="label-title" >是否存在下列疾病</td>
               	 		<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   		<td align="left"  >
                   			<select name="sampleTumorTemp.isAilment" id="sampleTumorTemp_isAilment" >
								<option value="" <s:if test="sampleTumorTemp.isAilment==''">selected="selected" </s:if>>请选择</option>
    							<option value="0" <s:if test="sampleTumorTemp.isAilment==0">selected="selected" </s:if>>是</option>
    							<option value="1" <s:if test="sampleTumorTemp.isAilment==1">selected="selected" </s:if>>否</option>
							</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note8"
                   	 		name="sampleTumorTemp.note8" title="备注"   
							value="<s:property value="sampleTumorTemp.note8"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >直系亲属中是否患有肿瘤</td>
               	 		<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   		<td align="left"  >
                   			<select name="sampleTumorTemp.isSickenTumour" id="sampleTumor_isSickenTumour" >
								<option value="" <s:if test="sampleTumorTemp.isSickenTumour==''">selected="selected" </s:if>>请选择</option>
    							<option value="0" <s:if test="sampleTumorTemp.isSickenTumour==0">selected="selected" </s:if>>是</option>
    							<option value="1" <s:if test="sampleTumorTemp.isSickenTumour==1">selected="selected" </s:if>>否</option>
							</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	</tr>
                   	<tr>
                   		<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note9"
                   	 		name="sampleTumorTemp.note9" title="备注"   
							value="<s:property value="sampleTumorTemp.note9"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >与受检者关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_relationship"
                   	 		name="sampleTumorTemp.relationship" title="与受检者关系"   
							value="<s:property value="sampleTumorTemp.relationship"/>"
                   	 	/>
                   	</td>
                   	</tr>
                   	 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>费用信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.isFee" id="sampleTumorTemp_isFee" >
						<option value="" <s:if test="sampleTumorTemp.isFee==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.isFee==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.isFeee==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.privilegeType" id="sampleTumorTemp_privilegeType" >
						<option value="" <s:if test="sampleTumorTemp.privilegeType==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.privilegeType==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.privilegeType==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		<td class="label-title" >推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_linkman"
                   	 	name="sampleTumorTemp.linkman" title="推荐人"   
						value="<s:property value="sampleTumorTemp.linkman"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.isInvoice" id="sampleTumorTemp_isInvoice" >
						<option value="" <s:if test="sampleTumorTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.isInvoice==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_createUser" name="sampleTumorTemp.createUser" value="<s:property value="sampleTumorTemp.createUser"/>" title="录入人"  />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note10"
                   	 	name="sampleTumorTemp.note10" title="备注"   
						value="<s:property value="sampleTumorTemp.note10"/>"
                   	 />
                   	</td>
              	</tr>
                <tr>
                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_money"
                   	 		name="sampleTumorTemp.money" title="金额"   
							value="<s:property value="sampleTumorTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_gestationalAge"
                   	 	name="sampleTumorTemp.gestationalAge" title="SP"   
						value="<s:property value="sampleTumorTemp.gestationalAge"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_paymentUnit"
                   	 	name="sampleTumorTemp.paymentUnit" title="开票单位"   
						value="<s:property value="sampleTumorTemp.paymentUnit"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                
                 <g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="UserFun" 
 					hasSetFun="true"
					documentId="sampleTumorTemp_reportMan"
					documentName="sampleTumorTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleTumorTemp_reportMan_name"  value="<s:property value="sampleTumorTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleTumorTemp_reportMan" name="sampleTumorTemp.reportMan.id"  value="<s:property value="sampleTumorTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="UserFun" 
 					hasSetFun="true"
					documentId="sampleTumorTemp_auditMan"
					documentName="sampleTumorTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleTumorTemp_auditMan_name"  value="<s:property value="sampleTumorTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleTumorTemp_auditMan" name="sampleTumorTemp.auditMan.id"  value="<s:property value="sampleTumorTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                  	
                  	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleTumorTemp.nextStepFlow" id="sampleTumorTemp_nextStepFlow">
                   			<option value="" <s:if test="sampleTumorTemp.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleTumorTemp.nextStepFlow==0">selected="selected" </s:if>>合格</option>
							<option value="1" <s:if test="sampleTumorTemp.nextStepFlow==1">selected="selected" </s:if>>退费</option>
							<option value="2" <s:if test="sampleTumorTemp.nextStepFlow==2">selected="selected" </s:if>>重抽血</option>
							<option value="3" <s:if test="sampleTumorTemp.nextStepFlow==3">selected="selected" </s:if>>暂停</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td> 
                	
                </tr>
               

            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleTumor.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
