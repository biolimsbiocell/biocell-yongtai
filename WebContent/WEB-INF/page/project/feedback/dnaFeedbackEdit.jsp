﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=dnaFeedback&id=${dnaFeedback.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/dnaFeedbackEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >dna编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_id"
                   	 name="dnaFeedback.id" title="dna编号"
                   	   
	value="<s:property value="dnaFeedback.id"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="dnaFeedback_name"
                   	 name="dnaFeedback.name" title="描述"
                   	   
	value="<s:property value="dnaFeedback.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="dnaFeedback_sampleCode"
                   	 name="dnaFeedback.sampleCode" title="样本编号"
                   	   
	value="<s:property value="dnaFeedback.sampleCode"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >体积</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_bluk"
                   	 name="dnaFeedback.bluk" title="体积"
                   	   
	value="<s:property value="dnaFeedback.bluk"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >od260/230</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="dnaFeedback_od230"
                   	 name="dnaFeedback.od230" title="od260/230"
                   	   
	value="<s:property value="dnaFeedback.od230"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >od260/280</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="dnaFeedback_od280"
                   	 name="dnaFeedback.od280" title="od260/280"
                   	   
	value="<s:property value="dnaFeedback.od280"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >浓度</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_concentrer"
                   	 name="dnaFeedback.concentrer" title="浓度"
                   	   
	value="<s:property value="dnaFeedback.concentrer"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >是否合格</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_isgood"
                   	 name="dnaFeedback.isgood" title="是否合格"
                   	   
	value="<s:property value="dnaFeedback.isgood"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_nextflow"
                   	 name="dnaFeedback.nextflow" title="下一步流向"
                   	   
	value="<s:property value="dnaFeedback.nextflow"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >处理意见</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_advice"
                   	 name="dnaFeedback.advice" title="处理意见"
                   	   
	value="<s:property value="dnaFeedback.advice"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >状态id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_state"
                   	 name="dnaFeedback.state" title="状态id"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="dnaFeedback.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="dnaFeedback_stateName"
                   	 name="dnaFeedback.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="dnaFeedback.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="dnaFeedback.id"/>" />
            </form>
            <div id="tabs">
            <ul>
           	</ul> 
			</div>
        	</div>
	</body>
	</html>
