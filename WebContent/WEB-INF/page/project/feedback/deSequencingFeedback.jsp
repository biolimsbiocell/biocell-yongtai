﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/project/feedback/deSequencingFeedback.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_id"
                   	 name="id" searchField="true" title="编号"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="deSequencingFeedback_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="deSequencingFeedback_sampleCode"
                   	 name="sampleCode" searchField="true" title="样本编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			
<%-- 			<g:LayOutWinTag buttonId="showcode" title="选择pooling编号" --%>
<%-- 				hasHtmlFrame="true" --%>
<%-- 				html="${ctx}/experiment/pooling/deSequencingTaskAbnormal/stringSelect.action" --%>
<%-- 				isHasSubmit="false" functionName="StringFun"  --%>
<%--  				hasSetFun="true" --%>
<%-- 				extRec="rec" --%>
<%-- 				extStr="document.getElementById('deSequencingTaskAbnormal_code').value=rec.get('id'); --%>
<%-- 				document.getElementById('deSequencingTaskAbnormal_code_name').value=rec.get('name');" /> --%>
               	 	<td class="label-title" >pooling编号</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="deSequencingFeedback_code_name" searchField="true"  name="code.name"  value="" class="text input" />
 						<input type="hidden" id="deSequencingFeedback_code" name="deSequencingFeedback.code.id"  value="" > 
 						<img alt='选择pooling编号' id='showcode' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	
     </tr>
     <tr>
               	 	<td class="label-title" >文库号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingFeedback_wkNum"
                   	 name="deSequencingFeedback.wkNum" title="文库号"
                   	   
	value="<s:property value="deSequencingFeedback.wkNum"/>"
                   	  />
                   	</td>
			
			 	<td class="label-title" >测试评估</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingFeedback_estimate"
                   	 name="deSequencingFeedback.estimate" title="错误率"
                   	   
	value="<s:property value="deSequencingFeedback.estimate"/>"
                   	  />
                   	</td>
                   	
                   	  	<td class="label-title">反馈时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	 <input type="text" size="20" maxlength="25"
						id="deSequencingFeedback_backDate" name="deSequencingFeedback.backDate"
						title="反馈时间" Class="Wdate" readonly="readonly"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
						value="<s:date name="deSequencingFeedback.backDate" format="yyyy-MM-dd"/>" />
                   	</td>
			</tr>
     
     		<tr>
     			<td class="label-title" >错误率</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="deSequencingFeedback_errorRate"
                   	 name="deSequencingFeedback.errorRate" title="错误率"
                   	   
	value="<s:property value="deSequencingFeedback.errorRate"/>"
                   	  />
                   	  
                   	</td>
               	 	<td class="label-title" >index</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="deSequencingFeedback_indexs"
                   	 name="indexs" searchField="true" title="index"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >浓度</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_concentrer"
                   	 name="concentrer" searchField="true" title="浓度"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			
			<tr>
               	 	<td class="label-title" >指控类型</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_qctype"
                   	 name="qctype" searchField="true" title="指控类型"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >是否合格</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_isgood"
                   	 name="isgood" searchField="true" title="是否合格"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >下一步流向</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_nextflow"
                   	 name="nextflow" searchField="true" title="下一步流向"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >处理意见</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_advice"
                   	 name="advice" searchField="true" title="处理意见"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >状态id</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_state"
                   	 name="state" searchField="true" title="状态id"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="deSequencingFeedback_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_deSequencingFeedback_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_deSequencingFeedback_tree_page"></div>
		<select id="isgood" style="display: none">
			<option value="0">合格</option>
			<option value="1">不合格</option>
		</select>
</body>
</html>



