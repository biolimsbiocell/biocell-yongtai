
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'> 
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInputTemp&id=${sampleInputTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/js/project/feedback/sampleInputTempEditFour.js"></script>
<%-- 	<s:if test="sampleInputTemp.upLoadAccessory.id != ''">
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:640px;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
	</s:if>
	<s:if test="sampleInputTemp.upLoadAccessory.id ==''"> 
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:640px;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src=""></div>
	</s:if> --%>
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputTempItemImg"><img onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}"> 
		<input type="hidden" id="id" value="${requestScope.id}"> 
		<input type="hidden" id="path" value="${requestScope.path}"> 
		<input type="hidden" id="fname" value="${requestScope.fname}">
		<input type="hidden"  id="str" value="${requestScope.str}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
<!-- 				<td class="label-title" style="color: red">点击上传图片</td> -->
				<td></td>
				<td>
<!--   					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" /> -->
					<input type="hidden"  id="upload_imga_id" name="sampleInputTemp.upLoadAccessory.id" value="<s:property value="sampleInputTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleInputTemp.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.upLoadAccessory.fileName"/>">
				</td>
			</tr>
				<tr>
					<!--                	 	<td class="label-title" style="display: none">编号</td> -->
					<!-- 					<td class="requiredcolunm" nowrap width="10px" style="display: none"></td> --%>
					<!-- 					<td align="left" style="display: none"><input type="text" size="20" maxlength="25" -->
					<!-- 						id="sampleInputTemp_id" -->
					<!-- 						class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.id" title="编号" -->
					<%-- 						value="<s:property value="sampleInputTemp.id"/>" /> --%>
					<!-- 					</td> -->

					<td class="label-title">样本编号</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_code" name="sampleInputTemp.sampleInfo.code" title="样本编号" class="text input readonlytrue" readonly="readonly" value="<s:property value="sampleInputTemp.sampleInfo.code"/>" />
						<input type="hidden" id="sampleInputTemp_id" name="sampleInputTemp.sampleInfo.id" value="<s:property value="sampleInputTemp.sampleInfo.id"/>" />
						<input type="hidden"  id="upload_imga" name="sampleInputTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleInputTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleInputTemp.sampleInfo.upLoadAccessory.id"/>">
					</td>
						
					<td class="label-title">描述</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_name" name="sampleInputTemp.name" title="描述" value="<s:property value="sampleInputTemp.name"/>" />
					</td>
					
					<td class="label-title">检测项目</td>
					<td class="requiredcolumn" nowrap width="10px" ></td>	
					<td align="left"><input type="hidden" size="20" id="sampleInputTemp_productId" searchField="true" name="sampleInputTemp.productId" value="<s:property value="sampleInputTemp.productId"/>" class="text input" /> 
						<input type="text" id="sampleInputTemp_productName" name="sampleInputTemp.productName" value="<s:property value="sampleInputTemp.productName"/>">
						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />
					</td>
				</tr>
				<tr>
				
					<td class="label-title">姓名</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left"><input type="text" size="20" maxlength="25" id="sampleInputTemp_patientName" name="sampleInputTemp.patientName" title="姓名" value="<s:property value="sampleInputTemp.patientName"/>" />
					</td>
					
					<td class="label-title">住院/门诊号</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_inHosNum" name="sampleInputTemp.inHosNum" title="住院/门诊号" value="<s:property value="sampleInputTemp.inHosNum"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">年龄</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" id="sampleInputTemp_age" name="sampleInputTemp.age" value="<s:property value="sampleInputTemp.age"/>" />
					</td>

					<td class="label-title">末次月经</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_endMenstruationDate" name="sampleInputTemp.endMenstruationDate" title="末次月经" Class="Wdate" readonly="readonly"
							onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="sampleInputTemp.endMenstruationDate" format="yyyy-MM-dd"/>" />
					</td>

					<td class="label-title">样本类型</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" readonly="readOnly" id="sampleInputTemp_sampleType_name" name="sampleInputTemp.sampleType.name" value="<s:property value="sampleInputTemp.sampleType.name"/>" /> <s:hidden id="sampleInputTemp_sampleType_id" name="sampleInputTemp.sampleType.id"></s:hidden> <span
							id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
					</td>

				</tr>
				<tr>
					
					<td class="label-title">临床诊断</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_diagnosis" name="sampleInputTemp.diagnosis" title="临床诊断" value="<s:property value="sampleInputTemp.diagnosis"/>" />
					</td>

					<td class="label-title">双胎 / 多胎妊娠</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<select name="sampleInputTemp.gestationIVF" id="sampleInputTemp_gestationIVF">
							<option value="0" <s:if test="sampleInputTemp.gestationIVF==0">selected="selected" </s:if>>否</option>
							<option value="1" <s:if test="sampleInputTemp.gestationIVF==1">selected="selected" </s:if>>是</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
				    </td>
				    
					<td class="label-title">样本状态</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<select name="sampleInputTemp.isInsure" id="sampleInputTemp_isInsure">
							<option value="1" <s:if test="sampleInputTemp.isInsure==1">selected="selected" </s:if>>正常</option>
							<option value="0" <s:if test="sampleInputTemp.isInsure==0">selected="selected" </s:if>>不正常</option>
						</select> 
						<img class='requiredimage' src='${ctx}/images/required.gif' />
					</td>

				</tr>
				<tr>
					<td class="label-title">送检科室</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_hospital" name="sampleInputTemp.hospital" title="送检科室" value="<s:property value="sampleInputTemp.hospital"/>" />
					</td>
					
					<td class="label-title">送检医生</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left"> 
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_doctor" name="sampleInputTemp.doctor" title="送检医生" value="<s:property value="sampleInputTemp.doctor"/>" />
					</td>
					
					<td class="label-title">采样日期</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_sendDate" name="sampleInputTemp.sendDate" title="采样日期" Class="Wdate" readonly="readonly"
							onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="sampleInputTemp.sendDate" format="yyyy-MM-dd"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">备注</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_note" name="sampleInputTemp.note" title="备注" value="<s:property value="sampleInputTemp.note"/>" />
					</td>
				</tr>
				<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>检测信息</label>
						</div>
					</td>
				</tr>
				<tr>
				<tr>
					<td class="label-title">21-三体比值</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome21Value" name="sampleInputTemp.trisome21Value"  title="21-三体比值" value="<s:property value="sampleInputTemp.trisome21Value"/>" />
					</td>
					
					<td class="label-title">21-三体（参考范围[-3,3]）</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25"  id="sampleInputTemp_reference21Range" name="sampleInputTemp.reference21Range"  title="21-三体（参考范围）" value="<s:property value="sampleInputTemp.reference21Range"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">18-三体比值</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome18Value" name="sampleInputTemp.trisome18Value" title="18-三体比值"  value="<s:property value="sampleInputTemp.trisome18Value"/>" />
					</td>
					
					<td class="label-title">18-三体（参考范围[-3,3]）</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_reference18Range" name="sampleInputTemp.reference18Range" title="18-三体（参考范围）" value="<s:property value="sampleInputTemp.reference18Range"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">13-三体比值</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome13Value" name="sampleInputTemp.trisome13Value" title="13-三体比值" value="<s:property value="sampleInputTemp.trisome13Value"/>" />
					</td>
					
					<td class="label-title">13-三体（参考范围[-3,3]）</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_reference13Range" name="sampleInputTemp.reference13Range" title="13-三体（参考范围）"  value="<s:property value="sampleInputTemp.reference13Range"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">结果描述</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_reason" name="sampleInputTemp.reason" title="结果描述" value="<s:property value="sampleInputTemp.reason"/>" />
					</td>
	
					<td class="label-title">其他提示</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_medicalHistory" name="sampleInputTemp.medicalHistory" title="其他提示" value="<s:property value="sampleInputTemp.medicalHistory"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">建议与解释</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="500" id="sampleInputTemp_badMotherhood" name="sampleInputTemp.badMotherhood" title="建议与见识" value="<s:property value="sampleInputTemp.badMotherhood"/>" />
					</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showdetectionMan" title="选择检测者"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="detectionUserFun" hasSetFun="true"
						documentId="sampleInputTemp_detectionMan_id"
						documentName="sampleInputTemp_detectionMan_name" />

					<td class="label-title">检测者</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="15" readonly="readOnly" id="sampleInputTemp_detectionMan_name" name="sampleInputTemp.detectionMan.name" value="<s:property value="sampleInputTemp.detectionMan.name"/>" />
						<input type="hidden" id="sampleInputTemp_detectionMan_id" name="sampleInputTemp.detectionMan.id" value="<s:property value="sampleInputTemp.detectionMan.id"/>">
						<img alt='选择检测者' id='showdetectionMan' src='${ctx}/images/img_lookup.gif' class='detail' /></td>

					<g:LayOutWinTag buttonId="showreportMan" title="选择报告者"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="reportUserFun" hasSetFun="true"
						documentId="sampleInputTemp_reportMan_id"
						documentName="sampleInputTemp_reportMan_name" />

					<td class="label-title">报告者</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="15" readonly="readOnly" id="sampleInputTemp_reportMan_name" name="sampleInputTemp.reportMan.name" value="<s:property value="sampleInputTemp.reportMan.name"/>" />
						<input type="hidden" id="sampleInputTemp_reportMan_id" name="sampleInputTemp.reportMan.id"  value="<s:property value="sampleInputTemp.reportMan.id"/>">
						<img alt='选择报告者' id='showreportMan' src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>


					<g:LayOutWinTag buttonId="showauditMan" title="选择审核者"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="auditUserFun" hasSetFun="true"
						documentId="sampleInputTemp_auditMan_id"
						documentName="sampleInputTemp_auditMan_name" />

					<td class="label-title">审核者</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left">
						<input type="text" size="15" readonly="readOnly" id="sampleInputTemp_auditMan_name" name="sampleInputTemp.auditMan.name" value="<s:property value="sampleInputTemp.auditMan.name"/>" />
						<input type="hidden" id="sampleInputTemp_auditMan_id" name="sampleInputTemp.auditMan.id" value="<s:property value="sampleInputTemp.auditMan.id"/>">
						<img alt='选择审核者' id='showauditMan' src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>

				</tr>
				<tr>
					<td class="label-title">报告日期</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_reportDate" name="sampleInputTemp.reportDate" title="报告日期" Class="Wdate" readonly="readonly"
							onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="sampleInputTemp.reportDate" format="yyyy-MM-dd"/>" />
					</td>
					
					<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.nextStepFlow" id="sampleInputTemp_nextStepFlow">
	    					<option value="" <s:if test="sampleInputTemp.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleInputTemp.nextStepFlow==0">selected="selected" </s:if>>合格</option>
							<option value="1" <s:if test="sampleInputTemp.nextStepFlow==1">selected="selected" </s:if>>退费</option>
							<option value="2" <s:if test="sampleInputTemp.nextStepFlow==2">selected="selected" </s:if>>重抽血</option>
							<option value="3" <s:if test="sampleInputTemp.nextStepFlow==3">selected="selected" </s:if>>暂停</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
			</table>
			<input type="hidden" id="id_parent_hidden"
				value="<s:property value="sampleInputTemp.id"/>" />
		</form>
		<!-- <div id="tabs">
            <ul>
           	</ul>  -->
	</div>
</body>
</html>
