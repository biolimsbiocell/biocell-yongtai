﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%-- <%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%> --%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/project/feedback/techCheckWkFeedBack.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="输入查询条件">
			样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="techCheckWkFeedBack_sampleCode" searchType ="1" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			文库编号
			</label>
			</td>
			<td>
			<input type="text"  name="wkCode" id="techCheckWkFeedBack_wkCode" searchType ="1" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			处理意见
			</label>
			</td>
			<td>
			<select id="techCheckWkFeedBack_method" name="method" searchType ="1" searchField="true" style="width:100">
                		<option value=" ">请选择</option>
    					<option value="0">文库质控</option>
				</select>
			</td>
			<td>
			<input type="button" onClick="selectTechCheckWkFeedBackInfo()" value="查询">
			</td>
			</tr>
		</table>
	<div id="techCheckWkFeedBackdiv"></div>
</body>
</html>



