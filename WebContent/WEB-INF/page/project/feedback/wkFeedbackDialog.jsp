﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/wkFeedbackDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">文库编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="wkFeedback_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="wkFeedback_sampleCode" searchField="true" name="sampleCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">体积</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_bluk" searchField="true" name="bluk"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">index</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="wkFeedback_indexs" searchField="true" name="indexs"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">浓度</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_concentrer" searchField="true" name="concentrer"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">指控类型</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_qctype" searchField="true" name="qctype"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">是否合格</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_isgood" searchField="true" name="isgood"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">下一步流向</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_nextflow" searchField="true" name="nextflow"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">处理意见</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_advice" searchField="true" name="advice"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态id</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkFeedback_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_wkFeedback_div"></div>
   		
</body>
</html>



