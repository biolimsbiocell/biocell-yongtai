﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInfo&id=${sampleInputTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/js/project/feedback/sampleInputTempEdit.js"></script>
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputTempItemImg"><img onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}"> 
		<input type="hidden" id="id" value="${requestScope.id}">
		<input type="hidden" id="path" value="${requestScope.path}"> 
		<input type="hidden" id="fname" value="${requestScope.fname}">
		<input type="hidden" id="saveType" value="cq">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left" style="display:none">
<!--                    	<input type="text" size="20" maxlength="25" -->
<!-- 						id="sampleInfo_id" name="sampleInfo.id" -->
<%-- 						title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/> --%>
						<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_id" name="sampleInputTemp.id"
						title="编号" value="<s:property value="sampleInputTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleInputTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   	</td>
                   	
					<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
<%-- 						<input type="text" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleInputTemp.sampleInfo.code"/>" /> --%>
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.code" title="样本编号" value="<s:property value="sampleInputTemp.code"/>" />
                   	</td>
				
					<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_name" name="sampleInputTemp.name" 
						title="描述" value="<s:property value="sampleInputTemp.name"/>" 
						class="sampleInputTemp_name"/>
                   	</td>
					
					<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="hidden" size="20"   id="sampleInputTemp_productId" searchField="true" name="sampleInputTemp.productId"  value="<s:property value="sampleInputTemp.productId"/>" class="text input" />
 						<input type="text" id="sampleInputTemp_productName" name="sampleInputTemp.productName" value="<s:property value="sampleInputTemp.productName"/>" readonly="readonly"  > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                    
<!--                    	<td class="label-title">编号</td> -->
<%--                      	<td class="requiredcolunm" nowrap width="10px" > </td> --%>
<!--                    	<td align="left"> -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleInput_id"  -->
<%--                    			name="sampleInput.id" title="编号" value="<s:property value="sampleInput.id"/>" --%>
<!--                    		/> -->
<!--                    	</td> -->
         			<tr>
         	
                   	<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
						<input type="text" size="20" readonly="readOnly" name="sampleInputTemp.sampleType.name"
						id="sampleInputTemp_sampleType_name" 
						value="<s:property value="sampleInputTemp.sampleType.name"/>" />
						<s:hidden id="sampleInputTemp_sampleType_id"
							name="sampleInputTemp.sampleType.id"></s:hidden>
						<span id="regionType" onClick="sampleKind()"
						class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>
                   	
                   	
	
              		<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"  
						id="sampleInputTemp_hospital" name="sampleInputTemp.hospital"
						title="送检医院" value="<s:property value="sampleInputTemp.hospital"/>" />
                   	</td>
                   	
                   	
                   	<td class="label-title">送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_doctor" name="sampleInputTemp.doctor"
						title="送检医生" value="<s:property value="sampleInputTemp.doctor"/>" />
                   	</td>
                   	
         		</tr>
         		<tr>
         		
         			<td class="label-title">审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="hidden" size="20" maxlength="25"
						id="sampleInputTemp_createUser_id"
						name="sampleInputTemp.createUser.id" title="审核人" 
						value="<s:property value="inputTempNew.createUser.id"/>"  readonly="readonly" class="text input readonlytrue"
						/>
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_createUser_name"
						class="text input readonlytrue" readonly="readonly"
						name="sampleInputTemp.createUser.name" title="审核人" 
						value="<s:property value="sampleInputTemp.createUser.name"/>" />
					<input type="hidden" size="20" maxlength="25" 
						id="sampleInputTemp_createUser1"
						name="sampleInputTemp.createUser1" title="审核人1" 
						value="<s:property value="sampleInputTemp.createUser1"/>" />
                   	</td>

<!--               		<td class="label-title">接收日期</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	 <input type="text" size="20" maxlength="25"  -->
<!-- 						id="sampleInputTemp_acceptDate" name="sampleInputTemp.acceptDate" -->
<!-- 						title="接收日期" Class="Wdate" readonly="readonly" -->
<!-- 						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" -->
<%-- 						value="<s:date name="sampleInputTemp.acceptDate" format="yyyy-MM-dd" />" /> --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">取样时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	 <input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_sendDate" name="sampleInputTemp.sendDate"
						title="取样时间" Class="Wdate" readonly="readonly"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
						value="<s:date name="sampleInputTemp.sendDate" format="yyyy-MM-dd"/>" />
                   	</td>
                </tr>
                <tr>
                
                </tr>
              	<tr>
              	
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
					
				</tr>
               	<tr>
               		<td class="label-title">孕妇姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_patientName"
						name="sampleInputTemp.patientName" title="孕妇姓名" 
						value="<s:property value="sampleInputTemp.patientName"/>" />
                   	</td>
                   	
                   	
<!--                    	<td class="label-title">姓名拼音</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	<input type="text" size="20" maxlength="25"  -->
<!-- 						id="sampleInputTemp_patientNameSpell" -->
<!-- 						name="sampleInputTemp.patientNameSpell" title="姓名拼音" -->
<%-- 						value="<s:property value="sampleInputTemp.patientNameSpell"/>" /> --%>
<!--                    	</td> -->
               		
               		
               	 	<td class="label-title">性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.gender" 
						id="sampleInputTemp_gender">
    					<option value="0" <s:if test="sampleInputTemp.gender==0">selected="selected" </s:if>>女</option>
    					<option value="1" <s:if test="sampleInputTemp.gender==1">selected="selected" </s:if>>男</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
				</tr>
				<tr>
				
               	 	<td class="label-title">年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" id="sampleInputTemp_age"
						name="sampleInputTemp.age" 
						value="<s:property value="sampleInputTemp.age"/>" />
                   	</td>
                   	
                   	
                   	<td class="label-title">出生日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	  	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_birthday" name="sampleInputTemp.birthday"
						title="出生日期" Class="Wdate" readonly="readonly"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
						value="<s:date name="sampleInputTemp.birthday" format="yyyy-MM-dd"/>" />
                   	</td>
                   	
                   	
                   	<td class="label-title">体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_weight" name="sampleInputTemp.weight"
						title="体重" value="<s:property value="sampleInputTemp.weight"/>" />
                   	</td>
				
			</tr>
			<tr>
			
					<td class="label-title">孕周</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_gestationalAge"
						name="sampleInputTemp.gestationalAge" title="孕周"
						value="<s:property value="sampleInputTemp.gestationalAge"/>" />
                   	</td>

                   	
					<td class="label-title">证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"
						id="sampleInputTemp_voucherType_name" name="sampleInputTemp.voucherType.name"
						value="<s:property value="sampleInputTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleInputTemp_voucherType"
						name="sampleInputTemp.voucherType.id"
						value="<s:property value="sampleInputTemp.voucherType.id"/>"> 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif'
						onClick="voucherTypeFun()" class='detail' />                   		

                   	</td>
                     
                   	
                   	<td class="label-title">证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_voucherCode" 
						name="sampleInputTemp.voucherCode" title="证件号码"
						onblur="checkFun()"
						value="<s:property value="sampleInputTemp.voucherCode"/>" />
                   	</td>
            	
			</tr>
			<tr>
			
					<td class="label-title">手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_phoneNum" name="sampleInputTemp.phoneNum"
						title="手机号码" onblur="checkPhone()"
						value="<s:property value="sampleInputTemp.phoneNum"/>" />
                   	</td>
                   	
                   	<td class="label-title">家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	 <input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_address" name="sampleInputTemp.address"
						title="家庭住址" onblur="checkAddress()"
						value="<s:property value="sampleInputTemp.address"/>" />
                   	 <img class='requiredimage'
						src='${ctx}/images/required.gif' />

                   	</td>
                   	
                   	
                   	<td class="label-title">病床号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_inHosNum" name="sampleInputTemp.inHosNum"
						title="病床号"
						value="<s:property value="sampleInputTemp.inHosNum"/>" />
                   	</td>
                   	
                   	
			</tr>
			<tr>
				

                   	<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_area" name="sampleInputTemp.area" title="地区"
						value="<s:property value="sampleInputTemp.area"/>" />

                   	</td>
                   	


               	 	<td class="label-title">推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>  	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"
						id="sampleInputTemp_linkman_name" 
						value="<s:property value="sampleInputTemp.linkman.name"/>"
						readonly="readOnly" />
 						<input type="hidden" id="sampleInputTemp_linkman"
						name="sampleInputTemp.linkman.id"
						value="<s:property value="sampleInputTemp.linkman.id"/>"> 
 						<img alt='选择联系人' id='showlinkman'
						src='${ctx}/images/img_lookup.gif' class='detail' />                   		

                   	</td>
                   	
                   	<td class="label-title">联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
	                   	<input type="text" size="20" maxlength="10" 
						id="sampleInputTemp_phone" name="sampleInputTemp.phone"
						title="联系方式" value="<s:property value="sampleInputTemp.phone"/>" />

                   	</td>
			</tr>
			<tr>
			
				<td colspan="9">
					<div class="standard-section-header type-title">
						<label>检查情况</label>
					</div>
				</td>
				
			</tr>
			<tr>
                   	<td class="label-title">末次月经</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	 <input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_endMenstruationDate"
						name="sampleInputTemp.endMenstruationDate" title="末次月经"
						Class="Wdate" readonly="readonly"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
						value="<s:date name="sampleInputTemp.endMenstruationDate" format="yyyy-MM-dd"/>" />
                   	</td>
                   	
                   	
                   	<td class="label-title">孕几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_pregnancyTime"
						name="sampleInputTemp.pregnancyTime" title="孕几次"
						value="<s:property value="sampleInputTemp.pregnancyTime"/>" />
                   	</td>
                   	
                   	
                   	<td class="label-title">产几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_parturitionTime"
						name="sampleInputTemp.parturitionTime" title="产几次"
						value="<s:property value="sampleInputTemp.parturitionTime"/>" />
                   	</td>
			</tr>
			
			<tr>
					<td class="label-title">IVF妊娠</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.gestationIVF" id="sampleInputTemp_gestationIVF">
	    					<option value="0"<s:if test="sampleInputTemp.gestationIVF==0">selected="selected" </s:if>>否</option>
	    					<option value="1"<s:if test="sampleInputTemp.gestationIVF==1">selected="selected" </s:if>>是</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                   	<td class="label-title">不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		</td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_badMotherhood" name="sampleInputTemp.badMotherhood" title="不良孕产史" value="<s:property value="sampleInputTemp.badMotherhood"/>" />
                   	</td>
                   	
                   	
                   	
                   	<td class="label-title">简要病史（家族史）</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_medicalHistory"
						name="sampleInputTemp.medicalHistory" title="简要病史（家族史）"
						value="<s:property value="sampleInputTemp.medicalHistory"/>" />
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title">器官移植</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.organGrafting" id="sampleInputTemp_organGrafting">
	    					<option value="0" <s:if test="sampleInputTemp.organGrafting==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.organGrafting==1">selected="selected" </s:if>>有</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                   	<td class="label-title">外源输血</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.outTransfusion" id="sampleInputTemp_outTransfusion" onchange="change1()">
	    					<option value="0"<s:if test="sampleInputTemp.outTransfusion==0">selected="selected" </s:if>>无</option>
	    					<option value="1"<s:if test="sampleInputTemp.outTransfusion==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">最后一次外源输血时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left" id="last1" >
                   	 <input type="text" size="20" maxlength="25"
						id="sampleInputTemp_firstTransfusionDate"
						name="sampleInputTemp.firstTransfusionDate" title="最后一次外源输血时间"
						Class="Wdate" readonly="readonly"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
						value="<s:date name="sampleInputTemp.firstTransfusionDate" format="yyyy-MM-dd"/>" />
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title">干细胞治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.stemCellsCure" id="sampleInputTemp_stemCellsCure">
	    					<option value="0"<s:if test="sampleInputTemp.stemCellsCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1"<s:if test="sampleInputTemp.stemCellsCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">免疫治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.immuneCure" id="sampleInputTemp_immuneCure" onchange="change()">
	    					<option value="0" <s:if test="sampleInputTemp.immuneCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.immuneCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">最后一次免疫治疗时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left" id="last" >
                   	 <input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_endImmuneCureDate"
						name="sampleInputTemp.endImmuneCureDate" title="最后一次免疫治疗时间"
						Class="Wdate" readonly="readonly"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
						value="<s:date name="sampleInputTemp.endImmuneCureDate" format="yyyy-MM-dd"/>" />
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title">单/双/多胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.embryoType"
						id="sampleInputTemp_embryoType">
    					<option value="0"<s:if test="sampleInputTemp.embryoType==0">selected="selected" </s:if>>单胎</option>
    					<option value="1"<s:if test="sampleInputTemp.embryoType==1">selected="selected" </s:if>>双胎</option>
    					<option value="2"<s:if test="sampleInputTemp.embryoType==2">selected="selected" </s:if>>多胎</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                   	<td class="label-title">NT值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		</td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_NT" name="sampleInputTemp.NT" title="NT值" value="<s:property value="sampleInputTemp.NT"/>" />
                   	</td>
                   	
                   	<td class="label-title">异常结果描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_reason" name="sampleInputTemp.reason" title="NT值" value="<s:property value="sampleInputTemp.reason"/>" />
                   	</td>
                   	
			</tr>
			<tr>
			
                   	<td class="label-title">筛查模式</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.testPattern" 
							id="sampleInputTemp_testPattern">
	    					<option value="2" <s:if test="sampleInputTemp.testPattern==2">selected="selected" </s:if>>早中孕期联合筛查</option>
	    					<option value="0" <s:if test="sampleInputTemp.testPattern==0">selected="selected" </s:if>>未做</option>
	    					<option value="1" <s:if test="sampleInputTemp.testPattern==1">selected="selected" </s:if>>早孕期筛查</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
					<td class="label-title">21-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_trisome21Value"
						name="sampleInputTemp.trisome21Value" title="21-三体比值"
						value="<s:property value="sampleInputTemp.trisome21Value"/>" />
                   	</td>
                   	
					<td class="label-title">18-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
	                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_trisome18Value"
						name="sampleInputTemp.trisome18Value" title="18-三体比值"
						value="<s:property value="sampleInputTemp.trisome18Value"/>" />
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title">临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_diagnosis" name="sampleInputTemp.diagnosis" title="临床诊断" value="<s:property value="sampleInputTemp.diagnosis"/>" />
                   	</td>
                   	
                   	<td class="label-title">夫妻双方染色体</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.coupleChromosome" 
						id="sampleInputTemp_coupleChromosome">
    					<option value="0"<s:if test="sampleInputTemp.coupleChromosome==0">selected="selected" </s:if>>未做</option>
    					<option value="1"<s:if test="sampleInputTemp.coupleChromosome==1">selected="selected" </s:if>>正常</option>
    					<option value="2"<s:if test="sampleInputTemp.coupleChromosome==2">selected="selected" </s:if>>异常</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">异常结果描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_reason2" name="sampleInputTemp.reason2" title="异常结果描述" value="<s:property value="sampleInputTemp.reason2"/>" />
                   	</td>
			</tr>
			<tr>
			
				<tr>
				<td colspan="9">
					<div class="standard-section-header type-title">
						<label>收费情况

		</label>
					</div>
				</td>
			</tr>		
			<tr>
						<td class="label-title">金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_money" name="sampleInputTemp.money" title="备注"
						value="<s:property value="sampleInputTemp.money"/>" />
                   	</td>
					
					<td class="label-title">是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.isFee" id="sampleInputTemp_isInsure">
	    					<option value="1"<s:if test="sampleInputTemp.isInsure==1">selected="selected" </s:if>>是</option>
	    					<option value="0"<s:if test="sampleInputTemp.isInsure==0">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.privilegeType" id="sampleInputTemp_privilegeType">
	    					<option value="0"<s:if test="sampleInputTemp.privilegeType==0">selected="selected" </s:if>>否</option>
	    					<option value="1"<s:if test="sampleInputTemp.privilegeType==1">selected="selected" </s:if>>是</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
			</tr>
			<tr>
					<td class="label-title">是否开票</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.isInvoice" id="sampleInputTemp_isInvoice">
	    					<option value="1" <s:if test="sampleInputTemp.isInvoice==0">selected="selected" </s:if>>是</option>
	    					<option value="0" <s:if test="sampleInputTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_paymentUnit"
						name="sampleInputTemp.paymentUnit" title="开票单位"
						value="<s:property value="sampleInputTemp.paymentUnit"/>" />
                   	</td>
                   	
                   	<td class="label-title">是否需要保险</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.isInsure"
						id="sampleInputTemp_isInsure">
    					<option value="1" <s:if test="sampleInputTemp.isInsure==0">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="sampleInputTemp.isInsure==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
			</tr>
			<tr>
                   	<td class="label-title">收据类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"
						id="sampleInputTemp_receiptType_name" name="sampleInputTemp.receiptType.name"
						value="<s:property value="sampleInputTemp.receiptType.name"/>" />
 						<input type="hidden" id="sampleInputTemp_receiptType"
						name="sampleInputTemp.receiptType.id"
						value="<s:property value="sampleInputTemp.receiptType.id"/>"> 
 						<img alt='选择收据类型' src='${ctx}/images/img_lookup.gif'
						onClick="receiptTypeFun()" class='detail' />                   		
                   	</td>
                   	
                   	
					<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_note" name="sampleInputTemp.note" title="备注"
						value="<s:property value="sampleInputTemp.note"/>" />
                   	</td>
					<td class="label-title">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25"
						id="sampleInputTemp_stateName" name="sampleInputTemp.stateName"
						title="工作流状态" readonly="readOnly"
						class="text input readonlytrue"
						value="<s:property value="sampleInputTemp.stateName"/>" />
                   	</td>
                   	
                   	<td class="label-title" style="display: none;">工作流状态ID</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none;"></td>            	 	
                   	<td align="left" style="display: none;">
                   	<input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_state" name="sampleInputTemp.state"
						title="工作流状态" readonly="readOnly"
						class="text input readonlytrue"
						value="<s:property value="sampleInputTemp.state"/>" />
                   	</td>
			</tr>
		
				<tr>
					<td class="label-title">补充协议</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	 <input type="text" size="20" maxlength="25" 
						id="sampleInputTemp_suppleAgreement" name="sampleInputTemp.suppleAgreement"
						title="补充协议" onblur="checkAddress()"
						value="<s:property value="sampleInputTemp.suppleAgreement"/>" />
                   	 <img class='requiredimage'
						src='${ctx}/images/required.gif' />
					</td>
					
					
					<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.nextStepFlow" id="sampleInputTemp_nextStepFlow">
                   			<option value="" <s:if test="sampleInputTemp.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleInputTemp.nextStepFlow==0">selected="selected" </s:if>>合格</option>
							<option value="1" <s:if test="sampleInputTemp.nextStepFlow==1">selected="selected" </s:if>>退费</option>
							<option value="2" <s:if test="sampleInputTemp.nextStepFlow==2">selected="selected" </s:if>>重抽血</option>
							<option value="3" <s:if test="sampleInputTemp.nextStepFlow==3">selected="selected" </s:if>>暂停</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td> 
					
				</tr>
				<tr>
					
					<td class="label-title">附件</td>
					<td></td>
					<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span
						class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
				</tr>
            </table>
		<input type="hidden" id="id_parent_hidden" value="<s:property value="sampleInputTemp.id"/>" /></form>
     		  <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
