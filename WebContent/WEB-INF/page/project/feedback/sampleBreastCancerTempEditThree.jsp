
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBreastCancerTemp&id=${sampleBreastCancerTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/sampleBreastCancerTempEditThree.js"></script>
<s:if test="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id != ''">
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id}"></div>
</s:if>
<s:if test="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id == ''">
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src=""></div>
</s:if>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleBreastCancerTemp.upLoadAccessory.id" value="<s:property value="sampleBreastCancerTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleBreastCancerTemp.upLoadAccessory.fileName" value="<s:property value="sampleBreastCancerTemp.upLoadAccessory.fileName"/>">
					<input type="hidden" id="saveType" value="rxa" >
				</td>
			</tr>
				<tr>
               	 	<td class="label-title">编号</td>
                    <td class="requiredcolunm" nowrap width="10px" ></td>
                   	<td align="left">
                   		<input type="text" 	  id="sampleBreastCancerTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleBreastCancerTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleBreastCancerTemp.sampleInfo.code"/>" />
						<input type="hidden"  id="sampleBreastCancerTemp_id"  name="sampleBreastCancerTemp.sampleInfo.id"  value="<s:property value="sampleBreastCancerTemp.sampleInfo.id"/>" />
						<input type="hidden"  name="sampleBreastCancerTemp.id" value="<s:property value="sampleBreastCancerTemp.id"/>" />
						<input type="hidden"  id="upload_imga_name" name="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id"/>">
                   	</td>
    
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_name"
                   	 		name="sampleBreastCancerTemp.name" title="描述"   
							value="<s:property value="sampleBreastCancerTemp.name"/>"/>
                   	</td>
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleBreastCancerTemp_product_name" searchField="true"  readonly="readonly"
 							name="sampleBreastCancerTemp.productName"  value="<s:property value="sampleBreastCancerTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleBreastCancerTemp_product_id" name="sampleBreastCancerTemp.productId"  
 							value="<s:property value="sampleBreastCancerTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   	
                    </tr>
                <tr> 	
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_acceptDate"
                   	 		name="sampleBreastCancerTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
       
                  	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_area"
                   	 		name="sampleBreastCancerTemp.area" title="地区"  onblur="checkAdd()" 
							value="<s:property value="sampleBreastCancerTemp.area"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_hospital"
                   	 		name="sampleBreastCancerTemp.hospital" title="送检医院"   
							value="<s:property value="sampleBreastCancerTemp.hospital"/>"
                   	 />
                   	</td>
                   	
                  </tr>
          
                  <tr>
                  	<td class="label-title" >病历号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_serialNum"
                   	 		name="sampleBreastCancerTemp.serialNum" title="病历号"   
							value="<s:property value="sampleBreastCancerTemp.serialNum"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >亲属编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_relationNum"
                   	 		name="sampleBreastCancerTemp.relationNum" title="亲属编号"   
							value="<s:property value="sampleBreastCancerTemp.relationNum"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >亲属关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.relationship1" id="sampleBreastCancerTemp_relationship1" >
							<option value="" <s:if test="sampleBreastCancerTemp.relationship1==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.relationship1==0">selected="selected" </s:if>>否</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.relationship1==1">selected="selected" </s:if>>是</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>		
                  </tr>
                  <tr>
                  	<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sendDate"
                   	 		name="sampleBreastCancerTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleBreastCancerTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	</td>
                   	<td class="label-title" >应出报告日期</td>
               	    <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                    <td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_reportDate" 
                   	 		name="sampleBreastCancerTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	 		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" 
                   	 		value="<s:date name="sampleBreastCancerTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	   />
                    </td>
                    <td class="label-title">样本编号</td>
                    <td class="requiredcolunm" nowrap width="10px" ></td>
                    <td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sampleNum" 
                   			name="sampleBreastCancerTemp.sampleNum" title="样本编号" value="<s:property value="sampleBreastCancerTemp.sampleNum"/>"
                   		/>
                   </td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
				 <tr>
                	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientName"
                   	 		name="sampleBreastCancerTemp.patientName" title="姓名"   
							value="<s:property value="sampleBreastCancerTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientNameSpell"
                   	 		name="sampleBreastCancerTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleBreastCancerTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gender" id="sampleBreastCancerTemp_gender" >
							<option value="" <s:if test="sampleBreastCancerTemp.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_age" name="sampleBreastCancerTemp.age"  value="<s:property value="sampleBreastCancerTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" >籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nativePlace"
                   	 		name="sampleBreastCancerTemp.nativePlace" title="籍贯"   
							value="<s:property value="sampleBreastCancerTemp.nativePlace"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nationality"
                   	 		name="sampleBreastCancerTemp.nationality" title="民族"   
							value="<s:property value="sampleBreastCancerTemp.nationality"/>"
                   	 	/>
                   	</td>
                 </tr>
                 <tr>
                	<td class="label-title" >身高</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_heights"
                   	 		name="sampleBreastCancerTemp.heights" title="身高" 
							value="<s:property value="sampleBreastCancerTemp.heights"/>"
                   		 />
                   	</td>
                   	
                   	<td class="label-title" >体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="5" id="sampleBreastCancerTemp_weight"
                   	 		name="sampleBreastCancerTemp.weight" title="体重" onkeypress="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onkeyup="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onblur="if(!this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?|\.\d*?)?$/))this.value=this.o_value;else{if(this.value.match(/^\.\d+$/))this.value=0+this.value;if(this.value.match(/^\.$/))this.value=0;this.o_value=this.value}"
							value="<s:property value="sampleBreastCancerTemp.weight"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.maritalStatus" id="sampleBreastCancerTemp_maritalStatus" >
							<option value="" <s:if test="sampleBreastCancerTemp.maritalStatus==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.maritalStatus==0">selected="selected" </s:if>>已婚</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.maritalStatus==1">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                  <tr>
                  	<td class="label-title" >联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleBreastCancerTemp_phone"
	                   		 name="sampleBreastCancerTemp.phone" title="联系方式"  value="<s:property value="sampleBreastCancerTemp.phone"/>"
	                   	 />
	                   	 <img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
               
                	<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_emailAddress"
                  	 		name="sampleBreastCancerTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleBreastCancerTemp.emailAddress"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_address"
                   	 		name="sampleBreastCancerTemp.address" title="通讯地址"   
							value="<s:property value="sampleBreastCancerTemp.address"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >入选者类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.choiceType" id="sampleBreastCancerTemp_choiceType" >
							<option value="" <s:if test="sampleBreastCancerTemp.choiceType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.choiceType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.choiceType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人及家族肿瘤病史</label>
						</div>
					</td>
				</tr>
               
                <tr>
                   	<td class="label-title" >个人乳腺肿瘤病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorHistory" id="sampleBreastCancerTemp_tumorHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorHistory==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >乳腺肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.ovaryTumorType" id="sampleBreastCancerTemp_ovaryTumorType" >
							<option value="" <s:if test="sampleBreastCancerTemp.ovaryTumorType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.ovaryTumorType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.ovaryTumorType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
        	
                   	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_confirmAgeOne" name="sampleBreastCancerTemp.confirmAgeone"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
 						value="<s:property value="sampleBreastCancerTemp.confirmAgeOne"/>" />
                   	</td>
                   	
                </tr>
                 <tr>
                   	<td class="label-title" >个人其他肿瘤病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.otherTumorHistory" id="sampleBreastCancerTemp_otherTumorHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.otherTumorHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.otherTumorHistory==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.otherTumorHistory==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorType" id="sampleBreastCancerTemp_tumorType" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_confirmAgeTwo" name="sampleBreastCancerTemp.confirmAgeTwo"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
 						value="<s:property value="sampleBreastCancerTemp.confirmAgeTwo"/>" />
                   	</td> 	
                </tr>
                <tr>
                	<td class="label-title" >是否有亲属患肿瘤</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isInvoice" id="sampleBreastCancerTemp_isInvoice" >
							<option value="" <s:if test="sampleBreastCancerTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorTypeOne" id="sampleBreastCancerTemp_tumorTypeOne" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorTypeOne==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorTypeOne==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorTypeOne==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >亲属关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.relationship2" id="sampleBreastCancerTemp_relationship2" >
							<option value="" <s:if test="sampleBreastCancerTemp.relationship2==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.relationship2==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.relationship2==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>		
                </tr>
                 <tr>
                	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_confirmAgeThree" name="sampleBreastCancerTemp.confirmAgeThree"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
 						value="<s:property value="sampleBreastCancerTemp.confirmAgeThree"/>" />
                   	</td> 
                </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人其他信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title" >初潮年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_confirmAge" name="sampleBreastCancerTemp.confirmAge"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
 							value="<s:property value="sampleBreastCancerTemp.confirmAge"/>" />
                   	</td>
                   	<td class="label-title" >月经周期</td>
		            <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
		            <td align="left"  >
	 					<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_MenstrualCycle" name="sampleBreastCancerTemp.MenstrualCycle"   onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
	 						value="<s:property value="sampleBreastCancerTemp.MenstrualCycle"/>" />
                   	</td> 
                 </tr>
                 <tr>	
                   	<td class="label-title" >首次妊娠年龄</td>
		            <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
		            <td align="left"  >
	 					<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_firstGestationAge" name="sampleBreastCancerTemp.firstGestationAge"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
	 						value="<s:property value="sampleBreastCancerTemp.firstGestationAge"/>" />
                   	</td> 	
                   	<td class="label-title" >孕次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="1" id="sampleBreastCancerTemp_pregnancyTime"
                   	 		name="sampleBreastCancerTemp.pregnancyTime" title="孕次数"   onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
							value="<s:property value="sampleBreastCancerTemp.pregnancyTime"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >产次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="1" id="sampleBreastCancerTemp_parturitionTime"
                   	 		name="sampleBreastCancerTemp.parturitionTime" title="产次数"   onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
							value="<s:property value="sampleBreastCancerTemp.parturitionTime"/>"
                   	 	/>
                   	</td>	
                 </tr>
                 <tr>
                   	<td class="label-title" >个人哺乳史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.otherBreastHistory" id="sampleBreastCancerTemp_otherBreastHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.otherBreastHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.otherBreastHistory==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.otherBreastHistory==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >哺乳次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="3" id="sampleBreastCancerTemp_gestationalAge"
                   	 		name="sampleBreastCancerTemp.gestationalAge" title="哺乳次数"   
							value="<s:property value="sampleBreastCancerTemp.gestationalAge"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >绝经情况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gestationIVF" id="sampleBreastCancerTemp_gestationIVF" >
							<option value="" <s:if test="sampleBreastCancerTemp.gestationIVF==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.gestationIVF==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gestationIVF==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                </tr>
                <tr>
                	<td class="label-title" >绝经年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_menopauseAge"
                   	 		name="sampleBreastCancerTemp.menopauseAge" title="绝经年龄"   onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
							value="<s:property value="sampleBreastCancerTemp.menopauseAge"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >是否接受乳腺活检</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.trisome21Value" id="sampleBreastCancerTemp_trisome21Value" >
							<option value="" <s:if test="sampleBreastCancerTemp.trisome21Value==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.trisome21Value==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.trisome21Value==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >接受次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_acceptNum"
                   	 		name="sampleBreastCancerTemp.acceptNum" title="接受次数"   
							value="<s:property value="sampleBreastCancerTemp.acceptNum"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >是否非典型乳腺增生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_isAtypicalBreast"
                   	 		name="sampleBreastCancerTemp.isAtypicalBreast" title="非典型乳腺增生"   
							value="<s:property value="sampleBreastCancerTemp.isAtypicalBreast"/>"
                   	 	/>
                    </td>
                 </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>有毒、有害物质长期接触史</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cigarette"
                   	 		name="sampleBreastCancerTemp.cigarette" title="烟"   
							value="<s:property value="sampleBreastCancerTemp.cigarette"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_wine"
                   	 		name="sampleBreastCancerTemp.wine" title="酒"   
							value="<s:property value="sampleBreastCancerTemp.wine"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicine"
                   	 		name="sampleBreastCancerTemp.medicine" title="药物"   
							value="<s:property value="sampleBreastCancerTemp.medicine"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >药物类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicineType"
                   	 		name="sampleBreastCancerTemp.medicineType" title="药物类型"   
							value="<s:property value="sampleBreastCancerTemp.medicineType"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_radioactiveRays"
                   	 		name="sampleBreastCancerTemp.radioactiveRays" title="放射线"   
							value="<s:property value="sampleBreastCancerTemp.radioactiveRays"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pesticide"
                   	 		name="sampleBreastCancerTemp.pesticide" title="农药"   
							value="<s:property value="sampleBreastCancerTemp.pesticide"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_plumbane"
                   	 		name="sampleBreastCancerTemp.plumbane" title="铅"   
							value="<s:property value="sampleBreastCancerTemp.plumbane"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_mercury"
                   	 		name="sampleBreastCancerTemp.mercury" title="汞"   
							value="<s:property value="sampleBreastCancerTemp.mercury"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cadmium"
                   	 	name="sampleBreastCancerTemp.cadmium" title="镉"   
						value="<s:property value="sampleBreastCancerTemp.cadmium"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                    <td class="label-title" >其它有害物质</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_otherJunk"
                   	 		name="sampleBreastCancerTemp.otherJunk" title="其它有害物质"   
							value="<s:property value="sampleBreastCancerTemp.otherJunk"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >乳房自检</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_breastExa"
                   	 		name="sampleBreastCancerTemp.breastExa" title="乳房自检"   
							value="<s:property value="sampleBreastCancerTemp.breastExa"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >病理类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pathologyType"
                   	 		name="sampleBreastCancerTemp.pathologyType" title="病理类型"   
							value="<s:property value="sampleBreastCancerTemp.pathologyType"/>"
                   	 	/>
                   	</td>
                </tr>
                	<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>TNM分期</label>
						</div>
					</td>
				</tr>	
                  <tr>
                  	<td class="label-title" >T</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_t"
                   	 		name="sampleBreastCancerTemp.t" title="T"   
							value="<s:property value="sampleBreastCancerTemp.t"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >N</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_n"
                   	 		name="sampleBreastCancerTemp.n" title="N"   
							value="<s:property value="sampleBreastCancerTemp.n"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >M</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_m"
                   	 		name="sampleBreastCancerTemp.m" title="M"   
							value="<s:property value="sampleBreastCancerTemp.m"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >分期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_staging"
                   	 		name="sampleBreastCancerTemp.staging" title="分期"   
							value="<s:property value="sampleBreastCancerTemp.staging"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>免疫组化验检查</label>
						</div>
					</td>
				</tr>	
                  <tr>
                  	<td class="label-title" >ER</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_er"
                   	 		name="sampleBreastCancerTemp.er" title="ER"   
							value="<s:property value="sampleBreastCancerTemp.er"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >PR</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pr"
                   	 		name="sampleBreastCancerTemp.pr" title="PR"   
							value="<s:property value="sampleBreastCancerTemp.pr"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >Her2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_her2"
                   	 		name="sampleBreastCancerTemp.her2" title="Her2"   
							value="<s:property value="sampleBreastCancerTemp.her2"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >FISH</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_fish"
                   	 		name="sampleBreastCancerTemp.fish" title="FISH"   
							value="<s:property value="sampleBreastCancerTemp.fish"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >Ki67</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_ki"
                   	 		name="sampleBreastCancerTemp.ki" title="Ki67"   
							value="<s:property value="sampleBreastCancerTemp.ki"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >P53</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_p53"
                   	 		name="sampleBreastCancerTemp.p53" title="P53"   
							value="<s:property value="sampleBreastCancerTemp.p53"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >癌胚抗原（CEA）（ug/L)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_antigen1"
                   	 		name="sampleBreastCancerTemp.antigen1" title="癌胚抗原（CEA）（ug/L)"   
							value="<s:property value="sampleBreastCancerTemp.antigen1"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >癌抗原125（CA125)（U/ml）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_antigen2"
                   	 		name="sampleBreastCancerTemp.antigen2" title="癌抗原125（CA125)（U/ml）"   
							value="<s:property value="sampleBreastCancerTemp.antigen2"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >癌抗原15-3（CA15-3)（U/ml）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_antigen3"
                   	 		name="sampleBreastCancerTemp.antigen3" title="癌抗原15-3（CA15-3)（U/ml）"   
							value="<s:property value="sampleBreastCancerTemp.antigen3"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>影像学筛查</label>
						</div>
					</td>
				</tr>	
                  <tr>
                  	<td class="label-title" >X线检查所见</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_roentgenFindings1"
                   	 		name="sampleBreastCancerTemp.roentgenFindings1" title="X线检查所见"   
							value="<s:property value="sampleBreastCancerTemp.roentgenFindings1"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >分级</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_rate1"
                   	 		name="sampleBreastCancerTemp.rate1" title="分级"   
							value="<s:property value="sampleBreastCancerTemp.rate1"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >B超检查所见</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_roentgenFindings2"
                   	 		name="sampleBreastCancerTemp.roentgenFindings2" title="B超检查所见"   
							value="<s:property value="sampleBreastCancerTemp.roentgenFindings2"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >分级</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_rate2"
                   	 		name="sampleBreastCancerTemp.rate2" title="分级"   
							value="<s:property value="sampleBreastCancerTemp.rate2"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >MRI检查所见</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_roentgenFindings3"
                   	 		name="sampleBreastCancerTemp.roentgenFindings3" title="MRI检查所见"   
							value="<s:property value="sampleBreastCancerTemp.roentgenFindings3"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >分级</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_rate3"
                   	 		name="sampleBreastCancerTemp.rate3" title="分级"   
							value="<s:property value="sampleBreastCancerTemp.rate3"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>信息确认核对</label>
						</div>
					</td>
				</tr>	
                  <tr>
                  	<td class="label-title" >填写人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_signed"
                   	 		name="sampleBreastCancerTemp.signed" title="signed"   
							value="<s:property value="sampleBreastCancerTemp.signed"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >填写人电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_phoneNum2"
                   	 		name="sampleBreastCancerTemp.phoneNum2" title="phoneNum2"   
							value="<s:property value="sampleBreastCancerTemp.phoneNum2"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isFee" id="sampleBreastCancerTemp_isFee" >
							<option value="" <s:if test="sampleBreastCancerTemp.isFee==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isFee==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isFee==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
<%--                   	<td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_id"
                   	 		name="sampleBreastCancer.createUser.id" title="录入人" value="<s:property value="sampleBreastCancer.createUser.id"/>"   
                   	 	/>
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_name" class="text input readonlytrue" readonly="readonly"
                   	 		name="sampleBreastCancerTemp.createUser.name" title="录入人"   
							value="<s:property value="sampleBreastCancerTemp.createUser.name"/>"
                   	 	/>
                   	</td> --%>
                   	<td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly" class="text input readonlytrue" id="sampleBreastCancerTemp_createUser" name="sampleBreastCancerTemp.createUser" value="<s:property value="sampleBreastCancerTemp.createUser"/>" />
<%--  						<input type="hidden" id="sampleBreastCancerTemp_createUser" name="sampleBreastCancerTemp.createUser.id"  value="<s:property value="sampleBreastCancerTemp.createUser.id"/>" >  --%>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note1"
                   	 		name="sampleBreastCancerTemp.note1" title="备注"
							value="<s:property value="sampleBreastCancerTemp.note1"/>"
                   	 	/>
                   	</td>
                 </tr>
                 <tr>
                	<g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reportManUserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_reportMan"
				documentName="sampleBreastCancerTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_reportMan_name"  value="<s:property value="sampleBreastCancerTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_reportMan" name="sampleBreastCancerTemp.reportMan.id"  value="<s:property value="sampleBreastCancerTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 <g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="auditManUserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_auditMan"
				documentName="sampleBreastCancerTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_auditMan_name"  value="<s:property value="sampleBreastCancerTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_auditMan" name="sampleBreastCancerTemp.auditMan.id"  value="<s:property value="sampleBreastCancerTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleBreastCancerTemp.nextStepFlow" id="sampleBreastCancerTemp_nextStepFlow">
                   			<option value="" <s:if test="sampleBreastCancerTemp.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.nextStepFlow==0">selected="selected" </s:if>>合格</option>
							<option value="1" <s:if test="sampleBreastCancerTemp.nextStepFlow==1">selected="selected" </s:if>>退费</option>
							<option value="2" <s:if test="sampleBreastCancerTemp.nextStepFlow==2">selected="selected" </s:if>>重抽血</option>
							<option value="3" <s:if test="sampleBreastCancerTemp.nextStepFlow==3">selected="selected" </s:if>>暂停</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>

            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBreastCancer.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
