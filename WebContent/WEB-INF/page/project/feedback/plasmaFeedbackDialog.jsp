﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/plasmaFeedbackDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">血浆编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="plasmaFeedback_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="plasmaFeedback_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="plasmaFeedback_sampleCode" searchField="true" name="sampleCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">体积</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="plasmaFeedback_bluk" searchField="true" name="bluk"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">是否合格</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="plasmaFeedback_isgood" searchField="true" name="isgood"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">下一步流向</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="plasmaFeedback_nextflow" searchField="true" name="nextflow"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">处理意见</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="plasmaFeedback_advice" searchField="true" name="advice"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态id</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="plasmaFeedback_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_plasmaFeedback_div"></div>
   		
</body>
</html>



