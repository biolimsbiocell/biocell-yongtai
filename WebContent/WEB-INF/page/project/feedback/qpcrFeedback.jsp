
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/project/feedback/qpcrFeedback.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="输入查询条件">
			样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="code" id="qpcrFeedback_code" searchType ="5" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			原始样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="qpcrFeedback_sampleCode" searchType ="5" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			文库编号
			</label>
			</td>
			<td>
			<input type="text"  name="wkCode" id="qpcrFeedback_wkCode" searchType ="5" searchField="true" >
			</td>
			<!-- <td >
			<label class="text label" title="输入查询条件">
			文库类型
			</label>
			</td>
			<td>
				<select id="qpcrFeedback_wkType" name="wkType" searchField="true" searchType ="5" style="width:100">
                		<option value="">请选择</option>
    					<option value="0">2100质控</option>
    					<option value="1">QPCR质控</option>
    					<option value="2">2100 or Caliper</option>
				</select>
			</td> -->
			<td>
			<input type="button" onClick="selectQcInfo()" value="查询">
			</td>
			</tr>
		</table>
		<div id="show_qpcrFeedback_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		
		<div id="bat_qcmethod_div" style="display: none">
			<table>
				<tr>
					<td class="label-title" ><span>批量处理</span></td>
	                <td><select id="qcmethod"  style="width:100">
	                		<option value="" >请选择</option>
	    					<option value="0">Pooling</option>
	    					<option value="1">重抽血</option>
	    					<option value="2">重质检</option>
	    					<option value="3">终止</option>
	    					<option value="4">入库</option>
						</select>
	                 </td>
				</tr>
			</table>
		</div>
		
		<div id="bat_qcsubmit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>批量提交</span></td>
                <td><select id="qcsubmit"  style="width:100">
                		<option value=" ">请选择</option>
    					<option value="1">是</option>
    					<option value="0">否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>



