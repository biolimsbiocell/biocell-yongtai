﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/sampleFeedBackComme.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		 <div id="tabs">
            <ul>
           <li><a href="#show_sampleFeedback_div">样本异常(${requestScope.count1})</a></li>
			<li><a href="#show_plasmaFeedback_div">样本处理异常(${requestScope.count2})</a></li>
			<li><a href="#show_dnaFeedback_div">核酸提取异常(${requestScope.count3})</a></li>
			
			<li><a href="#show_wkFeedback_div">文库构建异常(${requestScope.count4})</a></li>
          <%--  	<li><a href="#show_poolingFeedback_div">Pooling异常(${requestScope.count5})</a></li> --%>
           	<li><a href="#show_qualityFeedback_div">2100质控异常(${requestScope.count6})</a></li>
      		<li><a href="#show_qpcrFeedback_div">QPCR质控异常(${requestScope.count11})</a></li>
    <%--        	<li><a href="#show_sequencingFeedback_div">上机异常(${requestScope.count7})</a></li>
           	<li><a href="#show_deSequencingFeedback_div">下机异常(${requestScope.count8})</a></li> --%>
           	<li><a href="#show_analysisFeedback_div">分析异常(${requestScope.count9})</a></li>
           	<li><a href="#show_interpretFeedback_div">解读异常(${requestScope.count10})</a></li>
           <%-- 	<li><a href="#show_techCheckFeedBack_div">核酸检测反馈(${requestScope.count12})</a></li>
           	<li><a href="#show_techCheckWkFeedBack_div">文库检测反馈(${requestScope.count13})</a></li> --%>
           	</ul>
           			 <input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		
			<div id="show_sampleFeedback_div" width="100%" height:10px></div>
			<div id="show_plasmaFeedback_div" width="100%" height:10px></div>
			<div id="show_dnaFeedback_div" width="100%" height:10px></div>
			<div id="show_wkFeedback_div" width="100%" height:10px></div>
			<div id="show_poolingFeedback_div" width="100%" height:10px></div>
			<div id="show_qualityFeedback_div" width="100%" height:10px></div>
			<div id="show_qpcrFeedback_div" width="100%" height:10px></div>
			<div id="show_sequencingFeedback_div" width="100%" height:10px></div>
			<div id="show_deSequencingFeedback_div" width="100%" height:10px ></div>
			<div id="show_analysisFeedback_div" width="100%" height:10px></div>
			<div id="show_interpretFeedback_div" width="100%" height:10px></div>
			<!-- <div id="show_techCheckFeedBack_div" width="100%" height:10px></div>
			<div id="show_techCheckWkFeedBack_div" width="100%" height:10px></div> -->
		<!-- 	<div id="show_dnaFeedback_div" width="100%" height:10px></div> -->
		<!-- <div id="show_sampleFeedback_div"></div> -->
		 </div>
</body>
</html>



