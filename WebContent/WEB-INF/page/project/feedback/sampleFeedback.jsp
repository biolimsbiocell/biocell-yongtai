﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/sampleFeedback.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>

</head>
<body>
<%-- 
		
		 <div id="tabs">
            <ul>
            <li><a href="#show_sampleFeedback_div">样本异常(${requestScope.count1})</a></li>
			<li><a href="#show_plasmaFeedback_div">血浆异常(${requestScope.count2})</a></li>
			<li><a href="#show_dnaFeedback_div">DNA异常(${requestScope.count3})</a></li>
			
			<li><a href="#show_wkFeedback_div">文库异常(${requestScope.count4})</a></li>
           	<li><a href="#show_poolingFeedback_div">Pooling异常(${requestScope.count5})</a></li>
           	<li><a href="#show_qualityFeedback_div">2100质控异常(${requestScope.count6})</a></li>
      		<li><a href="#show_qpcrFeedback_div">QPCR质控异常(${requestScope.count11})</a></li>
           	<li><a href="#show_sequencingFeedback_div">上机异常(${requestScope.count7})</a></li>
           	<li><a href="#show_deSequencingFeedback_div">下机异常(${requestScope.count8})</a></li>
           	<li><a href="#show_analysisFeedback_div">分析异常(${requestScope.count9})</a></li>
           	<li><a href="#show_interpretFeedback_div">解读异常(${requestScope.count10})</a></li>
           	<li><a href="#show_techCheckFeedBack_div">核酸检测反馈(${requestScope.count12})</a></li>
           	<li><a href="#show_techCheckWkFeedBack_div">文库检测反馈(${requestScope.count13})</a></li>
           	</ul>
           			 <input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
				<td >
					<label class="text label" title="输入查询条件">
						样本编号
					</label>
				</td>
				<td>
					<input type="text"  name="id" id="sampleFeedback_id" searchType ="33" searchField="true" >
				</td>
				<td>
			<input type="button" onClick="selectSampleFeedback()" value="查询">
			</td>
		<!--	<td >
					<label class="text label" title="输入查询条件">
						样本编号
					</label>
				</td>
				<td>
					<input type="text"  name="sampleCode" id="dnaFeedback_sampleCode" searchType ="1" searchField="true" >
				</td>
				<td >
					<label class="text label" title="输入查询条件">
						处理意见
					</label>
				</td> -->
			</tr>
		</table>
			<div id="show_sampleFeedback_div" width="100%" height:10px></div>
			<div id="show_plasmaFeedback_div" width="100%" height:10px></div>
			<div id="show_dnaFeedback_div" width="100%" height:10px></div>
			<div id="show_wkFeedback_div" width="100%" height:10px></div>
			<div id="show_poolingFeedback_div" width="100%" height:10px></div>
			<div id="show_qualityFeedback_div" width="100%" height:10px></div>
			<div id="show_qpcrFeedback_div" width="100%" height:10px></div>
			<div id="show_sequencingFeedback_div" width="100%" height:10px></div>
			<div id="show_deSequencingFeedback_div" width="100%" height:10px ></div>
			<div id="show_analysisFeedback_div" width="100%" height:10px></div>
			<div id="show_interpretFeedback_div" width="100%" height:10px></div>
			<!-- <div id="show_techCheckFeedBack_div" width="100%" height:10px></div>
			<div id="show_techCheckWkFeedBack_div" width="100%" height:10px></div> -->
		<!-- 	<div id="show_dnaFeedback_div" width="100%" height:10px></div> -->
		<!-- <div id="show_sampleFeedback_div"></div> -->
		 </div>

   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleFeedback_tree_page"></div> --%>
		<input type="hidden" id="selectId"/>
		 <input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
				<td >
					<label class="text label" title="输入查询条件">
						样本编号
					</label>
				</td>
				<td>
					<input type="text"  name="sampleCode" id="sampleFeedback_sampleCode" searchType ="33" searchField="true" >
				</td>
				<td>
			<input type="button" onClick="selectSampleFeedback()" value="查询">
			</td>
		<!--	<td >
					<label class="text label" title="输入查询条件">
						样本编号
					</label>
				</td>
				<td>
					<input type="text"  name="sampleCode" id="dnaFeedback_sampleCode" searchType ="1" searchField="true" >
				</td>
				<td >
					<label class="text label" title="输入查询条件">
						处理意见
					</label>
				</td> -->
			</tr>
		</table>
		<div id="show_sampleFeedback_div" ></div>
		<div id="bat_submit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>是否提交</span></td>
                <td><select id="submit"  style="width:100">
                		<option value=" ">请选择</option>
    					<option value="1">是</option>
    					<option value="0">否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>



