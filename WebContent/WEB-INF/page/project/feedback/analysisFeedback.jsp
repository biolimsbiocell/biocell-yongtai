﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/project/feedback/analysisFeedback.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="输入查询条件">
			Pooling编号
			</label>
			</td>
			<td>
			<input type="text"  name="code" id="analysisFeedback_poolingCode" searchType ="7" searchField="true" >
			</td>
			
			<td >
			<label class="text label" title="输入查询条件">
			样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="analysisFeedback_sampleCode" searchType ="7"  searchField="true" >
			</td>
			<td>
			<label class="text label" title="输入查询条件">
			处理意见
			</label>
			</td>
			<td>
			<select id="analysisFeedback_advice" name="advice" searchType ="7" searchField="true" style="width:100">
                		<option value=" ">请选择</option>
    					<option value="0">T13高风险</option>
    					<option value="1">T18高风险</option>
    					<option value="2">T21高风险</option>
    					<option value="3">低风险</option>
    					<option value="4">重抽血</option>
    					<option value="5">退费</option>
    					<option value="6">重建库</option>
    					<option value="7">重上机</option>
				</select>
			</td>
			
			<td>
			<input type="button" onClick="selectAnalysisInfo()" value="查询">
			</td>
			</tr>
		</table>
		<div id="show_analysisFeedback_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_analysisFeedback_tree_page"></div>
		<select id="isgood" style="display: none">
			<option value="0">合格</option>
			<option value="1">不合格</option>
		</select>
</body>
</html>



