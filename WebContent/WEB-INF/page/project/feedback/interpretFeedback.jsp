﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/project/feedback/interpretFeedback.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >文库编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_id"
                   	 name="id" searchField="true" title="文库编号"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="interpretFeedback_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="interpretFeedback_sampleCode"
                   	 name="sampleCode" searchField="true" title="样本编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >体积</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_bluk"
                   	 name="bluk" searchField="true" title="体积"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >index</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="interpretFeedback_indexs"
                   	 name="indexs" searchField="true" title="index"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >浓度</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_concentrer"
                   	 name="concentrer" searchField="true" title="浓度"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >指控类型</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_qctype"
                   	 name="qctype" searchField="true" title="指控类型"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >是否合格</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_isgood"
                   	 name="isgood" searchField="true" title="是否合格"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >下一步流向</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_nextflow"
                   	 name="nextflow" searchField="true" title="下一步流向"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >处理意见</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_advice"
                   	 name="advice" searchField="true" title="处理意见"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >状态id</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_state"
                   	 name="state" searchField="true" title="状态id"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="interpretFeedback_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_interpretFeedback_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_interpretFeedback_tree_page"></div>
		<select id="isgood" style="display: none">
			<option value="0">合格</option>
			<option value="1">不合格</option>
		</select>
</body>
</html>



