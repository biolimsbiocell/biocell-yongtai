
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBloodDiseaseTemp&id=${sampleBloodDiseaseTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/feedback/sampleBloodDiseaseTempEdit.js"></script>
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBloodDiseaseTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleBloodDiseaseTemp.upLoadAccessory.id" value="<s:property value="sampleBloodDiseaseTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleBloodDiseaseTemp.upLoadAccessory.fileName" value="<s:property value="sampleBloodDiseaseTemp.upLoadAccessory.fileName"/>">
					<input type="hidden" id="saveType" value="xyb" >
				</td>
			</tr>
				<tr>
               	 	<td class="label-title">编号</td>
                    <td class="requiredcolunm" nowrap width="10px" ></td>
                   	<td align="left">
<%--                    		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleBloodDiseaseTemp.code" title="编号" value="<s:property value="sampleBloodDiseaseTemp.code"/>" /> --%>
<%-- 						<input type="hidden" name="sampleBloodDiseaseTemp.id" value="<s:property value="sampleBloodDiseaseTemp.id"/>" /> --%>
<%--                    		<input type="hidden"  id="upload_imga_name" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName"/>"> --%>
<%-- 						<input type="hidden"  id="upload_imga_id10" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id"/>"> --%>
                   		<input type="text" 	  id="sampleBloodDiseaseTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleBloodDiseaseTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.code"/>" />
						<input type="hidden"  id="sampleBloodDiseaseTemp_id"  name="sampleBloodDiseaseTemp.sampleInfo.id"  value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.id"/>" />
						<input type="hidden"  name="sampleBloodDiseaseTemp.id" value="<s:property value="sampleBloodDiseaseTemp.id"/>" />
						<input type="hidden"  id="upload_imga_name" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id"/>">
                  	</td>

                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_name" name="sampleBloodDiseaseTemp.name" title="描述" value="<s:property value="sampleBloodDiseaseTemp.name"/>" />
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleBloodDiseaseTemp_product_name" searchField="true"  
 							name="sampleBloodDiseaseTemp.productName"  value="<s:property value="sampleBloodDiseaseTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_product_id" name="sampleBloodDiseaseTemp.productId"  
 							value="<s:property value="sampleBloodDiseaseTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                  </tr>
                <tr>   	
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_acceptDate"
                   	 		name="sampleBloodDiseaseTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBloodDiseaseTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	    />
                   	</td>
              
                	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_area"
                   	 		name="sampleBloodDiseaseTemp.area" title="地区"  onblur="checkAdd()" 
							value="<s:property value="sampleBloodDiseaseTemp.area"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_hospital"
                   	 		name="sampleBloodDiseaseTemp.hospital" title="送检医院"   
							value="<s:property value="sampleBloodDiseaseTemp.hospital"/>"
                   	 	/>
                   	</td>
                   	
                   	  </tr>
                <tr>
                   	<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_sendDate"
                   	 		name="sampleBloodDiseaseTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleBloodDiseaseTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	 	/>
                   	</td>
              
                	<td class="label-title" >应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_reportDate"
                   	 		name="sampleBloodDiseaseTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBloodDiseaseTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	
                   	</td>
                   	
                   	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleBloodDiseaseTemp_sampleType_name"  value="<s:property value="sampleBloodDiseaseTemp.sampleType.name"/>"/>
						<s:hidden id="sampleBloodDiseaseTemp_sampleType_id" name="sampleBloodDiseaseTemp.sampleType.id"></s:hidden>
						<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>	
                </tr>
                <tr>
                	<td class="label-title" >数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBloodDiseaseTemp_confirmAgeOne" name="sampleBloodDiseaseTemp.confirmAgeOne"  
 							value="<s:property value="sampleBloodDiseaseTemp.confirmAgeOne"/>" />
                   	</td>
                   	<td class="label-title" >送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_doctor"
                   	 		name="sampleBloodDiseaseTemp.doctor" title="送检医生"   
							value="<s:property value="sampleBloodDiseaseTemp.doctor"/>"
                   		 />
                   	</td>
                   	<td class="label-title" >医生联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_phone"
                   	 		name="sampleBloodDiseaseTemp.phone" title="医生联系方式"   
							value="<s:property value="sampleBloodDiseaseTemp.phone"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title">样本编号</td>
                    <td class="requiredcolunm" nowrap width="10px" ></td>
                    <td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_sampleNum" 
                   			name="sampleBloodDiseaseTemp.sampleNum" title="样本编号" value="<s:property value="sampleBloodDiseaseTemp.sampleNum"/>"
                   		/>
                   </td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >患者姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_patientName"
                   	 		name="sampleBloodDiseaseTemp.patientName" title="姓名"   
							value="<s:property value="sampleBloodDiseaseTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_patientNameSpell"
                   	 		name="sampleBloodDiseaseTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleBloodDiseaseTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.gender" id="sampleBloodDiseaseTemp_gender" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBloodDiseaseTemp_age" name="sampleBloodDiseaseTemp.age"  value="<s:property value="sampleBloodDiseaseTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleBloodDiseaseTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleBloodDiseaseTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_voucherType" name="sampleBloodDiseaseTemp.voucherType.id"  value="<s:property value="sampleBloodDiseaseTemp.voucherType.id"/>" > 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_voucherCode"
                   	 		name="sampleBloodDiseaseTemp.voucherCode" title="证件号码"   onblur="checkFun()"
							value="<s:property value="sampleBloodDiseaseTemp.voucherCode"/>"
                   	 	/>
                   	</td>
                   	
                </tr>
                <tr>
                	<td class="label-title" >联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleBloodDiseaseTemp_phoneNum2"
	                   		 name="sampleBloodDiseaseTemp.phoneNum2" title="联系方式"  value="<s:property value="sampleBloodDiseaseTemp.phoneNum2"/>"
	                   	 />
                   	</td>

                   	<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_emailAddress"
                  	 		name="sampleBloodDiseaseTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleBloodDiseaseTemp.emailAddress"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_address"
                   	 		name="sampleBloodDiseaseTemp.address" title="通讯地址"   
							value="<s:property value="sampleBloodDiseaseTemp.address"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >检测时期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_gestationalAge"
                   	 		name="sampleBloodDiseaseTemp.gestationalAge" title="检测时期"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.gestationalAge"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >重要体征</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_reason"
                   	 		name="sampleBloodDiseaseTemp.reason" title="重要体征"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.reason"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   		<td class="label-title" >相关病史</td>
               	 		<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   		<td align="left"  >
                   			<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_reason2"
                   	 			name="sampleBloodDiseaseTemp.reason2" title="相关病史"   
                   	 			value="<s:property value="sampleBloodDiseaseTemp.reason2"/>"
                   	 		/>
                   	 		<img class='requiredimage' src='${ctx}/images/required.gif' />
                   		</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>既往病史</label>
						</div>
					</td>
				</tr>
                <tr>
                	
                </tr>
                <tr>
                	<td class="label-title" >化疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_tumorHistory"
                   	 		name="sampleBloodDiseaseTemp.tumorHistory" title="化疗"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.tumorHistory"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >最后一次化疗时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_firstTransfusionDate"
                   	 		name="sampleBloodDiseaseTemp.firstTransfusionDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBloodDiseaseTemp.firstTransfusionDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	
                   	</td>
                   	<td class="label-title" >近期用过免疫抑制剂</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.medicineType" id="sampleBloodDiseaseTemp_medicineType" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.medicineType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.medicineType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.medicineType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >具体的免疫抑制药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_medicine"
                   	 		name="sampleBloodDiseaseTemp.medicine" title="具体的免疫抑制药物"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.medicine"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" >放疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_radioactiveRays"
                   	 		name="sampleBloodDiseaseTemp.radioactiveRays" title="放疗"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.radioactiveRays"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   </td>
                   	<td class="label-title" >骨髓生长刺激因子</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_cadmium"
                   	 		name="sampleBloodDiseaseTemp.cadmium" title="骨髓生长刺激因子"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.cadmium"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>外周血检测结果</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >WBC</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_trisome21Value"
                   	 		name="sampleBloodDiseaseTemp.trisome21Value" title="WBC"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.trisome21Value"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >RBC</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_trisome18Value"
                   	 		name="sampleBloodDiseaseTemp.trisome18Value" title="WBC"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.trisome18Value"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >Hb</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_trisome13Value"
                   	 		name="sampleBloodDiseaseTemp.trisome13Value" title="Hb"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.trisome13Value"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >PLT</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_confirmAgeTwo"
                   	 		name="sampleBloodDiseaseTemp.confirmAgeTwo" title="PLT"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.confirmAgeTwo"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" >原始细胞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_confirmAgeThree"
                   	 		name="sampleBloodDiseaseTemp.confirmAgeThree" title="原始细胞"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.confirmAgeThree"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" >幼稚细胞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_confirmAge"
                   	 		name="sampleBloodDiseaseTemp.confirmAge" title="幼稚细胞"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.confirmAge"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >其他细胞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_stemCellsCure"
                   	 		name="sampleBloodDiseaseTemp.stemCellsCure" title="其他细胞"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.stemCellsCure"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" >骨髓检查结果</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_badMotherhood"
                   	 		name="sampleBloodDiseaseTemp.badMotherhood" title="骨髓检查结果"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.badMotherhood"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >临床诊断意见</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_diagnosis"
                   	 		name="sampleBloodDiseaseTemp.diagnosis" title="临床诊断意见"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.diagnosis"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>收费情况</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.isFee" id="sampleBloodDiseaseTemp_isInsure" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.isInsure==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.isInsure==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.isInsure==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.privilegeType" id="sampleBloodDiseaseTemp_privilegeType" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.privilegeType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.privilegeType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.privilegeType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_linkman"
                   	 		name="sampleBloodDiseaseTemp.linkman" title="推荐人"   
							value="<s:property value="sampleBloodDiseaseTemp.linkman"/>"
                   		/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.isInvoice" id="sampleBloodDiseaseTemp_isInvoice" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                    <td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="sampleBloodDiseaseTemp.createUser" title="录入人"   
							value="<s:property value="sampleBloodDiseaseTemp.createUser"/>"/>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_note"
                   	 		name="sampleBloodDiseaseTemp.note" title="备注"   
							value="<s:property value="sampleBloodDiseaseTemp.note"/>"
                   	 	/>
                   	</td>
                </tr>
                 <tr>
                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_money"
                   	 		name="sampleBloodDiseaseTemp.money" title="备注"   
							value="<s:property value="sampleBloodDiseaseTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_bloodHistory"
                   	 		name="sampleBloodDiseaseTemp.bloodHistory" title="SP"   
							value="<s:property value="sampleBloodDiseaseTemp.bloodHistory"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_paymentUnit"
                   	 		name="sampleBloodDiseaseTemp.paymentUnit" title="开票单位"   
							value="<s:property value="sampleBloodDiseaseTemp.paymentUnit"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                <g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reportManUserFun" 
 				hasSetFun="true"
				documentId="sampleBloodDiseaseTemp_reportMan"
				documentName="sampleBloodDiseaseTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBloodDiseaseTemp_reportMan_name"  value="<s:property value="sampleBloodDiseaseTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_reportMan" name="sampleBloodDiseaseTemp.reportMan.id"  value="<s:property value="sampleBloodDiseaseTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 <g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="auditManUserFun" 
 				hasSetFun="true"
				documentId="sampleBloodDiseaseTemp_auditMan"
				documentName="sampleBloodDiseaseTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBloodDiseaseTemp_auditMan_name"  value="<s:property value="sampleBloodDiseaseTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_auditMan" name="sampleBloodDiseaseTemp.auditMan.id"  value="<s:property value="sampleBloodDiseaseTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleBloodDiseaseTemp.nextStepFlow" id="sampleBloodDiseaseTemp_nextStepFlow">
                   			<option value="" <s:if test="sampleBloodDiseaseTemp.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.nextStepFlow==0">selected="selected" </s:if>>合格</option>
							<option value="1" <s:if test="sampleBloodDiseaseTemp.nextStepFlow==1">selected="selected" </s:if>>退费</option>
							<option value="2" <s:if test="sampleBloodDiseaseTemp.nextStepFlow==2">selected="selected" </s:if>>重抽血</option>
							<option value="3" <s:if test="sampleBloodDiseaseTemp.nextStepFlow==3">selected="selected" </s:if>>暂停</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
				
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBloodDisease.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
