﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/project/feedback/plasmaFeedback.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<!-- <div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >血浆编号</td>
                   	<td align="left">
                  
					<input type="text" size="30" maxlength="25" id="plasmaFeedback_bloodCode"
                   	 name="bloodCode" searchField="true" title="血浆编号"/>
                   	</td>
                   	
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="50" id="plasmaFeedback_sampleCode"
                   	 name="sampleCode" searchField="true" title="样本编号"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >处理意见</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="25" id="plasmaFeedback_method"
                   	 name="method" searchField="true" title="处理意见"    />
                   	
                   	</td>
                   	
                   	<td class="label-title" >是否提交</td>
                   	<td align="left">
                  	<td><select id="plasmaFeedback_submit" name="submit" searchField="true">
                		<option value="" <s:if test="submit==">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="submit==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="submit==0">selected="selected" </s:if>>否</option>
						</select>
                 	</td>
			</tr>
            </table>
		</form>
		</div> -->
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="输入查询条件">
			样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="code" id="plasmaFeedback_code" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			原始样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="plasmaFeedback_sampleCode" searchField="true" >
			</td>
			<td>
			<input type="button" onClick="selectPlasmaInfo()" value="查询">
			</td>
			</tr>
		</table>
		<div id="show_plasmaFeedback_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_plasmaFeedback_tree_page"></div>
	<div id="bat_bloodSubmit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>批量提交</span></td>
                <td><select id="bloodSubmit"  style="width:100">
                		<option value=" ">请选择</option>
    					<option value="1">是</option>
    					<option value="0">否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>



