﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%-- <%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%> --%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/project/feedback/techCheckFeedBack.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="输入查询条件">
			样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="techCheckFeedBack_sampleCode" searchType ="1" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			核酸编号
			</label>
			</td>
			<td>
			<input type="text"  name="dnaCode" id="techCheckFeedBack_dnaCode" searchType ="1" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			处理意见
			</label>
			</td>
			<td>
			<select id="techCheckFeedBack_method" name="method" searchType ="1" searchField="true" style="width:100">
                		<option value=" ">请选择</option>
    					<option value="0">出报告</option>
    					<option value="1">继续</option>
				</select>
			</td>
			<td>
			<input type="button" onClick="selectTechCheckFeedBackInfo()" value="查询">
			</td>
			</tr>
		</table>
	<div id="techCheckFeedBackdiv"></div>
</body>
</html>



