
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInputTemp&id=${sampleInputTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/js/project/feedback/sampleInputTempEditTwo.js"></script>
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
    <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
<!-- 				<td class="label-title" style="color: red">点击上传图片</td> -->
				<td></td>
				<td>
<!--   					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" /> -->
				</td>
			</tr>
				<tr>
<!--                	 	<td class="label-title" style="display: none">编号</td> -->
<!-- 					<td class="requiredcolunm" nowrap width="10px" style="display: none"></td> --%>
<!-- 					<td align="left" style="display: none"><input type="text" size="20" maxlength="25" -->
<!-- 						id="sampleInputTemp_id" -->
<!-- 						class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.id" title="编号" -->
<%-- 						value="<s:property value="sampleInputTemp.id"/>" /> --%>
<!-- 					</td> -->
                    <td class="label-title">样本编号</td>
					<td class="requiredcolumn" nowrap width="10px">
						
					</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_code" name="sampleInputTemp.sampleInfo.code" class="text input readonlytrue" readonly="readonly" title="样本编号" 
						class="text input readonlytrue" readonly="readonly" value="<s:property value="sampleInputTemp.sampleInfo.code"/>" />
						<input type="hidden" id="sampleInputTemp_id" name="sampleInputTemp.sampleInfo.id" value="<s:property value="sampleInputTemp.sampleInfo.id"/>" />
						<input type="hidden" name="sampleInputTemp.id" value="<s:property value="sampleInputTemp.id"/>" />
						<input type="hidden"  id="upload_imga" name="sampleInputTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.sampleInfo.upLoadAccessory.fileName"/>">
					</td>
                   
                   	
	                   	<td class="label-title" >描述</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_name" name="sampleInputTemp.name" title="描述" value="<s:property value="sampleInputTemp.name"/>" />
	                   	</td>
	                   	
	                   <td class="label-title" >检测项目</td>
	                   <td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="hidden" size="20"   id="sampleInputTemp_productId" searchField="true" name="sampleInputTemp.productId"  value="<s:property value="sampleInputTemp.productId"/>" class="text input" />
 						<input type="text" id="sampleInputTemp_productName" name="sampleInputTemp.productName" value="<s:property value="sampleInputTemp.productName"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                    </tr>
					<tr>
							<td></td>
                    			<td></td>
                    			<td></td>
                    		<s:if test="sampleInputTemp.name != inputTemp.name">
		                    	<td class="label-title" id="move2">第二次录入人的描述</td>
		                    	<td id="move2"></td>
		                    	<td id="move2">
									<input type="text" size="20" maxlength="25" readonly="readonly" class="text input readonlytrue" id="inputTemp_name" name="inputTemp.name" title="第二次录入的描述" value="<s:property value="inputTemp.name"/>" />
								</td>
							</s:if>	
							<s:if test="sampleInputTemp.productName != inputTemp.productName">
		                    	<td class="label-title" id="move3">第二次录入人的项目</td>
		                    	<td id="move3"></td>
		                    	<td id="move3">
		                    		<input type="text" id="inputTemp_productName" name="inputTemp.productName" value="<s:property value="inputTemp.productName"/>" readonly="readonly" class="text input readonlytrue" >
		                    	</td>
		                    </s:if>
					</tr>
                   <tr>
	                    <td class="label-title" >送检医院</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_hospital" name="sampleInputTemp.hospital" title="送检医院" value="<s:property value="sampleInputTemp.hospital"/>" />
	                   	</td>
	                   	
	                   	
	                   	<td class="label-title" >门诊号</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_inHosNum" name="sampleInputTemp.inHosNum" title="门诊号" value="<s:property value="sampleInputTemp.inHosNum"/>"/>
	                   	</td>
	                   	
	                   	
	                    <td class="label-title" >抽血时间</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   	 	<input type="text" size="20" maxlength="25" id="sampleInputTemp_sendDate" name="sampleInputTemp.sendDate" title="抽血时间" Class="Wdate" readonly="readonly" value="<s:date name="sampleInputTemp.sendDate" format="yyyy-MM-dd"/>"
	                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"    />
	                   	</td>
                    </tr>
					<tr>
						<s:if test="sampleInputTemp.name != inputTemp.name">
		                   	<td class="label-title" id="move2">第二次录入人的送检医院</td>
		                   	<td id="move2"></td>
		                   	<td id="move2">
								<input type="text" size="20" maxlength="25" readonly="readonly" class="text input readonlytrue" id="inputTemp_hospital" name="inputTemp.hospital" title="第二次录入的描述" value="<s:property value="inputTemp.hospital"/>" />
							</td>
						</s:if>
						<s:if test="sampleInputTemp.inHosNum != inputTemp.inHosNum">
		                   	<td class="label-title" id="move2">第二次录入的门诊号</td>
		                   	<td id="move2"></td>
		                   	<td id="move2">
								<input type="text" readonly="readonly" class="text input readonlytrue" id="inputTemp_inHosNum" name="inputTemp.inHosNum" value="<s:property value="inputTemp.inHosNum"/>" />
							</td>
						</s:if>	
						<s:if test="sampleInputTemp.sendDate != inputTemp.sendDate">
		                   	<td class="label-title" id="move1">第二次录入的抽血时间</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<input type="text" id="inputTemp.sendDate" name="inputTemp.sendDate" value="<s:property value="inputTemp.sendDate"/>" readonly="readonly" class="text input readonlytrue" >
		                   	</td>
	                	</s:if>
					</tr>	             
                    <tr>                	
	                   	<td class="label-title" >样本类型</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
							<input type="text" size="20" readonly="readOnly"  id="sampleInputTemp_sampleType_name" name="sampleInputTemp.sampleType.name" value="<s:property value="sampleInputTemp.sampleType.name"/>" />
							<s:hidden id="sampleInputTemp_sampleType_id" name="sampleInputTemp.sampleType.id"></s:hidden>
							<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
	                   	</td>
	                   	
	                   	<td class="label-title" >孕妇姓名</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_patientName" name="sampleInputTemp.patientName" title="孕妇姓名" value="<s:property value="sampleInputTemp.patientName"/>"/>
	                   	</td>
	                   	
	<!--                    	<td class="label-title" >姓名拼音</td> -->
	<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
	<!--                    	<td align="left"  > -->
	<%--                    		<input type="text" size="20" maxlength="25" id="sampleInputTemp_patientNameSpell" name="sampleInputTemp.patientNameSpell" title="姓名拼音" data="patientNameSpell" value="<s:property value="sampleInputTemp.patientNameSpell"/>"/> --%>
	<!--               		</td> -->
	
						<td class="label-title" >年龄</td>
	               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
	                   	<td align="left"  >
	 						<input type="text" size="20" id="sampleInputTemp_age" name="sampleInputTemp.age" value="<s:property value="sampleInputTemp.age"/>" />
	                   	</td>
              		
                   </tr>
                   <tr>
                   		<s:if test="sampleInputTemp.sampleType.id != inputTemp.sampleType.id">
		                   	<td class="label-title" id="move1">第二次录入人的样本类型</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<input type="text" size="20" readonly="readOnly" class="text input readonlytrue" name="inputTemp.sampleType.name" id="inputTemp_sampleType_name" value="<s:property value="inputTemp.sampleType.name"/>" />
		                   	</td>
	                 	</s:if>
                   		<s:if test="sampleInputTemp.patientName != inputTemp.patientName">
		                   	<td class="label-title" id="move1">第二次录入的孕妇姓名</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<input type="text" size="20" readonly="readOnly" class="text input readonlytrue" name="inputTemp.patientName" id="inputTemp_patientName" value="<s:property value="inputTemp.patientName"/>" />
		                   	</td>
	                 	</s:if>
	                 	<s:if test="sampleInputTemp.age != inputTemp.age">
		                   	<td class="label-title" id="move3">第二次录入的年龄</td>
		                   	<td id="move3"></td>
		                   	<td id="move3">
		                   		<input type="text" id="inputTemp.age" name="inputTemp.age" value="<s:property value="inputTemp.age"/>" readonly="readonly" class="text input readonlytrue" >
		                   	</td>
	                   	</s:if>	
                   </tr>
                   <tr>	
                  
					
                   	<td class="label-title" >体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_weight" name="sampleInputTemp.weight" title="体重" value="<s:property value="sampleInputTemp.weight"/>"/>
                   	</td>
                   	
					<td class="label-title" >孕周</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_gestationalAge" name="sampleInputTemp.gestationalAge" title="孕周" value="<s:property value="sampleInputTemp.gestationalAge"/>"/>
                   	</td>
                   	
                   	<td class="label-title" >手机号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_phoneNum" name="sampleInputTemp.phoneNum" title="手机号" onblur="checkPhone()" value="<s:property value="sampleInputTemp.phoneNum"/>"/>
                   	</td>
                   	
              	</tr>
              	<tr>
              		<s:if test="sampleInputTemp.weight != inputTemp.weight">
		                   	<td class="label-title" id="move2">第二次录入的体重</td>
		                   	<td id="move2"></td>
		                   	<td id="move2">
								<input type="text" size="20" maxlength="25" readonly="readonly" class="text input readonlytrue" id="inputTemp_weight" name="inputTemp.weight" title="第二次录入的描述" value="<s:property value="inputTemp.weight"/>" />
							</td>
						</s:if>
						<s:if test="sampleInputTemp.gestationalAge != inputTemp.gestationalAge">
		                   	<td class="label-title" id="move3">第二次录入的孕周</td>
		                   	<td id="move3"></td>
		                   	<td id="move3">
		                   		<input type="text" id="inputTemp_gestationalAge" name="inputTemp.gestationalAge" value="<s:property value="inputTemp.gestationalAge"/>" readonly="readonly" class="text input readonlytrue" >
		                   	</td>
	                   </s:if>
						<s:if test="sampleInputTemp.phoneNum != inputTemp.phoneNum">
		                   	<td class="label-title" id="move3">第二次录入的手机号</td>
		                   	<td id="move3"></td>
		                   	<td id="move3">
		                   		<input type="text" id="inputTemp_phoneNum" name="inputTemp.phoneNum" value="<s:property value="inputTemp.phoneNum"/>" readonly="readonly" class="text input readonlytrue" >
		                   	</td>
	                   </s:if>
              	</tr>
              	<tr>
              		<td class="label-title" >家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_address" name="sampleInputTemp.address" title="家庭住址" value="<s:property value="sampleInputTemp.address"/>" />
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
              	</tr>
              	<tr>
              		<s:if test="sampleInputTemp.address != inputTemp.address">
		                   	<td class="label-title" id="move1">第二次录入的家庭地址</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<input type="text" size="20" readonly="readOnly" class="text input readonlytrue" name="inputTemp.address" id="inputTemp_address" value="<s:property value="inputTemp.address"/>" />
		                   	</td>
	                 	</s:if>
              	</tr>
			<tr>
					<td class="label-title" >末次月经</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleInputTemp_endMenstruationDate" name="sampleInputTemp.endMenstruationDate" title="末次月经" Class="Wdate" readonly="readonly" 
                   	 		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleInputTemp.endMenstruationDate" format="yyyy-MM-dd"/>"/>
                   	</td>
                   	
                   	
                   	 <td class="label-title" >孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_parturitionTime" name="sampleInputTemp.parturitionTime" title="孕产史" value="<s:property value="sampleInputTemp.parturitionTime"/>"/>
                   	</td>
                 </tr>
                 <tr>
                 	<s:if test="sampleInputTemp.endMenstruationDate != inputTemp.endMenstruationDate">
		                   	<td class="label-title" id="move1">第二次录入的末次月经</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<input type="text" size="20" readonly="readOnly" class="text input readonlytrue" name="inputTemp.endMenstruationDate" id="inputTemp_endMenstruationDate" value="<s:property value="inputTemp.endMenstruationDate"/>" />
		                   	</td>
	                 	</s:if>
	                 	<s:if test="sampleInputTemp.parturitionTime != inputTemp.parturitionTime">
		                   	<td class="label-title" id="move3">第二次录入的孕产史</td>
		                   	<td id="move3"></td>
		                   	<td id="move3">
		                   		<input type="text" id="inputTemp_parturitionTime" name="inputTemp.parturitionTime" value="<s:property value="inputTemp.parturitionTime"/>" readonly="readonly" class="text input readonlytrue" >
		                   	</td>
	                   </s:if>
                 </tr>
			     <tr>
			     <td class="label-title" >IVF妊娠</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.gestationIVF" id="sampleInputTemp_gestationIVF" >
    					<option value="0" <s:if test="sampleInputTemp.gestationIVF==0">selected="selected" </s:if>>否</option>
    					<option value="1" <s:if test="sampleInputTemp.gestationIVF==1">selected="selected" </s:if>>是</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_badMotherhood" name="sampleInputTemp.badMotherhood"  title="不良孕产史" value="<s:property value="sampleInputTemp.badMotherhood"/>" />
                   	</td>
			</tr>
			<tr>
				<s:if test="sampleInputTemp.gestationIVF != inputTemp.gestationIVF">
		                   	<td class="label-title" id="move1">第二次录入的IVF妊娠</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<select name="inputTemp.gestationIVF" id="inputTemp_gestationIVF">
									<option value=""<s:if test="inputTemp.gestationIVF==''">selected="selected" </s:if>>请选择</option>
			    					<option value="0"<s:if test="inputTemp.gestationIVF==0">selected="selected" </s:if>>是</option>
			    					<option value="1"<s:if test="inputTemp.gestationIVF==1">selected="selected" </s:if>>否</option>
								</select>
		                   	</td>
	                 	</s:if>
	                 	<s:if test="sampleInputTemp.badMotherhood != inputTemp.badMotherhood">
		                   	<td class="label-title" id="move2">第二次录入的不良孕产史</td>
		                   	<td id="move2"></td>
		                   	<td id="move2">
								<input type="text" readonly="readonly" class="text input readonlytrue" id="inputTemp_badMotherhood" name="inputTemp.badMotherhood" value="<s:property value="inputTemp.badMotherhood"/>" />
							</td>
						</s:if>
			</tr>
			<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>既往史</label>
						</div>
					</td>
			</tr>
			<tr>
					<td class="label-title" >器官移植</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.organGrafting" id="sampleInputTemp_organGrafting">
<!-- 						<option value="" <s:if test="sampleInputTemp.organGrafting==''">selected="selected" </s:if>>请选择</option> -->
    					<option value="0" <s:if test="sampleInputTemp.organGrafting==0">selected="selected" </s:if>>无</option>
    					<option value="1" <s:if test="sampleInputTemp.organGrafting==1">selected="selected" </s:if>>有</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
					<td class="label-title" >外源输血</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.outTransfusion" id="sampleInputTemp_outTransfusion" onchange="change1()">
<!-- 						<option value="" <s:if test="sampleInputTemp.outTransfusion==''">selected="selected" </s:if>>请选择</option> -->
    						<option value="0" <s:if test="sampleInputTemp.outTransfusion==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.outTransfusion==1">selected="selected" </s:if>>有</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >最后一次外源输血时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" id="last1" >
                   	 	<input type="text" size="20" maxlength="25" id="sampleInputTemp_firstTransfusionDate" name="sampleInputTemp.firstTransfusionDate" title="最后一次外源输血时间" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="sampleInputTemp.firstTransfusionDate" format="yyyy-MM-dd"/>" />
                   	</td>
                </tr>
                <tr>
                	<s:if test="sampleInputTemp.organGrafting != inputTemp.organGrafting">
		                   	<td class="label-title" id="move1">第二次录入的器官移植</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<select name="inputTemp.gestationIVF" id="inputTemp_gestationIVF">
		                   			<option value="0" <s:if test="inputTemp.organGrafting==0">selected="selected" </s:if>>无</option>
    								<option value="1" <s:if test="inputTemp.organGrafting==1">selected="selected" </s:if>>有</option>
								</select>
		                   	</td>
	                 	</s:if>
	                 	<s:if test="sampleInputTemp.outTransfusion != inputTemp.outTransfusion">
		                   	<td class="label-title" id="move1">第二次录入的外源输血</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<select name="inputTemp.outTransfusion" id="inputTemp_outTransfusion">
		                   			<option value="0" <s:if test="inputTemp.outTransfusion==0">selected="selected" </s:if>>无</option>
    								<option value="1" <s:if test="inputTemp.outTransfusion==1">selected="selected" </s:if>>有</option>
								</select>
		                   	</td>
	                 	</s:if>
	                 	<s:if test="sampleInputTemp.firstTransfusionDate != inputTemp.firstTransfusionDate">
		                   	<td class="label-title" id="move3">第二次录入的最后一次外源输血时间</td>
		                   	<td id="move3"></td>
		                   	<td id="move3">
		                   		<input type="text" id="inputTemp_firstTransfusionDate" name="inputTemp.firstTransfusionDate" value="<s:property value="inputTemp.firstTransfusionDate"/>" readonly="readonly" class="text input readonlytrue" >
		                   	</td>
	                   </s:if>	
                </tr>
                <tr>
                   	<td class="label-title" >干细胞治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.stemCellsCure" id="sampleInputTemp_stemCellsCure">
	    					<option value="0" <s:if test="sampleInputTemp.stemCellsCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.stemCellsCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >免疫治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.immuneCure" id="sampleInputTemp_immuneCure" onchange="change()">
	    					<option value="0" <s:if test="sampleInputTemp.immuneCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.immuneCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >最后一次免疫治疗时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" id="last" >
                   	 	<input type="text" size="20" maxlength="25" id="sampleInputTemp_endImmuneCureDate" name="sampleInputTemp.endImmuneCureDate" title="最后一次免疫治疗时间" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="sampleInputTemp.endImmuneCureDate" format="yyyy-MM-dd"/>"/>
                   	</td>
			</tr>
			<tr>
				<s:if test="sampleInputTemp.stemCellsCure != inputTemp.stemCellsCure">
                   	<td class="label-title" id="move1">第二次录入的干细胞治疗</td>
                   	<td id="move1"></td>
                   	<td id="move1">
                   		<select name="inputTemp.stemCellsCure" id="inputTemp_stemCellsCure">
							<option value=""<s:if test="inputTemp.stemCellsCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTemp.stemCellsCure==0">selected="selected" </s:if>>是</option>
	    					<option value="1"<s:if test="inputTemp.stemCellsCure==1">selected="selected" </s:if>>否</option>
						</select>
                   	</td>
	            </s:if>
	            <s:if test="sampleInputTemp.immuneCure != inputTemp.immuneCure">
                   	<td class="label-title" id="move2">第二次录入的免疫治疗</td>
                   	<td id="move2"></td>
                   	<td id="move2">
                   		<select name="inputTemp.immuneCure" id="inputTemp_immuneCure">
							<option value="" <s:if test="inputTemp.immuneCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTemp.immuneCure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="inputTemp.immuneCure==1">selected="selected" </s:if>>无</option>
						</select>
					</td>
				</s:if>     	
	            <s:if test="sampleInputTemp.endImmuneCureDate != inputTemp.endImmuneCureDate">
                   	<td class="label-title" id="move3">第二次录入的最后一次免疫治疗时间</td>
                   	<td id="move3"></td>
                   	<td id="move3">
                   		<input type="text" id="inputTemp_endImmuneCureDate" name="inputTemp.endImmuneCureDate" value="<s:property value="inputTemp.endImmuneCureDate"/>" readonly="readonly" class="text input readonlytrue" >
                   	</td>
	            </s:if>
			</tr>
			<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>辅助检查</label>
						</div>
					</td>
			</tr>

			<tr>
				<td class="label-title" >单/双/多胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.embryoType" id="sampleInputTemp_embryoType" onChange="change()">
	    					<option value="0" <s:if test="sampleInputTemp.embryoType==0">selected="selected" </s:if>>单胎</option>
	    					<option value="1" <s:if test="sampleInputTemp.embryoType==1">selected="selected" </s:if>>双胎</option>
	    					<option value="2" <s:if test="sampleInputTemp.embryoType==2">selected="selected" </s:if>>多胎</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_messages" name="sampleInputTemp.messages" title="异常提醒" value="<s:property value="sampleInputTemp.messages"/>" style="display:none"/>
                   	</td>
                   	
                   	<td class="label-title" >NT值</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_NT" name="sampleInputTemp.NT" title="NT值" value="<s:property value="sampleInputTemp.NT"/>"/>
                   	</td>
			</tr>
			<tr>
				<s:if test="sampleInputTemp.embryoType != inputTemp.embryoType">
                   	<td class="label-title" id="move1">第二次录入B超</td>
                   	<td id="move1"></td>
                   	<td id="move1">
                   		<select name="inputTemp.embryoType" id="inputTemp_embryoType">
							<option value="" <s:if test="inputTemp.embryoType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTemp.embryoType==0">selected="selected" </s:if>>单胎</option>
	    					<option value="1"<s:if test="inputTemp.embryoType==1">selected="selected" </s:if>>双胎</option>
	    					<option value="2" <s:if test="inputTemp.embryoType==2">selected="selected" </s:if>>多胎</option>
						</select>
                   	</td>
	            </s:if>
	            <s:if test="sampleInputTemp.NT != inputTemp.NT">
                   	<td class="label-title" id="move2">第二次录入的NT值</td>
                   	<td id="move2"></td>
                   	<td id="move2">
                   		<input type="text" id="inputTemp_NT" name="inputTemp.NT" value="<s:property value="inputTemp.NT"/>" />
					</td>
				</s:if>     	
			</tr>
			<tr>
                   	<td class="label-title" >筛查模式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.testPattern" id="sampleInputTemp_testPattern">
	    					<option value="2" <s:if test="sampleInputTemp.testPattern==2">selected="selected" </s:if>>早中孕期联合筛查</option>
	    					<option value="0" <s:if test="sampleInputTemp.testPattern==0">selected="selected" </s:if>>未做</option>
	    					<option value="1" <s:if test="sampleInputTemp.testPattern==1">selected="selected" </s:if>>早孕期筛查</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
					<td class="label-title" >21-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome21Value" name="sampleInputTemp.trisome21Value" title="21-三体比值" value="<s:property value="sampleInputTemp.trisome21Value"/>"/>
                   	</td>
					<td class="label-title" >18-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome18Value" name="sampleInputTemp.trisome18Value" title="18-三体比值" value="<s:property value="sampleInputTemp.trisome18Value"/>"/>
                   	</td>
			</tr>
			<tr>
				<s:if test="sampleInputTemp.testPattern != inputTemp.testPattern">
                   	<td class="label-title" id="move1">第二次录入的筛查模式</td>
                   	<td id="move1"></td>
                   	<td id="move1">
                   		<select name="inputTemp.testPattern" id="inputTemp_testPattern">
	    					<option value="0" <s:if test="inputTemp.testPattern==0">selected="selected" </s:if>>未做</option>
	    					<option value="1"<s:if test="inputTemp.testPattern==1">selected="selected" </s:if>>早孕期筛查</option>
	    					<option value="2"<s:if test="inputTemp.testPattern==2">selected="selected" </s:if>>早中孕期联合筛查</option>
						</select>
                   	</td>
	           </s:if>
	           <s:if test="sampleInputTemp.trisome21Value != inputTemp.trisome21Value">
                   	<td class="label-title" id="move2">第二次录入的21-三体比值</td>
                   	<td id="move2"></td>
                   	<td id="move2">
                   		<input type="text" class="text input readonlytrue" readonly="readonly" id="inputTemp_trisome21Value" name="inputTemp.trisome21Value" title="NT值" value="<s:property value="inputTemp.trisome21Value"/>" />
					</td>
				</s:if>	
					
				<s:if test="sampleInputTemp.trisome18Value != inputTemp.trisome18Value">
                   	<td class="label-title" id="move3">第二次录入的18-三体比值</td>
                   	<td id="move3"></td>
                   	<td id="move3">
                   		<input type="text" id="inputTemp_trisome18Value" name="inputTemp.trisome18Value" value="<s:property value="inputTemp.trisome18Value"/>" readonly="readonly" class="text input readonlytrue" >
                   	</td>
	            </s:if>
			</tr>
			<tr>
					<td class="label-title" >夫妻双方染色体</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.coupleChromosome" id="sampleInputTemp_coupleChromosome" >
	    					<option value="0" <s:if test="sampleInputTemp.coupleChromosome==0">selected="selected" </s:if>>未做</option>
	    					<option value="1" <s:if test="sampleInputTemp.coupleChromosome==1">selected="selected" </s:if>>正常</option>
	    					<option value="2" <s:if test="sampleInputTemp.coupleChromosome==2">selected="selected" </s:if>>异常</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >异常结果描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_reason2" name="sampleInputTemp.reason2" title="异常结果描述" value="<s:property value="sampleInputTemp.reason2"/>"/>
                   	</td>
               
                	<td class="label-title" >临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_diagnosis" name="sampleInputTemp.diagnosis" title="临床诊断" value="<s:property value="sampleInputTemp.diagnosis"/>"/>
                   	</td>
                </tr>
                <tr>
                	<s:if test="sampleInputTemp.coupleChromosome != inputTemp.coupleChromosome">
	                   	<td class="label-title" id="move2">第二次录入的夫妻双方染色体</td>
	                   	<td id="move2"></td>
	                   	<td id="move2">
	                   		<select name="inputTemp.coupleChromosome" id="inputTemp_coupleChromosome">
								<option value=""<s:if test="inputTemp.coupleChromosome==''">selected="selected" </s:if>>请选择</option>
		    					<option value="0"<s:if test="inputTemp.coupleChromosome==0">selected="selected" </s:if>>未做</option>
		    					<option value="1"<s:if test="inputTemp.coupleChromosome==1">selected="selected" </s:if>>正常</option>
		    					<option value="2"<s:if test="inputTemp.coupleChromosome==2">selected="selected" </s:if>>异常</option>
							</select>
						</td>
					</s:if>
					<s:if test="sampleInputTemp.reason2 != inputTemp.reason2">
	                   	<td class="label-title" id="move3">第二次录入的异常结果描述</td>
	                   	<td id="move3"></td>
	                   	<td id="move3">
	                   		<input type="text" id="inputTemp_reason2" name="inputTemp.reason2" value="<s:property value="inputTemp.reason2"/>" readonly="readonly" class="text input readonlytrue" >
	                   	</td>
	              	</s:if>
	              	
	              	<s:if test="sampleInputTemp.diagnosis != inputTemp.diagnosis">
		                   	<td class="label-title" id="move1">第二次录入的临床诊断</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<input type="text" class="text input readonlytrue" readonly="readonly" id="inputTemp_diagnosis" name="inputTemp.diagnosis" value="<s:property value="inputTemp.diagnosis"/>" />
		                   	</td>
	                 	</s:if>
                </tr>
                <tr>
                		<td class="label-title" >简要病史（家族史）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_medicalHistory" name="sampleInputTemp.medicalHistory" title="简要病史（家族史）" value="<s:property value="sampleInputTemp.medicalHistory"/>"/>
                   	</td>
              
               		<td class="label-title" >孕妇（签字）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_patientName" name="" title="孕妇（签字）" class="text input readonlytrue"readonly="readonly" value="<s:property value="sampleInputTemp.patientName"/>"/>
                   	</td>
                   	
                   	<td class="label-title" >孕妇证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInputTemp_voucherType_name" name="sampleInputTemp.voucherType.name" onblur="checkType()" value="<s:property value="sampleInputTemp.voucherType.name"/>"/>
 						<input type="hidden" id="sampleInputTemp_voucherType" name="sampleInputTemp.voucherType.id"  value="<s:property value="sampleInputTemp.voucherType.id"/>" > 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'/>                   		
                   	</td>
                </tr>
                <tr>
                	<s:if test="sampleInputTemp.medicalHistory != inputTemp.medicalHistory">
		                   	<td class="label-title" id="move3">第二次录入的简要病史（家族史）</td>
		                   	<td id="move3"></td>
		                   	<td id="move3">
		                   		<input type="text" id="inputTemp_medicalHistory" name="inputTemp.medicalHistory" value="<s:property value="inputTemp.medicalHistory"/>" readonly="readonly" class="text input readonlytrue" >
		                   	</td>
	                   </s:if>
	                   <s:if test="sampleInputTemp.voucherType.id != inputTemp.voucherType.id">
		                   	<td class="label-title" id="move1">第二次录入的证件类型</td>
		                   	<td id="move1"></td>
		                   	<td id="move1">
		                   		<input type="text" size="20" readonly="readOnly" class="text input readonlytrue" name="inputTemp.voucherType.name" id="inputTemp_voucherType_name" value="<s:property value="inputTemp.voucherType.name"/>" />
		                   	</td>
	                 	</s:if>
                </tr>
                <tr>
               	 	<td class="label-title" >送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_doctor"
                   	 	name="sampleInputTemp.doctor" title="送检医生" value="<s:property value="sampleInputTemp.doctor"/>" />
                   	</td>
                   	
                   	<td class="label-title" >孕妇证件号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_voucherCode" name="sampleInputTemp.voucherCode" title="孕妇证件号" onblur="checkFun()" value="<s:property value="sampleInputTemp.voucherCode"/>" />
                   	</td>
                   	
                   		<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTemp.nextStepFlow" id="sampleInputTemp_nextStepFlow">
							<option value="" <s:if test="sampleInputTemp.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleInputTemp.nextStepFlow==0">selected="selected" </s:if>>合格</option>
							<option value="1" <s:if test="sampleInputTemp.nextStepFlow==1">selected="selected" </s:if>>退费</option>
							<option value="2" <s:if test="sampleInputTemp.nextStepFlow==2">selected="selected" </s:if>>重抽血</option>
							<option value="3" <s:if test="sampleInputTemp.nextStepFlow==3">selected="selected" </s:if>>暂停</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
            </table>
<%--             <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleInputTemp.id"/>" /> --%>
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
