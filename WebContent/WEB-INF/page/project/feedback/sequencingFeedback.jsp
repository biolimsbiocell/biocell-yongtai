﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/project/feedback/sequencingFeedback.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="输入查询条件">
			POOLING编号
			</label>
			</td>
			<td>
			<input type="text"  name="poolingCode" id="sequencingFeedback_poolingCode" searchType ="6" searchField="true" >
			</td>
			
			<td >
			<label class="text label" title="输入查询条件">
			FC号
			</label>
			</td>
			<td>
			<input type="text"  name="fcCode" id="sequencingFeedback_fcCode" searchType ="6"  searchField="true" >
			</td>
			<td >
			
			<label class="text label" title="输入查询条件">
			处理意见
			</label>
			</td>
			<td>
			<select id="sequencingFeedback_method" name="method" searchType ="6" searchField="true" style="width:100">
                		<option value=" ">请选择</option>
    					<option value="0">重测序</option>
    					<option value="1">退费</option>
    					<option value="2">合格</option>
    					<option value="3">待反馈</option>
				</select>
			</td>
			
			<td>
			<input type="button" onClick="selectSequencingInfo()" value="查询">
			</td>
			
			</tr>
		</table>
		
		<div id="show_sequencingFeedback_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sequencingFeedback_tree_page"></div>
		<select id="isgood" style="display: none">
			<option value="0">合格</option>
			<option value="1">不合格</option>
		</select>
</body>
</html>



