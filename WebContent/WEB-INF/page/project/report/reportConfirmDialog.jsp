﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/report/reportConfirmDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportConfirm_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">姓名</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="reportConfirm_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">年龄</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportConfirm_age" searchField="true" name="age"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">性别</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportConfirm_sex" searchField="true" name="sex"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">联系人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportConfirm_linkMan" searchField="true" name="linkMan"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">联系人方式</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="reportConfirm_linkManWay" searchField="true" name="linkManWay"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本类型</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportConfirm_sampleType" searchField="true" name="sampleType"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_reportConfirm_div"></div>
   		
</body>
</html>



