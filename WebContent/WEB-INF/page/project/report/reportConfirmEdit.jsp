﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=reportConfirm&id=${reportConfirm.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/report/reportConfirmEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
            
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="reportConfirm_id"
                   	 name="reportConfirm.id" title="样本编号"
	value="<s:property value="reportConfirm.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="reportConfirm_name"
                   	 name="reportConfirm.name" title="姓名"
                   	   
	value="<s:property value="reportConfirm.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
               	 	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="reportConfirm_age_name" name="reportConfirm.age"  value="<s:property value="reportConfirm.age"/>" />
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportConfirm_sex"
                   	 name="reportConfirm.sex" title="性别"
                   	     
	value="<s:property value="reportConfirm.sex"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >联系人</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="reportConfirm_linkMan"
                   	 name="reportConfirm.linkMan" title="联系人"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="reportConfirm.linkMan"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >联系人方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="reportConfirm_linkManWay"
                   	 name="reportConfirm.linkManWay" title="联系人方式"
	value="<s:property value="reportConfirm.linkManWay"/>"
                   	  />
                   	  
                   	</td>
                   	
                   	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportConfirm_sampleType"
                   	 name="reportConfirm.sampleType" title="样本类型"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="reportConfirm.sampleType"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	
                   	<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="reportConfirm.id"/>" />
            </form>
            <div id="tabs">
            <ul>
           	</ul> 
			</div>
        	</div>
	</body>
	</html>
