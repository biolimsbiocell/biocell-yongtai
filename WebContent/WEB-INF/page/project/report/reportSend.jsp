﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/report/reportSend.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >报告编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="reportSend_reportId"
                   	 name="reportId" searchField="true" title="报告编号"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="reportSend_sampleId"
                   	 name="sampleId" searchField="true" title="样本编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showname" title="选择姓名"
				hasHtmlFrame="true"
				html="${ctx}/project/report/reportSend/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('reportSend_name').value=rec.get('id');
				document.getElementById('reportSend_name_name').value=rec.get('name');" />
               	 	<td class="label-title" >姓名</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="reportSend_name_name" searchField="true"  name="name.name"  value="" class="text input" />
 						<input type="hidden" id="reportSend_name" name="reportSend.name.id"  value="" > 
 						<img alt='选择姓名' id='showname' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >检测项目</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="reportSend_testItem"
                   	 name="testItem" searchField="true" title="检测项目"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >是否发送</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="reportSend_isSend"
                   	 name="isSend" searchField="true" title="是否发送"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >寄送地址</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="reportSend_sendAddress"
                   	 name="sendAddress" searchField="true" title="寄送地址"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >寄送人</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="reportSend_sender"
                   	 name="sender" searchField="true" title="寄送人"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >联系方式</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="reportSend_linkWay"
                   	 name="linkWay" searchField="true" title="联系方式"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showexpressNews" title="选择快递信息"
				hasHtmlFrame="true"
				html="${ctx}/project/report/reportSend/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('reportSend_expressNews').value=rec.get('id');
				document.getElementById('reportSend_expressNews_name').value=rec.get('name');" />
               	 	<td class="label-title" >快递信息</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="reportSend_expressNews_name" searchField="true"  name="expressNews.name"  value="" class="text input" />
 						<input type="hidden" id="reportSend_expressNews" name="reportSend.expressNews.id"  value="" > 
 						<img alt='选择快递信息' id='showexpressNews' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >发送日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startsendDate" name="startsendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="sendDate1" name="sendDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endsendDate" name="endsendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="sendDate2" name="sendDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="75" id="reportSend_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_reportSend_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_reportSend_tree_page"></div>
</body>
</html>



