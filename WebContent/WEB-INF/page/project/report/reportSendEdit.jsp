﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=reportSend&id=${reportSend.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/report/reportSendEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title">报告编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="reportSend_id"
                   	 name="reportSend.id" title="报告编号"
                   	   
	value="<s:property value="reportSend.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="reportSend_sampleId"
                   	 name="reportSend.sampleId" title="样本编号"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="reportSend.sampleId"/>"
                   	  />
                   	  
                   	</td>
			
			
			
<%-- 			<g:LayOutWinTag buttonId="showname" title="选择姓名" --%>
<%-- 				hasHtmlFrame="true" --%>
<%-- 				html="${ctx}/project/report/reportSend/stringSelect.action" --%>
<%-- 				isHasSubmit="false" functionName="StringFun"  --%>
<%--  				hasSetFun="true" --%>
<%-- 				extRec="rec" --%>
<%-- 				extStr="document.getElementById('reportSend_name').value=rec.get('id'); --%>
<%-- 				document.getElementById('reportSend_name_name').value=rec.get('name');" /> --%>
				
			
			
               	 	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"  name="reportSend.name" id="reportSend_name_name"  value="<s:property value="reportSend.name"/>"  readonly = "readOnly" class="text input readonlytrue" />
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportSend_testItem"
                   	 name="reportSend.testItem" title="检测项目"
                   	     readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="reportSend.testItem"/>"
                   	  />
                   	  
                   	</td>
			
<!-- 			readonly = "readOnly" class="text input readonlytrue" -->
               	 	<td class="label-title" >是否发送</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
<!--                    	<input type="text" size="20" maxlength="25" id="reportSend_isSend" -->
<!--                    	 name="reportSend.isSend" title="是否发送" -->
<!--                    	   readonly = "readOnly" class="text input readonlytrue"   -->
<%-- 	value="<s:property value="reportSend.isSend"/>" --%>
<!--                    	  /> -->
					<select name="reportSend.isSend">
						<option value="" <s:if test="reportSend.isSend==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="reportSend.isSend==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="reportSend.isSend==1">selected="selected" </s:if>>否</option>
					</select>
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >寄送地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportSend_sendAddress"
                   	 name="reportSend.sendAddress" title="寄送地址"
	value="<s:property value="reportSend.sendAddress"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >寄送人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportSend_sender"
                   	 name="reportSend.sender" title="寄送人"
	value="<s:property value="reportSend.sender"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="reportSend_linkWay"
                   	 name="reportSend.linkWay" title="联系方式"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="reportSend.linkWay"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			
               	 	<td class="label-title" >快递信息</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="reportSend_expressNews_name" name="reportSend.expressNews"  value="<s:property value="reportSend.expressNews"/>" class="text input readonlytrue" readonly="readOnly"  />
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >发送日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <input type="text" size="20" maxlength="" id="reportSend_sendDate"
                   	 name="reportSend.sendDate" title="创建日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="reportSend.sendDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="75" id="reportSend_note"
                   	 name="reportSend.note" title="备注"
	value="<s:property value="reportSend.note"/>"
                   	  />
                   	  
                   	</td>
                   	<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="reportSend.id"/>" />
            </form>
            <div id="tabs">
            <ul>
           	</ul> 
			</div>
        	</div>
	</body>
	</html>
