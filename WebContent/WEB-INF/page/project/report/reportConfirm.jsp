﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/report/reportConfirm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >样本编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="reportConfirm_id"
                   	 name="id" searchField="true" title="样本编号"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >姓名</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="reportConfirm_name"
                   	 name="name" searchField="true" title="姓名"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showage" title="选择年龄"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showageFun" 
				documentId="reportConfirm_age"
				documentName="reportConfirm_age_name"
			 />
               	 	<td class="label-title" >年龄</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="reportConfirm_age_name" searchField="true"  name="age.name"  value="" class="text input" />
 						<input type="hidden" id="reportConfirm_age" name="reportConfirm.age.id"  value="" > 
 						<img alt='选择年龄' id='showage' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >性别</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="reportConfirm_sex"
                   	 name="sex" searchField="true" title="性别"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >联系人</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="reportConfirm_linkMan"
                   	 name="linkMan" searchField="true" title="联系人"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >联系人方式</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="reportConfirm_linkManWay"
                   	 name="linkManWay" searchField="true" title="联系人方式"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >样本类型</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="reportConfirm_sampleType"
                   	 name="sampleType" searchField="true" title="样本类型"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_reportConfirm_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_reportConfirm_tree_page"></div>
</body>
</html>



