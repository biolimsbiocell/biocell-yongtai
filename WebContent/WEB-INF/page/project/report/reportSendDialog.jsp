﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/report/reportSendDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">报告编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_reportId" searchField="true" name="reportId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="reportSend_sampleId" searchField="true" name="sampleId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">姓名</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">检测项目</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_testItem" searchField="true" name="testItem"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">是否发送</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_isSend" searchField="true" name="isSend"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">寄送地址</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_sendAddress" searchField="true" name="sendAddress"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">寄送人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_sender" searchField="true" name="sender"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">联系方式</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="reportSend_linkWay" searchField="true" name="linkWay"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">快递信息</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_expressNews" searchField="true" name="expressNews"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">发送日期</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="reportSend_sendDate" searchField="true" name="sendDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="75" id="reportSend_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_reportSend_div"></div>
   		
</body>
</html>



