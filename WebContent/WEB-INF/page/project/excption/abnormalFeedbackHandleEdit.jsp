﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=abnormalFeedbackHandle&id=${abnormalFeedbackHandle.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/excption/abnormalFeedbackHandleEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			
			<tr>
					<td class="label-title"  >血浆编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="abnormalFeedbackHandle_id"
                   		 name="abnormalFeedbackHandle.id" title="血浆编号"
							value="<s:property value="abnormalFeedbackHandle.id"/>"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" >体积</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="abnormalFeedbackHandle_bluk"
                   	 name="abnormalFeedbackHandle.bluk" title="体积"
                   	   
	value="<s:property value="abnormalFeedbackHandle.bluk"/>"
                   	  />
                   	  
                   	</td>
			
			
			
						<g:LayOutWinTag buttonId="showresultDecide" title="选择结果判定"
				hasHtmlFrame="true"
				html="${ctx}/project/excpt/abnormalFeedbackHandle/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('abnormalFeedbackHandle_resultDecide').value=rec.get('id');
				document.getElementById('abnormalFeedbackHandle_resultDecide_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >结果判定</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="abnormalFeedbackHandle_resultDecide_name"  value="<s:property value="abnormalFeedbackHandle.resultDecide.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="abnormalFeedbackHandle_resultDecide" name="abnormalFeedbackHandle.resultDecide.id"  value="<s:property value="abnormalFeedbackHandle.resultDecide.id"/>" > 
 						<img alt='选择结果判定' id='showresultDecide' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			
			</tr>
			<tr>
			
			
			
			
               	 	<td class="label-title" >下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="abnormalFeedbackHandle_nextFlow"
                   	 name="abnormalFeedbackHandle.nextFlow" title="下一步流向"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="abnormalFeedbackHandle.nextFlow"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
               	 	<td class="label-title"  style="display:none"  >处理意见</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="abnormalFeedbackHandle_handleIdea"
                   	 name="abnormalFeedbackHandle.handleIdea" title="处理意见"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="abnormalFeedbackHandle.handleIdea"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="75" id="abnormalFeedbackHandle_note"
                   	 name="abnormalFeedbackHandle.note" title="备注"
	value="<s:property value="abnormalFeedbackHandle.note"/>"
                   	  />
                   	  
                   	</td>
                   	<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			</tr>
			<tr>
			
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="abnormalFeedbackHandle.id"/>" />
            </form>
        	</div>
	</body>
	</html>
