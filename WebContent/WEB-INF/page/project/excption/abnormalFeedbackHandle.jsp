﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/project/excption/abnormalFeedbackHandle.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  >血浆编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="abnormalFeedbackHandle_id"
                   	 name="id" searchField="true" title="血浆编号" />
                   	
					
 
                  
                   	
                   	  
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >体积</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="abnormalFeedbackHandle_bluk"
                   	 name="bluk" searchField="true" title="体积"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showresultDecide" title="选择结果判定"
				hasHtmlFrame="true"
				html="${ctx}/project/excpt/abnormalFeedbackHandle/stringSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('abnormalFeedbackHandle_resultDecide').value=rec.get('id');
				document.getElementById('abnormalFeedbackHandle_resultDecide_name').value=rec.get('name');" />
               	 	<td class="label-title" >结果判定</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="abnormalFeedbackHandle_resultDecide_name" searchField="true"  name="resultDecide.name"  value="" class="text input" />
 						<input type="hidden" id="abnormalFeedbackHandle_resultDecide" name="abnormalFeedbackHandle.resultDecide.id"  value="" > 
 						<img alt='选择结果判定' id='showresultDecide' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >下一步流向</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="abnormalFeedbackHandle_nextFlow"
                   	 name="nextFlow" searchField="true" title="下一步流向"    />
                   	
					
 
                  
                   	
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >处理意见</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="abnormalFeedbackHandle_handleIdea"
                   	 name="handleIdea" searchField="true" title="处理意见"   style="display:none"    />
                   	
					
 
                  
                   	
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="75" id="abnormalFeedbackHandle_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_abnormalFeedbackHandle_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_abnormalFeedbackHandle_tree_page"></div>
</body>
</html>



