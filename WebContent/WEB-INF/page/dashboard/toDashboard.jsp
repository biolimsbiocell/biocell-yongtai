<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>工作台</title>
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common0.jsp"%>
<link rel=stylesheet type="text/css" href="${ctx}/css/dashboard.css">
<script type="text/javascript" src="${ctx}/javascript/dashboard/toDashboard.js"></script>
</head>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
	 <div id="dashboard-container" >
	    <div id="dashboard-widgets" class="ui-layout-center">
	      <div id="dashboard-widgets-header">
	      	<button id="add-widget-btn"><span><fmt:message key="biolims.dashboard.addComponent"/></span></button>
	      	<button id="change-layout-btn"><span><fmt:message key="biolims.dashboard.modifyModel"/></span></button>
	      </div>
	      <div id="dashboard-widgets-content">
      		   <s:iterator value="#request.dashboardMap.list" id="layout">
				 <s:set name="j" value="0"></s:set>
				 <s:iterator value="#request.dashboardList" id="dashboard">
			 		<s:if test="#layout.column==#dashboard.layoutColumn">
			 			<s:set name="j" value="j+1"></s:set>
			 			<div class="col col-<s:property value='#layout.colWidth'/>" id="<s:property value='#dashboard.layoutColumn'/>Column">
				 			 <s:iterator value="#dashboard.children" id="modile">
						        <div id="dashboard-widget-<s:property value='#modile.id'/>" class="widget widget-model" dashboardId="<s:property value='#modile.id'/>" module-url="<s:property value='#modile.moduleUrl'/>" config-url="<s:property value='#modile.moduleConfUrl'/>">
						          <div class="widget-header ui-helper-reset ui-widget-header ui-corner-top">
						            <span class="title"><s:property value='#modile.moduleName'/></span>
						            <a href="#" class="setting"><span class="ui-icon ui-icon-gear">组件属性设置</span></a>
						            <a href="#" class="close"><span class="ui-icon ui-icon-close">移除组件</span></a>
						          </div>
						          <textarea id="<s:property value='#modile.id'/>Config" style="display:none;"><s:property value='#modile.config' default=""/></textarea>
						          <div class="widget-content ui-helper-reset ui-widget-content ui-corner-bottom" dashboardId="<s:property value='#modile.id' />" config-url="<s:property value='#modile.moduleConfUrl'/>" module-url="<s:property value='#modile.moduleUrl'/>" is-frame="<s:property value='#modile.isFrame'/>">
						            <div style="text-align:center;">
						            	<s:if test="#modile.isFrame==1">
						            		 <iframe  frameborder="0"></iframe>
						            	</s:if>
						            </div>
						          </div>
						        </div>
				 			 </s:iterator>
				 			 <div class="widget dummy"></div>
			 			 </div>
			 		</s:if>
				</s:iterator>
				<s:if test="#j==0">
			      <div class="col col-<s:property value='#layout.colWidth'/>" id="<s:property value='#layout.column'/>Column">
			      	<div class="widget dummy"></div>
			      </div>
			    </s:if>
			 </s:iterator>
		  </div>
	    </div>
	  </div>
</body>
</html>