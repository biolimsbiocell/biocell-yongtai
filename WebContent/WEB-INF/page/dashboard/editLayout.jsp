<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>工作台布局</title>

<body>
	<div id="main-content">
	<table width="100%">
		<tr>
			<td width="25%">
				<table width="80%" style="border-collapse:collapse;">
					<tr height="150px">
						<td style="width:30%;border:1px solid #66CCFF;text-align:center;"></td>
						<td style="width:50%;border:1px solid #66CCFF;text-align:center;"></td>
					</tr>
				</table>
			</td>
			<td width="25%">
				<table width="80%" style="border-collapse:collapse;">
					<tr height="150px">
						<td style="width:40%;border:1px solid #66CCFF;text-align:center;"></td>
						<td style="width:40%;border:1px solid #66CCFF;text-align:center;"></td>
					</tr>
				</table>
			</td>
			<td width="25%">
				<table width="80%" style="border-collapse:collapse;">
					<tr height="150px">
						<td style="width:50%;border:1px solid #66CCFF;text-align:center;"></td>
						<td style="width:30%;border:1px solid #66CCFF;text-align:center;"></td>
					</tr>
				</table>
			</td>
			<td width="25%">
				<table width="80%" style="border-collapse:collapse;">
					<tr height="150px">
						<td style="width:20%;border:1px solid #66CCFF;text-align:center;"></td>
						<td style="width:20%;border:1px solid #66CCFF;text-align:center;"></td>
						<td style="width:20%;border:1px solid #66CCFF;text-align:center;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="margin-top: 50px;">
		<s:iterator value="%{#request.layOutList}" id="layout" >
			<td style="text-align:center;">
				<input type="radio" name="layoutNameChk" 
					   value="<s:property value='#layout.id'/>" 
					<s:if test="#layout.id==#request.userLayoutId">checked="checked"</s:if>>
				<s:property value="#layout.layoutName"/>
			</td>
		</s:iterator>
		</tr>
	</table>
</div>
</body>
</html>