<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>指定角色设置选择组件</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common0.jsp"%>
<body>
	<div id="dashboard_model_div"></div>
	<s:hidden name="#request.roleId" id="role_id"> </s:hidden>
</body>
<script type="text/javascript" src="${ctx}/javascript/dashboard/toRoleDashboardModel.js"></script>
</html>