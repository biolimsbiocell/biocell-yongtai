<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="crmLinkMan_label" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=crmLinkMan&id=crmLinkMan.id"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/linkman/crmLinkManEdit.js"></script>
	<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title">联系人编码</td>
					<td align="left"><input type="text" size="20" maxlength="36"
						id="crmLinkMan_id" name="crmLinkMan.id" title="编码" readonly="readOnly" class="text input readonlytrue"
						class="text input" value="<s:property value="crmLinkMan.id"/>" />
					</td>
					<td class="label-title">姓名</td>
					<td align="left"><input type="text" size="20" maxlength="20"
						id="crmLinkMan_name" name="crmLinkMan.name" title="姓名"
						class="text input" value="<s:property value="crmLinkMan.name"/>" />
					</td>
					<g:LayOutWinTag buttonId="showsex" title="选择性别" hasHtmlFrame="true"
						html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
						functionName="xb" hasSetFun="true" documentId="crmLinkMan_sex"
						documentName="crmLinkMan_sex_name" />
					<td class="label-title">性别</td>
					<td align="left">
					<select name="crmLinkMan.sex" id="crmLinkMan_sex" style="width:80px">
					<option value="1" <s:if test="crmLinkMan.sex==1">selected="selected" </s:if>>男</option>
					<option value="2" <s:if test="crmLinkMan.sex==2">selected="selected" </s:if>>女</option>
					</select>
					</td>
				</tr>
				<tr>
					<%-- <g:LayOutWinTag buttonId="showcustomerId" title="选择客户来源"
						hasHtmlFrame="true"
						html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
						isHasSubmit="false" functionName="CrmCustomerFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmLinkMan_customerId').value=rec.get('id');
				document.getElementById('crmLinkMan_customerId_name').value=rec.get('name');
				document.getElementById('crmLinkMan_dutyUser').value=rec.get('dutyManId-id')
				document.getElementById('crmLinkMan_dutyUser_name').value=rec.get('dutyManId-name');" /> --%>

					 <td class="label-title">所属客户
					 <img class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left">
					<input type="text" size="30"
						readonly="readOnly" id="crmLinkMan_customerId_name"
						value="<s:property value="crmLinkMan.customerId.name"/>"
						class="text input" /> <input type="hidden"
						id="crmLinkMan_customerId" name="crmLinkMan.customerId.id"
						value="<s:property value="crmLinkMan.customerId.id"/>"> <img
						alt='选择客户来源' id='showcustomerId' onClick="showCustomer()"
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					<%-- 	<g:LayOutWinTag buttonId="showtrailId" title="选择线索来源"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/linkman/crmTrailSelect.action"
				isHasSubmit="false" functionName="CrmTrailFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmLinkMan_trailId').value=rec.get('id');
				document.getElementById('crmLinkMan_trailId_name').value=rec.get('name');" />
			
               	 	<td class="label-title">线索来源</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmLinkMan_trailId_name"  value="<s:property value="crmLinkMan.trailId.name"/>" class="text input"/>
 						<input type="hidden" id="crmLinkMan_trailId" name="crmLinkMan.trailId.id"  value="<s:property value="crmLinkMan.trailId.id"/>" > 
 						<img alt='选择线索来源' id='showtrailId' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
                   		<td class="label-title">主要联系人</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isZhuYaoLinkMan" name="crmLinkMan.isZhuYaoLinkMan" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isZhuYaoLinkMan==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isZhuYaoLinkMan==0">selected="selected"</s:if>>否</option>
					</select>
					<td class="label-title">手机</td>
					<td align="left"><input type="text" size="15" maxlength="20"
						id="crmLinkMan_mobile" name="crmLinkMan.mobile" title="手机"
						class="text input" value="<s:property value="crmLinkMan.mobile"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">传真</td>
					<td align="left"><input type="text" size="15" maxlength="20"
						id="crmLinkMan_fax" name="crmLinkMan.fax" title="传真"
						class="text input" value="<s:property value="crmLinkMan.fax"/>" />
					</td>
								 <td class="label-title">电话</td>
					<td align="left"><input type="text" size="10" maxlength="20"
						id="crmLinkMan_telephone" name="crmLinkMan.telephone" title="电话"
						class="text input"
						value="<s:property value="crmLinkMan.telephone"/>" />
					</td>
					<td class="label-title">email</td>
					<td align="left"><input type="text" size="40" maxlength="50"
						id="crmLinkMan_email" name="crmLinkMan.email" title="email"
						class="text input" value="<s:property value="crmLinkMan.email"/>" />
					</td>
					
				</tr>
				<tr>
					<%-- 	<g:LayOutWinTag buttonId="showmarketActId" title="选择市场活动来源"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/linkman/crmMarketActSelect.action"
				isHasSubmit="false" functionName="CrmMarketActFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmLinkMan_marketActId').value=rec.get('id');
				document.getElementById('crmLinkMan_marketActId_name').value=rec.get('name');" />
			
               	 	<td class="label-title">市场活动来源</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmLinkMan_marketActId_name"  value="<s:property value="crmLinkMan.marketActId.name"/>" class="text input"/>
 						<input type="hidden" id="crmLinkMan_marketActId" name="crmLinkMan.marketActId.id"  value="<s:property value="crmLinkMan.marketActId.id"/>" > 
 						<img alt='选择市场活动来源' id='showmarketActId' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
<td class="label-title">部门</td>
					<td align="left"><input type="text" size="10" maxlength="36"
						id="crmLinkMan_deptName" name="crmLinkMan.deptName" title="所属部门"
						class="text input"
						value="<s:property value="crmLinkMan.deptName"/>" />
					</td>
					<td class="label-title">职务</td>
					<td align="left"><input type="text" size="15" maxlength="50"
						id="crmLinkMan_job" name="crmLinkMan.job" title="职务"
						class="text input" value="<s:property value="crmLinkMan.job"/>" />
					</td>
					<g:LayOutWinTag buttonId="showstate" title="选择状态"
						hasHtmlFrame="true"
						html="${ctx}/dic/state/stateSelect.action?type=commonState"
						isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
						documentId="crmLinkMan_state" documentName="crmLinkMan_state_name" />
					<td class="label-title">状态<img class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="15"
						readonly="readOnly" id="crmLinkMan_state_name"
						value="<s:property value="crmLinkMan.state.name"/>"
						class="text input" /> <input type="hidden" id="crmLinkMan_state"
						name="crmLinkMan.state.id"
						value="<s:property value="crmLinkMan.state.id"/>"> <img
						alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
	 				<g:LayOutWinTag buttonId="showdutyUser" title="选择负责人"
						hasHtmlFrame="true" hasSetFun="true"
						width="document.body.clientWidth/1.5"
						html="${ctx}/core/user/userSelect.action" isHasSubmit="false"
						functionName="showdutyUserFun" documentId="crmLinkMan_dutyUser"
						documentName="crmLinkMan_dutyUser_name" /> 
				</tr>
				
				<tr>
					
					<td class="label-title">密码</td>
					<td align="left"><input type="text" size="10" maxlength="36"
						id="crmLinkMan_pwd" name="crmLinkMan.pwd" title="密码"
						class="text input"
						value="<s:property value="crmLinkMan.pwd"/>" />
					</td>
					<td class="label-title">负责人</td>
					<td align="left"><input type="text" size="10"
						readonly="readOnly" id="crmLinkMan_dutyUser_name"
						value="<s:property value="crmLinkMan.dutyUser.name"/>"
						class="text input readonlytrue" /> <input type="hidden"
						id="crmLinkMan_dutyUser" name="crmLinkMan.dutyUser.id"
						value="<s:property value="crmLinkMan.dutyUser.id"/>"></td>
					<td class="label-title">创建人</td>
					<td align="left"><input type="text" size="10"
						id="crmLinkMan_createUser_name"
						value="<s:property value="crmLinkMan.createUser.name"/>"
						readonly="readOnly" class="text input readonlytrue" /> <input
						type="hidden" id="crmLinkMan_createUser"
						name="crmLinkMan.createUser.id"
						value="<s:property value="crmLinkMan.createUser.id"/>"> <%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
 --%></td>
			</tr>	
				
				<!-- 	<tr>
               	 	<td class="label-title">请勿致电</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isTel" name="crmLinkMan.isTel" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isTel==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isTel==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isTel",
								width : 80,
								hiddenId : "crmLinkMan_isTel",
								hiddenName : "crmLinkMan.isTel"
							});
					});
 					</script>
                   	</td>
               	 	<td class="label-title">请勿传真</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isFax" name="crmLinkMan.isFax" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isFax==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isFax==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isFax",
								width : 80,
								hiddenId : "crmLinkMan_isFax",
								hiddenName : "crmLinkMan.isFax"
							});
					});
 					</script>
                   	</td>
               	 	<td class="label-title">请勿邮件</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isMail" name="crmLinkMan.isMail" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isMail==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isMail==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isMail",
								width : 80,
								hiddenId : "crmLinkMan_isMail",
								hiddenName : "crmLinkMan.isMail"
							});
					});
 					</script>
                   	</td>
				</tr> -->
				<tr>
					<%-- 	<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="crmLinkMan_createUser"
				documentName="crmLinkMan_createUser_name"
			 /> --%>

					<td class="label-title">创建日期</td>
					<td align="left"><input type="text" size="12" maxlength="50"
						id="crmLinkMan_createDate" name="crmLinkMan.createDate"
						title="创建日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmLinkMan.createDate" format="yyyy-MM-dd"/>" />
					</td>
					 
					<!-- 
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isPrivate",
								width : 80,
								hiddenId : "crmLinkMan_isPrivate",
								hiddenName : "crmLinkMan.isPrivate"
							});
					});
 					</script>
                   	</td> -->
				</tr>
				<%-- 		<tr>
		
			<g:LayOutWinTag buttonId="showdutyDept" title="选择负责部门" 
			hasHtmlFrame="true" html="${ctx}/core/department/departmentSelect.action"
			isHasSubmit="false" functionName="showdutyDeptFun" hasSetFun="true"
			documentId="crmLinkMan_dutyDept"
			documentName="crmLinkMan_dutyDept_name"  />
               	 	<td class="label-title">负责部门</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmLinkMan_dutyDept_name"  value="<s:property value="crmLinkMan.dutyDept.name"/>" class="text input"/>
 						<input type="hidden" id="crmLinkMan_dutyDept" name="crmLinkMan.dutyDept.id"  value="<s:property value="crmLinkMan.dutyDept.id"/>" > 
 						<img alt='选择负责部门' id='showdutyDept' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
		
				</tr> --%>
			</table>
			<input type="hidden" name="crmLinkManItemJson"
				id="crmLinkManItemJson" value="" /> <input type="hidden"
				id="id_parent_hidden" value="<s:property value="crmLinkMan.id"/>" />
		</form>
		<div id="tabs">
	<!-- 			<ul>
					<li><a href="#crmLinkManItempage">联系人明细</a></li>
				</ul> -->
			<div id="crmLinkManItempage" width="100%" height:10px></div>
		</div>
	</div>
</body>
</html>
