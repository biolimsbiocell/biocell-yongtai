﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/customer/linkman/crmLinkMan.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table>
			<tr>
               		<td class="label-title">联系人编码</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="36" searchField="true" id="crmLinkMan_id" name="id" title="编码" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">姓名</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="20" searchField="true" id="crmLinkMan_name" name="name" title="姓名" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			
			</tr>
			<tr>
			<%-- 	<g:LayOutWinTag buttonId="showtrailId" title="选择线索来源"
					hasHtmlFrame="true"
					html="${ctx}/crm/customer/linkman/showDialogCrmTrailList.action"
					isHasSubmit="false" functionName="CrmTrailFun" hasSetFun="true"
					documentId="crmLinkMan_trailId"
					documentName="crmLinkMan_trailId_name" />
	               		<td class="label-title">线索来源</td>
	                   	<td align="left">
	 						<input type="text" size="15" readonly="readOnly"  id="crmLinkMan_trailId_name"  value="" class="text input"/>
	 						<input type="hidden" id="crmLinkMan_trailId" name="trailId.id"  value="" searchField="true" > 
	 						<img alt='选择线索来源' id='showtrailId' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
	                   	</td> --%>
               		<td class="label-title">手机</td>
                   	<td align="left">
                   	<input type="text" size="15" maxlength="20" searchField="true" id="crmLinkMan_mobile" name="mobile" title="手机" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">email</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="50" searchField="true" id="crmLinkMan_email" name="email" title="email" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcustomerId" title="选择客户来源"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="CrmCustomerFun" hasSetFun="true"
				 extRec="rec"
		extStr="document.getElementById('crmLinkMan_customerId').value=rec.get('id');
		document.getElementById('crmLinkMan_customerId_name').value=rec.get('name');" />
		               		<td class="label-title">客户来源</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmLinkMan_customerId_name"  value="" class="text input"/>
 						<input type="hidden" id="crmLinkMan_customerId" name="customerId.id"  value="" searchField="true" > 
 						<img alt='选择客户来源' id='showcustomerId' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
               		<td class="label-title">电话</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="20" searchField="true" id="crmLinkMan_telephone" name="telephone" title="电话" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               					</tr>
			<tr>
			<%-- <g:LayOutWinTag buttonId="showmarketActId" title="选择市场活动来源"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/linkman/showDialogCrmMarketActList.action"
				isHasSubmit="false" functionName="CrmMarketActFun" hasSetFun="true"
				documentId="crmLinkMan_marketActId"
				documentName="crmLinkMan_marketActId_name" />
               		<td class="label-title">市场活动来源</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmLinkMan_marketActId_name"  value="" class="text input"/>
 						<input type="hidden" id="crmLinkMan_marketActId" name="marketActId.id"  value="" searchField="true" > 
 						<img alt='选择市场活动来源' id='showmarketActId' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
               		<td class="label-title">传真</td>
                   	<td align="left">
                   	<input type="text" size="15" maxlength="20" searchField="true" id="crmLinkMan_fax" name="fax" title="传真" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">职务</td>
                   	<td align="left">
                   	<input type="text" size="15" maxlength="50" searchField="true" id="crmLinkMan_job" name="job" title="职务" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			</tr>
			<!-- <tr>
               		<td class="label-title">请勿致电</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isTel" searchField="true" name="isTel" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isTel==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isTel==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isTel",
								width : 80,
								hiddenId : "crmLinkMan_isTel",
								hiddenName : "crmLinkMan.isTel"
							});
						});
 					</script>
                   	</td>
               		<td class="label-title">请勿传真</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isFax" searchField="true" name="isFax" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isFax==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isFax==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isFax",
								width : 80,
								hiddenId : "crmLinkMan_isFax",
								hiddenName : "crmLinkMan.isFax"
							});
						});
 					</script>
                   	</td>
               		<td class="label-title">请勿邮件</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isMail" searchField="true" name="isMail" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isMail==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isMail==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isMail",
								width : 80,
								hiddenId : "crmLinkMan_isMail",
								hiddenName : "crmLinkMan.isMail"
							});
						});
 					</script>
                   	</td>
			</tr> -->
			<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="crmLinkMan_createUser"
				documentName="crmLinkMan_createUser_name"
			 />
               		<td class="label-title">创建人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmLinkMan_createUser_name"  value="" class="text input"/>
 						<input type="hidden" id="crmLinkMan_createUser" name="createUser.id"  value="" searchField="true" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
                   	<g:LayOutWinTag buttonId="showstate" title="选择状态"
				hasHtmlFrame="true"
				html="${ctx}/dic/state/stateSelect.action?type=commonState"
				isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
				documentId="crmLinkMan_state"
				documentName="crmLinkMan_state_name" />
               		<td class="label-title">状态</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmLinkMan_state_name"  value="" class="text input"/>
 						<input type="hidden" id="crmLinkMan_state" name="state.id"  value="" searchField="true" > 
 						<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
               		<!-- <td class="label-title">创建日期</td>
                   	<td align="left">
                   	<input type="text" size="12" maxlength="50" searchField="true" id="crmLinkMan_createDate" name="createDate" title="创建日期" value="" 
                    	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	 />
                   	</td> -->
<!--                		<td class="label-title">是否私有</td>
                   	<td align="left">
					
					<select id="crmLinkMan_isPrivate" searchField="true" name="isPrivate" class="input-10-length">
								<option value="1" <s:if test="crmLinkMan.isPrivate==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmLinkMan.isPrivate==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "crmLinkMan_isPrivate",
								width : 80,
								hiddenId : "crmLinkMan_isPrivate",
								hiddenName : "crmLinkMan.isPrivate"
							});
						});
 					</script>
                   	</td> -->
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showdutyUser" title="选择负责人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showdutyUserFun" 
				documentId="crmLinkMan_dutyUser"
				documentName="crmLinkMan_dutyUser_name"
			 />
               		<td class="label-title">负责人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmLinkMan_dutyUser_name"  value="" class="text input"/>
 						<input type="hidden" id="crmLinkMan_dutyUser" name="dutyUser.id"  value="" searchField="true" > 
 						<img alt='选择负责人' id='showdutyUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showdutyDept" title="选择{po.filedComment}" 
			hasHtmlFrame="true" html="${ctx}/core/department/departmentSelect.action"
			isHasSubmit="false" functionName="showdutyDeptFun" hasSetFun="true"
			documentId="crmLinkMan_dutyDept"
			documentName="crmLinkMan_dutyDept_name"  />
               		<td class="label-title">负责部门</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmLinkMan_dutyDept_name"  value="" class="text input"/>
 						<input type="hidden" id="crmLinkMan_dutyDept" name="dutyDept.id"  value="" searchField="true" > 
 						<img alt='选择负责部门' id='showdutyDept' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			
			</tr>
			<%-- <tr>
			<g:LayOutWinTag buttonId="showsex" title="选择性别"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="yjlx" hasSetFun="true"
				documentId="crmLinkMan_sex"
				documentName="crmLinkMan_sex_name"
			/>
               		<td class="label-title">性别</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmLinkMan_sex_name"  value="" class="text input"/>
 						<input type="hidden" id="crmLinkMan_sex" name="sex.id"  value="" searchField="true" > 
 						<img alt='选择性别' id='showsex' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
                   	<td class="label-title">所属部门</td>
                   	<td align="left">
                   	<input type="text" size="10" maxlength="36" searchField="true" id="crmLinkMan_deptName" name="deptName" title="所属部门" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
                   	
			</tr> --%>
			<tr>
			
			
			
			</tr>
        </table>
		</form>
		</div>
		<div id="show_crmLinkMan_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
</body>
</html>



