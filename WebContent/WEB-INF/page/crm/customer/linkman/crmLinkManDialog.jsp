﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/customer/linkman/crmLinkManDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
	
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">姓名</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	
           	 	<!-- <td class="label-title">线索来源</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_trailId" searchField="true" name="trailId"  class="input-20-length"></input>
               	</td> -->
			</tr>
			<tr>
			<td class="label-title">性别</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmLinkMan_sex" searchField="true" name="sex"  class="input-20-length"></input>
               	</td>
           	 	<!-- <td class="label-title">手机</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmLinkMan_mobile" searchField="true" name="mobile"  class="input-20-length"></input>
               	</td>
           	 	 -->
           	 	 <td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmLinkMan_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<!-- <td class="label-title">所属部门</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_deptName" searchField="true" name="deptName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">市场活动来源</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_marketActId" searchField="true" name="marketActId"  class="input-20-length"></input>
               	</td> -->
               	<!-- <td class="label-title">email</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmLinkMan_email" searchField="true" name="email"  class="input-20-length"></input>
               	</td> -->
           	 	<!--  <td class="label-title">客户来源</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_customerId" searchField="true" name="customerId"  class="input-20-length"></input>
               	</td> -->
           	 	 
			</tr>
			<tr>
           	 	<!-- <td class="label-title">请勿致电</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmLinkMan_isTel" searchField="true" name="isTel"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">请勿传真</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmLinkMan_isFax" searchField="true" name="isFax"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">请勿邮件</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmLinkMan_isMail" searchField="true" name="isMail"  class="input-20-length"></input>
               	</td> -->
              <!--  	<td class="label-title">电话</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmLinkMan_telephone" searchField="true" name="telephone"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">传真</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmLinkMan_fax" searchField="true" name="fax"  class="input-20-length"></input>
               	</td> -->
           	 	
			</tr>
			<tr>
			<!-- <td class="label-title">职务</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmLinkMan_job" searchField="true" name="job"  class="input-20-length"></input>
               	</td> -->
           	 	
           	 	<!-- <td class="label-title">是否私有</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmLinkMan_isPrivate" searchField="true" name="isPrivate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="6" id="crmLinkMan_dutyUser" searchField="true" name="dutyUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">负责部门</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_dutyDept" searchField="true" name="dutyDept"  class="input-20-length"></input>
               	</td> -->
			</tr>
			<tr>
			<!-- <td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmLinkMan_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmLinkMan_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td -->
           	 	
			</tr>
        </table>
		</form>
		</div>
		
		
		<span onclick="sc()" ><font color="blue">搜索</font></span>
			<input type="hidden" id="custurmId" value="${requestScope.custurmId}"/>
		<div id="show_dialog_crmLinkMan_div"></div>
   		
</body>
</html>



