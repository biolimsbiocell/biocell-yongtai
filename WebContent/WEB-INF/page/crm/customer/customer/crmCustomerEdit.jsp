<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>

<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>
						<fmt:message key="biolims.common.hospitalInfo" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.common.customerID" />: <span id="crmCustomer_id"><s:property
									value="crmCustomer.id" /></span> <fmt:message
								key="biolims.common.commandPerson" />: <span
							userId="<s:property value="crmCustomer.createUser.id"/>"
							id="crmCustomer_createUser"><s:property
									value="crmCustomer.createUser.name" /></span> <fmt:message
								key="biolims.sample.createDate" />: <span
							id="crmCustomer_createDate"><s:date
									name="crmCustomer.createDate" format="yyyy-MM-dd" /></span> <fmt:message
								key="biolims.common.state" />: <span
							state="<s:property value="crmCustomer.state"/>"
							id="crmCustomer_state"><s:property
									value="crmCustomer.state.name" /></span>
						</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<%-- 	<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>客户管理</small>
								</span>
							</a></li>
										<li><a class="disabled step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>科研项目</small></span>
								</a></li>
										<li><a class="disabled step"> <span class="step_no">3</span>
											<span class="step_descr"> Step 3<br /> <small>检测个体</small></span>
								</a></li>
						</ul>
					</div> --%>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">医院名称</span> <input type="text"
											size="50" maxlength="50" id="crmCustomer_name" name="name"
											changelog="<s:property value="crmCustomer.name"/>"
											class="form-control"
											value="<s:property value="crmCustomer.name"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.district" /> </span> <input type="text"
											size="20" readonly="readOnly" id="crmCustomer_district_name"
											changelog="<s:property value="crmCustomer.district.name"/>"
											value="<s:property value="crmCustomer.district.name"/>"
											class="form-control" /> <input type="hidden"
											id="crmCustomer_district_id" name="district-id"
											value="<s:property value="crmCustomer.district.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showdistrict()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.type" /> </span> <input type="text"
											size="20" readonly="readOnly" id="crmCustomer_type_name"
											changelog="<s:property value="crmCustomer.type.name"/>"
											value="<s:property value="crmCustomer.type.name"/>"
											class="form-control" /> <input type="hidden"
											id="crmCustomer_type_id" name="type-id"
											value="<s:property value="crmCustomer.type.id"/>"> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showtype()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.street" /> </span> <input type="text"
											size="20" maxlength="25" id="crmCustomer_street"
											name="street"
											changelog="<s:property value="crmCustomer.street"/>"
											class="form-control"
											value="<s:property value="crmCustomer.street"/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.postcode" /> </span> <input type="text"
											size="20" maxlength="25" id="crmCustomer_postcode"
											name="postcode"
											changelog="<s:property value="crmCustomer.postcode"/>"
											class="form-control"
											value="<s:property value="crmCustomer.postcode"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.email" /> </span> <input type="text"
											size="20" maxlength="25" id="crmCustomer_email" name="email"
											changelog="<s:property value="crmCustomer.email"/>"
											class="form-control"
											value="<s:property value="crmCustomer.email"/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.yjly" /> </span> <input type="text"
											size="20" maxlength="25" id="crmCustomer_yjly" name="yjly"
											changelog="<s:property value="crmCustomer.yjly"/>"
											class="form-control"
											value="<s:property value="crmCustomer.yjly"/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">医院负责人</span> <input
											type="text" size="20" maxlength="25"
											id="crmCustomer_dutyManName" name="dutyManName"
											changelog="<s:property value="crmCustomer.dutyManName"/>"
											class="form-control"
											value="<s:property value="crmCustomer.dutyManName"/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">负责人联系电话</span> <input
											type="text" size="20" maxlength="25"
											id="crmCustomer_dutyManPhone" name="dutyManPhone"
											changelog="<s:property value="crmCustomer.dutyManPhone"/>"
											class="form-control"
											value="<s:property value="crmCustomer.dutyManPhone"/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">客户类型<img
											class='requiredimage' src='${ctx}/images/required.gif' /></span> <select
											lay-ignore class="form-control" id="crmCustomer_customerType"
											name="customerType">
											<option value="0"
												<s:if test="crmCustomer.customerType==0">selected="selected"</s:if>>渠道客户</option>
											<option value="1"
												<s:if test="crmCustomer.customerType==1">selected="selected"</s:if>>直接用户</option>
										</select>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.state" /> </span> <input type="text"
											size="20" readonly="readOnly" id="crmCustomer_state_name"
											changelog="<s:property value="crmCustomer.state.name"/>"
											value="<s:property value="crmCustomer.state.name"/>"
											class="form-control" /> <input type="hidden"
											id="crmCustomer_state_id" name="state-id"
											value="<s:property value="crmCustomer.state.id"/>"> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showstate()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.crmPhone" /> </span> <input type="text" size="20"
											maxlength="25" id="crmCustomer_crm_phone" name="crmPhone"
											changelog="<s:property value="crmCustomer.crmPhone"/>"
											class="form-control"
											value="<s:property value="crmCustomer.crmPhone"/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<%-- 	<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.crmCustomer.createUser"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="crmCustomer_createUser_name"  changelog="<s:property value="crmCustomer.createUser.name"/>"
												value="<s:property value="crmCustomer.createUser.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="crmCustomer_createUser_id" name="createUser-id"
												value="<s:property value="crmCustomer.createUser.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showcreateUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>	 --%>
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.crmCustomer.createDate"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="crmCustomer_createDate"
											name="createDate"  changelog="<s:property value="crmCustomer.createDate.name"/>"
											  class="form-control"   
                   	  value="<s:date name="createDate" format="yyyy-MM-dd"/>"
                   	   						 />  
					
									</div>
								</div>	 --%>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">本公司负责人</span> <input
											type="text" size="20" readonly="readOnly"
											id="crmCustomer_dutyManId_name"
											changelog="<s:property value="crmCustomer.dutyManId.name"/>"
											value="<s:property value="crmCustomer.dutyManId.name"/>"
											class="form-control" /> <input type="hidden"
											id="crmCustomer_dutyManId_id" name="dutyManId-id"
											value="<s:property value="crmCustomer.dutyManId.id"/>">
										<input type="hidden" id="crmCustomer_id" name="id"
											value="<s:property value="crmCustomer.id"/>"> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showdutyManId()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">负责人联系电话</span> <input
											type="text" size="20" maxlength="25" id="crmCustomer_ks"
											name="ks" changelog="<s:property value="crmCustomer.ks"/>"
											class="form-control"
											value="<s:property value="crmCustomer.ks"/>" />

									</div>
								</div>
							</div>
							<%-- 							<div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> <fmt:message key="biolims.common.customFields"/>
													
											</h3>
										</div>
									</div>
								</div> --%>
							<div id="fieldItemDiv"></div>

							<br> <input type="hidden" name="projectJson"
								id="projectJson" value="" /> <input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="crmCustomer.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="crmCustomer.fieldContent"/>" />

						</form>
					</div>
					<!--table表格-->
					<div class="HideShowPanel" style="display: none">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="projectTable" style="font-size: 14px;">
						</table>
					</div>
					<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;" id="rqyb">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="crmCustomerIteam" style="font-size: 14px;"></table>
				</div>
				</div>
				<!-- 				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div>
 -->
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/customer/project.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/customer/crmCustomerEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/customer/crmCustomerIteam.js"></script> 
</body>

</html>
