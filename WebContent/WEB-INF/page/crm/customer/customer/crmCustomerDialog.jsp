﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/customer/customer/crmCustomerDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<script>

function setPValue(rec){
           window.parent.set<%=request.getParameter("flag")%>(rec);
}

</script>
<g:LayOutWinTag buttonId="showdutyManId" title="选择负责人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showdutyManIdFun" 
				documentId="crmCustomer_dutyManId"
				documentName="crmCustomer_dutyManId_name"
			 /> 
			 	<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="crmCustomer_createUser"
				documentName="crmCustomer_createUser_name"
			 />
			 <g:LayOutWinTag buttonId="showstate" title="选择状态"
				hasHtmlFrame="true"
				html="${ctx}/dic/state/stateSelect.action?type=commonState"
				isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
				documentId="crmCustomer_state"
				documentName="crmCustomer_state_name" />
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.sample.customerId"/></td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.linkmanName"/></td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_linkManId" searchField="true" name="linkManId"  class="input-20-length"></input>
               	</td>
               	</tr>
               	<tr>
           	 	<!-- <td class="label-title">负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_dutyManId" searchField="true" name="dutyManId"  class="input-20-length"></input>
               	</td> -->
               	<td class="label-title"><fmt:message key="biolims.master.personName"/></td>
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="crmCustomer_dutyManId_name"  value="" class="text input"/>
 						<input type="hidden" id="crmCustomer_dutyManId" name="dutyManId.id"  value="" searchField="true" > 
 						<img alt='选择负责人' id='showdutyManId' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
           	 	<td class="label-title"><fmt:message key="biolims.crm.dutyDeptName"/></td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_dutyDeptId" searchField="true" name="dutyDeptId"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.user.eduName"/></td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmCustomer_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<!-- <td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td> -->
               	<td class="label-title"><fmt:message key="biolims.sample.createUserName"/></td>
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="crmCustomer_createUser_name"  value="" class="text input"/>
 						<input type="hidden" id="crmCustomer_createUser" name="createUser.id"  value="" searchField="true" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.state"/></td>
               	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="crmCustomer_state_name"  value="" class="text input"/>
 						<input type="hidden" id="crmCustomer_state" name="state.id"  value="" searchField="true" > 
 						<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
           	 	
               	<!-- <td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td> -->
               	 <td class="label-title"><fmt:message key="biolims.storage.studyType"/>
					<td align="left"><input type="text" size="15"
						readonly="readOnly" id="crmCustomer_type_name"
						value=""
						class="text input" /> <input type="hidden" id="crmCustomer_type"
						name="type.id" searchField="true" 
						value=""> <img
						alt='选择分类' onClick="showCustomerFun()" src='${ctx}/images/img_lookup.gif'
						class='detail' /></td> 
					
               	</tr>
               	<!-- <tr>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" size="20" maxlength="50" searchField="true" id="crmCustomer_createDate" 
                   	name="createDate" title="创建日期" value="" 
                    	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
               	</td> -->
           	 	<!-- <td class="label-title">注册资本</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmCustomer_capital" searchField="true" name="capital"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">币种</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_currencyTypeId" searchField="true" name="currencyTypeId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">机构代码</td>
               	<td align="left">
                    		<input type="text" maxlength="40" id="crmCustomer_orgCode" searchField="true" name="orgCode"  class="input-20-length"></input>
               	</td>
               	</tr>
               	<tr>
           	 	<td class="label-title">行业</td>
               	<td align="left">
                    		<input type="text" maxlength="100" id="crmCustomer_industry" searchField="true" name="industry"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">企业性质</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmCustomer_kind" searchField="true" name="kind"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">资质1</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmCustomer_aptitude1" searchField="true" name="aptitude1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">资质2</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmCustomer_aptitude2" searchField="true" name="aptitude2"  class="input-20-length"></input>
               	</td>
               	</tr>
               	<tr>
           	 	<td class="label-title">资质3</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmCustomer_aptitude3" searchField="true" name="aptitude3"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">开户行</td>
               	<td align="left">
                    		<input type="text" maxlength="100" id="crmCustomer_bank" searchField="true" name="bank"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">账号</td>
               	<td align="left">
                    		<input type="text" maxlength="40" id="crmCustomer_account" searchField="true" name="account"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">银行代码</td>
               	<td align="left">
                    		<input type="text" maxlength="40" id="crmCustomer_bankCode" searchField="true" name="bankCode"  class="input-20-length"></input>
               	</td>
			</tr> -->
        </table>
		</form>
		</div>
		<span onclick="sc()" ><input type="button" value=<fmt:message key="biolims.common.search"/> size="10"/></span>
		<span>   </span>
		<span onclick="nd()" ><input type="button" value=<fmt:message key="biolims.common.create"/> size="10"/></span>
		
		<div id="show_dialog_crmCustomer_div"></div>
   		
</body>
</html>



