﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/customer/patient/crmPatient.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		
		
		<table class="frame-table">

				<tr>
					<td class="label-title"><fmt:message key="biolims.common.electronicCaseNumber"/>
					</td>
					<td align="left"><input type="text"  searchField="true" size="10" maxlength="36"
						id="crmPatient_id" name="id" title="<fmt:message key="biolims.common.patientID"/>"
						class="text input" value="<s:property value="crmPatient.id"/>" />
					</td>
					

					
				</tr>
				<tr>
					<td class="label-title" id="xing4" ><fmt:message key="biolims.common.sname"/></td>
					<td align="left" id="xing5" ><input type="text"  searchField="true" size="50" maxlength="20"
						style="height: 20px; width: 80px" id="crmPatient_name"
						name="name" title="<fmt:message key="biolims.common.sname"/>" class="text input"
						 value="<s:property value="crmPatient.name"/>" />
					</td>
					<td class="label-title" id="xing6" ><fmt:message key="biolims.common.iDcard"/></td>
					<td align="left" id="xing7" ><input type="text"  searchField="true" size="15" maxlength="200"
						style="height: 20px; width: 160px" id="crmPatient_sfz" onBlur="sfzVerify()"
						name="sfz" title="<fmt:message key="biolims.common.iDcard"/>" class="text input"
						value="<s:property value="crmPatient.sfz"/>" /></td>
					<!--  -->
					
					<td class="label-title"><span id="dq" style="display:none"><fmt:message key="biolims.common.currentAge"/></span><span id="sw" style="display:none"><fmt:message key="biolims.common.dateOfDeath"/></span></td>
					<td align="left" id="dq1" style="display:none"><input type="text"  searchField="true" size="12" maxlength="60"
						id="dqAge" name="dqAge"
						title="<fmt:message key="biolims.common.currentAge"/>" readonly="readOnly" class="text input readonlytrue"
						 />
					</td>
					<td align="left" id="sw1" style="display:none"><input type="text"  searchField="true" size="12" maxlength="60"
						id="crmPatient_deathDate" name="deathDate"
						title="<fmt:message key="biolims.common.dateOfDeath"/>" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="deathDate" format="yyyy-MM-dd"/>" />
					</td>
				</tr>
				<tr>
				<td class="label-title" id="xing8" ><fmt:message key="biolims.common.dateOfBirth"/></td>
					<td align="left" id="xing9" ><input type="text"  searchField="true" size="12" maxlength="100"
						id="crmPatient_dateOfBirth" name="dateOfBirth"
						title="<fmt:message key="biolims.common.dateOfBirth"/>" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="dateOfBirth" format="yyyy-MM-dd"/>" />
					</td>
					<td class="label-title" id="xing10" ><fmt:message key="biolims.common.inputtingTheAge"/></td>
					<td align="left" id="xing11"><input type="text"  searchField="true" size="15" maxlength="100"
						id="crmPatient_age" style="height: 20px; width: 80px"
						name="age" title="<fmt:message key="biolims.common.inputtingTheAge"/>" class="text input readonlyfalse"
						 value="<s:property value="crmPatient.age"/>" />
					</td>
					
					<td class="label-title" id="xing12"><fmt:message key="biolims.common.national"/></td>
					<td align="left" id="xing13"><select id="crmPatient_race" searchField="true" 
						name="race" class="input-10-length">
							<option value=""
								<s:if test="crmPatient.race==''">selected="selected"</s:if>>--<fmt:message key="biolims.common.pleaseSelect"/>--</option>
							<option value="0"
								<s:if test="crmPatient.race==0">selected="selected"</s:if>><fmt:message key="biolims.common.han"/>
							</option>
							<option value="1"
								<s:if test="crmPatient.race==1">selected="selected"</s:if>><fmt:message key="biolims.common.zhuang"/>
							</option>
							<option value="2"
								<s:if test="crmPatient.race==2">selected="selected"</s:if>><fmt:message key="biolims.common.manchu"/></option>
							<option value="3"
								<s:if test="crmPatient.race==3">selected="selected"</s:if>><fmt:message key="biolims.common.hui"/></option>
							<option value="4"
								<s:if test="crmPatient.race==4">selected="selected"</s:if>><fmt:message key="biolims.common.miao"/></option>
							<option value="5"
								<s:if test="crmPatient.race==5">selected="selected"</s:if>><fmt:message key="biolims.common.uyghur"/>
							</option>
							<option value="6"
								<s:if test="crmPatient.race==6">selected="selected"</s:if>><fmt:message key="biolims.common.tujia"/>
							</option>
							<option value="7"
								<s:if test="crmPatient.race==7">selected="selected"</s:if>><fmt:message key="biolims.common.yi"/>
							</option>
							<option value="8"
								<s:if test="crmPatient.race==8">selected="selected"</s:if>><fmt:message key="biolims.common.mongol"/></option>
							<option value="9"
								<s:if test="crmPatient.race==9">selected="selected"</s:if>><fmt:message key="biolims.common.tibetan"/>
							</option>
							<option value="10"
								<s:if test="crmPatient.race==10">selected="selected"</s:if>><fmt:message key="biolims.common.buyei"/>
							</option>
							<option value="11"
								<s:if test="crmPatient.race==11">selected="selected"</s:if>><fmt:message key="biolims.common.dong"/>
							</option>
							<option value="12"
								<s:if test="crmPatient.race==12">selected="selected"</s:if>><fmt:message key="biolims.common.yao"/></option>
							<option value="13"
								<s:if test="crmPatient.race==13">selected="selected"</s:if>><fmt:message key="biolims.common.korean"/></option>
							<option value="14"
								<s:if test="crmPatient.race==14">selected="selected"</s:if>><fmt:message key="biolims.common.bai"/>
							</option>
							<option value="15"
								<s:if test="crmPatient.race==15">selected="selected"</s:if>><fmt:message key="biolims.common.hani"/></option>
							<option value="16"
								<s:if test="crmPatient.race==16">selected="selected"</s:if>><fmt:message key="biolims.common.kazak"/></option>
							<option value="17"
								<s:if test="crmPatient.race==17">selected="selected"</s:if>><fmt:message key="biolims.common.li"/></option>
							<option value="18"
								<s:if test="crmPatient.race==18">selected="selected"</s:if>><fmt:message key="biolims.common.dai"/></option>
							<option value="19"
								<s:if test="crmPatient.race==19">selected="selected"</s:if>><fmt:message key="biolims.common.she"/></option>
							<option value="20"
								<s:if test="crmPatient.race==20">selected="selected"</s:if>><fmt:message key="biolims.common.lisu"/></option>
							<option value="21"
								<s:if test="crmPatient.race==21">selected="selected"</s:if>><fmt:message key="biolims.common.gelao"/></option>
							<option value="22"
								<s:if test="crmPatient.race==22">selected="selected"</s:if>><fmt:message key="biolims.common.dongxiang"/></option>
							<option value="23"
								<s:if test="crmPatient.race==23">selected="selected"</s:if>><fmt:message key="biolims.common.gaoShan"/></option>

							<option value="24"
								<s:if test="crmPatient.race==24">selected="selected"</s:if>><fmt:message key="biolims.common.lahu"/></option>
							<option value="25"
								<s:if test="crmPatient.race==25">selected="selected"</s:if>><fmt:message key="biolims.common.sui"/></option>
							<option value="26"
								<s:if test="crmPatient.race==26">selected="selected"</s:if>><fmt:message key="biolims.common.va"/>
							</option>
							<option value="27"
								<s:if test="crmPatient.race==27">selected="selected"</s:if>><fmt:message key="biolims.common.naxi"/>
							</option>
							<option value="28"
								<s:if test="crmPatient.race==28">selected="selected"</s:if>><fmt:message key="biolims.common.qiang"/>
							</option>
							<option value="29"
								<s:if test="crmPatient.race==29">selected="selected"</s:if>><fmt:message key="biolims.common.tu"/></option>
							<option value="30"
								<s:if test="crmPatient.race==30">selected="selected"</s:if>><fmt:message key="biolims.common.mulao"/>
							</option>
							<option value="31"
								<s:if test="crmPatient.race==31">selected="selected"</s:if>><fmt:message key="biolims.common.xibe"/></option>
							<option value="32"
								<s:if test="crmPatient.race==32">selected="selected"</s:if>><fmt:message key="biolims.common.kirgiz"/></option>
							<option value="33"
								<s:if test="crmPatient.race==33">selected="selected"</s:if>><fmt:message key="biolims.common.daur"/>
							</option>
							<option value="34"
								<s:if test="crmPatient.race==34">selected="selected"</s:if>><fmt:message key="biolims.common.jingpo"/></option>
							<option value="35"
								<s:if test="crmPatient.race==35">selected="selected"</s:if>><fmt:message key="biolims.common.maonan"/></option>
							<option value="36"
								<s:if test="crmPatient.race==36">selected="selected"</s:if>><fmt:message key="biolims.common.salar"/></option>
							<option value="37"
								<s:if test="crmPatient.race==37">selected="selected"</s:if>><fmt:message key="biolims.common.blang"/></option>
							<option value="38"
								<s:if test="crmPatient.race==38">selected="selected"</s:if>><fmt:message key="biolims.common.tajik"/></option>
							<option value="39"
								<s:if test="crmPatient.race==39">selected="selected"</s:if>><fmt:message key="biolims.common.achang"/></option>

							<option value="40"
								<s:if test="crmPatient.race==40">selected="selected"</s:if>><fmt:message key="biolims.common.pumi"/></option>
							<option value="41"
								<s:if test="crmPatient.race==41">selected="selected"</s:if>><fmt:message key="biolims.common.ewenki"/></option>

							<option value="42"
								<s:if test="crmPatient.race==42">selected="selected"</s:if>><fmt:message key="biolims.common.nu"/></option>

							<option value="43"
								<s:if test="crmPatient.race==43">selected="selected"</s:if>><fmt:message key="biolims.common.gin"/></option>
							<option value="44"
								<s:if test="crmPatient.race==44">selected="selected"</s:if>><fmt:message key="biolims.common.jino"/></option>
							<option value="45"
								<s:if test="crmPatient.race==45">selected="selected"</s:if>><fmt:message key="biolims.common.de'ang"/></option>

							<option value="46"
								<s:if test="crmPatient.race==46">selected="selected"</s:if>><fmt:message key="biolims.common.bonan"/></option>
							<option value="47"
								<s:if test="crmPatient.race==47">selected="selected"</s:if>><fmt:message key="biolims.common.russians"/></option>
							<option value="48"
								<s:if test="crmPatient.race==48">selected="selected"</s:if>><fmt:message key="biolims.common.yugur"/></option>

							<option value="49"
								<s:if test="crmPatient.race==49">selected="selected"</s:if>><fmt:message key="biolims.common.uzbek"/>
							</option>
							<option value="50"
								<s:if test="crmPatient.race==50">selected="selected"</s:if>><fmt:message key="biolims.common.monba"/></option>
							<option value="51"
								<s:if test="crmPatient.race==51">selected="selected"</s:if>><fmt:message key="biolims.common.oroqen"/></option>

							<option value="52"
								<s:if test="crmPatient.race==52">selected="selected"</s:if>><fmt:message key="biolims.common.derung"/></option>
							<option value="53"
								<s:if test="crmPatient.race==53">selected="selected"</s:if>><fmt:message key="biolims.common.tatar"/></option>
							<option value="54"
								<s:if test="crmPatient.race==54">selected="selected"</s:if>><fmt:message key="biolims.common.hezhen"/></option>

							<option value="55"
								<s:if test="crmPatient.race==55">selected="selected"</s:if>><fmt:message key="biolims.common.lhoba"/>
							</option>





					</select></td>
				</tr>

				<tr>
					<td class="label-title" id="xing14"><fmt:message key="biolims.common.gender"/></td>
					<td align="left" id="xing15"><select id="crmPatient_gender" searchField="true" 
						name="gender" class="input-10-length">
							<option value=""
								<s:if test="crmPatient.gender==''">selected="selected"</s:if>>--<fmt:message key="biolims.common.pleaseSelect"/>--</option>
							<option value="1"
								<s:if test="crmPatient.gender==1">selected="selected"</s:if>><fmt:message key="biolims.common.male"/></option>
							<option value="0"
								<s:if test="crmPatient.gender==0">selected="selected"</s:if>><fmt:message key="biolims.common.female"/></option>
					</select>
					<td class="label-title" id="xing28" ><fmt:message key="biolims.common.street"/></td>
					<td align="left" id="xing29" ><input type="text"  searchField="true" size="15" maxlength="200"
						style="height: 20px; width: 160px" id="crmPatient_address"
						name="address" title="<fmt:message key="biolims.common.homeAddress"/>" class="text input"
						value="<s:property value="crmPatient.address"/>" /></td>
				</tr>
				<tr>
				<td class="label-title"><fmt:message key="biolims.common.nativePlace"/></td>
					<td align="left"><input type="text"  searchField="true" size="15" maxlength="200"
						style="height: 20px; width: 160px" id="crmPatient_placeOfBirth"
						name="placeOfBirth" title="<fmt:message key="biolims.common.nativePlace"/>" class="text input"
						value="<s:property value="crmPatient.placeOfBirth"/>" /></td>
					<td class="label-title"><fmt:message key="biolims.common.adultChild"/></td>
					<td align="left"><input type="text"  searchField="true" id="crmPatient_adultTeenager"
						name="adultTeenager"  class="text input"
						 value="<s:property value="crmPatient.adultTeenager"/>" />
					</td>
					<td class="label-title"><fmt:message key="biolims.common.maritalStatus"/></td>
					<td align="left"><select id="crmPatient_maritalStatus" searchField="true" 
						name="maritalStatus" class="input-10-length">
							<option value=""
								<s:if test="crmPatient.maritalStatus==''">selected="selected"</s:if>>--<fmt:message key="biolims.common.pleaseSelect"/>--</option>
							<option value="1"
								<s:if test="crmPatient.maritalStatus==1">selected="selected"</s:if>><fmt:message key="biolims.common.married"/></option>
							<option value="0"
								<s:if test="crmPatient.maritalStatus==0">selected="selected"</s:if>><fmt:message key="biolims.common.unmarried"/></option>
					</select></td>
					
				</tr>
				<tr>
					<td class="label-title" id="xing16" ><fmt:message key="biolims.common.professional"/></td>
					<td align="left" id="xing17" ><input type="text"  searchField="true" size="15" maxlength="100"
						id="crmPatient_occupation" name="occupation" title="<fmt:message key="biolims.common.professional"/>"
						class="text input"
						value="<s:property value="crmPatient.occupation"/>" /></td>
					<td class="label-title" id="xing18" ><fmt:message key="biolims.common.contact"/></td>
					<td align="left" id="xing19" ><input type="text"  searchField="true" size="20" maxlength="100"
						id="crmPatient_telphoneNumber2" name="telphoneNumber2"
						title="<fmt:message key="biolims.common.mainContactPhoneNumber"/>" class="text input"
						value="<s:property value="crmPatient.telphoneNumber2"/>" />
						</td>
					<td class="label-title" id="xing20" >email</td>
					<td align="left" id="xing21" ><input type="text"  searchField="true" size="20" maxlength="100"
						id="crmPatient_email" name="email"
						title="email" class="text input"
						value="<s:property value="crmPatient.email"/>" /></td>
				</tr>
			<tr>
				<%--
						<td class="label-title" id="xing22" ><fmt:message key="biolims.common.hospital"/></td>
								<td align="left" id="xing23" ><input type="text"  searchField="true" size="15"
						id="sbr_customer_name"
						value="<s:property value="crmPatient.customer.name"/>"
						class="text input" /> <input type="hidden"
						id="sbr_customer_id" name="customer.id"
						value="<s:property value="crmPatient.customer.id"/>">
						
							</td>
						<td class="label-title" id="xing24" ><fmt:message key="biolims.common.department"/></td>
						<td align="left" id="xing25" ><input type="text"  searchField="true" size="15"
						id="sbr_ks_name"
						value="<s:property value="crmPatient.ks.name"/>"
						class="text input" /> <input type="hidden"
						id="sbr_ks_id" name="ks.id"
						value="<s:property value="crmPatient.ks.id"/>">
					
							</td>
							
							
							
							<td class="label-title" id="xing26" ><fmt:message key="biolims.common.hospital"/></td>
							<td align="left" id="xing27" ><input type="text"  searchField="true" size="15"
					 	id="sbr_customerDoctor_name"
						value="<s:property value="crmPatient.customerDoctor.name"/>"
						class="text input" /> <input type="hidden"
						id="sbr_customerDoctor_id" name="customerDoctor.id"
						value="<s:property value="crmPatient.customerDoctor.id"/>">
                       
							</td>
					--%>
			</tr>
			</table>
		
		
		</form>
		</div>
		<div id="show_crmPatient_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_crmPatient_tree_page"></div>
</body>
</html>



