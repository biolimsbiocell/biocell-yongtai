﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/customer/patient/crmPatientDialogTwo.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<g:LayOutWinTag buttonId="showcrmCustomerId" title='<fmt:message key="biolims.common.selectTheCustomerID"/>'
						hasHtmlFrame="true" width = "600" height="400"
						html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
						isHasSubmit="false" functionName="CrmCustomerFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmPatient_crmCustomerId').value=rec.get('id');
				document.getElementById('crmCustomerId_name').value=rec.get('name');" />

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
			<input type="hidden" id="code" value="${requestScope.code}">
           	 	<td class="label-title"><fmt:message key="biolims.common.patientID"/></td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmPatient_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	    	 	<td class="label-title"><fmt:message key="biolims.common.sname"/></td>
               	<td align="left">
                    		<input type="text" maxlength="100" id="crmPatient_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
			</tr>
			
		
			
		<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.customerID"/></td>
               	<!-- <td align="left">
                    		<input type="text" maxlength="36" id="crmPatient_crmCustomerId" searchField="true" name="crmCustomerId"  class="input-20-length"></input>
               	</td> -->
					<td align="left"><input type="text" size="15"
						readonly="readOnly" id="crmCustomerId_name"
						value=""
						class="text input" /> <input type="hidden"
						id="crmPatient_crmCustomerId" name="crmCustomerId.id" searchField="true"
						value="">
						<img alt='<fmt:message key="biolims.common.selectTheCustomerID"/>' id='showcrmCustomerId'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
			</tr> 
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_crmPatientTwo_div"></div>
   		
</body>
</html>



