<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="add"'>
<g:LayOutWinTag buttonId="doclinks_labels" title='<fmt:message key="biolims.common.attachment"/>'
					hasHtmlFrame="true" width="900" height="500"
					html="${ctx}/operfile/initFileList.action?modelType=patienteditxgbs&id=${crmPatient.id}"
					isHasSubmit="false" functionName="doc" />
		
</s:if>
<g:LayOutWinTag buttonId="hos" title='<fmt:message key="biolims.common.smokingHistory"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	functionName="xys" hasSetFun="true" documentId="crmPatient_historyOfSmoking"
	documentName="crmPatient_historyOfSmoking" />
	<g:LayOutWinTag buttonId="cpyjs" title='<fmt:message key="biolims.common.historyOfDrinking"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	functionName="yjs" hasSetFun="true" documentId="crmPatient_yjs"
	documentName="crmPatient_yjs" />
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/patient/crmPatientEdit.js"></script>

	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
			<input type="hidden" id="filingUser" value="${requestScope.filingUser}">
		<input type="hidden" id="isSaleManage" value="${requestScope.isSaleManage}">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">

				<tr>
					
					
					
					
					<td class="label-title" id="xing30"  style="display:none">
					</td>
					<td align="left" id="xing1" style="display:none" ></td>
					<td class="label-title" id="xing2" style="display:none">
					</td>
					<td align="left" id="xing3" style="display:none"></td>
						
				</tr>
				<tr>
				<td class="label-title"><fmt:message key="biolims.common.electronicMedicalRecords"/><img class='requiredimage'
						src='${ctx}/images/required.gif' />
					</td>
					<td align="left"><input type="text" size="10" maxlength="36"
						id="crmPatient_id" name="crmPatient.id" title="<fmt:message key="biolims.common.patient"/>"
						class="text input" value="<s:property value="crmPatient.id"/>" />
					</td>
					<td class="label-title" id="xing4" ><fmt:message key="biolims.common.sname"/></td>
					<td align="left" id="xing5" ><input type="text" size="50" maxlength="20"
						style="height: 20px; width: 80px" id="crmPatient_name"
						name="crmPatient.name" title="<fmt:message key="biolims.common.sname"/>" class="text input"
						 value="<s:property value="crmPatient.name"/>" />
					</td>
					<td class="label-title" id="xing6" ><fmt:message key="biolims.common.iDcard"/></td>
					<td align="left" id="xing7" ><input type="text" size="15" maxlength="200"
						style="height: 20px; width: 160px" id="crmPatient_sfz" onBlur="sfzVerify()"
						name="crmPatient.sfz" title="<fmt:message key="biolims.common.iDcard"/>" class="text input"
						value="<s:property value="crmPatient.sfz"/>" /></td>
					
					<!-- onBlur="sfzVerify1()" -->
					
					<script>
						$(function() {

							new Ext.form.ComboBox({
								transform : "crmPatient_gender",
								width : 80,
								hiddenId : "crmPatient_gender",
								hiddenName : "crmPatient.gender"
							});
						});
					</script>
					<td class="label-title"><span id="dq" style="display:none"><fmt:message key="biolims.common.currentAge"/></span><span id="sw" style="display:none"><fmt:message key="biolims.common.dateOfDeath"/></span></td>
					<td align="left" id="dq1" style="display:none"><input type="text" size="12" maxlength="60"
						id="dqAge" name="dqAge"
						title="<fmt:message key="biolims.common.currentAge"/>" readonly="readOnly" class="text input readonlytrue"
						 />
					</td>
					<td align="left" id="sw1" style="display:none"><input type="text" size="12" maxlength="60"
						id="crmPatient_deathDate" name="crmPatient.deathDate"
						title="<fmt:message key="biolims.common.dateOfDeath"/>" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmPatient.deathDate" format="yyyy-MM-dd"/>" />
					</td>
					
				</tr>
				<tr>
				<td class="label-title" id="xing8" ><fmt:message key="biolims.common.dateOfBirth"/></td>
					<td align="left" id="xing9" ><input type="text" size="12" maxlength="100"
						id="crmPatient_dateOfBirth" name="crmPatient.dateOfBirth"
						title="<fmt:message key="biolims.common.dateOfBirth"/>" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmPatient.dateOfBirth" format="yyyy-MM-dd"/>" />
					</td>
					<td class="label-title" id="xing10" ><fmt:message key="biolims.common.inputtingTheAge"/></td>
					<td align="left" id="xing11"><input type="text" size="15" maxlength="100"
						id="crmPatient_age" style="height: 20px; width: 80px"
						name="crmPatient.age" title="<fmt:message key="biolims.common.inputtingTheAge"/>" class="text input readonlyfalse"
						 value="<s:property value="crmPatient.age"/>" />
					</td>
					
					<td class="label-title" id="xing12"><fmt:message key="biolims.common.national"/></td>
					<td align="left" id="xing13"><select id="crmPatient_race"
						name="crmPatient.race" class="input-10-length">
							<option value=""
								<s:if test="crmPatient.race==''">selected="selected"</s:if>>--<fmt:message key="biolims.common.pleaseSelect"/>--</option>
							<option value="0"
								<s:if test="crmPatient.race==0">selected="selected"</s:if>><fmt:message key="biolims.common.han"/>
							</option>
							<option value="1"
								<s:if test="crmPatient.race==1">selected="selected"</s:if>><fmt:message key="biolims.common.zhuang"/>
							</option>
							<option value="2"
								<s:if test="crmPatient.race==2">selected="selected"</s:if>><fmt:message key="biolims.common.manchu"/></option>
							<option value="3"
								<s:if test="crmPatient.race==3">selected="selected"</s:if>><fmt:message key="biolims.common.hui"/></option>
							<option value="4"
								<s:if test="crmPatient.race==4">selected="selected"</s:if>><fmt:message key="biolims.common.miao"/></option>
							<option value="5"
								<s:if test="crmPatient.race==5">selected="selected"</s:if>><fmt:message key="biolims.common.uyghur"/>
							</option>
							<option value="6"
								<s:if test="crmPatient.race==6">selected="selected"</s:if>><fmt:message key="biolims.common.tujia"/>
							</option>
							<option value="7"
								<s:if test="crmPatient.race==7">selected="selected"</s:if>><fmt:message key="biolims.common.yi"/>
							</option>
							<option value="8"
								<s:if test="crmPatient.race==8">selected="selected"</s:if>><fmt:message key="biolims.common.mongol"/></option>
							<option value="9"
								<s:if test="crmPatient.race==9">selected="selected"</s:if>><fmt:message key="biolims.common.tibetan"/>
							</option>
							<option value="10"
								<s:if test="crmPatient.race==10">selected="selected"</s:if>><fmt:message key="biolims.common.buyei"/>
							</option>
							<option value="11"
								<s:if test="crmPatient.race==11">selected="selected"</s:if>><fmt:message key="biolims.common.dong"/>
							</option>
							<option value="12"
								<s:if test="crmPatient.race==12">selected="selected"</s:if>><fmt:message key="biolims.common.yao"/></option>
							<option value="13"
								<s:if test="crmPatient.race==13">selected="selected"</s:if>><fmt:message key="biolims.common.korean"/></option>
							<option value="14"
								<s:if test="crmPatient.race==14">selected="selected"</s:if>><fmt:message key="biolims.common.bai"/>
							</option>
							<option value="15"
								<s:if test="crmPatient.race==15">selected="selected"</s:if>><fmt:message key="biolims.common.hani"/></option>
							<option value="16"
								<s:if test="crmPatient.race==16">selected="selected"</s:if>><fmt:message key="biolims.common.kazak"/></option>
							<option value="17"
								<s:if test="crmPatient.race==17">selected="selected"</s:if>><fmt:message key="biolims.common.li"/></option>
							<option value="18"
								<s:if test="crmPatient.race==18">selected="selected"</s:if>><fmt:message key="biolims.common.dai"/></option>
							<option value="19"
								<s:if test="crmPatient.race==19">selected="selected"</s:if>><fmt:message key="biolims.common.she"/></option>
							<option value="20"
								<s:if test="crmPatient.race==20">selected="selected"</s:if>><fmt:message key="biolims.common.lisu"/></option>
							<option value="21"
								<s:if test="crmPatient.race==21">selected="selected"</s:if>><fmt:message key="biolims.common.gelao"/></option>
							<option value="22"
								<s:if test="crmPatient.race==22">selected="selected"</s:if>><fmt:message key="biolims.common.dongxiang"/></option>
							<option value="23"
								<s:if test="crmPatient.race==23">selected="selected"</s:if>><fmt:message key="biolims.common.gaoShan"/></option>

							<option value="24"
								<s:if test="crmPatient.race==24">selected="selected"</s:if>><fmt:message key="biolims.common.lahu"/></option>
							<option value="25"
								<s:if test="crmPatient.race==25">selected="selected"</s:if>><fmt:message key="biolims.common.sui"/></option>
							<option value="26"
								<s:if test="crmPatient.race==26">selected="selected"</s:if>><fmt:message key="biolims.common.va"/>
							</option>
							<option value="27"
								<s:if test="crmPatient.race==27">selected="selected"</s:if>><fmt:message key="biolims.common.naxi"/>
							</option>
							<option value="28"
								<s:if test="crmPatient.race==28">selected="selected"</s:if>><fmt:message key="biolims.common.qiang"/>
							</option>
							<option value="29"
								<s:if test="crmPatient.race==29">selected="selected"</s:if>><fmt:message key="biolims.common.tu"/></option>
							<option value="30"
								<s:if test="crmPatient.race==30">selected="selected"</s:if>><fmt:message key="biolims.common.mulao"/>
							</option>
							<option value="31"
								<s:if test="crmPatient.race==31">selected="selected"</s:if>><fmt:message key="biolims.common.xibe"/></option>
							<option value="32"
								<s:if test="crmPatient.race==32">selected="selected"</s:if>><fmt:message key="biolims.common.kirgiz"/></option>
							<option value="33"
								<s:if test="crmPatient.race==33">selected="selected"</s:if>><fmt:message key="biolims.common.daur"/>
							</option>
							<option value="34"
								<s:if test="crmPatient.race==34">selected="selected"</s:if>><fmt:message key="biolims.common.jingpo"/></option>
							<option value="35"
								<s:if test="crmPatient.race==35">selected="selected"</s:if>><fmt:message key="biolims.common.maonan"/></option>
							<option value="36"
								<s:if test="crmPatient.race==36">selected="selected"</s:if>><fmt:message key="biolims.common.salar"/></option>
							<option value="37"
								<s:if test="crmPatient.race==37">selected="selected"</s:if>><fmt:message key="biolims.common.blang"/></option>
							<option value="38"
								<s:if test="crmPatient.race==38">selected="selected"</s:if>><fmt:message key="biolims.common.tajik"/></option>
							<option value="39"
								<s:if test="crmPatient.race==39">selected="selected"</s:if>><fmt:message key="biolims.common.achang"/></option>

							<option value="40"
								<s:if test="crmPatient.race==40">selected="selected"</s:if>><fmt:message key="biolims.common.pumi"/></option>
							<option value="41"
								<s:if test="crmPatient.race==41">selected="selected"</s:if>><fmt:message key="biolims.common.ewenki"/></option>

							<option value="42"
								<s:if test="crmPatient.race==42">selected="selected"</s:if>><fmt:message key="biolims.common.nu"/></option>

							<option value="43"
								<s:if test="crmPatient.race==43">selected="selected"</s:if>><fmt:message key="biolims.common.gin"/></option>
							<option value="44"
								<s:if test="crmPatient.race==44">selected="selected"</s:if>><fmt:message key="biolims.common.jino"/></option>
							<option value="45"
								<s:if test="crmPatient.race==45">selected="selected"</s:if>><fmt:message key="biolims.common.de'ang"/></option>

							<option value="46"
								<s:if test="crmPatient.race==46">selected="selected"</s:if>><fmt:message key="biolims.common.bonan"/></option>
							<option value="47"
								<s:if test="crmPatient.race==47">selected="selected"</s:if>><fmt:message key="biolims.common.russians"/></option>
							<option value="48"
								<s:if test="crmPatient.race==48">selected="selected"</s:if>><fmt:message key="biolims.common.yugur"/></option>

							<option value="49"
								<s:if test="crmPatient.race==49">selected="selected"</s:if>><fmt:message key="biolims.common.uzbek"/>
							</option>
							<option value="50"
								<s:if test="crmPatient.race==50">selected="selected"</s:if>><fmt:message key="biolims.common.monba"/></option>
							<option value="51"
								<s:if test="crmPatient.race==51">selected="selected"</s:if>><fmt:message key="biolims.common.oroqen"/></option>

							<option value="52"
								<s:if test="crmPatient.race==52">selected="selected"</s:if>><fmt:message key="biolims.common.derung"/></option>
							<option value="53"
								<s:if test="crmPatient.race==53">selected="selected"</s:if>><fmt:message key="biolims.common.tatar"/></option>
							<option value="54"
								<s:if test="crmPatient.race==54">selected="selected"</s:if>><fmt:message key="biolims.common.hezhen"/></option>

							<option value="55"
								<s:if test="crmPatient.race==55">selected="selected"</s:if>><fmt:message key="biolims.common.lhoba"/>
							</option>





					</select> <script>
						$(function() {

							new Ext.form.ComboBox({
								transform : "crmPatient_race",
								width : 80,
								hiddenId : "crmPatient_race",
								hiddenName : "crmPatient.race"
							});
						});
					</script></td>
				</tr>

				<tr>
					<td class="label-title" id="xing14"><fmt:message key="biolims.common.gender"/></td>
					<td align="left" id="xing15"><select id="crmPatient_gender"
						name="crmPatient.gender" class="input-10-length">
							<option value=""
								<s:if test="crmPatient.gender==''">selected="selected"</s:if>>--<fmt:message key="biolims.common.pleaseSelect"/>--</option>
							<option value="1"
								<s:if test="crmPatient.gender==1">selected="selected"</s:if>><fmt:message key="biolims.common.male"/></option>
							<option value="0"
								<s:if test="crmPatient.gender==0">selected="selected"</s:if>><fmt:message key="biolims.common.female"/></option>
					</select>
					<td class="label-title" id="xing28" ><fmt:message key="biolims.common.street"/></td>
					<td align="left" id="xing29" ><input type="text" size="15" maxlength="200"
						style="height: 20px; width: 160px" id="crmPatient_address"
						name="crmPatient.address" title="<fmt:message key="biolims.common.homeAddress"/>" class="text input"
						value="<s:property value="crmPatient.address"/>" /></td>
						<td class="label-title" id="xing8" ><fmt:message key="biolims.common.MedicalNumber"/> </td>
					<td align="left" id="xing7" ><input type="text" size="15" maxlength="200"
						style="height: 20px; width: 160px" id="crmPatient_yibao" 
						 
						name="crmPatient.yibao" title="医保证号" class="text input"
						value="<s:property value="crmPatient.yibao"/>" /></td>
				</tr>
				<tr>
				<td class="label-title"><fmt:message key="biolims.common.nativePlace"/></td>
					<td align="left"><input type="text" size="15" maxlength="200"
						style="height: 20px; width: 160px" id="crmPatient_placeOfBirth"
						name="crmPatient.placeOfBirth" title="<fmt:message key="biolims.common.nativePlace"/>" class="text input"
						value="<s:property value="crmPatient.placeOfBirth"/>" /></td>
					<td class="label-title"><fmt:message key="biolims.common.adultChild"/></td>
					<td align="left"><input type="text" id="crmPatient_adultTeenager"
						name="crmPatient.adultTeenager"  class="text input readonlytrue"
						readonly="readOnly" value="<s:property value="crmPatient.adultTeenager"/>" />
					</td>
					<td class="label-title"><fmt:message key="biolims.common.maritalStatus"/></td>
					<td align="left"><select id="crmPatient_maritalStatus"
						name="crmPatient.maritalStatus" class="input-10-length">
							<option value=""
								<s:if test="crmPatient.maritalStatus==''">selected="selected"</s:if>>--<fmt:message key="biolims.common.pleaseSelect"/>--</option>
							<option value="1"
								<s:if test="crmPatient.maritalStatus==1">selected="selected"</s:if>><fmt:message key="biolims.common.married"/></option>
							<option value="0"
								<s:if test="crmPatient.maritalStatus==0">selected="selected"</s:if>><fmt:message key="biolims.common.unmarried"/></option>
					</select> <script>
						$(function() {

							new Ext.form.ComboBox({
								transform : "crmPatient_maritalStatus",
								width : 80,
								hiddenId : "crmPatient_maritalStatus",
								hiddenName : "crmPatient.maritalStatus"
							});
						});
					</script></td>
					
				</tr>
				<tr>
					<td class="label-title" id="xing16" ><fmt:message key="biolims.common.professional"/></td>
					<td align="left" id="xing17" ><input type="text" size="15" maxlength="100"
						id="crmPatient_occupation" name="crmPatient.occupation" title="<fmt:message key="biolims.common.professional"/>"
						class="text input"
						value="<s:property value="crmPatient.occupation"/>" /></td>
					<td class="label-title" id="xing18" ><fmt:message key="biolims.common.contact"/></td>
					<td align="left" id="xing19" ><input type="text" size="20" maxlength="100"
						id="crmPatient_telphoneNumber2" name="crmPatient.telphoneNumber2"
						title="<fmt:message key="biolims.common.mainContactPhoneNumber"/>" class="text input"
						value="<s:property value="crmPatient.telphoneNumber2"/>" />
						<input type="text" size="30" maxlength="100"
						id="crmPatient_telphoneNumber1" name="crmPatient.telphoneNumber1"
						title="<fmt:message key="biolims.common.contact1"/>" class="text input"
						value="<s:property value="crmPatient.telphoneNumber1"/>" /></td>
					<td class="label-title" id="xing20" ><fmt:message key="biolims.common.email"/></td>
					<td align="left" id="xing21" ><input type="text" size="20" maxlength="100"
						id="crmPatient_email" name="crmPatient.email"
						title="email" class="text input"
						value="<s:property value="crmPatient.email"/>" /></td>
				</tr>
			<tr>
				<td class="label-title" id="xing22" ><fmt:message key="biolims.common.hospital"/></td>
								<td align="left" id="xing23" ><input type="text" size="15"
						readonly="readOnly" id="sbr_customer_name"
						value="<s:property value="crmPatient.customer.name"/>"
						class="text input" /> <input type="hidden"
						id="sbr_customer_id" name="crmPatient.customer.id"
						value="<s:property value="crmPatient.customer.id"/>">
						<span
							id="customer" onClick="showCustomer()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
							</td>
						<td class="label-title" id="xing24" ><fmt:message key="biolims.common.department"/></td>
						<td align="left" id="xing25" ><input type="text" size="15"
						readonly="readOnly" id="sbr_ks_name"
						value="<s:property value="crmPatient.ks.name"/>"
						class="text input" /> <input type="hidden"
						id="sbr_ks_id" name="crmPatient.ks.id"
						value="<s:property value="crmPatient.ks.id"/>">
						<span
							id="ks" onClick="showkindFun()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
							</td>
							
				<g:LayOutWinTag buttonId="customerDoctor" title='<fmt:message key="biolims.common.selectTheDoctor"/>'
				hasHtmlFrame="true"
				html="${ctx}/crm/doctor/crmPatientSelect.action"
				isHasSubmit="false" functionName="DicProbeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sbr_customerDoctor_id').value=rec.get('id');
				document.getElementById('sbr_customerDoctor_name').value=rec.get('name');" />
							
							
							<td class="label-title" id="xing26" ><fmt:message key="biolims.common.doctor"/></td>
							<td align="left" id="xing27" ><input type="text" size="15"
						readonly="readOnly" id="sbr_customerDoctor_name"
						value="<s:property value="crmPatient.customerDoctor.name"/>"
						class="text input" /> <input type="hidden"
						id="sbr_customerDoctor_id" name="crmPatient.customerDoctor.id"
						value="<s:property value="crmPatient.customerDoctor.id"/>">
                       <span
							id="customerDoctor"  class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
							</td>
			</tr>
				<tr>
	<td class="label-title"><fmt:message key="biolims.common.agreeToReceive"/></td>
					<td align="left"><select id="crmPatient_consentReceived"
						name="crmPatient.consentReceived" class="input-10-length">
							<option value="1"
								<s:if test="crmPatient.consentReceived==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes"/></option>
							<option value="0"
								<s:if test="crmPatient.consentReceived==0">selected="selected"</s:if>><fmt:message key="biolims.common.no"/></option>
					</select> <script>
						$(function() {

							new Ext.form.ComboBox({
								transform : "crmPatient_consentReceived",
								width : 80,
								hiddenId : "crmPatient_consentReceived",
								hiddenName : "crmPatient.consentReceived"
							});
						});
					</script></td>
					<g:LayOutWinTag buttonId="showpatientStatus" title='<fmt:message key="biolims.common.selectTheStateOfPatient"/>'
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="brzt" hasSetFun="true"
						documentId="crmPatient_patientStatus"
						documentName="crmPatient_patientStatus_name" />
					<td class="label-title"><fmt:message key="biolims.common.stateOfThePatient"/></td>
					<td align="left"><input type="text" size="10"
						readonly="readOnly" id="crmPatient_patientStatus_name"
						value="<s:property value="crmPatient.patientStatus.name"/>"
						class="text input" /> <input type="hidden"
						id="crmPatient_patientStatus" name="crmPatient.patientStatus.id"
						value="<s:property value="crmPatient.patientStatus.id"/>">
						<img alt='<fmt:message key="biolims.common.selectTheStateOfPatient"/>' id='showpatientStatus'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					<td class="label-title"><fmt:message key="biolims.common.wechatNumber"/></td>
					<td align="left"><input type="text" size="15" maxlength="15"
						style="height: 20px; width: 160px" id="crmPatient_weichatId"
						name="crmPatient.weichatId" title="<fmt:message key="biolims.common.wechatNumber"/>" class="text input"
						value="<s:property value="crmPatient.weichatId"/>" /></td>
				</tr>
				<s:if test='#request.handlemethod!="add"'>
				<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
					hasHtmlFrame="true" width="900" height="500"
					html="${ctx}/operfile/initFileList.action?modelType=crmPatient&id=${crmPatient.id}"
					isHasSubmit="false" functionName="fj2" />
				</s:if>	
				<tr>
<td class="label-title"><fmt:message key="biolims.common.commandPerson"/></td>
					<td align="left"><input type="text" size="10"
						readonly="readOnly" id="crmPatient_createUser_name"
						value="<s:property value="crmPatient.createUser.name"/>"
						class="text input readonlytrue" /> <input type="hidden"
						id="crmPatient_createUser" name="crmPatient.createUser.id"
						value="<s:property value="crmPatient.createUser.id"/>"></td>

					<g:LayOutWinTag buttonId="showcrmCustomerId" title='<fmt:message key="biolims.common.selectTheCustomerID"/>'
						hasHtmlFrame="true" width = "800" height="600"
						html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
						isHasSubmit="false" functionName="CrmCustomerFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmPatient_crmCustomerId').value=rec.get('id');
				document.getElementById('crmPatient_crmCustomerId_name').value=rec.get('name');" />





									<td class="label-title"><fmt:message key="biolims.common.commandTime"/></td>
					<td align="left"><input type="text" size="12" maxlength="60"
						id="crmPatient_createDate" name="crmPatient.createDate"
						title="<fmt:message key="biolims.common.commandTime"/>" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmPatient.createDate" format="yyyy-MM-dd"/>" />
					</td>				
					<td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.attachment"/>"
						class="text label"><fmt:message key="biolims.common.attachment"/></label>&nbsp;</td>
					<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
					</td>		
				</tr>
				
				<tr>
				
	<td class="label-title"><fmt:message key="biolims.common.checkCompleted"/></td>
					<td align="left"><select id="crmPatient_isEnd"
						name="crmPatient.isEnd" class="input-10-length">
						<option value=""
								<s:if test="crmPatient.isEnd==''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
							<option value="1"
								<s:if test="crmPatient.isEnd==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes"/></option>
							<option value="0"
								<s:if test="crmPatient.isEnd==0">selected="selected"</s:if>><fmt:message key="biolims.common.no"/></option>
					</select> <script>
						$(function() {

							new Ext.form.ComboBox({
								transform : "crmPatient_isEnd",
								width : 80,
								hiddenId : "crmPatient_isEnd",
								hiddenName : "crmPatient.isEnd"
							});
						});
					</script></td>
					</tr>
			</table>

		
            <input type="hidden" name="crmLinkManItemJson" id="crmLinkManItemJson" value="" />
			<input type="hidden" name="crmPatientTumorInformationJson" id="crmPatientTumorInformationJson" value="" /> 
			<input type="hidden" name="crmPatientDiagnosisJson"     id="crmPatientDiagnosisJson" value="" /> 
			<input type="hidden" name="crmPatientRadiologyJson"  id="crmPatientRadiologyJson" value="" />
			<input type="hidden" name="crmPatientLaboratoryJson" id="crmPatientLaboratoryJson" value="" /> 
			<input type="hidden" name="crmPatientSurgeriesJson"  id="crmPatientSurgeriesJson" value="" />
			<input type="hidden" name="crmPatientRestsJson"      id="crmPatientRestsJson" value="" /> 
			<input type="hidden" name="crmPatientPathologyJson"  id="crmPatientPathologyJson" value="" /> 
			<input type="hidden" name="crmPatientGeneticTestingJson" id="crmPatientGeneticTestingJson" value="" /> 
			<input type="hidden" name="crmPatientHistoryJson"    id="crmPatientHistoryJson" value="" />
			<input type="hidden" name="crmCustomerLinkManJson"   id="crmCustomerLinkManJson" value="" />
			<input type="hidden" name="crmPatientRestsJson"      id="crmPatientRestsJson" value="" />
			<input type="hidden" name="crmPatientTreatJson"      id="crmPatientTreatJson" value="" />
			<input type="hidden" name="crmConsumerMarketJson"    id="crmConsumerMarketJson" value="" />
			<input type="hidden" name="crmPatientPersonnelJson" id="crmPatientPersonnelJson" value="" />
			<input type="hidden" name="crmPatientItemJson" id="crmPatientItemJson" value="" />
			
			<input type="hidden" id="id_parent_hidden" value="<s:property value="crmPatient.id"/>" />
			<div id="tabss">
				<ul>
					<li><a href="#crmPatientMedicalHistorypage"><fmt:message key="biolims.common.relevantMedicalHistory"/></a></li>
					<li><a href="#crmPatientLinkManpage"><fmt:message key="biolims.common.contact"/></a></li>
					<li><a href="#crmLinkManItempage"><fmt:message key="biolims.common.communicateInformation"/></a></li>
					<li><a href="#sampleOrderpage"><fmt:message key="biolims.common.orderInformation"/></a></li>
					<li><a href="#sampleInputpage"><fmt:message key="biolims.common.clinicalSamplesInformation"/></a></li>
					<li><a href="#crmCustoremerpage"><fmt:message key="biolims.common.confirmTheCharge"/></a></li>
<%-- 					<li><a href="#scientificSampleInputpage"><fmt:message key="biolims.common.scientificResearchSampleInformation"/></a></li> --%>
				</ul>
				<div id="crmCustoremerpage" style="width:100%;height:57%"></div>
				<div id="sampleOrderpage" style="width:100%;height:57%"></div>
				<div id="sampleInputpage" style="width:100%;height:57%"></div>
<!-- 				<div id="scientificSampleInputpage" style="width:100%;height:57%"></div> -->
				<div id="crmPatientLinkManpage" style="width: 100%; height: 32%"></div>
				<div id="crmLinkManItempage" style="width:100%;height:57%"></div>
				
				<div id="crmPatientMedicalHistorypage" style="width:100%;height:32%">
				
					<table class="frame-table">
						<tr>

						</tr>
						<tr>

                 	
						
						</tr>
						<tr>
							<td class="label-title"><fmt:message key="biolims.common.infectiousDiseases"/></td>
							<td align="left"><select id="crmPatient_infectiousDiseases"
								name="crmPatient.infectiousDiseases" class="input-10-length">
									<option value="2"
										<s:if test="crmPatient.infectiousDiseases==2">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
									<option value="0"
										<s:if test="crmPatient.infectiousDiseases==0">selected="selected"</s:if>><fmt:message key="biolims.common.no"/></option>
										<option value="1"
										<s:if test="crmPatient.infectiousDiseases==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes"/></option>
							</select> <script>
								$(function() {

									new Ext.form.ComboBox(
											{
												transform : "crmPatient_infectiousDiseases",
												width : 80,
												hiddenId : "crmPatient_infectiousDiseases",
												hiddenName : "crmPatient.infectiousDiseases"
											});
								});
							</script></td>

							<td class="label-title"><fmt:message key="biolims.common.situationOfInfectiousDiseases"/></td>
							<td align="left"><input type="text" size="55"
								id="crmPatient_infectiousDiseasestext" name="crmPatient.infectiousDiseasestext"
								value="<s:property value="crmPatient.infectiousDiseasestext"/>"
								class="text input" /></td>
						</tr>
						<tr>
							<td class="label-title"><fmt:message key="biolims.common.smokingHistory"/></td>
							<td align="left"><input type="text" id="crmPatient_historyOfSmoking" name="crmPatient.historyOfSmoking"
							value="<s:property value="crmPatient.historyOfSmoking"/>">
							<span id="hos" class="select-search-btn"></span>
							</td>
							<td class="label-title"><fmt:message key="biolims.common.situationOfSmoking"/></td>
							<td align="left"><input type="text" size="55"
								id="crmPatient_historyOfSmokingtext" name="crmPatient.historyOfSmokingtext"
								value="<s:property value="crmPatient.historyOfSmokingtext"/>"
								class="text input" /></td>
						</tr>
						<tr>
							<td class="label-title"><fmt:message key="biolims.common.environmentalRiskPoison"/></td>
							<td align="left"><select
								id="crmPatient_environmentalExposures"
								name="crmPatient.environmentalExposures" class="input-10-length">
									<option value="2"
										<s:if test="crmPatient.environmentalExposures==2">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
									<option value="0"
										<s:if test="crmPatient.environmentalExposures==0">selected="selected"</s:if>><fmt:message key="biolims.common.no"/></option>
										<option value="1"
										<s:if test="crmPatient.environmentalExposures==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes"/></option>
							</select> <script>
								$(function() {

									new Ext.form.ComboBox(
											{
												transform : "crmPatient_environmentalExposures",
												width : 80,
												hiddenId : "crmPatient_environmentalExposures",
												hiddenName : "crmPatient.environmentalExposures"
											});
								});
							</script></td>
							<td class="label-title"><fmt:message key="biolims.common.situationOfEnvironmentalRiskPoison"/></td>
							<td align="left"><input type="text" size="55"
							 id="crmPatient_environmentalExposurestext" name="crmPatient.environmentalExposurestext"
								value="<s:property value="crmPatient.environmentalExposurestext"/>"
								class="text input" /></td>
						</tr>
							<tr>
							<td class="label-title"><fmt:message key="biolims.common.historyOfDrinking"/></td>
							<td align="left"><input type="text" id="crmPatient_yjs" name="crmPatient.ysj"
							value="<s:property value="crmPatient.ysj"/>">
							<span id="cpyjs" class="select-search-btn"></span>
							</td>
							<td class="label-title"><fmt:message key="biolims.common.historyOfDrinkingSituation"/></td>
							<td align="left"><input type="text" size="55"
								id="crmPatient_ysjtext" name="crmPatient.ysjtext"
								value="<s:property value="crmPatient.ysjtext"/>"
								class="text input" /></td>
							</tr>
						<tr>
							<td class="label-title"><fmt:message key="biolims.common.otherDiseases"/></td>
							<td align="left"><select id="crmPatient_otherDiseases"
								name="crmPatient.otherDiseases" class="input-10-length">
									<option value="2"
										<s:if test="crmPatient.otherDiseases==2">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
									<option value="0"
										<s:if test="crmPatient.otherDiseases==0">selected="selected"</s:if>><fmt:message key="biolims.common.no"/></option>
										
									<option value="1"
										<s:if test="crmPatient.otherDiseases==1">selected="selected"</s:if>><fmt:message key="biolims.common.yes"/></option>	
							</select> <script>
								$(function() {

									new Ext.form.ComboBox({
										transform : "crmPatient_otherDiseases",
										width : 80,
										hiddenId : "crmPatient_otherDiseases",
										hiddenName : "crmPatient.otherDiseases"
									});
								});
							</script></td>

							<td class="label-title"><fmt:message key="biolims.common.historyOfOtherDiseases"/></td>
							<td align="left"><input type="text" size="55"
								id="crmPatient_otherDiseasestext" name="crmPatient.otherDiseasestext"
								value="<s:property value="crmPatient.otherDiseasestext"/>"
								class="text input" /></td>
												<td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label title="<fmt:message key="biolims.common.attachment"/>"
						class="text label"><fmt:message key="biolims.common.attachment"/></label>&nbsp;</td>
					<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_labels"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum1}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
						</tr>
						
						
					</table>
				</div>
				
				</div>
				
				
				<div id="tabs">
				<ul>
					<li><a href="#crmPatientDiagnosispage"><fmt:message key="biolims.common.comprehensiveDiagnosisOfTheDetail"/></a></li>
					<li><a href="#crmPatientTreatpage"><fmt:message key="biolims.common.treatmentOptions"/></a></li>
					<li><a href="#crmPatientDiagnosispage2"><fmt:message key="biolims.common.operationDetails"/></a></li>
					<li><a href="#crmPatientRadiologypage"><fmt:message key="biolims.common.radiologyDepartmentToCheck"/></a></li>
					<li><a href="#crmPatientLaboratorypage"><fmt:message key="biolims.common.clinicalLaboratoryTesting"/></a></li>
					<li><a href="#crmPatientPathologypage"><fmt:message key="biolims.common.pathologicalExamination"/></a></li>
					<li><a href="#crmPatientGeneticTestingpage"><fmt:message key="biolims.common.molecularPathologicalExamination/historyOfGeneticTesting"/></a></li>
					<li><a href="#crmPatientPersonnelpage"><fmt:message key="biolims.common.familyHistory2"/></a></li>
					 <li><a href="#crmPatientitempage"><fmt:message key="biolims.common.medicalInformation"/></a></li> 
				</ul>
                <div id="crmPatientDiagnosispage" width="100%" height:10px>
                <!-- <div id="crmPatientDiagnosispage1" width="100%" height:10px></div> -->
                <div id="crmPatientRestspage" width="100%" height:10px></div>
                </div>
                <div id="crmPatientTreatpage" width="100%" height:10px></div>
				<div id="crmPatientDiagnosispage2" width="100%" height:10px></div>
				<div id="crmPatientRadiologypage" width="100%" height:10px></div>
				<div id="crmPatientLaboratorypage" width="100%" height:10px></div>
				<div id="crmPatientPathologypage" width="100%" height:10px></div>
				<div id="crmPatientGeneticTestingpage" width="100%" height:10px></div>
				<div id="crmPatientPersonnelpage" width="100%" height:10px></div>
				<div id="crmPatientitempage" width="100%" height:10px></div>
			</div>
		</form>
	</div>
</body>
</html>
<!-- <script>
	xzCheckValue();
	checkValue2();
	</script> -->
