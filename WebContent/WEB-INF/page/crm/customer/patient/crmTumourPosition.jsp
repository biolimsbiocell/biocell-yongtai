<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/crm/customer/patient/crmTumourPosition.js"></script>
</head>
<g:HandleDataExtTag hasConfirm="false" paramsValue="" funName="modifyData" url="${ctx}/tumour/position/saveDicType.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/tumour/position/delDicType.action" />
<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
<body>
<input	type="hidden" id="id" value="" />
查询、增加字典信息，请选择字典类型：<input type="text" title="输入类型"  name="mainType" id="mainType" style="width:200px">
<g:GridEditTag isAutoWidth="true" newObjCode = " if(!cob.getValue()){p.set('superior','0');}else{p.set('superior',cob.getValue());} "
	col="${col}" statement="${statement}" type="${type}" title="字典管理" delMethodAll="true" allButton="true" pageSize="1000"
	url="${path}" statementLisener="${statementLisener}" handleMethod="modify" />
</body>
</html>