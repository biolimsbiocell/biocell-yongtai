<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<input type="hidden" id="open" value="${requestScope.open}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>
						<fmt:message key="biolims.common.electronicMedicalRecord" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.common.commandPerson" />: <span
							id="crmPatient_createUser"><s:property
									value="crmPatient.createUser.name" /></span> <!--接收日期: <span id="sampleReceive_acceptDate"><s:date name="sampleReceive.acceptDate" format="yyyy-MM-dd"/></span>-->
							<fmt:message key="biolims.common.commandTime" />: <span
							id="crmPatient_createDate"><s:date
									name="crmPatient.createDate" format="yyyy-MM-dd" /></span>
						</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();">
										<fmt:message key="biolims.common.print" />
								</a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();">
										<fmt:message key="biolims.common.copyData" />
								</a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"> <fmt:message
											key="biolims.common.lock2Col" />
								</a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"> <fmt:message
											key="biolims.common.cancellock2Col" />
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="done step"> <span class="step_no">1</span>
									<span class="step_descr"><small><fmt:message
												key="biolims.common.basicInformation" /></small> </span>
							</a></li>
							<%-- <li>
									<a class="done step"> <span class="step_no">2</span>
										<span class="step_descr"><small>相关病史</small></span>
									</a>
								</li> --%>
							<li><a class="done step"> <span class="step_no">2</span>
									<span class="step_descr"><small><fmt:message
												key="biolims.common.communicateInformation" /></small></span>
							</a></li>
							<li><a class="done step"> <span class="step_no">3</span>
									<span class="step_descr"><small><fmt:message
												key="biolims.common.orderInformation" /></small></span>
							</a></li>
							<%-- <li><a class="done step"> <span class="step_no">4</span>
									<span class="step_descr"><small><fmt:message
												key="biolims.common.clinicalSamplesInformation" /></small></span>
							</a></li>
							<li><a class="done step"> <span class="step_no">5</span>
									<span class="step_descr"><small><fmt:message
												key="biolims.common.confirmTheCharge" /></small></span>
							</a></li> --%>
						</ul>
					</div>
					<!--form表单-->

					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter(" bpmTaskId ")%>" /> <br> <br>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.patientInformation" />

										</h3>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.electronicMedicalRecords" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="50" maxlength="127" id="crmPatient_id"
											changelog="<s:property value="crmPatient.id"/>"
											name="crmPatient.id"
											title="<fmt:message key="biolims.common.electronicMedicalRecords"/>"
											class="form-control readonlytrue"
											value="<s:property value="crmPatient.id"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.sname" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="50" maxlength="127" id="crmPatient_name"
											name="crmPatient.name"
											changelog="<s:property value="crmPatient.name"/>"
											title="<fmt:message key="biolims.common.sname"/>"
											class="form-control"
											value="<s:property value="crmPatient.name"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.dateOfBirth" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											class="form-control "
											changelog="<s:property value="crmPatient.age"/>"
											value="<s:date name=" crmPatient.dateOfBirth " format="yyyy-MM-dd "/>"
											type="text" size="20" maxlength="25"
											id="crmPatient_dateOfBirth" name="crmPatient.dateOfBirth"
											title="<fmt:message key="biolims.common.dateOfBirth"/>"
											class="Wdate"
											value="<s:date name=" crmPatient.dateOfBirth " format="yyyy-MM-dd "/>" />


									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<input type="hidden" name="crmPatient.gender" changelog
										id="crmPatient_gender"
										value="<s:property value=" crmPatient.gender "/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.gender" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span>
										<s:if test="crmPatient.gender==1">
											<input type="checkbox" value="1" note="1" changelog checked
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" note="1" changelog
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" note="1" changelog
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test="crmPatient.gender==0">
											<input type="checkbox" value="1" note="1" changelog
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" note="1" changelog checked
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" note="1" changelog
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test="crmPatient.gender==3">
											<input type="checkbox" value="1" note="1" changelog
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" note="1" changelog
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" note="1" changelog checked
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test='crmPatient.gender==null'>
											<input type="checkbox" value="1" note="1" changelog
												title=<fmt:message key="biolims.common.male" />>
											<input type="checkbox" value="0" note="1" changelog
												title=<fmt:message key="biolims.common.female" />>
											<input type="checkbox" value="3" note="1" changelog
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.tStorage.state"/></span> <input
											type="hidden" size="50" maxlength="127"
											id="crmPatient_state"
											changelog="<s:property value="crmPatient.state"/>"
											name="crmPatient.state" title=""
											class="form-control"
											value="<s:property value="crmPatient.state"/>" />
										<div class="input-group">
											<s:if test="crmPatient.state==1">
												<input type="checkbox" value="1" note="11" changelog checked
													title="valid">
												<input type="checkbox" value="0" note="11" changelog
													title="invalid">
											</s:if>
											<s:if test="crmPatient.state==0">
												<input type="checkbox" value="1" note="11" changelog
													title="valid">
												<input type="checkbox" value="0" note="11" changelog checked
													title="invalid">
											</s:if>
										</div>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.inputtingTheAge" /> </span> <input type="text"
											size="20" id="crmPatient_age" name="crmPatient.age"
											changelog="<s:property value="crmPatient.age"/>"
											value="<s:property value="crmPatient.age"/>"
											class="form-control"
											title="<fmt:message key="biolims.common.inputtingTheAge"/>" />


									</div>
								</div>
							</div>
							<div class="row">
							
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">姓名缩写 </span> <input
											changelog="<s:property value="crmPatient.abbreviation"/>"
											type="text" size="20" maxlength="50"
											id="crmPatient_abbreviation" name="crmPatient.abbreviation"
											class="form-control" title="姓名缩写"
											value="<s:property value=" crmPatient.abbreviation "/>" />
									</div>
								</div>
								
									<div class="col-xs-4">
										<div class="input-group">
										<span class="input-group-addon">民族</span> <input type="text"
											size="50" maxlength="127" id="crmPatient_race"
											name="crmPatient.race" 
											changelog="<s:property value="crmPatient.race"/>"
											title="<fmt:message key="biolims.common.race"/>"
											class="form-control"
											value="<s:property value="crmPatient.race"/>" />
											</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
										<span class="input-group-addon">感染筛查</span> <input type="text"
											size="50" maxlength="127" id="crmPatient_infectionScreening"
											name="crmPatient.infectionScreening" 
											changelog="<s:property value="crmPatient.infectionScreening"/>"
											title="<fmt:message key="biolims.common.infectionScreening"/>"
											class="form-control"
											value="<s:property value="crmPatient.infectionScreening"/>" />
											</div>
									</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.iDcard" /></span> <input type="text" size="50"
											maxlength="127" id="crmPatient_sfz"
											changelog="<s:property value="crmPatient.sfz"/>"
											name="crmPatient.sfz"
											title="<fmt:message key="biolims.common.iDcard"/>"
											class="form-control"
											value="<s:property value="crmPatient.sfz"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<input type="hidden" name="crmPatient.maritalStatus" changelog
										id="crmPatient_maritalStatus"
										value="<s:property value=" crmPatient.maritalStatus "/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.marital" /> </span>
										<s:if test="crmPatient.maritalStatus==1">
											<input type="checkbox" value="1" note="2" changelog checked
												title=<fmt:message key="biolims.common.married"/>>
											<input type="checkbox" value="0" note="2" changelog
												title=<fmt:message key="biolims.common.unmarried"/>>
											<input type="checkbox" value="3" note="2" changelog
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test="crmPatient.maritalStatus==0">
											<input type="checkbox" value="1" note="2" changelog
												title=<fmt:message key="biolims.common.married"/>>
											<input type="checkbox" value="0" note="2" changelog checked
												title=<fmt:message key="biolims.common.unmarried"/>>
											<input type="checkbox" value="3" note="2" changelog
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test="crmPatient.maritalStatus==3">
											<input type="checkbox" value="1" note="2" changelog
												title=<fmt:message key="biolims.common.married"/>>
											<input type="checkbox" value="0" note="2" changelog
												title=<fmt:message key="biolims.common.unmarried"/>>
											<input type="checkbox" value="3" note="2" changelog checked
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>
										<s:if test='crmPatient.maritalStatus==null'>
											<input type="checkbox" value="1" note="2" changelog
												title=<fmt:message key="biolims.common.married"/>>
											<input type="checkbox" value="0" note="2" changelog
												title=<fmt:message key="biolims.common.unmarried"/>>
											<input type="checkbox" value="3" note="2" changelog
												title=<fmt:message key="biolims.common.unknown" />>
										</s:if>

									</div>

								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.street" /> </span> <input type="text"
											size="50" maxlength="127" id="crmPatient_address"
											changelog="<s:property value="crmPatient.address"/>"
											name="crmPatient.address"
											title="<fmt:message key="biolims.common.street"/>"
											class="form-control"
											value="<s:property value="crmPatient.address"/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.nativePlace" /> </span> <input type="text"
											size="50" maxlength="127" id="crmPatient_placeOfBirth"
											changelog="<s:property value="crmPatient.placeOfBirth"/>"
											name="crmPatient.placeOfBirth"
											title="<fmt:message key="biolims.common.nativePlace"/>"
											class="form-control"
											value="<s:property value="crmPatient.placeOfBirth"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.professional" /> </span> <input type="text"
											size="50" maxlength="127" id="crmPatient_occupation"
											changelog="<s:property value="crmPatient.occupation"/>"
											name="crmPatient.occupation"
											title="<fmt:message key="biolims.common.professional"/>"
											class="form-control"
											value="<s:property value="crmPatient.occupation"/>" />

									</div>
								</div>
								<!-- 采血轮次 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon" style="font-family: 黑体;">
											采血轮次<img class="requiredimage" src="/images/required.gif">
										</span> <input class="form-control " 
											changelog="<s:property value="crmPatient.round"/>"
											type="text" size="20" maxlength="25" id="crmPatient_round"
											name="crmPatient.round" title="轮次" onchange="lunci(this.value)"
											value="<s:property value="crmPatient.round "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.contact" />1 </span> <input type="text"
											size="20" id="crmPatient_telphoneNumber1"
											name="crmPatient.telphoneNumber1"
											changelog="<s:property value="crmPatient.telphoneNumber1"/>"
											value="<s:property value="crmPatient.telphoneNumber1"/>"
											class="form-control" />


									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.contact" />2 </span> <input type="text"
											size="50" maxlength="127" id="crmPatient_telphoneNumber2"
											name="crmPatient_telphoneNumber2"
											changelog="<s:property value="crmPatient.telphoneNumber2"/>"
											name="safeNum" class="form-control"
											value="<s:property value="crmPatient.telphoneNumber2"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.wechatNumber" /> </span> <input type="text"
											size="50" maxlength="127" id="crmPatient_weichatId"
											changelog="<s:property value="crmPatient.weichatId"/>"
											name="crmPatient.weichatId"
											title="<fmt:message key="biolims.common.wechatNumber"/>"
											class="form-control"
											value="<s:property value="crmPatient.weichatId"/>" />

									</div>
								</div>
							</div>
								<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">筛选号<img
											class="requiredimage" src="/images/required.gif">
										</span> <input type="text"
											changelog="<s:property value="crmPatient.filtrateCode"/>"
											size="30" maxlength="50" class="form-control"
											id="crmPatient_filtrateCode" name="crmPatient.filtrateCode"
											title="筛选号" onchange="filtrateCode(this.value)"
											value="<s:property value="crmPatient.filtrateCode "/>" />
									</div>
								</div>
							<!-- </div>
							随机号
							<div class="row"> -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">随机号<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text"
											changelog="<s:property value="crmPatient.randomCode"/>"
											size="30" maxlength="50" class="form-control"
											id="crmPatient_randomCode" name="crmPatient.randomCode"
											title="随机号" onchange="randomCode(this.value)"
											value="<s:property value=" crmPatient.randomCode "/>" />
									</div>
								</div>

								<div class="col-xs-4">
										<div class="input-group">
										<span class="input-group-addon">CCOI</span> <input type="text"
											size="50" maxlength="127" id="crmPatient_ccoi"
											name="crmPatient.ccoi" readonly="readonly"
											changelog="<s:property value="crmPatient.ccoi"/>"
											class="form-control"
											value="<s:property value="crmPatient.ccoi"/>" />
											</div>
									</div>
								
								</div>
							<div class="row">
							<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.hospitalPatientID" /></span> <input
											type="text" size="20"
											changelog="<s:property value="crmPatient.hospitalPatientID"/>"
											maxlength="50" class="form-control"
											id="crmPatient_hospitalPatientID"
											name="crmPatient.hospitalPatientID" title="门诊/住院号"
											value="<s:property value=" crmPatient.hospitalPatientID "/>" />
									</div>
									<!-- </div> -->
								</div>
								
								<!-- 产品名称 -->
				              <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmDoctorItem.productName" /><img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="20"
											changelog="<s:property value="crmPatient.productName"/>"
											readonly="readOnly" id="crmPatient_productName"
											name="crmPatient.productName"
											value="<s:property value="crmPatient.productName "/>"
											class="form-control" /> <input type="hidden"
											id="crmPatient_productId" name="crmPatient.productId"
											value="<s:property value="crmPatient.productId "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='showage'
												onClick="voucherProductFun()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
					
								
								
							</div>
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-minus" onclick="patientShowAndHide2(this)"></i>
											<fmt:message key="biolims.common.hospitalInfo" />
										</h3>
									</div>
								</div>
							</div>
							<div class="row changeView2">

								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.hospital" /><img class="requiredimage" src="/images/required.gif"> </span> <input type="hidden"
											id="crmPatient_customer_id" name="crmPatient.customer.id"
											value="<s:property value="crmPatient.customer.id"/>">
										<input type="text" size="50" maxlength="127"
											id="crmPatient_customer_name" readonly="readOnly"
											changelog="<s:property value="crmPatient.customer.name"/>"
											title="<fmt:message key="biolims.common.hospital"/>"
											class="form-control"
											value="<s:property value="crmPatient.customer.name"/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showHos()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.department" /><img class="requiredimage" src="/images/required.gif"> </span> <input type="hidden"
											id="crmPatient_ks_id" name="crmPatient.ks.id"
											value="<s:property value="crmPatient.ks.id"/>"> <input
											type="text" size="50" maxlength="127" id="crmPatient_ks_name"
											readonly="readOnly"
											changelog="<s:property value="crmPatient.ks.name"/>"
											title="<fmt:message key="biolims.common.department"/>"
											class="form-control"
											value="<s:property value="crmPatient.ks.name"/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showinspectionDepartment()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.doctor" /> </span> <input type="hidden"
											id="crmPatient_customerDoctor_id"
											name="crmPatient.customerDoctor.id"
											value="<s:property value="crmPatient.customerDoctor.id"/>">
										<input type="text" size="50" maxlength="127"
											id="crmPatient_customerDoctor_name" readonly="readOnly"
											changelog="<s:property value="crmPatient.customerDoctor.name"/>"
											title="<fmt:message key="biolims.common.doctor"/>"
											class="form-control"
											value="<s:property value="crmPatient.customerDoctor.name"/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showDoctors() ">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							
							<div class="row changeView2">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">医疗机构联系地址</span> <input
											type="text"
											changelog="<s:property value="crmPatient.customer.street"/>"
											size="30" maxlength="50"
											id="crmPatient_customer_street" class="form-control"
											name="crmPatient.customer.street" title="医疗机构联系地址"
											value="<s:property value="crmPatient.customer.street"/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">科室电话 </span><input
											type="text"
											changelog="<s:property value="crmPatient.crmPhone"/>"
											size="20" maxlength="25"
											id="crmPatient_crmPhone"
											class="form-control"
											name="crmPatient.crmPhone" title="科室电话"
											value="<s:property value=" crmPatient.crmPhone "/>" />
									</div>
								</div>								
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">主治医生联系方式</span>
										<input type="text"
											changelog="<s:property value="crmPatient.customerDoctor.mobile"/>"
											size="20" maxlength="25"
											id="crmPatient_customerDoctor_mobile"
											onchange="checktelephone(this.value)" class="form-control"
											name="crmPatient.customerDoctor.mobile"
											title="<fmt:message key="biolims.common.attendingPhysicianContactInformation"/>"
											value="<s:property value=" crmPatient.customerDoctor.mobile "/>" />
									</div>
								</div>
							</div>
							<div class="row changeView2">
							<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">临床用药方案</span> <select
											class="form-control"
											changelog="<s:property value="crmPatient.medicationPlan"/>"
											lay-ignore="" name="crmPatient.medicationPlan"
											id="crmPatient_medicationPlan">
											<option value=""
												<s:if test='crmPatient.medicationPlan==""'>selected="selected"</s:if>>请选择</option>
											<option value="1"
												<s:if test='crmPatient.medicationPlan=="1"'>selected="selected"</s:if>>放疗</option>
											<option value="2"
												<s:if test='crmPatient.medicationPlan=="2"'>selected="selected"</s:if>>化疗</option>
											<option value="3"
												<s:if test='crmPatient.medicationPlan=="3"'>selected="selected"</s:if>>靶向药</option>
											<option value="4"
												<s:if test='crmPatient.medicationPlan=="4"'>selected="selected"</s:if>>手术</option>
											<option value="5"
												<s:if test='crmPatient.medicationPlan=="5"'>selected="selected"</s:if>>其他</option>
										</select>
									</div>
								</div>
								</div>
							
							<div class="row changeView2">
								<div class="col-xs-4">
									<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
								</div>
							</div>

							<br>
							<!-- 肿瘤类型及分期类型 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-minus" onclick="patientShowAndHide2(this)"></i>
											肿瘤类型及分期
										</h3>
									</div>
								</div>
							</div>
							<!-- 肿瘤类型 -->
							<div class="row">
						      <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.dicCancarTyp" /></span> 
											<input   name="crmPatient.tumourType.id"
											changelog="<s:property value="crmPatient.tumourType.id"/>"
											type="hidden" size="20" id="crmPatient_tumourType_id"
											value="<s:property value=" crmPatient.tumourType.id "/>"
											class="form-control" />
											<input  readonly="readonly" 
											changelog="<s:property value="crmPatient.tumourType.name"/>"
											type="text" size="20" id="crmPatient_tumourType_name"
											value="<s:property value=" crmPatient.tumourType.name "/>"
											class="form-control" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick='showcancerTypeTable()'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>

								<!-- 肿瘤分期 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.cancerInstalment" /></span> <input list="sjks"
											changelog="<s:property value="crmPatient.tumorStaging.id"/>"
											type="text" size="20" id="crmPatient_cancerInstalment_name"
											readonly="readonly"
											value="<s:property value=" crmPatient.tumorStaging.name "/>"
											class="form-control" /> <input type="hidden"
											id="crmPatient_cancerInstalment_id"
											name="crmPatient.tumorStaging.id"
											value="<s:property value="crmPatient.tumorStaging.id "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick='showTimesTable()'>
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
							</div>

							<div class="row">
								<!-- 肿瘤类型备注 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">肿瘤类型备注</span> <input
											type="text"
											changelog="<s:property value="crmPatient.zhongliuNote"/>"
											size="30" maxlength="50" class="form-control"
											id="crmPatient_zhongliuNote" name="crmPatient.zhongliuNote"
											title="肿瘤类型备注"
											value="<s:property value="crmPatient.zhongliuNote "/>" />
									</div>
								</div>
								<!-- 分期备注 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">肿瘤分期备注</span> <input
											type="text"
											changelog="<s:property value="crmPatient.tumorStagingNote"/>"
											size="30" maxlength="50" class="form-control"
											id="crmPatient_tumorStagingNote"
											name="crmPatient.tumorStagingNote" title="肿瘤分期备注"
											value="<s:property value=" crmPatient.tumorStagingNote "/>" />
									</div>
								</div>
							</div>
							
							
							<!--传染病检测(HIV和TPHA血样) -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-minus" onclick="patientShowAndHide2(this)"></i>
											传染病检测(HIV和TPHA血样) 
										</h3>
									</div>
								</div>
							</div>
								<div class="row">
								<!-- 乙肝HBV -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.HBV" /><img class="requiredimage"
											src="/images/required.gif"></span> <select
											class="form-control" lay-ignore=""
											name="crmPatient.hepatitisHbv" id="crmPatient_hepatitisHbv">
											<option value=""
												<s:if test='crmPatient.hepatitisHbv==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='crmPatient.hepatitisHbv=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='crmPatient.hepatitisHbv=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
								<!-- 丙肝HCV -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.HCV" /><img class="requiredimage"
											src="/images/required.gif"></span> <select
											class="form-control" lay-ignore=""
											name="crmPatient.hepatitisHcv" id="crmPatient_hepatitisHcv">
											<option value=""
												<s:if test='crmPatient.hepatitisHcv==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='crmPatient.hepatitisHcv=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='crmPatient.hepatitisHcv=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
								<!-- 艾滋HIV -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.HIV" /><img class="requiredimage"
											src="/images/required.gif"></span> <select
											class="form-control" lay-ignore=""
											name="crmPatient.HivVirus" id="crmPatient_HivVirus">
											<option value=""
												<s:if test='crmPatient.HivVirus==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='crmPatient.HivVirus=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='crmPatient.HivVirus=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<!-- 梅毒TPHA -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.TPHA" /><img class="requiredimage"
											src="/images/required.gif"></span> <select
											class="form-control" lay-ignore=""
											name="crmPatient.syphilis" id="crmPatient_syphilis">
											<option value=""
												<s:if test='crmPatient.syphilis==""'>selected="selected"</s:if>></option>
											<option value="0"
												<s:if test='crmPatient.syphilis=="0"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.feminine" />-</option>
											<option value="1"
												<s:if test='crmPatient.syphilis=="1"'>selected="selected"</s:if>><fmt:message
													key="biolims.common.masculine" />+</option>
										</select>
									</div>
								</div>
								<!-- 白细胞计数 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">白细胞计数<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text"
											changelog="<s:property value="crmPatient.whiteBloodCellNum"/>"
											size="30" maxlength="50" class="form-control"
											id="crmPatient_whiteBloodCellNum"
											name="crmPatient.whiteBloodCellNum" onchange="whiteBloodCellNum(this.value)"
											value="<s:property value=" crmPatient.whiteBloodCellNum "/>" />
										<%-- <span class="input-group-addon">(单位10^/L)</span> --%>
									</div>
								</div>
								<!-- 淋巴细胞百分比 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">淋巴细胞百分比<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text"
											changelog="<s:property value="crmPatient.percentageOfLymphocytes"/>"
											size="30" maxlength="50" class="form-control"
											id="crmPatient_percentageOfLymphocytes"
											name="crmPatient.percentageOfLymphocytes" onchange="percentageOfLymphocytes(this.value)"
											value="<s:property value=" crmPatient.percentageOfLymphocytes "/>" />
										<%-- <span class="input-group-addon">(单位%)</span>  --%>
									</div>
								</div>
							</div>

							<div class="row">
								<!-- 淋巴细胞 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.lymphoidCellSeries" /></span> <input
											type="text"
											changelog="<s:property value="crmPatient.lymphoidCellSeries"/>"
											size="30" maxlength="50" class="form-control"
											id="lymphoidCellSeries" name="crmPatient.lymphoidCellSeries"
											onchange="lymphoidCell(this.value)"
											value="<s:property value=" crmPatient.lymphoidCellSeries "/>" />
									</div>
								</div>
								<!-- 计算采血量 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.counterDrawBlood" /></span> <input type="text"
											changelog="<s:property value="crmPatient.counterDrawBlood"/>"
											size="30" maxlength="50" id="text70" onchange="shuzi(this.value)"
											name="crmPatient.counterDrawBlood" class="form-control"
											title="<fmt:message key="biolims.common.counterDrawBlood"/>"
											onchange="jisuan(this.value)"
											value="<s:property value=" crmPatient.counterDrawBlood "/>" />
									</div>
								</div>
								<!-- 采血管数 -->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">采血管数</span> <input type="text"
											changelog="<s:property value="crmPatient.heparinTube"/>"
											size="30" maxlength="50" class="form-control" id="text70"
											name="crmPatient.heparinTube" title="heparinTube"
											value="<s:property value=" crmPatient.heparinTube "/>" />
									</div>
								</div>
							</div>
							
							
							<!-- 家属信息 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-minus" onclick="patientShowAndHide2(this)"></i>
											家属信息
										</h3>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersOfTheContact" /></span> <input
											type="text"
											changelog="<s:property value="crmPatient.family"/>"
											size="20" maxlength="25" id="text48"
											name="crmPatient.family" class="form-control"
											title="<fmt:message key="biolims.common.familyMembersOfTheContact"/>"
											value="<s:property value=" crmPatient.family "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersOfTheContactPhoneNumber" /></span>

										<input type="text"
											changelog="<s:property value="crmPatient.familyPhone"/>"
											size="20" maxlength="25" class="form-control" id="text49"
											onchange="checktelephone(this.value)"
											name="crmPatient.familyPhone"
											title="<fmt:message key="biolims.common.familyMembersOfTheContactPhoneNumber"/>"
											value="<s:property value="crmPatient.familyPhone "/>" />
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.familyMembersAddress" /></span> <input
											type="text"
											changelog="<s:property value="crmPatient.familySite"/>"
											size="30" maxlength="50" class="form-control" id="text70"
											name="crmPatient.familySite"
											title="<fmt:message key="biolims.common.familyMembersAddress"/>"
											value="<s:property value=" crmPatient.familySite "/>" />
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.email" /> </span> <input type="text" size="50"
											maxlength="127" id="crmPatient_email"
											changelog="<s:property value="crmPatient.email"/>"
											name="crmPatient.email"
											title="<fmt:message key="biolims.common.email"/>"
											class="form-control"
											value="<s:property value="crmPatient.email"/>" />

									</div>
                                </div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.zipCode" /></span> <input type="text"
											changelog="<s:property value="crmPatient.zipCode"/>"
											size="30" maxlength="50" id="text70"
											name="crmPatient.zipCode" class="form-control"
											title="<fmt:message key="biolims.common.zipCode"/>"
											value="<s:property value="crmPatient.zipCode "/>" />

									</div>
								</div>
							</div>
							
							
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 10px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-minus" onclick="patientShowAndHide(this)"></i>
											<fmt:message key="biolims.common.relevantMedicalHistory" />
										</h3>
									</div>
								</div>
							</div>
							<div class="row changeView">
								<div class="col-xs-6">
									<input type="hidden" name="crmPatient.infectiousDiseases"
										changelog id="crmPatient_infectiousDiseases"
										value="<s:property value=" crmPatient.infectiousDiseases "/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.infectiousDiseases" /></span>
										<s:if test="crmPatient.infectiousDiseases==2">
											<input type="checkbox" value="2" note="4" changelog checked
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="4" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="4" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.infectiousDiseases==0">
											<input type="checkbox" value="2" note="4" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="4" changelog checked
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="4" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.infectiousDiseases==1">
											<input type="checkbox" value="2" note="4" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="4" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="4" changelog checked
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test='crmPatient.infectiousDiseases==null'>
											<input type="checkbox" value="2" note="4" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="4" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="4" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>

									</div>

								</div>
								<div class="col-xs-6" id="infectiousDiseasestext">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.situationOfInfectiousDiseases" /> </span> <input
											type="text" size="50" maxlength="127"
											id="crmPatient_infectiousDiseasestext"
											changelog="<s:property value="crmPatient.infectiousDiseasestext"/>"
											name="crmPatient.infectiousDiseasestext"
											title="<fmt:message key="biolims.common.situationOfInfectiousDiseases"/>"
											class="form-control"
											value="<s:property value="crmPatient.infectiousDiseasestext"/>" />

									</div>
								</div>
							</div>
							<div class="row changeView">
								<div class="col-xs-6">
									<input type="hidden" name="crmPatient.historyOfSmoking"
										changelog id="crmPatient_historyOfSmoking"
										value="<s:property value="crmPatient.historyOfSmoking"/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.smokingHistory" /></span>
										<s:if test="crmPatient.historyOfSmoking==2">
											<input type="checkbox" value="2" note="5" changelog checked
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="5" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="5" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.historyOfSmoking==0">
											<input type="checkbox" value="2" note="5" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="5" changelog checked
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="5" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.historyOfSmoking==1">
											<input type="checkbox" value="2" note="5" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="5" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="5" changelog checked
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test='crmPatient.historyOfSmoking==null'>
											<input type="checkbox" value="2" note="5" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="5" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="5" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>

									</div>

								</div>
								<div class="col-xs-6" id="historyOfSmoking">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.situationOfSmoking" /> </span> <input
											type="text" size="50" maxlength="127"
											id="crmPatient_historyOfSmokingtext"
											changelog="<s:property value="crmPatient.historyOfSmokingtext"/>"
											name="crmPatient.historyOfSmokingtext"
											title="<fmt:message key="biolims.common.situationOfSmoking"/>"
											class="form-control"
											value="<s:property value="crmPatient.historyOfSmokingtext"/>" />

									</div>
								</div>
							</div>
							<div class="row changeView">
								<div class="col-xs-6">
									<input type="hidden" name="crmPatient.environmentalExposures"
										changelog id="crmPatient_environmentalExposures"
										value="<s:property value=" crmPatient.environmentalExposures "/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.environmentalRiskPoison" /></span>
										<s:if test="crmPatient.environmentalExposures==2">
											<input type="checkbox" value="2" note="6" changelog checked
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="6" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="6" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.environmentalExposures==0">
											<input type="checkbox" value="2" note="6" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="6" changelog checked
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="6" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.environmentalExposures==1">
											<input type="checkbox" value="2" note="6" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="6" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="6" changelog checked
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test='crmPatient.environmentalExposures==null'>
											<input type="checkbox" value="2" note="6" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="6" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="6" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>

									</div>

								</div>
								<div class="col-xs-6" id="environmentalExposures">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.situationOfEnvironmentalRiskPoison" />
										</span> <input type="text" size="50" maxlength="127"
											id="crmPatient_environmentalExposurestext"
											changelog="<s:property value="crmPatient.environmentalExposurestext"/>"
											name="crmPatient.environmentalExposurestext"
											title="<fmt:message key="biolims.common.situationOfEnvironmentalRiskPoison"/>"
											class="form-control"
											value="<s:property value="crmPatient.environmentalExposurestext"/>" />

									</div>
								</div>
							</div>
							<div class="row changeView">
								<div class="col-xs-6">
									<input type="hidden" name="crmPatient.ysj" changelog
										id="crmPatient_ysj"
										value="<s:property value="crmPatient.ysj"/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.historyOfDrinking" /> </span>
										<s:if test="crmPatient.ysj==2">
											<input type="checkbox" value="2" note="7" changelog checked
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="7" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="7" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.ysj==0">
											<input type="checkbox" value="2" note="7" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="7" changelog checked
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="7" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.ysj==1">
											<input type="checkbox" value="2" note="7" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="7" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="7" changelog checked
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test='crmPatient.ysj==null'>
											<input type="checkbox" value="2" note="7" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="7" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="7" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>

									</div>

								</div>
								<div class="col-xs-6" id="ysj">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.historyOfDrinkingSituation" /> </span> <input
											type="text" size="50" maxlength="127" id="crmPatient_ysjtext"
											changelog="<s:property value="crmPatient.ysjtext"/>"
											name="crmPatient.ysjtext"
											title="<fmt:message key="biolims.common.historyOfDrinkingSituation"/>"
											class="form-control"
											value="<s:property value="crmPatient.ysjtext"/>" />

									</div>
								</div>
							</div>
							<div class="row changeView">
								<div class="col-xs-6">
									<input type="hidden" name="crmPatient.otherDiseases" changelog
										id="crmPatient_otherDiseases"
										value="<s:property value=" crmPatient.otherDiseases "/>" />
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.otherDiseases" /></span>
										<s:if test="crmPatient.otherDiseases==2">
											<input type="checkbox" value="2" note="8" changelog checked
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="8" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="8" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.otherDiseases==0">
											<input type="checkbox" value="2" note="8" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="8" changelog checked
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="8" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test="crmPatient.otherDiseases==1">
											<input type="checkbox" value="2" note="8" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="8" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="8" changelog checked
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>
										<s:if test='crmPatient.otherDiseases==null'>
											<input type="checkbox" value="2" note="8" changelog
												title=<fmt:message key="biolims.common.unknown" />>
											<input type="checkbox" value="0" note="8" changelog
												title=<fmt:message key="biolims.common.no"/>>
											<input type="checkbox" value="1" note="8" changelog
												title=<fmt:message key="biolims.common.yes"/>>
										</s:if>

									</div>

								</div>
								<div class="col-xs-6" id="otherDiseases">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.common.historyOfOtherDiseases" /> </span> <input
											type="text" size="50" maxlength="127"
											id="crmPatient_otherDiseasestext"
											changelog="<s:property value="crmPatient.otherDiseasestext"/>"
											name="crmPatient.otherDiseasestext"
											title="<fmt:message key="biolims.common.historyOfOtherDiseases"/>"
											class="form-control"
											value="<s:property value="crmPatient.otherDiseasestext"/>" />

									</div>
								</div>
							</div>
							<div class="row changeView">
								<div class="col-xs-6">
									<div class="input-group">
										<button type="button" class="btn btn-info" onclick="fileUp2()">
											<fmt:message key="biolims.common.uploadAttachment" />
										</button>
										<span class="text"><fmt:message
												key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum1}&nbsp;&nbsp;<fmt:message
												key="biolims.common.attachment" /></span>

										<button type="button" class="btn btn-info"
											onclick="fileView2()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
									</div>
								</div>
							</div>


							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.customFields" />

										</h3>
									</div>
								</div>
							</div>
							<!--
                                	作者：offline
                                	时间：2018-03-19
                                	描述：自定义字段
                                -->
							<div id="fieldItemDiv"></div>


							<input type="hidden" name="crmLinkManItemJson"
								id="crmLinkManItemJson" value="" /> <input type="hidden"
								id="id_parent_hidden"
								value="<s:property value=" crmPatient.id "/>" /> <input
								type="hidden" id="fieldContent" name="crmPatient.fieldContent"
								value="<s:property value=" crmPatient.fieldContent "/>" /> <input
								type="hidden" id="changeLog" name="changeLog" />
						</form>
					</div>



					<!--table表格-->

					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="crmLinkManItemTable" style="font-size: 14px;">
						</table>
					</div>
					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="crmSampleOrderGrid" style="font-size: 14px;">
						</table>
					</div>
					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="crmSampleMainGrid" style="font-size: 14px;">
						</table>
					</div>
					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="crmCustormerGrid" style="font-size: 14px;">
						</table>
					</div>
				</div>
				<!-- <div class="box-footer">
						<div class="pull-right">
							<button type="button" class="btn btn-primary" id="pre">上一步</button>
							<button type="button" class="btn btn-primary" id="next">下一步</button>
						</div>
					</div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/patient/crmPatientEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/patient/crmLinkManItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/patient/crmSampleOrder.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/patient/crmSampleInfo.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/customer/patient/crmShowCustormList.js"></script>
</body>
</html>