<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/customer/patient/crmFamilyPatientShipEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  ><fmt:message key="biolims.common.serialNumber"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="crmFamilyPatientShip_id"
                   	 name="crmFamilyPatientShip.id" title="<fmt:message key="biolims.common.serialNumber"></fmt:message>"
                   	   
					value="<s:property value="crmFamilyPatientShip.id"/>"
                   	   
                   	  />
                   	  
                   	</td>
				
			
               	 	<td class="label-title" ><fmt:message key="biolims.sample.personShipName"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="crmFamilyPatientShip_name"
                   	 name="crmFamilyPatientShip.name" title="<fmt:message key="biolims.sample.personShipName"></fmt:message>"
                   	   
	value="<s:property value="crmFamilyPatientShip.name"/>"
                   	  />
                   	  
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.state"></fmt:message></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	  <td align="left"  >
                   	<select id="crmFamilyPatientShip._state" name="crmFamilyPatientShip.state" style="width:169" class="input-10-length" >
								<option value="1" <s:if test="crmFamilyPatientShip.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"></fmt:message></option>
								<option value="0" <s:if test="crmFamilyPatientShip.state==0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"></fmt:message></option>
					</select>
                   	  
                   	</td>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="crmFamilyPatientShip.id"/>" />
            </form>
            
        	</div>
	</body>
	</html>
