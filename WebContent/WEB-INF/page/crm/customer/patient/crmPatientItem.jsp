﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/js/crm/customer/patient/crmPatientItem.js"></script>
</head>
<body>
		<div id="bat_effectOfProgressSpeed_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.drugEffectProgress"/></span></td>
                <td><select id="effectOfProgressSpeed"  style="width:100">
               			<option value="1"><fmt:message key="biolims.common.rapidProgress"/></option>
    					<option value="2"><fmt:message key="biolims.common.localProgress"/></option>
    					<option value="3"><fmt:message key="biolims.common.slowProgress"/></option>
					</select>
                </td>
			</tr>
		</table>
		</div>
	<div id="crmPatientItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
</body>
</html>
