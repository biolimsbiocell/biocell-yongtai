<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
.tablebtns{
				float:right;
			}
.dt-buttons{
				margin:0;
				float:right;
			}
.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
</style>
</head>
<body>
	<input type="hidden" id="code" value="${requestScope.name}">
	<%-- <div class="col-md-4 col-sm-4 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">姓名</span> 
			<input type="text" size="20" maxlength="25" id="name" name="name" class="form-control"  placeholder="姓名查询" >
		</div>
		
	</div> --%>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">筛选号</span> 
			<input type="text" size="20" maxlength="25" id="filtrateCode" name="filtrateCode" searchname="filtrateCode" class="form-control" placeholder="筛选号查询" >
		</div>
		
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="input-group">
			<input style="text-align: center;" type="button" class="form-control btn-success" onclick="query()" value="查找">
		</div>
		
	</div>
	<div id="crmPatientSelectTable" >
		<table class="table table-hover table-bordered table-condensed"
			id="addPatientTable" style="font-size: 12px;"></table>
	</div>
<script type="text/javascript" src="${ctx}/js/crm/customer/patient/crmPatientSelectTable.js"></script>
</body>
</html>