<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/crm/customer/patient/crmShowCustormList.js "></script>
</head>
<body>
	<div id="crmCustormerdiv"></div>
		<select id="grid_is_fee" style="display:none">
		<option value="">请选择</option>
		<option value="1">已付费</option>
		<option value="0">未付费</option>
		<option value="2">取消</option>
		<option value="3">退费</option>
		<option value="5">月结</option>
		<option value="6">延迟收费</option>
	</select>
</body>
</html>


