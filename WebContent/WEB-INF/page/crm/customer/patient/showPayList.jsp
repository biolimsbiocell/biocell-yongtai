<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>样本接收</title> -->
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/js/crm/customer/patient/showPayList.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
	<input type="hidden"  id="state_sam" value="<%=request.getAttribute("crmPatient.state") %>" />
<div>
<div  class="mainlistclass" id="markup">
	<form name='excelfrm' action='/common/exportexcel.jsp' method='POST'>
	<input type='hidden' name='gridhtm' value=''/></form>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">
	<table cellspacing="0" cellpadding="0" class="toolbarsection">
	<tr>
	<td >
	<label class="text label" title='<fmt:message key="biolims.common.inputTheQueryConditions" />'>
	<fmt:message key="biolims.common.selectCollectionStatus" />
	</label>
	</td>
	<td>
	
	
	<select id="mainType" name="isFee" class="input-20-length"  searchField="true" >
	<option value=""><fmt:message key="biolims.common.selectCollectionStatus" /></option>
	<!-- <option value="0" >未收费</option> -->
	<option value="0" ><fmt:message key="biolims.common.cashPaid" /></option>
	<option value="1" ><fmt:message key="biolims.common.theAgentHasPaid" /></option>
	<!-- <option value="5" >月结</option>
	<option value="6" >延迟收费</option> -->
	<option value="2" ><fmt:message key="biolims.common.cancel" /></option>
	<option value="3" ><fmt:message key="biolims.common.refund" /></option>
	</select>
	</td>
	<td >
<!-- 	<label class="text label" title= -->
<%-- '<fmt:message key="biolims.common.inputTheQueryConditions" />'> --%>
<%-- <fmt:message key="biolims.common.dateOfCollection" /> --%>
<!-- 	</label> -->
	</td>
<!-- 	<td> -->
<!-- 		<input type="text" class="Wdate" readonly="readonly" id="sampleDate" name="sampleDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> -->
<!-- 		<input type="hidden" id="sampleDate1" name="consumptionDate##@@##1"  searchField="true" /></td> -->
<%-- 	<td class="label-title"><fmt:message key="biolims.common.to" /></td> --%>
<!-- 	<td> -->
<!-- 		<input type="text" class="Wdate" readonly="readonly" id="endAcceptDate" name="endAcceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> -->
<!-- 		<input type="hidden" id="sampleDate2" name="consumptionDate##@@##2"  searchField="true" /> -->
<!-- 	</td> -->
	

		<td >
	<label class="text label" title='<fmt:message key="biolims.common.inputTheQueryConditions" />'>
	<fmt:message key="biolims.common.patientName" />
	</label>
	</td>
	<td>
	<input type="text"  name="sampleOrder.name" id="patient" searchField="true" >
	</td>
<!-- 	<td > -->
<%-- 	<label class="text label" title='<fmt:message key="biolims.common.inputTheQueryConditions" />'> --%>
<%-- 	<fmt:message key="biolims.common.sampleCode" /> --%>
<!-- 	</label> -->
<!-- 	</td> -->
<!-- 	<td> -->
<!-- 	<input type="text"  name="crmPatient.code" id="sampleCode" searchField="true" > -->
<!-- 	</td> -->
		<td >
	<label class="text label" title='<fmt:message key="biolims.common.inputTheQueryConditions" />'>
	<fmt:message  key="biolims.common.productName"/>
	</label>
	</td>
	<td>
	<input type="text"  name="sampleOrder.productName" id="item"  searchField="true" >
	</td>
		<td >
	<label class="text label" title='<fmt:message key="biolims.common.inputTheQueryConditions" />'>
	<fmt:message key="biolims.common.salesman" />
	</label>
	</td>
	<td>
	<input type="text"   name="sampleOrder.commissioner.name" searchField="true"  id="user">
	</td>
	
	<td>
	<input type="button" onClick="selectCustomer();" value=
'<fmt:message key="biolims.common.find" />'>
	</td>
	</tr>
	</table>
	<div id="show_detail_grid_div"></div>
	<div id="bat_div" style="display: none ;height: 438px"  >
		<textarea id="bat_text" style="width:390px;height: 339px"></textarea>
	</div>
	<div id="many_bat_div" style="display: none">
			<div class="ui-widget;">
				<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
				</div>
			</div>
			<textarea id="many_bat_text" style="width:650px;height: 339px"></textarea>
	</div>
	<div id="customer_info_bat_div" style="display: none">
		<textarea id="customer_info_bat_text" style="width:625px;height: 339px"></textarea>
	</div>
  <select id="grid_contract" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value="1"><fmt:message key="biolims.common.yes" /></option>
			<option value="0"><fmt:message key="biolims.common.no" /></option>
		</select> 
        <select id="grid_is_good" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value="1"><fmt:message key="biolims.common.yes" /></option>
			<option value="0"><fmt:message key="biolims.common.no" /></option>
		</select> <select id="grid_is_pass" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value="1"><fmt:message key="biolims.common.yes" /></option>
			<option value="0"><fmt:message key="biolims.common.no" /></option>
		</select> <select id="grid_is_transport" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value=
'<fmt:message key="biolims.common.yes" />'
><fmt:message key="biolims.common.yes" /></option>
			<option value='<fmt:message key="biolims.common.no" />'><fmt:message key="biolims.common.no" /></option>
		</select> <select id="grid_is_label" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value=
'<fmt:message key="biolims.common.yes" />'
><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value='<fmt:message key="biolims.common.no" />'><fmt:message key="biolims.common.no" /></option>
		</select> 
		<select id="grid_month_fee" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value="1"><fmt:message key="biolims.common.pleaseSelect" /></option>
			<option value="0"><fmt:message key="biolims.common.no" /></option>
		</select>
		<select id="grid_fee_way" style="display:none">
		<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
		<option value="1"><fmt:message key="biolims.common.POSTransfer" /></option>
		<option value="0"><fmt:message key="biolims.common.bankTransfer" /></option>
		<option value="2"><fmt:message key="biolims.common.cash" /></option>
	</select>
		<select id="grid_is_fee" style="display:none">
		<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
		<option value="0"><fmt:message key="biolims.common.cashPaid" /></option>
		<option value="1"><fmt:message key="biolims.common.theAgentHasPaid" /></option>
		<option value="2"><fmt:message key="biolims.common.cancel" /></option>
		<option value="3"><fmt:message key="biolims.common.refund" /></option>
		<!-- <option value="5">月结</option>
		<option value="6">延迟收费</option> -->
	</select>
</div>
<div id="bat_sampleInfo_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.whetherItHasBeenCharged" /></span></td>
				<td><select id="isFee">
						<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
						<option value="1"><fmt:message key="biolims.common.theAgentHasPaid" /></option>
						<option value="0"><fmt:message key="biolims.common.cashPaid" /></option>
						<option value="2"><fmt:message key="biolims.common.cancel" /></option>
						<option value="3"><fmt:message key="biolims.common.refund" /></option>
<%-- 						<option value="5"><fmt:message key="biolims.common.monthlyStatement" /></option> --%>
<%-- 						<option value="6"><fmt:message key="biolims.common.delayCharge" /></option> --%>
				</select></td>
			</tr>
			
		</table>
	</div>
	<div id="bat_fee_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><fmt:message key="biolims.common.arrivalAmount" /></td>
				<td><input id="feeReal"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_invoice_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><fmt:message key="biolims.common.invoiceNumbe" /></td>
				<td><input id="invoiceNumber"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_invoiceName_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><fmt:message key="biolims.common.invoiceHeader" /></td>
				<td><input id="invoiceName"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_consumption_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><fmt:message key="biolims.common.dateOfCollection" /></td>
				<td ><input type="text" size="12" maxlength="100" id="skrq"/>
				</td>
			</tr>
		</table>
	</div>
	<div id="bat_fprq_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><fmt:message key="biolims.common.invoiceDate" /></td>
				<td ><input type="text" size="12" maxlength="100" id="kprq"/>
				</td>
			</tr>
		</table>
	</div>
</div>
<form name='excelfrm1' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm1' name='gridhtm' value=''/></form>
</body>
</html>