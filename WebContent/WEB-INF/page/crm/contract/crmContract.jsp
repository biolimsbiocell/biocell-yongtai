﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/contract/crmContract.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="18" id="crmContract_id"
                   	 name="id" searchField="true" title="编码"    />
                   	</td>
               	 	
               	 	<td class="label-title" >合同名称</td>
                   	<td align="left"  >
					<input type="text" size="30" maxlength="60" id="crmContract_name"
                   	 name="name" searchField="true" title="合同名称"    /> 
                   	</td>
 			</tr>
			<tr>                      	
			
					<td class="label-title" >合同总金额</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="10" id="crmContract_money"
                   	 name="money" searchField="true" title="合同总金额"    />
                   	</td>
                   	
                   	<td class="label-title">币种</td>
					
					<td align="left"><select id="crmContract_currency"
						name="currency" class="input-10-length" searchField="true"
						style="width: 152px;">
							<option value="人民币">人民币</option>
							<option value="美元" >美元</option>
							<option value="" selected="selected">请选择</option>
					</select>
					</td>
           
 			</tr>
 			<tr>
 					<g:LayOutWinTag buttonId="showcrmCustomer" title="选择委托人"
						hasHtmlFrame="true"
						html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
						isHasSubmit="false" functionName="CrmCustomerFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_crmCustomer').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmCustomer_name').value=rec.get('name');" />
					
               	 	<td class="label-title" >委托人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmCustomer_name" searchField="true"  name="crmCustomer.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmCustomer" name="crmSaleOrder.crmCustomer.id"  value="" > 
 						<img alt='选择委托人' id='showcrmCustomer' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
 					<!-- <td class="label-title" >委托人</td>
                   	<td align="left"  >
					<input type="text" size="30" maxlength="60" id="crmContract_crmCustomer_name"
                   	 name="crmCustomer.name" searchField="true" title="委托人"    /> 
                   	</td> -->
                   	
                   	<td class="label-title" >委托单位</td>
                   	<td align="left"  >
					<input type="text" size="30" maxlength="60" id="crmContract_client"
                   	 name="client" searchField="true" title="委托单位"    /> 
                   	</td>
 			</tr>
			<tr>
					<!--<td class="label-title" >委托人单位</td>
                   	<td align="left"  >
					<input type="text" size="30" maxlength="60" id="crmContract_crmCustomerDept_name"
                   	 name="crmCustomerDept.name" searchField="true" title="委托人单位"    /> 
                   	</td>-->
                   	
					<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
						hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false"		functionName="showmanagerFun" 
						documentId="crmContract_manager"
						documentName="crmContract_manager_name"
					 />
               	 	<td class="label-title" >客户经理</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_manager_name" searchField="true"  name="manager.name"  value="" class="text input" />
 						<input type="hidden" id="crmContract_manager" name="crmContract.manager.id"  value="" > 
 						<img alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
             <tr>       	                      	
               	 	<td class="label-title" >合同签订日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startwriteDate" name="startwriteDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="writeDate1" name="writeDate##@@##1"  searchField="true" /> -
						<input type="text" class="Wdate" readonly="readonly" id="endwriteDate" name="endwriteDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="writeDate2" name="writeDate##@@##2"  searchField="true" />
                   	</td>
 			</tr>
 			<tr>
 					<td class="label-title" >合同开始日期</td>
                   	<td align="left"  >
 						<!--<input type="text" class="Wdate" readonly="readonly" id="crmContract_startDate" name="startDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate1" name="startDate##@@##1"  searchField="true" /> -->
						
 						<input type="text" class="Wdate" readonly="readonly" id="startstartDate" name="startstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate1" name="startDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endstartDate" name="endstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate2" name="startDate##@@##2"  searchField="true" />
 						

                   	</td>
               	 	<td class="label-title" >合同结束日期</td>
                   	<td align="left"  >
 						<!--  <input type="text" class="Wdate" readonly="readonly" id="crmContract_endDate" name="endDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" /> -->
 						
 						<input type="text" class="Wdate" readonly="readonly" id="startendDate" name="startendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endendDate" name="endendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate2" name="endDate##@@##2"  searchField="true" />
                   	</td>
 			</tr>
			<tr>
					<td class="label-title" >实际合同开始日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startfactStartDate" name="startfactStartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="factStartDate1" name="factStartDate##@@##1"  searchField="true" /> -
						<input type="text" class="Wdate" readonly="readonly" id="endfactStartDate" name="endfactStartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="factStartDate2" name="factStartDate##@@##2"  searchField="true" />
                   	</td>
                   	<td class="label-title" >实际合同结束日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startfactEndDate" name="startfactEndDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="factEndDate1" name="factEndDate##@@##1"  searchField="true" /> -
						<input type="text" class="Wdate" readonly="readonly" id="endfactEndDate" name="endfactEndDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="factEndDate2" name="factEndDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
			<tr>                  	                      	
               	 	<!-- <td class="label-title" >合同开始日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="crmContract_startDate" name="startDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate1" name="startDate##@@##1"  searchField="true" />
						
 						<input type="text" class="Wdate" readonly="readonly" id="startstartDate" name="startstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate1" name="startDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endstartDate" name="endstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate2" name="startDate##@@##2"  searchField="true" />
 						

                   	</td>
               	 	<td class="label-title" >合同结束日期</td>
                   	<td align="left"  >
 						 <input type="text" class="Wdate" readonly="readonly" id="crmContract_endDate" name="endDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" />
 						
 						<input type="text" class="Wdate" readonly="readonly" id="startendDate" name="startendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endendDate" name="endendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate2" name="endDate##@@##2"  searchField="true" />
                   	</td> -->
 			</tr>
			
			<tr>        	  
					<g:LayOutWinTag buttonId="showtype" title="选择合同类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="htlx" hasSetFun="true"
						documentId="crmContract_type"
						documentName="crmContract_type_name"
					/>
               	 	<td class="label-title" >合同类型</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="crmContract_type" name="crmContract.type.id"  value="" > 
 						<img alt='选择合同类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	<g:LayOutWinTag buttonId="showsource" title="选择合同来源"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectSource" hasSetFun="true"
						documentId="crmContract_chanceSource"
						documentName="crmContract_chanceSource_name" />
					<td class="label-title">合同来源</td>
					<td align="left">
						<input type="text" size="20"
							readonly="readOnly" id="crmContract_chanceSource_name" name="chanceSource.name" searchField="true"
							value="" /> 
						<input type="hidden" id="crmContract_chanceSource"
							name="crmContract.chanceSource.id"
							value=""> 
						<img alt='选择项目来源' id='showsource' src='${ctx}/images/img_lookup.gif'
							class='detail' />
					</td>
           </tr>
           
			<tr>
				<td class="label-title" >基因ID</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="crmContract_geneId"
                   	 name="geneId" searchField="true" title="基因ID" />
                   	</td>
                   	
				<td class="label-title" >基因正式名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="crmContract_geneName"
                   	 name="geneName" searchField="true" title="基因正式名称" />
                   	</td>

				<!-- <td class="label-title" >基因别名</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="crmContract_geneByName"
                   	 name="geneByName" searchField="true" title="基因别名" />
                   	</td> -->
            </tr>

			<tr>	
					<td class="label-title" >基因别名</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="crmContract_geneByName"
                   	 name="geneByName" searchField="true" title="基因别名" />
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content1</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content1"
                   	 name="content1" searchField="true" title="content1"   style="display:none"    />
                
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content2</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content2"
                   	 name="content2" searchField="true" title="content2"   style="display:none"    />
            
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content3</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content3"
                   	 name="content3" searchField="true" title="content3"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content4</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content4"
                   	 name="content4" searchField="true" title="content4"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content5</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content5"
                   	 name="content5" searchField="true" title="content5"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content6</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content6"
                   	 name="content6" searchField="true" title="content6"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content7</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content7"
                   	 name="content7" searchField="true" title="content7"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content8</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmContract_content8"
                   	 name="content8" searchField="true" title="content8"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content9</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent9" name="startcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content91" name="content9##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent9" name="endcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content92" name="content9##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content10</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent10" name="startcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content101" name="content10##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent10" name="endcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content102" name="content10##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content11</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent11" name="startcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content111" name="content11##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent11" name="endcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content112" name="content11##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content12</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent12" name="startcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content121" name="content12##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent12" name="endcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:block;"  />
						<input type="hidden" id="content122" name="content12##@@##2"  searchField="true" />
                  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_crmContract_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_crmContract_tree_page"></div>
</body>
</html>



