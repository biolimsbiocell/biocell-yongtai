﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner,.x-grid3-hd-inner {
	overflow: hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
	padding: 3px 3px 3px 5px;
	white-space: normal !important;
}
</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/crm/contract/crmContrctCollectItem.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/>
	</form>
	<form name="form1" id="form1" method="post">
		<input type="hidden" name="crmContrctCollectItemJson"
			id="crmContrctCollectItemJson" value="" />
		<div id="crmContrctCollectdiv"></div>
		<div id="bat_uploadcsv_div" style="display: none">
			<input type="file" name="file" id="file-uploadcsv">上传CSV文件
		</div>

		<div id="collect_data_div" style="display: none">
			<input type="hidden" id="extJsonDataString" name="extJsonDataString">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>

					<td><label class="text label" title="输入查询条件"> 回款类型 </label></td>
<!-- 					<td><input type="text" name="crmContract.collectType"
						id="crmContract-collectType" searchField="true"></td> -->
					<td align="left">
						<select id="collectType" name="collectType"
							class="input-10-length" style="width: 130px;"searchField="true">
								<option value="">所有</option>
								<option value="0">银行转账</option>
								<option value="1">支票</option>
								<option value="2">现金</option>
						</select>
					</td>
					
					<td><label class="text label" title="输入查询条件"> 回款单位 </label></td>
					<td>
						<input type="text" name="collectDept" id="collectDept"
							searchField="true">
					</td>
				</tr>
				<tr>
					<td class="label-title">回款日期</td>
					<td align="left"><input type="text" class="Wdate"
							readonly="readonly" id="startreceivedDate" name="startreceivedDate"
							onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
							type="hidden" id="receivedDate1" name="receivedDate##@@##1"
							searchField="true" /> - 
							<input type="text" class="Wdate"readonly="readonly" id="endreceivedDate" name="endreceivedDate"
							onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
							type="hidden" id="receivedDate2" name="receivedDate##@@##2"
							searchField="true" />
					</td>
						
					<td><label class="text label" title="输入查询条件"> 回款金额 </label></td>
					<td><input type="text" name="money" id="money"
						searchField="true">
					</td>
				</tr>
				<tr>
					<td><label class="text label" title="输入查询条件"> 备注 </label></td>
					<td><input type="text" name="note" id="note"
						searchField="true"></td>
				</tr>			
				<tr>	
					<td><label class="text label" title="输入查询条件"> 发票状态 </label></td>
					<!-- td><input type="text" name="crmContract.billState"
						id="crmContract-billState" searchField="true"></td> -->
					<td align="left"><select id="billState" name="billState"
						class="input-10-length" style="width: 130px;"searchField="true">
							<option value="">所有</option>
							<option value="1">已开</option>
							<option value="0">未开</option>
					</select></td>
					
					<td><label class="text label" title="输入查询条件"> 发票号 </label></td>
					<td><input type="text" name="billId" id="billId"
						searchField="true"></td>
				</tr>
				
			</table>
		</div>


	</form>
</body>
</html>


