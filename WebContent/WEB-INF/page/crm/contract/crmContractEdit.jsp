﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="/operfile/initFileList.action?modelType=crmContract&id=${crmContract.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/crm/contract/crmContractEdit.js"></script>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<%--  <script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script> --%>
	<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass" width="100%">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
		<input type="hidden" id="crmContract_projectStart" name="crmContract.projectStart.id" value="<s:property value="crmContract.projectStart.id"/>">
		<input type="hidden" size="20" maxlength="18" id="crmContract_conStateName" 
			   name="crmContract.conStateName" value="<s:property value="crmContract.conStateName"/>"  />
			 <table width="100%" class="section_table" cellpadding="0" >
		 		<tbody>
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">合同基本信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody> 
			</table>			
			<table class="frame-table">
				<tr>
					<td class="label-title">编码</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="18"
						id="crmContract_id" name="crmContract.id" title="编码"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="crmContract.id"/>" />
					</td>
					
					<td class="label-title">合同名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="25" maxlength="120"
						id="crmContract_name" name="crmContract.name" title="${crmContract.name }"
						value="<s:property value="crmContract.name"/>" /><!-- onblur="nameDuplicateCheck()" -->
						<!-- <input type="button" onclick="nameDuplicateCheck()" value="验" class='detail' /> -->
					</td>
					
					<g:LayOutWinTag buttonId="showtype1" title="选择合同类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="htlx" hasSetFun="true"
						documentId="crmContract_type" documentName="crmContract_type_name" />
						
					<td class="label-title">合同类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' />
					</td>
					<td align="left"><input type="text" size="20"
						 id="crmContract_type_name"
						value="<s:property value="crmContract.type.name"/>" /> <input
						type="hidden" id="crmContract_type" name="crmContract.type.id"
						value="<s:property value="crmContract.type.id"/>"> <img
						alt='选择合同类型' id='showtype1' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showparent" title="选择父级合同"
						hasHtmlFrame="true" width="600" height="500"
						html="${ctx}/crm/contract/crmContract/crmContractSelect.action"
						isHasSubmit="false" functionName="CrmContractFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmContract_parent').value=rec.get('id');
						document.getElementById('crmContract_parent_name').value=rec.get('name');" />
						
					<td class="label-title">父级合同</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmContract_parent_name"
						value="<s:property value="crmContract.parent.name"/>" /> <input
						type="hidden" id="crmContract_parent" name="crmContract.parent.id"
						value="<s:property value="crmContract.parent.id"/>"> <img
						alt='选择父级合同' id='showparent' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
							
					<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
						hasHtmlFrame="true" hasSetFun="true"
						width="900" height="500"
						html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
						functionName="showmanagerFun" documentId="crmContract_manager"
						documentName="crmContract_manager_name" />
						
					<td class="label-title">客户经理</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmContract_manager_name"
						value="<s:property value="crmContract.manager.name"/>" /> <input
						type="hidden" id="crmContract_manager"
						name="crmContract.manager.id"
						value="<s:property value="crmContract.manager.id"/>"> <img
						alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
							
					<g:LayOutWinTag buttonId="showsource" title="选择合同来源"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectSource" hasSetFun="true"
						documentId="crmContract_chanceSource"
						documentName="crmContract_chanceSource_name" />
						
					<td class="label-title">合同来源</td>
					<td class="requiredcolumn" nowrap width="10px">
						<img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmContract_chanceSource_name"
						value="<s:property value="crmContract.chanceSource.name"/>" /> <input
						type="hidden" id="crmContract_chanceSource"
						name="crmContract.chanceSource.id"
						value="<s:property value="crmContract.chanceSource.id"/>"> 
						<img alt='选择项目来源' id='showsource' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
						
							<!-- <td class="label-title">受托方（乙方）</td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td align="left">
								<select id="crmContract_organization" name="crmContract.organization" class="input-30-length">
									<option value=""
										<s:if test="crmContract.organization==''">selected="selected"</s:if>></option>
									<option value="北京百奥赛图基因生物技术有限公司"
										<s:if test="crmContract.organization=='北京百奥赛图基因生物技术有限公司'">selected="selected"</s:if>>北京百奥赛图基因生物技术有限公司</option>
									<option value="百奥赛图江苏基因生物技术有限公司"
										<s:if test="crmContract.organization=='百奥赛图江苏基因生物技术有限公司'">selected="selected"</s:if>>百奥赛图江苏基因生物技术有限公司</option>
									<option value="Biocytogen LLC."
										<s:if test="crmContract.organization=='Biocytogen LLC.'">selected="selected"</s:if>>Biocytogen LLC.</option>
								</select>
							</td> -->
						</tr>
					<tr>
					<td class="label-title">受托方（乙方）</td>
							<td class="requiredcolumn" nowrap width="10px"></td>
							<td align="left">
								<select id="crmContract_organization" name="crmContract.organization" class="input-30-length">
									<option value="长和生物技术有限公司"
										<s:if test="crmContract.organization=='长和生物技术有限公司'">selected="selected"</s:if>>长和生物技术有限公司</option>
									<!-- <option value="百奥赛图江苏基因生物技术有限公司"
										<s:if test="crmContract.organization=='百奥赛图江苏基因生物技术有限公司'">selected="selected"</s:if>>百奥赛图江苏基因生物技术有限公司</option> -->
								</select>
							</td>
					
					<td class="label-title">签订日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_writeDate" name="crmContract.writeDate"
						title="签订日期" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.writeDate" format="yyyy-MM-dd"/>" />
					</td>

					<!-- readonly = "readOnly" class="text input readonlytrue" -->
					<td class="label-title">合同开始日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_startDate" name="crmContract.startDate"
						title="合同开始日期" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.startDate" format="yyyy-MM-dd"/>" />
					</td>
					<%-- <td class="label-title">合同结束日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_endDate" name="crmContract.endDate" title="合同结束日期"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.endDate" format="yyyy-MM-dd"/>" />
					</td> --%>
				</tr>
				<tr>
					<td class="label-title">合同结束日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_endDate" name="crmContract.endDate" title="合同结束日期"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.endDate" format="yyyy-MM-dd"/>" />
					</td>
					
					<td class="label-title">实际合同开始日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_factStartDate" name="crmContract.factStartDate"
						title="合同开始日期" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.factStartDate" format="yyyy-MM-dd"/>" />
					</td>
					<td class="label-title">实际合同结束日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_factEndDate" name="crmContract.factEndDate" title="合同结束日期"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.factEndDate" format="yyyy-MM-dd"/>" />
					</td>
				</tr>
				<tr>
					
				
					<td class="label-title">合同邮寄日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_toDate" name="crmContract.toDate" title="合同邮寄日期"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.toDate" format="yyyy-MM-dd"/>" />
					</td>
					<td class="label-title">合同返回日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_fromDate" name="crmContract.fromDate" title="合同返回日期"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.fromDate" format="yyyy-MM-dd"/>" />
					</td>
					<td class="label-title">合同签订状态</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<%-- <s:if test="crmContract.toDate == ''  && crmContract.fromDate == ''">
							<input type="text" value="签订中"/>
						</s:if> --%>
						<s:if test="crmContract_fromDate == '' && crmContract_toDate == '' " >
							<input type="text" value=""/>
						</s:if>
						<s:if test="crmContract_toDate != ''  && crmContract_fromDate==''">
							<input type="text" value="签订中..."/>
						</s:if>
						<s:elseif test="crmContract_fromDate != '' && crmContract_toDate != ''" >
							<input type="text" value="已签订"/>
						</s:elseif>
						
						<%--<select id="crmContract_conStateName" name="crmContract.conStateName" class="input-10-length">
							<option value="空白" selected="selected"></option>
							<option value="签订中"  <s:if test="crmContract_fromDate == '' && crmContract_toDate == '' ">selected="selected"</s:if>>签订中</option>
							<option value="签订中"  <s:if test="crmContract_toDate != ''  && crmContract_fromDate=='' ">selected="selected"</s:if>>签订中</option>
							<option value="已签订"  <s:if test="crmContract_fromDate != '' && crmContract_toDate != '' ">selected="selected"</s:if>>已签订</option>
						</select>--%>
					</td>	
				</tr>
				<tr>
				<g:LayOutWinTag buttonId="showconState" title="选择合同实施状态"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="conState" hasSetFun="true"
						documentId="crmContract_conState"
						documentName="crmContract_conState_name" />
					<td class="label-title">合同实施状态</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmContract_conState_name"
						value="<s:property value="crmContract.conState.name"/>" /> <input
						type="hidden" id="crmContract_conState"
						name="crmContract.conState.id"
						value="<s:property value="crmContract.conState.id"/>"> <img
						alt='选择合同实施状态' id='showconState'
						src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
					<td class="label-title">是否备案</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<select id="crmContract_isRecord" name="crmContract.isRecord" class="input-10-length"
							    style="width: 152px;">
							<option value="" ></option>
							<option value="0"
								<s:if test="crmContract.isRecord==0">selected="selected"</s:if>>否</option>
							<option value="1"
								<s:if test="crmContract.isRecord==1">selected="selected"</s:if>>是</option>
						</select>
					</td>
					
					<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						width="900" height="500"
						extRec="rec"
						extStr="document.getElementById('crmContract_createUser').value=rec.get('id');
						document.getElementById('crmContract_createUser_name').value=rec.get('name');" 
					/> --%>
						
					<td class="label-title">创建人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmContract_createUser_name"
						value="<s:property value="crmContract.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="crmContract_createUser"
						name="crmContract.createUser.id"
						value="<s:property value="crmContract.createUser.id"/>"> 
						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' class='detail' /> --%>
					</td>
				</tr>
				
				<!-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true"
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="createUserFun" 
		 				hasSetFun="true"
						documentId="crmContract_createUser"
						documentName="crmContract_createUser_name"/>
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmContract_createUser_name"  value="<s:property value="crmContract.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="crmContract_createUser" name="crmContract.createUser.id"  value="<s:property value="crmContract.createUser.id"/>" > 
 						 <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> -->
				
				
				<tr>
					<td class="label-title">创建日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_createDate" name="crmContract.createDate"
						title="创建日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmContract.createDate" format="yyyy-MM-dd"/>" />
					</td>
					
					<%-- <g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						extRec="rec" width="900" height="500"
						extStr="document.getElementById('crmContract_confirmUser').value=rec.get('id');
						document.getElementById('crmContract_confirmUser_name').value=rec.get('name');" /> --%>
						
					<g:LayOutWinTag buttonId="showConfirmUser" title="选择审核人"
						hasHtmlFrame="true"
						html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="ConfirmUserFun" 
		 				hasSetFun="true" width="900" height="500"
						documentId="crmContract_confirmUser"
						documentName="crmContract_confirmUser_name"/>
						
					<td class="label-title">审核人</td>
					<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="20" readonly="readOnly" id="crmContract_confirmUser_name"
							value="<s:property value="crmContract.confirmUser.name"/>"
							readonly="readOnly" /> 
						<input type="hidden" id="crmContract_confirmUser"
							name="crmContract.confirmUser.id"
							value="<s:property value="crmContract.confirmUser.id"/>">
						<img alt='选择审核人' id='showConfirmUser' src='${ctx}/images/img_lookup.gif' class='detail'/>
					</td>
					
					<td class="label-title">审核日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmContract_confirmDate" name="crmContract.confirmDate"
						title="审核日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmContract.confirmDate" format="yyyy-MM-dd"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title" style="display: none">工作流状态</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="hidden"
						id="crmContract_state" name="crmContract.state"
						value="<s:property value="crmContract.state"/>" /></td>
					<td class="label-title">工作流状态</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="30"
						id="crmContract_stateName" name="crmContract.stateName"
						title="工作流状态" readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="crmContract.stateName"/>" /></td>
				
					<td class="label-title">合同编号</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="120"
						id="crmContract_identifiers" name="crmContract.identifiers" title="${crmContract.identifiers }"
						value="<s:property value="crmContract.identifiers"/>" />
					</td>
					
					<g:LayOutWinTag buttonId="showkind" title="选择合同种类"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="htzl" hasSetFun="true"
						documentId="crmContract_kind" documentName="crmContract_kind_name"
				 	/>
				 	<td class="label-title">合同种类</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' />
					</td>
					<td align="left"><input type="text" size="20"
						 id="crmContract_kind_name"
						value="<s:property value="crmContract.kind.name"/>" /> <input
						type="hidden" id="crmContract_kind" name="crmContract.kind.id"
						value="<s:property value="crmContract.kind.id"/>"> <img
						alt='选择合同种类' id='showkind' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
				</tr>
				<tr>
					<td class="label-title" >备注</td>
					<td class="requiredcolumn" nowrap width="10px">
					<img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" >
					
					<textarea  id="crmContract_generationTarget" name="crmContract.content2"  maxlength="500" style="overflow: hidden; width: 250px; height: 50px;" ><s:property value="crmContract.content2"/></textarea>
				
				</tr>
				</table>
				<table width="100%" class="section_table" cellpadding="0" >
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">客户信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
			</table>	
			<table class="frame-table">
			<tr>
					<g:LayOutWinTag buttonId="showConfirmUser1" title="选择审核人"
						hasHtmlFrame="true"
						html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="ConfirmUserFun" 
		 				hasSetFun="true" width="900" height="500"
						documentId="crmContract_crmCustomer"
						documentName="crmContract_crmCustomer_name"/>
					<td class="label-title">委托人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmContract_crmCustomer_name"
						value="<s:property value="crmContract.crmCustomer.name"/>" /> <input
						type="hidden" id="crmContract_crmCustomer"
						name="crmContract.crmCustomer.id"
						value="<s:property value="crmContract.crmCustomer.id"/>">
						<img alt='选择委托人' id='showConfirmUser1'
						src='${ctx}/images/img_lookup.gif' class='detail'
						 />
					</td>
					<td class="label-title">客户性质</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<select id="crmContract_customerProperty" name="crmContract.customerProperty" style="width: 152px;">
							<option value="新客户"
								<s:if test="crmContract.customerProperty=='新客户'">selected="selected"</s:if>>新客户</option>
							<option value="老客户"
								<s:if test="crmContract.customerProperty=='老客户'">selected="selected"</s:if>>老客户</option>
						</select>
					</td>
					<%-- <td class="label-title">委托人单位</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="hidden"
							id="crmContract_crmCustomerDept"
							name="crmContract.crmCustomerDept.id"
							value="<s:property value="crmContract.crmCustomerDept.id"/>">
							<input type="text" class="readonlytrue" size="40"
							readonly="readOnly" id="crmContract_crmCustomerDept_name"
							value="<s:property value="crmContract.crmCustomerDept.name"/>" />
						
					</td> --%>
				</tr>
				<tr>
					<%-- <td class="label-title">委托单位</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" id="crmContract_client" name="crmContract.client" size="40" readonly="readonly"
							   value="<s:property value="crmContract.client"/>" 
						/>
					 <img alt='选择委托单位' id='showDept' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="CrmCustomerDeptFun()" /> 
					</td> --%>
					
					<%-- <td class="label-title">法人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="20"	readonly="readOnly" id="crmContract_legalPerson_name"	value="<s:property value="crmContract.legalPerson.name"/>" />
					    <input	type="hidden" id="crmContract_legalPerson"	name="crmContract.legalPerson.id"	value="<s:property value="crmContract.legalPerson.id"/>">
						<img alt='选择委托人' id='showlegalPerson'	src='${ctx}/images/img_lookup.gif' class='detail' onclick="legalPersonFun()" />
					</td> --%>
					<%-- <td class="label-title">省份</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" class="readonlytrue"
						size="20" readonly="readOnly" id="crmContract_crmCustomer_selA"
						value="<s:property value="crmContract.crmCustomer.selA"/>" />  						<input type="hidden" id="crmContract_crmCustomerDept" name="crmContract.crmCustomerDept.id"  value="<s:property value="crmContract.crmCustomerDept.id"/>" > 
						<img alt='选择委托单位' id='showDept' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="CrmCustomerDeptFun()" />    
					</td> --%>
				</tr>
				</table>
				<table width="100%" class="section_table" cellpadding="0" >
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">合同款信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
			</table>
			<table class="frame-table">	
				<tr>
					<td class="label-title">合同总金额（元）</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="10"
						id="crmContract_money" title="合同金额" name="crmContract.money"
						value="<s:property value="crmContract.money"/>" />
					</td>
					<td class="label-title">币种</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><select id="crmContract_currency"
						name="crmContract.currency" class="input-10-length"
						style="width: 152px;">
							<option value="人民币"
								<s:if test="crmContract.currency=='人民币'">selected="selected"</s:if>>人民币</option>
							<option value="美元"
								<s:if test="crmContract.currency=='美元'">selected="selected"</s:if>>美元</option>
					</select>
					</td>
					<td class="label-title">收款期数</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="crmContract_periodsNum" name="crmContract.periodsNum"
						title="收款期数" value="<s:property value="crmContract.periodsNum"/>" />
					</td>
					<g:LayOutWinTag buttonId="showconMoneyState" title="选择合同款使用状态"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="conMoneyState" hasSetFun="true"
						documentId="crmContract_conMoneyState"
						documentName="crmContract_conMoneyState_name" />
					<td class="label-title">合同款使用状态</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmContract_conMoneyState_name"
						value="<s:property value="crmContract.conMoneyState.name"/>" /> <input
						type="hidden" id="crmContract_conMoneyState"
						name="crmContract.conMoneyState.id"
						value="<s:property value="crmContract.conMoneyState.id"/>">
						<img alt='选择合同款使用状态' id='showconMoneyState'
						src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
				</tr>
				</table>
				<!-- <table width="100%" class="section_table" cellpadding="0" >
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">项目基本信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table> -->
				<%-- <table class="frame-table">	
					<g:LayOutWinTag buttonId="showproject" title="选择项目编号"
						hasHtmlFrame="true" width="600" height="580"
						html="${ctx}/exp/project/project/projectSelect.action"
						isHasSubmit="false" functionName="ProjectFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmContract_project').value=rec.get('id');
						document.getElementById('crmContract_project_name').value=rec.get('name');
						document.getElementById('crmContract_project_projectId').value=rec.get('projectId');" />
					<tr>
					<td class="label-title">项目编号</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left">
						<input type="text" size="20" id="crmContract_project_projectId"  value="<s:property value="crmContract.project.projectId"/>" readonly="readOnly"  />
						<input type="hidden" size="20" readonly="readOnly" id="crmContract_project_name"
						       value="<s:property value="crmContract.project.name"/>" readonly="readOnly" /> 
						<input type="hidden" id="crmContract_project" name="crmContract.project.id"
						       value="<s:property value="crmContract.project.id"/>"/> 
						<img alt='选择项目编号' id='showproject' src='${ctx}/images/img_lookup.gif'
								class='detail' />
					</td>
					<td class="label-title">项目名称</td>
					<td class="requiredcolumn" nowrap width="10px"
						><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text"
						size="20" maxlength="25" id="crmContract_content3"
						name="crmContract.content3" title="content3"
						value="<s:property value="crmContract.content3"/>"
						/></td>
					<g:LayOutWinTag buttonId="showType" title="选择项目类型"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="workType" hasSetFun="true"
							documentId="crmContract_workType"
							documentName="crmContract_workType_name"
					/>
               	 	<td class="label-title" >项目类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmContract_workType_name"  value="<s:property value="crmContract.workType.name"/>" />
 						<input type="hidden" id="crmContract_workType" name="crmContract.workType.id"  value="<s:property value="crmContract.workType.id"/>" > 
 						<img alt='选择项目类型' id='showType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	<g:LayOutWinTag buttonId="showprojectStart" title="选择技术类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="modelPreparation" hasSetFun="true"
						documentId="crmContract_technologyType"
						documentName="crmContract_technologyType_name"
				 	/>
               	 	<td class="label-title" >技术类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left" >
 						<input type="text" size="20"   id="crmContract_technologyType_name"  value="<s:property value="crmContract.technologyType.name"/>"  />
 						<input type="hidden" id="crmContract_technologyType" name="crmContract.technologyType.id"  value="<s:property value="crmContract.technologyType.id"/>" > 
 						<img alt='选择技术类型' id='showprojectStart' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showtype" title="选择产品类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
						documentId="crmContract_productType"
						documentName="crmContract_productType_name"
				 	/>
               	 	<td class="label-title" >产品类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_productType_name"  value="<s:property value="crmContract.productType.name"/>"  />
 						<input type="hidden" id="crmContract_productType" name="crmContract.productType.id"  value="<s:property value="crmContract.productType.id"/>" > 
 						<img alt='选择产品类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	<g:LayOutWinTag buttonId="showgenus" title="选择种属"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="genera" hasSetFun="true"
						documentId="crmContract_genus"
						documentName="crmContract_genus_name"
				 	/>
               	 	<td class="label-title" >种属</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_genus_name"  value="<s:property value="crmContract.genus.name"/>"  />
 						<input type="hidden" id="crmContract_genus" name="crmContract.genus.id"  value="<s:property value="crmContract.genus.id"/>" > 
 						<img alt='选择种属' id='showgenus' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   		<g:LayOutWinTag buttonId="showstrain" title="选择品系"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="saleChanceStrain" hasSetFun="true"
						documentId="crmContract_strain"
						documentName="crmContract_strain_name"
				 	/>
               	 	<td class="label-title" >品系</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_strain_name"  value="<s:property value="crmContract.strain.name"/>"  />
 						<input type="hidden" id="crmContract_strain" name="crmContract.strain.id"  value="<s:property value="crmContract.strain.id"/>" > 
 						<img alt='选择品系' id='showstrain' src='${ctx}/images/img_lookup.gif' class='detail'/>                   		
                   	</td>
                   		<g:LayOutWinTag buttonId="showGeneTargetingType" title="选择基因打靶类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="dbMethod" hasSetFun="true"
						documentId="crmContract_geneTargetingType"
						documentName="crmContract_geneTargetingType_name"
				 	/>
               	 	<td class="label-title" >基因打靶类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_geneTargetingType_name"  value="<s:property value="crmContract.geneTargetingType.name"/>"  />
 						<input type="hidden" id="crmContract_geneTargetingType" name="crmContract.geneTargetingType.id"  value="<s:property value="crmContract.geneTargetingType.id"/>" > 
 						<img alt='选择基因打靶类型' id='showGeneTargetingType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                </tr>
				<tr>
					<td class="label-title" >基因ID(NCBI)</td>
					<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="crmContract_geneId" name="crmContract.geneId" value="<s:property value="crmContract.geneId"/>" />
 					</td>
					<td class="label-title" >基因正式名称</td>
					<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="crmContract_geneName" name="crmContract.geneName" value="<s:property value="crmContract.geneName"/>" />
 					</td>
					<td class="label-title" >基因别名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="crmContract_geneByName" name="crmContract.geneByName" value="<s:property value="crmContract.geneByName"/>" />
 					</td>
				</tr>
				</table> --%>
				<table width="100%" class="section_table" cellpadding="0" >
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">产品信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
			</table>
			<table class="frame-table">	
			<tr>
					<%-- <g:LayOutWinTag buttonId="showProduct" title="选择终产品"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="finalType" hasSetFun="true"
						documentId="crmContract_product"
						documentName="crmContract_product_name"
					/> --%>

					<td class="label-title" >终产品</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmContract_productName" name="crmContract.productName" value="<s:property value="crmContract.productName"/>" />
 						<input type="hidden" id="crmContract_product" name="crmContract.product"  value="<s:property value="crmContract.product"/>" > 
 						<input type="hidden" id="crmContract_finalProduct" name="crmContract.finalProduct.id"  value="<s:property value="crmContract.finalProduct.id"/>" > 
 						<img alt='选择终产品'  onClick="voucherProductFun()" src='${ctx}/images/img_lookup.gif' 	class='detail'    /><!--id='showProduct'  -->                   		
                   	</td>
                   	
                   	<%-- <td class="label-title" >代养目标</td>
             	 	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                 	<td align="left"  >
						<input type="text" size="20" id="crmContract_generationTarget" name="crmContract.generationTarget" value="<s:property value="crmContract.generationTarget"/>" />
						<input type="hidden" id="project_product" name="project.product.id"  value="<s:property value="project.product.id"/>" > 
						<img alt='选择终产品' id='showProduct' src='${ctx}/images/img_lookup.gif' 	class='detail'/>                   		
                 	</td> --%>
                   	
					<td class="label-title" >终产品数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"  maxlength="60" id="crmContract_productNo" name="crmContract.productNo" 
 						title= "终产品数量" value="<s:property value="crmContract.productNo"/>"  />
                   	</td>
					<%-- <td class="label-title" >Southern blot鉴定</td>
                   	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
						<select id="crmContract_checkup" name="crmContract.checkup" class="input-10-length" style="width:152px;">
								<option value="0" <s:if test="crmContract.checkup==0">selected="selected"</s:if>>不需要</option>
								<option value="1" <s:if test="crmContract.checkup==1">selected="selected"</s:if>>需要</option>
						</select>
                   	</td> --%>
                   	<%-- <td class="label-title" >代养目标</td>
						<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
			            <td align="left"  colspan="4">
	               	 		<textarea  id="crmContract_generationTarget" name="crmContract.generationTarget"  maxlength="500" style="overflow: hidden; width: 500px; height: 40px;" ><s:property value="crmContract.generationTarget"/></textarea>
						</td> --%>
						<td class="label-title">备注</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="crmContract_content1" name="crmContract.content1"
						title="content1"
						value="<s:property value="crmContract.content1"/>" /></td>
              </tr>
              
				<%-- <tr>
                   	<g:LayOutWinTag buttonId="showCheckUp" title="选择质控要求"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="checkUp" hasSetFun="true"
						documentId="crmContract_checkup"
						documentName="crmContract_checkup_name"
					/>
             	 	<td class="label-title" >质控要求</td>
             	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                 	<td align="left">
						<input type="text" size="20" readonly="readOnly"  id="crmContract_checkup_name"  value="<s:property value="crmContract.checkup.name"/>" />
						<input type="hidden" id="crmContract_checkup" name="crmContract.checkup.id"  value="<s:property value="crmContract.checkup.id"/>" > 
						<img alt='选择终产品' id='showCheckUp' src='${ctx}/images/img_lookup.gif' 	class='detail'/>                   		
                 	</td>
                 	
					<td class="label-title" >是否去Neo</td>
                   	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
						<select id="crmContract_neoResult" name="crmContract.neoResult" class="input-10-length" style="width:152px;">
								<option value="0" <s:if test="crmContract.neoResult==0">selected="selected"</s:if>>否</option>
								<option value="1" <s:if test="crmContract.neoResult==1">selected="selected"</s:if>>是</option>
						</select>
                   	</td>
                   	<td class="label-title">备注</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="crmContract_content1" name="crmContract.content1"
						title="content1"
						value="<s:property value="crmContract.content1"/>" /></td>
				</tr> --%>
				<tr>
				<td class="label-title" >客户要求</td>
				<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
	            <td align="left"  colspan="6">
	               	 	<textarea  id="crmContract_customerRequirements" name="crmContract.customerRequirements"  title="客户要求" style="overflow: hidden; width: 600px; height: 40px;" ><s:property value="crmContract.customerRequirements"/></textarea>
				</td>
				</tr>
				<tr>
					<%-- <g:LayOutWinTag buttonId="showprojectType" title="选择项目类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectType" hasSetFun="true"
						documentId="crmContract_projectType"
						documentName="crmContract_projectType_name" />
					<td class="label-title">项目类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmContract_projectType_name"
						value="<s:property value="crmContract.projectType.name"/>" /> <input
						type="hidden" id="crmContract_projectType"
						name="crmContract.projectType.id"
						value="<s:property value="crmContract.projectType.id"/>">
						<img alt='选择项目类型' id='showprojectType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td> --%>
				</tr>
				
				<tr>
					<%-- <g:LayOutWinTag buttonId="showcrmSaleOrder" title="选择订单"
				hasHtmlFrame="true" width="600" height="580"
				html="${ctx}/crm/sale/crmSaleOrder/crmSaleOrderSelect.action"
				isHasSubmit="false" functionName="CrmSaleOrderFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmContract_crmSaleOrder').value=rec.get('id');
				document.getElementById('crmContract_crmSaleOrder_name').value=rec.get('name');" />
               	 	<td class="label-title" >相关订单</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmContract_crmSaleOrder_name"  value="<s:property value="crmContract.crmSaleOrder.name"/>" />
 						<input type="hidden" id="crmContract_crmSaleOrder" name="crmContract.crmSaleOrder.id"  value="<s:property value="crmContract.crmSaleOrder.id"/>" > 
 						<img alt='选择订单' id='showcrmSaleOrder' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
				</tr>
				<tr>
					<%-- <g:LayOutWinTag buttonId="showcrmContractPay" title="选择付款计划"
				hasHtmlFrame="true"
				html="${ctx}/crm/contract/crmContract/crmContrctPaySelect.action"
				isHasSubmit="false" functionName="CrmContrctPayFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmContract_crmContractPay').value=rec.get('id');
				document.getElementById('crmContract_crmContractPay_name').value=rec.get('name');" />
               	 	<td class="label-title" >付款计划</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmContract_crmContractPay_name"  value="<s:property value="crmContract.crmContractPay.name"/>" />
 						<input type="hidden" id="crmContract_crmContractPay" name="crmContract.crmContractPay.id"  value="<s:property value="crmContract.crmContractPay.id"/>" > 
 						<img alt='选择付款计划' id='showcrmContractPay' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
					<%-- 		<g:LayOutWinTag buttonId="showcrmCustomer" title="选择委托人"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="CrmCustomerFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmContract_crmCustomer').value=rec.get('id');
				document.getElementById('crmContract_crmCustomer_name').value=rec.get('name');" /> --%>
					
				</tr>
				<tr>
					<%--                 <g:LayOutWinTag buttonId="showDept" title="选择委托单位"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/crm/customer/customerdept/crmCustomerDept/crmCustomerDeptSelect.action"
				isHasSubmit="false"		functionName="CrmCustomerDeptFun" 
				documentId="crmContract_crmCustomerDept"
				documentName="crmContract_crmCustomerDept_name"/> --%>
					<%-- <td class="label-title" >省份</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ></td>            	 	
                   	<td align="left"  >
 						<input type="text" readonly="readOnly"  class="readonlytrue" id="crmContract_crmCustomer_selA"  value="<s:property value="crmContract.crmCustomer.selA"/>" >
                   	</td> --%>
				</tr>
				<tr>
                   	<%-- <td align="left"  >
 						<input type="text" size="20"   id="crmContract_productType_name"  value="<s:property value="crmContract.productType.name"/>"  />
 						<input type="hidden" id="crmContract_productType" name="crmSaleOrder.productType.id"  value="<s:property value="crmContract.productType.id"/>" > 
 						<img alt='选择产品类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
				</tr>
				</table>
				<!-- <table width="100%" class="section_table" cellpadding="0" >
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">后续工作信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
			</table> -->
			<%-- <table class="frame-table">	
				<tr>
					<td class="label-title">订单数量</td>
					<td class="requiredcolumn" nowrap width="10px"><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="10"
						id="crmContract_orderNum" name="crmContract.orderNum" title="订单数量"
						value="<s:property value="crmContract.orderNum"/>" />
					</td>
					
					<td class="label-title" >订单接收人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  	<input type="text" size="30" maxlength="30" id="crmContract_orderRecipient"
                   	 		 name="crmContract.orderRecipient" title="订单接收人"	value="<s:property value="crmContract.orderRecipient"/>"/>
                   	  	<input type="hidden" id="injectNaturalMate_mateUserId" name="injectNaturalMate.mateUserName.id"  value="<s:property value="injectNaturalMate.mateUserName.id"/>" >
                   	  	<img alt='选择订单接收人' id='showOrderRecipient' src='${ctx}/images/img_lookup.gif' 	class='detail' onClick="ddjsUser()"   />
                   	</td>
					
					<td class="label-title">订单创建</td>
					<td class="requiredcolumn" nowrap width="10px">
						<img class='requiredimage' src='${ctx}/images/notrequired.gif' />
					</td>
						<td>
							<select id="crmContract_createOrder" name="crmContract.createOrder" class="input-10-length"
								    style="width: 152px;">
								<option value="0"
									<s:if test="crmContract.createOrder==0">selected="selected"</s:if>>否</option>
								<option value="1"
									<s:if test="crmContract.createOrder==1">selected="selected"</s:if>>是</option>
							</select>
						</td>
				</tr>
				<tr>
						<td class="label-title">附件</td>
						<td class="requiredcolumn" nowrap width="10px">
						<img class='requiredimage' src='${ctx}/images/notrequired.gif' />
							</td>
						<td title="保存基本后,可以维护查看附件">
							<div id="doclinks_img" style="width:100px;">
								<span class="attach-btn"></span>
								<span class="text label">共有${requestScope.fileNum}个附件</span>
							</div>
						</td>
				</tr>
				
				<tr>
					<td class="label-title" style="display: none">content4</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmContract_content4"
						name="crmContract.content4" title="content4"
						value="<s:property value="crmContract.content4"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content5</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmContract_content5"
						name="crmContract.content5" title="content5"
						value="<s:property value="crmContract.content5"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content6</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmContract_content6"
						name="crmContract.content6" title="content6"
						value="<s:property value="crmContract.content6"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content7</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmContract_content7"
						name="crmContract.content7" title="content7"
						value="<s:property value="crmContract.content7"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content8</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmContract_content8"
						name="crmContract.content8" title="content8"
						value="<s:property value="crmContract.content8"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content9</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmContract_content9"
						name="crmContract.content9" title="content9" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.content9" format="yyyy-MM-dd"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content10</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmContract_content10"
						name="crmContract.content10" title="content10" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.content10" format="yyyy-MM-dd"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content11</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmContract_content11"
						name="crmContract.content11" title="content11" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.content11" format="yyyy-MM-dd"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content12</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmContract_content12"
						name="crmContract.content12" title="content12" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmContract.content12" format="yyyy-MM-dd"/>"
						style="display: none" /></td>
				</tr>
			</table> --%>
			<input type="hidden" name="crmContrctPayJson" id="crmContrctPayJson" value="" />
			<input type="hidden" name="crmContractOrderJson" id="crmContractOrderJson" value="" />
			<input type="hidden" name="crmContrctBillJson" id="crmContrctBillJson" value="" /> 
			<input type="hidden" name="crmContractLinkManJson"	id="crmContractLinkManJson" value="" /> 
			<input type="hidden" name="crmContrctCollectJson" id="crmContrctCollectJson" value="" />
			<input type="hidden" id="id_parent_hidden" value="<s:property value="crmContract.id"/>" />
		</form>
		<div id="tabs">
			<ul>
				<!-- <li><a href="#crmContrctPaypage">合同价款信息</a></li> -->
				<li><a href="#crmContrctCollectpage">合同回款管理</a></li>
				<!-- <li><a href="#crmContractOrderpage">合同订单管理</a></li> -->
				<li><a href="#crmContrctBillpage">合同发票管理</a></li>
				<!-- <li><a href="#crmContractLinkManpage">合同管理联系人</a></li> -->
			</ul>
			<!-- <div id="crmContrctPaypage" width="100%" height:10px></div> -->
			<div id="crmContrctCollectpage" width="100%" height:10px></div>
			<!-- <div id="crmContractOrderpage" width="100%" height:10px></div> -->
			<div id="crmContrctBillpage" width="100%" height:10px></div>
			<!-- <div id="crmContractLinkManpage" width="100%" height:10px></div> -->
		</div>
	</div>
</body>
</html>
