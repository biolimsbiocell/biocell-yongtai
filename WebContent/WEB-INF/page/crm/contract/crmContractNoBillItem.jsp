﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>

<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/crm/contract/crmContractNoBillItem.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
	<!-- <form name='excelfrmO' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/>
	</form> -->
	<form id="form1" name="form1" method="post">
		<input type="hidden" name="crmContrctNoBillItemJson" id="crmContrctNoBillItemJson" value=""/>
	<div id="crmContrctNoBilldiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	
	<!-- <div id="batcc_data_div" style="display: none">
	<input type="hidden" id="extJsonDataString" name="extJsonDataString">
	<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
	<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
				<td>
					<label class="text label" title="输入查询条件">
						合同编号no
					</label>
				</td>
				<td>
					<input type="text"  name="crmContract.id" id="crmContract-id" searchField="true" >
				</td>
				
				<td>
					<label class="text label" title="输入查询条件">
						合同名称
					</label>
				</td>
				<td>
					<input type="text"  name="crmContract.name" id="crmContract-name" searchField="true" >
				</td>
				
				<td>
					<label class="text label" title="输入查询条件">
						发票号
					</label>
				</td>
				<td>
					<input type="text"  name="billId" id="billId" searchField="true" >
				</td>
				<td>
					<input type="button" onClick="selectInfo()" value="查询">
				</td>
			</tr>
			<tr>
				<td >
					<label class="text label" title="输入查询条件">
						是否开票
					</label>
				</td>
				<td>
					<input type="text"  name="crmContract.id" id="crmContract-id" searchField="true" >
				</td>
				<td align="left">
					<select id="isBill" name="isBill" class="input-10-length" style="width:130px;">
							<option value="1" >是</option>
							<option value="0" >否</option>
					</select>
				</td>
				
				<td>
					<label class="text label" title="输入查询条件">
						发票抬头
					</label>
				</td>
				<td>
					<input type="text"  name="billCode" id="billCode" searchField="true" >
				</td>
				
				<td>
					<label class="text label" title="输入查询条件">
						明细
					</label>
				</td>
				<td>
					<input type="text"  name="details" id="details" searchField="true" >
				</td>
			</tr>
			<tr>
				<td>
					<label class="text label" title="输入查询条件">
						期数
					</label>
				</td>
				<td>
					<input type="text"  name="num" id="num" searchField="true" >
				</td>
				
				<td>
					<label class="text label" title="输入查询条件">
						金额（元）
					</label>
				</td>
				<td>
					<input type="text"  name="billMoney" id="billMoney" searchField="true" >
				</td>
			</tr>
			<tr>
				<td>
					<label class="text label" title="输入查询条件">
						税点（%）
					</label>
				</td>
				<td>
					<input type="text"  name="taxPoint" id="taxPoint" searchField="true" >
				</td>
				
				<td>
					<label class="text label" title="输入查询条件">
						币种
					</label>
				</td>
				<td>
					<input type="text"  name="taxPoint" id="taxPoint" searchField="true" >
					<select id="currency" name="currency" searchField="true" class="input-10-length">
							<option value="" selected="selected">无</option>
							<option value="0" >人民币</option>
							<option value="1" >美元</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label class="text label" title="输入查询条件">
						发票号
					</label>
				</td>
				<td>
					<input type="text"  name="billId" id="billId" searchField="true" >
				</td>
				
				<td class="label-title" >申请开票日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startapplyBillDate" name="startapplyBillDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="applyBillDate1" name="applyBillDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endapplyBillDate" name="endapplyBillDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="applyBillDate2" name="applyBillDate##@@##2"  searchField="true" />
                </td>
			</tr>
			<tr>
				<td class="label-title" >开票日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startbillDate" name="startbillDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="billDate1" name="billDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endbillDate" name="endbillDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="billDate2" name="billDate##@@##2"  searchField="true" />
                </td>
                
                <td>
					<label class="text label" title="输入查询条件">
						发票类型
					</label>
				</td>
				<td>
					<input type="text"  name="taxPoint" id="taxPoint" searchField="true" >
					<select id="billType" name="billType" searchField="true" class="input-10-length">
							<option value="" selected="selected">无</option>
							<option value="0" >普通发票</option>
							<option value="1" >增值税发票</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label class="text label" title="输入查询条件">
						开票要求
					</label>
				</td>
				<td>
					<input type="text"  name="require" id="require" searchField="true" >
				</td>
				
				<td>
					<label class="text label" title="输入查询条件">
						备注
					</label>
				</td>
				<td>
					<input type="text"  name="note" id="note" searchField="true" >
				</td>
			</tr>
			<tr>
				<td>
					<label class="text label" title="输入查询条件">
						账期（天）
					</label>
				</td>
				<td>
					<input type="text" name="accountPeriod" id="accountPeriod" searchField="true" >
				</td>
			</tr>
	</table>
	</div> -->
	</form>
</body>
</html>


