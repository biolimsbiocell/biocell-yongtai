﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/contract/crmContractDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="crmContract_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">合同名称</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmContract_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	<tr>
           	</tr>
           	 	<g:LayOutWinTag buttonId="showcrmCustomer" title="选择委托人"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="CrmCustomerFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmContract_crmCustomer').value=rec.get('id');
				document.getElementById('crmContract_crmCustomer_name').value=rec.get('name');" />

               	 	<td class="label-title" >委托人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_crmCustomer_name" searchField="true"  name="crmCustomer.name"  value="" class="text input" />
 						<input type="hidden" id="crmContract_crmCustomer" name="crmContract.crmCustomer.id"  value="" > 
 						<img alt='选择委托人' id='showcrmCustomer' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
           	 	
           	 		<td class="label-title" >签订日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startwriteDate" name="startwriteDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="writeDate1" name="writeDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endwriteDate" name="endwriteDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="writeDate2" name="writeDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
			<tr>
           	 	<td class="label-title" >合同类型</td>
               	<td align="left"  >
					<input type="text" size="20"   id="crmContract_type_name" searchField="true"  name="type.name"  value="" class="text input" />
					<input type="hidden" id="crmContract_type" name="crmContract.type.id"  value="" > 
					<img alt='选择合同类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               	</td>
           	 		<td class="label-title" >合同开始日期</td>
                   	<td align="left"  >
             	
 						<input type="text" class="Wdate" readonly="readonly" id="startstartDate" name="startstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate1" name="startDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endstartDate" name="endstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate2" name="startDate##@@##2"  searchField="true" />

                   	</td>
               	 	
            <tr>
           	</tr>
           	 	<g:LayOutWinTag buttonId="showtype" title="选择合同类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
				documentId="crmContract_type"
				documentName="crmContract_type_name"
					/>
           	 	<td class="label-title">合同金额</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmContract_money" searchField="true" name="money"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" >合同结束日期</td>
               	<td align="left"  >
			
						<input type="text" class="Wdate" readonly="readonly" id="startendDate" name="startendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" /> -
						<input type="text" class="Wdate" readonly="readonly" id="endendDate" name="endendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate2" name="endDate##@@##2"  searchField="true" />
               	</td>
			</tr>
			<tr>
           	 	<g:LayOutWinTag buttonId="showcrmSaleOrder" title="选择订单"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/crmSaleOrder/crmSaleOrderSelect.action"
				isHasSubmit="false" functionName="CrmSaleOrderFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmContract_crmSaleOrder').value=rec.get('id');
				document.getElementById('crmContract_crmSaleOrder_name').value=rec.get('name');" />
               	 	<td class="label-title" >订单</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_crmSaleOrder_name" searchField="true"  name="crmSaleOrder.name"  value="" class="text input" />
 						<input type="hidden" id="crmContract_crmSaleOrder" name="crmContract.crmSaleOrder.id"  value="" > 
 						<img alt='选择订单' id='showcrmSaleOrder' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>

			<g:LayOutWinTag buttonId="showparent" title="选择父级合同"
				hasHtmlFrame="true"
				html="${ctx}/crm/contract/crmContract/crmContractSelect.action"
				isHasSubmit="false" functionName="CrmContractFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmContract_parent').value=rec.get('id');
				document.getElementById('crmContract_parent_name').value=rec.get('name');" />
               	 	<td class="label-title" >父级合同</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_parent_name" searchField="true"  name="parent.name"  value="" class="text input" />
 						<input type="hidden" id="crmContract_parent" name="crmContract.parent.id"  value="" > 
 						<img alt='选择父级合同' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
            <tr>
           	</tr>
           	 	
           	 	<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showmanagerFun" 
				documentId="crmContract_manager"
				documentName="crmContract_manager_name"
			 />
           	 	<td class="label-title" >客户经理</td>
               	<td align="left"  >
						<input type="text" size="20"   id="crmContract_manager_name" searchField="true"  name="manager.name"  value="" class="text input" />
						<input type="hidden" id="crmContract_manager" name="crmContract.manager.id"  value="" > 
						<img alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               	</td>
                   	
                <td class="label-title">受托方（乙方）</td>
               	<td align="left">
                  		<input type="text" maxlength="" id="crmContract_organization" searchField="true" name="crmContract.organization"  class="input-20-length"></input>
               	</td>
			
			</tr>
			<tr>
				<td class="label-title">委托人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmContract_crmCustomer_name" searchField="true" name="crmContract.crmCustomer.name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">委托人单位</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmContract_crmCustomerDept_name" searchField="true" name="crmContract.crmCustomerDept.name"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
				
           	 	
           	 	<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="crmContract_createUser"
				documentName="crmContract_createUser_name"/>
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmContract_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmContract_createUser" name="crmContract.createUser.id"  value="" > 
 						 <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />    
 						                     		
               	</td>
           	 	<td class="label-title" >创建日期</td>
               	<td align="left"  >
       	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
 
               	</td>
           	<tr>
           	</tr>
           	 	<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="ConfirmUserFun" 
	 				hasSetFun="true"
					documentId="crmContract_confirmUser"
					documentName="crmContract_confirmUser_name"/>
           	 	<td class="label-title" >审核人</td>
               	<td align="left"  >
					<input type="text" size="20"   id="crmContract_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
					<input type="hidden" id="crmContract_confirmUser" name="crmContract.confirmUser.id"  value="" > 
					<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               	</td>
           	 	<td class="label-title">审核日期</td>
               	<td align="left">
						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                </td>
			</tr>
			<tr>
				<td class="label-title" >是否备案</td>
               	<td align="left"  >
					<select id="crmContract_isRecord" name="isRecord" searchField="true" class="input-10-length" >
								<option value="1" <s:if test="crmContract.isRecord==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="crmContract.isRecord==0">selected="selected"</s:if>>否</option>
					</select>
					
               	</td>
           	 	
           	 	<td class="label-title" style="display:none">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content1" searchField="true" style="display:none" name="content1"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content2" searchField="true" style="display:none" name="content2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content3" searchField="true" style="display:none" name="content3"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content4" searchField="true" style="display:none" name="content4"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content5" searchField="true" style="display:none" name="content5"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content6" searchField="true" style="display:none" name="content6"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content7" searchField="true" style="display:none" name="content7"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmContract_content8" searchField="true" style="display:none" name="content8"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmContract_content9" searchField="true" style="display:none" name="content9"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmContract_content10" searchField="true" style="display:none" name="content10"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmContract_content11" searchField="true" style="display:none" name="content11"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmContract_content12" searchField="true" style="display:none" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_crmContract_div"></div>
   		
</body>
</html>



