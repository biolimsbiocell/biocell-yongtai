﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}

</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/contract/crmContrctBillDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
	<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
	<input type="hidden" id="id_parent_hidden" value="${requestScope.id}">
	<div id="crmContrctBilldiv"></div>
	<!-- <div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div> -->
	
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	<td class="label-title">发票抬头</td>
               	<td align="left">
                	<input type="text" maxlength="60" id="crmContrctBill_billCode" searchField="true" name="billCode"  class="input-20-length"></input>
               	</td>
               	<td class="label-title">金额</td>
               	<td align="left">
                	<input type="text" maxlength="60" id="crmContrctBill_billMoney" searchField="true" name="billMoney"  class="input-20-length"></input>
               	</td>
           	</tr>
        </table>
		</form>
		</div>
		<!-- <span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span> -->
		<div id="show_dialog_crmContrctBill_div"></div>
</body>
</html>


