﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/crm/agent/primary/primaryAgreementTaskItem.js"></script>
</head>
<body>
	<div id="primaryAgreementTaskItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="bat_isvalid_div" style="display: none">
	<table>
			<tr>
				<td class="label-title" ><span>是否有效</span></td>
                <td><select id="isvalid" style="width:100">
<!--                 		<option value="" <s:if test="result==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>>有效</option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>>无效</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>
