﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/agent/primary/primaryTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="30" id="primaryTask_id"
                   	 name="id" searchField="true" title='<fmt:message key="biolims.common.serialNumber" />'   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.name" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="primaryTask_name"
                   	 name="name" searchField="true" title='<fmt:message key="biolims.common.name" />'   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.addr" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_address"
                   	 name="address" searchField="true" title='<fmt:message key="biolims.common.addr" />'
   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.leader" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_principal"
                   	 name="principal" searchField="true" title='<fmt:message key="biolims.common.leader" />'  />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.dashboard.errorTel" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_phone"
                   	 name="phone" searchField="true" title='<fmt:message key="biolims.dashboard.errorTel" />'  />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.salesManagement" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_marketer"
                   	 name="marketer" searchField="true" title=
'<fmt:message key="biolims.common.salesManagement" />'    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.prepaidAmount" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_money"
                   	 name="money" searchField="true" title=
'<fmt:message key="biolims.common.prepaidAmount" />'   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/crm/agent/primary/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('primaryTask_createUser').value=rec.get('id');
				document.getElementById('primaryTask_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson" /></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="primaryTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="primaryTask_createUser" name="primaryTask.createUser.id"  value="" > 
 						<img alt=
'<fmt:message key="biolims.common.selectTheCreatePerson" />' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate" /></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.count" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_sum"
                   	 name="sum" searchField="true" title='<fmt:message key="biolims.common.count" />'   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.state" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_state"
                   	 name="state" searchField="true" title='<fmt:message key="biolims.common.state" />'   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.nameOfTheState" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="primaryTask_stateName"
                   	 name="stateName" searchField="true" title='<fmt:message key="biolims.common.nameOfTheState" />'   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >biolims.common.nameOfTheState</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="50" id="primaryTask_note"
                   	 name="note" searchField="true" title='<fmt:message key="biolims.common.nameOfTheState" />'  style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_primaryTask_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_primaryTask_tree_page"></div>
</body>
</html>



