<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img"
		title='<fmt:message key="biolims.common.attachment" />'
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=primaryTask&id=${primaryTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/crm/agent/primary/primaryTaskEdit.js"></script>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}"> <input type="hidden"
			id="taskName" value="${requestScope.taskName}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>


					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="30" maxlength="25"
						id="primaryTask_id" name="primaryTask.id"
						title='<fmt:message key="biolims.common.serialNumber" />'
						value="<s:property value="primaryTask.id"/>"
						class="text input readonlytrue" readonly="readOnly" />

					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.name" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30" maxlength="50"
						id="primaryTask_name" name="primaryTask.name"
						title='<fmt:message key="biolims.common.name" />'
						value="<s:property value="primaryTask.name"/>" />

					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.addr" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30" maxlength="25"
						id="primaryTask_address" name="primaryTask.address"
						title='<fmt:message key="biolims.common.addr" />'
						value="<s:property value="primaryTask.address"/>" />

					</td>
				</tr>
				<tr>


					<td class="label-title"><fmt:message
							key="biolims.common.leader" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30" maxlength="25"
						id="primaryTask_principal" name="primaryTask.principal"
						title='<fmt:message key="biolims.common.leader" />'
						value="<s:property value="primaryTask.principal"/>" />

					</td>


					<td class="label-title"><fmt:message
							key="biolims.dashboard.errorTel" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30" maxlength="25"
						id="primaryTask_phone" name="primaryTask.phone"
						title='<fmt:message key="biolims.dashboard.errorTel" />'
						value="<s:property value="primaryTask.phone"/>" />

					</td>




					<g:LayOutWinTag buttonId="showmarketer"
						title='<fmt:message key="biolims.common.selectSalesManagement" />'
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun1" hasSetFun="true"
						documentId="primaryTask_marketer"
						documentName="primaryTask_marketer_name" />


					<td class="label-title"><fmt:message
							key="biolims.common.salesManagement" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30"
						readonly="readOnly" id="primaryTask_marketer_name"
						value="<s:property value="primaryTask.marketer.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="primaryTask_marketer" name="primaryTask.marketer.id"
						value="<s:property value="primaryTask.marketer.id"/>"> <img
						alt='<fmt:message key="biolims.common.selectSalesManagement" />'
						id='showmarketer' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>

				</tr>
				<tr>


					<td class="label-title"><fmt:message
							key="biolims.common.prepaidBalance" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30" maxlength="25"
						id="primaryTask_money" name="primaryTask.money"
						title='<fmt:message key="biolims.common.prepaidBalance=" />'
						value="<s:property value="primaryTask.money"/>"
						class="text input readonlytrue" readonly="readOnly" /></td>


					<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('primaryTask_createUser').value=rec.get('id');
				document.getElementById('primaryTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="primaryTask_createUser_name"  value="<s:property value="primaryTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="primaryTask_createUser" name="primaryTask.createUser.id"  value="<s:property value="primaryTask.createUser.id"/>" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
					<td class="label-title"><fmt:message
							key="biolims.common.commandPerson" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30"
						id="primaryTask_createUser_name"
						value="<s:property value="primaryTask.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="primaryTask_createUser"
						name="primaryTask.createUser.id"
						value="<s:property value="primaryTask.createUser.id"/>">
					</td>


					<td class="label-title"><fmt:message
							key="biolims.common.createDate" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30" maxlength="25"
						id="primaryTask_createDate" name="primaryTask.createDate"
						title='<fmt:message key="biolims.common.createDate" />'
						value="<s:date name="primaryTask.createDate" format="yyyy-MM-dd"/>"
						class="text input readonlytrue" readonly="readOnly" /></td>

				</tr>
				<tr>

					<td class="label-title"><fmt:message
							key="biolims.common.grade" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="30"
						readonly="readOnly" id="primaryTask_rate_name"
						value="<s:property value="primaryTask.rate.name"/>"> <input
						type="hidden" id="primaryTask_rate" name="primaryTask.rate.id"
						value="<s:property value="primaryTask.rate.id"/>"> <img
						alt='<fmt:message key="biolims.common.selectGrade" />'
						id='showrate' onclick="rate();" src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>


					<g:LayOutWinTag buttonId="showstate"
						title='<fmt:message key="biolims.common.selectTheState" />'
						hasHtmlFrame="true"
						html="${ctx}/dic/state/stateSelect.action?type=commonState"
						isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
						documentId="primaryTask_state"
						documentName="primaryTask_state_name" />


					<td class="label-title"><fmt:message
							key="biolims.common.state" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="30"
						readonly="readOnly" id="primaryTask_state_name"
						name="primaryTask.state.name"
						value="<s:property value="primaryTask.state.name"/>"> <input
						type="hidden" id="primaryTask_state" name="primaryTask.state.id"
						value="<s:property value="primaryTask.state.id"/>"> <img
						alt='<fmt:message key="biolims.common.selectTheState" />'
						id='showstate' src='${ctx}/images/img_lookup.gif' class='detail' />

					</td>






					<td class="label-title"><fmt:message
							key="biolims.common.attachment" /></td>
					<td></td>
					<td
						title='<fmt:message key="biolims.common.afterthepreservation" />'
						id="doclinks_img"><span class="attach-btn"></span><span
						class="text label"><fmt:message key="biolims.common.common" />${requestScope.fileNum}<fmt:message
								key="biolims.common.attachment" /></span>
				</tr>
				<tr>

					<td class="label-title" style="display: none"><fmt:message
							key="biolims.common.note" /></td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"></td>
					<td align="left" style="display: none"><input type="text"
						size="50" maxlength="50" id="primaryTask_note"
						name="primaryTask.note"
						title='<fmt:message key="biolims.common.note" />'
						value="<s:property value="primaryTask.note"/>"
						style="display: none" /></td>
				</tr>
				<tr>

				</tr>


			</table>
			<input type="hidden" name="primaryAdvanceTaskItemJson"
				id="primaryAdvanceTaskItemJson" value="" /> <input type="hidden"
				name="primaryAgreementTaskItemJson"
				id="primaryAgreementTaskItemJson" value="" /> <input type="hidden"
				id="id_parent_hidden" value="<s:property value="primaryTask.id"/>" />
		</form>
		<div id="tabs">
			<ul>
				<li><a href="#primaryAdvanceTaskItempage"><fmt:message
							key="biolims.common.advanceInformationDetail" /></a></li>
				<li><a href="#primaryAgreementTaskItempage"><fmt:message
							key="biolims.common.protocolInformationDetail" /></a></li>
				<li><a href="#primaryOrderTaskItempage"><fmt:message
							key="biolims.common.orderDetail" /></a></li>
				<li><a href="#primarySampleTaskItempage"><fmt:message
							key="biolims.common.sampleDetail" /></a></li>
			</ul>
			<div id="primaryAdvanceTaskItempage" width="100%" height:10px></div>
			<div id="primaryAgreementTaskItempage" width="100%" height:10px></div>
			<div id="primaryOrderTaskItempage" width="100%" height:10px></div>
			<div id="primarySampleTaskItempage" width="100%" height:10px></div>
		</div>
	</div>
</body>
</html>
