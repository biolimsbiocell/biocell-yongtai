<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
#btn_changeState{
	display:none
}
#btn_submit{
	display:none
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>代理商主数据
					</h3>
					<small style="color: #fff;"> 
					创建人: <span id="primaryTaskNew_createUser"><s:property
								value=" primaryTaskNew.createUser.name" /></span> 
					创建时间: <span id="primaryTaskNew_createDate"><s:date
								name="primaryTaskNew.createDate" format="yyyy-MM-dd" /></span> 
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<%-- <div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>代理商主数据</small>
								</span>
							</a></li>
						</ul>
					</div> --%>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<input type="hidden" name="createUser-id"  class="form-control" changelog="<s:property value="primaryTaskNew.createUser.id"/>" value="<s:property value="primaryTaskNew.createUser.id"/>">
							<input type="hidden" name="createDate"  class="form-control"  changelog="<s:property value="primaryTaskNew.createDate"/>" class="form-control" value="<s:date name="primaryTaskNew.createDate" format="yyyy-MM-dd HH:mm:ss"/>" />  
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">编号<img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
                   						<input type="text"
											size="20" maxlength="25" id="primaryTaskNew_id" read
											name="id"  changelog="<s:property value="primaryTaskNew.id"/>"
											  class="form-control" value="<s:property value="primaryTaskNew.id"/>" />  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">描述</span> 
                   						<input type="text"
											size="50" maxlength="50" id="primaryTaskNew_name"
											name="name"  changelog="<s:property value="primaryTaskNew.name"/>"
											  class="form-control" value="<s:property value="primaryTaskNew.name"/>" />  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">地址</span> 
	                   						<input type="text"
												size="20" maxlength="25" id="primaryTaskNew_address"
												name="address"  changelog="<s:property value="primaryTaskNew.address"/>"
												  class="form-control" value="<s:property value="primaryTaskNew.address"/>" />  
										</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">负责人</span> 
                   						<input type="text"
											size="20" maxlength="25" id="primaryTaskNew_principal"
											name="principal"  changelog="<s:property value="primaryTaskNew.principal"/>"
											  class="form-control" value="<s:property value="primaryTaskNew.principal"/>" />  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">联系电话</span> 
                   						<input type="text"
											size="20" maxlength="25" id="primaryTaskNew_phone"
											name="phone"  changelog="<s:property value="primaryTaskNew.phone"/>"
											  class="form-control" value="<s:property value="primaryTaskNew.phone"/>" />  
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">状态</span> 
                   						<input type="hidden" size="20" maxlength="25" id="primaryTaskNew_state_id"
											name="state-id"  changelog="<s:property value="primaryTaskNew.state.id"/>"
											class="form-control"  value="<s:property value="primaryTaskNew.state.id"/>" />  
										<input type="text" size="20" maxlength="25" id="primaryTaskNew_state_name"
											 changelog="<s:property value="primaryTaskNew.state.name"/>"
											class="form-control"  value="<s:property value="primaryTaskNew.state.name"/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showState()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
										
									</div>
								</div>			
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">备注</span> 
                   						<input type="text"
											size="50" maxlength="50" id="primaryTaskNew_note"
											name="note"  changelog="<s:property value="primaryTaskNew.note.name"/>"
											  class="form-control" value="<s:property value="primaryTaskNew.note"/>" />  
									</div>
								</div>				
									<button type="button" class="btn btn-info" onclick="fileUp()">上传附件</button>
											<span class="text label"><fmt:message
													key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
													key="biolims.common.attachment" /></span>
											<button type="button" class="btn btn-info"
												onclick="fileView()">查看附件</button>
									</div>
							<div id="fieldItemDiv"></div>
								
							<br> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="primaryTaskNew.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="primaryTaskNew.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/crm/agent/primarynew/primaryTaskNewEdit.js"></script>
</body>

</html>	
