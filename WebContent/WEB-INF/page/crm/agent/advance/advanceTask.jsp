﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/agent/advance/advanceTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="advanceTask_id"
                   	 name="id" searchField="true" title=
'<fmt:message key="biolims.common.serialNumber" />'
    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="advanceTask_name"
                   	 name="name" searchField="true" title='<fmt:message key="biolims.common.describe" />'    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.state" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="advanceTask_state"
                   	 name="state" searchField="true" title='<fmt:message key="biolims.common.state" />'   />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.nameOfTheState" /></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="advanceTask_stateName"
                   	 name="stateName" searchField="true" title='<fmt:message key="biolims.common.nameOfTheState" />'  />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note" /></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="50" id="advanceTask_note"
                   	 name="note" searchField="true" title='<fmt:message key="biolims.common.note" />'  style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择申请人"
				hasHtmlFrame="true"
				html="${ctx}/crm/agent/advance/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('advanceTask_createUser').value=rec.get('id');
				document.getElementById('advanceTask_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.applicant" /></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="advanceTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="advanceTask_createUser" name="advanceTask.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectApplicant" />' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.applyForDate" /></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_advanceTask_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_advanceTask_tree_page"></div>
</body>
</html>



