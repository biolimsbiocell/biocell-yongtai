<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment" />'
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=agreementTask&id=${agreementTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/agent/agreement/agreementTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="25" id="agreementTask_id"
                   	 name="agreementTask.id" title='<fmt:message key="biolims.common.serialNumber" />'
                   	   
	value="<s:property value="agreementTask.id"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="agreementTask_name"
                   	 name="agreementTask.name" title=
'<fmt:message key="biolims.common.describe" />'
                   	   
					value="<s:property value="agreementTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="agreementTask_stateName"
                   	 name="agreementTask.stateName" title=
'<fmt:message key="biolims.common.workflowState" />'
                   	    readonly = "readOnly" class="text input readonlytrue"  
					value="<s:property value="agreementTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			
               	 
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title=
'<fmt:message key="biolims.common.selectApplicant" />'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('agreementTask_createUser').value=rec.get('id');
				document.getElementById('agreementTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.applicant" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="30"   id="agreementTask_createUser_name"  value="<s:property value="agreementTask.createUser.name"/>"  >
 						<input type="hidden" id="agreementTask_createUser" name="agreementTask.createUser.id"  value="<s:property value="agreementTask.createUser.id"/>" > 
                   	</td>
			
                   	
                   	<%-- 	<td class="label-title" >申请日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="30" maxlength="25" id="agreementTask_createDate"
                   	 name="agreementTask.createDate" title="申请日期"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  
                   	    value="<s:date name="agreementTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td> --%>
                   		<td class="label-title" ><fmt:message key="biolims.common.applyForDate" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="30" maxlength="25" id="agreementTask_createDate"
                   	 name="agreementTask.createDate" title=
'<fmt:message key="biolims.common.applyForDate" />'
                   	    value="<s:date name="agreementTask.createDate" format="yyyy-MM-dd"/>" 
                   	      class="text input readonlytrue" readonly="readOnly"  />
                   	  
                   	</td>
                   		<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title=
'<fmt:message key="biolims.common.afterthepreservation" />' id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf" />${requestScope.fileNum}'<fmt:message key="biolims.common.attachment" />'</span>
			</tr>
			<tr>
				<td class="label-title" style="display:none"><fmt:message key="biolims.common.state" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="25" id="agreementTask_state"
                   	 name="agreementTask.state" title=
'<fmt:message key="biolims.common.state" />'
                   	   
					value="<s:property value="agreementTask.state"/>"
					style="display:none"
                   	  />
                   	  
                   	</td>
                   	
                   	
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="agreementTask_note"
                   	 name="agreementTask.note" title=
'<fmt:message key="biolims.common.note" />'
                   	   
	value="<s:property value="agreementTask.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
		
			</tr>
			
			
            </table>
            <input type="hidden" name="agreementTaskItemJson" id="agreementTaskItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="agreementTask.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#agreementTaskItempage"><fmt:message key="biolims.common.protocolDetail" /></a></li>
           	</ul> 
			<div id="agreementTaskItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
