<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
			<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.crmDoctor.info"/>
						<small style="color:#fff;">
										<fmt:message key="biolims.common.customerID"/>: <span><s:property value="crmDoctor.id"/></span>
										<fmt:message key="biolims.common.state"/>: <span state="<s:property value="crmDoctor.state"/>"  id="crmDoctor_state"><s:property value="crmDoctor.state"/></span>
									</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
				<%-- 	<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>医生管理</small>
								</span>
							</a></li>
										<li><a class="disabled step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>医生管理明细</small></span>
								</a></li>
						</ul>
					</div> --%>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.name"/> </span> 
                   						<input type="text"
											size="50" maxlength="50" id="crmDoctor_name"
											name="name"  changelog="<s:property value="crmDoctor.name"/>"
											  class="form-control"   
											value="<s:property value="crmDoctor.name"/>"
										 />  
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
                   						<input type="hidden"
											size="20" maxlength="25" id="crmDoctor_id"
											name="id"  value="<s:property value="crmDoctor.id"/>"
										 />  
					
									</div>
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.mobile"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="11" id="crmDoctor_mobile"
											name="mobile"  changelog="<s:property value="crmDoctor.mobile"/>" class="form-control"   
											value="<s:property value="crmDoctor.mobile"/>"
										 />  
					
									</div>
								</div>				
								
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.payTreasure"/> </span> 
                   						<input type="text"
											size="20" maxlength="25" id="crmDoctor_payTreasure"
											name="payTreasure"  changelog="<s:property value="crmDoctor.payTreasure"/>" class="form-control"   
											value="<s:property value="crmDoctor.payTreasure"/>"
										 />  
									</div>
								</div>				
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.isMonthFee"/> </span> 
                   						<input type="text"
											size="20" maxlength="10" id="crmDoctor_isMonthFee"
											name="isMonthFee"  changelog="<s:property value="crmDoctor.isMonthFee"/>"
											  class="form-control"   
											value="<s:property value="crmDoctor.isMonthFee"/>"
										 />  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">医生级别</span> 
										<input type="text" size="20" readonly="readOnly"
												id="crmDoctor_type_name"  changelog="<s:property value="crmDoctor.type.name"/>"
												value="<s:property value="crmDoctor.type.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="crmDoctor_type_id" name="type-id"
												value="<s:property value="crmDoctor.type.id"/>">
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showtypeId()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>		
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.mail"/> </span> 
                   						<input type="text"
											size="20" maxlength="15" id="crmDoctor_mail"
											name="mail"  changelog="<s:property value="crmDoctor.mail"/>" class="form-control"   
											value="<s:property value="crmDoctor.mail"/>"
										 />  
									</div>
								</div>		
								<div class="col-md-4 col-sm-6 col-xs-12" hidden="none">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.state"/> </span> 
                   						<input type="text"
											size="20" maxlength="10" id="crmDoctor_state"
											name="state"  changelog="<s:property value="crmDoctor.state"/>"
											  class="form-control"   
											value="<s:property value="crmDoctor.state"/>"
						 				/>  
					
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.weiChatId"/> </span> 
                   						<input type="text"
											size="20" maxlength="10" id="crmDoctor_weichatId"
											name="weichatId"  changelog="<s:property value="crmDoctor.weichatId"/>" class="form-control"   
											value="<s:property value="crmDoctor.weichatId"/>"
										 />  
					
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.crmCustomer"/> </span> 
										<input type="text" size="20" readonly="readOnly"
												id="crmDoctor_crmCustomer_name"  changelog="<s:property value="crmDoctor.crmCustomer.name"/>"
												value="<s:property value="crmDoctor.crmCustomer.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="crmDoctor_crmCustomer_id" name="crmCustomer-id"
												value="<s:property value="crmDoctor.crmCustomer.id"/>">
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showcrmCustomer()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.ks"/> </span> 
										<input type="text" size="20" readonly="readOnly"
												id="crmDoctor_ks_name"  changelog="<s:property value="crmDoctor.ks.name"/>"
												value="<s:property value="crmDoctor.ks.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="crmDoctor_ks_id" name="ks-id"
												value="<s:property value="crmDoctor.ks.id"/>">
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showks()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.clinicCharacteristics"/> </span> 
                   						<input type="text"
											size="50" maxlength="100" id="crmDoctor_clinicCharacteristics"
											name="clinicCharacteristics"  changelog="<s:property value="crmDoctor.clinicCharacteristics"/>" class="form-control"   
											value="<s:property value="crmDoctor.clinicCharacteristics"/>"
						 				/>  
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.goodAtDisease"/> </span> 
                   						<input type="text"
											size="50" maxlength="100" id="crmDoctor_goodAtDisease"
											name="goodAtDisease"  changelog="<s:property value="crmDoctor.goodAtDisease"/>" class="form-control"   
											value="<s:property value="crmDoctor.goodAtDisease"/>"
						 				/>  
					
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.crmDoctor.doctorIntroduction"/> </span> 
                   						<input type="text"
											size="50" maxlength="100" id="crmDoctor_doctorIntroduction"
											name="doctorIntroduction"  changelog="<s:property value="crmDoctor.doctorIntroduction"/>" class="form-control"   
											value="<s:property value="crmDoctor.doctorIntroduction"/>"
										 />  
					
									</div>
								</div>
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
									</div>
								
							<br> 
							<input type="hidden" name="crmDoctorItemJson"
								id="crmDoctorItemJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="crmDoctor.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="crmDoctor.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					  	<div class="HideShowPanel" style="display:none">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="crmDoctorItemTable" style="font-size: 14px;">
						</table>
					</div>
					</div>
				<!-- <div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/crm/doctor/crmDoctorEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/doctor/crmDoctorItem.js"></script>
</body>

</html>	
