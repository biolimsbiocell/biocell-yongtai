﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/crm/doctor/crmDoctorItem.js"></script>
</head>
<body>
	<div id="crmDoctorItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"></fmt:message>
	</div>
	<div id="bat_way_div" style="display: none">
	<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.termsOfPayment"></fmt:message></span></td>
                <td><select id="way" style="width:100">
                		<option value="" <s:if test="result==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"></fmt:message></option>
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>><fmt:message key="biolims.common.quarterlyInvoicing"></fmt:message></option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>><fmt:message key="biolims.common.monthlyStatement"></fmt:message></option>
    					<option value="2" <s:if test="result==1">selected="selected" </s:if>><fmt:message key="biolims.common.halfMonth"></fmt:message></option>
    					<option value="3" <s:if test="result==0">selected="selected" </s:if>><fmt:message key="biolims.common.paymentNow"></fmt:message></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>
