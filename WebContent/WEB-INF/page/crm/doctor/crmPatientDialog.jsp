﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/doctor/crmPatientDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>

            <input type="hidden" name="crmCustomerId" id="crmCustomerId" value="<%=request.getParameter("crmCustomerId") %>" />

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	<td class="label-title"><fmt:message key="biolims.user.itemNo"/> </td>
                <td align="left">
					<input type="text" size="30" maxlength="20" searchField="true" id="crmDoctor_id" name="id" title="医生编号" value="" 
                   		 	class="text input"/>
                </td>
             </tr>
             <tr>
              	<td> </td>
             </tr>
              		 
             <tr>
				<td class="label-title"><fmt:message key="biolims.common.sname"/></td>
                <td align="left">
					<input type="text" size="30" maxlength="100" searchField="true" id="crmDoctor_name" name="name" title="姓名" value="" 
                   		 	class="text input"/>
                </td>
              </tr>
              <tr>
               	<td> </td>
               </tr>
               <tr>
                   	<td class="label-title"><fmt:message key="biolims.common.ofHospital"/></td>
                   	<td align="left">
					
                   	<input type="text" size="30" maxlength="100" searchField="true" id="crmCustomer-name" name="crmCustomer.name" title="所属医院" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><input type="button" value=<fmt:message key="biolims.common.search"/> size="10"/></span>
		<span>   </span>
		<span onclick="nd()" ><input type="button" value=<fmt:message key="biolims.common.create"/> size="10"/></span>
		
		<div id="show_dialog_crmPatient_div"></div>
   		
</body>
</html>



