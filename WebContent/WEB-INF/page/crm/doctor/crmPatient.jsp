﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/doctor/crmPatient.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>		 
	<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm"><!-- 医生主数据  搜索界面 -->
		<table>
			<tr>
               	<td class="label-title"><fmt:message key="biolims.common.serialNumber" /></td>
                <td align="left">
				<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_id" name="id" title='<fmt:message key="biolims.common.doctorNumber" />' value="" 
                   				class="text input"/>
               	</td>
          
                <td class="label-title"><fmt:message key="biolims.common.sname" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_name" name="name" title=
'<fmt:message key="biolims.common.sname" />' value="" 
                   		 		class="text input"/>
                </td>
			</tr>
			<tr>
               	<td class="label-title"><fmt:message key="biolims.common.state" /></td>
                <td align="left">
					<select id="crmDoctor_zt" searchField="true" name="state" class="input-10-length">
						<option value=""><fmt:message key="biolims.common.pleaseSelect" /></option>
						<option value="1"><fmt:message key="biolims.common.effective" /></option>
						<option value="0"><fmt:message key="biolims.common.invalid" /></option>
					</select>
                </td>
                <td class = "Label-title"><fmt:message key="biolims.common.whetherTheMonthKnot" /></td>
                <td align = "left">
                   	<select id="crmDoctor_isMonthFee" name="isMonthFee"
                   		 class="input-10-length">
						<option value="1"><fmt:message key="biolims.common.yes" /></option>
						<option value="0"><fmt:message key="biolims.common.no" /></option>
					</select> 
				</td>
			</tr>
			<tr>
		        <td class="label-title"><fmt:message key="biolims.master.hospitalId" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true"
					id="crmCustomer" name="crmCustomer"
					title='<fmt:message key="biolims.master.hospitalId" />' 
					value="" class="text input"/>
                </td>

                <td class="label-title"><fmt:message key="biolims.common.departmentId" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_ks_name" name="ks" title='<fmt:message key="biolims.common.department" />' value="" 
                   		 		class="text input"/>
                </td>
            </tr>
			<tr>

                <td class="label-title"><fmt:message key="biolims.common.cellPhoneNumber" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_telNumber" name="mobile" title='<fmt:message key="biolims.common.cellPhoneNumber" />' value="" 
                   		 		class="text input"/>
                </td>
            </tr>
			<tr>
                 <td class="label-title"><fmt:message key="biolims.common.alipay" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_payTreasure" name="payTreasure" title='<fmt:message key="biolims.common.alipay" />' value="" 
                   		 		class="text input"/>
                </td>
 
                 <td class="label-title"><fmt:message key="biolims.dashboard.errorEmail" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_mail" name="mail" title='<fmt:message key="biolims.dashboard.errorEmail" />' value="" 
                   		 		class="text input"/>
                </td>
            </tr>
			<tr>
                <td class="label-title"><fmt:message key="biolims.common.doctorProfile" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_doctorIntroduction" name="doctorIntroduction" title='<fmt:message key="biolims.common.doctorProfile" />' value="" 
                   		 		class="text input"/>
                </td>

                <td class="label-title"><fmt:message key="biolims.common.goodAtDisease" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_goodAtDisease" name="goodAtDisease" title='<fmt:message key="biolims.common.goodAtDisease" />' value="" 
                   		 		class="text input"/>
                </td>
            </tr>
			<tr>
                <td class="label-title"><fmt:message key="biolims.common.receptionCharacteristics" /></td>
                <td align="left">
					<input type="text" size="20" maxlength="20" searchField="true" id="crmDoctor_clinicCharacteristics" name="clinicCharacteristics" title=
'<fmt:message key="biolims.common.receptionCharacteristics" />' value="" 
                   		 		class="text input"/>
                </td>
			</tr>     	
        </table>
		</form>
		</div>
		<div id="show_crmPatient_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	</body>
</html>