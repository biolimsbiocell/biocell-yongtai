<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="add"'>

</s:if>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var crmCustomer = Ext.get('crmCustomer');
crmCustomer.on('click', 
 function showdutyManIdFun(){
var win = Ext.getCmp('showdutyManIdFun');
if (win) {win.close();}
var showdutyManIdFun= new Ext.Window({
id:'showdutyManIdFun',modal:true,title:'<fmt:message key="biolims.common.hospital" />',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/customer/customer/crmCustomerSelect.action?flag=showdutyManIdFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 showdutyManIdFun.close(); }  }]  });     showdutyManIdFun.show(); }
);
});
 function setshowdutyManIdFun(rec){

		 document.getElementById('crmDoctor_crmCustomer_id').value=rec.get('id');
		 document.getElementById('crmDoctor_crmCustomer_name').value=rec.get('name');
				
var win = Ext.getCmp('showdutyManIdFun')
if(win){win.close();}
}
</script>
 
				
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var ksSelect = Ext.get('ksSelect');
ksSelect.on('click', 
 function ks(){
var win = Ext.getCmp('ks');
if (win) {win.close();}
var ks= new Ext.Window({
id:'ks',modal:true,title:'<fmt:message key="biolims.common.department" />',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=ks' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 ks.close(); }  }]  });     ks.show(); }
);
});
 function setks(id,name){
 document.getElementById("crmDoctor_ks_id").value = id;
document.getElementById("crmDoctor_ks_name").value = name;
var win = Ext.getCmp('ks')
if(win){win.close();}
}
</script>
				


<body>
	<%@include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
	src="${ctx}/js/crm/doctor/crmPatientEdit.js"></script>
    <script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message key="biolims.common.serialNumber" /></td>
					<td align="left"><input type="text" size="30" maxlength="36"
						id="crmDoctor_id" name="crmDoctor.id" title='<fmt:message key="biolims.common.doctorNumber" />' class="text input readonlytrue" readonly="readOnly"
						value="<s:property value="crmDoctor.id"/>" />
					</td>
				<td class="label-title"><fmt:message key="biolims.common.sname" /></td>
					<td align="left"><input type="text" size="30" maxlength="30"
						 id="crmDoctor_name"
						name="crmDoctor.name" title='<fmt:message key="biolims.common.sname" />'

						 value="<s:property value="crmDoctor.name"/>" />
					</td>
               		<td class="label-title">
<fmt:message key="biolims.common.affiliatedHospital" /></td>
                   	<td align="left">
 						<input type="text" size="30" readonly="readOnly"  id="crmDoctor_crmCustomer_name" name = "crmDoctor.crmCustomer.name" value="<s:property value="crmDoctor.crmCustomer.name"/>"  class="text input"/>
 						<input type="hidden" id="crmDoctor_crmCustomer_id" name="crmDoctor.crmCustomer.id"  value="<s:property value="crmDoctor.crmCustomer.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.hospital" />' id='crmCustomer' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
                </tr>	
				<tr>
				<td class="label-title"><fmt:message key="biolims.common.department" /></td>
                   	<td align="left">
 						<input type="text" size="30" readonly="readOnly"  id="crmDoctor_ks_name" name = "crmDoctor.ks.name" value="<s:property value="crmDoctor.ks.name"/>"  class="text input"/>
 						<input type="hidden" id="crmDoctor_ks_id" name="crmDoctor.ks.id"  value="<s:property value="crmDoctor.ks.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.hospital" />' id='ksSelect' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
					
					<td class = "Label-title"><fmt:message key="biolims.common.whetherTheMonthKnot" /></td>
                   	<td align = "left">
                   	 	<select id="crmDoctor.isMonthFee" name="crmDoctor.isMonthFee" class="input-10-length">
							<option <s:if test="crmDoctor.isMonthFee==''">selected="selected"</s:if> value=""></option>
							<option <s:if test="crmDoctor.isMonthFee==1">selected="selected"</s:if> value="1"><fmt:message key="biolims.common.yes" /></option>
							<option <s:if test="crmDoctor.isMonthFee==0">selected="selected"</s:if> value="0">'<fmt:message key="biolims.common.no" />'</option>
					    </select> 
					</td>
					
					<td class="label-title"><fmt:message key="biolims.common.title" /></td>
					<td align="left"><input type="text" size="30" maxlength="30"
						 id="crmDoctor_jobTitle"
						name="crmDoctor.jobTitle" title=
'<fmt:message key="biolims.common.title" />'
						 value="<s:property value="crmDoctor.jobTitle"/>" />
					</td> 
					
					</tr>
					<tr>
					<td class="label-title"><fmt:message key="biolims.common.cellPhoneNumber" /></td>
					<td align="left"><input type="text" size="30" maxlength="30"
						 id="crmDoctor_mobile"
						name="crmDoctor.mobile" title='<fmt:message key="biolims.common.cellPhoneNumber" />'
						 value="<s:property value="crmDoctor.mobile"/>" />
					</td>
					
					<td class="label-title"><fmt:message key="biolims.common.alipay" /></td>
					<td align="left"><input type="text" size="30" maxlength="30"
						 id="crmDoctor_payTreasure"
						name="crmDoctor.payTreasure" title='<fmt:message key="biolims.common.alipay" />'
						 value="<s:property value="crmDoctor.payTreasure"/>" />
					</td>
					
					<td class="label-title"><fmt:message key="biolims.dashboard.errorEmail" /></td>
					<td align="left"><input type="text" size="30" maxlength="40"
						 id="crmDoctor_mail"
						 name="crmDoctor.mail" title='<fmt:message key="biolims.dashboard.errorEmail" />'
						 value="<s:property value="crmDoctor.mail"/>" />
					</td>
					
					</tr>
					<tr>
					<td class="label-title"><fmt:message key="biolims.common.state" /></td>
					<td align="left">
					<select id="crmDoctor_state"  name="crmDoctor.state" class="input-10-length">
							<option value="1"><fmt:message key="biolims.common.effective" /></option>
							<option value="0">
<fmt:message key="biolims.common.invalid" /></option>
					</select>
						<td class="label-title"><fmt:message key="biolims.common.goodAtDisease" /></td>
					<td align="left"><input type="text" size="30" maxlength="40"
						 id="crmDoctor_goodAtDisease"
						 name="crmDoctor.goodAtDisease" title=
'<fmt:message key="biolims.common.goodAtDisease" />'
						 value="<s:property value="crmDoctor.goodAtDisease"/>" />
					</td>
						<td class="label-title"><fmt:message key="biolims.common.wechatNumber" /></td>
					<td align="left"><input type="text" size="30" maxlength="40"
						 id="crmDoctor_weichatId"
						 name="crmDoctor.weichatId" title='<fmt:message key="biolims.common.wechatNumber" />'
						 value="<s:property value="crmDoctor.weichatId"/>" />
					</td>
					
					</tr>
					<tr>
					<td class="label-title"><fmt:message key="biolims.common.customerCategory" /></td>
					<td align="left"><input type="text" size="15"
						readonly="readOnly" id="crmDoctor_type_name"
						value="<s:property value="crmDoctor.type.name"/>"
						class="text input" /> <input type="hidden" id="crmDoctor_type"
						name="crmDoctor.type.id"
						value="<s:property value="crmDoctor.type.id"/>"> <img
						alt= '选择客户类别' onClick="showCrmDoctorFun()" src='${ctx}/images/img_lookup.gif'
						class='detail' /></td> 
					
					
<%-- 						<td class="label-title"><fmt:message key="biolims.common.cellPhoneNumber" /></td> --%>
<!-- 					<td align="left"><input type="text" size="30" maxlength="30" -->
<!-- 						 id="crmDoctor_mobile" -->
<%-- 						name="crmDoctor.mobile" title='<fmt:message key="biolims.common.cellPhoneNumber" />' --%>
<%-- 						 value="<s:property value="crmDoctor.mobile"/>" /> --%>
<!-- 					</td> -->
	               
				</tr>
				
				<tr>
				 <td class="label-title"><fmt:message key="biolims.common.doctorProfile" /></td>
                    <td ><textarea class="text input false" id="crmDoctor_doctorIntroduction" name="crmDoctor.doctorIntroduction" tabindex="0" title="内容" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" style="height:150;width:260"><s:property value="crmDoctor.doctorIntroduction"/></textarea></td>
                   
					<td class="label-title"><fmt:message key="biolims.common.receptionCharacteristics" /></td>
					<td ><textarea class="text input false" id="crmDoctor_clinicCharacteristics" name="crmDoctor.clinicCharacteristics" tabindex="0" title="内容" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" style="height:150;width:260"><s:property value="crmDoctor.clinicCharacteristics"/></textarea></td>
					
					 <td class="label-title"><fmt:message key="biolims.common.otherInfo" /></td>
                    <td ><textarea class="text input false" id="crmDoctor_otherInformation" name="crmDoctor.otherInformation" tabindex="0" title="内容" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" style="height:150;width:260"><s:property value="crmDoctor.otherInformation"/></textarea></td>
                   
				</tr>
			</table>
			<input type="hidden" name="crmDoctorItemJson" id="crmDoctorItemJson" value="" />
			  <input type="hidden"  id="id_parent_hidden" value="<s:property value="crmDoctor.id"/>" />
			<div id="tabs">
		<ul>
					<li><a href="#crmDoctorItempage">
'<fmt:message key="biolims.common.doctorManagementDetails" /></a></li>
				</ul>
		    <div id="crmDoctorItempage" width="100%" height:10px></div>
		    </div>
			
		</form>
	</div>
</body>
</html>

