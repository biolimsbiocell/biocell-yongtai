﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/kb/knowledgeBaseDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="knowledgeBase_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">选择类别</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="knowledgeBase_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td>
           	   <td class="label-title">是否原创</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="knowledgeBase_original" searchField="true" name="original"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">标题</td>
               	<td align="left">
                    		<input type="text" maxlength="2000" id="knowledgeBase_title" searchField="true" name="title"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">导读</td>
               	<td align="left">
                    		<input type="text" maxlength="2000" id="knowledgeBase_daodu" searchField="true" name="daodu"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">内容</td>
               	<td align="left">
                    		<input type="text" maxlength="2000" id="knowledgeBase_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">关键词</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="knowledgeBase_antistop" searchField="true" name="antistop"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">相关知识</td>
               	<td align="left">
                    		<input type="text" maxlength="2000" id="knowledgeBase_correlation" searchField="true" name="correlation"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">不公开</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="knowledgeBase_noopen" searchField="true" name="noopen"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="knowledgeBase_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="knowledgeBase_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="knowledgeBase_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_knowledgeBase_div"></div>
   		
</body>
</html>



