﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=crmSaleBill&id=${crmSaleBill.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmSaleBillEdit.js"></script>
<%--  <script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script> --%>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编码</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="crmSaleBill_id"
                   	 name="crmSaleBill.id" title="编码" readonly = "readOnly" class="text input readonlytrue" 
                   	   
	value="<s:property value="crmSaleBill.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="60" id="crmSaleBill_name"
                   	 name="crmSaleBill.name" title="描述"
                   	   
	value="<s:property value="crmSaleBill.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showcrmContract" title="选择所属合同"
				hasHtmlFrame="true"
				html="${ctx}/crm/contract/crmContract/crmContractSelect.action"
				isHasSubmit="false" functionName="CrmContractFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleBill_crmContract').value=rec.get('id');
				document.getElementById('crmSaleBill_crmContract_name').value=rec.get('name');" />
				
			
			 --%>
               	 	<td class="label-title" >所属合同</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleBill_crmContract_name"  value="<s:property value="crmSaleBill.crmContract.name"/>"   readonly="readOnly"  />
 						<input type="hidden" id="crmSaleBill_crmContract" name="crmSaleBill.crmContract.id"  value="<s:property value="crmSaleBill.crmContract.id"/>" > 
 						<img alt='选择所属合同' id='showcrmContract' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="CrmContractFun()" />                   		
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title" >客户经理</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="crmSaleBill_crmContract_manager_name"
                   	 name="crmSaleBill.crmContract.manager.name" title="客户经理"
                   	   
					value="<s:property value="crmSaleBill.crmContract.manager.name"/>"
					class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
                   
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleBill_createUser').value=rec.get('id');
				document.getElementById('crmSaleBill_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleBill_createUser_name"  value="<s:property value="crmSaleBill.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="crmSaleBill_createUser" name="crmSaleBill.createUser.id"  value="<s:property value="crmSaleBill.createUser.id"/>" > 
 						 <%--   <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>                   		
                   	</td>
			
			
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="crmSaleBill_createDate"
                   	 name="crmSaleBill.createDate" title="创建日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="crmSaleBill.createDate" format="yyyy-MM-dd HH:mm:ss"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
						
			
			
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleBill_confirmUser').value=rec.get('id');
				document.getElementById('crmSaleBill_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleBill_confirmUser_name"  value="<s:property value="crmSaleBill.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="crmSaleBill_confirmUser" name="crmSaleBill.confirmUser.id"  value="<s:property value="crmSaleBill.confirmUser.id"/>" > 
 						 <%--   <img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>                  		
                   	</td>

               	 	<td class="label-title" >审核日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="crmSaleBill_confirmDate"
                   	 name="crmSaleBill.confirmDate" title="审核日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="crmSaleBill.confirmDate" format="yyyy-MM-dd  HH:mm:ss"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >工作流状态ID</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="crmSaleBill_state" name="crmSaleBill.state" value="<s:property value="crmSaleBill.state"/>"/>
                   	</td>
			
			
               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="crmSaleBill_stateName"
                   	 name="crmSaleBill.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="crmSaleBill.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content1</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content1"
                   	 name="crmSaleBill.content1" title="content1"
                   	   
	value="<s:property value="crmSaleBill.content1"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content2</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content2"
                   	 name="crmSaleBill.content2" title="content2"
                   	   
	value="<s:property value="crmSaleBill.content2"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content3</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content3"
                   	 name="crmSaleBill.content3" title="content3"
                   	   
	value="<s:property value="crmSaleBill.content3"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content4</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content4"
                   	 name="crmSaleBill.content4" title="content4"
                   	   
	value="<s:property value="crmSaleBill.content4"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content5</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content5"
                   	 name="crmSaleBill.content5" title="content5"
                   	   
	value="<s:property value="crmSaleBill.content5"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content6</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content6"
                   	 name="crmSaleBill.content6" title="content6"
                   	   
	value="<s:property value="crmSaleBill.content6"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content7</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content7"
                   	 name="crmSaleBill.content7" title="content7"
                   	   
	value="<s:property value="crmSaleBill.content7"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content8</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="crmSaleBill_content8"
                   	 name="crmSaleBill.content8" title="content8"
                   	   
	value="<s:property value="crmSaleBill.content8"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content9</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	  	<input type="text" size="20" maxlength="" id="crmSaleBill_content9"
                   	 name="crmSaleBill.content9" title="content9"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="crmSaleBill.content9" format="yyyy-MM-dd"/>" 
                   	      style="display:none" 
                   	  />
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content10</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	  	<input type="text" size="20" maxlength="" id="crmSaleBill_content10"
                   	 name="crmSaleBill.content10" title="content10"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="crmSaleBill.content10" format="yyyy-MM-dd"/>" 
                   	      style="display:none" 
                   	  />
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content11</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	  	<input type="text" size="20" maxlength="" id="crmSaleBill_content11"
                   	 name="crmSaleBill.content11" title="content11"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="crmSaleBill.content11" format="yyyy-MM-dd"/>" 
                   	      style="display:none" 
                   	  />
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content12</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="crmSaleBillItemJson" id="crmSaleBillItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="crmSaleBill.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
			<li><a href="#crmSaleBillItempage">发票开票信息</a></li>
           	</ul>  -->
			<div id="crmSaleBillItempage" width="100%" height:10px></div>
			</div>
        	<!-- </div> -->
	</body>
	</html>
