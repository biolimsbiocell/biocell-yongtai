﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmSaleOrder.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="18" id="crmSaleOrder_id"
                   	 name="id" searchField="true" title="编码"    />
                   	</td>
                   	
                   	<td class="label-title" >订单名称</td>
                   	<td align="left">
					<input type="text" size="20" maxlength="18" id="crmSaleOrder_name"
                   	 name="name" searchField="true" title="订单名称" />
                   	</td>
                   	
               	 	<!-- <td class="label-title" >描述</td>
                   	<td align="left"  >
					<input type="text" size="50" maxlength="60" id="crmSaleOrder_name"
                   	 name="name" searchField="true" title="描述"    />
                   	</td> -->
                   	
			</tr>
			<tr>
					<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
						hasHtmlFrame="true" 
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="ManagerUserFun" 
		 				hasSetFun="true" width="900" height="500"
						documentId="crmSaleOrder_manager"
						documentName="crmSaleOrder_manager_name"/>
               	 	<td class="label-title" >客户经理</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_manager_name" searchField="true"  name="manager.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_manager" name="crmSaleOrder.manager.id"  value="" > 
 						<img alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
	               	<g:LayOutWinTag buttonId="showorderSource" title="选择订单来源"
						hasHtmlFrame="true"
						html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectSource" 
		 				hasSetFun="true"
						documentId="crmSaleOrder_orderSource"
						documentName="crmSaleOrder_orderSource_name"/>
	               	<td class="label-title" >订单来源</td>
	                <td align="left"  >
	 					<input type="text" size="20"   id="crmSaleOrder_orderSource_name" searchField="true"  name="orderSource.name"  value="" class="text input" />
	 					<input type="hidden" id="crmSaleOrder_orderSource" name="crmSaleOrder.orderSource.id"  value="" > 
	 					<img alt='选择来源' id='showorderSource' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
            </tr>
            <tr>
                   	<td class="label-title" >下单日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startgenProjectDate" name="startgenProjectDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="genProjectDate1" name="genProjectDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endgenProjectDate" name="endgenProjectDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="genProjectDate2" name="genProjectDate##@@##2"  searchField="true" />
                   	</td>
                   	
	                 <g:LayOutWinTag buttonId="showparent" title="选择父级订单"
						hasHtmlFrame="true"
						html="${ctx}/crm/sale/crmSaleOrder/crmSaleOrderSelect.action"
						isHasSubmit="false" functionName="CrmSaleOrderFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_parent').value=rec.get('id');
						document.getElementById('crmSaleOrder_parent_name').value=rec.get('name');" />
               	 	<td class="label-title" >父级订单</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_parent_name" searchField="true"  name="parent.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_parent" name="crmSaleOrder.parent.id"  value="" > 
 						<img alt='选择父级订单' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>   	
			<%-- <g:LayOutWinTag buttonId="showcrmMarcketSale" title="选择市场推广活动"
				hasHtmlFrame="true"
				html="${ctx}/crm/marcket/crmMarcketActivity/crmMarcketActivitySelect.action"
				isHasSubmit="false" functionName="CrmMarcketSaleFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleOrder_crmMarcketActivity').value=rec.get('id');
				document.getElementById('crmSaleOrder_crmMarcketActivity_name').value=rec.get('name');" /> --%>
               	 <%-- 	<td class="label-title" >市场推广活动</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmMarcketActivity_name" searchField="true"  name="crmMarcketActivity.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmMarcketActivity" name="crmSaleOrder.crmMarcketActivity.id"  value="" > 
 						<img alt='选择市场推广活动' id='showcrmMarcketSale' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="CrmMarcketActivityFun()"/>                   		
                   	</td> --%>

			<%-- <g:LayOutWinTag buttonId="showcrmCustomer" title="选择客户"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="CrmCustomerFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleOrder_crmCustomer').value=rec.get('id');
				document.getElementById('crmSaleOrder_crmCustomer_name').value=rec.get('name');" />
               	 	<td class="label-title" >客户</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmCustomer_name" searchField="true"  name="crmCustomer.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmCustomer" name="crmSaleOrder.crmCustomer.id"  value="" > 
 						<img alt='选择客户' id='showcrmCustomer' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			</tr>
			<tr>
					<td class="label-title" >实际合同开始日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startstartDate" name="startstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate1" name="startDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endstartDate" name="endstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate2" name="startDate##@@##2"  searchField="true" />

                   	</td>
            </tr>
            <tr>       	
                   	<td class="label-title" >实际合同结束日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startendDate" name="startendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endendDate" name="endendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate2" name="endDate##@@##2"  searchField="true" />
                   	</td>
                   <%-- 	<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true"
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="CreateUserFun" 
		 				hasSetFun="true"
						documentId="crmSaleOrder_createUser"
						documentName="crmSaleOrder_createUser_name"/>
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_createUser" name="crmSaleOrder.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
            </tr>
            <tr>
            		<td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />

                   	</td>
                   	
                   	<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true"
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="CreateUserFun" 
		 				hasSetFun="true" width="900" height="500"
						documentId="crmSaleOrder_createUser"
						documentName="crmSaleOrder_createUser_name"/>
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_createUser" name="crmSaleOrder.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                        
              <!--  <td class="label-title" >金额</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="10" id="crmSaleOrder_money"
                   	 name="money" searchField="true" title="金额"    />
                   	</td> -->
			<%-- <g:LayOutWinTag buttonId="showtype" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
				documentId="crmSaleOrder_type"
				documentName="crmSaleOrder_type_name"/>
               	 	<td class="label-title" >分类</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_type" name="crmSaleOrder.type.id"  value="" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			</tr>
			<%-- <tr>
					<g:LayOutWinTag buttonId="showcrmSaleChance" title="选择商机"
						hasHtmlFrame="true"
						html="${ctx}/crm/sale/crmSaleChance/crmSaleChanceSelect.action"
						isHasSubmit="false" functionName="CrmSaleChanceFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_crmSaleChance').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmSaleChance_name').value=rec.get('name');" />
               	 	<td class="label-title" >商机</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmSaleChance_name" searchField="true"  name="crmSaleChance.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmSaleChance" name="crmSaleOrder.crmSaleChance.id"  value="" > 
 						<img alt='选择商机' id='showcrmSaleChance' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			       	
			</tr> --%>
			<tr>                   	
				<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="CreateUserFun" 
	 				hasSetFun="true"
					documentId="crmSaleOrder_createUser"
					documentName="crmSaleOrder_createUser_name"/>
	               	 	<td class="label-title" >创建人</td>
	                   	<td align="left"  >
	 						<input type="text" size="20"   id="crmSaleOrder_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
	 						<input type="hidden" id="crmSaleOrder_createUser" name="crmSaleOrder.createUser.id"  value="" > 
	 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
	                   	</td> --%>

               	 	<!-- <td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />

                   	</td> -->
			</tr>
			<%-- <tr>                   	
		
					<g:LayOutWinTag buttonId="showcrmContract" title="选择销售合同"
						hasHtmlFrame="true"
						html="${ctx}/crm/contract/crmContract/crmContractSelect.action"
						isHasSubmit="false" functionName="CrmContractFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_crmContract').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmContract_name').value=rec.get('name');" />
               	 	<td class="label-title" >销售合同</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmContract_name" searchField="true"  name="crmContract.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmContract" name="crmSaleOrder.crmContract.id"  value="" > 
 						<img alt='选择销售合同' id='showcrmContract' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr> --%>
			<tr>
				<td class="label-title" >审核日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                   	</td>
				<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="ConfirmUserFun" 
	 				hasSetFun="true" width="900" height="500"
					documentId="crmSaleOrder_confirmUser"
					documentName="crmSaleOrder_confirmUser_name"/>
               	 	<td class="label-title" >审核人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_confirmUser" name="crmSaleOrder.confirmUser.id"  value="" > 
 						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>                   	
               	 	<!-- <td class="label-title"  style="display:none"  >工作流状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="crmSaleOrder_state"
                   	 name="state" searchField="true" title="工作流状态"   style="display:none"    />

 					<input type="hidden" id="crmSaleOrder_state" name="state" searchField="true" value=""/>"
                   	</td>

               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="crmSaleOrder_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />

                   	</td>
                   	
                   	<td class="label-title" >备注</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="18" id="crmSaleOrder_note"
                   	 name="note" searchField="true" title="备注" />
                   	</td> -->
               	 	<!-- <td class="label-title" >项目要求</td>
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="crmSaleOrder_projectAsk"
                   	 name="projectAsk" searchField="true" title="项目要求"    />
                   	<textarea class="text input  false" id="crmSaleOrder_projectAsk" name="projectAsk" searchField="true"  title="项目要求" style="overflow: hidden; width: 350px; height: 140px;" ><s:property value="crmSaleOrder.projectAsk"/></textarea>
                </td> -->
			</tr>
			<tr>
				<td class="label-title" >合同编号</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="18" id="crmSaleOrder_crmContract_id"
                   	 name="crmContract.id" searchField="true" title="合同编号" />
                   	</td>
                <td class="label-title" >合同名称</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="18" id="crmSaleOrder_crmContract_name"
                   	 name="crmContract.name" searchField="true" title="合同名称" />
                 </td>
             <tr>
             	<!-- <td class="label-title" >实际合同开始日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startstartDate" name="startstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate1" name="startDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endstartDate" name="endstartDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="startDate2" name="startDate##@@##2"  searchField="true" />
                </td>
                <td class="label-title" >实际合同结束日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startendDate" name="startendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endendDate" name="endendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate2" name="endDate##@@##2"  searchField="true" />
                </td> -->
             	
             </tr>  
            
						
				<g:LayOutWinTag buttonId="showcrmCustomer" title="选择委托人"
					hasHtmlFrame="true"
					html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
					isHasSubmit="false" functionName="CrmCustomerFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('crmSaleOrder_crmCustomer').value=rec.get('id');
					document.getElementById('crmSaleOrder_crmCustomer_name').value=rec.get('name');" />
					
               	 	<td class="label-title" >委托人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmCustomer_name" searchField="true"  name="crmCustomer.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmCustomer" name="crmSaleOrder.crmCustomer.id"  value="" > 
 						<img alt='选择委托人' id='showcrmCustomer' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   
                   	<td class="label-title" >委托单位</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="18" id="crmSaleOrder_crmContract_client"
	                   	 name="client" searchField="true" title="委托单位" />
                 	</td>
               <tr>
                 	<td class="label-title" >项目名称</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="18" id="crmSaleOrder_content2"
	                   	 name="content2" searchField="true" title="项目名称" />
                 	</td>
                 	<g:LayOutWinTag buttonId="showprojectStart" title="选择初步设计"
						hasHtmlFrame="true"
						html="${ctx}/exp/project/projectStart/projectStartSelect.action"
						isHasSubmit="false" functionName="ProjectStartFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_projectStart').value=rec.get('id');
						document.getElementById('crmSaleOrder_projectStart_name').value=rec.get('name');" />
                 	<td class="label-title" >初步设计</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_projectStart_name" searchField="true"  name="projectStart.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_projectStart" name="crmSaleOrder.projectStart.id"  value="" > 
 						<img alt='选择初步设计' id='showprojectStart' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               </tr>    	
               <tr>
                 	<td class="label-title" >基因ID</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="18" id="crmSaleOrder_geneId"
	                   	 name="geneId" searchField="true" title="基因ID" />
                 	</td>
                 	<td class="label-title" >基因名称</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="18" id="crmSaleOrder_geneName"
	                   	 name="geneName" searchField="true" title="基因名称" />
                 	</td>
                </tr>
                <tr>
                 	<td class="label-title" >基因别名</td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="18" id="crmSaleOrder_geneByName"
	                   	 name="geneByName" searchField="true" title="基因别名" />
                 	</td>
                
	                <g:LayOutWinTag buttonId="showType" title="选择项目类型"
						hasHtmlFrame="true"
						html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="workType" 
			 			hasSetFun="true"
						documentId="crmSaleOrder_workType"
						documentName="crmSaleOrder_workType_name" />
		            <td class="label-title" >项目类型</td>
		            <td align="left"  >
	 					<input type="text" size="20"   id="crmSaleOrder_workType_name" searchField="true"  name="workType.name"  value="" class="text input" />
	 					<input type="hidden" id="crmSaleOrder_workType" name="crmSaleOrder.workType.id"  value="" > 
	 					<img alt='选择项目类型' id='showType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
	                </td>
				</tr>
				<tr>
						<g:LayOutWinTag buttonId="showtechnologyType" title="选择技术类型"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="modelPreparation"
							hasSetFun="true" documentId="crmSaleOrder_technologyType"
							documentName="crmSaleOrder_technologyType_name" />
						<td class="label-title" >技术类型</td>
			                 <td align="left"  >
		 						<input type="text" size="20"   id="crmSaleOrder_technologyType_name" searchField="true"  name="technologyType.name"  value="" class="text input" />
		 						<input type="hidden" id="crmSaleOrder_technologyType" name="crmSaleOrder.technologyType.id"  value="" > 
		 						<img alt='选择项目类型' id='showtechnologyType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                   	</td>
						
						<g:LayOutWinTag buttonId="showGeneTargetingType" title="选择基因打靶类型"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="dbMethod" hasSetFun="true"
							documentId="crmSaleOrder_geneTargetingType"
							documentName="crmSaleOrder_geneTargetingType_name" />
						<td class="label-title" >基因打靶类型</td>
			                 <td align="left"  >
		 						<input type="text" size="20"   id="crmSaleOrder_geneTargetingType_name" searchField="true"  name="geneTargetingType.name"  value="" class="text input" />
		 						<input type="hidden" id="crmSaleOrder_geneTargetingType" name="crmSaleOrder.geneTargetingType.id"  value="" > 
		 						<img alt='选择基因打靶类型' id='showGeneTargetingType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                   	</td>
		           </tr>        	
		           <tr>   	
						<g:LayOutWinTag buttonId="showtype" title="选择产品类型"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
							documentId="crmSaleOrder_type"
							documentName="crmSaleOrder_type_name" />
						<td class="label-title" >产品类型</td>
			                 <td align="left"  >
		 						<input type="text" size="20"   id="crmSaleOrder_type_name" searchField="true"  name="type.name"  value="" class="text input" />
		 						<input type="hidden" id="crmSaleOrder_type" name="crmSaleOrder.type.id"  value="" > 
		 						<img alt='选择产品类型' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                   	</td>
		                   	
		                <g:LayOutWinTag buttonId="showgenus" title="选择种属"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="genera" hasSetFun="true"
							documentId="crmSaleOrder_genus"
							documentName="crmSaleOrder_genus_name" />
						<td class="label-title" >种属</td>
			                 <td align="left"  >
		 						<input type="text" size="20"   id="crmSaleOrder_genus_name" searchField="true"  name="genus.name"  value="" class="text input" />
		 						<input type="hidden" id="crmSaleOrder_genus" name="crmSaleOrder.genus.id"  value="" > 
		 						<img alt='选择种属' id='showgenus' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                   	</td>
		           </tr>
		           <tr>   	
		                <g:LayOutWinTag buttonId="showstrain" title="选择品系"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="saleChanceStrain" hasSetFun="true"
							documentId="crmSaleOrder_strain"
							documentName="crmSaleOrder_strain_name" />
						<td class="label-title" >品系</td>
			            <td align="left"  >
		 						<input type="text" size="20"   id="crmSaleOrder_strain_name" searchField="true"  name="strain.name"  value="" class="text input" />
		 						<input type="hidden" id="crmSaleOrder_strain" name="crmSaleOrder.strain.id"  value="" > 
		 						<img alt='选择品系' id='showstrain' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                </td>
		                <g:LayOutWinTag buttonId="showCheckUp" title="选择质控要求"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="checkUp" hasSetFun="true"
							documentId="crmSaleOrder_checkup"
							documentName="crmSaleOrder_checkup_name" />
						<td class="label-title" >质控要求</td>
			            <td align="left"  >
		 					<input type="text" size="20"   id="crmSaleOrder_checkup_name" searchField="true"  name="checkup.name"  value="" class="text input" />
		 					<input type="hidden" id="crmSaleOrder_checkup" name="crmSaleOrder.checkup.id"  value="" > 
		 					<img alt='选择质控要求' id='showCheckUp' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                </td>
		            </tr>
		            <tr>
		            	<g:LayOutWinTag buttonId="showProduct" title="选择终产品"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="finalType" hasSetFun="true"
							documentId="crmSaleOrder_product"
							documentName="crmSaleOrder_product_name" />
						<td class="label-title" >终产品</td>
			            <td align="left"  >
		 						<input type="text" size="20"   id="crmSaleOrder_product_name" searchField="true"  name="product.name"  value="" class="text input" />
		 						<input type="hidden" id="crmSaleOrder_product" name="crmSaleOrder.product.id"  value="" > 
		 						<img alt='选择终产品' id='showProduct' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                </td>
		                <td class="label-title" >终产品数量</td>
	                   	<td align="left"  >
							<input type="text" size="20" maxlength="18" id="crmSaleOrder_productNo"
		                   	 name="productNo" searchField="true" title="基因别名" />
                 		</td>
	                 	<%-- <g:LayOutWinTag buttonId="showCheckUp" title="选择质控要求"
							hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
							isHasSubmit="false" functionName="checkUp" hasSetFun="true"
							documentId="crmSaleOrder_checkup"
							documentName="crmSaleOrder_checkup_name" />
						<td class="label-title" >质控要求</td>
			            <td align="left"  >
		 					<input type="text" size="20"   id="crmSaleOrder_checkup_name" searchField="true"  name="checkup.name"  value="" class="text input" />
		 					<input type="hidden" id="crmSaleOrder_checkup" name="crmSaleOrder.checkup.id"  value="" > 
		 					<img alt='选择质控要求' id='showCheckUp' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
		                </td> --%>
		           </tr>
		           <tr>
		           		<td class="label-title" >客户要求</td>
		                   	<td align="left"  >
								<input type="text" size="20" maxlength="18" id="crmSaleOrder_customerRequirements"
			                   	 name="customerRequirements" searchField="true" title="基因别名" />
	                 	</td>
		                <td class="label-title">是否去Neo</td>
						<%-- <td class="requiredcolumn" nowrap width="10px"><img
							class='requiredimage' src='${ctx}/images/notrequired.gif' /></td> --%>
						<td align="left">
							<select id="crmSaleOrder_neoResult" name="neoResult" searchField="true" class="input-10-length">
								<option value=""selected="selected">请选择</option>
								<option value="0">否</option>
								<option value="1">是</option>
							</select>
				 		</td>
					 	<!-- <td class="label-title" >客户要求</td>
		                   	<td align="left"  >
								<input type="text" size="20" maxlength="18" id="crmSaleOrder_customerRequirements"
			                   	 name="customerRequirements" searchField="true" title="基因别名" />
	                 	</td> -->
		            </tr>
					<tr>	
							<td class="label-title"  style="display:none"  >工作流状态</td>
		                   	<td align="left"   style="display:none">
		                  
							<input type="text" size="40" maxlength="30" id="crmSaleOrder_state"
		                   	 name="state" searchField="true" title="工作流状态"   style="display:none"    />
		
		 					<input type="hidden" id="crmSaleOrder_state" name="state" searchField="true" value=""/>"
		                   	</td>

		               	 	<td class="label-title" >工作流状态</td>
		                   	<td align="left"  >
		                  
							<input type="text" size="40" maxlength="30" id="crmSaleOrder_stateName"
		                   	 name="stateName" searchField="true" title="工作流状态"    />
		
		                   	</td>
                   	
                   	<td class="label-title" >备注</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="18" id="crmSaleOrder_note"
                   	 name="note" searchField="true" title="备注" />
                   	</td>
					</tr>
				<%-- <g:LayOutWinTag buttonId="showorderSource" title="选择来源"
					hasHtmlFrame="true"
					html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="orderSource" 
	 				hasSetFun="true"
					documentId="crmSaleOrder_orderSource"
					documentName="crmSaleOrder_orderSource_name"/>
               	 	<td class="label-title" >来源</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_orderSource_name" searchField="true"  name="orderSource.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_orderSource" name="crmSaleOrder.orderSource.id"  value="" > 
 						<img alt='选择来源' id='showorderSource' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
                   	
                   	
            <tr>                 	
               	 	<td class="label-title"  style="display:none"  >content1</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content1"
                   	 name="content1" searchField="true" title="content1"   style="display:none"    />

                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content2</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content2"
                   	 name="content2" searchField="true" title="content2"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content3</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content3"
                   	 name="content3" searchField="true" title="content3"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content4</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content4"
                   	 name="content4" searchField="true" title="content4"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content5</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content5"
                   	 name="content5" searchField="true" title="content5"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content6</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content6"
                   	 name="content6" searchField="true" title="content6"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content7</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content7"
                   	 name="content7" searchField="true" title="content7"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content8</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleOrder_content8"
                   	 name="content8" searchField="true" title="content8"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content9</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent9" name="startcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content91" name="content9##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent9" name="endcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content92" name="content9##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content10</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent10" name="startcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content101" name="content10##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent10" name="endcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content102" name="content10##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content11</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent11" name="startcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content111" name="content11##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent11" name="endcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content112" name="content11##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content12</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent12" name="startcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content121" name="content12##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent12" name="endcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content122" name="content12##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_crmSaleOrder_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_crmSaleOrder_tree_page"></div>
</body>
</html>



