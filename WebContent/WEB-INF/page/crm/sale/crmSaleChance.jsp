﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmSaleChance.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="crmSaleChance_id"
                   	 name="id" searchField="true" title="编码"    />
  
                   	<!-- </td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="crmSaleChance_name"
                   	 name="name" searchField="true" title="描述"    />
                   	</td> -->
                   	<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
						hasHtmlFrame="true"
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="ManagerUserFun" 
		 				hasSetFun="true"
						documentId="crmSaleChance_manager"
						documentName="crmSaleChance_manager_name"/>
               	 	<td class="label-title" >客户经理</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_manager_name" searchField="true"  name="manager.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_manager" name="crmSaleChance.manager.id"  value="" > 
 						<img alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>                   	
				<g:LayOutWinTag buttonId="showsource" title="选择项目来源"
					hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="projectSource" hasSetFun="true"
					documentId="crmSaleChance_chanceSource"
					documentName="crmSaleChance_chanceSource_name"
				/>
               	<td class="label-title" >项目来源</td>
                <td align="left"  >
 					<input type="text" size="20"   id="crmSaleChance_chanceSource_name" searchField="true"  name="chanceSource.name"  value="" class="text input" />
 					<input type="hidden" id="crmSaleChance_chanceSource" name="crmSaleChance.chanceSource.id"  value="" > 
 					<img alt='选择来源' id='showsource' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                </td>
                
                <g:LayOutWinTag buttonId="showbussinessType" title="选择项目类型"
					hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="crmSaleChance" hasSetFun="true"
					documentId="crmSaleChance_bussinessType"
					documentName="crmSaleChance_bussinessType_name" />

					<td class="label-title">项目类型</td>
					<td align="left"><input type="text" size="20"
						id="crmSaleChance_bussinessType_name" name="bussinessType.name" searchField="true"
						value="" />
						<input type="hidden" id="crmSaleChance_bussinessType"
						name="crmSaleChance.bussinessType.id"
						value="">
						<img alt='选择项目类型' id='showbussinessType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>

				<%-- <g:LayOutWinTag buttonId="showtype" title="选择分类"
					hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
					documentId="crmSaleChance_type"
					documentName="crmSaleChance_type_name"
				/>
               	 	<td class="label-title" >分类</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_type" name="crmSaleChance.type.id"  value="" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			</tr>
			<tr>     
					<g:LayOutWinTag buttonId="showproductType" title="选择产品类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
						documentId="crmSaleChance_productType"
						documentName="crmSaleChance_productType_name" />
					<td class="label-title">产品类型</td>
				
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_productType_name" name="productType.name" searchField="true"
						value=""
						readonly="readOnly" /> 
						<input type="hidden"
						id="crmSaleChance_productType" name="crmSaleChance.productType.id"
						value="">
						<img alt='选择产品类型' id='showproductType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					
					<g:LayOutWinTag buttonId="showdbMethodType" title="选择基因打靶类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="dbMethod" hasSetFun="true"
						documentId="crmSaleChance_dbMethod"
						documentName="crmSaleChance_dbMethod_name" />
					<td class="label-title">基因打靶类型</td>
					
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_dbMethod_name" name="dbMethod.name" searchField="true"
						value=""
						readonly="readOnly" /> 
						<input type="hidden"
						id="crmSaleChance_dbMethod" name="crmSaleChance.dbMethod.id"
						value="">
						<img alt='选择基因打靶类型' id='showdbMethodType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
						              	
					<%-- <g:LayOutWinTag buttonId="showarea" title="选择地区"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="showAreaList" hasSetFun="true"
						documentId="crmSaleChance_area"
						documentName="crmSaleChance_area_name"
					/>
               	 	<td class="label-title" >地区</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_area_name" searchField="true"  name="area.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_area" name="crmSaleChance.area.id"  value="" > 
 						<img alt='选择地区' id='showarea' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >邮件</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="30" id="crmSaleChance_email"
                   	 name="email" searchField="true" title="邮件"    />
                   	</td> --%>
			</tr>
			<tr>
					<g:LayOutWinTag buttonId="showtechnologyType" title="选择技术类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="modelPreparation" hasSetFun="true"
						documentId="crmSaleChance_technologyType"
						documentName="crmSaleChance_technologyType_name" />
					<td class="label-title">技术类型</td>
					
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_technologyType_name"  searchField="true" 
						name="technologyType.name" value="" readonly="readOnly" /> 
						<input type="hidden" id="crmSaleChance_technologyType"
						name="crmSaleChance.technologyType.id" value="">
						<img alt='选择技术类型' id='showtechnologyType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
               	 	
               	 	<!-- <td class="label-title" >电话</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="20" id="crmSaleChance_phone"
                   	 name="phone" searchField="true" title="电话"    />
                   	</td> -->
                	<g:LayOutWinTag buttonId="showgenus" title="选择种属"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="saleChanceGeners" hasSetFun="true"
						documentId="crmSaleChance_genus"
						documentName="crmSaleChance_genus_name"
				 	/>
               	 	<td class="label-title" >种属</td>
               	             	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_genus_name" name="genus.name" value="" searchField="true" />
 						<input type="hidden" id="crmSaleChance_genus" name="crmSaleChance.genus.id"  value="" > 
 						<img alt='选择种属' id='showgenus' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                	
				<%-- <g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="ManagerUserFun" 
	 				hasSetFun="true"
					documentId="crmSaleChance_manager"
					documentName="crmSaleChance_manager_name"/>
               	 	<td class="label-title" >客户经理</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_manager_name" searchField="true"  name="manager.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_manager" name="crmSaleChance.manager.id"  value="" > 
 						<img alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			</tr>
			<tr>
					<g:LayOutWinTag buttonId="showstrain" title="选择品系"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="saleChanceStrain" hasSetFun="true"
						documentId="crmSaleChance_strain"
						documentName="crmSaleChance_strain_name"
				 	/>
               	 	<td class="label-title" >品系</td>
               	 	            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_strain_name" name="strain.name" value="" searchField="true" />
 						<input type="hidden" id="crmSaleChance_strain" name="crmSaleChance.strain.id"  value="" > 
 						<img alt='选择品系' id='showstrain' src='${ctx}/images/img_lookup.gif' class='detail'    />                   		
                   	</td>
                   	
                   	<td class="label-title" >期望结束日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startendDate" name="startendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="endDate1" name="endDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endendDate" name="endendDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
            		    <input type="hidden" id="endDate2" name="endDate##@@##2"  searchField="true" />
                   	</td>
                   	
             <tr>
					<td class="label-title" >基因ID(NCBI)</td>
                   	<td align="left"  >
						<input type="text" size="20" maxlength="18" id="crmSaleChance_geneId"
                   	 		name="geneId" searchField="true" title="基因ID" />
                   	</td>
                   	 
                   	 <td class="label-title" >基因名称</td>
                   	<td align="left"  >
						<input type="text" size="20" maxlength="18" id="crmSaleChance_geneName"
                   	 		name="geneName" searchField="true" title="基因名称" />
             		</td>
             </tr>
                   	<g:LayOutWinTag buttonId="showchangeUser" title="选择项目评估负责人"
						hasHtmlFrame="true"
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="changeUserFun" 
		 				hasSetFun="true" width="900" height="500"
						documentId="crmSaleChance_changeUserId"
						documentName="crmSaleChance_changeUser"/>
                   	<td class="label-title">项目评估负责人</td>
			                    	 				 
                   	<td align="left">                   	
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleChance_changeUser" name="changeUser" searchField="true" value="" class="text input"/>
 						<input type="hidden" size="20" readonly="readOnly"  id="crmSaleChance_changeUserId" name="changeUserId"  value="" class="text input"/>
 						<img alt='选择项目评估负责人' id='showchangeUser'  src='${ctx}/images/img_lookup.gif' 	class='detail' />
                   	</td>
                   	                   	
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true" width="900" height="500"
				documentId="crmSaleChance_createUser"
				documentName="crmSaleChance_createUser_name"/>
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_createUser" name="crmSaleChance.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>

               	 	<!-- <td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
            		    <input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                   	</td> -->
			</tr>
			
			<tr>    
					<td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
            		    <input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                   	</td>               	
               	 	<td class="label-title"  style="display:none"  >工作流状态ID</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="30" id="crmSaleChance_state"
                   	 name="state" searchField="true" title="工作流状态ID"   style="display:none"    /> 
 					<input type="hidden" id="crmSaleChance_state" name="state" searchField="true" value=""/>"
                   	</td>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="30" id="crmSaleChance_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	</td>
            </tr>       	
            <tr>
                   	<td class="label-title" >客户要求</td>
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="crmSaleChance_content1"
                   	 name="content1" searchField="true" title="客户要求"    />
                   	</td>
           	</tr>
			<tr> 

               	 	<!-- <td class="label-title" >商机内容</td>
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="crmSaleChance_content"
                   	 name="content" searchField="true" title="工作流状态"    />
                   	</td> -->
                   <!-- 	<textarea class="text input  false" id="crmSaleChance_content" name="content" searchField="true"  title="商机内容" style="overflow: hidden; width: 350px; height: 140px;" ><s:property value="crmSaleChance.content"/></textarea> -->
                   	<!-- <td class="label-title">跟踪人员</td>
               		<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_followUser" searchField="true" name="followUser"  class="input-20-length"></input>
               	</td> -->
               	</tr>
			<tr> 
           	 	<!-- <td class="label-title">跟踪日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_followDate" searchField="true" name="followDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">跟踪结果</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_followResult" searchField="true" name="followResult"  class="input-20-length"></input>
               	</td> -->
			</tr>
			<tr>
           	 	<!-- <td class="label-title">结果说明</td>
               	<td align="left">
                    		<input type="text" maxlength="250" id="crmSaleChance_followExplain" searchField="true" name="followExplain"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">商机跟踪成单可能性</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmSaleChance_orderChance" searchField="true" name="orderChance"  class="input-20-length"></input>
               	</td> -->
             </tr>
			<tr>  	
               	<!-- <td class="label-title">评估类型</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_evalustionType" searchField="true" name="evalustionType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">业务类型</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_bussinessType" searchField="true" name="bussinessType"  class="input-20-length"></input>
               	</td> -->
            </tr>
			<tr>   	
           	 	<!-- <td class="label-title">产品类型</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmSaleChance_productType" searchField="true" name="productType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">物种</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmSaleChance_resType" searchField="true" name="resType"  class="input-20-length"></input>
               	</td> -->
            </tr>
			<tr>   	
           	 	<!-- <td class="label-title">技术类型</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmSaleChance_technologyType" searchField="true" name="technologyType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">预计评估完成时间</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_preFinishDate" searchField="true" name="preFinishDate"  class="input-20-length"></input>
               	</td> -->
           </tr>
			<tr>    	
           	 	<!-- <td class="label-title">项目评估负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_evalustionUser" searchField="true" name="evalustionUser"  class="input-20-length"></input>
               	</td> -->
               	<%-- <g:LayOutWinTag buttonId="showEvaUser" title="选择项目评估负责人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="EvaUserFun" 
 				hasSetFun="true"
				documentId="crmSaleChance_evalustionUser"
				documentName="crmSaleChance_evalustionUser_name"/>
               	 	<td class="label-title" >项目评估负责人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_evalustionUser_name" searchField="true"  name="evalustionUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_evalustionUser" name="crmSaleChance.evalustionUser.id"  value="" > 
 						<img alt='选择项目评估负责人' id='showEvaUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
           	 	<td class="label-title">评估成单可能性</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="crmSaleChance_successChance" searchField="true" name="successChance"  class="input-20-length"></input>
               	</td> --%>
			</tr>
			<tr>                   	
               	 	<td class="label-title"  style="display:none"  >content1</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content1"
                   	 name="content1" searchField="true" title="content1"   style="display:none"    />
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content2</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content2"
                   	 name="content2" searchField="true" title="content2"   style="display:none"    />

                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content3</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content3"
                   	 name="content3" searchField="true" title="content3"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content4</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content4"
                   	 name="content4" searchField="true" title="content4"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content5</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content5"
                   	 name="content5" searchField="true" title="content5"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content6</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content6"
                   	 name="content6" searchField="true" title="content6"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content7</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content7"
                   	 name="content7" searchField="true" title="content7"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content8</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleChance_content8"
                   	 name="content8" searchField="true" title="content8"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content9</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent9" name="startcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content91" name="content9##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent9" name="endcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content92" name="content9##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content10</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent10" name="startcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content101" name="content10##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent10" name="endcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content102" name="content10##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content11</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent11" name="startcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content111" name="content11##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent11" name="endcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content112" name="content11##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content12</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent12" name="startcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content121" name="content12##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent12" name="endcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content122" name="content12##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_crmSaleChance_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_crmSaleChance_tree_page"></div>
</body>
</html>



