﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmBusinessManage.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="crmBusinessManage_id"
                   	 name="id" searchField="true" title="编码"    />
                   	
					
 					
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >商机来源</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="crmBusinessManage_name"
                   	 name="name" searchField="true" title="商机来源"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showtype" title="选择客户经理"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
				documentId="crmBusinessManage_type"
				documentName="crmBusinessManage_type_name"
			/>
               	 	<td class="label-title" >客户经理</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="crmBusinessManage_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="crmBusinessManage_type" name="crmBusinessManage.type.id"  value="" > 
 						<img alt='选择客户经理' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showstate" title="选择状态"
				hasHtmlFrame="true"
				html="${ctx}/dic/state/stateSelect.action?type=commonState"
				isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
				documentId="crmBusinessManage_state"
				documentName="crmBusinessManage_state_name" />
               	 	<td class="label-title" >状态</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="crmBusinessManage_state_name" searchField="true"  name="state.name"  value="" class="text input" />
 						<input type="hidden" id="crmBusinessManage_state" name="crmBusinessManage.state.id"  value="" > 
 						<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmBusinessManage_createUser').value=rec.get('id');
				document.getElementById('crmBusinessManage_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="crmBusinessManage_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmBusinessManage_createUser" name="crmBusinessManage.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="100" id="crmBusinessManage_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showpostType" title="选择岗位类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showpostTypeFun" hasSetFun="true"
				documentId="crmBusinessManage_postType"
				documentName="crmBusinessManage_postType_name"
			/>
               	 	<td class="label-title" >岗位类型</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="crmBusinessManage_postType_name" searchField="true"  name="postType.name"  value="" class="text input" />
 						<input type="hidden" id="crmBusinessManage_postType" name="crmBusinessManage.postType.id"  value="" > 
 						<img alt='选择岗位类型' id='showpostType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showlinkmanNname" title="选择联系人姓名"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmBusinessManage_linkmanNname').value=rec.get('id');
				document.getElementById('crmBusinessManage_linkmanNname_name').value=rec.get('name');" />
               	 	<td class="label-title" >联系人姓名</td>
                   	<td align="left"  >
 						<input type="text" size="50"   id="crmBusinessManage_linkmanNname_name" searchField="true"  name="linkmanNname.name"  value="" class="text input" />
 						<input type="hidden" id="crmBusinessManage_linkmanNname" name="crmBusinessManage.linkmanNname.id"  value="" > 
 						<img alt='选择联系人姓名' id='showlinkmanNname' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >联系人手机</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="crmBusinessManage_linkmanMobile"
                   	 name="linkmanMobile" searchField="true" title="联系人手机"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >固定电话</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="crmBusinessManage_linkmanPhone"
                   	 name="linkmanPhone" searchField="true" title="固定电话"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >相关主表</td>
                   	<td align="left"   style="display:none">
 						<input type="text" size="15"   id="crmBusinessManage_businessManage_name" searchField="true"  name="businessManage.name"  value="" class="text input" 	style="display:none" />
 						<input type="hidden" id="crmBusinessManage_businessManage" name="crmBusinessManage.businessManage.id"  value="" > 
 						<img alt='选择相关主表' id='showbusinessManage' src='${ctx}/images/img_lookup.gif' 	class='detail'   style="display:none"    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >email</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="crmBusinessManage_email"
                   	 name="email" searchField="true" title="email"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >单位名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="crmBusinessManage_unitName"
                   	 name="unitName" searchField="true" title="单位名称"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >单位地址</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="crmBusinessManage_unitAddress"
                   	 name="unitAddress" searchField="true" title="单位地址"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showbelongConsignor" title="选择所属委托人"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmBusinessManage_belongConsignor').value=rec.get('id');
				document.getElementById('crmBusinessManage_belongConsignor_name').value=rec.get('name');" />
               	 	<td class="label-title" >所属委托人</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="crmBusinessManage_belongConsignor_name" searchField="true"  name="belongConsignor.name"  value="" class="text input" />
 						<input type="hidden" id="crmBusinessManage_belongConsignor" name="crmBusinessManage.belongConsignor.id"  value="" > 
 						<img alt='选择所属委托人' id='showbelongConsignor' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >研究方向</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="crmBusinessManage_resDirection"
                   	 name="resDirection" searchField="true" title="研究方向"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >部门/学科</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="crmBusinessManage_dept"
                   	 name="dept" searchField="true" title="部门/学科"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showindustryType" title="选择行业类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showindustryTypeFun" hasSetFun="true"
				documentId="crmBusinessManage_industryType"
				documentName="crmBusinessManage_industryType_name"
			/>
               	 	<td class="label-title" >行业类型</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="crmBusinessManage_industryType_name" searchField="true"  name="industryType.name"  value="" class="text input" />
 						<input type="hidden" id="crmBusinessManage_industryType" name="crmBusinessManage.industryType.id"  value="" > 
 						<img alt='选择行业类型' id='showindustryType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >国家</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="crmBusinessManage_country"
                   	 name="country" searchField="true" title="国家"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >省</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="crmBusinessManage_privince"
                   	 name="privince" searchField="true" title="省"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >市</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="15" id="crmBusinessManage_city"
                   	 name="city" searchField="true" title="市"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content1</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="50" id="crmBusinessManage_content1"
                   	 name="content1" searchField="true" title="content1"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content2</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="50" id="crmBusinessManage_content2"
                   	 name="content2" searchField="true" title="content2"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content3</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="50" id="crmBusinessManage_content3"
                   	 name="content3" searchField="true" title="content3"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content4</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="50" maxlength="50" id="crmBusinessManage_content4"
                   	 name="content4" searchField="true" title="content4"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content5</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent5" name="startcontent5" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content51" name="content5##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent5" name="endcontent5" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content52" name="content5##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content6</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent6" name="startcontent6" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content61" name="content6##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent6" name="endcontent6" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content62" name="content6##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content7</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent7" name="startcontent7" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content71" name="content7##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent7" name="endcontent7" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content72" name="content7##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content8</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent8" name="startcontent8" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content81" name="content8##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent8" name="endcontent8" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content82" name="content8##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content9</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="10" id="crmBusinessManage_content9"
                   	 name="content9" searchField="true" title="content9"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content10</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="10" id="crmBusinessManage_content10"
                   	 name="content10" searchField="true" title="content10"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content11</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="10" id="crmBusinessManage_content11"
                   	 name="content11" searchField="true" title="content11"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content12</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="10" id="crmBusinessManage_content12"
                   	 name="content12" searchField="true" title="content12"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_crmBusinessManage_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_crmBusinessManage_tree_page"></div>
</body>
</html>



