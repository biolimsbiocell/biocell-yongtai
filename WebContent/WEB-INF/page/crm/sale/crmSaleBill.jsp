﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmSaleBill.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="crmSaleBill_id"
                   	 name="id" searchField="true" title="编码"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="crmSaleBill_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcrmContract" title="选择所属合同"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/crmSaleBill/crmContractSelect.action"
				isHasSubmit="false" functionName="CrmContractFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleBill_crmContract').value=rec.get('id');
				document.getElementById('crmSaleBill_crmContract_name').value=rec.get('name');" />
               	 	<td class="label-title" >所属合同</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleBill_crmContract_name" searchField="true"  name="crmContract.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleBill_crmContract" name="crmSaleBill.crmContract.id"  value="" > 
 						<img alt='选择所属合同' id='showcrmContract' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/crmSaleBill/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleBill_createUser').value=rec.get('id');
				document.getElementById('crmSaleBill_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleBill_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleBill_createUser" name="crmSaleBill.createUser.id"  value="" > 
 						 <%--   <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>                   		
                   	</td>
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/crmSaleBill/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleBill_confirmUser').value=rec.get('id');
				document.getElementById('crmSaleBill_confirmUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >审核人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleBill_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleBill_confirmUser" name="crmSaleBill.confirmUser.id"  value="" > 
 						 <%--   <img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>                  		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >审核日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >工作流状态ID</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="40" maxlength="30" id="crmSaleBill_state"
                   	 name="state" searchField="true" title="工作流状态ID"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
 					<input type="hidden" id="crmSaleBill_state" name="state" searchField="true" value=""/>"
                   	</td>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="crmSaleBill_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content1</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content1"
                   	 name="content1" searchField="true" title="content1"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content2</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content2"
                   	 name="content2" searchField="true" title="content2"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content3</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content3"
                   	 name="content3" searchField="true" title="content3"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content4</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content4"
                   	 name="content4" searchField="true" title="content4"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content5</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content5"
                   	 name="content5" searchField="true" title="content5"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content6</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content6"
                   	 name="content6" searchField="true" title="content6"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content7</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content7"
                   	 name="content7" searchField="true" title="content7"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content8</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="crmSaleBill_content8"
                   	 name="content8" searchField="true" title="content8"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content9</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent9" name="startcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content91" name="content9##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent9" name="endcontent9" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content92" name="content9##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >content10</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent10" name="startcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content101" name="content10##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent10" name="endcontent10" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content102" name="content10##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content11</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent11" name="startcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content111" name="content11##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent11" name="endcontent11" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content112" name="content11##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >content12</td>
                   	<td align="left"   style="display:none">
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcontent12" name="startcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content121" name="content12##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcontent12" name="endcontent12" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 	style="display:none"  />
						<input type="hidden" id="content122" name="content12##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_crmSaleBill_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_crmSaleBill_tree_page"></div>
</body>
</html>



