﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmSaleChanceDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="crmSaleChance_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmSaleChance_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	</tr>
           	<tr>
           	 	
               	<g:LayOutWinTag buttonId="showsource" title="选择来源"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="chanceSource" hasSetFun="true"
				documentId="crmSaleChance_chanceSource"
				documentName="crmSaleChance_chanceSource_name"
			/>
               	 	<td class="label-title" >来源</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_chanceSource_name" searchField="true"  name="chanceSource.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_chanceSource" name="crmSaleChance.chanceSource.id"  value="" > 
 						<img alt='选择来源' id='showsource' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
           	 	
           	 		               	<g:LayOutWinTag buttonId="showtype" title="选择分类"
					hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
					documentId="crmSaleChance_type"
					documentName="crmSaleChance_type_name"
						/>
			
			
               	 	<td class="label-title" >分类</td>
                   	<td align="left"  >
 						<input type="text" size="20"  searchField="true" name="type.name" readonly="readOnly"  />
 						<input type="hidden" id="crmSaleChance_type" name="crmSaleChance.type.id"   > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
           	 	<!-- <td class="label-title">地区</td>
               	<td align="left">
                  		<input type="text" maxlength="" id="crmSaleChance_area" searchField="true" name="area"  class="input-20-length"></input>
               	</td> -->
               	<g:LayOutWinTag buttonId="showarea" title="选择地区"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="showAreaList" hasSetFun="true"
						documentId="crmSaleChance_area"
						documentName="crmSaleChance_area_name"
					/>
               	 	<td class="label-title" >地区</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_area_name" searchField="true"  name="area.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_area" name="crmSaleChance.area.id"  value="" > 
 						<img alt='选择地区' id='showarea' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
           	 	
           	 	<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="ManagerUserFun" 
 				hasSetFun="true"
				documentId="crmSaleChance_manager"
				documentName="crmSaleChance_manager_name"/>
           	 	<td class="label-title" >客户经理</td>
               	<td align="left"  >
						<input type="text" size="20"   id="crmSaleChance_manager_name" searchField="true"  name="manager.name"  value="" class="text input" />
						<input type="hidden" id="crmSaleChance_manager" name="manager.id"  value="" > 
						<img alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               	</td>
			</tr>
			<tr>
           	 	  	
               <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="crmSaleChance_createUser"
				documentName="crmSaleChance_createUser_name"/>
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleChance_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleChance_createUser" name="crmSaleChance.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>

               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
                   	</td>
			</tr>
			<tr>      	
               	<td class="label-title">评估类型</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_evalustionType" searchField="true" name="evalustionType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">业务类型</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_bussinessType" searchField="true" name="bussinessType"  class="input-20-length"></input>
               	</td>
            </tr>
			<tr>  	
           	 	<td class="label-title">产品类型</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmSaleChance_productType" searchField="true" name="productType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">技术类型</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmSaleChance_technologyType" searchField="true" name="technologyType"  class="input-20-length"></input>
               	</td>
			
            </tr>
			<tr>  	
             	
           	 	<td class="label-title">物种</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmSaleChance_resType" searchField="true" name="resType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">项目评估负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_evalustionUser" searchField="true" name="evalustionUser"  class="input-20-length"></input>
               	</td>
	 	
            </tr>
			<tr>   	
           	 	<td class="label-title" style="display:none">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content1" searchField="true" style="display:none" name="content1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content2" searchField="true" style="display:none" name="content2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content3" searchField="true" style="display:none" name="content3"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content4" searchField="true" style="display:none" name="content4"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content5" searchField="true" style="display:none" name="content5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content6" searchField="true" style="display:none" name="content6"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content7" searchField="true" style="display:none" name="content7"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleChance_content8" searchField="true" style="display:none" name="content8"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_content9" searchField="true" style="display:none" name="content9"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_content10" searchField="true" style="display:none" name="content10"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_content11" searchField="true" style="display:none" name="content11"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleChance_content12" searchField="true" style="display:none" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_crmSaleChance_div"></div>
   		
</body>
</html>



