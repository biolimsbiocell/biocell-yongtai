﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="/operfile/initFileList.action?modelType=crmSaleChance&id=${crmSaleChance.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/crm/sale/crmSaleChanceEdit.js"></script>
	<%--  <script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script> --%>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table width="100%" class="section_table" cellpadding="0">
				<tbody>
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td
													class="section_header_left text standard_section_label labelcolor"
													align="left"><span class="section_label">项目基本信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<table class="frame-table" id="frame-table1">
				<tr>
					<td class="label-title">编码</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left" id="td"><input type="text" size="20"
						maxlength="18" id="crmSaleChance_id" name="crmSaleChance.id"
						title="编码" readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="crmSaleChance.id"/>" /></td>

					<g:LayOutWinTag buttonId="showproject" title="选择项目编号" width="600"
						height="580" hasHtmlFrame="true"
						html="${ctx}/exp/project/project/projectSelect.action"
						isHasSubmit="false" functionName="ProjectFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleChance_project').value=rec.get('id');
						document.getElementById('crmSaleChance_project_name').value=rec.get('name');
						document.getElementById('crmSaleChance_project_projectId').value=rec.get('projectId');" />

					<td class="label-title">模型制备项目编号和项目名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="15"
						id="crmSaleChance_project_projectId"
						value="<s:property value="crmSaleChance.project.projectId"/>"
						readonly="readOnly" class="text input" /> <input type="text"
						size="15" id="crmSaleChance_project_name"
						value="<s:property value="crmSaleChance.project.name"/>"
						readonly="readOnly" class="text input" /> <input type="hidden"
						id="crmSaleChance_project" name="crmSaleChance.project.id"
						value="<s:property value="crmSaleChance.project.id"/>"> <img
						alt='选择项目编号' id='showproject' src='${ctx}/images/img_lookup.gif'
						class='detail' /> <input type="button"
						onclick="lookForProjectMessage()" value="获取项目信息" /></td>

					<g:LayOutWinTag buttonId="showmanager" title="客户经理"
						hasHtmlFrame="true" hasSetFun="true"
						width="document.body.clientWidth/1.5"
						html="${ctx}/core/user/userSelect.action" isHasSubmit="false"
						functionName="managerFun" documentId="crmSaleChance_manager"
						documentName="crmSaleChance_manager_name" />

					<td class="label-title">客户经理</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_manager_name"
						name="crmSaleChance.manager.name"
						value="<s:property value="crmSaleChance.manager.name"/>"
						class="text input" /> <input type="hidden"
						id="crmSaleChance_manager" name="crmSaleChance.manager.id"
						value="<s:property value="crmSaleChance.manager.id"/>"> <img
						alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>

					<%-- <g:LayOutWinTag buttonId="showsource" title="选择项目来源"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectSource" hasSetFun="true"
						documentId="crmSaleChance_chanceSource"
						documentName="crmSaleChance_chanceSource_name" />
						
					<td class="label-title">项目来源</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_chanceSource_name"
						value="<s:property value="crmSaleChance.chanceSource.name"/>" />
						<input type="hidden" id="crmSaleChance_chanceSource"
						name="crmSaleChance.chanceSource.id"
						value="<s:property value="crmSaleChance.chanceSource.id"/>">
						<img alt='选择项目来源' id='showsource'
						src='${ctx}/images/img_lookup.gif' class='detail' />
					</td> --%>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showbussinessType" title="选择项目类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="crmSaleChance" hasSetFun="true"
						documentId="crmSaleChance_bussinessType"
						documentName="crmSaleChance_bussinessType_name" />

					<td class="label-title">项目类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleChance_bussinessType_name"
						value="<s:property value="crmSaleChance.bussinessType.name"/>" />
						<input type="hidden" id="crmSaleChance_bussinessType"
						name="crmSaleChance.bussinessType.id"
						value="<s:property value="crmSaleChance.bussinessType.id"/>">
						<img alt='选择项目类型' id='showbussinessType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>

					<g:LayOutWinTag buttonId="showproductType" title="选择产品类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
						documentId="crmSaleChance_productType"
						documentName="crmSaleChance_productType_name" />

					<td class="label-title">产品类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_productType_name"
						value="<s:property value="crmSaleChance.productType.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="crmSaleChance_productType" name="crmSaleChance.productType.id"
						value="<s:property value="crmSaleChance.productType.id"/>">
						<img alt='选择产品类型' id='showproductType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>

					<%-- 	<g:LayOutWinTag buttonId="showresType" title="选择打靶类别"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="dbType" hasSetFun="true"
						documentId="crmSaleChance_resType"
						documentName="crmSaleChance_resType_name"
					/>
               	 	<td class="label-title" >打靶类别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleChance_resType_name"  value="<s:property value="crmSaleChance.resType.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="crmSaleChance_resType" name="crmSaleChance.resType.id"  value="<s:property value="crmSaleChance.resType.id"/>" > 
 						<img alt='选择打靶类别' id='showresType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>

					<g:LayOutWinTag buttonId="showdbMethodType" title="选择基因打靶类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="dbMethod" hasSetFun="true"
						documentId="crmSaleChance_dbMethod"
						documentName="crmSaleChance_dbMethod_name" />

					<td class="label-title">基因打靶类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_dbMethod_name"
						value="<s:property value="crmSaleChance.dbMethod.name"/>" /> <input
						type="hidden" id="crmSaleChance_dbMethod"
						name="crmSaleChance.dbMethod.id"
						value="<s:property value="crmSaleChance.dbMethod.id"/>"> <img
						alt='选择基因打靶类型' id='showdbMethodType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showtechnologyType" title="选择技术类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="modelPreparation"
						hasSetFun="true" documentId="crmSaleChance_technologyType"
						documentName="crmSaleChance_technologyType_name" />

					<td class="label-title">技术类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_technologyType_name"
						value="<s:property value="crmSaleChance.technologyType.name"/>" />
						<input type="hidden" id="crmSaleChance_technologyType"
						name="crmSaleChance.technologyType.id"
						value="<s:property value="crmSaleChance.technologyType.id"/>">
						<img alt='选择技术类型' id='showtechnologyType'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>

					<g:LayOutWinTag buttonId="showgenus" title="选择种属"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="saleChanceGeners"
						hasSetFun="true" documentId="crmSaleChance_genus"
						documentName="crmSaleChance_genus_name" />

					<td class="label-title">种属</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleChance_genus_name"
						value="<s:property value="crmSaleChance.genus.name"/>" /> <input
						type="hidden" id="crmSaleChance_genus"
						name="crmSaleChance.genus.id"
						value="<s:property value="crmSaleChance.genus.id"/>"> <img
						alt='选择种属' id='showgenus' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>

					<g:LayOutWinTag buttonId="showstrain" title="选择品系"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="saleChanceStrain"
						hasSetFun="true" documentId="crmSaleChance_strain"
						documentName="crmSaleChance_strain_name" />

					<td class="label-title">品系</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleChance_strain_name"
						value="<s:property value="crmSaleChance.strain.name"/>" /> <input
						type="hidden" id="crmSaleChance_strain"
						name="crmSaleChance.strain.id"
						value="<s:property value="crmSaleChance.strain.id"/>"> <img
						alt='选择种属' id='showstrain' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showsource" title="选择项目来源"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectSource" hasSetFun="true"
						documentId="crmSaleChance_chanceSource"
						documentName="crmSaleChance_chanceSource_name" />

					<td class="label-title">项目来源</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_chanceSource_name"
						value="<s:property value="crmSaleChance.chanceSource.name"/>" />
						<input type="hidden" id="crmSaleChance_chanceSource"
						name="crmSaleChance.chanceSource.id"
						value="<s:property value="crmSaleChance.chanceSource.id"/>">
						<img alt='选择项目来源' id='showsource'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>

					<%-- <td class="label-title" >期望结束日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
                   	  	<input type="text" size="20" maxlength="" id="crmSaleChance_endDate"  name="crmSaleChance.endDate" title="期望结束日期"
		                   	   Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="crmSaleChance.endDate" format="yyyy-MM-dd"/>" 
                   	  	/>
                   	</td> --%>

					<td class="label-title">基因ID(NCBI)</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" id="td"><input type="text" size="20"
						maxlength="60" id="crmSaleChance_geneId"
						name="crmSaleChance.geneId" title="基因ID"
						value="<s:property value="crmSaleChance.geneId"/>" /></td>
					<td class="label-title">基因名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" id="td"><input type="text" size="20"
						maxlength="60" id="crmSaleChance_geneName"
						name="crmSaleChance.geneName" title="基因名称"
						value="<s:property value="crmSaleChance.geneName"/>" /></td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showDesignUser" title="选择项目评估负责人"
						hasHtmlFrame="true" hasSetFun="true"
						width="document.body.clientWidth/1.5"
						html="${ctx}/core/user/userSelect.action" isHasSubmit="false"
						functionName="changeUserFun" documentId="crmSaleChance_changeUser"
						documentName="crmSaleChance_changeUser_name" />
					<td class="label-title">项目评估负责人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleChance_changeUser_name"
						value="<s:property value="crmSaleChance.changeUser.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="crmSaleChance_changeUser" name="crmSaleChance.changeUser.id"
						value="<s:property value="crmSaleChance.changeUser.id"/>">
						<img alt='选择项目评估负责人' id='showDesignUser'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="showcreateUserFun"
						hasSetFun="true" extRec="rec" width="900" height="500"
						extStr="document.getElementById('crmSaleChance_createUser').value=rec.get('id');
						document.getElementById('crmSaleChance_createUser_name').value=rec.get('name');" />
					<td class="label-title">创建人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleChance_createUser_name"
						value="<s:property value="crmSaleChance.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="crmSaleChance_createUser"
						name="crmSaleChance.createUser.id"
						value="<s:property value="crmSaleChance.createUser.id"/>">
						<%-- <img alt='选择创建人' id='showcreateUser'
						src='${ctx}/images/img_lookup.gif' class='detail' /> --%></td>
					<td class="label-title">创建日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleChance_createDate" name="crmSaleChance.createDate"
						title="创建日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmSaleChance.createDate" format="yyyy-MM-dd"/>" />
					</td>
					<%-- <td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<select id="crmSaleChance_resType" name="crmSaleChance.resType" class="input-10-length" style="width:152px;">
							<option value=""
								<s:if test="crmSaleChance.resType==''">selected="selected"</s:if>>请选择</option>
							<option value="0"
								<s:if test="crmSaleChance.resType==0">selected="selected"</s:if>>人</option>
							<option value="1"
								<s:if test="crmSaleChance.resType==1">selected="selected"</s:if>>小鼠</option>
							<option value="2"
								<s:if test="crmSaleChance.resType==2">selected="selected"</s:if>>大鼠</option>
							<option value="3"
								<s:if test="crmSaleChance.resType==3">selected="selected"</s:if>>线虫(caenorhabditis elegans)</option>
						</select>
					</td> --%>
				</tr>
				<tr>
					<%-- <g:LayOutWinTag buttonId="selectCrmSaleChange" title="项目评估申请负责人"
							hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
							html="${ctx}/core/user/showUserListAllSelect.action"
							isHasSubmit="false"		functionName="showcreateUserFun" 
							documentId="crmSaleChance_changeUser"
							documentName="crmSaleChance_changeUser"
			 			/> --%>

					<td class="label-title">期望结束日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleChance_endDate" name="crmSaleChance.endDate"
						title="期望结束日期" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleChance.endDate" format="yyyy-MM-dd"/>" />
					</td>

					<td class="label-title">工作流状态</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="30"
						id="crmSaleChance_stateName" class="text input readonlytrue"
						name="crmSaleChance.stateName" title="工作流状态" readonly="readOnly"
						value="<s:property value="crmSaleChance.stateName"/>" /></td>
				</tr>
				<tr>
					<td class="label-title">客户要求</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" colspan="4">
				
					<textarea  id="crmSaleChance_content1" name="crmSaleChance.content1" title="客户要求" style="overflow: hidden; width: 300px; height: 100px;" ><s:property value="crmSaleChance.content1"/></textarea>
					
					<%-- <td class="label-title">终产品</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" id="td"><select readonly="readOnly"
						id="crmContract_product_name" name="crmContract.product.name"
						title="终产品">
							<option value=""
								<s:if test="crmContract_product_name=='F0嵌合鼠'">selected="selected"</s:if>>F0嵌合鼠</option>
							<option value=""
								<s:if test="crmContract_product_name=='F1杂合子'">selected="selected"</s:if>>F1杂合子</option>
							<option value=""
								<s:if test="crmContract_product_name=='sgRNA载体及活性检测'">selected="selected"</s:if>>sgRNA载体及活性检测</option>
							<option value=""
								<s:if test="crmContract_product_name=='sgRNA的活性检测'">selected="selected"</s:if>>sgRNA的活性检测</option>
							<option value=""
								<s:if test="crmContract_product_name=='RNA样品'">selected="selected"</s:if>>RNA样品</option>
							<option value=""
								<s:if test="crmContract_product_name=='打靶载体'">selected="selected"</s:if>>打靶载体</option>
							<option value=""
								<s:if test="crmContract_product_name=='其他'">selected="selected"</s:if>>其他</option>
					</select></td> --%>
				</tr>
				<tr>
					<td class="label-title">附件</td>
					<td title="保存基本后,可以维护查看附件" colspan="2">
						<div id="doclinks_img" style="width: 100px;">
							<span class="attach-btn"></span> <span class="text label detail">共有${requestScope.fileNum}个附件${requestScope.fileName}</span>
						</div>
					</td>
					<td>
						<input type="hidden"  id="crmSaleChance_changeStateName" name="crmSaleChance.changeStateName"
						value="<s:property value="crmSaleChance.changeStateName"/>" />
						<!-- <input type="button" onclick="javascript:createchangeStateName()" value="生成初步设计" /> -->
					</td>
				</tr>
				<tr>
					<td class="label-title" style="display: none">工作流状态ID</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="hidden"
						id="crmSaleChance_state" name="crmSaleChance.state"
						value="<s:property value="crmSaleChance.state"/>" /></td>
				</tr>

				<%-- <tr>
               	 	<td class="label-title" >商机内容</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left" id="textareas"  >
                   	<textarea class="crmSaleChance_content" id="crmSaleChance_content" name="crmSaleChance.content"  title="商机内容" style="overflow: hidden; width: 350px; height: 140px;" ><s:property value="crmSaleChance.content"/></textarea>
                   	</td>
			</tr> --%>

			</table>
			<!-- <table class="frame-table">
				<tr> -->
			<%--
			<g:LayOutWinTag buttonId="showfollowUser" title="选择跟踪人员"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showfollowUserFun" 
				documentId="crmSaleChance_followUser"
				documentName="crmSaleChance_followUser_name"
				/>
               	 	<td class="label-title" >跟踪人员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleChance_followUser_name"  value="<s:property value="crmSaleChance.followUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="crmSaleChance_followUser" name="crmSaleChance.followUser.id"  value="<s:property value="crmSaleChance.followUser.id"/>" > 
 						<img alt='选择跟踪人员' id='showfollowUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title" >跟踪日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="crmSaleChance_followDate"
                   	 name="crmSaleChance.followDate" title="跟踪日期"
                   	       
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	  value="<s:date name="crmSaleChance.followDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			<g:LayOutWinTag buttonId="showfollowResult" title="选择跟踪结果"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showFollowList" hasSetFun="true"
				documentId="crmSaleChance_followResult"
				documentName="crmSaleChance_followResult_name"
			/>
			
			
               	 	<td class="label-title" >跟踪结果</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleChance_followResult_name"  value="<s:property value="crmSaleChance.followResult.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="crmSaleChance_followResult" name="crmSaleChance.followResult.id"  value="<s:property value="crmSaleChance.followResult.id"/>" > 
 						<img alt='选择跟踪结果' id='showfollowResult' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >结果说明</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<textarea  id="crmSaleChance_followExplain" name="crmSaleChance.followExplain"  title="结果说明" style="overflow: hidden; width: 350px; height: 140px;" ><s:property value="crmSaleChance.followExplain"/></textarea>
                   	</td>
			
			
               	 	<td class="label-title" >商机跟踪成单可能性</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="10" maxlength="5" id="crmSaleChance_orderChance"
                   	 name="crmSaleChance.orderChance" title="商机跟踪成单可能性"
                   	   
	value="<s:property value="crmSaleChance.orderChance"/>"
                   	  />
                   	  
                   	</td>
			</tr>
		</table>
			<table width="100%" class="section_table" cellpadding="0" >
		 		<tbody>
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"> 项目评估申请：</span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</tbody> 
		</table>
		<table class="frame-table">	
		--%>
			<%-- <g:LayOutWinTag buttonId="showbussinessType" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
				documentId="crmSaleChance_bussinessType"
				documentName="crmSaleChance_bussinessType_name"
				 />
               	 	<td class="label-title" >分类</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleChance_bussinessType_name"  value="<s:property value="crmSaleChance.bussinessType.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="crmSaleChance_bussinessType" name="crmSaleChance.bussinessType.id"  value="<s:property value="crmSaleChance.bussinessType.id"/>" > 
 						<img alt='选择分类' id='showbussinessType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			<%-- <td class="label-title">项目联系人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
					<input type="text" id="crmSaleChance_linkMan_name" name="crmSaleChance_linkMan_name" value="<s:property value="crmSaleChance.linkMan.name"/>">
						<input type="hidden" id="crmSaleChance_linkMan"
						name="crmSaleChance.linkMan.id"
						value="<s:property value="crmSaleChance.linkMan.id"/>"> <img
						alt='选择项目联系人' id='showProjectStart'
						src='${ctx}/images/img_lookup.gif' class='detail'
						onClick="selectProjectLinkMan()" /></td>
						
					<td class="label-title">联系人单位</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="30" maxlength="20"
						id="crmSaleChance_linkMan_customerId_department_name" name="crmSaleChance.linkMan.customerId.department.name" title="联系人单位" readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="crmSaleChance.linkMan.customerId.department.name"/>" />
						</td>	
						<td class="label-title">固定电话</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="20" readonly="readOnly" class="text input readonlytrue"
						id="crmSaleChance_linkMan_telephone" name="crmSaleChance.linkMan.telephone" title="电话"
						value="<s:property value="crmSaleChance.linkMan.telephone"/>" />
				</tr> --%>
			<!-- 				<tr>		 -->
			<%-- <g:LayOutWinTag buttonId="showresType" title="选择打靶类别"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="dbType" hasSetFun="true"
				documentId="crmSaleChance_resType"
				documentName="crmSaleChance_resType_name"
				 />
               	 	<td class="label-title" >打靶类别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleChance_resType_name"  value="<s:property value="crmSaleChance.resType.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="crmSaleChance_resType" name="crmSaleChance.resType.id"  value="<s:property value="crmSaleChance.resType.id"/>" > 
 						<img alt='选择打靶类别' id='showresType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>

			<%-- <td class="label-title" >评估成单可能性</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="5" id="crmSaleChance_successChance"
                   	 name="crmSaleChance.successChance" title="评估成单可能性" value="<s:property value="crmSaleChance.successChance"/>"
                   	  />
                   	</td> --%>
			<%-- <g:LayOutWinTag buttonId="showLinkMan" title="选择项目联系人"
						hasHtmlFrame="true" hasSetFun="true"
						width="document.body.clientWidth/1.5"
						html="${ctx}/core/user/userSelect.action" isHasSubmit="false"
						functionName="showfollowUserFun"
						documentId="crmSaleChance_linkMan"
						documentName="crmSaleChance_linkMan_name" /> --%>

			<%-- <g:LayOutWinTag buttonId="showarea" title="选择地区"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="areaType" hasSetFun="true"
						documentId="crmSaleChance_area"
						documentName="crmSaleChance_area_name" /> --%>


			<%-- <td class="label-title">地区</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleChance_area_name"
						value="<s:property value="crmSaleChance.area.name"/>"
						readonly="readOnly" /> <input type="hidden"
						id="crmSaleChance_area" name="crmSaleChance.area.id"
						value="<s:property value="crmSaleChance.area.id"/>"> <img
						alt='选择地区' id='showarea' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td> --%>
			<%-- <td class="label-title">国家</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="20" readonly="readOnly" class="text input readonlytrue"
						id="crmSaleChance_linkMan_area" name="crmSaleChance.linkMan.area" title="国家"
						value="<s:property value="crmSaleChance.linkMan.area"/>" />
						<td class="label-title">省份</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="20" readonly="readOnly" class="text input readonlytrue"
						id="crmSaleChance_linkMan_selA" name="crmSaleChance.linkMan.selA" title="省份"
						value="<s:property value="crmSaleChance.linkMan.selA"/>" />
						<td class="label-title">市</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="20" readonly="readOnly" class="text input readonlytrue"
						id="crmSaleChance_linkMan_selB" name="crmSaleChance.linkMan.selB" title="省份"
						value="<s:property value="crmSaleChance.linkMan.selB"/>" />
						<input type="hidden" id="crmSaleChance_selB" name="crmSaleChance.selB"  value="<s:property value="crmSaleChance.linkMan.selB"/>" >
						</td>
				</tr>
				<tr>
				<td class="label-title">手机</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="20" readonly="readOnly" class="text input readonlytrue"
						id="crmSaleChance_linkMan_mobile" name="crmSaleChance.linkMan.mobile" title="联系人电话"
						value="<s:property value="crmSaleChance.linkMan.mobile"/>" />
						<input type="hidden" id="crmSaleChance_phone" name="crmSaleChance.phone"  value="<s:property value="crmSaleChance.linkMan.mobile"/>" >
						</td>
				<td class="label-title">Email</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
					<input type="text" size="20" maxlength="20" readonly="readOnly" class="text input readonlytrue"
						id="crmSaleChance_linkMan_email" name="crmSaleChance.linkMan.email" title="联系人邮箱"
						value="<s:property value="crmSaleChance.linkMan.email"/>" />
					<input type="hidden" size="40" maxlength="30"
						id="crmSaleChance_email" name="crmSaleChance.email" title="邮件"
						value="<s:property value="crmSaleChance.linkMan.email"/>" />
						</td> --%>
			<!-- 				</tr> -->
			<tr>
				<td class="requiredcolumn" nowrap width="10px"><img
					class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
			</tr>
			<%-- <tr>
               	 	<td class="label-title" >预计评估完成时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="crmSaleChance_preFinishDate"
                   	 name="crmSaleChance.preFinishDate" title="预计评估完成时间"
                   	       
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	  value="<s:date name="crmSaleChance.preFinishDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	</td>
				</tr> --%>
			<table>
				<tr>
					<td class="label-title" style="display: none">content2</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleChance_content2"
						name="crmSaleChance.content2" title="content2"
						value="<s:property value="crmSaleChance.content2"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content3</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleChance_content3"
						name="crmSaleChance.content3" title="content3"
						value="<s:property value="crmSaleChance.content3"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content4</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleChance_content4"
						name="crmSaleChance.content4" title="content4"
						value="<s:property value="crmSaleChance.content4"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content5</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleChance_content5"
						name="crmSaleChance.content5" title="content5"
						value="<s:property value="crmSaleChance.content5"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content6</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleChance_content6"
						name="crmSaleChance.content6" title="content6"
						value="<s:property value="crmSaleChance.content6"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content7</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleChance_content7"
						name="crmSaleChance.content7" title="content7"
						value="<s:property value="crmSaleChance.content7"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content8</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleChance_content8"
						name="crmSaleChance.content8" title="content8"
						value="<s:property value="crmSaleChance.content8"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content9</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmSaleChance_content9"
						name="crmSaleChance.content9" title="content9" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleChance.content9" format="yyyy-MM-dd"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content10</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmSaleChance_content10"
						name="crmSaleChance.content10" title="content10" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleChance.content10" format="yyyy-MM-dd"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content11</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmSaleChance_content11"
						name="crmSaleChance.content11" title="content11" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleChance.content11" format="yyyy-MM-dd"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content12</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmSaleChance_content12"
						name="crmSaleChance.content12" title="content12" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleChance.content12" format="yyyy-MM-dd"/>"
						style="display: none" /></td>
				</tr>
			</table>
			<input type="hidden" name="crmSaleChanceLinkmanJson"
				id="crmSaleChanceLinkmanJson" value="" /> <input type="hidden"
				id="id_parent_hidden" value="<s:property value="crmSaleChance.id"/>" />
		</form>
		<div id="crmSaleChanceLinkmanpage" width="100%" height:10px></div>
		<!--  <div id="tabs">
            <ul>
           	</ul> 
			</div> -->
	</div>
</body>
</html>
