﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmSaleBillDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="crmSaleBill_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmSaleBill_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">所属合同</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_crmContract" searchField="true" name="crmContract"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">审核人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_confirmUser" searchField="true" name="confirmUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">审核日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_confirmDate" searchField="true" name="confirmDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">工作流状态ID</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="crmSaleBill_state" searchField="true" style="display:none" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="crmSaleBill_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content1" searchField="true" style="display:none" name="content1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content2" searchField="true" style="display:none" name="content2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content3" searchField="true" style="display:none" name="content3"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content4" searchField="true" style="display:none" name="content4"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content5" searchField="true" style="display:none" name="content5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content6" searchField="true" style="display:none" name="content6"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content7" searchField="true" style="display:none" name="content7"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleBill_content8" searchField="true" style="display:none" name="content8"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_content9" searchField="true" style="display:none" name="content9"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_content10" searchField="true" style="display:none" name="content10"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_content11" searchField="true" style="display:none" name="content11"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleBill_content12" searchField="true" style="display:none" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_crmSaleBill_div"></div>
   		
</body>
</html>



