<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=crmBusinessManage&id=${crmBusinessManage.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmBusinessManageEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
           	 	<td class="label-title" >编码</td>
           	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	<td align="left"  >
	               	<input type="text" size="20" maxlength="18" id="crmBusinessManage_id"
	               	 name="crmBusinessManage.id" title="编码"
	               	 readonly = "readOnly" class="text input readonlytrue"  value="<s:property value="crmBusinessManage.id"/>"
	               	  />
               	</td>
			
           	 	<td class="label-title" >商机来源</td>
           	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
               	<td align="left"  >
	               	<input type="text" size="50" maxlength="60" id="crmBusinessManage_name"
	               	 name="crmBusinessManage.name" title="商机来源"  value="<s:property value="crmBusinessManage.name"/>"
	               	/>
               	</td>
			
				<g:LayOutWinTag buttonId="showtype" title="选择客户经理"
					hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
					documentId="crmBusinessManage_type"
					documentName="crmBusinessManage_type_name"
				/>
		
           	 	<td class="label-title" >客户经理</td>
           	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
               	<td align="left"  >
					<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_type_name"  value="<s:property value="crmBusinessManage.type.name"/>" readonly="readOnly"  />
					<input type="hidden" id="crmBusinessManage_type" name="crmBusinessManage.type.id"  value="<s:property value="crmBusinessManage.type.id"/>" > 
					<img alt='选择客户经理' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               	</td>
			</tr>
			
			<tr>
				<g:LayOutWinTag buttonId="showstate" title="选择状态"
					hasHtmlFrame="true"
					html="${ctx}/dic/state/stateSelect.action?type=commonState"
					isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
					documentId="crmBusinessManage_state"
					documentName="crmBusinessManage_state_name" />
				
           	 	<td class="label-title" >状态</td>
           	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
               	<td align="left"  >
					<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_state_name"  value="<s:property value="crmBusinessManage.state.name"/>" readonly="readOnly"  />
					<input type="hidden" id="crmBusinessManage_state" name="crmBusinessManage.state.id"  value="<s:property value="crmBusinessManage.state.id"/>" > 
					<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               	</td>
			
				<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
					hasHtmlFrame="true"
					html="${ctx}/crm/sale/userSelect.action"
					isHasSubmit="false" functionName="UserFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('crmBusinessManage_createUser').value=rec.get('id');
					document.getElementById('crmBusinessManage_createUser_name').value=rec.get('name');" />
				
           	 	<td class="label-title" >创建人</td>
           	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
               	<td align="left"  >
					<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_createUser_name"  value="<s:property value="crmBusinessManage.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
					<input type="hidden" id="crmBusinessManage_createUser" name="crmBusinessManage.createUser.id"  value="<s:property value="crmBusinessManage.createUser.id"/>" > 
					<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                </td>
			
			
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="" id="crmBusinessManage_createDate"
                   	 name="crmBusinessManage.createDate" title="创建日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="crmBusinessManage.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showpostType" title="选择岗位类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showpostTypeFun" hasSetFun="true"
				documentId="crmBusinessManage_postType"
				documentName="crmBusinessManage_postType_name"
			/>
               	 	<td class="label-title" >岗位类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_postType_name"  value="<s:property value="crmBusinessManage.postType.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="crmBusinessManage_postType" name="crmBusinessManage.postType.id"  value="<s:property value="crmBusinessManage.postType.id"/>" > 
 						<img alt='选择岗位类型' id='showpostType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showindustryType" title="选择行业类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showindustryTypeFun" hasSetFun="true"
				documentId="crmBusinessManage_industryType"
				documentName="crmBusinessManage_industryType_name"
			/>
               	 	<td class="label-title" >行业类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_industryType_name"  value="<s:property value="crmBusinessManage.industryType.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="crmBusinessManage_industryType" name="crmBusinessManage.industryType.id"  value="<s:property value="crmBusinessManage.industryType.id"/>" > 
 						<img alt='选择行业类型' id='showindustryType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
           	 	<td class="label-title" >联系人</td>
           	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
               	<td align="left"  >
					<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_linkmanNname_name"  value="<s:property value="crmBusinessManage.linkmanNname.name"/>"  readonly="readOnly"  />
					<input type="hidden" id="crmBusinessManage_linkmanNname" name="crmBusinessManage.linkmanNname.id"  value="<s:property value="crmBusinessManage.linkmanNname.id"/>" > 
					<img title='选择联系人姓名' src='${ctx}/images/img_lookup.gif' onClick="selectProjectLinkMan()"	class='detail'    />                   		
               		<input type="button" title='新建联系人' onclick="addLinkMan()" value="新建" class='detail' />
               	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >国家</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="crmBusinessManage_country"
                   	 name="crmBusinessManage.country" title="国家" value="<s:property value="crmBusinessManage.country"/>"
                   	  />
                   	</td>
			
               	 	<td class="label-title" >省</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="crmBusinessManage_privince"
                   	 name="crmBusinessManage.privince" title="省"
				 	 value="<s:property value="crmBusinessManage.privince"/>"/>
                   	</td>
                   	<td class="label-title" >市</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="15" id="crmBusinessManage_city"
                   	 name="crmBusinessManage.city" title="市"
					 value="<s:property value="crmBusinessManage.city"/>"/>
                   	</td>
			</tr>
			<tr>
               	 	<%-- <td class="label-title" >联系人手机</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="10" id="crmBusinessManage_linkmanMobile"
                   	 name="crmBusinessManage.linkmanMobile" title="联系人手机" value="<s:property value="crmBusinessManage.linkmanMobile"/>"
                   	  />
                   	</td> --%>
			
               	 	<%-- <td class="label-title" >固定电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="10" id="crmBusinessManage_linkmanPhone"
                   	 name="crmBusinessManage.linkmanPhone" title="固定电话" value="<s:property value="crmBusinessManage.linkmanPhone"/>"
                   	  />
                   	</td> --%>
				<g:LayOutWinTag buttonId="showbelongConsignor" title="选择所属委托人"
				hasHtmlFrame="true"
				html="${ctx}/crm/sale/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmBusinessManage_belongConsignor').value=rec.get('id');
				document.getElementById('crmBusinessManage_belongConsignor_name').value=rec.get('name');" />
			
               	 	<td class="label-title" >所属委托人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_belongConsignor_name"  value="<s:property value="crmBusinessManage.belongConsignor.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="crmBusinessManage_belongConsignor" name="crmBusinessManage.belongConsignor.id"  value="<s:property value="crmBusinessManage.belongConsignor.id"/>" > 
<%--  						<img alt='选择所属委托人' id='showbelongConsignor' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
				<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="100" id="crmBusinessManage_note"
                   	 name="crmBusinessManage.note" title="备注" value="<s:property value="crmBusinessManage.note"/>"
                   	  />
                   	</td>
				<td class="label-title" >email</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="crmBusinessManage_email"
                   	 name="crmBusinessManage.email" title="email" value="<s:property value="crmBusinessManage.email"/>"
                   	  />
                   	</td>
               	 	<td class="label-title"  style="display:none"  >相关主表</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
 						<input type="text" size="20" readonly="readOnly"  id="crmBusinessManage_businessManage_name"  value="<s:property value="crmBusinessManage.businessManage.name"/>" class="text input readonlytrue" readonly="readOnly" 	style="display:none"  />
 						<input type="hidden" id="crmBusinessManage_businessManage" name="crmBusinessManage.businessManage.id"  value="<s:property value="crmBusinessManage.businessManage.id"/>" > 
 						<img alt='选择相关主表' id='showbusinessManage' src='${ctx}/images/img_lookup.gif' 	class='detail'   style="display:none"    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<%-- <td class="label-title" >单位名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="crmBusinessManage_unitName"
                   	 name="crmBusinessManage.unitName" title="单位名称" value="<s:property value="crmBusinessManage.unitName"/>"
                   	  />
                   	</td> --%>
			
               	 	<%-- <td class="label-title" >单位地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="crmBusinessManage_unitAddress"
                   	 name="crmBusinessManage.unitAddress" title="单位地址" value="<s:property value="crmBusinessManage.unitAddress"/>"
                   	  />
                   	</td> --%>
			</tr>
			<tr>
               	 	<%-- <td class="label-title" >研究方向</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="crmBusinessManage_resDirection"
                   	 name="crmBusinessManage.resDirection" title="研究方向" value="<s:property value="crmBusinessManage.resDirection"/>"
                   	  />
                   	</td> --%>
			
			
               	 	<%-- <td class="label-title" >部门/学科</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="crmBusinessManage_dept"
                   	 name="crmBusinessManage.dept" title="部门/学科" value="<s:property value="crmBusinessManage.dept"/>"
                   	  />
                   	</td> --%>
			</tr>
			
			<tr>
               	 	<td class="label-title"  style="display:none"  >content1</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="crmBusinessManage_content1"
                   	 name="crmBusinessManage.content1" title="content1"
                   	   
	value="<s:property value="crmBusinessManage.content1"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content2</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="crmBusinessManage_content2"
                   	 name="crmBusinessManage.content2" title="content2"
                   	   
	value="<s:property value="crmBusinessManage.content2"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content3</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="crmBusinessManage_content3"
                   	 name="crmBusinessManage.content3" title="content3"
                   	   
	value="<s:property value="crmBusinessManage.content3"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content4</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="crmBusinessManage_content4"
                   	 name="crmBusinessManage.content4" title="content4"
                   	   
	value="<s:property value="crmBusinessManage.content4"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content5</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="15" maxlength="" id="crmBusinessManage_content5"
                   	 name="crmBusinessManage.content5" title="content5"
                   	   
                   	  value="<s:date name="crmBusinessManage.content5" format="yyyy-MM-dd"/>"
                   	                      	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content6</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="15" maxlength="" id="crmBusinessManage_content6"
                   	 name="crmBusinessManage.content6" title="content6"
                   	   
                   	  value="<s:date name="crmBusinessManage.content6" format="yyyy-MM-dd"/>"
                   	                      	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content7</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="15" maxlength="" id="crmBusinessManage_content7"
                   	 name="crmBusinessManage.content7" title="content7"
                   	   
                   	  value="<s:date name="crmBusinessManage.content7" format="yyyy-MM-dd"/>"
                   	                      	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content8</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="15" maxlength="" id="crmBusinessManage_content8"
                   	 name="crmBusinessManage.content8" title="content8"
                   	   
                   	  value="<s:date name="crmBusinessManage.content8" format="yyyy-MM-dd"/>"
                   	                      	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content9</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="10" id="crmBusinessManage_content9"
                   	 name="crmBusinessManage.content9" title="content9"
                   	   
	value="<s:property value="crmBusinessManage.content9"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content10</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="10" id="crmBusinessManage_content10"
                   	 name="crmBusinessManage.content10" title="content10"
                   	   
	value="<s:property value="crmBusinessManage.content10"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >content11</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="10" id="crmBusinessManage_content11"
                   	 name="crmBusinessManage.content11" title="content11"
                   	   
	value="<s:property value="crmBusinessManage.content11"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >content12</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="10" id="crmBusinessManage_content12"
                   	 name="crmBusinessManage.content12" title="content12"
                   	   
	value="<s:property value="crmBusinessManage.content12"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="crmBusinessManageItemJson" id="crmBusinessManageItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="crmBusinessManage.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#crmBusinessManageItempage">商机管理明细</a></li>
           	</ul> 
			<div id="crmBusinessManageItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
