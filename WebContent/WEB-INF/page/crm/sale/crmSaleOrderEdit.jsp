﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="/operfile/initFileList.action?modelType=crmSaleOrder&id=${crmSaleOrder.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/crm/sale/crmSaleOrderEdit.js"></script>
	<%--  <script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script> --%>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
		<table width="100%" class="section_table" cellpadding="0" >
		 		<tbody>
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">订单基本信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody> 
			</table>			
			<table class="frame-table">
				<tr>
					<td class="label-title">编码</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="18"
						id="crmSaleOrder_id" name="crmSaleOrder.id" title="编码"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="crmSaleOrder.id"/>" />
					</td>
					
					<td class="label-title">订单名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="50" maxlength="60"
						id="crmSaleOrder_name" name="crmSaleOrder.name" title="${crmSaleOrder.name}"
						value="<s:property value="crmSaleOrder.name"/>" /></td>
						
					<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="showmanagerFun" hasSetFun="true"
						width="900" height="500"
						documentId="crmSaleOrder_manager"
						documentName="crmSaleOrder_manager_name"
					/>
					
					<td class="label-title">客户经理</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_manager_name"
						value="<s:property value="crmSaleOrder.manager.name"/>" /> <input
						type="hidden" id="crmSaleOrder_manager"
						name="crmSaleOrder.manager.id"
						value="<s:property value="crmSaleOrder.manager.id"/>"> <img
						alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
						
					<%-- <g:LayOutWinTag buttonId="showcrmMarcketSale" title="选择市场推广活动"
						hasHtmlFrame="true" width="600" height="580"
						html="${ctx}/crm/marcket/crmMarcketActivity/crmMarcketActivitySelect.action"
						isHasSubmit="false" functionName="CrmMarcketActivityFun"
						hasSetFun="true" extRec="rec"
						extStr="document.getElementById('crmSaleOrder_crmMarcketActivity').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmMarcketActivity_name').value=rec.get('name');" /> --%>

					<%-- <td class="label-title" >市场推广活动</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmMarcketActivity_name"  value="<s:property value="crmSaleOrder.crmMarcketActivity.name"/>" />
 						<input type="hidden" id="crmSaleOrder_crmMarcketActivity" name="crmSaleOrder.crmMarcketActivity.id"  value="<s:property value="crmSaleOrder.crmMarcketActivity.id"/>" > 
 						<img alt='选择市场推广活动' id='showcrmMarcketSale' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showorderSource" title="选择订单来源"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectSource" hasSetFun="true"
						documentId="crmSaleOrder_orderSource"
						documentName="crmSaleOrder_orderSource_name" />
					<td class="label-title">订单来源</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_orderSource_name"
						value="<s:property value="crmSaleOrder.orderSource.name"/>" /> <input
						type="hidden" id="crmSaleOrder_orderSource"
						name="crmSaleOrder.orderSource.id"
						value="<s:property value="crmSaleOrder.orderSource.id"/>">
						<img alt='选择来源' id='showorderSource'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
						
					<g:LayOutWinTag buttonId="showparent" title="选择父级订单"
						hasHtmlFrame="true" width="600" height="580"
						html="${ctx}/crm/sale/crmSaleOrder/crmSaleOrderSelect.action"
						isHasSubmit="false" functionName="CrmSaleOrderFun"
						hasSetFun="true" extRec="rec"
						extStr="document.getElementById('crmSaleOrder_parent').value=rec.get('id');
						document.getElementById('crmSaleOrder_parent_name').value=rec.get('name');" />

					<td class="label-title">父级订单</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_parent_name"
						value="<s:property value="crmSaleOrder.parent.name"/>" /> <input
						type="hidden" id="crmSaleOrder_parent"
						name="crmSaleOrder.parent.id"
						value="<s:property value="crmSaleOrder.parent.id"/>"> <img
						alt='选择父级订单' id='showparent' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
						
						<td class="label-title">下单日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleOrder_genProjectDate"
						name="crmSaleOrder.genProjectDate" title="下单日期"
						readonly="readonly" class="text input readonlytrue"
						value="<s:date name="crmSaleOrder.genProjectDate" format="yyyy-MM-dd"/>" />
					</td>
			<%-- 			
			<g:LayOutWinTag buttonId="showcrmCustomer" title="选择客户"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="CrmCustomerFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmSaleOrder_crmCustomer').value=rec.get('id');
				document.getElementById('crmSaleOrder_crmCustomer_name').value=rec.get('name');" /> --%>

				<%-- <td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="10" id="crmSaleOrder_money"
	                   	 	   name="crmSaleOrder.money" title="金额" value="<s:property value="crmSaleOrder.money"/>"
	                   	/>
                   	</td> --%>
				</tr>
			
					<%-- <td class="label-title">委托人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_crmCustomer_name"
						value="<s:property value="crmSaleOrder.crmCustomer.name"/>" /> <input
						type="hidden" id="crmSaleOrder_crmCustomer"
						name="crmSaleOrder.crmCustomer.id"
						value="<s:property value="crmSaleOrder.crmCustomer.id"/>">
						<img title='选择委托人' id='showcrmCustomer'
						src='${ctx}/images/img_lookup.gif' class='detail'
						onClick="CrmCustomerFun()" /></td>
						
						<g:LayOutWinTag buttonId="showType" title="选择项目类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="workType" hasSetFun="true"
						documentId="crmSaleOrder_workType"
						documentName="crmSaleOrder_workType_name" /> --%>
					
					<%-- <td class="label-title">委托单位</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="30" title="${crmSaleOrder.client}" id="crmSaleOrder_client"
							   name="crmSaleOrder.client" 
							   value="<s:property value="crmSaleOrder.client"/>"/>
						</td> --%>
				
					<%-- <g:LayOutWinTag buttonId="showcrmSaleChance" title="选择商机"
						hasHtmlFrame="true"
						html="${ctx}/crm/sale/crmSaleChance/crmSaleChanceSelect.action"
						isHasSubmit="false" functionName="CrmSaleChanceFun"
						hasSetFun="true" extRec="rec"
						extStr="document.getElementById('crmSaleOrder_crmSaleChance').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmSaleChance_name').value=rec.get('name');" /> --%>

					<%-- <td class="label-title" >商机</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleOrder_crmSaleChance_name"  value="<s:property value="crmSaleOrder.crmSaleChance.name"/>" />
 						<input type="hidden" id="crmSaleOrder_crmSaleChance" name="crmSaleOrder.crmSaleChance.id"  value="<s:property value="crmSaleOrder.crmSaleChance.id"/>" > 
 						<img alt='选择商机' id='showcrmSaleChance' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="CrmSaleChanceFun()" />                   		
                   	</td> --%>
					
					<%-- <g:LayOutWinTag buttonId="showtype" title="选择产品类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
						documentId="crmSaleOrder_type"
						documentName="crmSaleOrder_type_name" />
					<td class="label-title">产品类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleOrder_type_name"
						value="<s:property value="crmSaleOrder.type.name"/>" /> <input
						type="hidden" id="crmSaleOrder_type" name="crmSaleOrder.type.id"
						value="<s:property value="crmSaleOrder.type.id"/>"> <img
						alt='选择产品类型' id='showtype' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<g:LayOutWinTag buttonId="showgenus" title="选择种属"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="genera" hasSetFun="true"
						documentId="crmSaleOrder_genus"
						documentName="crmSaleOrder_genus_name" />
					<td class="label-title">种属</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleOrder_genus_name"
						value="<s:property value="crmSaleOrder.genus.name"/>" /> <input
						type="hidden" id="crmSaleOrder_genus" name="crmSaleOrder.genus.id"
						value="<s:property value="crmSaleOrder.genus.id"/>"> <img
						alt='选择种属' id='showgenus' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<g:LayOutWinTag buttonId="showstrain" title="选择品系"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="saleChanceStrain" hasSetFun="true"
						documentId="crmSaleOrder_strain"
						documentName="crmSaleOrder_strain_name" />
					<td class="label-title">品系</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="20" id="crmSaleOrder_strain_name"
								value="<s:property value="crmSaleOrder.strain.name"/>" />
						<input type="hidden" id="crmSaleOrder_strain" name="crmSaleOrder.strain.id"
								value="<s:property value="crmSaleOrder.strain.id"/>"/> 
						<img alt='选择品系' id='showstrain' src='${ctx}/images/img_lookup.gif' class='detail' /></td>
				</tr>
				<tr>
						<g:LayOutWinTag buttonId="showprojectStart" title="选择技术类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="modelPreparation"
						hasSetFun="true" documentId="crmSaleOrder_technologyType"
						documentName="crmSaleOrder_technologyType_name" />
					<td class="label-title">技术类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleOrder_technologyType_name"
						value="<s:property value="crmSaleOrder.technologyType.name"/>" />
						<input type="hidden" id="crmSaleOrder_technologyType"
						name="crmSaleOrder.technologyType.id"
						value="<s:property value="crmSaleOrder.technologyType.id"/>">
						<img alt='选择技术类型' id='showprojectStart'
						src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
				<g:LayOutWinTag buttonId="showGeneTargetingType" title="选择基因打靶类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="dbMethod" hasSetFun="true"
						documentId="crmSaleOrder_geneTargetingType"
						documentName="crmSaleOrder_geneTargetingType_name" />
					<td class="label-title">基因打靶类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="50"
						id="crmSaleOrder_geneTargetingType_name"
						value="<s:property value="crmSaleOrder.geneTargetingType.name"/>" />
						<input type="hidden" id="crmSaleOrder_geneTargetingType"
						name="crmSaleOrder.geneTargetingType.id"
						value="<s:property value="crmSaleOrder.geneTargetingType.id"/>">
						<img alt='选择基因打靶类型' id='showGeneTargetingType'
						src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
					<td class="label-title">是否去Neo</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<select id="crmSaleOrder_neoResult" name="crmSaleOrder.neoResult" class="input-10-length"
							style="width: 152px;">
							<option value="0"
								<s:if test="crmSaleOrder.neoResult==0">selected="selected"</s:if>>否</option>
							<option value="1"
								<s:if test="crmSaleOrder.neoResult==1">selected="selected"</s:if>>是</option>
						</select>
				 	</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showProduct" title="选择终产品"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="finalType" hasSetFun="true"
						documentId="crmSaleOrder_product"
						documentName="crmSaleOrder_product_name" />
					<td class="label-title">终产品</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_product_name"
						value="<s:property value="crmSaleOrder.product.name"/>" /> <input
						type="hidden" id="crmSaleOrder_product"
						name="crmSaleOrder.product.id"
						value="<s:property value="crmSaleOrder.product.id"/>"> <img
						alt='选择终产品' id='showProduct' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<td class="label-title">终产品数量</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_productNo" name="crmSaleOrder.productNo"
						title="终产品数量" value="<s:property value="crmSaleOrder.productNo"/>" />
					</td>
					<g:LayOutWinTag buttonId="showCheckUp" title="选择质控要求"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="checkUp" hasSetFun="true"
						documentId="crmSaleOrder_checkup"
						documentName="crmSaleOrder_checkup_name" />
					<td class="label-title">质控要求</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_checkup_name"
						value="<s:property value="crmSaleOrder.checkup.name"/>" /> <input
						type="hidden" id="crmSaleOrder_checkup"
						name="crmSaleOrder.checkup.id"
						value="<s:property value="crmSaleOrder.checkup.id"/>"> <img
						alt='选择终产品' id='showCheckUp' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
				</tr>
				<tr>
					<td class="label-title">基因ID</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_geneId" name="crmSaleOrder.geneId" readonly="readonly" class="text input readonlytrue"
						title="基因ID" value="<s:property value="crmSaleOrder.geneId"/>" />
					</td>
					<td class="label-title">基因正式名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_geneName" name="crmSaleOrder.geneName" readonly="readonly" class="text input readonlytrue"
						title="基因名称" value="<s:property value="crmSaleOrder.geneName"/>" />
					</td>
					<td class="label-title">基因别名</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_geneByName" name="crmSaleOrder.geneByName" readonly="readonly" class="text input readonlytrue"
						title="基因别名" value="<s:property value="crmSaleOrder.geneByName"/>" />
					</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showBoss" title="选择项目负责人"
						class='detail' /></td> --%>
					
					<%-- <g:LayOutWinTag buttonId="showBoss" title="选择项目负责人"
						hasHtmlFrame="true" hasSetFun="true"
						width="document.body.clientWidth/1.5"
						html="${ctx}/core/user/userSelect.action" isHasSubmit="false"
						functionName="showconfirmUser" documentId="crmSaleOrder_boss"
						documentName="crmSaleOrder_boss_name" /> --%>
					<%-- <td class="label-title" >项目负责人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleOrder_boss_name"  value="<s:property value="crmSaleOrder.boss.name"/>" />
 						<input type="hidden" id="crmSaleOrder_boss" name="crmSaleOrder.boss.id"  value="<s:property value="crmSaleOrder.boss.id"/>" > 
 						<img alt='选择项目负责人' id='showBoss' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
					<%-- <td class="label-title" >Southern blot鉴定</td>
                   	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left">
						<select id="crmSaleOrder_checkup" name="crmSaleOrder.checkup" class="input-10-length" style="width:152px;">
								<option value="0" <s:if test="crmSaleOrder.checkup==0">selected="selected"</s:if>>不需要</option>
								<option value="1" <s:if test="crmSaleOrder.checkup==1">selected="selected"</s:if>>需要</option>
						</select>
                   	</td> --%>

                   	<%-- <td class="label-title">实际合同开始日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text"
						id="crmSaleOrder_startDate" name="crmSaleOrder.startDate"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleOrder.startDate" format="yyyy-MM-dd"/>">
					</td>
					<td class="label-title">实际合同结束日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text"
						id="crmSaleOrder_endDate" name="crmSaleOrder.endDate"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleOrder.endDate" format="yyyy-MM-dd"/>">
					</td> --%>
					<%-- <g:LayOutWinTag buttonId="showorderSource" title="选择来源"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="projectSource" hasSetFun="true"
						documentId="crmSaleOrder_orderSource"
						documentName="crmSaleOrder_orderSource_name" />
					<td class="label-title">订单来源</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_orderSource_name"
						value="<s:property value="crmSaleOrder.orderSource.name"/>" /> <input
						type="hidden" id="crmSaleOrder_orderSource"
						name="crmSaleOrder.orderSource.id"
						value="<s:property value="crmSaleOrder.orderSource.id"/>">
						<img alt='选择来源' id='showorderSource'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td> --%>
					<%-- <g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
							hasHtmlFrame="true"
							html="${ctx}/crm/sale/crmSaleOrder/userSelect.action"
							isHasSubmit="false" functionName="UserFun" 
			 				hasSetFun="true"
							extRec="rec"
							extStr="document.getElementById('crmSaleOrder_confirmUser').value=rec.get('id');
							document.getElementById('crmSaleOrder_confirmUser_name').value=rec.get('name');" /> --%>
				
				<tr>
					<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true"
						html="${ctx}/crm/sale/crmSaleOrder/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_createUser').value=rec.get('id');
						document.getElementById('crmSaleOrder_createUser_name').value=rec.get('name');" />

					<td class="label-title">创建人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_createUser_name"
						value="<s:property value="crmSaleOrder.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="crmSaleOrder_createUser"
						name="crmSaleOrder.createUser.id"
						value="<s:property value="crmSaleOrder.createUser.id"/>">
						
					<td class="label-title">创建日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleOrder_createDate" name="crmSaleOrder.createDate"
						title="创建日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmSaleOrder.createDate" format="yyyy-MM-dd"/>" />
					</td>
					
					<g:LayOutWinTag buttonId="showConfirmUser" title="选择审核人"
						hasHtmlFrame="true"
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="ConfirmUserFun" 
		 				hasSetFun="true" width="900" height="500"
						documentId="crmSaleOrder_confirmUser"
						documentName="crmSaleOrder_confirmUser_name"/>
					
					<td class="label-title">审核人</td>
					<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="20" readonly="readOnly" id="crmSaleOrder_confirmUser_name" value="<s:property value="crmSaleOrder.confirmUser.name"/>" class="text input"/> 
						<input type="hidden" id="crmSaleOrder_confirmUser" name="crmSaleOrder.confirmUser.id" value="<s:property value="crmSaleOrder.confirmUser.id"/>">
						<img alt='选择创建人' id='showConfirmUser' src='${ctx}/images/img_lookup.gif' class='detail'/>                   		
					</td>
 					
					<g:LayOutWinTag buttonId="showparent" title="选择父级订单"
						hasHtmlFrame="true" width="600" height="580"
						html="${ctx}/crm/sale/crmSaleOrder/crmSaleOrderSelect.action"
						isHasSubmit="false" functionName="CrmSaleOrderFun"
						hasSetFun="true" extRec="rec"
						extStr="document.getElementById('crmSaleOrder_parent').value=rec.get('id');
						document.getElementById('crmSaleOrder_parent_name').value=rec.get('name');" />

					<%-- <td class="label-title">父级订单</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_parent_name"
						value="<s:property value="crmSaleOrder.parent.name"/>" /> <input
						type="hidden" id="crmSaleOrder_parent"
						name="crmSaleOrder.parent.id"
						value="<s:property value="crmSaleOrder.parent.id"/>"> <img
						alt='选择父级订单' id='showparent' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr> --%>
				
					<%-- <td class="label-title">下单日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleOrder_genProjectDate"
						name="crmSaleOrder.genProjectDate" title="下单日期"
						readonly="readonly" class="text input readonlytrue"
						value="<s:date name="crmSaleOrder.genProjectDate" format="yyyy-MM-dd"/>" />
					</td>
					
					<td class="label-title">审核人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_confirmUser_name"
						value="<s:property value="crmSaleOrder.confirmUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="crmSaleOrder_confirmUser"
						name="crmSaleOrder.confirmUser.id"
						value="<s:property value="crmSaleOrder.confirmUser.id"/>">
					</td>
					<td class="label-title">审核日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleOrder_confirmDate" name="crmSaleOrder.confirmDate"
						title="审核日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmSaleOrder.confirmDate" format="yyyy-MM-dd"/>" />
					</td> --%>
				<tr>
					<%-- <td class="label-title">审核日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleOrder_confirmDate" name="crmSaleOrder.confirmDate"
						title="审核日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmSaleOrder.confirmDate" format="yyyy-MM-dd"/>" />
					</td> --%>
					<%-- <g:LayOutWinTag buttonId="showcrmContract" title="选择销售合同"
						hasHtmlFrame="true"
						html="${ctx}/crm/contract/crmContract/crmContractSelect.action"
						isHasSubmit="false" functionName="CrmContractFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_crmContract').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmContract_name').value=rec.get('name');" /> --%>
						
					<g:LayOutWinTag buttonId="showcrmContract" title="选择销售合同"
						hasHtmlFrame="true"
						html="${ctx}/crm/contract/crmContract/crmContractSelect.action"
						isHasSubmit="false" functionName="CrmContractFun" hasSetFun="true"
						extRec="rec"
						extStr="
						document.getElementById('crmSaleOrder_crmContract').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmContract_identifier').value=rec.get('identifier');
						document.getElementById('crmSaleOrder_crmContract_name').value=rec.get('name');" />

					<%-- <g:LayOutWinTag buttonId="showcrmContract" title="选择销售合同"
						hasHtmlFrame="true" html="${ctx}/crm/contract/crmContract/crmContractSelect.action"
						isHasSubmit="false" functionName="CrmContractFun" hasSetFun="true"
						documentId="crmSaleOrder_crmContract"
						documentName="crmSaleOrder_crmContract_name"
						/> --%>

					<%-- <td class="label-title" >销售合同</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="crmSaleOrder_crmContract_name"  value="<s:property value="crmSaleOrder.crmContract.name"/>"  />
 						<input type="hidden" id="crmSaleOrder_crmContract" name="crmSaleOrder.crmContract.id"  value="<s:property value="crmSaleOrder.crmContract.id"/>" > 
 						<img alt='选择销售合同' id='showcrmContract' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="CrmContractFun()" />                   		
                   	</td> --%>
				</tr>
				<tr>
					<%-- <td class="label-title">备注</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" colspan="4"><input type="text" size="95"
						maxlength="60" id="crmSaleOrder_note" name="crmSaleOrder.note"
						title="备注" value="<s:property value="crmSaleOrder.note"/>" /></td> --%>
					<td class="label-title">审核日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength=""
						id="crmSaleOrder_confirmDate" name="crmSaleOrder.confirmDate"
						title="审核日期" readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="crmSaleOrder.confirmDate" format="yyyy-MM-dd"/>" />
					</td>	
					<td class="label-title" style="display: none">工作流状态</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="hidden"
						id="crmSaleOrder_state" name="crmSaleOrder.state"
						value="<s:property value="crmSaleOrder.state"/>" /></td>
					<td class="label-title">工作流状态</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="30"
						id="crmSaleOrder_stateName" name="crmSaleOrder.stateName"
						title="工作流状态" readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="crmSaleOrder.stateName"/>" />
					</td>
					
					<td class="label-title">备注</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" colspan="4"><input type="text" size="40"
						maxlength="60" id="crmSaleOrder_note" name="crmSaleOrder.note"
						title="备注" value="<s:property value="crmSaleOrder.note"/>" /></td>
				
				</tr>	
				<tr>	
					<td class="label-title">附件</td>
					<td></td>
					<td title="保存基本后,可以维护查看附件">
						<div id="doclinks_img" style="width:100px;">
							<span class="attach-btn"></span> 
							<span class="text label">共有${requestScope.fileNum}个附件</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="label-title" style="display: none">content1</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content1"
						name="crmSaleOrder.content1" title="content1"
						value="<s:property value="crmSaleOrder.content1"/>"
						style="display: none" /></td>
				</tr>
				<tr>
					<td class="label-title" style="display: none">content3</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content3"
						name="crmSaleOrder.content3" title="content3"
						value="<s:property value="crmSaleOrder.content3"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content4</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content4"
						name="crmSaleOrder.content4" title="content4"
						value="<s:property value="crmSaleOrder.content4"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content5</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content5"
						name="crmSaleOrder.content5" title="content5"
						value="<s:property value="crmSaleOrder.content5"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content6</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content6"
						name="crmSaleOrder.content6" title="content6"
						value="<s:property value="crmSaleOrder.content6"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content7</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content7"
						name="crmSaleOrder.content7" title="content7"
						value="<s:property value="crmSaleOrder.content7"/>"
						style="display: none" /></td>
				</tr>
				<tr>


					<td class="label-title" style="display: none">content8</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content8"
						name="crmSaleOrder.content8" title="content8"
						value="<s:property value="crmSaleOrder.content8"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content9</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmSaleOrder_content9"
						name="crmSaleOrder.content9" title="content9" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleOrder.content9" format="yyyy-MM-dd"/>"
						style="display: none" /></td>


					<td class="label-title" style="display: none">content10</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmSaleOrder_content10"
						name="crmSaleOrder.content10" title="content10" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleOrder.content10" format="yyyy-MM-dd"/>"
						style="display: none" /></td>
				</tr>
				<tr>
					<td class="label-title" style="display: none">content11</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="" id="crmSaleOrder_content11"
						name="crmSaleOrder.content11" title="content11" Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleOrder.content11" format="yyyy-MM-dd"/>"
						style="display: none" /></td>
				</tr>
			</table>
			<table width="100%" class="section_table" cellpadding="0" >
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">合同基本信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
			</table>
			<table class="frame-table">
				<tr>
				
				<%-- <td class="label-title">合同编号</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" id="crmSaleOrder_crmContract" name="crmSaleOrder.crmContract.id"
						   	   value="<s:property value="crmSaleOrder.crmContract.id"/>">
					</td>--%>
					
				 <td class="label-title">合同编号</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="hidden" id="crmSaleOrder_crmContract" name="crmSaleOrder.crmContract.id"
						   	   value="<s:property value="crmSaleOrder.crmContract.id"/>">
						<input type="text" id="crmSaleOrder_crmContract_identifier" name="crmSaleOrder.crmContract.identifier"
						   	   value="<s:property value="crmSaleOrder.crmContract.identifier"/>">
					</td>  
				<td class="label-title">合同名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="50" title="${crmSaleOrder.crmContract.name}" id="crmSaleOrder_crmContract_name"
							   name="crmSaleOrder.crmContract.name" 
							   value="<s:property value="crmSaleOrder.crmContract.name"/>"/>
						<!-- <input type="button" onclick="lookCrmSaleOrder()" value="查" class='detail' /> -->
						</td>
					<td class="label-title">实际合同开始日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text"
						id="crmSaleOrder_startDate" name="crmSaleOrder.startDate"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleOrder.startDate" format="yyyy-MM-dd"/>">
					</td>
				</tr>
				<tr>
					<td class="label-title">实际合同结束日期</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text"
						id="crmSaleOrder_endDate" name="crmSaleOrder.endDate"
						Class="Wdate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmSaleOrder.endDate" format="yyyy-MM-dd"/>">
					</td>
					
					<td class="label-title">委托人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_crmCustomer_name"
						value="<s:property value="crmSaleOrder.crmCustomer.name"/>" /> <input
						type="hidden" id="crmSaleOrder_crmCustomer"
						name="crmSaleOrder.crmCustomer.id"
						value="<s:property value="crmSaleOrder.crmCustomer.id"/>">
						<img title='选择委托人' id='showcrmCustomer'
						src='${ctx}/images/img_lookup.gif' class='detail'
						onClick="CrmCustomerFun()" /></td>
						
					<%-- <g:LayOutWinTag buttonId="showType" title="选择项目类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="workType" hasSetFun="true"
						documentId="crmSaleOrder_workType"
						documentName="crmSaleOrder_workType_name" /> --%>
					
					<td class="label-title">委托单位</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="30" title="${crmSaleOrder.client}" id="crmSaleOrder_client"
							   name="crmSaleOrder.client" 
							   value="<s:property value="crmSaleOrder.client"/>"/>
						</td>
				</tr>
			</table>
			<table width="100%" class="section_table" cellpadding="0" >
		 		<tbody>
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">项目基本信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody> 
			</table>
			<table class="frame-table">
				<tr>
					<td class="label-title">项目名称</td>
					<td class="requiredcolumn" nowrap width="10px"
						><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text"
						size="20" maxlength="25" id="crmSaleOrder_content2"
						name="crmSaleOrder.content2" title="content2"
						value="<s:property value="crmSaleOrder.content2"/>"
						/></td>
						
					<td class="label-title">初步设计</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text"
						id="crmSaleOrder_projectStart_name"
						name="crmSaleOrder_projectStart_name"
						value="<s:property value="crmSaleOrder.projectStart.name"/>">
						<input type="hidden" id="crmSaleOrder_projectStart"
						name="crmSaleOrder.projectStart.id"
						value="<s:property value="crmSaleOrder.projectStart.id"/>">
						<img alt='选择初步设计' id='showProjectStart'
						src='${ctx}/images/img_lookup.gif' class='detail'
						onClick="ProjectStartFun()" /> <input type="button"
						onclick="lookProjectStart()" value="查" class='detail' /></td>
						
					<td class="label-title">基因ID</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_geneId" name="crmSaleOrder.geneId" readonly="readonly" class="text input readonlytrue"
						title="基因ID" value="<s:property value="crmSaleOrder.geneId"/>" />
					</td>
				</tr>
				<tr>
					<td class="label-title">基因名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_geneName" name="crmSaleOrder.geneName" 
						title="基因名称" value="<s:property value="crmSaleOrder.geneName"/>" />
					</td>
					<td class="label-title">基因别名</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_geneByName" name="crmSaleOrder.geneByName" readonly="readonly" class="text input readonlytrue"
						title="基因别名" value="<s:property value="crmSaleOrder.geneByName"/>" />
					</td>
					
					<g:LayOutWinTag buttonId="showType" title="选择项目类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="workType" hasSetFun="true"
						documentId="crmSaleOrder_workType"
						documentName="crmSaleOrder_workType_name" />
					<td class="label-title">项目类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_workType_name"
						value="<s:property value="crmSaleOrder.workType.name"/>" /> <input
						type="hidden" id="crmSaleOrder_workType"
						name="crmSaleOrder.workType.id"
						value="<s:property value="crmSaleOrder.workType.id"/>"> <img
						alt='选择项目类型' id='showType' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showprojectStart" title="选择技术类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="modelPreparation"
						hasSetFun="true" documentId="crmSaleOrder_technologyType"
						documentName="crmSaleOrder_technologyType_name" />
					<td class="label-title">技术类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleOrder_technologyType_name"
						value="<s:property value="crmSaleOrder.technologyType.name"/>" />
						<input type="hidden" id="crmSaleOrder_technologyType"
						name="crmSaleOrder.technologyType.id"
						value="<s:property value="crmSaleOrder.technologyType.id"/>">
						<img alt='选择技术类型' id='showprojectStart'
						src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
					<g:LayOutWinTag buttonId="showGeneTargetingType" title="选择基因打靶类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="dbMethod" hasSetFun="true"
						documentId="crmSaleOrder_geneTargetingType"
						documentName="crmSaleOrder_geneTargetingType_name" />
					<td class="label-title">基因打靶类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="50"
						id="crmSaleOrder_geneTargetingType_name"
						value="<s:property value="crmSaleOrder.geneTargetingType.name"/>" />
						<input type="hidden" id="crmSaleOrder_geneTargetingType"
						name="crmSaleOrder.geneTargetingType.id"
						value="<s:property value="crmSaleOrder.geneTargetingType.id"/>">
						<img alt='选择基因打靶类型' id='showGeneTargetingType'
						src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
					<g:LayOutWinTag buttonId="showtype" title="选择产品类型"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="showTypeList" hasSetFun="true"
						documentId="crmSaleOrder_type"
						documentName="crmSaleOrder_type_name" />
					<td class="label-title">产品类型</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleOrder_type_name"
						value="<s:property value="crmSaleOrder.type.name"/>" /> <input
						type="hidden" id="crmSaleOrder_type" name="crmSaleOrder.type.id"
						value="<s:property value="crmSaleOrder.type.id"/>"> <img
						alt='选择产品类型' id='showtype' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showgenus" title="选择种属"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="genera" hasSetFun="true"
						documentId="crmSaleOrder_genus"
						documentName="crmSaleOrder_genus_name" />
					<td class="label-title">种属</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						id="crmSaleOrder_genus_name"
						value="<s:property value="crmSaleOrder.genus.name"/>" /> <input
						type="hidden" id="crmSaleOrder_genus" name="crmSaleOrder.genus.id"
						value="<s:property value="crmSaleOrder.genus.id"/>"> <img
						alt='选择种属' id='showgenus' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<g:LayOutWinTag buttonId="showstrain" title="选择品系"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="saleChanceStrain" hasSetFun="true"
						documentId="crmSaleOrder_strain"
						documentName="crmSaleOrder_strain_name" />
					<td class="label-title">品系</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<input type="text" size="20" id="crmSaleOrder_strain_name"
								value="<s:property value="crmSaleOrder.strain.name"/>" />
						<input type="hidden" id="crmSaleOrder_strain" name="crmSaleOrder.strain.id"
								value="<s:property value="crmSaleOrder.strain.id"/>"/> 
						<img alt='选择品系' id='showstrain' src='${ctx}/images/img_lookup.gif' class='detail' /></td>
				</tr>
			</table>
			<table width="100%" class="section_table" cellpadding="0" >
		 		<tbody>
					<tr class="sectionrow " valign="top">
						<td class="sectioncol " width='50%' valign="top" align="right">
							<div class="section standard_section marginsection  ">
								<div class="section_header standard_section_header">
									<table width="100%" cellpadding="0" cellspacing="0">
										<tbody>
											<tr>
												<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">产品信息</span>
												</td>
												<td class="section_header_right" align="right"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody> 
			</table>
			<table class="frame-table">
				<tr>
					<g:LayOutWinTag buttonId="showProduct" title="选择终产品"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="finalType" hasSetFun="true"
						documentId="crmSaleOrder_product"
						documentName="crmSaleOrder_product_name" />
					<td class="label-title">终产品</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_product_name"
						value="<s:property value="crmSaleOrder.product.name"/>" /> <input
						type="hidden" id="crmSaleOrder_product"
						name="crmSaleOrder.product.id"
						value="<s:property value="crmSaleOrder.product.id"/>"> <img
						alt='选择终产品' id='showProduct' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<td class="label-title">终产品数量</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="crmSaleOrder_productNo" name="crmSaleOrder.productNo"
						title="终产品数量" value="<s:property value="crmSaleOrder.productNo"/>" />
					</td>
					<g:LayOutWinTag buttonId="showCheckUp" title="选择质控要求"
						hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
						isHasSubmit="false" functionName="checkUp" hasSetFun="true"
						documentId="crmSaleOrder_checkup"
						documentName="crmSaleOrder_checkup_name" />
					<td class="label-title">质控要求</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="crmSaleOrder_checkup_name"
						value="<s:property value="crmSaleOrder.checkup.name"/>" /> <input
						type="hidden" id="crmSaleOrder_checkup"
						name="crmSaleOrder.checkup.id"
						value="<s:property value="crmSaleOrder.checkup.id"/>"> <img
						alt='选择终产品' id='showCheckUp' src='${ctx}/images/img_lookup.gif'
						class='detail' />
					</td>
				</tr>
				<tr>
					<td class="label-title">是否去Neo</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left">
						<select id="crmSaleOrder_neoResult" name="crmSaleOrder.neoResult" class="input-10-length"
							style="width: 152px;">
							<option value="0"
								<s:if test="crmSaleOrder.neoResult==0">selected="selected"</s:if>>否</option>
							<option value="1"
								<s:if test="crmSaleOrder.neoResult==1">selected="selected"</s:if>>是</option>
						</select>
				 	</td>
				 	<td class="label-title">客户要求</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" colspan="4">
						<textarea  title="客户要求" style="overflow: hidden; width: 600px; height: 40px;" id="crmSaleOrder_customerRequirements" name="crmSaleOrder.customerRequirements"> <s:property value="crmSaleOrder.customerRequirements"/></textarea>
					</td>
				</tr>
				<tr>
						<td class="label-title" >代养目标</td>
						<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
			            <td align="left"  colspan="4">
	               	 		<textarea  id="crmSaleOrder_generationTarget" name="crmSaleOrder.generationTarget"  maxlength="500" style="overflow: hidden; width: 500px; height: 40px;" ><s:property value="crmSaleOrder.generationTarget"/></textarea>
						</td>
				</tr>
			</table>						
			<input type="hidden" name="crmSaleOrderLinkmanJson" id="crmSaleOrderLinkmanJson" value="" /> 
			<input type="hidden" id="id_parent_hidden" value="<s:property value="crmSaleOrder.id"/>" />
		</form>
		<!-- <div id="tabs">
            <ul>
			<li><a href="#crmSaleOrderLinkmanpage">订单管理联系人</a></li>
           	</ul>  -->
		<div id="crmSaleOrderLinkmanpage" width="100%" height:10px></div>
	</div>
	<!-- </div> -->
</body>
</html>
