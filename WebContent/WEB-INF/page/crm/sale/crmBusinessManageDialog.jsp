﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmBusinessManageDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="crmBusinessManage_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">商机来源</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmBusinessManage_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">客户经理</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="100" id="crmBusinessManage_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">岗位类型</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_postType" searchField="true" name="postType"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">联系人姓名</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_linkmanNname" searchField="true" name="linkmanNname"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">联系人手机</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmBusinessManage_linkmanMobile" searchField="true" name="linkmanMobile"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">固定电话</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmBusinessManage_linkmanPhone" searchField="true" name="linkmanPhone"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">相关主表</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_businessManage" searchField="true" name="businessManage"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">email</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmBusinessManage_email" searchField="true" name="email"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">单位名称</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmBusinessManage_unitName" searchField="true" name="unitName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">单位地址</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmBusinessManage_unitAddress" searchField="true" name="unitAddress"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">所属委托人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_belongConsignor" searchField="true" name="belongConsignor"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">研究方向</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmBusinessManage_resDirection" searchField="true" name="resDirection"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">部门/学科</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmBusinessManage_dept" searchField="true" name="dept"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">行业类型</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_industryType" searchField="true" name="industryType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">国家</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmBusinessManage_country" searchField="true" name="country"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">省</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmBusinessManage_privince" searchField="true" name="privince"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">市</td>
               	<td align="left">
                    		<input type="text" maxlength="15" id="crmBusinessManage_city" searchField="true" name="city"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmBusinessManage_content1" searchField="true" name="content1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmBusinessManage_content2" searchField="true" name="content2"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmBusinessManage_content3" searchField="true" name="content3"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmBusinessManage_content4" searchField="true" name="content4"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_content5" searchField="true" name="content5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_content6" searchField="true" name="content6"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_content7" searchField="true" name="content7"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmBusinessManage_content8" searchField="true" name="content8"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmBusinessManage_content9" searchField="true" name="content9"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmBusinessManage_content10" searchField="true" name="content10"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmBusinessManage_content11" searchField="true" name="content11"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmBusinessManage_content12" searchField="true" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_crmBusinessManage_div"></div>
   		
</body>
</html>



