﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<body>
<script type="text/javascript" src="${ctx}/js/crm/sale/crmSaleOrderDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="crmSaleOrder_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="crmSaleOrder_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	</tr>
           	<tr>
           			<td class="label-title" >市场推广活动</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmMarcketActivity_name" searchField="true"  name="crmMarcketActivity.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmMarcketActivity" name="crmSaleOrder.crmMarcketActivity.id"  value="" > 
 						<img alt='选择市场推广活动' id='showcrmMarcketSale' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="CrmMarcketActivityFun()"/>                   		
                   	</td>

					<g:LayOutWinTag buttonId="showcrmCustomer" title="选择客户"
						hasHtmlFrame="true"
						html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
						isHasSubmit="false" functionName="CrmCustomerFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('crmSaleOrder_crmCustomer').value=rec.get('id');
						document.getElementById('crmSaleOrder_crmCustomer_name').value=rec.get('name');" />
               	 	<td class="label-title" >客户</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_crmCustomer_name" searchField="true"  name="crmCustomer.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_crmCustomer" name="crmSaleOrder.crmCustomer.id"  value="" > 
 						<img alt='选择客户' id='showcrmCustomer' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
           	 	<td class="label-title">金额</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmSaleOrder_money" searchField="true" name="money"  class="input-20-length"></input>
               	</td>
           	 	<g:LayOutWinTag buttonId="showtype" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
				documentId="crmSaleOrder_type"
				documentName="crmSaleOrder_type_name"
			/>
               	 	<td class="label-title" >分类</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_type_name" searchField="true"  name="type.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_type" name="crmSaleOrder.type.id"  value="" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
           	</tr>
           	<tr>
           	 	
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleOrder_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
                   		<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                   	</td>
           	</tr>
           	<tr>
           		<g:LayOutWinTag buttonId="showmanager" title="选择客户经理"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="ManagerUserFun" 
	 				hasSetFun="true"
					documentId="crmSaleOrder_manager"
					documentName="crmSaleOrder_manager_name"/>
               	 	<td class="label-title" >客户经理</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_manager_name" searchField="true"  name="manager.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_manager" name="crmSaleOrder.manager.id"  value="" > 
 						<img alt='选择客户经理' id='showmanager' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	<g:LayOutWinTag buttonId="showorderSource" title="选择来源"
					hasHtmlFrame="true"
					html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="orderSource" 
	 				hasSetFun="true"
					documentId="crmSaleOrder_orderSource"
					documentName="crmSaleOrder_orderSource_name"/>
               	 	<td class="label-title" >来源</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="crmSaleOrder_orderSource_name" searchField="true"  name="orderSource.name"  value="" class="text input" />
 						<input type="hidden" id="crmSaleOrder_orderSource" name="crmSaleOrder.orderSource.id"  value="" > 
 						<img alt='选择来源' id='showorderSource' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
           	 	
	
           	 	<td class="label-title" style="display:none">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content1" searchField="true" style="display:none" name="content1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content2" searchField="true" style="display:none" name="content2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content3" searchField="true" style="display:none" name="content3"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content4" searchField="true" style="display:none" name="content4"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content5" searchField="true" style="display:none" name="content5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content6" searchField="true" style="display:none" name="content6"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content7" searchField="true" style="display:none" name="content7"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="crmSaleOrder_content8" searchField="true" style="display:none" name="content8"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleOrder_content9" searchField="true" style="display:none" name="content9"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleOrder_content10" searchField="true" style="display:none" name="content10"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleOrder_content11" searchField="true" style="display:none" name="content11"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="crmSaleOrder_content12" searchField="true" style="display:none" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_crmSaleOrder_div"></div>
   		
</body>
</html>



