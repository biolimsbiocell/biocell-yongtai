﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/overseas/crmOverseasMedicalDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmOverseasMedical_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="250" id="crmOverseasMedical_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">病人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmOverseasMedical_patient" searchField="true" name="patient"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">海外医疗中心</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmOverseasMedical_supplier" searchField="true" name="supplier"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">渠道推荐</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmOverseasMedical_customer" searchField="true" name="customer"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">护照号</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmOverseasMedical_passportNo" searchField="true" name="passportNo"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">病情小结</td>
               	<td align="left">
                    		<input type="text" maxlength="500" id="crmOverseasMedical_remark" searchField="true" name="remark"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">就医效果</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmOverseasMedical_effect" searchField="true" name="effect"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmOverseasMedical_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmOverseasMedical_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmOverseasMedical_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态名称</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="crmOverseasMedical_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_crmOverseasMedical_div"></div>
   		
</body>
</html>



