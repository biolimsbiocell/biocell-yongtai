<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="crmOverseasMedical_label" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=crmOverseasMedical&id=crmOverseasMedical.id"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/overseas/crmOverseasMedicalEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
               	 	<td class="label-title">编码
               	 		<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	 	</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="36" id="crmOverseasMedical_id" name="crmOverseasMedical.id" title="编码" 
                   		 	class="text input readonlytrue" readonly="readOnly"
                   		 value="<s:property value="crmOverseasMedical.id"/>" 
                   	 />
                   	</td>
               	 	<td class="label-title">描述
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	</td>
                   	<td align="left">
                   	<input type="text" size="50" maxlength="250" id="crmOverseasMedical_note" name="crmOverseasMedical.note" title="描述" 
                   		 	class="text input"
                   		 value="<s:property value="crmOverseasMedical.note"/>" 
                   	 />
                   	</td>
			<g:LayOutWinTag buttonId="showpatient" title="选择病人"
				hasHtmlFrame="true" width="800"
				html="${ctx}/crm/customer/patient/crmPatientSelect.action"
				isHasSubmit="false" functionName="CrmPatientFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmOverseasMedical_patient').value=rec.get('id');
				document.getElementById('crmOverseasMedical_patient_name').value=rec.get('name');" />
			
               	 	<td class="label-title">病人
               	 		<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	 	</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmOverseasMedical_patient_name"  value="<s:property value="crmOverseasMedical.patient.name"/>" class="text input"/>
 						
 						<img alt='选择病人' id='showpatient' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
				</tr>
				
                   
				<tr>
               	 
			
               	 	<td class="label-title">创建人
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmOverseasMedical_createUser_name"  value="<s:property value="crmOverseasMedical.createUser.name"/>" class="text input"/>
 						<input type="hidden" id="crmOverseasMedical_createUser" name="crmOverseasMedical.createUser.id"  value="<s:property value="crmOverseasMedical.createUser.id"/>" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
				
               	 	<td class="label-title">创建日期
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
              
                   	<td align="left">
                   	<input type="text" size="12" maxlength="50" id="crmOverseasMedical_createDate" name="crmOverseasMedical.createDate" title="创建日期" 
                   		 	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"    
                   		value="<s:date name="crmOverseasMedical.createDate" format="yyyy-MM-dd"/>"
                   	 />
                   	</td>
               	 		<td class="label-title">状态名称
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="10" id="crmOverseasMedical_stateName" name="crmOverseasMedical.stateName" title="状态名称" 
                   		 	class="text input readonlytrue"
                   		 value="<s:property value="crmOverseasMedical.stateName"/>"/>
                   	 	<input type="hidden" id="crmOverseasMedical_state" name="crmOverseasMedical.state" 
                   		 value="<s:property value="crmOverseasMedical.state"/>"/>
                   	</td>
               	 	
				</tr>
            </table>
             <table width="100%" class="section_table" cellpadding="0">
				<tr valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section_header standard_section_header">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td
											class="section_header_left text standard_section_label labelcolor"
											align="left"><span class="section_label"> 病人信息 </span></td>
										<td class="section_header_right" align="right"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
			</table>
			<table class="frame-table">

				<tr>
					 <td class="label-title">病人ID 
					</td>
					<td align="left">
					<input type="text" size="10" maxlength="36" 
					   id="crmOverseasMedical_patient" 
					   name="crmOverseasMedical.patient.id"  
					   class="text input readonlytrue"
					   readonly="readOnly"
					   value="<s:property value="crmOverseasMedical.patient.id"/>" > 
						
					</td> 
					 <td class="label-title">出生年月</td>
					<td align="left"><input type="text" size="12" maxlength="100"
						id="crmOverseasMedical_patient_dateOfBirth" name="crmOverseasMedical.patient.dateOfBirth"
						title="出生年月" Class="Wdate"
						class="text input readonlytrue"
						readonly="readOnly"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
						value="<s:date name="crmOverseasMedical.patient.dateOfBirth" format="yyyy-MM-dd"/>" />
					</td>
			
					<td class="label-title">年龄</td>
					<td align="left"><input type="text" size="15" maxlength="100"
						id="crmOverseasMedical_patient_age" style="height: 20px; width: 80px"
						name="crmOverseasMedical.patient.age" title="年龄" class="text input readonlytrue"
						readonly="readOnly" value="<s:property value="crmOverseasMedical.patient.age"/>" />
					</td>
					
				</tr>
				<tr>
					<td class="label-title">姓名</td>
					<td align="left"><input type="text" size="50" maxlength="20"
						style="height: 20px; width: 80px" id="crmOverseasMedical_patient_name"
						name="crmOverseasMedical.patient.name" title="姓名" class="text input readonlytrue"
						readonly="readOnly" value="<s:property value="crmOverseasMedical.patient.name"/>" />
					</td>
					<td class="label-title">身份证号</td>
					<td align="left"><input type="text" size="15" maxlength="200"
					    class="text input readonlytrue"
						readonly="readOnly"
						style="height: 20px; width: 160px" id="crmOverseasMedical_patient_sfz"
						name="crmOverseasMedical.patient.sfz" title="身份证号" class="text input"
						value="<s:property value="crmOverseasMedical.patient.sfz"/>" /></td>
						<td class="label-title">性别</td>
					<td align="left"><select id="crmOverseasMedical_patient_gender"
						name="crmOverseasMedical.patient.gender" class="input-10-length">
						<option value="0"
								<s:if test="crmOverseasMedical.patient.gender==0">selected="selected"</s:if>>--请选择--</option>
							<option value="1"
								<s:if test="crmOverseasMedical.patient.gender==1">selected="selected"</s:if>>男</option>
							<option value="2"
								<s:if test="crmOverseasMedical.patient.gender==2">selected="selected"</s:if>>女</option>
					</select>
						
						</tr>
						
						
							<tr>
					<td class="label-title">联系电话</td>
					<td align="left"><input type="text" size="40" maxlength="100"
						id="crmPatient_telphoneNumber1" name=""
						title="联系电话" class="text input readonlytrue"
						readonly="readOnly"
						value="<s:property value="crmOverseasMedical.patient.telphoneNumber1"/>" /></td>
						<td class="label-title">职业</td>
					<td align="left"><input type="text" size="15" maxlength="100"
					    class="text input readonlytrue"
						readonly="readOnly"
						id="crmOverseasMedical_patient_occupation" name="crmOverseasMedical.patient.occupation" title="职业"
						class="text input"
						value="<s:property value="crmOverseasMedical.patient.occupation"/>" /></td>
						<td class="label-title">家庭住址</td>
					<td align="left"><input type="text" size="15" maxlength="200"
					    class="text input readonlytrue"
					    readonly="readOnly"
						style="height: 20px; width: 160px" id="crmOverseasMedical_patient_address"
						name="crmOverseasMedical.patient.address" title="家庭住址" class="text input"
						value="<s:property value="crmOverseasMedical.patient.address"/>" /></td>
				</tr>
						
						<tr>
						
					<td class="label-title">病人状态</td>
					<td align="left"><input type="text" size="10"
					    class="text input readonlytrue"
						readonly="readOnly" id="crmOverseasMedical_patient_patientStatus_name"
						value="<s:property value="crmOverseasMedical.patient.patientStatus.name"/>"
						class="text input" /> <input type="hidden"
						id="crmOverseasMedical_patient_patientStatus" name="crmOverseasMedical.patient.patientStatus.id"
						value="<s:property value="crmOverseasMedical.patient.patientStatus.id"/>">
						</td>
					
				</tr>
						
					<%-- 	<tr>
					<td class="label-title">联系电话</td>
					<td align="left"><input type="text" size="10" maxlength="100"
						id="crmOverseasMedical_patient_telphoneNumber1" name="crmOverseasMedical.patient.telphoneNumber1"
						title="联系电话1" class="text input"
						value="<s:property value="crmOverseasMedical.patient.telphoneNumber1"/>" /></td>
					<td class="label-title">其他联系人</td>
					<td align="left"><input type="text" size="10" maxlength="100"
						id="crmOverseasMedical_patient_telphoneNumber2" name="crmOverseasMedical.patient.telphoneNumber2"
						title="联系电话2" class="text input"
						value="<s:property value="crmOverseasMedical.patient.telphoneNumber2"/>" /></td>
					<td class="label-title">联系人电话</td>
					<td align="left"><input type="text" size="10" maxlength="100"
						id="crmOverseasMedical_patient_telphoneNumber3" name="crmOverseasMedical.patient.telphoneNumber3"
						title="联系电话3" class="text input"
						value="<s:property value="crmOverseasMedical.patient.telphoneNumber3"/>" /></td>
				</tr>
				<tr>
				<td class="label-title">诊断</td>
							<td align="left"><input type="text" size="15"
								id="crmOverseasMedical_patient_familyHistoryOfCancertext" name="crmOverseasMedical.patient.familyHistoryOfCancertext"
								value="<s:property value="crmOverseasMedical.patient.familyHistoryOfCancertext"/>"
								class="text input" /></td>
				<td class="label-title">诊断日期</td>
							<td align="left"><input type="text" size="15"
								id="crmOverseasMedical_patient_familyHistoryOfCancertext" name="crmOverseasMedical.patient.familyHistoryOfCancertext"
								value="<s:property value="crmOverseasMedical.patient.familyHistoryOfCancertext"/>"
								class="text input" /></td>	
								
				<td class="label-title">医院名称</td>
							<td align="left"><input type="text" size="15"
								id="crmOverseasMedical_patient_familyHistoryOfCancertext" name="crmOverseasMedical.patient.familyHistoryOfCancertext"
								value="<s:property value="crmOverseasMedical.patient.familyHistoryOfCancertext"/>"
								class="text input" /></td>	
					</tr>
					<tr>			
				<td class="label-title">主治医生</td>
							<td align="left"><input type="text" size="15"
								id="crmOverseasMedical_patient_familyHistoryOfCancertext" name="crmOverseasMedical.patient.familyHistoryOfCancertext"
								value="<s:property value="crmOverseasMedical.patient.familyHistoryOfCancertext"/>"
								class="text input" /></td>	
				<td class="label-title">备注</td>
							<td align="left"><input type="text" size="15"
								id="crmOverseasMedical_patient_familyHistoryOfCancertext" name="crmOverseasMedical.patient.familyHistoryOfCancertext"
								value="<s:property value="crmOverseasMedical.patient.familyHistoryOfCancertext"/>"
								class="text input" /></td>													
								
		</tr> --%>
</table>
<table width="100%" class="section_table" cellpadding="0">
				<tr valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section_header standard_section_header">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td
											class="section_header_left text standard_section_label labelcolor"
											align="left"><span class="section_label">出国前信息</span></td>
										<td class="section_header_right" align="right"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
			</table>
 			<table class="frame-table">
			
			<tr>
			<g:LayOutWinTag buttonId="showsupplier" title="选择海外医疗中心"
				hasHtmlFrame="true" width="800"
				html="${ctx}/supplier/common/supplierSelect.action?id=4028814a48a132d20148a66019e30002"
				isHasSubmit="false" functionName="SupplierFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmOverseasMedical_supplier').value=rec.get('id');
				document.getElementById('crmOverseasMedical_supplier_name').value=rec.get('name');" />
			
               	 	<td class="label-title">海外医疗中心</td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	
                   	<td align="left">
 						<input type="text" size="30" readonly="readOnly"  id="crmOverseasMedical_supplier_name"  value="<s:property value="crmOverseasMedical.supplier.name"/>" class="text input"/>
 						<input type="hidden" id="crmOverseasMedical_supplier" name="crmOverseasMedical.supplier.id"  value="<s:property value="crmOverseasMedical.supplier.id"/>" > 
 						<img alt='选择海外医疗中心' id='showsupplier' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcustomer" title="选择渠道推荐"
				hasHtmlFrame="true"  width="800"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="CrmCustomerFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmOverseasMedical_customer').value=rec.get('id');
				document.getElementById('crmOverseasMedical_customer_name').value=rec.get('name');" />
			
               	 	<td class="label-title">渠道推荐</td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	
                   	<td align="left">
 						<input type="text" size="30" readonly="readOnly"  id="crmOverseasMedical_customer_name"  value="<s:property value="crmOverseasMedical.customer.name"/>" class="text input"/>
 						<input type="hidden" id="crmOverseasMedical_customer" name="crmOverseasMedical.customer.id"  value="<s:property value="crmOverseasMedical.customer.id"/>" > 
 						<img alt='选择渠道推荐' id='showcustomer' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
                   	            <td class="label-title">护照号</td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	
               	 		<td align="left">
                   	<input type="text" size="30" maxlength="50" id="crmOverseasMedical_passportNo" name="crmOverseasMedical.passportNo" title="护照号" 
                   		 	class="text input"
                   		 value="<s:property value="crmOverseasMedical.passportNo"/>" 
                   	 />
                   	</td>
				</tr>
				
               <tr>
				      <td class="label-title">海外就医ID</td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	
               	 		<td align="left">
                   	<input type="text" size="30" maxlength="50" id="crmOverseasMedical_overseasHosNo" name="crmOverseasMedical.overseasHosNo" title="海外就医ID" 
                   		 	class="text input"
                   		 value="<s:property value="crmOverseasMedical.overseasHosNo"/>" 
                   	 />
                   	</td>
				
				</tr>
			</table>
			<table width="100%" class="section_table" cellpadding="0">
				<tr valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section_header standard_section_header">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td
											class="section_header_left text standard_section_label labelcolor"
											align="left"><span class="section_label">出国后信息</span></td>
										<td class="section_header_right" align="right"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</td>
				</tr>
			</table>
		 <table class="frame-table">
		 <g:LayOutWinTag buttonId="showEffect" title="选择就医效果" hasHtmlFrame="true"
					html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
					functionName="jyxg" hasSetFun="true" documentId="crmOverseasMedical_effect"
					documentName="crmOverseasMedical_effect" />
			      <tr>
			      
			      <td class="label-title">就医效果</td>
               	 		            	 	
               	 	
               	 		<td align="left">
                   	<input type="text" size="50" maxlength="50" id="crmOverseasMedical_effect" name="crmOverseasMedical.effect" title="护照号" 
                   		 	class="text input"
                   		 value="<s:property value="crmOverseasMedical.effect"/>" 
                   	 />
                   	 	<img alt='选择海外医疗中心' id='showEffect' src='${ctx}/images/img_lookup.gif' 	class='detail' />          
                   	</td>
                   	<td class="label-title">&nbsp;&nbsp;</td>
                   	
			      </tr>
			</table> 
            <input type="hidden" name="applicationTypeActionLogJson" id="applicationTypeActionLogJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="crmOverseasMedical.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#applicationTypeActionLogpage">过程记录</a></li>
			<li><a href="#filegroup">相关文件</a></li>
           	</ul> 
			<div id="applicationTypeActionLogpage" width="100%" style="height:350px"></div>
			<div id="filegroup" style="height:550px">
			<iframe scrolling="auto" name="tab11frame"
				src="${ctx}/file/group/showFileGroupList.action?handlemethod=${handlemethod}&modelContentId=${crmOverseasMedical.id}&modelType=crmOverseasMedical"
				frameborder="0" width="100%" height="100%"></iframe>
			
			</div>
			</div>
        	</div>
        	
	</body>
	</html>
