﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/overseas/crmOverseasMedical.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table>
			<tr>
               	   <td class="label-title">编码</td>
                   	<td align="left">
					
                   	<input type="text" size="20" maxlength="36" searchField="true" id="crmOverseasMedical_id" name="id" title="编码" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">描述</td>
                   	<td align="left">
					
                   	<input type="text" size="25" maxlength="250" searchField="true" id="crmOverseasMedical_note" name="note" title="描述" value="" 
                   		 	class="text input"
                   	 />
                   	</td> 
             </tr>
             <tr>      	
			<g:LayOutWinTag buttonId="showpatient" title="选择病人"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/patient/crmPatientSelect.action"
				isHasSubmit="false" functionName="CrmPatientFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmOverseasMedical_patient').value=rec.get('id');
				document.getElementById('crmOverseasMedical_patient_name').value=rec.get('name');" />
               		<td class="label-title">病人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmOverseasMedical_patient_name"  value="" class="text input"/>
 						<input type="hidden" id="crmOverseasMedical_patient" name="patient.id"  value="" searchField="true" > 
 						<img alt='选择病人' id='showpatient' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			
			
			<g:LayOutWinTag buttonId="showsupplier" title="选择海外医疗中心"
				hasHtmlFrame="true"
				html="${ctx}/supplier/common/supplierSelect.action?id=4028814a48a132d20148a66019e30002"
				isHasSubmit="false" functionName="SupplierFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmOverseasMedical_supplier').value=rec.get('id');
				document.getElementById('crmOverseasMedical_supplier_name').value=rec.get('name');" />
			
               		<td class="label-title">海外医疗中心</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmOverseasMedical_supplier_name"  value="" class="text input"/>
 						<input type="hidden" id="crmOverseasMedical_supplier" name="supplier.id"  value="" searchField="true" > 
 						<img alt='选择海外医疗中心' id='showsupplier' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
                   	</tr>
                   		<tr>
               	   <td class="label-title">姓</td>
                   	<td align="left">
					
                   	<input type="text" size="20" maxlength="36" searchField="true" id="" name="patient.lastName" title="" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
               		<td class="label-title">名</td>
                   	<td align="left">
					
                   	<input type="text" size="25" maxlength="36" searchField="true" id="" name="patient.firstName" title="" value="" 
                   		 	class="text input"
                   	 />
                   	</td> 
                   	</tr>
                   	<tr>
               	   <td class="label-title">病人ID</td>
                   	<td align="left">
					
                   	<input type="text" size="20" maxlength="36" searchField="true" id="" name="patient.id" title="" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
                   	<tr>
			<g:LayOutWinTag buttonId="showcustomer" title="选择客户"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="CrmCustomerFun" hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('crmOverseasMedical_customer').value=rec.get('id');
				document.getElementById('crmOverseasMedical_customer_name').value=rec.get('name');" />
               		<td class="label-title">选择客户</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmOverseasMedical_customer_name"  value="" class="text input"/>
 						<input type="hidden" id="crmOverseasMedical_customer" name="customer.id"  value="" searchField="true" > 
 						<img alt='选择客户' id='showcustomer' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
               		<td class="label-title">就医效果</td>
                   	<td align="left">
					
                   	<input type="text" size="15" maxlength="50" searchField="true" id="crmOverseasMedical_effect" name="effect" title="就医效果" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
                   	</tr>
                   	<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="crmOverseasMedical_createUser"
				documentName="crmOverseasMedical_createUser_name"
			 />
               		<td class="label-title">创建人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmOverseasMedical_createUser_name"  value="" class="text input"/>
 						<input type="hidden" id="crmOverseasMedical_createUser" name="createUser.id"  value="" searchField="true" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
				<!-- <td class="label-title">创建日期</td>
                   	<td align="left">
					
                   	<input type="text" size="12" maxlength="50" searchField="true" id="crmOverseasMedical_createDate" name="createDate" title="创建日期" value="" 
                    	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	 />
                   	 <input type="hidden" id="receiveDate1" name="receiveDate##@@##1"  searchField="true" />
                   	</td> -->
                   	
               		<td class="label-title">状态</td>
                   	<td align="left">
					
                   
                   	 <select searchField="true" id="crmOverseasMedical_stateName" name="stateName">
                   	 <option value="">-请选择-</option>
                   	 <option value="取得签证">取得签证 </option>
					 <option value="递交材料">递交材料</option>
  					 <option value="翻译材料">翻译材料</option>
					 <option value="补充材料">补充材料</option>
					 <option value="医院审核">医院审核</option>
					 <option value="审核拒绝">审核拒绝</option>
					 <option value="签证申请">签证申请</option>
					 <option value="取得签证">取得签证</option>
                   	 </select>
                   	 
                   	</td>
			</tr>
			<tr>	
					<td class="label-title">创建日期</td>
					<td>
						<input type="text" class="Wdate" readonly="readonly" id="startCreateDate" name="startCreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" />
					</td>
					<td class="label-title">到</td>
				    <td>
						<input type="text" class="Wdate" readonly="readonly" id="endCreateDate" name="endCreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
					</td>
				</tr>
        </table>
		</form>
		</div>
		<div id="show_crmOverseasMedical_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
</body>
</html>



