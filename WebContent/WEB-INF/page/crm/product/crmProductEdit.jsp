<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>

<s:if test='#request.handlemethod!="add"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=crmProduct&id=${crmProduct.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>

<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/product/crmProductEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
               	 	<td class="label-title">编码</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="36" id="crmProduct_id" name="crmProduct.id" title="编码" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.id"/>" 
                   	 /><font class="label-title">小写,无特殊字符,连续</font>
                   	</td>
			<g:LayOutWinTag buttonId="showtype" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
				
				extStr="document.getElementById('crmProduct_type').value=id;document.getElementById('crmProduct_type_name').value=name;
		if(id!='jccp'){
			document.getElementById('notOverSeas').style.display='none';
			document.getElementById('isOverSeas').style.display='block';
		}else{
		     document.getElementById('isOverSeas').style.display='none';
			document.getElementById('notOverSeas').style.display='block';
		}
		" />
				
			
               	 	<td class="label-title">分类</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmProduct_type_name"  value="<s:property value="crmProduct.type.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_type" name="crmProduct.type.id"  value="<s:property value="crmProduct.type.id"/>" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showdutyUser" title="选择负责人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showdutyUserFun" 
				documentId="crmProduct_dutyUser"
				documentName="crmProduct_dutyUser_name"
			 />
               	 	<td class="label-title">负责人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmProduct_dutyUser_name"  value="<s:property value="crmProduct.dutyUser.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_dutyUser" name="crmProduct.dutyUser.id"  value="<s:property value="crmProduct.dutyUser.id"/>" > 
 						<img alt='选择负责人' id='showdutyUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
				</tr>
				<tr>
				<td class="label-title">名称</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="50" id="crmProduct_name" name="crmProduct.name" title="描述" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.name"/>" 
                   	 />
                   	</td>
				 <td class="label-title">描述</td>
                   	<td align="left">
                   	<input type="text" size="40" maxlength="50" id="crmProduct_note" name="crmProduct.note" title="描述" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.note"/>" 
                   	 />
                 </td>
                  	<g:LayOutWinTag buttonId="showkind" title="选择产品类型"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showkindFun" hasSetFun="true"
				
				extStr="document.getElementById('crmProduct_kind').value=id;document.getElementById('crmProduct_kind_name').value=name;
		" />
                   	<td class="label-title">产品类型</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmProduct_kind_name"  value="<s:property value="crmProduct.kind.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_kind" name="crmProduct.kind.id"  value="<s:property value="crmProduct.kind.id"/>" > 
 						<img alt='选择产品类型' id='showkind' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
				</tr>
				<tr style="display:none">
			<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="crmProduct_createUser"
				documentName="crmProduct_createUser_name"
			 />
               	 	<td class="label-title">创建人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmProduct_createUser_name"  value="<s:property value="crmProduct.createUser.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_createUser" name="crmProduct.createUser.id"  value="<s:property value="crmProduct.createUser.id"/>" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
               	 	
                   	<g:LayOutWinTag buttonId="showunit" title="选择单位"
			     hasHtmlFrame="true" html="${ctx}/dic/currency/currencyTypeSelect.action"
			     isHasSubmit="false" 	functionName="bz" hasSetFun="true"
			     documentId="crmProduct_unit"
				 documentName="crmProduct_unit_name" 
			/>
               	 
                   	<td class="label-title">标准价格</td>
                   	<td align="left">
                   	<input type="text" size="10" maxlength="20" id="crmProduct_standardPrice" name="crmProduct.standardPrice" title="标准价格" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.standardPrice"/>" 
                   	 />
                   	 
                   	</td>
                   		<td class="label-title">币种</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmProduct_unit_name"  value="<s:property value="crmProduct.currencyType.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_unit" name="crmProduct.currencyType.id"  value="<s:property value="crmProduct.currencyType.id"/>" > 
 						<img alt='选择单位' id='showunit' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			<%-- <g:LayOutWinTag buttonId="showkind" title="选择类别"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showkindFun" hasSetFun="true"
				documentId="crmProduct_kind"
				documentName="crmProduct_kind_name"
			/>
               	 	<td class="label-title">类别</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmProduct_kind_name"  value="<s:property value="crmProduct.kind.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_kind" name="crmProduct.kind.id"  value="<s:property value="crmProduct.kind.id"/>" > 
 						<img alt='选择类别' id='showkind' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
				</tr>
				<tr>
			<%-- <g:LayOutWinTag buttonId="showdutyDept" title="选择负责部门" 
			hasHtmlFrame="true" html="${ctx}/core/department/departmentSelect.action"
			isHasSubmit="false" functionName="showdutyDeptFun" hasSetFun="true"
			documentId="crmProduct_dutyDept"
			documentName="crmProduct_dutyDept_name"  />
               	 	<td class="label-title">负责部门</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmProduct_dutyDept_name"  value="<s:property value="crmProduct.dutyDept.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_dutyDept" name="crmProduct.dutyDept.id"  value="<s:property value="crmProduct.dutyDept.id"/>" > 
 						<img alt='选择负责部门' id='showdutyDept' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
			<%-- <g:LayOutWinTag buttonId="showstate" title="选择状态"
				hasHtmlFrame="true"
				html="${ctx}/dic/state/stateSelect.action?type=commonState"
				isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
				documentId="crmProduct_state"
				documentName="crmProduct_state_name" />
               	 	<td class="label-title">状态</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmProduct_state_name"  value="<s:property value="crmProduct.state.name"/>" class="text input"/>
 						<input type="hidden" id="crmProduct_state" name="crmProduct.state.id"  value="<s:property value="crmProduct.state.id"/>" > 
 						<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
               	 	<%-- <td class="label-title">创建日期</td>
                   	<td align="left">
                   	<input type="text" size="12" maxlength="20" id="crmProduct_createDate" name="crmProduct.createDate" title="创建日期" 
                   		 	readonly = "readOnly" 
                   		 	class="text input readonlytrue"  
                   		value="<s:date name="crmProduct.createDate" format="yyyy-MM-dd"/>"
                   	 />
                   	</td> --%>
                  
                   	
                   		
			
				<g:LayOutWinTag buttonId="showDicState" title="选择状态"
		hasHtmlFrame="true"
		html="${ctx}/dic/state/stateSelect.action?type=storage"
		isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true"
		documentId="crmProduct_state_id" documentName="crmProduct_state_name" />
			
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="状态" class="text label"> 状态 </label>
												</td>
											
												<td nowrap>
													<input type="text"
														onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)"
														class="input_parts text input default  readonlyfalse false"
														name="" id="crmProduct_state_name"
														readonly="readonly"
														value="<s:property value="crmProduct.state.name"/>" size="20" />
														<img alt='选择' id='showDicState'
														src='${ctx}/images/img_lookup.gif' class='detail' />
													  <input
														type="hidden" name="crmProduct.state.id" id="crmProduct_state_id"
														value="<s:property value="crmProduct.state.id"/>" />
													</td>
												<td nowrap>&nbsp;</td>
											
                   	
				</tr>
			<tr style="display:none">	

               	 	<td class="label-title">报告编码</td>
                   	<td align="left">
 						<input type="text" size="15"  id="crmProduct_code" name="crmProduct.code" value="<s:property value="crmProduct.code"/>" class="text input"/>
                   	</td>
               	    <td class="label-title">排序</td>
                   	<td align="left">
                   	<input type="text" size="10" maxlength="20" id="crmProduct_referPrice" name="crmProduct.referPrice" title="参考价格" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.referPrice"/>" 
                   	 />
                   	</td> 

                  
                   	                   	<td class="label-title">合同文件版本</td>
						<td title="保存基本后,可以维护查看附件" id="crmProduct_label"><span id ="doclinks_img"
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
							
							
							</td>
							
							
				
				
							
				</tr>
				<tr style="display:none">		 
					<td class="label-title">是否启动检测</td>
					<td>
						
							<select name="crmProduct.feeType" id="crmProduct_feeType" style="width:120px">
								
								<option value="0" <s:if test="crmProduct.feeType==0">selected="selected" </s:if>>付费后启动检测</option>
								<option value="1" <s:if test="crmProduct.feeType==1">selected="selected" </s:if>>直接检测</option>
								
							</select> <img class='requiredimage'
						src='${ctx}/images/required.gif' /></td>
				
				
					<!-- td class="label-title">报告模板文件</td>
					<td><s:hidden 	id="crmProduct_path" name="crmProduct.path.id"></s:hidden>
					
					<input type="text" size="15"  id="crmProduct_path_name" name=""  value="<s:property value="crmProduct.path.fileName"/>" class="text input readonlytrue" readonly="readOnly"/>
					
					    <input type="button"
						onclick="uploadFile()" value="上传"> <input type="button"
						onclick="downFile()" value="查看"></td-->
				</tr>
				<tr style="display:none">	
						<td colspan="6">
							<div class="standard-section-header type-title">
								<label>定期沟通提醒设置时间</label>
								
							</div>
						</td>
					</tr>
				
					<tr id="notOverSeas" style="display:none"	<s:if
		test='crmProduct.type.id=="jccp"'>
		
		
		</s:if>
				<s:if
		test='crmProduct.type==null||crmProduct.type.id!="jccp"'>
		style="display:none"
		
		</s:if>>
					 	<td class="label-title">检测中</td>
                   	<td align="left">
                   	<input type="hidden" size="20" maxlength="36" id="crmProduct_detectionIn" name="crmProduct.detectionIn" title="检测中" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.detectionIn"/>" 
                   	 />
                   	<input type="hidden" size="10" maxlength="36" id="crmProduct_detectionIn_text" name="crmProduct.detectionIn.text" title="检测中" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.detectionIn.text"/>" 
                   	 />
                   	 <input type="radio" id="crmProduct_detectionIn_radio" name="crmProduct.detectionIn.radio" value="" checked>无 
                   	 <input type="radio" id="crmProduct_detectionIn_radio" name="crmProduct.detectionIn.radio" value="zhou">每周  
                     <input type="radio" id="crmProduct_detectionIn_radio" name="crmProduct.detectionIn.radio" value="yue">每月
                   	</td>
               	 	
               	 		<td class="label-title">报告后</td>
                   	<td align="left">
                   	<input type="hidden" size="20" maxlength="36" id="crmProduct_reportBack" name="crmProduct.reportBack" title="报告后" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.reportBack"/>" 
                   	 />
                   	<input type="hidden" size="10" maxlength="36" id="crmProduct_reportBack_text" name="crmProduct.reportBack.text" title="报告后" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.reportBack.text"/>" 
                   	 />
                   	                    	 <input type="radio" id="crmProduct_reportBack_radio" name="crmProduct.reportBack.radio" value="" checked>无
                   	 <input type="radio" id="crmProduct_reportBack_radio" name="crmProduct.reportBack.radio" value="zhou">每周  
                     <input type="radio" id="crmProduct_reportBack_radio" name="crmProduct.reportBack.radio" value="yue">每月
                                          <input type="radio" id="crmProduct_reportBack_radio" name="crmProduct.reportBack.radio" value="byear">半年
                                                               <input type="radio" id="crmProduct_reportBack_radio" name="crmProduct.reportBack.radio" value="oneyear">一年
                   	</td>
                   	<td class="label-title">科研</td>
                   	<td align="left">
                   	<input type="hidden" size="20" maxlength="36" id="crmProduct_science" name="crmProduct.science" title="科研" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.science"/>" 
                   	 />
                   	<input type="hidden" size="10" maxlength="36" id="crmProduct_science_text" name="crmProduct.science.text" title="科研" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.detectionIn.text"/>" 
                   	 />
                   	 <input type="radio" id="crmProduct_science_radio" name="crmProduct.science.radio" value="" checked>无  
                   	 <input type="radio" id="crmProduct_science_radio" name="crmProduct.science.radio" value="zhou">每周  
                     <input type="radio" id="crmProduct_science_radio" name="crmProduct.science.radio" value="yue">每月
                   	</td></div>
					</tr>
					
					<tr id="isOverSeas" 
					<s:if
		test='crmProduct.type.id=="hwcp"'>
		
		
		</s:if>
				<s:if
		test='crmProduct.type==null||crmProduct.type.id!="hwcp"'>
		style="display:none"
		
		</s:if>
					>
					<td class="label-title">海外归来</td>
                   	<td align="left">
                   	<input type="hidden" size="20" maxlength="36" id="crmProduct_overseasReturn" name="crmProduct.overseasReturn" title="海外归来" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.overseasReturn"/>" 
                   	 />
                   	<input type="hidden" size="10" maxlength="36" id="crmProduct_overseasReturn_text" name="crmProduct.overseasReturn.text" title="海外归来" 
                   		 	class="text input"
                   		 value="<s:property value="crmProduct.overseasReturn.text"/>" 
                   	 />
                   	                    	 <input type="radio" id="crmProduct_overseasReturn_radio" name="crmProduct.overseasReturn.radio" value="" checked>无  
                   	 <input type="radio" id="crmProduct_overseasReturn_radio" name="crmProduct.overseasReturn.radio" value="zhou">每周  
                     <input type="radio" id="crmProduct_overseasReturn_radio" name="crmProduct.overseasReturn.radio" value="yue">每月
                   	</td>
					</tr>
					
						<tr style="display:none">
						<td colspan="6">
							<div class="standard-section-header type-title">
								<label>检测过程跟踪邮件接收人</label>
							</div>
						</td>
					</tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="crmProduct.id"/>" />

            <div id="tabs">
            <ul>
            <li><a href="#crmProductEmailpage">项目检测邮件提示接收人</a></li>
			<li><a href="#crmPatientEmailpage">检测个体检测邮件提示接收人</a></li>
           	</ul> 
           		<div id="crmProductEmailpage" style="width: 100%; height: 10px"></div>
           			<div id="crmPatientEmailpage" style="width: 100%; height: 10px"></div>
			</div>
			<input type="hidden" name="crmProductEmailJson"
				id="crmProductEmailJson" value="" />
				<input type="hidden" name="crmPatientEmailJson"
				id="crmPatientEmailJson" value="" />
			            </form>
        	</div>
	</body>
	</html>
