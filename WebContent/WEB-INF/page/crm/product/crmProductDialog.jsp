﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/product/crmProductDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<input type="hidden" id="WKSelectItemId" value="${requestScope.WKSelectItemId}">
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">分类</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">负责人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_dutyUser" searchField="true" name="dutyUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="crmProduct_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">类别</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_kind" searchField="true" name="kind"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">负责部门</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_dutyDept" searchField="true" name="dutyDept"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmProduct_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">上级产品</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_upProductId" searchField="true" name="upProductId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">参考价格</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmProduct_referPrice" searchField="true" name="referPrice"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">标准价格</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmProduct_standardPrice" searchField="true" name="standardPrice"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">单位</td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="crmProduct_unit" searchField="true" name="unit"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">单位成本</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="crmProduct_cost" searchField="true" name="cost"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_crmProduct_div"></div>
   		
</body>
</html>



