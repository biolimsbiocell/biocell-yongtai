﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/crm/product/crmProduct.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table>
			<tr>
               		<td class="label-title">编码</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="36" searchField="true" id="crmProduct_id" name="id" title="编码" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			<g:LayOutWinTag buttonId="showtype" title="选择分类"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
				documentId="crmProduct_type"
				documentName="crmProduct_type_name"
			/>
               		<td class="label-title">分类</td>
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="crmProduct_type_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_type" name="type.id"  value="" searchField="true" > 
 						<img alt='选择分类' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			
			</tr>
			<tr>
			<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="crmProduct_createUser"
				documentName="crmProduct_createUser_name"
			 />
               		<td class="label-title">创建人</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmProduct_createUser_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_createUser" name="createUser.id"  value="" searchField="true" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
                   	<g:LayOutWinTag buttonId="showdutyUser" title="选择负责人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showdutyUserFun" 
				documentId="crmProduct_dutyUser"
				documentName="crmProduct_dutyUser_name"
			 />
               		<td class="label-title">负责人</td>
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="crmProduct_dutyUser_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_dutyUser" name="dutyUser.id"  value="" searchField="true" > 
 						<img alt='选择负责人' id='showdutyUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
               		<td class="label-title">名称</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="50" searchField="true" id="crmProduct_name" name="name" title="描述" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showkind" title="选择类别"
				hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="showkindFun" hasSetFun="true"
				documentId="crmProduct_kind"
				documentName="crmProduct_kind_name"
			/>
               		<td class="label-title">类别</td>
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="crmProduct_kind_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_kind" name="kind.id"  value="" searchField="true" > 
 						<img alt='选择类别' id='showkind' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			<%-- <g:LayOutWinTag buttonId="showdutyDept" title="选择{po.filedComment}" 
			hasHtmlFrame="true" html="${ctx}/core/department/departmentSelect.action"
			isHasSubmit="false" functionName="showdutyDeptFun" hasSetFun="true"
			documentId="crmProduct_dutyDept"
			documentName="crmProduct_dutyDept_name"  />
               		<td class="label-title">负责部门</td>
                   	<td align="left">
 						<input type="text" size="10" readonly="readOnly"  id="crmProduct_dutyDept_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_dutyDept" name="dutyDept.id"  value="" searchField="true" > 
 						<img alt='选择负责部门' id='showdutyDept' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
			<%-- <g:LayOutWinTag buttonId="showstate" title="选择状态"
				hasHtmlFrame="true"
				html="${ctx}/dic/state/stateSelect.action?type=commonState"
				isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
				documentId="crmProduct_state"
				documentName="crmProduct_state_name" />
               		<td class="label-title">状态</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmProduct_state_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_state" name="state.id"  value="" searchField="true" > 
 						<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
               		<!-- <td class="label-title">创建日期</td>
                   	<td align="left">
                   	<input type="text" size="12" maxlength="20" searchField="true" id="crmProduct_createDate" name="createDate" title="创建日期" value="" 
                    	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	 />
                   	</td> -->
                   	<td class="label-title">标准价格</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="20" searchField="true" id="crmProduct_standardPrice" name="standardPrice" title="标准价格" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
			</tr>
			<tr>
			<%-- <g:LayOutWinTag buttonId="showupProductId" title="选择上级产品"
				hasHtmlFrame="true"
				html="${ctx}/crm/service/product/showDialogCrmProductList.action"
				isHasSubmit="false" functionName="CrmProductFun" hasSetFun="true"
				documentId="crmProduct_upProductId"
				documentName="crmProduct_upProductId_name" />
               		<td class="label-title">上级产品</td>
                   	<td align="left">
 						<input type="text" size="15" readonly="readOnly"  id="crmProduct_upProductId_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_upProductId" name="upProductId.id"  value="" searchField="true" > 
 						<img alt='选择上级产品' id='showupProductId' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
               		<!-- <td class="label-title">参考价格</td>
                   	<td align="left">
                   	<input type="text" size="10" maxlength="20" searchField="true" id="crmProduct_referPrice" name="referPrice" title="参考价格" value="" 
                   		 	class="text input"
                   	 />
                   	</td> -->
               		
                   	<g:LayOutWinTag buttonId="showunit" title="选择单位"
			     hasHtmlFrame="true" html="${ctx}/dic/unit/dicUnitSelect.action"
			     isHasSubmit="false" 	functionName="showunitFun" hasSetFun="true"
			     documentId="crmProduct_unit"
				 documentName="crmProduct_unit_name" 
			/>
               		<td class="label-title">币种单位</td>
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="crmProduct_unit_name"  value="" class="text input"/>
 						<input type="hidden" id="crmProduct_unit" name="unit.id"  value="" searchField="true" > 
 						<img alt='选择单位' id='showunit' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
			</tr>
			<tr>
			
               		<!-- <td class="label-title">单位成本</td>
                   	<td align="left">
                   	<input type="text" size="10" maxlength="20" searchField="true" id="crmProduct_cost" name="cost" title="单位成本" value="" 
                   		 	class="text input"
                   	 />
                   	</td> -->
			</tr>
        </table>
		</form>
		</div>
		<div id="show_crmProduct_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
</body>
</html>



