<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript">
	 <%
		String setFunName = "";
		String projectId = "";
		if (request.getParameter("flag") != null && !request.getParameter("flag").equals("")) {
			setFunName = request.getParameter("flag");
		}
	%>
	 function search(){
		 var data = "{\"id\": \""+document.getElementById("project_id").value+"\",\"name\":\""+document.getElementById("project_name").value+"\"}";
			var o = {start:0, limit:20,data: data};
	        gridGrid.store.load({params:o});
	      //  document.getElementById("selectButton").select();
		}
	 function gotovalue(){
			if(gridGrid.store.getCount()==1){		
				window.parent.set<%=setFunName%>(gridGrid.store.getAt(0));
			}else{
		}
	} 
	function checkKey(){
		if(event.keyCode == 13)
			search();
	} 

	function newButton(){
		window.open(window.ctx + '/crm/project/toEditProject.action','','');
	}
</script>
</head>
<body>
<%--   <input type="hidden" name="crmCus" id="crmCus" value="<%=request.getParameter("crmCus") %>" /> --%>

	<div class="mainfullclass" id="markup">
		<table>
			<tr>
				<td>
					<label title="<fmt:message key="biolims.common.itemCode"/>" class="text label"><fmt:message key="biolims.common.itemCode"/></label>
					<input type="text" class="input_parts text input default  false false" name="project.id" id="project_id" title="<fmt:message key="biolims.common.itemCode"/>" onkeydown="checkKey(this);" onblur="textbox_ondeactivate(event);" size="10" maxlength="32"></input>
					<input id="selectButton" type="button" value="<fmt:message key="biolims.common.selected"/>" onclick="javascript:gotovalue();"></input><!-- onchange="search()"-->
				</td>
				<td>
					<label title="<fmt:message key="biolims.common.projectName"/>" class="text label"><fmt:message key="biolims.common.projectName"/></label>
					<input type="text" class="input_parts text input default  false false" name="project.name" id="project_name" title="<fmt:message key="biolims.common.projectName"/>" onkeydown="checkKey(this);" onblur="textbox_ondeactivate(event);" size="10" maxlength="32"></input>
					<img alt='<fmt:message key="biolims.common.retrieve"/>' id="searchProject" src='${ctx}/images/img_lookup.gif' onclick="search();" class='detail' />
				</td>
				<td>
					<input id="newButton" type="button" value=<fmt:message key="biolims.common.create"/> onclick="javascript:newButton();">
				</td>
			</tr>
		</table>
		<input type="hidden" id="id" value="" />
		<script language = "javascript" >
function gridSetSelectionValue(){
var r = gridGrid.getSelectionModel().getSelected(); 
UserFun(r);
}
var gridGrid;
Ext.onReady(function(){
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'mainProject-id',type: 'string'},{name:'mainProject-name',type: 'string'},{name:'crmDoctor-id',type: 'string'},{name:'crmDoctor-name',type: 'string'},{name:'crmlinkMan-id',type: 'string'},{name:'crmlinkMan-name',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: ctx+'/crm/project/showProjectListJson.action?crmCus=',method: 'POST'})
});
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
height:document.body.clientHeight,
title:biolims.master.projectManagement,
store: store,
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:[new Ext.grid.RowNumberer(),{dataIndex:'id',header: biolims.common.projectId,width: 120,sortable: true},{dataIndex:'name',header: biolims.common.projectName,width: 150,sortable: true},{dataIndex:'mainProject-id',header: biolims.sample.customerId,width: 120,sortable: true},{dataIndex:'mainProject-name',header: biolims.common.custom,width: 150,sortable: true},{dataIndex:'crmDoctor-id',header:biolims.sample.crmDoctorId,width: 120,sortable: true},{dataIndex:'crmDoctor-name',header: biolims.sample.crmDoctorName,width: 150,sortable: true},{dataIndex:'crmlinkMan-id',header:biolims.crm.crmlinkManNameId,width: 120,sortable: true},{dataIndex:'crmlinkMan-name',header: biolims.crm.crmlinkManName,width: 150,sortable: true}],
stripeRows: true,
bbar: new Ext.PagingToolbar({
pageSize: parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1),
store: store,
displayInfo: true,
displayMsg: biolims.common.displayMsg,
beforePageText:biolims.common.page,
afterPageText: biolims.common.afterPageText,
emptyMsg: biolims.common.noData
})
});
gridGrid.render('grid');
gridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
});
gridGrid.on('rowdblclick',function(){gridSetSelectionValue();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1)}});
});
//function UserFun(rec){
//window.parent.setUserFun(rec);
//}
function UserFun(id,name){
 	<%if(request.getParameter("flag")!=null){%>
 		window.parent.set<%=request.getParameter("flag")%>(id,name);
  	<%}%>
	}
</script >
<input type='hidden'  id='extJsonDataString'  name='extJsonDataString' value=''/>
<div id='grid' ></div>

	</div>

</body>
</html>