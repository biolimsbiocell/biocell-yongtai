<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>

<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message
																	key="biolims.common.projectManagement" />
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.itemCode" />
																	<img class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
									
										
                   						<input type="text"
											size="50" maxlength="50" id="project_id"
											name="project.id"  changelog="<s:property value="project.id"/>"
											  class="form-control"   
	value="<s:property value="project.id"/>"
						 />  
					
									</div>
								</div>		
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.projectName"/><img
															class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
									
										<input type="text" size="20" name="project.name"
												id="project_name"  changelog="<s:property value="project.name"/>"
												value="<s:property value="project.name"/>"
												class="form-control" /> 
												
										
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.type"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="project_type_name"  changelog="<s:property value="project.type.name"/>"
												value="<s:property value="project.type.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="project_type_id" name="project.type.id"
												value="<s:property value="project.type.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showXMLX()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.projectLeader"/></span> 
									
										<input type="hidden"
											size="20" maxlength="25" id="project_dutyUser_id"
											name="project.dutyUser.id"  changelog="<s:property value="project.dutyUser.id"/>"
											  class="form-control"   
	value="<s:property value="project.dutyUser.id"/>"
						 />  
                   						<input type="text" id="project_dutyUser_name"
											size="20" maxlength="25" readonly="readOnly"
											name="project.dutyUser.name"  changelog="<s:property value="project.dutyUser.name"/>"
											  class="form-control"   
	value="<s:property value="project.dutyUser.name"/>"
						 />  
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showconfirmUser()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.selectInstitutions"/></span> 
									
										
                   							<input type="hidden" id="project_mainProject_id" 
                   							name="project.mainProject.id" value="<s:property value="project.mainProject.id"/>">
											<input type="text" size="50" maxlength="127"  
											readOnly="readOnly" id="project_mainProject_name" 
											changelog="<s:property value="project.mainProject.name"/>"  
											 class="form-control" value="<s:property value="project.mainProject.name"/>" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showHos()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
					
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.selectTheDoctor" /></span> 
									
										
                   						<input type="text"  id="project_crmDoctor_name"
											size="20" maxlength="25" 
											readOnly="readOnly"
											  changelog="<s:property value="project.crmDoctor.name"/>"
											  class="form-control"   
	value="<s:property value="project.crmDoctor.name"/>"
						 />  
						 				<input type="hidden"
											size="20" maxlength="25" id="project_crmDoctor_id"
											name="project.crmDoctor.id"  changelog="<s:property value="project.crmDoctor.id"/>"
											  class="form-control"   
	value="<s:property value="project.crmDoctor.id"/>"
						 />  
						 					<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showDoctors()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
					
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.startTime"/></span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="project_planStartDate"
											name="project.planStartDate"  changelog="<s:date name="project.planStartDate" format="yyyy-MM-dd "/>"
											  class="form-control"   
	class="Wdate" value="<s:date name="project.planStartDate" format="yyyy-MM-dd "/>"
						 />  
					
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.expectedDeliveryDate" /> </span> 
									
										<input type="text" size="20" name="project.factStartDate"
												id="project_factStartDate"  
												changelog="<s:date name="project.factStartDate" format="yyyy-MM-dd "/>"
												class="form-control" 
												class="Wdate" value="<s:date name="project.factStartDate" format="yyyy-MM-dd "/>" /> 
												
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.actualDeliveryDate"/> </span> 
									
										<input type="text" size="20" 
												name="project.planEndDate"
												id="project_planEndDate"  changelog="<s:date name="project.planEndDate" format="yyyy-MM-dd "/>"
												class="form-control" 
												class="Wdate" value="<s:date name="project.planEndDate" format="yyyy-MM-dd "/>"/> 
												
										
									</div>
								</div>				
									</div>
										<div class="row">
							<%-- 	<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.crmCustomer.createUser"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="crmCustomer_createUser_name"  changelog="<s:property value="crmCustomer.createUser.name"/>"
												value="<s:property value="crmCustomer.createUser.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="crmCustomer_createUser_id" name="createUser-id"
												value="<s:property value="crmCustomer.createUser.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showcreateUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>	 --%>			
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.crmCustomer.createDate"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="crmCustomer_createDate"
											name="createDate"  changelog="<s:property value="crmCustomer.createDate.name"/>"
											  class="form-control"   
                   	  value="<s:date name="createDate" format="yyyy-MM-dd"/>"
                   	   						 />  
					
									</div>
								</div>	 --%>			
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.returnRatio"></fmt:message></span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="project_sumScale"
											name="project.sumScale"  changelog="<s:property value="project.sumScale"/>"
											  class="form-control"   
	value="<s:property value="project.sumScale"/>"
						 />  
					
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.state"/></span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" 
											id="project_state_name"  changelog="<s:property value="project.state.name"/>"
											  class="form-control"   
	value="<s:property value="project.state.name"/>"
						 />  
						 				<input type="hidden"
						 				readOnly="readOnly"
											size="20" maxlength="25" id="project_state_id"
											name="project.state.id"  changelog="<s:property value="project.state.id"/>"
											  class="form-control"   
	value="<s:property value="project.state.id"/>"
						 />  
						 					<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showZT()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
					
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
								</div>	
								</div>
								<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.sales"></fmt:message></span> 
									
										
                   						<input type="hidden"
											size="20" maxlength="25" id="project_person_id"
											name="project.person.id"  changelog="<s:property value="project.person.id"/>"
											  class="form-control"   
	value="<s:property value="project.person.id"/>"
						 />  
						 					<input type="text"
						 					readOnly="readOnly"
											size="20" maxlength="25" id="project_person_name"
											name="project.person.name"  changelog="<s:property value="project.person.name"/>"
											  class="form-control"   
	value="<s:property value="project.person.name"/>"
						 />  
						 					<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showXS()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
					
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.common.itemAmount"></fmt:message></span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="project_money"
											name="project.money"  changelog="<s:property value="project.money"/>"
											  class="form-control"   
	value="<s:property value="project.money"/>"
						 />  
					
									</div>
								</div>					
									</div>
								<div class="row">
									<div class="col-md-8 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message
																	key="biolims.common.projectDescription" /></span> 
									
										
                   						<textarea 
											 id="project_note"
											name="project.note" 
											  class="form-control"   
											  style="overflow: hidden; width: 400px; height: 100px;"
						 ><s:property value="project.note"/></textarea>  
					
									</div>
									</div>
								</div>
								<div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> <fmt:message key="biolims.common.customFields" />
													
											</h3>
										</div>
									</div>
								</div>
							<div id="fieldItemDiv"></div>
								
							<br> 
							<input type="hidden" name="projectJson"
								id="projectJson" value="" /> 
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="project.id"/>" />
							<input type="hidden" id="fieldContent" name="project.fieldContent" value="<s:property value="project.fieldContent"/>" />	
							<input type="hidden" id="changeLog" name="changeLog"  />
						</form>
					</div>
					<!--table表格-->
<!-- 					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="projectTable" style="font-size: 14px;">
						</table>
					</div> -->
					</div>
<!-- 				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div>
 -->			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/crm/project/editProject.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>	
