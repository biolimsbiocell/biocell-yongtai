<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>



<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>


</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
	<script type="text/javascript" src="${ctx}/js/crm/project/showProjectList.js"></script>


	
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>

					<td  valign="top" align="right" ><span>&nbsp;</span> <label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.itemCode"/></label></td>

					<td nowrap class="quicksearch_control" valign="middle"><input type="text"
						class="input_parts text input default  readonlyfalse false" name="id" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" searchField="true" id="id" title="<fmt:message key="biolims.common.inputID"/>" ></td>
					<td  valign="top" align="right" ><span>&nbsp;</span> <label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.projectName"/></label></td>

					<td nowrap class="quicksearch_control" valign="middle"><input type="text"
						class="input_parts text input default  readonlyfalse false" name="name" onblur="textbox_ondeactivate(event);"
						searchField="true" onfocus="shared_onfocus(event,this)" id="name" title="<fmt:message key="biolims.common.inputName"/>" ></td>

				</tr>
				<tr>
                   <td  valign="top" align="right" ><span>&nbsp;</span> <label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.selectTheDoctor"/></label></td>

					<td nowrap class="quicksearch_control" valign="middle"><input type="text"
											class="input_parts text input default  readonlyfalse false"
											name="crmDoctor.name" 
											id=""
											searchField="true"
											value=''
											title="<fmt:message key="biolims.common.selectTheCustomer"/> " onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
											<input type="hidden" id="project_mainProject_id"
											name="project.mainProject.id"
											value='<s:property value="project.mainProject.id"/>'> </td>
					
					<td  valign="top" align="right" ><span>&nbsp;</span> <label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.projectType"/></label></td>

					<td nowrap class="quicksearch_control" valign="middle"><input type="text"
						class="input_parts text input default  readonlyfalse false" name="" searchField="true" id="type_name" title="<fmt:message key="biolims.common.inputType"/>" > <input type="hidden"
						name="type.id" id="type_id" searchField="true"></input> </td>
				</tr>
				<tr>
				<td  valign="top" align="right" ><span>&nbsp;</span> <label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.selectLeader"/></label></td>

					<td nowrap class="quicksearch_control" valign="middle"><input type="text"
											class="input_parts text input default  readonlyfalse false"
											name="project.dutyUser.name" searchField="true"
											id="project.dutyUser.name"
											
											value='<s:property value="project.dutyUser.name"/>'
											title="<fmt:message key="biolims.common.projectManager"/> " onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
											<input type="hidden" id="project.dutyUser.id"
											name="dutyUser.id"
											searchField="true"
											value='<s:property value="project.dutyUser.id"/>'>
			
											<td  valign="top" align="right" ><span>&nbsp;</span> <label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.projectDescription"/></label></td> 

					<td nowrap class="quicksearch_control" valign="middle"><input type="text"
											class="input_parts text input default  readonlyfalse false"
											name="note" id="note"
											searchField="true"
											value="<s:property value="note"/>" title="<fmt:message key="biolims.common.projectDescription"/>  "
											size="20" maxlength="1000"> 
				
				</td>
				</tr>
			</table>
			<div id="search_show_div"></div>
		</div>

	<s:hidden id="rightsId" name="rightsId"></s:hidden>

		<input type="hidden" id="id" value="" />
		<script language = "javascript" >
		
		
</script >
<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
<input type='hidden' id='extJsonDataString' name='extJsonDataString' value=''/>
<div id='grid'  ></div>
<div id='gridcontainer'  ></div>

	</div>

</body>
</html>