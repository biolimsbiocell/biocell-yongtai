<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>biolims</title>
<style>
</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/crm/project/editProject.js"></script>

<s:if test='#request.handlemethod!="view"'>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchcontract = Ext.get('searchcontract');
searchcontract.on('click', 
 function searchcontractFun(){
var win = Ext.getCmp('searchcontractFun');
if (win) {win.close();}
var searchcontractFun= new Ext.Window({
id:'searchcontractFun',modal:true,title:'<fmt:message key="biolims.common.selectTheContract"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/contract/common/contractSelect.action?flag=searchcontractFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 searchcontractFun.close(); }  }]  });     searchcontractFun.show(); }
);
});
 function setsearchcontractFun(rec){
document.getElementById('project_contract_id').value=rec.get('id');
var win = Ext.getCmp('searchcontractFun')
if(win){win.close();}
}
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchfzr = Ext.get('searchfzr');
searchfzr.on('click', 
 function fzr(){
var win = Ext.getCmp('fzr');
if (win) {win.close();}
var fzr= new Ext.Window({
id:'fzr',modal:true,title:'<fmt:message key="biolims.common.selectLeader"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/common/showUserList.action?flag=fzr' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 fzr.close(); }  }]  });     fzr.show(); }
);
});
 function setfzr(id,name){
 document.getElementById("project_dutyUser_id").value = id;
document.getElementById("project_dutyUser_name").value = name;
var win = Ext.getCmp('fzr')
if(win){win.close();}
}
</script>



	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchkhlxr = Ext.get('searchkhlxr');
searchkhlxr.on('click', 
 function CrmLinkManFun(){
var win = Ext.getCmp('CrmLinkManFun');
if (win) {win.close();}
var CrmLinkManFun= new Ext.Window({
id:'CrmLinkManFun',modal:true,title:'<fmt:message key="biolims.common.chooseTheCustomerContact"/>',layout:'fit',width:600,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/customer/linkman/crmLinkManSelect.action?id=&flag=CrmLinkManFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 CrmLinkManFun.close(); }  }]  });     CrmLinkManFun.show(); }
);
});
 function setCrmLinkManFun(rec){
document.getElementById('project_crmlinkMan_id').value=rec.get('id');document.getElementById('project_crmlinkMan_name').value=rec.get('name');
var win = Ext.getCmp('CrmLinkManFun')
if(win){win.close();}
}
</script>





	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchtype = Ext.get('searchtype');
searchtype.on('click', 
 function xmlx(){
var win = Ext.getCmp('xmlx');
if (win) {win.close();}
var xmlx= new Ext.Window({
id:'xmlx',modal:true,title:'<fmt:message key="biolims.common.selectTheProjectType"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=xmlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 xmlx.close(); }  }]  });     xmlx.show(); }
);
});
 function setxmlx(id,name){
 document.getElementById("project_type_id").value = id;
document.getElementById("project_type_name").value = name;
var win = Ext.getCmp('xmlx')
if(win){win.close();}
}
</script>



	<%-- <%if(request.getAttribute("project.projectTemplate.id")==null) {%>
<g:LayOutWinTag buttonId="searchMb" title="选择项目模板" width="900" height="530" hasHtmlFrame="true" 	html="${ctx}/common/showProjectTemplateList.action"	
 isHasSubmit="false" functionName="searchmainprojectFun"  hasSetFun="true" extRec="rec" extStr="document.getElementById('project.projectTemplate.id').value=rec.get('id');"/>	
<%} %> --%>

</s:if>

<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var projectNote = Ext.get('projectNote');
projectNote.on('click', 
 function projectNoteFun(){
var win = Ext.getCmp('projectNoteFun');
if (win) {win.close();}
var projectNoteFun= new Ext.Window({
id:'projectNoteFun',modal:true,title:'<fmt:message key="biolims.common.detailed"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.form.TextArea({id:'projectNote1',value:document.getElementById('project_note').value}),
buttons: [
{text:biolims.common.selected,
handler: function(){
document.getElementById('project_note').value=document.getElementById('projectNote1').value;Ext.getCmp('projectNoteFun').close(); } },
{ text: biolims.common.close,
 handler: function(){
 projectNoteFun.close(); }  }]  });     projectNoteFun.show(); }
);
});
</script>

<script language="javascript">
	function workflowSubmit() {
		commonWorkflowSubmit("applicationTypeTableId=project&formId=${project.id}&title="
				+ encodeURI(encodeURI('${project.name}')));
	}
	function workflowLook() {
		commonWorkflowLook("applicationTypeTableId=project&formId=${project.id}");
	}
	function workflowView() {
		commonWorkflowView("applicationTypeTableId=project&formId=${project.id}");
	}
	function changeState() {
		commonChangeState("formId=${project.id}&tableId=project");
	}
</script>



<s:if
	test='#request.handlemethod=="modify"||#request.handlemethod=="view"'>
	<g:LayOutWinTag buttonId="doclinks_img"
		title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="1000" height="600"
		html="${ctx}/operfile/initFileList.action?canDelete=true&modelType=project&id=${project.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>

<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showDicState = Ext.get('showDicState');
showDicState.on('click', 
 function showDicStateFun(){
var win = Ext.getCmp('showDicStateFun');
if (win) {win.close();}
var showDicStateFun= new Ext.Window({
id:'showDicStateFun',modal:true,title:'<fmt:message key="biolims.common.selectTheState"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/state/stateSelect.action?type=project&flag=showDicStateFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 showDicStateFun.close(); }  }]  });     showDicStateFun.show(); }
);
});
 function setshowDicStateFun(id,name){
 document.getElementById("project_state_id").value = id;
document.getElementById("project_state_name").value = name;
var win = Ext.getCmp('showDicStateFun')
if(win){win.close();}
}
</script>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var dutyNameBtn = Ext.get('dutyNameBtn');
dutyNameBtn.on('click', 
 function dutyUserFun(){
var win = Ext.getCmp('dutyUserFun');
if (win) {win.close();}
var dutyUserFun= new Ext.Window({
id:'dutyUserFun',modal:true,title:'<fmt:message key="biolims.common.selectLeader"/>',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=dutyUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 dutyUserFun.close(); }  }]  });     dutyUserFun.show(); }
);
});
 function setdutyUserFun(id,name){
 document.getElementById("project.dutyUser.id").value = id;
document.getElementById("project.dutyUser.name").value = name;
var win = Ext.getCmp('dutyUserFun')
if(win){win.close();}
}
</script>

<body>
	<script type="text/javascript">
		//decodeURI()和encodeURI()

		Ext.onReady(function() {
			var tabs = new Ext.TabPanel({
				id : 'tabs',
				renderTo : 'maintab',
				height : document.body.clientHeight - 30,
				autoWidth : true,
				activeTab : 0,
				margins : '0 0 0 0',
				items : [ {
					title : biolims.master.projectMessage,
					contentEl : 'markup'

				} ]
			});
			tabs.on('beforetabchange', function() {
	<%if (request.getParameter("id") == null) {%>
		MsgInfo(biolims.master.pleaseSaveBasic, '', 300);
				//	return alertConfirm('dssds');
				//	save();
				return false;
	<%}%>
		});
		});

		function sjmx(projectTaskId) {
			Ext
					.onReady(function() {
						Ext.getCmp('tabs').setActiveTab(2);
						window.frames['tab13frame'].location.href = '${ctx}/project/project/showProjectFactItemList.action?projectId=${project.id}&handlemethod=${handlemethod}&projectTaskId='
								+ projectTaskId;
					});
		}
	</script>
	<s:if
		test='#request.handlemethod=="modify"||#request.handlemethod=="add"'>
		<script type="text/javascript"
			src="${ctx}/javascript/common/handleVaLeave.js"></script>
	</s:if>

	<%
		request.setAttribute("tableTypeId", "project");

		request.setAttribute("contentId",
				request.getAttribute("project.id"));
	%>

	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<div id="maintab" style="margin: 0 0 0 0"></div>


	<div id="markup" class="mainclass">
		<s:form name="form1" theme="simple">
			<table width="100%" class="section_table" cellpadding="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0"
								cellspacing="0">
								<tbody>
									<tr>
										<td>
											<table width="100%" class="section_table" cellpadding="0"
												cellspacing="0">
												<tbody>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>

															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.itemCode"/>"
															class="text label"> <fmt:message
																	key="biolims.common.itemCode" />
														</label>
														</td>

														<td class="requiredcolumn" nowrap width="10px"><img
															class='requiredimage' src='${ctx}/images/required.gif' /></td>

														<td nowrap><input type="text" size="20"
															maxlength="36" id="project_id" name="project.id"
															title="<fmt:message key="biolims.common.serialNumber"/>"
															class="text input"
															value="<s:property value="project.id"/>" />
														<td nowrap>&nbsp;</td>
													</tr>

													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.projectLeader"/>"
															class="text label"><fmt:message
																	key="biolims.common.projectLeader" /></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px"><img
															class='requiredimage' src='${ctx}/images/required.gif' /></td>
														<td nowrap><input type="text"
															class="input_parts text input default  readonlyfalse false"
															name="project.dutyUser.name" readonly="readOnly"
															id="project.dutyUser.name" style="width:152px"
															value='<s:property value="project.dutyUser.name"/>'
															title="<fmt:message key="biolims.common.projectLeader"/> "
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="10"
															maxlength="30"> <input type="hidden"
															id="project.dutyUser.id" name="project.dutyUser.id"
															value='<s:property value="project.dutyUser.id"/>'>
															<img alt='<fmt:message key="biolims.common.select"/>'
															id='dutyNameBtn' src='${ctx}/images/img_lookup.gif'
															name='dutyNameBtn' class='detail' /></td>
														<td nowrap>&nbsp;</td>
													</tr>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.startTime"/>"
															class="text label"> <fmt:message
																	key="biolims.common.startTime" /></label>
														</td>

														<td class="requiredcolumn" nowrap width="10px"></td>

														<td nowrap><input type="text" Class="Wdate"
															onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
															name="project.planStartDate" id="project_planStartDate"
															value="<s:date name="project.planStartDate" format="yyyy-MM-dd"/>"
															title="<fmt:message key="biolims.common.startTime"/>  "
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="20"
															maxlength="30"></input></td>
														<td nowrap>&nbsp;</td>
													</tr>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>

															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="" class="text label"> <fmt:message key="biolims.common.returnRatio"></fmt:message> </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px"><img
															class='requiredimage' src='${ctx}/images/required.gif' /></td>

														<td nowrap><input type="text" size="20"
															maxlength="36" id="project_sumScale"
															name="project.sumScale" title="" class="text input"
															value="<s:property value="project.sumScale"/>" />
														<td nowrap>&nbsp;</td>
													</tr>
													<tr>
														<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var dutyNameBtn00 = Ext.get('dutyNameBtn00');
dutyNameBtn00.on('click', 
 function dutyUserFun00(){
var win = Ext.getCmp('dutyUserFun00');
if (win) {win.close();}
var dutyUserFun00= new Ext.Window({
id:'dutyUserFun00',modal:true,title:'selective selling ',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=dutyUserFun00' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 dutyUserFun00.close(); }  }]  });     dutyUserFun00.show(); }
);
});
 function setdutyUserFun00(id,name){
 document.getElementById("project_person").value = id;
document.getElementById("project_person_name").value = name;
var win = Ext.getCmp('dutyUserFun00')
if(win){win.close();}
}
</script>


														<td class="label-title"><fmt:message key="biolims.common.sales"></fmt:message> </td>
														<td class="requiredcolumn" nowrap width="10px"></td>
														<td align="left"><input type="text" size="20"
															readonly="readOnly" id="project_person_name"
															value="<s:property value="project.person.name"/>"
															class="text input readonlyfalse" readonly="readOnly" />
															<input type="hidden" id="project_person"
															name="project.person.id"
															value="<s:property value="project.person.id"/>">
															<img alt='选择销售' id='dutyNameBtn00'
															src='${ctx}/images/img_lookup.gif' class='detail' /></td>
													</tr>
													<tr class="control textboxcontrol">

														<td valign="top" align="right" class="controlTitle" nowrap>

															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.projectDescription"/>"
															class="text label"><fmt:message
																	key="biolims.common.projectDescription" /> </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px"></td>

														<td><textarea class="text input  false"
																name="project.note" id="project_note" tabindex="0"
																title=""
																style="overflow: hidden; width: 400px; height: 100px;">
																<s:property value="project.note" />
															</textarea></td>
														<td nowrap>&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td class="sectioncol " width='33%' valign="top" align="right">
											<table width="100%" class="section_table" cellpadding="0"
												cellspacing="0">
												<tbody>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>

															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.projectName"/>"
															class="text label"> <fmt:message
																	key="biolims.common.projectName" />
														</label>
														</td>

														<td class="requiredcolumn" nowrap width="10px"><img
															class='requiredimage' src='${ctx}/images/required.gif' /></td>

														<td nowrap><s:textfield
																class="input_parts text input default  readonlyfalse false"
																name="project.name" style="width:152px"
																id="project_name"
																title='<fmt:message key="biolims.common.projectName"/> '
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="50"
																maxlength="55"></s:textfield></td>

														<td nowrap>&nbsp;</td>

													</tr>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.selectInstitutions"/>"
															class="text label"><fmt:message
																	key="biolims.common.selectTheCustomer" /> </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px"><img
															class='requiredimage' src='${ctx}/images/required.gif' /></td>
														<td nowrap><input type="text"
															class="input_parts text input default  readonlyfalse false"
															name="project.mainProject.name" readonly="readOnly"
															id="project_mainProject_name" style="width: 152px"
															value='<s:property value="project.mainProject.name"/>'
															title="<fmt:message key="biolims.common.selectInstitutions"/>  "
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="40"
															maxlength="30"> <input type="hidden"
															id="project_mainProject_id" name="project.mainProject.id"
															value='<s:property value="project.mainProject.id"/>'>
															<img alt='<fmt:message key="biolims.common.select"/>'
															id='mainProjectBtn' src='${ctx}/images/img_lookup.gif'
															name='mainProjectBtn' class='detail' /></td>

														<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var mainProjectBtn = Ext.get('mainProjectBtn');
mainProjectBtn.on('click', 
 function CrmCustomerFun(){
var win = Ext.getCmp('CrmCustomerFun');
if (win) {win.close();}
var CrmCustomerFun= new Ext.Window({
id:'CrmCustomerFun',modal:true,title:'<fmt:message key="biolims.common.selectTheCustomerID"/>',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 CrmCustomerFun.close(); }  }]  });     CrmCustomerFun.show(); }
);
});
 function setCrmCustomerFun(rec){
document.getElementById('project_mainProject_id').value=rec.get('id');
											document.getElementById('project_mainProject_name').value=rec.get('name');
var win = Ext.getCmp('CrmCustomerFun')
if(win){win.close();}
}
</script>


														<td nowrap>&nbsp;</td>

													</tr>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>

															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.expectedDeliveryDate"/>"
															class="text label"> <fmt:message
																	key="biolims.common.expectedDeliveryDate" /></label>
														</td>

														<td class="requiredcolumn" nowrap width="10px"></td>

														<td nowrap><input type="text" Class="Wdate"
															onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
															name="project.factStartDate" id="project_factStartDate"
															value="<s:date name="project.factStartDate" format="yyyy-MM-dd"/>"
															title="<fmt:message key="biolims.common.finalDeliveryDate"/>"
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="20"
															maxlength="30"></input></td>
														<td nowrap>&nbsp;</td>

													</tr>

													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.state"/>"
															class="text label"><fmt:message
																	key="biolims.common.currentState" /> </label>
														</td>
														<td class="requiredcolumn" nowrap width="10px"></td>
														<td nowrap><input type="text"
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)"
															class="input_parts text input default"
															name="project.state.name" id="project_state_name"
															readonly="readonly"
															value="<s:property value="project.state.name"/>"
															size="20" /> <input type="hidden"
															name="project.state.id" id="project_state_id"
															value="<s:property value="project.state.id"/>" /> <img
															alt='<fmt:message key="biolims.common.select"/>'
															id='showDicState' src='${ctx}/images/img_lookup.gif'
															class='detail' /></td>
														<td nowrap>&nbsp;</td>
													</tr>
													<tr class="control textboxcontrol">
														<td valign="top" align="right" class="controlTitle" nowrap>

															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="" class="text label"> <fmt:message key="biolims.common.itemAmount"></fmt:message> </label>
														</td>

														<td class="requiredcolumn" nowrap width="10px"><img
															class='requiredimage' src='${ctx}/images/required.gif' /></td>

														<td nowrap><s:textfield
																class="input_parts text input default  readonlyfalse false"
																name="project.money" id="project_money" title=" "
																onblur="textbox_ondeactivate(event);" style="width:152px"
																onfocus="shared_onfocus(event,this)" size="30"
																maxlength="55"></s:textfield></td>

														<td nowrap>&nbsp;</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td class="sectioncol " width='33%' valign="top" align="right">
											<div class="section">
												<table width="100%" class="section_table" cellpadding="0"
													cellspacing="0">
													<tbody>
														<tr class="control textboxcontrol">
															<td valign="top" align="right" class="controlTitle"
																nowrap><span class="labelspacer">&nbsp;&nbsp;</span>
																<label title="<fmt:message key="biolims.common.type"/>"
																class="text label"><fmt:message
																		key="biolims.common.projectType" /> </label></td>
															<td class="requiredcolumn" nowrap width="10px"></td>
															<td nowrap><input type="text"
																class="input_parts text input default  readonlyfalse false"
																readonly="readOnly" name="" id="project_type_name"
																value='<s:property value="project.type.name"/>'
																title="Index " onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="20"
																maxlength="30"> <input type="hidden"
																id="project_type_id" name="project.type.id"
																value='<s:property value="project.type.id"/>'> <img
																alt='<fmt:message key="biolims.common.select"/>'
																id='searchtype' src='${ctx}/images/img_lookup.gif'
																class='detail' /></td>
															<td nowrap>&nbsp;</td>
														</tr>

														<tr>





															<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showDoctor = Ext.get('showDoctor');
showDoctor.on('click', 
 function DicProbeFun(){
var win = Ext.getCmp('DicProbeFun');
if (win) {win.close();}
var DicProbeFun= new Ext.Window({
id:'DicProbeFun',modal:true,title:'<fmt:message key="biolims.common.selectTheDoctor"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/crm/doctor/crmPatientSelect.action?flag=DicProbeFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 DicProbeFun.close(); }  }]  });     DicProbeFun.show(); }
);
});
 function setDicProbeFun(rec){
document.getElementById('project_crmDoctor_id').value=rec.get('id');
											document.getElementById('project_crmDoctor_name').value=rec.get('name');
var win = Ext.getCmp('DicProbeFun')
if(win){win.close();}
}
</script>




															<td class="label-title"><fmt:message
																	key="biolims.common.selectTheDoctor" /></td>
															<td class="requiredcolumn" nowrap width="10px"></td>
															<td align="left"><input type="text" size="20"
																maxlength="25" id="project_crmDoctor_name"
																name="project.crmDoctor.name"
																title="<fmt:message key="biolims.common.attendingPhysician"/>"
																value="<s:property value="project.crmDoctor.name"/>" />
																<input type="hidden" id="project_crmDoctor_id"
																name="project.crmDoctor.id"
																value="<s:property value="project.crmDoctor.id"/>">
																<img
																alt='<fmt:message key="biolims.common.selectTheCustomerName"/>'
																id='showDoctor' src='${ctx}/images/img_lookup.gif'
																class='detail' /></td>

														</tr>

														<tr class="control textboxcontrol">
															<td valign="top" align="right" class="controlTitle"
																nowrap><span class="labelspacer">&nbsp;&nbsp;</span>
																<label
																title="<fmt:message key="biolims.common.actualDeliveryDate"/>"
																class="text label"> <fmt:message
																		key="biolims.common.actualDeliveryDate" /></label></td>

															<td class="requiredcolumn" nowrap width="10px"></td>

															<td nowrap><input type="text" Class="Wdate"
																onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
																name="project.planEndDate" id="project_planEndDate"
																value="<s:date name="project.planEndDate" format="yyyy-MM-dd"/>"
																title="<fmt:message key="biolims.common.finalDeliveryDate"/>  "
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="20"
																maxlength="30"></input></td>

															<td nowrap>&nbsp;</td>

														</tr>


														<!-- 		<tr> -->
														<!-- 									<td class="label-title" >检测项目</td> -->
														<%--                	 	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	 --%>
														<!--                    	<td align="left"  > -->
														<%--  						<input  type="text"  size="20"  readonly="readOnly"  id="project_productName"  name="project.productName"  value="<s:property value="project.productName"/>" class="text input "  /> --%>
														<%--  						<input type="hidden"  id="project_productId" name="project.productId"  value="<s:property value="project.productId"/>" >  --%>
														<%--  						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' /> --%>

														<!--                    	</td> -->




														<tr id="doclinks">


															<td valign="top" align="right" class="controlTitle"
																nowrap><span class="labelspacer">&nbsp;&nbsp;</span>
																<label
																title="<fmt:message key="biolims.common.attachment"/>"
																id="doclinks_label" name="doclinks_label"
																class="text label"><fmt:message
																		key="biolims.common.attachment" /></label>&nbsp;</td>
															<td class="requiredcolumn" nowrap width="10px"></td>
															<td
																title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>"
																id="doclinks_img"><span class="attach-btn"></span><span
																class="text label"><fmt:message
																		key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
																		key="biolims.common.attachment" /></span> <%-- <td align="left"  class="text"><img title="附件" id="doclinks_img"
								class="detail" src="${ctx}/images/img_attach.gif"  />
								<%if(request.getAttribute("project.fileUploadDate")!=null){%>报告上传日期
								<s:date name="project.fileUploadDate" format="yyyy-MM-dd"/>
								<%} %>
								</td> --%>
															<td nowrap>&nbsp;</td>
															<td nowrap></td>
															<td nowrap></td>
														</tr>

													</tbody>
												</table>
											</div>

										</td>
									</tr>

								</tbody>
							</table>

							<div id="tabs">
								<ul>
									<li><a href="#applicationTypeActionLogZJpage"><fmt:message key="biolims.common.qcTask"></fmt:message></a></li>
									<li><a href="#projectObjectivesJKpage"><fmt:message key="biolims.common.scienceServiceBuild"></fmt:message></a></li>
									<li><a href="#projectObjectivesSJpage"><fmt:message key="biolims.common.sequenceTask"></fmt:message></a></li>
								</ul>
								<div id="projectObjectivesSJpage" style="height: 350px"></div>
								<div id="projectObjectivesJKpage" style="height: 350px"></div>
								<div id="applicationTypeActionLogZJpage" style="height: 350px"></div>

								<input type="hidden" name="projectObjectivesJKJson"
									id="projectObjectivesJKJson" value="" /> 

								<input type="hidden" name="projectObjectivesSJJson"
									id="projectObjectivesSJJson" value="" /> 

								<input type="hidden" name="projectObjectivesSXJson"
									id="projectObjectivesSXJson" value="" /> 
								<input type="hidden"
									name="applicationTypeActionLogZJJson"
									id="applicationTypeActionLogZJJson" value="" />
</div>
								<%
									if (request.getParameter("id") != null) {
								%>
								<input type="hidden" name="modifyFlag" value="true"></input>
								<%
									}
								%>
								</s:form>

								<div id="tabs1" style="margin: 1 0 1 0"></div>
							</div>
							<!-- 
							<div id="tabs">
								<ul>
									<li><a href="#applicationTypeActionLogpage"><fmt:message
												key="biolims.common.projectProgress" /></a></li>
									<li><a href="#projectObjectivespage">订单信息111</a></li>
								</ul>
								<div id="projectObjectivespage" width="100%" height:10px></div>
								<div id="applicationTypeActionLogpage" style="height: 350px"></div>

								<input type="hidden" name="projectObjectivesJson"
									id="projectObjectivesJson" value="" /> <input type="hidden"
									name="applicationTypeActionLogJson"
									id="applicationTypeActionLogJson" value="" />

								<%
									if (request.getParameter("id") != null) {
								%>
								<input type="hidden" name="modifyFlag" value="true"></input>
								<%
									}
								%>

								<div id="tabs1" style="margin: 1 0 1 0"></div>
							</div>
							-->
							
							 <script type="text/javascript">
								<s:if test='#request.handlemethod=="modify"'>

								var t = "#project_id";
								settextreadonlyById(t);

								</s:if>
								<s:if test='#request.handlemethod=="view"'>
								settextreadonlyByAll();

								</s:if>
							</script>
</body>
</html>
