<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>biolims</title>
<style>
</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/borrow/editInstrumentBorrow.js"></script>
<s:if test='#request.handlemethod!="view"'>
	<script type="text/javascript">
function modifyData(params){
var paramsValue = "instrumentBorrowId:'',instrumentBorrowDetailId:''";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : biolims.common.pleaseWait});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/borrow/saveInstrumentBorrowDetail.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : biolims.common.info,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>

	<script type="text/javascript">
function delData(params){
var paramsValue = "";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : biolims.common.pleaseWait});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/borrow/delInstrumentBorrowDetail.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : biolims.common.info,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>

		
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchtype = Ext.get('searchtype');
searchtype.on('click', 
 function jydb(){
var win = Ext.getCmp('jydb');
if (win) {win.close();}
var jydb= new Ext.Window({
id:'jydb',modal:true,title:'<fmt:message key="biolims.common.chooseTheType"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=jydb' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 jydb.close(); }  }]  });     jydb.show(); }
);
});
 function setjydb(id,name){
 document.getElementById("instrumentBorrow_type_id").value = id;
document.getElementById("instrumentBorrow_type_name").value = name;
var win = Ext.getCmp('jydb')
if(win){win.close();}
}
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchnewdutyuser = Ext.get('searchnewdutyuser');
searchnewdutyuser.on('click', 
 function zrr(){
var win = Ext.getCmp('zrr');
if (win) {win.close();}
var zrr= new Ext.Window({
id:'zrr',modal:true,title:'<fmt:message key="biolims.common.selectPersonnel"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=zrr' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 zrr.close(); }  }]  });     zrr.show(); }
);
});
 function setzrr(id,name){
document.getElementById('instrumentBorrow_newDutyUser_id').value=id;document.getElementById('instrumentBorrow_newDutyUser_name').value=name;
var win = Ext.getCmp('zrr')
if(win){win.close();}
}
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchnewfinancecode = Ext.get('searchnewfinancecode');
searchnewfinancecode.on('click', 
 function searchnewfinancecodeFun(){
var win = Ext.getCmp('searchnewfinancecodeFun');
if (win) {win.close();}
var searchnewfinancecodeFun= new Ext.Window({
id:'searchnewfinancecodeFun',modal:true,title:'<fmt:message key="biolims.common.selectNewFinancialSubjects"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/financeItem/financeItemSelect.action?flag=searchnewfinancecodeFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 searchnewfinancecodeFun.close(); }  }]  });     searchnewfinancecodeFun.show(); }
);
});
 function setsearchnewfinancecodeFun(id,name){
document.getElementById('instrumentBorrow_newFinanceCode_name').value=name;document.getElementById('instrumentBorrow_newFinanceCode_id').value=id;
var win = Ext.getCmp('searchnewfinancecodeFun')
if(win){win.close();}
}
</script>



	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchnewdepartment = Ext.get('searchnewdepartment');
searchnewdepartment.on('click', 
 function searchdepartmentfun(){
var win = Ext.getCmp('searchdepartmentfun');
if (win) {win.close();}
var searchdepartmentfun= new Ext.Window({
id:'searchdepartmentfun',modal:true,title:'<fmt:message key="biolims.common.chooseDepartment"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/department/departmentSelect.action?flag=searchdepartmentfun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 searchdepartmentfun.close(); }  }]  });     searchdepartmentfun.show(); }
);
});
 function setsearchdepartmentfun(id,name){
 document.getElementById("instrumentBorrow_newDepartment_id").value = id;
document.getElementById("instrumentBorrow_newDepartment_name").value = name;
var win = Ext.getCmp('searchdepartmentfun')
if(win){win.close();}
}
</script>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showDicState = Ext.get('showDicState');
showDicState.on('click', 
 function showDicStateFun(){
var win = Ext.getCmp('showDicStateFun');
if (win) {win.close();}
var showDicStateFun= new Ext.Window({
id:'showDicStateFun',modal:true,title:'<fmt:message key="biolims.common.selectTheState"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/state/stateSelect.action?type=instrument&flag=showDicStateFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 showDicStateFun.close(); }  }]  });     showDicStateFun.show(); }
);
});
 function setshowDicStateFun(id,name){
 document.getElementById("instrumentBorrow_newState_id").value = id;
document.getElementById("instrumentBorrow_newState_name").value = name;
var win = Ext.getCmp('showDicStateFun')
if(win){win.close();}
}
</script>



</s:if>
<body>
	<s:if
		test='#request.handlemethod=="modify"||#request.handlemethod=="add"'>
		<script type="text/javascript"
			src="${ctx}/javascript/handleVaLeave.js"></script>
	</s:if>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<s:form name="form1" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.instrumentAdjustingCode"/>" class="text label"><fmt:message key="biolims.common.instrumentAdjustingCode"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												readonly="true" name="instrumentBorrow.id"
												id="instrumentBorrow_id" title='<fmt:message key="biolims.common.borrowTheAllocatedCode"/> '
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col1_sec1_2_detail' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>

									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.applicant"/>" class="text label"><fmt:message key="biolims.common.applicant"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden name="instrumentBorrow.appleUser.id"
												id="instrumentBorrow_appleUser_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												readonly="true" name="instrumentBorrow.appleUser.name"
												id="instrumentBorrow_appleUser_name" title='<fmt:message key="biolims.common.applicant"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col1_sec1_2_detail' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.leader"/>" class="text label"><fmt:message key="biolims.common.leader"/> </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden name="instrumentBorrow.dutyUser.id"
												id="instrumentBorrow_dutyUser_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentBorrow.dutyUser.name"
												id="instrumentBorrow_dutyUser_name" readonly="true"
												title='<fmt:message key="biolims.common.leader"/>' onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="20"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>

									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.workflowState"/>" class="text label"><fmt:message key="biolims.common.workflowState"/> </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentBorrow.stateName" readonly="true"
												id="instrumentBorrow_stateName" title='<fmt:message key="biolims.common.workflowState"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30"></s:textfield></td>
										<s:hidden id="instrumentBorrow.state"
											name="instrumentBorrow.state"></s:hidden>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title='<fmt:message key="biolims.common.commandTime"/>' class="text label"><fmt:message key="biolims.common.commandTime"/> </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td align="left" nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentBorrow.createDate" readonly="true"
												id="instrumentBorrow_createDate" title='<fmt:message key="biolims.common.commandTime"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="20">
												<s:param name="value">
													<s:date name="instrumentBorrow.createDate"
														format="yyyy-MM-dd" />
												</s:param>
											</s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.subordinateDepartments"/>" class="text label"><fmt:message key="biolims.common.subordinateDepartments"/></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:hidden
														name="instrumentBorrow.oldDepartment.id"
														id="instrumentBorrow_oldDepartment_id"></s:hidden> <s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="instrumentBorrow.oldDepartment.name"
														id="instrumentBorrow_oldDepartment_name" readonly="true"
														title='<fmt:message key="biolims.common.subordinateDepartments"/>' onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="20"
														maxlength="20"></s:textfield> <img alt=''
													id='main_grid1_row1_col2_sec2_4_detail'
													src='${ctx}/images/blank.gif'
													name='main_grid1_row1_col2_sec2_4_detail' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.approver"/> " class="text label"><fmt:message key="biolims.common.approver"/> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td align="left" nowrap><s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="instrumentBorrow.confirmUser.name" readonly="true"
														id="instrumentBorrow_confirmUser_name" title='<fmt:message key="biolims.common.approver"/> '
														onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="10"
														maxlength="20"></s:textfield> <s:hidden
														name="instrumentBorrow.confirmUser.id"
														id="instrumentBorrow.confirmUser.id"></s:hidden></td>
												<td nowrap>&nbsp;</td>
											</tr>


										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<div class="section standard_section  ">
				<div class="section">
					<table width="100%" class="section_table" cellpadding="0">
						<tbody>
							<tr class="sectionrow " valign="top">
								<td class="sectioncol " width='50%' valign="top" align="right">
									<div class="section standard_section marginsection  ">
										<div width="100%"
											class="section_header standard_section_header">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tbody>
													<tr width="100%">
														<td
															class="section_header_left text standard_section_label labelcolor"
															align="left"><span class="section_label"><fmt:message key="biolims.common.instrumentAdjustingInformation"/> 
														</span></td>
														<td class="section_header_right" align="right"></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class=" section_content_border">
											<table width="100%" class="section_table" cellpadding="0">
												<tbody>
													<tr class="control textboxcontrol">

														<td nowrap>&nbsp;</td>
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.operationDate"/>" class="text label"><fmt:message key="biolims.common.operationDate"/></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><input type="text" Class="Wdate"
															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})"
															name="instrumentBorrow.handleDate"
															id="instrumentBorrow_handleDate"
															value="<s:date name="instrumentBorrow.handleDate" format="yyyy-MM-dd"/>"
															title='<fmt:message key="biolims.common.operationDate"/>' onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="20"
															maxlength="20" /> <img alt=''
															src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>


														<td nowrap><span class="labelspacer">&nbsp;&nbsp;</span>
															<label title="<fmt:message key="biolims.common.state"/>" class="text label"><fmt:message key="biolims.common.equipmentStateOfAdjustment"/></label>
															<input type="text"
															class="input_parts text input default  readonlyfalse false"
															name="" id="instrumentBorrow_newState_name"
															readonly="readonly"
															value="<s:property value="instrumentBorrow.newState.name"/>"
															size="10" readonly="readonly" /> <input type="hidden"
															name="instrumentBorrow.newState.id"
															id="instrumentBorrow_newState_id"
															value="<s:property value="instrumentBorrow.newState.id"/>" />
															<img alt='<fmt:message key="biolims.common.select"/>' id='showDicState'
															src='${ctx}/images/img_lookup.gif' class='detail' /></td>
													</tr>


												</tbody>
											</table>

										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table align="center">
						<tr>
							<td nowrap><span class="labelspacer">&nbsp;&nbsp;</span> <label
								title="<fmt:message key="biolims.common.adjustTheReason"/>" class="text label"><fmt:message key="biolims.common.adjustTheReason"/></label></td>
							<td><s:textarea name="instrumentBorrow.note"
									id="instrumentBorrow_note" style="width:1000px;height:100px;"></s:textarea></td>
						</tr>
					</table>
				</div>
			</div>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value="" />
			<input type="hidden" name="jsonDataStr1" id="jsonDataStr1" value="" />
			<input type="hidden" name="jsonDataStr2" id="jsonDataStr2" value="" />
		</s:form>
		<input type="hidden" id="handlemethod" value="${handlemethod}" /> <input
			type="hidden" id="handlemethod" value="${copyMode}" /> <input
			type="hidden" id="id" value="${id}" /> <input type="hidden"
			id="instrumentBorrowId" value="${instrumentBorrowId}" /> <input
			type="hidden" id="instrumentBorrowDetailId"
			value="${instrumentBorrowDetailId}" />
						<!-- <g:GridEditTag isAutoWidth="true"
							height="parent.document.body.clientHeight-125"
							validateMethod="validateFun" pageSize="100000" col="${col}"
							statement="${statement}" type="${type}" title="" url="${path}"
							statementLisener="${statementLisener}"
							handleMethod="${handlemethod}" /> -->
			<div id='bat_svnuploadcsv_div'  style="display: none">
			<input type="file" name="file" id="file-svnuploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
			</div>
	<div id='grid' style='width: 100%;' ></div>
	</div>
	<div id="tabs1" style="margin: 1 0 1 0"></div>
</body>
</html>