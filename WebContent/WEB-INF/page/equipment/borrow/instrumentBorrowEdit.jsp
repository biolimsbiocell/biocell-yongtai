<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}

#btn_submit {
	display: none;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>
						<fmt:message key="biolims.common.instrumentAdjustingInformation" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.equipment.taskId" />： <span
							id="instrumentBorrow_id"><s:property
									value="instrumentBorrow.id" /></span> <fmt:message
								key="biolims.common.applicant" />: <span
							userId="<s:property value="instrumentBorrow.creatUser.id"/>"
							id="instrumentBorrow_createUser"><s:property
									value="instrumentBorrow.appleUser.name" /></span> <fmt:message
								key="biolims.common.adjustTheDate" />: <span
							id="instrumentBorrow_handleDate"><s:date
									name=" instrumentBorrow.handleDate " format="yyyy-MM-dd " /></span> <fmt:message
								key="biolims.tInstrument.state" />: <span
							state="<s:property value="instrumentBorrow.state"/>"
							id="headStateName"><s:property
									value="instrumentBorrow.stateName" /></span>
						</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>

							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<input type="hidden" size="20" maxlength="25"
											id="instrumentBorrowId" name="id" readonly="readOnly"
											value="<s:property value="instrumentBorrow.id"/>" />
										<input type="hidden" size="20" maxlength="25"
											id="instrumentBorrowHandleDate" name="handleDate"
											value="<s:date name="handleDate" format="yyyy-MM-dd"/>" />
										<input type="hidden" id="instrumentBorrow_appleUserId"
											name="appleUser-id"
											value="<s:property value="instrumentBorrow.appleUser.id"/>" />
										<input type="hidden" size="20" maxlength="25"
											id="instrumentBorrow_stateName" name="stateName"
											readonly="readOnly"
											value="<s:property value="instrumentBorrow.stateName"/>" />
										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrumentBorrow.newState" /> </span> <input
											type="text" size="20" readonly="readOnly"
											id="instrumentBorrow_newState_name"
											changelog="<s:property value="instrumentBorrow.newState.name"/>"
											value="<s:property value="instrumentBorrow.newState.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrumentBorrow_newState_id" name="newState-id"
											value="<s:property value="instrumentBorrow.newState.id"/>">
											<input type="hidden" id="instrumentBorrow_state_id"
											name="state"
											value="<s:property value="instrumentBorrow.state"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="shownewState()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrumentBorrow.note" /> </span> <input
											type="textarea" size="20" maxlength="25"
											id="instrumentBorrow_note" name="note"
											changelog="<s:property value="instrumentBorrow.note"/>"
											class="form-control"
											value="<s:property value="instrumentBorrow.note"/>" />
									</div>
								</div>
							</div>
							<%-- <div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> <fmt:message key="biolims.common.customFields"/>
													
											</h3>
										</div>
									</div>
								</div> --%>
							<div id="fieldItemDiv"></div>

							<br> <input type="hidden" name="instrumentBorrowDetailJson"
								id="instrumentBorrowDetailJson" value="" /> <input
								type="hidden" id="id_parent_hidden"
								value="<s:property value="instrumentBorrow.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="instrumentBorrow.fieldContent"/>" />

						</form>
					</div>
					<!--table表格-->
					<!--table表格-->
					<!--库存主数据-->
					<div id="leftDiv" class="col-md-4 col-xs-12">
						<div class="box box-success">
							<div class="box-header with-border">
								<i class="glyphicon glyphicon-leaf"></i>
								<h3 class="box-title">
									<fmt:message key="biolims.common.instrumentName" />
								</h3>
							</div>
							<div class="box-body ipadmini">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="instrumentTempdiv" style="font-size: 14px;"></table>
							</div>
						</div>
					</div>
					<!--排板后样本-->
					<div id="rightDiv" class="col-md-8 col-xs-12">
						<div class="box box-success">
							<div class="box-header with-border">
								<i class="glyphicon glyphicon-leaf"></i>
								<h3 class="box-title">
									<fmt:message
										key="biolims.common.instrumentAdjustingInformation" />
								</h3>
							</div>
							<div class="box-body ipadmini">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="instrumentBorrowDetaildiv" style="font-size: 14px;"></table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 	<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div> -->
		</div>
	</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/borrow/instrumentBorrowEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/borrow/instrumentBorrowDetail.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/borrow/instrumentBorrowTemp.js"></script>
</body>

</html>
