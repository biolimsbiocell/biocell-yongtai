<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/borrow/showInstrumentBorrowList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<g:LayOutWinTag buttonId="searchtype"
	title='<fmt:message key="biolims.common.chooseTheType"/>'
	hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
	isHasSubmit="false" functionName="jydb" hasSetFun="true"
	documentId="type_id" documentName="type_name" />

<g:LayOutWinTag buttonId="searchnewdutyuser"
	title='<fmt:message key="biolims.common.selectPersonnel"/>'
	hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="zrr" hasSetFun="true"
	extRec="id,name"
	extStr="document.getElementById('newDutyUser_id').value=id;document.getElementById('newDutyUser_name').value=name;" />

</head>
<body>

	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div id="jstj" style="display: none">
		<table cellspacing="0" cellpadding="0" class="frame-iefull-table">
			<tr>
				<td class="label-title"><fmt:message
						key="biolims.common.adjustTheNumber" /></td>
				<td><input maxlength="32" id="sid" searchField="true" name="id"></input>
				</td>
				<td class="label-title"><fmt:message key="biolims.common.type" /></td>
				<td nowrap><input name="type.id" id="type_id" type=hidden
					searchField="true" /> <input
					class="input_parts text input default  readonlyfalse false"
					name="type.name" id="type_name"
					title="<fmt:message key="biolims.common.type"/>  "
					onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)" size="10" maxlength="10"
					readonly="true" /> <img
					alt='<fmt:message key="biolims.common.chooseTheType"/>'
					id='searchtype' src='${ctx}/images/img_lookup.gif'
					name='searchtype' class='detail' /></td>

			</tr>
		</table>
	</div>
	<div>
		<div id="show_equipment_borrow_div"></div>
	</div>
	<div class="mainlistclass" id="markup">
		<input type="hidden" id="id" value="" />
		<form name='excelfrm' action='/common/exportExcel.action'
			method='POST'>
			<input type='hidden' id='gridhtm' name='gridhtm' value='' />
		</form>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' />
		<div id='grid'></div>
		<div id='gridcontainer'></div>

	</div>

</body>
</html>