<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
#btn_submit{
  display:none;
}
</style>
</head>

<body>
<div>
			<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
		</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message
						key="biolims.common.instrumentMaintenance" />
						<small style="color: #fff;"> <fmt:message
						key="biolims.equipment.taskId" />: <span id="instrumentMaintain_id"><s:property value="instrumentMaintain.id" /></span>
								<fmt:message
						key="biolims.common.commandPerson" />: <span
								userId="<s:property value="instrumentMaintain.creatUser.id"/>"
								id="instrumentMaintain_createUser"><s:property
										value="instrumentMaintain.createUser.name" /></span> 	<fmt:message
						key="biolims.common.commandTime" />: <span
								id="instrumentMaintain_createDate"><s:date name=" instrumentMaintain.createDate " format="yyyy-MM-dd "/></span> <fmt:message
						key="biolims.tInstrument.state" />: <span
								state="<s:property value="instrumentMaintain.state"/>"
								id="headStateName"><s:property
										value="instrumentMaintain.stateName" /></span>
							</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
									<input type="hidden"
										size="20" maxlength="25" id="instrumentMaintainId" name="id" readonly = "readOnly"  
										value="<s:property value="instrumentMaintain.id"/>"
						 			/>  
									<input type="hidden"
										id="instrumentMaintain_createUserId" name="createUser-id"
										value="<s:property value="instrumentMaintain.createUser.id"/>"
									/>
									<input type="hidden"
										size="20" maxlength="25" id="instrumentMaintainCreateDate" name="createDate" 
										value="<s:date name="instrumentMaintain.createDate" format="yyyy-MM-dd"/>"
                   	   				/> 
									<input type="hidden"
										size="20" maxlength="25" id="instrumentMaintainState" name="state"  
										value="<s:property value="instrumentMaintain.state"/>"
						 			/> 
									<input type="hidden"
										size="20" maxlength="25" id="instrumentMaintainStateName" name="stateName"  
										value="<s:property value="instrumentMaintain.stateName"/>"
						 			/>  
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.tInstrumentMaintain.name"/> </span> 
	                   						<input type="text"
												size="50" maxlength="50" id="instrumentMaintain_name" name="name"  changelog="<s:property value="instrumentMaintain.name"/>"
												class="form-control" value="<s:property value="instrumentMaintain.name"/>"
							 				/>  
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">验证时间</span> 
                   						<input type="text"
											size="20" maxlength="25" id="instrumentMaintain_maintainDate" name="maintainDate"  changelog="<s:property value="instrumentMaintain.maintainDate"/>"
											class="form-control"   class="Wdate" value="<s:date name="instrumentMaintain.maintainDate" format="yyyy-MM-dd"/>"
                   	   					/>  
					
									</div>
								</div>				
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key="biolims.tInstrumentMaintain.note"/> </span> 
	                   						<input type="text"
												size="20" maxlength="25" id="instrumentMaintain_note"
												name="note"  changelog="<s:property value="instrumentMaintain.note"/>"
												class="form-control" value="<s:property value="instrumentMaintain.note"/>"
											 />  
									</div>
								</div>				
									</div>
								<div class="row">
										<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepairPlan.type"/><img class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="instrumentMaintain_type_name"  changelog="<s:property value="instrumentMaintain.type.name"/>"
												value="<s:property value="instrumentMaintain.type.name"/>" class="form-control"
										/> 
										<input type="hidden"
												id="instrumentMaintain_type_id" name="type-id"
												value="<s:property value="instrumentMaintain.type.id"/>"
										/>
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showtype()">
													<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>	
									</div>
							<!-- 	<div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> 自定义字段
													
											</h3>
										</div>
									</div>
								</div> -->
							<div id="fieldItemDiv"></div>
								
							<br> 
							<input type="hidden" name="instrumentMaintainItemJson"
								id="instrumentMaintainItemJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="instrumentMaintain.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="instrumentMaintain.fieldContent"/>" />	
								
						</form>
					</div>
					</div>
					<div class="box-footer ipadmini">
						<!--库存主数据-->
						<div id="leftDiv" class="col-xs-4">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.WaitingMaintenanceInstruments"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table class="table table-hover table-striped table-bordered table-condensed" id="instrumentMaintainTempdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-xs-8">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.detailed"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table class="table table-hover table-striped table-bordered table-condensed" id="instrumentMaintainItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>

					</div>
					
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/javascript/equipment/maintain/instrumentMaintainEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/maintain/instrumentMaintainItem.js"></script>
	<script type="text/javascript" 
		src="${ctx}/javascript/equipment/maintain/instrumentMaintainTemp.js"></script>
</body>

</html>	
