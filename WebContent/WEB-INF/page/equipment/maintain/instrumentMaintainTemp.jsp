﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/javascript/equipment/maintain/instrumentMaintainTemp.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="show_instrumentMaintainTemp_div"></div>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  >编号</td>
                   	<td align="left">                 
					<input type="text" size="20" maxlength="25" id="instrumentMaintainTemp_id"
                   	 name="id" searchField="true" title="编号"/>
                   	</td>
                   		<tr >
               	 	<td class="label-title" >验证类型</td>
                   	<td align="left"  >
					<select id="instrumentMaintainTemp_type" name="instrumentRepairPlanTask.type.id" searchField="true" >
								<option value="" selected="selected">请选择</option>
								<option value="whlx1" >日常验证</option>
								<option value="whlx2" >预防性验证</option>
								<option value="whlx3" >校准</option>
								<option value="whlx4" >Function Check</option>
								<option value="whlx5" >Equivalence</option>
					</select>
                   	</td>
			</tr>
			</tr>
            </table>
		</form>
		</div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
</body>
</html>



