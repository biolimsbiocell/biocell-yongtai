<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>

<style>
#btn_submit{
  display:none;
}
</style>
</head>

<body>


	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>

	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>
						<fmt:message key="biolims.common.ireportMaintenance" />
						<small style="color: #fff;"> <fmt:message
								key="biolims.common.encrypt" />: <span id="instrumentFault_id"><s:property
									value="instrumentFault.id" /></span> <fmt:message
								key="biolims.tInstrumentFault.handleUser" />: <span
							userId="<s:property value="instrumentFault.handleUser.id"/>"
							id="instrumentFault_handleUser"><s:property
									value="instrumentFault.handleUser.name" /></span> <fmt:message
								key="biolims.tInstrumentFault.handleDate" />: <span
							handleDate="<s:property value = "instrumentFault_createDate"/>"
							id="instrumentFault_handleDate"> <s:date
									name="instrumentFault.createDate" format="yyyy-MM-dd" /></span> <fmt:message
								key="biolims.tInstrumentRepairPlan.state" />: <span
							state="<s:property value="instrumentFault.state"/>"
							id="headStateName"><s:property
									value="instrumentFault.stateName" /></span>
						</small>

					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<input type="hidden" size="20" maxlength="25"
											id="instrumentFaultId" name="id"
											value="<s:property value="instrumentFault.id"/>" />
									</div>
									<div class="input-group">
										<input type="hidden" size="20" maxlength="25"
											id="instrumentFaultCreatDate" name="createDate"
											value="<s:date name="instrumentFault.createDate" format="yyyy-MM-dd"/>" />
									</div>
									<div class="input-group">
										<input type="hidden" size="20" maxlength="25"
											id="instrumentFaultHandleUserId" name="handleUser-id"
											value="<s:property value="instrumentFault.handleUser.id"/>" />
										<div class="input-group">
											<input type="hidden" size="20" maxlength="25"
												id="instrumentFaultState" name="state"
												value="<s:property value="instrumentFault.state"/>" />

										</div>
										<div class="input-group">
											<input type="hidden" size="20" maxlength="25"
												id="instrumentFaultStateName" name="stateName"
												value="<s:property value="instrumentFault.stateName"/>" />
										</div>
									</div>
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrumentFault.note" /> </span> <input type="text"
											size="50" maxlength="50" id="instrumentFault_note"
											name="note"
											changelog="<s:property value="instrumentFault.note"/>"
											class="form-control"
											value="<s:property value="instrumentFault.note"/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrumentFault.type" /> </span> <input type="text"
											size="20" readonly="readOnly" id="instrumentFault_type_name"
											changelog="<s:property value="instrumentFault.type.name"/>"
											value="<s:property value="instrumentFault.type.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrumentFault_type_id" name="type-id"
											value="<s:property value="instrumentFault.type.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showtype()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 报修时间 </span> <input
											type="text" changelog="" size="20" maxlength="25"
											id="handleDate" name="handleDate" class="form-control"
											title="报修时间"  value="<s:date name="instrumentFault.handleDate" format="yyyy-MM-dd"/>" />
									</div>
								</div>

							</div>
							<%-- 	<div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> <fmt:message key="biolims.common.customFields"/>
													
											</h3>
										</div>
									</div>
								</div> --%>
							<div id="fieldItemDiv"></div>

							<br> <input type="hidden" id="id_parent_hidden"
								value="<s:property value="instrumentFault.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="instrumentFault.fieldContent"/>" />

						</form>
					</div>
					<!--table表格-->
				</div>
				<div id="leftDiv" class="col-md-4 col-xs-12">
					<div class="box box-success">
						<div class="box-header with-border">
							<i class="glyphicon glyphicon-leaf"></i>
							<h3 class="box-title">
								<fmt:message key="biolims.common.machineNumber" />
							</h3>
						</div>
						<div class="box-body ipadmini">
							<table
								class="table table-hover table-striped table-bordered table-condensed"
								id="instrumentTempdiv" style="font-size: 14px;"></table>
						</div>
					</div>
				</div>
				<div id="rightDiv" class="col-md-8 col-xs-12">
					<div class="box box-success">
						<div class="box-header with-border">
							<i class="glyphicon glyphicon-leaf"></i>
							<h3 class="box-title">
								<fmt:message key="biolims.common.detailed" />
							</h3>
						</div>
						<div class="box-body ipadmini">
							<table
								class="table table-hover table-striped table-bordered table-condensed"
								id="instrumentFaultDetaildiv" style="font-size: 14px;">
							</table>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/fault/instrumentFaultEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/fault/instrumentFaultDetail.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/fault/instrumentTemp.js"></script>
</body>

</html>
