<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/fault/showInstrumentFaultList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

<g:LayOutWinTag buttonId="searchUser" title='<fmt:message key="biolims.common.reporter"/>' hasHtmlFrame="true"
	hasSetFun="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="searchUserList"
	documentId="handleUser_id" documentName="handleUser_name" />

<g:LayOutWinTag buttonId="searchtypr" title='<fmt:message key="biolims.common.selectFaultType"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	functionName="gzlx" hasSetFun="true" documentId="type_id"
	documentName="type_name" />

<g:LayOutWinTag buttonId="guigetype" title='<fmt:message key="biolims.common.selectTheInstrument"/>' hasHtmlFrame="true"
	html="${ctx}/equipment/common/instrumentSelect.action"
	isHasSubmit="false" functionName="searchInstrumentList"
	hasSetFun="true" documentId="storage_id" documentName="storage_id" />
</head>
<body>
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td><span>&nbsp;</span> <label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.troubleTicketCode"/></label></td>
					<td class="quicksearch_control" nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="id" searchField="true" id="id" title="<fmt:message key="biolims.common.inputID"/>"
						style="width: 90"></td>

					<td><span>&nbsp;</span> <label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.faultDescription"/></label></td>
					<td class="quicksearch_control" nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="note" searchField="true" searchField="true" id="note"
						title="<fmt:message key="biolims.common.faultDescription"/>" style="width: 90"></td>
					<td><span>&nbsp;&nbsp;</span></td>
					<td><span>&nbsp;</span> <label title="<fmt:message key="biolims.common.equipmentSerialNumber"/>" class="text label"><fmt:message key="biolims.common.equipmentSerialNumber"/></label>
					</td>
					<td class="quicksearch_control" nowrap><input type="text"
						class="input_parts text input default  " searchField="true"
						id="storage_id" name="storage.id" title="<fmt:message key="biolims.common.equipment"/>" size="10"
						maxlength="20"> <!--  <input type="hidden"  name="storage.id" id="storage_id">  -->
						<%-- <img title='选择' id="guigetype" src='${ctx}/images/img_lookup.gif' class='detail' />  --%>
					</td>
				</tr>
				<tr>
					<td><span>&nbsp;</span> <label title="<fmt:message key="biolims.common.faultTypes"/>" class="text label"><fmt:message key="biolims.common.faultTypes"/></label>
					</td>
					<td class="quicksearch_control" nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						readonly="readOnly" id="type_name" name="type.name" title="<fmt:message key="biolims.common.faultTypes"/>"
						size="10" maxlength="20"> <input type="hidden"
						searchField="true" name="type.id" id="type_id"> <img
						title='<fmt:message key="biolims.common.select"/>' id='searchtypr' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>

					<td><span>&nbsp;</span> <label title="<fmt:message key="biolims.common.reporter"/>" class="text label"><fmt:message key="biolims.common.reporter"/></label>
					</td>
					<td class="quicksearch_control" nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						readonly="readOnly" name="handleUser.name" id="handleUser_name"
						value='' title="<fmt:message key="biolims.common.reporter"/>" size="10" maxlength="30"> <input
						type="hidden" searchField="true" name="handleUser.id"
						id="handleUser_id" value=''> <img title='<fmt:message key="biolims.common.select"/>'
						id="searchUser" src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
					<td><span>&nbsp;&nbsp;</span></td>
					<%-- <td>
						<span>&nbsp;</span>
						<label title="规格型号" class="text label">规格型号</label>
					</td>
						<td class="quicksearch_control" nowrap>
						<input type="text" class="input_parts text input default  " searchField="true" id="storage_spec" name="storage.spec" title="规格型号" size="10" maxlength="20">
					    <input type="hidden"  searchField="true" name="storage.spec" id="storage_spec"> 
					   <img title='选择' id="guigetype" src='${ctx}/images/img_lookup.gif' class='detail' /> 
					</td> --%>
				</tr>
				<%-- <tr>
				<td class="label-title">报告日期</td>
			    <td>
					<input type="text" class="Wdate" readonly="readonly" id=handleDate1 name="handleDate##@@##>=##@@##date" searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
				</td>
				<td class="label-title">到</td>
			    <td>
					<input type="text" class="Wdate" readonly="readonly" id="handleDate2" name="handleDate##@@##<=##@@##date"  searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
				</td>
			</tr> --%>
			</table>
		</div>
	</script >
<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
<input type='hidden' id='extJsonDataString' name='extJsonDataString' value=''/>
<div id='grid'  ></div>
<div id='gridcontainer'  ></div>
	</div>
</body>
</html>