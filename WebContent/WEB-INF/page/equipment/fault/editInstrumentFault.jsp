<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="biolims.common.failureReport" /></title>
<style>
</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/fault/editInstrumentFault.js"></script>
<s:if test='#request.handlemethod!="view"'>
<script type="text/javascript">
function modifyData(params){
var paramsValue = "instrumentFaultId:'',instrumentFaultDetailId:''";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : '请等待...'});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/fault/saveInstrumentFaultDetail.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : '消息',
				msg : '操作失败',
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>

	<script type="text/javascript">
function delData(params){
var paramsValue = "";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : '请等待...'});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/fault/delInstrumentFaultDetail.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : '消息',
				msg : '操作失败',
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchInstrument = Ext.get('searchInstrument');
searchInstrument.on('click', 
 function searchInstrumentList(){
var win = Ext.getCmp('searchInstrumentList');
if (win) {win.close();}
var searchInstrumentList= new Ext.Window({
id:'searchInstrumentList',modal:true,title:'<fmt:message key="biolims.common.selectTheInstrument"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/equipment/common/instrumentSelect.action?flag=searchInstrumentList' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 searchInstrumentList.close(); }  }]  });     searchInstrumentList.show(); }
);
});
 function setsearchInstrumentList(rec){
document.getElementById('instrumentFault_storage_id').value=rec.get('id');document.getElementById('instrumentFault_storage_note').value=rec.get('note');document.getElementById('instrumentFault_storage_spec').value=rec.get('spec');document.getElementById('instrumentFault_storage_date').value=rec.get('purchaseDate');
var win = Ext.getCmp('searchInstrumentList')
if(win){win.close();}
}
</script>


	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchtypr = Ext.get('searchtypr');
searchtypr.on('click', 
 function gzlx(){
var win = Ext.getCmp('gzlx');
if (win) {win.close();}
var gzlx= new Ext.Window({
id:'gzlx',modal:true,title:'<fmt:message key="biolims.common.selectFaultType"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=gzlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 gzlx.close(); }  }]  });     gzlx.show(); }
);
});
 function setgzlx(id,name){
 document.getElementById("instrumentFault_type_id").value = id;
document.getElementById("instrumentFault_type_name").value = name;
var win = Ext.getCmp('gzlx')
if(win){win.close();}
}
</script>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchUser = Ext.get('searchUser');
searchUser.on('click', 
 function lyr(){
var win = Ext.getCmp('lyr');
if (win) {win.close();}
var lyr= new Ext.Window({
id:'lyr',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=lyr' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 lyr.close(); }  }]  });     lyr.show(); }
);
});
 function setlyr(id,name){
 document.getElementById("instrumentFault_confirmUser_id").value = id;
document.getElementById("instrumentFault_confirmUser_name").value = name;
var win = Ext.getCmp('lyr')
if(win){win.close();}
}
</script>

</s:if>
<body>
	<s:if
		test='#request.handlemethod=="modify"||#request.handlemethod=="add"'>
		<script type="text/javascript"
			src="${ctx}/javascript/handleVaLeave.js"></script>
	</s:if>

	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${request.handlemethod}">
		<input type="hidden" id="copy" value="${request.copy}"> <input
			type="hidden" id="copyMode" value="${request.copyMode}">
		<s:form name="form1" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0"
								cellspacing="2">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.applicationCode"/>"
											class="text label"> <fmt:message
													key="biolims.common.applicationCode" />
										</label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentFault.id" id="instrumentFault_id"
												readonly="true"
												title='<fmt:message key="biolims.common.applicationCode"/> '
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title=<fmt:message key="biolims.common.type"/>
											class="text label"><fmt:message
													key="biolims.common.type" /></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden name="instrumentFault.type.id"
												id="instrumentFault_type_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="instrumentFault.type.name"
												id="instrumentFault_type_name"
												title='<fmt:message key="biolims.common.type"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="20" readonly="true"></s:textfield> <img
											alt='<fmt:message key="biolims.common.type"/>'
											id='searchtypr' src='${ctx}/images/img_lookup.gif'
											name='searchtypr' class='detail' /> <img alt=''
											id='searchtype' src='${ctx}/images/blank.gif'
											name='searchtype' class='detail' style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.describe"/>"
											class="text label"> <fmt:message
													key="biolims.common.describe" />
										</label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><textarea class="text input false"
												name="instrumentFault.note" id="instrumentFault_note"
												tabindex="0"
												title="<fmt:message key="biolims.common.describe"/>"
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)"
												style="overflow: hidden; width: 350px; height: 140px;"><s:property
													value="instrumentFault.note" /></textarea></td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.reportMaintenanceUser"/>"
											class="text label"><fmt:message key="biolims.common.reportMaintenanceUser"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden name="instrumentFault.handleUser.id"
												id="instrumentFault_handleUser_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentFault.handleUser.name"
												id="instrumentFault_handleUser_name" readonly="true"
												title='<fmt:message key="biolims.common.reporter"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="20"></s:textfield> <img alt=''
											id='main_grid1_row1_col2_sec2_4_detail'
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col2_sec2_4_detail' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.auditor"/>"
											class="text label"><fmt:message key="biolims.common.auditor"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden name="instrumentFault.confirmUser.id"
												id="instrumentFault_confirmUser_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlyfalse true"
												name="instrumentFault.confirmUser.name"
												id="instrumentFault_confirmUser_name"
												title=''
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="20" readonly="true"></s:textfield> <img
											alt=''
											id='searchUser' src='${ctx}/images/img_lookup.gif'
											name='searchUser' class='detail' /></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.reportDate"/>"
													class="text label"> <fmt:message
															key="biolims.common.reportDate" /></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><input type="text"
													class="input_parts text input default  readonlytrue false"
													name="instrumentFault.handleDate"
													id="instrumentFault_handleDate"
													value="<s:date name="instrumentFault.handleDate" format="yyyy-MM-dd"/>"
													readonly="readOnly"
													title='<fmt:message key="biolims.common.reportDate"/>'
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="20"
													maxlength="20" /><img alt=''
													id='main_grid1_row1_col2_sec2_4_detail'
													src='${ctx}/images/blank.gif'
													name='main_grid1_row1_col2_sec2_4_detail' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.workflowState"/>"
													class="text label"><fmt:message
															key="biolims.common.workflowState" /></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="instrumentFault.stateName" readonly="true"
														id="instrumentFault_stateName"
														title='<fmt:message key="biolims.common.workflowState"/>'
														onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="10"
														maxlength="30"></s:textfield> <s:hidden
														name="instrumentFault.state" id="instrumentFault.state"></s:hidden>
												</td>
											</tr>

										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value="" />
			<input type="hidden" name="jsonDataStr1" id="jsonDataStr1" value="" />
			<input type="hidden" name="jsonDataStr2" id="jsonDataStr2" value="" />
		</s:form>
		<input type="hidden" id="handlemethod" value="${handlemethod}" /> <input
			type="hidden" id="handlemethod" value="${copyMode}" /> <input
			type="hidden" id="id" value="${id}" /> <input type="hidden"
			id="instrumentFaultId" value="${instrumentFaultId}" /> <input
			type="hidden" id="instrumentFaultDetailId"
			value="${instrumentFaultDetailId}" />	
		<!-- <g:GridEditTag isAutoWidth="true"
							height="parent.document.body.clientHeight-125"
							validateMethod="validateFun" pageSize="100000" col="${col}"
							statement="${statement}" type="${type}" title="" url="${path}"
							statementLisener="${statementLisener}"
							handleMethod="${handlemethod}" /> -->
		<div id='bat_svnuploadcsv_div' style="display: none">
			<input type="file" name="file" id="file-svnuploadcsv"><fmt:message key="biolims.common.upLoadFile"></fmt:message>
		</div>
		<div id='grid' style='width: 100%;'></div>
		</div>
</body>
</html>
