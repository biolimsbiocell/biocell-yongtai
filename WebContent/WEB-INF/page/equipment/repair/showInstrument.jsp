﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript"
	src="${ctx}/javascript/equipment/repair/showInstrumentRepairTaskList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<%-- <g:LayOutWinTag buttonId="showStorageType" title='' hasHtmlFrame="true" --%>
<%-- 	html="${ctx}/storage/common/storageTypeSelect.action" --%>
<%-- 	isHasSubmit="false" functionName="instrument" hasSetFun="true" --%>
<%-- 	documentId="instrument_type_id" documentName="instrument_type_name" /> --%>
</head>
<body>
	<input type="hidden" id="gid" value="${requestScope.gid}">
<!-- 	<div id="jstj" style="display: none"> -->
<!-- 		<input type="hidden" id="selectId" /> <input type="hidden" -->
<!-- 			id="extJsonDataString" name="extJsonDataString"> -->
		
<!-- 	</div> -->
	<div id="show_instrument_div">
	<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString">
	<form id="searchForm">
			<table >
				<tr>
				<td>
					<td class="label-title">EQL</td>
					<td align="left"><input type="text" size="20" maxlength="18"
						id="instrument_id" name="id" searchField="true" title="EQL" />
					</td>
					<td class="label-title"><fmt:message key="biolims.common.instrument"/></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="instrument_name" name="name" searchField="true"
						title="" />
					</td>
					<td class="label-title"><fmt:message key="biolims.common.cosClassify"/></td>
					<td align="left"><input type="text" size="20" maxlength="60"
						id="instrument_type_name" name="type.name" searchField="true"
						title="" />
					</td>
					<td class="label-title" ><fmt:message key="biolims.common.cosType"/></td>
                   	<td align="left"  >
					<select id="instrument_type" name="p_type" searchField="true" >
								<option value=""  ><fmt:message key="biolims.common.pleaseSelect"/></option>
								<option value="1" ><fmt:message key="biolims.common.equipment"/></option>
								<option value="2" ><fmt:message key="biolims.common.refrigerators"/></option>
								<option value="3" ><fmt:message key="biolims.common.transferpettor"/></option>
								<option value="4" ><fmt:message key="biolims.common.humidity"/></option>
					</select>
                   	</td>
                   	<td>
                   	<input type="button" onclick="searchInstrument()" value=<fmt:message key="biolims.common.search"/>>
                   	</td>
                   	</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>

