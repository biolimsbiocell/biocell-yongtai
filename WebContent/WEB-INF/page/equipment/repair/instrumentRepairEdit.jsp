<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title></title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style>
			.dataTables_scrollBody {
				min-height: 100px;
			}
			
			#btn_submit{
			  display:none;
			}
		</style>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
			<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
		</div>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<div style="height: 14px"></div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 46px">
			<!--Form表单-->
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.common.instrumentRepair"/>
						<small style="color: #fff;"> <fmt:message key="biolims.equipment.taskId"/>: <span id="instrumentRepair_id"><s:property value="instrumentRepair.id" /></span>
								<fmt:message key="biolims.common.commandPerson"/>: <span
								userId="<s:property value="instrumentRepair.creatUser.id"/>"
								id="instrumentRepair_createUser"><s:property
										value="instrumentRepair.createUser.name" /></span> <fmt:message key="biolims.common.commandTime"/>: <span
								id="instrumentRepair_createDate"><s:date name=" instrumentRepair.createDate " format="yyyy-MM-dd "/></span> <fmt:message key="biolims.tInstrument.state"/>: <span
								state="<s:property value="instrumentRepair.state"/>"
								id="headStateName"><s:property
										value="instrumentRepair.stateName" /></span>
							</small>
					</h3>

						<div class="box-tools pull-right" style="display: none;">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="$('.buttons-print').click();">
											<fmt:message key="biolims.common.print" />
										</a>
									</li>
									<li>
										<a href="#####" onclick="$('.buttons-copy').click();">
											<fmt:message key="biolims.common.copyData" />
										</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" onclick="itemFixedCol(2)">
											<fmt:message key="biolims.common.lock2Col" />
										</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">
											<fmt:message key="biolims.common.cancellock2Col" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body">

						<!--form表单-->
						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<input type="hidden" id="bpmTaskId" value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<input type="hidden" size="20" maxlength="25" id="instrumentRepairId" name="id" value="<s:property value=" instrumentRepair.id "/>" />
											<input type="hidden" size="20" maxlength="25" id="instrumentRepairState" name="state" value="<s:property value=" instrumentRepair.state "/>" />

											<input type="hidden" size="20" maxlength="25" id="instrumentRepairCreatDate" name="createDate" value="<s:date name=" createDate " format="yyyy-MM-dd "/>" />

											<input type="hidden" size="20" maxlength="25" id="instrumentRepairCreateUserId" name="createUser.id" value="<s:property value=" instrumentRepair.createUser.id "/>" />

											<input type="hidden" size="20" maxlength="25" id="instrumentRepairStateName" name="stateName" value="<s:property value=" instrumentRepair.stateName "/>" />
										</div>
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepair.name"/> </span>

											<input type="text" size="50" maxlength="50" id="instrumentRepair_name" name="name" changelog="<s:property value=" instrumentRepair.name "/>" class="form-control" value="<s:property value=" instrumentRepair.name "/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepair.factEndDate"/> </span>

											<input type="text" size="20" maxlength="25" id="instrumentRepair_factEndDate" name="factEndDate" changelog="<s:property value=" instrumentRepair.factEndDate"/>" class="form-control" class="Wdate" value="<s:date name=" instrumentRepair.factEndDate " format="yyyy-MM-dd "/>" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepair.verify"/> </span>

											<input type="text" size="50" maxlength="100" id="instrumentRepair_verify" name="verify" changelog="<s:property value=" instrumentRepair.verify"/>" class="form-control" value="<s:property value=" instrumentRepair.verify "/>" />

										</div>
									</div>
								</div>
								<input type="hidden" id="id_parent_hidden" value="<s:property value=" instrumentRepair.id "/>" />
								<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value=" instrumentRepair.fieldContent "/>" />

							</form>
						</div>
						<!--table表格-->
					</div>
					<div class="box-footer ipadmini">
						<!--库存主数据-->
						<div id="leftDiv" class="col-xs-4">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.instrumentName"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table class="table table-hover table-striped table-bordered table-condensed" id="instrumentRepairTempdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-xs-8">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.detailed"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table class="table table-hover table-striped table-bordered table-condensed" id="instrumentRepairTaskdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>

	
		</div>
		</div>
		</div>
		<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
		<script type="text/javascript" src="${ctx}/javascript/equipment/repair/instrumentRepairEdit.js"></script>
		<script type="text/javascript" src="${ctx}/javascript/equipment/repair/instrumentRepairTask.js"></script>
		<script type="text/javascript" src="${ctx}/javascript/equipment/repair/instrumentRepairTemp.js"></script>
		<%-- <script type="text/javascript"
				src="${ctx}/javascript/common/dataTablesExtend.js"></script> --%>
	</body>

</html>