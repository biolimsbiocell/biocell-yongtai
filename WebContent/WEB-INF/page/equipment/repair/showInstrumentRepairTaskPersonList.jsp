<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="instrumentRepairTaskId:'${instrumentRepairTaskId}'"
	funName="modifyData"
	url="${ctx}/equipment/repair/saveInstrumentRepairTaskPerson.action" />
<g:HandleDataExtTag funName="delData"
	url="${ctx}/equipment/repair/delInstrumentRepairTaskPerson.action" />
<g:LayOutWinTag title='<fmt:message key="biolims.common.selectTypeOfWork"/>' hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	isHasButton="false" functionName="gz" hasSetFun="true" extRec="id,name"
	extStr="var record = gridGrid.getSelectionModel().getSelected();	record.set('workType-id',id);	record.set('workType-name',name);" />
<g:LayOutWinTag title='<fmt:message key="biolims.common.selectPersonnel"/>' hasHtmlFrame="true"
	html="${ctx}/core/user/userSelect.action" isHasButton="false"
	isHasSubmit="false" functionName="showUser" hasSetFun="true"
	extRec="id,name"
	extStr="var record = gridGrid.getSelectionModel().getSelected();	record.set('user-id',id);	record.set('user-name',name);" />
<script type="text/javascript"
	src="${ctx}/javascript/equipment/repair/showInstrumentRepairTaskPersonList.js"></script>
</head>
<body>
	<div class="mainlistclass" id="markup">
		<input type="hidden" id="id" value="" />
		<g:GridEditTag isAutoWidth="true"
			height="parent.document.body.clientHeight-70" col="${col}"
			statement="${statement}" type="${type}" title=""
			newObjCode=" 'instrumentRepairTask-id':'${instrumentRepairTaskId}'"
			url="${path}" statementLisener="${statementLisener}"
			handleMethod="${handlemethod}" />
	</div>
</body>
</html>