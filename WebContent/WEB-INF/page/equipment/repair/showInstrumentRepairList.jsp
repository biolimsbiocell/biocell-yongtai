<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/repair/showInstrumentRepairList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>



<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td><span>&nbsp;</span> <label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message key="biolims.common.repairOrderCoding"/></label> <span>&nbsp;</span></td>
					<td nowrap class="quicksearch_control"><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="id" searchField="true" id="id" title="<fmt:message key="biolims.common.inputID"/>"
						style="width: 90"></td>
					
					
					<td><span>&nbsp;</span> <label title="<fmt:message key="biolims.common.maintenanceTypes"/>" class="text label"><fmt:message key="biolims.common.maintenanceTypes"/></label>
					</td>
					<%-- <td nowrap class="quicksearch_control"><input type="text"
						class="input_parts text input default  readonlyfalse false"
						readonly="readOnly" id="type_name" name="type.name" value=''
						title="<fmt:message key="biolims.common.depository"/> " size="10" maxlength="30" /> <input type="hidden"
						searchField="true" name="type.id" id="type_id" value=''> <img
						title='<fmt:message key="biolims.common.select"/>' id="weihutype" src='${ctx}/images/img_lookup.gif'
						class='detail' /></td> --%>
				</tr>
				<tr>
					<%-- <td>
						<span>&nbsp;</span>
						<label title="保管人" class="text label">保管人</label>
					</td>
					<td nowrap class="quicksearch_control">
						<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" id="storage_dutyUser" name="storage.dutyUser.name" value='' title="保管人 " size="10" maxlength="30" searchField="true" />
						<input type="hidden" searchField="true" name="storage.dutyUser.id" id="storage_dutyUser_id" value=''>
						<img title='选择' id="searchUser" src='${ctx}/images/img_lookup.gif' class='detail' />
					</td> --%>

				</tr>
				<%-- <tr>
				<td class="label-title">维修日期</td>
			    <td>
					<input type="text" class="Wdate" readonly="readonly" id=createDate1 name="createDate##@@##>=##@@##date" searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
				</td>
				<td class="label-title">到</td>
			    <td>
					<input type="text" class="Wdate" readonly="readonly" id="createDate2" name="createDate##@@##<=##@@##date"  searchField="true"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" />
				</td>
			</tr> --%>
			</table>
		</div>
		<form name='excelfrm' action='/common/exportExcel.action'
			method='POST'>
			<input type='hidden' id='gridhtm' name='gridhtm' value='' />
		</form>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' />
		<input type="hidden" id="p_type" value="${requestScope.p_type}">
		<div id='grid'></div>
		<div id='gridcontainer'></div>

	</div>

</body>
</html>