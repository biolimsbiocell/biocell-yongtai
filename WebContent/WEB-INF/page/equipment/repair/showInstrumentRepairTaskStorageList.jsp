<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/repair/showInstrumentRepairTaskStorageList.js"></script>
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="instrumentRepairTaskId:'${instrumentRepairTaskId}'"
	funName="modifyData"
	url="${ctx}/equipment/repair/saveInstrumentRepairTaskStorage.action" />
<g:HandleDataExtTag funName="delData"
	url="${ctx}/equipment/repair/delInstrumentRepairTaskStorage.action" />
<g:LayOutWinTag title='<fmt:message key="biolims.common.selectTheObject"/>' hasHtmlFrame="true"
	html="${ctx}/storage/common/storageSelect.action" isHasSubmit="false"
	isHasButton="false" functionName="duixiangbianma1" hasSetFun="true"
	extRec="rec"
	extStr="var record = gridGrid.getSelectionModel().getSelected();record.set('storage-id',rec.get('id'));record.set('storage-name',rec.get('name'));record.set('price',rec.get('price'));" />
</head>
<body>
	<div class="mainlistclass" id="markup">
		<input type="hidden" id="id" value="" />
		<g:GridEditTag isAutoWidth="true"
			height="parent.document.body.clientHeight-70" col="${col}"
			statement="${statement}" type="${type}" title=""
			newObjCode="'instrumentRepairTask-id':'${instrumentRepairTaskId}'"
			url="${path}" statementLisener="${statementLisener}"
			handleMethod="${handlemethod}" />
	</div>
</body>
</html>