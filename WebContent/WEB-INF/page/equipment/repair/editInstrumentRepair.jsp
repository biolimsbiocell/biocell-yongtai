<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>biolims</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="searchtype"
		title='<fmt:message key="biolims.common.selectMaintenanceTypes"/>'
		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
		isHasSubmit="false" functionName="whlx" hasSetFun="true"
		documentId="instrumentRepair_type_id"
		documentName="instrumentRepair_type_name" />

	<%
		if (request
					.getAttribute("instrumentRepair.instrumentRepairPlan") == null) {
	%>
	<g:LayOutWinTag buttonId="showInstrumentRepairPlan"
		title='<fmt:message key="biolims.common.selectMaintenanceCode"/>'
		hasHtmlFrame="true" hasSetFun="true"
		html="${ctx}/equipment/plan/instrumentRepairPlanSelect.action"
		isHasSubmit="false" functionName="showInstrumentRepairPlanFun"
		extRec="rec"
		extStr="document.getElementById('instrumentRepair_instrumentRepairPlan_id').value=rec.get('id'); document.getElementById('instrumentRepair_storage_id').value=rec.get('storage-id');document.getElementById('instrumentRepair_storage_note').value=rec.get('storage-note'); 
		var store1 = new Ext.data.JsonStore({
		root: 'results',
		totalProperty: 'total',
		remoteSort: true,
		fields: [{name:'id',type: 'string'},{name:'type-id',type: 'string'},{name:'type-name',type: 'string'},{name:'note',type: 'string'}],
		proxy: new Ext.data.HttpProxy({url: window.ctx+'/equipment/repair/showInstrumentRepairPlanTaskListJson.action?instrumentRepairPlanId='+document.getElementById('instrumentRepair_instrumentRepairPlan_id').value,method: 'POST'})
		});
		var cm1 = new  Ext.grid.ColumnModel(
	    [new Ext.grid.RowNumberer(),
	    {dataIndex:'id',header: '任务单号',width: 120,sortable: true,hidden: false,editable: true,tooltip: '可编辑',editor: id},
	    {dataIndex:'type-id',header: '类型',width: 120,sortable: true,hidden: true,editable: true,tooltip: '可编辑',editor: type},
	    {dataIndex:'type-name',header: '类型名称',width: 150,sortable: true,hidden: false,editable: true,tooltip: '可编辑啊',editor: type},
	    {dataIndex:'note',header: '描述',width: 300,sortable: true,editable: true,tooltip: '可编辑',editor: new Ext.form.TextArea()}]
	  	);
	    gridGrid.reconfigure(store1,cm1);
		var o1 = {start:0,limit:1000};
		gridGrid.pageSize=1000;
		gridGrid.store.load({params:o1});
		setTimeout('setGridId()',1000); 
	" />
	<%
		}
	%>
	<g:LayOutWinTag buttonId="showUserGroup"
		title='<fmt:message key="biolims.common.selectTheWorkingGroup"/>'
		hasHtmlFrame="true" hasSetFun="true"
		html="${ctx}/core/userGroup/userGroupSelect.action"
		isHasSubmit="false" functionName="showUserGroupFun" extRec="rec"
		extStr="document.getElementById('instrumentRepair_repairTeam_id').value=rec.get('id'); document.getElementById('instrumentRepair_repairTeam_name').value=rec.get('name');" />
	<g:LayOutWinTag buttonId="searchfault"
		title='<fmt:message key="biolims.common.selectTheTroubleTicketCode"/>'
		hasHtmlFrame="true" hasSetFun="true"
		html="${ctx}/equipment/fault/instrumentFaultSelect.action"
		isHasSubmit="false" functionName="searchfaultFun" extRec="rec"
		extStr="document.getElementById('instrumentRepair_instrumentFault_id').value=rec.get('id'); document.getElementById('instrumentRepair_storage_id').value=rec.get('storage-id'); document.getElementById('instrumentRepair_storage_note').value=rec.get('storage-note'); " />
	<g:LayOutWinTag buttonId="searchdepartment"
		title='<fmt:message key="biolims.common.chooseDepartment"/>'
		hasHtmlFrame="true"
		html="${ctx}/core/department/departmentSelect.action"
		isHasSubmit="false" functionName="searchdepartmentfun"
		hasSetFun="true" documentId="instrumentRepair_department_id"
		documentName="instrumentRepair_department_name" />
	<g:LayOutWinTag
		title='<fmt:message key="biolims.common.selectTheObject"/>'
		hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
		isHasSubmit="false" isHasButton="false" functionName="wxrwlx"
		hasSetFun="true" extRec="id,name"
		extStr="var record = gridGrid.getSelectionModel().getSelected();	record.set('type-id',id);	record.set('type-name',name);" />
	<g:HandleDataExtTag hasConfirm="false"
		paramsValue="instrumentRepairId:'${instrumentRepairId}',instrumentRepairTaskId:'${instrumentRepairTaskId}'"
		funName="modifyData"
		url="${ctx}/equipment/mainRepair/saveInstrumentRepairTask.action" />
	<g:HandleDataExtTag funName="delData"
		url="${ctx}/equipment/repair/delInstrumentRepairTask.action" />
	<g:LayOutWinTag buttonId="searchstrogecode"
		title='<fmt:message key="biolims.common.chooseFinancialSubjects"/>'
		hasHtmlFrame="true" html="${ctx}/financeItem/financeItemSelect.action"
		isHasSubmit="false" functionName="cwkm1" hasSetFun="true"
		extRec="id,name"
		extStr="document.getElementById('instrumentRepair_materialCode_name').value=name;	document.getElementById('instrumentRepair_materialCode_id').value=id;" />
	<g:LayOutWinTag buttonId="searchrepaircode"
		title='<fmt:message key="biolims.common.chooseFinancialSubjects"/>'
		hasHtmlFrame="true" html="${ctx}/financeItem/financeItemSelect.action"
		isHasSubmit="false" functionName="cwkm2" hasSetFun="true"
		extRec="id,name"
		extStr="document.getElementById('instrumentRepair_repairCode_name').value=name;	document.getElementById('instrumentRepair_repairCode_id').value=id;" />
	<g:LayOutWinTag buttonId="searchpersoncode"
		title='<fmt:message key="biolims.common.chooseFinancialSubjects"/>'
		hasHtmlFrame="true" html="${ctx}/financeItem/financeItemSelect.action"
		isHasSubmit="false" functionName="cwkm3" hasSetFun="true"
		extRec="id,name"
		extStr="document.getElementById('instrumentRepair_personCode_name').value=name;	document.getElementById('instrumentRepair_personCode_id').value=id;" />
	<g:HandleDataExtTag funName="delStorageData"
		url="${ctx}/equipment/repair/delInstrumentRepairTaskStorage.action" />
	<g:HandleDataExtTag funName="delPersonData"
		url="${ctx}/equipment/repair/delInstrumentRepairTaskPerson.action" />

</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/repair/editInstrumentRepair.js"></script>
	<div id="instrumentRepairTaskTempPage" style="width: 25%; float: left"></div>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass" >
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<s:form name="form1" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td class="sectioncol" valign="top">
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.repairOrderCoding"/>"
											class="text label"><fmt:message
													key="biolims.common.repairOrderCoding" /> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentRepair.id" id="instrumentRepair_id"
												readonly="true"
												title='<fmt:message key="biolims.common.repairOrderCoding"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<%-- 	<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.preventiveMaintenanceCode"/>"
											class="text label"><fmt:message
													key="biolims.common.preventiveMaintenanceCode" /> </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="instrumentRepair.instrumentRepairPlan.id"
												id="instrumentRepair_instrumentRepairPlan_id"
												readonly="true"
												title='<fmt:message key="biolims.common.preventiveMaintenanceCode"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"></s:textfield> <img
											alt='<fmt:message key="biolims.common.preventiveMaintenanceCode"/>'
											id='showInstrumentRepairPlan'
											src='${ctx}/images/img_lookup.gif' class='detail' /> <img
											class="detail" src="${ctx}/images/menu_icon_link.gif"
											style="display: none" onclick="viewInstruentFaultPlan();"
											alt="<fmt:message key="biolims.common.viewTheTroubleTicket"/>"></td>
										<td nowrap>&nbsp;</td>
									</tr> --%>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.commandTime"/>"
											class="text label"><fmt:message
													key="biolims.common.commandTime" /></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text"
											class="input_parts text input default  readonlytrue false"
											readonly="readOnly" name="instrumentRepair.createDate"
											id="instrumentRepair_createDate"
											value="<s:date name="instrumentRepair.createDate" format="yyyy-MM-dd"/>"
											title="<fmt:message key="biolims.common.commandTime"/>"
											onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="30">
											</input> <img alt='' id='main_grid1_row1_col2_sec2_3_detail'
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col2_sec2_3_detail' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.maintenanceDate"/>"
											class="text label"><fmt:message key="biolims.common.maintenanceDate"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><input type="text" Class="Wdate"
											onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})"
											name="instrumentRepair.factStartDate"
											id="instrumentRepair_factStartDate"
											value="<s:date name="instrumentRepair.factStartDate" format="yyyy-MM-dd"/>"
											title="<fmt:message key="biolims.common.actualStartDate"/> "
											onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="20" maxlength="50">
											<img alt='' src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>

									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.describe"/>"
											class="text label"><fmt:message
													key="biolims.common.describe" /></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default readonlyfalse false"
												name="instrumentRepair.name" id="instrumentRepair_name"
												title='<fmt:message key="biolims.common.describe"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="30"
												maxlength="50"></s:textfield></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.commandPerson"/>" class="text label"><fmt:message key="biolims.common.commandPerson"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px"></td>
										<td align="left" nowrap><input type="text"
											class="input_parts text input default  readonlytrue false"
											readonly="readOnly" name="instrumentRepair.createUser.name"
											id="instrumentRepair_createUser_name"
											value="<s:property value="instrumentRepair.createUser.name"/>"
											title='创建人' onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="10" maxlength="20">
											<input type="hidden" name="instrumentRepair.createUser.id"
											id="instrumentRepair_createUser_id"
											value="<s:property value="instrumentRepair.createUser.id"/>">
										</td>
									</tr>
									<!--维修价格 -->
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											class="text label"><fmt:message key="biolims.common.maintenancePrice"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px"></td>
										<td align="left" nowrap><input type="text"
											class="input_parts text input default readonlyfalse false"
											name="instrumentRepair.repairPrice"
											id="instrumentRepair_repairPrice"
											onblur="textbox_ondeactivate(event);"
											value="<s:property value="instrumentRepair.repairPrice"/>"
											onfocus="shared_onfocus(event,this)" size="30" maxlength="30"></td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.workflowState"/>"
													class="text label"><fmt:message
															key="biolims.common.workflowState" /></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:textfield
														cssClass="input_parts text input default  readonlytrue false"
														name="instrumentRepair.stateName"
														id="instrumentRepair_stateName"
														title='<fmt:message key="biolims.common.workflowState"/>'
														readonly="true" onblur="textbox_ondeactivate(event);"
														onfocus="shared_onfocus(event,this)" size="10"
														maxlength="30"></s:textfield></td>
												<s:hidden name="instrumentRepair.state"
													id="instrumentRepair_state"></s:hidden>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.maintenanceUnit"/>" class="text label"><fmt:message key="biolims.common.maintenanceUnit"/></label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td align="left" nowrap><input type="text"
													class="input_parts text input default readonlyfalse false"
													name="instrumentRepair.verify" id="instrumentRepair_verify"
													title='<fmt:message key="biolims.common.maintenanceUnit"/>' onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="30"
													maxlength="30"
													value="<s:property value="instrumentRepair.verify"/>"
													></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<%-- <tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.maintenanceTypes"/>"
													class="text label"><fmt:message
															key="biolims.common.maintenanceTypes" /> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px">
												</td>
												<td nowrap><s:hidden name="instrumentRepair.type.id"
														id="instrumentRepair_type_id"></s:hidden> <s:textfield
														cssClass="input_parts text input default  readonlyfalse false"
														name="instrumentRepair.type.name"
														id="instrumentRepair_type_name"
														title='<fmt:message key="biolims.common.maintenanceTypes"/>'
														onblur="textbox_ondeactivate(event);" readonly="true"
														onfocus="shared_onfocus(event,this)" size="10"
														maxlength="30"></s:textfield> <img
													alt='<fmt:message key="biolims.common.selectMaintenanceTypes"/>'
													id='searchtype' src='${ctx}/images/img_lookup.gif'
													name='searchtype' class='detail' /></td>
												<td nowrap>&nbsp;</td>
											</tr> --%>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
<!-- 			<div class="section standard_section  "> -->
<!-- 				<div class="section"> -->
<!-- 					<table width="100%" class="section_table" cellpadding="0"> -->
<!-- 						<tbody> -->
<!-- 							<tr width='100%' class="sectionrow " valign="top"> -->
<!-- 								<td class="sectioncol " width='50%' valign="top" align="right"> -->
<!-- 									<div class="section standard_section marginsection  "> -->
<!-- 										 <div class="section_header standard_section_header"> -->
<!-- 											<table width="100%" cellpadding="0" cellspacing="0"> -->
<!-- 												<tbody> -->
<!-- 													<tr width="100%"> -->
<!-- 														<td -->
<!-- 															class="section_header_left text standard_section_label labelcolor" -->
<%-- 															align="left"><span class="section_label"> <fmt:message --%>
<%-- 																	key="biolims.common.maintenanceDate" /> --%>
<!-- 														</span></td> -->
<!-- 														<td class="section_header_right" align="right"></td> -->
<!-- 													</tr> -->
<!-- 												</tbody> -->
<!-- 											</table> -->
<!-- 										</div>  -->
<!-- 										 	<div width="100%" height="" class=" section_content_border"> -->
<!-- 											<table width="100%" class="section_table" cellpadding="0"> -->
<!-- 												<tbody> -->
<!-- 													<tr> -->
<!-- 														<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 															<span class="labelspacer">&nbsp;&nbsp;</span> <label -->
<%-- 															title="<fmt:message key="biolims.common.situationATCOTPOEAM"/>" --%>
<!-- 															class="text label">故障原因</label> -->
<!-- 														</td> -->
<!-- 														<td class="requiredcolumn" nowrap width="10px"> --%>
<!-- 														</td> -->
<!-- 														<td nowrap><textarea class="text input false" -->
<!-- 																name="instrumentRepair.maintain" -->
<!-- 																id="instrumentRepair_maintain" tabindex="0" -->
<%-- 																title='<fmt:message key="biolims.common.situationATCOTPOEAM"/>' --%>
<!-- 																onblur="textbox_ondeactivate(event);" -->
<!-- 																onfocus="shared_onfocus(event,this)" -->
<%-- 																style="overflow: hidden; width: 280px; height: 40px;"><s:property --%>
<%-- 																	value="instrumentRepair.maintain" /></textarea></td> --%>
<!-- 														<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 															<span class="labelspacer">&nbsp;&nbsp;</span> <label -->
<%-- 															title="<fmt:message key="biolims.common.afterMaintenanceCheckTheCalibrationRecord"/>" --%>
<!-- 															class="text label">维修方式及备注</label> -->
<!-- 														</td> -->
<!-- 														<td class="requiredcolumn" nowrap width="10px"> --%>
<!-- 														</td> -->
<!-- 														<td nowrap><textarea class="text input  false" -->
<!-- 																name="instrumentRepair.note" id="instrumentRepair_note" -->
<!-- 																tabindex="0" title='' -->
<!-- 																onblur="textbox_ondeactivate(event);" -->
<!-- 																onfocus="shared_onfocus(event,this)" -->
<%-- 																style="overflow: hidden; width: 280px; height: 40px;"><s:property --%>
<%-- 																	value="instrumentRepair.note" /></textarea></td> --%>
<!-- 													</tr> -->
<!-- 												</tbody> -->
<!-- 											</table> -->
<!-- 										</div>  -->
<!-- 										<div width="100%" height="" class=" section_content_border" -->
<!-- 											style="display: none"> -->
<!-- 											<table width="100%" class="section_table" cellpadding="0"> -->
<!-- 												<tbody> -->
<!-- 													<tr class="control textboxcontrol" -->
<%-- 														label="<fmt:message key="biolims.common.timeInformation"/>"> --%>
<!-- 														<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 															<span class="labelspacer">&nbsp;&nbsp;</span> <label -->
<%-- 															title="<fmt:message key="biolims.common.plansToStartDate"/>" --%>
<%-- 															class="text label"><fmt:message --%>
<%-- 																	key="biolims.common.plansToStartDate" /></label> --%>
<!-- 														</td> -->
<!-- 														<td class="requiredcolumn" nowrap width="10px"> --%>
<!-- 														</td> -->
<!-- 														<td nowrap><input type="text" Class="Wdate" -->
<!-- 															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})" -->
<!-- 															name="instrumentRepair.startDate" -->
<!-- 															id="instrumentRepair_startDate_name" -->
<%-- 															value="<s:date name="instrumentRepair.startDate" format="yyyy-MM-dd"/>" --%>
<%-- 															title='<fmt:message key="biolims.common.plansToStartDate"/>' --%>
<!-- 															onblur="textbox_ondeactivate(event);" -->
<!-- 															onfocus="shared_onfocus(event,this)" size="20" -->
<!-- 															maxlength="50"> </input> <img alt='' -->
<%-- 															src='${ctx}/images/blank.gif' class='detail' --%>
<!-- 															style='display: none' /></td> -->
<!-- 														<td nowrap>&nbsp;</td> -->
<!-- 														<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 															<span class="labelspacer">&nbsp;&nbsp;</span> <label -->
<%-- 															title="<fmt:message key="biolims.common.plansToEndDate"/>" --%>
<%-- 															class="text label"><fmt:message --%>
<%-- 																	key="biolims.common.plansToEndDate" /></label> --%>
<!-- 														</td> -->
<!-- 														<td class="requiredcolumn" nowrap width="10px"> --%>
<!-- 														</td> -->
<!-- 														<td nowrap><input type="text" Class="Wdate" -->
<!-- 															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})" -->
<!-- 															name="instrumentRepair.endDate" -->
<!-- 															id="instrumentRepair_endDate" -->
<%-- 															value="<s:date name="instrumentRepair.endDate" format="yyyy-MM-dd"/>" --%>
<%-- 															title='<fmt:message key="biolims.common.plansToEndDate"/>' --%>
<!-- 															onblur="textbox_ondeactivate(event);" -->
<!-- 															onfocus="shared_onfocus(event,this)" size="20" -->
<!-- 															maxlength="50"> </input> <img alt='' -->
<%-- 															src='${ctx}/images/blank.gif' class='detail' --%>
<!-- 															style='display: none' /></td> -->
<!-- 														<td nowrap>&nbsp;</td> -->
<!-- 													</tr> -->
<!-- 													<tr class="control textboxcontrol" -->
<%-- 														label="<fmt:message key="biolims.common.actualDate"/>"> --%>
<!-- 														<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 															<span class="labelspacer">&nbsp;&nbsp;</span> <label -->
<%-- 															title="<fmt:message key="biolims.common.actualStartDate"/>" --%>
<%-- 															class="text label"><fmt:message --%>
<%-- 																	key="biolims.common.actualStartDate" /></label> --%>
<!-- 														</td> -->
<!-- 														<td class="requiredcolumn" nowrap width="10px"> --%>
<!-- 														</td> -->
<!-- 														<td nowrap><input type="text" Class="Wdate" -->
<!-- 															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})" -->
<!-- 															name="instrumentRepair.factStartDate" -->
<!-- 															id="instrumentRepair_factStartDate" -->
<%-- 															value="<s:date name="instrumentRepair.factStartDate" format="yyyy-MM-dd"/>" --%>
<%-- 															title='<fmt:message key="biolims.common.actualStartDate"/>' --%>
<!-- 															onblur="textbox_ondeactivate(event);" -->
<!-- 															onfocus="shared_onfocus(event,this)" size="20" -->
<!-- 															maxlength="50"> </input> <img alt='' -->
<%-- 															src='${ctx}/images/blank.gif' class='detail' --%>
<!-- 															style='display: none' /></td> -->
<!-- 														<td nowrap>&nbsp;</td> -->
<!-- 														<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 															<span class="labelspacer">&nbsp;&nbsp;</span> <label -->
<%-- 															title="<fmt:message key="biolims.common.actualEndDate"/>" --%>
<%-- 															class="text label"><fmt:message --%>
<%-- 																	key="biolims.common.actualEndDate" /></label> --%>
<!-- 														</td> -->
<!-- 														<td class="requiredcolumn" nowrap width="10px"> --%>
<!-- 														</td> -->
<!-- 														<td nowrap><input type="text" Class="Wdate" -->
<!-- 															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})" -->
<!-- 															name="instrumentRepair.factEndDate" -->
<!-- 															id="instrumentRepair_factEndDate" -->
<%-- 															value="<s:date name="instrumentRepair.factEndDate" format="yyyy-MM-dd"/>" --%>
<%-- 															title='<fmt:message key="biolims.common.actualEndDate"/>' --%>
<!-- 															onblur="textbox_ondeactivate(event);" -->
<!-- 															onfocus="shared_onfocus(event,this)" size="20" -->
<!-- 															maxlength="50"> </input> <img alt='' -->
<%-- 															src='${ctx}/images/blank.gif' class='detail' --%>
<!-- 															style='display: none' /></td> -->
<!-- 														<td nowrap>&nbsp;</td> -->
<!-- 													</tr> -->
<!-- 													<tr> -->
<!-- 														<td height="2"></td> -->
<!-- 													</tr> -->
<!-- 												</tbody> -->
<!-- 											</table> -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 						</tbody> -->
<!-- 					</table> -->
<!-- 				</div> -->
<!-- 			</div> -->
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value="" />
			<input type="hidden" name="jsonDataStr1" id="jsonDataStr1" value="" />
			<input type="hidden" name="jsonDataStr2" id="jsonDataStr2" value="" />
		</s:form>
		<input type="hidden" id="handlemethod" value="${handlemethod}" /> <input
			type="hidden" id="handlemethod" value="${copyMode}" /> <input
			type="hidden" id="id" value="${id}" /> <input type="hidden"
			id="instrumentRepairId" value="${instrumentRepairId}" /> <input
			type="hidden" id="instrumentRepairTaskId"
			value="${instrumentRepairTaskId}" />


		<!-- <g:GridEditTag isAutoWidth="true"
							height="parent.document.body.clientHeight-125"
							validateMethod="validateFun" pageSize="100000" col="${col}"
							statement="${statement}" type="${type}" title="" url="${path}"
							statementLisener="${statementLisener}"
							handleMethod="${handlemethod}" /> -->
		<div id='bat_svnuploadcsv_div' style="display: none">
			<input type="file" name="file" id="file-svnuploadcsv">上传CSV文件
		</div>
		<div id="instrumentRepairTaskItempage" width="100%"></div>
	</div>
	<input type="hidden" id="p_type" value="${requestScope.p_type}">
</body>
</html>
