<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/repair/showInstrumentRepairTaskList.js"></script>
<g:HandleDataExtTag hasConfirm="false"
	paramsValue="instrumentRepairId:'${instrumentRepairId}',instrumentRepairTaskId:'${instrumentRepairTaskId}'"
	funName="modifyData"
	url="${ctx}/equipment/repair/saveInstrumentRepairTask.action" />
<g:HandleDataExtTag funName="delData"
	url="${ctx}/equipment/repair/delInstrumentRepairTask.action" />
</head>
<g:LayOutWinTag title='<fmt:message key="biolims.common.selectTheObject"/>' width="400" hasHtmlFrame="true"
	html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false"
	isHasButton="false" functionName="lx" hasSetFun="true" extRec="id,name"
	extStr="var record = gridGrid.getSelectionModel().getSelected();	record.set('type-id',id);	record.set('type-name',name);" />
<body>
	<div class="mainlistclass" id="markup">
		<input type="hidden" id="id" value="" />
		<g:GridEditTag isAutoWidth="true"
			height="parent.document.body.clientHeight-125" col="${col}"
			statement="${statement}" type="${type}" title='<fmt:message key="biolims.common.maintenanceTasks"/>'
			tbl="{text:'<fmt:message key="biolims.common.repairKit"/>',  scope:this,  handler:function(b,e){var record = gridGrid.getSelectionModel().getSelected(); zjlb(record.get('id'));}},{text:'维修人员',  scope:this,  handler:function(b,e){var record = gridGrid.getSelectionModel().getSelected();chmx(record.get('id'));}}"
			url="${path}" statementLisener="${statementLisener}"
			handleMethod="${handlemethod}" />
	</div>
</body>
</html>