<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/plan/showInstrumentRepairPlanList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<g:LayOutWinTag buttonId="searchInstrument"
	title='<fmt:message key="biolims.common.selectTheInstrument"/>'
	hasHtmlFrame="true" hasSetFun="true"
	html="${ctx}/equipment/common/instrumentSelect.action"
	isHasSubmit="false" functionName="searchInstrumentList" extRec="rec"
	extStr="document.getElementById('storage_id').value=rec.get('id');" />
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td>
				<tr>
					<td align="left"><span>&nbsp;</span> <label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
							<fmt:message key="biolims.common.serialNumber" />
					</label></td>
					<td align="left"><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="id" onblur="textbox_ondeactivate(event);" searchField="true"
						id="id" title="<fmt:message key="biolims.common.inputID"/>" /></td>
					<td align="left"><span>&nbsp;</span> <label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message
								key="biolims.common.faultEquipment" /></label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="storage.id" searchField="true" id="storage_id"
						title="<fmt:message key="biolims.common.inputFaultEquipment"/>"
						style="width: 90"> <img
						title='<fmt:message key="biolims.common.select"/>'
						id="searchInstrument" src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>

					<td class="label-title"><fmt:message
							key="biolims.common.recordNumber" /></td>
					<td><input type="text" Class="input-10-length" name="limitNum"
						id="limitNum"
						title="<fmt:message key="biolims.common.inputTheRecordNumber"/>"></td>
				</tr>
				<!-- <tr>
							<td valign="top" align="right">
						<label title="开始日期" class="text label">申请日期从</label>
					</td>
					<td nowrap class="quicksearch_control" valign="middle" >
						<input type="text" searchField="true"  name="queryStartDate" readonly="readOnly" Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
							onblur="textbox_ondeactivate(event);" 
onfocus="shared_onfocus(event,this)" id="queryStartDate" title="开始日期">
					</td>
					<td valign="top" align="right">
						<label title="结束日期" class="text label">到</label>
					</td>
					<td nowrap class="quicksearch_control" valign="middle" >
						<input type="text" Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  name="queryEndDate"  id="queryEndDate"
							onblur="textbox_ondeactivate(event);" 
onfocus="shared_onfocus(event,this)" searchField="true"   title="结束日期">
					</td>
			</tr> -->
			</table>
		</div>
		<form name='excelfrm' action='/common/exportExcel.action'
			method='POST'>
			<input type='hidden' id='gridhtm' name='gridhtm' value='' />
		</form>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' />
		<div id='grid'></div>
		<div id='gridcontainer'></div>

	</div>

</body>
</html>