<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>


	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.common.instrumentCycleVerificationVlan" />
							<small style="color: #fff;"> <fmt:message key="biolims.equipment.taskId" />: <span id="instrumentRepairPlan_id"><s:property value="instrumentRepairPlan.id" /></span>
								<fmt:message key="biolims.common.commandPerson" />: <span
								userId="<s:property value="instrumentRepairPlan.creatUser.id"/>"
								id="instrumentRepairPlan_createUser"><s:property
										value="instrumentRepairPlan.createUser.name" /></span> 
								<fmt:message key="biolims.tInstrument.state" />: <span
								state="<s:property value="instrumentRepairPlan.state.id"/>"
								id="headStateName"><s:property
										value="instrumentRepairPlan.stateName" /></span>
							</small>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--进度条-->
				<%-- 	<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>设备周期检定计划</small>
								</span>
							</a></li>
										<li><a class="disabled step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>检定计划表</small></span>
								</a></li>
						</ul>
					</div> --%>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepairPlanTask.tsTime" /><img class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
                   						<input type="text"
											size="20" maxlength="25" id="instrumentRepairPlan_tsTime"
											name="tsTime"  changelog="<s:property value="instrumentRepairPlan.tsTime"/>"
											class="form-control"   
											value="<s:property value="instrumentRepairPlan.tsTime"/>"
											onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^0-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"
						 				/>   
					
									</div>
								</div>			
											
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepairPlan.period"/> <img class='requiredimage' src='${ctx}/images/required.gif' /></span> 
                   						<input type="text"
											size="20" maxlength="25" id="instrumentRepairPlan_period"
											name="period"  changelog="<s:property value="instrumentRepairPlan.period"/>"
											class="form-control"   
											value="<s:property value="instrumentRepairPlan.period"/>"
											onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^0-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"
										 />  
					
									</div>
								</div>		
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<input type="hidden"
											size="20" maxlength="25" id="instrumentRepairPlanId" name="id"   readonly = "readOnly" 
											value="<s:property value="instrumentRepairPlan.id"/>"
										 />
										<input type="hidden"
												size="20" maxlength="25" id="instrumentRepairPlan_stateName"
												name="stateName"  class="form-control"   
												value="<s:property value="instrumentRepairPlan.stateName"/>"
										/>		
										
										<input type="hidden"
												size="20" maxlength="25" id="instrumentRepairPlan_state"
												name="state-id"  class="form-control"   
												value="<s:property value="instrumentRepairPlan.state.id"/>"
										/>	
											
										<input type="hidden"
												id="instrumentRepairPlan_createUser_id" name="createUser-id"
												value="<s:property value="instrumentRepairPlan.createUser.id"/>"
										/>
										 <input type="hidden" id="instrumentRepairPlan_dutyUser_id" name="dutyUser-id"
											value="<s:property value="instrumentRepairPlan.dutyUser.id"/>"
										/>
										<span class="input-group-addon">
												<fmt:message key="biolims.tInstrumentRepairPlan.unit"/> 
											</span>
											<%-- <input type="hidden" name="unitName " changelog="<s:property value="instrumentRepairPlan.unitName"/>" id="instrumentRepairPlan_unitName " value="<s:property value=" instrumentRepairPlan.unitName " />" /> --%>  
									<select class="form-control" name="unitName">
											<option value="1"
												<s:if test="instrumentRepairPlan.unitName==1">selected="selected"</s:if>>
												<fmt:message key="biolims.common.day" />
											</option>
											<option value="2"
												<s:if test="instrumentRepairPlan.unitName==2">selected="selected"</s:if>>
											<fmt:message key="biolims.common.week" />
											</option>
											<option value="3"
												<s:if test="instrumentRepairPlan.unitName==3">selected="selected"</s:if>>
												<fmt:message key="biolims.common.month" />
											</option>
										</select>
									</div>
								</div>			
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepairPlan.note"/> </span> 
                   						<input type="text"
											size="50" maxlength="50" id="instrumentRepairPlan_note"
											name="note"  changelog="<s:property value="instrumentRepairPlan.note"/>"
											class="form-control"   
											value="<s:property value="instrumentRepairPlan.note"/>"
						 				/>  
					
									</div>
								</div>				
									<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.tInstrumentRepairPlan.type"/><img class='requiredimage' src='${ctx}/images/required.gif' /> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="instrumentRepairPlan_type_name"  changelog="<s:property value="instrumentRepairPlan.type.name"/>"
												value="<s:property value="instrumentRepairPlan.type.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="instrumentRepairPlan_type_id" name="type-id"
												value="<s:property value="instrumentRepairPlan.type.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showtype()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>		
									</div>
								<%-- <div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> <fmt:message key="biolims.common.customFields"/>
													
											</h3>
										</div>
									</div>
								</div> --%>
							<div id="fieldItemDiv"></div>
								
							<br> 
							<input type="hidden" name="instrumentRepairPlanTaskJson"
								id="instrumentRepairPlanTaskJson" value="" /> 
								
							<input type="hidden" name="instrumentRepairPlan.id"
								id="id_parent_hidden"
								value="<s:property value="instrumentRepairPlan.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="instrumentRepairPlan.fieldContent"/>" />	
								
						</form>
					</div>
					<!--table表格-->
					  					<!--库存主数据-->
						<div id="leftDiv" class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.instrumentName"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="instrumentTempdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div id="rightDiv" class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.common.detailed"/></h3>
								</div>
								<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="instrumentRepairPlanTaskdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/javascript/equipment/plan/instrumentRepairPlanEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/plan/instrumentRepairPlanTask.js"></script>
	<script type="text/javascript" 
		src="${ctx}/javascript/equipment/plan/instrumentPlanTemp.js"></script>
</body>

</html>	
