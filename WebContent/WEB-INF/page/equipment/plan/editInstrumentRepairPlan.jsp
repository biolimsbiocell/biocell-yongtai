<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>biolims</title>
<script type="text/javascript" src="WEB-INF/page/js/jquery-1.7.2.js"></script>
<script type="text/javascript">
$(function() {
	if ($("#instrumentRepairPlan_state_name").val() == "") {
		$("#instrumentRepairPlan_state_name").val("有效");
		$("#instrumentRepairPlan_state_id").val("1q");
	}
});
</script>
</head>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/plan/editInstrumentRepairPlan.js"></script>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showDicState = Ext.get('showDicState');
showDicState.on('click', 
 function showDicStateFun(){
var win = Ext.getCmp('showDicStateFun');
if (win) {win.close();}
var showDicStateFun= new Ext.Window({
id:'showDicStateFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/state/stateSelect.action?type=instrumentRepairPlan&flag=showDicStateFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 showDicStateFun.close(); }  }]  });     showDicStateFun.show(); }
);
});
 function setshowDicStateFun(id,name){
 document.getElementById("instrumentRepairPlan_state_id").value = id;
document.getElementById("instrumentRepairPlan_state_name").value = name;
var win = Ext.getCmp('showDicStateFun')
if(win){win.close();}
}
</script>

	<script type="text/javascript">
function modifyData(params){
var paramsValue = "instrumentRepairPlanId:'',instrumentRepairPlanTaskId:''";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : biolims.common.pleaseWait});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/mainRepairPlan/saveInstrumentRepairPlanTask.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : biolims.common.info,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>

	<script type="text/javascript">
function delData(params){
var paramsValue = "";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : biolims.common.pleaseWait});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/mainRepairPlan/delInstrumentRepairPlanTask.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : biolims.common.info,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var searchUnit = Ext.get('searchUnit');
searchUnit.on('click', 
 function time(){
var win = Ext.getCmp('time');
if (win) {win.close();}
var time= new Ext.Window({
id:'time',modal:true,title:'<fmt:message key="biolims.common.selectInstitutions"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/unit/dicUnitSelect.action?flag=equipment&flag=time' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 time.close(); }  }]  });     time.show(); }
);
});
 function settime(id,name){
 document.getElementById("instrumentRepairPlan_unit_id").value = id;
document.getElementById("instrumentRepairPlan_unit_name").value = name;
var win = Ext.getCmp('time')
if(win){win.close();}
}
</script>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var dutyNameBtn = Ext.get('dutyNameBtn');
dutyNameBtn.on('click', 
 function dutyUserFun(){
var win = Ext.getCmp('dutyUserFun');
if (win) {win.close();}
var dutyUserFun= new Ext.Window({
id:'dutyUserFun',modal:true,title:'',layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=dutyUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 dutyUserFun.close(); }  }]  });     dutyUserFun.show(); }
);
});
 function setdutyUserFun(id,name){
 document.getElementById("instrumentRepairPlan_dutyUser_id").value = id;
document.getElementById("instrumentRepairPlan_dutyUser_name").value = name;
var win = Ext.getCmp('dutyUserFun')
if(win){win.close();}
}
</script>

	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showDicType = Ext.get('showDicType');
showDicType.on('click', 
 function whlx(){
var win = Ext.getCmp('whlx');
if (win) {win.close();}
var whlx= new Ext.Window({
id:'whlx',modal:true,title:'<fmt:message key="biolims.common.selectMaintenanceTypes"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/dic/type/dicTypeSelect.action?flag=whlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 whlx.close(); }  }]  });     whlx.show(); }
);
});
 function setwhlx(id,name){
 document.getElementById("instrumentRepairPlan_type_id").value = id;
document.getElementById("instrumentRepairPlan_type_name").value = name;
var win = Ext.getCmp('whlx')
if(win){win.close();}
}
</script>




</s:if>

<script type="text/javascript">
function delStorageData(params){
var paramsValue = "";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : biolims.common.pleaseWait});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/plan/delInstrumentRepairPlanTaskStorage.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : biolims.common.info,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>

<script type="text/javascript">
function delPersonData(params){
var paramsValue = "";
if(paramsValue!=''){
params = params+','+paramsValue;}
params = '({'+params+'})';
params = eval(params);
var myMask = new Ext.LoadMask(Ext.getBody(), {msg : biolims.common.pleaseWait});
myMask.show();
	Ext.Ajax.request( {
	url : '/equipment/plan/delInstrumentRepairPlanTaskPerson.action',
	method : 'POST',
	params : params,
		success : function(response) {
       myMask.hide();
				gridGrid.store.reload();
},
		failure : function(response) {
		myMask.hide();
		Ext.MessageBox.show( {
				title : biolims.common.info,
				msg : biolims.common.failed,
				width : 300,
				buttons : Ext.MessageBox.OK,
				icon : Ext.MessageBox.WARNING});
				gridGrid.store.reload();
		}});}
	</script>


<script language = "javascript" >
 function duixiangbianma1(){
var win = Ext.getCmp('duixiangbianma1');
if (win) {win.close();}
var duixiangbianma1= new Ext.Window({
id:'duixiangbianma1',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/storage/common/storageSelect.action?flag=duixiangbianma1' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 duixiangbianma1.close(); }  }]  });     duixiangbianma1.show(); }
 function setduixiangbianma1(rec){
var record = gridGrid.getSelectionModel().getSelected();record.set('storage-id',rec.get('id'));record.set('storage-name',rec.get('name'));record.set('price',rec.get('price'));
var win = Ext.getCmp('duixiangbianma1')
if(win){win.close();}
}
</script>

<body>
	<s:if
		test='#request.handlemethod=="modify"||#request.handlemethod=="add"'>
		<script type="text/javascript"
			src="${ctx}/javascript/handleVaLeave.js"></script>
	</s:if>
	<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<s:form name="form1" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0">
				<tbody>
					<tr>
						<td>
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" class="controlTitle" nowrap><span
											class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.projectNumber"/>"
											class="text label"><fmt:message
													key="biolims.common.projectNumber" /> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><s:textfield readonly="true"
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentRepairPlan.id" id="instrumentRepairPlan_id"
												title='<fmt:message key="biolims.common.projectNumber"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="20"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
									<tr>
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label title=""
											class="text label"><fmt:message key="biolims.common.maintenanceTypes"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="hidden"
											id="instrumentRepairPlan_type_id"
											name="instrumentRepairPlan.type.id"
											value='<s:property value="instrumentRepairPlan.type.id"/>'
											><input
											type="text"
											class="input_parts text input default  readonlyfalse false"
											name="" readonly="readOnly"
											id="instrumentRepairPlan_type_name"
											value='<s:property value="instrumentRepairPlan.type.name"/>'
											title='' onblur="textbox_ondeactivate(event);"
											onfocus="shared_onfocus(event,this)" size="10" maxlength="30">
											<img alt='<fmt:message key="biolims.common.select"/>'
											id='showDicType' src='${ctx}/images/img_lookup.gif'
											class='detail' /></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<table width="100%" class="section_table" cellpadding="0">
								<tbody>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.describe"/>"
											class="text label"><fmt:message key="biolims.common.describe"/></label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:textfield
												cssClass="input_parts text input default  readonlyfalse false"
												name="instrumentRepairPlan.note"
												id="instrumentRepairPlan_note"
												title='<fmt:message key="biolims.common.sescribe"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="50"
												maxlength="55"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>

									<tr class="control textboxcontrol"
										label="<fmt:message key="biolims.common.commandPerson"/>">
										<td valign="top" align="right" class="controlTitle" nowrap>
											<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="<fmt:message key="biolims.common.commandPerson"/>"
											class="text label"><fmt:message
													key="biolims.common.commandPerson" /> </label>
										</td>
										<td class="requiredcolumn" nowrap width="10px">
										</td>
										<td nowrap><s:hidden
												name="instrumentRepairPlan.createUser.id"
												id="instrumentRepairPlan_createUser_id"></s:hidden> <s:textfield
												cssClass="input_parts text input default  readonlytrue false"
												name="instrumentRepairPlan.createUser.name"
												id="instrumentRepairPlan_createUser_name" readonly="true"
												title='<fmt:message key="biolims.common.commandPerson"/>'
												onblur="textbox_ondeactivate(event);"
												onfocus="shared_onfocus(event,this)" size="10"
												maxlength="30"></s:textfield> <img alt=''
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col1_sec1_2_detail' class='detail'
											style='display: none' /></td>
										<td nowrap>&nbsp;</td>
									</tr>
								</tbody>
							</table>
						</td>
						<td class="sectioncol " width='33%' valign="top" align="right">
							<div class="section standard_section  ">
								<div class="section">
									<table width="100%" class="section_table" cellpadding="0">
										<tbody>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.state"/>"
													class="text label"><fmt:message
															key="biolims.common.state" /> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
												<td nowrap><input type="text"
													class="input_parts text input default  readonlyfalse false"
													name="" id="instrumentRepairPlan_state_name"
													value="<s:property value="instrumentRepairPlan.state.name"/>"
													size="20" readonly="readonly"> <input type="hidden"
													name="instrumentRepairPlan.state.id"
													id="instrumentRepairPlan_state_id"
													value="<s:property value="instrumentRepairPlan.state.id"/>" />
													<img alt='<fmt:message key="biolims.common.select"/>'
													id='showDicState' src='${ctx}/images/img_lookup.gif'
													class='detail' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
											<tr class="control textboxcontrol">
												<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span> <label
													title="<fmt:message key="biolims.common.commandTime"/>"
													class="text label"><fmt:message
															key="biolims.common.commandTime" /> </label>
												</td>
												<td class="requiredcolumn" nowrap width="10px"></td>
												<td nowrap><input type="text"
													class="input_parts text input default  readonlytrue false"
													readonly="readOnly" name="instrumentRepairPlan.createDate"
													id="instrumentRepairPlan_createDate"
													value="<s:date name="instrumentRepairPlan.createDate" format="yyyy-MM-dd"/>"
													title='<fmt:message key="biolims.common.commandTime"/>'
													onblur="textbox_ondeactivate(event);"
													onfocus="shared_onfocus(event,this)" size="20"
													maxlength="30"> </input> <img alt=''
													id='main_grid1_row1_col2_sec2_3_detail'
													src='${ctx}/images/blank.gif'
													name='main_grid1_row1_col2_sec2_3_detail' class='detail'
													style='display: none' /></td>
												<td nowrap>&nbsp;</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td height="2"></td>
					</tr>
				</tbody>
			</table>
			<div class="section standard_section ">
				<div class="section">
					<table width="100%" class="section_table" cellpadding="0">
						<tbody>
							<tr width='100%' class="sectionrow " valign="top">
								<td class="sectioncol " width='50%' valign="top" align="right">
									<div class="section standard_section marginsection  ">
										<div class="section_header standard_section_header">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tbody>
													<tr width="100%">
														<td
															class="section_header_left text standard_section_label labelcolor"
															align="left"><span class="section_label"> <fmt:message
																	key="biolims.common.timeInformation" />
														</span></td>
														<td class="section_header_right" align="right"></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div width="100%" height="" class=" section_content_border">
											<table width="100%" class="section_table" cellpadding="0">
												<tbody>
													<tr class="control textboxcontrol"
														label="<fmt:message key="biolims.common.actualDate"/>">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.cycle"/>"
															class="text label"><fmt:message
																	key="biolims.common.cycle" /></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
														<td nowrap><s:textfield
																cssClass="input_parts text input default  readonlyfalse false"
																name="instrumentRepairPlan.period"
																id="instrumentRepairPlan_period"
																title='<fmt:message key="biolims.common.cycle"/>'
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="20"
																maxlength="50"
																onkeyup="this.value=this.value.replace(/\D/g,'')"></s:textfield>
															<img alt='' src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
														
														
														<td class="label-title"><fmt:message key="biolims.common.unit"/></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><select id="instrumentRepairPlan_unitName"
						name="instrumentRepairPlan.unitName" onchange="changeUnit()">
						    <option value="" selected="selected"><fmt:message key="biolims.common.pleaseSelect"/></option>
							<option value="1"<s:if  test="instrumentRepairPlan.unitName==1">selected="selected"</s:if>><fmt:message key="biolims.common.day"/></option>
							<option value="2"<s:if  test="instrumentRepairPlan.unitName==2">selected="selected"</s:if>><fmt:message key="biolims.common.week"/></option>
							<option value="3"<s:if  test="instrumentRepairPlan.unitName==3">selected="selected"</s:if>><fmt:message key="biolims.common.year"/></option>
					</select></td>
<!-- 										<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 											<span class="labelspacer">&nbsp;&nbsp;</span> <label title="" -->
<!-- 											class="text label">单位</label> -->
<!-- 										</td> -->
<!-- 										<td class="requiredcolumn" nowrap width="10px"><img -->
<%-- 											class='requiredimage' src='${ctx}/images/required.gif' /></td> --%>
<!-- 										<td nowrap><input type="hidden" -->
<!-- 											id="instrumentRepairPlan_unit_id" -->
<!-- 											name="instrumentRepairPlan.unit.id" -->
<%-- 											value='<s:property value="instrumentRepairPlan.unit.id"/>' --%>
<!-- 											><input -->
<!-- 											type="text" -->
<!-- 											class="input_parts text input default  readonlyfalse false" -->
<!-- 											name="" readonly="readOnly" -->
<!-- 											id="instrumentRepairPlan_unit_name" -->
<%-- 											value='<s:property value="instrumentRepairPlan.unit.name"/>' --%>
<!-- 											title='' onblur="textbox_ondeactivate(event);" -->
<!-- 											onfocus="shared_onfocus(event,this)" size="10" maxlength="30"> -->
<%-- 											<img alt='<fmt:message key="biolims.common.select"/>' --%>
<%-- 											id='searchUnit' src='${ctx}/images/img_lookup.gif' --%>
<!-- 											class='detail' /></td> -->
														
<!-- 														<td valign="top" align="right" class="controlTitle" nowrap> -->
<!-- 															<span class="labelspacer">&nbsp;&nbsp;</span> <label -->
<%-- 															title="<fmt:message key="biolims.common.unit"/>" --%>
<%-- 															class="text label"><fmt:message --%>
<%-- 																	key="biolims.common.unit" /></label> --%>
<!-- 														</td> -->
<!-- 														<td class="requiredcolumn" nowrap width="10px"><img -->
<%-- 											class='requiredimage' src='${ctx}/images/required.gif' /></td> --%>
<%-- 														<td nowrap><s:textfield --%>
<%-- 																cssClass="input_parts text input default  readonlyfalse false" --%>
<%-- 																name="instrumentRepairPlan.unit.name" --%>
<%-- 																id="instrumentRepairPlan_unit_name" --%>
<%-- 																title='<fmt:message key="biolims.common.unit"/>' --%>
<%-- 																onblur="textbox_ondeactivate(event);" --%>
<%-- 																onfocus="shared_onfocus(event,this)" size="20" --%>
<%-- 																maxlength="50" readonly="true"></s:textfield> <s:hidden --%>
<%-- 																name="instrumentRepairPlan.unit.id" --%>
<%-- 																id="instrumentRepairPlan_unit_id"></s:hidden> <img --%>
<%-- 															alt='<fmt:message key="biolims.common.selectTheUnit"/>' --%>
<%-- 															id='searchUnit' src='${ctx}/images/img_lookup.gif' --%>
<!-- 															name='searchUnit' class='detail' /></td> -->
<!-- 														<td nowrap>&nbsp;</td> -->
													</tr>
													<tr>
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.promptingTime"/>"
															class="text label"><fmt:message key="biolims.common.promptingTime"/></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage' src='${ctx}/images/required.gif' /></td>
														<td nowrap><s:textfield
																cssClass="input_parts text input default  readonlyfalse false"
																name="instrumentRepairPlan.tsTime"
																id="instrumentRepairPlan_tsTime" title='<fmt:message key="biolims.common.promptingTime"/>'
																onblur="textbox_ondeactivate(event);"
																onfocus="shared_onfocus(event,this)" size="15"
																maxlength="50"
																onkeyup="this.value=this.value.replace(/\D/g,'')"></s:textfield>
															<img alt='' src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /><label class="text label"><fmt:message key="biolims.common.day"/></label></td>
														<td nowrap>&nbsp;</td>
														<%-- <td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.duringTheInspectionDate"/>"
															class="text label"><fmt:message
																	key="biolims.common.verificationPromptDate" /></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><input type="text" Class="Wdate"
															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})"
															class="input_parts text input default  readonlyfalse"
															name="instrumentRepairPlan.tsDate"
															id="instrumentRepairPlan_tsDate"
															value="<s:date name="instrumentRepairPlan.tsDate" format="yyyy-MM-dd"/>"
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="20"
															maxlength="50"> </input><label
															title="<fmt:message key="biolims.common.duringTheInspectionDate"/>"
															class="text label">(<fmt:message
																	key="biolims.common.nullValueCanBeAutomaticallyCalculated" />)
														</label></td> --%>
													</tr>
													<%--<tr class="control textboxcontrol"
														label="<fmt:message key="biolims.common.timeInformation"/>">
														 <td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.commissioningDate"/>"
															class="text label"><fmt:message
																	key="biolims.common.commissioningDate" /></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><input type="text" Class="Wdate"
															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})"
															name="instrumentRepairPlan.effectiveDate"
															id="instrumentRepairPlan_effectiveDate"
															value="<s:date name="instrumentRepairPlan.effectiveDate" format="yyyy-MM-dd"/>"
															title='<fmt:message key="biolims.common.commissioningDate"/>'
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="20"
															maxlength="50"> </input> <img alt=''
															src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td> 
														<td nowrap>&nbsp;</td>

														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.endTime"/>"
															class="text label"><fmt:message
																	key="biolims.common.endTime" /></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><input type="text" Class="Wdate"
															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})"
															name="instrumentRepairPlan.expiryDate"
															id="instrumentRepairPlan_expiryDate"
															value="<s:date name="instrumentRepairPlan.expiryDate" format="yyyy-MM-dd"/>"
															title='<fmt:message key="biolims.common.endTime"/>'
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="20"
															maxlength="50"> </input> <img alt=''
															src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /></td>
														<td nowrap>&nbsp;</td>
													</tr>--%>

													<%-- <tr class="control textboxcontrol"
														label="<fmt:message key="biolims.common.actualDate"/>">
														<td valign="top" align="right" class="controlTitle" nowrap>
															<span class="labelspacer">&nbsp;&nbsp;</span> <label
															title="<fmt:message key="biolims.common.nextTestDate"/>"
															class="text label"><fmt:message
																	key="biolims.common.nextTestDate" /></label>
														</td>
														<td class="requiredcolumn" nowrap width="10px">
														</td>
														<td nowrap><input type="text" Class="Wdate"
															onfocus="WdatePicker ({skin:'ext',dateFmt:'yyyy-MM-dd'})"
															name="instrumentRepairPlan.nextDate"
															id="instrumentRepairPlan_nextDate"
															value="<s:date name="instrumentRepairPlan.nextDate" format="yyyy-MM-dd"/>"
															title='<fmt:message key="biolims.common.nextTestDate"/>'
															onblur="textbox_ondeactivate(event);"
															onfocus="shared_onfocus(event,this)" size="20"
															maxlength="50"> </input> <img alt=''
															src='${ctx}/images/blank.gif' class='detail'
															style='display: none' /> <input type="hidden"
															name="instrumentRepairPlan.startDate"
															id="instrumentRepairPlan_startDate"
															value="<s:date name="instrumentRepairPlan.startDate" format="yyyy-MM-dd"/>">
															</input> <label
															title='<fmt:message key="biolims.common.duringTheInspectionDate"/>'
															class="text label">(<fmt:message
																	key="biolims.common.inputCycleInformationCBAC" />)
														</label></td>
														<td nowrap>&nbsp;</td>
														
														<td nowrap>&nbsp;</td>
													</tr> --%>

												</tbody>
											</table>
										</div>
										<div class="section"></div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<input type="hidden" name="oldId" value="${instrumentRepairPlan.id}"></input>
			<input type="hidden" name="copyMode"
				value="<%=request.getParameter("copyMode") != null ? request
						.getParameter("copyMode") : ""%>"></input>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value="" />
			<input type="hidden" name="jsonDataStr1" id="jsonDataStr1" value="" />
			<input type="hidden" name="jsonDataStr2" id="jsonDataStr2" value="" />
			<input type="hidden" name="selectInstrumentRepairPlanTaskId"
				id="selectInstrumentRepairPlanTaskId" value="" />
		</s:form>
		<input type="hidden" id="handlemethod" value="${request.handlemethod}" />
		<input type="hidden" id="copyMode" value="${request.copyMode}" /> <input
			type="hidden" id="id" value="${id}" /> <input type="hidden"
			id="instrumentRepairPlanId" value="${instrumentRepairPlanId}" /> <input
			type="hidden" id="instrumentRepairPlanTaskId"
			value="${instrumentRepairPlanTaskId}" /> <input type="hidden"
			name="instrumentRepairPlan.tsDate" id="instrumentRepairPlan_tsDate"
			value="<s:date name="instrumentRepairPlan.tsDate" format="yyyy-MM-dd"/>">
		</input>

		<div id="item0">
			<div id='grid' style='width: 100%;'></div>
			<!--  
				<c:choose>
					<c:when test='${copyBl==true}'>
						<g:GridEditTag isAutoWidth="true"
							height="parent.document.body.clientHeight-125"
							validateMethod="validateFun" pageSize="100000" col="${col}"
							statement="${statement}" type="${type}" title='<fmt:message key="biolims.common.taskList"/>'
							url="${path}" statementLisener="${statementLisener}"
							handleMethod="${handlemethod}" />
					</c:when>
					<c:otherwise>
						<g:GridEditTag isAutoWidth="true"
							height="parent.document.body.clientHeight-125"
							validateMethod="validateFun" col="${col}"
							statement="${statement}" type="${type}" title='<fmt:message key="biolims.common.taskList"/>'
							url="${path}" statementLisener="${statementLisener}"
							handleMethod="${handlemethod}" />
					</c:otherwise>
				</c:choose>-->
		</div>
		<div id='bat_svnuploadcsv_div' style="display: none">
			<input type="file" name="file" id="file-svnuploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
		</div>
	</div>
</body>
</html>
