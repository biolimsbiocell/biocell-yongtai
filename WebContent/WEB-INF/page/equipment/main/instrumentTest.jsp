<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="biolims.common.instrumentManagement" /></title>
<style type="text/css">
.tri {
	display: none;
}
</style>
</head>
<body>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/main/instrumentTest.js"></script>
	<div id="testGrid111" style="margin: 0 0 0 0"></div>
</body>
</html>