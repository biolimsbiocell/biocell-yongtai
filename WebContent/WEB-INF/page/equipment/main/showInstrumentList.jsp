<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>

<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<g:LayOutWinTag buttonId="showPosition"
	title='<fmt:message key="biolims.common.subordinatePosition"/>'
	hasHtmlFrame="true"
	html="${ctx}/storage/common/showStoragePositionTree.action"
	isHasSubmit="false" functionName="searchPosition" hasSetFun="true"
	documentId="position_id" documentName="position_name" />
<g:LayOutWinTag buttonId="showDicState"
	title='<fmt:message key="biolims.common.selectTheState"/>'
	hasHtmlFrame="true"
	html="${ctx}/dic/state/stateSelect.action?type=instrument"
	isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true"
	documentId="state_id" documentName="state_name" />
<g:LayOutWinTag buttonId="searchdepartment"
	title='<fmt:message key="biolims.common.selectTheOrganization"/>'
	hasHtmlFrame="true" width="800" height="520"
	html="${ctx}/core/department/departmentSelect.action"
	isHasSubmit="false" functionName="searchdepartmentFun" hasSetFun="true"
	documentId="department_id" documentName="department_name" />
<g:LayOutWinTag buttonId="searchUser"
	title='<fmt:message key="biolims.common.selectLeader"/>'
	hasHtmlFrame="true" hasSetFun="true"
	html="${ctx}/core/user/userSelect.action" isHasSubmit="false"
	functionName="dutyUserFun" documentId="dutyUser_id"
	documentName="dutyUser_name" />
<g:LayOutWinTag buttonId="showPosition"
	title='<fmt:message key="biolims.common.selectLocation"/>'
	hasHtmlFrame="true"
	html="${ctx}/storage/common/showStoragePositionTree.action"
	isHasSubmit="false" functionName="searchPosition" hasSetFun="true"
	documentId="position_id" documentName="position_name" />

</head>
<body>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/main/showInstrumentList.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/handleSearchForm.js"></script>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label title=""
						class="text label">EQL</label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="id" onblur="textbox_ondeactivate(event);" searchField="true"
						onfocus="shared_onfocus(event,this)" id="id"
						title="<fmt:message key="biolims.common.inputID"/>"></td>
					<td valign="top" align="right" class="controlTitle" nowrap><span>&nbsp;</span>
						<label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
						<fmt:message key="biolims.common.instrumentName"/> </label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="name" onblur="textbox_ondeactivate(event);"
						searchField="true" onfocus="shared_onfocus(event,this)" id="note"
						title="<fmt:message key="biolims.common.inputDescribe"/>">
					</td>
				</tr>
				<tr>
					<td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label
						title="<fmt:message key="biolims.common.state"/>"
						class="text label"><fmt:message key="biolims.common.state" /></label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="" id="state_name" readonly="readonly" value=""
						readonly="readonly"> <img
						alt='<fmt:message key="biolims.common.select"/>' id='showDicState'
						src='${ctx}/images/img_lookup.gif' class='detail' /> <input
						type="hidden" name="state.id" searchField="true" id="state_id"
						value="" /></td>
					<%-- <td valign="top" align="right" class="controlTitle" nowrap><label
						title="<fmt:message key="biolims.common.leader"/>"
						class="text label"> <fmt:message
								key="biolims.common.leader" />
					</label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						readonly="readOnly" id="dutyUser_name" value=''
						title="<fmt:message key="biolims.common.leader"/> " maxlength="30">
						<img title='<fmt:message key="biolims.common.select"/>'
						id="searchUser" src='${ctx}/images/img_lookup.gif' class='detail' />
						<input type="hidden" searchField="true" name="dutyUser.id"
						id="dutyUser_id" value=''></td> --%>
					<%-- <td valign="top" align="right" class="controlTitle" nowrap>
						<span class="labelspacer">&nbsp;&nbsp;</span>
						<label title="组织机构" class="text label"> 组织机构 </label>
					</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="" id="department_name" title="组织机构  " onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" maxlength="20">
						<img title='选择' id='searchdepartment' src='${ctx}/images/img_lookup.gif' class='detail' />
						<input type="hidden" name="department.id" searchField="true" id="department_id">
					</td> --%>
				</tr>
				<tr>
					<%-- <td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label
						title="<fmt:message key="biolims.common.location"/>"
						class="text label"> <fmt:message
								key="biolims.common.location" />
					</label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						readonly="readOnly" id="position_name"
						title="<fmt:message key="biolims.common.location"/>"
						onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" maxlength="20"> <img
						title='<fmt:message key="biolims.common.select"/>'
						id='showPosition' src='${ctx}/images/img_lookup.gif'
						class='detail' /> <input type="hidden" searchField="true"
						name="position.id" id="position_id"></td>
 --%>
					<td valign="top" align="right" class="controlTitle" nowrap><span
						class="labelspacer">&nbsp;&nbsp;</span> <label title=""
						class="text label"><fmt:message key="biolims.storage.studyType"/> </label></td>
					<td nowrap><input type="text"
						class="input_parts text input default  readonlyfalse false"
						id="type_name" title="" name="type.name"
						onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" maxlength="20" searchField="true"></td>
					<%-- <td><span>&nbsp;</span> <label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message
								key="biolims.common.recordNumber" /></label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="text"
						class="input_parts text input default  readonlyfalse false"
						name="limitNum" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="limitNum"
						title="<fmt:message key="biolims.common.inputTheRecordNumber"/>">
					</td> --%>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="hidden"
						class="input_parts text input default  readonlyfalse false"
						name="p_type" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" id="p_type" title=""
						value="${requestScope.p_type}" searchField="true"></td>
				</tr>
			</table>
		</div>
		<input type="hidden" id="id" value="" />
		<form name='excelfrm' action='/common/exportExcel.action'
			method='POST'>
			<input type='hidden' id='gridhtm' name='gridhtm' value='' />
		</form>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' /> <input type='hidden' id='p_type'
			value='${requestScope.p_type}' />
		<div id='grid'></div>
		<div id='gridcontainer'></div>

	</div>

</body>
</html>