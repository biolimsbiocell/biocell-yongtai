<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
				<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i></i><fmt:message key="biolims.common.instrument"/>
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<%-- 	<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>设备</small>
								</span>
							</a></li>
							<li><a class="disabled step"> <span class="step_no">2</span>
									<span class="step_descr"> Step 2<br /> <small>设备实验记录</small></span>
							</a></li>
						</ul>
					</div> --%>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">编号 <img class='requiredimage'
											src='${ctx}/images/required.gif' /> </span> <input type="text"
											size="20" maxlength="25" id="instrument_id" name="instrument.id"
											changelog="<s:property value="instrument.id"/>"
											class="form-control"
											value="<s:property value="instrument.id"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">名称</span> <input type="text"
											size="20" maxlength="25" id="instrument_name" name="instrument.name"
											changelog="<s:property value="instrument.name"/>"
											class="form-control"
											value="<s:property value="instrument.name"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
										<span class="input-group-addon">是否需验证</span> 
											 <select class="form-control" id="instrument_verify" name="instrument.verify">
											<option value="0"
												<s:if test='instrument.verify=="0"'>selected="selected"</s:if>>
												否</option>
											<option value="1"
												<s:if test='instrument.verify=="1"'>selected="selected"</s:if>>
												是</option>
										</select>
										</div>
									</div>
								
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.serialNumber" /> </span> <input
											type="text" size="20" maxlength="25"
											id="instrument_serialNumber" name="instrument.serialNumber"
											changelog="<s:property value="instrument.serialNumber"/>"
											class="form-control"
											value="<s:property value="instrument.serialNumber"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.mainType" /><img class='requiredimage'
											src='${ctx}/images/required.gif' /></span> <input type="text"
											size="20" readonly="readOnly" id="instrument_mainType_name"
											changelog="<s:property value="instrument.mainType.name"/>"
											value="<s:property value="instrument.mainType.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrument_mainType_id" name="instrument.mainType.id"
											value="<s:property value="instrument.mainType.id"/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showmainType()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.spec" /> </span> <input type="text"
											size="20" maxlength="25" id="instrument_spec" name="instrument.spec"
											changelog="<s:property value="instrument.spec"/>"
											class="form-control"
											value="<s:property value="instrument.spec"/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.state" /> </span> <input type="text"
											size="20" readonly="readOnly" id="instrument_state_name"
											changelog="<s:property value="instrument.state.name"/>"
											value="<s:property value="instrument.state.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrument_state_id" name="instrument.state.id"
											value="<s:property value="instrument.state.id"/>"> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showstate()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
										<span class="input-group-addon">占用状态</span> 
											 <select class="form-control" id="instrument_isFull" name="instrument.isFull">
											<option value="0"
												<s:if test='instrument.isFull=="0"'>selected="selected"</s:if>>
												未占用</option>
											<option value="1"
												<s:if test='instrument.isFull=="1"'>selected="selected"</s:if>>
												占用</option>
										</select>
										</div>
									</div>
																	<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.createUser" /> </span> <input type="text"
											size="20" maxlength="25" id="instrument_createUser"
											name="instrument.createUser.name"
											changelog="<s:property value="instrument.createUser.name"/>"
											class="form-control" readonly="readonly"
											value="<s:property value="instrument.createUser.name"/>" />
										<input type="hidden" id="instrument_createUser_id"
											name="instrument.createUser.id"
											value="<s:property value="instrument.createUser.id"/>">

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.createDate" /> </span> <input type="text"
											size="50" maxlength="127" id="instrument_createDate"
											name="instrument.createDate" readonly="readonly"
											changelog="<s:date name="storage.createDate" format="yyyy-MM-dd "/>"
											title="<fmt:message key= 'biolims.tInstrument.createDate' />"
											class="form-control"
											value="<s:date name="instrument.createDate" format="yyyy-MM-dd "/>" />
											<input type="hidden"
											size="50" maxlength="127" id="instrument_tsDate"
											name="instrument.tsDate" readonly="readonly"
											changelog="<s:date name="storage.tsDate" format="yyyy-MM-dd "/>"
											class="form-control"
											value="<s:date name="instrument.tsDate" format="yyyy-MM-dd "/>" />
											<input type="hidden"
											size="50" maxlength="127" id="instrument_nextDate"
											name="instrument.nextDate" readonly="readonly"
											changelog="<s:date name="storage.nextDate" format="yyyy-MM-dd "/>"
											class="form-control"
											value="<s:date name="instrument.nextDate" format="yyyy-MM-dd "/>" />
											<input type="hidden"
											size="50" maxlength="127" id="instrument_nextDate"
											name="instrument.useDate" readonly="readonly"
											changelog="<s:date name="storage.useDate" format="yyyy-MM-dd "/>"
											class="form-control"
											value="<s:date name="instrument.useDate" format="yyyy-MM-dd "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.department" /> </span> <input type="text"
											size="20" readonly="readOnly" id="instrument_department_name"
											changelog="<s:property value="instrument.department.name"/>"
											value="<s:property value="instrument.department.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrument_department_id" name="instrument.department.id"
											value="<s:property value="instrument.department.id"/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showdepartment()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.price" /> </span> <input type="text"
											size="20" maxlength="25" id="instrument_price" name="instrument.price"
											changelog="<s:property value="instrument.price"/>"
											class="form-control"
											value="<s:property value="instrument.price"/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.currencyType" /> </span> <input
											type="text" size="20" readonly="readOnly"
											id="instrument_currencyType_name"
											changelog="<s:property value="instrument.currencyType.name"/>"
											value="<s:property value="instrument.currencyType.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrument_currencyType_id" name="instrument.currencyType.id"
											value="<s:property value="instrument.currencyType.id"/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showcurrencyType()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.note" /> </span> <input type="text"
											size="20" maxlength="25" id="instrument_note" name="instrument.note"
											changelog="<s:property value="instrument.note"/>"
											class="form-control"
											value="<s:property value="instrument.note"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.capitalCode" /> </span> <input
											type="text" size="20" maxlength="25"
											id="instrument_capitalCode" name="instrument.capitalCode"
											changelog="<s:property value="instrument.capitalCode"/>"
											class="form-control"
											value="<s:property value="instrument.capitalCode"/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">房间</span> <input type="text"
											size="20" readonly="readOnly" id="instrument_roomManagement_name"
											changelog="<s:property value="instrument.roomManagement.roomName"/>"
											value="<s:property value="instrument.roomManagement.roomName"/>"
											class="form-control" /> 
										<input type="hidden" class="form-control" id="instrument_roomManagement_id" name="instrument.roomManagement.id"
											changelog="<s:property value="instrument.roomManagement.id"/>"
											value="<s:property value="instrument.roomManagement.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showRoomManagements()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.effectiveDate" /> </span> 
												
												<input type="text"
											size="50" maxlength="127" id="instrument_effectiveDate"
											name="instrument.effectiveDate"
											changelog="<s:date name="instrument.effectiveDate" format="yyyy-MM-dd "/>"
											title="<fmt:message key= 'biolims.tInstrument.effectiveDate' />"
											class="form-control"
											value="<s:date name="instrument.effectiveDate" format="yyyy-MM-dd "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.products" /> </span> <input type="text"
											size="20" readonly="readOnly" id="instrument_supplier_name"
											changelog="<s:property value="instrument.supplier.name"/>"
											value="<s:property value="instrument.supplier.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrument_supplier_id" name="instrument.supplier.id"
											value="<s:property value="instrument.supplier.id"/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showproducts()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<%-- <div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrument.showstate" /> </span> <input type="text"
											size="20" maxlength="25" id="instrument_showstate"
											name="showstate"
											changelog="<s:property value="instrument.showstate.name"/>"
											class="form-control"
											value="<s:property value="instrument.showstate"/>" />

									</div>
								</div> --%>
							</div>
							<div class="row" id="rq">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">容器</span> <input type="text"
											size="20" readonly="readOnly" id="instrument_storageContainer_name"
											changelog="<s:property value="instrument.storageContainer.name"/>"
											value="<s:property value="instrument.storageContainer.name"/>"
											class="form-control" /> <input type="hidden"
											id="instrument_storageContainer_id" name="instrument.storageContainer.id"
											value="<s:property value="instrument.storageContainer.id"/>">

										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showStorageContainer()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">总位置数 </span> <input
											type="text" size="20" maxlength="25" 
											id="instrument_totalLocationsNumber" name="instrument.totalLocationsNumber"
											changelog="<s:property value="instrument.totalLocationsNumber"/>"
											class="form-control" readonly="readOnly"
											value="<s:property value="instrument.totalLocationsNumber"/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">剩余位置数 </span> <input
											type="text" size="20" maxlength="25"
											id="instrument_surplusLocationsNumber" name="instrument.surplusLocationsNumber"
											changelog="<s:property value="instrument.surplusLocationsNumber"/>"
											class="form-control" readonly="readOnly"
											value="<s:property value="instrument.surplusLocationsNumber"/>" />

									</div>
								</div>
							</div>
							<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
								</div>
							</div>
							<br> <input type="hidden" name="instrumentStateJson"
								id="instrumentStateJson" value="" /> <input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="instrument.id"/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value="instrument.fieldContent"/>" />
								<input type="hidden" id="changeLog" name="changeLog"  />
						</form>
					</div>
					<!--table表格-->
					<!-- <div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="instrumentStateTable" style="font-size: 14px;">
						</table>
					</div> -->
				</div>
				<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;" id="rqyb">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="incubatorSampleInfoTable" style="font-size: 14px;"></table>
				</div>
				<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="instrumentStateTable" style="font-size: 14px;"></table>
				</div>
				<!-- <div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/main/instrumentEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/main/instrumentState.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/equipment/main/incubatorSampleInfo.js"></script>
</body>

</html>
