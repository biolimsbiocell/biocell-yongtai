<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>选择设备</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/equipment/common/selEquipmentList.js"></script>
</head>
<body>
	<div>
		<div>
			<table>
				<tr>
					<td></td>
					<td class="label-title">设备编号</td>
					<td><input type="text" searchField="true" name="id"
						onkeydown="checkKey(this);" id="sel_search_id"></td>
					<td class="label-title">设备名称</td>
					<td><input type="text" onkeydown="checkKey(this);"
						searchField="true" name="note" id="sel_search_note"> <input
						type="button" value="检索" onclick="search()" /></td>

				</tr>
			</table>
			<input type="hidden" id="extJsonDataString" value="" /> <input
				type="hidden" id="eType" value="<%=request.getParameter("eType")%>" />

		</div>
		<div id="sel_equipment_gird_div"></div>
	</div>
</body>
</html>