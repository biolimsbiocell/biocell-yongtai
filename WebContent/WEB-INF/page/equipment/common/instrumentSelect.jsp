<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript">
	function setValue(rec){
		<%if(request.getParameter("flag")!=null){%>
		 	window.parent.set<%=request.getParameter("flag")%>(rec);
		<%}%>
	}
	function search() {
		commonSearchActionByHeight(gridGrid, document.body.clientHeight - 30);

	}
</script>
</head>
<body>
	<div class="mainfullclass" id="markup">
		<table>
			<tr>
				<td nowrap><span>&nbsp;</span> <label title="编码"
					class="text label">编码</label> <input type="text"
					class="input_parts text input default  false false"
					title="输入检索条件时，前后加%，可进行模糊检索，如%2011%" name="id" searchField="true"
					onblur="textbox_ondeactivate(event);" onkeydown="checkKey(this);"
					maxlength="32" /></td>
				<td nowrap><span>&nbsp;</span> <label class="text label"
					title="输入查询条件"> 名称</label></td>

				<td nowrap class="quicksearch_control" valign="middle"><input
					type="text" size="20"
					class="input_parts text input default  readonlyfalse false"
					name="note" onblur="textbox_ondeactivate(event);" maxlength="55"
					searchField="true" onfocus="shared_onfocus(event,this)" id="note"
					title="输入检索条件时，前后加%，可进行模糊检索，如%名称%"></td>

				<td nowrap><span>&nbsp;</span> <label class="text label"
					title="输入查询条件"> 记录数</label></td>
				<td nowrap class="quicksearch_control" valign="middle"><input
					type="text"
					class="input_parts text input default  readonlyfalse false"
					name="limitNum" onblur="textbox_ondeactivate(event);"
					onfocus="shared_onfocus(event,this)" id="limitNum" title="输入记录数"
					style="width: 40"></td>

				<td nowrap><span>&nbsp;</span> <img
					class="quicksearch_findimage" id="quicksearch_findimage"
					name="quicksearch_findimage" onclick="search()"
					src="${ctx}/images/quicksearch.gif" title="搜索"> <img
					class="quicksearch_findimage" id="quicksearch_findimage"
					name="quicksearch_findimage" onclick="form_reset()"
					src="${ctx}/images/no_draw.gif" title="清空"></td>
			</tr>
		</table>
		<input type="hidden" id="id" value="" />
		<g:GridTagNoLock isAutoWidth="true" col="${col}"
			height="document.body.clientHeight-30" type="${type}"
			title="${title}" url="${path}" callFun="setValue"
			callFunRecord="true" />
	</div>
</body>
</html>