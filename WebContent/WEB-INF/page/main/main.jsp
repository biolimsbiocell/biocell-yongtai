﻿<%@page import="org.jboss.logging.Param"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Bio-LIMS
</title>
<!-- 读取哪一个资源文件 -->
 <fmt:setBundle basename="ResouseInternational/msg"/>
</head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common0.jsp"%>
<script type="text/JavaScript" src="${ctx}/js/language/ext-lang-<%=session.getAttribute("lan") %>.js"></script>
<script type="text/javascript" src="${ctx}/javascript/main/main.js"></script>
<style type="text/css">
p {
	margin: 5px;
}

.settings {
	background-image:
		url(${ctx}/javascript/lib/ext-3.4.0/examples/shared/icons/fam/folder_wrench.png);
}

.nav {
	background-image:
		url(${ctx}/javascript/lib/ext-3.4.0/examples/shared/icons/fam/folder_go.png);
}

.info {
	background-image:
		url(${ctx}/javascript/lib/ext-3.4.0/examples/shared/icons/fam/information.png);
}
.x-tree-node-collapsed .x-tree-node-icon{
 background-image:url('${ctx}/images/folder.png');
 font-size:12px;
}
.x-tree-node-expanded .x-tree-node-icon
{
 background-image: url('${ctx}/images/folder_lightbulb.png');
font-size:12px;
}
.x-tree-node-leaf .x-tree-node-icon{
 background-image:url('${ctx}/images/application_go.png');
font-size:12px;
}
</style>
<body style="background-color: #FFFFFF;">

<input type="hidden" id="wcollapseid" name="wcollapse" value="true">
<input type="hidden" id="alwaysopen" name="alwaysopen" value="false">
<div id="north" class="x-hide-display" >
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="bgnavbar" >
		<tr>
			<td width="35"  rowspan="2" nowrap><img id="appimage" border="0"  height="27"
				src="${ctx}/images/appimg.gif" align="left" hspace="0" alt="BIO-LIMS">
			  </td>
			<td nowrap><fmt:message key="biolims.common.company"/></td>
			<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="" nowrap valign="top">					
						<span  title='<fmt:message key="biolims.common.navigation"/>'  id='showbtn'  class='text hyperlink powerwhite' onclick='javascript:openwest();setalwaysopen();' >
							 <img border='0'  src='${ctx}/images/btn_goto.gif'><fmt:message key="biolims.common.navigation"/>
						</span>
					</td>
					
					<!-- td class="navbar" nowrap valign="top">
						<span id='reportslink' class='text hyperlink powerwhite' title='报表' onclick="javascript:window.frames['tab1frame'].location.href='storage/storagelist.html';">
						<img border='0' src='${ctx}/images/btn_reporting.gif'>报表
						</span>
					</td-->
					<td class="" nowrap valign="top">
						<span id='sslink' class='text hyperlink powerwhite'  title='<fmt:message key="biolims.common.personalPortal"/>'  onclick="javascript:window.location.href='${ctx}/main/toPortal.action';" >
							<img border='0' alt='Start Center' src='${ctx}/images/btn_startcenter.gif'><fmt:message key="biolims.common.personalPortal"/>
						</span>
					</td>
					<td class="" valign="top" nowrap>
						<span id='profilelink' class='text hyperlink powerwhite' title='<fmt:message key="biolims.common.settings"/>' onclick='hyperlink_onclick();' >
							<img border='0'  src='${ctx}/images/btn_profile.gif' ><fmt:message key="biolims.common.settings"/>
						</span>
					</td>
					<td class="" nowrap valign="top"><span 
						id='signoutlink' class='text hyperlink powerwhite' title='<fmt:message key="biolims.common.exit"/>' 
					 onclick="javascript:window.location.href='${ctx}/main/logOut.action';"
						><img border='0' 
						src='${ctx}/images/btn_signout.gif' ><fmt:message key="biolims.common.exit"/></span>
					</td>
					<td class="" nowrap valign="top">
						<span id='helplink' class='text hyperlink powerwhite' title='<fmt:message key="biolims.common.help"/>' onclick="javascript:window.open('${ctx}/help/help.jsp','','');">
					 	<img border='0' src='${ctx}/images/btn_help.gif'><fmt:message key="biolims.common.help"/>
					 	</span>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</div>
</body>
</html>