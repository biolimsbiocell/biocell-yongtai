<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<head><!-- 读取哪一个资源文件 -->
<fmt:setBundle basename="ResouseInternational/msg" /></head>
	<s:form action="" method="post" theme="simple">				
		<table width="100%" class="section_table" cellpadding="0" 
					cellspacing="0" >
					<tbody>
						<tr>
							<td width="30%">
							<table width="100%" class="section_table" cellpadding="0"
								cellspacing="0"  >
								<tbody>
									<tr class="control textboxcontrol"
										 >
										<td  valign="top" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span>
										 <label	title="用户帐号"
											 class="text label">
										<fmt:message key="biolims.common.userName" /> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											class='requiredimage'
											src='${ctx}/images/required.gif' /></td>
										<td nowrap><s:textfield cssClass="input_parts text input default  readonlytrue false" id="user.id" name="loginUser.id" readonly="true" disabled="true"></s:textfield><img 
											
											src='${ctx}/images/blank.gif'
											 class='detail'
											style='display:none' /></td>

										<td nowrap>&nbsp;</td>
									</tr>
							<tr class="control textboxcontrol"
										 >
										<td  valign="top" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="旧密码"
											 class="text label"><fmt:message key="biolims.common.password" /></label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											 class='requiredimage'
											src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="password" name="loginUser.oldPassword" id="user.oldPassword"> <img alt=''
											
											src='${ctx}/images/blank.gif'
											 class='detail'
											style='display:none' /></td>

										<td nowrap>&nbsp;</td>
									</tr>

									<tr class="control textboxcontrol"
										 >
										<td  valign="top" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="密码"
											 class="text label"><fmt:message key="biolims.common.password" /></label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											 class='requiredimage'
											src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="password" name="loginUser.password" id="user.password"> <img alt=''
											
											src='${ctx}/images/blank.gif'
											 class='detail'
											style='display:none' /></td>

										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol"
										 >
										<td  valign="top" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="确认密码"
											 class="text label">
										<fmt:message key="biolims.common.ensurePassword" /> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											 class='requiredimage'
											src='${ctx}/images/required.gif' /></td>
										<td nowrap><input type="password" name="" id="pwdreal"> <img alt=''
											
											src='${ctx}/images/blank.gif'
											 class='detail'
											style='display:none' /></td>

										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="电话" 
											
											 class="text label"><fmt:message key="biolims.common.tel" /></label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											 class='requiredimage'
											src='${ctx}/images/notrequired.gif' /></td>
										<td nowrap><s:textfield cssClass="input_parts text input default  readonlyfalse false" id="user.mobile" name="loginUser.mobile"></s:textfield>


										<img  
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col1_sec1_2_detail' class='detail'
											style='display:none' /></td>

										<td nowrap>&nbsp;</td>
									</tr>
									<tr>
										<td height="1"></td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="地址" 
											
											 class="text label">
										<fmt:message key="biolims.common.addr" /> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											 class='requiredimage'
											src='${ctx}/images/notrequired.gif' /></td>
										<td nowrap>	<s:textfield id="user.address" name="loginUser.address" cssClass="input_parts text input default  readonlyfalse false"></s:textfield>
											<img 
											
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col1_sec1_2_detail' class='detail'
											style='display:none' /></td>

										<td nowrap>&nbsp;</td>
									</tr>
									<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="email密码" 
											
											 class="text label">
										<fmt:message key="biolims.common.emailPassword" /> </label></td>
										<td class="requiredcolumn" nowrap width="10px"><img
											 class='requiredimage'
											src='${ctx}/images/notrequired.gif' /></td>
										<td nowrap>
										<input type="password" name="loginUser.emailPassword" id="user_emailPassword">
										
											
											<img 
											
											src='${ctx}/images/blank.gif'
											name='main_grid1_row1_col1_sec1_2_detail' class='detail'
											style='display:none' /></td>

										<td nowrap>&nbsp;</td>
									</tr>
										<tr class="control textboxcontrol">
										<td valign="top" align="right" class="controlTitle" nowrap>
										</td>
										<td class="requiredcolumn" nowrap width="10px"><img
											 class='requiredimage'
											src='${ctx}/images/notrequired.gif' /></td>
										<td valign="top" align="left" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label
											title="email密码" 
											
											 class="text label">
										<fmt:message key="biolims.common.detail" /></label></td>

										<td nowrap>&nbsp;</td>
									</tr>
									</tbody>
									</table>
									</td>
									</tr>
									</tbody>
									</table>
									
		
		
		
		
		
		
		
		
		
	</s:form>
