﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Welcome to Bio-LIMS</title>
<!-- 传递参数 -->
<c:if test="${param.lan!=null}">
<fmt:setLocale value="${param.lan}" scope="session"/>
<c:set value="${param.lan}" var="lan" scope="session"></c:set>
</c:if>
<c:if test="${param.lan==null}">
<c:set value="zh_CN" var="lan" scope="session"></c:set>
<fmt:setLocale value="zh_CN" scope="session"/>
</c:if>
<!-- 读取哪一个资源文件 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>

<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/lib/jquery.cookie.js"></script>
<link href="${ctx}/css/login.css" rel="stylesheet" type="text/css">
<body class="bgpage"
	style="background-image: url(${ctx}/images/login/bg_login_template.jpg)">
	<div align="right"></div>

	<script type="text/javascript">
function checkSubmit(){
	var mess="";
	var fs = [document.getElementById("userid_textbox").value,document.getElementById("userpassword_textbox").value];
	var nsc=[biolims.main.usernull,biolims.main.passwordnull];
	    mess=commonFieldsNotNullVerify(fs,nsc);
	    if(mess!=""){
	    	message(mess);
	    	return false;
	    }
	    return true;
}

function formsubmit(){
	//checkSubmit();
	if(checkSubmit()==true){
		form1.method = "POST";
		form1.action =  window.ctx+"/main/toMain.action";
		form1.submit();
		
		var option = {};
		option.expires = 365;
		option.path = "/";
		$.cookie("username", $("#userid_textbox").val(), option);
	}else{
		return false;
	}
}
$(function(){
	var cookieUsername = $.cookie("username");
	if(cookieUsername){
		$("#userid_textbox").val(cookieUsername);
		$("#userpassword_textbox").focus();
	}else{
		$("#userid_textbox").focus();
	}
	 $(document).keydown(function(e) {
         var key = (e.keyCode) || (e.which) || (e.charCode);//兼容IE(e.keyCode)和Firefox(e.which)
         if (key == "13"){
        	 formsubmit();
         }
     });
});
</script>

	<!--  <table width="80%" border="0" cellpadding="0" cellspacing="0"
	class="welcometo">
	<tr>
		<td nowrap><font color="red">Bio-LIMS</font>实验室信息管理系统</td>
	</tr>
</table>  -->
	<table width="80%" border="0" cellpadding="0" cellspacing="0"
		class="releasestamp">
		<tr>
		 <td align="right"><a href="?lan=en">English</a> | <a
			href="?lan=zh_CN">中文</a></td>
		</tr>
	</table>
	<table border="0" cellpadding="0" cellspacing="0" class="companylogo">
		<tr>
			<td valign="top"><img src="${ctx}/images/login/companylogo.gif">
			</td>
		</tr>
	</table>

	<table border="0" cellpadding="0" cellspacing="0" class="mainbgtable"
		style="width: 1330px;">
		<tr>
			<td align="center" style="margin-left: 40px;">
				<table border="0" cellpadding="0" cellspacing="0"
					class="inputheader">
					<s:form name="form1" theme="simple">
						<tr>
							<td style="font-size: 12px;"><fmt:message key="biolims.common.userName" /></td>
							<td><s:textfield
									cssClass="input_parts text input default  readonlyfalse false"
									name="loginUser.id" id="userid_textbox" title="用户帐号  "
									onblur="textbox_ondeactivate(event);" size="20" maxlength="30"></s:textfield>
						</tr>
						<tr>
							<td height="3"></td>
						</tr>
						<tr>
							<td style="font-size: 12px;"><fmt:message key="biolims.common.password" /></td>
							<td><s:password
									cssClass="input_parts text input default  readonlyfalse false"
									name="loginUser.userPassword" id="userpassword_textbox"
									title="密码  " onblur="textbox_ondeactivate(event);" size="20"
									maxlength="30"></s:password></td>
						</tr>
						<tr>
							<td height="3"></td>
						</tr>
						<tr>
							<td style="font-size: 12px;"><fmt:message key="biolims.common.systemWide" /></td>
							<td><select name="dicCostCenter.id" id="dicCostCenter_id" class="input_parts text input default  readonlyfalse false" style="width: 138px;">
							</select></td>
						</tr>
						<tr>
							<td>&nbsp;</td>

							<td id="loginbtn" class="pushbutton" align="center">
								<table id="loginbtn_table" border="0" cellspacing="0"
									cellpadding="0">
									<tr>
										<td><img src="${ctx}/images/login/login_icon_newuser.gif"
											width="17" height="18"></td>
										<td id="loginbtn_middle" nowrap align="center"
											class="text  pushbutton_default " valign="middle"><a
											onClick="formsubmit();" style="font-size: 12px;" href="#"><fmt:message
													key="biolims.common.login" /></a></td>
									</tr>
								</table>
							</td>
						</tr>
					</s:form>
				</table>
			</td>
		</tr>

	</table>
</body>
<script type="text/javascript">

<s:if test="#request.messageMap.message!=null&&#request.messageMap.message!=''">


         Ext.onReady(function(){
        	 message('${messageMap.message}');
   
});

</s:if>
</script>
</html>