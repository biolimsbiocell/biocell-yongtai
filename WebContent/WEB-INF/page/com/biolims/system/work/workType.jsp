﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/work/workType.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="workType_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="workType_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcycle" title="选择检测周期"
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/work/workType/integerSelect.action"
				isHasSubmit="false" functionName="IntegerFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('workType_cycle').value=rec.get('id');
				document.getElementById('workType_cycle_name').value=rec.get('name');" />
               	 	<td class="label-title" >检测周期</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="workType_cycle_name" searchField="true"  name="cycle.name"  value="" class="text input" />
 						<input type="hidden" id="workType_cycle" name="workType.cycle.id"  value="" > 
 						<img alt='选择检测周期' id='showcycle' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showparent" title="选择父级编号"
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/work/workType/workTypeSelect.action"
				isHasSubmit="false" functionName="WorkTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('workType_parent').value=rec.get('id');
				document.getElementById('workType_parent_name').value=rec.get('name');" />
               	 	<td class="label-title" >父级编号</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="workType_parent_name" searchField="true"  name="parent.name"  value="" class="text input" />
 						<input type="hidden" id="workType_parent" name="workType.parent.id"  value="" > 
 						<img alt='选择父级编号' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="workType_state"
                   	 name="state" searchField="true" title="状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_workType_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_workType_tree_page"></div>
</body>
</html>



