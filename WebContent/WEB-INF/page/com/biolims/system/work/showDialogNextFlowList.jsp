<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/work/showDialogNextFlowList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="selectIds" value="<%=request.getParameter("pId")%>"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">表单名称</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>
 <div id="show_dialog_nextflow_grid_div"></div>
</body>
</html>