﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" src="${ctx}/lims/plugins/gojs/release/go.js"></script>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>

	<body>
		<input type="hidden" id="workOrderFlowchartJson" value='${requestScope.content}' />
		<div class="container-fluid" style="margin-top: 58px">
			<!--订单录入的form表单-->
			<div class="col-xs-12" style="padding: 0px">
				<div id="sample">
					<div style="width: 100%;display: flex; justify-content: space-between">
						<!--<div id="myPaletteDiv" style="width: 100px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>-->
						<div id="myDiagramDiv" style="flex-grow: 1; height: 500px; border: solid 1px black"></div>
					</div>
				</div>
			</div>
		</div>

	</body>
	<script type="text/javascript" src="${ctx}/js/com/biolims/system/work/workOrderFlowchart.js"></script>

</html>