﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript"
	src="${ctx}/lims/plugins/gojs/release/go.js"></script>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
				<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 58px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="sampleReceiveTitle"
							type="<s:property value="sampleReceive.type"/>"><fmt:message key="biolims.common.experimentalConfigurationSheet"/></strong> <small
							style="color: #fff;"> <fmt:message key="biolims.common.codingA"/>: <span id="workOrder_id"><s:property
									value="workOrder.id " /></span> <fmt:message key="biolims.tStorage.createUser"/>: <span
							id="workOrder_createUser_name"><s:property
									value="workOrder.createUser.name" /></span> <fmt:message key="biolims.crmPatient.createDate"/>:<span
							id="workOrder_createDate"><s:date
									name="workOrder.createDate" format="yyyy-MM-dd " /></span></small>
					</h3>
				</div>
				<div class="box-body">
					<div class="col-xs-8">
						<div id="sample">
							<div
								style="width: 100%; display: flex; justify-content: space-between">
								<div id="myPaletteDiv"
									style="width: 100px; margin-right: 2px; background-color: whitesmoke; border: solid 1px black"></div>
								<div id="myDiagramDiv"
									style="flex-grow: 1; height: 500px; border: solid 1px black"></div>
							</div>

							<textarea id="mySavedModelnew"
								style="width: 100%; height: 300px; display: none;">
{ "class": "go.GraphLinksModel",
  "linkFromPortIdProperty": "fromPort",
  "linkToPortIdProperty": "toPort",
  "nodeDataArray": [],
  "linkDataArray": []}
  </textarea>
						</div>
					</div>
					<div class="col-xs-4">
						<form name="form1" id="form1" method="post">
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group" style="margin-top: 15px;">
										<span class="input-group-addon"><fmt:message key="biolims.common.describe"/>
										</span> <input type="text" size="20" maxlength="25"
											id="workOrder_name" name="workOrder.name"
											class="form-control" title=""
											value="<s:property value=" workOrder.name "/>" />
									</div>
								</div>
								<div class="col-xs-12">
									<div class="input-group" style="margin-top: 15px;">
										<span class="input-group-addon"><fmt:message key="biolims.tStorage.state"/>
										</span> <select class="form-control" id="workOrder_state"
											name="workOrder.state">
											<option value="1"
												<s:if test="workOrder.state==1">selected="selected"</s:if>><fmt:message key="biolims.master.valid"/></option>
											<option value="0"
												<s:if test="workOrder.state==0">selected="selected"</s:if>><fmt:message key="biolims.master.invalid"/></option>
										</select>
									</div>
								</div>
							</div>
							<div class="row" style="display:none">
								<div class="col-xs-12">
									<div class="input-group" style="margin-top: 15px;">
										<span class="input-group-addon"><fmt:message key="biolims.pooling.sequencingPlatform"/>
										</span> <input type="hidden" size="20" maxlength="25"
											id="workOrder_sequencingPlatform"
											name="workOrder.sequencingPlatform" class="form-control"
											title=""
											value="<s:property value=" workOrder.sequencingPlatform "/>" />
										<c:forEach var="listPlatForm" items="${listPlatForm}">
											<input type="checkbox" name="sequencingPlatform"
												id="sequencingPlatform" value="${listPlatForm.name}"
												style="margin-left: 10px;" />
											<span id="${listPlatForm.name}" class="label-title">${listPlatForm.name}</span>
											<br />
										</c:forEach>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
										<span class="input-group-addon"><fmt:message key="biolims.common.relatedProducts"/>
										</span>
								</div>
								<div class="col-xs-12">
									<textarea readonly="readOnly" class="form-control"><s:property
											value=" workOrder.productName " /></textarea>
									<input type="hidden"
											id="workOrder_productId" name="workOrder.productId"
											value="<s:property value=" workOrder.productId "/>">
											<input type="hidden"
											id="workOrder_productName" name="workOrder.productName"
											value="<s:property value=" workOrder.productName"/>">
								</div>
							</div>
							<input type="hidden" id="workOrder_createDate"
								name="workOrder.createDate"
								value="<s:property value=" workOrder.createDate "/>"> <input
								type="hidden" id="workOrder_createUser"
								name="workOrder.createUser.id"
								value="<s:property value=" workOrder.createUser.id "/>">
							<input type="hidden" id="id_parent_hidden" name="workOrder.id"
								value="<s:property value=" workOrder.id "/>" /> <input
								type="hidden" id="workOrderFlowchartJson"
								name="workOrder.content"
								value="<s:property value=" workOrder.content "/>" />
						</form>
					</div>

				</div>
				<div class="box-footer ipadmini"
					style="padding-top: 30px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="workOrderItem" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>

</body>
<script type="text/javascript"
	src="${ctx}/js/com/biolims/system/work/workOrderEdit.js"></script>
<script type="text/javascript"
	src="${ctx}/js/com/biolims/system/work/workOrderEditFlowchart.js"></script>

</html>