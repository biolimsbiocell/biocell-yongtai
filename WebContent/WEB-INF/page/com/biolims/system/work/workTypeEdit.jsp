﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=workType&id=${workType.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/work/workTypeEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="workType_id"
                   	 name="workType.id" title="编号"
                   	   
	value="<s:property value="workType.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="60" id="workType_name"
                   	 name="workType.name" title="描述"
                   	   
	value="<s:property value="workType.name"/>"
                   	  />
                   	  
                   	</td>

			<g:LayOutWinTag buttonId="showcreateUser" title="选择负责人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('workType_person').value=rec.get('id');
				document.getElementById('workType_person_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >负责人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="workType_person_name"  value="<s:property value="workType.person.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="workType_person" name="workType.person.id"  value="<s:property value="workType.person.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
 			</tr>
			<tr>
                   	<td class="label-title" >检测周期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="workType_cycle"
                   	 name="workType.cycle" title="检测周期"
                   	   
	value="<s:property value="workType.cycle"/>"
                   	  />
                   	  
                   	</td>

			
			
			
			<g:LayOutWinTag buttonId="showparent" title="选择父级编号"
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/work/workType/workTypeSelect.action"
				isHasSubmit="false" functionName="WorkTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('workType_parent').value=rec.get('id');
				document.getElementById('workType_parent_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >父级编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="workType_parent_name"  value="<s:property value="workType.parent.name"/>"/>
 						<input type="hidden" id="workType_parent" name="workType.parent.id"  value="<s:property value="workType.parent.id"/>" > 
 						<img alt='选择父级编号' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	
                   	<td class="label-title" >标识码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="workType_mark"
                   	 name="workType.mark" title="标识码"
                   	   
	value="<s:property value="workType.mark"/>"
                   	  />
                   	  
                   	</td>
			
			</tr>
			<tr>			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<select id="workType_state" name="workType.state" class="input-10-length" >
								<option value="1" <s:if test="workType.state==1">selected="selected"</s:if>>有效</option>
								<option value="0" <s:if test="workType.state==0">selected="selected"</s:if>>无效</option>
					</select>
                   	  
                   	</td>
                   	
			<%-- <g:LayOutWinTag buttonId="showtype" title="选择状态"
				hasHtmlFrame="true"
				html="${ctx}/dic/state/StateSelect.action"
				isHasSubmit="false" functionName="commonState" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('workType_state').value=rec.get('id');
				document.getElementById('workType_state_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="workType_state_name"  value="<s:property value="workType.state.name"/>"/>
 						<input type="hidden" id="workType_state" name="workType.state.id"  value="<s:property value="workType.state.id"/>" > 
 						<img alt='选择状态' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
<!--             <input type="hidden" name="workTypeItemJson" id="workTypeItemJson" value="" />
 -->            <input type="hidden"  id="id_parent_hidden" value="<s:property value="workType.id"/>" />
            </form>
           <!--  <div id="tabs">
            <ul>
			<li><a href="#workTypeItempage">检测方法明细</a></li>
           	</ul> 
			<div id="workTypeItempage" width="100%" height:10px></div>
			</div> -->
        	</div>
	</body>
	</html>
