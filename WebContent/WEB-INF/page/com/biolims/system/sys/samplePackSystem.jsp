﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/sys/samplePackSystem.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="18" id="samplePackSystem_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
					<input type="text" size="50" maxlength="60" id="samplePackSystem_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.businessTypes"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="samplePackSystem_product_name" searchField="true"  name="product.name"  value="" class="text input" />
 						<input type="hidden" id="samplePackSystem_product" name="samplePackSystem.product.id"  value="" > 
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.stateID"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="40" maxlength="30" id="samplePackSystem_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.stateID"/>"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_samplePackSystem_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_samplePackSystem_tree_page"></div>
</body>
</html>



