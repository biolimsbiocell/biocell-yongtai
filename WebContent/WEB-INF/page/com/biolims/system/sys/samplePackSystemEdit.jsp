﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=samplePackSystem&id=${samplePackSystem.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/sys/samplePackSystemEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="samplePackSystem_id"
                   	 name="samplePackSystem.id" title="<fmt:message key="biolims.common.serialNumber"/>"
	value="<s:property value="samplePackSystem.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="60" id="samplePackSystem_name"
                   	 name="samplePackSystem.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="samplePackSystem.name"/>"
                   	  />
                   	  
                   		
               	 	<td class="label-title" ><fmt:message key="biolims.common.businessTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="samplePackSystem_product_name"  value="<s:property value="samplePackSystem.product.name"/>"/>
 						<input type="hidden" id="samplePackSystem_product" name="samplePackSystem.product.id"  value="<s:property value="samplePackSystem.product.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectBusinessTypes"/>' id='showproduct' src='${ctx}/images/img_lookup.gif' onClick="ProductFun()"	class='detail'    />                   		
                   	</td>
			
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select id="samplePackSystem_state" name="samplePackSystem.state"  class="input-10-length"> 
                   	  		<option value="1" <s:if test="samplePackSystem.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"/></option>
							<option value="0" <s:if test="samplePackSystem.state==0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"/></option>
                   	  </select>
                   	</td>
			
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="samplePackSystemItemJson" id="samplePackSystemItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="samplePackSystem.id"/>" />
            </form>
			<div id="samplePackSystemItempage" width="100%" height:10px></div>
			</div>
	</body>
	</html>
