﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=product&id=${product.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/product/productEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"    ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"     ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   >
                   	<input type="text" size="20" maxlength="18" id="product_id"
                   	 name="product.id" title="<fmt:message key="biolims.common.serialNumber"/>"
	value="<s:property value="product.id"/>"
                   	   
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="product_name"
                   	 name="product.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="product.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.testingCycle"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="product_testTime"
                   	 name="product.testTime" title="<fmt:message key="biolims.common.testingCycle"/>"
                   	   
	value="<s:property value="product.testTime"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.detectionMethod"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="product_testMethod_name"  value="<s:property value="product.testMethod.name"/>"  />
 						<input type="hidden" id="product_testMethod" name="product.testMethod.id"  value="<s:property value="product.testMethod.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectDetectionMethod"/>' id='showtestMethod' src='${ctx}/images/img_lookup.gif' onClick="checkTestMethod()"	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.leader"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="product_createUser_name"  value="<s:property value="product.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="product_createUser" name="product.createUser.id"  value="<s:property value="product.createUser.id"/>" > 
 						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />    --%>                		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="product_createDate"
                   	 name="product.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="product.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.identificationCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="product_mark"
                   	 name="product.mark" title="<fmt:message key="biolims.common.identificationCode"/>" value="<s:property value="product.mark"/>"
                   	  />
                </td>
				
                <g:LayOutWinTag buttonId="showparent" title='<fmt:message key="biolims.common.selectTheParent"/>'
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/product/productSelect.action"
				isHasSubmit="false" functionName="ProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('product_parent').value=rec.get('id');
				document.getElementById('product_parent_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.parent"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="product_parent_name"  value="<s:property value="product.parent.name"/>" />
 						<input type="hidden" id="product_parent" name="product.parent.id"  value="<s:property value="product.parent.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheParent"/>' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />
 						<input type="button" value="清" onClick="javascript:$('#product_parent').val('');$('#product_parent_name').val('');">        		
                   	</td>
                   
                   		<td class="label-title" ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	  <td align="left"  >
                   	<select id="product_state" name="product.state">
								<option value="1" <s:if test="product.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"/></option>
								<option value="0" <s:if test="product.state==0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"/></option>
					</select>
                   	  
                   	</td>
			</tr>
			<tr>
				<g:LayOutWinTag buttonId="showAcceptUser" title='<fmt:message key="biolims.common.selectGroup"/>'
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('product_acceptUserGroup').value=rec.get('id'); 
					document.getElementById('product_acceptUserGroup_name').value=rec.get('name');" /> 			
					<td class="label-title" ><fmt:message key="biolims.common.responsibleGroup"/> </td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="product_acceptUserGroup_name"  value="<s:property value="product.acceptUserGroup.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="product_acceptUserGroup" name="product.acceptUserGroup.id"  value="<s:property value="product.acceptUserGroup.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td> 
               		<td class="label-title" ><fmt:message key="biolims.common.selectionBlock"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td> 			
					<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="product_orderBlock" name="product.orderBlock" value="<s:property value="product.orderBlock"/>"  readonly="readOnly"  />
 						<input type="hidden" id="product.orderBlockId" name="product.orderBlockId"  value="<s:property value="product.orderBlockId"/>" > 
 						<img alt='选择区块' id='showOrderBlock' src='${ctx}/images/img_lookup.gif' onClick="checkOrderBlock()" class='detail'    />                   		
               		</td> 
               		
               		<td class="label-title" ><fmt:message key="biolims.common.dose"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="product_sampleNum"
                   	 name="product.sampleNum" title="<fmt:message key="产品用量"/>" value="<s:property value="product.sampleNum"/>"
                   	  />
                </td>
			</tr>
			<tr>
			
			</tr>
			
					
            </table>
        		<table width="100%" class="section_table" cellpadding="0" >
			<tbody>
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"><fmt:message key="biolims.common.kjPrijectDetail"/></span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
			<table class="frame-table">
			
			
			
			
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.libraryType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select name="product.libraryType" id="product_libraryType" >
                  			<option value="" <s:if test="product.libraryType == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                  			<c:forEach var="listLibraryType" items="${listLibraryType}">
							<option value="${listLibraryType.name}" <c:if test="${product.libraryType==listLibraryType.name}">selected="selected"</c:if>>${listLibraryType.name}</option>
							</c:forEach>
                  		</select>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.methodology"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	   <select name="product.methodology" id="product_libraryType" >
                  			<option value="" <s:if test="product.methodology == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                  			<c:forEach var="listMethodology" items="${listMethodology}">
							<option value="${listMethodology.name}" <c:if test="${product.methodology==listMethodology.name}">selected="selected"</c:if>>${listMethodology.name}</option>
							</c:forEach>
                  		</select>
                   	</td>
                   	<td class="label-title" >Index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	   <select name="product.lciIndex" id="product_lciIndex" >
                  			<option value="" <s:if test="product.lciIndex == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                  			<c:forEach var="listLCIIndex" items="${listLCIIndex}">
							<option value="${listLCIIndex.name}" <c:if test="${product.lciIndex==listLCIIndex.name}">selected="selected"</c:if>>${listLCIIndex.name}</option>
							</c:forEach>
                  		</select>
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.insertSize"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_insertSize"
                   	 name="product.insertSize" title="Insert Size"
                   	   value="<s:property value="product.insertSize"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.targetYield"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_targetYield"
                   	 name="product.targetYield" title="Target Yield"
                   	   value="<s:property value="product.targetYield"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.QCRequirements"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_qcRequirements"
                   	 name="product.qcRequirements" title="QC Requirements"
                   	   value="<s:property value="product.qcRequirements"/>"
                   	  />
                   	</td>
				</tr>
				<tr style="display: none;">
                   	<td class="label-title" >GC%</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_lciGc"
                   	 name="product.lciGc" title="GC"
                   	   value="<s:property value="product.lciGc"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_notes"
                   	 name="product.notes" title="Notes"
                   	   value="<s:property value="product.notes"/>"
                   	  />
                   	</td>
                   		<td class="label-title" >GC%</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="product_siGc"
                   	 name="product.siGc" title="GC"
                   	   value="<s:property value="product.siGc"/>"
                   	  />
                   	</td> 
				</tr>
				<tr>
					<td class="label-title" ><fmt:message key="biolims.common.sequencingPlatform"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select name="product.sequencePlatform" id="product_sequencePlatform" >
                   			<option value="" <s:if test="product.sequencePlatform == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			
                   			<c:forEach var="listPlatForm" items="${listPlatForm}">
								<option value="${listPlatForm.name}" <c:if test="${product.sequencePlatform==listPlatForm.name}">selected="selected"</c:if>>${listPlatForm.name}</option>
								</c:forEach>
                   			
                   		</select>
                   	</td>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.sequencingType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select name="product.sequenceType" id="product_sequenceType" >
                   			<option value="" <s:if test="product.sequenceType == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listReadType" items="${listReadType}">
								<option value="${listReadType.name}" <c:if test="${product.sequenceType==listReadType.name}">selected="selected"</c:if>>${listReadType.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sequencingReadLong"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select name="product.sequenceLength" id="product_sequenceLength" >
                   			<option value="" <s:if test="product.sequenceLength == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listSequencingCycle" items="${listSequencingCycle}">
								<option value="${listSequencingCycle.name}" <c:if test="${product.sequenceLength==listSequencingCycle.name}">selected="selected"</c:if>>${listSequencingCycle.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
                   
               	 	
			</tr>
			<tr>
					
                   	<td class="label-title" >Index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select name="product.siIndex" id="product_siIndex" >
                   			<option value="" <s:if test="product.siIndex == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listIndex" items="${listIndex}">
								<option value="${listIndex.name}" <c:if test="${product.siIndex==listIndex.name}">selected="selected"</c:if>>${listIndex.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.clientIndex"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <select name="product.clientIndex" id="product_clientIndex" >
                   			<option value="" <s:if test="product.clientIndex == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listClientIndex" items="${listClientIndex}">
								<option value="${listClientIndex.name}" <c:if test="${product.clientIndex==listClientIndex.name}">selected="selected"</c:if>>${listClientIndex.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
                   	<td class="label-title" >phix%</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="product_phix"
                   	 name="product.phix" title="phix"
                   	   value="<s:property value="product.phix"/>"
                   	  />
                   	</td> 
			</tr>
			<tr style="display: none;">
					
                   	<td class="label-title" >Exclusive Lane</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_exclusiveLane"
                   	 name="product.exclusiveLane" title="Exclusive Lane"
                   	   value="<s:property value="product.exclusiveLane"/>"
                   	  />
                   	</td>
                   	<td class="label-title" >Exclusive Flowcell</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_exclusiveFlowcell"
                   	 name="product.exclusiveFlowcell" title="Exclusive Flowcell"
                   	   value="<s:property value="product.exclusiveFlowcell"/>"
                   	  />
                   	</td>
                   		<td class="label-title" ><fmt:message key="biolims.common.speciesType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="product_species"
                   	 name="product.species" title="<fmt:message key="biolims.common.speciesType"/>"
                   	   value="<s:property value="product.species"/>"
                   	  />
                   	</td> 
                   
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.deliveryMethod"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="product_deliveryMethod"
                   	 name="product.deliveryMethod" title="Delivery Method"
                   	   value="<s:property value="product.deliveryMethod"/>"
                   	  />
                   	</td> 
                   	<td class="label-title" ><fmt:message key="biolims.common.dataAnalysis"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_dataAnalysis"
                   	 name="product.dataAnalysis" title="Data Analysis"
                   	   value="<s:property value="product.dataAnalysis"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.analysisDetails"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_analysisDetails"
                   	 name="product.analysisDetails" title="Analysis Details"
                   	   value="<s:property value="product.analysisDetails"/>"
                   	  />
                   	</td>
			</tr>
			<tr style="display: none;">
					<td class="label-title" ><fmt:message key="biolims.common.extraction"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="product_extraction"
                   	 name="product.extraction" title="Delivery Method"
                   	   value="<s:property value="product.extraction"/>"
                   	  />
                   	</td> 
                   	<td class="label-title" ><fmt:message key="biolims.common.qcProtocol"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_qcProtocol"
                   	 name="product.qcProtocol" title="Data Analysis"
                   	   value="<s:property value="product.qcProtocol"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.sequencingApplication"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="product_sequencingApplication"
                   	 name="product.sequencingApplication" title="Analysis Details"
                   	   value="<s:property value="product.sequencingApplication"/>"
                   	  />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.flux"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="product_flux"
                   	 name="product.flux" title="<fmt:message key="biolims.common.flux"/>" value="<s:property value="product.flux"/>"
                   	  />
                </td>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
        		<table width="100%" class="section_table" cellpadding="0" >
			<tbody>
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"><fmt:message key="biolims.common.quoteInformation"/></span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
			<table class="frame-table">
			<tr>
			<td class="label-title" ><fmt:message key="biolims.common.projectQuote"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="product_money"
                   	 name="product.money" title="<fmt:message key="biolims.common.projectQuote"/>" value="<s:property value="product.money"/>"
                   	  />
                </td>
                <td class="label-title" ><fmt:message key="biolims.common.projectDiscount"/>(%)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="product_discount"
                   	 name="product.discount" title="<fmt:message key="biolims.common.projectDiscount"/>" value="<s:property value="product.discount"/>"
                   	  />
                </td>
			<td class="label-title" ><fmt:message key="biolims.common.projectPricing"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="product_productFee"
                   	 name="product.productFee" title="<fmt:message key="biolims.common.projectPricing"/>" value="<s:property value="product.productFee"/>"
                   	  />
                </td>	
			</tr>
			<tr>
				 	<td class="label-title" ><fmt:message key="biolims.common.termsOfPayment"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	  <td align="left"  >
                   	<select id="product_way" name="product.way" style="width:169" class="input-10-length" >
								<option value="0" <s:if test="product.way==0">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
								<option value="1" <s:if test="product.way==1">selected="selected"</s:if>><fmt:message key="biolims.common.halfMonth"/></option>
								<option value="2" <s:if test="product.way==2">selected="selected"</s:if>><fmt:message key="biolims.common.monthlyStatement"/></option>
								<option value="3" <s:if test="product.way==3">selected="selected"</s:if>><fmt:message key="biolims.common.quarterlyInvoicing"/></option>
								<option value="4" <s:if test="product.way==4">selected="selected"</s:if>><fmt:message key="biolims.common.paymentNow"/></option>
					</select>
			</tr>
			  </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="product.id"/>" />
            </form>
          
        	</div>
	</body>
	</html>
