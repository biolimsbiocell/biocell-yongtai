<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.input-group {
	margin-top: 10px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div class="container-fluid" style="margin-top: 56px">
		<!--form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title"><fmt:message key="biolims.dashboard.productData"/></h3>
					<small style="color: #fff;"> <fmt:message key="biolims.common.commandPerson"/>: <span><s:property
								value=" product.createUser.name " /></span> <fmt:message key="biolims.common.commandTime"/>: <span><s:date
								name="product.createDate" format="yyyy-MM-dd" /></span>
					</small>
				</div>
				<div class="box-body ipadmini">
					<div class="HideShowPanel">
						<input type="hidden" id="handlemethod"
							value="${requestScope.handlemethod}">
						<form name="form1" class="layui-form" id="form" method="post">
							<input type="hidden" name="product.createUser.id"
								value="<s:property value=" product.createUser.id "/>" /> <input
								changelog="<s:property value="product.createUser.name"/>"
								type="hidden" name="product.createUser.name"
								value="<s:property value=" product.createUser.name "/>" /> <input
								changelog="<s:property value="product.createDate"/>"
								type="hidden" name="product.createDate"
								value="<s:date name=" product.createDate " format="yyyy-MM-dd " />" />
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.codingA"/><img
											class='requiredimage' src='${ctx}/images/required.gif' />
										</span> <input type="text" changelog="<s:property value="product.id"/>"
										 id="product_id" name="product.id"
											class="form-control"
											value="<s:property value=" product.id "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.tInstrumentRepair.name"/></span> 
										<input type="text" changelog="<s:property value="product.name"/>"
											name="product.name" class="form-control"
											value="<s:property value=" product.name "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.testingCycle"/></span> <input
										changelog="<s:property value="product.testTime"/>"
											type="text" name="product.testTime" class="form-control"
											value="<s:property value=" product.testTime "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.detectionMethod"/></span> <input
											changelog="<s:property value="product.testMethod.name"/>"
											type="text" id="testMethodName" class="form-control"
											name="product.testMethod.name" readonly="readonly"
											value="<s:property value=" product.testMethod.name "/>" /> <input
											changelog="<s:property value="product.testMethod.id"/>"
											type="hidden" id="testMethodId" class="form-control"
											name="product.testMethod.id"
											value="<s:property value=" product.testMethod.id "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="choseJCFF()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.identificationCode"/> </span> <input
											type="text" class="form-control" name="product.mark"
											changelog="<s:property value="product.mark"/>"
											value="<s:property value=" product.mark "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.master.parentName"/> </span> <input
											changelog="<s:property value="product.parent"/>"
											type="hidden" id="product_parent_id" class="form-control"
											name="product.parent"
											value="<s:property value=" product.parent "/>" /> <input
											changelog="<s:property value="product.parentName"/>"
											type="text" id="product_parent_name" class="form-control"
											name="product.parentName" readonly="readonly"
											value="<s:property value=" product.parentName "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="choseParent()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.tStorage.state"/> </span> <input
											changelog="<s:property value="product.stateName"/>"
											type="hidden" id="stateName" name="product.stateName"
											value="<s:property value=" product.stateName "/>" /> <select
											class="form-control" name="product.state"
											onchange="changeState (this)">
											<option value="1"
												<s:if test="product.state==1">selected="selected"</s:if>>
												<fmt:message key="biolims.common.effective" />
											</option>
											<option value="0"
												<s:if test="product.state==0">selected="selected"</s:if>>
												<fmt:message key="biolims.common.invalid" />
											</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.responsibleGroup"/> </span> <input
											changelog="<s:property value="product.acceptUserGroup.id"/>"
											type="hidden" id="userGroupId" class="form-control"
											name="product.acceptUserGroup.id"
											value="<s:property value=" product.acceptUserGroup.id "/>" />
										<input type="text" id="userGroupName" readonly="readonly"
											changelog="<s:property value="product.acceptUserGroup.name"/>"
											class="form-control" name="product.acceptUserGroup.name"
											value="<s:property value=" product.acceptUserGroup.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="choseUserGroup()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.tInstrumentState.countData"/> </span> <input type="text"
											changelog="<s:property value="product.sampleNum"/>"
											class="form-control" name="product.sampleNum"
											value="<s:property value=" product.sampleNum "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.customFields"/> </span> <input
											changelog="<s:property value="product.fieldTemplate.id"/>"
											type="hidden" id="fieldTemplateId" readonly="readonly"
											class="form-control" name="product.fieldTemplate.id"
											value="<s:property value=" product.fieldTemplate.id "/>" /> <input
											changelog="<s:property value="product.fieldTemplate.note"/>"
											type="text" id="fieldTemplateName" class="form-control"
											name="product.fieldTemplate.note" readonly="readonly"
											value="<s:property value=" product.fieldTemplate.note "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="choseCustom()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.workflow.selectionProcess"/><img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											changelog="<s:property value="product.workOrder.id"/>"
											type="hidden" id="workOrderId" readonly="readonly"
											class="form-control" name="product.workOrder.id"
											value="<s:property value=" product.workOrder.id "/>" /> <input
											changelog="<s:property value="product.workOrder.name"/>"
											type="text" id="workOrderName" class="form-control"
											name="product.workOrder.name" readonly="readonly"
											value="<s:property value=" product.workOrder.name "/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="choseLC()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
											<button class="btn btn-info" type="button" onclick="viewLC()">
												<i class="glyphicon glyphicon-eye-open"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
									</div>
								</div>

								<%-- <div class="col-xs-12">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> <fmt:message key="biolims.common.kjPrijectDetail"/>
											</h3>
										</div>
									</div>
								</div>
								<br />

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.libraryType"/></span> <select
											class="form-control" name="product.libraryType">
											<option value=""
												<s:if test="product.libraryType == ''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<c:forEach var="listLibraryType" items="${listLibraryType}">
												<option value="${listLibraryType.name}"
													<c:if test="${product.libraryType==listLibraryType.name}">selected="selected"</c:if>>${listLibraryType.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.methodology"/> </span> <select
											class="form-control" name="product.methodology">
											<option value=""
												<s:if test="product.methodology == ''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<c:forEach var="listMethodology" items="${listMethodology}">
												<option value="${listMethodology.name}"
													<c:if test="${product.methodology==listMethodology.name}">selected="selected"</c:if>>${listMethodology.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> index </span> <select
											class="form-control" name="product.lciIndex">
											<option value=""
												<s:if test="product.lciIndex == ''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<c:forEach var="listLCIIndex" items="${listLCIIndex}">
												<option value="${listLCIIndex.name}"
													<c:if test="${product.lciIndex==listLCIIndex.name}">selected="selected"</c:if>>${listLCIIndex.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.insertSize"/> </span> <input
											changelog="<s:property value="product.insertSize"/>"
											type="text" class="form-control" name="product.insertSize"
											value="<s:property value=" product.insertSize "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.targetYield"/> </span> <input
											changelog="<s:property value="product.targetYield"/>"
											type="text" class="form-control" name="product.targetYield"
											value="<s:property value=" product.targetYield "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.QCRequirements"/> </span> <input
										changelog="<s:property value="product.qcRequirements"/>"
											type="text" class="form-control"
											name="product.qcRequirements"
											value="<s:property value=" product.qcRequirements "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.pooling.sequencingPlatform"/> </span> <select
											class="form-control" name="product.sequencePlatform">
											<option value=""
												<s:if test="product.sequencePlatform == ''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>

											<c:forEach var="listPlatForm" items="${listPlatForm}">
												<option value="${listPlatForm.name}"
													<c:if test="${product.sequencePlatform==listPlatForm.name}">selected="selected"</c:if>>${listPlatForm.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.pooling.sequencingType"/> </span> <select
											class="form-control" name="product.sequenceType">
											<option value=""
												<s:if test="product.sequenceType == ''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<c:forEach var="listReadType" items="${listReadType}">
												<option value="${listReadType.name}"
													<c:if test="${product.sequenceType==listReadType.name}">selected="selected"</c:if>>${listReadType.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.sequencingReadLong"/> </span> <select
											class="form-control" name="product.sequenceLength">
											<option value=""
												<s:if test="product.sequenceLength == ''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<c:forEach var="listSequencingCycle"
												items="${listSequencingCycle}">
												<option value="${listSequencingCycle.name}"
													<c:if test="${product.sequenceLength==listSequencingCycle.name}">selected="selected"</c:if>>${listSequencingCycle.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.clientIndex"/> </span> <select
											class="form-control" name="product.clientIndex">
											<option value=""
												<s:if test="product.clientIndex == ''">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<c:forEach var="listClientIndex" items="${listClientIndex}">
												<option value="${listClientIndex.name}"
													<c:if test="${product.clientIndex==listClientIndex.name}">selected="selected"</c:if>>${listClientIndex.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> phix% </span> <input
											changelog="<s:property value="product.phix"/>"
											type="text" class="form-control" name="product.phix"
											value="<s:property value=" product.phix "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.deliveryMethod"/> </span> <input
											changelog="<s:property value="product.deliveryMethod"/>"
											type="text" class="form-control"
											name="product.deliveryMethod"
											value="<s:property value=" product.deliveryMethod "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.dataAnalysis"/> </span> <input
											changelog="<s:property value="product.dataAnalysis"/>"
											type="text" class="form-control" name="product.dataAnalysis"
											value="<s:property value=" product.dataAnalysis "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.analysisDetails"/> </span> <input
											changelog="<s:property value="product.analysisDetails"/>"
											type="text" class="form-control"
											name="product.analysisDetails"
											value="<s:property value=" product.analysisDetails "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.flux"/> </span> <input type="text"
											changelog="<s:property value="product.flux"/>"
											class="form-control" name="product.flux"
											value="<s:property value=" product.flux "/>" />
									</div>
								</div> --%>

								<div class="col-xs-12">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> <fmt:message key="biolims.common.quoteInformation"/>
											</h3>
										</div>
									</div>
								</div>
								<br />
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.projectQuote"/> </span> <input
											changelog="<s:property value="product.money"/>"
											type="text" class="form-control" name="product.money"
											value="<s:property value=" product.money "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.projectDiscount"/>(%) </span> <input
											changelog="<s:property value="product.discount"/>"
											type="text" class="form-control" name="product.discount"
											value="<s:property value=" product.discount "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.projectPricing"/> </span> <input
											changelog="<s:property value="product.productFee"/>"
											type="text" class="form-control" name="product.productFee"
											value="<s:property value=" product.productFee "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message key="biolims.common.termsOfPayment"/> </span> <select
											class="form-control" name="product.way">
											<option value="0"
												<s:if test="product.way==0">selected="selected"</s:if>>
												<fmt:message key="biolims.common.pleaseSelect" />
											</option>
											<option value="1"
												<s:if test="product.way==1">selected="selected"</s:if>>
												<fmt:message key="biolims.common.halfMonth" />
											</option>
											<option value="2"
												<s:if test="product.way==2">selected="selected"</s:if>>
												<fmt:message key="biolims.common.monthlyStatement" />
											</option>
											<option value="3"
												<s:if test="product.way==3">selected="selected"</s:if>>
												<fmt:message key="biolims.common.quarterlyInvoicing" />
											</option>
											<option value="4"
												<s:if test="product.way==4">selected="selected"</s:if>>
												<fmt:message key="biolims.common.paymentNow" />
											</option>
										</select>
									</div>
								</div>
								<input type="hidden" id="productItemJson" name="productItemJson"/>
								<input type="hidden" id="id_parent_hidden"
									value="<s:property value=" product.id "/>" />
									<!-- 日志 dwb 2018-05-12 14:39:12 -->
                                <input type="hidden" id="changeLog" name="changeLog"  />
                                </div>
                                <div class="row">
                            
                              	<div class="col-xs-12">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> 
												对应模板
											</h3>
										</div>
									</div>
								</div>
								 </div>
								<div class="row">
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
	
											<span class="input-group-addon">生产模版<img
												class='requiredimage' src='${ctx}/images/required.gif' /> </span><input
												type="hidden" size="50" maxlength="125"
												id="product_template_id" name="product.template.id"
												changelog="<s:property value="product.template.id"/>"
												class="form-control"
												value="<s:property value="product.template.name"/>" /> <input
												type="text" size="50" maxlength="125"
												id="product_template_name" 
												changelog="<s:property value="product.template.name"/>"
												class="form-control"
												value="<s:property value="product.template.name"/>" />
												<span class="input-group-btn">
												<button class="btn btn-info" type="button"
													id='' onClick="selectTemplate()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">隐藏字段</span>
											<textarea id="productHideFields"
												name="product.hideFields" placeholder="长度限制2000个字符"
												class="form-control"
												style="overflow: hidden; width: 65%; height: 80px;"><s:property
													value="product.hideFields" /></textarea>
													<button style=" height: 80px;" class="btn btn-info" type="button"
													onclick="chosesoId()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
										</div>
										
									</div>
								
							</div>
						</form>
					</div>
					<!--table表格-->
					<div class="HideShowPanel" style="display: none;">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="productItemTable" style="font-size: 14px;">
						</table>
					</div>
					<!-- <div class="box-footer"></div> -->
				</div>
			</div>
		</div>
		<script type="text/javascript"
			src="${ctx}/js/com/biolims/system/product/productEditNew.js"></script>
		<script type="text/javascript"
			src="${ctx}/js/com/biolims/system/product/productItem.js"></script>
		<script type="text/javascript"
			src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>