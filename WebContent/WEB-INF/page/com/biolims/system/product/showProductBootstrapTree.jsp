<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bio-LIMS</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/treegrid/css/jquery.treegrid.css" />
		<link rel="stylesheet" href="${ctx}/lims/plugins/icheck/skins/square/blue.css">
		<script type="text/javascript" src="${ctx}/lims/plugins/icheck/icheck.min.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.min.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.bootstrap3.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.extension.js"></script>
		<style>
			.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
			#mytreeGrid tr{
				cursor: pointer;
			}
		</style>
	
	</head>

	<body>
	
		<table class="table table-hover table-bordered table-condensed"
			id="mytreeGrid" style="font-size: 12px;"></table>
	

		<!--<table id="mytreeGrid"></table>  -->

		<script type="text/javascript">
			<%--$(document).ready(function() {		
				$('#mytreeGrid').treegridData({
					id: 'id',
					parentColumn: 'parent',
					type: "GET", //请求数据的ajax类型
					url: '/com/biolims/system/product/showProductSelTreeJson.action', //请求数据的ajax的url
					ajaxParams: {}, //请求数据的ajax的data属性
					expandColumn: null, //在哪一列上面显示展开按钮
					striped: false, //是否各行渐变色
					bordered: true, //是否显示边框
					expandAll: false, //是否全部展开
					columns: [{
							title: biolims.tInstrumentFaultDetail.id,
							field: 'id'
						},
						{
							title: biolims.master.product,
							field: 'name'
						},
						{
							title: biolims.master.productFee,
							field: 'productFee'
						}
					]
					
				});
				

			});--%>
			var productTable;
			$(function() {
				var colOpts = [];
				colOpts.push({
					"data" : "id",
					"title" : "编号",
					"createdCell" : function(td, data, rowData) {
						   $(td).attr("saveName", "id");
						   $(td).attr("hideFields", rowData['hideFields']);
						  },
				});
				colOpts.push({
					"data" : "name",
					"title" : "产品",
				});
				colOpts.push({
					"data" : "productFee",
					"title" : "项目定价",
				});
				colOpts.push({
					"data" : "hideFields",
					"title" : "隐藏字段",
					"visible":false
				});
				var tbarOpts = [];
				var addProductOptions = table(false, null,
						'/com/biolims/system/product/showProductTableJson.action',
						colOpts,tbarOpts)
						productTable = renderData($("#mytreeGrid"), addProductOptions);
				$("#mytreeGrid").on(
						'init.dt',
						function(e, settings) {
							// 清除操作按钮
							$("#mytreeGrid_wrapper .dt-buttons").empty();
							$('#mytreeGrid_wrapper').css({
								"padding": "0 16px"
							});
						var trs = $("#mytreeGrid tbody tr");
						productTable.ajax.reload();
						productTable.on('draw', function() {
							trs = $("#mytreeGrid tbody tr");
							trs.click(function() {
								$(this).addClass("chosed").siblings("tr")
									.removeClass("chosed");
							});
						});
						trs.click(function() {
							$(this).addClass("chosed").siblings("tr")
								.removeClass("chosed");
						});
						});
			})
		

			
		</script>
	</body>

</html>