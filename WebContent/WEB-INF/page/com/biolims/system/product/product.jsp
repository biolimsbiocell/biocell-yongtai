﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/product/product.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="18" id="product_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"   style="display:none"    />
                   	
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="product_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.testingCycle"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="product_testTime"
                   	 name="testTime" searchField="true" title="<fmt:message key="biolims.common.testingCycle"/>"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.detectionMethod"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="product_workType_name" searchField="true"  name="workType.name"  value="" class="text input" />
 						<input type="hidden" id="product_workType" name="product.workType.id"  value="" > 
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.projectPricing"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="product_productFee"
                   	 name="productFee" searchField="true" title="<fmt:message key="biolims.common.projectPricing"/>"    />
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="product_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="product_createUser" name="product.createUser.id"  value="" > 
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="product_createDate_name" searchField="true"  name="createDate.name"  value="" class="text input" />
 						<input type="hidden" id="product_createDate" name="product.createDate.id"  value="" > 
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.stateID"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="product_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.stateID"/>"    />
                   	
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="product_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.leader"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="product_personName"
                   	 name="personName" searchField="true" title="<fmt:message key="biolims.common.leader"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showparent" title='<fmt:message key="biolims.common.selectTheParent"/>'
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/product/productSelect.action"
				isHasSubmit="false" functionName="ProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('product_parent').value=rec.get('id');
				document.getElementById('product_parent_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.parent"/></td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="product_parent_name" searchField="true"  name="parent.name"  value="" class="text input" />
 						<input type="hidden" id="product_parent" name="product.parent.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheParent"/>' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_product_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_product_tree_page"></div>
</body>
</html>



