﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title></title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style>
			.dataTables_scrollBody {
				min-height: 100px;
			}
		</style>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
			<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
		</div>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<div style="height: 14px"></div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 46px">
			<!--Form表单-->
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i><fmt:message key="biolims.unitGroupNew.unitGroup" />
					</h3>

						<div class="box-tools pull-right" style="display: none;">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="$('.buttons-print').click();">
											<fmt:message key="biolims.common.print" />
										</a>
									</li>
									<li>
										<a href="#####" onclick="$('.buttons-copy').click();">
											<fmt:message key="biolims.common.copyData" />
										</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" onclick="itemFixedCol(2)">
											<fmt:message key="biolims.common.lock2Col" />
										</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">
											<fmt:message key="biolims.common.cancellock2Col" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
								<input type="hidden" id="bpmTaskId" value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.common.serialNumber" /> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span>

											<input type="text" size="50" maxlength="127" id="unitGroupNew_id" changelog="<s:property value="unitGroupNew.id"/>" name="unitGroupNew.id" title="<fmt:message key="biolims.common.electronicMedicalRecords"/>" readonly="readOnly" class="form-control readonlytrue" value="<s:property value="unitGroupNew.id"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.common.describe" /> </span>

											<input type="text" size="50" maxlength="127" id="unitGroupNew_name" name="unitGroupNew.name" changelog="<s:property value="unitGroupNew.name"/>" title="<fmt:message key="biolims.common.sname"/>" class="form-control" value="<s:property value="unitGroupNew.name"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.unitGroupNew.conversionFactor" /> </span>

											<input type="text" size="50" maxlength="127" id="unitGroupNew_cycle" changelog="<s:property value="unitGroupNew.cycle"/>" name="unitGroupNew.cycle" title="转换系数" class="form-control" value="<s:property value="unitGroupNew.cycle"/>" />

										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.common.state" /> </span>
												<select class="form-control"  id="unitGroupNew_state" name="unitGroupNew.state">
														<option value=""
															<s:if test="tunitGroupNew.state==''">selected="selected"</s:if>>
															<fmt:message
												key="biolims.common.pleaseSelect" />
														</option>
														<option value="1"
																<c:if test="${unitGroupNew.state==1}">selected="selected"</c:if>>
																<fmt:message
												key="biolims.master.valid" />
														</option>
														<option value="0"
																<c:if test="${unitGroupNew.state==0}">selected="selected"</c:if>>
																<fmt:message
												key="biolims.master.invalid" />
														</option>
												</select>
											<%-- <input type="text" size="50" maxlength="127" id="unitGroupNew_state" changelog="<s:property value="unitGroupNew.state"/>" name="unitGroupNew.state" title="<fmt:message key="biolims.common.email"/>" class="form-control" value="<s:property value="unitGroupNew.state"/>" /> --%>

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.unitGroupNew.primaryUnit" /></span>
											<input type="hidden" id="unitGroupNew_mark_id" name="unitGroupNew.mark.id" value="<s:property value="unitGroupNew.mark.id"/>">
											<input type="text" size="50" maxlength="127" readonly="readonly" id="unitGroupNew_mark_name" changelog="<s:property value="unitGroupNew.mark.name"/>"   class="form-control" value="<s:property value="unitGroupNew.mark.name"/>" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="unitBatch()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.unitGroupNew.deputyUunit" /> </span>
											<input type="hidden" id="unitGroupNew_mark2_id" name="unitGroupNew.mark2.id" value="<s:property value="unitGroupNew.mark2.id"/>">
											<input type="text" size="50" readonly="readonly" maxlength="127" id="unitGroupNew_mark2_name" changelog="<s:property value="unitGroupNew.mark2.name"/>"  title="<fmt:message key="biolims.common.department"/>" class="form-control" value="<s:property value="unitGroupNew.mark2.name"/>" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="unitBatch2()">
													<i class="glyphicon glyphicon-search"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
											<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
									</div>
								</div>
								
							

								
								

								<input type="hidden" id="id_parent_hidden" value="<s:property value=" unitGroupNew.id "/>" />
								<input type="hidden" id="changeLog" name="changeLog"  />
						</form>
						</div>
					
							
							
						
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" 
			src="${ctx}/js/com/biolims/system/unitGroupNew/workTypeEdit.js"></script>
		<script type="text/javascript"
			src="${ctx}/javascript/common/dataTablesExtend.js"></script>
	
	</body>

</html>