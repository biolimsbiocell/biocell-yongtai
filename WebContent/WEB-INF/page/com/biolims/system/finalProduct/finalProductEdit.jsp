﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=finalProduct&id=${finalProduct.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/com/biolims/system/finalProduct/finalProductEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"    ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"     ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_id"
                   	 name="finalProduct.id" title="<fmt:message key="biolims.common.serialNumber"/>"
	value="<s:property value="finalProduct.id"/>"
                   	   
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="25" maxlength="50" id="finalProduct_name"
                   	 name="finalProduct.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="finalProduct.name"/>"
                   	  />
                   	  
                   	</td>
					<td class="label-title" ><fmt:message key="biolims.common.leader"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="finalProduct_createUser_name"  value="<s:property value="finalProduct.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="finalProduct_createUser" name="finalProduct.createUser.id"  value="<s:property value="finalProduct.createUser.id"/>" > 
 						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />    --%>                		
                   	</td>
			
               	 	<%-- <td class="label-title" ><fmt:message key="biolims.common.testingCycle"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_testTime"
                   	 name="finalProduct.testTime" title="<fmt:message key="biolims.common.testingCycle"/>"
                   	   
	value="<s:property value="finalProduct.testTime"/>"
                   	  />
                   	  
                   	</td> --%>
			</tr>
			<%-- <tr>
					<td class="label-title" ><fmt:message key="biolims.common.detectionMethod"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="finalProduct_testMethod_name"  value="<s:property value="finalProduct.testMethod.name"/>"  />
 						<input type="hidden" id="finalProduct_testMethod" name="finalProduct.testMethod.id"  value="<s:property value="finalProduct.testMethod.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectDetectionMethod"/>' id='showtestMethod' src='${ctx}/images/img_lookup.gif' onClick="checkTestMethod()"	class='detail'    />                   		
                   	</td>
               	 	
               	 	
			</tr> --%>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="finalProduct_createDate"
                   	 name="finalProduct.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="finalProduct.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
				<%-- <td class="label-title" ><fmt:message key="biolims.common.identificationCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_mark"
                   	 name="finalProduct.mark" title="<fmt:message key="biolims.common.identificationCode"/>" value="<s:property value="finalProduct.mark"/>"
                   	  />
                </td> --%>
				
                <g:LayOutWinTag buttonId="showparent" title='<fmt:message key="biolims.common.selectTheParent"/>'
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/finalProduct/productSelect.action"
				isHasSubmit="false" functionName="ProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('finalProduct_parent').value=rec.get('id');
				document.getElementById('finalProduct_parent_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.parent"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="finalProduct_parent_name"  value="<s:property value="finalProduct.parent.name"/>" />
 						<input type="hidden" id="finalProduct_parent" name="finalProduct.parent.id"  value="<s:property value="finalProduct.parent.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheParent"/>' id='showparent' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> <input type="button" value="清" onClick="javascript:$('#finalProduct_parent').val('');$('#finalProduct_parent_name').val('');">        		
                   	</td>
                   
                   		<td class="label-title" ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	  <td align="left"  >
                   	<select id="finalProduct_state" name="finalProduct.state">
								<option value="1" <s:if test="finalProduct.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"/></option>
								<option value="0" <s:if test="finalProduct.state==0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"/></option>
					</select>
                   	  
                   	</td>
			</tr>
			<%-- <tr>
				<g:LayOutWinTag buttonId="showAcceptUser" title='<fmt:message key="biolims.common.selectGroup"/>'
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('finalProduct_acceptUserGroup').value=rec.get('id'); 
					document.getElementById('finalProduct_acceptUserGroup_name').value=rec.get('name');" /> 			
					<td class="label-title" >负责组</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="finalProduct_acceptUserGroup_name"  value="<s:property value="finalProduct.acceptUserGroup.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="finalProduct_acceptUserGroup" name="finalProduct.acceptUserGroup.id"  value="<s:property value="finalProduct.acceptUserGroup.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td> 
               		<td class="label-title" >选择区块</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td> 			
					<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="finalProduct_orderBlock" name="finalProduct.orderBlock" value="<s:property value="finalProduct.orderBlock"/>"  readonly="readOnly"  />
 						<input type="hidden" id="finalProduct.orderBlockId" name="finalProduct.orderBlockId"  value="<s:property value="finalProduct.orderBlockId"/>" > 
 						<img alt='选择区块' id='showOrderBlock' src='${ctx}/images/img_lookup.gif' onClick="checkOrderBlock()" class='detail'    />                   		
               		</td> 
               		
               		<td class="label-title" >用量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_sampleNum"
                   	 name="finalProduct.sampleNum" title="<fmt:message key="产品用量"/>" value="<s:property value="finalProduct.sampleNum"/>"
                   	  />
                </td>
			</tr> --%>
			<tr>
					<td class="label-title" >终产品样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="finalProduct_dicSampleType_name"  value="<s:property value="finalProduct.dicSampleType.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="finalProduct_dicSampleType" name="finalProduct.dicSampleType.id"  value="<s:property value="finalProduct.dicSampleType.id"/>" > 
 						<img alt='选择终产品类型' onClick="loadTestDicSampleType();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td> 
               		<td class="label-title" >终产品代次</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15"   id="finalProduct_pronoun" name="finalProduct.pronoun" value="<s:property value="finalProduct.pronoun"/>"    />
               		</td>
               		<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_notes"
                   	 name="finalProduct.notes" title="Notes"
                   	   value="<s:property value="finalProduct.notes"/>"
                   	  />
                   	</td>
			</tr>
			
					
            </table>
        		<!-- <table width="100%" class="section_table" cellpadding="0" >
			<tbody>
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">科技服务产品明细</span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table> -->
			<%-- <table class="frame-table">
			
			
			
			
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.libraryType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  <select name="finalProduct.libraryType" id="finalProduct_libraryType" >
                  			<option value="" <s:if test="finalProduct.libraryType == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                  			<c:forEach var="listLibraryType" items="${listLibraryType}">
							<option value="${listLibraryType.name}" <c:if test="${finalProduct.libraryType==listLibraryType.name}">selected="selected"</c:if>>${listLibraryType.name}</option>
							</c:forEach>
                  		</select>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.methodology"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	   <select name="finalProduct.methodology" id="finalProduct_libraryType" >
                  			<option value="" <s:if test="finalProduct.methodology == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                  			<c:forEach var="listMethodology" items="${listMethodology}">
							<option value="${listMethodology.name}" <c:if test="${finalProduct.methodology==listMethodology.name}">selected="selected"</c:if>>${listMethodology.name}</option>
							</c:forEach>
                  		</select>
                   	</td>
                   	<td class="label-title" >Index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	   <select name="finalProduct.lciIndex" id="finalProduct_lciIndex" >
                  			<option value="" <s:if test="finalProduct.lciIndex == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                  			<c:forEach var="listLCIIndex" items="${listLCIIndex}">
							<option value="${listLCIIndex.name}" <c:if test="${finalProduct.lciIndex==listLCIIndex.name}">selected="selected"</c:if>>${listLCIIndex.name}</option>
							</c:forEach>
                  		</select>
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.insertSize"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_insertSize"
                   	 name="finalProduct.insertSize" title="Insert Size"
                   	   value="<s:property value="finalProduct.insertSize"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.targetYield"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_targetYield"
                   	 name="finalProduct.targetYield" title="Target Yield"
                   	   value="<s:property value="finalProduct.targetYield"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.QCRequirements"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_qcRequirements"
                   	 name="finalProduct.qcRequirements" title="QC Requirements"
                   	   value="<s:property value="finalProduct.qcRequirements"/>"
                   	  />
                   	</td>
				</tr>
				<tr style="display: none;">
                   	<td class="label-title" >GC%</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_lciGc"
                   	 name="finalProduct.lciGc" title="GC"
                   	   value="<s:property value="finalProduct.lciGc"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_notes"
                   	 name="finalProduct.notes" title="Notes"
                   	   value="<s:property value="finalProduct.notes"/>"
                   	  />
                   	</td>
                   		<td class="label-title" >GC%</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="finalProduct_siGc"
                   	 name="finalProduct.siGc" title="GC"
                   	   value="<s:property value="finalProduct.siGc"/>"
                   	  />
                   	</td> 
				</tr>
				<tr>
					<td class="label-title" ><fmt:message key="biolims.common.sequencingPlatform"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  <select name="finalProduct.sequencePlatform" id="finalProduct_sequencePlatform" >
                   			<option value="" <s:if test="finalProduct.sequencePlatform == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			
                   			<c:forEach var="listPlatForm" items="${listPlatForm}">
								<option value="${listPlatForm.name}" <c:if test="${finalProduct.sequencePlatform==listPlatForm.name}">selected="selected"</c:if>>${listPlatForm.name}</option>
								</c:forEach>
                   			
                   		</select>
                   	</td>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.sequencingType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  <select name="finalProduct.sequenceType" id="finalProduct_sequenceType" >
                   			<option value="" <s:if test="finalProduct.sequenceType == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listReadType" items="${listReadType}">
								<option value="${listReadType.name}" <c:if test="${finalProduct.sequenceType==listReadType.name}">selected="selected"</c:if>>${listReadType.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sequencingReadLong"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  <select name="finalProduct.sequenceLength" id="finalProduct_sequenceLength" >
                   			<option value="" <s:if test="finalProduct.sequenceLength == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listSequencingCycle" items="${listSequencingCycle}">
								<option value="${listSequencingCycle.name}" <c:if test="${finalProduct.sequenceLength==listSequencingCycle.name}">selected="selected"</c:if>>${listSequencingCycle.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
                   
               	 	
			</tr>
			<tr>
					
                   	<td class="label-title" >Index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  <select name="finalProduct.siIndex" id="finalProduct_siIndex" >
                   			<option value="" <s:if test="finalProduct.siIndex == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listIndex" items="${listIndex}">
								<option value="${listIndex.name}" <c:if test="${finalProduct.siIndex==listIndex.name}">selected="selected"</c:if>>${listIndex.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.clientIndex"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	  <select name="finalProduct.clientIndex" id="finalProduct_clientIndex" >
                   			<option value="" <s:if test="finalProduct.clientIndex == ''">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
                   			<c:forEach var="listClientIndex" items="${listClientIndex}">
								<option value="${listClientIndex.name}" <c:if test="${finalProduct.clientIndex==listClientIndex.name}">selected="selected"</c:if>>${listClientIndex.name}</option>
								</c:forEach>
                   		</select>
                   	</td>
                   	<td class="label-title" >phix%</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="finalProduct_phix"
                   	 name="finalProduct.phix" title="phix"
                   	   value="<s:property value="finalProduct.phix"/>"
                   	  />
                   	</td> 
			</tr>
			<tr style="display: none;">
					
                   	<td class="label-title" >Exclusive Lane</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_exclusiveLane"
                   	 name="finalProduct.exclusiveLane" title="Exclusive Lane"
                   	   value="<s:property value="finalProduct.exclusiveLane"/>"
                   	  />
                   	</td>
                   	<td class="label-title" >Exclusive Flowcell</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_exclusiveFlowcell"
                   	 name="finalProduct.exclusiveFlowcell" title="Exclusive Flowcell"
                   	   value="<s:property value="finalProduct.exclusiveFlowcell"/>"
                   	  />
                   	</td>
                   		<td class="label-title" ><fmt:message key="biolims.common.speciesType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="finalProduct_species"
                   	 name="finalProduct.species" title="<fmt:message key="biolims.common.speciesType"/>"
                   	   value="<s:property value="finalProduct.species"/>"
                   	  />
                   	</td> 
                   
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.deliveryMethod"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="finalProduct_deliveryMethod"
                   	 name="finalProduct.deliveryMethod" title="Delivery Method"
                   	   value="<s:property value="finalProduct.deliveryMethod"/>"
                   	  />
                   	</td> 
                   	<td class="label-title" ><fmt:message key="biolims.common.dataAnalysis"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_dataAnalysis"
                   	 name="finalProduct.dataAnalysis" title="Data Analysis"
                   	   value="<s:property value="finalProduct.dataAnalysis"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.analysisDetails"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_analysisDetails"
                   	 name="finalProduct.analysisDetails" title="Analysis Details"
                   	   value="<s:property value="finalProduct.analysisDetails"/>"
                   	  />
                   	</td>
			</tr>
			<tr style="display: none;">
					<td class="label-title" ><fmt:message key="biolims.common.extraction"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="finalProduct_extraction"
                   	 name="finalProduct.extraction" title="Delivery Method"
                   	   value="<s:property value="finalProduct.extraction"/>"
                   	  />
                   	</td> 
                   	<td class="label-title" ><fmt:message key="biolims.common.qcProtocol"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_qcProtocol"
                   	 name="finalProduct.qcProtocol" title="Data Analysis"
                   	   value="<s:property value="finalProduct.qcProtocol"/>"
                   	  />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.sequencingApplication"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img   class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="20" id="finalProduct_sequencingApplication"
                   	 name="finalProduct.sequencingApplication" title="Analysis Details"
                   	   value="<s:property value="finalProduct.sequencingApplication"/>"
                   	  />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.flux"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_flux"
                   	 name="finalProduct.flux" title="<fmt:message key="biolims.common.flux"/>" value="<s:property value="finalProduct.flux"/>"
                   	  />
                </td>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table> --%>
        		<table width="100%" class="section_table" cellpadding="0" >
			<tbody>
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"><fmt:message key="biolims.common.quoteInformation"/></span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
			<table class="frame-table">
			<tr>
			<td class="label-title" ><fmt:message key="biolims.common.projectQuote"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_money"
                   	 name="finalProduct.money" title="<fmt:message key="biolims.common.projectQuote"/>" value="<s:property value="finalProduct.money"/>"
                   	  />
                </td>
                <td class="label-title" ><fmt:message key="biolims.common.projectDiscount"/>(%)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_discount"
                   	 name="finalProduct.discount" title="<fmt:message key="biolims.common.projectDiscount"/>" value="<s:property value="finalProduct.discount"/>"
                   	  />
                </td>
			<td class="label-title" ><fmt:message key="biolims.common.projectPricing"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="finalProduct_productFee"
                   	 name="finalProduct.productFee" title="<fmt:message key="biolims.common.projectPricing"/>" value="<s:property value="finalProduct.productFee"/>"
                   	  />
                </td>	
			</tr>
			<tr>
				 	<td class="label-title" ><fmt:message key="biolims.common.termsOfPayment"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	  <td align="left"  >
                   	<select id="finalProduct_way" name="finalProduct.way" style="width:169" class="input-10-length" >
								<option value="0" <s:if test="finalProduct.way==0">selected="selected"</s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
								<option value="1" <s:if test="finalProduct.way==1">selected="selected"</s:if>><fmt:message key="biolims.common.halfMonth"/></option>
								<option value="2" <s:if test="finalProduct.way==2">selected="selected"</s:if>><fmt:message key="biolims.common.monthlyStatement"/></option>
								<option value="3" <s:if test="finalProduct.way==3">selected="selected"</s:if>><fmt:message key="biolims.common.quarterlyInvoicing"/></option>
								<option value="4" <s:if test="finalProduct.way==4">selected="selected"</s:if>><fmt:message key="biolims.common.paymentNow"/></option>
					</select>
			</tr>
			  </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="finalProduct.id"/>" />
            </form>
          
        	</div>
	</body>
	</html>
