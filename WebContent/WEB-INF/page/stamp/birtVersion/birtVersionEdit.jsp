<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>

.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}

#yesSpan {
	display: none;
}

#auditDiv {
	display: none;
}

#btn_submit {
	display: none;
}

#btn_changeState {
	display: none;
}

.sampleNumberInput {
	width: 250px;
	height: 34px;
	border-radius: 6px;
	border: 1px solid #ccc;
	/* box-shadow: 1px 1px 1px #888888; */
	position: absolute;
	left: 200px;
	top: 212px;
}

.sampleNumberInputTow {
	width: 250px;
	height: 34px;
	border-radius: 6px;
	border: 1px solid #ccc;
	/* box-shadow: 1px 1px 1px #888888; */
	position: absolute;
	left: 510px;
	top: 212px;
}

.sampleNumberDiv {
	position: absolute;
	left: 140px;
	top: 223px;
}

.sampleNumberDivtow {
	position: absolute;
	left: 466px;
	top: 223px;
}

.sampleNumberbutton {
	position: absolute;
	left: 760px;
	top: 212px;
	border-radius: 4px;
	border: 1px solid #ccc;
	background-color: #5cb85c;
	width: 38px;
	height: 36px;
}
</style>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">报表版本管理</h3>
					<small style="color: #fff;"> 创建人: <span
						id="birtVersion_createUser"><s:property
								value=" birtVersion.createUser.name" /></span> 创建时间: <span
						id="birtVersion_createDate"><s:date
								name="birtVersion.createDate" format="yyyy-MM-dd" /></span> 状态: <span
						id="birtVersion_state"><s:property
								value="birtVersion.stateName" /></span>
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="createUser_id"
								name="birtVersion.createUser.id"
								value="<s:property value=" birtVersion.createUser.id "/>" />
							<input type="hidden" id="createDate"
								name="birtVersion.createDate"
								value="<s:property value=" birtVersion.createDate "/>" /> <input
								type="hidden" id="state" name="birtVersion.state"
								value="<s:property value=" birtVersion.state "/>" /> <input
								type="hidden" id="stateName" name="birtVersion.stateName"
								value="<s:property value=" birtVersion.stateName "/>" /> <input
								type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
							<br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">编号 </span> <input
											list="sampleIdOne"
											changelog="<s:property value="birtVersion.id"/>"
											type="text" size="20" maxlength="25" id="birtVersion_id"
											name="birtVersion.id" class="form-control" title="编号"
											value="<s:property value="birtVersion.id "/>" /> <input
											type="hidden" size="20" maxlength="25"
											id="birtVersion_id_id"
											value="<s:property value="birtVersion.id"/>" /> <span
											class="input-group-btn"> </span>
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 名称 </span> <input
											list="sampleName"
											changelog="<s:property value="birtVersion.name"/>"
											type="text" size="20" maxlength="50"
											id="birtVersion_name" name="birtVersion.name"
											class="form-control" title="名称"
											value="<s:property value=" birtVersion.name "/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">模块 <img
											class='requiredimage' src='/images/required.gif' /></span> <input
											list="sampleName"
											changelog="<s:property value="birtVersion.models.id"/>"
											type="hidden" maxlength="50"
											id="birtVersion_models_id"
											name="birtVersion.models.id"
											class="form-control"
											value="<s:property value=" birtVersion.models.id "/>" />
										<input list="sampleName"
											changelog="<s:property value=" birtVersion.models.name "/>"
											type="text" readonly="readonly" size="20" maxlength="50"
											id="birtVersion_models_name"
											class="form-control"
											value="<s:property value=" birtVersion.models.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showModels()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div> 
							</div>
							<input type="hidden" id="id_parent_hidden"
								value="<s:property value=" birtVersion.id "/>" /> 
								<input type="hidden" id="changeLog" name="changeLog" />
							 <input type="hidden" id="itemJson" name="itemJson" />
						</form>
					</div>
				</div>
				    <!--table表格-->
					<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="birtVersionItemTable" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/stamp/birtVersion/birtVersionEdit.js"></script>
    <script type="text/javascript"
		src="${ctx}/js/stamp/birtVersion/birtVersionItem.js"></script>
</body>
</html>