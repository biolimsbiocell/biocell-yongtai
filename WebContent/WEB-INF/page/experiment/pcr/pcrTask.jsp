﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/pcr/pcrTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="pcrTask_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="pcrTask_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showreciveUser" title="选择实验员"
				hasHtmlFrame="true"
				html="${ctx}/experiment/pcr/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('pcrTask_reciveUser').value=rec.get('id');
				document.getElementById('pcrTask_reciveUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >实验员</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="pcrTask_reciveUser_name" searchField="true"  name="reciveUser.name"  value="" class="text input" />
 						<input type="hidden" id="pcrTask_reciveUser" name="pcrTask.reciveUser.id"  value="" > 
 						<img alt='选择实验员' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >实验时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startreciveDate" name="startreciveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="reciveDate1" name="reciveDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreciveDate" name="endreciveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="reciveDate2" name="reciveDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/pcr/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('pcrTask_createUser').value=rec.get('id');
				document.getElementById('pcrTask_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >下达人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="pcrTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="pcrTask_createUser" name="pcrTask.createUser.id"  value="" > 
 						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >下达时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showtemplate" title="选择模板"
				hasHtmlFrame="true"
				html="${ctx}/experiment/pcr/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('pcrTask_template').value=rec.get('id');
				document.getElementById('pcrTask_template_name').value=rec.get('name');" />
               	 	<td class="label-title" >模板</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="pcrTask_template_name" searchField="true"  name="template.name"  value="" class="text input" />
 						<input type="hidden" id="pcrTask_template" name="pcrTask.template.id"  value="" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >index</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="pcrTask_indexa"
                   	 name="indexa" searchField="true" title="index"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="pcrTask_state"
                   	 name="state" searchField="true" title="状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >状态名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="pcrTask_stateName"
                   	 name="stateName" searchField="true" title="状态名称"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="pcrTask_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/experiment/pcr/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('pcrTask_acceptUser').value=rec.get('id');
				document.getElementById('pcrTask_acceptUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >实验组</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="pcrTask_acceptUser_name" searchField="true"  name="acceptUser.name"  value="" class="text input" />
 						<input type="hidden" id="pcrTask_acceptUser" name="pcrTask.acceptUser.id"  value="" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >容器数量</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="pcrTask_maxNum"
                   	 name="maxNum" searchField="true" title="容器数量"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >质控品数量</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="pcrTask_qcNum"
                   	 name="qcNum" searchField="true" title="质控品数量"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_pcrTask_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_pcrTask_tree_page"></div>
</body>
</html>



