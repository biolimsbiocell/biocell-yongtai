<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=pcrTask&id=${pcrTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/pcr/pcrTaskEdit.js"></script>
<div style="float:left;width:25%" id="pcrTaskTempPage">
		<!-- <div id="tabs1">
            <ul>
	            <li><a href="#linchuang">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="linchuang" style="width: 100%;height: 10px;"></div>
				<div id="keji" style="width: 100%;height: 10px;"></div>
		 </div> -->

</div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_id"
                   	 name="pcrTask.id" title="编号" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="pcrTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_name"
                   	 name="pcrTask.name" title="描述"
                   	   
	value="<s:property value="pcrTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			 <td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="pcrTask_reciveUser_name"  value="<s:property value="pcrTask.reciveUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="pcrTask_reciveUser" name="pcrTask.reciveUser.id"  value="<s:property value="pcrTask.reciveUser.id"/>" > 
 						<img alt='请选择实验员' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
			
<%-- 			<g:LayOutWinTag buttonId="showreciveUser" title="选择实验员" --%>
<%-- 				hasHtmlFrame="true" --%>
<%-- 				html="${ctx}/core/user/userSelect.action" --%>
<%-- 				isHasSubmit="false" functionName="UserFun1"  --%>
<%--  				hasSetFun="true" --%>
<%-- 				documentId="pcrTask_reciveUser" --%>
<%-- 				documentName="pcrTask_reciveUser_name" /> --%>
				
			
			
<!--                	 	<td class="label-title" >实验员</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<%--  						<input type="text" size="20" readonly="readOnly"  id="pcrTask_reciveUser_name"  value="<s:property value="pcrTask.reciveUser.name"/>"  readonly="readOnly"  /> --%>
<%--  						<input type="hidden" id="pcrTask_reciveUser" name="pcrTask.reciveUser.id"  value="<s:property value="pcrTask.reciveUser.id"/>" >  --%>
<%--  						<img alt='选择实验员' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
<!--                    	</td> -->
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >实验时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="pcrTask_reciveDate"
                   	 name="pcrTask.reciveDate" title="实验时间"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="pcrTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun2" 
 				hasSetFun="true"
				documentId="pcrTask_createUser"
				documentName="pcrTask_createUser_name" />
				
			
			
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="pcrTask_createUser_name"  value="<s:property value="pcrTask.createUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="pcrTask_createUser" name="pcrTask.createUser.id"  value="<s:property value="pcrTask.createUser.id"/>" > 
 						<%-- <img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>              		
                   	</td>
			
			
               	 	<td class="label-title" >下达时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="pcrTask_createDate"
                   	 name="pcrTask.createDate" title="下达时间"
                   	   readonly = "readOnly" 
                   	      value="<s:date name="pcrTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			</tr>
			<tr>
			
			<td class="label-title" >完成时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="pcrTask_confirmDate"
                   	 name="pcrTask.confirmDate" title="下达时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="pcrTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
                   	
                   	
               	 	<td class="label-title" >模板</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="pcrTask_template_name"  value="<s:property value="pcrTask.template.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="pcrTask_template" name="pcrTask.template.id"  value="<s:property value="pcrTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"     />                   		
                   	</td>
                   	
                   	
				<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('pcrTask_acceptUser').value=rec.get('id');
				document.getElementById('pcrTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >实验组</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="pcrTask_acceptUser_name"  value="<s:property value="pcrTask.acceptUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="pcrTask_acceptUser" name="pcrTask.acceptUser.id"  value="<s:property value="pcrTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	
                   	
			
               	 <%-- 	<td class="label-title" >index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_indexa"
                   	 name="pcrTask.indexa" title="index"
                   	   
	value="<s:property value="pcrTask.indexa"/>"
                   	  />
                   	  
                   	</td> 
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_state"
                   	 name="pcrTask.state" title="状态"
                   	   
	value="<s:property value="pcrTask.state"/>"
                   	  />
                   	  
                   	</td>--%>
			</tr>
			<tr>
			
			<td class="label-title" >状态名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_stateName"
                   	 name="pcrTask.stateName" title="状态名称" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="pcrTask.stateName"/>"
                   	  />
                   	  
                   	</td>
               	 	
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_note"
                   	 name="pcrTask.note" title="备注"
                   	   
	value="<s:property value="pcrTask.note"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			</tr>
			<%-- <tr>
			
			
               	 	<td class="label-title" >容器数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_maxNum"
                   	 name="pcrTask.maxNum" title="容器数量"
                   	   
	value="<s:property value="pcrTask.maxNum"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >质控品数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="pcrTask_qcNum"
                   	 name="pcrTask.qcNum" title="质控品数量"
                   	   
	value="<s:property value="pcrTask.qcNum"/>"
                   	  />
                   	  
                   	</td>
			</tr> --%>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="pcrTaskItemJson" id="pcrTaskItemJson" value="" />
            <input type="hidden" name="pcrTaskTemplateJson" id="pcrTaskTemplateJson" value="" />
            <input type="hidden" name="pcrTaskReagentJson" id="pcrTaskReagentJson" value="" />
            <input type="hidden" name="pcrTaskCosJson" id="pcrTaskCosJson" value="" />
            <input type="hidden" name="pcrTaskResultJson" id="pcrTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="pcrTask.id"/>" />
            </form>
            <div id="pcrTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#pcrTaskTemplatepage">执行步骤</a></li>
			<li><a href="#pcrTaskReagentpage">原辅料明细</a></li>
			<li><a href="#pcrTaskCospage">设备明细</a></li>
           	</ul> 
			
			<div id="pcrTaskTemplatepage" width="100%" height:10px></div>
			<div id="pcrTaskReagentpage" width="100%" height:10px></div>
			<div id="pcrTaskCospage" width="100%" height:10px></div>
			
			</div>
			<div id="pcrTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
