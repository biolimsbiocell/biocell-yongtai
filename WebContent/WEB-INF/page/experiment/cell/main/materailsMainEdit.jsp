﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.chosedType {
	border: 2px solid #BAB5F6 !important;
}

.input-group {
	margin-top: 10px;
}

</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 50px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong><fmt:message
								key="biolims.common.cellMainData" /></strong>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefresh()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>

				</div>
				<div class="box-body">
					<input type="hidden" name="bpmTaskId" id="bpmTaskId"
						value="<%=request.getParameter(" bpmTaskId ")%>" />
					<form name="form1" id="form1" method="post">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.codingA" /><img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_id" name="id" class="form-control"
										title="" value="<s:property value=" materailsMain.id "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.designation" />
									</span> <input  changelog="<s:property value=" materailsMain.name "/>" list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_name" name="name"
										class="form-control" title=""
										value="<s:property value=" materailsMain.name "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.code" />
									</span> 
									<input readonly="readonly" changelog="<s:property value=" materailsMain.code "/>" list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_code" name="code"
										class="form-control" title=""
										value="<s:property value=" materailsMain.code "/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.tStorageModifyItem.num" />
									</span> <input  changelog="<s:property value=" materailsMain.stockNum "/>" list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_stockNum" name="stockNum"
										class="form-control" title=""
										value="<s:property value=" materailsMain.stockNum "/>" />
								</div>
							</div>
							<!-- 细胞类型 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.cellType" />
									</span> 
										<input type="text"
											changelog="<s:property value="materailsMain.cellType.name"/>" 
											size="10" id="materailsMain_cellType_name" name="cellType-name"
											readonly="readonly"
											value="<s:property value=" materailsMain.cellType.name "/>"
											class="form-control" /> <input type="hidden" 
											changelog="<s:property value="materailsMain.cellType.id"/>"
											id="materailsMain_cellType_id" name="cellType-id"
											value="<s:property value=" materailsMain.cellType.id "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='yushufs'
												onclick="showCellType()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
								</div>
							</div>
							<!-- 细胞培养方式 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.cultureType" />
									</span> 
									<input type="text"
											changelog="<s:property value="materailsMain.cultureType.name"/>" 
											size="10" id="materailsMain_cultureType_name" name="cultureType-name"
											readonly="readonly"
											value="<s:property value=" materailsMain.cultureType.name "/>"
											class="form-control" /> <input type="hidden" 
											changelog="<s:property value="materailsMain.cultureType.id"/>"
											id="materailsMain_cultureType_id" name="cultureType-id"
											value="<s:property value=" materailsMain.cultureType.id "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" id='yushufs'
												onclick="showCellCultureMethod()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.tStorage.source" />
									</span> <input  changelog="<s:property value=" materailsMain.source "/>" list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_source" name="source"
										class="form-control" title=""
										value="<s:property value=" materailsMain.source "/>" />
								</div>
							</div>
							<!-- 状态 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message key='biolims.common.state'/></span>
										<select lay-ignore id="materailsMain_dicState_id" name="materailsMain.dicState.id" class="form-control" 
										changelog="<s:property value=" materailsMain.dicState.id "/>">
											<option value="1b" <s:if test="materailsMain.dicState.id==1b">selected="selected"</s:if>>
												<fmt:message key='biolims.common.effective' />
											</option>
											<!-- 有效 -->
											<option value="0b" <s:if test="materailsMain.dicState.id==0b">selected="selected"</s:if>>
												<fmt:message key='biolims.common.invalid' />
											</option>
											<!-- 无效 -->
										</select>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.tInstrument.note" />
									</span> <input  changelog="<s:property value=" materailsMain.materailsName "/>" list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_materailsName" name="materailsName"
										class="form-control" title=""
										value="<s:property value="materailsMain.materailsName"/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.pronoun" />
									</span> <input  changelog="<s:property value=" materailsMain.pronoun "/>" list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_pronoun" name="pronoun"
										class="form-control" title=""
										value="<s:property value="materailsMain.pronoun"/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
							</div>
									<input type="hidden" changelog="<s:property value=" materailsMain.sampleCode "/>" list="materailsMainOne" type="text" size="20" maxlength="25"
										id="materailsMain_sampleCode" name="sampleCode"
										class="form-control" title=""
										value="<s:property value="materailsMain.sampleCode"/>" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/cell/main/materailsMainEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>
</html>