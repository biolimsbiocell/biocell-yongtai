﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/cell/main/materailsMainDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="type" value="${requestScope.type}">
		<input type="text"  id="projectId" value="${requestScope.projectId}">
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="materailsMain_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
               	<td class="label-title">名称</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="materailsMain_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
               	
            </tr>
			<tr>
           	 	
           	 	<td class="label-title">类型</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="materailsMain_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td>
            	
           	 	<td class="label-title">所属项目</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="materailsMain_project" searchField="true" name="project"  class="input-20-length"></input>
               	</td>
             </tr>
			<tr>    	
           	 	<td class="label-title">在库数量</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="materailsMain_stockNum" searchField="true" name="stockNum"  class="input-20-length"></input>
               	</td>
            	
           	 	<td class="label-title">入库日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="materailsMain_inDate" searchField="true" name="inDate"  class="input-20-length"></input>
               	</td>
           	 	<!-- <td class="label-title">存储位置</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="materailsMain_location" searchField="true" name="location"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">来源</td>
               	<td align="left">
                    		<input type="text" maxlength="20" id="materailsMain_source" searchField="true" name="source"  class="input-20-length"></input>
               	</td> -->
			 </tr>
			<tr> 
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="materailsMain_dicState" searchField="true" name="dicState"  class="input-20-length"></input>
               	</td>
            </tr>
			<tr>   	
           	 	<td class="label-title" style="display:none">content1</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content1" searchField="true" style="display:none" name="content1"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content2</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content2" searchField="true" style="display:none" name="content2"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content3</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content3" searchField="true" style="display:none" name="content3"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content4</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content4" searchField="true" style="display:none" name="content4"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content5</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content5" searchField="true" style="display:none" name="content5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content6</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content6" searchField="true" style="display:none" name="content6"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content7</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content7" searchField="true" style="display:none" name="content7"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content8</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="materailsMain_content8" searchField="true" style="display:none" name="content8"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content9</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="materailsMain_content9" searchField="true" style="display:none" name="content9"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content10</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="materailsMain_content10" searchField="true" style="display:none" name="content10"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title" style="display:none">content11</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="materailsMain_content11" searchField="true" style="display:none" name="content11"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title" style="display:none">content12</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="materailsMain_content12" searchField="true" style="display:none" name="content12"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
        
		</form>
		</div>
		<span onclick="sc()" style="cursor:hand" ><font color="blue">搜索</font></span>		
		<div id="show_dialog_materailsMain_div"></div>
		<div id="materailsMainCellDialogdiv"></div>
   		<div id="materailsMainPlasmidDialogdiv"></div>
   		<div id="materailsMainBACDialogdiv"></div>
   		<div id="materailsMainTissueDialogdiv"></div>
   		<div id="materailsMainSemenDialogdiv"></div>
	</body>
</html>



