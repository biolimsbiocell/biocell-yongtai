<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/bootstrap-slider/slider.css" />
		<style type="text/css">
			.table>tbody>tr>td {
				border-top: none;
			}
			
			.slider-horizontal {
				margin: 18px 0px;
			}
			
			.scjl .form-control {
				-webkit-box-shadow: none;
				box-shadow: none;
				border-top: none;
				border-left: none;
				border-right: none;
				margin-bottom: 8px;
			}
			.bwizard-steps li {
				padding: 0 !important;
			}
			.nav li a{
				display: inline-block !important;
				width: 133px !important;
				line-height: 39px !important;
				text-align: center !important;
			}
			.qualified {
				color : red;
			}
			/* element.style {
	           margin-top: 30px;
			} */
			.bootstrap-switch-label{
				width: 0;
			}
			.xbshow{
				display:none;
			}
		</style>
	</head>

	<body>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
		<!--下一步的询问狂-->
		<div id="nextAsk" class="hide">

		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
			<input type="hidden" id='orderNumBy' value="${requestScope.orderNumBy}" />

			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title"><fmt:message key="biolims.common.testSteps"/>
									<small style="color:#fff;">
										<fmt:message key="biolims.common.taskId" />: <span id="cellPassage_id"><s:property value="cellPassage.id"/></span>
										生产批号: <span id="cellPassage_batch"><s:property value="cellPassage.batch"/></span>
										<fmt:message key="biolims.common.commandPerson"/>: <span userId="<s:property value="cellPassage.createUser.id"/>"  id="cellPassage_createUser"><s:property value="cellPassage.createUser.name"/></span>
										<fmt:message key="biolims.sample.createDate"/>: <span id="cellPassage_createDate"><s:property  value="cellPassage.createDate"/></span>
										<fmt:message key="biolims.common.state"/>: <span state="<s:property value="cellPassage.state"/>"  id="cellPassage_state"><s:property value="cellPassage.stateName"/></span>
										<fmt:message key="biolims.common.experimenter"/>: <span id="testUserOneName"><s:property value="cellPassage.testUserOneName"/></span>
									</small>
								</h3>

							</div>
							<div class="box-body ipadmini">
								<!--步骤-->
								<div class="col-xs-2">
									<div id="wizard_verticle" class="form_wizard wizard_verticle" style="padding-top: 50px;">
										<ul class="list-unstyled wizard_steps">
										</ul>

									</div>
								</div>
								<!--步骤明细-->
								<div class="col-xs-10">
									<div class="box box-primary box-solid ipddd">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title">
												<span id="steptiele"></span>
											<small style="color:#fff;">
   												<fmt:message key="biolims.common.templateName"/>: <span id="templateName"><s:property value="cellPassage.template.name"/></span>
												<%-- <button class="btn btn-box-tool" onclick="$(this).next('i').text(parent.moment(new Date()).format('YYYY-MM-DD  HH:mm'))"><fmt:message key="biolims.common.startTime"/> </button><i id="startTime"  class="stepsTime"></i>
      											<button class="btn btn-box-tool" onclick="$(this).next('i').text(parent.moment(new Date()).format('YYYY-MM-DD  HH:mm'))"><fmt:message key="biolims.common.endTime"/> </button><i id="endTime"  class="stepsTime"></i> --%>
											</small>
											
											</h3>
											<div class="box-tools pull-right">
												<button type="button" id="stepContentBtn" class="btn btn-box-tool" style="font-size: 20px;border: 1px solid;"><i class="fa fa-file-word-o"></i>
                </button>
												<button type="button" class="btn btn-box-tool" style="font-size: 16px;border: 1px solid;" onclick="printTempalte()"><i class="fa fa-print"></i> 打印
                </button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12" id="stepContentModer" style="position: absolute;z-index: 6;height: 0%;right: 10px;width: 0%;overflow: hidden;">
												<div class="box box-success">
													<div class="box-header">
														<i class="glyphicon glyphicon-leaf"></i>
														<h3 class="box-title"><fmt:message key="biolims.common.stepDetail"/></h3>
														<div class="box-tools pull-right">

														</div>
													</div>
													<div class="box-body" id="stepcontent" style="height:100%; overflow: hidden;">
													</div>
												</div>
											</div>
											<input type="hidden" id="cellpassagebatch" value="<s:property value="cellPassage.batch"/>">
											<ul class="nav nav-tabs bwizard-steps" role="tablist" style="border: none; padding-left: 13px;">
											<li id="tt" role="presentation" class="active" href="#firstTab"  note="1"   aria-controls="firstTab" role="tab" data-toggle="tab">
												<a id="oo" >工前准备及检查</a>
											</li>
											<li id="qq" role="presentation" note="3"  href="#thirdTab" aria-controls="secondTab" role="tab" data-toggle="tab">
												<a id='qqq'>生产操作</a>
											</li>
											<li id="zj" role="presentation" note="2"  href="#secondTab" aria-controls="firstTab" role="tab" data-toggle="tab">
												<a>质检信息</a>
											</li>
											<!-- <li role="presentation" note="2">
												<a href="#secondTab" aria-controls="firstTab" role="tab" data-toggle="tab">质检信息</a>
											</li> -->
											<!-- <li role="presentation" note="4">
												<a href="#fourTab" aria-controls="thirdTab" role="tab" data-toggle="tab">完工清场</a> -->
											<li id="ww" role="presentation" note="4"  href="#fourTab" aria-controls="thirdTab" role="tab" data-toggle="tab">
												<a id="www">完工清场</a>
											</li>
											<!-- <button class="btn btn-info pull-right">xxxxx</button> -->
											
											</ul>

											<!-- Tab panes -->
											<div class="tab-content" style="padding-top: 8px;">
												<div role="tabpanel" class="tab-pane active" id="firstTab">
												<div class="col-xs-12">
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="glyphicon glyphicon-home"></i>
																<h3 class="box-title">房间确认</h3>
																<div class="box-tools pull-left">
																	监测信息：
																	<button class='btn btn-xs ' id="enmon1" onclick="enmonitor(1);">尘埃粒子</button>
																	<button class='btn btn-xs ' id="enmon2" onclick="enmonitor(2);">浮游菌</button>
																	<button class='btn btn-xs ' id="enmon3" onclick="enmonitor(3);">风量</button>
																	<button class='btn btn-xs ' id="enmon4" onclick="enmonitor(4);">沉降菌</button>
																	<button class='btn btn-xs ' id="enmon5" onclick="enmonitor(5);">表面微生物</button>
																	<button class='btn btn-xs ' id="enmon6" onclick="enmonitor(6);">压差</button>
																</div>
															</div>
															<div id="" class="box-body" style="overflow: auto;">
																<table class="table table-striped table-condensed table-responsive" style="font-size: 12px">
																	<thead>
																		<tr>
																			<th><span class="badge label-warning">序号</span></th>
																			<th><span class="badge label-warning">核查流程</span></th>
																			<th><span class="badge label-warning">核查结果</span></th>
																		</tr>
																	</thead>
																	<tbody id="pageOneRoomConfirm">
																	</tbody>
																</table>
															</div>
														</div>
												</div>
												<div class="col-xs-12">
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="glyphicon glyphicon-qrcode"></i>
																<h3 class="box-title">条码确认</h3>
																<div class="box-tools pull-right">
																</div>
															</div>
															<div id="" class="box-body" style="overflow: auto;">
																<div class="col-xs-12 col-sm-6 col-md-6">
																	<span class="badge bg-light-blue">样本条码确认</span>
																	<div class="input-group-addon">
																		<input type="text" id="sampleCodeConfirm" class="form-control" changelog="" placeholder="请扫码">
																	</div>
																	<table class="table table-striped table-condensed table-responsive" style="font-size: 12px">
																		<thead>
																			<tr>
																				<th><span class="badge bg-light-blue">条码</span></th>
																				<th><span class="badge bg-light-blue">核对结果</span></th>
																			</tr>
																		</thead>
																		<tbody id="pageOneQRcodesample">
																		</tbody>
																	</table>
																</div>
																<div class="col-xs-12 col-sm-6 col-md-6">
																	<span class="badge bg-light-blue">操作条码确认</span>
																	<div class="input-group-addon">
																		<input type="text" id="sampleCodeAfterConfirm" class="form-control" changelog="" placeholder="请扫码">
																	</div>
																	<table class="table table-striped table-condensed table-responsive" style="font-size: 12px">
																		<thead>
																			<tr>
																				<th><span class="badge bg-light-blue">条码</span></th>
																				<th><span class="badge bg-light-blue">核对结果</span></th>
																			</tr>
																		</thead>
																		<tbody id="pageOneQRcodeoperation">
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
												</div>
												<div class="col-xs-12" id="xbgc" >
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="glyphicon glyphicon-eye-open"></i>
																<h3 class="box-title">细胞观察</h3>
																<div class="box-tools pull-right">
												<!-- <button type="button" class="btn btn-box-tool btn-primary" id="qrsbfj" onclick="savePage1Cos()"><i class="fa  fa-save"></i>  保存设备房间并占用
									                </button> -->
									           <!--  <button type="button" class="btn btn-box-tool" id="ksgqzb" onclick="startPrepare()"><i class="fa  fa-save"></i>  开始工前准备
									                </button> -->
																</div>
															</div>
															<div id="" class="box-body" style="overflow: auto;">
																<table class="table table-striped table-condensed table-responsive" style="font-size: 12px">
																	<thead>
																		<tr>
																			<th><span class="badge label-warning">显微镜编号</span></th>
																			<th><span class="badge label-warning">日期</span></th>
																			<th><span class="badge label-warning">观察结果</span></th>
																			<th id="xbshow" class="xbshow"><span class="badge label-warning" >细胞计数</span></th>
																			<th><span class="badge label-warning">是否操作</span></th>
																			<th><span class="badge label-warning">观察人</span></th>
																			<th><span class="badge label-warning">备注</span></th>
																		</tr>
																	</thead>
																	<tbody id="pageOneChoseXbgc">
																	</tbody>
																</table>
															</div>
														</div>
												</div>
												<div class="col-xs-12">
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="fa fa-balance-scale"></i>
																<h3 class="box-title">选择设备</h3>
																<div class="box-tools pull-right">
												<button type="button" class="btn btn-box-tool btn-primary" id="qrsbfj" onclick="savePage1Cos()"><i class="fa  fa-save"></i>  保存设备房间并占用
									                </button>
									           <!--  <button type="button" class="btn btn-box-tool" id="ksgqzb" onclick="startPrepare()"><i class="fa  fa-save"></i>  开始工前准备
									                </button> -->
																</div>
															</div>
															<div id="" class="box-body" style="overflow: auto;">
																<table class="table table-striped table-condensed table-responsive" style="font-size: 12px">
																	<thead>
																		<tr>
																			<th><span class="badge bg-light-blue">生产步骤</span></th>
																			<th><span class="badge bg-light-blue">设备类型</span></th>
																			<th><span class="badge bg-light-blue">设备编号</span></th>
																			<th><span class="badge bg-light-blue">选定设备</span></th>
																			<th><span class="badge bg-light-blue">设备确认</span></th>
																		</tr>
																	</thead>
																	<tbody id="pageOneChoseYQ">
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													<div class="col-xs-12">
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="fa fa-balance-scale"></i>
																<h3 class="box-title">试剂准备</h3>
																<div class="box-tools pull-right">
												<!-- <button type="button" class="btn btn-box-tool btn-primary" id="qrsbfj" onclick="savePage1Cos()"><i class="fa  fa-save"></i>  保存设备房间并占用
									                </button> -->
									           <!--  <button type="button" class="btn btn-box-tool" id="ksgqzb" onclick="startPrepare()"><i class="fa  fa-save"></i>  开始工前准备
									                </button> -->
																</div>
															</div>
															<div id="" class="box-body" style="overflow: auto;">
																<table class="table table-striped table-condensed table-responsive" style="font-size: 12px">
																	<thead>
																		<tr>
																			<th><span class="badge label-warning">名称</span></th>
																			<th><span class="badge label-warning">厂家/品牌</span></th>
																			<th><span class="badge label-warning">规格</span></th>
																			<th><span class="badge label-warning">扫码</span></th>
																			<th><span class="badge label-warning">批号</span></th>
																			<th><span class="badge label-warning">有效期</span></th>
																			<th><span class="badge label-warning">数量</span></th>
																			<th><span class="badge label-warning">单位</span></th>
																		</tr>
																	</thead>
																	<tbody id="pageOneChoseSJ">
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													<div class="col-xs-9">
														<div class="box box-primary">
															<div class="box-header with-border">
																<i class="fa fa-hand-o-right"></i>
																<h3 class="box-title">操作指令</h3>
															</div>
															<div id="" class="box-body" style="overflow: auto;">
																<table class="table table-striped table-condensed table-responsive" style="font-size: 12px">
																	<thead>
																		<tr>
																			<th><span class="badge bg-light-blue">序号</span></th>
																			<th><span class="badge bg-light-blue">操作指令及工艺参数</span></th>
																			<th><span class="badge bg-light-blue">生产检查</span></th>
																			<th><span class="badge bg-light-blue">操作记录</span></th>
																		</tr>
																	</thead>
																	<tbody id="pageOneOperationalOrder">
																		
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													<div class="col-xs-3">
														<div class="box box-info">
															<div class="box-header with-border">
																<i class="fa  fa-keyboard-o"></i>
																<h3 class="box-title">生产记录</h3>
															</div>
															<div class="box-body scjl" id="pageOneProductionRecord"> 
														
															</div>

														</div>
													</div>
													
												</div>
												<div role="tabpanel" class="tab-pane" id="secondTab">
													<div class="col-xs-12">
														<div class="box box-primary">
															<div class="box-header with-border">
																<i class="fa fa-flask"></i>
																<h3 class="box-title">样本信息</h3>
															</div>
															<!-- 混合前 -->
															<div class="box-body "  style="display:none" >
																<table class="table table-hover table-striped table-bordered table-condensed" id="noBlendTable" style="font-size: 12px;"></table>
															</div>
															<!-- 混合后 -->
															<div class="box-body  " id="blend_div" style="display: none" >
																<table class="table table-hover table-striped table-bordered table-condensed" id="blendTable" style="font-size: 12px;"></table>
															</div>
															
															<div class="box-body" id="plateModal"
																style="display: none">
															<!-- <div id="plateModal"> class="col-md-12 col-xs-12"-->
																<input type="hidden" id="maxNum"
																	value="<s:property value=" qualityTest.maxNum "/>"> <input
																	type="hidden" id="temRow"
																	value="<s:property value=" qualityTest.template.storageContainer.rowNum "/>">
																<input type="hidden" id="temCol"
																	value="<s:property value=" qualityTest.template.storageContainer.colNum "/>">
																<div class="col-xs-12 plateDiv">
																	<div class="box box-primary">
																		<div class="box-header with-border">
																			<i class="glyphicon glyphicon-leaf"></i>
																			<h3 class="box-title">培养箱</h3>
																			<div class="box-tools pull-right" style="display: none">
																				<button type="button" class="btn btn-sm btn-info active"
																					order="h">
																					<i class="glyphicon glyphicon-resize-horizontal"></i> 横向排列
																				</button>
																				<button type="button" class="btn btn-sm btn-info" order="z">
																					<i class="glyphicon glyphicon-resize-vertical"></i> 纵向排列
																				</button>
																			</div>
																		</div>
									
																		<div class="box-body">
																			<div plateNum="" id="plateNum">
																				<table class="table table-bordered  plate"></table>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-xs-12" >
														<div class="box box-info" style="display:none">
															<div class="box-header with-border">
																<i class="fa fa-flask"></i>
																<h3 class="box-title">质检内容</h3>
																<small id="zjType" style="margin-left: 20px;font-weight: 900;"></small>
																<div class="box-tools" style="width: 70%; top: 2px;">
																	<!--<button class="btn btn-info btn-xs pull-right">生成指间任务</button>-->
																</div>
															</div>
															<div class="box-body">
																<ul class="todo-list" id="zhijianBody">
																</ul>
															</div>
														</div>
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="fa fa-flask"></i>
																<h3 class="box-title">已提交质检明细</h3>
															</div>
															<div id="gu" class="box-body">
														<div class="col-xs-12 col-sm-6 col-md-6">
															<span class="badge bg-light-blue">提交质检</span>
															<div class="input-group-addon">
																<input type="text" id="zhiJianCodeTiJiao"
																	class="form-control" changelog="" placeholder="请扫码">
															</div>
														</div>
														<table class="table table-hover table-striped table-bordered table-condensed" id="zhijianDetails" style="font-size: 12px;"></table>
															</div>
														</div>
													</div>
												</div>
												<div role="tabpanel" class="tab-pane" id="thirdTab">
													<ul class="timeline" style="margin: 0 15px;">
												
													</ul>
												</div>
												<div role="tabpanel" class="tab-pane" id="fourTab">
													<div class="col-xs-8">
														<div class="box box-primary">
															<div class="box-header with-border">
																<i class="fa fa-hand-o-right"></i>
																<h3 class="box-title">操作指令</h3>
															</div>
															<div id="" class="box-body" style="overflow: auto;">
																<table class="table table-responsive table-striped">
																	<thead>
																		<tr>
																			<th><span class="badge bg-light-blue">序号</span></th>
																			<th><span class="badge bg-light-blue">操作指令及工艺参数</span></th>
																			<th><span class="badge bg-light-blue">生产检查</span></th>
																		</tr>
																	</thead>
																	<tbody id="pageFourOperationalOrder">
																
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													<div class="col-xs-4">
														<div class="box box-info">
															<div class="box-header with-border">
																<i class="fa  fa-keyboard-o"></i>
																<h3 class="box-title">生产记录</h3>
															</div>
															<div class="box-body scjl" id="pageFourProductionRecord">
																
															</div>
															<div>
																<div id="tableFileLoad"></div>
																<div class="input-group">
																	<button type="button" class="btn btn-info btn-sm" onclick="fileInput1()">
																		上传附件
																	</button>&nbsp;&nbsp;
																	<button type="button" class="btn btn-info btn-sm"
																		onclick="fileView()">
																		查看附件
																	</button>
																</div>
																<br>
																<input type="button" class="form-control btn-success " id="jxsc" onclick="finishStep()" value="继续生产">
																<input type="button" style="margin-top:15px;display: none" class="form-control btn-success " id="next" value="填写生产结果">
																<br>
																<input type="button" class="form-control btn-success" id="bhgzz" onclick="finishStepFailed()" value="不合格终止">
																<br>
															<!-- 	<input type="button" id="chongfu" class="form-control btn-success" onclick="chongfu()" value="重复当前步骤"> -->
																<input type="hidden" id="changelog" name="changelog"  />
																
															</div>
														</div>
													</div>

												</div>

											</div>

										</div>
									</div>
								</div>

							</div>
							<div class="box-footer" style="position: relative;">
							
								<div class="save-item" style="position: fixed;right: 50px; top:132px">
									<button type="button" class="btn btn-primary" id="save" onclick="saveStepItem()"><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.save"/>
                </button>
									<button type="button" style="display: none" class="btn btn-primary" id="sp" onclick="sp()"><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.handle"/>
                </button>
									<button type="button" style="display: none" class="btn btn-primary" id="prev"><i class="glyphicon glyphicon-arrow-up"></i><fmt:message key="biolims.common.back"/>
                </button>
									<button type="button" style="display: none" class="btn btn-primary" id="next"><i class="glyphicon glyphicon-arrow-down"></i> <fmt:message key="biolims.common.nextStep"/>
                </button>
								</div>

							</div>
						</div>
					</div>

				</div>
			</section>
		</div>
		<script src="${ctx}/lims/dist/js/app.min.js "></script>
		<script src="${ctx}/lims/plugins/layer/layer.js" type="text/javascript" charset="utf-8"></script>
		<script src="${ctx}/lims/plugins/bootstrap-slider/bootstrap-slider.js" type="text/javascript" charset="utf-8"></script>
		<script src="${ctx}/js/experiment/cell/passage/cellPassageSteps.js" type="text/javascript" charset="utf-8"></script>
	</body>

</html>