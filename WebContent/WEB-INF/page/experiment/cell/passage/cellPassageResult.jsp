<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%@ include file="/WEB-INF/page/include/common.jsp"%>
	<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}
</style>
</head>

	<body>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<div id="batch_data"></div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
		  <input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" /> 
		  <input type="hidden" id="orderNumBy" value="${requestScope.orderNumBy}" /> 
		  <input type="hidden" id="confirmUser_name"
			value="<s:property value="cellPassage.confirmUser.name"/>">
		<input type="hidden" id="confirmUser_id"
			value="<s:property value="cellPassage.confirmUser.id"/>">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">
							<fmt:message key="biolims.common.experimentalResults"/> <small style="color: #fff;"> <fmt:message key="biolims.common.taskId"/>: <span
								id="cellPassage_id"><s:property value="cellPassage.id" /></span>
								<fmt:message key="biolims.common.commandPerson"/>: <span
								userId="<s:property value="cellPassage.createUser.id"/>"
								id="cellPassage_createUser"><s:property
										value="cellPassage.createUser.name" /></span> <fmt:message key="biolims.sample.createDate"/>: <span
								id="cellPassage_createDate"><s:property
										value="cellPassage.createDate" /></span> <fmt:message key="biolims.common.state"/>: <span
								state="<s:property value="cellPassage.state"/>"
								id="cellPassage_state"><s:property
										value="cellPassage.stateName" /></span>
							</small>
						</h3>
						<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefresh()"><i class="glyphicon glyphicon-refresh"></i>
                </button>
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
										<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();">
													<fmt:message key="biolims.common.print" />
												</a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();">
													复制表格数据
												</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="fixedCol()">
													固定列</a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()">
											            取消固定
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="box-body ipadmini">
							<input type="hidden"
							value="<s:property value="cellPassage.template.templateFieldsItemCode"/>"
							id="templateFieldsItemCode">
								<table class="table table-hover table-striped table-bordered table-condensed" id="cellPassageResultdiv" style="font-size: 14px;">
								</table>
							</div>
							<div class="box-footer">
								<!--<div class="pull-left">
									<button type="button" class="btn btn-primary" id="makeUpSave"><i class="glyphicon glyphicon-random"></i> 提交
                </button>
								</div>-->
								<div class="pull-right">
						<button type="button" class="btn btn-primary" id="save" onclick="saveItem()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.save"/>
                </button>
								<button type="button" class="btn btn-primary" style="display: none" id="sp" onclick="sp()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.handle"/>
                </button>
									<button type="button" class="btn btn-primary" id="prev"><i class="glyphicon glyphicon-arrow-up"></i> <fmt:message key="biolims.common.back"/>
                </button>
									<button type="button" class="btn btn-primary" id="finish"><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.workflow.completeName"/>
                </button>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>
		<div style="display: none" id="pronoun_data" >
			<div  class="input-group">
				<span class="input-group-addon bg-aqua">细胞代次</span> 
				<input type="text"
				id="pronoun" class="form-control"
				placeholder="请输入细胞代次"
				value="">
			</div>
		</div>
		<div style="display: none" id="batch_volume" >
			<div  class="input-group">
			<span class="input-group-addon bg-aqua">体积</span> 
			<input type="number"
				id="volume_value" class="form-control"
				placeholder="请输入体积"
				value="">
			</div>
		</div>
		<script type="text/javascript" src="${ctx}/js/experiment/cell/passage/cellPassageResult.js"></script>
	</body>

</html>