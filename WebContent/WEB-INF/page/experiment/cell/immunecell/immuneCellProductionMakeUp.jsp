<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>
		<style type="text/css">
			.dt-buttons {
				float: none;
			}
			.tablebtns{
				position: initial;
			}
		</style>
	</head>

	<body style="height:94%">
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
		<input type="hidden" id="immuneCellProduction_name" value="<s:property value="immuneCellProduction.name"/>">
	    <input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" /> 	
        <input
			type="hidden" id="confirmUser_name"
			value="<s:property value="immuneCellProduction.confirmUser.name"/>"> <input
			type="hidden" id="confirmUser_id"
			value="<s:property value="immuneCellProduction.confirmUser.id"/>">		
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">放置样本
									<small style="color:#fff;">
										<fmt:message key="biolims.common.taskId"/>: <span id="immuneCellProduction_id"><s:property value="immuneCellProduction.id"/></span>
										创建人: <span userId="<s:property value="immuneCellProduction.createUser.id"/>"  id="immuneCellProduction_createUser"><s:property value="immuneCellProduction.createUser.name"/></span>
										创建日期: <span id="immuneCellProduction_createDate"><s:property  value="immuneCellProduction.createDate"/></span>
										状态: <span state="<s:property value="immuneCellProduction.state"/>"  id="immuneCellProduction_state"><s:property value="immuneCellProduction.stateName"/></span>
									</small>
								</h3>
							</div>
							<div class="box-body">
								<!--排板前样本-->
								<div class="col-md-4 col-xs-12">
									<div class="box box-success">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title">待排板样本</h3>
										</div>
										<div class="box-body">
											<table class="table table-hover table-striped table-bordered table-condensed" id="immuneCellProductionMakeUpdiv" style="font-size:14px;"></table>
											<input type="hidden" id="immuneCellProduction_template_isSeparate" value="<s:property value="immuneCellProduction.template.isSeparate"/>">
										
										</div>
									</div>
								</div>
								<!--排板后样本-->
								<div class="col-md-8 col-xs-12">
									<div class="box box-success">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title">已排板样本</h3>
											<div class="pull-right">
												<input type="hidden" id="isBlend" value="<s:property value="immuneCellProduction.template.isBlend"/>" />
												<button class="btn btn-info" id="isBlendBtn"  onclick="compoundSample()" style="position:  fixed;right: 50px;z-index: 1000;">混样</button>
											</div>
										</div>
										<div class="box-body">
											<input type="hidden" id="blendCode" value="" />
											<table class="table table-hover table-striped table-bordered table-condensed" id="immuneCellProductionMakeUpAfdiv" style="font-size:14px;"></table>
										</div>
									</div>
								</div>
								<div class="col-md-12 col-xs-12" id="plateModal"  style="display: none">
									<input type="hidden" id="maxNum" value="<s:property value=" immuneCellProduction.maxNum "/>">
									<input type="hidden" id="temRow" value="<s:property value=" immuneCellProduction.template.storageContainer.rowNum "/>">
									<input type="hidden" id="temCol" value="<s:property value=" immuneCellProduction.template.storageContainer.colNum "/>">
									<div class="col-xs-12 plateDiv">
										<div class="box box-primary">
											<div class="box-header with-border">
												<i class="glyphicon glyphicon-leaf"></i>
												<h3 class="box-title">孔板</h3>
												<div class="box-tools pull-right">
												<button type="button" class="btn btn-sm btn-info active" order="h"><i class="glyphicon glyphicon-resize-horizontal"></i> 横向排列
                </button>
												<button type="button" class="btn btn-sm btn-info" order="z"><i class="glyphicon glyphicon-resize-vertical"></i> 纵向排列</button>
											</div>
											</div>

											<div class="box-body">
												<div plateNum="p1" id="plateNum">
													<table class="table table-bordered  plate"></table>
												</div>

											</div>
										</div>
									</div>

								</div>

							</div>
							<div class="box-footer">
								<div class="pull-right">
					<button type="button" class="btn btn-primary" onclick="saveImmuneCellProductionMakeUpAfTab()" id="save"><i class="glyphicon glyphicon-saved"></i> 保存
                </button>				
								<button type="button" class="btn btn-primary" onclick="tjsp()" id="tjsp"><i class="glyphicon glyphicon-random"></i> 提交
                </button>
                <button type="button" class="btn btn-primary" style="display: none" onclick="sp()" id="sp"><i class="glyphicon glyphicon-saved"></i> 办理
                </button>
									<button type="button" class="btn btn-primary" id="prev"><i class="glyphicon glyphicon-arrow-up"></i> 上一步
                </button>
									<button type="button" class="btn btn-primary" id="next"><i class="glyphicon glyphicon-arrow-down"></i> 下一步
                </button>
								</div>

							</div>
						</div>
					</div>

				</div>
			</section>
		</div>
		<div style="display: none" id="batch_data" >
		<div  class="input-group">
		<span class="input-group-addon bg-aqua">产物数量</span> 
		<input type="number"
			id="productNum" class="form-control"
			placeholder="请输入产物数量"
			value="">
		</div>
	</div>
		<script type="text/javascript" src="${ctx}/js/experiment/cell/immunecell/immuneCellProductionMakeUp.js"></script>
	    <script type="text/javascript" src="${ctx}/js/experiment/cell/immunecell/immuneCellProductionMakeUpAf.js"></script>
	</body>

</html>