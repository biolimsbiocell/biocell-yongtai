<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
/*用于检测类型的显示隐藏*/
.hiddle {
	display: none;
}

#btn_changeState {
	visibility: hidden;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 55px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">表面微生物监测记录</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<button id="butBirt" type="button" class="btn btn-box-tool"
							style="font-size: 16px; border: 1px solid;" onclick="dayin()">
							<i class="fa fa-print"></i> 打印
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<!--  <li><a href="####" onclick="dayin()">打印</a></li>-->
									<!-- <li><a href="####" onclick="printDataTablesItem()">打印</a></li> -->
									<li><a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
									</li>
									<li><a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li><a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
									</li>
									<li><a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
									</li>
								</ul>
							</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<form name="form1" id="form1" class="layui-form" method="post">
						<%-- <input type="hidden"
							value="<%=request.getParameter(" bpmTaskId ")%>" /> --%>
						<input type="hidden" id="bpmTaskId"
							value="${requestScope.bpmTaskId}" /> <br>
						<div class="row">
							<!-- 文件名称 -->
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">编号</span> <input type="text"
										id="id" changelog="<s:property value=" cam.id "/>"
										name="cam.id" class="form-control" readonly="readonly"
										value="<s:property value=" cam.id "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">车间/部门</span> <input type="text"
										changelog="<s:property value=" cam.workshopDepartment "/>"
										name="cam.workshopDepartment" class="form-control"
										value="<s:property value=" cam.workshopDepartment "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">区域<img
										class="requiredimage" src="/images/required.gif"></span> <input
										type="text" changelog="<s:property value=" cam.region "/>"
										id="region" name="cam.region" class="form-control"
										readonly="readonly" value="<s:property value=" cam.region "/>" />
									<input type="hidden" id="regionId" name="cam.regionId"
										class="form-control" title="区域" readonly="readonly"
										value="<s:property value=" cam.regionId"/>" /><span
										class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="choseArea()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">测试日期<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										changelog="<s:property value=" cam.testTime "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="cam_testTime" name="cam.testTime" title=""
										value="<s:property value="cam.testTime"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">测试人</span> <input type="text"
										changelog="<s:property value=" cam.testUser "/>"
										name="cam.testUser" class="form-control"
										value="<s:property value=" cam.testUser "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">培养基名称及规格</span> <input
										type="text" changelog="<s:property value=" cam.medium"/>"
										name="cam.medium" class="form-control"
										value="<s:property value=" cam.medium"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">培养基批号</span> <input type="text"
										changelog="<s:property value=" cam.mediumBatch"/>"
										name="cam.mediumBatch" class="form-control"
										value="<s:property value=" cam.mediumBatch"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">培养地点或设备名称</span> <input
										type="text"
										changelog="<s:property value=" cam.cultivateAddr"/>"
										name="cam.cultivateAddr" class="form-control"
										value="<s:property value=" cam.cultivateAddr"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">设备编号</span> <input type="text"
										changelog="<s:property value=" cam.equipmentNo"/>"
										name="cam.equipmentNo" class="form-control"
										value="<s:property value=" cam.equipmentNo"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">培养开始时间</span> <input
										changelog="<s:property value=" cam.cultivateStart "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="cam_cultivateStart" name="cam.cultivateStart" title=""
										value="<s:property value=" cam.cultivateStart " />" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">培养结束时间</span> <input
										changelog="<s:property value=" cam.cultivateEnd "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="cam_cultivateEnd" name="cam.cultivateEnd" title=""
										value="<s:property value=" cam.cultivateEnd " />" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">观察人</span> <input type="text"
										changelog="<s:property value=" cam.observeUser"/>"
										name="cam.observeUser" class="form-control"
										value="<s:property value=" cam.observeUser"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">观察日期</span> <input
										changelog="<s:property value=" cam.observeDate "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="cam_observeDate" name="cam.observeDate" title=""
										value="<s:date name=" cam.observeDate " format="yyyy-MM-dd"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">状态</span> <input type="text"
										id="stateName"
										changelog="<s:property value=" cam.stateName "/>"
										name="cam.stateName" class="form-control"
										value="<s:property value=" cam.stateName"/>" /> <input
										type="hidden" id="state"
										changelog="<s:property value=" cam.state "/>" name="cam.state"
										class="form-control" value="<s:property value=" cam.state"/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">创建人</span><input
										changelog="<s:property value=" cam.createUser.id "/>"
										type="hidden" size="20" id="cam_createUser_id"
										name="cam.createUser.id"
										value="<s:property value=" cam.createUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" cam.createUser.name "/>"
										type="text" readonly="readonly" id="cam_createUser_name"
										value="<s:property value=" cam.createUser.name "/>"
										class="form-control" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">复核人<img
										class="requiredimage" src="/images/required.gif"></span><input
										type="hidden" name="cam.confirmUser.id"
										id="cam_confirmUser_id"
										value="<s:property value=" cam.confirmUser.id"/>" /> <input
										type="text"
										changelog="<s:property value=" cam.confirmUser.name"/>"
										name="cam.confirmUser.name" class="form-control"
										id="cam_confirmUser_name"
										value="<s:property value=" cam.confirmUser.name"/>" /> <span
										class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showConfirmUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">复核日期</span> <input
										changelog="<s:property value=" cam.confirmDate "/>"
										class="form-control" type="text" size="20" readonly="readonly"
										maxlength="25" id="cam_confirmDate" name="cam.confirmDate"
										title="复核日期"
										value="<s:date name=" cam.confirmDate " format="yyyy-MM-dd"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">创建日期</span> <input type="text"
										readonly="readonly" id="gdate"
										changelog="<s:property value=" cam.createDate "/>"
										name="cam.createDate" class="form-control"
										value="<s:property value=" cam.createDate"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">通知人</span><input
										changelog="<s:property value=" cam.notifierId "/>"
										type="hidden" size="20" id="cam_notifierId"
										name="cam.notifierId"
										value="<s:property value=" cam.notifierId "/>"
										class="form-control" /> <input
										changelog="<s:property value=" cam.notifierName "/>"
										type="text" readonly="readonly" id="cam_notifierName"
										name="cam.notifierName"
										value="<s:property value=" cam.notifierName "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="findUsers()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">监测类型</span> <select
										class="form-control" lay-ignore="" id="type" name="cam.type"
										onchange="checkType()">
										<option value="0"
											<s:if test="cam.type==0">selected="selected"</s:if>>房间</option>
										<option value="1"
											<s:if test="cam.type==1">selected="selected"</s:if>>设备</option>
									</select>
								</div>
							</div>


							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">警戒线<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text" changelog="<s:property value="cam.cordon"/>"
										name="cam.cordon" class="form-control" id="cam_cordon"
										value="<s:property value=" cam.cordon "/>" />
								</div>
							</div>


						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">纠偏线<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text"
										changelog="<s:property value=" cam.deviationCorrectionLine"/>"
										name="cam.deviationCorrectionLine" class="form-control"
										id="cam_deviationCorrectionLine"
										value="<s:property value=" cam.deviationCorrectionLine"/>" />
								</div>
							</div>
							<div class="col-xs-4 ">
								<div class="input-group">
									<span class="input-group-addon">批准人<img
										class="requiredimage" src="/images/required.gif"></span><input
										changelog="<s:property value=" cam.approver.id "/>"
										type="hidden" size="20" id="cam_approver_id"
										name="cam.approver.id"
										value="<s:property value=" cam.approver.id "/>"
										class="form-control" title="批准人" /> <input
										changelog="<s:property value=" cam.approver.name "/>"
										type="text" readonly="readonly" id="cam_approver_name"
										value="<s:property value=" cam.approver.name "/>"
										class="form-control" title="批准人" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUser1()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>



						</div>


						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>
									&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileView()">
										<fmt:message key="biolims.report.checkFile" />
									</button>
								</div>
							</div>
						</div>
						<input type="hidden" id="changeLog" name="changeLog" /> <input
							type="hidden" id="changeLogItem" name="changeLogItem" /> <input
							type="hidden" id="changeLogItem2" name="changeLogItem2" /> <input
							type="hidden" id="documentInfoItemJson"
							name="documentInfoItemJson" /><input type="hidden"
							id=documentInfoItemJsontow name="documentInfoItemJsontow" />
					</form>
					<div style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>
					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="documentTable" style="font-size: 14px;">
						</table>
					</div>
					<div style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>
					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="cleanAreaMicroorganismTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/enmonitor/microorganism/cleanAreaMicroorganismEdit.js"></script>
</body>

</html>