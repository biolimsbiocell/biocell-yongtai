<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
/*用于检测类型的显示隐藏*/
.hiddle {
	display: none;
}
.dataTables_scrollHeadInner{
	width: 100% !important;
}
.dataTables_scrollHeadInner>table{
	width: 100% !important;
}
#btn_changeState{
   visibility:hidden;
}
.box{
    box-shadow: 0px 0px 0px !important;
}
.box.box-solid.box-info{
	border:none !important;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div id="tableFileLoad"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 55px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">房间模板配置</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									
									<li><a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
									</li>
									<li><a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li><a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
									</li>
									<li><a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
									</li>
								</ul>
							</div>
					</div>
				</div>
				<div class="box-body ">
					<!--form表单-->
					<form name="form1" id="form1" class="layui-form" method="post">
						<input type="hidden" id="bpmTaskId"
							value="${requestScope.bpmTaskId}" /> <br>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">编号</span> <input type="text"
										changelog="<s:property value="rt.id"/>" name="rt.id"
										class="form-control" title="编号" id="id"
										value="<s:property value="rt.id"/>" readOnly="readonly"/>
								</div>
							</div>
							<div class="col-xs-4 ">
								<div class="input-group">
									<span class="input-group-addon">创建人</span><input
										changelog="<s:property value=" rt.createUser.id "/>"
										type="hidden" size="20" id="createUser_id"
										name="rt.createUser.id"
										value="<s:property value=" rt.createUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" rt.createUser.name "/>"
										type="text" readonly="readonly" id="createUser_name"
										value="<s:property value=" rt.createUser.name "/>"
										class="form-control" />
								</div>
							</div>
						
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">监测类型</span> <select
										class="form-control" lay-ignore="" id="type" name="rt.type"
										onchange="checkType()">
										<option value="0"
											<s:if test="rt.type==0">selected="selected"</s:if>>房间</option>
									<!-- 	<option value="1"
											<s:if test="rt.type==1">selected="selected"</s:if>>设备</option> -->
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">备注</span> <input type="text"
										changelog="<s:property value=" rt.note "/>" name="rt.note"
										class="form-control" title="备注"
										value="<s:property value=" rt.note"/>" />
								</div>
							</div>
							<div class="col-xs-4 ">
								<div class="input-group">
									<span class="input-group-addon">审核人</span><input
										changelog="<s:property value="rt.confirmUser.id "/>"
										type="hidden" size="20" id="confirmUser_id"
										name="rt.confirmUser.id"
										value="<s:property value=" rt.confirmUser.id "/>"
										class="form-control" title="审核人" /> <input
										changelog="<s:property value=" rt.confirmUser.name "/>"
										type="text" readonly="readonly" id="confirmUser_name"
										value="<s:property value=" rt.confirmUser.name "/>"
										class="form-control" title="审核人" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>

						
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">状态</span> <input type="text"
										readonly="readonly"
										changelog="<s:property value="rt.stateName "/>"
										name="rt.stateName" class="form-control"
										value="<s:property value=" rt.stateName"/>" /> <input
										type="hidden" changelog="<s:property value="rt.state"/>"
										name="rt.state" class="form-control"
										value="<s:property value="rt.state"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">创建日期</span> <input type="text"
										id="gdate" readonly="readonly"
										changelog="<s:date name="rt.createDate" format="yyyy-MM-dd"/>"
										name="rt.createDate" class="form-control"
										value="<s:date name="rt.createDate" format="yyyy-MM-dd "/>" />
								</div>
							</div>
							<%-- <div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">完成日期</span> <input type="text"
										id="gdate" readonly="readonly"
										changelog="<s:date name="rt.finishDate" format="yyyy-MM-dd"/>"
										name="rt.finishDate" class="form-control"
										value="<s:date name="rt.finishDate" format="yyyy-MM-dd"/>" />
								</div>
							</div> --%>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">审核日期</span> <input type="text"
										readonly="readonly"
										changelog="<s:property value=" rt.confirmDate "/>"
										name="rt.confirmDate" class="form-control" id="rt_confirmDate"
										value="<s:date name="rt.confirmDate" format="yyyy-MM-dd"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">区域<img class="requiredimage" src="/images/required.gif"></span> <input type="text"
										readonly="readonly" id="region"
										changelog="<s:property value="rt.region "/>"
										name="rt.region" class="form-control"
										value="<s:property value="rt.region "/>" />
										<input type="hidden"
										readonly="readonly" id="regionId"
										name="rt.regionId" class="form-control"
										value="<s:property value="rt.regionId"/>" /> <span
										class="input-group-btn"><button class="btn btn-info"
											type="button" re onclick="choseArea()">
											<i class="glyphicon glyphicon-search"></i>
										</button></span>
								</div>
							</div>
						</div>
						
						<input type="hidden" id="changeLog" name="changeLog" />
						<input type="hidden" id="changeLogItem" name="changeLogItem" /> <input
							type="hidden" id="documentInfoItemJson"
							name="documentInfoItemJson" />
					</form>
					<div style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>
					<div class="">
						<div class="" style="min-height: 250px; width: 100%; position: relative;">
							<div class="HideShowPanel" style="position: absolute;width: 100%;">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="documentTable" style="font-size: 14px;">
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/enmonitor/template/roomTemplateEdit.js"></script>
</body>
</html>