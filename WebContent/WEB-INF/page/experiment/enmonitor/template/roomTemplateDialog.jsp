<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- 读取哪一个资源文件 -->
<fmt:setBundle basename="ResouseInternational/msg"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
.tablebtns{
				float:right;
			}
.dt-buttons{
				margin:0;
				float:right;
			}
.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
</style>
</head>
<body style="height:94%">
    	<input type="hidden" id="region" value="${requestScope.region}">
    
	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">编号</span> 
			<input type="text" size="200" maxlength="200" id="id"  searchname="id" class="form-control" placeholder="编号查询" >
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="input-group">
			<input style="text-align: center;" type="button" class="form-control btn-success" onclick="query()" value="查找">
		</div>
	</div>
	<div style="height:25px"></div>
	<div class="box-body ipadmini">
		<table class="table table-hover table-striped table-bordered table-condensed" id="main" style="font-size:14px;">
		</table>
	</div>
					
		<script type="text/javascript" src="${ctx}/js/experiment/enmonitor/template/roomTemplateDialog.js"></script>
</body>
</html>
