<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
/*用于检测类型的显示隐藏*/
.hiddle {
	display: none;
}

#btn_changeState {
	visibility: hidden;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 55px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">压差记录</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<button id="butBirt" type="button" class="btn btn-box-tool"
							style="font-size: 16px; border: 1px solid;" onclick="dayin()">
							<i class="fa fa-print"></i> 打印

							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<!-- <li><a href="####" onclick="dayin()">打印</a></li> -->
									<!-- <li><a href="####" onclick="printDataTablesItem()">打印</a></li> -->
									<li><a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
									</li>
									<li><a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li><a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
									</li>
									<li><a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
									</li>
								</ul>
							</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<form name="form1" id="form1" class="layui-form" method="post">
						<%-- <input type="hidden"
							value="<%=request.getParameter(" bpmTaskId ")%>" /> --%>
						<input type="hidden" id="bpmTaskId"
							value="${requestScope.bpmTaskId}" /> <br>
						<div class="row">
							<!-- 文件名称 -->
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">编号</span> <input type="text"
										id="id" changelog="<s:property value=" cad.id "/>"
										name="cad.id" class="form-control" readonly="readonly"
										value="<s:property value=" cad.id "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">测试人</span> <input type="text"
										changelog="<s:property value=" cad.testUser "/>"
										name="cad.testUser" class="form-control"
										value="<s:property value=" cad.testUser "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">测试时间<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text" changelog="<s:property value=" cad.testTime "/>"
										name="cad.testTime" class="form-control" id="cad_testTime"
										value="<s:property value=" cad.testTime "/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<%-- <div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">设备及编号</span> <input type="text"
										changelog="<s:property value=" cad.measuringInstrumentCode"/>"
										name="cad.measuringInstrumentCode" class="form-control"
										value="<s:property value=" cad.measuringInstrumentCode"/>" />
								</div>
							</div> --%>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">区域<img class="requiredimage" src="/images/required.gif"></span> <input type="text"
										readonly="readonly" id="region"
										changelog="<s:property value=" cad.region "/>"
										name="cad.region" class="form-control"
										value="<s:property value=" cad.region "/>" /> <input
										type="hidden" id="regionId" name="cad.regionId"
										class="form-control" title="区域" readonly="readonly"
										value="<s:property value="cad.regionId"/>" /><span
										class="input-group-btn"><button class="btn btn-info"
											type="button" re onclick="choseArea()">
											<i class="glyphicon glyphicon-search"></i>
										</button></span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">洁净级别</span><select
										class="form-control" lay-ignore="" id="cad_cleanlinessClass"
										name="cad.cleanlinessClass">
										<option value="0"
											<s:if test="cad.cleanlinessClass==0">selected="selected"</s:if>>A</option>
										<option value="1"
											<s:if test="cad.cleanlinessClass==1">selected="selected"</s:if>>B</option>
										<option value="2"
											<s:if test="cad.cleanlinessClass==2">selected="selected"</s:if>>C</option>
										<option value="3"
											<s:if test="cad.cleanlinessClass==3">selected="selected"</s:if>>D</option>
									</select>
									<%-- <input type="text"
										changelog="<s:property value=" cad.cleanlinessClass "/>"
										name="cad.cleanlinessClass" class="form-control"
										value="<s:property value=" cad.cleanlinessClass "/>" /> --%>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">状态</span> <input type="text"
										id="stateName"
										changelog="<s:property value=" cad.stateName "/>"
										name="cad.stateName" class="form-control"
										value="<s:property value=" cad.stateName"/>" /> <input
										type="hidden" id="state"
										changelog="<s:property value=" cad.state "/>" name="cad.state"
										class="form-control" value="<s:property value=" cad.state"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">复核人<img
										class="requiredimage" src="/images/required.gif"></span><input
										type="hidden" name="cad.confirmUser.id"
										id="cad_confirmUser_id"
										value="<s:property value=" cad.confirmUser.id"/>" /> <input
										type="text"
										changelog="<s:property value=" cad.confirmUser.name"/>"
										readonly name="cad.confirmUser.name" class="form-control"
										id="cad_confirmUser_name"
										value="<s:property value=" cad.confirmUser.name"/>" /> <span
										class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showConfirmUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">复核日期</span> <input type="text"
										readonly="readonly"
										changelog="<s:property value=" cad.confirmDate "/>"
										name="cad.confirmDate" class="form-control"
										value="<s:property value=" cad.confirmDate"/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">创建人</span><input
										changelog="<s:property value=" cad.createUser.id "/>"
										type="hidden" size="20" id="cad_createUser_id"
										name="cad.createUser.id"
										value="<s:property value=" cad.createUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" cad.createUser.name "/>"
										type="text" readonly="readonly" id="cad_createUser_name"
										value="<s:property value=" cad.createUser.name "/>"
										class="form-control" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">创建日期</span> <input type="text"
										readonly="readonly" id="gdate"
										changelog="<s:property value=" cad.createDate "/>"
										name="cad.createDate" class="form-control"
										value="<s:property value=" cad.createDate"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">监测类型</span> <select
										class="form-control" lay-ignore="" id="type" name="cad.type"
										onchange="checkType()">
										<option value="0"
											<s:if test="cad.type==0">selected="selected"</s:if>>房间</option>
										<option value="1"
											<s:if test="cad.type==1">selected="selected"</s:if>>设备</option>
									</select>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">通知人</span><input
										changelog="<s:property value=" cad.notifierId "/>"
										type="hidden" size="20" id="cad_notifierId"
										name="cad.notifierId"
										value="<s:property value=" cad.notifierId "/>"
										class="form-control" /> <input
										changelog="<s:property value=" cad.notifierName "/>"
										type="text" readonly="readonly" id="cad_notifierName"
										name="cad.notifierName"
										value="<s:property value=" cad.notifierName "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="findUsers()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-xs-4 ">
								<div class="input-group">
									<span class="input-group-addon">批准人<img
										class="requiredimage" src="/images/required.gif"></span><input
										changelog="<s:property value=" cad.approver.id "/>"
										type="hidden" size="20" id="cad_approver_id"
										name="cad.approver.id"
										value="<s:property value=" cad.approver.id "/>"
										class="form-control" title="批准人" /> <input
										changelog="<s:property value=" cad.approver.name "/>"
										type="text" readonly="readonly" id="cad_approver_name"
										value="<s:property value=" cad.approver.name "/>"
										class="form-control" title="批准人" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUser1()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>
									&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileView()">
										<fmt:message key="biolims.report.checkFile" />
									</button>
								</div>
							</div>
						</div>
						<input type="hidden" id="changeLog" name="changeLog" /> <input
							type="hidden" id="changeLogItem" name="changeLogItem" /> <input
							type="hidden" id="documentInfoItemJson"
							name="documentInfoItemJson" />
					</form>
					<div style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>
					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="documentTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/enmonitor/differentialpresure/cleanAreaDiffpresureEdit.js"></script>
</body>

</html>