﻿<%-- 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/system/quality/qualityProduct.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
	<style>
		.dataTables_scrollHead{
			width: 100% !important;
		}
		.dataTables_scrollHeadInner{
			width: 100% !important;
		}
		.dataTables_scrollHeadInner>table{
			width: 100% !important;
		}
	</style>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="qualityProduct_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"   style="display:none"    />
                 
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="qualityProduct_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.selectTheCreatePerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/system/quality/qualityProduct/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('qualityProduct_createUser').value=rec.get('id');
				document.getElementById('qualityProduct_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="qualityProduct_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="qualityProduct_createUser" name="qualityProduct.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheCreatePerson"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.expectedValue"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="qualityProduct_expectValue"
                   	 name="expectValue" searchField="true" title="<fmt:message key="biolims.common.state"/>"    />
                   	 
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_qualityProduct_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_qualityProduct_tree_page"></div>
</body>
</html>



 --%>
 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Font Awesome -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>

	<body style="height:94%">
		<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12 col-md-12">
						<div class="box box-info box-solid" id="box">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title"><fmt:message key="biolims.common.QualityProduct"/></h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefresh()"><i class="glyphicon glyphicon-refresh"></i>
                </button>
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
										<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();">
													<!-- 打印 -->
													<fmt:message key="biolims.common.print" />
												</a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();">
													<!-- 复制表格数据 -->
													<fmt:message key="biolims.common.copyData" />
												</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="fixedCol()">
													<!-- 固定前两列 -->
													<fmt:message key="biolims.common.lock2Col" /></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()">
													<fmt:message key="biolims.common.cancellock2Col" />
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="box-body">
								<table class="table table-hover table-responsive table-striped table-bordered table-condensed" id="main" style="font-size:14px;">
								</table>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<div id="show_sampleOrder_div"></div>
		<script type="text/javascript" src="${ctx}/js/system/quality/qualityProduct.js"></script>
	</body>

</html>