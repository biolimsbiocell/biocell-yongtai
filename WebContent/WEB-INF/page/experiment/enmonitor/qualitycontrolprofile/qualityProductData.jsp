﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- Font Awesome -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<!--<link rel="stylesheet" href="${ctx}/lims/css/layui.css" /> -->
<script type="text/javascript" src="${ctx}/biolims/layui.all.js"></script>
<script type="text/javascript" src="${ctx}/biolims/js/echarts.js"></script>
<script language="javascript" src="${ctx}/javascript/jQuery.print.js"></script>
<script  src="http://www.jq22.com/jquery/jquery-migrate-1.2.1.min.js"></script>
<style type="text/css">
.dataTables_scrollBody {
	max-height: 250px;
}
.dataTables_scrollHead{
			width: 100% !important;
		}
		.dataTables_scrollHeadInner{
			width: 100% !important;
		}
		.dataTables_scrollHeadInner>table{
			width: 100% !important;
		}
		.dataTables_scrollBody{
			width: 100% !important;
		}
</style>
</head>

<body style="height: 94%">
	<%-- 	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div> --%>

	<div class="content-wrapper" id="content" style="margin-left: 0px;">

		<section class="content">

		<div class="row">
			<!--表格-->
			<div class="col-xs-12 col-md-12">
				<div class="box box-info box-solid" id="box">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>

						<h3 class="box-title">趋势图查询</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh"
								onclick="tableRefresh()">
								<i class="glyphicon glyphicon-refresh"></i>
							</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="####" onclick="$('.buttons-print').click();">
											<!-- 打印 --> <fmt:message key="biolims.common.print" />
									</a></li>
									<li><a href="#####" onclick="$('.buttons-copy').click();">
											<!-- 复制表格数据 --> <fmt:message key="biolims.common.copyData" />
									</a></li>
									<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
									</li>
									<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li><a href="####" onclick="fixedCol()"> <!-- 固定前两列 -->
											<fmt:message key="biolims.common.lock2Col" /></a></li>
									<li><a href="####" id="unfixde" onclick="unfixde()"> <fmt:message
												key="biolims.common.cancellock2Col" />
									</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div id="searchTop" style="margin-top: 10px">
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">趋势图模块</span>
								<select class="form-control" id="qstmk">
									<option value=''>空</option>
									<option value='1'>洁净区尘埃粒子0.5μm</option>
									<option value='2'>洁净区尘埃粒子5.0μm</option>
									<option value='3'>洁净区浮游菌菌落数</option>
									<option value='4'>洁净区沉降菌菌落数</option>
									<option value='5'>表面微生物菌落数</option>
									<!-- <option value='6' >洁净区风量换气次数</option>
									<option value='7' >压差相对压差</option> -->
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">开始日期</span>
								<input id="startDate" class="form-control Wdate1 col-md-4"
									type="text" placeholder="开始时间(yyyy-MM-dd)" title="开始时间" />

							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">结束日期</span>
								<input id="endDate" class="form-control Wdate1" type="text"
									placeholder="结束时间(yyyy-MM-dd)" title="结束时间" />

							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">

							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">类型</span>
								<select class="form-control" id="typeSelect">
									<option value=''>空</option>
									<option value='0'>房间</option>
									<option value='1'>设备</option>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">房间/设备</span>
								<input id="bianma" class="form-control" type="text"
									title="房间/设备编码" /> <input id="bianma" class="form-control"
									type="hidden" title="房间/设备编码" /> <span class="input-group-btn">
									<button class="btn btn-info" type="button"
										onclick="showApprovalUser()">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</span>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">清洁区状态</span>
								<select class="form-control" id="cleanStateSelect">
									<option value=''>空</option>
									<option value='0'>动态</option>
									<option value='1'>静态</option>
								</select>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">监测点</span>
								<input id="monitoringPoint" class="form-control " type="text"
									title="监测点" />

							</div>
						</div>
						<!-- <div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">置信上限(0.5um)</span>
								<input id="uclMaxpointFive" class="form-control " type="text"
									title="置信上限0.5um" />

							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<span class="input-group-addon" style="font-family: 黑体;">置信上限(5um)</span>
								<input id="uclMaxFive" class="form-control " type="text"
									title="置信上限5um" />

							</div>
						</div> -->
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="input-group ">
								<button id="searchBtn" onclick="chaxun()"
									class="btn btn-info btn-sm">搜索</button>
								<span> </span>
							</div>
						</div>
					</div>
					<div class="box-body">
						<table
							class="table table-hover table-responsive table-striped table-bordered table-condensed"
							id="mains" style="font-size: 14px;">
						</table>
					</div>
				</div>
				<div id="messaryReport" style="width: 100%; height: 500px;"></div>
				<!-- <div id="chart" style="width: 100%; height: 500px;"></div>-->
			</div>

		</div>

		</section>
	</div>
	<!-- 	<div id="img_box4" style=" display: none;">
		<div style="height: 30px;margin-left: 200px"><strong>&emsp;质控图</strong></div>
		<div style="height: 40px">仪器名称:<span id="yqmc">分子病毒核酸检测[47]</span>&emsp;质控项目:<span id="zkxm"></span>&emsp;日期区间:<span id="rqqj"></span></div>
		<div style="height: 30px">质控规则:<span id="zkgz">1_3S,2_2S,R4S,10X</span></div>
		<div id="zkt"></div>
		<div style="height: 30px">水平H:<span id="sp"></span></div>
		<div>
		
			<table
							class="table table-hover table-responsive table-striped table-bordered table-condensed"
							 style="font-size: 14px;margin-top: 15px">
			<tbody id="rishui">
			
			</tbody>
			</table>
		</div>
		<div style="height: 100px">失控及处理:<br>
			<span id="skcl"></span>
		</div>
		<div style="height: 40px">质控评价:<br><span id="zkpj"></span></div>
		<div style="padding-top: 370px"> 操作者:<U id="zkp">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</U>&emsp;&emsp;&emsp;&emsp;&emsp;核对者:<U>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</U>&emsp;&emsp;&emsp;&emsp;&emsp;主管签名:<U>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</U></div>
	</div> -->
	<script type="text/javascript"
		src="${ctx}/js/experiment/enmonitor/qualitycontrolprofile/qualityProductData.js"></script>
	<!-- <script type="text/javascript"
		src="${ctx}/js/system/quality/qualityChart.js"></script>-->
</body>


</html>