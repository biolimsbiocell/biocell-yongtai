<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>质控报表</title>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/css/layui.css" />
<script type="text/javascript" src="${ctx}/biolims/layui.all.js"></script>
<script type="text/javascript" src="${ctx}/biolims/js/echarts.js"></script>
<script type="text/javascript" src="${ctx}/biolims/js/jquery-1.7.2.js"></script>
<style type="text/css">
body {
	padding: 0;
	margin: 0;
	height: 100%;
	width: 100%;
}

#searchTop {
	margin-top: 1%;
}

#searchTop .layui-input {
	display: inline-block;
	/*行列元素*/
	width: 12%;
}

#searchTop button {
	display: inline-block;
}

#searchTop span {
	font-size: 18px;
	padding-left: 2%;
}
</style>
<script type="text/javascript">
	//x轴最大值默认
	var xMax = 30;
	// Y轴min值
	var yMin = parseFloat(50.00);
	// Y轴max值
	var yMax = parseFloat(250.00);
	//改变后点的数据
	var reportDate = "";
	//当前页数
	var currentPage = 1;
	//总共的数据条数
	var countData = 15;

	var dataQua = new Array();

	
	$(function() {
		$("#searchBtn").click(function() {
			var number = 0;
			reportDate = "";
			messReport.showLoading();
			var dateStart = document.getElementById("startDate").value;
			var dateEnd = document.getElementById("endDate").value;
			var day = Math.floor((new Date(dateEnd).getTime() - new Date(dateStart).getTime())
							/ (3600 * 1000 * 24));
			if (dateStart != '' && dateStart != '') {
				xMax = day;
			}
			//更新echarts报表数据
			var options = messReport.getOption();
			options.xAxis[0].max = xMax;
			messReport.hideLoading();
			messReport.setOption(options);
			//ajax异步加载数据,计算参数
			/* $.ajax({
				url : ctx + '/quality/Control/showQualityControlReportListJson.action',
				dataType : "json",
				type : "post",
				data : {
					startDate : $("#startDate").val(),
					endDate : $("#endDate").val(),
					page : 1,
					qualityTypeSelect : $("#qualityTypeSelect").val(),
					limit : 1000
				},
				success : function(data) {
					$.each(data.data, function(i, n) {
						var str = n.qualityNumValue;
						var strDate = n.qualityDate;
						var strArray = str.split(",");
						
						for(var m=0;m<strArray.length;m++){
							strDate = strDate.substring(strDate.lastIndexOf("-")+1,strDate.lastIndexOf("-")+3);
							reportDate += "["+parseInt(strDate, 10)+","+parseInt(strArray[m], 10)+"],";
							dataQua[number]=strArray[m];
							number++;
						}
						
					});
				}
			}); */
			//重加载表格
			layui.use('table', function() {
				var table = layui.table;
				table.reload('massaryTable', {
					where : {
						//设定异步数据接口的额外参数，任意设
						startDate : $("#startDate").val(),
						endDate : $("#endDate").val(),
						qualityTypeSelect : $("#qualityTypeSelect").val(),
						productIDSelect : $("#productIDSelect").val()
					},
					page : {
						curr : 1
					//重新从第 1 页开始
					}
				});
			});
		});
		//计算参数值
		$("#searchMathBtn").click(function() {
			var xValue = Math.round(mean(dataQua) * 100) / 100;
			var sdValue = Math.round(stdDev(dataQua) * 100) / 100;
			var cvValue = Math.round(((sdValue/xValue) * 100) * 100) / 100;
			var xs1Value = Math.round((xValue + 1 * sdValue) * 100) / 100;
			var fuxs1Value = Math.round((xValue - 1 * sdValue) * 100) / 100;
			var xs2Value = Math.round((xValue + 2 * sdValue) * 100) / 100;
			var fuxs2Value = Math.round((xValue - 2 * sdValue) * 100) / 100;
			var xs3Value = Math.round((xValue + 3 * sdValue) * 100) / 100;
			var fuxs3Value = Math.round((xValue - 3 * sdValue) * 100) / 100;
			var xs6Value = Math.round((xValue + 6 * sdValue) * 100) / 100;
			var fuxs6Value = Math.round((xValue - 6 * sdValue) * 100) / 100;
			document.getElementById("xValue").value = xValue;
			document.getElementById("sdValue").value = sdValue;
			document.getElementById("cvValue").value = cvValue;
			document.getElementById("xs1Value").value = xs1Value;
			document.getElementById("fuxs1Value").value = fuxs1Value;
			document.getElementById("xs2Value").value = xs2Value;
			document.getElementById("fuxs2Value").value = fuxs2Value;
			document.getElementById("xs3Value").value = xs3Value;
			document.getElementById("fuxs3Value").value = fuxs3Value;
			document.getElementById("xs6Value").value = xs6Value;
			document.getElementById("fuxs6Value").value = fuxs6Value;
			
			//更新echarts报表数据
			var optionY = messReport.getOption();
			//更新Y轴数据
			optionY.yAxis[0].max = xs6Value;
			optionY.yAxis[0].min = fuxs6Value;
			//更新点的数据
			if(reportDate!=""){
				reportDate = reportDate.substring(0,reportDate.length-1);
			}
			optionY.series[0].data = eval("("+"["+reportDate+"]"+")");
			//更新横线的数据
			markLineOpt[0].tooltip.formatter='data：'+fuxs1Value;
			
			var coordfu1 = "[ {" + "coord : [ 0, "+fuxs1Value+" ],"+
				"symbol : 'none'"+ "}, {"+
				"coord : [ "+xMax+", "+fuxs1Value+" ],"+
				"symbol : 'arrow'"+ "} ]";
			markLineOpt[0].data[0]= eval("("+coordfu1+")");
			
			markLineOpt[1].tooltip.formatter='data：'+xs1Value;
			var coord1 = "[ {" + "coord : [ 0, "+xs1Value+" ],"+
				"symbol : 'none'"+ "}, {"+
				"coord : [ "+xMax+", "+xs1Value+" ],"+ 
				"symbol : 'arrow'"+ "} ]";
			markLineOpt[1].data[0]= eval("("+coord1+")");
			
			markLineOpt[2].tooltip.formatter='data：'+fuxs2Value;
			var coordfu2 = "[ {" + "coord : [ 0, "+fuxs2Value+" ],"+
				"symbol : 'none'"+ "}, {"+
				"coord : [ "+xMax+", "+fuxs2Value+" ],"+ 
				"symbol : 'arrow'"+ "} ]";
			markLineOpt[2].data[0]= eval("("+coordfu2+")");
			
			markLineOpt[3].tooltip.formatter='data：'+xs2Value;
			var coord2 = "[ {" + "coord : [ 0, "+xs2Value+" ],"+
				"symbol : 'none'"+ "}, {"+
				"coord : [ "+xMax+", "+xs2Value+" ],"+ 
				"symbol : 'arrow'"+ "} ]";
			markLineOpt[3].data[0]= eval("("+coord2+")");
			
			markLineOpt[4].tooltip.formatter='data：'+fuxs3Value;
			var coordfu3 = "[ {" + "coord : [ 0, "+fuxs3Value+" ],"+
				"symbol : 'none'"+ "}, {"+
				"coord : [ "+xMax+", "+fuxs3Value+" ],"+ 
				"symbol : 'arrow'"+ "} ]";
			markLineOpt[4].data[0]= eval("("+coordfu3+")");
			
			markLineOpt[5].tooltip.formatter='data：'+xs3Value;
			var coord3 = "[ {" + "coord : [ 0, "+xs3Value+" ],"+
				"symbol : 'none'"+ "}, {"+
				"coord : [ "+xMax+", "+xs3Value+" ],"+ 
				"symbol : 'arrow'"+ "} ]";
			markLineOpt[5].data[0]= eval("("+coord3+")");
			
			markLineOpt[6].tooltip.formatter='data：'+xValue;
			var coordx = "[ {" + "coord : [ 0, "+xValue+" ],"+
				"symbol : 'none'"+ "}, {"+
				"coord : [ "+xMax+", "+xValue+" ],"+ 
				"symbol : 'arrow'"+ "} ]";
			markLineOpt[6].data[0]= eval("("+coordx+")");
			
			optionY.series[0].markLine=markLineOpt[0];
			optionY.series[1].markLine=markLineOpt[1];
			optionY.series[2].markLine=markLineOpt[2];
			optionY.series[3].markLine=markLineOpt[3];
			optionY.series[4].markLine=markLineOpt[4];
			optionY.series[5].markLine=markLineOpt[5];
			optionY.series[6].markLine=markLineOpt[6];
			messReport.hideLoading();
			messReport.setOption(optionY);
		});
	});

	function setProductFun(id, name) {
		document.getElementById("productIDSelect").value = id;
		document.getElementById("productNameSelect").value = name;
		layer.closeAll('iframe');
	}
	
	
	//计算平均值
	function mean(a) {
		var sum = eval(a.join("+"));
		return sum / a.length;
	}

	//计算标准差
	function stdDev(a) {
		var m = mean(a); 
		var sum = 0;
		var l = a.length;
		for ( var i = 0; i < l; i++) {
			var dev = a[i] - m;
			sum += (dev * dev);
		}
		return Math.sqrt(sum / (l - 1));
	}
</script>
</head>

<body>
	
	<!--
        	作者：offline
        	时间：2017-12-05
        	描述：数据和报表
        -->
	
	<div id="messaryReport" style="width: 100%; height: 500px;"></div>


	<script>
		var messReport = echarts.init(document.getElementById("messaryReport"));
		var dataAll = [ [ [ 11.0, 118.04 ], [ 8.0, 116.95 ], [ 13.0, 117.58 ],
				[ 19.0, 108.81 ], [ 11.0, 108.33 ], [ 14.0, 129.96 ],
				[ 26.0, 137.24 ], [ 24.0, 124.26 ], [ 12.0, 110.84 ],
				[ 27.0, 124.82 ], [ 25.0, 115.68 ], [ 30.0, 155.68 ] ] ];
		var markLineOpt = [ {
			animation : false,
			label : {
				normal : {
					formatter : 'X-1S',
					textStyle : {
						align : 'right',
						verticalAlign : 'bottom',
						fontSize : 16
					}
				}
			},
			lineStyle : {
				normal : {
					type : 'dashed',
					color : '#3d72a9'
				}
			},
			tooltip : {
				formatter : 'data：3'
			},
			data : [ [ {
				coord : [ 0, 110 ],
				symbol : 'none'
			}, {
				coord : [ 30, 110 ],
				symbol : 'none'
			} ] ]
		}, {
			animation : false,
			label : {
				normal : {
					formatter : 'X+1S',
					textStyle : {
						align : 'right',
						verticalAlign : 'bottom',
						fontSize : 16
					}
				}
			},
			lineStyle : {
				normal : {
					type : 'dashed',
					color : '#3d72a9'
				}
			},
			tooltip : {
				formatter : 'data：3'
			},
			data : [ [ {
				coord : [ 0, 130 ],
				symbol : 'none'
			}, {
				coord : [ 30, 130 ],
				symbol : 'arrow'
			} ] ]
		}, {
			animation : false,
			label : {
				normal : {
					formatter : 'X-2S',
					textStyle : {
						align : 'right',
						verticalAlign : 'bottom',
						fontSize : 16
					}
				}
			},
			lineStyle : {
				normal : {
					type : 'dashed',
					color : 'yellow'
				}
			},
			tooltip : {
				formatter : 'data：3'
			},
			data : [ [ {
				coord : [ 0, 100 ],
				symbol : 'none'
			}, {
				coord : [ 30, 100 ],
				symbol : 'arrow'
			} ] ]
		}, {
			animation : false,
			label : {
				normal : {
					formatter : 'X+2S',
					textStyle : {
						align : 'right',
						verticalAlign : 'bottom',
						fontSize : 16
					}
				}
			},
			lineStyle : {
				normal : {
					type : 'dashed',
					color : 'yellow'
				}
			},
			tooltip : {
				formatter : 'data：3'
			},
			data : [ [ {
				coord : [ 0, 140 ],
				symbol : 'none'
			}, {
				coord : [ 30, 140 ],
				symbol : 'arrow'
			} ] ]
		}, {
			animation : false,
			label : {
				normal : {
					formatter : 'X-3S',
					textStyle : {
						align : 'right',
						verticalAlign : 'bottom',
						fontSize : 16
					}
				}
			},
			lineStyle : {
				normal : {
					type : 'dashed',
					color : 'red'
				}
			},
			tooltip : {
				formatter : 'data：3'
			},
			data : [ [ {
				coord : [ 0, 70 ],
				symbol : 'none'
			}, {
				coord : [ 30, 70 ],
				symbol : 'arrow'
			} ] ]
		},{
			animation : false,
			label : {
				normal : {
					formatter : 'X+3S',
					textStyle : {
						align : 'right',
						verticalAlign : 'bottom',
						fontSize : 16
					}
				}
			},
			lineStyle : {
				normal : {
					type : 'dashed',
					color : 'red'
				}
			},
			tooltip : {
				formatter : 'data：3'
			},
			data : [ [ {
				coord : [ 0, 160 ],
				symbol : 'none'
			}, {
				coord : [ 30, 160 ],
				symbol : 'arrow'
			} ] ]
		},{
			animation : false,
			label : {
				normal : {
					formatter : 'X',
					textStyle : {
						align : 'right',
						verticalAlign : 'bottom',
						fontSize : 16
					}
				}
			},
			lineStyle : {
				normal : {
					type : 'solid',
					color : 'black',
					width : 3
				}
			},
			tooltip : {
				formatter : 'data：3'
			},
			data : [ [ {
				coord : [ 0, 120 ],
				symbol : 'none'
			}, {
				coord : [ 30, 120 ],
				symbol : 'arrow'
			} ] ]
		} ];
		
		var option = {
			title : {
				text : '质控数据解读',
				subtext : '质控图',
				x : '7%'
			},
			tooltip : {
				formatter : 'Data: ({c})'
			},
			xAxis : [ {
				type : 'value',
				gridIndex : 0,
				min : 0,
				max : xMax,
				axisLabel : {
					formatter : '{value} Day'
				}
			} ],
			yAxis : [ {
				type : 'value',
				gridIndex : 0,
				min : yMin,
				max : yMax,
				axisLabel : {
					formatter : '{value} ng/ml'
				}
			} ],
			series : [ {
				name : '12',
				type : 'scatter',
				xAxisIndex : 0,
				yAxisIndex : 0,
				data : dataAll[0],
				markLine : markLineOpt[0]
			}, {
				name : '13',
				type : 'scatter',
				xAxisIndex : 0,
				yAxisIndex : 0,
				markLine : markLineOpt[1]
			}, {
				name : '14',
				type : 'scatter',
				xAxisIndex : 0,
				yAxisIndex : 0,
				markLine : markLineOpt[2]
			}, {
				name : '15',
				type : 'scatter',
				xAxisIndex : 0,
				yAxisIndex : 0,
				markLine : markLineOpt[3]
			}, {
				name : '16',
				type : 'scatter',
				xAxisIndex : 0,
				yAxisIndex : 0,
				markLine : markLineOpt[4]
			}, {
				name : '17',
				type : 'scatter',
				xAxisIndex : 0,
				yAxisIndex : 0,
				markLine : markLineOpt[5]
			}, {
				name : '18',
				type : 'scatter',
				xAxisIndex : 0,
				yAxisIndex : 0,
				markLine : markLineOpt[6]
			}  ]
		};
		messReport.setOption(option);
	</script>
</body>

</html>