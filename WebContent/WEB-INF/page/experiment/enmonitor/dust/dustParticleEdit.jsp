<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
/*用于检测类型的显示隐藏*/
.hiddle {
	display: none;
}
.dataTables_scrollHeadInner{
	width: 100% !important;
}
.dataTables_scrollHeadInner>table{
	width: 100% !important;
}
#btn_changeState{
   visibility:hidden;
}

</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div id="tableFileLoad"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 55px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">洁净区尘埃粒子测试记录</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<button type="button" class="btn btn-box-tool"
							style="font-size: 16px; border: 1px solid;" onclick="dayin()">
							<i class="fa fa-print"></i> 打印
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<%-- <li>
									<!-- <a href="####" onclick="printDataTablesItem()">打印</a> -->
									<a href="####" onclick="dayin()">打印</a>
								</li>
								--%>
									<li><a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
									</li>
									<li><a href="####" onclick="excelDataTablesItem()">导出Excel</a>
									</li>
									<li><a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
									</li>
									<li><a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
									</li>
								</ul>
							</div>
					</div>
				</div>
				<div class="box-body ">
					<!--form表单-->
					<form name="form1" id="form1" class="layui-form" method="post">
						<input type="hidden" id="state"
							value="<s:property value=" dif.state "/>" /> <input
							type="hidden" id="stateName"
							value="<s:property value=" dif.stateName "/>" />
						<%-- <input type="hidden"
							value="<%=request.getParameter(" bpmTaskId ")%>" /> --%>
						<input type="hidden" id="bpmTaskId"
							value="${requestScope.bpmTaskId}" /> <br>
						<div class="row">
							<!-- 文件名称 -->
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">编号</span> <input type="text"
										id="id" changelog="<s:property value="dif.id "/>"
										name="dif.id" class="form-control" readonly
										value="<s:property value=" dif.id "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">车间/部门</span> <input type="text"
										changelog="<s:property value="dif.workshopDepartment "/>"
										name="dif.workshopDepartment" class="form-control"
										title="车间/部门"
										value="<s:property value=" dif.workshopDepartment "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">区域<img class="requiredimage" src="/images/required.gif"></span> <input type="text"
										changelog="<s:property value=" dif.region "/>" id="region"
										name="dif.region" class="form-control" title="区域"
										readonly="readonly" value="<s:property value=" dif.region "/>" />
										<input type="hidden"
										 id="regionId"
										name="dif.regionId" class="form-control" title="区域"
										readonly="readonly" value="<s:property value=" dif.regionId"/>" />
									<span class="input-group-btn"><button
											class="btn btn-info" type="button" onclick="choseArea()">
											<i class="glyphicon glyphicon-search"></i>
										</button></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">测量设备及编号 </span> <input
										type="text"
										changelog="<s:property value=" dif.measuringInstrument"/>"
										name="dif.measuringInstrument" class="form-control" title="测量设备及编号"
										value="<s:property value=" dif.measuringInstrument"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">测试人</span> <input type="text"
										changelog="<s:property value=" dif.testUser "/>"
										name="dif.testUser" class="form-control" title="测试人"
										value="<s:property value=" dif.testUser "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">洁净区</span> 
									<select id ="dustP" class="form-control" lay-ignore="" name="dif.cleanZoneStatus" onchange="checkZdStatus()">
										<option value="0"
											<s:if test="dif.cleanZoneStatus==0">selected="selected"</s:if>>动态</option>
										<option value="1"
											<s:if test="dif.cleanZoneStatus==1">selected="selected"</s:if>>静态</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">最大允许数等级 </span> <select
										class="form-control" lay-ignore=""
										id="dif_maximumAllowableDust" name="dif.maximumAllowableDust" onchange="checkZdStatus()">
										<option value="0"
											<s:if test="dif.maximumAllowableDust==0">selected="selected"</s:if>>A</option>
										<option value="1"
											<s:if test="dif.maximumAllowableDust==1">selected="selected"</s:if>>B</option>
										<option value="2"
											<s:if test="dif.maximumAllowableDust==2">selected="selected"</s:if>>C</option>
										<option value="3"
											<s:if test="dif.maximumAllowableDust==3">selected="selected"</s:if>>D</option>
									</select>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">0.5um最大允许数<img class="requiredimage" src="/images/required.gif"></span> <input id="ldw"
										type="text" readonly
										changelog="<s:property value=" dif.dustZeropointFive"/>"
										name="dif.dustZeropointFive" class="form-control" title="0.5um最大允许数"
										value="<s:property value=" dif.dustZeropointFive"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">5.0um最大允许数<img class="requiredimage" src="/images/required.gif"></span> <input  id="w"
										type="text" readonly changelog="<s:property value=" dif.maxFivedust"/>"
										name="dif.maxFivedust" class="form-control" title="5.0um最大允许数"
										value="<s:property value=" dif.maxFivedust"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">监测类型</span> <select
										class="form-control" lay-ignore="" id="type" name="dif.type"
										onchange="checkType()">
										<option value="0"
											<s:if test="dif.type==0">selected="selected"</s:if>>房间</option>
										<option value="1"
											<s:if test="dif.type==1">selected="selected"</s:if>>设备</option>
									</select>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">备注</span> <input type="text"
										changelog="<s:property value=" dif.note "/>" name="dif.note"
										class="form-control" title="备注"
										value="<s:property value=" dif.note"/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">复核人<img class="requiredimage" src="/images/required.gif"></span><input
										changelog="<s:property value=" dif.confirmUser.id "/>"
										type="hidden" size="20" id="dif_confirmUser_id"
										name="dif.confirmUser.id"
										value="<s:property value=" dif.confirmUser.id "/>"
										class="form-control" title="复核人" /> <input
										changelog="<s:property value=" dif.confirmUser.name "/>"
										type="text" readonly="readonly" id="dif_confirmUser_name"
										value="<s:property value=" dif.confirmUser.name "/>"
										class="form-control" title="复核人" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">状态</span> <input type="text"
										readonly="readonly"
										changelog="<s:property value=" dif.stateName "/>"
										name="dif.stateName" class="form-control"
										value="<s:property value=" dif.stateName"/>" /> <input
										type="hidden" changelog="<s:property value=" dif.state "/>"
										name="dif.state" class="form-control"
										value="<s:property value=" dif.state"/>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">创建人</span><input
										changelog="<s:property value=" dif.createUser.id "/>"
										type="hidden" size="20" id="dif_createUser_id"
										name="dif.createUser.id"
										value="<s:property value=" dif.createUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" dif.createUser.name "/>"
										type="text" readonly="readonly" id="dif_createUser_name"
										value="<s:property value=" dif.createUser.name "/>"
										class="form-control" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">创建日期</span> <input type="text"
										id="gdate" readonly="readonly"
										changelog="<s:property value=" dif.createDate "/>"
										name="dif.createDate" class="form-control"
										value="<s:property value=" dif.createDate"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<%-- <div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">合格标准线<img 
									class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text" id="dif_qualifiedStandardLine"
										changelog="<s:property value=" dif.qualifiedStandardLine"/>"
										name="dif.qualifiedStandardLine" class="form-control"
										value="<s:property value=" dif.qualifiedStandardLine"/>" />
								</div>
							</div> --%>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">0.5um警戒线<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text" id="dif_cordon"
										changelog="<s:property value=" dif.cordon"/>"
										name="dif.cordon" class="form-control"
										value="<s:property value=" dif.cordon"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">0.5um纠偏线<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text"
										changelog="<s:property value=" dif.deviationCorrectionLine"/>"
										name="dif.deviationCorrectionLine" class="form-control"
										id="dif_deviationCorrectionLine"
										value="<s:property value=" dif.deviationCorrectionLine"/>" />
								</div>
							</div>
							<!-- </div>
						<div class="row"> -->
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">复核日期</span> <input type="text"
										readonly="readonly"
										changelog="<s:property value=" dif.confirmDate "/>"
										name="dif.confirmDate" class="form-control"
										value="<s:property value=" dif.confirmDate"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">5um警戒线<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text" id="dif_cordon2"
										changelog="<s:property value=" dif.cordon2"/>"
										name="dif.cordon2" class="form-control"
										value="<s:property value=" dif.cordon2"/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">5um纠偏线<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										type="text"
										changelog="<s:property value=" dif.deviationCorrectionLine2"/>"
										name="dif.deviationCorrectionLine2" class="form-control"
										id="dif_deviationCorrectionLine2"
										value="<s:property value=" dif.deviationCorrectionLine2"/>" />
								</div>
							</div>
							<div class="col-xs-4 ">
								<div class="input-group">
									<span class="input-group-addon">批准人<img
										class="requiredimage" src="/images/required.gif"></span><input
										changelog="<s:property value=" dif.approver.id "/>"
										type="hidden" size="20" id="dif_approver_id"
										name="dif.approver.id"
										value="<s:property value=" dif.approver.id "/>"
										class="form-control" title="批准人" /> <input
										changelog="<s:property value=" dif.approver.name "/>"
										type="text" readonly="readonly" id="dif_approver_name"
										value="<s:property value=" dif.approver.name "/>"
										class="form-control" title="批准人" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUser1()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">测试时间<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <input
										class="form-control"
										changelog="<s:property value="dif.testDate"/>" type="text"
										size="20" maxlength="25" id="dif_testDate" name="dif.testDate"
										title="测试日期" value="<s:property value=" dif.testDate "/>" />
									<%-- <input type="text" changelog="<s:property value=" dif.testDate "/>" name="dif.testDate" class="form-control" value="<s:property value=" dif.testDate "/>" /> --%>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">通知人</span><input
										changelog="<s:property value=" dif.notifierId "/>"
										type="hidden" size="20" id="dif_notifierId"
										name="dif.notifierId"
										value="<s:property value=" dif.notifierId "/>"
										class="form-control" /> <input
										changelog="<s:property value=" dif.notifierName "/>"
										type="text" readonly="readonly" id="dif_notifierName"
										name="dif.notifierName"
										value="<s:property value=" dif.notifierName "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="findUsers()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>
									&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileView()">
										<fmt:message key="biolims.report.checkFile" />
									</button>
								</div>
							</div>
						</div>
						<input type="hidden" id="changeLog" name="changeLog" />
						<input type="hidden" id="changeLogItem" name="changeLogItem" /> <input
							type="hidden" id="documentInfoItemJson"
							name="documentInfoItemJson" />
					</form>
					<div style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>
					<div class="row">
						<div class="" style="min-height: 250px; width: 100%; position: relative;">
							<div class="HideShowPanel" style="position: absolute;width: 100%;">
								<table
									class="table table-hover table-striped table-bordered table-condensed"
									id="documentTable" style="font-size: 14px;">
								</table>
							</div>
						</div>
					</div>
					<!-- <div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="documentTable" style="font-size: 14px;">
						</table>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/enmonitor/dust/dustParticleEdit.js"></script>
</body>
</html>