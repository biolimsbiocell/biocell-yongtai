<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
.tablebtns{
				float:right;
			}
.dt-buttons{
				margin:0;
				float:right;
			}
.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
</style>
</head>
<body>
	<div id="dicTypeTable" class="table-responsive ipadmini">
	<table id="selTbale"  class="table">
			<tr align="left">
				<td>
					<small>编号：<input type="text" id="id" size="20" searchname="id" />	
					姓名：<input type="text" id="name" size="20" searchname="name" />	
					<button class="btn btn-info" type="button"
							onClick="searchData()" >
							<i class="glyphicon glyphicon-search"></i> 
				</button></small></td>
			</tr>
		</table>
	
	  <div class="ipadmini">
		<table class="table table-hover table-bordered table-condensed"
			id="addDicTypeTable" style="font-size: 12px;"></table>
			</div>
	</div>
<script type="text/javascript" src="${ctx}/js/experiment/enmonitor/dust/selUsersTableList.js"></script>
</body>
</html>