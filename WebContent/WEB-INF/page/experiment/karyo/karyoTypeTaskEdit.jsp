<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=karyoTypeTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/karyo/karyoTypeTaskEdit.js"></script>
 <div style="float:left;width:30%" id="karyoTypeTaskTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
				<input type="hidden" id="user" value="${requestScope.user2}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoTypeTask_id"
                   	 name="karyoTypeTask.id" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="karyoTypeTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoTypeTask_name"
                   	 name="karyoTypeTask.name" title=""
                   	   
	value="<s:property value="karyoTypeTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showAcceptUser = Ext.get('showAcceptUser');
showAcceptUser.on('click', 
 function loadAcceptUser(){
var win = Ext.getCmp('loadAcceptUser');
if (win) {win.close();}
var loadAcceptUser= new Ext.Window({
id:'loadAcceptUser',modal:true,title:'Selection of experimental group',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=loadAcceptUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 loadAcceptUser.close(); }  }]  });     loadAcceptUser.show(); }
);
});
 function setloadAcceptUser(rec){
document.getElementById('karyoTypeTask_acceptUser').value=rec.get('id'); 
					document.getElementById('karyoTypeTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('loadAcceptUser')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyoTypeTask_createUser_name"  value="<s:property value="karyoTypeTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="karyoTypeTask_createUser" name="karyoTypeTask.createUser.id"  value="<s:property value="karyoTypeTask.createUser.id"/>" > 
 						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoTypeTask_createDate"
                   	 name="karyoTypeTask.createDate" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="karyoTypeTask.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
				<%-- <g:LayOutWinTag buttonId="showsampleType" title="选择样本类型"
				hasHtmlFrame="true"
				html="${ctx}/sample/dicSampleType/dicSampleTypeSelect.action"
				isHasSubmit="false" functionName="DicSampleTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyoTypeTask_sampleType').value=rec.get('id');
				document.getElementById('karyoTypeTask_sampleType_name').value=rec.get('name');" /> --%>
				
			
			<%-- 
               	 	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyoTypeTask_sampleType_name"  value="<s:property value="karyoTypeTask.sampleType.name"/>"/>
 						<input type="hidden" id="karyoTypeTask_sampleType" name="karyoTypeTask.sampleType.id"  value="<s:property value="karyoTypeTask.sampleType.id"/>" > 
 						<img alt='选择样本类型' id='showsampleType' src='${ctx}/images/img_lookup.gif' class='detail' onclick="loadTestDicSampleType()"/>                   		
                   	</td>
			 --%>
			 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
           	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	<td align="left"  >
					<input type="text" size="20" readonly="readOnly"  id="karyoTypeTask_template_name"  value="<s:property value="karyoTypeTask.template.name"/>" readonly="readOnly"  />
					<input type="hidden" id="karyoTypeTask_template" name="karyoTypeTask.template.id"  value="<s:property value="karyoTypeTask.template.id"/>" > 
					<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' class='detail'  onclick="TemplateFun()"/>                   		
               	</td>
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/experiment/karyo/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(rec){
document.getElementById('karyoTypeTask_createUser').value=rec.get('id');
				document.getElementById('karyoTypeTask_createUser_name').value=rec.get('name');
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>

			
					<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyoTypeTask_acceptUser_name"  value="<s:property value="karyoTypeTask.acceptUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="karyoTypeTask_acceptUser" name="karyoTypeTask.acceptUser.id"  value="<s:property value="karyoTypeTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="karyoTypeTask_state"
                   	 name="karyoTypeTask.state" title="状态"
                   	   
	value="<s:property value="karyoTypeTask.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
				<%-- <g:LayOutWinTag buttonId="showtemplate" title="选择实验模板"
				hasHtmlFrame="true"
				html="${ctx}/system/template/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyoTypeTask_template').value=rec.get('id');
				document.getElementById('karyoTypeTask_template_name').value=rec.get('name');" /> --%>
           	 	
			</tr>
			<tr>
					
			<td class="label-title" ><fmt:message key="biolims.common.confirmDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoTypeTask_confirmDate"
                   	 name="karyoTypeTask.confirmDate" title="完成时间"
                   	   class="text input readonlytrue" readonly="readOnly"
					value="<s:property value="karyoTypeTask.confirmDate"/>"
                   	  /> 
                   	</td>       
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoTypeTask_stateName"
                   	 name="karyoTypeTask.stateName" title="状态名称"
                   	   class="text input readonlytrue" readonly="readOnly"
	value="<s:property value="karyoTypeTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="karyoTypeTask_note"
                   	 name="karyoTypeTask.note" title="备注"
                   	   
	value="<s:property value="karyoTypeTask.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>      
					     	
					<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>
							${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="karyoTypeTaskItemJson" id="karyoTypeTaskItemJson" value="" />
            <input type="hidden" name="karyoTypeTaskTemplateJson" id="karyoTypeTaskTemplateJson" value="" />
            <input type="hidden" name="karyoTypeTaskAgentiaJson" id="karyoTypeTaskAgentiaJson" value="" />
            <input type="hidden" name="karyoTypeTaskCosJson" id="karyoTypeTaskCosJson" value="" />
            <input type="hidden" name="karyoTypeTaskHarvestJson" id="karyoTypeTaskHarvestJson" value="" />
<!--             <input type="hidden" name="karyoTypeTaskTempJson" id="karyoTypeTaskTempJson" value="" /> -->
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="karyoTypeTask.id"/>" />
            </form>
            <div id="karyoTypeTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#karyoTypeTaskTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#karyoTypeTaskAgentiapage" ><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#karyoTypeTaskCospage" ><fmt:message key="biolims.common.instrumentDetail"/></a></li>
           	</ul> 
			<div id="karyoTypeTaskTemplatepage" width="100%" height:10px></div>
			<div id="karyoTypeTaskAgentiapage" width="100%" height:10px></div>
			<div id="karyoTypeTaskCospage" width="100%" height:10px></div>
			</div>
			<div id="karyoTypeTaskHarvestpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
