<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
/*用于检测类型的显示隐藏*/
.hiddle {
	display: none;
}
#btn_changeState{
   visibility:hidden;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 55px">
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">生产质检结果管理</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#####" onclick="copyDataTablesItem ()">复制表格数据</a>
								</li>
								<li><a href="####" onclick="excelDataTablesItem()">导出Excel</a>
								</li>
								<li><a href="####" onclick="csvDataTablesItem ()">导出CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)">固定前两列</a>
								</li>
								<li><a href="####" id="unfixde" onclick="unfixde()">解除固定</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<form name="form1" id="form1" class="layui-form" method="post">
						<%-- <input type="hidden"
							value="<%=request.getParameter(" bpmTaskId ")%>" /> --%>
							<input type="hidden" id="bpmTaskId"
								value="${requestScope.bpmTaskId}" /> <br>
						<div class="row">
							<!-- 文件名称 -->
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">编号</span> <input type="text"
										id="id" changelog="<s:property value=" qualityTestResultManage.id "/>"
										name="qualityTestResultManage.id" class="form-control" readonly
										value="<s:property value=" qualityTestResultManage.id "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">订单编号</span> <input type="text" id="sampleOrder_id"
										changelog="<s:property value=" qualityTestResultManage.sampleOrder.id "/>"
										name="qualityTestResultManage.sampleOrder.id" class="form-control" readonly
										value="<s:property value=" qualityTestResultManage.sampleOrder.id "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">产品批号</span> <input type="text"
										changelog="<s:property value=" qualityTestResultManage.sampleOrder.barcode "/>" id="sampleOrder_barcode"
										class="form-control" readonly
										value="<s:property value=" qualityTestResultManage.sampleOrder.barcode "/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">批次状态</span> 
									<input class="form-control"
											changelog="<s:property value="qualityTestResultManage.sampleOrder.batchStateName"/>"
											type="text" size="20" maxlength="25"
											id="sampleOrder_batchStateName" 
											title="批次状态"
											value="<s:property value=" qualityTestResultManage.sampleOrder.batchStateName "/>" />
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">状态</span> <input type="text"
										id="state" changelog="<s:property value=" qualityTestResultManage.stateName "/>"
										name="qualityTestResultManage.stateName" class="form-control"
										value="<s:property value=" qualityTestResultManage.stateName"/>" /> <input
										type="hidden" id="stateName" readonly="readonly"
										changelog="<s:property value=" qualityTestResultManage.state "/>" name="qualityTestResultManage.state"
										class="form-control" value="<s:property value=" qualityTestResultManage.state"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">第一提交人</span><input 
											changelog="<s:property value=" qualityTestResultManage.createUser.id "/>" type="hidden"
										size="20" id="qualityTestResultManage_createUser_id" name="qualityTestResultManage.createUser.id"
										value="<s:property value=" qualityTestResultManage.createUser.id "/>"
										class="form-control" /> <input 
										changelog="<s:property value=" qualityTestResultManage.createUser.name "/>" type="text" 
										readonly="readonly" id="qualityTestResultManage_createUser_name"
										value="<s:property value=" qualityTestResultManage.createUser.name "/>"
										class="form-control" /> 
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">第一提交时间</span> <input type="text" readonly="readonly"
										id="qualityTestResultManage_createDate" changelog="<s:date name=" qualityTestResultManage.createDate " format="yyyy-MM-dd"/>" name="qualityTestResultManage.createDate"
										class="form-control" value="<s:date name=" qualityTestResultManage.createDate " format="yyyy-MM-dd"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">第一审核人</span><input
										changelog="<s:property value=" qualityTestResultManage.confirmUser.id "/>"
										type="hidden" size="20" id="qualityTestResultManage_confirmUser_id"
										name="qualityTestResultManage.confirmUser.id"
										value="<s:property value=" qualityTestResultManage.confirmUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" qualityTestResultManage.confirmUser.name "/>"
										type="text" readonly="readonly" id="qualityTestResultManage_confirmUser_name"
										value="<s:property value=" qualityTestResultManage.confirmUser.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">第一审核日期</span> <input type="text" readonly="readonly"
										id="qualityTestResultManage_confirmDate" changelog="<s:date name=" qualityTestResultManage.confirmDate " format="yyyy-MM-dd"/>" name="qualityTestResultManage.confirmDate"
										class="form-control" value="<s:date name=" qualityTestResultManage.confirmDate " format="yyyy-MM-dd"/>" />
								</div>
							</div>
							
							
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">第二提交人</span><input 
											changelog="<s:property value=" qualityTestResultManage.createUserTwo.id "/>" type="hidden"
										size="20" id="qualityTestResultManage_createUserTwo_id" name="qualityTestResultManage.createUserTwo.id"
										value="<s:property value=" qualityTestResultManage.createUserTwo.id "/>"
										class="form-control" /> <input 
										changelog="<s:property value=" qualityTestResultManage.createUserTwo.name "/>" type="text" 
										readonly="readonly" id="qualityTestResultManage_createUserTwo_name"
										value="<s:property value=" qualityTestResultManage.createUserTwo.name "/>"
										class="form-control" /> 
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">第二提交时间</span> <input type="text" readonly="readonly"
										id="qualityTestResultManage_createDateTwo" changelog="<s:date name=" qualityTestResultManage.createDateTwo " format="yyyy-MM-dd"/>" name="qualityTestResultManage.createDateTwo"
										class="form-control" value="<s:date name=" qualityTestResultManage.createDateTwo " format="yyyy-MM-dd"/>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">第二审核人</span><input
										changelog="<s:property value=" qualityTestResultManage.confirmUserTwo.id "/>"
										type="hidden" size="20" id="qualityTestResultManage_confirmUserTwo_id"
										name="qualityTestResultManage.confirmUserTwo.id"
										value="<s:property value=" qualityTestResultManage.confirmUserTwo.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" qualityTestResultManage.confirmUserTwo.name "/>"
										type="text" readonly="readonly" id="qualityTestResultManage_confirmUserTwo_name"
										value="<s:property value=" qualityTestResultManage.confirmUserTwo.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUserTwo()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">第二审核日期</span> <input type="text" readonly="readonly"
										id="qualityTestResultManage_confirmDateTwo" changelog="<s:date name=" qualityTestResultManage.confirmDateTwo " format="yyyy-MM-dd"/>" name="qualityTestResultManage.confirmDateTwo"
										class="form-control" value="<s:date name=" qualityTestResultManage.confirmDateTwo " format="yyyy-MM-dd"/>" />
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-4">
								<div class="input-group">
									<button id="cpbgdy" class="btn btn-info" type="button"
										onclick="showcpjyy(this);">成品检验报告单（一）</button>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="input-group">
									<button id="cpbgde" class="btn btn-info" type="button"
										onclick="showcpjye(this);">成品检验报告单（二）</button>
								</div>
							</div>
						</div>
						<%-- <div class="row">
				           
				
							<div class="col-xs-4">
								<div class="input-group">
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>
									&nbsp;&nbsp;
									<span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" />
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileView()">
										<fmt:message key="biolims.report.checkFile" />
									</button>
								</div>
							</div>
						
						
						</div> --%>
						<input type="hidden" id="changeLog" name="changeLog" /> 
						<input type="hidden" id="changeLogItem" name="changeLogItem" />
						<input
							type="hidden" id="documentInfoItemJson"
							name="documentInfoItemJson" />
							                          
					</form>
					<div style="border: 1px solid #eee; height: 1px; margin: 10px 0px;"></div>
					<div class="HideShowPanel">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="documentTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/quality/editQualityTestResultManage.js"></script>
</body>

</html>