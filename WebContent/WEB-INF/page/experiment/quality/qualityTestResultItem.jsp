﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>
		<style type="text/css">
			.dt-buttons {
			float: none;
		}

			.tablebtns {
			position: initial;
		}
		</style>
	</head>

	<body>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
		  <input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" /> 
		  <input type="hidden" id="itemid" value="${requestScope.itemid}" /> 
		  <input type="hidden" id="jiance" value="${requestScope.jiance}" /> 
		   <input type="hidden" id="sczj" value="${requestScope.sczj}" /> 
		   <input type="hidden" id="ly" value="${requestScope.ly}" /> 
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-body ipadmini">
							<input type="hidden"
							value="<s:property value="qualityTest.template.templateFieldsItemCode"/>"
							id="templateFieldsItemCode">
								<table class="table table-hover table-striped table-bordered table-condensed" id="qualityTestResultItemdiv" style="font-size: 14px;">
								</table>
							</div>
							<div class="box-footer">
								<!--<div class="pull-left">
									<button type="button" class="btn btn-primary" id="makeUpSave"><i class="glyphicon glyphicon-random"></i> 提交
                </button>
								</div>-->
								<div class="pull-right">
				<button type="button" class="btn btn-primary" id="save" onclick="saveInfoItem()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.save"/>
                </button>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<script type="text/javascript" src="${ctx}/js/experiment/quality/qualityTestResultItem.js"></script>
	</body>

</html>