﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
</style>
</head>
<body>
	<div  class="ipadmini">
		<table class="table table-hover table-bordered table-condensed"
			id="qualityTestResult" style="font-size: 12px;"></table>
	</div>
	<input id="testId" type="hidden" value="${requestScope.testId}">
<script type="text/javascript" src="${ctx}/js/experiment/quality/qualityTestResultAllForzj.js"></script>
</body>
</html>