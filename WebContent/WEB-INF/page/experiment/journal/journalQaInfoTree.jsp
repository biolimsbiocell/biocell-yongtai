<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bio-LIMS</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" type="text/css" href="${ctx}/lims/plugins/treegrid/css/jquery.treegrid.css" />
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.min.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.bootstrap3.js"></script>
		<script type="text/javascript" src="${ctx}/lims/plugins/treegrid/js/jquery.treegrid.extension.js"></script>
		<style>
			.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
			#mytreeGrid tr{
				cursor: pointer;
			}
		</style>
	</head>

	<body>
		<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid" style="min-height: 94%;">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">主数据
									<small>  
										列表
									</small>
								</h3>
							</div>
							<div class="box-body">
								<table id="mytreeGrid"></table>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<script type="text/javascript" src="${ctx}/js/com/biolims/experiment/journal/journalQaInfoTree.js"></script>
	</body>
</html>