﻿﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style type="text/css">
			.dt-buttons {
				float: none;
			}
			.tablebtns{
				position: initial;
			}
			.btn-info{
			display:none
			}
			.dataTable>thead>tr>th{
    			word-break: keep-all !important;
    		}
		</style>
		
	</head>
	<body >
	<div id="tableFileLoad"></div>
	<!--上传文件模态框-->
			<div class="modal fade" id="uploadFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="exampleModalLabel">上传文件
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h3>
						</div>
						<div class="modal-body">
							<div class="file-loading">
								<input id="uploadFileVal" name="excelFile" multiple type="file">
							</div>
							<div id="kartik-file-errors"></div>
						</div>
						<div class="modal-footer">
        					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
    					  </div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="uploadCsv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h3 class="modal-title" id="exampleModalLabel">上传文件
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button></h3>
						</div>
						<div class="modal-body">
							<div class="file-loading">
								<input id="uploadCsvVal" name="excelFile" multiple type="file">
							</div>
							<div id="kartik-file-errors"></div>
						</div>
						<div class="modal-footer">
        					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
    					  </div>
					</div>
				</div>
			</div>
		
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">QA日志管理里<small>QA日志管理里</small></h3>
								<div class="box-tools pull-right">
								</div>
							</div>
							<div class="box-body ipadmini" >
								<table class="table table-hover table-striped table-bordered table-condensed" id="main" style="font-size:14px;">
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div id="show_journalQaInfo_div"></div>
		<script type="text/javascript" src="${ctx}/js/experiment/journal/journalQaInfo.js"></script>
	</body>
</html>