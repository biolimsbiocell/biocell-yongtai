<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}

#yesSpan {
	display: none;
}

#auditDiv {
	display: none;
}

#btn_submit {
	display: none;
}

#btn_changeState {
	display: none;
}

.sampleNumberInput {
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 200px;
    top: 212px;
}

.sampleNumberInputTow{
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 510px;
    top: 212px;
}

.sampleNumberDiv {
    position: absolute;
    left: 140px;
    top: 223px;
}
.sampleNumberDivtow {
    position: absolute;
	left: 466px;
    top: 223px;
}

.sampleNumberbutton {
    position: absolute;
    left: 760px;
    top: 212px;
    border-radius: 4px;
    border: 1px solid #ccc;
    background-color: #5cb85c;
    width: 38px;
    height: 36px;
}

</style>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<!--AI录入-->
	<form method="post" class="hide" id="fileForm" name="fileForm"
		action='${ctx}/common/aiUtils/isIdAreThere.action'
		enctype="multipart/form-data">
		<input type="file" name="AiPicture" id="AiPicture" accept="image/*"
			onchange="subimtBtn()">
	</form>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<input type="hidden" id="flag" value="${requestScope.flag}">
	<input type="hidden" id="changeId" value="${requestScope.changeId}">
	<input type="hidden" id="order_type" value="${requestScope.type}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						成品放行审核单
					</h3>
					<small style="color: #fff;"> <%-- <fmt:message
							key="biolims.common.commandPerson" />: <span
						id="releaseAudit_createUser"><s:property
								value=" releaseAudit.createUser.name" /></span> <fmt:message
							key="biolims.common.createDate" />: <span
						id="releaseAudit_createDate"><s:date
								name="createDate" format="yyyy-MM-dd" /></span> <fmt:message
							key="biolims.common.state" />: <span id="releaseAudit_state"><s:property
								value="releaseAudit.stateName" /></span> <fmt:message
							key="biolims.common.auditor" />：<span id="confirmUserName"><s:property
								value="releaseAudit.confirmUser.name " /></span> <fmt:message
							key="biolims.common.completionTime" />：<span
						id="releaseAudit_confirmDate"><s:date
								name=" releaseAudit.confirmDate " format="yyyy-MM-dd " /></span> --%>
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="${requestScope.bpmTaskId}" /> <br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 放行审核单编码
										</span> <input
											changelog="<s:property value="releaseAudit.id"/>" type="text"
											size="20" maxlength="25" id="releaseAudit_id"
											name="id" class="form-control"
											title="放行审核单编码"
											value="<s:property value="releaseAudit.id"/>" /> <span
											class="input-group-btn">
										</span> 
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 患者姓名 <img class='requiredimage'
											src='${ctx}/images/required.gif' />
										</span> <input 
											changelog="<s:property value="releaseAudit.patientName"/>"
											type="text" size="20" maxlength="50" id="releaseAudit_patientName"
											name="patientName" class="form-control"
											title="患者姓名"
											value="<s:property value=" releaseAudit.patientName "/>" />

									</div>
								</div>
								
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> CCOI <img class='requiredimage'
											src='${ctx}/images/required.gif' />
										</span> <input 
											changelog="<s:property value="releaseAudit.ccoi"/>"
											type="text" size="20" maxlength="50" id="releaseAudit_ccoi"
											name="ccoi" class="form-control"
											title="CCOI"
											value="<s:property value=" releaseAudit.ccoi "/>" />

									</div>
								</div>
								<%-- <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 样本编号 <img class='requiredimage'
											src='${ctx}/images/required.gif' />
										</span> <input 
											changelog="<s:property value="releaseAudit.code"/>"
											type="text" size="20" maxlength="50" id="releaseAudit_code"
											name="code" class="form-control"
											title="样本编号"
											value="<s:property value=" releaseAudit.code "/>" />

									</div>
								</div> --%>

								
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">产品批次</span> <input 
											changelog="<s:property value="releaseAudit.productBatchNumber"/>"
											type="text" size="20" maxlength="50" id="releaseAudit_productBatchNumber"
											name="productBatchNumber" class="form-control"
											title="产品批次"
											value="<s:property value=" releaseAudit.productBatchNumber "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">规格</span> <input type="text"
											changelog="<s:property value="releaseAudit.specifications"/>"
											size="20" maxlength="50" class="form-control"
											id="specifications" name="specifications"
											title="规格"
											value="<s:property value=" releaseAudit.specifications "/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon">细胞总数量</span> <input type="text"
											changelog="<s:property value="releaseAudit.num"/>"
											size="20" maxlength="50" class="form-control"
											id="releaseAudit_num" name="num"
											title="细胞总数量"
											value="<s:property value=" releaseAudit.num "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">医院</span> <input
											type="text"
											changelog="<s:property value="releaseAudit.hospital"/>"
											size="20" id="releaseAudit_hospital"
											name="hospital"
											value="<s:property value=" releaseAudit.hospital "/>"
											class="form-control" /> <span class="input-group-btn">
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">科室</span> <input type="text"
											changelog="<s:property value="releaseAudit.department"/>" size="20"
											maxlength="50" class="form-control" id="releaseAudit_department"
											name="department"
											title="科室"
											value="<s:property value=" releaseAudit.department "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">病历号/门诊号</span> <input type="text"
											changelog="<s:property value="releaseAudit.patientId"/>" size="20"
											maxlength="50" class="form-control" id="releaseAudit_patientId"
											name="patientId"
											title="病历号/门诊号"
											value="<s:property value=" releaseAudit.patientId "/>" />
									</div>
								</div>
							</div>

							<!-- 生产情况 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											生产情况
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">生产指令是否符合工艺规程需求</span> 
										
										
										<s:if test="releaseAudit.result1==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result1" 
												name="result1" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result1" 
												name="result1" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result1==0">
											<input
												type="radio"
												id="releaseAudit_result1" 
												name="result1" title="是"
												value="1" />
											<input
												type="radio"  checked="checked"
												id="releaseAudit_result1" 
												name="result1" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result1==null||releaseAudit.result1==''">
											<input
												type="radio"
												id="releaseAudit_result1" 
												name="result1" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result1" 
												name="result1" title="否"
												value="0" />
										</s:if>
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">是否已按照指令和工艺规程完成所有工序的生产</span> 
										
										
										<s:if test="releaseAudit.result2==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result2" 
												name="result2" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result2" 
												name="result2" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result2==0">
											<input
												type="radio" 
												id="releaseAudit_result2" 
												name="result2" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result2" 
												name="result2" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result2==null||releaseAudit.result2==''">
											<input
												type="radio" 
												id="releaseAudit_result2" 
												name="result2" title="是"
												value="1" />
											<input
												type="radio" 
												id="releaseAudit_result2" 
												name="result2" title="否"
												value="0" />
										</s:if>
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">所用物料是否已完成所有检查，检验，并经保证部批准放行</span> 
										
										<s:if test="releaseAudit.result3==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result3" 
												name="result3" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result3" 
												name="result3" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result3==0">
											<input
												type="radio" 
												id="releaseAudit_result3" 
												name="result3" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result3" 
												name="result3" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result3==null||releaseAudit.result3==''">
											<input
												type="radio" 
												id="releaseAudit_result3" 
												name="result3" title="是"
												value="1" />
											<input
												type="radio" 
												id="releaseAudit_result3" 
												name="result3" title="否"
												value="0" />
										</s:if>
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">物料平衡是否在规定范围内</span> 
										
										<s:if test="releaseAudit.result4==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result4" 
												name="result4" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result4" 
												name="result4" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result4==0">
											<input
												type="radio" 
												id="releaseAudit_result4" 
												name="result4" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result4" 
												name="result4" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result4==null||releaseAudit.result4==''">
											<input
												type="radio" 
												id="releaseAudit_result4" 
												name="result4" title="是"
												value="1" />
											<input
												type="radio" 
												id="releaseAudit_result4" 
												name="result4" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">批生产/包装记录是否内容齐全、书写正确、数据完整，已由生产和质量制指定人员完成审核</span> 
										
										<s:if test="releaseAudit.result5==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result5" 
												name="result5" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result5" 
												name="result5" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result5==0">
											<input
												type="radio" 
												id="releaseAudit_result5" 
												name="result5" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result5" 
												name="result5" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result5==null||releaseAudit.result5==''">
											<input
												type="radio" 
												id="releaseAudit_result5" 
												name="result5" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result5" 
												name="result5" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">所用标签是否正确，批号打印及有效期是否正确</span> 
										
										<s:if test="releaseAudit.result6==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result6" 
												name="result6" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result6" 
												name="result6" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result6==0">
											<input
												type="radio" 
												id="releaseAudit_result6" 
												name="result6" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result6" 
												name="result6" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result6==null||releaseAudit.result6==''">
											<input
												type="radio" 
												id="releaseAudit_result6" 
												name="result6" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result6" 
												name="result6" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">关键工艺参数设置及控制是否符合工艺要求</span> 
										
										<s:if test="releaseAudit.result7==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result7" 
												name="result7" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result7" 
												name="result7" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result7==0">
											<input
												type="radio" 
												id="releaseAudit_result7" 
												name="result7" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result7" 
												name="result7" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result7==null||releaseAudit.result7==''">
											<input
												type="radio" 
												id="releaseAudit_result7" 
												name="result7" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result7" 
												name="result7" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">所有与该批产品有关的偏差是否已完成调查和适当处理</span> 
										
										<s:if test="releaseAudit.result8==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result8" 
												name="result8" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result8" 
												name="result8" title="否"
												value="0" />
											<input
												type="radio"
												id="releaseAudit_result8" 
												name="result8" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result8==0">
											<input
												type="radio" 
												id="releaseAudit_result8" 
												name="result8" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result8" 
												name="result8" title="否"
												value="0" />
											<input
												type="radio"
												id="releaseAudit_result8" 
												name="result8" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result8==2">
											<input
												type="radio" 
												id="releaseAudit_result8" 
												name="result8" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result8" 
												name="result8" title="否"
												value="0" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result8" 
												name="result8" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result8==null||releaseAudit.result8==''">
											<input
												type="radio" 
												id="releaseAudit_result8" 
												name="result8" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result8" 
												name="result8" title="否"
												value="0" />
											<input
												type="radio" 
												id="releaseAudit_result8" 
												name="result8" title="N/A"
												value="2" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">所有与该产品有关的变更是否已按照相关规定处理完毕</span> 
										
										
										<s:if test="releaseAudit.result9==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result9" 
												name="result9" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result9" 
												name="result9" title="否"
												value="0" />
											<input
												type="radio"
												id="releaseAudit_result9" 
												name="result9" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result9==0">
											<input
												type="radio" 
												id="releaseAudit_result9" 
												name="result9" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result9" 
												name="result9" title="否"
												value="0" />
											<input
												type="radio" 
												id="releaseAudit_result9" 
												name="result9" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result9==2">
											<input
												type="radio" 
												id="releaseAudit_result9" 
												name="result9" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result9" 
												name="result9" title="否"
												value="0" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result9" 
												name="result9" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result9==null||releaseAudit.result9==''">
											<input
												type="radio" 
												id="releaseAudit_result9" 
												name="result9" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result9" 
												name="result9" title="否"
												value="0" />
											<input
												type="radio" 
												id="releaseAudit_result9" 
												name="result9" title="N/A"
												value="2" />
										</s:if>
										
									</div>
								</div>
							</div>
							
							<!-- 监控情况 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											监控情况
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">生产条件是否符合工艺要求，生产人员卫生是否符合规定</span> 
										
										<s:if test="releaseAudit.result10==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result10" 
												name="result10" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result10" 
												name="result10" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result10==0">
											<input
												type="radio" 
												id="releaseAudit_result10" 
												name="result10" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result10" 
												name="result10" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result10==null||releaseAudit.result10==''">
											<input
												type="radio" 
												id="releaseAudit_result10" 
												name="result10" title="是"
												value="1" />
											<input
												type="radio" 
												id="releaseAudit_result10" 
												name="result10" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">中间控制项目是否符合工艺要求，检查结果是否符合标准要求</span> 
										
										<s:if test="releaseAudit.result11==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result11" 
												name="result11" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result11" 
												name="result11" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result11==0">
											<input
												type="radio"
												id="releaseAudit_result11" 
												name="result11" title="是"
												value="1" />
											<input
												type="radio"  checked="checked"
												id="releaseAudit_result11" 
												name="result11" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result11==null||releaseAudit.result11==''">
											<input
												type="radio"
												id="releaseAudit_result11" 
												name="result11" title="是"
												value="1" />
											<input
												type="radio" 
												id="releaseAudit_result11" 
												name="result11" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">环境监控是否符合规定，日常及定期监测结果是否符合标准要求</span> 
										
										<s:if test="releaseAudit.result12==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result12" 
												name="result12" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result12" 
												name="result12" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result12==0">
											<input
												type="radio" 
												id="releaseAudit_result12" 
												name="result12" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result12" 
												name="result12" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result12==null||releaseAudit.result12==''">
											<input
												type="radio" 
												id="releaseAudit_result12" 
												name="result12" title="是"
												value="1" />
											<input
												type="radio" 
												id="releaseAudit_result12" 
												name="result12" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							
							<!-- 检验情况 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											检验情况
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">中间产品、成品是否已按照质量标准完成检验，检验结果是否符合放行质量标准要求</span> 
										
										<s:if test="releaseAudit.result13==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result13" 
												name="result13" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result13" 
												name="result13" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result13==0">
											<input
												type="radio" 
												id="releaseAudit_result13" 
												name="result13" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result13" 
												name="result13" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result13==null||releaseAudit.result13==''">
											<input
												type="radio" 
												id="releaseAudit_result13" 
												name="result13" title="是"
												value="1" />
											<input
												type="radio" 
												id="releaseAudit_result13" 
												name="result13" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">批检验记录是否内容齐全、书写正确、数据完整，已由QC和QA指定人员完成审核并签名</span> 
										
										<s:if test="releaseAudit.result14==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result14" 
												name="result14" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result14" 
												name="result14" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result14==0">
											<input
												type="radio" 
												id="releaseAudit_result14" 
												name="result14" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result14" 
												name="result14" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result14==null||releaseAudit.result14==''">
											<input
												type="radio" 
												id="releaseAudit_result14" 
												name="result14" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result14" 
												name="result14" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">是否依据检验记录出具成品检验报告单，并由检验负责人签名</span> 
										
										<s:if test="releaseAudit.result15==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result15" 
												name="result15" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result15" 
												name="result15" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result15==0">
											<input
												type="radio" 
												id="releaseAudit_result15" 
												name="result15" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result15" 
												name="result15" title="否"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result15==null||releaseAudit.result15==''">
											<input
												type="radio" 
												id="releaseAudit_result15" 
												name="result15" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result15" 
												name="result15" title="否"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">是否有检验偏差，如有应执行00S/00T调查程序，并完成相应处理、评估及批准</span> 
										
										<s:if test="releaseAudit.result16==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result16" 
												name="result16" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result16" 
												name="result16" title="否"
												value="0" />
											<input
												type="radio"
												id="releaseAudit_result16" 
												name="result16" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result16==0">
											<input
												type="radio" 
												id="releaseAudit_result16" 
												name="result16" title="是"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result16" 
												name="result16" title="否"
												value="0" />
											<input
												type="radio"
												id="releaseAudit_result16" 
												name="result16" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result16==2">
											<input
												type="radio" 
												id="releaseAudit_result16" 
												name="result16" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result16" 
												name="result16" title="否"
												value="0" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result16" 
												name="result16" title="N/A"
												value="2" />
										</s:if>
										
										<s:if test="releaseAudit.result16==null||releaseAudit.result16==''">
											<input
												type="radio" 
												id="releaseAudit_result16" 
												name="result16" title="是"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result16" 
												name="result16" title="否"
												value="0" />
											<input
												type="radio"
												id="releaseAudit_result16" 
												name="result16" title="N/A"
												value="2" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							
							<!-- 审核结论 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											审核结论
										</h3>
									</div>
								</div>
							</div>
							<br>
							
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">经审核，本批次产品已完成物料和产品质量的全部评价及生产过程监控评价，结果</span> 
										
										<s:if test="releaseAudit.result==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_result" 
												name="result" title="符合规定"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result" 
												name="result" title="不符合规定"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result==0">
											<input
												type="radio" 
												id="releaseAudit_result" 
												name="result" title="符合规定"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_result" 
												name="result" title="不符合规定"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.result==null||releaseAudit.result==''">
											<input
												type="radio" 
												id="releaseAudit_result" 
												name="result" title="符合规定"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_result" 
												name="result" title="不符合规定"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="input-group">
										<span class="input-group-addon"> 质量保证部负责人
										</span> <input
											changelog="<s:property value="releaseAudit.chargePerson"/>" type="text"
											size="20" maxlength="25" id="releaseAudit_chargePerson"
											name="chargePerson" class="form-control"
											title="质量保证部负责人"
											value="<s:property value="releaseAudit.chargePerson"/>" /> <span
											class="input-group-btn">
										</span> 
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showconfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="input-group">
										<span class="input-group-addon">日期</span> <input type="text"
											changelog="<s:property value="releaseAudit.qualityAssuranceTime"/>"
											size="30" maxlength="25" id="releaseAudit_qualityAssuranceTime" readonly="readonly"
											name="qualityAssuranceTime" class="form-control"
											title="日期"
											value="<s:date name=" releaseAudit.qualityAssuranceTime " format="yyyy-MM-dd "/>" />
									</div>
								</div>
							</div>
							<!-- 审批结论 -->
							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											审批结论
										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">本批产品是否放行</span> 
										
										<s:if test="releaseAudit.releaseConclusion==1">
											<input
												type="radio" checked="checked"
												id="releaseAudit_releaseConclusion" 
												name="releaseConclusion" title="同意"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_releaseConclusion" 
												name="releaseConclusion" title="不同意"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.releaseConclusion==0">
											<input
												type="radio" 
												id="releaseAudit_releaseConclusion" 
												name="releaseConclusion" title="同意"
												value="1" />
											<input
												type="radio" checked="checked"
												id="releaseAudit_releaseConclusion" 
												name="releaseConclusion" title="不同意"
												value="0" />
										</s:if>
										
										<s:if test="releaseAudit.releaseConclusion==null||releaseAudit.releaseConclusion==''">
											<input
												type="radio" 
												id="releaseAudit_releaseConclusion" 
												name="releaseConclusion" title="同意"
												value="1" />
											<input
												type="radio"
												id="releaseAudit_releaseConclusion" 
												name="releaseConclusion" title="不同意"
												value="0" />
										</s:if>
										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="input-group">
										<span class="input-group-addon"> 质量授权人
										</span> <input
											changelog="<s:property value="releaseAudit.qualityAuthorizer"/>" type="text"
											size="20" maxlength="25" id="releaseAudit_qualityAuthorizer"
											name="qualityAuthorizer" class="form-control"
											title="质量授权人"
											value="<s:property value="releaseAudit.qualityAuthorizer"/>" /> <span
											class="input-group-btn">
										</span> 
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="showconfirmUser1()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="input-group">
										<span class="input-group-addon">日期</span> <input type="text"
											changelog="<s:property value="releaseAudit.qualityAuthorizationDate"/>"
											size="30" maxlength="25" id="releaseAudit_qualityAuthorizationDate" readonly="readonly"
											name="qualityAuthorizationDate" class="form-control"
											title="日期"
											value="<s:date name=" releaseAudit.qualityAuthorizationDate " format="yyyy-MM-dd "/>" />
									</div>
								</div>
							</div>


							<%-- 							<div class="row">
								<div class="panel"
									style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
									<div class="panel-heading text-left">
										<h3 class="panel-title" style="font-family: 黑体;">
											<i class="glyphicon glyphicon-bookmark"></i>
											<fmt:message key="biolims.common.customFields" />

										</h3>
									</div>
								</div>
							</div>
							<br>
							<div class="row" id="fieldItemDiv"></div> --%>
							
							<input
								changelog="<s:property value="releaseAudit.state"/>" type="hidden"
								size="20" maxlength="25" id="releaseAudit_state"
								name=state class="form-control"
								title="状态"
								value="<s:property value="releaseAudit.state"/>" />
							<input
								changelog="<s:property value="releaseAudit.stateName"/>" type="hidden"
								size="20" maxlength="25" id="releaseAudit_stateName"
								name="stateName" class="form-control"
								title="质量授权人"
								value="<s:property value="releaseAudit.stateName"/>" />
							<button class="layui-btn hidden" lay-submit lay-filter="formDemo" id='radiosub' >立即提交</button>
						</form>
					</div>
					<!--table表格-->
					<div class="HideShowPanel ipadmini" style="display: none;">
						<p class="sampleNumberDiv"><fmt:message key="biolims.common.numberVessels" /></p>
						<input class="sampleNumberInput" id="sampleNumberInput"/>
						<p class="sampleNumberDivtow">条形码<%-- <fmt:message key="biolims.common.numberVessels" /> --%></p>
						<input class="sampleNumberInputTow" id="sampleBarCode"/><button class="sampleNumberbutton" onclick="saveNumreleaseAudit()"><fmt:message key="biolims.common.numVesselsconfirm" /></button>
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="sampleInfoTable" style="font-size: 14px;">
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="lan" value="${sessionScope.lan}">
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/releaseAudit/releaseAuditEdit.js"></script>
</body>
</html>