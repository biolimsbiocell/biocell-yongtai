﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=qaTask&id=${qaTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qa/qaTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<div style="float:left;width:25%" id="qaTaskTempPage">	
</div>
  	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>						
               	 	<td class="label-title" >任务编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="hidden" size="40" id="type"  value="qaTask" />
                   	<input type="text" size="20" maxlength="18" id="qaTask_id"
                   	 name="qaTask.id" title="任务编号"
                   	readonly = "readOnly" class="text input readonlytrue"   
	value="<s:property value="qaTask.id"/>"
                   	  />                   	  
                   	</td>						
               	 	<%-- <td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="30" maxlength="60" id="qaTask_maxNum" name="qaTask.maxNum" title="容器数量"
							value="<s:property value="qaTask.maxNum"/>"
                   	  />
                   	<input type="text" size="20" maxlength="60" id="qaTask_name"
                   	 name="qaTask.name" title="描述"
                   	   
	value="<s:property value="qaTask.name"/>"
                   	  />                   	  
                   	</td>	 --%>								
			<g:LayOutWinTag buttonId="showcreateUser" title="创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="CreateUserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('qaTask_createUser').value=rec.get('id');
				document.getElementById('qaTask_createUser_name').value=rec.get('name');" />										
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="qaTask_createUser_name"  value="<s:property value="qaTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="qaTask_createUser" name="qaTask.createUser.id"  value="<s:property value="qaTask.createUser.id"/>" > 
                   	</td>
             
             <td class="label-title" >受理人员</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ></td>            	 	
                   	<td align="left" >
                   	<input type="hidden" size="40" id="type"  value="qaTask" />
                   	<input type="text" size="20" maxlength="18" id="qaTask_awardedUser"  name="qaTask.awardedUser" title="受理人员" value="<s:property value="qaTask.awardedUser"/>"
                   	  />                   	  
                   	</td>
             
			</tr>
			<tr>						
               	 	<td class="label-title" >任务发布日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                   	  
                   	  	<input type="text" size="15" maxlength="" id="qaTask_createDate"
                   	 name="qaTask.createDate" title="任务发布日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:property value="qaTask.createDate"/>"                  	     
                   	  />
                   	</td>                   	
                   		<td class="label-title" >任务完成日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                  	  
                   	  	<input type="text" size="20" maxlength="" id="qaTask_confirmDate"
                   	 name="qaTask.confirmDate" title="任务完成日期"
                   	   readonly = "readOnly" class="Wdate"  
                   	  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm:ss'})"  
                   	    value="<s:date name="qaTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"   />
                   	 
                   	
                   	</td>	
                   	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" >
 						<input type="text" size="20" readonly="readOnly"  id="qaTask_confirmUser_name"  value="<s:property value="qaTask.confirmUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="qaTask_confirmUser" name="qaTask.confirmUser.id"  value="<s:property value="qaTask.confirmUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(3);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>		
				 <td class="label-title" >选择实验模板</td>
			        <td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="40" readonly="readOnly"  id="qaTask_template_name"  value="<s:property value="qaTask.template.name"/>" />
 						<input type="text"  readonly="readOnly" id="qaTask_template" name="qaTask.template.id"  value="<s:property value="qaTask.template.id"/>" > 
 						<input type="hidden" readonly="readOnly" id="qaTask_template_templateFieldsCode" name="qaTask.template.templateFieldsCode"  value="<s:property value="qaTask.template.templateFieldsCode"/>" >
 						<input type="hidden" readonly="readOnly" id="qaTask_template_templateFieldsItemCode" name="qaTask.template.templateFieldsItemCode"  value="<s:property value="qaTask.template.templateFieldsItemCode"/>" >
 						<img alt='选择模板' id='showTemplateFun' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()" />                   		
                   	</td>			
               	 	<%--<td class="label-title" style="display:none">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="40" maxlength="30" id="qaTask_state"
                   	 name="qaTask.state" title="工作流状态"
                   	   style="display:none"
	value="<s:property value="qaTask.state"/>"
                   	  />                  	  
                   	</td>	 --%>		
			</tr>
			<%-- <tr>
			<g:LayOutWinTag buttonId="showAcceptUser" title="选择实验组"
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('qaTask_acceptUser').value=rec.get('id'); 
					document.getElementById('qaTask_acceptUser_name').value=rec.get('name');" /> 			
					<td class="label-title" >实验组</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="qaTask_acceptUser_name"  value="<s:property value="qaTask.acceptUser.name"/>" class="text input" readonly="readOnly"  />
 						<input type="hidden" id="qaTask_acceptUser" name="qaTask.acceptUser.id"  value="<s:property value="qaTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>               		
			<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="qaTask_stateName"
                   	 name="qaTask.stateName" title="工作流状态"
                   	   class="text input readonlytrue" readonly="readOnly"  
	value="<s:property value="qaTask.stateName"/>"
                   	  />
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>	 --%>		
            </table>
            <input type="hidden" name="qaTaskItemJson" id="qaTaskItemJson" value="" />
            <input type="hidden" name="qaTaskResultJson" id="qaTaskResultJson" value="" />
            <input type="hidden" name="qaTaskTemplateItemJson" id="qaTaskTemplateItemJson" value="" />
            <input type="hidden" name="qaTaskTemplateReagentJson" id="qaTaskTemplateReagentJson" value="" />
            <input type="hidden" name="qaTaskTemplateCosJson" id="qaTaskTemplateCosJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="qaTask.id"/>" />
            </form>
			<div id="qaTaskItempage" width="100%" height:10px></div>
			<div id = '<%="3d_image0" %>'></div>
			<div id = '<%="3d_image1" %>'></div>
			<div id="tabs">
            <ul>
			<li><a href="#qaTaskTemplateItempage">模版明细</a></li>
			<li><a href="#qaTaskTemplateReagentpage" onClick="showReagent()">原辅料明细</a></li>
			<li><a href="#qaTaskTemplateCospage" onClick="showCos()">设备明细</a></li>
           	</ul>
           	<div id="qaTaskTemplateItempage" width="100%" height:10px></div>
           	<div id="qaTaskTemplateReagentpage" width="100%" height:10px></div>
           	<div id="qaTaskTemplateCospage" width="100%" height:10px></div> 
           	</div>
			<div id="qaTaskResultpage" width="100%" height:10px></div>
			</div>
	</body>
	</html>
