﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/qa/qaTaskItem.js"></script>
</head>
<body>
	<div id="qaTaskItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="bat_item_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>样本用量（μg）</span></td>
				<td><input id="Consume"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_productNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>中间产物数量</span></td>
				<td><input id="productNum"/></td>
			</tr>
		</table>
		</div>
	<div id="bat_qaResult_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>处理具体描述</span></td>
				<td><input id="handleJtms"/></td>
			</tr>
		</table>
		</div>
	<div id="bat_qaResult1_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>事件描述</span></td>
				<td><input id="eventMs"/></td>
			</tr>
		</table>
		</div>
</body>
</html>


