﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/experiment/sequencingLife/sequencingLife.js"></script>
<script type="text/javascript"
	src="${ctx}/js/experiment/sequencingLife/sequencingLifeResult.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString">
		<form id="searchForm">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td align="left"><input type="text" size="30" maxlength="30"
						id="sequencing_id" name="id" searchField="true"
						title="<fmt:message key="biolims.common.serialNumber"/>" /></td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.describe" /></td>
					<td align="left"><input type="text" size="30" maxlength="30"
						id="sequencing_name" name="name" searchField="true"
						title="<fmt:message key="biolims.common.describe"/>" /></td>
				</tr>
				<!--  
				<tr>
					<g:LayOutWinTag buttonId="showreciveUser" title=''
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="ReciverUserFun" hasSetFun="true"
						documentId="sequencing_reciveUser"
						documentName="sequencing_reciveUser_name" />
					<td class="label-title"><fmt:message
							key="biolims.common.experimenter" /></td>
					<td align="left"><input type="text" size="30"
						id="sequencing_reciveUser_name" searchField="true"
						name="reciveUser.name" value="" class="text input" /> <input
						type="hidden" id="sequencing_reciveUser"
						name="sequencing.reciveUser.id" value=""> <img
						alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>'
						id='showreciveUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				-->
				<!--  
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.testTime" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startreciveDate" name="startreciveDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="reciveDate1" name="reciveDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endreciveDate" name="endreciveDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="reciveDate2" name="reciveDate##@@##2"
						searchField="true" /></td>
				</tr>
				-->
				<tr>
					<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(id,name){
 document.getElementById("sequencing_createUser").value = id;
document.getElementById("sequencing_createUser_name").value = name;
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>

					<td class="label-title"><fmt:message
							key="biolims.common.commandPerson" /></td>
					<td align="left"><input type="text" size="30"
						id="sequencing_createUser_name" searchField="true"
						name="createUser.name" value="" class="text input" /> <input
						type="hidden" id="sequencing_createUser"
						name="sequencing.createUser.id" value=""> <img
						alt='<fmt:message key="biolims.common.chooseFromPeople"/>'
						id='showcreateUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.commandTime" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startcreateDate" name="startcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate1" name="createDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endcreateDate" name="endcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate2" name="createDate##@@##2"
						searchField="true" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="show_sequencing_div"></div>
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value='' />
	</form>
	<div id="show_sequencing_tree_page"></div>
</body>
</html>



