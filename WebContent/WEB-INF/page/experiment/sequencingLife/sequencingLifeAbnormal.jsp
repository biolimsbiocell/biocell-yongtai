﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/experiment/sequencingLife/sequencingLifeAbnormal.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	</td>
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.poolingNumber"/></td>
                   	<td align="left"  >
 						<input type="text" size="20" maxlength="25" id="sequencingAbnormal_code"
                   	 name="code" searchField="true" title="<fmt:message key="biolims.common.poolingNumber"/>"    />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.fcSerialNumber"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_fcCode"
                   	 name="fcCode" searchField="true" title="<fmt:message key="biolims.common.fcSerialNumber"/>"    />
                   	</td>
                   	
			</tr>
			<tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.laneNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_laneCode"
                   	 name="laneCode" searchField="true" title="<fmt:message key="biolims.common.laneNumber"/>"    />
                   	</td>
                   	
               		<td class="label-title" ><fmt:message key="biolims.common.machineNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_machineNum"
                   	 name="machineNum" searchField="true" title="<fmt:message key="biolims.common.laneNumber"/>"    />
                   	</td>
               	 
               	 	<td class="label-title" ><fmt:message key="biolims.common.mixingvolume"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_mixVolume"
                   	 name="mixVolume" searchField="true" title="<fmt:message key="biolims.common.mixingvolume"/>"    />
                   	  
                   	</td>
		
                   	  
			</tr>
			<tr>
                   
               	 	<td class="label-title" ><fmt:message key="biolims.common.resultsDetermine"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_resultDecide"
                   	 name="resultDecide" searchField="true" title="<fmt:message key="biolims.common.resultsDetermine"/>"    />
                   	</td>
                   	
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.theNextStepTo"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_flowStep"
                   	 name="flowStep" searchField="true" title="<fmt:message key="biolims.common.theNextStepTo"/>"    />
                   	</td>
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.affirmToPerform"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sequencingAbnormal_confirm"
                   	 name="confirm" searchField="true" title="<fmt:message key="biolims.common.affirmToPerform"/>"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="sequencingAbnormaldiv"></div>
		
		<div id="bat_ok_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.affirmToPerform"/></span></td>
                <td><select id="isExecute"  style="width:100">
               			<option value="" <s:if test="isExecute==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isExecute==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isExecute==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
   		<!-- <form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sequencingAbnormal_tree_page"></div> -->
		<select id="resultDecide" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
			<option value="1"><fmt:message key="biolims.common.unqualified"/></option>
			<option value="0"><fmt:message key="biolims.common.qualified"/></option>
		</select>
		<select id="confirm" style="display: none">
			<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
			<option value="1"><fmt:message key="biolims.common.pass"/></option>
			<option value="0"><fmt:message key="biolims.common.noPass"/></option>
		</select>
</body>
</html>



