﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.attachment"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=sequencing&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/sequencingLife/sequencingLifeEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
  <div style="float:left;width:25%;" id="sequencingTemppage"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			   <input type="hidden" name="sequencing_method" id="sequencing_method" value="<%=request.getParameter("method") %>" />
			<table class="frame-table" width="100%">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_id"
                   	 name="sequencing.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="sequencing.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_name"
                   	 name="sequencing.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="sequencing.name"/>"
                   	  />
                   	  
                   	</td>
			
					<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sequencing_createUser_name"  value="<s:property value="sequencing.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sequencing_createUser" name="sequencing.createUser.id"  value="<s:property value="sequencing.createUser.id"/>" > 
                   	</td>
			
				
			</tr>
			<tr>
			
					<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_createDate"
                   	 name="sequencing.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:property value="sequencing.createDate"/>"
                   	                      	  />
                   	  
                   	</td>
                   	
					<td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sequencing_template_name"  value="<s:property value="sequencing.template.name"/>" class="text input" readonly="readOnly"  />
 						<input type="hidden" id="sequencing_template" name="sequencing.template.id"  value="<s:property value="sequencing.template.id"/>" > 
 						<input type="hidden" readonly="readOnly" id="sequencing_template_templateFieldsCode" name="sequencing.template.templateFieldsCode"  value="<s:property value="sequencing.template.templateFieldsCode"/>" >
 						<input type="hidden" readonly="readOnly" id="sequencing_template_templateFieldsItemCode" name="sequencing.template.templateFieldsItemCode"  value="<s:property value="sequencing.template.templateFieldsItemCode"/>" >
 						<img alt='<fmt:message key="biolims.common.chooseExperimentalTemplate"/>' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="TemplateFun()" />                   		
                   	</td>
			
					
					<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showAcceptUser = Ext.get('showAcceptUser');
showAcceptUser.on('click', 
 function loadAcceptUser(){
var win = Ext.getCmp('loadAcceptUser');
if (win) {win.close();}
var loadAcceptUser= new Ext.Window({
id:'loadAcceptUser',modal:true,title:'<fmt:message key="biolims.common.selectGroup"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=loadAcceptUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 loadAcceptUser.close(); }  }]  });     loadAcceptUser.show(); }
);
});
 function setloadAcceptUser(rec){
document.getElementById('sequencing_acceptUser').value=rec.get('id'); 
					document.getElementById('sequencing_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('loadAcceptUser')
if(win){win.close();}
}
</script>

			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sequencing_acceptUser_name"  value="<s:property value="sequencing.acceptUser.name"/>" class="text input" readonly="readOnly"  />
 						<input type="hidden" id="sequencing_acceptUser" name="sequencing.acceptUser.id"  value="<s:property value="sequencing.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.chooseTester1"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sequencing_testUserOneName" name="sequencing.testUserOneName" value="<s:property value="sequencing.testUserOneName"/>" readonly="readOnly"  />
 						<input type="hidden" id="sequencing_testUserOneId" name="sequencing.testUserOneId"  value="<s:property value="sequencing.testUserOneId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(1);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
               		<td class="label-title" ><fmt:message key="biolims.common.chooseTester2"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sequencing_testUserTwoName" name="sequencing.testUserTwoName"    value="<s:property value="sequencing.testUserTwoName"/>" readonly="readOnly"  />
 						<input type="hidden" id="sequencing_testUserTwoId" name="sequencing.testUserTwoId"  value="<s:property value="sequencing.testUserTwoId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(2);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
               		<td class="label-title" ><fmt:message key="biolims.wk.confirmUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" >
 						<input type="text" size="20" readonly="readOnly"  id="sequencing_confirmUser_name"  value="<s:property value="sequencing.confirmUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="sequencing_confirmUser" name="sequencing.confirmUser.id"  value="<s:property value="sequencing.confirmUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(3);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.chipBar"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_fcCode"
                   	 name="sequencing.fcCode" title="芯片条码"
                   	   
						value="<s:property value="sequencing.fcCode"/>"
                   	  />
                   	  <td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="sequencing_confirmDate"
                   	 name="sequencing.confirmDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:date name="sequencing.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
                  <!--  	  <td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sequencing_reciveUser_name"  value="<s:property value="sequencing.reciveUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="sequencing_reciveUser" name="sequencing.reciveUser.id"  value="<s:property value="sequencing.reciveUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
                    -->	
            </tr>
			<tr>						
						
						<td class="label-title" ><fmt:message key="biolims.common.machineNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="sequencing_deviceCode_name"  value="<s:property value="sequencing.deviceCode.name"/>"  readonly="readOnly"  />
 						<input type="text" id="sequencing_deviceCode" readonly="readOnly" name="sequencing.deviceCode.id"  value="<s:property value="sequencing.deviceCode.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectInstrumentCode"/>' id='showdeviceCode' src='${ctx}/images/img_lookup.gif' onClick="selectinstrument()"	class='detail'    />                   		
                   	</td>
						
						
						<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_note"
                   	 name="sequencing.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
						value="<s:property value="sequencing.note"/>"
                   	  />
                   	  
                   	</td>
						
						
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.stateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="sequencing_state"
                   	 name="sequencing.state" title="<fmt:message key="biolims.common.stateID"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sequencing.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_stateName"
                   	 name="sequencing.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sequencing.stateName"/>"
                   	  />
                   	  
                   	</td>
			
               	 	
			</tr>
			
			<tr>
			  	 <%-- <td class="label-title" ><fmt:message key="biolims.common.type"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
					<select id="sequencing_type" name="sequencing.type" onchange="changeType()">
								<option value="1" <s:if test="sequencing.type==1">selected="selected"</s:if>>illumina平台</option>
								<option value="2" <s:if test="sequencing.type==2">selected="selected"</s:if>>Life平台</option>
					</select>
	                 </td>   --%>
				<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
							</td>
			</tr>
			<%-- <tr>
                  
                   <td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_note"
                   	 name="sequencing.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
						value="<s:property value="sequencing.note"/>"
                   	  />
                  
                   </td>
			</tr> --%>
			<tr>
				<td class="label-title" >Key Signal（≥60)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_keySignal"
                   	 name="sequencing.keySignal" title="Key Signal（≥60)"
                   	   
						value="<s:property value="sequencing.keySignal"/>"
                   	 />                  
                 </td>
                 
                 <td class="label-title" >ISP Loading（≥70%）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_ispLoding"
                   	 name="sequencing.ispLoding" title="ISP Loading（≥70%）"
                   	   
						value="<s:property value="sequencing.ispLoding"/>"
                   	 />                  
                 </td>
                 
			</tr>
			<tr>
				<td class="label-title" >Total Reads（≥70M)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 
                   	<input type="text" size="20" maxlength="25" id="sequencing_totalReads"
                   	 name="sequencing.totalReads" title="Total Reads（≥70M)"
                   	   
						value="<s:property value="sequencing.totalReads"/>"
                   	 />                  
                 </td>
                 
                              
                 </td>
                 <td class="label-title" >Polyclonal（≤30%)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_polyclonal"
                   	 name="sequencing.polyclonal" title="Polyclonal(≤30%)"                  	   
						value="<s:property value="sequencing.polyclonal"/>"
                   	 />                  
                 </td>  
                 
			</tr>
			<tr>
				<td class="label-title" >Read Length（100-170bp)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_readLength"
                   	 name="sequencing.readLength" title="Read Length(100-170bp)"
                   	   
						value="<s:property value="sequencing.readLength"/>"
                   	 />                  
                 </td>
				
				
				<td class="label-title" >Low Quality（≤25%)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sequencing_lowQuality"
                   	 name="sequencing.lowQuality" title="Low Quality(≤25%)"
                   	   
						value="<s:property value="sequencing.lowQuality"/>" 
                   	 />                  
                 </td>
                 
                 
			</tr>
			
            </table>
            <input type="hidden" name="sequencingItemJson" id="sequencingItemJson" value="" />
            <input type="hidden" name="sequencingTemplateJson" id="sequencingTemplateJson" value="" />
            <input type="hidden" name="sequencingReagentJson" id="sequencingReagentJson" value="" />
            <input type="hidden" name="sequencingCosJson" id="sequencingCosJson" value="" />
            <input type="hidden" name="sequencingResultJson" id="sequencingResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sequencing.id"/>" />
            </form>
            <div id="tabs">
            <div id="sequencingItempage" width="100%" height:10px></div>
            <ul>
				<li><a href="#sequencingTemplatepage"><fmt:message key="biolims.common.executionStep"/></a></li>
				<li><a href="#sequencingReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
				<li><a href="#sequencingCospage" onClick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
			
			<div id="sequencingTemplatepage" width="100%" height:10px></div>
			<div id="sequencingReagentpage" width="100%" height:10px></div>
			<div id="sequencingCospage" width="100%" height:10px></div>
			
			</div>
			<div id="sequencingResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
