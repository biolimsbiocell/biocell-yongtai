<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<link rel="stylesheet" type="text/css"
	href="${ctx}/lims/plugins/bootstrap-slider/slider.css" />
<style type="text/css">
.table>tbody>tr>td {
	border-top: none;
}

.slider-horizontal {
	margin: 18px 0px;
}

.scjl .form-control {
	-webkit-box-shadow: none;
	box-shadow: none;
	border-top: none;
	border-left: none;
	border-right: none;
	margin-bottom: 8px;
}
#pageOneOperationalOrder>tr{
	font-size:16px;
}

</style>
</head>

<body>
	<!--下一步的询问狂-->
	<div id="nextAsk" class="hide"></div>
	<div class="content-wrapper" id="content" style="margin-left: 0px;">
		<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<input type="hidden" id='orderNumBy'
			value="${requestScope.orderNumBy}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.common.testSteps" />
							<small style="color: #fff;"> <fmt:message
									key="biolims.common.taskId" />: <span id="cellPassage_id"><s:property
										value="reagentTask.id" /></span> <fmt:message
									key="biolims.common.commandPerson" />: <span
								userId="<s:property value="reagentTask.createUser.id"/>"
								id="cellPassage_createUser"><s:property
										value="reagentTask.createUser.name" /></span> <fmt:message
									key="biolims.sample.createDate" />: <span
								id="cellPassage_createDate"><s:property
										value="reagentTask.createDate" /></span> <fmt:message
									key="biolims.common.state" />: <span
								state="<s:property value="reagentTask.state"/>"
								id="cellPassage_state"><s:property
										value="reagentTask.stateName" /></span> <fmt:message
									key="biolims.common.experimenter" />: <span
								id="testUserOneName"><s:property
										value="reagentTask.testUserOneName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body">
						<!--步骤-->
						<div class="col-xs-2">
							<div id="wizard_verticle" class="form_wizard wizard_verticle"
								style="padding-top: 50px;">
								<ul class="list-unstyled wizard_steps">
								</ul>
							</div>
						</div>
						<!--步骤明细-->
						<div class="col-xs-10">
							<div class="box box-primary box-solid ipddd">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
										<span id="steptiele"></span> <small style="color: #fff;">
											<fmt:message key="biolims.common.templateName" />: <span
											id="templateName"><s:property
													value="reagentTask.template.name" /></span>
										</small>

									</h3>
									<div class="box-tools pull-right">
										<button type="button" id="stepContentBtn"
											class="btn btn-box-tool"
											style="font-size: 20px; border: 1px solid;">
											<i class="fa fa-file-word-o"></i>
										</button>
										<button type="button" class="btn btn-box-tool"
											style="font-size: 16px; border: 1px solid;"
											onclick="printTempalte()">
											<i class="fa fa-print"></i> 打印
										</button>
									</div>
								</div>
								<div class="box-body">
									<div class="col-md-12" id="stepContentModer"
										style="position: absolute; z-index: 6; height: 0%; right: 10px; width: 0%; overflow: hidden;">
										<div class="box box-success">
											<div class="box-header">
												<i class="glyphicon glyphicon-leaf"></i>
												<h3 class="box-title">
													<fmt:message key="biolims.common.stepDetail" />
												</h3>
												<div class="box-tools pull-right"></div>
											</div>
											<div class="box-body" id="stepcontent"
												style="height: 100%; overflow: hidden;"></div>
										</div>
									</div>

									<ul class="nav nav-tabs bwizard-steps" role="tablist"
										style="border: none; padding-left: 13px;">
										<li role="presentation" class="active" note="1"><a
											href="#firstTab" aria-controls="firstTab" role="tab"
											data-toggle="tab">工前准备及检查</a></li>
										<li role="presentation" note="3"><a href="#thirdTab"
											aria-controls="secondTab" role="tab" data-toggle="tab">生产操作</a>
										</li>
										<li id="cc" role="presentation" note="2"><a href="#secondTab"
											aria-controls="firstTab" role="tab" data-toggle="tab">质检项</a>
										</li> 
										<li role="presentation" note="4"><a href="#fourTab"
											aria-controls="thirdTab" role="tab" data-toggle="tab">完工清场</a>
										</li>
									</ul>

									<!-- Tab panes -->
									<div class="tab-content" style="padding-top: 8px;">
										<div role="tabpanel" class="tab-pane active" id="firstTab">
											<div class="col-xs-12">
												<div class="box box-warning">
													<div class="box-header with-border">
														<i class="fa fa-balance-scale"></i>
														<h3 class="box-title">选择设备</h3>
														<div class="box-tools pull-right">
															<button type="button"
																class="btn btn-box-tool btn-primary" id="qrsbfj"
																onclick="savePage1Cos()">
																<i class="fa  fa-save"></i> 保存设备房间并占用
															</button>
															<!--  <button type="button" class="btn btn-box-tool" id="ksgqzb" onclick="startPrepare()"><i class="fa  fa-save"></i>  开始工前准备
									                </button> -->
														</div>
													</div>
													<div id="" class="box-body" style="overflow: auto;">
														<table
															class="table table-striped table-condensed table-responsive"
															style="font-size: 12px">
															<thead>
																<tr>
																	<th><span class="badge label-warning">生产步骤</span></th>
																	<th><span class="badge label-warning">设备类型</span></th>
																	<th><span class="badge label-warning">设备编号</span></th>
																	<th><span class="badge label-warning">选定设备</span></th>
																</tr>
															</thead>
															<tbody id="pageOneChoseYQ">
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="col-xs-9">
												<div class="box box-primary">
													<div class="box-header with-border">
														<i class="fa fa-hand-o-right"></i>
														<h3 class="box-title">操作指令</h3>
													</div>
													<div id="" class="box-body" style="overflow: auto;">
														<table
															class="table table-striped table-condensed table-responsive"
															style="font-size: 12px">
															<thead>
																<tr>
																	<th><span class="badge bg-light-blue">序号</span></th>
																	<th><span class="badge bg-light-blue">操作指令及工艺参数</span></th>
																	<th><span class="badge bg-light-blue">生产检查</span></th>
																	<th><span class="badge bg-light-blue">操作记录</span></th>
																</tr>
															</thead>
															<tbody id="pageOneOperationalOrder">

															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="col-xs-3">
												<div class="box box-info">
													<div class="box-header with-border">
														<i class="fa  fa-keyboard-o"></i>
														<h3 class="box-title">生产记录</h3>
													</div>
													<div class="box-body scjl" id="pageOneProductionRecord">

													</div>

												</div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane" id="secondTab">
													<div class="col-xs-7">
														<div class="box box-primary">
															<div class="box-header with-border">
																<i class="fa fa-flask"></i>
																<h3 class="box-title">样本信息</h3>
															</div>
															<!-- 混合前 -->
															<div class="box-body ipadmini"   >
																<table class="table table-hover table-striped table-bordered table-condensed" id="noBlendTable" style="font-size: 12px;"></table>
															</div>
															<!-- 混合后 -->
															<!--  <div class="box-body  ipadmini" id="blend_div" style="display: none" >
																<table class="table table-hover table-striped table-bordered table-condensed" id="blendTable" style="font-size: 12px;"></table>
															</div>-->
															
															<div class="box-body" id="plateModal"
																style="display: none">
															<!-- <div id="plateModal"> class="col-md-12 col-xs-12"-->
																<input type="hidden" id="maxNum"
																	value="<s:property value=" qualityTest.maxNum "/>"> <input
																	type="hidden" id="temRow"
																	value="<s:property value=" qualityTest.template.storageContainer.rowNum "/>">
																<input type="hidden" id="temCol"
																	value="<s:property value=" qualityTest.template.storageContainer.colNum "/>">
																<div class="col-xs-12 plateDiv">
																	<div class="box box-primary">
																		<div class="box-header with-border">
																			<i class="glyphicon glyphicon-leaf"></i>
																			<h3 class="box-title">培养箱</h3>
																			<div class="box-tools pull-right" style="display: none">
																				<button type="button" class="btn btn-sm btn-info active"
																					order="h">
																					<i class="glyphicon glyphicon-resize-horizontal"></i> 横向排列
																				</button>
																				<button type="button" class="btn btn-sm btn-info" order="z">
																					<i class="glyphicon glyphicon-resize-vertical"></i> 纵向排列
																				</button>
																			</div>
																		</div>
									
																		<div class="box-body">
																			<div plateNum="" id="plateNum">
																				<table class="table table-bordered  plate"></table>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="col-xs-5">
														<div class="box box-info">
															<div class="box-header with-border">
																<i class="fa fa-flask"></i>
																<h3 class="box-title">质检内容</h3>
																<small id="zjType" style="margin-left: 20px;font-weight: 900;"></small>
																<div class="box-tools" style="width: 70%; top: 2px;">
																	<!--  <button class="btn btn-info btn-xs pull-right">生成指间任务</button>-->
																</div>
															</div>
															<div class="box-body ipadmini">
																<ul class="todo-list" id="zhijianBody">
																</ul>
															</div>
														</div> 
														<div class="box box-warning">
															<div class="box-header with-border">
																<i class="fa fa-flask"></i>
																<h3 class="box-title">已提交质检明细</h3>
															</div>
															<div id="gu" class="box-body ipadmini">
															
																<table class="table table-hover table-striped table-bordered table-condensed" id="zhijianDetails" style="font-size: 12px;"></table>
															</div>
														</div>
													</div>
												</div>
										<!-- <div role="tabpanel" class="tab-pane" id="secondTab">
											<div class="col-xs-12">
												<div class="box box-info">
													<div class="box-header with-border">
														<i class="fa fa-flask"></i>
														<h3 class="box-title">质检内容</h3>
														<small id="zjType"
															style="margin-left: 20px; font-weight: 900;"></small>
														<div class="box-tools" style="width: 70%; top: 2px;">
															<button class="btn btn-info btn-xs pull-right">生成指间任务</button>
														</div>
													</div>
													<div class="box-body">
														<ul class="todo-list" id="zhijianBody">
														</ul>
													</div>
												</div>
											</div>
										</div> -->
										<div role="tabpanel" class="tab-pane" id="thirdTab">
											<ul class="timeline" style="margin: 0 15px;">

											</ul>
										</div>
										<div role="tabpanel" class="tab-pane" id="fourTab">
											<div class="col-xs-8">
												<div class="box box-primary">
													<div class="box-header with-border">
														<i class="fa fa-hand-o-right"></i>
														<h3 class="box-title">操作指令</h3>
													</div>
													<div id="" class="box-body" style="overflow: auto;">
														<table class="table table-responsive table-striped">
															<thead>
																<tr>
																	<th><span class="badge bg-light-blue">序号</span></th>
																	<th><span class="badge bg-light-blue">操作指令及工艺参数</span></th>
																	<th><span class="badge bg-light-blue">生产检查</span></th>
																</tr>
															</thead>
															<tbody id="pageFourOperationalOrder">

															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="box box-info">
													<div class="box-header with-border">
														<i class="fa  fa-keyboard-o"></i>
														<h3 class="box-title">生产记录</h3>
													</div>
													<div class="box-body scjl" id="pageFourProductionRecord">

													</div>
													<div id="tableFileLoad"></div>
													<div class="input-group">
														<button type="button" class="btn btn-info btn-sm"
															onclick="fileInput1()">上传附件</button>
														&nbsp;&nbsp;
														<button type="button" class="btn btn-info btn-sm"
															onclick="fileView()">查看附件</button>
													</div>
													<br>
													<input type="button" class="form-control btn-success " id="jxsc" onclick="finishStep()" value="继续生产">
													<!-- <input type="button" style="margin-top:15px;display: none" class="form-control btn-success " id="next" value="填写生产结果"> -->
													<br>
												</div>
											</div>
										</div>

									</div>

								</div>

							</div>
						</div>
					</div>

				</div>
				<div class="box-footer" style="position: relative;">
			<div style="position: absolute;right: 30px;bottom: 10px;">
						<button type="button" class="btn btn-primary" id="save"
							onclick="saveStepItem()">
							<i class="glyphicon glyphicon-saved"></i>
							<fmt:message key="biolims.common.save" />
						</button>
						<button type="button" style="display: none"
							class="btn btn-primary" id="sp" onclick="sp()">
							<i class="glyphicon glyphicon-saved"></i>
							<fmt:message key="biolims.common.handle" />
						</button>
						<%-- <button type="button" class="btn btn-primary" id="prev">
							<i class="glyphicon glyphicon-arrow-up"></i>
							<fmt:message key="biolims.common.back" />
						</button>
						<button type="button" class="btn btn-primary" id="next">
							<i class="glyphicon glyphicon-arrow-down"></i>
							<fmt:message key="biolims.common.nextStep" />
						</button> --%>
					</div>

				</div>
			</div>
		</div>
	</div>

	</section>
	</div>
	<script src="${ctx}/lims/dist/js/app.min.js "></script>
	<script src="${ctx}/lims/plugins/layer/layer.js" type="text/javascript" charset="utf-8"></script>
	<script src="${ctx}/lims/plugins/bootstrap-slider/bootstrap-slider.js"
		type="text/javascript" charset="utf-8"></script>
	<script src="${ctx}/js/experiment/reagent/reagentTaskSteps.js"
		type="text/javascript" charset="utf-8"></script>
</body>
</html>