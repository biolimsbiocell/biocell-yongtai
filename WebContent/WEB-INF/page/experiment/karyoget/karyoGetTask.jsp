﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/karyoget/karyoGetTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="karyoGetTask_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="karyoGetTask_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 </tr>
 <tr>
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="karyoGetTask_createUser"
				documentName="karyoGetTask_createUser_name"/>
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="karyoGetTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="karyoGetTask_createUser" name="karyoGetTask.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	
                   	 <td class="label-title"  style="display:none"  >状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="karyoGetTask_state"
                   	 name="state" searchField="true" title="状态"   style="display:none"    />
                   	


                   	</td>


					            
               	 	<td class="label-title" >状态名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="karyoGetTask_stateName"
                   	 name="stateName" searchField="true" title="状态名称"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>                   	
			</tr>
			<tr>
           
                   	  
                   	</td>
<%-- 			<g:LayOutWinTag buttonId="showsampleType" title="选择样本类型"
				hasHtmlFrame="true"
				html="${ctx}/experiment/karyo/dicSampleTypeSelect.action"
				isHasSubmit="false" functionName="DicSampleTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyoGetTask_sampleType').value=rec.get('id');
				document.getElementById('karyoGetTask_sampleType_name').value=rec.get('name');" /> --%>
<%--                	 	<td class="label-title" >样本类型</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="karyoGetTask_sampleType_name" searchField="true"  name="sampleType.name"  value="" class="text input" />
 						<input type="hidden" id="karyoGetTask_sampleType" name="karyoGetTask.sampleType.id"  value="" > 
 						<img alt='选择样本类型' id='showsampleType' src='${ctx}/images/img_lookup.gif' 	class='detail'  onclick="loadTestDicSampleType()"  />                   		
                   	</td>
                   	
                   	 --%>
                   	            
                   	                     	
                   	<g:LayOutWinTag buttonId="showtemplate" title="选择实验模板"
				hasHtmlFrame="true"
				html="${ctx}/system/template/template/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyoGetTask_template').value=rec.get('id');
				document.getElementById('karyoGetTask_template_name').value=rec.get('name');" />
               	 	<td class="label-title" >实验模板</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="karyoGetTask_template_name" searchField="true"  name="template.name"  value="" class="text input" />
 						<input type="hidden" id="karyoGetTask_template" name="karyoGetTask.template.id"  value="" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	            
                   	               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="karyoGetTask_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
			
			</tr>
			<tr>


                   	
                   	
          
                   	
					</tr>
					<tr>
 
                  
                   	
                   	  
     
                   	
                   	    	 	<td class="label-title" >创建时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	
                   	
                   	
			</tr>
            </table>
		</form>
		</div>
		<div id="show_karyoGetTask_div"></div>
   		<form name='excelfrm' action='${ctx}/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_karyoGetTask_tree_page"></div>
</body>
</html>



