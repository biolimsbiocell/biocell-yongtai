<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=blendTask&id=${blendTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/blend/blendTaskEdit.js"></script>
<div style="float:left;width:25%" id="blendTaskTempPage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="blendTask_id"
                   	 name="blendTask.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="blendTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.laneNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="blendTask_lane"
                   	 name="blendTask.lane" title="<fmt:message key="biolims.common.laneNumber"/>"
                   	   
	value="<s:property value="blendTask.lane"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="blendTask_name"
                   	 name="blendTask.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="blendTask.name"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
			<g:LayOutWinTag buttonId="showreciveUser" title='<fmt:message key="biolims.common.chooseTheExperimenter"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_reciveUser').value=rec.get('id');
				document.getElementById('blendTask_reciveUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="blendTask_reciveUser_name"  value="<s:property value="blendTask.reciveUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="blendTask_reciveUser" name="blendTask.reciveUser.id"  value="<s:property value="blendTask.reciveUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="blendTask_reciveDate"
                   	 name="blendTask.reciveDate" title="<fmt:message key="biolims.common.experimentalTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="blendTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_createUser').value=rec.get('id');
				document.getElementById('blendTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="blendTask_createUser_name"  value="<s:property value="blendTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="blendTask_createUser" name="blendTask.createUser.id"  value="<s:property value="blendTask.createUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseFromPeople"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="blendTask_createDate"
                   	 name="blendTask.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="blendTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showtemplate" title='<fmt:message key="biolims.common.selectATemplate"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_template').value=rec.get('id');
				document.getElementById('blendTask_template_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.template"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="blendTask_template_name"  value="<s:property value="blendTask.template.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="blendTask_template" name="blendTask.template.id"  value="<s:property value="blendTask.template.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectATemplate"/>' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showacceptUser" title='<fmt:message key="biolims.common.selectGroup"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_acceptUser').value=rec.get('id');
				document.getElementById('blendTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="blendTask_acceptUser_name"  value="<s:property value="blendTask.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="blendTask_acceptUser" name="blendTask.acceptUser.id"  value="<s:property value="blendTask.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="blendTask_state"
                   	 name="blendTask.state" title="<fmt:message key="biolims.common.state"/>"
                   	   
	value="<s:property value="blendTask.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.nameOfTheState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="20" maxlength="25" id="blendTask_state"
                   	 name="blendTask.state" value="<s:property value="blendTask.state"/>"/>
                   	<input type="text" size="20" maxlength="25" id="blendTask_stateName"
                   	 name="blendTask.stateName" title="<fmt:message key="biolims.common.nameOfTheState"/>"
	value="<s:property value="blendTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="blendTask_note"
                   	 name="blendTask.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="blendTask.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="blendTaskItemJson" id="blendTaskItemJson" value="" />
            <input type="hidden" name="blendTaskTemplateJson" id="blendTaskTemplateJson" value="" />
            <input type="hidden" name="blendTaskReagentJson" id="blendTaskReagentJson" value="" />
            <input type="hidden" name="blendTaskCosJson" id="blendTaskCosJson" value="" />
            <input type="hidden" name="blendTaskResultJson" id="blendTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="blendTask.id"/>" />
            </form>
            <div id="blendTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#blendTaskTemplatepage"><fmt:message key="biolims.common.executionStep"/></a></li>
			<li><a href="#blendTaskReagentpage"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#blendTaskCospage"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
			<div id="blendTaskTemplatepage" width="100%" height:10px></div>
			<div id="blendTaskReagentpage" width="100%" height:10px></div>
			<div id="blendTaskCospage" width="100%" height:10px></div>
			</div>
			<div id="blendTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
