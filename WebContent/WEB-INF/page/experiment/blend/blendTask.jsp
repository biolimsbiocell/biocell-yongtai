﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/blend/blendTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="blendTask_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.laneNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="blendTask_lane"
                   	 name="lane" searchField="true" title="<fmt:message key="biolims.common.laneNumber"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="blendTask_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showreciveUser" title='<fmt:message key="biolims.common.chooseTheExperimenter"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_reciveUser').value=rec.get('id');
				document.getElementById('blendTask_reciveUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="blendTask_reciveUser_name" searchField="true"  name="reciveUser.name"  value="" class="text input" />
 						<input type="hidden" id="blendTask_reciveUser" name="blendTask.reciveUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startreciveDate" name="startreciveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="reciveDate1" name="reciveDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreciveDate" name="endreciveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="reciveDate2" name="reciveDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_createUser').value=rec.get('id');
				document.getElementById('blendTask_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="blendTask_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="blendTask_createUser" name="blendTask.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseFromPeople"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showtemplate" title='<fmt:message key="biolims.common.selectATemplate"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_template').value=rec.get('id');
				document.getElementById('blendTask_template_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.template"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="blendTask_template_name" searchField="true"  name="template.name"  value="" class="text input" />
 						<input type="hidden" id="blendTask_template" name="blendTask.template.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectATemplate"/>' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showacceptUser" title='<fmt:message key="biolims.common.experimentalGroup"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/blend/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('blendTask_acceptUser').value=rec.get('id');
				document.getElementById('blendTask_acceptUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="blendTask_acceptUser_name" searchField="true"  name="acceptUser.name"  value="" class="text input" />
 						<input type="hidden" id="blendTask_acceptUser" name="blendTask.acceptUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.experimentalGroup"/>' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.state"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="blendTask_state"
                   	 name="state" searchField="true" title="<fmt:message key="biolims.common.state"/>"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.nameOfTheState"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="blendTask_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.nameOfTheState"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="blendTask_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_blendTask_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_blendTask_tree_page"></div>
</body>
</html>



