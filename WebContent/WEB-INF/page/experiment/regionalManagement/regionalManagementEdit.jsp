<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
#btn_submit {
	display: none;
}

#btn_changeState {
	display: none;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						区域管理
					</h3>
					<small style="color: #fff;"> 创建人: <span
						id="regionalManagement_createUser"><s:property
								value=" regionalManagement.createUser.name" /></span> 创建时间: <span
						id="regionalManagement_createDate"><s:date
								name="regionalManagement.createDate" format="yyyy-MM-dd" /></span> 状态: <span id="regionalManagement_state"><s:property
								value="regionalManagement.stateName" /></span> 
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<%-- <div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>区域管理</small>
								</span>
							</a></li>
						</ul>
					</div> --%>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<input type="hidden"  name="createUser-id"
								value="<s:property value=" regionalManagement.createUser.id "/>" />
							<input type="hidden"  name="createDate"
									value="<s:date name="regionalManagement.createDate" format="yyyy-MM-dd HH:mm:ss" />"
								/>
							<input type="hidden"  name="state"
								value="<s:property value=" regionalManagement.state "/>" />
							<input type="hidden"  name="stateName"
								value="<s:property value=" regionalManagement.stateName "/>" />
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">区域编号</span> 
                   						<input type="text" readonly="readonly"
											size="20" maxlength="20" id="regionalManagement_id"
											name="id"  changelog="<s:property value="regionalManagement.id"/>"
											  class="form-control"  value="<s:property value="regionalManagement.id"/>"/>  
					
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">区域名称</span> 
                   						<input type="text"
											size="20" maxlength="20" id="regionalManagement_name"
											name="name"  changelog="<s:property value="regionalManagement.name"/>"
											  class="form-control"  value="<s:property value="regionalManagement.name"/>"/>  
					
									</div>
								</div>	
								<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
										<span class="input-group-addon">区域状态</span> 
	                   						<select class="form-control" onchange="regionalStateChange()" id="regionalManagement_regionalState" name="regionalState" 
	                   						changelog="<s:property value="regionalManagement.regionalState"/>" >
											<option value="1"
												<s:if test='regionalManagement.regionalState=="1"'>selected="selected"</s:if>>
												可用</option>
											<option value="0"
												<s:if test='regionalManagement.regionalState=="0"'>selected="selected"</s:if>>
												不可用</option>
											</select> 
										</div>
								</div>		
									</div>
									<div class="row">
											<div class="col-md-8 col-sm-6 col-xs-12">
												<div class="input-group">
												<span class="input-group-addon">区域描述</span> 
			                   						<textarea   id="regionalManagement_note" name="note"    class="form-control"   
														  style="overflow: hidden; width: 420px; height: 80px;"  ><s:property value="regionalManagement.note"/></textarea>  
												</div>
											</div>
									</div>
								<!-- <div class="row">
									<div class="panel" style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i> 自定义字段
													
											</h3>
										</div>
									</div>
								</div> -->
							<div id="fieldItemDiv"></div>
								
							<br> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="regionalManagement.id"/>" />
							<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value="regionalManagement.fieldContent"/>" />
							<input type="hidden" id="itemJson" name="itemJson" value="" />	
								
						</form>
					</div>
					<!--table表格-->
					<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="regionalManagementTable" style="font-size: 14px;"></table>
				    </div>
					</div>
				<!-- <div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/experiment/regionalManagement/regionalManagementEdit.js"></script>
	<script type="text/javascript" src="${ctx}/js/experiment/regionalManagement/regionalManagementItem.js"></script>
</body>

</html>	
