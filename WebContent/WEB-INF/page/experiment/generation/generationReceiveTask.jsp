﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<g:LayOutWinTag buttonId="dnawzBtn" title="选择位置" hasHtmlFrame="true"
		html="${ctx}/storage/position/showStoragePositionTree1.action?setStoragePostion=true"
		isHasSubmit="false" functionName="showStoragePositionFund"
		hasSetFun="true" documentId="generationReceiveTask_storagePrompt"
		documentName="generationReceiveTask_storagePrompt_name" />
		<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=generationReceiveTask&id=${generationReceiveTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/generation/generationReceiveTask.js"></script>
  <div style="float:left;width:25%" id="generationReceiveTempPage"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
						<td colspan="9">
							<div class="standard-section-header type-title">
								<label><fmt:message key="biolims.common.basicInformation"/></label>
							</div>
						</td>
				</tr>
			<tr>
			
			
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="generationReceiveTask_id"
                   	 name="generationReceiveTask.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   
	value="<s:property value="generationReceiveTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="generationReceiveTask_name"
                   	 name="generationReceiveTask.name" title="<fmt:message key="biolims.common.name"/>"
                   	   
	value="<s:property value="generationReceiveTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationReceiveTask_describe"
                   	 name="generationReceiveTask.describe" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="generationReceiveTask.describe"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="generationReceiveTask_receiver_name"  value="<s:property value="generationReceiveTask.receiver.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="generationReceiveTask_receiver" name="qpcrReceiveTask.receiver.id"  value="<s:property value="qpcrReceiveTask.receiver.id"/>" > 
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="generationReceiveTask_receiverDate"
                   	 name="generationReceiveTask.receiverDate" title="<fmt:message key="biolims.common.receivingDate"/>" class="text input readonlytrue"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="generationReceiveTask.receiverDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
					<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowStateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="15" id="generationReceiveTask_state"
                   	 name="generationReceiveTask.state" title="<fmt:message key="biolims.common.workflowStateID"/>"
                   	   
	value="<s:property value="generationReceiveTask.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
					<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationReceiveTask_stateName"
                   	 name="generationReceiveTask.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="generationReceiveTask.stateName"/>"
                   	  />
			
			
			</tr>
			<tr>
			
			
               	 	
                   	  
                   	</td>
                   	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
				<tr>
						<td colspan="9">
							<div class="standard-section-header type-title">
								<label><fmt:message key="biolims.common.basicInformation"/></label>
							</div>
						</td>
				</tr>
				<tr>
<%-- 					<g:LayOutWinTag buttonId="showstoragePrompt" title="选择储位提示" --%>
<%-- 					hasHtmlFrame="true" --%>
<%-- 					html="${ctx}/experiment/wk/generationReceiveTask/storagePositionSelect.action" --%>
<%-- 					isHasSubmit="false" functionName="StoragePositionFun"  --%>
<%-- 	 				hasSetFun="true" --%>
<%-- 					extRec="rec" --%>
<%-- 					extStr="document.getElementById('generationReceiveTask_storagePrompt').value=rec.get('id'); --%>
<%-- 					document.getElementById('generationReceiveTask_storagePrompt_name').value=rec.get('name');" /> --%>
				
			
			
<!--                	 	<td class="label-title" >储位提示</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<%--  						<input type="text" size="20" readonly="readOnly"  id="generationReceiveTask_storagePrompt_name"  value="<s:property value="generationReceiveTask.storagePrompt.name"/>" class="text input readonlytrue" readonly="readOnly"  /> --%>
<%--  						<input type="hidden" id="generationReceiveTask_storagePrompt" name="generationReceiveTask.storagePrompt.id"  value="<s:property value="generationReceiveTask.storagePrompt.id"/>" >  --%>
<%--  						<img alt='选择储位提示' id='showstoragePrompt' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
<!--                    	</td> -->
							<td class="label-title"><fmt:message key="biolims.common.storageLocationHints"/></td>
					<td class="requiredcolumn"  colspan="9">
						<input type="text"
							class="input_parts text input  false false"
							name="generationReceiveTask.storagePrompt.id" id="generationReceiveTask_storagePrompt"
							value='<s:property value="generationReceiveTask.storagePrompt.id"/>'
							title="<fmt:message key="biolims.common.storageLocation"/>" size="10"
							maxlength="30" readonly="readOnly"> 
							<img alt='<fmt:message key="biolims.common.select"/>' id='dnawzBtn'
							name='show-btn' src='${ctx}/images/img_menu.gif'
							class='detail' /> 
						<input type="text" class="input_parts text input  readonlytrue"
							name="generationReceiveTask.storagePrompt.name" id="generationReceiveTask_storagePrompt_name"
							readonly="readonly"
							value='<s:property value="generationReceiveTask.storagePrompt.name"/>'
							title="<fmt:message key="biolims.common.storageLocation"/> "  size="30"
							maxlength="62"> 
					</td>
				</tr>
			
            </table>
            <input type="hidden" name="generationReceiveInfoJson" id="generationReceiveInfoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="generationReceiveTask.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#generationReceiveInfopage"><fmt:message key="biolims.common.plasmaReceiveDetailed"/></a></li>
           	</ul> 
			<div id="generationReceiveInfopage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
