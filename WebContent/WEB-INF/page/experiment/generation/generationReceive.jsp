﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/generation/generationReceive.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="generationReceiveTask_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"   style="display:none"    />
                   	
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="generationReceiveTask_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="generationReceiveTask_describe"
                   	 name="describe" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showreceiver" title='<fmt:message key="biolims.common.selectRecipients"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/generation/generationReceiveTask/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('generationReceiveTask_receiver').value=rec.get('id');
				document.getElementById('generationReceiveTask_receiver_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
                   	<td align="left"  >
 						<input type="text" size="50"   id="generationReceiveTask_receiver_name" searchField="true"  name="receiver.name"  value="" class="text input" />
 						<input type="hidden" id="generationReceiveTask_receiver" name="generationReceiveTask.receiver.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectRecipients"/>' id='showreceiver' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startreceiverDate" name="startreceiverDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiverDate1" name="receiverDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreceiverDate" name="endreceiverDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiverDate2" name="receiverDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showstoragePrompt" title='<fmt:message key="biolims.common.storageLocationHints"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/generation/generationReceiveTask/storagePositionSelect.action"
				isHasSubmit="false" functionName="StoragePositionFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('generationReceiveTask_storagePrompt').value=rec.get('id');
				document.getElementById('generationReceiveTask_storagePrompt_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.storageLocationHints"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="generationReceiveTask_storagePrompt_name" searchField="true"  name="storagePrompt.name"  value="" class="text input" />
 						<input type="hidden" id="generationReceiveTask_storagePrompt" name="generationReceiveTask.storagePrompt.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.storageLocationHints"/>' id='showstoragePrompt' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="generationReceiveTask_workState"
                   	 name="workState" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_generationReceive_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
<!-- 		<div id="show_generationReceive_tree_page"></div> -->
</body>
</html>



