﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- <style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style> -->
<script type="text/javascript" src="${ctx}/js/experiment/generation/generationAbnormalTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="generationAbnormalTask_sampleCode"
                   	 name="sampleCode" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    />
                   	  
                   	</td>
		
                   	  
                   
               	 	<td class="label-title" ><fmt:message key="biolims.common.resultsDetermine"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="generationAbnormalTask_resultDecide"
                   	 name="resultDecide" searchField="true" title="<fmt:message key="biolims.common.resultsDetermine"/>"    />
                   	</td>
                   	
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.theNextStepTo"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="generationAbnormalTask_nextFlow"
                   	 name="nextFlow" searchField="true" title="<fmt:message key="biolims.common.theNextStepTo"/>"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >INDEX</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="125" id="generationAbnormalTask_indexa"
                   	 name="indexa" searchField="true" title="INDEX"    />
                   	</td>
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.whetherOrNotToPerform"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="generationAbnormalTask_isExecute"
                   	 name="isExecute" searchField="true" title="<fmt:message key="biolims.common.whetherOrNotToPerform"/>"    />
                   	</td>
                   	
               	 	<!-- <td class="label-title" >反馈时间</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="150" id="generationAbnormalTask_backTime"
                   	 name="backTime" searchField="true" title="反馈时间"    />
                   	  
                   	</td> -->
			</tr>
            </table>
		</form>
		</div>
		
		
			<div id="generationAbnormalTaskdiv"></div>
			<div id="bat_uploadcsv_div" style="display: none">
				<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
			</div>
			<select id="resultDecide" style="display: none">
				<option value="0"><fmt:message key="biolims.common.qualified"/></option>
				<option value="1"><fmt:message key="biolims.common.unqualified"/></option>
			</select>
		
	

</body>
</html>


