﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=generationTest&id=${generationTest.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/generation/generationTestEdit.js"></script>
<div style="float:left;width:25%" id="generationTestTemppage"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationTest_id"
                   	 name="generationTest.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	    class="text input readonlytrue"
	value="<s:property value="generationTest.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationTest_name"
                   	 name="generationTest.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="generationTest.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true"
				html="${ctx}/experiment/tp/generationTest/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('generationTest_createUser').value=rec.get('id');
				document.getElementById('generationTest_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="generationTest_createUser_name"  value="<s:property value="generationTest.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="generationTest_createUser" name="generationTest.createUser.id"  value="<s:property value="generationTest.createUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseFromPeople"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationTest_createDate"
                   	 name="generationTest.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="generationTest.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showreciveUser" title='<fmt:message key="biolims.common.chooseTheExperimenter"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="generationTest_reciveUser"
				documentName="generationTest_reciveUser_name" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="generationTest_reciveUser_name"  value="<s:property value="generationTest.reciveUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="generationTest_reciveUser" name="generationTest.reciveUser.id"  value="<s:property value="generationTest.reciveUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationTest_reciveDate"
                   	 name="generationTest.reciveDate" title="<fmt:message key="biolims.common.experimentalTime"/>"
                   	  Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
	value="<s:property value="generationTest.reciveDate"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" style="display:none"><fmt:message key="biolims.common.stateID"/></td>
               	 	<td class="requiredcolumn" style="display:none"nowrap width="10px" ></td>            	 	
                   	<td align="left" style="display:none" >
                   	<input type="text" size="20"style="display:none" maxlength="25" id="generationTest_state"
                   	 name="generationTest.state" title="<fmt:message key="biolims.common.stateID"/>"
                   	   
	value="<s:property value="generationTest.state"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationTest_stateName"
                   	 name="generationTest.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	    readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="generationTest.stateName"/>"
                   	  />
                   	  
                   	</td>
                   	
					<td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
			        <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="40" readonly="readOnly"  id="generationTest_template_name"  value="<s:property value="generationTest.template.name"/>" />
 						<input type="text" id="generationTest_template" name="generationTest.template.id"  value="<s:property value="generationTest.template.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseExperimentalTemplate"/>' id='showTemplateFun' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()" />                   		
                   	</td>
			
			
					<g:LayOutWinTag buttonId="showAcceptUser" title='<fmt:message key="biolims.common.selectGroup"/>'
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('generationTest_acceptUser').value=rec.get('id'); 
					document.getElementById('generationTest_acceptUser_name').value=rec.get('name');" /> 
			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="generationTest_acceptUser_name"  value="<s:property value="generationTest.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="generationTest_acceptUser" name="generationTest.acceptUser.id"  value="<s:property value="generationTest.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			
               	 
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="generationTest_note"
                   	 name="generationTest.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="generationTest.note"/>"
                   	  />
                   	  
                   	</td>
			
			
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="generationTestDetailJson" id="generationTestDetailJson" value="" />
            <input type="hidden" name="generationTestStepJson" id="generationTestStepJson" value="" />
            <input type="hidden" name="generationTestItemJson" id="generationTestItemJson" value="" />
            <input type="hidden" name="generationTestParticuarsJson" id="generationTestParticuarsJson" value="" />
            <input type="hidden" name="generationTestResultJson" id="generationTestResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="generationTest.id"/>" />
            </form>
			<div id="generationTestDetailpage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<!-- <li><a href="#generationTestDetailpage">测序样本</a></li> -->
			<li><a href="#generationTestSteppage"><fmt:message key="biolims.common.executionStep"/></a></li>
			<li><a href="#generationTestItempage"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#generationTestParticuarspage"><fmt:message key="biolims.common.splitCos"/></a></li>
			<!-- <li><a href="#generationTestResultpage">测序结果</a></li> -->
           	</ul> 
			<div id="generationTestSteppage" width="100%" height:10px></div>
			<div id="generationTestItempage" width="100%" height:10px></div>
			<div id="generationTestParticuarspage" width="100%" height:10px></div>
			</div>
			<div id="generationTestResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
