	<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=cfdnaTask&id=${cfdnaTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/cfdna/cfdnaTaskEdit.js"></script>
<div style="float:left;width:25%" id="cfdnaTaskTempPage">
		<!-- <div id="tabs1">
            <ul>
	            <li><a href="#linchuang">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="linchuang" style="width: 100%;height: 10px;"></div>
				<div id="keji" style="width: 100%;height: 10px;"></div>
		 </div> -->

</div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_id"
                   	 name="cfdnaTask.id" title="编号" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="cfdnaTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_name"
                   	 name="cfdnaTask.name" title="描述"
                   	   
	value="<s:property value="cfdnaTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			 <td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="cfdnaTask_reciveUser_name"  value="<s:property value="cfdnaTask.reciveUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="cfdnaTask_reciveUser" name="cfdnaTask.reciveUser.id"  value="<s:property value="cfdnaTask.reciveUser.id"/>" > 
 						<img alt='请选择实验员' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
			
<%-- 			<g:LayOutWinTag buttonId="showreciveUser" title="选择实验员" --%>
<%-- 				hasHtmlFrame="true" --%>
<%-- 				html="${ctx}/core/user/userSelect.action" --%>
<%-- 				isHasSubmit="false" functionName="UserFun1"  --%>
<%--  				hasSetFun="true" --%>
<%-- 				documentId="cfdnaTask_reciveUser" --%>
<%-- 				documentName="cfdnaTask_reciveUser_name" /> --%>
				
			
			
<!--                	 	<td class="label-title" >实验员</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<%--  						<input type="text" size="20" readonly="readOnly"  id="cfdnaTask_reciveUser_name"  value="<s:property value="cfdnaTask.reciveUser.name"/>"  readonly="readOnly"  /> --%>
<%--  						<input type="hidden" id="cfdnaTask_reciveUser" name="cfdnaTask.reciveUser.id"  value="<s:property value="cfdnaTask.reciveUser.id"/>" >  --%>
<%--  						<img alt='选择实验员' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
<!--                    	</td> -->
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >实验时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="cfdnaTask_reciveDate"
                   	 name="cfdnaTask.reciveDate" title="实验时间" 
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="cfdnaTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun2" 
 				hasSetFun="true"
				documentId="cfdnaTask_createUser"
				documentName="cfdnaTask_createUser_name" />
				
			
			
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="cfdnaTask_createUser_name"  value="<s:property value="cfdnaTask.createUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="cfdnaTask_createUser" name="cfdnaTask.createUser.id"  value="<s:property value="cfdnaTask.createUser.id"/>" > 
 						<%-- <img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />     --%>               		
                   	</td>
			
			
               	 	<td class="label-title" >下达时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="cfdnaTask_createDate"
                   	 name="cfdnaTask.createDate" title="下达时间"
                   	   readonly = "readOnly"    value="<s:date name="cfdnaTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
                   	
				
			</tr>
			<tr>
			
			<td class="label-title" >完成时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="cfdnaTask_confirmDate"
                   	 name="cfdnaTask.confirmDate" title="下达时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:date name="cfdnaTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
			
			
               	 	<td class="label-title" >模板</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="cfdnaTask_template_name"  value="<s:property value="cfdnaTask.template.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="cfdnaTask_template" name="cfdnaTask.template.id"  value="<s:property value="cfdnaTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"     />                   		
                   	</td>
                   	
                   	
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('cfdnaTask_acceptUser').value=rec.get('id');
				document.getElementById('cfdnaTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >实验组</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="cfdnaTask_acceptUser_name"  value="<s:property value="cfdnaTask.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="cfdnaTask_acceptUser" name="cfdnaTask.acceptUser.id"  value="<s:property value="cfdnaTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
				
               	 	<%-- <td class="label-title" >index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_indexa"
                   	 name="cfdnaTask.indexa" title="index"
                   	   
	value="<s:property value="cfdnaTask.indexa"/>"
                   	  />
                   	  
                   	</td>--%>
			
			
               	 	<td class="label-title" style="display: none">状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none"></td>            	 	
                   	<td align="left"  style="display: none">
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_state"
                   	 name="cfdnaTask.state" title="状态" style="display: none"
                   	   
	value="<s:property value="cfdnaTask.state"/>"
                   	  />
                   	  
                   	</td> 
			</tr>
			<tr>
			<td class="label-title" >状态名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_stateName"
                   	 name="cfdnaTask.stateName" title="状态名称" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="cfdnaTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			
               	 	
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_note"
                   	 name="cfdnaTask.note" title="备注"
                   	   
	value="<s:property value="cfdnaTask.note"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			</tr>
			<%-- <tr>
			
			
               	 	<td class="label-title" >容器数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_maxNum"
                   	 name="cfdnaTask.maxNum" title="容器数量"
                   	   
	value="<s:property value="cfdnaTask.maxNum"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >质控品数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="cfdnaTask_qcNum"
                   	 name="cfdnaTask.qcNum" title="质控品数量"
                   	   
	value="<s:property value="cfdnaTask.qcNum"/>"
                   	  />
                   	  
                   	</td>
			</tr> --%>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="cfdnaTaskItemJson" id="cfdnaTaskItemJson" value="" />
            <input type="hidden" name="cfdnaTaskTemplateJson" id="cfdnaTaskTemplateJson" value="" />
            <input type="hidden" name="cfdnaTaskReagentJson" id="cfdnaTaskReagentJson" value="" />
            <input type="hidden" name="cfdnaTaskCosJson" id="cfdnaTaskCosJson" value="" />
            <input type="hidden" name="cfdnaTaskResultJson" id="cfdnaTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="cfdnaTask.id"/>" />
            </form>
            <div id="cfdnaTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#cfdnaTaskTemplatepage">执行步骤</a></li>
			<li><a href="#cfdnaTaskReagentpage">原辅料明细</a></li>
			<li><a href="#cfdnaTaskCospage">设备明细</a></li>
           	</ul> 
			
			<div id="cfdnaTaskTemplatepage" width="100%" height:10px></div>
			<div id="cfdnaTaskReagentpage" width="100%" height:10px></div>
			<div id="cfdnaTaskCospage" width="100%" height:10px></div>
			
			</div>
			<div id="cfdnaTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
