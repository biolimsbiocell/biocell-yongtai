﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
 <!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.attachment"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=qtTaskReceive&id=HSJJ1712210002&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qt/qtTaskReceiveEdit.js"></script>
<div style="float:left;width:25%" id="QtTaskReceviceLeftPage"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="qtTaskReceive_id"
                   	 name="qtTaskReceive.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	 readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="qtTaskReceive.id"/>"
                   	  />                  	  
                   	</td>                  	  
                   	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="qtTaskReceive_name"
                   	 name="qtTaskReceive.name" title="<fmt:message key="biolims.common.describe"/>"                   	   
	value="<s:property value="qtTaskReceive.name"/>"
                   	  />                  	  
                   	</td>  
                   	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"   >
 						<input type="text" size="20" readonly="readOnly"  id="qtTaskReceive_receiveUser_name"  value="<s:property value="qtTaskReceive.receiveUser.name"/>" class="text input readonlytrue" readonly="readOnly" />
 						<input type="hidden" id="qtTaskReceive_receiveUser" name="qtTaskReceive.receiveUser.id"  value="<s:property value="qtTaskReceive.receiveUser.id"/>" > 
                   	</td>	             	 	
			</tr>
			<tr>						
               	 	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                   	  
                   	  	<input type="text" size="20" maxlength="25" id="qtTaskReceive_receiverDate"
                   	 name="qtTaskReceive.receiverDate" title="<fmt:message key="biolims.common.receivingDate"/>"
                   	  value="<s:property value="qtTaskReceive.receiverDate"/>" readonly = "readOnly" class="text input readonlytrue"  
                   	  />
                   	</td>						
               	 			               	 	
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="20" maxlength="25" id="qtTaskReceive_state"
                   	 name="qtTaskReceive.state" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="qtTaskReceive.state"/>"
                   	  />
                   	<input type="text" size="20" maxlength="25" id="qtTaskReceive_stateName"
                   	 name="qtTaskReceive.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="qtTaskReceive.stateName"/>"
                   	  />
                   	  <td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>   
			</tr>
            </table>
            <input type="hidden" name="qtTaskReceiveItemJson" id="qtTaskReceiveItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="qtTaskReceive.id"/>" />
            </form>
            <div id="tabs">
			<div id="qtTaskReceiveItempage" width="100%" height:10px></div>
			</div>
        	</div>
</body>
</html>
