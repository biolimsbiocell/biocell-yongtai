﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/qt/qtTaskItem.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
	<div id="qtTaskItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_item_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.sampleDosage"/>（μg）</span></td>
				<td><input id="Consume"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_item1_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>请选择移动的位置：</span></td>
				<td><input id="local"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>请选择排序方式：</span></td>
				<td><select id="order1"  style="width:100">
    					<option value="0" >横向</option>
    					<option value="1" >竖向</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_productNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.dicSampleNum"/></span></td>
				<td><input id="productNum"/></td>
			</tr>
		</table>
		</div>
	<div id="many_batItem_div" style="display: none">
		<div class="ui-widget;">
			<div id="bat_position_format_div" class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_batItem_text" style="width:650px;height: 339px"></textarea>
	</div>
	<div id="bat_counts_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.boardNum"/></span></td>
				<td><input id="counts"/></td>
			</tr>
		</table>
	</div>
</body>
</html>


