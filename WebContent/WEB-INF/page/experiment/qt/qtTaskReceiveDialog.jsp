﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qt/qtTaskReceiveDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
 <!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="qtTaskReceive_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.sampleCode"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_Code" searchField="true" name="Code"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.recipient"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_receiveUser" searchField="true" name="receiveUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.sampleType"/></td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="qtTaskReceive_Type" searchField="true" name="Type"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.receivingDate"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_receiverDate" searchField="true" name="receiverDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.sampleState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_Condition" searchField="true" name="Condition"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.resultsDetermine"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_method" searchField="true" name="method"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.storageLocationHints"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_location" searchField="true" name="location"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.theNextStepTo"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_nextFlow" searchField="true" name="nextFlow"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.workflowState"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.processingOpinion"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_advice" searchField="true" name="advice"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.state"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.affirmToPerform"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_isExecute" searchField="true" name="isExecute"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.feedbackTime"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_feedbackTime" searchField="true" name="feedbackTime"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.note"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.qpcrExperimentOfCode"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="qtTaskReceive_qtTaskCode" searchField="true" name="qtTaskCode"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>		
		<div id="show_dialog_qtTaskReceive_div"></div>  		
</body>
</html>



