﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/qt/qtTaskTemp.js"></script>
  <script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
 <!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
<input type="hidden" id="id" value="${requestScope.id}">
	<div id="qtTaskTempdiv"></div>
	<div id="many_batTemp_div" style="display: none">
		<div class="ui-widget;">
			<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_batTemp_text" style="width:650px;height: 339px"></textarea>
	</div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	
		<div id="bat_js_div" style="display: none">
	<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
	<form id="searchForm">
		<table class="frame-table">
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.sampleCode"/></span></td>
				<td><input type="text" size="20" maxlength="25" id="sampleCode"
                   	 name="sampleCode" searchField="true" title=""    /></td>
			</tr>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.taskNumber"/></span></td>
				<td><input type="text" size="20" maxlength="25" id="techJkServiceTask_id"
                   	 name="techJkServiceTask.id" searchField="true" title=""    /></td>
			</tr>
		</table>
	</form>
		</div>
</body>
</html>


