﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.attachment"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=qtTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qt/qtTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<div style="float:left;width:25%" id="qtTaskTempPage">	
</div>
  	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<input type="hidden" name="qtTask_method" id="qtTask_method" value="<%=request.getParameter("method") %>" />
			<input type="hidden" size="40" id="qtTask_orders" name='qtTask.orders' value="<s:property value="qtTask.orders"/>" />
			<table class="frame-table">
			<tr>						
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="hidden" size="40" id="type"  value="qtTask" />
                   	<input type="hidden" size="40" id="kong"  value="" />
                   	<input type="text" size="20" maxlength="18" id="qtTask_id"
                   	 name="qtTask.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	readonly = "readOnly" class="text input readonlytrue"   
	value="<s:property value="qtTask.id"/>"
                   	  />                   	  
                   	</td>						
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="30" maxlength="60" id="qtTask_maxNum" name="qtTask.maxNum" title="容器数量"
							value="<s:property value="qtTask.maxNum"/>"
                   	  />
                   	<input type="text" size="20" maxlength="60" id="qtTask_name"
                   	 name="qtTask.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="qtTask.name"/>"
                   	  />                   	  
                   	</td>	
                  <td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
			        <td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="40"  style="width: 152px" readonly="readOnly"  id="qtTask_template_name"  value="<s:property value="qtTask.template.name"/>" />
 						<input type="hidden"  readonly="readOnly" id="qtTask_template" name="qtTask.template.id"  value="<s:property value="qtTask.template.id"/>" > 
 						<input type="hidden" readonly="readOnly" id="qtTask_template_templateFieldsCode" name="qtTask.template.templateFieldsCode"  value="<s:property value="qtTask.template.templateFieldsCode"/>" >
 						<input type="hidden" readonly="readOnly" id="qtTask_template_templateFieldsItemCode" name="qtTask.template.templateFieldsItemCode"  value="<s:property value="qtTask.template.templateFieldsItemCode"/>" >
 						
 						<img alt='<fmt:message key="biolims.common.chooseExperimentalTemplate"/>' id='showTemplateFun' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()" />                   		
                   	</td>			
               	 								
			
			<tr>	
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function CreateUserFun(){
var win = Ext.getCmp('CreateUserFun');
if (win) {win.close();}
var CreateUserFun= new Ext.Window({
id:'CreateUserFun',modal:true,title:'<fmt:message key="biolims.common.chooseFromPeople"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=CreateUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 CreateUserFun.close(); }  }]  });     CreateUserFun.show(); }
);
});
 function setCreateUserFun(rec){
document.getElementById('qtTask_createUser').value=rec.get('id');
				document.getElementById('qtTask_createUser_name').value=rec.get('name');
var win = Ext.getCmp('CreateUserFun')
if(win){win.close();}
}
</script>
										
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" style="width:152px" readonly="readOnly"  id="qtTask_createUser_name"  value="<s:property value="qtTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="qtTask_createUser" name="qtTask.createUser.id"  value="<s:property value="qtTask.createUser.id"/>" > 
                   	</td>
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                   	  
                   	  	<input type="text" size="15" maxlength="" style="width: 152px"  id="qtTask_createDate"
                   	 name="qtTask.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:property value="qtTask.createDate"/>"                  	     
                   	  />
                   	</td>                   	
                   		<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                  	  
                   	  	<input type="text" size="20" maxlength="" style="width:152px"  id="qtTask_confirmDate"
                   	 name="qtTask.confirmDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="qtTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>			
						
			</tr>
			<tr>
			  		
               		 </tr>	
               		 <tr>
               		  <script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showAcceptUser = Ext.get('showAcceptUser');
showAcceptUser.on('click', 
 function loadAcceptUser(){
var win = Ext.getCmp('loadAcceptUser');
if (win) {win.close();}
var loadAcceptUser= new Ext.Window({
id:'loadAcceptUser',modal:true,title:'<fmt:message key="biolims.common.commandTime"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=loadAcceptUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 loadAcceptUser.close(); }  }]  });     loadAcceptUser.show(); }
);
});
 function setloadAcceptUser(rec){
document.getElementById('qtTask_acceptUser').value=rec.get('id'); 
					document.getElementById('qtTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('loadAcceptUser')
if(win){win.close();}
}
</script>
		
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly" style="width: 152px"  id="qtTask_acceptUser_name"  value="<s:property value="qtTask.acceptUser.name"/>" class="text input" readonly="readOnly"  />
 						<input type="hidden" id="qtTask_acceptUser" name="qtTask.acceptUser.id"  value="<s:property value="qtTask.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>             
               		
               		<td class="label-title" style="display: none;"><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none;"></td>            	 	
                   	<td align="left"  style="display: none;">
 						<input type="text" size="20" readonly="readOnly"  id="qtTask_testUser_name"  value="<s:property value="qtTask.testUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="qtTask_testUser" name="qtTask.testUser.id"  value="<s:property value="qtTask.testUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
					<td class="label-title" ><fmt:message key="biolims.common.chooseTester1"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qtTask_testUserOneName" name="qtTask.testUserOneName" value="<s:property value="qtTask.testUserOneName"/>" readonly="readOnly"  />
 						<input type="hidden" id="qtTask_testUserOneId" name="qtTask.testUserOneId"  value="<s:property value="qtTask.testUserOneId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(1);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
               		<td class="label-title" ><fmt:message key="biolims.wk.confirmUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
 						<input type="text" size="20" readonly="readOnly"  id="qtTask_confirmUser_name"  value="<s:property value="qtTask.confirmUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="qtTask_confirmUser" name="qtTask.confirmUser.id"  value="<s:property value="qtTask.confirmUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(3);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
			</tr>
               		 <tr>
               		
			<%-- <td class="label-title" >第二个实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qtTask_testUserTwoName" name="qtTask.testUserTwoName"    value="<s:property value="qtTask.testUserTwoName"/>" readonly="readOnly"  />
 						<input type="hidden" id="qtTask_testUserTwoId" name="qtTask.testUserTwoId"  value="<s:property value="qtTask.testUserTwoId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(2);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td> --%>
               		
                   	<input type="hidden" id="qtTask_state"      	 name="qtTask.state" value="<s:property value="qtTask.state"/>"    />                  	  
			<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="qtTask_stateName"
                   	 name="qtTask.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   class="text input readonlytrue" readonly="readOnly"  
	value="<s:property value="qtTask.stateName"/>"
                   	  />
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			
               		 </tr>		
            </table>
            <input type="hidden" name="qtTaskItemJson" id="qtTaskItemJson" value="" />
            <input type="hidden" name="qtTaskResultJson" id="qtTaskResultJson" value="" />
            <input type="hidden" name="qtTaskTemplateItemJson" id="qtTaskTemplateItemJson" value="" />
            <input type="hidden" name="qtTaskTemplateReagentJson" id="qtTaskTemplateReagentJson" value="" />
            <input type="hidden" name="qtTaskTemplateCosJson" id="qtTaskTemplateCosJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="qtTask.id"/>" />
            </form>
			<div id="qtTaskItempage" width="100%" height:10px></div>
			<div id = '<%="3d_image0" %>'></div>
			
			<div id="tabs">
            <ul>
			<li><a href="#qtTaskTemplateItempage"><fmt:message key="biolims.common.splitTemplate"/></a></li>
			<li><a href="#qtTaskTemplateReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#qtTaskTemplateCospage" onClick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul>
           	<div id="qtTaskTemplateItempage" width="100%" height:10px></div>
           	<div id="qtTaskTemplateReagentpage" width="100%" height:10px></div>
           	<div id="qtTaskTemplateCospage" width="100%" height:10px></div> 
           	</div>
			<div id="qtTaskResultpage" width="100%" height:10px></div>
			<%-- <div id = '<%="3d_image1" %>'></div> --%>
			</div>
	</body>
	</html>
