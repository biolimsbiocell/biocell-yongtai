<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=wkndTask&id=${wkndTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/sj/wknd/wkndTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="wkndTaskTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
                <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wkndTask_id"
                   	 name="wkndTask.id" title="编号"
                   	   
					value="<s:property value="wkndTask.id"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="wkndTask_name"
                   	 name="wkndTask.name" title="描述"
                   	   
					value="<s:property value="wkndTask.name"/>"
                   	  />
                   	  
                   	</td>
			
                   	
                   		<td class="label-title" >实验时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wkndTask.reciveDate"
                   	 name="wkndTask.reciveDate" title="实验时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="wkndTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			<td class="label-title" >实验模板</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkndTask_template_name"  value="<s:property value="wkndTask.template.name"/>"  />
 						<input type="hidden" id="wkndTask_template" name="wkndTask.template.id"  value="<s:property value="wkndTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"   />                   		
                   	</td>
			
			
			
			
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wkndTask_acceptUser').value=rec.get('id');
				document.getElementById('wkndTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >实验组</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkndTask_acceptUser_name"  value="<s:property value="wkndTask.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wkndTask_acceptUser" name="wkndTask.acceptUser.id"  value="<s:property value="wkndTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			 <td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkndTask_testUser_name"  value="<s:property value="wkndTask.testUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wkndTask_testUser" name="wkndTask.testUser.id"  value="<s:property value="wkndTask.testUser.id"/>" > 
 						<img alt='请选择实验组' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
			
			
			
		
			</tr>
			<tr>
			<td class="label-title" >下达时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wkndTask.createDate"
                   	 name="wkndTask.createDate" title="下达时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="wkndTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wkndTask_confirmUser').value=rec.get('id');
				document.getElementById('wkndTask_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkndTask_confirmUser_name"  value="<s:property value="wkndTask.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wkndTask_confirmUser" name="wkndTask.confirmUser.id"  value="<s:property value="wkndTask.confirmUser.id"/>" > 
<%--  						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
			
					<td class="label-title" >完成时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wkndTask.confirmDate"
                   	 name="wkndTask.confirmDate" title="完成时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="wkndTask.confirmDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	
			</tr>
			<tr>
			
			
               	 	
				<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wkndTask_createUser').value=rec.get('id');
				document.getElementById('wkndTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkndTask_createUser_name"  value="<s:property value="wkndTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wkndTask_createUser" name="wkndTask.createUser.id"  value="<s:property value="wkndTask.createUser.id"/>" > 
<%--  						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wkndTask_stateName"
                   	 name="wkndTask.stateName" title="状态名称"
                   	   
	value="<s:property value="wkndTask.stateName"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
                   	<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
							
			
			
			</tr>
			<tr>
			
							<td class="label-title" style="display:none">状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="25" id="wkndTask_state"
                   	 name="wkndTask.state" title="状态"
                   	   
	value="<s:property value="wkndTask.state"/>"
                   	  />
                   	  
                   	</td>
                   	
               	 	<td class="label-title"  style="display:none"  >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="wkndTask_note"
                   	 name="wkndTask.note" title="备注"
                   	   
	value="<s:property value="wkndTask.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			
			
            </table>
            <input type="hidden" name="wkndTaskItemJson" id="wkndTaskItemJson" value="" />
            <input type="hidden" name="wkndTaskTemplateJson" id="wkndTaskTemplateJson" value="" />
            <input type="hidden" name="wkndTaskReagentJson" id="wkndTaskReagentJson" value="" />
            <input type="hidden" name="wkndTaskCosJson" id="wkndTaskCosJson" value="" />
            <input type="hidden" name="wkndTaskResultJson" id="wkndTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wkndTask.id"/>" />
            </form>
            <div id="wkndTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#wkndTaskTemplatepage">执行步骤</a></li>
			<li><a href="#wkndTaskReagentpage">原辅料明细</a></li>
			<li><a href="#wkndTaskCospage">设备明细</a></li>
           	</ul> 
			<div id="wkndTaskTemplatepage" width="100%" height:10px></div>
			<div id="wkndTaskReagentpage" width="100%" height:10px></div>
			<div id="wkndTaskCospage" width="100%" height:10px></div>
			
			</div>
			<div id="wkndTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
