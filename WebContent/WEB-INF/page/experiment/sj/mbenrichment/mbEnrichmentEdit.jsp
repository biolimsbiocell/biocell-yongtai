<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=mbEnrichment&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/sj/mbenrichment/mbEnrichmentEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="mbEnrichmentTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" name="mbEnrichment_method" id="mbEnrichment_method" value="<%=request.getParameter("method") %>" />
           	<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
           	 <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" > <fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mbEnrichment_id"
                   	 name="mbEnrichment.id" title=""  readonly = "readOnly" class="text input readonlytrue"
                   	   
	value="<s:property value="mbEnrichment.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="mbEnrichment_name"
                   	 name="mbEnrichment.name" title=""
                   	   
	value="<s:property value="mbEnrichment.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.testTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mbEnrichment_reciveDate"
                   	 name="mbEnrichment.reciveDate" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="mbEnrichment.reciveDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
	<%-- 		<g:LayOutWinTag buttonId="showtemplate" title="选择模板"
				hasHtmlFrame="true"
				html="${ctx}/experiment/sj/mbenrichment/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('mbEnrichment_template').value=rec.get('id');
				document.getElementById('mbEnrichment_template_name').value=rec.get('name');" /> --%>
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.templateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="mbEnrichment_template_name"  value="<s:property value="mbEnrichment.template.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="mbEnrichment_template" name="mbEnrichment.template.id"  value="<s:property value="mbEnrichment.template.id"/>" > 
 						<input type="hidden" readonly="readOnly" id="mbEnrichment_template_templateFieldsCode" name="mbEnrichment.template.templateFieldsCode"  value="<s:property value="mbEnrichment.template.templateFieldsCode"/>" >
 						<input type="hidden" readonly="readOnly" id="mbEnrichment_template_templateFieldsItemCode" name="mbEnrichment.template.templateFieldsItemCode"  value="<s:property value="mbEnrichment.template.templateFieldsItemCode"/>" >
 
 						<img alt='' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="TemplateFun()"  />                   		
                   	</td>
			
			
			
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showacceptUser = Ext.get('showacceptUser');
showacceptUser.on('click', 
 function UserGroupFun(){
var win = Ext.getCmp('UserGroupFun');
if (win) {win.close();}
var UserGroupFun= new Ext.Window({
id:'UserGroupFun',modal:true,title:'<fmt:message key="biolims.common.selectGroup"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=UserGroupFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserGroupFun.close(); }  }]  });     UserGroupFun.show(); }
);
});
 function setUserGroupFun(rec){
document.getElementById('mbEnrichment_acceptUser').value=rec.get('id');
				document.getElementById('mbEnrichment_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('UserGroupFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="mbEnrichment_acceptUser_name"  value="<s:property value="mbEnrichment.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="mbEnrichment_acceptUser" name="mbEnrichment.acceptUser.id"  value="<s:property value="mbEnrichment.acceptUser.id"/>" > 
 						<img alt='' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/sj/mbenrichment/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('mbEnrichment_createUser').value=rec.get('id');
				document.getElementById('mbEnrichment_createUser_name').value=rec.get('name');" /> --%>
				
			
			
               	 	
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.firstLaboratorian"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="mbEnrichment_testUserOneName" name="mbEnrichment.testUserOneName" value="<s:property value="mbEnrichment.testUserOneName"/>" readonly="readOnly"  />
 						<input type="hidden" id="mbEnrichment_testUserOneId" name="mbEnrichment.testUserOneId"  value="<s:property value="mbEnrichment.testUserOneId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(1);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
					
			
			
			
			<%-- <g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/sj/mbenrichment/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('mbEnrichment_confirmUser').value=rec.get('id');
				document.getElementById('mbEnrichment_confirmUser_name').value=rec.get('name');" /> --%>
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.wk.confirmUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="mbEnrichment_confirmUser_name"  value="<s:property value="mbEnrichment.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="mbEnrichment_confirmUser" name="mbEnrichment.confirmUser.id"  value="<s:property value="mbEnrichment.confirmUser.id"/>" > 
 						<img alt='' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' onclick="testUser(3);"	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.confirmDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mbEnrichment_confirmDate"
                   	 name="mbEnrichment.confirmDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="mbEnrichment.confirmDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
					<td class="label-title" ><fmt:message key="biolims.sample.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="mbEnrichment_createUser_name"  value="<s:property value="mbEnrichment.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="mbEnrichment_createUser" name="mbEnrichment.createUser.id"  value="<s:property value="mbEnrichment.createUser.id"/>" > 
 						<%-- <img alt='' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />   --%>                 		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.sample.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="mbEnrichment_createDate"
                   	 name="mbEnrichment.createDate" title="<fmt:message key="biolims.common.experimentalTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="mbEnrichment.createDate" format="yyyy-MM-dd"/>" 
                   	  />
                   
                   	  
                   	</td>
               	 	<td class="label-title" style="display:none"><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="25" id="mbEnrichment_state"
                   	 name="mbEnrichment.state" title=""
                   	   
	value="<s:property value="mbEnrichment.state"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.stateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="mbEnrichment_stateName"
                   	 name="mbEnrichment.stateName" title="" readonly = "readOnly" class="text input readonlytrue"
                   	   
	value="<s:property value="mbEnrichment.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="mbEnrichment_note"
                   	 name="mbEnrichment.note" title=""
                   	   
	value="<s:property value="mbEnrichment.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title=<fmt:message key="biolims.common.afterthepreservation"/> id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="mbEnrichmentItemJson" id="mbEnrichmentItemJson" value="" />
            <input type="hidden" name="mbEnrichmentTemplateJson" id="mbEnrichmentTemplateJson" value="" />
            <input type="hidden" name="mbEnrichmentReagentJson" id="mbEnrichmentReagentJson" value="" />
            <input type="hidden" name="mbEnrichmentCosJson" id="mbEnrichmentCosJson" value="" />
            <input type="hidden" name="mbEnrichmentResultJson" id="mbEnrichmentResultJson" value="" />
           <!--  <input type="hidden" name="mbEnrichmentTempJson" id="mbEnrichmentTempJson" value="" /> -->
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="mbEnrichment.id"/>" />
            </form>
             <div id="mbEnrichmentItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<!-- <li><a href="#mbEnrichmentItempage">模板富集明细</a></li> -->
			<li><a href="#mbEnrichmentTemplatepage"><fmt:message key="biolims.common.executionStep"/> </a></li>
			<li><a href="#mbEnrichmentReagentpage" onclick="showReagent()"><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#mbEnrichmentCospage"  onclick="showCos()"><fmt:message key="biolims.common.instrumentDetail"/></a></li>
			<!-- <li><a href="#mbEnrichmentResultpage">模板富集结果</a></li> -->
			<!-- <li><a href="#mbEnrichmentTemppage">模板富集临时表</a></li> -->
           	</ul> 
			<!-- <div id="mbEnrichmentItempage" width="100%" height:10px></div> -->
			<div id="mbEnrichmentTemplatepage" width="100%" height:10px></div>
			<div id="mbEnrichmentReagentpage"  width="100%" height:10px></div>
			<div id="mbEnrichmentCospage" width="100%" height:10px></div>
			
			<!-- <div id="mbEnrichmentTemppage" width="100%" height:10px></div> -->
			</div>
			<div id="mbEnrichmentResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
