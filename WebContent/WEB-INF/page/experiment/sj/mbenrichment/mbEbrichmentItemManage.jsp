﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
 <script type="text/javascript" src="${ctx}/js/experiment/sj/mbenrichment/mbEnrichmentItemManage.js"></script>
 <script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.wk.wkCode"/>
			</label>
			</td>	
			<td>
			<input type="text"  name="fjwk" id="dnaGetItemManager_code" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.originalSampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="dnaGetItemManager_sampleCode" searchField="true" >
			</td>
			<td>
			<input type="button" onClick="selectInfo()" value="<fmt:message key="biolims.common.find"/>">
			</td>
			</tr>
		</table>
		<div id="dnaGetItemManagerdiv"></div>
		<div id="bat_result1_div" style="display: none">
		<table>
				<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="result1" style="width:100">
                		<option value="" <s:if test="result1==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="result1==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0" <s:if test="result1==0">selected="selected" </s:if>><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_next1_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td>
                <td><select id="nextFlow1"  style="width:100">
                		<option value="" <s:if test="nextFlow1==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0" <s:if test="nextFlow1==0">selected="selected" </s:if>><fmt:message key="biolims.common.libraryBuilding"/></option>
    					<option value="1" <s:if test="nextFlow1==1">selected="selected" </s:if>><fmt:message key="biolims.common.repeatExtract"/></option>
    					<option value="2" <s:if test="nextFlow1==2">selected="selected" </s:if>><fmt:message key="biolims.common.putInStorage"/></option>
    					<!-- <option value="3" <s:if test="nextFlow1==3">selected="selected" </s:if>>反馈至项目组</option> -->
    					<option value="3" <s:if test="nextFlow1==3">selected="selected" </s:if>><fmt:message key="biolims.common.termination"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>



