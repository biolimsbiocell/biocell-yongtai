﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/experiment/sj/mbenrichment/mbEnrichment.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString">
		<form id="searchForm">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message key="biolims.user.itemNo" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="mbEnrichment_id" name="id" searchField="true" title="编号" /></td>
					<td class="label-title"><fmt:message key="biolims.common.name" /></td>
					<td align="left"><input type="text" size="20" maxlength="50"
						id="mbEnrichment_name" name="name" searchField="true" title="描述" />






					</td>

				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.testTime" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startreciveDate" name="startreciveDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="reciveDate1" name="reciveDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endreciveDate" name="endreciveDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="reciveDate2" name="reciveDate##@@##2"
						searchField="true" /></td>

					<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun1(){
var win = Ext.getCmp('UserFun1');
if (win) {win.close();}
var UserFun1= new Ext.Window({
id:'UserFun1',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun1' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun1.close(); }  }]  });     UserFun1.show(); }
);
});
 function setUserFun1(id,name){
 document.getElementById("mbEnrichment_createUser").value = id;
document.getElementById("mbEnrichment_createUser_name").value = name;
var win = Ext.getCmp('UserFun1')
if(win){win.close();}
}
</script>

					<td class="label-title"><fmt:message
							key="biolims.common.createUserName" /></td>
					<td align="left"><input type="text" size="20"
						id="mbEnrichment_createUser_name" searchField="true"
						name="createUser.name" value="" class="text input" /> <input
						type="hidden" id="mbEnrichment_createUser"
						name="mbEnrichment.createUser.id" value=""> <img
						alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showtemplate = Ext.get('showtemplate');
showtemplate.on('click', 
 function TemplateFun(){
var win = Ext.getCmp('TemplateFun');
if (win) {win.close();}
var TemplateFun= new Ext.Window({
id:'TemplateFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/system/template/template/templateSelect.action?flag=TemplateFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 TemplateFun.close(); }  }]  });     TemplateFun.show(); }
);
});
 function setTemplateFun(rec){
document.getElementById('mbEnrichment_template').value=rec.get('id');
				document.getElementById('mbEnrichment_template_name').value=rec.get('name');
var win = Ext.getCmp('TemplateFun')
if(win){win.close();}
}
</script>

					<td class="label-title"><fmt:message
							key="biolims.common.templateName" /></td>
					<td align="left"><input type="text" size="20"
						id="mbEnrichment_template_name" searchField="true"
						name="template.name" value="" class="text input" /> <input
						type="hidden" id="mbEnrichment_template"
						name="mbEnrichment.template.id" value=""> <img alt='选择模板'
						id='showtemplate' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showacceptUser = Ext.get('showacceptUser');
showacceptUser.on('click', 
 function UserGroupFun(){
var win = Ext.getCmp('UserGroupFun');
if (win) {win.close();}
var UserGroupFun= new Ext.Window({
id:'UserGroupFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=UserGroupFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserGroupFun.close(); }  }]  });     UserGroupFun.show(); }
);
});
 function setUserGroupFun(rec){
document.getElementById('mbEnrichment_acceptUser').value=rec.get('id');
				document.getElementById('mbEnrichment_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('UserGroupFun')
if(win){win.close();}
}
</script>

					<td class="label-title"><fmt:message
							key="biolims.common.acceptUserName" /></td>
					<td align="left"><input type="text" size="20"
						id="mbEnrichment_acceptUser_name" searchField="true"
						name="acceptUser.name" value="" class="text input" /> <input
						type="hidden" id="mbEnrichment_acceptUser"
						name="mbEnrichment.acceptUser.id" value=""> <img
						alt='' id='showacceptUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>

				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.createDate" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startcreateDate" name="startcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate1" name="createDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endcreateDate" name="endcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate2" name="createDate##@@##2"
						searchField="true" /></td>
					<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showconfirmUser = Ext.get('showconfirmUser');
showconfirmUser.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(id,name){
 document.getElementById("mbEnrichment_confirmUser").value = id;
document.getElementById("mbEnrichment_confirmUser_name").value = name;
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>

					<td class="label-title"><fmt:message
							key="biolims.wk.confirmUserName" /></td>
					<td align="left"><input type="text" size="20"
						id="mbEnrichment_confirmUser_name" searchField="true"
						name="confirmUser.name" value="" class="text input" /> <input
						type="hidden" id="mbEnrichment_confirmUser"
						name="mbEnrichment.confirmUser.id" value=""> <img
						alt='' id='showconfirmUser'
						src='${ctx}/images/img_lookup.gif' class='detail' /></td>
					<!--  
					<td class="label-title"><fmt:message
							key="biolims.common.confirmDate" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startconfirmDate" name="startconfirmDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="confirmDate1" name="confirmDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endconfirmDate" name="endconfirmDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="confirmDate2" name="confirmDate##@@##2"
						searchField="true" /></td>
						-->
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.state" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="mbEnrichment_state" name="state" searchField="true" title="" />



					</td>
					<td class="label-title"><fmt:message
							key="biolims.common.stateName" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="mbEnrichment_stateName" name="stateName" searchField="true"
						title="" /></td>
					<td class="label-title" style="display: none"><fmt:message
							key="biolims.common.note" /></td>
					<td align="left" style="display: none"><input type="text"
						size="50" maxlength="50" id="mbEnrichment_note" name="note"
						searchField="true" title="" style="display: none" /></td>
				</tr>
			</table>
		</form>
	</div>
	<div id="show_mbEnrichment_div"></div>
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value='' />
	</form>
	<div id="show_mbEnrichment_tree_page"></div>
</body>
</html>



