﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/sj/wklifeblend/wkLifeBlendTaskResult.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	<div id="wkBlendTaskResultdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
		<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td>
                <td><select id="nextFlow" style="width:100">
<!--                 		<option value="" <s:if test="nextFlow==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="nextFlow==1">selected="selected" </s:if>><fmt:message key="biolims.common.putInStorage"/></option>
    					<option value="2" <s:if test="nextFlow==2">selected="selected" </s:if>><fmt:message key="biolims.common.libraryOfQuantitative"/></option>
    					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>><fmt:message key="biolims.common.computer"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	</div>
</body>
</html>
