<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.attachment"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=wkBlendTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/sj/wklifeblend/wkLifeBlendTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="wkBlendTaskTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			   <input type="hidden" name="wkBlendTask_method" id="wkBlendTask_method" value="<%=request.getParameter("method") %>" />
			<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="wkBlendTask_id"
                   	 name="wkBlendTask.id" title="<fmt:message key="biolims.common.serialNumber"/>"
					value="<s:property value="wkBlendTask.id"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wkBlendTask_name"
                   	 name="wkBlendTask.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="wkBlendTask.name"/>"
                   	  />
                   	</td>
				<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wkBlendTask.reciveDate"
                   	 name="wkBlendTask.reciveDate" title="<fmt:message key="biolims.common.experimentalTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="wkBlendTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	  />
                   	</td>
			</tr>
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_template_name"  value="<s:property value="wkBlendTask.template.name"/>"  />
 						<input type="hidden" id="wkBlendTask_template" name="wkBlendTask.template.id"  value="<s:property value="wkBlendTask.template.id"/>" > 
 						<input type="hidden" readonly="readOnly" id="wkBlendTask_template_templateFieldsCode" name="wkBlendTask.template.templateFieldsCode"  value="<s:property value="wkBlendTask.template.templateFieldsCode"/>" >
 						<input type="hidden" readonly="readOnly" id="wkBlendTask_template_templateFieldsItemCode" name="wkBlendTask.template.templateFieldsItemCode"  value="<s:property value="wkBlendTask.template.templateFieldsItemCode"/>" >
 						<img alt='<fmt:message key="biolims.common.chooseExperimentalTemplate"/>' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"   />                   		
                   	</td>
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showacceptUser = Ext.get('showacceptUser');
showacceptUser.on('click', 
 function UserGroupFun(){
var win = Ext.getCmp('UserGroupFun');
if (win) {win.close();}
var UserGroupFun= new Ext.Window({
id:'UserGroupFun',modal:true,title:'<fmt:message key="biolims.common.selectGroup"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=UserGroupFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserGroupFun.close(); }  }]  });     UserGroupFun.show(); }
);
});
 function setUserGroupFun(rec){
document.getElementById('wkBlendTask_acceptUser').value=rec.get('id');
				document.getElementById('wkBlendTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('UserGroupFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_acceptUser_name"  value="<s:property value="wkBlendTask.acceptUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="wkBlendTask_acceptUser" name="wkBlendTask.acceptUser.id"  value="<s:property value="wkBlendTask.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			 <td class="label-title" style="display: none;"><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none;"></td>            	 	
                   	<td align="left"  style="display: none;">
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_testUser_name"  value="<s:property value="wkBlendTask.testUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wkBlendTask_testUser" name="wkBlendTask.testUser.id"  value="<s:property value="wkBlendTask.testUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.chooseTester1"/> </td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_testUserOneName" name="wkBlendTask.testUserOneName" value="<s:property value="wkBlendTask.testUserOneName"/>" readonly="readOnly"  />
 						<input type="hidden" id="wkBlendTask_testUserOneId" name="wkBlendTask.testUserOneId"  value="<s:property value="wkBlendTask.testUserOneId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(1);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
               		<td class="label-title" ><fmt:message key="biolims.common.chooseTester2"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_testUserTwoName" name="wkBlendTask.testUserTwoName"    value="<s:property value="wkBlendTask.testUserTwoName"/>" readonly="readOnly"  />
 						<input type="hidden" id="wkBlendTask_testUserTwoId" name="wkBlendTask.testUserTwoId"  value="<s:property value="wkBlendTask.testUserTwoId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(2);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
               		<td class="label-title" ><fmt:message key="biolims.wk.confirmUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" >
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_confirmUser_name" name="wkBlendTask.confirmUser.name"  value="<s:property value="wkBlendTask.confirmUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wkBlendTask_confirmUser" name="wkBlendTask.confirmUser.id"  value="<s:property value="wkBlendTask.confirmUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(3);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
			</tr>
			<tr>
			
			<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wkBlendTask.createDate"
                   	 name="wkBlendTask.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="wkBlendTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td>
			
			
			
			
		<%--  	<g:LayOutWinTag buttonId="showconfirmUser" title='<fmt:message key="biolims.common.selectTheAuditor"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wkBlendTask_confirmUser').value=rec.get('id');
				document.getElementById('wkBlendTask_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_confirmUser_name"  value="<s:property value="wkBlendTask.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wkBlendTask_confirmUser" name="wkBlendTask.confirmUser.id"  value="<s:property value="wkBlendTask.confirmUser.id"/>" > 
						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			 --%>
			<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wkBlendTask.confirmDate"
                   	 name="wkBlendTask.confirmDate" title="<fmt:message key="biolims.common.completionTime"/>"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="wkBlendTask.confirmDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			</tr>
			<tr>
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'<fmt:message key="biolims.common.chooseFromPeople"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(rec){
document.getElementById('wkBlendTask_createUser').value=rec.get('id');
				document.getElementById('wkBlendTask_createUser_name').value=rec.get('name');
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>


				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkBlendTask_createUser_name"  value="<s:property value="wkBlendTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wkBlendTask_createUser" name="wkBlendTask.createUser.id"  value="<s:property value="wkBlendTask.createUser.id"/>" > 
<%--  						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wkBlendTask_stateName"
                   	 name="wkBlendTask.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   
						value="<s:property value="wkBlendTask.stateName"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
                   	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			<tr>
			
               	 	<td class="label-title"style="display: none;" ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" style="display: none;" >
                   	<input type="hidden" size="20" maxlength="25" id="wkBlendTask_state"
                   	 name="wkBlendTask.state" title="<fmt:message key="biolims.common.state"/>"
                   	   
					value="<s:property value="wkBlendTask.state"/>"
                   	  />
                   	  
                   	</td>
                   	
                   	
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="wkBlendTask_note"
                   	 name="wkBlendTask.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="wkBlendTask.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			
            </table>
            <input type="hidden" name="wkBlendTaskItemJson" id="wkBlendTaskItemJson" value="" />
            <input type="hidden" name="wkBlendTaskTemplateJson" id="wkBlendTaskTemplateJson" value="" />
            <input type="hidden" name="wkBlendTaskReagentJson" id="wkBlendTaskReagentJson" value="" />
            <input type="hidden" name="wkBlendTaskCosJson" id="wkBlendTaskCosJson" value="" />
            <input type="hidden" name="wkBlendTaskResultJson" id="wkBlendTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wkBlendTask.id"/>" />
            </form>
            <div id="wkBlendTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#wkBlendTaskTemplatepage" ><fmt:message key="biolims.common.performStepsInDetail"/></a></li>
			<li><a href="#wkBlendTaskReagentpage" onclick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#wkBlendTaskCospage" onclick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
			<div id="wkBlendTaskTemplatepage" width="100%" height:10px></div>
			<div id="wkBlendTaskReagentpage" width="100%" height:10px></div>
			<div id="wkBlendTaskCospage" width="100%" height:10px></div>
			
			</div>
			<div id="wkBlendTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
