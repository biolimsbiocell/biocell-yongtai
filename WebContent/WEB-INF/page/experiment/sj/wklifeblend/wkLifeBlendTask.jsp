﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript"
	src="${ctx}/js/experiment/sj/wklifeblend/wkLifeBlendTask.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString">
		<form id="searchForm">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="wkBlendTask_id" name="id" searchField="true"
						title="<fmt:message key="biolims.common.serialNumber"/>" /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.describe" /></td>
					<td align="left"><input type="text" size="20" maxlength="50"
						id="wkBlendTask_name" name="name" searchField="true"
						title="<fmt:message key="biolims.common.describe"/>" /></td>

				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.testTime" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startreciveDate" name="startreciveDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="reciveDate1" name="reciveDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endreciveDate" name="endreciveDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="reciveDate2" name="reciveDate##@@##2"
						searchField="true" /></td>


					<td class="label-title"><fmt:message
							key="biolims.common.stateName" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="wkBlendTask_stateName" name="stateName" searchField="true"
						title="<fmt:message key="biolims.common.stateName"/>" /></td>
					<td class="label-title" style="display: none"><fmt:message
							key="biolims.common.note" /></td>
					<td align="left" style="display: none"><input type="text"
						size="50" maxlength="50" id="wkBlendTask_note" name="note"
						searchField="true"
						title="<fmt:message key="biolims.common.note"/>"
						style="display: none" /></td>
				</tr>
				<tr>
					<!--  
					<g:LayOutWinTag buttonId="showtemplate"
						title='<fmt:message key="biolims.common.selectATemplate"/>'
						hasHtmlFrame="true"
						html="${ctx}/experiment/sj/wkblend/templateSelect.action"
						isHasSubmit="false" functionName="TemplateFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('wkBlendTask_template').value=rec.get('id');
				document.getElementById('wkBlendTask_template_name').value=rec.get('name');" />
					<td class="label-title"><fmt:message
							key="biolims.common.template" /></td>
					<td align="left"><input type="text" size="20"
						id="wkBlendTask_template_name" searchField="true"
						name="template.name" value="" class="text input" /> <input
						type="hidden" id="wkBlendTask_template"
						name="wkBlendTask.template.id" value=""> <img
						alt='<fmt:message key="biolims.common.selectATemplate"/>'
						id='showtemplate' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
						-->
					<g:LayOutWinTag buttonId="showacceptUser"
						title='<fmt:message key="biolims.common.selectGroup"/>'
						hasHtmlFrame="true"
						html="${ctx}/experiment/sj/wkblend/userGroupSelect.action"
						isHasSubmit="false" functionName="UserGroupFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('wkBlendTask_acceptUser').value=rec.get('id');
				document.getElementById('wkBlendTask_acceptUser_name').value=rec.get('name');" />
					<td class="label-title"><fmt:message
							key="biolims.common.experimentalGroup" /></td>
					<td align="left"><input type="text" size="20"
						id="wkBlendTask_acceptUser_name" searchField="true"
						name="acceptUser.name" value="" class="text input" /> <input
						type="hidden" id="wkBlendTask_acceptUser"
						name="wkBlendTask.acceptUser.id" value=""> <img
						alt='<fmt:message key="biolims.common.selectGroup"/>'
						id='showacceptUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<g:LayOutWinTag buttonId="showcreateUser" title=''
						hasHtmlFrame="true"
						html="${ctx}/experiment/sj/wkblend/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('wkBlendTask_createUser').value=rec.get('id');
				document.getElementById('wkBlendTask_createUser_name').value=rec.get('name');" />
					<td class="label-title"><fmt:message
							key="biolims.common.commandPerson" /></td>
					<td align="left"><input type="text" size="20"
						id="wkBlendTask_createUser_name" searchField="true"
						name="createUser.name" value="" class="text input" /> <input
						type="hidden" id="wkBlendTask_createUser"
						name="wkBlendTask.createUser.id" value=""> <img
						alt='<fmt:message key="biolims.common.chooseFromPeople"/>'
						id='showcreateUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.commandTime" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startcreateDate" name="startcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate1" name="createDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endcreateDate" name="endcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate2" name="createDate##@@##2"
						searchField="true" /></td>
					<g:LayOutWinTag buttonId="showconfirmUser" title=''
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						documentId="wkBlendTask_confirmUser"
						documentName="wkBlendTask_confirmUser_name" />
					<td class="label-title"><fmt:message
							key="biolims.common.auditor" /></td>
					<td align="left"><input type="text" size="20"
						id="wkBlendTask_confirmUser_name" searchField="true"
						name="confirmUser.name" value="" class="text input" /> <input
						type="hidden" id="wkBlendTask_confirmUser"
						name="wkBlendTask.confirmUser.id" value=""> <img
						alt='<fmt:message key="biolims.common.selectTheAuditor"/>'
						id='showconfirmUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
					<!-- 
					<td class="label-title"><fmt:message
							key="biolims.common.completionTime" /></td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startconfirmDate" name="startconfirmDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="confirmDate1" name="confirmDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endconfirmDate" name="endconfirmDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="confirmDate2" name="confirmDate##@@##2"
						searchField="true" /></td>
						 -->
				</tr>
				<tr>
					<!-- 
					<td class="label-title"><fmt:message
							key="biolims.common.state" /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="wkBlendTask_state" name="state" searchField="true"
						title="<fmt:message key="biolims.common.state"/>" /></td>
						
						 -->

				</tr>
			</table>
		</form>
	</div>
	<div id="show_wkBlendTask_div"></div>
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value='' />
	</form>
	<div id="show_wkBlendTask_tree_page"></div>
</body>
</html>



