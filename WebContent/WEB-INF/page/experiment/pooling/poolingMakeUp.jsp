<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}
</style>
</head>

<body style="height: 94%">
	<div class="content-wrapper" id="content" style="margin-left: 0px;">
		<input type="hidden" id="pooling_name"
			value="<s:property value="pooling.name"/>"> <input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.test.placeSample" />
							<small style="color: #fff;"> <fmt:message
									key="biolims.common.taskId" />: <span id="pooling_id"><s:property
										value="pooling.id" /></span> <fmt:message
									key="biolims.storageIn.createUser" />: <span
								userId="<s:property value="pooling.createUser.id"/>"
								id="pooling_createUser"><s:property
										value="pooling.createUser.name" /></span> <fmt:message
									key="biolims.storageIn.createDate" />: <span
								id="pooling_createDate"><s:property
										value="pooling.createDate" /></span> <fmt:message
									key="biolims.storageIn.state" />: <span
								state="<s:property value="pooling.state"/>" id="pooling_state"><s:property
										value="pooling.stateName" /></span>
							</small>
						</h3>
					</div>
					<div class="box-body">
						<!--排板前样本-->
						<div class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
										<fmt:message key="biolims.test.2plateSample" />
									</h3>
								</div>
								<div class="box-body">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="poolingMakeUpdiv" style="font-size: 14px;"></table>
									<input type="hidden" id="pooling_template_isSeparate"
										value="<s:property value="pooling.template.isSeparate"/>">

								</div>
							</div>
						</div>
						<!--排板后样本-->
						<div class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">
										<fmt:message key="biolims.test.plateSample" />
									</h3>
									<div class="pull-right">
										<input type="hidden" id="isBlend"
											value="<s:property value="pooling.template.isBlend"/>" />
										<button class="btn btn-info" id="isBlendBtn"
											onclick="compoundSample()"
											style="position: fixed; right: 50px; z-index: 1000;"><fmt:message key="biolims.test.mixSample" /></button>
									</div>
								</div>
								<div class="box-body">
									<input type="hidden" id="blendCode" value="" />
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="poolingMakeUpAfdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-xs-12" id="plateModal"
							style="display: none">
							<input type="hidden" id="maxNum"
								value="<s:property value=" pooling.maxNum "/>"> <input
								type="hidden" id="temRow"
								value="<s:property value=" pooling.template.storageContainer.rowNum "/>">
							<input type="hidden" id="temCol"
								value="<s:property value=" pooling.template.storageContainer.colNum "/>">
							<div class="col-xs-12 plateDiv">
								<div class="box box-primary">
									<div class="box-header with-border">
										<i class="glyphicon glyphicon-leaf"></i>
										<h3 class="box-title"><fmt:message key="biolims.common.porePlate" /></h3>
										<div class="box-tools pull-right">
											<button type="button" class="btn btn-sm btn-info active"
												order="h">
												<i class="glyphicon glyphicon-resize-horizontal"></i> <fmt:message key="biolims.test.horizontal" />
											</button>
											<button type="button" class="btn btn-sm btn-info" order="z">
												<i class="glyphicon glyphicon-resize-vertical"></i> <fmt:message key="biolims.test.vertical" />
											</button>
										</div>
									</div>

									<div class="box-body">
										<div plateNum="p1" id="plateNum">
											<table class="table table-bordered  plate"></table>
										</div>

									</div>
								</div>
							</div>

						</div>

					</div>
					<div class="box-footer">
						<div class="pull-right">
							<button type="button" class="btn btn-primary"
								onclick="savePoolingMakeUpAfTab()" id="save">
								<i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.save" />
							</button>
							<button type="button" class="btn btn-primary" onclick="tjsp()"
								id="tjsp">
								<i class="glyphicon glyphicon-random"></i> <fmt:message key="biolims.common.submit" />
							</button>
							<button type="button" class="btn btn-primary" id="prev">
								<i class="glyphicon glyphicon-arrow-up"></i> <fmt:message key="biolims.common.back" />
							</button>
							<button type="button" class="btn btn-primary" id="next">
								<i class="glyphicon glyphicon-arrow-down"></i> <fmt:message key="biolims.common.nextStep" />
							</button>
						</div>

					</div>
				</div>
			</div>

		</div>
		</section>
	</div>
	<div style="display: none" id="batch_data">
		<div  class="input-group">
		<span class="input-group-addon bg-aqua"><fmt:message
				key="biolims.common.productNum" /></span> <input type="number"
			id="productNum" class="form-control" placeholder="" value="">
		</div>
		
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/pooling/poolingMakeUp.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/pooling/poolingMakeUpAf.js"></script>
</body>

</html>