﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/producer/producerTaskResult.js"></script>
</head>
<body>
<form name='excelfrm' action='${ctx}/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	<div id="producerTaskResultdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="bat_slideCode_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.slideNumber"/></span>
				<input type="text" id="slideCode"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="result" style="width:100">
    					<option value="1"><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0"><fmt:message key="biolims.common.disqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_submit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.whetherToCommit"/></span></td>
                <td><select id="submit"  style="width:100">
    					<option value="1"><fmt:message key="biolims.common.yes"/></option>
    					<option value="0"><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>
