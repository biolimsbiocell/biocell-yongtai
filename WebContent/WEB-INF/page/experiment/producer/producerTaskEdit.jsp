<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=producerTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/producer/producerTaskEdit.js"></script>
<div style="float:left;width:25%" id="producerTaskTempPage">
		<!-- <div id="tabs1">
            <ul>
	            <li><a href="#linchuang">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="linchuang" style="width: 100%;height: 10px;"></div>
				<div id="keji" style="width: 100%;height: 10px;"></div>
		 </div> -->

</div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="producerTask_id"
                   	 name="producerTask.id" title="编号" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="producerTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="producerTask_name"
                   	 name="producerTask.name" title="描述"
                   	   
	value="<s:property value="producerTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
				<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun2(){
var win = Ext.getCmp('UserFun2');
if (win) {win.close();}
var UserFun2= new Ext.Window({
id:'UserFun2',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=UserFun2' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun2.close(); }  }]  });     UserFun2.show(); }
);
});
 function setUserFun2(id,name){
 document.getElementById("producerTask_createUser").value = id;
document.getElementById("producerTask_createUser_name").value = name;
var win = Ext.getCmp('UserFun2')
if(win){win.close();}
}
</script>


				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="producerTask_createUser_name"  value="<s:property value="producerTask.createUser.name"/>" class="text input readonlytrue"   />
 						<input type="hidden" id="producerTask_createUser" name="producerTask.createUser.id"  value="<s:property value="producerTask.createUser.id"/>" > 
 						<%-- <img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>              		
                   	</td>
			
			<%-- <g:LayOutWinTag buttonId="showreciveUser" title="选择实验员"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun1" 
 				hasSetFun="true"
				documentId="producerTask_reciveUser"
				documentName="producerTask_reciveUser_name" />
				
			
			
               	 	<td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="producerTask_reciveUser_name"  value="<s:property value="producerTask.reciveUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="producerTask_reciveUser" name="producerTask.reciveUser.id"  value="<s:property value="producerTask.reciveUser.id"/>" > 
 						<img alt='选择实验员' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			</tr>
			<tr>
			
					<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="producerTask_createDate"
                   	 name="producerTask.createDate" title="下达时间"
                   	   class="text input readonlytrue" readonly="readOnly" 
                   	      value="<s:date name="producerTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.testTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="producerTask_reciveDate"
                   	 name="producerTask.reciveDate" title="实验时间"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="producerTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
				<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showacceptUser = Ext.get('showacceptUser');
showacceptUser.on('click', 
 function UserGroupFun(){
var win = Ext.getCmp('UserGroupFun');
if (win) {win.close();}
var UserGroupFun= new Ext.Window({
id:'UserGroupFun',modal:true,title:'Selection of experimental group',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=UserGroupFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserGroupFun.close(); }  }]  });     UserGroupFun.show(); }
);
});
 function setUserGroupFun(rec){
document.getElementById('producerTask_acceptUser').value=rec.get('id');
				document.getElementById('producerTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('UserGroupFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="producerTask_acceptUser_name"  value="<s:property value="producerTask.acceptUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="producerTask_acceptUser" name="producerTask.acceptUser.id"  value="<s:property value="producerTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="producerTask_template_name"  value="<s:property value="producerTask.template.name"/>"/>
 						<input type="hidden" id="producerTask_template" name="producerTask.template.id"  value="<s:property value="producerTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"     />                   		
                   	</td>
                   	
					<td class="label-title" ><fmt:message key="biolims.common.confirmDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="producerTask_confirmDate"
                   	 name="producerTask.confirmDate" title="完成时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:property  value="producerTask.confirmDate"/>"/>
                   	</td>
                   	
               	 <%-- 	<td class="label-title" >index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="producerTask_indexa"
                   	 name="producerTask.indexa" title="index"
                   	   
	value="<s:property value="producerTask.indexa"/>"
                   	  />
                   	  
                   	</td> --%>
			
			
               	 	<td class="label-title" style="display: none">状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display: none"></td>            	 	
                   	<td align="left"   style="display: none">
                   	<input type="text" size="20" maxlength="25" id="producerTask_state"
                   	 name="producerTask.state" title="状态"
                   	    style="display: none" class="text input readonlytrue" readonly="readOnly"
	value="<s:property value="producerTask.state"/>"
                   	  />
                   	  
                   	</td>

			<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="producerTask_stateName"
                   	 name="producerTask.stateName" title="状态名称" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="producerTask.stateName"/>"
                   	  />
                   	  
                   	</td>
               	 	
			</tr>
			<tr>
						
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="65" id="producerTask_note"
                   	 name="producerTask.note" title="备注"
                   	   
	value="<s:property value="producerTask.note"/>"
                   	  />
                   	  
                   	</td>
			
			<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}
							<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="producerTaskItemJson" id="producerTaskItemJson" value="" />
            <input type="hidden" name="producerTaskTemplateJson" id="producerTaskTemplateJson" value="" />
            <input type="hidden" name="producerTaskReagentJson" id="producerTaskReagentJson" value="" />
            <input type="hidden" name="producerTaskCosJson" id="producerTaskCosJson" value="" />
            <input type="hidden" name="producerTaskResultJson" id="producerTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="producerTask.id"/>" />
            </form>
            <div id="producerTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#producerTaskTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#producerTaskReagentpage"><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#producerTaskCospage"><fmt:message key="biolims.common.instrumentDetail"/></a></li>
           	</ul> 
			
			<div id="producerTaskTemplatepage" width="100%" height:10px></div>
			<div id="producerTaskReagentpage" width="100%" height:10px></div>
			<div id="producerTaskCospage" width="100%" height:10px></div>
			
			</div>
			<div id="producerTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
