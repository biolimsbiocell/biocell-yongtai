﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/producer/producerTaskDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="producerTask_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">实验员</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_reciveUser" searchField="true" name="reciveUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">实验时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_reciveDate" searchField="true" name="reciveDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">下达人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">下达时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">模板</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_template" searchField="true" name="template"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">index</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_indexa" searchField="true" name="indexa"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态名称</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="producerTask_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">实验组</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_acceptUser" searchField="true" name="acceptUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">容器数量</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_maxNum" searchField="true" name="maxNum"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">质控品数量</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="producerTask_qcNum" searchField="true" name="qcNum"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_producerTask_div"></div>
   		
</body>
</html>



