﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=outTaskReceive&id=${outTaskReceive.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/out/outTaskReceiveEdit.js"></script>
<div style="float:left;width:25%" id="OutTaskReceviceLeftPage"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="outTaskReceive_id"
                   	 name="outTaskReceive.id" title="编号"
                   	 readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="outTaskReceive.id"/>"
                   	  />                  	  
                   	</td>                  	  
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="outTaskReceive_name"
                   	 name="outTaskReceive.name" title="描述"                   	   
	value="<s:property value="outTaskReceive.name"/>"
                   	  />                  	  
                   	</td>               	 	
			</tr>
			<tr>						
               	 	<td class="label-title" >接收时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                   	  
                   	  	<input type="text" size="20" maxlength="25" id="outTaskReceive_receiverDate"
                   	 name="outTaskReceive.receiverDate" title="接收时间"
                   	  value="<s:property value="outTaskReceive.receiverDate"/>" readonly = "readOnly" class="text input readonlytrue"  
                   	  />
                   	</td>						
               	 	<td class="label-title" >接收人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"   >
 						<input type="text" size="20" readonly="readOnly"  id="outTaskReceive_receiveUser_name"  value="<s:property value="outTaskReceive.receiveUser.name"/>" class="text input readonlytrue" readonly="readOnly" />
 						<input type="hidden" id="outTaskReceive_receiveUser" name="outTaskReceive.receiveUser.id"  value="<s:property value="outTaskReceive.receiveUser.id"/>" > 
                   	</td>			               	 	
               	 	<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="outTaskReceive_stateName"
                   	 name="outTaskReceive.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="outTaskReceive.stateName"/>"
                   	  />
			</tr>
			<tr>						
					<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>               	 	
			</tr>						
            </table>
            <input type="hidden" name="outTaskReceiveItemJson" id="outTaskReceiveItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="outTaskReceive.id"/>" />
            </form>
            <div id="tabs">
			<div id="outTaskReceiveItempage" width="100%" height:10px></div>
			</div>
        	</div>
</body>
</html>
