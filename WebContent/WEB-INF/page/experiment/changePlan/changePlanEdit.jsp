<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}

#yesSpan {
	display: none;
}

#auditDiv {
	display: none;
}

#btn_submit {
	display: none;
}

.sampleNumberInput {
	width: 250px;
	height: 34px;
	border-radius: 6px;
	border: 1px solid #ccc;
	/* box-shadow: 1px 1px 1px #888888; */
	position: absolute;
	left: 200px;
	top: 212px;
}

.sampleNumberInputTow {
	width: 250px;
	height: 34px;
	border-radius: 6px;
	border: 1px solid #ccc;
	/* box-shadow: 1px 1px 1px #888888; */
	position: absolute;
	left: 510px;
	top: 212px;
}

.sampleNumberDiv {
	position: absolute;
	left: 140px;
	top: 223px;
}

.sampleNumberDivtow {
	position: absolute;
	left: 466px;
	top: 223px;
}

.sampleNumberbutton {
	position: absolute;
	left: 760px;
	top: 212px;
	border-radius: 4px;
	border: 1px solid #ccc;
	background-color: #5cb85c;
	width: 38px;
	height: 36px;
}
</style>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">计划调整</h3>
					<small style="color: #fff;"> 创建人: <span
						id="changePlan_createUser"><s:property
								value=" changePlan.createUser.name" /></span> 创建时间: <span
						id="changePlan_createDate"><s:date
								name="changePlan.createDate" format="yyyy-MM-dd" /></span> 状态: <span
						id="changePlan_state"><s:property
								value="changePlan.stateName" /></span>
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="createUser_id"
								name="changePlan.createUser.id"
								value="<s:property value=" changePlan.createUser.id "/>" /> <input
								type="hidden" id="createDate" name="changePlan.createDate"
								value="<s:property value=" changePlan.createDate "/>" /> <input
								type="hidden" id="state" name="changePlan.state"
								value="<s:property value=" changePlan.state "/>" /> <input
								type="hidden" id="stateName" name="changePlan.stateName"
								value="<s:property value=" changePlan.stateName "/>" /> <input
								type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
							<br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 计划调整单号 </span> <input
											list="sampleIdOne"
											changelog="<s:property value="changePlan.id"/>" type="text"
											size="20" maxlength="25" id="changePlan_id"
											name="changePlan.id" class="form-control" title="计划调整单号"
											value="<s:property value=" changePlan.id "/>" /> <span
											class="input-group-btn"> </span>
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 描述 </span> <input
											list="sampleName"
											changelog="<s:property value="changePlan.name"/>" type="text"
											size="20" maxlength="50" id="changePlan_name"
											name="changePlan.name" class="form-control" title="描述"
											value="<s:property value=" changePlan.name "/>" />

									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">计划调整类型</span> <input
											changelog="<s:property value="changePlan.changeType.name"/>"
											type="text" size="20" maxlength="25"
											id="changePlan_changeType_name"
											name="changePlan.changeType.name" title="选择计划调整类型"
											class="form-control"
											value="<s:property value="changePlan.changeType.name"/>"
											readonly="readonly" /> <input
											changelog="<s:property value="changePlan.changeType.id"/>"
											type="hidden" size="20" maxlength="25"
											id="changePlan_changeType_id" name="changePlan.changeType.id"
											title="选择计划调整类型" class="form-control"
											value="<s:property value="changePlan.changeType.id"/>"
											readonly="readonly" /> <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onclick="changeType()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">医事服务部人员<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text" size="20" readonly="readOnly"
											id="changePlan_ysComfirmUser_name"
											changelog="<s:property value="changePlan.ysComfirmUser.name"/>"
											value="<s:property value="changePlan.ysComfirmUser.name"/>"
											class="form-control" /> <input type="hidden"
											id="changePlan_ysComfirmUser_id"
											name="changePlan.ysComfirmUser.id"
											value="<s:property value="changePlan.ysComfirmUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showYsComfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">QA审核人<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text" size="20" readonly="readOnly"
											id="changePlan_qaConfirmUser_name"
											changelog="<s:property value="changePlan.qaConfirmUser.name"/>"
											value="<s:property value="changePlan.qaConfirmUser.name"/>"
											class="form-control" /> <input type="hidden"
											id="changePlan_qaConfirmUser_id"
											name="changePlan.qaConfirmUser.id"
											value="<s:property value="changePlan.qaConfirmUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showqaConfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">QC审核人<img
											class="requiredimage" src="/images/required.gif"></span> <input
											size="50" maxlength="100" type="hidden"
											id="changePlan_qcConfirmUser_id"
											name="changePlan.qcConfirmUser.id"
											changelog="<s:property value="changePlan.qcConfirmUser.id"/>"
											class="form-control"
											value="<s:property value="changePlan.qcConfirmUser.id"/>" />
										<input type="text" readonly="readonly" size="50"
											maxlength="100" id="changePlan_qcConfirmUser_name"
											changelog="<s:property value="changePlan.qcConfirmUser.name"/>"
											class="form-control"
											value="<s:property value="changePlan.qcConfirmUser.name"/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showqcConfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<%-- <div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									<span class="input-group-addon">监控人</span> 
										<input type="text" size="20" readonly="readOnly"
												id="changePlan_monitor_name"  changelog="<s:property value="changePlan.monitor.name"/>"
												value="<s:property value="changePlan.monitor.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="changePlan_monitor_id" name="changePlan.monitor.id"
												value="<s:property value="changePlan.monitor.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showmonitor()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
							</div> --%>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">生产审核人<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text" size="20" readonly="readOnly"
											id="changePlan_productConfirmUser_name"
											changelog="<s:property value="changePlan.productConfirmUser.name"/>"
											value="<s:property value="changePlan.productConfirmUser.name"/>"
											class="form-control" /> <input type="hidden"
											id="changePlan_productConfirmUser_id"
											name="changePlan.productConfirmUser.id"
											value="<s:property value="changePlan.productConfirmUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showproductConfirmUser()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">批准人<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text" size="20" readonly="readOnly"
											id="changePlan_approverUser_name"
											changelog="<s:property value="changePlan.approverUser.name"/>"
											value="<s:property value="changePlan.approverUser.name"/>"
											class="form-control" /> <input type="hidden"
											id="changePlan_approverUser_id"
											name="changePlan.approverUser.id"
											value="<s:property value="changePlan.approverUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showApproverUser()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4" id="rwdh">
									<div class="input-group">
										<span class="input-group-addon">计划调整任务单单号<img
											class="requiredimage" src="/images/required.gif"></span> <input
											type="text" size="20" id="changePlan_changeTaskId"
											name="changePlan.changeTaskId"
											changelog="<s:property value="changePlan.changeTaskId"/>"
											value="<s:property value="changePlan.changeTaskId"/>"
											class="form-control" />
										<%-- <span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showmonitor()">
											<i class="glyphicon glyphicon-search"></i>
										</span> --%>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4" id="cpph">
									<div class="input-group">
										<span class="input-group-addon">产品批号</span> <input type="text"
											size="20" id="changePlan_batch" name="changePlan.batch"
											changelog="<s:property value="changePlan.batch"/>"
											value="<s:property value="changePlan.batch"/>"
											class="form-control" />
										<%-- <span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showmonitor()">
											<i class="glyphicon glyphicon-search"></i>
										</span> --%>
									</div>
								</div>
							</div>
							<div class="row"  id="zt">
							<div class="col-xs-4">
								<div class="input-group">
									<span class="input-group-addon">状态</span> <select
										class="form-control" lay-ignore="" id="changePlan_sampleState" name="changePlan.sampleState">
										<option value="0"
											<s:if test="changePlan.sampleState==0">selected="selected"</s:if>>在组</option>
										<option value="1"
											<s:if test="changePlan.sampleState==1">selected="selected"</s:if>>剔除</option>
										<option value="2"
											<s:if test="changePlan.sampleState==2">selected="selected"</s:if>>脱落</option>
										<option value="3"
											<s:if test="changePlan.sampleState==3">selected="selected"</s:if>>复发</option>
										<option value="4"
											<s:if test="changePlan.sampleState==4">selected="selected"</s:if>>死亡</option>
										<option value="5"
											<s:if test="changePlan.sampleState==5">selected="selected"</s:if>>其它</option>
									</select>
								</div>
							</div>
							<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 日期 <img
											class='requiredimage' src='/images/required.gif' /></span> <input
											type="text" changelog="" size="20" maxlength="25"
											id="changePlan_dateRiQi"
											name="changePlan.dateRiQi"
											class="form-control" title="日期 " value="<s:property value="changePlan.dateRiQi"/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4" id="hssj">
									<div class="input-group">
										<span class="input-group-addon"> 计划调整后时间 <img
											class='requiredimage' src='/images/required.gif' /></span> <input
											type="text" changelog="" size="20" maxlength="25"
											id="changePlan_reinfusionPlanDateUpdate"
											name="changePlan.reinfusionPlanDateUpdate"
											class="form-control planDate" title="计划调整后时间 " value="<s:property value="changePlan.reinfusionPlanDateUpdate"/>" />
									</div>
								</div>
								<div class="col-xs-4" id="sxh">
									<div class="input-group">
										<span class="input-group-addon">筛选号<img class="requiredimage" src="/images/required.gif"></span> <input type="text"
											size="20" id="changePlan_filtrateCode" name="changePlan.filtrateCode"
											changelog="<s:property value="changePlan.filtrateCode"/>"
											value="<s:property value="changePlan.filtrateCode"/>"
											class="form-control" />
									</div>
								</div>
								<%-- <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">QC审核意见</span> <input type="text"
											size="20" id="changePlan_qcAdvice" name="changePlan.qcAdvice"
											changelog="<s:property value="changePlan.qcAdvice"/>"
											value="<s:property value="changePlan.qcAdvice"/>"
											class="form-control" />
									</div>
								</div> --%>
								</div>
							<%-- <div class="row" id="hssa">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">生产审核意见</span> <input type="text"
											size="20" id="changePlan_scAdvice" name="changePlan.scAdvice"
											changelog="<s:property value="changePlan.scAdvice"/>"
											value="<s:property value="changePlan.scAdvice"/>"
											class="form-control" />
									</div>
								</div> 
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">QA审核意见</span> <input type="text"
											size="20" id="changePlan_qaAdvice" name="changePlan.qaAdvice"
											changelog="<s:property value="changePlan.qaAdvice"/>"
											value="<s:property value="changePlan.qaAdvice"/>"
											class="form-control" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">批准人审核意见</span> <input type="text"
											size="20" id="changePlan_approverUserAdvice" name="changePlan.approverUserAdvice"
											changelog="<s:property value="changePlan.approverUserAdvice"/>"
											value="<s:property value="changePlan.approverUserAdvice"/>"
											class="form-control" />
									</div>
								</div>
								
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 回输修改时间<img
											class='requiredimage' src='/images/required.gif' /> </span> <input
											type="text" changelog="" size="20" maxlength="25"
											id="changePlan_reinfusionPlanDateUpdate" name="changePlan.reinfusionPlanDateUpdate" class="form-control"
											title="回输修改时间"  value="<s:date name="changePlan.reinfusionPlanDateUpdate" format="yyyy-mm-dd hh:ii"/>" />
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
									<span class="input-group-addon">批次号</span> 
										<input type="text" size="20"
												id="changePlan_batch" name="changePlan.batch" changelog="<s:property value="changePlan.batch"/>"
												value="<s:property value="changePlan.batch"/>"
												class="form-control" /> 
									</div>
								</div>
							</div> --%>
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">计划调整内容描述</span>
										<textarea id="changePlan_changeContent"
											name="changePlan.changeContent" placeholder="长度限制2000个字符"
											class="form-control"
											style="overflow: hidden; width: 65%; height: 80px;"><s:property
												value="changePlan.changeContent" /></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<button type="button" class="btn btn-info" onclick="fileUp()">上传附件</button>
									<span class="text "><fmt:message
											key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /></span>

									<button type="button" class="btn btn-info" onclick="fileView()">查看附件</button>
								</div>
							</div>

							<input type="hidden" id="id_parent_hidden"
								value="<s:property value=" changePlan.id "/>" /> <input
								type="hidden" id="changeLog" name="changeLog" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/changePlan/changePlanEdit.js"></script>
</body>
</html>