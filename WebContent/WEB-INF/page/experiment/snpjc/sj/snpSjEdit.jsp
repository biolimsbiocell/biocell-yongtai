<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=snpSj&id=${snpSj.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/snpjc/sj/snpSjEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpSj_id"
                   	 name="snpSj.id" title="编号"  class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="snpSj.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="snpSj_name"
                   	 name="snpSj.name" title="描述"
                   	   
	value="<s:property value="snpSj.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/snpjc/sj/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpSj_createUser').value=rec.get('id');
				document.getElementById('snpSj_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="snpSj_createUser_name"  value="<s:property value="snpSj.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="snpSj_createUser" name="snpSj.createUser.id"  value="<s:property value="snpSj.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpSj_createDate"
                   	 name="snpSj.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="snpSj.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.sequencing.sequencingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="snpSj_sjDate"
                   	 name="snpSj.sjDate" title="上机日期"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="snpSj.sjDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>

					<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
					hasHtmlFrame="true"
					html="${ctx}/core/userGroup/userGroupSelect.action"
					isHasSubmit="false" functionName="UserGroupFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('snpSj_acceptUser').value=rec.get('id');
					document.getElementById('snpSj_acceptUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="snpSj_acceptUser_name"  value="<s:property value="snpSj.acceptUser.name"/>"   />
 						<input type="hidden" id="snpSj_acceptUser" name="snpSj.acceptUser.id"  value="<s:property value="snpSj.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>			
					<%-- <g:LayOutWinTag buttonId="showsnpCross" title="选择snp杂交编号"
					hasHtmlFrame="true"
					html="${ctx}/experiment/snpjc/sj/snpCrossSelect.action"
					isHasSubmit="false" functionName="SnpCrossFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('snpSj_snpCross').value=rec.get('id');
					document.getElementById('snpSj_snpCross_name').value=rec.get('name');" />
					 --%>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.hybridizationTask"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="snpSj_snpCrossId" name="snpSj.snpCrossId"  value="<s:property value="snpSj.snpCrossId"/>"  />
 						<input type="text" id="snpSj_snpCrossName" name="snpSj.snpCrossName"  value="<s:property value="snpSj.snpCrossName"/>" > 
 						<img alt='选择snp杂交编号' id='showsnpCross' src='${ctx}/images/img_lookup.gif' class='detail'  onclick="ProducerTaskFun()"  />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="snpSj_note"
                   	 name="snpSj.note" title="备注"
                   	   
	value="<s:property value="snpSj.note"/>"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="snpSj_state"
                   	 name="snpSj.state" title="状态"
                   	   
					value="<s:property value="snpSj.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpSj_stateName"
                   	 name="snpSj.stateName" title="状态名称"  class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="snpSj.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}
							<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="snpSjItemJson" id="snpSjItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="snpSj.id"/>" />
            </form>
            <div id="tabs">
            <ul>
<!-- 			<li><a href="#snpSjItempage">SNP上机结果</a></li> -->
           	</ul> 
			<div id="snpSjItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
