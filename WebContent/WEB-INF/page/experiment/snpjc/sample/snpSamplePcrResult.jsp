﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/snpjc/sample/snpSamplePcrResult.js"></script>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=snpSamplePcrResult&id=${snpSamplePcrResult.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<div id="snpSamplePcrResultdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>结果</span></td>
                <td><select id="result" style="width:100">
<!--                 		<option value="" <s:if test="result==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1">合格</option>
    					<option value="0">不合格</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_submit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>是否提交</span></td>
                <td><select id="submit"  style="width:100">
<!--                 		<option value="" <s:if test="submit==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="submit==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="submit==0">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>
