<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=snpSample&id=${snpSample.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/snpjc/sample/snpSampleEdit.js"></script>
<div style="float:left;width:25%" id="snpSampleTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpSample_id"
                   	 name="snpSample.id" title="编号" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="snpSample.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="55" id="snpSample_name"
                   	 name="snpSample.name" title="描述"
                   	   
	value="<s:property value="snpSample.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/snpjc/sample/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpSample_createUser').value=rec.get('id');
				document.getElementById('snpSample_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="snpSample_createUser_name"  value="<s:property value="snpSample.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="snpSample_createUser" name="snpSample.createUser.id"  value="<s:property value="snpSample.createUser.id"/>" > 
			<%--  	<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />    --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpSample_createDate"
                   	 name="snpSample.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="snpSample.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="snpSample_template_name"  value="<s:property value="snpSample.template.name"/>"   />
 						<input type="hidden" id="snpSample_template" name="snpSample.template.id"  value="<s:property value="snpSample.template.id"/>" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail' onClick="TemplateFun()"   />                   		
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpSample_acceptUser').value=rec.get('id');
				document.getElementById('snpSample_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="snpSample_acceptUser_name"  value="<s:property value="snpSample.acceptUser.name"/>"   />
 						<input type="hidden" id="snpSample_acceptUser" name="snpSample.acceptUser.id"  value="<s:property value="snpSample.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="snpSample_state"
                   	 name="snpSample.state" title="状态"
                   	   
	value="<s:property value="snpSample.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpSample_stateName"
                   	 name="snpSample.stateName" title="工作流状态" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="snpSample.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="45" id="snpSample_note"
                   	 name="snpSample.note" title="备注"
                   	   
	value="<s:property value="snpSample.note"/>"
                   	  />
                   	  
                   	</td>

					<td class="label-title"><input type="button" onclick="loadPic()" value="查看附件" style="display: none;">
					<fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}
							<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="snpSampleItemJson" id="snpSampleItemJson" value="" />
            <input type="hidden" name="snpSampleTemplateJson" id="snpSampleTemplateJson" value="" />
            <input type="hidden" name="snpSampleReagentJson" id="snpSampleReagentJson" value="" />
            <input type="hidden" name="snpSampleCosJson" id="snpSampleCosJson" value="" />
            <input type="hidden" name="snpSamplePcrResultJson" id="snpSamplePcrResultJson" value="" />
            <input type="hidden" name="snpSampelChResultJson" id="snpSampelChResultJson" value="" />
            <input type="hidden" name="snpSamplePdhResultJson" id="snpSamplePdhResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="snpSample.id"/>" />
            </form>
            <div id="snpSampleItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
<!-- 			<li><a href="#snpSampleItempage">SNP样本处理明细</a></li> -->
			<li><a href="#snpSampleTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#snpSampleReagentpage"><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#snpSampleCospage"><fmt:message key="biolims.common.instrumentDetail"/></a></li>
<!-- 			<li><a href="#snpSamplePcrResultpage">SNP样本处理PCR结果</a></li> -->
<!-- 			<li><a href="#snpSampelChResultpage">SNP样本处理纯化结果</a></li> -->
<!-- 			<li><a href="#snpSamplePdhResultpage">SNP样本处理片段化结果</a></li> -->
           	</ul> 
			
			<div id="snpSampleTemplatepage" width="100%" height:10px></div>
			<div id="snpSampleReagentpage" width="100%" height:10px></div>
			<div id="snpSampleCospage" width="100%" height:10px></div>
			
			</div>
			<div id="snpSamplePcrResultpage" width="100%" height:10px></div>
			<div id="snpSampelChResultpage" width="100%" height:10px></div>
			<div id="snpSamplePdhResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
