﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/snpjc/sample/snpSampleItem.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>
</head>
<body>
<form name='excelfrm' action='${ctx}/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	<div id="snpSampleItemdiv"></div>
	<div id="bat_uploadcsvItem_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsvItem">上传CSV文件
	</div>
			<div id="bat_productNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>中间产物数量</span></td>
				<td><input id="productNum"/></td>
			</tr>
		</table>
		</div>
		<div id="many_bat_div2" style="display: none">
		<div class="ui-widget;">
			<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_bat_text2" style="width:650px;height: 339px"></textarea>
		</div>
</body>
</html>
