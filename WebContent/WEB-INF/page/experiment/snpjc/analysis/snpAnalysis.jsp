﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/snpjc/analysis/snpAnalysis.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="snpAnalysis_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="snpAnalysis_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					 	</td>
 
                  
                   	</tr>

			<tr>
               	 	<td class="label-title" >创建时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  </td>
                   	</tr>
                   	  <tr>
                   	
               	 	<td class="label-title" >分析日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startfxDate" name="startfxDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="fxDate1" name="fxDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endfxDate" name="endfxDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="fxDate2" name="fxDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
                   	</tr>
                   				<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="snpAnalysis_createUser"
				documentName="snpAnalysis_createUser_name"/>
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="snpAnalysis_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="snpAnalysis_createUser" name="snpAnalysis.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   		</td>
               	 	<td class="label-title" >状态名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="snpAnalysis_stateName"
                   	 name="stateName" searchField="true" title="状态名称"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
                   	</tr>
                   	<tr>
			<g:LayOutWinTag buttonId="showtemplate" title="选择实验模板"
				hasHtmlFrame="true"
				html="${ctx}/system/template/template/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpAnalysis_template').value=rec.get('id');
				document.getElementById('snpAnalysis_template_name').value=rec.get('name');" />
               	 	<td class="label-title" >实验模板</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="snpAnalysis_template_name" searchField="true"  name="template.name"  value="" class="text input" />
 						<input type="hidden" id="snpAnalysis_template" name="snpAnalysis.template.id"  value="" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   		<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="snpAnalysis_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpAnalysis_acceptUser').value=rec.get('id');
				document.getElementById('snpAnalysis_acceptUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >实验组</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="snpAnalysis_acceptUser_name" searchField="true"  name="acceptUser.name"  value="" class="text input" />
 						<input type="hidden" id="snpAnalysis_acceptUser" name="snpAnalysis.acceptUser.id"  value="" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	</tr>
  <%--                  	<tr>
			<g:LayOutWinTag buttonId="showsnpSj" title="选择上机编号"
				hasHtmlFrame="true"
				html="${ctx}/experiment/snpjc/analysis/snpSjSelect.action"
				isHasSubmit="false" functionName="SnpSjFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpAnalysis_snpSj').value=rec.get('id');
				document.getElementById('snpAnalysis_snpSj_name').value=rec.get('name');" />
               	 	<td class="label-title" >上机编号</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="snpAnalysis_snpSj_name" searchField="true"  name="snpSj.name"  value="" class="text input" />
 						<input type="hidden" id="snpAnalysis_snpSj" name="snpAnalysis.snpSj.id"  value="" > 
 						<img alt='选择上机编号' id='showsnpSj' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 
 
                  
                   	
                   	  
                   	</td>
			</tr> --%>
			<tr>
               	 	<td class="label-title"  style="display:none"  >状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="snpAnalysis_state"
                   	 name="state" searchField="true" title="状态"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   
			</tr>
            </table>
		</form>
		</div>
		<div id="show_snpAnalysis_div"></div>
   		<form name='excelfrm' action='${ctx}/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_snpAnalysis_tree_page"></div>
</body>
</html>



