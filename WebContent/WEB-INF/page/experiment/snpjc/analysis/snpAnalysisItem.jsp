﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/snpjc/analysis/snpAnalysisItem.js"></script>
</head>
<body>
	<div id="snpAnalysisItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>是否合格</span></td>
                <td><select id="result" style="width:100">
<!--                 		<option value="" <s:if test="result==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>>合格</option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>>不合格</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_submit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>是否提交</span></td>
                <td><select id="submit"  style="width:100">
<!--                 		<option value="" <s:if test="submit==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="submit==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="submit==0">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_area_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>分析结果</span></td>
				<td><textarea id="jg" style="width: 500px;height: 100px;"></textarea></td>
			</tr>
			<tr>
				<td class="label-title"><span>结果解释（限700字）</span></td>
				<td><textarea id="re" style="width: 500px;height: 100px;"></textarea></td>
			</tr>
			<tr>
				<!-- <td class="label-title"><span>临床建议</span></td>
				<td><textarea id="lc" style="width: 500px;height: 100px;"></textarea></td> -->
				<td class="label-title" ><span>临床建议</span></td>
                <td><select id="lc"  style="width:150;height:20px;">
                		<option value="" >请选择</option>
    					<option value="1" >门诊随访</option>
    					<option value="0" >遗传门诊随访</option>
					</select>
                 </td>
			</tr>
			<tr>
				<td class="label-title" ><span>是否异常报告</span></td>
                <td><select id="yc"  style="width:150;height:20px;">
    					<option value="" >请选择</option>
    					<option value="1" >正常</option>
    					<option value="0" >异常</option>
    					<option value="2" >多态</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_area1_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>分析结果</span></td>
				<td><textarea id="jg1" style="width: 500px;height: 100px;"></textarea></td>
			</tr>
			<tr>
				<td class="label-title"><span>结果解释</span></td>
				<td><textarea id="re1" style="width: 500px;height: 100px;"></textarea></td>
			</tr>
			<tr>
				<td class="label-title"><span>临床建议</span></td>
				<td><textarea id="lc1" style="width: 500px;height: 100px;"></textarea></td>
			</tr>
			<tr>
				<td class="label-title" ><span>是否异常报告</span></td>
                <td><select id="yc1"  style="width:150">
                		<option value="" >请选择</option>
    					<option value="1" >是</option>
    					<option value="0" >否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>
