<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=snpAnalysis&id=${snpAnalysis.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/snpjc/analysis/snpAnalysisEdit.js"></script>
<div style="float:left;width:25%" id="snpAnalysisTempPage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpAnalysis_id"
                   	 name="snpAnalysis.id" title="编号" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="snpAnalysis.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="snpAnalysis_name"
                   	 name="snpAnalysis.name" title="描述"
                   	   
	value="<s:property value="snpAnalysis.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/snpjc/analysis/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpAnalysis_createUser').value=rec.get('id');
				document.getElementById('snpAnalysis_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="snpAnalysis_createUser_name"  value="<s:property value="snpAnalysis.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="snpAnalysis_createUser" name="snpAnalysis.createUser.id"  value="<s:property value="snpAnalysis.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpAnalysis_createDate"
                   	 name="snpAnalysis.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="snpAnalysis.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.analysisData"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="snpAnalysis_fxDate"
                   	 name="snpAnalysis.fxDate" title="分析日期"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="snpAnalysis.fxDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
<!--                	 	<td class="label-title" >实验模板</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<%--  						<input type="text" size="20" readonly="readOnly"  id="snpAnalysis_template_name"  value="<s:property value="snpAnalysis.template.name"/>"  /> --%>
<%--  						<input type="hidden" id="snpAnalysis_template" name="snpAnalysis.template.id"  value="<s:property value="snpAnalysis.template.id"/>" >  --%>
<%--  						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"  />                   		 --%>
<!--                    	</td> -->
					<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
					hasHtmlFrame="true"
					html="${ctx}/core/userGroup/userGroupSelect.action"
					isHasSubmit="false" functionName="UserGroupFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('snpAnalysis_acceptUser').value=rec.get('id');
					document.getElementById('snpAnalysis_acceptUser_name').value=rec.get('name');" />

               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="snpAnalysis_acceptUser_name"  value="<s:property value="snpAnalysis.acceptUser.name"/>"  />
 						<input type="hidden" id="snpAnalysis_acceptUser" name="snpAnalysis.acceptUser.id"  value="<s:property value="snpAnalysis.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
            </tr>
			<tr>
				<%-- <g:LayOutWinTag buttonId="showsnpSj" title="选择上机编号"
				hasHtmlFrame="true"
				html="${ctx}/experiment/snpjc/analysis/snpSjSelect.action"
				isHasSubmit="false" functionName="SnpSjFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpAnalysis_snpSj').value=rec.get('id');
				document.getElementById('snpAnalysis_snpSj_name').value=rec.get('name');" /> --%>
				
           	 	<%-- <td class="label-title" >选择洗染扫描</td>
           	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
               	<td align="left"  >
				<input type="text" size="20" readonly="readOnly"  id="snpAnalysis_snpSj_name"  value="<s:property value="snpAnalysis.snpSj.name"/>" />
				<input type="hidden" id="snpAnalysis_snpSj" name="snpAnalysis.snpSj.id"  value="<s:property value="snpAnalysis.snpSj.id"/>" > 
				<img alt='选择洗染扫描' id='showsnpSj' src='${ctx}/images/img_lookup.gif' class='detail' onclick="SnpSjFun()"/>                   		
               	</td>
 --%>

           	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
           	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                  	<td align="left"  >
                  	<input type="text" size="30" maxlength="50" id="snpAnalysis_note"
                  	 name="snpAnalysis.note" title="备注"
                  	   
				value="<s:property value="snpAnalysis.note"/>"
               	  />
               	  
               	</td>
			
           	 	<td class="label-title"  style="display:none"  >状态</td>
           	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
               	<td align="left"   style="display:none">
               	<input type="text" size="20" maxlength="25" id="snpAnalysis_state"
               	 name="snpAnalysis.state" title="状态"
                   	   
	value="<s:property value="snpAnalysis.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="snpAnalysis_stateName"
                   	 name="snpAnalysis.stateName" title="状态名称"   class="text input readonlytrue" readonly="readOnly" 
                   	   
	value="<s:property value="snpAnalysis.stateName"/>"
                   	  />
                   	  
                   	</td>
                   		<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>
							${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span>
			</tr>
			<tr>
			
			</tr>
			
			
            </table>
            <input type="hidden" name="snpAnalysisItemJson" id="snpAnalysisItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="snpAnalysis.id"/>" />
            </form>
            <div id="tabs">
            <ul>
<!-- 			<li><a href="#snpAnalysisItempage">SNP分析明细</a></li> -->
           	</ul> 
			<div id="snpAnalysisItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
