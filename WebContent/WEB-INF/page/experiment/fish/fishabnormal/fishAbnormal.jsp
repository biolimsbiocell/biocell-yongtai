﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/fish/fishabnormal/fishAbnormal.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left" >
					 	<input type="text" size="25" maxlength="60" id="fishAbnormal_code"
                   	 	name="code" searchField="true" title="样本编号"/>
                   	 </td>
                   	 
                   	 
                   	<td class="label-title">原始样本编号</td>
                   	<td align="left"  >
						<input type="text" size="25" maxlength="60" id="fishAbnormal_sampleCode"
                   	 	name="sampleCode" searchField="true" title="原始样本编号"/>
                   	 	
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >检测项目</td>
                   	<td align="left"  >
                  
					<input type="text" size="25" maxlength="30" id="fishAbnormal_productName"
                   	 name="productName" searchField="true" title="检测项目"    />
                   	
          			 </td>
               	 	
                   	<td class="label-title" >样本类型</td>
                   	<td align="left"  >
                  
					<input type="text" size="25" maxlength="30" id="fishAbnormal_sampleType"
                   	 name="sampleType" searchField="true" title="样本类型"    />
                   	
          			 </td>
				</tr>
            </table>          
		</form>
		</div>
	<div id="fishAbnormaldiv"></div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>结果</span></td>
                <td><select id="result" style="width:100">
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>>合格</option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>>不合格</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_isSubmit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>是否提交</span></td>
                <td><select id="isSubmit"  style="width:100">
    					<option value="1" <s:if test="submit==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="submit==0">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>
