﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<%-- <script type="text/javascript" src="${ctx}/js/experiment/blood/samplePreTask.js"></script> --%>
<script type="text/javascript"
	src="${ctx}/js/experiment/fish/samplePreTask.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
	<div id="jstj" style="display: none">
		<input type="hidden" id="selectId" /> <input type="hidden"
			id="extJsonDataString" name="extJsonDataString">
		<form id="searchForm">
			<table class="frame-table">
				<tr>
					<td class="label-title">编号</td>
					<td align="left"><input type="text" size="30" maxlength="30"
						id="samplePreTask_id" name="id" searchField="true" title="编号" />
					</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						documentId="samplePreTask_createUser"
						documentName="samplePreTask_createUser_name" />
					<td class="label-title">下达人</td>
					<td align="left"><input type="text" size="30"
						id="samplePreTask_createUser_name" searchField="true"
						name="createUser.name" value="" class="text input" /> <input
						type="hidden" id="samplePreTask_createUser"
						name="samplePreTask.createUser.id" value=""> <img
						alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				<tr>
					<td class="label-title">下达时间</td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="startcreateDate" name="startcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate1" name="createDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endcreateDate" name="endcreateDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="createDate2" name="createDate##@@##2"
						searchField="true" /></td>
				</tr>
<!-- 
				<tr>
					<g:LayOutWinTag buttonId="showtestUser" title="选择实验员"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="TestUserFun" hasSetFun="true"
						documentId="samplePreTask_testUser"
						documentName="samplePreTask_testUser_name" />
					<td class="label-title">实验员</td>
					<td align="left"><input type="text" size="30"
						id="samplePreTask_testUser_name" searchField="true"
						name="testUser.name" value="" class="text input" /> <input
						type="hidden" id="samplePreTask_testUser"
						name="samplePreTask.testUser.id" value=""> <img
						alt='选择实验员' id='showtestUser' src='${ctx}/images/img_lookup.gif'
						class='detail' /></td>
				</tr>
				 -->
				<tr>
					<td class="label-title">实验时间</td>
					<td align="left"><input type="text" class="Wdate"
						readonly="readonly" id="starttestDate" name="starttestDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="testDate1" name="testDate##@@##1"
						searchField="true" /> - <input type="text" class="Wdate"
						readonly="readonly" id="endtestDate" name="endtestDate"
						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" /> <input
						type="hidden" id="testDate2" name="testDate##@@##2"
						searchField="true" /></td>
				</tr>

			</table>
		</form>
	</div>
	<div id="show_samplePreTask_div"></div>
	<form name='excelfrm' action='${ctx}/common/exportExcel.action'
		method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value='' />
	</form>
	<div id="show_samplePreTask_tree_page"></div>
</body>
</html>



