﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/fish/samplePreTaskDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="samplePreTask_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="samplePreTask_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">下达人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="samplePreTask_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">下达时间</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="samplePreTask_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">所属任务</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="samplePreTask_workOrder" searchField="true" name="workOrder"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">实验员</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="samplePreTask_testUser" searchField="true" name="testUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">实验时间</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="samplePreTask_testDate" searchField="true" name="testDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">选择模板</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="samplePreTask_template" searchField="true" name="template"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="samplePreTask_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="30" id="samplePreTask_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_samplePreTask_div"></div>
   		
</body>
</html>



