﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/experiment/fish/fishProTaskItem.js"></script>
</head>
<body>

	<div id="fishProTaskItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_infos_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.concentration"/></span></td>
				<td><input id="concentration"/></td>
			</tr>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.volume"/></span></td>
				<td><input id="volume"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_productNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.dicSampleNum"/></span></td>
				<td><input id="productNum"/></td>
			</tr>
		</table>
	</div>
	<div id="bat_sampleNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.probeNumber"/></span></td>
				<td><input id="probe"/></td>
			</tr>
		</table>
	</div>
</body>
</html>


