<%-- <%@page import="org.apache.catalina.connector.Request"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=fishCrossTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/fish/fishCrossTaskEdit.js"></script>
	<div style="float:left;width:25%" id=fishCrossTaskTemppage>
</div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="40" id="type"  value="plasma" />
                   	<input type="text" size="20" maxlength="18" id="fishCrossTask_id"
                   	 name="fishCrossTask.id" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="fishCrossTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="60" id="fishCrossTask_name"
                   	 name="fishCrossTask.name" title="" value="<s:property value="fishCrossTask.name"/>"
                   	  />
                   	  <input type="hidden" size="30" maxlength="60" id="fishCrossTask_maxNum" name="fishCrossTask.maxNum" title=""
							value="<s:property value="fishCrossTask.maxNum"/>"
                   	  />
                   	</td>
			
			
			
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function CreateUserFun(){
var win = Ext.getCmp('CreateUserFun');
if (win) {win.close();}
var CreateUserFun= new Ext.Window({
id:'CreateUserFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/user/userSelect.action?flag=CreateUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 CreateUserFun.close(); }  }]  });     CreateUserFun.show(); }
);
});
 function setCreateUserFun(rec){
document.getElementById('fishCrossTask_createUser').value=rec.get('id');
				document.getElementById('fishCrossTask_createUser_name').value=rec.get('name');
var win = Ext.getCmp('CreateUserFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="fishCrossTask_createUser_name"  value="<s:property value="fishCrossTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="fishCrossTask_createUser" name="fishCrossTask.createUser.id"  value="<s:property value="fishCrossTask.createUser.id"/>" > 
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="fishCrossTask_createDate"
                   	 name="fishCrossTask.createDate" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:property  value="fishCrossTask.createDate"/>"/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.confirmDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="fishCrossTask_confirmDate"
                   	 name="fishCrossTask.confirmDate" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:property  value="fishCrossTask.confirmDate"/>"/>
                   	</td>
				
			
				<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showAcceptUser = Ext.get('showAcceptUser');
showAcceptUser.on('click', 
 function loadAcceptUser(){
var win = Ext.getCmp('loadAcceptUser');
if (win) {win.close();}
var loadAcceptUser= new Ext.Window({
id:'loadAcceptUser',modal:true,title:'Selection of experimental group',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=loadAcceptUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 loadAcceptUser.close(); }  }]  });     loadAcceptUser.show(); }
);
});
 function setloadAcceptUser(rec){
document.getElementById('fishCrossTask_acceptUser').value=rec.get('id'); 
					document.getElementById('fishCrossTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('loadAcceptUser')
if(win){win.close();}
}
</script>
 

			
					<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="fishCrossTask_acceptUser_name"  value="<s:property value="fishCrossTask.acceptUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="fishCrossTask_acceptUser" name="fishCrossTask.acceptUser.id"  value="<s:property value="fishCrossTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			</tr>
			<tr>
			
					<td class="label-title" ><fmt:message key="biolims.common.testTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="fishCrossTask_testDate"
                   	 name="fishCrossTask.testDate" title=""
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="fishCrossTask.testDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="fishCrossTask_template_name"  value="<s:property value="fishCrossTask.template.name"/>"  />
 						<input type="hidden" id="fishCrossTask_template" name="fishCrossTask.template.id"  value="<s:property value="fishCrossTask.template.id"/>" > 
 						<img alt='选择选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="TemplateFun()" />                   		
                   	</td>
			
			
               	 	<%-- <td class="label-title"  >类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ></td>            	 	
                   	<td align="left"  >
                   	  <select id="fishCrossTask_type" name="fishCrossTask.type">
                   	  	<option value="" <s:if test="result==">selected="selected" </s:if>>请选择</option>
                   	  	<option value="1" <s:if test="fishCrossTask.type==1">selected="selected" </s:if>>分割</option>
                   	  	<option value="2" <s:if test="fishCrossTask.type==2">selected="selected" </s:if>>分离</option>
                   	  </select>
                   	</td> --%>
			
					<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="fishCrossTask_state" name="fishCrossTask.state" value="<s:property value="fishCrossTask.state"/>"/>
                   	</td>
                   				
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="fishCrossTask_stateName"
                   	 name="fishCrossTask.stateName" title=""
                   	  readonly = "readOnly" class="text input readonlytrue" 
					value="<s:property value="fishCrossTask.stateName"/>"
                   	  />
                   	  
                   	</td>
            </tr>
			<tr>   	
				<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
				<td title=<fmt:message key="biolims.common.afterthepreservation"/> id="doclinks_img"><span 
					class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span>	</td>
                   	
			</tr>
            </table>
            <input type="hidden" name="fishCrossTaskItemJson" id="fishCrossTaskItemJson" value="" />
            <input type="hidden" name="fishCrossTaskResultJson" id="fishCrossTaskResultJson" value="" />
            <input type="hidden" name="fishCrossTaskTemplateJson" id="fishCrossTaskTemplateJson" value="" />
            <input type="hidden" name="fishCrossTaskReagentJson" id="fishCrossTaskReagentJson" value="" />
            <input type="hidden" name="fishCrossTaskCosJson" id="fishCrossTaskCosJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="fishCrossTask.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
			<li><a href="#fishCrossTaskItempage">血浆分离明细</a></li>
			<li><a href="#fishCrossTaskResultpage">血浆分离结果</a></li>
           	</ul>  -->
			<div id="fishCrossTaskItempage" width="100%" height:10px></div>
			<%-- <% 
			String maxNum = (String)request.getParameter("maxNum");
			System.out.print(maxNum);
			if(maxNum!=null && !maxNum.equals("")&& !maxNum.equals("null")){
			Integer max = Integer.parseInt(maxNum);
			for(int i=0;i<max;i++){
			%> --%>
			<%-- <div id = '<%="gridContainerdiv0" %>'></div>
			<div id = '<%="gridContainerdiv1" %>'></div> --%>
			<%-- <%} 
			}%> --%>
			<!-- <div id="3d_image"></div> -->
			<div id="tabs">
            <ul>
			<li><a href="#fishCrossTaskTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#fishCrossTaskReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#fishCrossTaskCospage" onClick="showCos()"><fmt:message key="biolims.common.instrumentDetail"/></a></li>
           	</ul>
           	<div id="fishCrossTaskTemplatepage" width="100%" height:10px></div>
           	<div id="fishCrossTaskReagentpage" width="100%" height:10px></div>
           	<div id="fishCrossTaskCospage" width="100%" height:10px></div>
           	</div> 
			<div id="fishCrossTaskResultpage" width="100%" height:10px></div>
			</div>
        	
	</body>
	</html>
