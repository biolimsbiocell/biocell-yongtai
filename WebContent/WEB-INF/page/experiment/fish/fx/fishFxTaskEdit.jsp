<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=fishFxTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>


</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/fish/fx/fishFxTaskEdit.js"></script>
<div style="float:left;width:25%" id="fishFxTaskTempPage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="fishFxTask_id"
                   	 name="fishFxTask.id" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="fishFxTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="25" id="fishFxTask_name"
                   	 name="fishFxTask.name" title=""
                   	   
	value="<s:property value="fishFxTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/experiment/fish/fx/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(rec){
document.getElementById('fishFxTask_createUser').value=rec.get('id');
				document.getElementById('fishFxTask_createUser_name').value=rec.get('name');
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>


				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.analysiser" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="fishFxTask_createUser_name"  value="<s:property value="fishFxTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="fishFxTask_createUser" name="fishFxTask.createUser.id"  value="<s:property value="fishFxTask.createUser.id"/>" > 
<%--  						<img alt='选择分析人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.analysisData" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="fishFxTask_createDate"
                   	 name="fishFxTask.createDate" title=""
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	    value="<s:date name="fishFxTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showacceptUser = Ext.get('showacceptUser');
showacceptUser.on('click', 
 function UserGroupFun(){
var win = Ext.getCmp('UserGroupFun');
if (win) {win.close();}
var UserGroupFun= new Ext.Window({
id:'UserGroupFun',modal:true,title:'Selection of experimental group',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=UserGroupFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserGroupFun.close(); }  }]  });     UserGroupFun.show(); }
);
});
 function setUserGroupFun(rec){
document.getElementById('fishFxTask_acceptUser').value=rec.get('id');
				document.getElementById('fishFxTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('UserGroupFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="fishFxTask_acceptUser_name"  value="<s:property value="fishFxTask.acceptUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="fishFxTask_acceptUser" name="fishFxTask.acceptUser.id"  value="<s:property value="fishFxTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="fishFxTask_state"
                   	 name="fishFxTask.state" title=""
                   	   
	value="<s:property value="fishFxTask.state"/>"
                   	   style="display:none"
                   	  />
                   <%--<g:LayOutWinTag buttonId="showFishSjTask" title="选择上机阅片"
					hasHtmlFrame="true"
					html="${ctx}/experiment/fish/fishSjTask/fishSjTaskSelect.action"
					isHasSubmit="false" functionName="FishSjTaskFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('fishFxTask_fishSjTask').value=rec.get('id');
					document.getElementById('fishFxTask_fishSjTask_name').value=rec.get('name');" /> --%>
                	<%-- <td class="label-title" >选择上机阅片</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="fishFxTask_fishSjTask_name"  value="<s:property value="fishFxTask.fishSjTask.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="fishFxTask_fishSjTask" name="fishFxTask.fishSjTask.id"  value="<s:property value="fishFxTask.fishSjTask.id"/>" > 
 						<img alt='选择上机阅片' id='showFishSjTask' src='${ctx}/images/img_lookup.gif' class='detail' onclick="FishSjTaskFun()"/>                   		
                   	</td>  --%> 
                   	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="fishFxTask_stateName"
                   	 name="fishFxTask.stateName" title="" class="text input readonlytrue" readonly="readonly"
                   	   
	value="<s:property value="fishFxTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	
				</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.note" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="fishFxTask_note"
                   	 name="fishFxTask.note" title=""
                   	   
	value="<s:property value="fishFxTask.note"/>"
                   	  />
                   	  
                   	</td>
			<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title=<fmt:message key="biolims.common.afterthepreservation"/> id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="fishFxTaskResultJson" id="fishFxTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="fishFxTask.id"/>" />
            </form>
            <div id="tabs">
			<div id="fishFxTaskResultpage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
