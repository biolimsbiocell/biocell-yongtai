﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/experiment/fish/samplePreTaskResult.js"></script>
</head>
<body>
<form name='excelfrm' action='${ctx}/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	<div id="samplePreTaskResultdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="batccc_suspensionCode_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.suspensionNumber"/></span></td>
				<td><input id="suspensionCode"/></td>
			</tr>
			<!-- <tr>
				<td class="label-title"><span>浓度</span></td>
				<td><input id="plasmaConcentration"/></td>
			</tr>
			 <tr>
				<td class="label-title"><span>体积</span></td>
				<td><input id="plasmaVolume"/></td>
			</tr> -->
		</table>
	</div>
		<!-- <div id="bat_unit_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>单位</span></td>
				<td><input id="unit"/></td>
			</tr>
		</table>
	</div> -->
	<g:LayOutWinTag buttonId="dicUnitBtn" title="选择单位" hasHtmlFrame="true"
		html="${ctx}/dic/unit/dicUnitSelect.action" isHasSubmit="false"
		functionName="weight" hasSetFun="true" documentId="unit"
		documentName="zjdwName" />
		<div id="bat_unit_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>单位<fmt:message key=""/></span></td>
				<td><input type="hidden" id="unit"/>
				<input type="text" readonly="readOnly"  id="zjdwName"/> 
				<img alt='选择' id='dicUnitBtn' src='${ctx}/images/img_lookup.gif'class='detail' />
				</td>
			</tr>
		</table>
		</div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="result" style="width:100">
    					<option value="1"><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0"><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_submit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.whetherToCommit"/></span></td>
                <td><select id="submit"  style="width:100">
    					<option value="1"><fmt:message key="biolims.common.yes"/></option>
    					<option value="0"><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>

</body>
</html>


