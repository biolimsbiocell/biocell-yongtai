﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=snpTask&id=${snpTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/snp/snpTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<div style="float:left;width:25%" id="snpTaskTempPage">	
</div>
  	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>						
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="hidden" size="40" id="type"  value="snpTask" />
                   	<input type="text" size="20" maxlength="18" id="snpTask_id"
                   	 name="snpTask.id" title="编号"
                   	readonly = "readOnly" class="text input readonlytrue"   
	value="<s:property value="snpTask.id"/>"
                   	  />                   	  
                   	</td>						
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="30" maxlength="60" id="snpTask_maxNum" name="snpTask.maxNum" title="容器数量"
							value="<s:property value="snpTask.maxNum"/>"
                   	  />
                   	<input type="text" size="20" maxlength="60" id="snpTask_name"
                   	 name="snpTask.name" title="描述"
                   	   
	value="<s:property value="snpTask.name"/>"
                   	  />                   	  
                   	</td>									
			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="CreateUserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('snpTask_createUser').value=rec.get('id');
				document.getElementById('snpTask_createUser_name').value=rec.get('name');" />										
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="snpTask_createUser_name"  value="<s:property value="snpTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="snpTask_createUser" name="snpTask.createUser.id"  value="<s:property value="snpTask.createUser.id"/>" > 
                   	</td>
			</tr>
			<tr>						
               	 	<td class="label-title" >下达日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                   	  
                   	  	<input type="text" size="15" maxlength="" id="snpTask_createDate"
                   	 name="snpTask.createDate" title="下达日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:property value="snpTask.createDate"/>"                  	     
                   	  />
                   	</td>                   	
                   		<td class="label-title" >完成时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                  	  
                   	  	<input type="text" size="20" maxlength="" id="snpTask_confirmDate"
                   	 name="snpTask.confirmDate" title="下达时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="snpTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>			
				<td class="label-title" >选择实验模板</td>
			        <td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="40" readonly="readOnly"  id="snpTask_template_name"  value="<s:property value="snpTask.template.name"/>" />
 						<input type="text"  readonly="readOnly" id="snpTask_template" name="snpTask.template.id"  value="<s:property value="snpTask.template.id"/>" > 
 						<img alt='选择模板' id='showTemplateFun' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()" />                   		
                   	</td>			
               	 	<td class="label-title" style="display:none">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="40" maxlength="30" id="snpTask_state"
                   	 name="snpTask.state" title="工作流状态"
                   	   style="display:none"
	value="<s:property value="snpTask.state"/>"
                   	  />                  	  
                   	</td>			
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showAcceptUser" title="选择实验组"
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('snpTask_acceptUser').value=rec.get('id'); 
					document.getElementById('snpTask_acceptUser_name').value=rec.get('name');" /> 			
					<td class="label-title" >实验组</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="snpTask_acceptUser_name"  value="<s:property value="snpTask.acceptUser.name"/>" class="text input" readonly="readOnly"  />
 						<input type="hidden" id="snpTask_acceptUser" name="snpTask.acceptUser.id"  value="<s:property value="snpTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>               		
			<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="snpTask_stateName"
                   	 name="snpTask.stateName" title="工作流状态"
                   	   class="text input readonlytrue" readonly="readOnly"  
	value="<s:property value="snpTask.stateName"/>"
                   	  />
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>			
            </table>
            <input type="hidden" name="snpTaskItemJson" id="snpTaskItemJson" value="" />
            <input type="hidden" name="snpTaskResultJson" id="snpTaskResultJson" value="" />
            <input type="hidden" name="snpTaskTemplateItemJson" id="snpTaskTemplateItemJson" value="" />
            <input type="hidden" name="snpTaskTemplateReagentJson" id="snpTaskTemplateReagentJson" value="" />
            <input type="hidden" name="snpTaskTemplateCosJson" id="snpTaskTemplateCosJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="snpTask.id"/>" />
            </form>
			<div id="snpTaskItempage" width="100%" height:10px></div>
			<div id = '<%="3d_image0" %>'></div>
			<div id = '<%="3d_image1" %>'></div>
			<div id="tabs">
            <ul>
			<li><a href="#snpTaskTemplateItempage">模版明细</a></li>
			<li><a href="#snpTaskTemplateReagentpage" onClick="showReagent()">原辅料明细</a></li>
			<li><a href="#snpTaskTemplateCospage" onClick="showCos()">设备明细</a></li>
           	</ul>
           	<div id="snpTaskTemplateItempage" width="100%" height:10px></div>
           	<div id="snpTaskTemplateReagentpage" width="100%" height:10px></div>
           	<div id="snpTaskTemplateCospage" width="100%" height:10px></div> 
           	</div>
			<div id="snpTaskResultpage" width="100%" height:10px></div>
			</div>
	</body>
	</html>
