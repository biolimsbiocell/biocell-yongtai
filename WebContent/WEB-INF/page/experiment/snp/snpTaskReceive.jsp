﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/snp/snpTaskReceive.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >                 
					<input type="text" size="20" maxlength="25" id="experimentDnaReceive_id"
                   	 name="id" searchField="true" title="编号"    />
                   	</td>
			</tr>
			<tr>
			</tr>
			<tr>
               	 	<td class="label-title" >接收日期</td>
                   	<td align="left"  >
                  
 						<input type="text" class="Wdate" readonly="readonly" id="startreceiverDate" name="startreceiverDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiverDate1" name="receiverDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreceiverDate" name="endreceiverDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiverDate2" name="receiverDate##@@##2"  searchField="true" />
                   	</td>                   						                                                      	  
               	 	<td class="label-title" >结果判定</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="experimentDnaReceive_method"
                   	 name="method" searchField="true" title="结果判定"    />                   						                                      
                   	</td> 
			</tr>
            </table>
		</form>
		</div>
		<div id="show_snpTaskReceive_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_experimentDnaReceive_tree_page"></div>
</body>
</html>



