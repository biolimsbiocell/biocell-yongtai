﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/snp/snpTaskRepeat.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="jstj" style="display: none">
			<input type="hidden" id="selectId"/>
			<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
			<form id="searchForm">
				<table class="frame-table">
				<tr>
					<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="snpTaskAbnormal_code"
                   	 name="code" searchField="true" title="名称"    />
                   	</td>
                   	<td class="label-title" >原始样本编号</td>
                   	<td align="left"  >                 
					<input type="text" size="20" maxlength="50" id="snpTaskAbnormal_Code"
                   	 name="snpTaskCode" searchField="true" title="名称"    />
                   	</td>
                   	</tr>
				</table>
		</form>
		</div>
		<div id="show_snpTaskRepeat_div" width="100%" height:10px></div>
		<div id="bat_next_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>处理意见</span></td>
                <td><select id="method"  style="width:100">
              		<option value="" <s:if test="method==">selected="selected" </s:if>>请选择</option>
  					<option value="1" <s:if test="method==1">selected="selected" </s:if>>重新fluidigm实验</option>
  					<option value="0" <s:if test="method==0">selected="selected" </s:if>>不合格</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>



