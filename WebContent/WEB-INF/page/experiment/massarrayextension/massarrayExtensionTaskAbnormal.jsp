﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/massarrayExtension/massarrayExtensionTaskAbnormal.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>                   	               	 	                                     	                  	  
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >                 
					<input type="text" size="30" maxlength="50" id="massarrayExtensionTaskAbnormal_code"
                   	 name="code" searchField="true" title="名称"    />
                   	</td>
                   	<td class="label-title" >原始样本编号</td>
                   	<td align="left"  >                  
					<input type="text" size="30" maxlength="50" id="massarrayExtensionTaskAbnormal_Code"
                   	 name="Code" searchField="true" title="名称"    />
                   	</td>              	 	
			</tr>
        </table>
		</form>
		</div>
		<div id="show_massarrayExtensionTaskAbnormal_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_massarrayExtensionTaskAbnormal_tree_page"></div>
		<div id="bat_next_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>下一步流向</span></td>
                <td><select id="nextFlow"  style="width:100">
              		<option value="" <s:if test="nextFlow==">selected="selected" </s:if>>请选择</option>
  					<option value="0" <s:if test="nextFlow==0">selected="selected" </s:if>>文库构建</option>
  					<option value="1" <s:if test="nextFlow==1">selected="selected" </s:if>>重新MassarrayExtension任务</option>
  					<option value="2" <s:if test="nextFlow==2">selected="selected" </s:if>>入库</option>
  					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>>反馈至项目组</option>
  					<option value="4" <s:if test="nextFlow==4">selected="selected" </s:if>>终止</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_ok_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>确认执行</span></td>
                <td><select id="isExecute"  style="width:100">
    					<option value="1" <s:if test="isExecute==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="isExecute==0">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>批量结果</span></td>
                <td><select id="result"  style="width:100">
               			<option value="" <s:if test="result==">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>>合格</option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>>不合格</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>



