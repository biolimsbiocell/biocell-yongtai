﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
 <script type="text/javascript" src="${ctx}/js/experiment/massarrayExtension/massarrayExtensionTaskManage.js"></script>
 <script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>		
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
			<td >
			<label class="text label" title="输入查询条件">
			样本编号
			</label>
			</td>	
			<td>
			<input type="text"  name="code" id="massarrayExtensionTaskManage_code" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			原始样本编号
			</label>
			</td>
			<td>
			<input type="text"  name="Code" id="massarrayExtensionTaskManage_Code" searchField="true" >
			</td>
			<td>
			<input type="button" onClick="selectMassarrayExtensionTaskInfo()" value="查询">
			</td>
			</tr>
		</table>			
		
		</form>
		<div id="show_massarrayExtensionTaskManage_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_massarrayExtensionTaskManage_tree_page"></div>
		<select id="method" style="display: none">
			<option value="">请选择</option>
			<option value="1">合格</option>
			<option value="0">不合格</option>
		</select>
		<div id="bat_managerNext_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>下一步流向</span></td>
                <td><select id="nextFlow"  style="width:100">
                		<option value="" <s:if test="nextFlow==">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="nextFlow==0">selected="selected" </s:if>>文库构建</option>
    					<option value="1" <s:if test="nextFlow==1">selected="selected" </s:if>>重新MassarrayExtension任务</option>
    					<option value="2" <s:if test="nextFlow==2">selected="selected" </s:if>>入库</option>
    					<!-- <option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>>反馈至项目组</option> -->
    					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>>终止</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_managerResult_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>结果</span></td>
                <td><select id="result" style="width:100">
                		<option value="" <s:if test="result==">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>>合格</option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>>不合格</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>



