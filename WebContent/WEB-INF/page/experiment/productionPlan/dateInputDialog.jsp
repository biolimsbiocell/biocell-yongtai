﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}

.tablebtns {
	float: right;
}

.dt-buttons {
	margin: 0;
	float: right;
}

.chosed {
	background-color: #5AC8D8 !important;
	color: #fff;
}

.col-sm-2 {
	float: left;
}
</style>
</head>
<body>
	<div class="input-group">
		<span class="input-group-addon" style="font-family: 黑体;">
			预计操作时间 </span> <input class="form-control datetimepicker" type="text"
			size="20" maxlength="25" id="date_input" /> <input
			class="form-control datetimepicker" type="text" size="20"
			maxlength="25" id="date_inputend" />
	</div>
	<div class="input-group">
		<span class="input-group-addon">步骤名称</span>
		<textarea id="productHideFields" name="product.hideFields"
			placeholder="长度限制2000个字符" class="form-control"
			style="overflow: hidden; width: 65%; height: 80px;"></textarea>
		<button style="height: 80px;" class="btn btn-info" type="button"
			onclick="choseName()">
			<i class="glyphicon glyphicon-search"></i>
		</button>
	</div>
	<script type="text/javascript">
		$(function() {
			$("#date_input").datepicker({
				language : "zh-CN",
				autoclose : true, //选中之后自动隐藏日期选择框
				format : "yyyy-mm-dd" //日期格式，详见 
			});
			$("#date_inputend").datepicker({
				language : "zh-CN",
				autoclose : true, //选中之后自动隐藏日期选择框
				format : "yyyy-mm-dd" //日期格式，详见 
			});
		});
		//选择步骤名称
		function choseName(){
			top.layer.open({
				title: biolims.common.pleaseChoose,
				type: 2,
				area: ["550px", "300px"],
				btn: biolims.common.selected,
				content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=bzcx", ''],
				yes: function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#sysCode").val();
					console.log(name)
					top.layer.close(index)
					$("#productHideFields").val(name);
				},
			})
			
		}
	</script>
</body>
</html>