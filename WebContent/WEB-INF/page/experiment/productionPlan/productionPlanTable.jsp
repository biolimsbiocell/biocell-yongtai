<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<link rel="stylesheet"
	href="${ctx}/lims/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${ctx}/lims/dist/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${ctx}/lims/dist/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" type="text/css"
	href="${ctx}/lims/css/dashboard.css" />
<link rel="stylesheet" href="${ctx}/lims/dist/css/AdminLTE.min.css">
<link rel="stylesheet" type="text/css"
	href="${ctx}/lims/plugins/datatables/datatables.min.css" />
<link rel="stylesheet"
	href="${ctx}/lims/plugins/icheck/skins/square/blue.css">
<script>
	layui.use('form', function() {
		formm = layui.form;
		// formm.on('checkbox', function(data){
		// 	if(data.elem.checked){
		// 	$('input[type=checkbox]').prop('checked',false);
		// 	$(data.elem).prop('checked',true);
		// 	}
		// 	formm.render();

		// 	}); 

		// 之前的
		formm.on('checkbox', function(data) {

			$(data.othis[0]).siblings(".layui-form-checkbox").removeClass(
					"layui-form-checked");

		});
	});
</script>
</head>
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}

.flexx {
	display: flex;
}

.flexx>div {
	flex: 1;
}

.p_right {
	padding-right: 0;
}

.p_left {
	padding-left: 0;
}

.obse {
	background: skyblue;
}

.obresult {
	background: #fff !important;
}
</style>
<body>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div id="tableFileLoad"></div>
	<div class="content-wrapper" id="content" style="margin-left: 0px;">
		<input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<input type="hidden" id="type" value="${requestScope.type}" /> <input
			type="hidden" id="types" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">生产计划</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh"
								onclick="tableRefresh()">
								<i class="glyphicon glyphicon-refresh"></i>
							</button>
							<div class="btn-group">
								<button type="button" class="btn btn-box-tool"
									style="font-size: 16px; border: 1px solid;" onclick="dayin()">
									<i class="fa fa-print"></i>打印生产计划
							</div>


							<%-- <div class="btn-group">
							<div class="col-xs-2 col-sm-2">
								<div class="input-group">
									<span class="input-group-addon">瓶/袋<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span> <select
										lay-ignore class="form-control" id="bottleOrBag">
										<option value="0">瓶</option>
										<option value="1">袋</option>
									</select>
								</div>
							</div>
							</div> --%>
							<!--<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
										<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="dayin()">
													打印生产计划
												</a>
											</li>
											<%-- <li>
												<a href="####" onclick="$('.buttons-print').click();">
													<fmt:message key="biolims.common.print" />
												</a>
											</li> --%>
											<%--< li>
												<a href="#####" onclick="$('.buttons-copy').click();">
													<fmt:message key="biolims.common.copyRecord" />
												</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="fixedCol()">
													<fmt:message key="biolims.common.freezenColumn" />(2)</a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()">
													<fmt:message key="biolims.common.releaseFreezen" />
												</a>
											</li> --%>
										</ul>
									</div>-->
						</div>
					</div>
					<%-- <div class="row" style="padding-left: 9px">
						<div class="col-xs-1 col-sm-1">
							<div class="input-group">
								<span class="input-group-addon">瓶/袋</span> <select lay-ignore
									class="form-control" id="bottleOrBag" style="width: 65px">
									<option value=""></option>
									<option value="0">瓶</option>
									<option value="1">袋</option>
								</select>
							</div>
						</div>

						<div class="col-xs-2 col-sm-2" style="padding-left: 60px">
							<div class="input-group">
								<span class="input-group-addon">感染类型</span> <select lay-ignore
									class="form-control" id="infectionType" style="width: 95px">
									<option value=""></option>
									<option value="感染">感染</option>
									<option value="无感染">无感染</option>
								</select>
							</div>
						</div>
						<div class="col-xs-2 col-sm-2" style="padding-left: 60px">
							<div class="input-group">
								<span class="input-group-addon">开始日期<img
									class='requiredimage' src='${ctx}/images/required.gif' />
								</span> <input style="width: 110px" class="form-control" type="text"
									size="20" maxlength="25" id="date_input" name="startBackTime"
									title=""
									value="<s:date name="sampleReceive.startBackTime" format="yyyy-MM-dd"/>" />
							</div>
						</div>
						<div class="col-xs-2 col-sm-2" style="padding-left: 80px">
							<div class="input-group">
								<span class="input-group-addon">结束日期<img
									class='requiredimage' src='${ctx}/images/required.gif' />
								</span> <input style="width: 110px" class="form-control" type="text"
									size="20" maxlength="25" id="date_inputEnd"
									name="startBackTime" title=""
									value="<s:date name="sampleReceive.startBackTime" format="yyyy-MM-dd"/>" />
							</div>
						</div>
                      <div class="col-xs-4 col-sm-4" style="padding-left: 110px">
						<div class="input-group">
							<span class="input-group-addon">步骤名称</span>
							<textarea id="productHideFields" name="product.hideFields"
								placeholder="长度限制2000个字符" class="form-control"
								style="overflow: hidden; width: 65%; height: 80px;"></textarea>
							<button style="height: 80px;" class="btn btn-info" type="button"
								onclick="choseName()">
								<i class="glyphicon glyphicon-search"></i>
							</button>
						</div>
						</div>
						<div class="input-group">
										<input type="button" class="btn btn-info btn-sm"
											onClick="serchpici();" value='查询' />
									</div>
						
					</div> --%>

					<div class="row layui-form ipadmini" style="padding-left: 9px">
						<div class="col-xs-3 col-sm-3 p_right" style>
							<div id="bottleOrBagG">
								<div class="input-group" id="bottleOrBag" style="height: 34px;"ckGender">
									<span class="input-group-addon">瓶/袋</span> <input
										type="checkbox" value="0" title="瓶"> <input
										type="checkbox" value="1" title="袋">
								</div>
							</div>
						</div>

						<div class="col-xs-5 col-sm-5 p_right">
							<div id="infectionTypeG">
								<div class="input-group" id="produceId" style="height: 34px;"ckGender">
									<span class="input-group-addon">生产状态</span> <input
										type="checkbox" value="1" title="已生产"> <input
										id="produceIdC"
										type="checkbox" value="2" checked title="待生产"> <input
										type="checkbox" value="3" title="全部生产">
								</div>
							</div>
						</div>
						<div class="col-xs-4 col-sm-4 p_right">
							<div class="input-group">
								<span class="input-group-addon">产品批号</span> <input type="text"
									id="pici" name="pici" class="form-control"
									style="width: 200px;">
							</div>
						</div>
					</div>
					<div class="row layui-form ipadmini" style="padding-left: 9px">
						<div class="col-xs-3 col-sm-3">
							<div class="input-group">
								<span class="input-group-addon">开始日期
								</span> <input style="width: 110px" class="form-control" type="text"
									size="20" maxlength="25" id="date_input" name="startBackTime"
									title=""
									value="<s:date name="sampleReceive.startBackTime" format="yyyy-MM-dd"/>" />
							</div>
							<div class="input-group">
								<span class="input-group-addon">结束日期
								</span> <input style="width: 110px" class="form-control" type="text"
									size="20" maxlength="25" id="date_inputEnd"
									name="startBackTime" title=""
									value="<s:date name="sampleReceive.startBackTime" format="yyyy-MM-dd"/>" />
							</div>
						</div>


						<div class="col-xs-4 col-sm-4">
							<div class="input-group">
								<span class="input-group-addon">步骤名称</span>
								<textarea id="productHideFields" name="product.hideFields"
									placeholder="长度限制2000个字符" class="form-control"
									style="overflow-y: auto; resize: none; width: 65%; height: 80px;"></textarea>
								<button style="height: 80px;" class="btn btn-info" type="button"
									onclick="choseName()">
									<i class="glyphicon glyphicon-search"></i>
								</button>

								<!-- 	<input type="button" class="btn btn-info btn-sm"
									onClick="serchpici();" value='查询' />
								<input type="button" class="btn btn-info btn-sm"
									onClick="serchTomorrow();" value='查看明天待生产' /> -->
								<!-- <input type="button" class="btn btn-info btn-sm"
									onClick="showDHR();" value='查看偏差' />  -->
							</div>


						</div>
						<div class="col-xs-5 col-sm-5" style="margin-top: 34px;">
							<input type="button" class="btn btn-info btn-sm"
								onClick="serchpici();" value='查询' /> 
								<input type="button" class="btn btn-info btn-sm" style="margin-right: 3px;" onClick="productReload();"
								value='重置' /><input type="button"
								class="btn btn-info btn-sm" onClick="serchTomorrow();"
								value='查看明天待生产' /> <input type="button"
								class="btn btn-info btn-sm" onClick="showDHR();" value='查看偏差' />
							<input type="button" class="btn btn-info btn-sm" onClick="Dy();"
								value='打印标签' />
								
							<!--   <button type="button" class="layui-btn layui-btn-normal layui-btn-radius">百搭按钮</button>
									     <button type="button" class="layui-btn layui-btn-normal layui-btn-radius">百搭按钮</button>
									      <button type="button" class="layui-btn layui-btn-normal layui-btn-radius">百搭按钮</button> -->

						</div>


					</div>
					<div class="row layui-form ipadmini" style="padding-left: 9px">
	                  <div class="col-xs-7 col-sm-7" style="margin-top: 34px;">
						<div id="infectionTypeG">
							<div class="input-group" id="infectionType" style="height: 34px;"ckGender">
								<span class="input-group-addon">感染类型</span> <input
									type="checkbox" value="Hbv" title="Hbv"> <input
									type="checkbox" value="Hcv" title="Hcv"> <input
									type="checkbox" value="Hbv,Hcv" title="Hbv,Hcv"> <input
									type="checkbox" value="无感染" title="无感染">
							</div>

						</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<input type="hidden"
							value="<s:property value="dnaTask.template.templateFieldsItemCode"/>"
							id="templateFieldsItemCode">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="main" style="font-size: 14px;">
						</table>
					</div>
					<%-- 	<div class="box-footer">
								<!--<div class="pull-left">
									<button type="button" class="btn btn-primary" id="makeUpSave"><i class="glyphicon glyphicon-random"></i> 提交
                </button>
								</div>-->
								<div class="pull-right">
						<button type="button" class="btn btn-primary" id="save" onclick="saveItem()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.save"/>
                </button>
								<button type="button" class="btn btn-primary" style="display: none" id="sp" onclick="sp()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.handle"/>
                </button>
									<button type="button" class="btn btn-primary" id="prev"><i class="glyphicon glyphicon-arrow-up"></i> <fmt:message key="biolims.common.back"/>
                </button>
									<button type="button" class="btn btn-primary"  id="finish"><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.workflow.completeName"/>
                </button>
								</div>

							</div> --%>
				</div>
			</div>
		</div>
		</section>
	</div>
	<input type="hidden" id="hide_input_date" />
	<input type="hidden" id="hide_input_dateend" />
	<input type="hidden" id="hide_input_dsccx" />
	<script type="text/javascript"
		src="${ctx}/js/experiment/productionPlan/productionPlanTable.js"></script>

</body>

</html>