﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/bobs/sj/bobsSj.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="bobsSj_id"
                   	 name="id" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="bobsSj_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
                   	
                   	</tr>
                   	<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="bobsSj_createUser"
				documentName="bobsSj_createUser_name" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="bobsSj_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="bobsSj_createUser" name="bobsSj.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >创建时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
                   	</tr>
                   	<tr>
               	 	<td class="label-title" >上机日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startsjDate" name="startsjDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="sjDate1" name="sjDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endsjDate" name="endsjDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="sjDate2" name="sjDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
                   	</tr>
                   	
			<g:LayOutWinTag buttonId="showtemplate" title="选择实验模板"
				hasHtmlFrame="true"
				html="${ctx}/system/template/template/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsSj_template').value=rec.get('id');
				document.getElementById('bobsSj_template_name').value=rec.get('name');" />
               	 	<td class="label-title" >实验模板</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="bobsSj_template_name" searchField="true"  name="template.name"  value="" class="text input" />
 						<input type="hidden" id="bobsSj_template" name="bobsSj.template.id"  value="" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	    	</td>
               	 	<td class="label-title" >状态名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="bobsSj_stateName"
                   	 name="stateName" searchField="true" title="状态名称"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsSj_acceptUser').value=rec.get('id');
				document.getElementById('bobsSj_acceptUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >实验组</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="bobsSj_acceptUser_name" searchField="true"  name="acceptUser.name"  value="" class="text input" />
 						<input type="hidden" id="bobsSj_acceptUser" name="bobsSj.acceptUser.id"  value="" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
		
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="bobsSj_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
<%-- 			<tr>
               	 	<td class="label-title"  style="display:none"  >状态</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="bobsSj_state"
                   	 name="state" searchField="true" title="状态"   style="display:none"    />
                   	
					
 
                  
                   		<g:LayOutWinTag buttonId="showbobsCross" title="选择杂交编号"
				hasHtmlFrame="true"
				html="${ctx}/experiment/bobs/sj/bobsCrossSelect.action"
				isHasSubmit="false" functionName="BobsCrossFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsSj_bobsCross').value=rec.get('id');
				document.getElementById('bobsSj_bobsCross_name').value=rec.get('name');" />
               	 	<td class="label-title" >杂交编号</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="bobsSj_bobsCross_name" searchField="true"  name="bobsCross.name"  value="" class="text input" />
 						<input type="hidden" id="bobsSj_bobsCross" name="bobsSj.bobsCross.id"  value="" > 
 						<img alt='选择杂交编号' id='showbobsCross' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	  
               
			</tr> --%>
            </table>
		</form>
		</div>
		<div id="show_bobsSj_div"></div>
   		<form name='excelfrm' action='${ctx}/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_bobsSj_tree_page"></div>
</body>
</html>



