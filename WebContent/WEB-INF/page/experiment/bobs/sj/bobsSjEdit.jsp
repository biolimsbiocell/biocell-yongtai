<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=bobsSj&id=${bobsSj.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/bobs/sj/bobsSjEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsSj_id"
                   	 name="bobsSj.id" title="编号" class="text input readonlytrue" readonly="readOnly" 
                   	   
	value="<s:property value="bobsSj.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsSj_name"
                   	 name="bobsSj.name" title="描述"
                   	   
	value="<s:property value="bobsSj.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/bobs/sj/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsSj_createUser').value=rec.get('id');
				document.getElementById('bobsSj_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsSj_createUser_name"  value="<s:property value="bobsSj.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="bobsSj_createUser" name="bobsSj.createUser.id"  value="<s:property value="bobsSj.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="bobsSj_createDate"
                   	 name="bobsSj.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:date name="bobsSj.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.sequencing.sequencingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="bobsSj_sjDate"
                   	 name="bobsSj.sjDate" title="上机日期"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="bobsSj.sjDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsSj_template_name"  value="<s:property value="bobsSj.template.name"/>"  />
 						<input type="hidden" id="bobsSj_template" name="bobsSj.template.id"  value="<s:property value="bobsSj.template.id"/>" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"  />                   		
                   	</td>
			</tr>
			<tr>
					<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
					hasHtmlFrame="true"
					html="${ctx}/core/userGroup/userGroupSelect.action"
					isHasSubmit="false" functionName="UserGroupFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('bobsSj_acceptUser').value=rec.get('id');
					document.getElementById('bobsSj_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsSj_acceptUser_name"  value="<s:property value="bobsSj.acceptUser.name"/>"   />
 						<input type="hidden" id="bobsSj_acceptUser" name="bobsSj.acceptUser.id"  value="<s:property value="bobsSj.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			
				<%-- <g:LayOutWinTag buttonId="showbobsCross" title="选择杂交编号"
				hasHtmlFrame="true"
				html="${ctx}/experiment/bobs/cross/bobsCross/bobsCrossSelect.action"
				isHasSubmit="false" functionName="BobsCrossFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsSj_bobsCross').value=rec.get('id');
				document.getElementById('bobsSj_bobsCross_name').value=rec.get('name');" /> --%>
				
               	 	<td class="label-title" ><fmt:message key="biolims.common.bobsHybridization"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="bobsSj_bobsCross_name"  value="<s:property value="bobsSj.bobsCross.name"/>"  />
 						<input type="text" id="bobsSj_bobsCross" name="bobsSj.bobsCross.id"  value="<s:property value="bobsSj.bobsCross.id"/>" > 
 						<img alt='选择BoBs杂交' id='showbobsCross' src='${ctx}/images/img_lookup.gif' class='detail' onclick="BobsCrossFun()"/>                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsSj_note"
                   	 name="bobsSj.note" title="备注"
                   	   
	value="<s:property value="bobsSj.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="bobsSj_state"
                   	 name="bobsSj.state" title="状态"
                   	   
	value="<s:property value="bobsSj.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsSj_stateName"
                   	 name="bobsSj.stateName" title="状态名称" class="text input readonlytrue" readonly="readOnly" 
                   	   
	value="<s:property value="bobsSj.stateName"/>"
                   	  />
                   	  
                   	</td>
			
					<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}
							<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="bobsSjItemJson" id="bobsSjItemJson" value="" />
            <input type="hidden" name="bobsSjTemplateJson" id="bobsSjTemplateJson" value="" />
            <input type="hidden" name="bobsSjReagentJson" id="bobsSjReagentJson" value="" />
            <input type="hidden" name="bobsSjCosJson" id="bobsSjCosJson" value="" />
            <input type="hidden" name="bobsSjResultJson" id="bobsSjResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="bobsSj.id"/>" />
            </form>
            <div id="bobsSjItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#bobsSjTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#bobsSjReagentpage"><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#bobsSjCospage"><fmt:message key="biolims.common.instrumentDetail"/></a></li>
           	</ul> 
			
			<div id="bobsSjTemplatepage" width="100%" height:10px></div>
			<div id="bobsSjReagentpage" width="100%" height:10px></div>
			<div id="bobsSjCospage" width="100%" height:10px></div>
			
			</div>
			<div id="bobsSjResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
