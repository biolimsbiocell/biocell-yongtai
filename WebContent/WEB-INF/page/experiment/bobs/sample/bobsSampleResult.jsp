﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/bobs/sample/bobsSampleResult.js"></script>
</head>
<body>
<form name='excelfrm' action='${ctx}/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	<div id="bobsSampleResultdiv"></div>
	<div id="batResult_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="fileResult-uploadcsv">上传CSV文件
	</div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>结果</span></td>
                <td><select id="result" style="width:100">
<!--                 		<option value="" <s:if test="result==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>>合格</option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>>不合格</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_submit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>是否提交</span></td>
                <td><select id="submit"  style="width:100">
<!--                 		<option value="" <s:if test="submit==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="submit==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="submit==0">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_infos_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>纯化后浓度</span></td>
				<td><input id="contraction"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>OD260/280</span></td>
				<td><input id="od280"/></td>
			</tr>
			<tr>
				<td class="label-title"><span>OD260/230</span></td>
				<td><input id="od230"/></td>
			</tr>
		</table>
		</div>
		<div id="many_bat_div" style="display: none">
		<div class="ui-widget;">
			<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_bat_text" style="width:650px;height: 339px"></textarea>
	</div>
</body>
</html>
