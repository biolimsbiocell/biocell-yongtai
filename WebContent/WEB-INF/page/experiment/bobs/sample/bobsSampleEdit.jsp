<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=bobsSample&id=${bobsSample.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/bobs/sample/bobsSampleEdit.js"></script>
<div style="float:left;width:25%" id="bobsSampleTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsSample_id"
                   	 name="bobsSample.id" title="编号"  class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="bobsSample.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsSample_name"
                   	 name="bobsSample.name" title="描述"
                   	   
	value="<s:property value="bobsSample.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/bobs/sample/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsSample_createUser').value=rec.get('id');
				document.getElementById('bobsSample_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsSample_createUser_name"  value="<s:property value="bobsSample.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="bobsSample_createUser" name="bobsSample.createUser.id"  value="<s:property value="bobsSample.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="bobsSample_createDate"
                   	 name="bobsSample.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="bobsSample.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="30" readonly="readOnly"  id="bobsSample_template_name"  value="<s:property value="bobsSample.template.name"/>"/>
 						<input type="hidden" id="bobsSample_template" name="bobsSample.template.id"  value="<s:property value="bobsSample.template.id"/>" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="TemplateFun()" />                   		
                   	</td>
                   	
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsSample_acceptUser').value=rec.get('id');
				document.getElementById('bobsSample_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsSample_acceptUser_name"  value="<s:property value="bobsSample.acceptUser.name"/>" />
 						<input type="hidden" id="bobsSample_acceptUser" name="bobsSample.acceptUser.id"  value="<s:property value="bobsSample.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="bobsSample_state"
                   	 name="bobsSample.state" title="状态"
                   	   
	value="<s:property value="bobsSample.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
                   	
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsSample_stateName"
                   	 name="bobsSample.stateName" title="状态名称"  class="text input readonlytrue" readonly="readOnly" 
                   	   
	value="<s:property value="bobsSample.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsSample_note"
                   	 name="bobsSample.note" title="备注"
                   	   
	value="<s:property value="bobsSample.note"/>"
                   	  />
                   	  
                   	</td>
					<td class="label-title"><input type="button" onclick="loadPic()" 
					value= <fmt:message key="biolims.report.checkFile"/> ><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>
							${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="bobsSampleItemJson" id="bobsSampleItemJson" value="" />
            <input type="hidden" name="bobsSampleTemplateJson" id="bobsSampleTemplateJson" value="" />
            <input type="hidden" name="bobsSampleReagentJson" id="bobsSampleReagentJson" value="" />
            <input type="hidden" name="bobsSampleCosJson" id="bobsSampleCosJson" value="" />
            <input type="hidden" name="bobsSampleResultJson" id="bobsSampleResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="bobsSample.id"/>" />
            </form>
            <div id="bobsSampleItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#bobsSampleTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#bobsSampleReagentpage"><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#bobsSampleCospage"><fmt:message key="biolims.common.instrumentDetail"/></a></li>
           	</ul> 
			
			<div id="bobsSampleTemplatepage" width="100%" height:10px></div>
			<div id="bobsSampleReagentpage" width="100%" height:10px></div>
			<div id="bobsSampleCospage" width="100%" height:10px></div>
			
			</div>
			<div id="bobsSampleResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
