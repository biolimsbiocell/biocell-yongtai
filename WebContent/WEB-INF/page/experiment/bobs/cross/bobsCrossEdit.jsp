<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=bobsCross&id=${bobsCross.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/bobs/cross/bobsCrossEdit.js"></script>
 <!-- <div style="float:left;width:25%" id="bobsCrossTemppage"></div> -->
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsCross_id"
                   	 name="bobsCross.id" title="编号" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="bobsCross.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsCross_name"
                   	 name="bobsCross.name" title="描述"
                   	   
	value="<s:property value="bobsCross.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/bobs/cross/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsCross_createUser').value=rec.get('id');
				document.getElementById('bobsCross_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsCross_createUser_name"  value="<s:property value="bobsCross.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="bobsCross_createUser" name="bobsCross.createUser.id"  value="<s:property value="bobsCross.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="bobsCross_createDate"
                   	 name="bobsCross.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"    value="<s:date name="bobsCross.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.testTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="bobsCross_washDate"
                   	 name="bobsCross.washDate" title="洗脱时间"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="bobsCross.washDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsCross_template_name"  value="<s:property value="bobsCross.template.name"/>" />
 						<input type="hidden" id="bobsCross_template" name="bobsCross.template.id"  value="<s:property value="bobsCross.template.id"/>" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="TemplateFun()" />                   		
                   	</td>
			</tr>
			<tr>
			
			
			
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsCross_acceptUser').value=rec.get('id');
				document.getElementById('bobsCross_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="bobsCross_acceptUser_name"  value="<s:property value="bobsCross.acceptUser.name"/>"  />
 						<input type="hidden" id="bobsCross_acceptUser" name="bobsCross.acceptUser.id"  value="<s:property value="bobsCross.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
			
				<%-- <g:LayOutWinTag buttonId="showbobsSample" title="选择样本处理"
				hasHtmlFrame="true"
				html="${ctx}/experiment/bobs/sample/bobsSample/bobsSampleSelect.action"
				isHasSubmit="false" functionName="BobsSampleFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsCross_bobsSample').value=rec.get('id');
				document.getElementById('bobsCross_bobsSample_name').value=rec.get('name');" /> --%>
				
               	 	<td class="label-title" ><fmt:message key="biolims.common.bobsSampleHandle"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="30" maxlength="50" id="bobsCross_bobsSampleName"
                   	 name="bobsCross.bobsSampleName" title="样本处理"
                   	   
	value="<s:property value="bobsCross.bobsSampleName"/>"/>
		<input type="text" size="30" maxlength="50" id="bobsCross_bobsSampleId"
                   	 name="bobsCross.bobsSampleId" title="样本处理"
                   	   
	value="<s:property value="bobsCross.bobsSampleId"/>"/>
 						
 						<img alt='选择样本处理' id='showbobsSample' src='${ctx}/images/img_lookup.gif' class='detail' onclick="BobsSampleFun()"/>                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsCross_note"
                   	 name="bobsCross.note" title="备注"
                   	   
	value="<s:property value="bobsCross.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="bobsCross_state"
                   	 name="bobsCross.state" title="状态"
                   	   
	value="<s:property value="bobsCross.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsCross_stateName"
                   	 name="bobsCross.stateName" title="工作流状态"  class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="bobsCross.stateName"/>"
                   	  />
                   	  
                   	</td>

					<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}
							<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="bobsCrossItemJson" id="bobsCrossItemJson" value="" />
            <input type="hidden" name="bobsCrossTemplateJson" id="bobsCrossTemplateJson" value="" />
            <input type="hidden" name="bobsCrossReagentJson" id="bobsCrossReagentJson" value="" />
            <input type="hidden" name="bobsCrossCosJson" id="bobsCrossCosJson" value="" />
            <input type="hidden" name="bobsCrossResultJson" id="bobsCrossResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="bobsCross.id"/>" />
            </form>
            <div id="bobsCrossItempage" width="100%" height:10px></div>
            <div id="tabs" style="display:none">
            <ul>
			<li><a href="#bobsCrossTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#bobsCrossReagentpage"><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#bobsCrossCospage"><fmt:message key="biolims.common.instrumentDetail"/></a></li>
           	</ul> 
			
			<div id="bobsCrossTemplatepage" width="100%" height:10px style="display:none"></div>
			<div id="bobsCrossReagentpage" width="100%" height:10px style="display:none"></div>
			<div id="bobsCrossCospage" width="100%" height:10px style="display:none"></div>
			
<!-- 			<div id="bobsCrossTemppage" width="100%" height:10px></div> -->
			</div>
			<div id="bobsCrossResultpage" width="100%" height:10px style="display:none"></div>
        	</div>
	</body>
	</html>
