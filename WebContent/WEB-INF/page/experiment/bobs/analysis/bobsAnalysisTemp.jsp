﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/bobs/analysis/bobsAnalysisTemp.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<input type="hidden" id="extJsonDataString" name="extJsonDataString">
	<!-- <form id="searchForm"> -->
	<table cellspacing="0" cellpadding="0" class="toolbarsection">
		<tr>
			<td class="text label" ><label>&nbsp;&nbsp;&nbsp;<fmt:message key="biolims.common.sampleCode"/>&nbsp;&nbsp;&nbsp;&nbsp;</label>
			</td>
			<td>
				<input type="text" id="sampleCode" name="sampleCode" searchField="true" >
			</td>
			<td>
				<input type="button" onClick="selectWaitSamples()" style="font-weight:bold;"
				 value=<fmt:message key="biolims.common.find"/>>
			</td>
		</tr>
	</table>
	<div id="bobsAnalysisTempdiv"></div>
	
	<div id="many_bat_div" style="display: none">
			<div class="ui-widget;">
				<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
				</div>
			</div>
			<textarea id="many_bat_text" style="width:650px;height: 339px"></textarea>
		</div>
	
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
</body>
</html>
