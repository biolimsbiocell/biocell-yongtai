<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=bobsAnalysis&id=${bobsAnalysis.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/bobs/analysis/bobsAnalysisEdit.js"></script>
<div style="float:left;width:25%" id="bobsAnalysisTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsAnalysis_id"
                   	 name="bobsAnalysis.id" title="编号"  class="text input readonlytrue" readonly="readOnly" 
                   	   
	value="<s:property value="bobsAnalysis.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsAnalysis_name"
                   	 name="bobsAnalysis.name" title="描述"
                   	   
	value="<s:property value="bobsAnalysis.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/bobs/analysis/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsAnalysis_createUser').value=rec.get('id');
				document.getElementById('bobsAnalysis_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsAnalysis_createUser_name"  value="<s:property value="bobsAnalysis.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="bobsAnalysis_createUser" name="bobsAnalysis.createUser.id"  value="<s:property value="bobsAnalysis.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="bobsAnalysis_createDate"
                   	 name="bobsAnalysis.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="bobsAnalysis.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="bobsAnalysis_sjDate"
                   	 name="bobsAnalysis.sjDate" title="上机日期"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="bobsAnalysis.sjDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 <%-- 	<td class="label-title" >实验模板</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsAnalysis_template_name"  value="<s:property value="bobsAnalysis.template.name"/>"/>
 						<input type="hidden" id="bobsAnalysis_template" name="bobsAnalysis.template.id"  value="<s:property value="bobsAnalysis.template.id"/>" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
				<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('bobsAnalysis_acceptUser').value=rec.get('id');
				document.getElementById('bobsAnalysis_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsAnalysis_acceptUser_name"  value="<s:property value="bobsAnalysis.acceptUser.name"/>" />
 						<input type="hidden" id="bobsAnalysis_acceptUser" name="bobsAnalysis.acceptUser.id"  value="<s:property value="bobsAnalysis.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>			
					<%-- <g:LayOutWinTag buttonId="showbobsSj" title="选择上机编号"
					hasHtmlFrame="true"
					html="${ctx}/experiment/bobs/sj/bobsSj/bobsSjSelect.action"
					isHasSubmit="false" functionName="BobsSjFun" 
	 				hasSetFun="true"
					extRec="rec"
					extStr="document.getElementById('bobsAnalysis_bobsSj').value=rec.get('id');
					document.getElementById('bobsAnalysis_bobsSj_name').value=rec.get('name');" /> --%>
				
               <%-- 	 	<td class="label-title" >BoBs上机</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="bobsAnalysis_bobsSj_name"  value="<s:property value="bobsAnalysis.bobsSj.name"/>"/>
 						<input type="hidden" id="bobsAnalysis_bobsSj" name="bobsAnalysis.bobsSj.id"  value="<s:property value="bobsAnalysis.bobsSj.id"/>" > 
 						<img alt='选择Bobs上机' id='showbobsSj' src='${ctx}/images/img_lookup.gif' class='detail' onclick="BobsSjFun()"/>                   		
                   	</td> --%>
			
			<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="bobsAnalysis_stateName"
                   	 name="bobsAnalysis.stateName" title="状态名称"  class="text input readonlytrue" readonly="readOnly" 
                   	   
	value="<s:property value="bobsAnalysis.stateName"/>"
                   	  />
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="bobsAnalysis_note"
                   	 name="bobsAnalysis.note" title="备注"
                   	   
	value="<s:property value="bobsAnalysis.note"/>"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="bobsAnalysis_state"
                   	 name="bobsAnalysis.state" title="状态"
                   	   
	value="<s:property value="bobsAnalysis.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>
							${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span>
               	 	
			</tr>
			<tr>
			
			</tr>
			
			
            </table>
            <input type="hidden" name="bobsAnalysisResultJson" id="bobsAnalysisResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="bobsAnalysis.id"/>" />
            </form>
            <div id="tabs">
            <ul>
<!-- 			<li><a href="#bobsAnalysisResultpage">BoBss分析结果</a></li> -->
           	</ul> 
			<div id="bobsAnalysisResultpage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
