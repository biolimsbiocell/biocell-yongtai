<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}

#yesSpan {
	display: none;
}

#auditDiv {
	display: none;
}

#btn_submit {
	display: none;
}

#btn_changeState {
	display: none;
}

.sampleNumberInput {
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 200px;
    top: 212px;
}

.sampleNumberInputTow{
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 510px;
    top: 212px;
}

.sampleNumberDiv {
    position: absolute;
    left: 140px;
    top: 223px;
}
.sampleNumberDivtow {
    position: absolute;
	left: 466px;
    top: 223px;
}

.sampleNumberbutton {
    position: absolute;
    left: 760px;
    top: 212px;
    border-radius: 4px;
    border: 1px solid #ccc;
    background-color: #5cb85c;
    width: 38px;
    height: 36px;
}

</style>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						传递窗管理
					</h3>
					<small style="color: #fff;"> 创建人: <span
						id="transferWindowManage_createUser"><s:property
								value=" transferWindowManage.createUser.name" /></span> 创建时间: <span
						id="transferWindowManage_createDate"><s:date
								name="transferWindowManage.createDate" format="yyyy-MM-dd" /></span> 状态: <span id="transferWindowManage_state"><s:property
								value="transferWindowManage.stateName" /></span> 
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
								<input type="hidden" id="createUser_id" name="transferWindowManage.createUser.id"
								value="<s:property value=" transferWindowManage.createUser.id "/>" />
								<input type="hidden" id="createDate" name="transferWindowManage.createDate"
								value="<s:property value=" transferWindowManage.createDate "/>" />
								<input type="hidden" id="state" name="transferWindowManage.state"
								value="<s:property value=" transferWindowManage.state "/>" />
								<input type="hidden" id="stateName" name="transferWindowManage.stateName"
								value="<s:property value=" transferWindowManage.stateName "/>" />
							<input type="hidden" id="bpmTaskId"
								value="${requestScope.bpmTaskId}" /> <br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 传递窗编号
										</span> <input list="sampleIdOne"
											changelog="<s:property value="transferWindowManage.id"/>" type="text"
											size="20" maxlength="25" id="transferWindowManage_id"
											name="transferWindowManage.id" class="form-control"
											title="传递窗编号"
											value="<s:property value=" transferWindowManage.id "/>" /> 
											<input type="hidden"
											size="20" maxlength="25" id="transferWindowManage_id_id"
											value="<s:property value="transferWindowManage.id"/>" /> <span
											class="input-group-btn">
										</span> 
									</div>
								</div>
								
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 传递窗名称
										</span> <input list="sampleName"
											changelog="<s:property value="transferWindowManage.transferWindowName"/>"
											type="text" size="20" maxlength="50" id="transferWindowManage_transferWindowName"
											name="transferWindowManage.transferWindowName" class="form-control"
											title="传递窗名称"
											value="<s:property value=" transferWindowManage.transferWindowName "/>" />

									</div>
								</div>
								<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">区域 <img class='requiredimage' src='/images/required.gif' /></span> 
											<input list="sampleName" changelog="<s:property value="transferWindowManage.regionalManagement.id"/>"
												type="hidden"  maxlength="50" id="transferWindowManage_regionalManagement_id"
												name="transferWindowManage.regionalManagement.id" class="form-control"
												value="<s:property value=" transferWindowManage.regionalManagement.id "/>" />
											<input list="sampleName" changelog="<s:property value="transferWindowManage.regionalManagement.name"/>"
												type="text" readonly="readonly"  size="20" maxlength="50" id="transferWindowManage_regionalManagement_name"
												 class="form-control"
												value="<s:property value=" transferWindowManage.regionalManagement.name "/>" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"
													onClick="showRegionalManagement()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">
												传递窗状态</span> 
											 <select
													class="form-control" id="transferWindowManage_transferWindowState" name="transferWindowManage.transferWindowState">
											<option value="1"
												<s:if test='transferWindowManage.transferWindowState=="1"'>selected="selected"</s:if>>
												可以使用</option>
											<option value="0"
												<s:if test='transferWindowManage.transferWindowState=="0"'>selected="selected"</s:if>>
												不可以使用</option>
										</select>
									</div>
								</div>
								<%-- <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">
												占用状态</span> 
											 <select class="form-control" id="transferWindowManage_isFull" name="transferWindowManage.isFull">
											<option value="0"
												<s:if test='transferWindowManage.isFull=="0"'>selected="selected"</s:if>>
												未占用</option>
											<option value="1"
												<s:if test='transferWindowManage.isFull=="1"'>selected="selected"</s:if>>
												占用</option>
										</select>
									</div>
								</div> --%>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 备注 
										</span> <input list="sampleName"
											changelog="<s:property value="transferWindowManage.name"/>"
											type="text" size="20" maxlength="50" id="transferWindowManage_name"
											name="transferWindowManage.name" class="form-control"
											title="备注"
											value="<s:property value=" transferWindowManage.name "/>" />

									</div>
								</div>
							</div>
							<div class="row">
							<%-- <div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 传递窗面积㎡
										</span> <input list="sampleName"
											changelog="<s:property value="transferWindowManage.centiare"/>"
											type="text" size="20" maxlength="50" id="transferWindowManage_centiare"
											name="transferWindowManage.centiare" class="form-control"
											title="传递窗面积㎡"
											value="<s:property value=" transferWindowManage.centiare "/>" />

									</div>
								</div>
							<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 传递窗体积m³
										</span> <input list="sampleName"
											changelog="<s:property value="transferWindowManage.stere"/>"
											type="text" size="20" maxlength="50" id="transferWindowManage_stere"
											name="transferWindowManage.stere" class="form-control"
											title="传递窗面积㎡"
											value="<s:property value=" transferWindowManage.stere "/>" />

									</div>
								</div> --%>
							
							
							
								<div class="col-md-8 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">描述</span> 
                   						<textarea   id="transferWindowManage_describe" name="transferWindowManage.describe" placeholder="长度限制2000个字符"    class="form-control"   
											  style="overflow: hidden; width: 420px; height: 80px;"  ><s:property value="transferWindowManage.describe"/></textarea>  
									</div>
								</div>
							</div>

							<input type="hidden" id="id_parent_hidden"
								value="<s:property value=" transferWindowManage.id "/>" /> 
							<input type="hidden"  id="changeLog" name="changeLog"
								 /> 

						</form>
					</div>
				</div>
				<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="transferWindowStateTable" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/transferWindowManage/transferWindowManageEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/transferWindowManage/transferWindowState.js"></script>
</body>
</html>