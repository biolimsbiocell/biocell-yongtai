<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<!-- 读取哪一个资源文件 -->
		<fmt:setBundle basename="ResouseInternational/msg" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>
	</head>

	<body style="height:94%">
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
		 <input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" /> 
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title"><fmt:message key="biolims.common.taskAllocation"/>
									<small style="color:#fff;">
										<fmt:message key="biolims.common.taskId"/>: <span id="qpcrTask_id"><s:property value="qpcrTask.id"/></span>
										<fmt:message key="biolims.common.commandPerson"/>: <span userId="<s:property value="qpcrTask.createUser.id"/>"  id="qpcrTask_createUser"><s:property value="qpcrTask.createUser.name"/></span>
										<fmt:message key="biolims.sample.createDate"/>: <span id="qpcrTask_createDate"><s:property  value="qpcrTask.createDate"/></span>
										<fmt:message key="biolims.common.state"/>: <span state="<s:property value="qpcrTask.state"/>"  id="qpcrTask_state"><s:property value="qpcrTask.stateName"/></span>
									</small>
								</h3>
							</div>
							<div class="box-body">
								<!--待处理样本-->
								<div class="col-md-8 col-xs-12">
									<div class="box box-success">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.common.pendingSample"/></h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<span><input type="text" width="0.2px" style="background-color:EAB9A8;width:30px" readonly="readonly"/></span><fmt:message key="biolims.common.cancelChange"/>&nbsp;&nbsp;&nbsp;
											<span><input type="text" width="0.2px" style="background-color:30B573;width:30px" readonly="readonly"/></span><fmt:message key="biolims.common.editChange"/>&nbsp;&nbsp;&nbsp;
											<span><input type="text" width="0.2px" style="background-color:8FAAC6;width:30px" readonly="readonly"/></span><fmt:message key="biolims.common.addChange"/>&nbsp;&nbsp;&nbsp;
										</div>
										<div class="box-body">
											<table class="table table-hover table-striped table-bordered table-condensed" id="main" style="font-size:14px;"></table>
										</div>
									</div>
								</div>
								<div class="col-md-4 col-xs-12">
									<!--SOP-->
									<div class="box box-primary">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.common.note"/></h3>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<div class="input-group">
												<span class="input-group-addon bg-aqua"><fmt:message key="biolims.common.describe"/></span>
												<input type="text" id="qpcrTask_name" class="form-control" placeholder="<fmt:message key="biolims.common.placeholder"/>" value="<s:property value=" qpcrTask.name "/>">
											</div>
											<div class="input-group" style="margin-top:10px">
												<span class="input-group-addon bg-aqua"><fmt:message key="biolims.common.containerQuantity"/></span>
												<input type="number" id="maxNum" class="form-control" placeholder="<fmt:message key="biolims.common.placeholder"/>" value="<s:property value=" qpcrTask.maxNum "/>">
												<span class="input-group-addon"><fmt:message key="biolims.common.num"/></span>
											</div>
										</div>

									</div>

									<!--SOP-->
									<div class="box box-primary">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.common.chooseExperimentalTemplate"/></h3>
										</div>
										<!-- /.box-header -->
										<div class="box-body">
											<ul class="todo-list sopBody" style="max-height: 164px;">
												<c:forEach var="selSop" items="${selTemplate}">
													<input type="hidden" id="sopChangelogId" value="${selSop.id}" />
													<input type="hidden" id="sopChangelogName" value="${selSop.name}" />
													<input type="hidden" id="sopChangelogSampleNum" value="<s:property value="bloodSplit.sampleNum"/>" />
												
													<li class="sopLi sopChosed" sopid="${selSop.id}" suerGroup="${selSop.acceptUser.id}">
														<span>
								                        <i class="fa fa-ellipsis-v"></i>
								                        <i class="fa fa-ellipsis-v"></i>
								                    </span>
														<span class="text">${selSop.name}</span>
														<small class="label label-primary">${selSop.storageContainer.name}</small>
														<span class="badge label-warning selectednum"><s:property value="qpcrTask.sampleNum"/></span>
													</li>
												</c:forEach>
													<input type="hidden" id="sopChangelogId" value="" />
													<input type="hidden" id="sopChangelogName" value="" />
													<input type="hidden" id="sopChangelogSampleNum" value="" />
												
												<c:forEach var="sop" items="${template}">
													<li class="sopLi" sopid="${sop.id}" suerGroup="${sop.acceptUser.id}">
														<span>
								                        <i class="fa fa-ellipsis-v"></i>
								                        <i class="fa fa-ellipsis-v"></i>
								                    </span>
														<span class="text">${sop.name}</span>
														<small class="label label-primary">${sop.storageContainer.name}</small>
														<span class="badge label-warning"></span>
													</li>
												</c:forEach>
											</ul>
										
										</div>

									</div>

									<!--实验员-->
									<div class="box box-info">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title"><fmt:message key="biolims.common.chooseTheExperimenter"/></h3>
										</div>
										<div class="box-body no-padding">
											<ul class="users-list clearfix">
												<c:forEach var="selUser" items="${selUser}">
													<input type="hidden" class="changelogUserid"  value="${selUser.user.id}" />
													<input type="hidden" class="changelogUsername"  value="${selUser.user.name}" />
												
													<li class="userLi" userid="${selUser.user.id}" suerGroup="${selUser.userGroup.id}">
														<img class="userChosed" src="${ctx}/lims/dist/img/testuser.png" alt="User Image">
														<a class="users-list-name" href="#">${selUser.user.name}</a>
													</li>
												</c:forEach>
												<c:forEach var="user" items="${user}">
													<li class="userLi" userid="${user.user.id}" suerGroup="${user.userGroup.id}">
														<img src="${ctx}/lims/dist/img/testuser.png" alt="User Image">
														<a class="users-list-name" href="#">${user.user.name}</a>
													</li>
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>

							</div>
							<div class="box-footer">
								<div class="pull-right">
									<button type="button" class="btn btn-primary" id="save"><i class="glyphicon glyphicon-random"></i><fmt:message key="biolims.common.save"/>
                </button>
									<button type="button" class="btn btn-primary" id="next"><i class="glyphicon glyphicon-arrow-down"></i><fmt:message key="biolims.common.nextStep"/>
                </button>
								</div>

							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<script type="text/javascript" src="${ctx}/js/experiment/qpcr/qpcrTaskAllot.js"></script>
	</body>

</html>