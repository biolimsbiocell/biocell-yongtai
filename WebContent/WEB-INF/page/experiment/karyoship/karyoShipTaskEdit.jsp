<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=karyoShipTask&id=${karyoShipTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/karyoship/karyoShipTaskEdit.js"></script>
 <div style="float:left;width:30%" id="karyoShipTaskTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="user" value="${requestScope.user2}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoShipTask_id"
                   	 name="karyoShipTask.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="karyoShipTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoShipTask_name"
                   	 name="karyoShipTask.name" title="描述"
                   	   
	value="<s:property value="karyoShipTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/karyo/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyoShipTask_createUser').value=rec.get('id');
				document.getElementById('karyoShipTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyoShipTask_createUser_name"  value="<s:property value="karyoShipTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="karyoShipTask_createUser" name="karyoShipTask.createUser.id"  value="<s:property value="karyoShipTask.createUser.id"/>" > 
 						<%-- <img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.createDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoShipTask_createDate"
                   	 name="karyoShipTask.createDate" title="创建时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="karyoShipTask.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
				<%-- <g:LayOutWinTag buttonId="showsampleType" title="选择样本类型"
				hasHtmlFrame="true"
				html="${ctx}/sample/dicSampleType/dicSampleTypeSelect.action"
				isHasSubmit="false" functionName="DicSampleTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyoShipTask_sampleType').value=rec.get('id');
				document.getElementById('karyoShipTask_sampleType_name').value=rec.get('name');" /> --%>
				
			
			
               	 	<%-- <td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyoShipTask_sampleType_name"  value="<s:property value="karyoShipTask.sampleType.name"/>"/>
 						<input type="hidden" id="karyoShipTask_sampleType" name="karyoShipTask.sampleType.id"  value="<s:property value="karyoShipTask.sampleType.id"/>" > 
 						<img alt='选择样本类型' id='showsampleType' src='${ctx}/images/img_lookup.gif' class='detail' onclick="loadTestDicSampleType()"/>                   		
                   	</td> --%>
			
			
			
				<%-- <g:LayOutWinTag buttonId="showtemplate" title="选择实验模板"
				hasHtmlFrame="true"
				html="${ctx}/system/template/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyoShipTask_template').value=rec.get('id');
				document.getElementById('karyoShipTask_template_name').value=rec.get('name');" /> --%>
           	 	<td class="label-title" ><fmt:message key="biolims.common.choseTamplate"/></td>
           	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
               	<td align="left"  >
					<input type="text" size="20" readonly="readOnly"  id="karyoShipTask_template_name"  value="<s:property value="karyoShipTask.template.name"/>" readonly="readOnly"  />
					<input type="hidden" id="karyoShipTask_template" name="karyoShipTask.template.id"  value="<s:property value="karyoShipTask.template.id"/>" > 
					<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' class='detail'  onclick="TemplateFun()"/>                   		
               	</td>
               	
               	<g:LayOutWinTag buttonId="showAcceptUser" title="Selection of experimental group"
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('karyoShipTask_acceptUser').value=rec.get('id'); 
					document.getElementById('karyoShipTask_acceptUser_name').value=rec.get('name');" /> 
			
					<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyoShipTask_acceptUser_name"  value="<s:property value="karyoShipTask.acceptUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="karyoShipTask_acceptUser" name="karyoShipTask.acceptUser.id"  value="<s:property value="karyoShipTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.confirmDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoShipTask_confirmDate"
                   	 name="karyoShipTask.confirmDate" title="完成时间"
                   	   class="text input readonlytrue" readonly="readOnly"
					value="<s:property value="karyoShipTask.confirmDate"/>"
                   	  /> 
                   	</td> 
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="karyoShipTask_state"
                   	 name="karyoShipTask.state" title="状态"
                   	   
	value="<s:property value="karyoShipTask.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyoShipTask_stateName"
                   	 name="karyoShipTask.stateName" title="状态名称"
                   	   class="text input readonlytrue" readonly="readOnly"
	value="<s:property value="karyoShipTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="50" id="karyoShipTask_note"
                   	 name="karyoShipTask.note" title="备注"
                   	   
	value="<s:property value="karyoShipTask.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>                   	
					<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">
							<fmt:message key="biolims.common.allHave"/>${requestScope.fileNum}
							<fmt:message key="biolims.common.total"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="karyoShipTaskItemJson" id="karyoShipTaskItemJson" value="" />
            <input type="hidden" name="karyoShipTaskTemplateJson" id="karyoShipTaskTemplateJson" value="" />
            <input type="hidden" name="karyoShipTaskReagentJson" id="karyoShipTaskReagentJson" value="" />
            <input type="hidden" name="karyoShipTaskCosJson" id="karyoShipTaskCosJson" value="" />
            <input type="hidden" name="karyoShipTaskResultJson" id="karyoShipTaskResultJson" value="" />
<!--             <input type="hidden" name="karyoShipTaskTempJson" id="karyoShipTaskTempJson" value="" /> -->
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="karyoShipTask.id"/>" />
            </form>
            <div id="karyoShipTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#karyoShipTaskTemplatepage"><fmt:message key="biolims.common.templateDetail"/></a></li>
			<li><a href="#karyoShipTaskReagentpage" ><fmt:message key="biolims.common.reagentDetail"/></a></li>
			<li><a href="#karyoShipTaskCospage" ><fmt:message key="biolims.common.instrumentDetail"/></a></li>
           	</ul> 
			<div id="karyoShipTaskTemplatepage" width="100%" height:10px></div>
			<div id="karyoShipTaskReagentpage" width="100%" height:10px></div>
			<div id="karyoShipTaskCospage" width="100%" height:10px></div>
			</div>
			<div id="karyoShipTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
