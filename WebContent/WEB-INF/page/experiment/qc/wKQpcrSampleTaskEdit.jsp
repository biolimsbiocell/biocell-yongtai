<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=wKQpcr&id=${wKQpcrSampleTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKQpcrSampleTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="wKQpcrTempPage">
		<!-- <div id="tabs1">
            <ul>
	            <li><a href="#linchuang">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="linchuang" style="width: 100%;height: 10px;"></div>
				<div id="keji" style="width: 100%;height: 10px;"></div>
		 </div> -->

</div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="hidden" size="40" id="type"  value="qcQpcr" />
                   	<input type="text" size="20" maxlength="25" id="wKQpcrSampleTask_id"
                   	 name="wKQpcrSampleTask.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="wKQpcrSampleTask.id"/>"
                   	  />
                   	  <input type="hidden" size="30" maxlength="60" id="wKQpcrSampleTask_maxNum" name="wKQpcrSampleTask.maxNum" title="容器数量"
							value="<s:property value="wKQpcrSampleTask.maxNum"/>"
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wKQpcrSampleTask_name"
                   	 name="wKQpcrSampleTask.name" title="描述"
                   	   
	value="<s:property value="wKQpcrSampleTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQpcrSampleTask_releasUser_name"  value="<s:property value="wKQpcrSampleTask.releasUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wKQpcrSampleTask_releasUser" name="wKQpcrSampleTask.releasUser.id"  value="<s:property value="wKQpcrSampleTask.releasUser.id"/>" > 
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wKQpcrSampleTask_releaseDate"
                   	 name="wKQpcrSampleTask.releaseDate" title="下达日期"
                   	   readonly = "readOnly" class="text input readonlytrue" 
                   	     value="<s:property value="wKQpcrSampleTask.releaseDate"/>"
                   	  />
                   	</td>
			
			
			<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="wKQpcrSampleTask_confirmDate"
                   	 name="wKQpcrSampleTask.confirmDate" title="下达时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:date name="wKQpcrSampleTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
				

					
			<%-- <g:LayOutWinTag buttonId="showtestUser" title="选择实验员"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="wKQpcrSampleTask_testUser"
				documentName="wKQpcrSampleTask_testUser_name" />
				
			
			
               	 	<td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQpcrSampleTask_testUser_name"  value="<s:property value="wKQpcrSampleTask.testUser.name"/>" class="text input readonlytrue" readonly="readOnly" />
 						<input type="hidden" id="wKQpcrSampleTask_testUser" name="wKQpcrSampleTask.testUser.id"  value="<s:property value="wKQpcrSampleTask.testUser.id"/>" > 
 						<img alt='选择实验员' id='showtestUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wKQpcrSampleTask_testDate"
                   	 name="wKQpcrSampleTask.testDate" title="实验日期"
                   	   readonly = "readOnly" 
                   	   Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  
                   	   value="<s:date name="wKQpcrSampleTask.testDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
               	 	
                   	
			</tr>
			<tr>
			<td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQpcrSampleTask_template_name"  value="<s:property value="wKQpcrSampleTask.template.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wKQpcrSampleTask_template" name="wKQpcrSampleTask.template.id"  value="<s:property value="wKQpcrSampleTask.template.id"/>" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="TemplateFun()" />                   		
                   	</td>
                   	<!--<td class="label-title" >INDEX</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wKQpcrSampleTask_indexa"
                   	 name="wKQpcrSampleTask.indexa" title="INDEX"
                   	   class="text input"  value="<s:property value="wKQpcrSampleTask.indexa"/>"
                   	  />
                   	</td>-->
			
			
					<g:LayOutWinTag buttonId="showAcceptUser" title="选择实验组"
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('wKQpcrSampleTask_acceptUser').value=rec.get('id'); 
					document.getElementById('wKQpcrSampleTask_acceptUser_name').value=rec.get('name');" /> 
			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQpcrSampleTask_acceptUser_name"  value="<s:property value="wKQpcrSampleTask.acceptUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wKQpcrSampleTask_acceptUser" name="wKQpcrSampleTask.acceptUser.id"  value="<s:property value="wKQpcrSampleTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			
			
					<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.stateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="wKQpcrSampleTask_state"
                   	 name="wKQpcrSampleTask.state" title="状态id"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wKQpcrSampleTask.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
                   		  <td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQpcrSampleTask_testUser_name"  value="<s:property value="wKQpcrSampleTask.testUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wKQpcrSampleTask_testUser" name="wKQpcrSampleTask.testUser.id"  value="<s:property value="wKQpcrSampleTask.testUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
                   	
               	 
            </tr>
            <tr>
            	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wKQpcrSampleTask_stateName"
                   	 name="wKQpcrSampleTask.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wKQpcrSampleTask.stateName"/>"
                   	  />
                   	  
                   	  
                   	</td>
                   	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span></td>
			</tr>
			
			
            </table>
            <input type="hidden" name="wKQpcrItemJson" id="wKQpcrItemJson" value="" />
            <input type="hidden" name="qcQpcrTaskItemmJson" id="qcQpcrTaskItemmJson" value="" />
            <input type="hidden" name="wKQpcrTemplateJson" id="wKQpcrTemplateJson" value="" />
            <input type="hidden" name="wKQpcrDeviceJson" id="wKQpcrDeviceJson" value="" />
            <input type="hidden" name="wKQpcrSampleInfoJson" id="wKQpcrSampleInfoJson" value="" />
             <input type="hidden" name="sampleQcPoolingInfoJson" id="sampleQcPoolingInfoJson" value="" />
            <input type="hidden" name="wKQpcrReagentJson" id="wKQpcrReagentJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wKQpcrSampleTask.id"/>" />
            </form>
           <!--  <div id="wKQpcrItempage" width="100%" height:10px></div> -->
            <div id="tabs_1">
            <ul>
				<li><a href="#wKQpcrItempage"><fmt:message key="biolims.common.qualityQcontrolDetail"/></a></li>
<!-- 				<li><a href="#qcQpcrTaskItemmpage">Pooling明细</a></li> -->
           	</ul> 
			
				<div id="wKQpcrItempage" width="100%" height:10px></div>
			<%-- 	<% 
			String maxNum = (String)request.getParameter("maxNum");
			System.out.print(maxNum);
			if(maxNum!=null && !maxNum.equals("")){
			Integer max = Integer.parseInt(maxNum);
			for(int i=0;i<max;i++){
			%> --%>
			<div id = '<%="3d_image0" %>'></div>
			<div id = '<%="3d_image1" %>'></div>
<!-- 				<div id="qcQpcrTaskItemmpage" width="100%" height:10px></div> -->
			
			</div>
            <div id="tabs_2">
            <ul>
				<li><a href="#wKQpcrTemplatepage"><fmt:message key="biolims.common.performStepsInDetail"/></a></li>
				<li><a href="#wKQpcrReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
				<li><a href="#wKQpcrDevicepage" onClick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
			
			<div id="wKQpcrTemplatepage" width="100%" height:10px></div>
			<div id="wKQpcrReagentpage" width="100%" height:10px></div>
			<div id="wKQpcrDevicepage" width="100%" height:10px></div>
			
			</div>
			<div id="tabs_3">
            <ul>
				<li><a href="#wKQpcrSampleInfopage"><fmt:message key="biolims.common.qualityQcontrolResultDetail"/></a></li>
<!-- 				<li><a href="#sampleQcPoolingInfopage">Pooling结果明细</a></li> -->
           	</ul> 
			
				<div id="wKQpcrSampleInfopage" width="100%" height:10px></div>
<!-- 				<div id="sampleQcPoolingInfopage" width="100%" height:10px></div> -->
			
			</div>
			<!-- <div id="wKQpcrSampleInfopage" width="100%" height:10px></div> -->
        	</div>
	</body>
	</html>
