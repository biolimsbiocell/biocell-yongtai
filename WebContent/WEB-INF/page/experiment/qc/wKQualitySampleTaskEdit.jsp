﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=wKQuality&id=${wKQualitySampleTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKQualitySampleTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="wKQualityTempPage">
		<!-- <div id="tabs1">
            <ul>
	            <li><a href="#linchuang">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="linchuang" style="width: 100%;height: 10px;"></div>
				<div id="keji" style="width: 100%;height: 10px;"></div>
		 </div> -->

</div>

  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left">
                   	<input type="hidden" size="40" id="type"  value="qc2100" />
                   	<input type="text" size="20" maxlength="25" id="wKQualitySampleTask_id"
                   	 name="wKQualitySampleTask.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="wKQualitySampleTask.id"/>"
                   	  />
                   	  <input type="hidden" size="30" maxlength="60" id="wKQualitySampleTask_maxNum" name="wKQualitySampleTask.maxNum" title="容器数量"
							value="<s:property value="wKQualitySampleTask.maxNum"/>"
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wKQualitySampleTask_name"
                   	 name="wKQualitySampleTask.name" title="描述"
                   	   
	value="<s:property value="wKQualitySampleTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQualitySampleTask_releasUser_name"  value="<s:property value="wKQualitySampleTask.releasUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wKQualitySampleTask_releasUser" name="wKQualitySampleTask.releasUser.id"  value="<s:property value="wKQualitySampleTask.releasUser.id"/>" > 
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wKQualitySampleTask_releaseDate"
                   	 name="wKQualitySampleTask.releaseDate" title="下达日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	     value="<s:property value="wKQualitySampleTask.releaseDate"/>"
                   	  />
                   	</td>
			
			<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="wKQualitySampleTask_confirmDate"
                   	 name="wKQualitySampleTask.confirmDate" title="下达时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:date name="wKQualitySampleTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
				
			

					
			<%-- <g:LayOutWinTag buttonId="showtestUser" title="选择实验员"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="wKQualitySampleTask_testUser"
				documentName="wKQualitySampleTask_testUser_name" />
				
			
			
               	 	<td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQualitySampleTask_testUser_name"  value="<s:property value="wKQualitySampleTask.testUser.name"/>" class="text input readonlytrue" readonly="readOnly" />
 						<input type="hidden" id="wKQualitySampleTask_testUser" name="wKQualitySampleTask.testUser.id"  value="<s:property value="wKQualitySampleTask.testUser.id"/>" > 
 						<img alt='选择实验员' id='showtestUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="wKQualitySampleTask_testDate"
                   	 name="wKQualitySampleTask.testDate" title="实验日期"
                   	   readonly = "readOnly" 
                   	   Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  
                   	   value="<s:date name="wKQualitySampleTask.testDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQualitySampleTask_template_name"  value="<s:property value="wKQualitySampleTask.template.name"/>"/>
 						<input type="hidden" id="wKQualitySampleTask_template" name="wKQualitySampleTask.template.id"  value="<s:property value="wKQualitySampleTask.template.id"/>" > 
 						<img alt='选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="TemplateFun()" />                   		
                   	</td>
                   	
                   	<!-- <td class="label-title" >INDEX</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wKQualitySampleTask_indexa"
                   	 name="wKQualitySampleTask.indexa" title="INDEX"
                   	   class="text input"  value="<s:property value="wKQualitySampleTask.indexa"/>"
                   	  />
                   	</td> -->
					
					
					
					<g:LayOutWinTag buttonId="showAcceptUser" title="选择实验组"
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('wKQualitySampleTask_acceptUser').value=rec.get('id'); 
					document.getElementById('wKQualitySampleTask_acceptUser_name').value=rec.get('name');" /> 
			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQualitySampleTask_acceptUser_name"  value="<s:property value="wKQualitySampleTask.acceptUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wKQualitySampleTask_acceptUser" name="wKQualitySampleTask.acceptUser.id"  value="<s:property value="wKQualitySampleTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			
					<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.stateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="wKQualitySampleTask_state"
                   	 name="wKQualitySampleTask.state" title="状态id"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wKQualitySampleTask.state"/>"
                   	   style="display:none"
                   	  />
                   	</td>
                   	
                   		  <td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKQualitySampleTask_testUser_name"  value="<s:property value="wKQualitySampleTask.testUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wKQualitySampleTask_testUser" name="wKQualitySampleTask.testUser.id"  value="<s:property value="wKQualitySampleTask.testUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
               	 	
                </tr>
            <tr>    	
            
            <td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wKQualitySampleTask_stateName"
                   	 name="wKQualitySampleTask.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wKQualitySampleTask.stateName"/>"
                   	  />
                   	  
                   	  
                   	</td>
                   	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span></td>
                   	
                   	
			</tr>
            </table>
            <input type="hidden" name="wKQualityItemJson" id="wKQualityItemJson" value="" />
            <input type="hidden" name="wKQualityTemplateJson" id="wKQualityTemplateJson" value="" />
            <input type="hidden" name="wKQualityDeviceJson" id="wKQualityDeviceJson" value="" />
            <input type="hidden" name="wKQualitySampleInfoJson" id="wKQualitySampleInfoJson" value="" />
            <input type="hidden" name="wKQualityReagentJson" id="wKQualityReagentJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wKQualitySampleTask.id"/>" />
            </form>
            <div id="wKQualityItempage" width="100%" height:10px></div>
            <%-- <% 
			String maxNum = (String)request.getParameter("maxNum");
			System.out.print(maxNum);
			if(maxNum!=null && !maxNum.equals("")){
			Integer max = Integer.parseInt(maxNum);
			for(int i=0;i<max;i++){
			%> --%>
			<div id = '<%="3d_image0" %>'></div>
			<div id = '<%="3d_image1" %>'></div>
			<%-- <%} 
			}%> --%>
            <div id="tabs">
            <ul>
<!-- 			<li><a href="#wKQualityItempage">文库质检明细</a></li> -->
			<li><a href="#wKQualityTemplatepage"><fmt:message key="biolims.common.performStepsInDetail"/></a></li>
<!-- 			<li><a href="#wKQualitySampleInfopage">质检结果明细</a></li> -->
			<li><a href="#wKQualityReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#wKQualityDevicepage" onClick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
			
			<div id="wKQualityTemplatepage" width="100%" height:10px></div>
			<div id="wKQualityReagentpage" width="100%" height:10px></div>
			<div id="wKQualityDevicepage" width="100%" height:10px></div>
			
			</div>
			
			<div id="wKQualitySampleInfopage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
