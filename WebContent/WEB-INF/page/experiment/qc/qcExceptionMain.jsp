
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="cs" uri="http://www.biolims.com/taglibs/constant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/qcExceptionMain.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="tabs">
            <ul>
	            <li><a href="#wKQualityExceptiondiv"><fmt:message key="biolims.common.qualityAbnormal"/>(${requestScope.count1})</a></li>
				<li><a href="#wKQpcrExceptiondiv"><fmt:message key="biolims.common.qpcrQualityAbnormal"/>(${requestScope.count2})</a></li>
<%-- 				<li><a href="#qcPoolingAbnormaldiv">Pooling异常(${requestScope.count3})</a></li> --%>
           	</ul>
				<div id="wKQualityExceptiondiv" width="100%" height:10px></div>
				<div id="wKQpcrExceptiondiv" width="100%" height:10px></div>
				<div id="qcPoolingAbnormaldiv" width="100%" height:10px></div>
		 </div>
		 
		 
	<%-- <%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup">
		<input type="hidden" id="task_id" value="${requestScope.poolingId}">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div  class="main-centent">
				<!-- <div id="show_sampleFeedback_div"></div> -->
				<div>
					<!-- <div id="tabs">
						<ul>
            <li><a href="#show_sampleFeedback_div">样本异常</a></li>
			<li><a href="#show_dnaFeedback_div">血浆异常</a></li>

           	</ul> -->
			<div id="show_sampleFeedback_div" width="100%" height:10px></div>
			<div id="show_dnaFeedback_div" width="100%" height:10px></div>
						
					</div>
				</div>
				
				
		</div> --%>
</body>
</html>