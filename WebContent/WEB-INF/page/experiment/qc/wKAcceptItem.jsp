﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKAcceptItem.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
	<div id="wKAcceptItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_wkType_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.libraryType"/></span></td>
                <td><select id="iswkType"  style="width:100">
               			<option value="" ><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0"><fmt:message key="biolims.common.libraryQualityControl"/></option>
    					<option value="2"><fmt:message key="biolims.common.qualityTcontrol"/></option>
    					<option value="1"><fmt:message key="biolims.common.qualityQcontrol"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
	<div id="bat_ok_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.processingResults"/></span></td>
                <td><select id="isExecute"  style="width:100">
               			<option value="" ><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1"><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0"><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>


