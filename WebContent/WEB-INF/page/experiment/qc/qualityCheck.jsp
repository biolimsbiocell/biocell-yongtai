﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
 
</style>
<script type="text/javascript" src="${ctx}/js/experiment/qc/qualityCheck.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   maxlength="25" id="qualityCheck_sampleCode"  
 						 name="sampleCode" searchField="true" title="样本编号"  />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.libraryNumber"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   maxlength="25" id="qualityCheck_wkCode"  
 						 name="wkCode" searchField="true" title="文库编号"  />
                   	</td>
               	 
               	 	<td class="label-title" ><fmt:message key="biolims.common.fragmentLength"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="qualityCheck_length"
                   	 name="length" searchField="true" title="片段长度"    />
                   	  
                   	</td>
		
			</tr>
			<tr>
                   	  
               	 	<td class="label-title" ><fmt:message key="biolims.common.qualitySituation"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="qualityCheck_testState"
                   	 name="testState" searchField="true" title="结果判定"    />
                   	</td>
                   	
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.theNextStepTo"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="qualityCheck_handleState"
                   	 name="handleState" searchField="true" title="下一步流向"    />
                   	</td>
                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.massConcentration"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="qualityCheck_qualityConcentrer"
                   	 name="qualityConcentrer" searchField="true" title="确认执行"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
	<div>
			<div id="qualityCheckdiv"></div>
			<input type="hidden" name="selectId" id="selectId">
			<div id="bat_uploadcsv_div" style="display: none">
				<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
			</div>
			
	</div>
		<div id="bat_next_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td>
                <td><select id="nextFlow"  style="width:100">
              		<option value="" <s:if test="nextFlow==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
  					<option value="0" <s:if test="nextFlow==0">selected="selected" </s:if>>Pooling</option>
  					<option value="1" <s:if test="nextFlow==1">selected="selected" </s:if>><fmt:message key="biolims.common.computerSequencing"/></option>
  					<option value="2" <s:if test="nextFlow==2">selected="selected" </s:if>><fmt:message key="biolims.common.inspectionAgain"/></option>
  					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>><fmt:message key="biolims.common.termination"/></option>
  					<option value="4" <s:if test="nextFlow==4">selected="selected" </s:if>><fmt:message key="biolims.common.abnormalFeedbackToProjectManagement"/></option>
  					<option value="5" <s:if test="nextFlow==5">selected="selected" </s:if>><fmt:message key="biolims.common.feedbackToBuildLibraryGroup"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="result"  style="width:100">
               			<option value="" <s:if test="result==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>


