<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>报表发送-待发送样本</title> -->
<script type="text/javascript" src="${ctx}/js/experiment/qc/qpcrxd/showTemplateWaitList.js"></script>
<body>
	<div>
		<input type="hidden" id="id" value="${requestScope.id}">
		<div id="template_wait_grid_div"></div>
	</div>
</body>
</html>