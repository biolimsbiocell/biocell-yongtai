<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=qpcrjdTask&id=${qpcrjdTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/qpcrjd/qpcrjdTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>

<div style="float:left;width:25%" id="qpcrjdTaskTemppage">
</div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="qpcrjdTask_id"
                   	 name="qpcrjdTask.id" title="编号"
                   	   
	value="<s:property value="qpcrjdTask.id"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="qpcrjdTask_name"
                   	 name="qpcrjdTask.name" title="描述"
                   	   
	value="<s:property value="qpcrjdTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="qpcrjdTask_reciveDate"
                   	 name="qpcrjdTask.reciveDate" title="实验时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="qpcrjdTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
	<%-- 		<g:LayOutWinTag buttonId="showtemplate" title="选择模板"
				hasHtmlFrame="true"
				html="${ctx}/experiment/qc/qpcrjd/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('qpcrjdTask_template').value=rec.get('id');
				document.getElementById('qpcrjdTask_template_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >模板</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qpcrjdTask_template_name"  value="<s:property value="qpcrjdTask.template.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="qpcrjdTask_template" name="qpcrjdTask.template.id"  value="<s:property value="qpcrjdTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			 --%>
			 
			 	<td class="label-title" ><fmt:message key="biolims.common.experimentPattern"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qpcrjdTask_template_name"  value="<s:property value="qpcrjdTask.template.name"/>"  />
 						<input type="hidden" id="qpcrjdTask_template" name="qpcrjdTask.template.id"  value="<s:property value="qpcrjdTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"   />                   		
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('qpcrjdTask_acceptUser').value=rec.get('id');
				document.getElementById('qpcrjdTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qpcrjdTask_acceptUser_name"  value="<s:property value="qpcrjdTask.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="qpcrjdTask_acceptUser" name="qpcrjdTask.acceptUser.id"  value="<s:property value="qpcrjdTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	
			  <td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qpcrjdTask_testUser_name"  value="<s:property value="qpcrjdTask.testUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="qpcrjdTask_testUser" name="qpcrjdTask.testUser.id"  value="<s:property value="qpcrjdTask.testUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
			
			
			
		
			</tr>
			<tr>
			
			
         	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="qpcrjdTask_createDate"
                   	 name="qpcrjdTask.createDate" title="下达时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="qpcrjdTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('qpcrjdTask_confirmUser').value=rec.get('id');
				document.getElementById('qpcrjdTask_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qpcrjdTask_confirmUser_name"  value="<s:property value="qpcrjdTask.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="qpcrjdTask_confirmUser" name="qpcrjdTask.confirmUser.id"  value="<s:property value="qpcrjdTask.confirmUser.id"/>" > 
<%--  						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
				<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="qpcrjdTask_confirmDate"
                   	 name="qpcrjdTask.confirmDate" title="完成时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="qpcrjdTask.confirmDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
				<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('qpcrjdTask_createUser').value=rec.get('id');
				document.getElementById('qpcrjdTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="qpcrjdTask_createUser_name"  value="<s:property value="qpcrjdTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="qpcrjdTask_createUser" name="qpcrjdTask.createUser.id"  value="<s:property value="qpcrjdTask.createUser.id"/>" > 
<%--  						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
               	 	<td class="label-title" style="display: none;"><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none;"></td>            	 	
                   	<td align="left"  style="display: none;">
                   	<input type="text" size="20" maxlength="25" id="qpcrjdTask_state"
                   	 name="qpcrjdTask.state" title="状态"
                   	   
	value="<s:property value="qpcrjdTask.state"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="qpcrjdTask_stateName"
                   	 name="qpcrjdTask.stateName" title="工作流状态"
                   	   
	value="<s:property value="qpcrjdTask.stateName"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="qpcrjdTask_note"
                   	 name="qpcrjdTask.note" title="备注"
                   	   
	value="<s:property value="qpcrjdTask.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
                   				<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>

			
            </table>
            <input type="hidden" name="qpcrjdTaskItemJson" id="qpcrjdTaskItemJson" value="" />
            <input type="hidden" name="qpcrjdTaskTemplateJson" id="qpcrjdTaskTemplateJson" value="" />
            <input type="hidden" name="qpcrjdTaskReagentJson" id="qpcrjdTaskReagentJson" value="" />
            <input type="hidden" name="qpcrjdTaskCosJson" id="qpcrjdTaskCosJson" value="" />
            <input type="hidden" name="qpcrjdTaskResultJson" id="qpcrjdTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="qpcrjdTask.id"/>" />
            </form>
            <div id="qpcrjdTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#qpcrjdTaskTemplatepage"><fmt:message key="biolims.common.executionStep"/></a></li>
			<li><a href="#qpcrjdTaskReagentpage"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#qpcrjdTaskCospage"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
			<div id="qpcrjdTaskTemplatepage" width="100%" height:10px></div>
			<div id="qpcrjdTaskReagentpage" width="100%" height:10px></div>
			<div id="qpcrjdTaskCospage" width="100%" height:10px></div>
			</div>
			<div id="qpcrjdTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
