<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=agrosTask&id=${agrosTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/agros/agrosTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="agrosTaskTemppage">
</div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="agrosTask_id"
                   	 name="agrosTask.id" title="编号"
                   	   
	value="<s:property value="agrosTask.id"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="agrosTask_name"
                   	 name="agrosTask.name" title="描述"
                   	   
	value="<s:property value="agrosTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="agrosTask_reciveDate"
                   	 name="agrosTask.reciveDate" title="实验时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="agrosTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			</tr>
			<tr>
			
			
<%-- 			
			<g:LayOutWinTag buttonId="showtemplate" title="选择模板"
				hasHtmlFrame="true"
				html="${ctx}/experiment/qc/agros/templateSelect.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('agrosTask_template').value=rec.get('id');
				document.getElementById('agrosTask_template_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >模板</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="agrosTask_template_name"  value="<s:property value="agrosTask.template.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="agrosTask_template" name="agrosTask.template.id"  value="<s:property value="agrosTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			 --%>
			 
			 	 	<td class="label-title" ><fmt:message key="biolims.common.experimentPattern"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="agrosTask_template_name"  value="<s:property value="agrosTask.template.name"/>"  />
 						<input type="hidden" id="agrosTask_template" name="agrosTask.template.id"  value="<s:property value="agrosTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()"   />                   		
                   	</td>
			
			
			<g:LayOutWinTag buttonId="showacceptUser" title="选择实验组"
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('agrosTask_acceptUser').value=rec.get('id');
				document.getElementById('agrosTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="agrosTask_acceptUser_name"  value="<s:property value="agrosTask.acceptUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="agrosTask_acceptUser" name="agrosTask.acceptUser.id"  value="<s:property value="agrosTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
				  <td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="agrosTask_testUser_name"  value="<s:property value="agrosTask.testUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="agrosTask_testUser" name="agrosTask.testUser.id"  value="<s:property value="agrosTask.testUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
		
			</tr>
			<tr>
				
			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('agrosTask_createUser').value=rec.get('id');
				document.getElementById('agrosTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="agrosTask_createUser_name"  value="<s:property value="agrosTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="agrosTask_createUser" name="agrosTask.createUser.id"  value="<s:property value="agrosTask.createUser.id"/>" > 
<%--  						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="agrosTask_createDate"
                   	 name="agrosTask.createDate" title="下达时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="agrosTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择审核人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('agrosTask_confirmUser').value=rec.get('id');
				document.getElementById('agrosTask_confirmUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="agrosTask_confirmUser_name"  value="<s:property value="agrosTask.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="agrosTask_confirmUser" name="agrosTask.confirmUser.id"  value="<s:property value="agrosTask.confirmUser.id"/>" > 
 					<%-- 	<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />      --%>              		
                   	</td>
			
			
               	 
			</tr>
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="agrosTask_confirmDate"
                   	 name="agrosTask.confirmDate" title="完成时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="agrosTask.confirmDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
               	 	<td class="label-title" style="display: none;"><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display: none;"></td>            	 	
                   	<td align="left" style="display: none;" >
                   	<input type="text" size="20" maxlength="25" id="agrosTask_state"
                   	 name="agrosTask.state" title="状态"
                   	   
	value="<s:property value="agrosTask.state"/>"
                   	  />
                   	  
                   	</td> 
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="agrosTask_stateName"
                   	 name="agrosTask.stateName" title="工作流状态"
                   	   
	value="<s:property value="agrosTask.stateName"/>"class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="agrosTask_note"
                   	 name="agrosTask.note" title="备注"
                   	   
	value="<s:property value="agrosTask.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>

			
			
            </table>
            <input type="hidden" name="agrosTaskItemJson" id="agrosTaskItemJson" value="" />
            <input type="hidden" name="agrosTaskTemplateJson" id="agrosTaskTemplateJson" value="" />
            <input type="hidden" name="agrosTaskReagentJson" id="agrosTaskReagentJson" value="" />
            <input type="hidden" name="agrosTaskCosJson" id="agrosTaskCosJson" value="" />
            <input type="hidden" name="agrosTaskResultJson" id="agrosTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="agrosTask.id"/>" />
            </form>
            <div id="agrosTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#agrosTaskTemplatepage"><fmt:message key="biolims.common.executionStep"/></a></li>
			<li><a href="#agrosTaskReagentpage"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#agrosTaskCospage"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
			<div id="agrosTaskTemplatepage" width="100%" height:10px></div>
			<div id="agrosTaskReagentpage" width="100%" height:10px></div>
			<div id="agrosTaskCospage" width="100%" height:10px></div>
			</div>
			<div id="agrosTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
