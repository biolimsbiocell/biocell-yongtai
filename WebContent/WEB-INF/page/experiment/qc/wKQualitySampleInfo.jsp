﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKQualitySampleInfo.js"></script>
</head>
<body>
<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
	<div id="wKQualitySampleInfodiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_data2_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.fragmentLength"/></span></td>
				<td><input id="length"/></td>
			</tr>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.concentration"/></span></td>
				<td><input id="concentration2"/></td>
			</tr>
			<!-- <tr>
				<td class="label-title"><span>QPCR浓度</span></td>
				<td><input id="qpcr2"/></td>
			</tr> -->
		</table>
	</div>
	<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="result" style="width:100">
                		<option value="" <s:if test="result==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="result==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0" <s:if test="result==0">selected="selected" </s:if>><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_next_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td>
                <td><select id="nextFlow"  style="width:100">
                		<option value="" <s:if test="nextFlow==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0" <s:if test="nextFlow==0">selected="selected" </s:if>>Pooling</option>
    					<option value="1" <s:if test="nextFlow==1">selected="selected" </s:if>><fmt:message key="biolims.common.drawBloodAgain"/></option>
    					<option value="2" <s:if test="nextFlow==2">selected="selected" </s:if>><fmt:message key="biolims.common.inspectionAgain"/></option>
    					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>><fmt:message key="biolims.common.termination"/></option>
    					<option value="4" <s:if test="nextFlow==4">selected="selected" </s:if>><fmt:message key="biolims.common.feedbackToTheProjectTeam"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
		<div id="bat_submit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.whetherToCommit"/></span></td>
                <td><select id="submit"  style="width:100">
<!--                 		<option value="" <s:if test="submit==">selected="selected" </s:if>>请选择</option> -->
    					<option value="1" <s:if test="submit==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="submit==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="many_bat_div" style="display:none" >
			<div class="ui-widget;">
				<div id="bat_position_format_div" class="ui-state-highlight ui-corner-all jquery-ui-warning">
				</div>
			</div>
			<textarea id="many_bat_text" style="width:395px;height: 339px"></textarea>
		</div>
</body>
</html>


