﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKQualityException.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="输入查询条件">
			<fmt:message key="biolims.common.libraryNumber"/>
			</label>
			</td>
			<td>
			<input type="text"  name="wkCode" id="wKQualityException_wkCode" searchType ="1" searchField="true" >
			</td>
			<td >
			<label class="text label" title="输入查询条件">
			<fmt:message key="biolims.common.originalSampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="wKQualityException_sampleCode" searchType ="1" searchField="true" >
			</td>
			<td >
			<!-- <label class="text label" title="输入查询条件">
			文库类型
			</label>
			</td>
			<td>
				<select id="wKQualityException_wkType" name="wkType" searchType ="1" searchField="true" style="width:100">
                		<option value="">请选择</option>
    					<option value="0">2100质控</option>
    					<option value="2">2100 or Caliper</option>
				</select>
			</td> -->
			<td >
			<label class="text label" title="输入查询条件">
			<fmt:message key="biolims.common.processingOpinion"/>
			</label>
			</td>
			<td>
				<select id="wKQualityException_method" name="method" searchType ="1" searchField="true" style="width:100">
                		<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0">Pooling</option>
    					<option value="1"><fmt:message key="biolims.common.drawBloodAgain"/></option>
    					<option value="2"><fmt:message key="biolims.common.inspectionAgain"/></option>
    					<option value="3"><fmt:message key="biolims.common.termination"/></option>
    					<option value="4"><fmt:message key="biolims.common.putInStorage"/></option>
				</select>
			</td>
			<td>
			<input type="button" onClick="selectQc2100Info()" value="查询">
			</td>
			</tr>
		</table>
		
		 
		<div id="bat_ok2100_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.affirmToPerform"/></span></td>
                <td><select id="isRun"  style="width:100">
               			<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1"><fmt:message key="biolims.common.yes"/></option>
    					<option value="0"><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<!--  <form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		 <input type='hidden' id='gridhtm' name='gridhtm' value=''/></form> -->
</body>
</html>


