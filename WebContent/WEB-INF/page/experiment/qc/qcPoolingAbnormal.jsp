<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/experiment/qc/qcPoolingAbnormal.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="qcPoolingAbnormaldiv"></div>
		
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
				
			<td >
			<label class="text label" title="输入查询条件">
			<fmt:message key="biolims.common.poolingSerialNumber"/>
			</label>
			</td>
			<td>
			<input type="text"  name="poolingCode" id="qcPoolingAbnormal_poolingCode" searchField="true" >
			</td>
			
			<td >
			<label class="text label" title="输入查询条件">
			<fmt:message key="biolims.common.libraryNumber"/>
			</label>
			</td>
			<td>
			<input type="text"  name="wkId" id="qcPoolingAbnormal_wkId" searchField="true" >
			</td>
			
			<td>
			<input type="button" onClick="selectQcPoolingInfo()" value="查询">
			</td>
			
			</tr>
		</table>
		
		<div id="bat_ok_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.affirmToPerform"/></span></td>
                <td><select id="isExecute2"  style="width:100">
               			<option value="" <s:if test="isExecute2==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isExecute2==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isExecute2==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
   		<!-- <form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form> -->
</body>
</html>



