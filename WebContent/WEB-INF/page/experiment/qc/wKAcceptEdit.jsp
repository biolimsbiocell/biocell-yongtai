﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<!--<g:LayOutWinTag buttonId="dnawzBtn" title="选择位置" hasHtmlFrame="true"
		html="${ctx}/storage/position/showStoragePositionTree1.action?setStoragePostion=true"
		isHasSubmit="false" functionName="showStoragePositionFund"
		hasSetFun="true" documentId="wKAccept_location"
		documentName="wKAccept_location_name" />-->
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=wKAccept&id=${wKAccept.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKAcceptEdit.js"></script>
<div style="float:left;width:25%" id="wKAcceptTempage">
<!-- <div id="tabs1">
     <ul>
        <li><a href="#wKAcceptTempdiv">临床</a></li>
		<li><a href="#wKAcceptTechOrderdiv">科技服务</a></li>
     </ul>
		<div id="wKAcceptTempdiv" width="100%" height:10px></div>
		<div id="wKAcceptTechOrderdiv" width="100%" height:10px></div>
</div> -->
</div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
						<td colspan="9">
							<div class="standard-section-header type-title">
								<label><fmt:message key="biolims.common.basicInformation"/></label>
							</div>
						</td>
				</tr>
			<tr>
			
			
               	 	<td class="label-title"   ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="wKAccept_id"
                   	 name="wKAccept.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="wKAccept.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wKAccept_name"
                   	 name="wKAccept.name" title="描述"
                   	   
	value="<s:property value="wKAccept.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
 						<input type="text" size="20" readonly="readOnly"  id="wKAccept_receiveUser_name"  value="<s:property value="wKAccept.receiveUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wKAccept_receiveUser" name="wKAccept.receiveUser.id"  value="<s:property value="wKAccept.receiveUser.id"/>" > 
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  <%-- 	<input type="text" size="20" maxlength="25" id="wKAccept_receiverDate"
                   	 name="wKAccept.receiverDate" title="接收时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	       value="<s:property name="wKAccept.receiverDate" />" 
                   	     
                   	  /> --%>
                   	  <input type="text" size="20" maxlength="25" id="wKAccept_receiverDate"
                   	 name="wKAccept.receiverDate" title="接收时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wKAccept.receiverDate"/>"
                   	  />
                   	</td>
			
			
               	 	
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wKAccept_stateName"
                   	 name="wKAccept.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wKAccept.stateName"/>"
                   	  />
                   	  
                   	</td>
                   	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			<tr>
						<td colspan="9">
							<div class="standard-section-header type-title">
								<label><fmt:message key="biolims.common.storageInformation"/></label>
							</div>
						</td>
				</tr>
			<tr>
			<td class="label-title" ><fmt:message key="biolims.common.storageLocationHints"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wKAccept_location"
                   	 name="wKAccept.location" title="储位提示"
                   	   
	value="<s:property value="wKAccept.location"/>"
                   	  />
                   	  
                   	</td>
                   <%-- 	<td class="label-title">储位提示</td>
                   	
                   	<g:LayOutWinTag buttonId="wzBtn" title="选择位置" hasHtmlFrame="true"
							html="${ctx}/storage/common/showStoragePositionTree.action?setStoragePostion=true"
							isHasSubmit="false" functionName="showStoragePosition"
							hasSetFun="true" documentId="wKAccept_location"
							documentName="wKAccept_location_name" />
					<td class="requiredcolumn"  colspan="9">
						<input type="text"  name="wKAccept.location.id" id="wKAccept_location" value='<s:property value="wKAccept.location.id"/>'
							title="存储位置 " size="10" maxlength="30" >  <img alt='选择' id='wzBtn'
							name='show-btn' src='${ctx}/images/img_menu.gif' class='detail' /> 
						<input type="text" class="input_parts text input  readonlytrue" name="wKAccept.location.name" id="wKAccept_location_name"
							readonly="readonly"  value='<s:property value="wKAccept.location.name"/>'
							title="存储位置 "  size="30"
							maxlength="62">  --%>
							
							
							<!--<g:LayOutWinTag buttonId="showlocation" title="选择储位提示"
						hasHtmlFrame="true"
						html="${ctx}/system/location/saveLocation/saveLocationSelect.action"
						isHasSubmit="false" functionName="MODELFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('wKAccept_location').value=rec.get('id');
						document.getElementById('wKAccept_location_name').value=rec.get('name');" />
               	 	<td class="label-title" >储位提示</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" id="wKAccept_location" name="wKAccept.location.id"  value="<s:property value="wKAccept.location.id"/>" > 
 						<img alt='选择储位提示' id='showlocation' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	<td class="label-title" ></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wKAccept_location_name"  name="wKAccept.location.name" value="<s:property value="wKAccept.location.name"/>" class="text input readonlytrue" readonly="readOnly"  />
                   	</td>
					</td> -->
                   	
			</tr>
			
			
            </table>
            <input type="hidden" name="wKAcceptItemJson" id="wKAcceptItemJson" value="" />
            <input type="hidden" name="pOOLINGAcceptItemJson" id="pOOLINGAcceptItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wKAccept.id"/>" />
            </form>
            <div id="WKSplitItempage" width="100%" height:10px></div>
<!--             <div id="tabs"> -->
            <ul>
			<li><a href="#wKAcceptItempage"></a></li>
			
           	</ul> 
           	<ul>
           		<li><a href="#pOOLINGAcceptItempage"></a></li>
           	</ul>
           	
			<div id="wKAcceptItempage" width="100%" height:10px></div>
			<div id="pOOLINGAcceptItempage" width="100%" height:10px></div>
			</div>
			<div id="WKQualitypage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
