﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKAcceptTempEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="tabs">
            <ul>
				<li><a href="#wKAcceptTempdiv"><fmt:message key="biolims.common.clinic"/></a></li>
				<li><a href="#wKAcceptTechOrderdiv"><fmt:message key="biolims.common.scienceAndTechnologyService"/></a></li>
           	</ul>
				<div id="wKAcceptTempdiv" width="100%" height:10px></div>
				<div id="wKAcceptTechOrderdiv" width="100%" height:10px></div>
		 </div>
</body>
</html>


