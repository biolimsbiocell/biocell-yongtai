﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKAccept.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left">
                  
					<input type="text" size="30" maxlength="25" id="wKAccept_id"
                   	 name="id" searchField="true" title="编号"/>
                   	</td>
			</tr>
			<tr>       
			<g:LayOutWinTag buttonId="showreceiveUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="wKAccept_receiveUser"
				documentName="wKAccept_receiveUser_name" />            	
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
                   	<td align="left">
 						<input type="text" size="30"   id="wKAccept_receiveUser_name" searchField="true"  name="receiveUser.name"  value=""/>
 						<input type="hidden" id="wKAccept_receiveUser" name="wKAccept.receiveUser.id"  value="" > 
 						<img alt='选择接收人' id='showreceiveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'/>                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startreceiverDate" name="startreceiverDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiverDate1" name="receiverDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreceiverDate" name="endreceiverDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receiverDate2" name="receiverDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_wKAccept_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_wKAccept_tree_page"></div>
</body>
</html>



