
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKQpcrSampleTask.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left">
                  
					<input type="text" size="30" maxlength="30" id="wKQpcrSampleTask_id"
                   	 name="id" searchField="true" title="编号"/>
                   	</td>
			</tr>
			<tr>                   	
			<g:LayOutWinTag buttonId="showreleasUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="wKQpcrSampleTask_releasUser"
				documentName="wKQpcrSampleTask_releasUser_name" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="30"   id="wKQpcrSampleTask_releasUser_name" searchField="true"  name="releasUser.name"  value="" class="text input" />
 						<input type="hidden" id="wKQpcrSampleTask_releasUser" name="wKQpcrSampleTask.releasUser.id"  value="" > 
 						<img alt='选择下达人' id='showreleasUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startreleaseDate" name="startreleaseDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="releaseDate1" name="releaseDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreleaseDate" name="endreleaseDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="releaseDate2" name="releaseDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
			<tr>                   	
			<g:LayOutWinTag buttonId="showtestUser" title="选择实验员"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="TestUserFun" 
 				hasSetFun="true"
				documentId="wKQpcrSampleTask_testUser"
				documentName="wKQpcrSampleTask_testUser_name" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
                   	<td align="left"  >
 						<input type="text" size="30"   id="wKQpcrSampleTask_testUser_name" searchField="true"  name="testUser.name"  value="" class="text input" />
 						<input type="hidden" id="wKQpcrSampleTask_testUser" name="wKQpcrSampleTask.testUser.id"  value="" > 
 						<img alt='选择实验员' id='showtestUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="starttestDate" name="starttestDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="testDate1" name="testDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endtestDate" name="endtestDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="testDate2" name="testDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showtemplate" title="选择选择实验模板"
				hasHtmlFrame="true"
				html="${ctx}/system/template/template/templateSelectByType.action?flag=TemplateFun&type=doQua"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wKQpcrSampleTask_template').value=rec.get('id');
				document.getElementById('wKQpcrSampleTask_template_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
                   	<td align="left"  >
 						<input type="text" size="30"   id="wKQpcrSampleTask_template_name" searchField="true"  name="template.name"  value="" class="text input" />
 						<input type="hidden" id="wKQpcrSampleTask_template" name="wKQpcrSampleTask.template.id"  value="" > 
 						<img alt='选择选择实验模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="wKQpcrSampleTaskdiv"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_wKQpcr_tree_page"></div>
</body>
</html>



