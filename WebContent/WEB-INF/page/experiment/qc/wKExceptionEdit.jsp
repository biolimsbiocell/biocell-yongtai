﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=wKException&id=${wKException.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/qc/wKExceptionEdit.js"></script>
  <div style="float:left;width:25%" id="WKExceptionPage"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			
			<tr>
			
			
               	 	<td class="label-title"   ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="wKException_id"
                   	 name="wKException.id" title="编号"
                   	   
	value="<s:property value="wKException.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wKException_name"
                   	 name="wKException.name" title="描述"
                   	   
	value="<s:property value="wKException.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wKException_note"
                   	 name="wKException.note" title="描述"
                   	   
	value="<s:property value="wKException.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wKException_stateName"
                   	 name="wKException.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wKException.stateName"/>"
                   	  />
                   	  
                   	</td>
			
					<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
					<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
						class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="wKQualityExceptionJson" id="wKQualityExceptionJson" value="" />
            <input type="hidden" name="poolingQualityExceptionJson" id="poolingQualityExceptionJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wKException.id"/>" />
            </form>
<!--             <div id="tabs"> -->
            <ul>
			<li><a href="#wKQualityExceptionpage"></a></li>
			<li><a href="#poolingQualityExceptionpage"></a></li>
           	</ul> 
			<div id="wKQualityExceptionpage" width="100%" height:10px></div>
			<div id="poolingQualityExceptionpage" width="100%" height:10px></div>
			</div>
<!--         	</div> -->
	</body>
	</html>
