﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=techReportMain&id=${techReportMain.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/techreport/techReportMainEdit.js"></script>
 <!-- <div  style="width:25%;float:left" id="reportpage"></div>  -->
  <div id="maintab" style="margin: 0 0 0 0"></div>
  
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="30" id="techReportMain_id"
                   	 name="techReportMain.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   class="text input readonlytrue" readonly="readOnly"
	value="<s:property value="techReportMain.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="40" maxlength="30" id="techReportMain_name"
                   	 name="techReportMain.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="techReportMain.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.reporter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="techReportMain_createUser_name"  value="<s:property value="techReportMain.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="techReportMain_createUser" name="techReportMain.createUser.id"  value="<s:property value="techReportMain.createUser.id"/>" > 
<%--  						<img alt='选择报告人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.reportDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="15" maxlength="" id="techReportMain_createDate"
                   	 name="techReportMain.createDate" title="<fmt:message key="biolims.common.reportDate"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	     value = "<s:property value="techReportMain.createDate"/>"
                   	  />
                   	</td>
			
			 <td class="label-title" ><fmt:message key="biolims.common.scientificRTL"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="hidden" size="20" readonly="readOnly"  id="techReportMain_techJkServiceTask_name"  value="<s:property value="techReportMain.techJkServiceTask.name"/>" />
 						<input type="text"  size="20" readonly="readOnly" id="techReportMain_techJkServiceTask" name="techReportMain.techJkServiceTask.id"  value="<s:property value="techReportMain.techJkServiceTask.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTSRTL"/>' src='${ctx}/images/img_lookup.gif' onClick="selectTechJkServiceTask()"	class='detail'    />                   		
                   	</td> 
			
			
<%--                	 	<td class="label-title" ><fmt:message key="biolims.common.taskNumber"/></td> --%>
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    	<input type="text" size="20" maxlength="30" id="techReportMain_orderId" -->
<%--                    	 name="techReportMain.orderId" title="<fmt:message key="biolims.common.taskNumber"/>" --%>
<!--                    	   readonly = "readOnly"   -->
<%-- 	value="<s:property value="techReportMain.orderId"/>" --%>
	
<!--                    	  /> -->
<%--                    	  <img src='${ctx}/images/img_lookup.gif' class='detail'  onClick="TechDnaServiceTaskFun()"/> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.selectTheReportTemplate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="techReportMain_reportTemplateInfo_name"  value="<s:property value="techReportMain.reportTemplateInfo.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="techReportMain_reportTemplateInfo" name="techReportMain.reportTemplateInfo.id"  value="<s:property value="techReportMain.reportTemplateInfo.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheReportTemplate"/>' id='showreportTemplateInfo' src='${ctx}/images/img_lookup.gif' 	class='detail' onClick="sampleReportSelectFun()"/>                   		
                  	</td>
			</tr>
			<tr>
				<td class="label-title"   style="display:none"><fmt:message key="biolims.common.reportCardID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"   style="display:none"></td>            	 	
                   	<td align="left"    style="display:none">
                   	<input type="text" size="20" maxlength="30" id="techReportMain_bgId"
                   	 name="techReportMain.bgId" title="<fmt:message key="biolims.common.reportCardID"/>"
                   	   readonly = "readOnly"  
	value="<s:property value="techReportMain.bgId"/>"
	
                   	  />
                   	</td>
			        <td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	  
 					<input type="hidden" id="techReportMain_state" name="techReportMain.state" value="<s:property value="techReportMain.state"/>"
                   	/></td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="techReportMain_stateName"
                   	 name="techReportMain.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="techReportMain.stateName"/>"
                   	  />
                   	  
                   	</td>

			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="techReportItemJson" id="techReportItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="techReportMain.id"/>" />
            </form>
			<!--<div id="tabs">
            <ul>
			<li><a href="#techReportItempage">报告明细</a></li>
           	</ul>  -->
			<div id="techReportItempage" width="100%" height:10px></div>
			</div>
        	<!-- </div> -->
	</body>
	</html>
