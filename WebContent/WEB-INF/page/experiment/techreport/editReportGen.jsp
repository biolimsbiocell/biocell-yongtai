<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ page language="java" import="java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>

<%
	String path = request.getContextPath();
	String cPath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path ;
	String basePath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadById.action?id="+request.getAttribute("fileId");
	String basePathDownId = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadById.action?id=";
	String basePathDownName = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ path + "/operfile/downloadFileByPath.action?fileName=";
%>
<script type="text/javascript">
function viewdoc() {
	document.all.DSOFramer.ShowView(3);
	document.all.DSOFramer.Open("<%=basePath%>", true, "Word.Document");
	document.all.DSOFramer.Menubar = true;
	document.all.DSOFramer.SetMenuDisplay(64);
	document.all.DSOFramer.SetMenuDisplay(126);
	var word = new Object(document.all.DSOFramer.ActiveDocument);
	<%if(request.getAttribute("fileExist")==null){%>
	cs();
	<%}%>
}
function savedoc() {
	}
		function printdoc() {
			document.all.DSOFramer.PrintOut();
		}
		function uploaddoc(){
			
			document.all.DSOFramer.HttpInit();
			document.all.DSOFramer.HttpAddPostCurrFile("File",encodeURI("<%=request.getAttribute("fileName")%>"));
			document.all.DSOFramer.HttpPost("<%=cPath%>/fileUpload?contentId=<%=request.getAttribute("contentId")%>&modelType=<%=request.getAttribute("type")%>&useType=doc&type=1&picUserId="+window.userId);
			alert(biolims.common.saveSuccess);
		}
		
		function ppw() {
			
			document.all.DSOFramer.PrintPreview();
			
			 
			
		}
		function ppwe() {
			
			document.all.DSOFramer.PrintPreviewExit();
			
			 
			
		}
		
		
		function reloaddoc(){
			
			document.location.href=this.location.href+"&reloadAction=true";
			
		}
		
		function cs() {
			document.all.DSOFramer.SetFieldValue("contractId","<%=request.getAttribute("contractId")%>","");
			document.all.DSOFramer.SetFieldValue("projectId","<%=request.getAttribute("projectId")%>","");
			document.all.DSOFramer.SetFieldValue("publishDate","<%=request.getAttribute("publishDate")%>","");
			document.all.DSOFramer.SetFieldValue("projectUser","<%=request.getAttribute("projectUser")%>","");
			document.all.DSOFramer.SetFieldValue("contentDept","<%=request.getAttribute("contentDept")%>","");
			document.all.DSOFramer.SetFieldValue("receiveDate","<%=request.getAttribute("receiveDate")%>","");
			document.all.DSOFramer.SetFieldValue("checkDate","<%=request.getAttribute("checkDate")%>","");
			document.all.DSOFramer.SetFieldValue("checkUser","<%=request.getAttribute("checkUser")%>","");
			document.all.DSOFramer.SetFieldValue("reportUser","<%=request.getAttribute("reportUser")%>","");
			document.all.DSOFramer.SetFieldValue("confirmUser","<%=request.getAttribute("confirmUser")%>","");

			var n=1;
			<s:iterator value="#request.tsList" id="tsList">
			n++;
			document.all.DSOFramer.ActiveDocument.Tables(2).Rows.Add();
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 3).Range.InsertAfter("<s:property escape="false" value='%{#tsList.dnaCode}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 4).Range.InsertAfter("<s:property escape="false" value='%{#tsList.concentration}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 5).Range.InsertAfter("<s:property escape="false" value='%{#tsList.volume}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 6).Range.InsertAfter("<s:property escape="false" value='%{#tsList.sumNum}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 7).Range.InsertAfter("<s:property escape="false" value='%{#tsList.od230}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 8).Range.InsertAfter("<s:property escape="false" value='%{#tsList.od260}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 9).Range.InsertAfter("<s:property escape="false" value='%{#tsList.s28}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 10).Range.InsertAfter("<s:property escape="false" value='%{#tsList.rin}'/>");
			//document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 11).Range.InsertAfter("<s:property escape="false" value='%{#tsList.sampleCode}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 12).Range.InsertAfter("<s:property escape="false" value='%{#tsList.result}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(n, 13).Range.InsertAfter("<s:property escape="false" value='%{#tsList.note}'/>");
			document.all.DSOFramer.SetFieldValue("tp"+n,"D://001//<s:property escape="false" value='%{#tsList.sampleCode}'/>.png","::JPG::");
			document.all.DSOFramer.SetFieldValue("tpName"+n,"<s:property escape="false" value='%{#tsList.sampleCode}'/>","");
		 	</s:iterator>
		 	
		 	var i=0;
			<s:iterator value="#request.wkList" id="wkList">
			i++;
			document.all.DSOFramer.ActiveDocument.Tables(2).Rows.Add();
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i+1, 2).Range.InsertAfter("<s:property escape="false" value='%{#wkList.sampleCode}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i+1, 3).Range.InsertAfter("<s:property escape="false" value='%{#wkList.wkCode}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i+1, 4).Range.InsertAfter("<s:property escape="false" value='%{#wkList.pdSzie}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i+1, 5).Range.InsertAfter("<s:property escape="false" value='%{#wkList.massCon}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(2).Cell(i+1, 6).Range.InsertAfter("<s:property escape="false" value='%{#wkList.molarCon}'/>");
			
			document.all.DSOFramer.ActiveDocument.Tables(3).Rows.Add();
			document.all.DSOFramer.ActiveDocument.Tables(3).Cell(i+1, 2).Range.InsertAfter("<s:property escape="false" value='%{#wkList.sampleCode}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(3).Cell(i+1, 3).Range.InsertAfter("<s:property escape="false" value='%{#wkList.volume}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(3).Cell(i+1, 4).Range.InsertAfter("<s:property escape="false" value='%{#wkList.pdResult}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(3).Cell(i+1, 5).Range.InsertAfter("<s:property escape="false" value='%{#wkList.molarResult}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(3).Cell(i+1, 6).Range.InsertAfter("<s:property escape="false" value='%{#wkList.note}'/>");
			
			document.all.DSOFramer.SetFieldValue("wn"+i,"<s:property escape="false" value='%{#wkList.sampleCode}'/>","");
			document.all.DSOFramer.SetFieldValue("w"+i,"D://002//<s:property escape="false" value='%{#wkList.sampleCode}'/>.png","::JPG::");
			
			document.all.DSOFramer.ActiveDocument.Tables(4).Rows.Add();
			document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i+1, 1).Range.InsertAfter("<s:property escape="false" value='%{#wkList.sampleCode}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i+1, 2).Range.InsertAfter("<s:property escape="false" value='%{#wkList.pdSzie}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i+1, 3).Range.InsertAfter("<s:property escape="false" value='%{#wkList.volume}'/>");
			document.all.DSOFramer.ActiveDocument.Tables(4).Cell(i+1, 4).Range.InsertAfter("<s:property escape="false" value='%{#wkList.massCon}'/>");
			</s:iterator>
		}
		 
		
</script>

		
	<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />	
</head>
<body onload="setTimeout(viewdoc,1000);">
		
		<div id="dsoDoc" align="center" >
		
		
		<input type="button" value="<fmt:message key="biolims.common.saveToTheServer"/>" onclick="uploaddoc()"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value="<fmt:message key="biolims.common.generateANewReport"/>" onclick="reloaddoc()"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value="<fmt:message key="biolims.common.printPreview"/>" onclick="ppw();"></input>
		&nbsp;&nbsp;&nbsp;
		<input type="button" value="<fmt:message key="biolims.common.outOfPrintPreview"/>" onclick="ppwe();"></input>
		<OBJECT id='DSOFramer' align='middle' style='LEFT: 0px; WIDTH: 100%; TOP: 0px; HEIGHT: 95%'
		classid=clsid:00460182-9E5E-11d5-B7C8-B8269041DD57 codeBase='${ctx}/dso/dso.CAB#V2.3.0.1'>
		<PARAM NAME='_ExtentX' VALUE='6350'>
		<PARAM NAME='_ExtentY' VALUE='6350'>
		<PARAM NAME='BorderColor' VALUE='-2147483632'>
		<PARAM NAME='BackColor' VALUE='-2147483643'>
		<PARAM NAME='ActivationPolicy' VALUE='6'>
		<PARAM NAME='ForeColor' VALUE='-2147483640'>
		<PARAM NAME='TitlebarColor' VALUE='-2147483635'>
		<PARAM NAME='TitlebarTextColor' VALUE='-2147483634'>
		<PARAM NAME='BorderStyle' VALUE='1'>
		<PARAM NAME='Titlebar' VALUE='0'>
		<PARAM NAME='Toolbars' VALUE='1'>
		<PARAM NAME='Menubar' VALUE='0'>
		<param name='BorderStyle' value='1'>
	</OBJECT>
	</div>
			
</body>
</html>