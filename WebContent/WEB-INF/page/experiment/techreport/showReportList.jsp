<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

<%-- <link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
 --%><script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/js/experiment/techreport/showReportList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
<%-- 	<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
<div  class="mainlistclass" id="markup">
	<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form> --%>
<input type="hidden" id="extJsonDataString" name="extJsonDataString">
<table cellspacing="0" cellpadding="0" class="toolbarsection">
	<tr>

<%-- <td >
	<label class="text label" >
	选择任务单
	</label>
	</td>
	<td>&nbsp;</td>
	<td>
	<input type="text" id="orderName" name="orderName" readonly="readOnly" style="width:120px;">
	<img src='${ctx}/images/img_lookup.gif' class='detail'  onClick="TechDnaServiceTaskFun()"/>
	</td> --%>
	<td class="text label" style="display:none">
	<label>
	<fmt:message key="biolims.common.taskNumber"/>
	</label>
	</td>
	<td>
	<input type="text" id="orderId" name="orderId" style="display:none" searchType ="21" readonly = "readOnly" searchField="true" >
	</td>
	<td>
	<input type="button" onClick="selectReport2()" value="<fmt:message key="biolims.common.find"/>" style="width:50px;display:none">
	</td>
	</tr>
	</table>
	
	<div id ="report_tem_grid_div"></div>
	<!--<div style="display: none">
		<select id="grid_state" style="display:none">
			<option value="0">失效</option>
			<option value="1">生效</option>
		</select>
	</div>
	<div id="state_div" style="display:none">
		<table>
		<tr>
			<td>状态</td>
			<td>
				<select id="zt">
					<option value="">请选择</option>
					<option value="0">待生成报告</option>
					<option value="1">已生成报告</option>
				</select>
			</td>
		</tr>
		</table>
	</div>
	<div id="arc_div" style="display:none">
		<table>
		<tr>
			<td>是否带发票</td>
			<td>
				<select id="fp">
					<option value="0">否</option>
					<option value="1">是</option>
				</select>
			</td>
		</tr>
		</table>
	</div> -->
<!-- </div> -->
</body>
</html>