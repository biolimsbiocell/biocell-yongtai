﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/karyotyping/karyotypingTaskItem.js"></script>
</head>
<body>
	<div id="karyotypingTaskItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	<div id="bat_isReport_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>是否异常报告</span></td>
                <td><select id="isReport" style="width:100">
    					<option value="1">是</option>
    					<option value="0">否</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_isGood_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.isQualified"/></span></td>
                <td><select id="isGood" style="width:100">
    					<option value="1"><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0"><fmt:message key="biolims.common.disqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_isCommit_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>是否提交</span></td>
                <td><select id="isCommit"  style="width:100">
    					<option value="1">是</option>
    					<option value="0">否</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_area_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>结果</span></td>
				<td><textarea id="result" style="width: 300px;height: 80px;"></textarea></td>
			</tr>
		</table>
	</div>
	<div id="bat_area1_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span>结果</span></td>
				<td><textarea id="result1" style="width: 300px;height: 80px;"></textarea></td>
			</tr>
		</table>
	</div>
</body>
</html>
