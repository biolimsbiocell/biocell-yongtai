<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=karyotypingTask&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/karyotyping/karyotypingTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="karyotypingTaskTemppage"></div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.user.itemNo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyotypingTask_id"
                   	 name="karyotypingTask.id" title="编号"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="karyotypingTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.name"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="30" maxlength="25" id="karyotypingTask_name"
                   	 name="karyotypingTask.name" title="描述"
                   	   
	value="<s:property value="karyotypingTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择分析人"
				hasHtmlFrame="true"
				html="${ctx}/experiment/karyotyping/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyotypingTask_createUser').value=rec.get('id');
				document.getElementById('karyotypingTask_createUser_name').value=rec.get('name');" /> --%>
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.analysiser"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyotypingTask_createUser_name"  value="<s:property value="karyotypingTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="karyotypingTask_createUser" name="karyotypingTask.createUser.id"  value="<s:property value="karyotypingTask.createUser.id"/>" > 
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.analysisData"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyotypingTask_createDate"
                   	 name="karyotypingTask.createDate" title="分析时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="karyotypingTask.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showtemplate" title="选择模板"
				hasHtmlFrame="true"
				html="${ctx}/system/template/template/templateSelectByType.action"
				isHasSubmit="false" functionName="TemplateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyotypingTask_template').value=rec.get('id');
				document.getElementById('karyotypingTask_template_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >模板</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyotypingTask_template_name"  value="<s:property value="karyotypingTask.template.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="karyotypingTask_template" name="karyotypingTask.template.id"  value="<s:property value="karyotypingTask.template.id"/>" > 
 						<img alt='选择模板' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			
			
			
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showacceptUser = Ext.get('showacceptUser');
showacceptUser.on('click', 
 function UserGroupFun(){
var win = Ext.getCmp('UserGroupFun');
if (win) {win.close();}
var UserGroupFun= new Ext.Window({
id:'UserGroupFun',modal:true,title:'Selection of experimental group',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=UserGroupFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserGroupFun.close(); }  }]  });     UserGroupFun.show(); }
);
});
 function setUserGroupFun(rec){
document.getElementById('karyotypingTask_acceptUser').value=rec.get('id');
				document.getElementById('karyotypingTask_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('UserGroupFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.acceptUserName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyotypingTask_acceptUser_name"  value="<s:property value="karyotypingTask.acceptUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="karyotypingTask_acceptUser" name="karyotypingTask.acceptUser.id"  value="<s:property value="karyotypingTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			<%-- <g:LayOutWinTag buttonId="showproducerTask" title="选择制片上机"
				hasHtmlFrame="true"
				html="${ctx}/experiment/producer/producerTask/producerTaskSelect.action"
				isHasSubmit="false" functionName="ProducerTaskFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('karyotypingTask_producerTask').value=rec.get('id');
				document.getElementById('karyotypingTask_producerTask_name').value=rec.get('name');" /> --%>
					<%-- <td class="label-title" >选择制片上机</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="karyotypingTask_producerTask_name"  value="<s:property value="karyotypingTask.producerTask.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="karyotypingTask_producerTask" name="karyotypingTask.producerTask.id"  value="<s:property value="karyotypingTask.producerTask.id"/>" > 
 						<img alt='选择制片上机' id='showproducerTask' src='${ctx}/images/img_lookup.gif' class='detail' onclick="ProducerTaskFun()"/>                  		
                   	</td> --%>
               	 	<td class="label-title"  style="display:none"  >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="karyotypingTask_state"
                   	 name="karyotypingTask.state" title="状态"
                   	   
	value="<s:property value="karyotypingTask.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
                   	
                   	          	 	<td class="label-title" ><fmt:message key="biolims.common.workFlowStateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="karyotypingTask_stateName"
                   	 name="karyotypingTask.stateName" title="工作流状态"
                   	   class="text input readonlytrue" readonly="readonly"
	value="<s:property value="karyotypingTask.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			</tr>
			<tr>		
			
     
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="karyotypingTask_note"
                   	 name="karyotypingTask.note" title="备注"
                   	   
	value="<s:property value="karyotypingTask.note"/>"
                   	  />
                   	  
                   	</td>
			
			<td class="label-title"><fmt:message key="biolims.common.attachment" /></td><td><input type="hidden" id="fj" value="${requestScope.fileNum}"></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.allHave"/>
							${requestScope.fileNum}<fmt:message key="biolims.common.total"/></span></td>
			</tr>
			
			
            </table>
            <input type="hidden" name="karyotypingTaskItemJson" id="karyotypingTaskItemJson" value="" />
<!--             <input type="hidden" name="karyotypingTaskTempJson" id="karyotypingTaskTempJson" value="" /> -->
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="karyotypingTask.id"/>" />
            </form>
           <!--  <div id="tabs">
            <ul>
			<li><a href="#karyotypingTaskItempage">核型分析明细</a></li>
			<li><a href="#karyotypingTaskTemppage">核型分析临时表</a></li>
           	</ul>  -->
			<div id="karyotypingTaskItempage" width="100%" height:10px></div>
<!-- 			<div id="karyotypingTaskTemppage" width="100%" height:10px></div> -->
			<!-- </div> -->
        	</div>
	</body>
	</html>
