<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=otherTask&id=${otherTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/other/otherTaskEdit.js"></script>
<div style="float:left;width:25%" id="otherTaskTempPage">
		<!-- <div id="tabs1">
            <ul>
	            <li><a href="#linchuang">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="linchuang" style="width: 100%;height: 10px;"></div>
				<div id="keji" style="width: 100%;height: 10px;"></div>
		 </div> -->

</div>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_id"
                   	 name="otherTask.id" title="<fmt:message key="biolims.common.serialNumber"/>" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="otherTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_name"
                   	 name="otherTask.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="otherTask.name"/>"
                   	  />
                   	  
                   	</td>
			
				 <td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="otherTask_reciveUser_name"  value="<s:property value="otherTask.reciveUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="otherTask_reciveUser" name="otherTask.reciveUser.id"  value="<s:property value="otherTask.reciveUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser();" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>	
			
<%-- 			<g:LayOutWinTag buttonId="showreciveUser" title='<fmt:message key="biolims.common.chooseTheExperimenter"/>' --%>
<%-- 				hasHtmlFrame="true" --%>
<%-- 				html="${ctx}/core/user/userSelect.action" --%>
<%-- 				isHasSubmit="false" functionName="UserFun1"  --%>
<%--  				hasSetFun="true" --%>
<%-- 				documentId="otherTask_reciveUser" --%>
<%-- 				documentName="otherTask_reciveUser_name" /> --%>
				
			
			
<%--                	 	<td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td> --%>
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<%--  						<input type="text" size="20" readonly="readOnly"  id="otherTask_reciveUser_name"  value="<s:property value="otherTask.reciveUser.name"/>"  readonly="readOnly"  /> --%>
<%--  						<input type="hidden" id="otherTask_reciveUser" name="otherTask.reciveUser.id"  value="<s:property value="otherTask.reciveUser.id"/>" >  --%>
<%--  						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
<!--                    	</td> -->
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="otherTask_reciveDate"
                   	 name="otherTask.reciveDate" title="<fmt:message key="biolims.common.experimentalTime"/>"
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="otherTask.reciveDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>	
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun2" 
 				hasSetFun="true"
				documentId="otherTask_createUser"
				documentName="otherTask_createUser_name" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="otherTask_createUser_name"  value="<s:property value="otherTask.createUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="otherTask_createUser" name="otherTask.createUser.id"  value="<s:property value="otherTask.createUser.id"/>" > 
 						<%-- <img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />        --%>            		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="otherTask_createDate"
                   	 name="otherTask.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly"
                   	    value="<s:date name="otherTask.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			</tr>
			<tr>
			<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="otherTask_confirmDate"
                   	 name="otherTask.confirmDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:date name="otherTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.template"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="otherTask_template_name"  value="<s:property value="otherTask.template.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="otherTask_template" name="otherTask.template.id"  value="<s:property value="otherTask.template.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectATemplate"/>' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail' onClick="TemplateFun()"      />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showacceptUser" title='<fmt:message key="biolims.common.selectGroup"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/userGroup/userGroupSelect.action"
				isHasSubmit="false" functionName="UserGroupFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('otherTask_acceptUser').value=rec.get('id');
				document.getElementById('otherTask_acceptUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="otherTask_acceptUser_name"  value="<s:property value="otherTask.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="otherTask_acceptUser" name="otherTask.acceptUser.id"  value="<s:property value="otherTask.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	
					
               	 	<%-- <td class="label-title" >index</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_indexa"
                   	 name="otherTask.indexa" title="index"
                   	   
	value="<s:property value="otherTask.indexa"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_state"
                   	 name="otherTask.state" title="状态"
                   	   
	value="<s:property value="otherTask.state"/>"
                   	  />
                   	  
                   	</td> --%>
			</tr>
			<tr>
			
			<td class="label-title" ><fmt:message key="biolims.common.stateName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_stateName"
                   	 name="otherTask.stateName" title="<fmt:message key="biolims.common.stateName"/>" class="text input readonlytrue" readonly="readOnly"
                   	   
	value="<s:property value="otherTask.stateName"/>"
                   	  />
                   	  
                   	</td>
               	 	
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_note"
                   	 name="otherTask.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="otherTask.note"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			</tr>
		<%-- 	<tr>
			
			
               	 	<td class="label-title" >容器数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_maxNum"
                   	 name="otherTask.maxNum" title="容器数量"
                   	   
	value="<s:property value="otherTask.maxNum"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >质控品数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="otherTask_qcNum"
                   	 name="otherTask.qcNum" title="质控品数量"
                   	   
	value="<s:property value="otherTask.qcNum"/>"
                   	  />
                   	  
                   	</td>
			</tr> --%>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
			
            </table>
            <input type="hidden" name="otherTaskItemJson" id="otherTaskItemJson" value="" />
            <input type="hidden" name="otherTaskTemplateJson" id="otherTaskTemplateJson" value="" />
            <input type="hidden" name="otherTaskReagentJson" id="otherTaskReagentJson" value="" />
            <input type="hidden" name="otherTaskCosJson" id="otherTaskCosJson" value="" />
            <input type="hidden" name="otherTaskResultJson" id="otherTaskResultJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="otherTask.id"/>" />
            </form>
            <div id="otherTaskItempage" width="100%" height:10px></div>
            <div id="tabs">
            <ul>
			<li><a href="#otherTaskTemplatepage"><fmt:message key="biolims.common.executionStep"/></a></li>
			<li><a href="#otherTaskReagentpage"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#otherTaskCospage"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul> 
		
			<div id="otherTaskTemplatepage" width="100%" height:10px></div>
			<div id="otherTaskReagentpage" width="100%" height:10px></div>
			<div id="otherTaskCospage" width="100%" height:10px></div>
			
			</div>
			<div id="otherTaskResultpage" width="100%" height:10px></div>
        	</div>
	</body>
	</html>
