<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}

#yesSpan {
	display: none;
}

#auditDiv {
	display: none;
}

#btn_changeState {
	display: none;
}

.sampleNumberInput {
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 200px;
    top: 212px;
}

.sampleNumberInputTow{
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 510px;
    top: 212px;
}

.sampleNumberDiv {
    position: absolute;
    left: 140px;
    top: 223px;
}
.sampleNumberDivtow {
    position: absolute;
	left: 466px;
    top: 223px;
}

.sampleNumberbutton {
    position: absolute;
    left: 760px;
    top: 212px;
    border-radius: 4px;
    border: 1px solid #ccc;
    background-color: #5cb85c;
    width: 38px;
    height: 36px;
}

</style>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						QA审批
					</h3>
					<small style="color: #fff;"> 创建人: <span
						id="qaAudit_createUser"><s:property
								value=" qaAudit.createUser.name" /></span> 创建时间: <span
						id="qaAudit_createDate"><s:date
								name="qaAudit.createDate" format="yyyy-MM-dd" /></span> 状态: <span id="qaAudit_state"><s:property
								value="qaAudit.stateName" /></span> 
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
								<input type="hidden" id="createUser_id" name="qaAudit.createUser.id"
								value="<s:property value=" qaAudit.createUser.id "/>" />
								<input type="hidden" id="createDate" name="qaAudit.createDate"
								value="<s:property value=" qaAudit.createDate "/>" />
								<input type="hidden" id="state" name="qaAudit.state"
								value="<s:property value=" qaAudit.state "/>" />
								<input type="hidden" id="stateName" name="qaAudit.stateName"
								value="<s:property value=" qaAudit.stateName "/>" />
							<input type="hidden" id="bpmTaskId"
								value="${requestScope.bpmTaskId}" /> <br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> QA审核编号
										</span> <input list="sampleIdOne"
											changelog="<s:property value="qaAudit.id"/>" type="text"
											size="20" maxlength="25" id="qaAudit_id"
											name="qaAudit.id" class="form-control"
											title="QA审核编号"
											value="<s:property value=" qaAudit.id "/>" /> <span
											class="input-group-btn">
										</span> 
									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 备注 
										</span> <input list="sampleName"
											changelog="<s:property value="qaAudit.name"/>"
											type="text" size="20" maxlength="50" id="qaAudit_name"
											name="qaAudit.name" class="form-control"
											title="备注"
											value="<s:property value=" qaAudit.name "/>" />

									</div>
								</div>

								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">事件类型</span> 
										<input
											changelog="<s:property value="qaAudit.eventType.name"/>"
											type="text" size="20" maxlength="25"
											id="qaAudit_eventType_name" name="qaAudit.eventType.name"
											title="选择原辅料类型" class="form-control"
											value="<s:property value="qaAudit.eventType.name"/>"
											readonly="readonly" />
										<input
											changelog="<s:property value="qaAudit.eventType.id"/>"
											type="hidden" size="20" maxlength="25"
											id="qaAudit_eventType_id" name="qaAudit.eventType.id"
											title="选择原辅料类型" class="form-control"
											value="<s:property value="qaAudit.eventType.id"/>"
											readonly="readonly" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" onclick="eventType()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
											 <%-- <select class="form-control" id="qaAudit_eventType" name="qaAudit.eventType">
												<option value="1"
													<s:if test='qaAudit.eventType=="1"'>selected="selected"</s:if>>
													不合格销毁</option>
												<option value="2"
													<s:if test='qaAudit.eventType=="2"'>selected="selected"</s:if>>
													不合格处理</option>
												<option value="3"
													<s:if test='qaAudit.eventType=="3"'>selected="selected"</s:if>>
													产品停止生产</option>	
											</select> --%>
									</div>
								</div>

							</div>
							<%-- <div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">产品名称</span> <input list="sampleName"
											changelog="<s:property value="qaAudit.productName"/>"
											type="text" size="20" maxlength="50" id="qaAudit_productName"
											name="qaAudit.productName" class="form-control"
											title="产品名称"
											value="<s:property value=" qaAudit.productName "/>" />
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon">产品批号</span> <input type="text"
											changelog="<s:property value="qaAudit.productBatchNumber"/>"
											size="20" maxlength="50" class="form-control"
											id="productBatchNumber" name="qaAudit.productBatchNumber"
											title="产品批号"
											value="<s:property value=" qaAudit.productBatchNumber "/>" />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">

										<span class="input-group-addon">产品规格</span> <input type="text"
											changelog="<s:property value="qaAudit.productSpecifications"/>"
											size="20" maxlength="50" class="form-control"
											id="qaAudit_productSpecifications" name="qaAudit.productSpecifications"
											title="产品规格"
											value="<s:property value=" qaAudit.productSpecifications "/>" />

									</div>
								</div>--%>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									<span class="input-group-addon">调查组组长</span> 
										<input type="text" size="20" readonly="readOnly" 
												id="qaAudit_groupLeader_name"  changelog="<s:property value="qaAudit.groupLeader.name"/>"
												value="<s:property value="qaAudit.groupLeader.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="qaAudit_groupLeader_id" name="qaAudit.groupLeader.id"
												value="<s:property value="qaAudit.groupLeader.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showgroupLeader()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
									<span class="input-group-addon">调查组员</span>
										<input 
											size="50" maxlength="100" type="hidden" id="qaAudit_groupMemberIds"
											name="qaAudit.groupMemberIds"  changelog="<s:property value="qaAudit.groupMemberIds"/>"
											class="form-control"   
											value="<s:property value="qaAudit.groupMemberIds"/>"/>  
                   						<input type="text"
											size="50" maxlength="100" id="qaAudit_groupMembers"
											name="qaAudit.groupMembers"  changelog="<s:property value="qaAudit.groupMembers"/>"
											class="form-control"   
											value="<s:property value="qaAudit.groupMembers"/>"
						 				/>  
						 				<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showGroupMembers()">
											<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
									<span class="input-group-addon">部门负责人</span> 
										<input type="text" size="20" readonly="readOnly" 
												id="qaAudit_departmentUser_name"  changelog="<s:property value="qaAudit.departmentUser.name"/>"
												value="<s:property value="qaAudit.departmentUser.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="qaAudit_departmentUser_id" name="qaAudit.departmentUser.id"
												value="<s:property value="qaAudit.departmentUser.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showdepartmentUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									<span class="input-group-addon">监控人</span> 
										<input type="text" size="20" readonly="readOnly"
												id="qaAudit_monitor_name"  changelog="<s:property value="qaAudit.monitor.name"/>"
												value="<s:property value="qaAudit.monitor.name"/>"
												class="form-control" /> 
										<input type="hidden"
												id="qaAudit_monitor_id" name="qaAudit.monitor.id"
												value="<s:property value="qaAudit.monitor.id"/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button" 
												onClick="showmonitor()">
											<i class="glyphicon glyphicon-search"></i>
										</span>		
									</div>
								</div>
							</div>
							<div class="row">
									<div class="col-md-8 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">描述</span> 
                   						<textarea   id="qaAudit_describe" name="qaAudit.describe" placeholder="长度限制2000个字符"    class="form-control"   
											  style="overflow: hidden; width: 420px; height: 80px;"  ><s:property value="qaAudit.describe"/></textarea>  
									</div>
									</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<button type="button" class="btn btn-info" onclick="fileUp()">上传附件</button>
											<span class="text label"><fmt:message
													key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
													key="biolims.common.attachment" /></span>

											<button type="button" class="btn btn-info"
												onclick="fileView()">查看附件</button>
								</div>			
							</div>

							<input type="hidden" id="id_parent_hidden"
								value="<s:property value=" qaAudit.id "/>" /> 
							<input type="hidden"  id="changeLog" name="changeLog"/> 
							<input type="hidden"  id="changeLogItem" name="changeLogItem" /> 
							<input type="hidden" 
								name="qaAuditItemJson" id="qaAuditItemJson" value="" />
							<input type="hidden" 
								name="qaAuditStorageItemJson" id="qaAuditStorageItemJson" value="" />
						</form>
					</div>
				</div>
				<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="qaAuditItem" style="font-size: 14px;"></table>
				</div>
				<div class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="qaAuditStorageItem" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/qaAudit/qaAuditEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/qaAudit/qaAuditItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/qaAudit/qaAuditStorageItem.js"></script>
</body>
</html>