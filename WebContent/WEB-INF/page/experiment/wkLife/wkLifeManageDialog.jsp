﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wkLifeManageDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.libraryNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkManage_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="wkManage_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">Blend Code</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkManage_index" searchField="true" name="index"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.volume"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkManage_bulk" searchField="true" name="bulk"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.concentration"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkManage_chroma" searchField="true" name="chroma"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.resultsDetermine"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkManage_resultDecide" searchField="true" name="resultDecide"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.theNextStepTo"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkManage_nextFlow" searchField="true" name="nextFlow"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.note"/></td>
               	<td align="left">
                    		<input type="text" maxlength="75" id="wkManage_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.libraryNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="wkManage_code" searchField="true" name="code"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_wkManage_div"></div>
   		
</body>
</html>



