﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wKLifeItem.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
<form name='excelfrmi' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtmi' name='gridhtm' value=''/></form>
<div id="bat_libType_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>选择文库类型</span></td>
                <td><select id="libType" style="width:100">
    					<option value="0" <s:if test="libType==0">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="libType==1">selected="selected" </s:if>>小片段文库（350bp,500bp,800bp）</option>
    					<option value="2" <s:if test="libType==2">selected="selected" </s:if>>大片段文库（2k）</option>
    					<option value="3" <s:if test="libType==3">selected="selected" </s:if>>大片段文库（3k）</option>
    					<option value="4" <s:if test="libType==4">selected="selected" </s:if>>大片段文库（5k）</option>
    					<option value="5" <s:if test="libType==5">selected="selected" </s:if>>大片段文库（8k）</option>
    					<option value="6" <s:if test="libType==6">selected="selected" </s:if>>大片段文库（10k）</option>
    					<option value="7" <s:if test="libType==7">selected="selected" </s:if>>大片段文库（20k）</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	
	<div id="bat_isInit_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>是否重建库</span></td>
                <td><select id="libType" style="width:100">
    					<option value="0" <s:if test="isInit==0">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="isInit==1">selected="selected" </s:if>>是</option>
    					<option value="2" <s:if test="isInit==2">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_isRna_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>是否有RNA污染</span></td>
                <td><select id="libType" style="width:100">
    					<option value="0" <s:if test="isRna==0">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="isRna==1">selected="selected" </s:if>>是</option>
    					<option value="2" <s:if test="isRna==2">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	

	<div id="wKItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_productNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.dicSampleNum"/></span></td>
				<td><input id="productNum"/></td>
			</tr>
		</table>
		</div>
		
		<div id="bat_sampleConsume_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.sampleConsume"/></span></td>
				<td><input id="sampleConsume"/></td>
			</tr>
		</table>
		</div>
		
		<div id="bat_expectNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.expectedFlux"/>(%)</span></td>
				<td><input id="expectNum"/></td>
			</tr>
		</table>
		</div>
		<div id="bat_counts_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.boardNum"/></span></td>
				<td><input id="counts"/></td>
			</tr>
		</table>
	</div>
	<div id="many_batItem_div" style="display: none">
		<div class="ui-widget;">
			<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_batItem_text" style="width:650px;height: 339px"></textarea>
	</div>
</body>
</html>


