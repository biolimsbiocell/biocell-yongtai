﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wKLifeItemManage.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.sampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="code" id="wKItemManage_code" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.originalSampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="wKItemManage_sampleCode" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			Blend Code
			</label>
			</td>
			<td>
			<input type="text"  name="indexa" id="wKItemManage_indexa" searchField="true" >
			</td>
			<td>
			<input type="button" onClick="selectWkItemInfo()" value="<fmt:message key="biolims.common.find"/>">
			</td>
			</tr>
		</table>
	<div id="wKItemManagediv"></div>
	<div id="bat_result1_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="result1" style="width:100">
                		<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1"><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0"><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_next1_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td>
                <td><select id="nextFlow1"  style="width:100">
                		<option value="" ><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0"><fmt:message key="biolims.common.qualityTcontrol"/></option>
    					<option value="1"><fmt:message key="biolims.common.qualityQcontrol"/></option>
    					<option value="2"><fmt:message key="biolims.common.termination"/></option>
    					<option value="3"><fmt:message key="biolims.common.putInStorage"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>


