<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/qualityProductAbnormal.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript" src="${ctx}/javascript/commonSearch.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.qualityProductSerialNumber"/>
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="qualityProduct_sampleCode" searchType ="1" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.libraryNumber"/>
			</label>
			</td>
			<td>
			<input type="text"  name="wkId" id="qualityProduct_wkId" searchType ="1" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.sampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="code" id="qualityProduct_code" searchType ="1" searchField="true" >
			</td>
			<!-- <td >
			<label class="text label" title="输入查询条件">
			处理结果
			</label>
			</td>
			<td>
			<select id="qualityProduct_result" name="result" searchType ="1" searchField="true" style="width:100">
                		<option value=" ">请选择</option>
    					<option value="1">合格</option>
    					<option value="0">不合格</option>
				</select>
			</td> -->
			<td>
			<input type="button" onClick="selectWkqualityProductInfo()" value="<fmt:message key="biolims.common.find"/>">
			</td>
			</tr>
		</table>
		<div id="qualityProductAbnormaldiv"></div>
		<div id="bat_next_div" style="display: none"></div>
		<div id="bat_ok_div" style="display: none"></div>
</body>
</html>



