﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wkLifeRepeat.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 
               	 	<td class="label-title" ><fmt:message key="biolims.common.libraryNumber"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkRepeat_wkCode"
                   	 name="wkCode" searchField="true" title="<fmt:message key="biolims.common.libraryNumber"/>"    />
                   	  
                   	</td>
		
                   	 <td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkRepeat_code"
                   	 name="code" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    />

			</tr>
			<tr>
			 		<td class="label-title" ><fmt:message key="biolims.common.originalSampleCode"/></td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkRepeat_sampleCode"
                   	 name="sampleCode" searchField="true" title="<fmt:message key="biolims.common.originalSampleCode"/>"    />
                   	  
                   	</td>
               	 	<!-- <td class="label-title" >INDEX</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="125" id="wkRepeat_indexa"
                   	name="indexa" searchField="true" title="INDEX"    />
                   	</td> -->
			</tr>
            </table>
		</form>
		</div>
		<div id="wkRepeatdiv"></div>
		
		<div id="bat_method_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.affirmToPerform"/></span></td>
                <td><select id="run"  style="width:100">
               			<option value="" <s:if test="method==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="method==7">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="method==5">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
</body>
</html>


