﻿﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wkLifeAbnormalBack.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<!-- <div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 
               	 	<td class="label-title" >文库编号</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkAbnormalBack_wkCode"
                   	 name="wkCode" searchField="true" title="文库编号"    />
                   	  
                   	</td>
		
                   	 <td class="label-title" >样本编号</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkAbnormalBack_code"
                   	 name="code" searchField="true" title="样本编号"    />
                   	  
                   	</td>
                   
               	 	<td class="label-title" >结果判定</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkAbnormalBack_resultDecide"
                   	 name="resultDecide" searchField="true" title="结果判定"    />
                   	</td>
                   	
                   	
               	 	<td class="label-title" >下一步流向</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkAbnormalBack_nextFlow"
                   	 name="nextFlow" searchField="true" title="下一步流向"    />
                   	</td>
			</tr>
			<tr>
			 		<td class="label-title" >原始样本编号</td>
                   	<td align="left"  >
					<input type="text" size="20" maxlength="25" id="wkAbnormalBack_sampleCode"
                   	 name="sampleCode" searchField="true" title="原始样本编号"    />
                   	  
                   	</td>
               	 	<td class="label-title" >INDEX</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="125" id="wkAbnormalBack_indexa"
                   	name="indexa" searchField="true" title="INDEX"    />
                   	</td>
                   	
               	 	<td class="label-title" >是否执行</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="wkAbnormalBack_isExecute"
                   	 name="isExecute" searchField="true" title="是否执行"    />
                   	</td>
                   	
               	 	<td class="label-title" >反馈时间</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="150" id="wkAbnormalBack_backTime"
                   	 name="backTime" searchField="true" title="反馈时间"    />
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div> 
		
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.libraryNumber"/>
			</label>
			</td>
			<td>
			<input type="text"  name="wkCode" id="wkAbnormalBack_wkCode" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.originalSampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="wkAbnormalBack_sampleCode" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.processingOpinion"/>
			</label>
			</td>
			<td>
			<select id="wkAbnormalBack_method" name="method" searchField="true" style="width:100">
                		<option value=" "><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0"><fmt:message key="biolims.common.libraryQualityControl"/></option>
    					<option value="1">2100 or Caliper</option>
    					<option value="6"><fmt:message key="biolims.common.qualityQcontrol"/></option>
    					<option value="2"><fmt:message key="biolims.common.buildLibrariesAgain"/></option>
    					<option value="3"><fmt:message key="biolims.common.termination"/></option>
    					<option value="4"><fmt:message key="biolims.common.putInStorage"/></option>
    					<option value="5"><fmt:message key="biolims.common.pause"/></option>
				</select>
			</td>
			<td>
			<input type="button" onClick="selectWkInfo()" value="<fmt:message key="biolims.common.find"/>">
			</td>
			</tr>
		</table>
		-->
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<div id="wkAbnormalBackdiv"></div>

		<div id="bat_next_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td>
                <td><select id="nextFlow"  style="width:100">
                		<option value="" <s:if test="nextFlow==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0" <s:if test="nextFlow==0">selected="selected" </s:if>><fmt:message key="biolims.common.qualityTcontrol"/></option>
    					<option value="1" <s:if test="nextFlow==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualityQcontrol"/></option>
    					<option value="2" <s:if test="nextFlow==2">selected="selected" </s:if>><fmt:message key="biolims.common.buildLibrariesAgain"/></option>
    					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>><fmt:message key="biolims.common.putInStorage"/></option>
    					<option value="4" <s:if test="nextFlow==4">selected="selected" </s:if>><fmt:message key="biolims.common.feedbackToTheProjectTeam"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_ok_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.affirmToPerform"/></span></td>
                <td><select id="isExecute"  style="width:100">
<!--                			<option value="" >请选择</option> -->
    					<option value="1"><fmt:message key="biolims.common.yes"/></option>
    					<option value="0"><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_result_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.processingResults"/></span></td>
                <td><select id="result"  style="width:100">
               			<option value="" ><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1"><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0"><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>

</body>
</html>


