﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wKLifeTaskModify.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="25" id="wK_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	</td>
			</tr>
			<tr>                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  		<input type="text" size="20" maxlength="25" id="wK_qcNum"
                   	 	name="qcNum" searchField="true" title="<fmt:message key="biolims.common.qualityProductQuantity"/>"    />
					<input type="text" size="30" maxlength="25" id="wK_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	</td>
			</tr>
			<tr>                   	
			<g:LayOutWinTag buttonId="showreciveUser" title='<fmt:message key="biolims.common.chooseTheExperimenter"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wK_reciveUser').value=rec.get('id');
				document.getElementById('wK_reciveUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimenter"/></td>
                   	<td align="left"  >
 						<input type="text" size="30"   id="wK_reciveUser_name" searchField="true"  name="reciveUser.name"  value="" class="text input" />
 						<input type="hidden" id="wK_reciveUser" name="wK.reciveUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startreciveDate" name="startreciveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="reciveDate1" name="reciveDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreciveDate" name="endreciveDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="reciveDate2" name="reciveDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
			<tr>                   	
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="CreateUserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wK_createUser').value=rec.get('id');
				document.getElementById('wK_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="30"   id="wK_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="wK_createUser" name="wK.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseFromPeople"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
						<tr>
                  	<g:LayOutWinTag buttonId="showAcceptUser" title='<fmt:message key="biolims.common.selectGroup"/>'
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('WkTask_acceptUser').value=rec.get('id'); 
					document.getElementById('WkTask_acceptUser_name').value=rec.get('name');" /> 
			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
                   	<td align="left"  >
 						<input type="text" size="30" readonly="readOnly"  id="WkTask_acceptUser_name"  value="<s:property value="WkTask.acceptUser.name"/>" />
 						<input type="hidden" id="WkTask_acceptUser" name="WkTask.acceptUser.id"  value="<s:property value="WkTask.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>

			</tr>
			<tr>                   	
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>

            </table>
		</form>
		</div>
		<div id="show_wKSampleTask_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_wKSampleTask_tree_page"></div>
</body>
</html>



