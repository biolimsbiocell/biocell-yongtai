﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=wkAbnormal&id=${wkAbnormal.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wkLifeAbnormalEdit.js"></script>

            <div id="tabs">
            <ul>
			<li><a href="#plasmaAbnormalBackpage"><fmt:message key="biolims.common.bloodabnormalfeedback"/></a></li>
			<li><a href="#wkAbnormalBackpage"><fmt:message key="biolims.common.libraryOfAbnormalFeedback"/></a></li>
           	</ul> 
			<div id="plasmaAbnormalBackpage" width="100%" height:10px></div>
			<div id="wkAbnormalBackpage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
