﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>

		<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
<g:LayOutWinTag buttonId="dnawzBtn" title='<fmt:message key="biolims.common.selectLocation"/>' hasHtmlFrame="true"
		html="${ctx}/storage/position/showStoragePositionTree1.action?setStoragePostion=true"
		isHasSubmit="false" functionName="showStoragePositionFund"
		hasSetFun="true" documentId="wkReceive_storagePrompt"
		documentName="wkReceive_storagePrompt_name" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=wKReceive&id=${wkReceive.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wKLifeReceiveEdit.js"></script>
  <div style="float:left;width:25%" id="wkReceiveTempPage"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
						<td colspan="9">
							<div class="standard-section-header type-title">
								<label><fmt:message key="biolims.common.basicInformation"/></label>
							</div>
						</td>
				</tr>
			<tr>
			
			
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="wkReceive_id"
                   	 name="wkReceive.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="wkReceive.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="wkReceive_name"
                   	 name="wkReceive.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="wkReceive.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<%-- <td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wkReceive_describe"
                   	 name="wkReceive.describe" title="描述"
                   	   
	value="<s:property value="wkReceive.describe"/>"
                   	  />
                   	  
                   	</td> --%>
			
			
			
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.recipient"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wkReceive_receiver_name"  value="<s:property value="wkReceive.receiver.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wkReceive_receiver" name="wkReceive.receiver.id"  value="<s:property value="wkReceive.receiver.id"/>" > 
                   	</td>
			
			
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  <input type="text" size="20" maxlength="25" id="wkReceive_receiverDate"
                   	 name="wkReceive.receiverDate" title="<fmt:message key="biolims.common.receivingDate"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wkReceive.receiverDate"/>"
                   	  />
                   	</td>
			
					<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.workflowStateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="15" id="wkReceive_state"
                   	 name="wkReceive.state" title="<fmt:message key="biolims.common.workflowStateID"/>"
                   	   
	value="<s:property value="wkReceive.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
					<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wkReceive_stateName"
                   	 name="wkReceive.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wkReceive.stateName"/>"
                   	  />

                   	<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
			</tr>
			
				<tr>
						<!-- <td colspan="9">
							<div class="standard-section-header type-title">
								<label>基本信息</label>
							</div>
						</td> -->
				</tr>
				<tr>
<%-- 					<g:LayOutWinTag buttonId="showstoragePrompt" title="选择储位提示" --%>
<%-- 					hasHtmlFrame="true" --%>
<%-- 					html="${ctx}/experiment/wk/wKReceive/storagePositionSelect.action" --%>
<%-- 					isHasSubmit="false" functionName="StoragePositionFun"  --%>
<%-- 	 				hasSetFun="true" --%>
<%-- 					extRec="rec" --%>
<%-- 					extStr="document.getElementById('wkReceive_storagePrompt').value=rec.get('id'); --%>
<%-- 					document.getElementById('wkReceive_storagePrompt_name').value=rec.get('name');" /> --%>
				
			
			
<!--                	 	<td class="label-title" >储位提示</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<%--  						<input type="text" size="20" readonly="readOnly"  id="wkReceive_storagePrompt_name"  value="<s:property value="wkReceive.storagePrompt.name"/>" class="text input readonlytrue" readonly="readOnly"  /> --%>
<%--  						<input type="hidden" id="wkReceive_storagePrompt" name="wkReceive.storagePrompt.id"  value="<s:property value="wkReceive.storagePrompt.id"/>" >  --%>
<%--  						<img alt='选择储位提示' id='showstoragePrompt' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
<!--                    	</td> -->
						<%-- 	<td class="label-title">储位提示</td>
					<td class="requiredcolumn"  colspan="9">
						<input type="text"
							class="input_parts text input  false false"
							name="wkReceive.storagePrompt.id" id="wkReceive_storagePrompt"
							value='<s:property value="wkReceive.storagePrompt.id"/>'
							title="存储位置 " size="10"
							maxlength="30" readonly="readOnly"> 
							<img alt='选择' id='dnawzBtn'
							name='show-btn' src='${ctx}/images/img_menu.gif'
							class='detail' /> 
						<input type="text" class="input_parts text input  readonlytrue"
							name="wkReceive.storagePrompt.name" id="wkReceive_storagePrompt_name"
							readonly="readonly"
							value='<s:property value="wkReceive.storagePrompt.name"/>'
							title="存储位置 "  size="30"
							maxlength="62"> 
					</td> --%>
				</tr>
			
            </table>
            <input type="hidden" name="wKReceiveItemJson" id="wKReceiveItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wkReceive.id"/>" />
            </form>
            <div id="tabs">
<!--             <ul> -->
<!-- 			<li><a href="#wKReceiveItempage">血浆接收明细</a></li> -->
<!--            	</ul>  -->
			<div id="wKReceiveItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
