﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wKLifeTaskModifyEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="wKTastModifyTempPage">
<!-- <div id="tabs1">
            <ul>
	            <li><a href="#chanqian">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="chanqian" width="100%" height:10px></div>
				<div id="keji" width="100%" height:10px></div>
		 </div> -->

</div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="qcNum" value="${requestScope.qcNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
              <input type="hidden" name="wk_method" id="wk_method" value="<%=request.getParameter("method") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="40" id="type"  value="wk" />
                   	<input type="text" size="20" maxlength="25" id="wk_id"
                   	 name="wk.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="wk.id"/>"
                   	  />
                   	  <input type="hidden" size="30" maxlength="60" id="wk_maxNum" name="wk.maxNum" title="<fmt:message key="biolims.common.containerQuantity"/>"
							value="<s:property value="wk.maxNum"/>"
                   	  />
                   	  <input type="hidden" size="30" maxlength="60" id="wk_qcNum" name="wk.qcNum" title="<fmt:message key="biolims.common.qualityProductQuantity"/>"
							value="<s:property value="wk.qcNum"/>"
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_name"
                   	 name="wk.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="wk.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showreciveUser" title="选择实验员"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reciveUserFun" 
 				hasSetFun="true"
				documentId="wk_reciveUser"
				documentName="wk_reciveUser_name" />
				
			
			
               	 	<td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_reciveUser_name"  value="<s:property value="wk.reciveUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="wk_reciveUser" name="wk.reciveUser.id"  value="<s:property value="wk.reciveUser.id"/>" > 
 						<img alt='选择实验员' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_reciveDate"
                   	 name="wk.reciveDate" title="<fmt:message key="biolims.common.experimentalTime"/>"
                   	    Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
                   	  value="<s:date name="wk.reciveDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			</tr>
			<tr>
			
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('wk_createUser').value=rec.get('id');
				document.getElementById('wk_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_createUser_name"  value="<s:property value="wk.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wk_createUser" name="wk.createUser.id"  value="<s:property value="wk.createUser.id"/>" > 
 						<%-- <img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_createDate"
                   	 name="wk.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:property value="wk.createDate"/>"
                   	                      	  />
                   	  
                   	</td>
                   	
                   		<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="wk_confirmDate"
                   	 name="wk.confirmDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	 value="<s:date name="wk.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
                   	
					
			
			</tr>
			<tr>
                   	
					<td class="label-title" style="display:none">INDEX</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="25" id="wk_indexa"
                   	 name="wk.indexa" title="INDEX"
                   	   class="text input"  value="<s:property value="wk.indexa"/>" style="display:none"
                   	  />
                   	</td>
                   	
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.stateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="wk_state"
                   	 name="wk.state" title="<fmt:message key="biolims.common.stateID"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wk.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
                   	
                   	<g:LayOutWinTag buttonId="showAcceptUser" title='<fmt:message key="biolims.common.selectGroup"/>'
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('wk_acceptUser').value=rec.get('id'); 
					document.getElementById('wk_acceptUser_name').value=rec.get('name');" /> 
			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_acceptUser_name"  value="<s:property value="wk.acceptUser.name"/>"/>
 						<input type="hidden" id="wk_acceptUser" name="wk.acceptUser.id"  value="<s:property value="wk.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			
				 <td class="label-title" >第一个实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_testUserOneName" name="wk.testUserOneName" value="<s:property value="wk.testUserOneName"/>" readonly="readOnly"  />
 						<input type="hidden" id="wk_testUserOneId" name="wk.testUserOneId"  value="<s:property value="wk.testUserOneId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(1);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 				
               		</td>
               		<td class="label-title" >第二个实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_testUserTwoName" name="wk.testUserTwoName"    value="<s:property value="wk.testUserTwoName"/>" readonly="readOnly"  />
 						<input type="hidden" id="wk_testUserTwoId" name="wk.testUserTwoId"  value="<s:property value="wk.testUserTwoId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(2);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
			</tr>
			<tr>
					
               		<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" >
 						<input type="text" size="20" readonly="readOnly"  id="wk_confirmUser_name"  value="<s:property value="wk.confirmUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wk_confirmUser" name="wk.confirmUser.id"  value="<s:property value="wk.confirmUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(3);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />  
 			</tr>			
            </table>
            <input type="hidden" name="wKTastModifyItemJson" id="wKTastModifyItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wk.id"/>" />
            </form>
			<%-- <% 
			<div id="wKTastModifyItempage" width="100%" height:10px></div>
			String maxNum = (String)request.getParameter("maxNum");
			System.out.print(maxNum);
			if(maxNum!=null && !maxNum.equals("")){
			Integer max = Integer.parseInt(maxNum);
			for(int i=0;i<max;i++){
			%> --%>
			<div id = '<%="3d_image0" %>'></div>
           <div id="tabs">
           	<!--
            <ul>
			<li><a href="#wKItempage">文库明细</a></li> 
			<li><a href="#wKTemplatepage"><fmt:message key="biolims.common.executionStep"/></a></li>
			<li><a href="#wKReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#wKCospage" onClick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
			 <li><a href="#wKResultpage">文库结果</a></li> 
           	</ul> 
             <div id="wKReagentpage" width="100%" height:10px></div>
			 <div id="wKCospage" width="100%" height:10px></div>
			</div> 
			<div id="wKTemplatepage" width="100%" height:10px></div>
			<div id="wKSampleInfoCtDNApage" width="100%" height:10px></div>
			<div id="wKSampleInforRNApage" width="100%" height:10px></div>
			<div id="wKSampleInfomRNApage" width="100%" height:10px></div>
			<div id = '<%="3d_image1" %>'></div>
			-->
			<div id="wKTastModifyItempage" width="100%" height:10px></div>
			
			
        	</div>
	</body>
	</html>
