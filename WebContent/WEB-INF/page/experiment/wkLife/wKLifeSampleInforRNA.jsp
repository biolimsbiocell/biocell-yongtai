﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wKLifeSampleInforRNA.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
<form name='excelfrmr' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtmr' name='gridhtm' value=''/></form>
	<div id="wKSampleInforRNAdiv"></div>

	<div id="bat_resultr_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="resultr" style="width:100">
<!--                 		<option value="">请选择</option> -->
    					<option value="1"><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0"><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_submitr_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.whetherToCommit"/></span></td>
                <td><select id="submitr"  style="width:100">
<!--                 		<option value="">请选择</option> -->
    					<option value="1"><fmt:message key="biolims.common.yes"/></option>
    					<option value="0"><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="bat_datar_div" style="display: none">
		<table>
		<!--<tr>
				<td class="label-title"><span>浓度</span></td>
				<td><input id="contraction"/></td>
			</tr> -->
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.volume"/></span></td>
				<td><input id="wkVolumer"/></td>
			</tr>
			<!--<tr>
				<td class="label-title"><span>RIN</span></td>
				<td><input id="rin"/></td>
			</tr> -->
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.sampleSize"/></span></td>
				<td><input id="sampleNumr"/></td>
			</tr>
		</table>
		</div>
</body>
</html>


