﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img = Ext.get('doclinks_img');
doclinks_img.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.attachment"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action\?modelType=wK&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img1 = Ext.get('doclinks_img1');
doclinks_img1.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.upLoadFile"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=wKdyt&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

		
		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var doclinks_img2 = Ext.get('doclinks_img2');
doclinks_img2.on('click', 
 function doc(){
var win = Ext.getCmp('doc');
if (win) {win.close();}
var doc= new Ext.Window({
id:'doc',modal:true,title:'<fmt:message key="biolims.common.upLoadFile"/>',layout:'fit',width:900,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?modelType=wK2100&id=NEW&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 doc.close(); }  }]  });     doc.show(); }
);
});
</script>

</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wKLifeSampleTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<div style="float:left;width:25%" id="wKTempPage">
<!-- <div id="tabs1">
            <ul>
	            <li><a href="#chanqian">临床</a></li>
				<li><a href="#keji">科技服务</a></li>
           	</ul>
				<div id="chanqian" width="100%" height:10px></div>
				<div id="keji" width="100%" height:10px></div>
		 </div> -->

</div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="qcNum" value="${requestScope.qcNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
             <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
              <input type="hidden" name="wk_method" id="wk_method" value="<%=request.getParameter("method") %>" />
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="40" id="type"  value="wk" />
                   	<input type="text" size="20" maxlength="25" id="wk_id"
                   	 name="wk.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"
	value="<s:property value="wk.id"/>"
                   	  />
                   	  <input type="hidden" size="30" maxlength="60" id="wk_maxNum" name="wk.maxNum" title="<fmt:message key="biolims.common.containerQuantity"/>"
							value="<s:property value="wk.maxNum"/>"
                   	  />
                   	  <input type="hidden" size="30" maxlength="60" id="wk_qcNum" name="wk.qcNum" title="<fmt:message key="biolims.common.qualityProductQuantity"/>"
							value="<s:property value="wk.qcNum"/>"
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_name"
                   	 name="wk.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="wk.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<%-- <g:LayOutWinTag buttonId="showreciveUser" title="选择实验员"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reciveUserFun" 
 				hasSetFun="true"
				documentId="wk_reciveUser"
				documentName="wk_reciveUser_name" />
				
			
			
               	 	<td class="label-title" >实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_reciveUser_name"  value="<s:property value="wk.reciveUser.name"/>"  readonly="readOnly"  />
 						<input type="hidden" id="wk_reciveUser" name="wk.reciveUser.id"  value="<s:property value="wk.reciveUser.id"/>" > 
 						<img alt='选择实验员' id='showreciveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.experimentalTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_reciveDate"
                   	 name="wk.reciveDate" title="<fmt:message key="biolims.common.experimentalTime"/>"
                   	    Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
                   	  value="<s:date name="wk.reciveDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			</tr>
			<tr>
			
			<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showcreateUser = Ext.get('showcreateUser');
showcreateUser.on('click', 
 function UserFun(){
var win = Ext.getCmp('UserFun');
if (win) {win.close();}
var UserFun= new Ext.Window({
id:'UserFun',modal:true,title:'<fmt:message key="biolims.common.chooseFromPeople"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/experiment/check/techCheckServiceTask/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 UserFun.close(); }  }]  });     UserFun.show(); }
);
});
 function setUserFun(rec){
document.getElementById('techCheckServiceTask_createUser').value=rec.get('id');
				document.getElementById('techCheckServiceTask_createUser_name').value=rec.get('name');
var win = Ext.getCmp('UserFun')
if(win){win.close();}
}
</script>

				
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_createUser_name"  value="<s:property value="wk.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="wk_createUser" name="wk.createUser.id"  value="<s:property value="wk.createUser.id"/>" > 
 						<%-- <img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_createDate"
                   	 name="wk.createDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:property value="wk.createDate"/>"
                   	                      	  />
                   	  
                   	</td>
                   	
                   		<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="" id="wk_confirmDate"
                   	 name="wk.confirmDate" title="<fmt:message key="biolims.common.commandTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	 value="<s:date name="wk.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>
                   	
					
			
			</tr>
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.chooseExperimentalTemplate"/></td>
			        <td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_template_name"  value="<s:property value="wk.template.name"/>" />
 						<input type="hidden" id="wk_template" name="wk.template.id"  value="<s:property value="wk.template.id"/>" > 
 						<input type="hidden" readonly="readOnly" id="wk_template_templateFieldsCode" name="wk.template.templateFieldsCode"  value="<s:property value="wk.template.templateFieldsCode"/>" >
 						<input type="hidden" readonly="readOnly" id="wk_template_templateFieldsItemCode" name="wk.template.templateFieldsItemCode"  value="<s:property value="wk.template.templateFieldsItemCode"/>" >
 						<img alt='<fmt:message key="biolims.common.chooseExperimentalTemplate"/>' id='showTemplateFun' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()" />                   		
                   	</td>
                   	
					<td class="label-title" style="display:none">Blend Code</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="25" id="wk_indexa"
                   	 name="wk.indexa" title="Blend Code"
                   	   class="text input"  value="<s:property value="wk.indexa"/>" style="display:none"
                   	  />
                   	</td>
                   	
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.stateID"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="wk_state"
                   	 name="wk.state" title="<fmt:message key="biolims.common.stateID"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wk.state"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
                   	
                   		<script language = "javascript" >
Ext.onReady(function() {
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
var showAcceptUser = Ext.get('showAcceptUser');
showAcceptUser.on('click', 
 function loadAcceptUser(){
var win = Ext.getCmp('loadAcceptUser');
if (win) {win.close();}
var loadAcceptUser= new Ext.Window({
id:'loadAcceptUser',modal:true,title:'<fmt:message key="biolims.common.selectGroup"/>',layout:'fit',width:500,height:500,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
html:"<iframe scrolling='no' name='maincontentframe' src='/core/userGroup/userGroupSelect.action?flag=loadAcceptUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: biolims.common.close,
 handler: function(){
 loadAcceptUser.close(); }  }]  });     loadAcceptUser.show(); }
);
});
 function setloadAcceptUser(rec){
document.getElementById('wk_acceptUser').value=rec.get('id'); 
					document.getElementById('wk_acceptUser_name').value=rec.get('name');
var win = Ext.getCmp('loadAcceptUser')
if(win){win.close();}
}
</script>
 

			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_acceptUser_name"  value="<s:property value="wk.acceptUser.name"/>"/>
 						<input type="hidden" id="wk_acceptUser" name="wk.acceptUser.id"  value="<s:property value="wk.acceptUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectGroup"/>' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>
			
				 
			</tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.chooseTester1" />
					</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_testUserOneName" name="wk.testUserOneName" value="<s:property value="wk.testUserOneName"/>" readonly="readOnly"  />
 						<input type="hidden" id="wk_testUserOneId" name="wk.testUserOneId"  value="<s:property value="wk.testUserOneId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(1);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
               		<%-- <td class="label-title" >第二个实验员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="wk_testUserTwoName" name="wk.testUserTwoName"    value="<s:property value="wk.testUserTwoName"/>" readonly="readOnly"  />
 						<input type="hidden" id="wk_testUserTwoId" name="wk.testUserTwoId"  value="<s:property value="wk.testUserTwoId"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(2);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td> --%>
               		<td class="label-title" ><fmt:message key="biolims.wk.confirmUserName" /></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
 						<input type="text" size="20" readonly="readOnly"  id="wk_confirmUser_name"  value="<s:property value="wk.confirmUser.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="wk_confirmUser" name="wk.confirmUser.id"  value="<s:property value="wk.confirmUser.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheExperimenter"/>' id='showtestUser' onclick="testUser(3);" src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 						
               		</td>
			</tr>
			<tr>
				<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_note"
                   	 name="wk.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="wk.note"/>"
                   	  />
                   	  
                   	</td>
			<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="wk_stateName"
                   	 name="wk.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="wk.stateName"/>"
                   	  />
                   	  
                   	</td>
                   	
				<%-- <td class="label-title" >测序类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" >
                   	  <select id="wk_seType" name="wk.seType" class="input-20-length"  onChange="seTypeChange()">
								<option value="1" <s:if test="wk.seType==1">selected="selected"</s:if>>Illumina</option>
								<option value="2" <s:if test="wk.seType==2">selected="selected"</s:if>>lon torrent</option>
					</select>
					</td> --%>
                   	
                   	
			
			</tr>
			<tr style="display: none;">
				<td class="label-title" ><fmt:message key="biolims.common.libraryType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" >
                   	  <select id="wk_type" name="wk.type" class="input-20-length"  onChange="change()">
								<option value="0" <s:if test="wk.type==0">selected="selected"</s:if>>FFPE，<fmt:message key="biolims.common.blood"/></option>
								<!-- option value="1" <s:if test="wk.type==1">selected="selected"</s:if>>ctDNA</option>
								<option value="2" <s:if test="wk.type==2">selected="selected"</s:if>>rRNA去除+RNA-seq</option>
								<option value="3" <s:if test="wk.type==3">selected="selected"</s:if>>mRNA纯化+RNA-Seq</option-->
					</select>
					</td>
			</tr>
			<tr>
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message key="biolims.common.attachment"/></span>
						</td>
						
						<td class="label-title"><fmt:message key="biolims.common.electrophoresis" /></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" >
							<span class="attach-btn" id="doclinks_img1"></span>
							<span class="text label">
								<fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum1}&nbsp;&nbsp;
								<fmt:message key="biolims.common.attachment"/>
							</span>
							<span ><input type="button" value=<fmt:message key="biolims.common.check"/> onclick="showimg('wKdyt')"/></span>
						</td>
							
			<td class="label-title"><fmt:message key="biolims.common.2100accessory" /></td><td></td>
						<td title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>" >
							<span class="attach-btn" id="doclinks_img2"></span>
							<span class="text label">
								<fmt:message key="biolims.common.aTotalOf"/>&nbsp;&nbsp;${requestScope.fileNum2}&nbsp;&nbsp;
								<fmt:message key="biolims.common.attachment"/>
							</span>
							<span ><input type="button" value=<fmt:message key="biolims.common.check"/> onclick="showimg('wK2100')"/></span>
						</td>
			</tr>
			<tr>
			<td class="label-title" ><fmt:message key="biolims.common.addTheQualityProduct"/></td>
			<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
			<td>
                   	<% 
			String maxNum = (String)request.getAttribute("qcNum");
			if(maxNum!=null && !maxNum.equals("")){
			Integer max = Integer.parseInt(maxNum);
			for(int i=0;i<max;i++){
			%>
						<a  href="#" onClick="addQc(<%=i %>)">CT<%=i %></a>
					<%}
			}%>
			</td>
			</tr>
			
			
            </table>
            <input type="hidden" name="wKItemJson" id="wKItemJson" value="" />
            <input type="hidden" name="wKTemplateJson" id="wKTemplateJson" value="" />
            <input type="hidden" name="wKReagentJson" id="wKReagentJson" value="" />
            <input type="hidden" name="wKCosJson" id="wKCosJson" value="" />
            <input type="hidden" name="wKSampleInfoJson" id="wKSampleInfoJson" value="" />
            <input type="hidden" name="wKSampleInfoCtDNAJson" id="wKSampleInfoCtDNAJson" value="" />
            <input type="hidden" name="wKSampleInforRNAJson" id="wKSampleInforRNAJson" value="" />
            <input type="hidden" name="wKSampleInfomRNAJson" id="wKSampleInfomRNAJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="wk.id"/>" />
            </form>
			<div id="wKItempage" width="100%" height:10px></div>
			<%-- <% 
			String maxNum = (String)request.getParameter("maxNum");
			System.out.print(maxNum);
			if(maxNum!=null && !maxNum.equals("")){
			Integer max = Integer.parseInt(maxNum);
			for(int i=0;i<max;i++){
			%> --%>
			<div id = '<%="3d_image0" %>'></div>
            <div id="tabs">
            <ul>
			<!-- <li><a href="#wKItempage">文库明细</a></li> -->
			<li><a href="#wKTemplatepage"><fmt:message key="biolims.common.executionStep"/></a></li>
			<li><a href="#wKReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#wKCospage" onClick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
			<!-- <li><a href="#wKResultpage">文库结果</a></li> -->
           	</ul> 
			<div id="wKTemplatepage" width="100%" height:10px></div>
			<div id="wKReagentpage" width="100%" height:10px></div>
			<div id="wKCospage" width="100%" height:10px></div>
			</div>
			<div id="wKSampleInfopage" width="100%" height:10px></div>
			<div id="wKSampleInfoCtDNApage" width="100%" height:10px></div>
			<div id="wKSampleInforRNApage" width="100%" height:10px></div>
			<div id="wKSampleInfomRNApage" width="100%" height:10px></div>
			<div id = '<%="3d_image1" %>'></div>
        	</div>
	</body>
	</html>
