﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/experiment/wkLife/wkLifeManage.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<!-- <form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >文库编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="wkManage_id"
                   	 name="id" searchField="true" title="文库编号"   style="display:none"    />
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="wkManage_name"
                   	 name="name" searchField="true" title="描述"    />
 
                   	</td>
                   	               	 	<td class="label-title" >文库编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="wkManage_code"
                   	 name="code" searchField="true" title="文库编号"    />

                   	</td>
            </tr>
			<tr>
               	 	<td class="label-title" >index</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="wkManage_indexs"
                   	 name="index" searchField="true" title="index"    />
                   
                   	</td>

               	 	<td class="label-title" >体积</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="wkManage_bulk"
                   	 name="bulk" searchField="true" title="体积"    />

                   	</td>
			</tr>
			<tr>                   	
               	 	<td class="label-title" >浓度</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="wkManage_chroma"
                   	 name="chroma" searchField="true" title="浓度"    />

                   	</td>
               	 	<td class="label-title" >结果判定</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="wkManage_resultDecide"
                   	 name="resultDecide" searchField="true" title="结果判定"    />

                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >下一步流向</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="wkManage_nextFlow"
                   	 name="nextFlow" searchField="true" title="下一步流向"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div> -->
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>	
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.sampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="code" id="wkManage_code" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			<fmt:message key="biolims.common.originalSampleCode"/>
			</label>
			</td>
			<td>
			<input type="text"  name="sampleCode" id="wkManage_sampleCode" searchField="true" >
			</td>
			<td >
			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>">
			Blend Code
			</label>
			</td>
			<td>
			<input type="text"  name="indexa" id="wkManage_a" searchField="true" >
			</td>
			
			<td>
			<input type="button" onClick="selectWkInfo()" value="<fmt:message key="biolims.common.find"/>">
			</td>
			<td></td>
			<td></td>
			<td>
			</td>
			</tr>
		</table>
		<div id="show_wkManage_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_wkManage_tree_page"></div>
		<div id="bat_result2_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="result2" style="width:100">
                		<option value="" <s:if test="result2==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="result2==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0" <s:if test="result2==0">selected="selected" </s:if>><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_next2_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td>
                <td><select id="nextFlow2"  style="width:100">
                		<option value="" <s:if test="nextFlow2==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="0" <s:if test="nextFlow2==0">selected="selected" </s:if>><fmt:message key="biolims.common.qualityTcontrol"/></option>
    					<option value="1" <s:if test="nextFlow2==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualityQcontrol"/></option>
    					<option value="2" <s:if test="nextFlow2==2">selected="selected" </s:if>><fmt:message key="biolims.common.termination"/></option>
    					<!-- <option value="3" <s:if test="nextFlow1==3">selected="selected" </s:if>>反馈至项目组</option> -->
    					<option value="3" <s:if test="nextFlow2==3">selected="selected" </s:if>><fmt:message key="biolims.common.putInStorage"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>



