<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>
	</head>

	<body>
		<!--下一步的询问狂-->
		<div id="nextAsk" class="hide">
			
		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
		 <input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" /> 
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title"><fmt:message key="biolims.common.testSteps"/>
									<small style="color:#fff;">
										<fmt:message key="biolims.common.experimentalResults"/>: <span id="ufTask_id"><s:property value="ufTask.id"/></span>
										<fmt:message key="biolims.common.commandPerson"/>: <span userId="<s:property value="ufTask.createUser.id"/>"  id="ufTask_createUser"><s:property value="ufTask.createUser.name"/></span>
										<fmt:message key="biolims.sample.createDate"/>: <span id="ufTask_createDate"><s:property  value="ufTask.createDate"/></span>
										<fmt:message key="biolims.common.state"/>: <span state="<s:property value="ufTask.state"/>"  id="ufTask_state"><s:property value="ufTask.stateName"/></span>
										<fmt:message key="biolims.common.experimenter"/>: <span id="testUserOneName"><s:property value="ufTask.testUserOneName"/></span>
									</small>
								</h3>

							</div>
							<div class="box-body">
								<!--步骤-->
								<div class="col-xs-2">
									<div id="wizard_verticle" class="form_wizard wizard_verticle" style="padding-top: 50px;">
										<ul class="list-unstyled wizard_steps">
										</ul>

									</div>
								</div>
								<!--步骤明细-->
								<div class="col-xs-10">
									<div class="box box-primary box-solid">
										<div class="box-header with-border">
											<i class="glyphicon glyphicon-leaf"></i>
											<h3 class="box-title">
												<span id="steptiele"></span>
											<small style="color:#fff;">
      												<button id="" class="btn btn-box-tool" onclick="$(this).next('i').text(parent.moment(new Date()).format('YYYY-MM-DD  HH:mm'))"><fmt:message key="biolims.common.startTime"/> </button><i id="startTime"  class="stepsTime">2018-01-23</i>
      												<button id="" class="btn btn-box-tool" onclick="$(this).next('i').text(parent.moment(new Date()).format('YYYY-MM-DD  HH:mm'))"><fmt:message key="biolims.common.endTime"/> </button><i id="endTime"  class="stepsTime">2018-01-23</i>
      												<fmt:message key="biolims.common.templateName"/>: <span><s:property value="ufTask.template.name"/></span>
											</small>
											
											</h3>
											<div class="box-tools pull-right">
												<button type="button" id="stepContentBtn" class="btn btn-box-tool" style="font-size: 20px;border: 1px solid;"><i class="fa fa-file-word-o"></i>
                </button>
												<button type="button" id="stepSaveBtn" class="btn btn-box-tool" style="border: 1px solid;" onclick="saveStepItem()"><fmt:message key="biolims.common.save"/></button>
											</div>
										</div>
										<div class="box-body">
											<div class="col-md-12" id="stepContentModer" style="position: absolute;z-index: 6;height: 0%;right: 10px;width: 0%;overflow: hidden;">
												<div class="box box-success">
													<div class="box-header">
														<i class="glyphicon glyphicon-leaf"></i>
														<h3 class="box-title"><fmt:message key="biolims.common.stepDetail"/></h3>
														<div class="box-tools pull-right">

														</div>
													</div>
													<div class="box-body" id="stepcontent" style="height:100%; overflow: hidden;">
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="box box-primary">
													<div class="box-header with-border">
														<i class="fa fa-flask"></i>
														<h3 class="box-title"><fmt:message key="biolims.common.reagentName"/></h3>
														<div class="box-tools" style="width: 70%;top: 2px;">
															<input type="text" placeholder="<fmt:message key="biolims.common.scanInsert"/>" class="form-control" id="reagentScanInput" />
														</div>
													</div>
													<div class="box-body">
														<ul class="todo-list" id="reagentBody" style="max-height: 250px;">
														</ul>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="box box-primary">
													<div class="box-header">
														<i class="fa fa-balance-scale"></i>
														<h3 class="box-title"><fmt:message key="biolims.common.instrumentName"/></h3>
														<div class="box-tools" style="width: 70%;top: 2px;">
															<input type="text" placeholder="<fmt:message key="biolims.common.scanInsert"/>" class="form-control" id="cosScanInput" />
														</div>
													</div>
													<div class="box-body">
														<ul class="todo-list" id="cosBody" style="max-height: 250px;">
														</ul>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="box box-primary">
													<div class="box-header">
														<i class="fa fa-building-o"></i>
														<h3 class="box-title"><fmt:message key="biolims.common.testRecord"/></h3>
														<div class="box-tools pull-right">

														</div>
													</div>
													<div class="box-body" id="contentData" style="max-height: 300px;">

													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="box box-primary">
													<div class="box-header">
														<i class="fa fa-calendar"></i>
														<h3 class="box-title"><fmt:message key="biolims.common.plateSample"/></h3>
														<div class="box-tools pull-right">

														</div>
													</div>
													<div class="box-body" id="plateModal">
														<div class="box box-warning collapsed-box box-solid" id="plateDiv">
															<div class="box-header">
																<i class="glyphicon glyphicon-leaf"></i>
																<h3 class="box-title" style="margin-left: 20px;">
																	<span class="box-titlem"></span>
																	<small style="color:#fff;">
																		<fmt:message key="biolims.common.sampleSize"/>：<span class="box-titlesub"></span>
																	</small>
																</h3>
																
																<div class="box-tools pull-right">
																	<div class="box-tools pull-right">
																		<button type="button" class="btn btn-box-tool addSamp"><i class="fa  fa-hand-pointer-o"></i>
                </button>
																		<button type="button" class="btn btn-box-tool showPlate" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
																	</div>
																</div>
															</div>
															<div class="box-body">
																<div>
																	<table class="table table-hover table-striped table-bordered table-condensed" id="ufTaskSample" style="font-size:14px;"></table>
																</div>
																
																<table class="table table-bordered  plate"></table>
																
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>

							</div>
							<div class="box-footer">
								<div class="pull-right">
								<button type="button" class="btn btn-primary" id="save" onclick="saveStepItem()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.save"/>
                </button>
								<button type="button" style="display: none" class="btn btn-primary" id="sp" onclick="sp()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.handle"/>
                </button>
									<button type="button" class="btn btn-primary" id="prev"><i class="glyphicon glyphicon-arrow-up"></i><fmt:message key="biolims.common.back"/>
                </button>
									<button type="button" class="btn btn-primary" id="next"><i class="glyphicon glyphicon-arrow-down"></i> <fmt:message key="biolims.common.nextStep"/>
                </button>
								</div>

							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<script src="${ctx}/lims/dist/js/app.min.js "></script>
		<script src="${ctx}/js/experiment/uf/ufTaskSteps.js" type="text/javascript" charset="utf-8"></script>
	</body>

</html>