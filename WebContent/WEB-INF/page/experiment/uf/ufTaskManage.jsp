<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<!-- 读取哪一个资源文件 -->
		<fmt:setBundle basename="ResouseInternational/msg" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>
		<style type="text/css">
			.layui-layer-content{
				padding: 0 20px;
			}
		</style>
	</head>

	<body>
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">
								<fmt:message key="biolims.common.bloodManage"/>
								</h3>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool" id="tableRefresh"
											onclick="tableRefresh()">
											<i class="glyphicon glyphicon-refresh"></i>
										</button>
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle"
												data-toggle="dropdown" aria-haspopup="true"
												aria-expanded="false">
												Action <span class="caret"></span>
											</button>
												<ul class="dropdown-menu">
														<li>
															<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
														</li>
														<li>
															<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
														</li>
														<li>
															<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
														</li>
														<li>
															<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
														</li>
														<li role="separator" class="divider"></li>
														<li>
															<a href="####" onclick="fixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
														</li>
														<li>
															<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
														</li>
													</ul>
										</div>
									</div>
						
							</div>
							<div class="box-body ipadmini">
								<table class="table table-hover table-striped table-bordered table-condensed" id="ufTaskManagediv" style="font-size: 14px;">
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<script type="text/javascript" src="${ctx}/js/experiment/uf/ufTaskManage.js"></script>
	</body>

</html>