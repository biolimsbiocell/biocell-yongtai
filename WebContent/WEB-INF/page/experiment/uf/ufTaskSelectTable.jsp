<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>
	<style>
		.dataTables_scrollBody {
			min-height: 100px;
		}
		.tablebtns{
						float:right;
		}
		.dt-buttons{
						margin:0;
						float:right;
		}
		.chosed {
						background-color: #5AC8D8 !important;
						color: #fff;
		}
	</style>
	<input type="hidden" id="code" value="${requestScope.code}">
	<button type="button" class="btn btn-info" onclick="searchOptions()">
			<i class="glyphicon glyphicon-save"></i> 搜索
		</button>
	<table class="table table-hover table-bordered table-condensed"
			id="addUfTaskTable" style="font-size: 12px;"></table>
	<script type="text/javascript" src="${ctx}/js/experiment/uf/ufTaskSelectTable.js"></script>
	</body>
</html>