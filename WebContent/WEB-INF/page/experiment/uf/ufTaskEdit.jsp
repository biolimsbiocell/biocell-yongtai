<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<i class="glyphicon glyphicon-pencil"></i>超声破碎
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print"/></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData"/></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" onclick="itemFixedCol(2)"><fmt:message key="biolims.common.lock2Col"/></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col"/></a>
											</li>
										</ul>
						</div>
					</div>
				</div>
				<div class="box-body">
					<!--进度条-->
					<div id="wizard" class="form_wizard wizard_horizontal">
						<ul class="wizard_steps">
							<li><a class="selected step"> <span class="step_no">1</span>
									<span class="step_descr"> Step 1<br /> <small>超声破碎</small>
								</span>
							</a></li>
										<li><a class="disabled step"> <span class="step_no">2</span>
											<span class="step_descr"> Step 2<br /> <small>设备明细</small></span>
								</a></li>
						</ul>
					</div>
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId")%>" /> <br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.id"/> <img class='requiredimage' src='${ctx}/images/required.gif' />  </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_id"
											name="id" title="<fmt:message key="biolims.ufTask.id"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.id"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.name"/> </span> 
									
										
                   						<input type="text"
											size="50" maxlength="50" id="ufTask_name"
											name="name" title="<fmt:message key="biolims.ufTask.name"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.name"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.reciveUser"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="ufTask_reciveUser_name" 
												value="<s:property value="ufTask.reciveUser.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="ufTask_reciveUser_id" name="reciveUser-id"
												value="<s:property value="ufTask.reciveUser.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showreciveUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.reciveDate"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_reciveDate"
											name="reciveDate" title="<fmt:message key="biolims.ufTask.reciveDate"/>" 
											  readonly = "readOnly" class="form-control readonlytrue" 
											  value="<s:date name="ufTask.reciveDate" format="yyyy-MM-dd"/>" 
											   class="Wdate" 									     	  
											 										 />  
										 
							<script>
									 
										 $("#ufTask_reciveDate").datepicker({
											language: "zh-TW",
											autoclose: true, 
											format: "yyyy-mm-dd" 
										});
							</script>			     	  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.createUser"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="ufTask_createUser_name" 
												value="<s:property value="ufTask.createUser.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="ufTask_createUser_id" name="createUser-id"
												value="<s:property value="ufTask.createUser.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showcreateUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.createDate"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_createDate"
											name="createDate" title="<fmt:message key="biolims.ufTask.createDate"/>" 
											  readonly = "readOnly" class="form-control readonlytrue" 
											  value="<s:date name="ufTask.createDate" format="yyyy-MM-dd"/>" 
											   class="Wdate" 									     	  
											 										 />  
										 
							<script>
									 
										 $("#ufTask_createDate").datepicker({
											language: "zh-TW",
											autoclose: true, 
											format: "yyyy-mm-dd" 
										});
							</script>			     	  
					
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.template"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="ufTask_template_name" 
												value="<s:property value="ufTask.template.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="ufTask_template_id" name="template-id"
												value="<s:property value="ufTask.template.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showtemplate()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.indexa"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_indexa"
											name="indexa" title="<fmt:message key="biolims.ufTask.indexa"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.indexa"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.state"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_state"
											name="state" title="<fmt:message key="biolims.ufTask.state"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.state"/>"
						 />  
					
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.stateName"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_stateName"
											name="stateName" title="<fmt:message key="biolims.ufTask.stateName"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.stateName"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.note"/> </span> 
									
										
                   						<input type="text"
											size="50" maxlength="50" id="ufTask_note"
											name="note" title="<fmt:message key="biolims.ufTask.note"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.note"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.acceptUser"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="ufTask_acceptUser_name" 
												value="<s:property value="ufTask.acceptUser.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="ufTask_acceptUser_id" name="acceptUser-id"
												value="<s:property value="ufTask.acceptUser.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showacceptUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
									</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.maxNum"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_maxNum"
											name="maxNum" title="<fmt:message key="biolims.ufTask.maxNum"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.maxNum"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.qcNum"/> </span> 
									
										
                   						<input type="text"
											size="20" maxlength="25" id="ufTask_qcNum"
											name="qcNum" title="<fmt:message key="biolims.ufTask.qcNum"/>" 
											  class="form-control"   
	value="<s:property value="ufTask.qcNum"/>"
						 />  
					
									</div>
								</div>				
								<div class="col-xs-4">
									<div class="input-group">
									
									<span class="input-group-addon"><fmt:message key="biolims.ufTask.parent"/> </span> 
									
										<input type="text" size="20" readonly="readOnly"
												id="ufTask_parent_name" 
												value="<s:property value="ufTask.parent.name"/>"
												class="form-control" /> 
												
										<input type="hidden"
												id="ufTask_parent_id" name="parent-id"
												value="<s:property value="ufTask.parent.id"/>">
										
										<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showparent()">
													<i class="glyphicon glyphicon-search"></i>
												</span>		
									</div>
								</div>				
									</div>
							<br> 
							<input type="hidden" name="ufTaskCosJson"
								id="ufTaskCosJson" value="" /> 
								
							<input type="hidden"
								id="id_parent_hidden"
								value="<s:property value="ufTask.id"/>" />
						</form>
					</div>
					<!--table表格-->
					  	<div class="HideShowPanel" style="display: none;">
						 <table
							class="table table-hover table-striped table-bordered table-condensed"
							id="ufTaskCosTable" style="font-size: 14px;">
						</table>
					</div>
					</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="button" class="btn btn-primary" id="pre">上一步</button>
						<button type="button" class="btn btn-primary" id="next">下一步</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript" src="${ctx}/js/experiment/uf/ufTaskEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/uf/ufTaskCos.js"></script>
</body>

</html>	
