﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}
</style>
<script type="text/javascript" src="${ctx}/js/experiment/uf/ufTaskItem.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
	<div id="ufTaskItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_productNum_div" style="display: none">
		<table>
			<tr>
				<td class="label-title"><span><fmt:message key="biolims.common.dicSampleNum"/></span></td>
				<td><input id="productNum"/></td>
			</tr>
		</table>
		</div>
		<div id="many_batItem_div" style="display: none">
		<div class="ui-widget;">
			<div id="bat_position_format_div" class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_batItem_text" style="width:650px;height: 339px"></textarea>
	</div>
</body>
</html>
