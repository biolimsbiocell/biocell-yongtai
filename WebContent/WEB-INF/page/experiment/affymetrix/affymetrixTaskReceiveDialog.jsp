﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/affymetrix/affymetrixTaskReceiveDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="affymetrixTaskReceive_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_Code" searchField="true" name="Code"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">接收人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_receiveUser" searchField="true" name="receiveUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本类型</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="affymetrixTaskReceive_Type" searchField="true" name="Type"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">接收日期</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_receiverDate" searchField="true" name="receiverDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本情况</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_Condition" searchField="true" name="Condition"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">结果判定</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_method" searchField="true" name="method"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">储位提示</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_location" searchField="true" name="location"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">下一步流向</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_nextFlow" searchField="true" name="nextFlow"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">处理意见</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_advice" searchField="true" name="advice"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">确认执行</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_isExecute" searchField="true" name="isExecute"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">反馈时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_feedbackTime" searchField="true" name="feedbackTime"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">Affymetrix实验编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="affymetrixTaskReceive_affymetrixTaskCode" searchField="true" name="affymetrixTaskCode"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>		
		<div id="show_dialog_affymetrixTaskReceive_div"></div>  		
</body>
</html>



