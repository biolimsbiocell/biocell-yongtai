﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=affymetrixTaskReceive&id=${affymetrixTaskReceive.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/experiment/affymetrix/affymetrixTaskReceiveEdit.js"></script>
	<div style="float: left; width: 25%"
		id="AffymetrixTaskReceviceLeftPage"></div>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="affymetrixTaskReceive_id" name="affymetrixTaskReceive.id"
						title="<fmt:message key="biolims.common.serialNumber"/>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="affymetrixTaskReceive.id"/>" /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.describe" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="affymetrixTaskReceive_name" name="affymetrixTaskReceive.name"
						title="<fmt:message key="biolims.common.describe"/>"
						value="<s:property value="affymetrixTaskReceive.name"/>" /></td>
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.receivingDate" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="affymetrixTaskReceive_receiverDate"
						name="affymetrixTaskReceive.receiverDate"
						title="<fmt:message key="biolims.common.receivingDate"/>"
						value="<s:property value="affymetrixTaskReceive.receiverDate"/>"
						readonly="readOnly" class="text input readonlytrue" /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.recipient" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="affymetrixTaskReceive_receiveUser_name"
						value="<s:property value="affymetrixTaskReceive.receiveUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="affymetrixTaskReceive_receiveUser"
						name="affymetrixTaskReceive.receiveUser.id"
						value="<s:property value="affymetrixTaskReceive.receiveUser.id"/>">
					</td>
					<td class="label-title"><fmt:message
							key="biolims.common.workflowState" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="hidden"
						id="affymetrixTaskReceive_state"
						name="affymetrixTaskReceive.state"
						title="<fmt:message key="biolims.common.workflowState"/>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="affymetrixTaskReceive.state"/>" /> <input
						type="text" size="20" maxlength="25"
						id="affymetrixTaskReceive_stateName"
						name="affymetrixTaskReceive.stateName"
						title="<fmt:message key="biolims.common.workflowState"/>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="affymetrixTaskReceive.stateName"/>" />
				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.attachment" /></td>
					<td></td>
					<td
						title="<fmt:message key="biolims.common.afterthepreservation"/>"
						id="doclinks_img"><span class="attach-btn"></span><span
						class="text label"><fmt:message
								key="biolims.common.aTotalOf" />${requestScope.fileNum}<fmt:message
								key="biolims.common.noAttachment" /></span>
				</tr>
			</table>
			<input type="hidden" name="affymetrixTaskReceiveItemJson"
				id="affymetrixTaskReceiveItemJson" value="" /> <input type="hidden"
				id="id_parent_hidden"
				value="<s:property value="affymetrixTaskReceive.id"/>" />
		</form>
		<div id="tabs">
			<div id="affymetrixTaskReceiveItempage" width="100%" height:10px></div>
		</div>
	</div>
</body>
</html>
