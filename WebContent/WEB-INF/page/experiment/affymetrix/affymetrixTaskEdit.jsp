﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=affymetrixTask&id=${affymetrixTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/affymetrix/affymetrixTaskEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<div style="float:left;width:25%" id="affymetrixTaskTempPage">	
</div>
  	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="maxNum" value="${requestScope.maxNum}">
			<input type="hidden" id="taskName" value="${requestScope.taskName}">
            <form name="form1" id="form1" method="post">
            <input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
			<table class="frame-table">
			<tr>						
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="hidden" size="40" id="type"  value="affymetrixTask" />
                   	<input type="text" size="20" maxlength="18" id="affymetrixTask_id"
                   	 name="affymetrixTask.id" title="<fmt:message key="biolims.common.serialNumber"/>"
                   	readonly = "readOnly" class="text input readonlytrue"   
	value="<s:property value="affymetrixTask.id"/>"
                   	  />                   	  
                   	</td>						
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" size="30" maxlength="60" id="affymetrixTask_maxNum" name="affymetrixTask.maxNum" title="容器数量"
							value="<s:property value="affymetrixTask.maxNum"/>"
                   	  />
                   	<input type="text" size="20" maxlength="60" id="affymetrixTask_name"
                   	 name="affymetrixTask.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="affymetrixTask.name"/>"
                   	  />                   	  
                   	</td>									
			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="CreateUserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('affymetrixTask_createUser').value=rec.get('id');
				document.getElementById('affymetrixTask_createUser_name').value=rec.get('name');" />										
               	 	<td class="label-title" ><fmt:message key="biolims.common.fromPeople"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="affymetrixTask_createUser_name"  value="<s:property value="affymetrixTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="affymetrixTask_createUser" name="affymetrixTask.createUser.id"  value="<s:property value="affymetrixTask.createUser.id"/>" > 
                   	</td>
			</tr>
			<tr>						
               	 	<td class="label-title" ><fmt:message key="biolims.common.releaseDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                   	  
                   	  	<input type="text" size="15" maxlength="" id="affymetrixTask_createDate"
                   	 name="affymetrixTask.createDate" title="<fmt:message key="biolims.common.releaseDate"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	   value="<s:property value="affymetrixTask.createDate"/>"                  	     
                   	  />
                   	</td>                   	
                   		<td class="label-title" ><fmt:message key="biolims.common.completionTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >                  	  
                   	  	<input type="text" size="20" maxlength="" id="affymetrixTask_confirmDate"
                   	 name="affymetrixTask.confirmDate" title="<fmt:message key="biolims.common.completionTime"/>"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="affymetrixTask.confirmDate" format="yyyy-MM-dd HH:mm:ss"/>"/>
                   	</td>			
				<td class="label-title" ><fmt:message key="biolims.common.selectATemplat"/></td>
			        <td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="40" readonly="readOnly"  id="affymetrixTask_template_name"  value="<s:property value="affymetrixTask.template.name"/>" />
 						<input type="hidden"  readonly="readOnly" id="affymetrixTask_template" name="affymetrixTask.template.id"  value="<s:property value="affymetrixTask.template.id"/>" > 
 						<img alt='选择模板' id='showTemplateFun' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()" />                   		
                   	</td>			
               	 	<td class="label-title" style="display:none"><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none"></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="40" maxlength="30" id="affymetrixTask_state"
                   	 name="affymetrixTask.state" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   style="display:none"
	value="<s:property value="affymetrixTask.state"/>"
                   	  />                  	  
                   	</td>			
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showAcceptUser" title="选择实验组"
 					hasHtmlFrame="true" 
					html="${ctx}/core/userGroup/userGroupSelect.action" 
					isHasSubmit="false" functionName="loadAcceptUser"  
 	 				hasSetFun="true" 
 					extRec="rec" 
 					extStr="document.getElementById('affymetrixTask_acceptUser').value=rec.get('id'); 
					document.getElementById('affymetrixTask_acceptUser_name').value=rec.get('name');" /> 			
					<td class="label-title" ><fmt:message key="biolims.common.experimentalGroup"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="affymetrixTask_acceptUser_name"  value="<s:property value="affymetrixTask.acceptUser.name"/>" class="text input" readonly="readOnly"  />
 						<input type="hidden" id="affymetrixTask_acceptUser" name="affymetrixTask.acceptUser.id"  value="<s:property value="affymetrixTask.acceptUser.id"/>" > 
 						<img alt='选择实验组' id='showAcceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
               		</td>               		
			<td class="label-title" ><fmt:message key="biolims.common.workflowState"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="30" id="affymetrixTask_stateName"
                   	 name="affymetrixTask.stateName" title="<fmt:message key="biolims.common.workflowState"/>"
                   	   class="text input readonlytrue" readonly="readOnly"  
	value="<s:property value="affymetrixTask.stateName"/>"
                   	  />
			<td class="label-title"><fmt:message key="biolims.common.attachment"/></td><td></td>
						<td title="<fmt:message key="biolims.common.afterthepreservation"/>" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label"><fmt:message key="biolims.common.aTotalOf"/>${requestScope.fileNum}<fmt:message key="biolims.common.noAttachment"/></span>
			</tr>			
            </table>
            <input type="hidden" name="affymetrixTaskItemJson" id="affymetrixTaskItemJson" value="" />
            <input type="hidden" name="affymetrixTaskResultJson" id="affymetrixTaskResultJson" value="" />
            <input type="hidden" name="affymetrixTaskTemplateItemJson" id="affymetrixTaskTemplateItemJson" value="" />
            <input type="hidden" name="affymetrixTaskTemplateReagentJson" id="affymetrixTaskTemplateReagentJson" value="" />
            <input type="hidden" name="affymetrixTaskTemplateCosJson" id="affymetrixTaskTemplateCosJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="affymetrixTask.id"/>" />
            </form>
			<div id="affymetrixTaskItempage" width="100%" height:10px></div>
			<div id = '<%="3d_image0" %>'></div>
			<div id = '<%="3d_image1" %>'></div>
			<div id="tabs">
            <ul>
			<li><a href="#affymetrixTaskTemplateItempage"><fmt:message key="biolims.common.splitTemplate"/></a></li>
			<li><a href="#affymetrixTaskTemplateReagentpage" onClick="showReagent()"><fmt:message key="biolims.common.splitReagent"/></a></li>
			<li><a href="#affymetrixTaskTemplateCospage" onClick="showCos()"><fmt:message key="biolims.common.splitCos"/></a></li>
           	</ul>
           	<div id="affymetrixTaskTemplateItempage" width="100%" height:10px></div>
           	<div id="affymetrixTaskTemplateReagentpage" width="100%" height:10px></div>
           	<div id="affymetrixTaskTemplateCospage" width="100%" height:10px></div> 
           	</div>
			<div id="affymetrixTaskResultpage" width="100%" height:10px></div>
			</div>
	</body>
	</html>
