﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/splitsample/splitSampleItemDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">拆分后编号</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_spiltCode" searchField="true" name="spiltCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_code" searchField="true" name="code"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">原始样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_sampleCode" searchField="true" name="sampleCode"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">实验室样本号</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_labCode" searchField="true" name="labCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">dna编号</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_dnaCode" searchField="true" name="dnaCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本类型</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_sampleType" searchField="true" name="sampleType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">检测项目id</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_productId" searchField="true" name="productId"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">检测项目</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_productName" searchField="true" name="productName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">结束日期</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_endDate" searchField="true" name="endDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">浓度(ng/ul)</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_contraction" searchField="true" name="contraction"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">qubit浓度(ng/ul)</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_qbContraction" searchField="true" name="qbContraction"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">od260/280</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_od280" searchField="true" name="od280"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">od260/230</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_od260" searchField="true" name="od260"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">总量</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_sampleNum" searchField="true" name="sampleNum"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">体积</td>
               	<td align="left">
                    		<input type="text" maxlength="127" id="splitSampleItem_volume" searchField="true" name="volume"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_splitSampleItem_div"></div>
   		
</body>
</html>



