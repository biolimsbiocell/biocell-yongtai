﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
.x-grid3-cell-inner,.x-grid3-hd-inner {
	overflow: hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
	padding: 3px 3px 3px 5px;
	white-space: normal !important;
}
</style>
<%-- <%@ include file="/WEB-INF/page/include/common3.jsp"%> --%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/splitsample/splitProductItem.js"></script>
  <script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<input type="hidden" id="extJsonDataString" name="extJsonDataString">
		
		<div id="splitProductItemdiv"></div>
</body>
</html>



