﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
</head>
<body>
	<div class="box-body ipadmini">
		<table
			class="table table-hover table-striped table-bordered table-condensed"
			id="plitProductDiv" style="font-size: 14px;">
		</table>
	</div>
	<input type="hidden" id="splitSampleId" value="${requestScope.id}">
	<script type="text/javascript"
		src="${ctx}/js/experiment/splitsample/splitProductItem1.js"></script>
</body>
</html>



