﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
 <script type="text/javascript" src="${ctx}/js/experiment/splitsample/splitSampleManage1.js"></script>
 <script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
 <!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>		
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
<!-- 		<table cellspacing="0" cellpadding="0" class="toolbarsection"> -->
<!-- 			<tr> -->
<!-- 			<td > -->
<%-- 			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"> --%>
<%-- 			<fmt:message key="biolims.common.sampleCode"/> --%>
<!-- 			</label> -->
<!-- 			</td>	 -->
<!-- 			<td> -->
<!-- 			<input type="text"  name="code" id="experimentDnaManage_code" searchField="true" > -->
<!-- 			</td> -->
<!-- 			<td > -->
<%-- 			<label class="text label" title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"> --%>
<%-- 			<fmt:message key="biolims.common.originalSampleCode"/> --%>
<!-- 			</label> -->
<!-- 			</td> -->
<!-- 			<td> -->
<!-- 			<input type="text"  name="sampleCode" id="experimentDnaManage_sampleCode" searchField="true" > -->
<!-- 			</td> -->
<!-- 			<td> -->
<%-- 			<input type="button" onClick="selectDnaInfo()" value="<fmt:message key="biolims.common.find"/>"> --%>
<!-- 			</td> -->
<!-- 			</tr> -->
<!-- 		</table>			 -->
<!-- 		<table class="frame-table"> -->
<!-- 			<tr> -->
<%--                	 	<td class="label-title" style="display:none"><fmt:message key="biolims.common.serialNumber"/></td> --%>
<!--                    	<td align="left" style="display:none" >                  -->
<!-- 					<input type="text" size="20" maxlength="25" id="experimentDnaManage_id" -->
<%--                    	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"   style="display:none" /> --%>
<!--                    	</td>                  	               	 	 -->
<%--                	 	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td> --%>
<!--                    	<td align="left"  >                   -->
<!-- 					<input type="text" size="20" maxlength="25" id="experimentDnaManage_code" -->
<%--                    	 name="code" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    /> --%>
<!--                    	</td> -->
<%--                	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td> --%>
<!--                    	<td align="left"  >                   -->
<!-- 					<input type="text" size="50" maxlength="50" id="experimentDnaManage_name" -->
<%--                    	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    /> --%>
<!--                    	</td> -->
<!-- 			</tr> -->
<!--             </table> -->
<!-- 		</form> -->
		<div id="splitSampleManage1div"></div>
<!--    		<form name='excelfrm' action='/common/exportExcel.action' method='POST'> -->
<!-- 		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form> -->
<!-- 		<div id="show_experimentDnaManage_tree_page"></div> -->
<!-- 		<select id="method" style="display: none"> -->
<%-- 			<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option> --%>
<%-- 			<option value="1"><fmt:message key="biolims.common.qualified"/></option> --%>
<%-- 			<option value="0"><fmt:message key="biolims.common.unqualified"/></option> --%>
<!-- 		</select> -->
<!-- 		<div id="bat_managerNext_div" style="display: none"> -->
<!-- 		<table> -->
<!-- 			<tr> -->
<%-- 				<td class="label-title" ><span><fmt:message key="biolims.common.theNextStepTo"/></span></td> --%>
<!--                 <td><select id="nextFlow"  style="width:100"> -->
<%--                 		<option value="" <s:if test="nextFlow==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option> --%>
<%--     					<option value="0" <s:if test="nextFlow==0">selected="selected" </s:if>><fmt:message key="biolims.common.libraryBuilding"/></option> --%>
<%--     					<option value="1" <s:if test="nextFlow==1">selected="selected" </s:if>><fmt:message key="biolims.common.repeatExtract"/></option> --%>
<%--     					<option value="2" <s:if test="nextFlow==2">selected="selected" </s:if>><fmt:message key="biolims.common.putInStorage"/></option> --%>
<!--     					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>>反馈至项目组</option> -->
<%--     					<option value="3" <s:if test="nextFlow==3">selected="selected" </s:if>><fmt:message key="biolims.common.termination"/></option> --%>
<!-- 					</select> -->
<!--                  </td> -->
<!-- 			</tr> -->
<!-- 		</table> -->
<!-- 	</div> -->
<!-- 	<div id="bat_managerResult_div" style="display: none"> -->
<!-- 		<table> -->
<!-- 			<tr> -->
<%-- 				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td> --%>
<!--                 <td><select id="result" style="width:100"> -->
<%--                 		<option value="" <s:if test="result==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option> --%>
<%--     					<option value="1" <s:if test="result==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"/></option> --%>
<%--     					<option value="0" <s:if test="result==0">selected="selected" </s:if>><fmt:message key="biolims.common.unqualified"/></option> --%>
<!-- 					</select> -->
<!--                  </td> -->
<!-- 			</tr> -->
<!-- 		</table> -->
<!-- 	</div> -->
</body>
</html>



