<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=splitSampleItem&id=${splitSampleItem.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/splitsample/splitSampleItemEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
 <div style="float:left;width:33%" id="splitItempage">

</div>
 
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >编码</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_id"
                   	 name="splitSampleItem.id" title="编码"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="splitSampleItem.id"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >拆分后编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_spiltCode"
                   	 name="splitSampleItem.spiltCode" title="拆分后编号"
                   	   
	value="<s:property value="splitSampleItem.spiltCode"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_code"
                   	 name="splitSampleItem.code" title="样本编号"
                   	   
	value="<s:property value="splitSampleItem.code"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >原始样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_sampleCode"
                   	 name="splitSampleItem.sampleCode" title="原始样本编号"
                   	   
	value="<s:property value="splitSampleItem.sampleCode"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >实验室样本号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_labCode"
                   	 name="splitSampleItem.labCode" title="实验室样本号"
                   	   
	value="<s:property value="splitSampleItem.labCode"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >dna编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_dnaCode"
                   	 name="splitSampleItem.dnaCode" title="dna编号"
                   	   
	value="<s:property value="splitSampleItem.dnaCode"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_sampleType"
                   	 name="splitSampleItem.sampleType" title="样本类型"
                   	   
	value="<s:property value="splitSampleItem.sampleType"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >检测项目id</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_productId"
                   	 name="splitSampleItem.productId" title="检测项目id"
                   	   
	value="<s:property value="splitSampleItem.productId"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_productName"
                   	 name="splitSampleItem.productName" title="检测项目"
                   	   
	value="<s:property value="splitSampleItem.productName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >结束日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_endDate"
                   	 name="splitSampleItem.endDate" title="结束日期"
                   	   
	value="<s:property value="splitSampleItem.endDate"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >浓度(ng/ul)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_contraction"
                   	 name="splitSampleItem.contraction" title="浓度(ng/ul)"
                   	   
	value="<s:property value="splitSampleItem.contraction"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >qubit浓度(ng/ul)</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_qbContraction"
                   	 name="splitSampleItem.qbContraction" title="qubit浓度(ng/ul)"
                   	   
	value="<s:property value="splitSampleItem.qbContraction"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >od260/280</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_od280"
                   	 name="splitSampleItem.od280" title="od260/280"
                   	   
	value="<s:property value="splitSampleItem.od280"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >od260/230</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_od260"
                   	 name="splitSampleItem.od260" title="od260/230"
                   	   
	value="<s:property value="splitSampleItem.od260"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >总量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_sampleNum"
                   	 name="splitSampleItem.sampleNum" title="总量"
                   	   
	value="<s:property value="splitSampleItem.sampleNum"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >体积</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="127" id="splitSampleItem_volume"
                   	 name="splitSampleItem.volume" title="体积"
                   	   
	value="<s:property value="splitSampleItem.volume"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="splitSampleItem.id"/>" />
            </form>
            <div id="tabs">
            <ul>
           	</ul> 
			</div>
        	</div>
	</body>
	</html>
