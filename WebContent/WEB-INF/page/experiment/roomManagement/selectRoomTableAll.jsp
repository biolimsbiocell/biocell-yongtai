<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}
.tablebtns{
				float:right;
			}
.dt-buttons{
				margin:0;
				float:right;
			}
.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
</style>
</head>
<body>
<input type="hidden" id="quId" value="${requestScope.id}">

<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="input-group">
			<span class="input-group-addon">编号</span> 
			<input type="text" size="20" maxlength="25" id="id"  searchname="id" class="form-control" placeholder="编号查询" >
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="input-group">
			<span class="input-group-addon">名称</span> 
			<input type="text" size="20" maxlength="25" id="roomName" name="roomName" searchname="roomName" class="form-control" placeholder="房间名查询" >
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="input-group">
			<span class="input-group-addon">区域</span> 
			<input type="text" size="20" maxlength="25" id="regional" searchname="regionalManagement-name" class="form-control" placeholder="区域查询" >
		</div>
	</div>
	
	<div class="col-md-3 col-sm-3 col-xs-12">
		<div class="input-group">
			<input style="text-align: center;" type="button" class="form-control btn-success" onclick="query()" value="查找">
		</div>
	</div>

	<div id="selectRoomTable" >
		<table class="table table-hover table-bordered table-condensed"
			id="addRoomTable" style="font-size: 12px;"></table>
	</div>
<script type="text/javascript" src="${ctx}/js/experiment/roomManagement/selectRoomTableAll.js"></script>
</body>
</html>