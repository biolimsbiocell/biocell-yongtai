﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}
#btn_submit{
	display: none;
}
</style>
</head>

<body style="height: 94%">
<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>

	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
<!-- 		<input type="hidden" id="sampleOutApply_name" -->
<%-- 			value="<s:property value="sampleOutApply.name"/>">  --%>
			<input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							回输计划 <small style="color: #fff;">回输计划编号:<span id="reinfusionPlan_id"><s:property value="reinfusionPlan.id" /></span>
								创建人:<span userId="<s:property value="reinfusionPlan.createUser.id"/>"
								id="reinfusionPlan_createUser"><s:property value="reinfusionPlan.createUser.name" /></span>
								创建日期:<span id="reinfusionPlan_createDate"><s:date name="reinfusionPlan.createDate" format="yyyy-MM-dd "/></span> 
								状态:<span state="<s:property value="reinfusionPlan.state"/>"
								id="reinfusionPlan_state"><s:property value="reinfusionPlan.stateName" /></span>
							</small>
						</h3>
					</div>
				<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">编号</span> 
											<input type="text" size="20" maxlength="25"
												id="reinfusionPlan.id" name="id"
												changelog="<s:property value="reinfusionPlan.id"/>"
												class="form-control" readonly="readonly"
												value="<s:property value="reinfusionPlan.id"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">描述
											</span> <input type="text" size="20" maxlength="25"
												id="reinfusionPlan_name" name="name"
												changelog="<s:property value="reinfusionPlan.name"/>"
												class="form-control"
												value="<s:property value="reinfusionPlan.name"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon" style="font-family: 黑体;">
												回输计划日期
											</span> <input class="form-control "
												changelog="<s:property value="reinfusionPlan.reinfusionPlanDate"/>"
												type="text" size="20" maxlength="25"
												id="reinfusionPlan_reinfusionPlanDate" 
												name="reinfusionPlan.reinfusionPlanDate" title="回输计划日期" class="Wdate"
												value="<s:date name=" reinfusionPlan.reinfusionPlanDate " format="yyyy-MM-dd "/>" />
										</div>
									</div>
								</div>
							</form>
						</div>

	</br>






						<!--计划待回输样本-->
						<div id="leftDiv" class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">计划待回输样本</h3>
								</div>
							<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="reinfusionPlanTempdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--计划回输样本-->
						<div id="rightDiv" class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title">计划回输样本</h3>
								</div>
							<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="reinfusionPlanItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		</section>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experiment/reinfusionPlan/reinfusionPlanTempTable.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/reinfusionPlan/reinfusionPlanItemTable.js"></script>
</body>

</html>