<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title><fmt:message key="biolims.common.noTitleDocuments" /></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/lims/plugins/layui/css/layui.css">
<script type="text/javascript" src="${ctx}/lims/plugins/layui/layui.js"></script>
<style>
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}

#yesSpan {
	display: none;
}

#auditDiv {
	display: none;
}

#btn_submit {
	display: none;
}

#btn_changeState {
	display: none;
}

.sampleNumberInput {
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 200px;
    top: 212px;
}

.sampleNumberInputTow{
	width: 250px;
    height: 34px;
    border-radius: 6px;
    border: 1px solid #ccc;
    /* box-shadow: 1px 1px 1px #888888; */
    position: absolute;
    left: 510px;
    top: 212px;
}

.sampleNumberDiv {
    position: absolute;
    left: 140px;
    top: 223px;
}
.sampleNumberDivtow {
    position: absolute;
	left: 466px;
    top: 223px;
}

.sampleNumberbutton {
    position: absolute;
    left: 760px;
    top: 212px;
    border-radius: 4px;
    border: 1px solid #ccc;
    background-color: #5cb85c;
    width: 38px;
    height: 36px;
}

</style>
</head>
<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">

	<div class="container-fluid" style="margin-top: 46px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						公告栏管理
					</h3>
					<small style="color: #fff;"> 创建人: <span
						id="noticeInformation_createUser"><s:property
								value=" noticeInformation.createUser.name" /></span> 创建时间: <span
						id="noticeInformation_createDate"><s:date
								name="noticeInformation.createDate" format="yyyy-MM-dd" /></span> 状态: <span id="noticeInformation_state"><s:property
								value="noticeInformation.stateName" /></span> 
					</small>
					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
								<input type="hidden" id="createUser_id" name="noticeInformation.createUser.id"
								value="<s:property value=" noticeInformation.createUser.id "/>" />
								<input type="hidden" id="createDate" name="noticeInformation.createDate"
								value="<s:property value=" noticeInformation.createDate "/>" />
								<input type="hidden" id="state" name="noticeInformation.state"
								value="<s:property value=" noticeInformation.state "/>" />
								<input type="hidden" id="stateName" name="noticeInformation.stateName"
								value="<s:property value=" noticeInformation.stateName "/>" />
							<input type="hidden" id="bpmTaskId"
								value="${requestScope.bpmTaskId}" /> <br>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 公告编号
										</span> <input list="sampleIdOne"
											changelog="<s:property value="noticeInformation.id"/>" type="text"
											size="20" maxlength="25" id="noticeInformation_id"
											name="noticeInformation.id" class="form-control"
											title="房间编号" readonly="readonly"
											value="<s:property value=" noticeInformation.id "/>" /> 
											<input type="hidden"
											size="20" maxlength="25" id="noticeInformation_id_id"
											value="<s:property value="noticeInformation.id"/>" /> <span
											class="input-group-btn">
										</span> 
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 公告简述 
										</span> <input list="sampleName"
											changelog="<s:property value="noticeInformation.name"/>"
											type="text" size="20" maxlength="50" id="noticeInformation_name"
											name="noticeInformation.name" class="form-control"
											title="备注"
											value="<s:property value=" noticeInformation.name "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 公告开始时间<img class="requiredimage" src="/images/required.gif"> 
										</span> <input type="text"
											changelog="<s:property value="noticeInformation.startDate"/>"
											size="20" maxlength="25" id="noticeInformation_startDate"
											name="noticeInformation.startDate" class="form-control"
											title="公告开始时间"
											value="<s:date name=" noticeInformation.startDate " format="yyyy-MM-dd" />"  />

									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"> 公告结束时间<img class="requiredimage" src="/images/required.gif"> 
										</span> <input type="text"
											changelog="<s:property value="noticeInformation.endDate"/>"
											size="20" maxlength="25" id="noticeInformation_endDate"
											name="noticeInformation.endDate" class="form-control"
											title="公告结束时间"
											value="<s:date name=" noticeInformation.endDate " format="yyyy-MM-dd" />"  />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="input-group">
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileUp()">
											<fmt:message key="biolims.common.uploadAttachment" />
										</button>
										<span class="text alabel"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" />
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-md-8 col-sm-6 col-xs-12">
									<div class="input-group">
									<span class="input-group-addon">公告详细</span> 
                   						<textarea   id="noticeInformation_describe" name="noticeInformation.describe" placeholder="长度限制2000个字符"    class="form-control"   
											  style="overflow: hidden; width: 420px; height: 80px;"  ><s:property value="noticeInformation.describe"/></textarea>  
									</div>
								</div>
							</div>

							<input type="hidden" id="id_parent_hidden"
								value="<s:property value=" noticeInformation.id "/>" /> 
							<input type="hidden"  id="changeLog" name="changeLog"
								 /> 

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/experiment/noticeInformation/noticeInformationEdit.js"></script>
</body>
</html>