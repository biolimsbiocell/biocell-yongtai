<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<link rel="stylesheet" href="${ctx}/css/experimentLab.css"/>
	</head>

	<body>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;">
		  <input type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" /> 
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12">
						<div class="box box-info box-solid">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title">
							<fmt:message key="biolims.common.experimentalResults"/> <small style="color: #fff;"> <fmt:message key="biolims.common.taskId"/>: <span
								id="mgmtTask_id"><s:property value="mgmtTask.id" /></span>
								<fmt:message key="biolims.common.commandPerson"/>: <span
								userId="<s:property value="mgmtTask.createUser.id"/>"
								id="mgmtTask_createUser"><s:property
										value="mgmtTask.createUser.name" /></span> <fmt:message key="biolims.sample.createDate"/>: <span
								id="mgmtTask_createDate"><s:property
										value="mgmtTask.createDate" /></span> <fmt:message key="biolims.common.state"/>: <span
								state="<s:property value="mgmtTask.state"/>"
								id="mgmtTask_state"><s:property
										value="mgmtTask.stateName" /></span>
							</small>
						</h3>
							</div>
							<div class="box-body ipadmini">
								<table class="table table-hover table-striped table-bordered table-condensed" id="mgmtTaskResultdiv" style="font-size: 14px;">
								</table>
							</div>
							<div class="box-footer">
								<!--<div class="pull-left">
									<button type="button" class="btn btn-primary" id="makeUpSave"><i class="glyphicon glyphicon-random"></i> 提交
                </button>
								</div>-->
								<div class="pull-right">
								<button type="button" class="btn btn-primary" style="display: none" id="sp" onclick="sp()" ><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.common.handle"/>
                </button>
									<button type="button" class="btn btn-primary" id="prev"><i class="glyphicon glyphicon-arrow-up"></i> <fmt:message key="biolims.common.back"/>
                </button>
									<button type="button" class="btn btn-primary" style="display: none" id="finish"><i class="glyphicon glyphicon-saved"></i> <fmt:message key="biolims.workflow.completeName"/>
                </button>
								</div>

							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<script type="text/javascript" src="${ctx}/js/experiment/mgmt/mgmtTaskResult.js"></script>
	</body>

</html>