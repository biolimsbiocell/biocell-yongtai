<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/experiment/temperature/temperatureControl.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJonDataString">	
				<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="50" id="sampleAbnormal_sampleCode"
                   	 name="sampleCode" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.sampleType"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="30" id="sampleAbnormal_sampleType"
                   	 name="sampleType" searchField="true" title="<fmt:message key="biolims.common.sampleType"/>"    />
                   	</td>
               	 	
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.patientName"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="60" id="sampleAbnormal_patientName"
                   	 name="patientName" searchField="true" title="<fmt:message key="biolims.common.patientName"/>"    />
                   	</td>
               	 	
               	 	<td class="label-title" ><fmt:message key="biolims.common.papersCode"/></td>
                   	<td align="left"  >
					<input type="text" size="40" maxlength="50" id="sampleAbnormal_idCard"
                   	 name="idCard" searchField="true" title="<fmt:message key="biolims.common.papersCode"/>"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleAbnormal_tree_page" ></div>
	<div id="show_sampleAbnormal_div"></div>
	<div id="bat_ok_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.affirmToPerform"/></span></td>
                <td><select id="isExecute"  style="width:100">
               			<option value="" <s:if test="isExecute==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="isExecute==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="isExecute==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_methods_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.batchResults"/></span></td>
                <td><select id="methods"  style="width:100">
               			<option value="" <s:if test="methods==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="methods==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"/></option>
    					<option value="0" <s:if test="methods==0">selected="selected" </s:if>><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>



