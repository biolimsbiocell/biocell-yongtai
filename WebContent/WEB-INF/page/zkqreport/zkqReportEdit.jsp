<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=zkqReport&id=${zkqReport.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/zkqreport/zkqReportEdit.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="127" id="zkqReport_id"
                   	 name="zkqReport.id" title="编号"
                   	   
	value="<s:property value="zkqReport.id"/>"
 class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="127" id="zkqReport_name"
                   	 name="zkqReport.name" title="描述"
                   	   
	value="<s:property value="zkqReport.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
 					documentId="zkqReport_createUser"
				documentName="zkqReport_createUser_name"  />
				
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="zkqReport_createUser_name"  value="<s:property value="zkqReport.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="zkqReport_createUser" name="zkqReport.createUser.id"  value="<s:property value="zkqReport.createUser.id"/>" > 
<%--  						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			</tr>
			<tr>
			
               	 	<td class="label-title" >创建时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="127" id="zkqReport_createDate"
                   	 name="zkqReport.createDate" title="创建时间"
                   	     readonly = "readOnly" class="text input readonlytrue" 
                   	  value="<s:date name="zkqReport.createDate" format="yyyy-MM-dd HH:mm:ss"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
				<g:LayOutWinTag buttonId="showconfirmUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
 					documentId="zkqReport_confirmUser"
				documentName="zkqReport_confirmUser_name"  />
			
			
               	 	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="zkqReport_confirmUser_name"  value="<s:property value="zkqReport.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="zkqReport_confirmUser" name="zkqReport.confirmUser.id"  value="<s:property value="zkqReport.confirmUser.id"/>" > 
 						<img alt='选择审核人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
                   	
                   	
               	 	<td class="label-title" >状态名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="127" id="zkqReport_stateName"
                   	 name="zkqReport.stateName" title="状态名称"
                   	   
	value="<s:property value="zkqReport.stateName"/>"
	class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
			
               	 	<td class="label-title"  style="display:none"  >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="127" id="zkqReport_note"
                   	 name="zkqReport.note" title="备注"
                   	   
	value="<s:property value="zkqReport.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
				<td class="label-title" >选择FC号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="zkqReport_flowCode"
                   	 name="zkqReport.flowCode" title="flow cell号" 
                   	 value="<s:property value="zkqReport.flowCode"/>"
                   	  />
                   	<img alt='选择FC号' id='flowCellNo' src='${ctx}/images/img_lookup.gif' 	class='detail'   onClick="SequencingFun()" />                   		
                   	</td> 
			
			<g:LayOutWinTag buttonId="showreceiveUser" title="选择接收人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
 					documentId="zkqReport_receiveUser"
				documentName="zkqReport_receiveUser_name"  />
			
			
               	 	<td class="label-title" >接收人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="zkqReport_receiveUser_name"  value="<s:property value="zkqReport.receiveUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="zkqReport_receiveUser" name="zkqReport.receiveUser.id"  value="<s:property value="zkqReport.receiveUser.id"/>" > 
 						<img alt='选择接收人' id='showreceiveUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   	
                   	
                   	
                   	</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
							
							
							
               	 	<td class="label-title" style="display:none" >状态</td>
               	 	<td style="display:none" class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td style="display:none" align="left"  >
                   	<input style="display:none" type="text" size="20" maxlength="127" id="zkqReport_state"
                   	 name="zkqReport.state" title="状态"
                   	   
	value="<s:property value="zkqReport.state"/>"
                   	  class="text input readonlytrue" readonly="readOnly" 
                   	  />
                   	  
                   	  
                   	</td>
			</tr>
			
			
            </table>
            <input type="hidden" name="zkqReportItemJson" id="zkqReportItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="zkqReport.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#zkqReportItempage">报告明细</a></li>
           	</ul> 
			<div id="zkqReportItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
