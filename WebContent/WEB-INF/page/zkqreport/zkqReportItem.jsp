﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: normal!important;
}
</style>
<script type="text/javascript" src="${ctx}/js/zkqreport/zkqReportItem.js"></script>
</head>
<body>
	<div id="zkqReportItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv">上传CSV文件
	</div>
	
	<div id="bat_tp_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>是否已释放数据</span></td>
                <td><select id="tp" style="width:100">
                		<option value="" <s:if test="tp==">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="tp==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="tp==0">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_genotype_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span>是否已发送报告</span></td>
                <td><select id="genotype" style="width:100">
                		<option value="" <s:if test="genotype==">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="genotype==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="genotype==0">selected="selected" </s:if>>否</option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	
</body>
</html>
