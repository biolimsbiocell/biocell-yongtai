<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="/operfile/initFileList.action?modelType=purchaseContract&id=${purchaseContract.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/contract/purchaseContractEdit.js"></script>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title">合同编码</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25" readonly="readonly"
						id="purchaseContract_id" name="purchaseContract.id" title="合同编码" class="input text readonlytrue"
						value="<s:property value="purchaseContract.id"/>" />
					</td>
					<td class="label-title">描述</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="50" maxlength="50"
						id="purchaseContract_name" name="purchaseContract.name" title="描述"
						value="<s:property value="purchaseContract.name"/>" />
					</td>
					<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true"
						html="${ctx}/purchase/contract/userSelect.action"
						isHasSubmit="false" functionName="UserFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('purchaseContract_createUser').value=rec.get('id');
				document.getElementById('purchaseContract_createUser_name').value=rec.get('name');" />
					<td class="label-title">创建人</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="purchaseContract_createUser_name"
						value="<s:property value="purchaseContract.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="purchaseContract_createUser"
						name="purchaseContract.createUser.id"
						value="<s:property value="purchaseContract.createUser.id"/>">
					</td>
				</tr>
				<tr>
					<td class="label-title">创建时间</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="purchaseContract_createDate" class="input text readonlytrue"
						name="purchaseContract.createDate" title="创建时间" readonly="readonly"
						value="<s:date name="purchaseContract.createDate" format="yyyy-MM-dd"/>" />
					</td>
					<g:LayOutWinTag buttonId="showsupplierId" title="选择供应商"
						hasHtmlFrame="true"
						html="${ctx}/purchase/contract/supplierSelect.action"
						isHasSubmit="false" functionName="SupplierFun" hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('purchaseContract_supplierId').value=rec.get('id');
				document.getElementById('purchaseContract_supplierId_name').value=rec.get('name');" />
					<td class="label-title">供应商</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="purchaseContract_supplierId_name"
						value="<s:property value="purchaseContract.supplierId.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="purchaseContract_supplierId"
						name="purchaseContract.supplierId.id"
						value="<s:property value="purchaseContract.supplierId.id"/>">
						</td>
							<td class="label-title">状态名称</td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>
					<td align="left"><input type="hidden" size="20" maxlength="25"
						id="purchaseContract_state" name="purchaseContract.state"
						title="状态" value="<s:property value="purchaseContract.state"/>" /><input type="text" size="20" maxlength="25"
						id="purchaseContract_stateName" name="purchaseContract.stateName"
						title="状态名称" readonly="readonly" class="input text readonlytrue"
						value="<s:property value="purchaseContract.stateName"/>" />

					</td>
				</tr>
				<tr>
					<td class="label-title" style="display: none">备注</td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/notrequired.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="50" maxlength="50" id="purchaseContract_note"
						name="purchaseContract.note" title="备注"
						value="<s:property value="purchaseContract.note"/>"
						style="display: none" /></td>
				</tr>
				<tr>
					<td class="label-title">附件</td>
					<td></td>
					<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span
						class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
				</tr>


			</table>
			<input type="hidden" name="purchaseContractItemJson"
				id="purchaseContractItemJson" value="" /> <input type="hidden"
				id="id_parent_hidden"
				value="<s:property value="purchaseContract.id"/>" />
		</form>
		<div id="tabs">
			<ul>
				<li><a href="#purchaseContractItempage">合同明细</a></li>
			</ul>
			<div id="purchaseContractItempage" width="100%" height:10px></div>
		</div>
	</div>
</body>
</html>
