﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/purchase/contract/purchaseContractDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">合同编码</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchaseContract_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="purchaseContract_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchaseContract_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchaseContract_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">供应商</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchaseContract_supplierId" searchField="true" name="supplierId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchaseContract_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态名称</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchaseContract_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="purchaseContract_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_purchaseContract_div"></div>
   		
</body>
</html>



