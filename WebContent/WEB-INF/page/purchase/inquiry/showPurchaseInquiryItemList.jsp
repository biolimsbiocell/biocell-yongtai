<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="${ctx}/javascript/purchase/inquiry/showPurchaseInquiryItemList.js"></script>
</head>
	<g:LayOutWinTag title="选择库存物资" width="780" height="500" hasHtmlFrame="true" html="${ctx}/storage/common/showMainStorageAllSelectList.action?type=1" isHasSubmit="false" isHasButton="false" functionName="showMainStorageAllSelectList"/>
<body>
	<div id="purchase_inquiry_item_grid_div"></div>
</body>
</html>