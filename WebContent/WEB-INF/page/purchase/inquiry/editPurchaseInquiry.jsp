<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
	<script type="text/javascript" src="${ctx}/javascript/purchase/inquiry/editPurchaseInquiry.js"></script>
	<c:if test="${empty purchaseInquiry.type.id}">
		<g:LayOutWinTag buttonId="searchType" title="选择类型" hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action" isHasSubmit="false" functionName="xjlx" hasSetFun="true" documentId="purchaseInquiry_type_id" documentName="purchaseInquiry_type_name"/>	
	</c:if>
</head>
<body>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="main-content" >
		<div>
			<s:form theme="simple" method="post" id="purchase_inquiry">
				<input type="hidden" id="task_id" name="task_id" value="${requestScope.taskId}"/>
				<input type="hidden" id="handlemethod" name="handlemethod" value="${requestScope.handlemethod}">
				<input type="hidden" id="supplierDataJson" name="supplierDataJson"/>
				<input type="hidden" id="itemDataJson" name="itemDataJson"/>
				<input type="hidden" id="equipmentItemDataJson" name="equipmentItemDataJson"/>
				<input type="hidden" id="serviceItemDataJson" name="serviceItemDataJson"/>
				<table class="frame-table">
					<tr>
						<td class="label-title">编号</td>
						<td>
							<s:textfield maxlength="32" id="purchaseInquiry_id" name="purchaseInquiry.id" cssClass="input-20-length"></s:textfield>
							<img class='requiredimage' src='${ctx}/images/required.gif' />
						</td>
					
						<td class="label-title">分类</td>
						<td>
							<s:textfield id="purchaseInquiry_type_name" name="purchaseInquiry.type.name" cssClass="input-20-length" readonly="readonly"></s:textfield>
							<img class='requiredimage' src='${ctx}/images/required.gif' />
							<s:hidden id="purchaseInquiry_type_id" name="purchaseInquiry.type.id"></s:hidden>
							<s:hidden id="purchaseInquiry_type_sysCode" name="purchaseInquiry.type.sysCode"></s:hidden>
							<img alt='分类' id='searchType' src='${ctx}/images/img_lookup.gif' name='searchType' class='detail' />
						</td>
						<td class="label-title">申请人</td>
						<td>
							<s:textfield id="purchaseInquiry_applyUser_name" name="purchaseInquiry.applyUser.name" cssClass="readonlytrue" readonly="readonly"></s:textfield>
							<s:hidden id="purchaseInquiry_applyUser_id" name="purchaseInquiry.applyUser.id"></s:hidden>
						</td>
					</tr>
					<tr>
						<td class="label-title">采购申请单</td>
						<td>
							<s:textfield id="purchaseInquiry_purchaseApply_id" name="purchaseInquiry.purchaseApply.id" cssClass="input-20-length" readonly="readonly"></s:textfield>
							<img id='searchOrder' name='searchOrder' class='detail' alt='领用单' src='${ctx}/images/img_menu.gif'/>
							<s:textfield id="purchaseInquiry_purchaseApply_note" name="purchaseInquiry.purchaseApply.note" cssClass="input-40-length" readonly="readonly"></s:textfield>
						</td>
						<td class="label-title">描述</td>
						<td>
							<s:textfield maxlength="110" id="purchaseInquiry_remark" name="purchaseInquiry.remark" cssClass="input-40-length" ></s:textfield>
						</td>
						
						<td class="label-title">申请日期</td>
						<td>
							<input type="text" class="readonlytrue" readonly="readonly" name="purchaseInquiry.applyDate" value='<s:date name="purchaseInquiry.applyDate" format="yyyy-MM-dd"/>'>
						</td>
					</tr>
					<tr>
						<td class="label-title">状态</td>
						<td>
							<s:textfield id="purchaseInquiry_stateName" name="purchaseInquiry.stateName" cssClass="input-20-length readonlytrue" readonly="readonly"></s:textfield>
						</td>
					</tr>
				</table>
			</s:form>
		</div>
		<div id="show_purchase_inquiry_supplier_div"></div>
		<div id="show_purchase_inquiry_item_div" style="display:none;"></div>
		<div id="show_spurchase_inquiry_equipment_item_div" style="display:none;"></div>
		<div id="show_purchase_inquiry_service_div" style="display:none;"></div>
	</div>
</body>
</html>