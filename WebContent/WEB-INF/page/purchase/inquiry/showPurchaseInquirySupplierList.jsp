<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="${ctx}/javascript/purchase/inquiry/showPurchaseInquirySupplierList.js"></script>
	<g:LayOutWinTag isHasButton="false" title="选择供应商" hasHtmlFrame="true" html="${ctx}/supplier/common/supplierSelect.action" isHasSubmit="false" 
	functionName="searchSupplierFun" hasSetFun="true" extRec="rec" extStr="insertPurchaseInquirySupplier(rec.get('id'),rec.get('name'));"/>
</head>
<body>
	<div id="purchase_inquiry_supplier_gird_div"></div>
</body>
</html>