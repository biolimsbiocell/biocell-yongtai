<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Font Awesome -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
	</head>

	<body style="height:94%">
		<div>
			<%@ include file="/WEB-INF/page/include/newToolbar.jsp"%>
		</div>
		<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
			<section class="content">
				<div class="row">
					<!--表格-->
					<div class="col-xs-12 col-md-12">
						<div class="box box-info box-solid" id="box">
							<div class="box-header with-border">
								<i class="fa fa-bell-o"></i>
								<h3 class="box-title"><fmt:message key="biolims.common.purchasingApplication" /><small><fmt:message key="biolims.common.purchasingApplication" /></small></h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefresh()"><i class="glyphicon glyphicon-refresh"></i>
                </button>
									<div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action <span class="caret"></span>
  </button>
										<ul class="dropdown-menu">
											<li>
												<a href="####" onclick="$('.buttons-print').click();"><fmt:message key="biolims.common.print" /></a>
											</li>
											<li>
												<a href="#####" onclick="$('.buttons-copy').click();"><fmt:message key="biolims.common.copyData" /></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-excel').click();"><fmt:message key="biolims.common.export" /></a>
											</li>
											<li>
												<a href="####" onclick="$('.buttons-csv').click();"><fmt:message key="biolims.common.exportCSV" /></a>
											</li>
											<li role="separator" class="divider"></li>
											<li>
												<a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message key="biolims.common.lock2Col" /></a>
											</li>
											<li>
												<a href="####" id="unfixde" onclick="unfixde()"><fmt:message key="biolims.common.cancellock2Col" /></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="box-body ipadmini">
								<table class="table table-hover table-responsive table-striped table-bordered table-condensed" id="main" style="font-size:14px;">
								</table>
							</div>
						</div>
					</div>

				</div>

			</section>
		</div>
		<div id="purchase_apply_div"></div>
		<script type="text/javascript" src="${ctx}/javascript/purchase/apply/showPurchaseApplyList.js"></script>
	</body>

</html>

<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message
		key="biolims.common.purchasingApplicationList" /></title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/purchase/apply/showPurchaseApplyList.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>
<g:LayOutWinTag buttonId="searchdepartment"
	title='<fmt:message key="biolims.common.chooseDepartment"/>'
	hasHtmlFrame="true"
	html="${ctx}/core/department/departmentSelect.action"
	isHasSubmit="false" functionName="department" hasSetFun="true"
	documentId="department_id" documentName="department_name" />
<g:LayOutWinTag buttonId="searchSqr"
	title='<fmt:message key="biolims.common.applicant"/>'
	hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="pzr" hasSetFun="true"
	documentId="createUser_id" documentName="createUser_name" />
<g:LayOutWinTag buttonId="searchType"
	title='<fmt:message key="biolims.common.applicant"/>'
	hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
	isHasSubmit="false" functionName="cglx" hasSetFun="true"
	documentId="type_id" documentName="type_name" />
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td><span>&nbsp;</span><label class="text label"
						title="<fmt:message key="biolims.common.inputTheQueryConditions"/>"><fmt:message
								key="biolims.common.serialNumber" /></label></td>
					<td><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="id" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" searchField="true" id="sid"
						title="<fmt:message key="biolims.common.inputID" />"></td>

					<td><span>&nbsp;</span> <label class="text label"
						title="<fmt:message key="biolims.common.inputDescribe"/>"><fmt:message
								key="biolims.common.inputDescribe" /></label></td>
					<td><input type="text"
						class="input_parts text input default  readonlyfalse false"
						name="note" onblur="textbox_ondeactivate(event);"
						onfocus="shared_onfocus(event,this)" searchField="true" id="note"
						title="<fmt:message key="biolims.common.inputID" />"></td>
				</tr>
				<tr>

					<td class="label-title"><label class="text label"
						title="<fmt:message key="biolims.common.inputlassify" />"><fmt:message key="biolims.common.classification" /></label></td>
					<td nowrap class="quicksearch_control" valign="middle"><input
						type="hidden" name="type.sysCode" id="type_sysCode" /> <input
						type="text" name=".type.name" id="type_name" title=<fmt:message key="biolims.common.classification" /> size="20"
						maxlength="20" /> <input type="hidden" searchField="true"
						name="type.id" id="type_id" /> <img alt=<fmt:message key="biolims.common.classification" /> id='searchType'
						src='${ctx}/images/img_lookup.gif' name='searchType'
						class='detail' /></td>
					<td class="label-title" nowrap><label title=<fmt:message key="biolims.common.applicant" />
						class="text label"><fmt:message key="biolims.common.applicant" /></label></td>
					<td nowrap><input type="text" searchField="true"
						class="input_parts text input default  readonlyfalse false"
						name="createUser-name" id="createUser_name" value='' title=<fmt:message key="biolims.common.applicant" />
						maxlength="30" /></td>
				</tr>


			</table>
		</div>
		<input type="hidden" id="id" value="" />
		<form name='excelfrm' action='/common/exportExcel.action'
			method='POST'>
			<input type='hidden' id='gridhtm' name='gridhtm' value='' />
		</form>
		<input type='hidden' id='extJsonDataString' name='extJsonDataString'
			value='' />

		<div id='purchase_apply_div'></div>
	</div>

</body>
</html> --%>