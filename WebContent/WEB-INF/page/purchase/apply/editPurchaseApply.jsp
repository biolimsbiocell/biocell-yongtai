<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 50px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="sampleReceiveTitle"
							type="<s:property value="sampleReceive.type"/>"><fmt:message
								key="biolims.common.purchasingApplication" /></strong>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefresh()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>

				</div>
				<div class="box-body">
					<input type="hidden" name="bpmTaskId" id="bpmTaskId"
						value="${requestScope.bpmTaskId}" />
					<form name="form1" id="form1" method="post">
						<div class="row">
							<!-- 编码 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.applicationCode" /><img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										 type="text" size="20" maxlength="25" readonly="readonly"
										 changelog="<s:property value=" purchaseApply.id  "/>"
										id="purchaseApply_id" name="id" class="form-control"
										title="" value="<s:property value=" purchaseApply.id "/>" />
								</div>
							</div>
							<!-- 描述 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.name" /></span><input
										 type="text" size="20" maxlength="25"
										id="purchaseApply_note" name="note" class="form-control"
										 changelog="<s:property value=" purchaseApply.note  "/>"
										title="" value="<s:property value=" purchaseApply.note "/>" />
								</div>
							</div>
							<!-- 分类 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">分类<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span>
										<select lay-ignore id="purchaseApply_type_id" class="form-control" name="type-id" onchange="selectType()">
											<option value="2" 
												<s:if test="purchaseApply.type.id==2">selected="selected"</s:if>>物资采购</option>
											<option value="77" 
												<s:if test="purchaseApply.type.id==77">selected="selected"</s:if>>设备采购</option>
											<option value="88" 
												<s:if test="purchaseApply.type.id==88">selected="selected"</s:if>>服务采购</option>
										</select>	
								</div>
							</div>
						</div>
						<div class="row">
							<!-- 申请人 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.tInstrumentBorrow.appleUser" />
									</span>
									<input type="hidden" changelog="<s:property value=" purchaseApply.createUser.id "/>" type="text" size="20" maxlength="25"
										id="purchaseApply_createUser_id" name="createUser-id"
										class="form-control" title=""
										value="<s:property value=" purchaseApply.createUser.id "/>" /> 
									<input  changelog="<s:property value=" purchaseApply.createUser.name "/>" type="text" size="20" maxlength="25"
										id="purchaseApply_createUser_name" name="createUser-name"
										class="form-control" title="" readonly="readonly"
										value="<s:property value=" purchaseApply.createUser.name "/>" />
								</div>
							</div>
							<!-- 申请日期 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group input-append date ">
									<span class="input-group-addon">申请日期
									</span> <input  changelog="<s:property value=" purchaseApply.createDate "/>" type="text" size="20" maxlength="25"
										id="purchaseApply_createDate" name="createDate"
										class="form-control" title="" readonly="readonly"
										value="<s:date name=" purchaseApply.createDate " format="yyyy-MM-dd"/>" />
								</div>
							</div>
							<!-- 批准人 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.ratifyPerson" />
									</span>
									<input type="hidden" changelog="<s:property value=" purchaseApply.confirmUser.id "/>" type="text" size="20" maxlength="25"
										id="purchaseApply_confirmUser" name="confirmUser-id"
										class="form-control" title=""
										value="<s:property value=" purchaseApply.confirmUser.id "/>" /> 
									<input  changelog="<s:property value=" purchaseApply.confirmUser.name "/>" type="text" size="20" maxlength="25"
										id="purchaseApply_confirmUser_name" name="confirmUser-name"
										class="form-control" title="" readonly="readonly"
										value="<s:property value=" purchaseApply.confirmUser.name "/>" />
									<span class="input-group-btn">
										<button class="btn btn-info" type="button"
										onClick="selectConfirmUser()">
										<i class="glyphicon glyphicon-search"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<!-- 状态 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.tStorage.state" /> </span>
										<input type="hidden" size="50" maxlength="127"
											id="purchaseApply_state" readonly="readOnly"
											title="" name="state"
											class="form-control"
											value="<s:property value="purchaseApply.state"/>" />
										<input type="text" size="50" maxlength="127"
											id="purchaseApply_stateName" readonly="readOnly"
											changelog="<s:property value="purchaseApply.stateName"/>"
											title="" name="stateName"
											class="form-control"
											value="<s:property value="purchaseApply.stateName"/>" />
									</div>
							</div>
							<!-- 期望到货日期 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group input-append date formatDate">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.expectedArrivalDate" /> </span>
										<input type="text" size="50" maxlength="127"
											id="purchaseApply_expectDate" 
											changelog="<s:property value="purchaseApply.expectDate"/>"
											title="" name="expectDate"
											class="form-control"
											value="<s:date name="purchaseApply.expectDate" format="yyyy-MM-dd"/>" />
									</div>
							</div>
							<!-- 备注 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">备注</span>
										<input type="text" size="50" maxlength="127"
											id="purchaseApply_notes" 
											changelog="<s:property value="purchaseApply.notes"/>"
											title="" name="notes"
											class="form-control"
											value="<s:property value="purchaseApply.notes"/>" />
									</div>
							</div>
						</div>
					</form>
				</div>
				<!-- 物资采购明细 -->
				<div id="goods" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;display: none;">
					<h3><fmt:message key="biolims.common.materialPurchaseDetail"/></h3>
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="goodsTable" style="font-size: 14px;"></table>
				</div>
				<!-- 设备采购明细 -->
				<div id="instrument" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;display: none;">
					<h3><fmt:message key="biolims.common.instrumentPurchaseDetail"/></h3>
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="instrumentTable" style="font-size: 14px;"></table>
				</div>
				<!-- 服务采购明细 -->
				<div id="service" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;display: none;">
					<h3><fmt:message key="biolims.common.servicePurchaseDetail"/></h3>
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="serviceTable" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/apply/editPurchaseApply.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/apply/showPurchaseEquipmentApplyItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/apply/showPurchaseGoodsApplyItem.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/apply/showPurchaseServiceApplyItem.js"></script>
</body>
</html>