<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>设备采购申请列表</title>
</head>
<body>
	<div>
		<table>
			<tr>
				<td  class="label-title">采购申请单号</td>
				<td>
					<input type="text"  id="sel_search_id">
				</td>
				<td  class="label-title" >描述</td>
				<td>
					<input type="text"  id="sel_search_note">
					<span class="search-btn" id="sel_search"></span>
				</td>
			</tr>
		</table>
	</div>
	<div>
		<div id="sel_purchase_apply_list_gird_div">
		<input type="hidden" value="${requestScope.applyType}" id="applyType"/>
		</div>
	</div>
<script type="text/javascript" src="${ctx}/javascript/purchase/apply/selPurchaseApplyList.js"></script>
</body>
</html>