<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/treegrid.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridSorter.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumnResizer.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridNodeUI.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridLoader.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGridColumns.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/ext-3.4.0/examples/ux/treegrid/TreeGrid.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/purchase/payable/purchasePayableTree.js"></script>

<script type="text/javascript"
	src="${ctx}/javascript/handleSearchForm.js"></script>

</head>

<body>
	
	<div id="jstj" style="display: none">
		
	</div>
	<input type="hidden" id="selectId" value="" />
	<input type="hidden" id="leaf" value="" />
	<input type="hidden" id="tf" value="" />
	<input type="hidden" id="purchasePayableTreePath" value="${path}" />
	<div class="mainlistclass" id="markup"></div>
</body>
</html>