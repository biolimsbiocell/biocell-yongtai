﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/purchase/payable/purchasePayableDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="purchasePayable_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">采购合同编码</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_contractId" searchField="true" name="contractId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">采购合同描述</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_contractName" searchField="true" name="contractName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">总价</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_totalPrice" searchField="true" name="totalPrice"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">已付</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_paid" searchField="true" name="paid"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">未付</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_unpaid" searchField="true" name="unpaid"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态名称</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="purchasePayable_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="purchasePayable_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_purchasePayable_div"></div>
   		
</body>
</html>



