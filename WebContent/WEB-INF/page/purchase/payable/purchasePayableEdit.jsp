<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=purchasePayable&id=${purchasePayable.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/purchase/payable/purchasePayableEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编码</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_id"
                   	 name="purchasePayable.id" title="编码"
                   	   
	value="<s:property value="purchasePayable.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="purchasePayable_name"
                   	 name="purchasePayable.name" title="描述"
                   	   
	value="<s:property value="purchasePayable.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/purchase/payable/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('purchasePayable_createUser').value=rec.get('id');
				document.getElementById('purchasePayable_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="purchasePayable_createUser_name"  value="<s:property value="purchasePayable.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="purchasePayable_createUser" name="purchasePayable.createUser.id"  value="<s:property value="purchasePayable.createUser.id"/>" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >创建时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_createDate"
                   	 name="purchasePayable.createDate" title="创建时间"
                   	   
                   	  value="<s:date name="purchasePayable.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >采购合同编码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_contractId"
                   	 name="purchasePayable.contractId" title="采购合同编码"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="purchasePayable.contractId"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >采购合同描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_contractName"
                   	 name="purchasePayable.contractName" title="采购合同描述"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="purchasePayable.contractName"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >总价</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_totalPrice"
                   	 name="purchasePayable.totalPrice" title="总价"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="purchasePayable.totalPrice"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >已付</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_paid"
                   	 name="purchasePayable.paid" title="已付"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="purchasePayable.paid"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >未付</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_unpaid"
                   	 name="purchasePayable.unpaid" title="未付"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="purchasePayable.unpaid"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_state"
                   	 name="purchasePayable.state" title="状态"
                   	   
	value="<s:property value="purchasePayable.state"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >状态名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="purchasePayable_stateName"
                   	 name="purchasePayable.stateName" title="状态名称"
                   	   
	value="<s:property value="purchasePayable.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ><img	class='requiredimage' src='${ctx}/images/notrequired.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="50" id="purchasePayable_note"
                   	 name="purchasePayable.note" title="备注"
                   	   
	value="<s:property value="purchasePayable.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="purchasePayable.id"/>" />
            </form>
        	</div>
	</body>
	</html>
