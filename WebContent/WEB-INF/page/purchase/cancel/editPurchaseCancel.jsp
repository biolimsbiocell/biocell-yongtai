<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>退货管理</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/purchase/cancel/editPurchaseCancel.js"></script>
</head>
<body>
<g:LayOutWinTag buttonId="searchStorageIn" title="选择入库单" hasHtmlFrame="true" 	html="${ctx}/storage/storageIn/storageInSelect.action" width="900" height="500"
	isHasSubmit="false" functionName="searchStorageInFun" hasSetFun="true" extRec="rec" extStr="
	document.getElementById('purchaseCancel_storageIn_id').value=rec.get('id');
	gridGrid.store.removeAll();
	gridGrid.store.commitChanges();
	store2 =new Ext.data.JsonStore({
	root: 'results',
	totalProperty: 'total',
	remoteSort: true,
	fields:  [{name:'id',type: 'string'},{name:'costCenter-id',type: 'string'},{name:'costCenter-name',type: 'string'},{name:'storage-id',type: 'string'},{name:'storage-type-id',type: 'string'},{name:'storage-name',type: 'string'},{name:'storage-unit-name',type: 'string'},{name:'storage-searchCode',type: 'string'},{name:'storage-spec',type: 'string'},{name:'productDate',type: 'date',dateFormat:'Y-m-d'},{name:'expireDate',type: 'date',dateFormat:'Y-m-d'},{name:'num',type: 'float'},{name:'price',type: 'float'},{name:'storage-num',type: 'float'},{name:'storageNum',type: 'float'},{name:'inNum',type: 'float'},{name:'supplierName',type: 'string'}],
	proxy: new Ext.data.HttpProxy({url: window.ctx+'/storage/storageIn/getStorageInItemListJson.action?storageInId='+document.getElementById('purchaseCancel_storageIn_id').value,method: 'POST'})
	});
	var o1 = {start:0,limit:1000};
	store2.load({params:o1});
	setTimeout(insertStorageInItem,1000); 
	"/>
<g:HandleDataExtTag hasConfirm="false" paramsValue="purchaseCancelId:'${purchaseCancelId}'" funName="modifyData" url="${ctx}/purchase/cancel/savePurchaseCancelItem.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/purchase/cancel/delPurchaseCancelItem.action" />
<g:LayOutWinTag title="选择库存" width="780" height="500" hasHtmlFrame="true"
	html="${ctx}/storage/common/showMainStorageAllSelectList.action"
	isHasSubmit="false" isHasButton="false"
	functionName="showMainStorageAllSelectList" />
<s:if test='#request.handlemethod=="modify"'>
<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true" width="900" height="500"
	html="${ctx}/operfile/initFileList.action?modelType=purchaseCancel&id=${purchaseCancel.id}"
	isHasSubmit="false" functionName="doc" />		
</s:if>	

	<g:LayOutWinTag title="选择库存" width="880" height="500"
		hasHtmlFrame="true"
		html="${ctx}/storage/common/showMainStorageAllSelectList.action"
		isHasSubmit="false" isHasButton="false"
		functionName="showMainStorageAllSelectList" />

<s:if test='#request.handlemethod=="modify"||#request.handlemethod=="add"'>
	<script type="text/javascript"
	src="${ctx}/javascript/handleVaLeave.js"></script>
</s:if>
<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<div id="maintab" style="margin:0 0 0 0"></div>
	<div id="markup" class="mainclass" >
		<s:form name="cancelForm" id="cancelForm" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0" >
			  <tbody>
			    <tr>
			      <td>
			        <table width="100%" class="section_table" cellpadding="0"      >
			          <tbody>
			            <tr class="control textboxcontrol" >
			              <td  valign="top" class="controlTitle" nowrap>
			                <span class="labelspacer">
			                  &nbsp;&nbsp;
			                </span>
			                <label title="退货编号"            class="text label">
			                  		退货编号
			                </label>
			              </td>
			              <td class="requiredcolumn" nowrap width="10px">
			                <img  class='requiredimage' src='${ctx}/images/required.gif'       />
			              </td>
			              <td nowrap>
			                <s:textfield cssClass="input_parts text input default  readonlytrue false"
			                name="purchaseCancel.id" id="purchaseCancel_id" readonly="true"
			                title="退货编号 " size="20" maxlength="30"></s:textfield>
			               </td>
			              <td nowrap>
			                &nbsp;
			              </td>
			            </tr>
			            <tr class="control textboxcontrol" >
			              <td valign="top" align="right" class="controlTitle" nowrap>
			                <span class="labelspacer">
			                  &nbsp;&nbsp;
			                </span>
			                <label title="创建人"                 class="text label">
			                  创建人
			                </label>
			              </td>
			              <td class="requiredcolumn" nowrap width="10px">
			                <img  class='requiredimage' src='${ctx}/images/notrequired.gif'
			                />
			              </td>
			              <td nowrap>
			                <s:hidden name="purchaseCancel.createUser.id" id="purchaseCancel_createUser__id"></s:hidden>
			                <s:textfield cssClass="input_parts text input default  readonlytrue false"
			                name="purchaseCancel.createUser.name" id="purchaseCancel_createUser_name" 
			               readonly="true" 
			                title="创建人 " size="10" maxlength="30"></s:textfield>
			              </td>
			              <td nowrap>
			                &nbsp;
			              </td>
			            </tr>
			            <tr class="control textboxcontrol" >
			              <td valign="top" align="right" class="controlTitle" nowrap>
			                <span class="labelspacer">
			                  &nbsp;&nbsp;
			                </span>
			                <label title="批准人"  
			                 class="text label">
			                  批准人
			                </label>
			              </td>
			              <td class="requiredcolumn" nowrap width="10px">
			                <img  class='requiredimage' src='${ctx}/images/notrequired.gif'         />
			              </td>
			              <td nowrap>
			              	<s:hidden name="purchaseCancel.confirmUser.id" id="purchaseCancel_confirmUser_id"></s:hidden>
			                <s:textfield cssClass="input_parts text input default  readonlytrue false"
			                name="purchaseCancel.confirmUser.name" id="purchaseCancel_confirmUser_name" readonly="true"
			                title="批准人  " size="10" maxlength="30"></s:textfield>
			              </td>
			              <td nowrap>
			                &nbsp;
			              </td>
			            </tr>
			          </tbody>
			        </table>
			      </td>
			      <td class="sectioncol " width='33%' valign="top" 
			      align="right">
			        <div class="section standard_section  " >
			          <div           class="section">
			          
			            <table width="100%" class="section_table" cellpadding="0"            >
			              <tbody>
			                <tr class="control textboxcontrol"  >
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="原因描述"  
			                    class="text label">
			                      原因描述
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif' />
			                  </td>
			                  <td nowrap>
			                    <s:textfield cssClass="input_parts text input default  readonlyfalse false"
			                    name="purchaseCancel.reason" id="purchaseCancel_reason"
			                    title="描述  " size="50" maxlength="55"></s:textfield>
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                </tr>
			                <tr class="control textboxcontrol" >
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="退货日期"  
			                    class="text label">
			                      退货日期
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			                  </td>
			                  <td nowrap>
			                    <input type="text" class="input_parts text input default  readonlytrue false"
			                    name="purchaseCancel.canelDate" id="purchaseCancel_canelDate" readonly="readOnly"
			                    value="<s:date name="purchaseCancel.canelDate" format="yyyy-MM-dd" />" title="退货日期 " size="20" maxlength="30">
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                  </tr>
		                  	<tr class="control textboxcontrol">
								<td valign="top" align="right" class="controlTitle" nowrap>
									<span class="labelspacer">&nbsp;&nbsp;</span> <label title="状态" 	class="text label">	状态 </label>
								</td>

							<td class="requiredcolumn" nowrap width="10px"><img class='requiredimage'
								src='${ctx}/images/notrequired.gif' /></td>
							<td nowrap>
								<input type="text"	class="input_parts text input default  readonlytrue false"
								name="purchaseCancel.stateName" value="<s:property value="purchaseCancel.stateName"/>"
								id="purchaseCancel_stateName" title="状态"  readonly="readOnly"	onblur="textbox_ondeactivate(event);"	 onfocus="shared_onfocus(event,this)"		size="20" maxlength="30" >
								</input> 
							</td>
								<s:hidden name="purchaseCancel.state" id="purchaseCancel_state" ></s:hidden>
							<td nowrap>&nbsp;</td>
						</tr>
			              </tbody>
			            </table>
			          </div>
			        </div>
			      </td>
		     <td class="sectioncol " width='33%' valign="top" align="right">
			        <div class="section standard_section  " >
			          <div           class="section">
			            <table width="100%" class="section_table" cellpadding="0" >
			              <tbody>
                             <tr class="control textboxcontrol"  >
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="采购订单号"       class="text label">
			                      		入库单号
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			                  </td>
			                  <td nowrap>
			                    <s:textfield cssClass="input_parts text input default  readonlyfalse false" readonly="true"
			                    name="purchaseCancel.storageIn.id" id="purchaseCancel_storageIn_id"
			                    title="入库单号 " size="30" maxlength="55"></s:textfield>
			                     <img alt='选择 入库单号' id='searchStorageIn' src='${ctx}/images/img_lookup.gif'
			                    name='searchStorageIn' class='detail' />
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                </tr>
						<tr id="doclinks" >
							<td valign="top" align="right" class="controlTitle" nowrap>
								<span class="labelspacer">&nbsp;&nbsp;</span>
								 <label	 title="附件" class="text label"  >附件</label>&nbsp;
							</td>
							<td class="requiredcolumn" nowrap width="10px">
								<img		class='requiredimage' src='${ctx}/images/notrequired.gif' />
							</td>
							<td align="left">
								<img alt="保存基本后,可以维护查看附件"  id="doclinks_img" 	class="detail" src="${ctx}/images/img_attach.gif"  />
							</td>
							<td nowrap>&nbsp;</td>
							<td nowrap></td>
							<td nowrap></td>
						</tr>
			            </tbody>
			            </table>
			          </div>
			        </div>
			      </td>
			      </tr>
			  </tbody>
			</table>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value=""/>
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="copyMode" value="${requestScope.copyMode}">
	
		</s:form>
	 <g:GridEditTag isAutoWidth="true"
				col="${col}" statement="${statement}" type="${type}" title="退货明细" addMethod="window.showMainStorageAllSelectList()" validateMethod="validateFun" 
				url="${path}" statementLisener="${statementLisener}" handleMethod="${handlemethod}"/>	
	</div>

</body>
</html>