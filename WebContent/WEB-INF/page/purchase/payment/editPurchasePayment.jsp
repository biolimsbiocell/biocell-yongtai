<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>付款管理</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/purchase/payment/editPurchasePayment.js"></script>
</head>
<body>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="searchType" title="选择收款形式" hasHtmlFrame="true" 	html="${ctx}/dic/type/dicTypeSelect.action"
	isHasSubmit="false" functionName="sfkxs"  hasSetFun="true" documentId="purchasePayment_type_id" documentName="purchasePayment_type_name"/>	
	<g:LayOutWinTag  title="选择采购订单" hasHtmlFrame="true" isHasButton="false"	html="${ctx}/purchase/order/purchaseOrderSelect.action" width="900" height="500"
	isHasSubmit="false" functionName="searchOrderFun" hasSetFun="true" extRec="rec" extStr="
	var record = orderGrid.getSelectionModel().getSelected();
	record.set('purchaseOrder-id',rec.get('id'));
	record.set('purchaseOrder-createUser-name',rec.get('createUser-name'));
	record.set('purchaseOrder-fee',rec.get('fee'));
	"/>
</s:if>
<g:HandleDataExtTag hasConfirm="false" paramsValue="purchasePaymentId:'${purchasePayment.id}'" funName="modifyData" url="${ctx}/purchase/payment/savePurchasePaymentItem.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/purchase/payment/delPurchasePaymentItem.action" />

<s:if test='#request.handlemethod=="modify"||#request.handlemethod=="add"'>
	<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
</s:if>
<%@ include file="/WEB-INF/page/include/toolbarNoTab.jsp"%>
	<div id="maintab" style="margin:0 0 0 0"></div>

<div id="markup" class="mainclass" >
		<s:form name="paymentForm" id="paymentForm" theme="simple">
			<table width="100%" cellpadding="0" cellspacing="0" >
			  <tbody>
			    <tr>
			      <td>
			        <table width="100%" class="section_table" cellpadding="0" >
			          <tbody>
			            <tr class="control textboxcontrol" >
			              <td  valign="top" class="controlTitle" nowrap>
			                <span class="labelspacer">
			                  &nbsp;&nbsp;
			                </span>
			                <label title="付款编号"   class="text label">
			                	  付款编号
			                </label>
			              </td>
			              <td class="requiredcolumn" nowrap width="10px">
			                <img  class='requiredimage' src='${ctx}/images/required.gif'/>
			              </td>
			              <td nowrap>
			                <s:textfield cssClass="input_parts text input default  readonlytrue false"
			                name="purchasePayment.id" id="purchasePayment_id"
			                title="付款编号 " size="20" maxlength="30"></s:textfield>
			              </td>
			              <td nowrap>
			                &nbsp;
			              </td>
			            </tr>
			            <tr class="control textboxcontrol" >
			              <td valign="top" align="right" class="controlTitle" nowrap>
			                <span class="labelspacer">
			                  &nbsp;&nbsp;
			                </span>
			                <label title="申请人"   class="text label">
			  				                申请人
			                </label>
			              </td>
			              <td class="requiredcolumn" nowrap width="10px">
			                <img  class='requiredimage' src='${ctx}/images/notrequired.gif' />
			              </td>
			              <td nowrap>
			                <s:hidden name="purchasePayment.createUser.id" id="purchasePayment_createUser__id"></s:hidden>
			                <s:textfield cssClass="input_parts text input default  readonlytrue false"
			                name="purchasePayment.createUser.name" id="purchasePayment_createUser_name" 
			               readonly="true"  title="申请人  " size="10" maxlength="30"></s:textfield>
			              </td>
			              <td nowrap>
			                &nbsp;
			              </td>
			            </tr>
			            <tr class="control textboxcontrol" >
			              <td valign="top" align="right" class="controlTitle" nowrap>
			                <span class="labelspacer">
			                  &nbsp;&nbsp;
			                </span>
			                <label title="批准人"  
			                 class="text label">
			         		         批准人
			                </label>
			              </td>
			              <td class="requiredcolumn" nowrap width="10px">
			                <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			              </td>
			              <td nowrap>
			              	<s:hidden name="purchasePayment.confirmUser.id" id="purchasePayment_confirmUser_id"></s:hidden>
			                <s:textfield cssClass="input_parts text input default  readonlytrue false"
			                name="purchasePayment.confirmUser.name" id="purchasePayment_confirmUser_name" readonly="true"
			                title="批准人  " size="10" maxlength="30"></s:textfield>
			              </td>
			              <td nowrap>
			                &nbsp;
			              </td>
			            </tr>
			          </tbody>
			        </table>
			      </td>
			      <td class="sectioncol " width='33%' valign="top" 
			      align="right">
			        <div class="section standard_section  " >
			          <div           class="section">
			            <div class="section_description" style="display:none">
			            </div>
			            <table width="100%" class="section_table" cellpadding="0" >
			              <tbody>
			                <tr class="control textboxcontrol"  >
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="描述"  class="text label">
			                      	描述
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			                  </td>
			                  <td nowrap>
			                    <s:textfield cssClass="input_parts text input default  readonlyfalse false"
			                    name="purchasePayment.note" id="purchasePayment_note"
			                    title="描述  " size="50" maxlength="55"></s:textfield>
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                </tr>
			                <tr class="control textboxcontrol" >
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="申请日期"  
			                    class="text label">
			                      申请日期
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			                  </td>
			                  <td nowrap>
			                    <input type="text" class="input_parts text input default  readonlytrue false"
			                    name="purchasePayment.createDate" id="purchasePayment_createDate" readonly="readOnly"
			                    value="<s:date name="purchasePayment.createDate" format="yyyy-MM-dd" />" title="申请日期 " size="20" maxlength="30">
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                  </tr>
		                  	<tr class="control textboxcontrol">
								<td valign="top" align="right" class="controlTitle" nowrap>
										<span class="labelspacer">&nbsp;&nbsp;</span> <label title="工作流状态" 	class="text label">工作流状态 </label>
								</td>
								<td class="requiredcolumn" nowrap width="10px">
									<img	class='requiredimage'	src='${ctx}/images/notrequired.gif' />
								</td>
								<td nowrap>
									<input type="text"			class="input_parts text input default  readonlytrue false"
									name="purchasePayment.stateName" value="<s:property value="purchasePayment.stateName"/>"
									id="purchasePayment_stateName" title="状态"  readonly="readOnly"
									onblur="textbox_ondeactivate(event);"	 onfocus="shared_onfocus(event,this)" size="20" maxlength="30" ></input>
								</td>
								<s:hidden name="purchasePayment.state" id="purchasePayment_state" ></s:hidden>
								<td nowrap>&nbsp;</td>
							</tr>
			              </tbody>
			            </table>
			          </div>
			        </div>
			      </td>
			      <td class="sectioncol " width='33%' valign="top"   align="right">
			        <div class="section standard_section  " >
			          <div  class="section">
			            <table width="100%" class="section_table" cellpadding="0" >
			              <tbody>
			                <tr class="control textboxcontrol"  >
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="付款类型"  class="text label">
			        			              付款类型
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			                  </td>
			                  <td nowrap>
			                    <s:hidden name="purchasePayment.type.id" id="purchasePayment_type_id"></s:hidden>
			                    <s:textfield cssClass="input_parts text input default  readonlyfalse false"
			                    name="purchasePayment.type.name" id="purchasePayment_type_name"
			                    title="付款类型" size="10" maxlength="20" readonly="true"></s:textfield>
			                    <img alt='付款类型' id='searchType' src='${ctx}/images/img_lookup.gif'  name='searchType' class='detail' />
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                </tr>
			                <!-- tr class="control textboxcontrol">
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="发票比例"  
			                     class="text label">
			                      发票比例
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'
			                    />
			                  </td>
			                  <td nowrap>
			                
			                    <s:textfield cssClass="input_parts text input default  readonlyfalse false"
			                    name="purchasePayment.scale" id="purchasePayment_.scale"
			                    title="发票比例" size="20" maxlength="20"></s:textfield>
			          
			                  
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                </tr-->
			      	        <tr class="control textboxcontrol">
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="发票状态"    class="text label">
			                      		发票状态
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			                  </td>
			                  <td nowrap>
			                    <s:textfield cssClass="input_parts text input default  readonlyfalse false"
			                    name="purchasePayment.invoiceState" id="purchasePayment_invoiceState"
			                    title="发票状态" size="20" maxlength="20"></s:textfield>
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                </tr>
		                    <tr class="control textboxcontrol">
			                  <td valign="top" align="right" class="controlTitle" nowrap>
			                    <span class="labelspacer">
			                      &nbsp;&nbsp;
			                    </span>
			                    <label title="付款状态"  
			                     class="text label">
			           			           付款状态
			                    </label>
			                  </td>
			                  <td class="requiredcolumn" nowrap width="10px">
			                    <img  class='requiredimage' src='${ctx}/images/notrequired.gif'/>
			                  </td>
			                  <td nowrap>
			                    <s:textfield cssClass="input_parts text input default  readonlyfalse false"
			                    name="purchasePayment.paymentState" id="purchasePayment_paymentState"
			                    title="付款状态" size="20" maxlength="20"></s:textfield>
			                  </td>
			                  <td nowrap>
			                    &nbsp;
			                  </td>
			                </tr>
			              </tbody>
			            </table>
			          </div>
			        </div>
			      </td>
			      </tr>
			  </tbody>
			</table>
		
						<div 		class="section">
							<table width="100%" class="section_table" cellpadding="0"
								cellspacing="0" >
								<tbody>
									<tr   class="sectionrow " valign="top" >
										<td class="sectioncol " width='50%' valign="top" align="right">
										<div  class="section_header standard_section_header">
										<table width="100%" cellpadding="0" cellspacing="0">
											<tbody>
												<tr >
													<td	class="section_header_left text standard_section_label labelcolor"	align="left">
														<span class="section_label">供应商信息 </span>
													</td>
													<td class="section_header_right" align="right">
													</td>
												</tr>
											</tbody>
										</table>
										</div>
										<div class=" section_content_border">
											<table width="100%" class="section_table" cellpadding="0"	cellspacing="0" >
											<tbody>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													<label	title="供应商" class="text label"> 供应商 </label></td>
													<td class="requiredcolumn" nowrap width="10px">
														<img class='requiredimage' src='${ctx}/images/notrequired.gif' />
													</td>
													<td nowrap>
														<s:textfield	cssClass="input_parts text input  readonlytrue false"
														name="purchasePayment.purchaseOrder.supplier.id" id="purchasePayment_purchaseOrder_supplier_id" title="供应商"
														onblur="textbox_ondeactivate(event);" readonly="true" onfocus="shared_onfocus(event,this)"
														size="15" maxlength="30">
														</s:textfield>
														<img  class='requiredimage' width='10'	 src='${ctx}/images/notrequired.gif' />
 														<img class="detail" src="${ctx}/images/menu_icon_link.gif" onclick="viewSupplier();" alt="查看合同">
														<s:textfield cssClass="input_parts text input  readonlytrue"
															name="purchasePayment.purchaseOrder.supplier.name"
															id="purchasePayment_purchaseOrder_supplier_name" readonly="true"
															 title="供应商名称 "  onblur="textbox_ondeactivate(event);"
															 onfocus="shared_onfocus(event,this)" size="40" maxlength="62">
														</s:textfield>
														<img id="customerDetail" src="${ctx}/images/img_longdescription_off.gif" class='detail'	alt="详细">
													</td>
													<td nowrap></td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span>
														<label	title="联系人" class="text label">联系人</label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
														<img class='requiredimage'	src='${ctx}/images/notrequired.gif' />
													</td>
													<td nowrap>
													<s:textfield cssClass="input_parts text input default  readonlytrue false" 
														name="purchasePayment.purchaseOrder.supplier.linkMan"
														id="purchasePayment_purchaseOrder_supplier_linkMan"
														 title="联系人"
														onblur="textbox_ondeactivate(event);"
														 onfocus="shared_onfocus(event,this)"
														size="10" maxlength="20" ></s:textfield> </td>
													<td nowrap>&nbsp;</td>
													<td valign="top" align="right" class="controlTitle" nowrap>
													<span class="labelspacer">&nbsp;&nbsp;</span>
													 <label		title="联系电话" 
														class="text label">联系电话</label></td>
													<td class="requiredcolumn" nowrap width="10px"><img
														 class='requiredimage'
														src='${ctx}/images/notrequired.gif' /></td>
													<td nowrap><s:textfield 
														cssClass="input_parts text input default  readonlytrue false"
														name="purchasePayment.purchaseOrder.supplier.linkTel"
														id="purchasePayment_purchaseOrder_supplier_linkTel"
														title="联系电话"
														onblur="textbox_ondeactivate(event);"
														 onfocus="shared_onfocus(event,this)"
														size="20" maxlength="20" > </s:textfield>
													</td>
													<td nowrap>&nbsp;</td>
												</tr>
											</tbody>
										</table>
										</div>
								</tbody>
							</table>
							</div>
						<div class="section">
							<table width="100%" class="section_table" cellpadding="0"
								cellspacing="0" >
								<tbody>
									<tr class="sectionrow "	valign="top" >
										<td class="sectioncol " width='50%' valign="top" align="right">
		
										<div  class="section_header standard_section_header">
										<table width="100%" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td		class="section_header_left text standard_section_label labelcolor"	align="left">
														<span class="section_label">账户信息 </span></td>
													<td class="section_header_right" align="right"></td>
												</tr>
											</tbody>
										</table>
										</div>
										<div class=" section_content_border">
										<table width="100%" class="section_table" cellpadding="0"	cellspacing="0" >
											<tbody>
												<tr class="control textboxcontrol">
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span>
													 	<label	title="开户行" class="text label"> 开户行 </label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
														<img class='requiredimage'	src='${ctx}/images/notrequired.gif' />
													</td>
													<td nowrap>
														<s:textfield
														cssClass="input_parts text input  readonlytrue false"
														name="purchasePayment.purchaseOrder.supplier.accountBank" id="purchasePayment_purchaseOrder_supplier_accountBank" title="开户行"
														onblur="textbox_ondeactivate(event);" readonly="true"
														 onfocus="shared_onfocus(event,this)"
														size="20" maxlength="50" ></s:textfield>
														<img  class='requiredimage' width='10' src='${ctx}/images/notrequired.gif' />
													</td>
													<td nowrap></td>
													<td valign="top" align="right" class="controlTitle" nowrap>
														<span class="labelspacer">&nbsp;&nbsp;</span>
														<label	title="账户"	class="text label">账户</label>
													</td>
													<td class="requiredcolumn" nowrap width="10px">
														<img class='requiredimage'	src='${ctx}/images/notrequired.gif' />
													</td>
													<td nowrap>
														<s:textfield	cssClass="input_parts text input default  readonlytrue false" 
															name="purchasePayment.purchaseOrder.supplier.account"	id="purchasePayment_purchaseOrder_supplier_account"
														 title="账户" 	onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" size="20" maxlength="40" ></s:textfield>
													</td>
													<td nowrap>&nbsp;</td>
												</tr>
											</tbody>
										</table>
										</div>
								</tbody>
							</table>
							</div>
			<input type="hidden" name="jsonDataStr" id="jsonDataStr" value=""/>
			<input type="hidden" name="jsonDataStr1" id="jsonDataStr1" value=""/>
			<input type="hidden" name="jsonDataStr2" id="jsonDataStr2" value=""/>
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
			<input type="hidden" id="copyMode" value="${requestScope.copyMode}">
		</s:form>
	<g:GridEditTag isAutoWidth="true"
				col="${col1}"  type="${type1}" title="订单信息" id="order" tbl="{text: '查看', handler : function(){view()}}" 
				url="${path1}" height="200" handleMethod="${handlemethod}"/>		
		
	 <g:GridEditTag isAutoWidth="true"
				col="${col}"  type="${type}" title="付款信息" 
				url="${path}" height="200" handleMethod="${handlemethod}"/>	
	
	 <g:GridEditTag isAutoWidth="true" id="invoice"
				col="${col2}"  type="${type2}" title="发票信息" 
				url="${path2}" height="200" handleMethod="${handlemethod}"/>	
	</div>

</body>
</html>