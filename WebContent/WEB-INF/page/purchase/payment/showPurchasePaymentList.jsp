<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>采购申请列表</title>
</head>
<body>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>

<script type="text/javascript" src="${ctx}/javascript/purchase/payment/showPurchasePaymentList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<g:LayOutWinTag buttonId="searchdepartment" title="选择部门"  hasHtmlFrame="true"
	html="${ctx}/core/department/departmentSelect.action"
	isHasSubmit="false" functionName="department"  hasSetFun="true" documentId="department_id" documentName="department_name" />
<g:LayOutWinTag buttonId="searchSqr" title="申请人"
	hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
	isHasSubmit="false" functionName="pzr" hasSetFun="true"
	documentId="createUser_id"
	documentName="createUser_name" />
<div  class="mainlistclass" id="markup">
<div  id="jstj" style="display:none" >
<table cellspacing="0" cellpadding="0" class="toolbarsection">
					<tr>
						<td>&nbsp;</td>
						<td ><span>&nbsp;</span>
						<label class="text label" title="输入查询条件">
						编号</label></td>
						<td>&nbsp;</td>
						<td nowrap class="quicksearch_control" valign="middle"><input type="text"
							class="input_parts text input default  readonlyfalse false"
							name="id" onblur="textbox_ondeactivate(event);" 
							onfocus="shared_onfocus(event,this)" searchField="true"
							id="id" title="输入ID" style="width: 90"> </td>
						<td ><span>&nbsp;</span>
						<label class="text label" title="输入查询条件">
						描述</label></td>
						<td>&nbsp;</td>
						<td nowrap class="quicksearch_control" valign="middle"><input type="text"
							class="input_parts text input default  readonlyfalse false" name="note"
							onblur="textbox_ondeactivate(event);" 
							onfocus="shared_onfocus(event,this)" searchField="true"
							id="note" title="输入描述" style="width: 90"></td>
			
						
						
						<td nowrap>&nbsp;</td>
								<td valign="top" align="right" class="controlTitle" nowrap >
									<span class="labelspacer">&nbsp;&nbsp;</span>
									<label title="组织机构"   class="text label" >
									组织机构
									</label>
								</td>
								
								<td nowrap>
									<input type="text"   class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="" id="department_name" title="类型  "   onblur="textbox_ondeactivate(event);"   onfocus="shared_onfocus(event,this)"  size="10"  maxlength="20"   >
									<input type="hidden" searchField="true" name="department.id" id="department_id" >
									
									<img  title='选择' id='searchdepartment'  src='${ctx}/images/img_lookup.gif'  class='detail' />

								</td>
								
								<td valign="top" align="right" class="controlTitle" nowrap >
				
						<label title="申请人"    class="text label" >
							申请人
						</label>
					</td>
						<td>&nbsp;</td>
					<td nowrap>
						<input type="text" class="input_parts text input default  readonlyfalse false" readonly="readOnly" name="createUser-name" id="createUser_name" value=''  title="申请人 " size="10"  maxlength="30"	>
						<input type="hidden" searchField="true" name="createUser.id" id="createUser_id" value=''>
						
 <img  title='选择'   id="searchSqr" src='${ctx}/images/img_lookup.gif'  class='detail'  />
					</td>	



			
						
						
								<td valign="top" align="right" class="controlTitle" nowrap >
									<span class="labelspacer">&nbsp;&nbsp;</span>
									<label title="状态"    class="text label" >
										状态
									</label>
									
								</td>
									<td>&nbsp;</td>
								<td nowrap>
							

									<select  id="storage_state" searchField="true" name="state" style="width:60pt">
									<option value="" >全部</option>
    								<option value="1" >有效</option>
    								<option value="3">新建</option>
    								<option value="0">无效</option>


												</select>
									
								</td>
							
						
						<td >
						
						
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件">
						记录数</label></td>
						<td>&nbsp;</td>
						<td nowrap class="quicksearch_control" valign="middle"><input type="text"
							class="input_parts text input default  readonlyfalse false" name="limitNum"
							onblur="textbox_ondeactivate(event);" 
							onfocus="shared_onfocus(event,this)"
							id="limitNum" title="输入记录数" style="width: 90">
							
							
							
							 <img 
							class="quicksearch_findimage" id="quicksearch_findimage"
							 name="quicksearch_findimage"
							onclick="commonSearchAction(gridGrid)" src="${ctx}/images/quicksearch.gif" title="搜索"
							>
							<img 
							class="quicksearch_findimage" id="quicksearch_findimage"
							 name="quicksearch_findimage"
							onclick="form_reset()" src="${ctx}/images/no_draw.gif" title="清空"
							></td>	
					</tr>
				</table>
</div>
	
	<g:GridTag
			isAutoWidth="true" col="${col}" type="${type}" title="${title}" url="${path}" />
</div>

</body>
</html>