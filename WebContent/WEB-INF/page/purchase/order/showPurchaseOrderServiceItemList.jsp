<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>服务采购申请明细列表</title>
<script type="text/javascript"
	src="${ctx}/javascript/purchase/order/showPurchaseOrderServiceItemList.js"></script>
</head>
<body>
	<div>
		<div id="purchase_order_service_item_gird_div"></div>
	</div>
</body>
</html>