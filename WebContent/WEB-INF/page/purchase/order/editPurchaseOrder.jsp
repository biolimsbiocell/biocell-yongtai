<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dataTables_scrollBody {
	min-height: 150px;
}

.layui-layer-content .input-group {
	margin-top: 12px;
}

#fieldItemDiv .icheckbox_square-blue {
	margin-left: 10px;
}
#btn_submit{
	display:none;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 50px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="sampleReceiveTitle"
							type="<s:property value="sampleReceive.type"/>"><fmt:message
								key="biolims.common.purchasingApplication" /></strong>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefresh()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>

				</div>
				<div class="box-body">
					<input type="hidden" name="bpmTaskId" id="bpmTaskId"
						value="${requestScope.bpmTaskId}" />
					<form name="form1" id="form1" method="post">
						<div id="jbxx">
							<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												基本信息
											</h3>
										</div>
									</div>
							</div>
							<br />
							<div class="row">
								<!-- 编码 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">采购订单编号<img
											class='requiredimage' src='${ctx}/images/required.gif' /></span><input
											 type="text" size="20" maxlength="25" readonly="readonly"
											id="purchaseOrder_id" name="id" class="form-control"
											title="" value="<s:property value=" purchaseOrder.id "/>" />
									</div>
								</div>
								<!-- 描述 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.name" /></span><input
											 type="text" size="20" maxlength="25"
											id="purchaseOrder_note" name="note" class="form-control"
											title="" value="<s:property value=" purchaseOrder.note "/>" />
									</div>
								</div>
								<!-- 分类 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">分类<img
											class='requiredimage' src='${ctx}/images/required.gif' /></span>
											<select lay-ignore id="purchaseOrder_type_id" class="form-control" name="type-id" onchange="selectType()">
												<option value="2" 
													<s:if test="purchaseOrder.type.id==2">selected="selected"</s:if>>物资采购</option>
												<option value="77" 
													<s:if test="purchaseOrder.type.id==77">selected="selected"</s:if>>设备采购</option>
												<option value="88" 
													<s:if test="purchaseOrder.type.id==88">selected="selected"</s:if>>服务采购</option>
											</select>	
									</div>
								</div>
							</div>
							<div class="row">
								<!-- 申请人 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.tInstrumentBorrow.appleUser" />
										</span>
										<input type="hidden" changelog="<s:property value=" purchaseOrder.createUser.id "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_createUser_id" name="createUser-id"
											class="form-control" title=""
											value="<s:property value=" purchaseOrder.createUser.id "/>" /> 
										<input  changelog="<s:property value=" purchaseOrder.createUser.name "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_createUser_name" name="createUser-name"
											class="form-control" title="" readonly="readonly"
											value="<s:property value=" purchaseOrder.createUser.name "/>" />
									</div>
								</div>
								<!-- 币种 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">币种
										</span>
										<input type="hidden" changelog="<s:property value=" purchaseOrder.currencyType.id "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_currencyType" name="currencyType-id"
											class="form-control" title=""
											value="<s:property value=" purchaseOrder.currencyType.id "/>" /> 
										<input  changelog="<s:property value=" purchaseOrder.currencyType.name "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_currencyType_name" name="currencyType-name"
											class="form-control" title="" readonly="readonly"
											value="<s:property value=" purchaseOrder.currencyType.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
											onClick="selectCurrency ()">
											<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<!-- 预计到货日期 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">预计到货日期
										</span> <input  changelog="<s:property value=" purchaseOrder.reachDate "/>" type="text" size="20" maxlength="25"
											id=purchaseOrder_reachDate name="reachDate"
											class="form-control" title="" 
											value="<s:date name=" purchaseOrder.reachDate " format="yyyy-MM-dd"/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<!-- 批准人 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.ratifyPerson" />
										</span>
										<input type="hidden" changelog="<s:property value=" purchaseOrder.confirmUser.id "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_confirmUser" name="confirmUser-id"
											class="form-control" title=""
											value="<s:property value=" purchaseOrder.confirmUser.id "/>" /> 
										<input  changelog="<s:property value=" purchaseOrder.confirmUser.name "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_confirmUser_name" name="confirmUser-name"
											class="form-control" title="" readonly="readonly"
											value="<s:property value=" purchaseOrder.confirmUser.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
											onClick="selectConfirmUser()">
											<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								
								<!-- 创建日期 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group input-append date formatDate">
										<span class="input-group-addon">创建日期</span>
										<input type="text" size="50" maxlength="127"
											id="purchaseOrder_createDate" 
											changelog="<s:property value="purchaseOrder.createDate"/>"
											title="" name="createDate"
											class="form-control" readonly="readonly"
											value="<s:date name="purchaseOrder.createDate" format="yyyy-MM-dd"/>" />
									</div>
								</div>
								<!-- 金额 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group input-append date formatDate">
										<span class="input-group-addon">金额</span>
										<input type="text" size="50" maxlength="127"
											id="purchaseOrder_fee" 
											changelog="<s:property value="purchaseOrder.fee"/>"
											title="" name="fee"
											class="form-control"
											value="<s:property value=" purchaseOrder.fee "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<!-- 状态 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message
													key="biolims.tStorage.state" /> </span>
											<input type="hidden" size="50" maxlength="127"
												id="purchaseOrder_state" readonly="readOnly"
												title="" name="state"
												class="form-control"
												value="<s:property value="purchaseOrder.state"/>" />
											<input type="text" size="50" maxlength="127"
												id="purchaseOrder_stateName" readonly="readOnly"
												changelog="<s:property value="purchaseOrder.stateName"/>"
												title="" name="stateName"
												class="form-control"
												value="<s:property value="purchaseOrder.stateName"/>" />
										</div>
								</div>
								<!-- 备注 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="input-group">
											<span class="input-group-addon">备注</span>
											<input type="text" size="50" maxlength="127"
												id="purchaseOrder_zhuSi" 
												changelog="<s:property value="purchaseOrder.zhuSi"/>"
												title="" name="zhuSi"
												class="form-control"
												value="<s:property value="purchaseOrder.zhuSi"/>" />
										</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
											<fmt:message key="biolims.common.uploadAttachment" />
										</button>&nbsp;&nbsp;
											<button type="button" class="btn btn-info btn-sm"
												onclick="fileView()">
												<fmt:message key="biolims.report.checkFile" />
											</button>
									</div>
								</div>
							</div>
						</div>
						<div id="shxx">
							<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												收货信息
											</h3>
										</div>
									</div>
							</div>
							<br />
							<div class="row">
								<!-- 备注 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">收货地址</span>
										<input type="text" size="50" maxlength="127"
											id="purchaseOrder_shAdress" 
											changelog="<s:property value="purchaseOrder.shAdress"/>"
											title="" name="shAdress"
											class="form-control"
											value="<s:property value="purchaseOrder.shAdress"/>" />
									</div>
								</div>
								<!-- 发票寄送地址 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">发票寄送地址</span>
										<input type="text" size="50" maxlength="127"
											id="purchaseOrder_fpAdress" 
											changelog="<s:property value="purchaseOrder.fpAdress"/>"
											title="" name="fpAdress"
											class="form-control"
											value="<s:property value="purchaseOrder.fpAdress"/>" />
									</div>
								</div>
								<!-- 采购联系人 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">采购联系人
										</span>
										<input type="hidden" changelog="<s:property value=" purchaseOrder.caigouUser.id "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_caigouUser" name="caigouUser-id"
											class="form-control" title=""
											value="<s:property value=" purchaseOrder.caigouUser.id "/>" /> 
										<input  changelog="<s:property value=" purchaseOrder.caigouUser.name "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_caigouUser_name" name="caigouUser-name"
											class="form-control" title="" readonly="readonly"
											value="<s:property value=" purchaseOrder.caigouUser.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
											onClick="selectBuyer()">
											<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<!-- 采购联系电话 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">采购联系人电话
										</span>
										<input  changelog="<s:property value=" purchaseOrder.caigouUser.mobile "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_caigouUser_mobile" name="caigouUser-mobile"
											class="form-control" title="" 
											value="<s:property value=" purchaseOrder.caigouUser.mobile "/>" />
									</div>
								</div>
								<!-- 采购联系人email -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">采购联系人email
										</span>
										<input  changelog="<s:property value=" purchaseOrder.caigouUser.email "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_caigouUser_email" name="caigouUser-email"
											class="form-control" title="" 
											value="<s:property value=" purchaseOrder.caigouUser.email "/>" />
									</div>
								</div>
							</div>
						</div>
						<div id="jkxx">
							<div class="row">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												付款信息
											</h3>
										</div>
									</div>
							</div>
							<br />
							<div class="row">
								<!-- 采购联系人email -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">付款方式
										</span>
										<input  changelog="<s:property value=" purchaseOrder.fkWays "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_fkWays" name="fkWays"
											class="form-control" title="" 
											value="<s:property value=" purchaseOrder.fkWays "/>" />
									</div>
								</div>
								<!-- 付款联系人 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">付款联系人
										</span>
										<input type="hidden" changelog="<s:property value=" purchaseOrder.fukuanUser.id "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_fukuanUser" name="fukuanUser-id"
											class="form-control" title=""
											value="<s:property value=" purchaseOrder.fukuanUser.id "/>" /> 
										<input  changelog="<s:property value=" purchaseOrder.fukuanUser.name "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_fukuanUser_name" name="fukuanUser-name"
											class="form-control" title="" readonly="readonly"
											value="<s:property value=" purchaseOrder.fukuanUser.name "/>" />
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
											onClick="selectPayer()">
											<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>
								<!-- 采购联系人电话 -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">付款联系人电话
										</span>
										<input  changelog="<s:property value=" purchaseOrder.fukuanUser.mobile "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_fukuanUser_mobile" name="fukuanUser-mobile"
											class="form-control" title="" 
											value="<s:property value=" purchaseOrder.fukuanUser.mobile "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<!-- 采购联系人email -->
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon">付款联系人email
										</span>
										<input  changelog="<s:property value=" purchaseOrder.fukuanUser.email "/>" type="text" size="20" maxlength="25"
											id="purchaseOrder_fukuanUser_email" name="fukuanUser-email"
											class="form-control" title="" 
											value="<s:property value=" purchaseOrder.fukuanUser.email "/>" />
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- 物资采购明细 -->
				<div id="goods" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;display: none;">
					<h3><fmt:message key="biolims.common.materialPurchaseDetail"/></h3>
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="goodsTable" style="font-size: 14px;"></table>
				</div>
				<!-- 设备采购明细 -->
				<div id="instrument" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;display: none;">
					<h3><fmt:message key="biolims.common.instrumentPurchaseDetail"/></h3>
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="instrumentTable" style="font-size: 14px;"></table>
				</div>
				<!-- 服务采购明细 -->
				<div id="service" class="box-footer ipadmini" style="padding-top: 5px; min-height: 320px;display: none;">
					<h3><fmt:message key="biolims.common.servicePurchaseDetail"/></h3>
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="serviceTable" style="font-size: 14px;"></table>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/order/editPurchaseOrder.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/order/showPurchaseOrderEquipmentItemList.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/order/showPurchaseOrderGoodsItemList.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/purchase/order/showPurchaseOrderServiceItemList.js"></script>
</body>
</html>