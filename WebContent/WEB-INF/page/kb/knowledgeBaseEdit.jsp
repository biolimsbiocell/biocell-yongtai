<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
</s:if>
<body>
				<g:LayOutWinTag buttonId="showstate" title="选择状态"
				hasHtmlFrame="true"
				html="${ctx}/dic/state/stateSelect.action?type=commonState"
				isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
				documentId="knowledgeBase_state"
				documentName="knowledgeBase_state_name" />
							<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="knowledgeBase_createUser"
				documentName="knowledgeBase_createUser_name"
			 />
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/kb/knowledgeBaseEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
               	 	
				</tr>
				<tr>
               	 	<td class="label-title">编码</td>
                   	<td>
                   	<input type="text" size="20" maxlength="36" id="knowledgeBase_id" name="knowledgeBase.id" title="编码" 
                   		 	class="text input readonlytrue" readonly="readOnly"
                   		 value="<s:property value="knowledgeBase.id"/>" 
                   	 />
                   	</td>
                   	<td class="label-title">突变基因</td>
                   	<td >
                   	<input type="text"  size="20" id="knowledgeBase_mutationGenes" name="knowledgeBase.mutationGenes"  title="突变基因" 
                   	class="text input" value="<s:property value="knowledgeBase.mutationGenes"/>">
                   	</td>
                   	
                   	<td class="label-title">其他名称</td>
                   	<td >
                   	<input type="text"  size="20" id="knowledgeBase_otherName" name="knowledgeBase.otherName"  title="其他名称" 
                   	class="text input" value="<s:property value="knowledgeBase.otherName"/>">
                   	</td>
                   <%-- 	<input type="hidden" id="knowledgeBase_title" name="knowledgeBase.title"  value="<s:property value="knowledgeBase.title"/>" > --%>
               	 	
               	 	<!-- <td class="label-title">是否原创</td>
                   	<td>
					<select id="knowledgeBase_original" name="knowledgeBase.original" class="input-10-length">
								<option value="1" <s:if test="knowledgeBase.original==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="knowledgeBase.original==0">selected="selected"</s:if>>否</option>
					</select> 
 					
                   	</td> -->
                   	
				</tr>
				<tr>
					<td class="label-title">选择类别</td>
	                   	<td>
	 						<input type="text" size="15" readonly="readOnly"  id="knowledgeBase_type_name"  value="<s:property value="knowledgeBase.type.name"/>" class="text input"/>
	 						<input type="hidden" id="knowledgeBase_type" name="knowledgeBase.type.id"  value="<s:property value="knowledgeBase.type.id"/>" > 
	 						<img alt='选择类别' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
	                   	</td>
	                   	
					<td class="label-title">基因名称</td>
                   	<td>
                   	<input type="text" size="15" maxlength="50" id="knowledgeBase_antistop" name="knowledgeBase.antistop" title="关键词" 
                   		 	class="text input"
                   		 value="<s:property value="knowledgeBase.antistop"/>" 
                   	 />
                   	</td>
               	 	<td class="label-title">不公开</td>
                   	<td>
					
					<select id="knowledgeBase_noopen" name="knowledgeBase.noopen" class="input-10-length">
								<option value="1" <s:if test="knowledgeBase.noopen==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="knowledgeBase.noopen==0">selected="selected"</s:if>>否</option>
					</select>
 					<script>		
 					$(function() {

							 new Ext.form.ComboBox({
								transform : "knowledgeBase_noopen",
								width : 80,
								hiddenId : "knowledgeBase_noopen",
								hiddenName : "knowledgeBase.noopen"
							});
					});
 					</script>
                   	</td>
                   
				</tr>
			
				<tr>
					<td class="label-title">状态</td>
                   	<td>
 						<input type="text" size="15" readonly="readOnly"  id="knowledgeBase_state_name"  value="<s:property value="knowledgeBase.state.name"/>" class="text input"/>
 						<input type="hidden" id="knowledgeBase_state" name="knowledgeBase.state.id"  value="<s:property value="knowledgeBase.state.id"/>" > 
 						<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td>
                   	
               	 	 <td class="label-title">基因相关附件</td>
					<td>
					<input type="button"
						onclick="uploadFile1()" value="上传"> <input type="button"
						onclick="downFile1()" value="查看">
						<s:hidden	id="knowledgeBase_path" name="knowledgeBase.path"></s:hidden>
					</td>
               	 	
				
               	 	<td class="label-title">创建人</td>
                   	<td>
 						<input type="text" size="10" readonly="readOnly" class="text input readonlytrue" id="knowledgeBase_createUser_name"  value="<s:property value="knowledgeBase.createUser.name"/>" class="text input"/>
 						<input type="hidden" id="knowledgeBase_createUser" name="knowledgeBase.createUser.id"  value="<s:property value="knowledgeBase.createUser.id"/>" > 
 						<%-- <img alt='选择创建人' id='showcreateUser'  src='${ctx}/images/img_lookup.gif' 	class='detail' /> --%>                   		
                   	</td>
               	 	
				</tr>	
				<tr>
					<td class="label-title">创建日期</td>
                   	<td>
                   	<input type="text" size="12" maxlength="50" id="knowledgeBase_createDate" name="knowledgeBase.createDate" title="创建日期" 
                   		  class="text input readonlytrue" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"    
                   		value="<s:date name="knowledgeBase.createDate" format="yyyy-MM-dd"/>"
                   	 />
                   	</td>
				</tr>
				
				<tbody>
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " colspan="9" width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"> 突变特点信息</span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
				
				<tr>
				    <td class="label-title">生物学特定功能</td>
                   	<td colspan='5'>
                   	<textarea class="text input  false" id="knowledgeBase_biologySpecific" name="knowledgeBase.biologySpecific" 
                   	
                   	tabindex="0" title="生物学特定功能" onblur="textbox_ondeactivate(event);" 
                   	
                   	onfocus="shared_onfocus(event,this)" style="overflow: hidden; width: 500px; height: 100px;"><s:property value="knowledgeBase.biologySpecific"/></textarea></td>
				</tr>
				<tr>
					<td class="label-title">常见突变与癌症</td>
                   	<td colspan='5'>
                   	<textarea class="text input  false" id="knowledgeBase_commonMutaionCancer" name="knowledgeBase.commonMutaionCancer" 
                   	
                   	tabindex="0" title="常见突变与癌症" onblur="textbox_ondeactivate(event);"
                   	
                   	onfocus="shared_onfocus(event,this)" style="overflow: hidden;width: 500px; height: 100px;"><s:property value="knowledgeBase.commonMutaionCancer"/></textarea>
                   	</td>
					
				</tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="knowledgeBase.id"/>" />
            </form>
        	</div>
	</body>
	</html>
