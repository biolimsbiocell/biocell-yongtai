<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=knowledgeZSK&id=${knowledgeZSK.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/kb/zsk/knowledgeZSKEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeZSK_id"
                   	 name="knowledgeZSK.id" title="检测申请号" 
                   	 readonly = "readOnly" class="text input readonlytrue"
			value="<s:property value="knowledgeZSK.id"/>"
                   	  />
                   	  
                   	</td>
			
			<td class="label-title" >基因名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeZSK_geneName"
                   	 name="knowledgeZSK.geneName" title="基因名称"
                   	   
				value="<s:property value="knowledgeZSK.geneName"/>"
                   	  />
                   	  
                   	</td>
                   	<td class="label-title" >突变类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeZSK_mutationType"
                   	 name="knowledgeZSK.mutationType" title="突变类型"
                   	   
				value="<s:property value="knowledgeZSK.mutationType"/>"
                   	  />
                   	  
                   	</td>
               	 	<%-- <td class="label-title" >病案号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeZSK_patientId"
                   	 name="knowledgeZSK.patientId" title="病案号"
                   	   
	value="<s:property value="knowledgeZSK.patientId"/>"
                   	  />
                   	  
                   	</td> --%>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择上传人"
				hasHtmlFrame="true"
				html="${ctx}/kb/zsk/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledgeZSK_createUser').value=rec.get('id');
				document.getElementById('knowledgeZSK_createUser_name').value=rec.get('name');" />
				
			
			
               	 	
			</tr>
			<tr>
			
			<td class="label-title" >上传人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="knowledgeZSK_createUser_name"  value="<s:property value="knowledgeZSK.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="knowledgeZSK_createUser" name="knowledgeZSK.createUser.id"  value="<s:property value="knowledgeZSK.createUser.id"/>" > 
 						<%-- <img alt='选择上传人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
               	 	<td class="label-title" >上传时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeZSK_createDate"
                   	 name="knowledgeZSK.createDate" title="上传时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="knowledgeZSK.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showsampleType" title="选择样本类型"
				hasHtmlFrame="true"
				html="${ctx}/kb/zsk/dicSampleTypeSelect.action"
				isHasSubmit="false" functionName="DicSampleTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledgeZSK_sampleType').value=rec.get('id');
				document.getElementById('knowledgeZSK_sampleType_name').value=rec.get('name');" />
				
			
			
               	 	<%-- <td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="knowledgeZSK_sampleType_name"  value="<s:property value="knowledgeZSK.sampleType.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="knowledgeZSK_sampleType" name="knowledgeZSK.sampleType.id"  value="<s:property value="knowledgeZSK.sampleType.id"/>" > 
 						<img alt='选择样本类型' id='showsampleType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			
			
			<g:LayOutWinTag buttonId="showtemplate" title="选择检测项目"
				hasHtmlFrame="true"
				html="${ctx}/kb/zsk/sysProductSelect.action"
				isHasSubmit="false" functionName="SysProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledgeZSK_product').value=rec.get('id');
				document.getElementById('knowledgeZSK_product_name').value=rec.get('name');" />
				
			
			
               	 	<%-- <td class="label-title" >检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="knowledgeZSK_product_name"  value="<s:property value="knowledgeZSK.product.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="knowledgeZSK_product" name="knowledgeZSK.product.id"  value="<s:property value="knowledgeZSK.product.id"/>" > 
 						<img alt='选择检测项目' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			</tr>
			<tr>
			
			
               	 	<%-- <td class="label-title" >合作实验室</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeZSK_joinlab"
                   	 name="knowledgeZSK.joinlab" title="合作实验室"
                   	   
	value="<s:property value="knowledgeZSK.joinlab"/>"
                   	  />
                   	  
                   	</td> --%>
			</tr>
			<%-- <tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr> --%>
			
			
            </table>
            <input type="hidden" name="knowledgeJDZSKResultJson" id="knowledgeJDZSKResultJson" value="" />
            <input type="hidden" name="knowledgeBXZLZSKJson" id="knowledgeBXZLZSKJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="knowledgeZSK.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#knowledgeJDZSKResultpage">解读结果</a></li>
			<!-- <li><a href="#knowledgeBXZLZSKpage">靶向治疗药物</a></li> -->
           	</ul> 
			<div id="knowledgeJDZSKResultpage" width="100%" height:10px></div>
			<!-- <div id="knowledgeBXZLZSKpage" width="100%" height:10px></div> -->
			</div>
        	</div>
	</body>
	</html>
