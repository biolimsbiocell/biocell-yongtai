<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=knowledgeBXZSK&id=${knowledgeBXZSK.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/kb/zsk/knowledgeBXZSKEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeBXZSK_id"
                   	 name="knowledgeBXZSK.id" title="检测申请号" 
                   	 readonly = "readOnly" class="text input readonlytrue"
			value="<s:property value="knowledgeBXZSK.id"/>"
                   	  />
                   	  
                   	</td>
			
			<td class="label-title" >药物名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeBXZSK_drugName"
                   	 name="knowledgeBXZSK.drugName" title="药物名称"
                   	   
				value="<s:property value="knowledgeBXZSK.drugName"/>"
                   	  />
                   	  
                   	</td>
                   	<td class="label-title" >商品名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeBXZSK_goodName"
                   	 name="knowledgeBXZSK.goodName" title="商品名称"
                   	   
				value="<s:property value="knowledgeBXZSK.goodName"/>"
                   	  />
                   	  
                   	</td>
               	 	<%-- <td class="label-title" >病案号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeBXZSK_patientId"
                   	 name="knowledgeBXZSK.patientId" title="病案号"
                   	   
	value="<s:property value="knowledgeBXZSK.patientId"/>"
                   	  />
                   	  
                   	</td> --%>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择上传人"
				hasHtmlFrame="true"
				html="${ctx}/kb/zsk/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledgeBXZSK_createUser').value=rec.get('id');
				document.getElementById('knowledgeBXZSK_createUser_name').value=rec.get('name');" />
				
			
			
               	 	
			</tr>
			<tr>
			<%-- <td class="label-title" >靶点/原理</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeBXZSK_spot"
                   	 name="knowledgeBXZSK.spot" title="靶点/原理"
                   	   
				value="<s:property value="knowledgeBXZSK.spot"/>"
                   	  />
                   	  
                   	</td> --%>
			<td class="label-title" >上传人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="knowledgeBXZSK_createUser_name"  value="<s:property value="knowledgeBXZSK.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="knowledgeBXZSK_createUser" name="knowledgeBXZSK.createUser.id"  value="<s:property value="knowledgeBXZSK.createUser.id"/>" > 
 						<%-- <img alt='选择上传人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    /> --%>                   		
                   	</td>
               	 	<td class="label-title" >上传时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeBXZSK_createDate"
                   	 name="knowledgeBXZSK.createDate" title="上传时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="knowledgeBXZSK.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showsampleType" title="选择样本类型"
				hasHtmlFrame="true"
				html="${ctx}/kb/zsk/dicSampleTypeSelect.action"
				isHasSubmit="false" functionName="DicSampleTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledgeBXZSK_sampleType').value=rec.get('id');
				document.getElementById('knowledgeBXZSK_sampleType_name').value=rec.get('name');" />
				
			
			
               	 	<%-- <td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="knowledgeBXZSK_sampleType_name"  value="<s:property value="knowledgeBXZSK.sampleType.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="knowledgeBXZSK_sampleType" name="knowledgeBXZSK.sampleType.id"  value="<s:property value="knowledgeBXZSK.sampleType.id"/>" > 
 						<img alt='选择样本类型' id='showsampleType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			
			
			
			<g:LayOutWinTag buttonId="showtemplate" title="选择检测项目"
				hasHtmlFrame="true"
				html="${ctx}/kb/zsk/sysProductSelect.action"
				isHasSubmit="false" functionName="SysProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledgeBXZSK_product').value=rec.get('id');
				document.getElementById('knowledgeBXZSK_product_name').value=rec.get('name');" />
				
			
			
               	 	<%-- <td class="label-title" >检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="knowledgeBXZSK_product_name"  value="<s:property value="knowledgeBXZSK.product.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="knowledgeBXZSK_product" name="knowledgeBXZSK.product.id"  value="<s:property value="knowledgeBXZSK.product.id"/>" > 
 						<img alt='选择检测项目' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			</tr>
			<tr>
			
			
               	 	<%-- <td class="label-title" >合作实验室</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledgeBXZSK_joinlab"
                   	 name="knowledgeBXZSK.joinlab" title="合作实验室"
                   	   
	value="<s:property value="knowledgeBXZSK.joinlab"/>"
                   	  />
                   	  
                   	</td> --%>
			</tr>
			<%-- <tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr> --%>
			
			
            </table>
            <input type="hidden" name="knowledgeBXJDZSKResultJson" id="knowledgeBXJDZSKResultJson" value="" />
            <input type="hidden" name="knowledgeBXBXZLZSKJson" id="knowledgeBXBXZLZSKJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="knowledgeBXZSK.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<!-- <li><a href="#knowledgeBXJDZSKResultpage">解读结果</a></li> -->
			<li><a href="#knowledgeBXBXZLZSKpage">靶向治疗药物</a></li>
           	</ul> 
			<!-- <div id="knowledgeBXJDZSKResultpage" width="100%" height:10px></div> -->
			<div id="knowledgeBXBXZLZSKpage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
