<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=knowledge&id=${knowledge.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/kb/knowle/knowledgeEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledge_id"
                   	 readonly = "readOnly" class="text input readonlytrue"
                   	 name="knowledge.id" title="检测申请号" value="<s:property value="knowledge.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >病案号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledge_patientId"
                   	 name="knowledge.patientId" title="病案号"
                   	   
	value="<s:property value="knowledge.patientId"/>"
                   	  />
                   	  
                   	</td>
			<td class="label-title" >检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<%-- <input type="text" size="20"  id="knowledge_product_name"  value="<s:property value="knowledge.product.name"/>"   />
 						<input type="hidden" id="knowledge_product_id" name="knowledge.product.id"  value="<s:property value="knowledge.product.id"/>" >  --%>
 						<input type="text" size="20" readonly="readonly" id="knowledge_productName" name="knowledge.productName" value="<s:property value="knowledge.productName"/>" class="text input readonlytrue"  />
 						<input type="hidden"  id="knowledge_productId" name="knowledge.productId"  value="<s:property value="knowledge.productId"/>" >
                   	</td>
               	 	
                   	
			</tr>
			<tr>
			<%-- <td class="label-title" >合作实验室</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledge_joinlab"
                   	 name="knowledge.joinlab" title="合作实验室"
                   	   
	value="<s:property value="knowledge.joinlab"/>"
                   	  /> --%>
			
               	 	
			
               	 	<td class="label-title" >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" id="knowledge_sampleCode" name="knowledge.sampleCode"  value="<s:property value="knowledge.sampleCode"/>" > 
                   	</td>
						
			<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readonly" id="knowledge_sampleType_name"  value="<s:property value="knowledge.sampleType.name"/>"  class="text input readonlytrue" />
 						<input type="hidden" id="knowledge_sampleType_id" name="knowledge.sampleType.id"  value="<s:property value="knowledge.sampleType.id"/>" > 
                   	</td>
               	 	
			</tr>
			<tr>
			
			<td class="label-title" >上传人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="knowledge_createUser_name"  value="<s:property value="knowledge.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="knowledge_createUser" name="knowledge.createUser.id"  value="<s:property value="knowledge.createUser.id"/>" > 
 						<%-- <img alt='选择上传人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />    --%>                		
                   	</td>
               	 	<td class="label-title" >上传时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="knowledge_createDate"
                   	 name="knowledge.createDate" title="上传时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="knowledge.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
                   	  
                   	<td class="label-title">上传结果</td>
                   	<td></td>
                   	<td >
                   	<input type="button"
						onclick="uploadFile1()" value="上传结果"> <input type="button"
						onclick="downFile1()" value="查看">
						<s:hidden	id="knowledge_path" name="knowledge.path"></s:hidden>
                   	</td>
			</tr>
			
			<%-- <tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr> --%>
			
			
            </table>
            <input type="hidden" name="knowledgeJYTBResultJson" id="knowledgeJYTBResultJson" value="" />
            <input type="hidden" name="knowledgeJYCPResultJson" id="knowledgeJYCPResultJson" value="" />
            <input type="hidden" name="knowledgeKBSJResultJson" id="knowledgeKBSJResultJson" value="" />
            <input type="hidden" name="knowledgeJDResultJson" id="knowledgeJDResultJson" value="" />
            <input type="hidden" name="knowledgeBXZLJson" id="knowledgeBXZLJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="knowledge.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#knowledgeJYTBResultpage">基因突变分析结果</a></li>
			<li><a href="#knowledgeJYCPResultpage">基因重排分析结果</a></li>
			<li><a href="#knowledgeKBSJResultpage">拷贝数据分析结果</a></li>
			<li><a href="#knowledgeJDResultpage">解读结果</a></li>
			<li><a href="#knowledgeBXZLpage">靶向治疗药物</a></li>
           	</ul> 
			<div id="knowledgeJYTBResultpage" width="100%" height:10px></div>
			<div id="knowledgeJYCPResultpage" width="100%" height:10px></div>
			<div id="knowledgeKBSJResultpage" width="100%" height:10px></div>
			<div id="knowledgeJDResultpage" width="100%" height:10px></div>
			<div id="knowledgeBXZLpage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
