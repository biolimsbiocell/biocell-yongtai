﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/kb/knowle/knowledge.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >检测申请号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="knowledge_id"
                   	 name="id" searchField="true" title="检测申请号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >病案号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="knowledge_patientId"
                   	 name="patientId" searchField="true" title="病案号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择上传人"
				hasHtmlFrame="true"
				html="${ctx}/kb/knowle/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledge_createUser').value=rec.get('id');
				document.getElementById('knowledge_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >上传人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="knowledge_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="knowledge_createUser" name="knowledge.createUser.id"  value="" > 
 						<img alt='选择上传人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >上传时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showsampleType" title="选择样本类型"
				hasHtmlFrame="true"
				html="${ctx}/kb/knowle/dicSampleTypeSelect.action"
				isHasSubmit="false" functionName="DicSampleTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledge_sampleType').value=rec.get('id');
				document.getElementById('knowledge_sampleType_name').value=rec.get('name');" />
               	 	<td class="label-title" >样本类型</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="knowledge_sampleType_name" searchField="true"  name="sampleType.name"  value="" class="text input" />
 						<input type="hidden" id="knowledge_sampleType" name="knowledge.sampleType.id"  value="" > 
 						<img alt='选择样本类型' id='showsampleType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showtemplate" title="选择检测项目"
				hasHtmlFrame="true"
				html="${ctx}/kb/knowle/sysProductSelect.action"
				isHasSubmit="false" functionName="SysProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('knowledge_template').value=rec.get('id');
				document.getElementById('knowledge_template_name').value=rec.get('name');" />
               	 	<td class="label-title" >检测项目</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="knowledge_template_name" searchField="true"  name="template.name"  value="" class="text input" />
 						<input type="hidden" id="knowledge_template" name="knowledge.template.id"  value="" > 
 						<img alt='选择检测项目' id='showtemplate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
		<!-- 	<tr>
               	 	<td class="label-title" >合作实验室</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="knowledge_joinlab"
                   	 name="joinlab" searchField="true" title="合作实验室"    />
                   	</td>
			</tr> -->
            </table>
		</form>
		</div>
		<div id="show_knowledge_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_knowledge_tree_page"></div>
</body>
</html>



