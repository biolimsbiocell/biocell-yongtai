﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/kb/knowle/knowledgeDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">检测申请号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="knowledge_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">病案号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="knowledge_patientId" searchField="true" name="patientId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">上传人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="knowledge_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">上传时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="knowledge_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">样本类型</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="knowledge_sampleType" searchField="true" name="sampleType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">检测项目</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="knowledge_template" searchField="true" name="template"  class="input-20-length"></input>
               	</td>
           	 	<!-- <td class="label-title">合作实验室</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="knowledge_joinlab" searchField="true" name="joinlab"  class="input-20-length"></input>
               	</td> -->
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_knowledge_div"></div>
   		
</body>
</html>



