﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/kb/knowledgeBase.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table>
			<tr>
               		<td class="label-title">编码</td>
                   	<td align="left">
                   	<input type="text" size="20" maxlength="36" searchField="true" id="knowledgeBase_id" name="id" title="编码" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
                   	
                   	<td class="label-title">突变基因</td>
                   	<td align="left">
                   	<input type="text" maxlength="30" id="knowledgeBase_mutationgenes" searchField="true" name="mutationgenes"  class="input-20-length"></input>
                   	</td> 
                   	
                   	<td class="label-title">其他名称</td>
                   	<td align="left">
                   	<input type="text" maxlength="30" id="knowledgeBase_othername" searchField="true" name="othername"  class="input-20-length"></input>
                   	</td> 
                   	
                   	 <td class="label-title">生物学特定功能</td>
                   	<td align="left">
                   	<input type="text" maxlength="2000" id="knowledgeBase_biologyspecific" searchField="true" name="biologyspecific"  class="input-20-length"></input>
                   	</td> 
                   	
                   	
               	  <!--  <td class="label-title">是否原创</td>
                   	<td align="left">
					
					<select id="knowledgeBase_original" searchField="true" name="original" class="input-10-length">
								<option value="1" <s:if test="knowledgeBase.original==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="knowledgeBase.original==0">selected="selected"</s:if>>否</option>
					</select> 
 					<script>		
 					
 					</script>
                   	</td> -->
			</tr>
			<tr>
				<td class="label-title">常见突变与癌症</td>
                   	<td align="left">
                   	<input type="text" maxlength="2000" id="knowledgeBase_commonMutationCancer" searchField="true" name="commonMutationCancer"  class="input-20-length"></input>
                   	</td> 
                   	
                   	
				<g:LayOutWinTag buttonId="showtype" title="选择选择类别"
					hasHtmlFrame="true" html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="showtypeFun" hasSetFun="true"
					documentId="knowledgeBase_type"
					documentName="knowledgeBase_type_name"
				/>
	               		<td class="label-title">选择类别</td>
	                   	<td align="left">
	 						<input type="text" size="15" readonly="readOnly"  id="knowledgeBase_type_name"  value="" class="text input"/>
	 						<input type="hidden" id="knowledgeBase_type" name="type.id"  value="" searchField="true" > 
	 						<img alt='选择选择类别' id='showtype' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
	                   	</td>
					
				 <td class="label-title">导读</td>
                   	<td align="left">
                   	<input type="text" maxlength="2000" id="knowledgeBase_daodu" searchField="true" name="daodu"  class="input-20-length"></input>
                   	</td> 
                   	
               		
			</tr>
			<tr>
					
                   	
               		<td class="label-title">关键词</td>
                   	<td align="left">
                   	<input type="text" size="15" maxlength="50" searchField="true" id="knowledgeBase_antistop" name="antistop" title="关键词" value="" 
                   		 	class="text input"
                   	 />
                   	</td>
                   	<g:LayOutWinTag buttonId="showstate" title="选择状态"
					hasHtmlFrame="true"
					html="${ctx}/dic/state/stateSelect.action?type=commonState"
					isHasSubmit="false" functionName="showstateFun" hasSetFun="true"
					documentId="knowledgeBase_state"
					documentName="knowledgeBase_state_name" />
	               		<td class="label-title">状态</td>
	                   	<td align="left">
	 						<input type="text" size="15" readonly="readOnly"  id="knowledgeBase_state_name"  value="" class="text input"/>
	 						<input type="hidden" id="knowledgeBase_state" name="state.id"  value="" searchField="true" > 
	 						<img alt='选择状态' id='showstate' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
	                   	</td>
				<td class="label-title">不公开</td>
                   	<td align="left">
					
					<select id="knowledgeBase_noopen" searchField="true" name="noopen" class="input-10-length">
								<option value="1" <s:if test="knowledgeBase.noopen==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="knowledgeBase.noopen==0">selected="selected"</s:if>>否</option>
					</select>
					</td>
			
                   	
			</tr>
			
				
               		
				
				<%-- <tr>
				<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
					hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false"		functionName="showcreateUserFun" 
					documentId="knowledgeBase_createUser"
					documentName="knowledgeBase_createUser_name"
				 />
	               		<td class="label-title">创建人</td>
	                   	<td align="left">
	 						<input type="text" size="10" readonly="readOnly"  id="knowledgeBase_createUser_name"  value="" class="text input"/>
	 						<input type="hidden" id="knowledgeBase_createUser" name="createUser.id"  value="" searchField="true" > 
	 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
	                   	</td>
	               		<td class="label-title">创建日期</td>
	                   	<td align="left">
	                   	<input type="text" size="12" maxlength="50" searchField="true" id="knowledgeBase_createDate" name="createDate" title="创建日期" value="" 
	                    	Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
	                   	 />
	                   	</td>
				</tr> --%>
        </table>
		</form>
		</div>
		<div id="show_knowledgeBase_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
</body>
</html>



