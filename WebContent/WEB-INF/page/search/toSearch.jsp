<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<head>
<script type="text/javascript" src="${ctx}/javascript/lib/json2.js"></script>
<script type="text/javascript" src="${ctx}/javascript/search/toSearch.js"></script>
</head>
<body>
	<div>
		<br>
		<div>
			条件名称：<input type="text" id="search_name">&nbsp;&nbsp;&nbsp;&nbsp;<span id="save_search_info">保存条件</span>
		</div>
		<ul class="removeable-items">
			<s:iterator value="%{#request.searchList}" id="search"> 
			<li searchId='<s:property value="#search.id"/>' searchInfo='<s:property value="#search.searchInfo"/>' allInfo='<s:property value="#search.allInfo"/>'  style="cursor: pointer;">
				<span class="search-name"><s:property value="#search.name"/></span>
				<span class='ui-icon ui-icon-close'>删除条件</span>
			</li>
			</s:iterator>
		</ul>
	</div>
</body>
</html>