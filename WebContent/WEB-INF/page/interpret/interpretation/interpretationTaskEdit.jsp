﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=interpretationTask&id=${interpretationTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/interpret/interpretation/interpretationTaskEdit.js"></script>
<div id="interpretationTemppage" style="float:left;width:25%"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left" >
                   	<input type="text" size="20" maxlength="25" id="interpretationTask_id"
                   	 	name="interpretationTask.id" title="编号"
						value="<s:property value="interpretationTask.id"/>"
                   	   
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="interpretationTask_name"
                   	 name="interpretationTask.name" title="描述"
                   	   
	value="<s:property value="interpretationTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/interpret/interpretation/interpretationTask/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('interpretationTask_createUser').value=rec.get('id');
				document.getElementById('interpretationTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="interpretationTask_createUser_name"  value="<s:property value="interpretationTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="interpretationTask_createUser" name="interpretationTask.createUser.id"  value="<s:property value="interpretationTask.createUser.id"/>" > 
<%--  						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
 --%>                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >下达时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="interpretationTask_createDate"
                   	 name="interpretationTask.createDate" title="下达时间"  class="text input readonlytrue" readonly="readOnly"
                   	   	   Class="Wdate" 
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
                   	  value="<s:date name="interpretationTask.createDate" format="yyyy-MM-dd"/>"
                   	  
                   	                      	  />
                   	  
                   	</td>
                   	
			<g:LayOutWinTag buttonId="showacceptUser" title="选择解读员"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="acceptUserFun" 
 				hasSetFun="true"
 				documentId="interpretationTask_acceptUser"
 				documentName="interpretationTask_acceptUser_name"/>
					
               	 	<td class="label-title" >解读员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="interpretationTask_acceptUser_name"  value="<s:property value="interpretationTask.acceptUser.name"/>"/>
 						<input type="hidden" id="interpretationTask_acceptUser" name="interpretationTask.acceptUser.id"  value="<s:property value="interpretationTask.acceptUser.id"/>" > 
 						<img alt='选择解读员' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
		
			
               	 	<td class="label-title" >解读时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="interpretationTask_acceptDate"
                   	 name="interpretationTask.acceptDate" title="解读时间"
                   	 	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
                   	   
                   	  value="<s:date name="interpretationTask.acceptDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
<%-- 			<g:LayOutWinTag buttonId="showanalysisTask" title="选择分析方案"
				hasHtmlFrame="true"
				html="${ctx}/analysis/analy/analysisTask/analysisTaskSelect.action"
				isHasSubmit="false" functionName="AnalysisTaskFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('interpretationTask_analysisTask').value=rec.get('id');
				document.getElementById('interpretationTask_analysisTask_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >分析方案</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="interpretationTask_analysisTask_name"  value="<s:property value="interpretationTask.analysisTask.name"/>"/>
 						<input type="hidden" id="interpretationTask_analysisTask" name="interpretationTask.analysisTask.id"  value="<s:property value="interpretationTask.analysisTask.id"/>" > 
						<img alt='选择分析方案' id='showanalysisTask' src='${ctx}/images/img_lookup.gif' 	class='detail' />                   		
                   	</td> --%>
               	 	<%-- <td class="label-title" >分析方案</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="interpretationTask_analysisCase"
                   	 name="interpretationTask.analysisCase" title="分析方案"
                   	   
	value="<s:property value="interpretationTask.analysisCase"/>" --%>
                   	 
			
			
               	 	<td class="label-title"   style="display:none"  >工作流id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"   style="display:none"  ></td>            	 	
                   	<td align="left"  style="display:none">
                   	<input type="text" size="20" maxlength="25" id="interpretationTask_state"
                   	 name="interpretationTask.state" title="工作流id"
                   	 style="display:none"  
	value="<s:property value="interpretationTask.state"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="interpretationTask_stateName"
                   	 name="interpretationTask.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="interpretationTask.stateName"/>"
                   	   
                   	  />
                   	  
                   	</td>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="interpretationItemJson" id="interpretationItemJson" value="" />
            <input type="hidden" name="interpretationCourseJson" id="interpretationCourseJson" value="" />
            <input type="hidden" name="interpretationBackJson" id="interpretationBackJson" value="" />
            <input type="hidden" name="interpretationInfoJson" id="interpretationInfoJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="interpretationTask.id"/>" />
            </form>
			<!-- <div id="tabs">
            <ul>
			<li><a href="#interpretationItempage">样本明细</a></li>
			<li><a href="#interpretationCoursepage">解读过程</a></li>
			<li><a href="#interpretationBackpage">反馈作图</a></li>
			<li><a href="#interpretationInfopage">解读结果</a></li>
           	</ul>  -->
			<div id="interpretationItempage" width="100%" height:10px></div>
			<div id="interpretationCoursepage" width="100%" height:10px></div>
			<!-- <div id="interpretationBackpage" width="100%" height:10px></div> -->
			<!-- <div id="interpretationInfopage" width="100%" height:10px></div> -->
			</div>
        	<!-- </div> -->
	</body>
	</html>
