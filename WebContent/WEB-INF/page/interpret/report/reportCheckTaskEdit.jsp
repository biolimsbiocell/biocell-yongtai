﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=reportCheckTask&id=${reportCheckTask.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/interpret/report/reportCheckTaskEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"   ></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="reportCheckTask_id"
                   	 name="reportCheckTask.id" title="编号"
						value="<s:property value="reportCheckTask.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportCheckTask_name"
                   	 name="reportCheckTask.name" title="描述"
                   	   
	value="<s:property value="reportCheckTask.name"/>"
                   	  />
                   	  
                   	</td>
			
			
                   	
                   	<g:LayOutWinTag buttonId="showcreateUser" title="选择下达人"
				hasHtmlFrame="true"
				html="${ctx}/interpret/report/reportCheckTask/userSelect.action"
				
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('reportCheckTask_createUser').value=rec.get('id');
				document.getElementById('reportCheckTask_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >下达人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="reportCheckTask_createUser_name" value="<s:property value="reportCheckTask.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="reportCheckTask_createUser" name="reportCheckTask.createUser.id"  value="<s:property value="reportCheckTask.createUser.id"/>" > 
 						<img alt='选择下达人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >下达日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportCheckTask_createDate"
                   	 name="reportCheckTask.createDate" title="下达日期"
                   	   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
                   	  value="<s:date name="reportCheckTask.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
                   		<g:LayOutWinTag buttonId="showacceptUser" title="选择报告员"
				hasHtmlFrame="true"
				html="${ctx}/interpret/report/reportCheckTask/userSelect.action"
				
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('reportCheckTask_acceptUser').value=rec.get('id');
				document.getElementById('reportCheckTask_acceptUser_name').value=rec.get('name');" />
			
               	 	<td class="label-title" >报告员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="reportCheckTask_acceptUser_name"  value="<s:property value="reportCheckTask.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="reportCheckTask_acceptUser" name="reportCheckTask.acceptUser.id"  value="<s:property value="reportCheckTask.acceptUser.id"/>" > 
 						<img alt='选择报告员' id='showacceptUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title" >报告生成日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportCheckTask_acceptDate"
                   	 name="reportCheckTask.acceptDate" title="报告生成日期"
                   	 	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"
                   	   
                   	  value="<s:date name="reportCheckTask.acceptDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >工作流id</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="reportCheckTask_state"
                   	 name="reportCheckTask.state" title="工作流id"
                   	   
	value="<s:property value="reportCheckTask.state"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="reportCheckTask_stateName"
                   	 name="reportCheckTask.stateName" title="工作流状态"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="reportCheckTask.stateName"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="reportCheckItemJson" id="reportCheckItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="reportCheckTask.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#reportCheckItempage">报告明细</a></li>
           	</ul> 
			<div id="reportCheckItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
