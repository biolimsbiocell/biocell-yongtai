﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/exp/cell/cellLifeDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="cellLife_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">名称</td>
               	<td align="left">
                    		<input type="text" maxlength="60" id="cellLife_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">跟踪人员</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="cellLife_followUser" searchField="true" name="followUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">跟踪日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="cellLife_followDate" searchField="true" name="followDate"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">跟踪结果</td>
               	<td align="left">
                    		<input type="text" maxlength="15" id="cellLife_followResult" searchField="true" name="followResult"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="cellLife_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建日期</td>
               	<td align="left">
                    		<input type="text" maxlength="" id="cellLife_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">成单可能性</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="cellLife_orderChance" searchField="true" name="orderChance"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">结果说明</td>
               	<td align="left">
                    		<input type="text" maxlength="250" id="cellLife_followExplain" searchField="true" name="followExplain"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_cellLife_div"></div>
   		
</body>
</html>



