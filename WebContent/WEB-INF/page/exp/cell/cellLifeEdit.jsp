<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=cellLife&id=${cellLife.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/exp/cell/cellLifeEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" >编码</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="18" id="cellLife_id"
                   	 name="cellLife.id" title="编码"
					 value="<s:property value="cellLife.id"/>"
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" >名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="60" id="cellLife_name"
                   	 name="cellLife.name" title="名称"
					 value="<s:property value="cellLife.name"/>"
                   	  />
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showfollowUser" title="选择跟踪人员"
				hasHtmlFrame="true"
				html="${ctx}/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('cellLife_followUser').value=rec.get('id');
				document.getElementById('cellLife_followUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >跟踪人员</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="cellLife_followUser_name"  value="<s:property value="cellLife.followUser.name"/>" class="text input" />
 						<input type="hidden" id="cellLife_followUser" name="cellLife.followUser.id"  value="<s:property value="cellLife.followUser.id"/>" > 
 						<img alt='选择跟踪人员' id='showfollowUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >跟踪日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="15" maxlength="" id="cellLife_followDate"
                   	 name="cellLife.followDate" title="跟踪日期"
                   	   Class="Wdate"  onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" 
                   	  					 value="<s:date name="cellLife.followDate" format="yyyy-MM-dd HH:mm"/>"
                   	                      	  />
                   	</td>
			
			
               	 	<td class="label-title" >跟踪结果</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="15" id="cellLife_followResult"
                   	 name="cellLife.followResult" title="跟踪结果"
					 value="<s:property value="cellLife.followResult"/>"
                   	  />
                   	</td>
			
			
			
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/exp/cell/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('cellLife_createUser').value=rec.get('id');
				document.getElementById('cellLife_createUser_name').value=rec.get('name');" />
				
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="cellLife_createUser_name"  value="<s:property value="cellLife.createUser.name"/>" class="text input" />
 						<input type="hidden" id="cellLife_createUser" name="cellLife.createUser.id"  value="<s:property value="cellLife.createUser.id"/>" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="15" maxlength="" id="cellLife_createDate"
                   	 name="cellLife.createDate" title="创建日期"
                   	  readonly = "readOnly"  
                   	                      	    class="text input readonlytrue"  
                   	  
                   	  					 value="<s:date name="cellLife.createDate" format="yyyy-MM-dd HH:mm"/>"
                   	                      	  />
                   	</td>
			
			
               	 	<td class="label-title" >成单可能性</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="10" maxlength="5" id="cellLife_orderChance"
                   	 name="cellLife.orderChance" title="成单可能性"
					 value="<s:property value="cellLife.orderChance"/>"
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" >结果说明</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<textarea  id="cellLife_followExplain" name="cellLife.followExplain"  title="结果说明" style="overflow: hidden; width: 350px; height: 140px;" ><s:property value="cellLife.followExplain"/></textarea>
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="cellLifeItemJson" id="cellLifeItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="cellLife.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#cellLifeItempage">细胞活性条件</a></li>
           	</ul> 
			<div id="cellLifeItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
