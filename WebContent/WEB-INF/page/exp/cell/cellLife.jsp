﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/exp/cell/cellLife.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="18" id="cellLife_id"
                   	 name="id" searchField="true" title="编码"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >名称</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="60" id="cellLife_name"
                   	 name="name" searchField="true" title="名称"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showfollowUser" title="选择跟踪人员"
				hasHtmlFrame="true"
				html="${ctx}/exp/cell/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('cellLife_followUser').value=rec.get('id');
				document.getElementById('cellLife_followUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >跟踪人员</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="cellLife_followUser_name" searchField="true"  name="followUser.name"  value="" class="text input" />
 						<input type="hidden" id="cellLife_followUser" name="cellLife.followUser.id"  value="" > 
 						<img alt='选择跟踪人员' id='showfollowUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >跟踪日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startfollowDate" name="startfollowDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="followDate1" name="followDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endfollowDate" name="endfollowDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="followDate2" name="followDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >跟踪结果</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="15" id="cellLife_followResult"
                   	 name="followResult" searchField="true" title="跟踪结果"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/exp/cell/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('cellLife_createUser').value=rec.get('id');
				document.getElementById('cellLife_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="15"   id="cellLife_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="cellLife_createUser" name="cellLife.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >成单可能性</td>
                   	<td align="left"  >
                  
					<input type="text" size="10" maxlength="5" id="cellLife_orderChance"
                   	 name="orderChance" searchField="true" title="成单可能性"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >结果说明</td>
                   	<td align="left"  >
                   	<textarea class="text input  false" id="cellLife_followExplain" name="followExplain" searchField="true"  title="结果说明" style="overflow: hidden; width: 350px; height: 140px;" ><s:property value="cellLife.followExplain"/></textarea>
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_cellLife_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_cellLife_tree_page"></div>
</body>
</html>



