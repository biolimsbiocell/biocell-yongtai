<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<script type="text/javascript">
	function setvalue(rec){
	 <%if(request.getParameter("flag")!=null){%>
	 	window.parent.set<%=request.getParameter("flag")%>(rec);
	 <%}%>
	}
	function view(){
		var record = gridGrid.getSelectionModel().getSelected(); 
		 var id =record.data.id;
		if(trim(id)==''){
			message("请选择一条记录!");
		}else{
			openDialog(window.ctx+'/supplier/toViewSupplier.action?noButton=true&id='+id);  
		}
	}
	function nd(){
		window.open(window.ctx + '/supplier/toEditSupplier.action','','');
	}
</script>
</head>
<body>
	<div class="mainfullclass" id="markup">
		<table cellspacing="0" cellpadding="0" class="toolbarsection">
			<tr>
				<td><span onclick="nd()" ><input type="button" value=<fmt:message key="biolims.common.create"/> size="10"/></span></td>
				<td>
					<span>&nbsp;</span>
					<label class="text label" title= <fmt:message key="biolims.common.inputFindCon"/>>ID</label>
				</td>
				<td>&nbsp;</td>
				<td nowrap class="quicksearch_control" valign="middle">
					<input type="text" class="input_parts text input default  readonlyfalse false" name="id" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" searchField="true" id="id" title=<fmt:message key="biolims.common.inputID"/> style="width: 90">
				</td>
				<td>
					<span>&nbsp;</span>
					<label class="text label" title=<fmt:message key="biolims.common.inputFindCon"/>> <fmt:message key="biolims.common.designation"/></label>
				</td>
				<td>&nbsp;</td>
				<td nowrap class="quicksearch_control" valign="middle">
					<input type="text" class="input_parts text input default  readonlyfalse false" name="name" onblur="textbox_ondeactivate(event);" searchField="true" onfocus="shared_onfocus(event,this)" id="name" title=<fmt:message key="biolims.common.inputName"/> style="width: 90">
				</td>
<!-- 				<td> -->
<!-- 					<span>&nbsp;</span> -->
<!-- 					<label class="text label" title="输入查询条件"> 记录数</label> -->
<!-- 				</td> -->
				<td>&nbsp;</td>
				<td nowrap class="quicksearch_control" valign="middle">
<!-- 					<input type="text" class="input_parts text input default  readonlyfalse false" name="limitNum" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" id="limitNum" title="输入记录数" style="width: 50"> -->
					<img class="quicksearch_findimage" id="quicksearch_findimage" name="quicksearch_findimage" onclick="commonSearchAction(gridGrid)" src="${ctx}/images/quicksearch.gif" alt="搜索">
					<img class="quicksearch_findimage" id="quicksearch_findimage" name="quicksearch_findimage" onclick="view()" src="${ctx}/images/nav_icon_preview.gif" alt="查看选中">
					<img class="quicksearch_findimage" id="quicksearch_findimage" name="quicksearch_findimage" onclick="form_reset()" src="${ctx}/images/no_draw.gif" title="清空">
				</td>
				<td>&nbsp;</td>
			</tr>
		</table>
				<script language = "javascript" >
function gridSetSelectionValue(){
var r = gridGrid.getSelectionModel().getSelected(); 
setvalue(r);
}
var gridGrid;
Ext.onReady(function(){
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'way',type: 'string'},{name:'supplierType-name',type: 'string'},{name:'entKind-name',type: 'string'},{name:'selA-name',type: 'string'},{name:'linkMan',type: 'string'},{name:'linkTel',type: 'string'},{name:'fax',type: 'string'},{name:'email',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: ctx+'/supplier/common/supplierSelectJson.action?id=',method: 'POST'})
});
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
height:document.body.clientHeight,
title:'',
store: store,
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:[new Ext.grid.RowNumberer(),{dataIndex:'id',header: biolims.purchase.supplierId,width: 120,sortable: true},{dataIndex:'name',header: biolims.equipment.supplierName,width: 200,sortable: true},{dataIndex:'way',header: biolims.purchase.industry,width: 120,sortable: true},{dataIndex:'supplierType-name',header: biolims.common.type,width: 150,sortable: true},{dataIndex:'entKind-name',header: biolims.purchase.enterpriseNature,width: 150,sortable: true,hidden: true},{dataIndex:'selA-name',header: biolims.purchase.nation,width: 100,sortable: true},{dataIndex:'linkMan',header: biolims.common.linkmanName,width: 100,sortable: true},{dataIndex:'linkTel',header: biolims.common.linkmanName,width: 100,sortable: true},{dataIndex:'fax',header: biolims.purchase.fax,width: 100,sortable: true},{dataIndex:'email',header: 'Email',width: 100,sortable: true}],
stripeRows: true,
bbar: new Ext.PagingToolbar({
pageSize: parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1),
store: store,
displayInfo: true,
displayMsg:biolims.common.displayMsg,
beforePageText: biolims.common.page,
afterPageText: biolims.common.afterPageText,
emptyMsg: biolims.common.noData,
})
});
gridGrid.render('grid');
gridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
});
gridGrid.on('rowdblclick',function(){gridSetSelectionValue();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1)}});
});
</script >
<input type='hidden'  id='extJsonDataString'  name='extJsonDataString' value=''/>
<div id='grid' ></div>

	</div>

	</div>
</body>

</html>