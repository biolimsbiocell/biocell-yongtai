<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/javascript/customer/main/showCustomerList.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
	<g:LayOutWinTag buttonId="showDicState" title="选择客户性质" hasHtmlFrame="true" html="${ctx}/dic/state/stateSelect.action?type=supplier" isHasSubmit="false" functionName="showDicStateFun" hasSetFun="true" documentId="state_id" documentName="state_name" />
	<div class="mainlistclass" id="markup">
		<div id="jstj" style="display: none">
			<table cellspacing="0" cellpadding="0" class="toolbarsection">
				<tr>
					<td>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> ID</label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="id" onblur="textbox_ondeactivate(event);" searchField="true" onfocus="shared_onfocus(event,this)" id="sid" title="输入ID">
					</td>
					<td>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> 名称</label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="name" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" searchField="true" id="name" title="输入名称">
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> 行业</label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="way" onblur="textbox_ondeactivate(event);" searchField="true" onfocus="shared_onfocus(event,this)" id="way" title="输入行业">
					</td>
					<td>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> 联系人</label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse false" name="linkMan" onblur="textbox_ondeactivate(event);" onfocus="shared_onfocus(event,this)" searchField="true" id="linkMan" title="输入联系人">
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span>&nbsp;</span>
						<label class="text label" title="输入查询条件"> 客户性质</label>
					</td>
					<td>&nbsp;</td>
					<td nowrap class="quicksearch_control" valign="middle">
						<input type="text" class="input_parts text input default  readonlyfalse true" name="" id="state_name" readonly="readonly" size="20">
						<input type="hidden" name="state.id" id="state_id" searchField="true" />
						<img alt='选择' id='showDicState' src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
				</tr>
			</table>
		</div>
		<input type="hidden" id="id" value="" />
		<g:GridTag  isAutoWidth="true" col="${col}" type="${type}" title="${title}" url="${path}" />
	</div>
</body>
</html>