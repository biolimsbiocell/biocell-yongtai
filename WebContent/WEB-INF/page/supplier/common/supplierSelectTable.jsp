<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title>
			<fmt:message key="biolims.common.noTitleDocuments" />
		</title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style>
			.dataTables_scrollBody {
				min-height: 100px;
			}
			
			.tablebtns {
				float: right;
			}
			
			.dt-buttons {
				margin: 0;
				float: right;
			}
			
			.chosed {
				background-color: #5AC8D8 !important;
				color: #fff;
			}
		</style>
	</head>

	<body>
		<input type="hidden" id="flag" value="${requestScope.flag}">
		<div id="supplierSelect">
			<table class="table table-hover table-bordered table-condensed" id="supplierSelectTable" style="font-size: 12px;"></table>
		</div>
		<script type="text/javascript">
			$(function() {
				var colOpts = [];
				colOpts.push({
					"data": "id",
					"title": biolims.common.id,
				});
				colOpts.push({
					"data": "name",
					"title": biolims.user.eduName1,
				});
				var tbarOpts = [];
				var addDicTypeOptions = table(false, null,
					'/supplier/common/supplierSelectTableJson.action?flag=' + $("#flag").val(),
					colOpts, tbarOpts)
				var supplierSelectTable = renderData($("#supplierSelectTable"), addDicTypeOptions);
				$("#supplierSelectTable").on(
					'init.dt',
					function(e, settings) {
						// 清除操作按钮
						$("#supplierSelectTable_wrapper .dt-buttons").empty();
						$('#supplierSelectTable_wrapper').css({
							"padding": "0 16px"
						});
						var trs = $("#supplierSelectTable tbody tr");
						supplierSelectTable.ajax.reload();
						supplierSelectTable.on('draw', function() {
							trs = $("#supplierSelectTable tbody tr");
							trs.click(function() {
								$(this).addClass("chosed").siblings("tr")
									.removeClass("chosed");
							});
						});
						trs.click(function() {
							$(this).addClass("chosed").siblings("tr")
								.removeClass("chosed");
						});
					});
			})
		</script>
	</body>

</html>