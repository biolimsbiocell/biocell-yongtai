<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.input-group {
	margin-top: 10px;
}
</style>
</head>
<script>
	$(function() {
		$("#scope_id_div").attr("style", "display:none");
	});
</script>

<body>
	<!--toolbar按钮组-->
	<div>
				<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div class="container-fluid" style="margin-top: 56px">
		<!--form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.common.supplierInformation" />
						<!-- 供应商信息 -->
					</h3>
					<%-- <small style="color: #fff;"> 创建人: <span><s:property
								value=" supplier.createUser.name " /></span> 创建日期: <span><s:date
								name="supplier.createDate" format="yyyy-MM-dd" /></span>
					</small> --%>
				</div>
				<div class="box-body ipadmini">
					<div class="HideShowPanel">
						<input type="hidden" id="handlemethod"
							value="${requestScope.handlemethod}">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" name="createUser.id"
								value="<s:property value=" supplier.createUser.id "/>" /> <input
								type="hidden" name="createUser.name"
								value="<s:property value=" supplier.createUser.name "/>" /> <input
								type="hidden" name="createDate"
								value="<s:date name=" createDate " format="yyyy-MM-dd " />" />
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.encrypt" /> <!-- 编码 -->  </span> <input
											type="text" id="supplier_id" name="supplier.id"
											class="form-control" changelog="<s:property value=" supplier.id "/>"
											value="<s:property value=" supplier.id "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.name" /> <!-- 描述 -->  </span> <input
											type="text" id="supplier_name" name="supplier.name"
											class="form-control" changelog="<s:property value=" supplier.name "/>"
											value="<s:property value=" supplier.name "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.addr" /> <!--  地址  --></span> <input
											type="text" id="supplie_address5" name="supplier.address5"
											class="form-control" changelog="<s:property value=" supplier.address5 "/>"
											value="<s:property value=" supplier.address5 "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.linkmanName" /> <!-- 联系人1 --> </span> <input
											type="text" id="supplier_linkMan" name="supplier.linkMan"
											class="form-control" changelog="<s:property value=" supplier.linkMan "/>"
											value="<s:property value=" supplier.linkMan "/>" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.phone" /> <!-- 联系方式 --> </span> <input
											type="text" class="form-control" id="supplier_linkTel"
											name="supplier.linkTel" changelog="<s:property value=" supplier.linkTel "/>"
											value="<s:property value=" supplier.linkTel "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.email" /> <!-- Email --> </span> <input
											type="text" id="supplier_email" class="form-control"
											name="supplier.email" changelog="<s:property value=" supplier.email "/>"
											value="<s:property value=" supplier.email "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.linkmanName" /> <!--  联系人2  --></span> <input
											type="text" id="supplier_linkMan1" class="form-control"
											name="supplier.linkMan1" changelog="<s:property value=" supplier.linkMan1 "/>"
											value="<s:property value=" supplier.linkMan1 "/>" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.phone" /> <!-- 联系方式 --> </span> <input
											type="text" id="supplier_linkTel1" class="form-control"
											name="supplier.linkTel1" changelog="<s:property value=" supplier.linkTel1 "/>"
											value="<s:property value=" supplier.linkTel1 "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.email" /> <!-- Email --> </span> <input
											type="text" id="supplier_email1" class="form-control"
											name="supplier.email1" changelog="<s:property value=" supplier.email1 "/>"
											value="<s:property value=" supplier.email1 "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.linkmanName" /> <!-- 联系人3 --> </span> <input
											type="text" id="supplier_linkMan2" class="form-control"
											name="supplier.linkMan2" changelog="<s:property value=" supplier.linkMan2 "/>"
											value="<s:property value=" supplier.linkMan2 "/>" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.phone" /> <!-- 联系方式 --> </span> <input
											type="text" id="supplier_linkTel2" class="form-control"
											name="supplier.linkTel2" changelog="<s:property value=" supplier.linkTel2 "/>"
											value="<s:property value=" supplier.linkTel2 "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.crmCustomer.email" /> <!-- Email --> </span> <input
											type="text" id="supplier_email2" class="form-control"
											name="supplier.email2" changelog="<s:property value=" supplier.email2 "/>"
											value="<s:property value=" supplier.email2 "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.fax" /> <!-- 传真  --></span> <input type="text"
											id="supplier_fax" class="form-control" name="supplier.fax" 
											changelog="<s:property value=" supplier.fax "/>"
											value="<s:property value=" supplier.fax "/>" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.postcode" /> <!-- 邮编 --> </span> <input
											type="text" id="supplier_postCode" class="form-control"
											name="supplier.postCode" changelog="<s:property value=" supplier.postCode "/>"
											value="<s:property value=" supplier.postCode "/>" />
									</div>
								</div>
								<!--状态-->
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message key='biolims.common.state'/></span>
										<select lay-ignore id="supplier_state_id" name="supplier.state.id" class="form-control" 
										changelog="<s:property value=" supplier.state.id "/>">
											<option value="1e" <s:if test="supplier.state.id==1e">selected="selected"</s:if>>
												<fmt:message key='biolims.common.effective' />
											</option>
											<!-- 有效 -->
											<option value="0e" <s:if test="supplier.state.id==0e">selected="selected"</s:if>>
												<fmt:message key='biolims.common.invalid' />
											</option>
											<!-- 无效 -->
										</select>
	
									</div>
								</div>
								<div class="col-xs-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.CostCentre" /> </span> <input type="hidden"
											id="supplier_scopeId" name="supplier.scopeId"
											value="<s:property value="supplier.scopeId"/>"> <input readonly="readonly"
											type="text" size="50" maxlength="127" id="supplier_scopeName"
											name="supplier.scopeName"
											changelog="<s:property value="supplier.scopeName"/>" title=""
											class="form-control"
											value="<s:property value="supplier.scopeName"/>" /> <span
											class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showScope()">
												<i class="glyphicon glyphicon-search"></i>
										</span>
									</div>
								</div>

								<div class="col-xs-12">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.enterpriseQualification" />
												<!-- 企业资质 -->
											</h3>
										</div>
									</div>
								</div>
								<br />

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.registeredCapital" /> <!-- 注册资本 --> </span> <input
											type="text" id="supplier_capitalNum" class="form-control"
											name="supplier.capitalNum" changelog="<s:property value=" supplier.capitalNum "/>"
											value="<s:property value=" supplier.capitalNum "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.organizationCode" /> <!-- 组织机构代码 --> </span> <input
											type="text" id="supplier_orgCode" class="form-control"
											name="supplier.orgCode" changelog="<s:property value=" supplier.orgCode "/>"
											value="<s:property value=" supplier.orgCode "/>" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.inputVocation" /> <!-- 职业 行业 --> </span> <input
											type="text" id="supplier_way" class="form-control"
											name="supplier.way" changelog="<s:property value=" supplier.way "/>"
											value="<s:property value=" supplier.way "/>" />
									</div>
								</div>
								<%-- <div class="input-group">
										<span class="input-group-addon"> 企业性质 </span> <input
											type="hidden" id="supplier_entKind_name" name="supplier.entKind.name"
											value="<s:property value=" supplier.entKind.name "/>" /> <fmt:message key="biolims.common.invalid" />
									</div> --%>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.aptitude1" /> <!-- 资质1 --> </span> <input
											type="text" id="supplier_aptitude1" class="form-control"
											name="supplier.aptitude1" changelog="<s:property value=" supplier.aptitude1 "/>"
											value="<s:property value=" supplier.aptitude1 "/>" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.aptitude2" /> </span> <input type="text"
											id="supplier_aptitude2" class="form-control"
											name="supplier.aptitude2" changelog="<s:property value=" supplier.aptitude2 "/>"
											value="<s:property value=" supplier.aptitude2 "/>" />
									</div>
								</div>

								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.aptitude3" /></span> <input type="text"
											id="supplier_aptitude3" class="form-control"
											name="supplier.aptitude3" changelog="<s:property value=" supplier.aptitude3 "/>"
											value="<s:property value=" supplier.aptitude3 "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="input-group">
									<button type="button" class="btn btn-info btn-sm" onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
								</div>
								</div>


								<div class="col-xs-12">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.accountInformation" />
												<!-- 账户信息 -->
											</h3>
										</div>
									</div>
								</div>
								<br />


								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.bankAccount" /> <!-- 开户行 --> </span> <input
											type="text" class="form-control" id="supplier_accountBank"
											name="supplier.accountBank" changelog="<s:property value=" supplier.accountBank "/>"
											value="<s:property value=" supplier.accountBank "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.bankAccountAddress" /> <!-- 开户行地址 --> </span>
										<input type="text" class="form-control"
											id="supplier_accountBankAddress"
											name="supplier.accountBankAddress" 
											changelog="<s:property value=" supplier.accountBankAddress "/>"
											value="<s:property value=" supplier.accountBankAddress "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.account" /> <!-- 帐号 --> </span> <input
											type="text" class="form-control" id="supplier_account"
											name="supplier.account" changelog="<s:property value=" supplier.ccount "/>"
											value="<s:property value=" supplier.ccount "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.accountName" /> <!-- 帐号名称 --> </span> <input
											type="text" class="form-control" id="supplier_accountName"
											name="supplier.accountName" changelog="<s:property value=" supplier.accountName "/>"
											value="<s:property value=" supplier.accountName "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.common.bankCode" /> <!-- 银行代码 --> </span> <input
											type="text" class="form-control" id="supplier_bankCode"
											name="supplier.bankCode" changelog="<s:property value=" supplier.bankCode "/>"
											value="<s:property value=" supplier.bankCode "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"> Swift Code(BIC) </span> <input
											type="text" class="form-control" id="supplier_bic"
											name="supplier.bic" changelog="<s:property value=" supplier.bic "/>"
											value="<s:property value=" supplier.bic "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"> ABA Routing Number</span> <input
											type="text" class="form-control" id="supplier_arn"
											name="supplier.arn" changelog="<s:property value=" supplier.arn "/>"
											value="<s:property value=" supplier.arn "/>" />
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="input-group">
										<span class="input-group-addon"> IBAN</span> <input type="text"
											class="form-control" id="supplier_iban" name="supplier.iban" 
											changelog="<s:property value=" supplier.iban "/>"
											value="<s:property value=" supplier.iban "/>" />
									</div>
								</div>
								
								<div class="col-xs-12">
									<div class="panel"
										style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
										<div class="panel-heading text-left">
											<h3 class="panel-title" style="font-family: 黑体;">
												<i class="glyphicon glyphicon-bookmark"></i>
												<fmt:message key="biolims.common.customFields" />
												<!-- 账户信息 -->
											</h3>
										</div>
									</div>
								</div>
								<!--
                                	作者：offline
                                	时间：2018-03-19
                                	描述：自定义字段
                                -->
								<div id="fieldItemDiv"></div>
								<br>
								<!-- <input type="text" name="supplierBySerialJson" id="supplierBySerialJson" value="" /> -->

								<input type="hidden" id="fieldContent" name="fieldContent" value="<s:property value=" supplier.fieldContent "/>" />
								<%-- <input type="hidden" id="supplier_scope_id" name="supplier.scope.id" value="<s:property value=" supplier.scope.id "/>" />
								<input type="hidden" id="supplier_scope_name" name="supplier.scope.name" value="<s:property value=" storage.scope.name "/>" /> --%>

								<input type="hidden" id="id_parent_hidden"
									value="<s:property value=" supplier.id "/>" />
								<input type="hidden" id="changeLog" name="changeLog"  />	
							</div>
						</form>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="storageReagentBuySerialTable" style="font-size: 14px;">
							</table>
						</div>
					</div>
				</div>
				<div class="box-footer"></div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
			src="${ctx}/javascript/supplier/main/editSupplier.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>