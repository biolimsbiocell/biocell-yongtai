
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBreastCancerTemp&id=${sampleBreastCancerTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleBreastCancerEditOne.js"></script>
<s:if test="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id != ''">
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id}"></div>
</s:if>
<s:if test="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id == ''">
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src=""></div>
</s:if>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
        <input type="hidden" id="id" value="${requestScope.id}">
        <input type="hidden" id="path" value="${requestScope.path}">
        <input type="hidden" id="fname" value="${requestScope.fname}">
        
        <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red"><fmt:message key="biolims.common.clickToUploadPictures"/></td>
				<td></td>
				<td>
  					<input type="button" value="<fmt:message key="biolims.common.uploadTheInformationRecordedImages"/>" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleBreastCancerTemp.upLoadAccessory.id" value="<s:property value="sampleBreastCancerTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleBreastCancerTemp.upLoadAccessory.fileName" value="<s:property value="sampleBreastCancerTemp.upLoadAccessory.fileName"/>">
				</td>
			</tr>
				<tr>
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25"
							id="sampleBreastCancerTemp_code" class="text input readonlytrue" readonly="readonly"
							name="sampleBreastCancerTemp.code" title="<fmt:message key="biolims.common.serialNumber"/>"
							value="<s:property value="sampleBreastCancerTemp.code"/>" />
<%-- 						<input type="hidden" id="sampleBreastCancerTemp_id" name="sampleBreastCancerTemp.sampleInfo.id" value="<s:property value="sampleBreastCancerTemp.sampleInfo.id"/>" /> --%>
						<input type="hidden" name="sampleBreastCancerTemp.id" value="<s:property value="sampleBreastCancerTemp.id"/>" />
                   		<input type="hidden"  id="upload_imga_name" name="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id"/>">
                   	</td>
                   
                   	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_name"
                   	 		name="sampleBreastCancerTemp.name" title="<fmt:message key="biolims.common.describe"/>"   
							value="<s:property value="sampleBreastCancerTemp.name"/>"
                   		/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.detectionItems"/></td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleBreastCancerTemp_product_name" searchField="true"  
 							name="sampleBreastCancerTemp.productName"  value="<s:property value="sampleBreastCancerTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleBreastCancerTemp_product_id" name="sampleBreastCancerTemp.productId"  
 							value="<s:property value="sampleBreastCancerTemp.productId"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheTestItems"/>' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   		
                </tr>
                <tr>       	
                   	<td class="label-title" ><fmt:message key="biolims.common.region"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_area"
                   	 		name="sampleBreastCancerTemp.area" title="<fmt:message key="biolims.common.region"/>"  onblur="checkAdd()" 
							value="<s:property value="sampleBreastCancerTemp.area"/>"
                   		 />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.inspectionHospital"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_hospital"
                   	 		name="sampleBreastCancerTemp.hospital" title="<fmt:message key="biolims.common.inspectionHospital"/>"   
							value="<s:property value="sampleBreastCancerTemp.hospital"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.medicalRecordCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_inHosNum"
                   	 		name="sampleBreastCancerTemp.inHosNum" title="><fmt:message key="biolims.common.medicalRecordCode"/>"   
							value="<s:property value="sampleBreastCancerTemp.inHosNum"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	

                   	<td class="label-title" ><fmt:message key="biolims.common.inspectionDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sendDate"
                   	 		name="sampleBreastCancerTemp.sendDate" title="<fmt:message key="biolims.common.inspectionDate"/>" Class="Wdate" readonly="readonly" 
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleBreastCancerTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_acceptDate"
                   	 		name="sampleBreastCancerTemp.acceptDate" title="<fmt:message key="biolims.common.receivingDate"/>" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.acceptDate" format="yyyy-MM-dd" />" 
                   		/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title"><fmt:message key="biolims.common.sampleCode"/></td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                    <td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sampleNum" 
                   			name="sampleBreastCancerTemp.sampleNum" title="<fmt:message key="biolims.common.sampleCode"/>" value="<s:property value="sampleBreastCancerTemp.sampleNum"/>"
                   		/>
                   </td>
                   		
                   <td class="label-title" ><fmt:message key="biolims.common.shouldTheReportDate"/></td>
               	   <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   <td align="left"  >
                   	   <input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_reportDate" 
                   	 		name="sampleBreastCancerTemp.reportDate" title="<fmt:message key="biolims.common.shouldTheReportDate"/>" Class="Wdate" readonly="readonly" 
                   	 		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" 
                   	 		value="<s:date name="sampleBreastCancerTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	   />
                   </td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.patientInformation"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.patientName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientName"
                   	 		name="sampleBreastCancerTemp.patientName" title="<fmt:message key="biolims.common.patientName"/>"   
							value="<s:property value="sampleBreastCancerTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
<!--                    	<td class="label-title" >姓名拼音</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientNameSpell" -->
<!--                    	 		name="sampleBreastCancerTemp.patientNameSpell" title="姓名拼音"    -->
<%-- 							value="<s:property value="sampleBreastCancerTemp.patientNameSpell"/>" --%>
<!--                    	 	/> -->
<!--                    	</td> -->
               		
               	 	<td class="label-title" ><fmt:message key="biolims.common.gender"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gender" id="sampleBreastCancerTemp_gender" >
<!-- 							<option value="" <s:if test="sampleBreastCancerTemp.gender==''">selected="selected" </s:if>>请选择</option> -->
    						<option value="0" <s:if test="sampleBreastCancerTemp.gender==0">selected="selected" </s:if>><fmt:message key="biolims.common.female"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gender==1">selected="selected" </s:if>><fmt:message key="biolims.common.male"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.age"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_age" name="sampleBreastCancerTemp.age"  value="<s:property value="sampleBreastCancerTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.nativePlace"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nativePlace"
                   	 		name="sampleBreastCancerTemp.nativePlace" title="<fmt:message key="biolims.common.nativePlace"/>"   
							value="<s:property value="sampleBreastCancerTemp.nativePlace"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.national"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nationality"
                   	 		name="sampleBreastCancerTemp.nationality" title="<fmt:message key="biolims.common.national"/>"   
							value="<s:property value="sampleBreastCancerTemp.nationality"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.height"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_heights"
                   	 		name="sampleBreastCancerTemp.heights" title="<fmt:message key="biolims.common.height"/>" 
							value="<s:property value="sampleBreastCancerTemp.heights"/>"
                   		 />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.weight"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="5" id="sampleBreastCancerTemp_weight"
                   	 		name="sampleBreastCancerTemp.weight" title="<fmt:message key="biolims.common.weight"/>" onkeypress="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onkeyup="if(!this.value.match(/^[\+\-]?\d*?\.?\d*?$/))this.value=this.t_value;else this.t_value=this.value;if(this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?)?$/))this.o_value=this.value" onblur="if(!this.value.match(/^(?:[\+\-]?\d+(?:\.\d+)?|\.\d*?)?$/))this.value=this.o_value;else{if(this.value.match(/^\.\d+$/))this.value=0+this.value;if(this.value.match(/^\.$/))this.value=0;this.o_value=this.value}"
							value="<s:property value="sampleBreastCancerTemp.weight"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.maritalStatus"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.maritalStatus" id="sampleBreastCancer_maritalStatus" >
							<option value="" <s:if test="sampleBreastCancerTemp.maritalStatus==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.maritalStatus==0">selected="selected" </s:if>><fmt:message key="biolims.common.married"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.maritalStatus==1">selected="selected" </s:if>><fmt:message key="biolims.common.unmarried"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.papersType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleBreastCancerTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleBreastCancerTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_voucherType" name="sampleBreastCancerTemp.voucherType.id"  value="<s:property value="sampleBreastCancerTemp.voucherType.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectPapersType"/>' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" ><fmt:message key="biolims.common.papersCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="18" id="sampleBreastCancerTemp_voucherCode"
                   	 		name="sampleBreastCancerTemp.voucherCode" title="<fmt:message key="biolims.common.papersCode"/>"   onblur="checkFun()"
							value="<s:property value="sampleBreastCancerTemp.voucherCode"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.contact"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleBreastCancerTemp_phone"
	                   		 name="sampleBreastCancerTemp.phone" title="<fmt:message key="biolims.common.contact"/>"  value="<s:property value="sampleBreastCancerTemp.phone"/>"
	                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.email"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_emailAddress"
                  	 		name="sampleBreastCancerTemp.emailAddress" title="<fmt:message key="biolims.common.email"/>"   
							value="<s:property value="sampleBreastCancerTemp.emailAddress"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.correspondenceAddress"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_address"
                   	 		name="sampleBreastCancerTemp.address" title="<fmt:message key="biolims.common.correspondenceAddress"/>"   
							value="<s:property value="sampleBreastCancerTemp.address"/>"
                   	 	/>
                   	</td>
                   	
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.personalAndFamilyHistoryOfTumor"/></label>
						</div>
					</td>
				</tr>
               
                <tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.personalHistoryOfOvarianTumors"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorHistory" id="sampleBreastCancerTemp_tumorHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorHistory==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorHistory==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorHistory==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.pvarianTumorTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.ovaryTumorType" id="sampleBreastCancerTemp_ovaryTumorType" >
							<option value="" <s:if test="sampleBreastCancerTemp.ovaryTumorType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.ovaryTumorType==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.ovaryTumorType==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
        	
                   	<td class="label-title" ><fmt:message key="biolims.common.confirmedTheAge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_confirmAge" name="sampleBreastCancerTemp.confirmAge"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAge"/>"  maxlength="2" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" />
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.personalHistoryOfOtherTumors"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.otherTumorHistory" id="sampleBreastCancerTemp_otherTumorHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.otherTumorHistory==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.otherTumorHistory==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.otherTumorHistory==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.tumorTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorType" id="sampleBreastCancerTemp_tumorType" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorType==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorType==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.confirmedTheAge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_confirmAgeOne" name="sampleBreastCancerTemp.confirmAgeOne"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAgeOne"/>"  maxlength="2" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" />
                   	</td> 	
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.personalHistoryOfBlood"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.bloodHistory" id="sampleBreastCancerTemp_bloodHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.bloodHistory==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.bloodHistory==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.bloodHistory==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.diseaseTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.sicknessType" id="sampleBreastCancerTemp_sicknessType" >
							<option value="" <s:if test="sampleBreastCancerTemp.sicknessType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.sicknessType==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.sicknessType==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.confirmedTheAge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_confirmAgeTwo" name="sampleBreastCancerTemp.confirmAgeTwo"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAgeTwo"/>"  maxlength="2" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" />
                   	</td> 
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.whetherRelativesHaveTumors"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isRelativesTumor" id="sampleBreastCancerTemp_isRelativesTumor" >
							<option value="" <s:if test="sampleBreastCancerTemp.isRelativesTumor==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isRelativesTumor==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isRelativesTumor==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.tumorTypes"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorTypeOne" id="sampleBreastCancerTemp_tumorTypeOne" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorTypeOne==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorTypeOne==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorTypeOne==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.kinship"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.relationship1" id="sampleBreastCancerTemp_relationship" >
							<option value="" <s:if test="sampleBreastCancerTemp.relationship1==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.relationship1==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.relationship1==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>		
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.confirmedTheAge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" maxlength="2" id="sampleBreastCancerTemp_confirmAgeThree" name="sampleBreastCancerTemp.confirmAgeThree"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAgeThree"/>" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" />
                   	</td> 
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.personalHistoryOfDiseaseToProduce"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.whetherReceivedChemotherapy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isChemotherapy" id="sampleBreastCancerTemp_isChemotherapy" >
							<option value="" <s:if test="sampleBreastCancerTemp.isChemotherapy==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isChemotherapy==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isChemotherapy==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.chemotherapyDrugs"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_chemotherapeutics"
                   	 		name="sampleBreastCancerTemp.chemotherapeutics" title="<fmt:message key="biolims.common.chemotherapyDrugs"/>"   
							value="<s:property value="sampleBreastCancerTemp.chemotherapeutics"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.whetherToAcceptRadiationAndChemotherapy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.outTransfusion" id="sampleBreastCancerTemp_outTransfusion" onchange="change()" >
<!-- 							<option value="" <s:if test="sampleBreastCancerTemp.outTransfusion==''">selected="selected" </s:if>>请选择</option> -->
    						<option value="0" <s:if test="sampleBreastCancerTemp.outTransfusion==0">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.outTransfusion==1">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.lastRadiationTreatmentTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left" id="last" >
                   	 	<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_endImmuneCureDate"
                   	 		name="sampleBreastCancerTemp.endImmuneCureDate" title="<fmt:message key="biolims.common.lastRadiationTreatmentTime"/>" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.endImmuneCureDate" format="yyyy-MM-dd"/>" 
                   	 	/>
                   	</td>
               
                	<td class="label-title" ><fmt:message key="biolims.common.whetherHistoryOfPelvicInflammatoryDisease"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.stemCellsCure" id="sampleBreastCancerTemp_stemCellsCure" >
							<option value="" <s:if test="sampleBreastCancerTemp.stemCellsCure==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="0" <s:if test="sampleBreastCancerTemp.stemCellsCure==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
	    					<option value="1" <s:if test="sampleBreastCancerTemp.stemCellsCure==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.whetherHaveHistoryOfUterineEctopic"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.immuneCure" id="sampleBreastCancerTemp_immuneCure" >
							<option value="" <s:if test="sampleBreastCancerTemp.immuneCure==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="0" <s:if test="sampleBreastCancerTemp.immuneCure==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
	    					<option value="1" <s:if test="sampleBreastCancerTemp.immuneCure==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	 </tr>
                <tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.whetherHaveHistoryOfOvarianCysts"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.embryoType" id="sampleBreastCancerTemp_embryoType" >
							<option value="" <s:if test="sampleBreastCancerTemp.embryoType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.embryoType==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.embryoType==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.individualReproductiveHistory"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_badMotherhood"
                   	 		name="sampleBreastCancerTemp.badMotherhood" title="<fmt:message key="biolims.common.individualReproductiveHistory"/>"   
							value="<s:property value="sampleBreastCancerTemp.badMotherhood"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.firstTimeThePregnancy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_firstTransfusionDate"
                   	 		name="sampleBreastCancerTemp.firstTransfusionDate" title="<fmt:message key="biolims.common.firstTimeThePregnancy"/>" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.firstTransfusionDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.numberOfPregnancy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="1" id="sampleBreastCancerTemp_pregnancyTime"
                   	 		name="sampleBreastCancerTemp.pregnancyTime" title="<fmt:message key="biolims.common.numberOfPregnancy"/>" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
							value="<s:property value="sampleBreastCancerTemp.pregnancyTime"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.reproductiveNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="1" id="sampleBreastCancerTemp_parturitionTime"
                   	 		name="sampleBreastCancerTemp.parturitionTime" title="<fmt:message key="biolims.common.reproductiveNumber"/>" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"
							value="<s:property value="sampleBreastCancerTemp.parturitionTime"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.menopausalStatus"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gestationIVF" id="sampleBreastCancerTemp_gestationIVF" >
							<option value="" <s:if test="sampleBreastCancerTemp.gestationIVF==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.gestationIVF==0">selected="selected" </s:if>><fmt:message key="biolims.common.have"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gestationIVF==1">selected="selected" </s:if>><fmt:message key="biolims.common.haven't"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.menopausalAge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_menopauseAge"
                   	 		name="sampleBreastCancerTemp.menopauseAge" title="<fmt:message key="biolims.common.menopausalAge"/>"   
							value="<s:property value="sampleBreastCancerTemp.menopauseAge"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.poisonousAndHarmfulSubstancesLong-termContact"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.smoke"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cigarette"
                   	 		name="sampleBreastCancerTemp.cigarette" title="<fmt:message key="biolims.common.smoke"/>"   
							value="<s:property value="sampleBreastCancerTemp.cigarette"/>"
                   	 />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.wine"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_wine"
                   	 		name="sampleBreastCancerTemp.wine" title="<fmt:message key="biolims.common.wine"/>"   
							value="<s:property value="sampleBreastCancerTemp.wine"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.drug"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicine"
                   	 		name="sampleBreastCancerTemp.medicine" title="<fmt:message key="biolims.common.drug"/>"   
							value="<s:property value="sampleBreastCancerTemp.medicine"/>"
                   		/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.drugType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicineType"
                   	 		name="sampleBreastCancerTemp.medicineType" title="<fmt:message key="biolims.common.drugType"/>"   
							value="<s:property value="sampleBreastCancerTemp.medicineType"/>"
                   	 />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.radiation"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_radioactiveRays"
                   	 		name="sampleBreastCancerTemp.radioactiveRays" title="<fmt:message key="biolims.common.radiation"/>"   
							value="<s:property value="sampleBreastCancerTemp.radioactiveRays"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.pesticide"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pesticide"
                   	 		name="sampleBreastCancerTemp.pesticide" title="<fmt:message key="biolims.common.pesticide"/>"   
							value="<s:property value="sampleBreastCancerTemp.pesticide"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.pb"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_plumbane"
                   	 		name="sampleBreastCancerTemp.plumbane" title="<fmt:message key="biolims.common.pb"/>"   
							value="<s:property value="sampleBreastCancerTemp.plumbane"/>"
                   	 />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.hg"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_mercury"
                   	 		name="sampleBreastCancerTemp.mercury" title="<fmt:message key="biolims.common.hg"/>"   
							value="<s:property value="sampleBreastCancerTemp.mercury"/>"
                   	 />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.cd"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cadmium"
                   	 		name="sampleBreastCancerTemp.cadmium" title="<fmt:message key="biolims.common.cd"/>"   
							value="<s:property value="sampleBreastCancerTemp.cadmium"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.otherHarmfulSubstances"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_otherHazardousSubstance"
                   	 		name="sampleBreastCancerTemp.otherHazardousSubstance" title="<fmt:message key="biolims.common.otherHarmfulSubstances"/>"   
							value="<s:property value="sampleBreastCancerTemp.otherHazardousSubstance"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.costInformation"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.whetherTheCharge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isFee" id="sampleBreastCancerTemp_isFee" >
							<option value="" <s:if test="sampleBreastCancerTemp.isFee==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isFee==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isFee==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.preferentialType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.privilegeType" id="sampleBreastCancerTemp_privilegeType" >
							<option value="" <s:if test="sampleBreastCancerTemp.privilegeType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.privilegeType==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.privilegeType==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.personOfRecommendations"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_doctor"
                   	 		name="sampleBreastCancerTemp.doctor" title="<fmt:message key="biolims.common.personOfRecommendations"/>"   
							value="<s:property value="sampleBreastCancerTemp.doctor"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.whetherTheInvoice"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isInvoice" id="sampleBreastCancerTemp_isInvoice" >
							<option value="" <s:if test="sampleBreastCancerTemp.isInvoice==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isInvoice==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isInvoice==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                    <td class="label-title" ><fmt:message key="biolims.common.enterPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_id"
                   	 		name="sampleBreastCancerTemp.createUser.id" title="<fmt:message key="biolims.common.enterPerson"/>"   
                   	 />
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_name" class="text input readonlytrue" readonly="readonly"
                   	 		name="sampleBreastCancerTemp.createUser.name" title="<fmt:message key="biolims.common.enterPerson"/>"   
							value="<s:property value="sampleBreastCancerTemp.createUser.name"/>"
                   	 />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_reason"
                   	 		name="sampleBreastCancerTemp.reason" title="<fmt:message key="biolims.common.note"/>"   
							value="<s:property value="sampleBreastCancerTemp.reason"/>"
                   	 />
                   	</td>
              	</tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.money"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_money"
                   	 		name="sampleBreastCancerTemp.money" title="<fmt:message key="biolims.common.money"/>"   
							value="<s:property value="sampleBreastCancerTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_gestationalAge"
                   	 		name="sampleBreastCancerTemp.gestationalAge" title="SP"   
							value="<s:property value="sampleBreastCancerTemp.gestationalAge"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.institutionsOfMakeOutAnInvoice"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_paymentUnit"
                   	 		name="sampleBreastCancerTemp.paymentUnit" title="<fmt:message key="biolims.common.institutionsOfMakeOutAnInvoice"/>"   
							value="<s:property value="sampleBreastCancerTemp.paymentUnit"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                
                
                 <g:LayOutWinTag buttonId="showreportMan" title='<fmt:message key="biolims.common.selectingTheCheckPerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reportManUserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_reportMan"
				documentName="sampleBreastCancerTemp_reportMan_name" />
			
			  	 	<td class="label-title" ><fmt:message key="biolims.common.checkThePeopleOne"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_reportMan_name"  value="<s:property value="sampleBreastCancerTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_reportMan" name="sampleBreastCancerTemp.reportMan.id"  value="<s:property value="sampleBreastCancerTemp.reportMan.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectingTheCheckPerson"/>' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 <g:LayOutWinTag buttonId="showauditMan" title='<fmt:message key="biolims.common.selectingTheCheckPerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="auditManUserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_auditMan"
				documentName="sampleBreastCancerTemp_auditMan_name" />
			
			  	 	<td class="label-title" ><fmt:message key="biolims.common.checkThePeopleTwo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_auditMan_name"  value="<s:property value="sampleBreastCancerTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_auditMan" name="sampleBreastCancerTemp.auditMan.id"  value="<s:property value="sampleBreastCancerTemp.auditMan.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectingTheCheckPerson"/>' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                	
                </tr>
               
                
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBreastCancer.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
