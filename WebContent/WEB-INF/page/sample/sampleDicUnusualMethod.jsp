
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<script type="text/javascript" src="${ctx}/js/sample/sampleDicUnusualMethod.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicType_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">异常处理</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="dicType_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">功能码</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicType_code" searchField="true" name="code"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">说明</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicType_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
               	<td class="label-title">系统标记</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicType_sysCode" searchField="true" name="sysCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">类型</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicType_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td>
               	<td class="label-title">排序号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicType_orderNumber" searchField="true" name="orderNumber"  class="input-20-length"></input>
               	</td>
               	<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicType_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<div id="show_DicUnusualMethod_div1"></div>
   		<input type="hidden" id="model" value="<%=request.getParameter("model")%>"/>
   		<input type="hidden" id="sequencingPlatform" value="<%=request.getParameter("sequencingPlatform")%>"/>
   		<input type="hidden" id="productId" value="<%=request.getParameter("productId")%>"/>
   		<input type="hidden" id="sampleTypeId" value="<%=request.getParameter("sampleTypeId")%>"/>
   		<input type="hidden" id="n" value="${requestScope.n}">
</body>
</html>



