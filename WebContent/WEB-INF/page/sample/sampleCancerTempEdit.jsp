<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleCancerTemp&id=${sampleCancerTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>

<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp" %>
<script type="text/javascript" src="${ctx}/js/sample/sampleCancerTempEdit.js"></script>
<%if(request.getParameter("imgId")!=null&&request.getParameter("imgId")!=""){%>
	<div style="overflow-y:auto;overflow-x:auto;width:400px;height:95%;float:left;" id="sampleInputTempItemImg"><img onmousewheel="return changeimg(this) " src=" ${ctx}/operfile/downloadById.action?id=${imgId}" ></div>
<%} %>	

 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				
               	 	
                   	<input type="hidden" size="20" maxlength="25" id="sampleCancerTemp_id"
                   	 name="sampleCancerTemp.id" title="编码" readonly="readOnly" class="text input readonlytrue" 
                   	 value="<s:property value="sampleCancerTemp.id"/>" />
                   	 
			
			
               	 	<td class="label-title" >订单号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_orderNumber"
                   	 name="sampleCancerTemp.orderNumber" title="订单号"
                   	   
	value="<s:property value="sampleCancerTemp.orderNumber"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	
			
               	 	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_name"
                   	 name="sampleCancerTemp.name" title="姓名"
                   	   
	value="<s:property value="sampleCancerTemp.name"/>"
                   	  />
                   	  
                   	</td>
                   	
                   	
                   	
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
					<select id="sampleCancerTemp_gender" name="sampleCancerTemp.gender" class="input-10-length" >
								<option value="1" <s:if test="sampleCancerTemp.gender==1">selected="selected"</s:if>>男</option>
								<option value="0" <s:if test="sampleCancerTemp.gender==0">selected="selected"</s:if>>女</option>
					</select>
                   	</td>
			
			
               	 	<td class="label-title" >出生日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_birthDate"
                   	 name="sampleCancerTemp.birthDate" title="出生日期"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleCancerTemp.birthDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	<td class="label-title" >确诊日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_diagnosisDate"
                   	 name="sampleCancerTemp.diagnosisDate" title="确诊日期"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleCancerTemp.diagnosisDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			</tr>
			<tr>
				<%-- <g:LayOutWinTag buttonId="showCancerType" title="选择肿瘤类别"
				hasHtmlFrame="true"
				html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="cancerType" 
				hasSetFun="true"
 				documentId="sampleCancerTemp_dicType"
				documentName="sampleCancerTemp_dicType_name" />
			
               	 	<td class="label-title" >肿瘤类别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   <input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_dicType_name"  value="<s:property value="sampleCancerTemp.dicType.name"/>" class="text input readonlyfalse" readonly="readOnly"  />
 						<input type="hidden" id="sampleCancerTemp_dicType" name="sampleCancerTemp.dicType.id"  value="<s:property value="sampleCancerTemp.dicType.id"/>" > 
 						<img alt='选择' id='showCancerType' src='${ctx}/images/img_lookup.gif' 	class='detail'    />     
                   	  --%>
                 	<td class="label-title" >肿瘤类型</td>
                 	<td class="requiredcolumn" nowrap width="10px" ></td>	
                 	<td align="left"  >
					<input type="hidden" size="20"   id="sampleCancerTemp_cancerType_Id" searchField="true"  name="sampleCancerTemp.cancerType.id"  value="<s:property value="sampleCancerTemp.cancerType.id"/>" class="text input" />
					<input type="text"  id="sampleCancerTemp_cancerType_cancerTypeName" name="sampleCancerTemp.cancerType.cancerTypeName" value="<s:property value="sampleCancerTemp.cancerType.cancerTypeName"/>" readonly="readonly" > 
					<img alt='选择肿瘤类型' id='showage' src='${ctx}/images/img_lookup.gif' onClick="WeiChatCancerTypeFun()" class='detail' />                   		
                 	</td>
                   	  
                   	<td class="label-title" >肿瘤子类一</td>
                 	<td class="requiredcolumn" nowrap width="10px" ></td>	
                 	<td align="left"  >
					<input type="hidden" size="20"   id="sampleCancerTemp_cancerTypeSeedOne_Id" searchField="true"  name="sampleCancerTemp.cancerTypeSeedOne.id"  value="<s:property value="sampleCancerTemp.cancerTypeSeedOne.id"/>" class="text input" />
					<input type="text"  id="sampleCancerTemp_cancerTypeSeedOne_cancerTypeName" name="sampleCancerTemp.cancerTypeSeedOne.cancerTypeName" value="<s:property value="sampleCancerTemp.cancerTypeSeedOne.cancerTypeName"/>" readonly="readonly" > 
					<img alt='选择肿瘤类型一' id='showageOne' src='${ctx}/images/img_lookup.gif' onClick="WeiChatCancerTypeOneFun()" class='detail' />                   		
                 	</td>
                 	
                 	<td class="label-title" >肿瘤子类二</td>
                 	<td class="requiredcolumn" nowrap width="10px" ></td>	
                 	<td align="left"  >
					<input type="hidden" size="20"   id="sampleCancerTemp_cancerTypeSeedTwo_Id" searchField="true"  name="sampleCancerTemp.cancerTypeSeedTwo.id"  value="<s:property value="sampleCancerTemp.cancerTypeSeedTwo.id"/>" class="text input" />
					<input type="text"  id="sampleCancerTemp_cancerTypeSeedTwo_cancerTypeName" name="sampleCancerTemp.cancerTypeSeedTwo.cancerTypeName" value="<s:property value="sampleCancerTemp.cancerTypeSeedTwo.cancerTypeName"/>" readonly="readonly" > 
					<img alt='选择肿瘤类型二' id='showageTwo' src='${ctx}/images/img_lookup.gif' onClick="WeiChatCancerTypeTwoFun()" class='detail' />                   		
                 	</td>
                   	
			
			
			
			
			
			
			</tr>
			<tr>
					<td class="label-title" >分期</td>
               	 	<td class="requiredcolumn" nowrap width="20px" ></td>            	 	
                   	<td align="left"  >
					<select id="sampleCancerTemp_sampleStage" name="sampleCancerTemp.sampleStage" class="input-10-length" >
								<option value="" >--请选择--</option>
								<option value="1" <s:if test="sampleCancerTemp.sampleStage==1">selected="selected"</s:if>>I</option>
								<option value="2" <s:if test="sampleCancerTemp.sampleStage==2">selected="selected"</s:if>>II</option>
								<option value="3" <s:if test="sampleCancerTemp.sampleStage==3">selected="selected"</s:if>>III</option>
								<option value="4" <s:if test="sampleCancerTemp.sampleStage==4">selected="selected"</s:if>>IV</option>
					</select>
                   	</td>
                   	  
                   	
			<g:LayOutWinTag buttonId="showinspectionDepartment" title="选择送检科室"
				hasHtmlFrame="true"
				html="${ctx}/dic/type/dicTypeSelect.action"
				isHasSubmit="false" functionName="ks" 
				hasSetFun="true"
 				documentId="sampleCancerTemp_inspectionDepartment"
				documentName="sampleCancerTemp_inspectionDepartment_name" />
				
			
			
               	 	<td class="label-title" >送检科室</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_inspectionDepartment_name"  value="<s:property value="sampleCancerTemp.inspectionDepartment.name"/>" class="text input readonlyfalse" readonly="readOnly"  />
 						<input type="hidden" id="sampleCancerTemp_inspectionDepartment" name="sampleCancerTemp.inspectionDepartment.id"  value="<s:property value="sampleCancerTemp.inspectionDepartment.id"/>" > 
 						<img alt='选择送检科室' id='showinspectionDepartment' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			</tr>
			<tr>
				<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="hidden" size="20"   id="sampleCancerTemp_productId" searchField="true"  name="sampleCancerTemp.productId"  value="" class="text input" />
 						<input type="text"  id="sampleCancerTemp_productName" name="sampleCancerTemp.productName" value="<s:property value="sampleCancerTemp.productName"/>" readonly="readonly" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   	
			
               	 	<td class="label-title" >采样日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_samplingDate"
                   	 name="sampleCancerTemp.samplingDate" title="采样日期"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleCancerTemp.samplingDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
			
				<g:LayOutWinTag buttonId="showsamplingLocation" title="选择采样部位"
					hasHtmlFrame="true"
					html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="cybw" 
	 				hasSetFun="true"
					documentId="sampleCancerTemp_samplingLocation"
					documentName="sampleCancerTemp_samplingLocation_name" />
				
			
			
               	 	<td class="label-title" >采样部位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_samplingLocation_name"  value="<s:property value="sampleCancerTemp.samplingLocation.name"/>" class="text input readonlyfalse" readonly="readOnly"  />
 						<input type="hidden" id="sampleCancerTemp_samplingLocation" name="sampleCancerTemp.samplingLocation.id"  value="<s:property value="sampleCancerTemp.samplingLocation.id"/>" > 
 						<img alt='选择采样部位' id='showsamplingLocation' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 
			</tr>
			<tr>
					<td class="label-title" >外部样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_samplingNumber"
                   	 name="sampleCancerTemp.samplingNumber" title="样本编号"
  					value="<s:property value="sampleCancerTemp.samplingNumber"/>"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" >是否病理确认</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
					<select id="sampleCancerTemp_pathologyConfirmed" name="sampleCancerTemp.pathologyConfirmed" class="input-10-length" >
								<option value="1" <s:if test="sampleCancerTemp.pathologyConfirmed==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="sampleCancerTemp.pathologyConfirmed==0">selected="selected"</s:if>>否</option>
					</select>
                   	</td>
			
			
               	 	<td class="label-title" >血液采样时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_bloodSampleDate"
                   	 name="sampleCancerTemp.bloodSampleDate" title="血液采样时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleCancerTemp.bloodSampleDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 	
			</tr>
			<tr>
			<td class="label-title" >血浆分离时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_plasmapheresisDate"
                   	 name="sampleCancerTemp.plasmapheresisDate" title="血浆分离时间"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleCancerTemp.plasmapheresisDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
				<g:LayOutWinTag buttonId="showcommissioner" title="选择负责人"
					hasHtmlFrame="true" hasSetFun="true"
					html="${ctx}/core/user/userSelect.action"
					width="document.body.clientWidth/1.5" isHasSubmit="false"
					functionName="dutyUserFun" documentId="sampleCancerTemp_commissioner"
					documentName="sampleCancerTemp_commissioner_name" />
			
				
			
			
               	 	<td class="label-title" >销售代表</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_commissioner_name"  value="<s:property value="sampleCancerTemp.commissioner.name"/>" class="text input readonlyfalse" readonly="readOnly"  />
 						<input type="hidden" id="sampleCancerTemp_commissioner" name="sampleCancerTemp.commissioner.id"  value="<s:property value="sampleCancerTemp.commissioner.id"/>" > 
 						<img alt='选择销售代表' id='showcommissioner' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			
			
               	 	<td class="label-title" >收样日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_receivedDate"
                   	 name="sampleCancerTemp.receivedDate" title="收样日期"
                   	   
                   	   Class="Wdate"
                   	    onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"   value="<s:date name="sampleCancerTemp.receivedDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			
			
               	 
                   	
			</tr>
			
			<tr>

                   	
                  <%--  	<td class="label-title" >电子病历编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_medicalNumber" name="sampleCancerTemp.medicalNumber" title="电子病历编号" 
                   	value="<s:property value="sampleCancerTemp.medicalNumber"/>"
                   	  />
                   	  <img alt='选择电子病历编号' id='showageTwo' src='${ctx}/images/img_lookup.gif' onClick="WeiChatCancerTypeTwoFun()" class='detail' /> 
                   	</td> --%>
                   	
                   	<td class="label-title" >电子病历编号</td>
                 	<td class="requiredcolumn" nowrap width="10px" ></td>	
                 	<td align="left"  >
					<input type="text" size="20"   id="sampleCancerTemp_medicalNumber" searchField="true"  name="sampleCancerTemp.medicalNumber"  value="<s:property value="sampleCancerTemp.medicalNumber"/>" class="text input" />
					<img alt='选择电子病历编号' id='showagemedicalNumber' src='${ctx}/images/img_lookup.gif' onClick="crmPatientTypeTwoFun()" class='detail' />                   		
                 	</td>
                 	
                 	
                 	<g:LayOutWinTag buttonId="showCollectionManner" title="选择收费方式"
					hasHtmlFrame="true"
					html="${ctx}/dic/type/dicTypeSelect.action"
					isHasSubmit="false" functionName="sflx" 
	 				hasSetFun="true"
					documentId="sampleCancerTemp_collectionManner"
					documentName="sampleCancerTemp_collectionManner_name" />
				
		
               	 	<td class="label-title" >收费方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_collectionManner_name"  value="<s:property value="sampleCancerTemp.collectionManner.name"/>" class="text input readonlyfalse"   />
 						<input type="hidden" id="sampleCancerTemp_collectionManner" name="sampleCancerTemp.collectionManner.id"  value="<s:property value="sampleCancerTemp.collectionManner.id"/>" > 
 						<img alt='选择收费方式' id='showCollectionManner' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                  <td class="label-title" >收费说明</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left">
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_chargeNote"
                   	 name="sampleCancerTemp.chargeNote" title="收费说明"
  					value="<s:property value="sampleCancerTemp.chargeNote"/>"
                   	  />
                   	  
                   	</td>

			</tr>
			
			<tr>
					<td class="label-title"  >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  colspan="8" 	 >
                   	  
 					<input  type="hidden" id="sampleCancerTemp_sampleTypeId" name="sampleCancerTemp.sampleTypeId" value="<s:property value="sampleCancerTemp.sampleTypeId"/>"	/>
					
					<c:forEach var="DicCountTable"	items="${dicSampleTypeList}">
						
						<span id="${DicCountTable.id}" class="label-title">${DicCountTable.name}</span>
						<input type="checkbox" name="sck.checkedBoxTest" id="sck_checkedBoxTest" value="${DicCountTable.id}" />&nbsp;&nbsp;&nbsp;&nbsp;
						
					</c:forEach>
				
			</tr>
			
			<tbody>
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " colspan="9" width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label"> 家属信息</span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			</tbody>
			<tr>
			
			
               	 	<td class="label-title" >家属联系人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_family"
                   	 name="sampleCancerTemp.family" title="家属联系人"
                   	   
	value="<s:property value="sampleCancerTemp.family"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >家属联系人电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_familyPhone"
                   	 name="sampleCancerTemp.familyPhone" title="家属联系人电话"
                   	   
	value="<s:property value="sampleCancerTemp.familyPhone"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >家属联系人地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="sampleCancerTemp_familySite"
                   	 name="sampleCancerTemp.familySite" title="家属联系人地址"
                   	   
	value="<s:property value="sampleCancerTemp.familySite"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			<g:LayOutWinTag buttonId="showHos" title="选择医疗机构"
				hasHtmlFrame="true"
				html="${ctx}/crm/customer/customer/crmCustomerSelect.action"
				isHasSubmit="false" functionName="HosFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_hos_id').value=rec.get('id');
				document.getElementById('sampleCancerTemp_medicalInstitutions').value=rec.get('name');
				document.getElementById('sampleCancerTemp_medicalInstitutionsPhone').value=rec.get('ks');
				document.getElementById('sampleCancerTemp_medicalInstitutionsSite').value=rec.get('street');
				" />
			<input type="hidden"  id="sampleCancerTemp_hos_id"    	 name="sampleCancerTemp.crmCustomer.id" >
               	 	<td class="label-title" >医疗机构</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_medicalInstitutions"
                   	 name="sampleCancerTemp.medicalInstitutions" title="医疗机构"
                   	   value="<s:property value="sampleCancerTemp.medicalInstitutions"/>"
                   	  />
                   	   <img alt='选择医疗机构' id='showHos' src='${ctx}/images/img_lookup.gif' 	class='detail'    />   
                   	</td>
			
			
               	 	<td class="label-title" >医疗机构联系电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_medicalInstitutionsPhone"
                   	 name="sampleCancerTemp.medicalInstitutionsPhone" title="医疗机构联系电话"
                   	   
	value="<s:property value="sampleCancerTemp.medicalInstitutionsPhone"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >医疗机构联系地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="sampleCancerTemp_medicalInstitutionsSite"
                   	 name="sampleCancerTemp.medicalInstitutionsSite" title="医疗机构联系地址"
                   	   
	value="<s:property value="sampleCancerTemp.medicalInstitutionsSite"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
				<g:LayOutWinTag buttonId="showDoctor" title="选择医生"
				hasHtmlFrame="true"
				html="${ctx}/crm/doctor/crmPatientSelect.action"
				isHasSubmit="false" functionName="DicProbeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_attendingDoctor_id').value=rec.get('id');
				document.getElementById('sampleCancerTemp_attendingDoctor').value=rec.get('name');
				document.getElementById('sampleCancerTemp_attendingDoctorPhone').value=rec.get('mobile');
				document.getElementById('sampleCancerTemp_attendingDoctorSite').value=rec.get('crmCustomer-name');
				" />
			
			<input type="hidden"  id="sampleCancerTemp_attendingDoctor_id"    	 name="sampleCancerTemp.crmDoctor.id" >
			
               	 	<td class="label-title" >主治医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_attendingDoctor"
                   	 name="sampleCancerTemp.attendingDoctor" title="主治医生"
						value="<s:property value="sampleCancerTemp.attendingDoctor"/>"
                   	  />
                   	  <img alt='选择医生' id='showDoctor' src='${ctx}/images/img_lookup.gif' 	class='detail'    />   
                   	</td>
			
			
               	 	<td class="label-title" >主治医生联系电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_attendingDoctorPhone"
                   	 name="sampleCancerTemp.attendingDoctorPhone" title="主治医生联系电话"
                   	   
	value="<s:property value="sampleCancerTemp.attendingDoctorPhone"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >主治医生联系地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="sampleCancerTemp_attendingDoctorSite"
                   	 name="sampleCancerTemp.attendingDoctorSite" title="主治医生联系地址"
                   	   
	value="<s:property value="sampleCancerTemp.attendingDoctorSite"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
						
				<tr class="sectionrow " valign="top">
					<td class="sectioncol " colspan="9" width='50%' valign="top" align="right">
						<div class="section standard_section marginsection  ">
							<div class="section_header standard_section_header">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="section_header_left text standard_section_label labelcolor" align="left">
													<span class="section_label">其他 </span>
											</td>
											<td class="section_header_right" align="right"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</td>
				</tr>
			
			<tr>
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_note"
                   	 name="sampleCancerTemp.note" title="备注"
                   	   
	value="<s:property value="sampleCancerTemp.note"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_createUser_name"  value="<s:property value="sampleCancerTemp.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleCancerTemp_createUser" name="sampleCancerTemp.createUser.id"  value="<s:property value="sampleCancerTemp.createUser.id"/>" > 
 						               		
                   	</td>
			
			
               	 	<td class="label-title" >创建时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_createDate"
                   	 name="sampleCancerTemp.createDate" title="创建时间"
                   	   class="text input readonlytrue" readonly="readOnly" value="<s:date name="sampleCancerTemp.createDate" format="yyyy-MM-dd"/>" />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
			
				
			
			
               	 	<td class="label-title" >审批人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_confirmUser_name"  value="<s:property value="sampleCancerTemp.confirmUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleCancerTemp_confirmUser" name="sampleCancerTemp.confirmUser.id"  value="<s:property value="sampleCancerTemp.confirmUser.id"/>" > 
 						                   		
                   	</td>
			
			
               	 	<td class="label-title" >审批时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleCancerTemp_confirmDate"
                   	 name="sampleCancerTemp.confirmDate" title="审批时间"
                   	   class="text input readonlytrue" readonly="readOnly" 
                   	  value="<s:date name="sampleCancerTemp.confirmDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="hidden" id="sampleCancerTemp_state"
                   	 name="sampleCancerTemp.state" 
                   value="<s:property  value="sampleCancerTemp.state"/>"  />
                   	  
                   	  <input type="text" size="20" maxlength="25" id="sampleCancerTemp_stateName"
                   	 name="sampleCancerTemp.stateName" title="状态名称"
                   	   class="text input readonlytrue" readonly="readOnly" 
	value="<s:property value="sampleCancerTemp.stateName"/>"
                   	   
                   	  />
                   	  
                   	</td>
			</tr>

			<tr>
					
				<td class="label-title">附件</td><td></td>
							<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
								class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
           <!--  <input type="hidden" name="sampleCancerTempPersonnelJson" id="sampleCancerTempPersonnelJson" value="" />
            <input type="hidden" name="sampleCancerTempItemJson" id="sampleCancerTempItemJson" value="" /> -->
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleCancerTemp.id"/>" />
            </form>
           <%--
            <div id="tabs">
            <ul>
			<li><a href="#sampleCancerTempPersonnelpage">检测订单-家庭病史</a></li>
			<li><a href="#sampleCancerTempItempage">检测订单-用药信息</a></li>
           	</ul> 
			<div id="sampleCancerTempPersonnelpage" width="100%" height:10px></div>
			<div id="sampleCancerTempItempage" width="100%" height:10px></div>
			</div> --%>
        	</div>
	</body>
	</html>
