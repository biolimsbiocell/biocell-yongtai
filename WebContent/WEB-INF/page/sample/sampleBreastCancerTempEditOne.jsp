
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBreastCancerTemp&id=${sampleBreastCancerTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleBreastCancerTempEditOne.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBreastCancerTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
        <input type="hidden" id="id" value="${requestScope.id}">
        <input type="hidden" id="path" value="${requestScope.path}">
        <input type="hidden" id="fname" value="${requestScope.fname}">
        <input type="hidden"  id="str" value="${requestScope.str}">
        <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
               	 	<td class="label-title">编号</td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25"
							id="sampleBreastCancerTemp_code" class="text input readonlytrue" readonly="readonly"
							name="sampleBreastCancerTemp.code" title="编号"
							value="<s:property value="sampleBreastCancerTemp.code"/>" />
<%-- 						<input type="hidden" id="sampleBreastCancerTemp_id" name="sampleBreastCancerTemp.sampleInfo.id" value="<s:property value="sampleBreastCancerTemp.sampleInfo.id"/>" /> --%>
						<input type="hidden" name="sampleBreastCancerTemp.id" value="<s:property value="sampleBreastCancerTemp.id"/>" />
                   	</td>
                   
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_name"
                   	 		name="sampleBreastCancerTemp.name" title="描述"   
							value="<s:property value="sampleBreastCancerTemp.name"/>"
                   		/>
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleBreastCancerTemp_product_name" searchField="true"  
 							name="sampleBreastCancerTemp.productName"  value="<s:property value="sampleBreastCancerTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleBreastCancerTemp_product_id" name="sampleBreastCancerTemp.productId"  
 							value="<s:property value="sampleBreastCancerTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   		
                </tr>
                <tr>       	
                   	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_area"
                   	 		name="sampleBreastCancerTemp.area" title="地区"  onblur="checkAdd()" 
							value="<s:property value="sampleBreastCancerTemp.area"/>"
                   		 />
                   	</td>
                   	
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_hospital"
                   	 		name="sampleBreastCancerTemp.hospital" title="送检医院"   
							value="<s:property value="sampleBreastCancerTemp.hospital"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >病历号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_inHosNum"
                   	 		name="sampleBreastCancerTemp.inHosNum" title="病历号"   
							value="<s:property value="sampleBreastCancerTemp.inHosNum"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	

                   	<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sendDate"
                   	 		name="sampleBreastCancerTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleBreastCancerTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	</td>
                   	
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_acceptDate"
                   	 		name="sampleBreastCancerTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.acceptDate" format="yyyy-MM-dd" />" 
                   		/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title">样本编号</td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                    <td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sampleNum" 
                   			name="sampleBreastCancerTemp.sampleNum" title="样本编号" value="<s:property value="sampleBreastCancerTemp.sampleNum"/>"
                   		/>
                   </td>
                   		
                   <td class="label-title" >应出报告日期</td>
               	   <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   <td align="left"  >
                   	   <input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_reportDate" 
                   	 		name="sampleBreastCancerTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	 		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" 
                   	 		value="<s:date name="sampleBreastCancerTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	   />
                   </td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientName"
                   	 		name="sampleBreastCancerTemp.patientName" title="姓名"   
							value="<s:property value="sampleBreastCancerTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientNameSpell"
                   	 		name="sampleBreastCancerTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleBreastCancerTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gender" id="sampleBreastCancerTemp_gender" >
							<option value="" <s:if test="sampleBreastCancerTemp.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancer_age" name="sampleBreastCancer.age"  value="<s:property value="sampleBreastCancer.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" >籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nativePlace"
                   	 		name="sampleBreastCancerTemp.nativePlace" title="籍贯"   
							value="<s:property value="sampleBreastCancerTemp.nativePlace"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nationality"
                   	 		name="sampleBreastCancerTemp.nationality" title="民族"   
							value="<s:property value="sampleBreastCancerTemp.nationality"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >身高</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_heights"
                   	 		name="sampleBreastCancerTemp.heights" title="身高" 
							value="<s:property value="sampleBreastCancerTemp.heights"/>"
                   		 />
                   	</td>
                   	
                   	<td class="label-title" >体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_weight"
                   	 		name="sampleBreastCancerTemp.weight" title="体重" 
							value="<s:property value="sampleBreastCancerTemp.weight"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isInsure" id="sampleBreastCancer_isInsure" >
							<option value="" <s:if test="sampleBreastCancerTemp.isInsure==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isInsure==0">selected="selected" </s:if>>已婚</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isInsure==1">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleBreastCancerTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleBreastCancerTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_voucherType" name="sampleBreastCancerTemp.voucherType.id"  value="<s:property value="sampleBreastCancerTemp.voucherType.id"/>" > 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_voucherCode"
                   	 		name="sampleBreastCancerTemp.voucherCode" title="证件号码"   onblur="checkFun()"
							value="<s:property value="sampleBreastCancerTemp.voucherCode"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleBreastCancerTemp_phone"
	                   		 name="sampleBreastCancerTemp.phone" title="联系方式"  value="<s:property value="sampleBreastCancerTemp.phone"/>"
	                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_emailAddress"
                  	 		name="sampleBreastCancerTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleBreastCancerTemp.emailAddress"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_address"
                   	 		name="sampleBreastCancerTemp.address" title="通讯地址"   
							value="<s:property value="sampleBreastCancerTemp.address"/>"
                   	 	/>
                   	</td>
                   	
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人及家族肿瘤病史</label>
						</div>
					</td>
				</tr>
               
                <tr>
                   	<td class="label-title" >个人卵巢肿瘤病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorHistory" id="sampleBreastCancerTemp_tumorHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorHistory==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >卵巢肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.ovaryTumorType" id="sampleBreastCancerTemp_ovaryTumorType" >
							<option value="" <s:if test="sampleBreastCancerTemp.ovaryTumorType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.ovaryTumorType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.ovaryTumorType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
        	
                   	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_confirmAge" name="sampleBreastCancerTemp.confirmAge"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAge"/>" />
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >个人其他肿瘤病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.otherTumorHistory" id="sampleBreastCancerTemp_otherTumorHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.otherTumorHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.otherTumorHistory==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.otherTumorHistory==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorType" id="sampleBreastCancerTemp_tumorType" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_confirmAgeOne" name="sampleBreastCancerTemp.confirmAgeOne"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAgeOne"/>" />
                   	</td> 	
                </tr>
                <tr>
                	<td class="label-title" >个人血液病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancer.bloodHistory" id="sampleBreastCancer_bloodHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.bloodHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.bloodHistory==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.bloodHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >疾病类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.sicknessType" id="sampleBreastCancerTemp_sicknessType" >
							<option value="" <s:if test="sampleBreastCancerTemp.sicknessType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.sicknessType==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.sicknessType==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_confirmAgeTwo" name="sampleBreastCancerTemp.confirmAgeTwo"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAgeTwo"/>" />
                   	</td> 
                </tr>
                <tr>
                	<td class="label-title" >是否有亲属患肿瘤</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.rate3" id="sampleBreastCancerTemp_rate3" >
							<option value="" <s:if test="sampleBreastCancerTemp.rate3==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.rate3==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.rate3==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorTypeOne" id="sampleBreastCancerTemp_tumorTypeOne" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorTypeOne==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorTypeOne==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorTypeOne==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >亲属关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.relationship" id="sampleBreastCancerTemp_relationship" >
							<option value="" <s:if test="sampleBreastCancerTemp.relationship==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.relationship==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.relationship==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>		
                </tr>
                <tr>
                	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_confirmAgeThree" name="sampleBreastCancerTemp.confirmAgeThree"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAgeThree"/>" />
                   	</td> 
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人病产史</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >是否接受过化疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.organGrafting" id="sampleBreastCancerTemp_organGrafting" >
							<option value="" <s:if test="sampleBreastCancerTemp.organGrafting==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.organGrafting==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.organGrafting==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >化疗药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_diagnosis"
                   	 		name="sampleBreastCancerTemp.diagnosis" title="化疗药物"   
							value="<s:property value="sampleBreastCancerTemp.diagnosis"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >是否接受过放化疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.outTransfusion" id="sampleBreastCancerTemp_outTransfusion" >
							<option value="" <s:if test="sampleBreastCancerTemp.outTransfusion==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.outTransfusion==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.outTransfusion==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                </tr>
                <tr>
                	<td class="label-title" >最后一次放化疗时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_endImmuneCureDate"
                   	 		name="sampleBreastCancerTemp.endImmuneCureDate" title="最后一次放化疗时间" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.endImmuneCureDate" format="yyyy-MM-dd"/>" 
                   	 	/>
                   	</td>
               
                	<td class="label-title" >是否有盆腔炎病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.stemCellsCure" id="sampleBreastCancerTemp_stemCellsCure" >
							<option value="" <s:if test="sampleBreastCancerTemp.stemCellsCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="sampleBreastCancerTemp.stemCellsCure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="sampleBreastCancerTemp.stemCellsCure==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >是否有子宫异位病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.immuneCure" id="sampleBreastCancerTemp_immuneCure" >
							<option value="" <s:if test="sampleBreastCancerTemp.immuneCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="sampleBreastCancerTemp.immuneCure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="sampleBreastCancerTemp.immuneCure==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	 </tr>
                <tr>
                   	<td class="label-title" >是否有卵巢囊肿病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.embryoType" id="sampleBreastCancerTemp_embryoType" >
							<option value="" <s:if test="sampleBreastCancerTemp.embryoType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.embryoType==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.embryoType==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >个人生育史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_badMotherhood"
                   	 		name="sampleBreastCancerTemp.badMotherhood" title="个人生育史"   
							value="<s:property value="sampleBreastCancerTemp.badMotherhood"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >首次妊娠</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_firstTransfusionDate"
                   	 		name="sampleBreastCancerTemp.firstTransfusionDate" title="首次妊娠" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.firstTransfusionDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >孕次</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pregnancyTime"
                   	 		name="sampleBreastCancerTemp.pregnancyTime" title="孕几次"   
							value="<s:property value="sampleBreastCancerTemp.pregnancyTime"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >产次</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_parturitionTime"
                   	 		name="sampleBreastCancerTemp.parturitionTime" title="产几次"   
							value="<s:property value="sampleBreastCancerTemp.parturitionTime"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >绝经情况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gestationIVF" id="sampleBreastCancerTemp_gestationIVF" >
							<option value="" <s:if test="sampleBreastCancerTemp.gestationIVF==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.gestationIVF==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gestationIVF==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                </tr>
                <tr>
                	<td class="label-title" >绝经年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nt"
                   	 		name="sampleBreastCancerTemp.nt" title="绝经年龄"   
							value="<s:property value="sampleBreastCancerTemp.nt"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>有毒、有害物质长期接触史</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cigarette"
                   	 		name="sampleBreastCancerTemp.cigarette" title="烟"   
							value="<s:property value="sampleBreastCancerTemp.cigarette"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_wine"
                   	 		name="sampleBreastCancerTemp.wine" title="酒"   
							value="<s:property value="sampleBreastCancerTemp.wine"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicine"
                   	 		name="sampleBreastCancerTemp.medicine" title="药物"   
							value="<s:property value="sampleBreastCancerTemp.medicine"/>"
                   		/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >药物类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicineType"
                   	 		name="sampleBreastCancerTemp.medicineType" title="药物类型"   
							value="<s:property value="sampleBreastCancerTemp.medicineType"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_radioactiveRays"
                   	 		name="sampleBreastCancerTemp.radioactiveRays" title="放射线"   
							value="<s:property value="sampleBreastCancerTemp.radioactiveRays"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pesticide"
                   	 		name="sampleBreastCancerTemp.pesticide" title="农药"   
							value="<s:property value="sampleBreastCancerTemp.pesticide"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_plumbane"
                   	 		name="sampleBreastCancerTemp.plumbane" title="铅"   
							value="<s:property value="sampleBreastCancerTemp.plumbane"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_mercury"
                   	 		name="sampleBreastCancerTemp.mercury" title="汞"   
							value="<s:property value="sampleBreastCancerTemp.mercury"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cadmium"
                   	 		name="sampleBreastCancerTemp.cadmium" title="镉"   
							value="<s:property value="sampleBreastCancerTemp.cadmium"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >其它有害物质</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note"
                   	 		name="sampleBreastCancerTemp.note" title="其它有害物质"   
							value="<s:property value="sampleBreastCancerTemp.note"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>费用信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isFee" id="sampleBreastCancerTemp_isFee" >
							<option value="" <s:if test="sampleBreastCancerTemp.isFee==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isFee==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isFee==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.privilegeType" id="sampleBreastCancerTemp_privilegeType" >
							<option value="" <s:if test="sampleBreastCancerTemp.privilegeType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.privilegeType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.privilegeType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_doctor"
                   	 		name="sampleBreastCancerTemp.doctor" title="推荐人"   
							value="<s:property value="sampleBreastCancerTemp.doctor"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isInvoice" id="sampleBreastCancerTemp_isInvoice" >
							<option value="" <s:if test="sampleBreastCancerTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                    <td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_id"
                   	 		name="sampleBreastCancerTemp.createUser.id" title="录入人"   
                   	 />
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_name" class="text input readonlytrue" readonly="readonly"
                   	 		name="sampleBreastCancerTemp.createUser.name" title="录入人"   
							value="<s:property value="sampleBreastCancerTemp.createUser.name"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_reason"
                   	 		name="sampleBreastCancerTemp.reason" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.reason"/>"
                   	 />
                   	</td>
              	</tr>
                <tr>
                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_money"
                   	 		name="sampleBreastCancerTemp.money" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_gestationalAge"
                   	 		name="sampleBreastCancerTemp.gestationalAge" title="SP"   
							value="<s:property value="sampleBreastCancerTemp.gestationalAge"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_paymentUnit"
                   	 		name="sampleBreastCancerTemp.paymentUnit" title="开票单位"   
							value="<s:property value="sampleBreastCancerTemp.paymentUnit"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                
                
                 <g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_reportMan"
				documentName="sampleBreastCancerTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_reportMan_name"  value="<s:property value="sampleBreastCancerTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_reportMan" name="sampleBreastCancerTemp.reportMan.id"  value="<s:property value="sampleBreastCancerTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 <g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_auditMan"
				documentName="sampleBreastCancerTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_auditMan_name"  value="<s:property value="sampleBreastCancerTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_auditMan" name="sampleBreastCancerTemp.auditMan.id"  value="<s:property value="sampleBreastCancerTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                   		<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleBreastCancerTemp.nextStepFlow" data="gender"
						id="sampleBreastCancerTemp_nextStepFlow">
						<option value=""
								<s:if test="sampleBreastCancerTemp.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
    					<option value="0"
								<s:if test="sampleBreastCancerTemp.nextStepFlow==0">selected="selected" </s:if>>合格</option>
    					<option value="1"
								<s:if test="sampleBreastCancerTemp.nextStepFlow==1">selected="selected" </s:if>>反馈项目管理</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                	
                	
                </tr>
               
                
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBreastCancer.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
