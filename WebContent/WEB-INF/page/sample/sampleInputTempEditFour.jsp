
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'> 
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInputTemp&id=${sampleInputTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/js/sample/sampleInputTempEditFour.js"></script>
	<s:if test="sampleInputTemp.upLoadAccessory.id != ''">
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" class="img" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
	</s:if>
	<s:if test="sampleInputTemp.upLoadAccessory.id ==''"> 
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src=""></div>
	</s:if>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}"> 
		<input type="hidden" id="id" value="${requestScope.id}"> 
		<input type="hidden" id="path" value="${requestScope.path}"> 
		<input type="hidden" id="fname" value="${requestScope.fname}">
		<input type="hidden" id="saveType" value="${requestScope.saveType}">
		<input type="hidden"  id="str" value="${requestScope.str}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
<!-- 				<td class="label-title" style="color: red">点击上传图片</td> -->
				<td></td>
				<td>
<!--   					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" /> -->
					<input type="hidden"  id="upload_imga_id" name="sampleInputTemp.upLoadAccessory.id" value="<s:property value="sampleInputTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleInputTemp.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.upLoadAccessory.fileName"/>">
				</td>
			</tr>
				<tr>
					<!--                	 	<td class="label-title" style="display: none">编号</td> -->
					<!-- 					<td class="requiredcolunm" nowrap width="10px" style="display: none"></td> --%>
					<!-- 					<td align="left" style="display: none"><input type="text" size="20" maxlength="25" -->
					<!-- 						id="sampleInputTemp_id" -->
					<!-- 						class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.id" title="编号" -->
					<%-- 						value="<s:property value="sampleInputTemp.id"/>" /> --%>
					<!-- 					</td> -->

					<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="inputTempNew.sampleInfo.code" title="样本编号" value="<s:property value="inputTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="inputTempNew.code" title="样本编号" value="<s:property value="inputTempNew.code"/>" />
                   	</td>
						
					<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="inputTempNew_name" name="inputTempNew.name" title="描述" value="<s:property value="inputTempNew.name"/>" class="sampleInputTemp_name"/>
                   	</td>
					
					<td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_productId" name="sampleInputTemp.productId"  value="<s:property value="sampleInputTemp.productId"/>"/>
  						<input type="hidden" id="sampleInputTemp_productName" name="sampleInputTemp.productName" value="<s:property value="sampleInputTemp.productName"/>"/>
  						<input type="hidden" id="inputTemp_productId" name="inputTemp.productId" value="<s:property value="inputTemp.productId"/>">
  						<input type="hidden" id="inputTemp_productName" name="inputTemp.productName" value="<s:property value="inputTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
                   		
								<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.productName"/>"><s:property value="sampleInputTemp.productName"/></option> 
									<option value="<s:property value="inputTemp.productName"/>"><s:property value="inputTemp.productName"/></option>
								</select> 
								<input type="text" name="inputTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="inputTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   				<input type="hidden" name="inputTempNew.productId" id="productIdNew" value="<s:property value="inputTempNew.productId"/>" />
                   	</td>
				</tr>
				<tr>
				
					<td class="label-title">姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_patientName" name="sampleInputTemp.patientName" value="<s:property value="sampleInputTemp.patientName"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_patientName" name="inputTemp.patientName"  value="<s:property value="inputTemp.patientName"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="aabb" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ccdd').value=document.getElementById('aabb').options[document.getElementById('aabb').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<!--下面的option的样式是为了使字体为灰色，只是视觉问题，看起来像是注释一样--> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.patientName"/>"><s:property value="sampleInputTemp.patientName"/></option> 
									<option value="<s:property value="inputTemp.patientName"/>"><s:property value="inputTemp.patientName"/></option>
								</select> 
								<input type="text" name="inputTempNew.patientName" id="ccdd" value="<s:property value="inputTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
					<td class="label-title">住院/门诊号</td>
					<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_inHosNum"   name="sampleInputTemp.inHosNum" value="<s:property value="sampleInputTemp.inHosNum"/>"> 
               	 		<input type="hidden" id="inputTemp_inHosNum" name="inputTemp.inHosNum" value="<s:property value="inputTemp.inHosNum"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="inHosNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('inHosNumNew').value=document.getElementById('inHosNum').options[document.getElementById('inHosNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.inHosNum"/>"><s:property value="sampleInputTemp.inHosNum"/></option> 
									<option value="<s:property value="inputTemp.inHosNum"/>"><s:property value="inputTemp.inHosNum"/></option>
								</select> 
								<input type="text" name="inputTempNew.inHosNum" id="inHosNumNew" value="<s:property value="inputTempNew.inHosNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
				</tr>
				<tr>
					<td class="label-title">年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_age" name="sampleInputTemp.age" value="<s:property value="sampleInputTemp.age"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_age" name="inputTemp.age"  value="<s:property value="inputTemp.age"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="age" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.age"/>"><s:property value="sampleInputTemp.age"/></option> 
									<option value="<s:property value="inputTemp.age"/>"><s:property value="inputTemp.age"/></option>
								</select> 
								<input type="text" name="inputTempNew.age" id="ageNew" value="<s:property value="inputTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>

					<td class="label-title">末次月经</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" id="sampleInputTemp_endMenstruationDate" name="sampleInputTemp.endMenstruationDate" value="<s:property value="sampleInputTemp.endMenstruationDate"/>" />
               	 		<input type="hidden" id="inputTemp_endMenstruationDate" name="inputTemp.endMenstruationDate" value="<s:property value="inputTemp.endMenstruationDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
						<select id="endMenstruationDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('endMenstruationDateNew').value=document.getElementById('endMenstruationDate').options[document.getElementById('endMenstruationDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.endMenstruationDate"/>"><s:property value="sampleInputTemp.endMenstruationDate"/></option> 
							<option value="<s:property value="inputTemp.endMenstruationDate"/>"><s:property value="inputTemp.endMenstruationDate"/></option>
						</select> 
						<input type="text" name="inputTempNew.endMenstruationDate" id="endMenstruationDateNew" value="<s:property value="inputTempNew.endMenstruationDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.endMenstruationDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>

					<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_sampleType_id" name="sampleInputTemp.sampleType.id"  value="<s:property value="sampleInputTemp.sampleType.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleInputTemp_sampleType_name" name="sampleInputTemp.sampleType.name"  value="<s:property value="sampleInputTemp.sampleType.name"/>" class="text input" />
  						<input type="hidden" id="inputTemp_sampleType_id" name="inputTemp.sampleType.id" value="<s:property value="inputTemp.sampleType.id"/>">
  						<input type="hidden" id="inputTemp_sampleType_name" name="inputTemp.sampleType.name" value="<s:property value="inputTemp.sampleType.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.sampleType.name"/>"><s:property value="sampleInputTemp.sampleType.name"/></option> 
									<option value="<s:property value="inputTemp.sampleType.name"/>"><s:property value="inputTemp.sampleType.name"/></option>
								</select> 
								<input type="text" name="inputTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="inputTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                  				<input type="hidden" name="inputTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="inputTempNew.sampleType.id"/>" />
                   	</td>

				</tr>
				<tr>
					
					<td class="label-title">临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_diagnosis" name="sampleInputTemp.diagnosis" value="<s:property value="sampleInputTemp.diagnosis"/>" />
               	 		<input type="hidden" id="inputTemp_diagnosis" name="inputTemp.diagnosis" value="<s:property value="inputTemp.diagnosis"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="diagnosis" style="width:152px;height:28px;" onChange="javascript:document.getElementById('diagnosisNew').value=document.getElementById('diagnosis').options[document.getElementById('diagnosis').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.diagnosis"/>"><s:property value="sampleInputTemp.diagnosis"/></option> 
									<option value="<s:property value="inputTemp.diagnosis"/>"><s:property value="inputTemp.diagnosis"/></option>
								</select> 
								<input type="text" name="inputTempNew.diagnosis" id="diagnosisNew" value="<s:property value="inputTempNew.diagnosis"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>

					<td class="label-title">双胎 / 多胎妊娠</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_gestationIVF" name="sampleInputTemp.gestationIVF" value="<s:property value="sampleInputTemp.gestationIVF"/>" />
               	 		<input type="hidden" id="inputTemp_gestationIVF" name="inputTemp.gestationIVF" value="<s:property value="inputTemp.gestationIVF"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.gestationIVF" id="inputTempNew_gestationIVF">
	    					<option value="0"<s:if test="inputTempNew.gestationIVF==0">selected="selected" </s:if>>否</option>
	    					<option value="1"<s:if test="inputTempNew.gestationIVF==1">selected="selected" </s:if>>是</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				    
					<td class="label-title">样本状态</td>
					<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_isInsure" name="sampleInputTemp.isInsure" value="<s:property value="sampleInputTemp.isInsure"/>" />
               	 		<input type="hidden" id="inputTemp_isInsure" name="inputTemp.isInsure" value="<s:property value="inputTemp.isInsure"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.isInsure" id="inputTempNew_isInsure">
	    					<option value="1" <s:if test="inputTempNew.isInsure==1">selected="selected" </s:if>>正常</option>
	    					<option value="0" <s:if test="inputTempNew.isInsure==0">selected="selected" </s:if>>不正常</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>

				</tr>
				<tr>
					<td class="label-title">送检科室</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_hospital" name="sampleInputTemp.hospital"  value="<s:property value="sampleInputTemp.hospital"/>"/>
  						<input type="hidden" id="inputTemp_hospital" name="inputTemp.hospital" value="<s:property value="inputTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="hospital" style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.hospital"/>"><s:property value="sampleInputTemp.hospital"/></option> 
									<option value="<s:property value="inputTemp.hospital"/>"><s:property value="inputTemp.hospital"/></option>
								</select> 
								<input type="text" name="inputTempNew.hospital" id="hospitalNew" value="<s:property value="inputTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
					<td class="label-title">送检医生</td>
					<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_doctor" name="sampleInputTemp.doctor"  value="<s:property value="sampleInputTemp.doctor"/>"/>
  						<input type="hidden" id="inputTemp_doctor" name="inputTemp.doctor" value="<s:property value="inputTemp.doctor"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="doctor" style="width:152px;height:28px;" onChange="javascript:document.getElementById('doctorNew').value=document.getElementById('doctor').options[document.getElementById('doctor').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.doctor"/>"><s:property value="sampleInputTemp.doctor"/></option> 
									<option value="<s:property value="inputTemp.doctor"/>"><s:property value="inputTemp.doctor"/></option>
								</select> 
								<input type="text" name="inputTempNew.doctor" id="doctorNew" value="<s:property value="inputTempNew.doctor"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
					<td class="label-title">采样日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_sendDate" name="sampleInputTemp.sendDate" value="<s:property value="sampleInputTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_sendDate" name="inputTemp.sendDate"  value="<s:property value="inputTemp.sendDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.sendDate"/>"><s:property value="sampleInputTemp.sendDate"/></option> 
									<option value="<s:property value="inputTemp.sendDate"/>"><s:property value="inputTemp.sendDate"/></option>
								</select> 
								
								<input type="text" name="inputTempNew.sendDate" id="sendDateNew" value="<s:property value="inputTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
				</tr>
				<tr>
					<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="inputTempNew_note" name="inputTempNew.note" title="备注" value="<s:property value="inputTempNew.note"/>" />
                   	</td>
				</tr>
				<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>检测信息</label>
						</div>
					</td>
				</tr>
				<tr>
				<tr>
					<td class="label-title">21-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_trisome21Value" name="sampleInputTemp.trisome21Value" value="<s:property value="sampleInputTemp.trisome21Value"/>" />
               	 		<input type="hidden" id="inputTemp_trisome21Value" name="inputTemp.trisome21Value" value="<s:property value="inputTemp.trisome21Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="trisome21Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome21ValueNew').value=document.getElementById('trisome21Value').options[document.getElementById('trisome21Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.trisome21Value"/>"><s:property value="sampleInputTemp.trisome21Value"/></option> 
									<option value="<s:property value="inputTemp.trisome21Value"/>"><s:property value="inputTemp.trisome21Value"/></option>
								</select> 
								<input type="text" name="inputTempNew.trisome21Value" id="trisome21ValueNew" value="<s:property value="inputTempNew.trisome21Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
<!-- 					<td class="label-title">21-三体（参考范围[-3,3]）</td> -->
<!-- 					<td class="requiredcolumn" nowrap width="10px"> -->
<%-- 						</td> --%>
<!-- 					<td align="left"> -->
<%-- 						<input type="text" size="20" maxlength="25"  id="sampleInputTemp_reference21Range" name="sampleInputTemp.reference21Range" data="reference21Range" title="21-三体（参考范围）" value="<s:property value="sampleInputTemp.reference21Range"/>" /> --%>
<!-- 					</td> -->
					<td class="label-title">21-三体（参考范围[-3,3]）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reference21Range" name="sampleInputTemp.reference21Range" value="<s:property value="sampleInputTemp.reference21Range"/>" />
               	 		<input type="hidden" id="inputTemp_reference21Range" name="inputTemp.reference21Range" value="<s:property value="inputTemp.reference21Range"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="reference21Range" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reference21RangeNew').value=document.getElementById('reference21Range').options[document.getElementById('reference21Range').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reference21Range"/>"><s:property value="sampleInputTemp.reference21Range"/></option> 
									<option value="<s:property value="inputTemp.reference21Range"/>"><s:property value="inputTemp.reference21Range"/></option>
								</select> 
								<input type="text" name="inputTempNew.reference21Range" id="reference21RangeNew" value="<s:property value="inputTempNew.reference21Range"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
				</tr>
				<tr>
					<td class="label-title">18-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_trisome18Value" name="sampleInputTemp.trisome18Value" value="<s:property value="sampleInputTemp.trisome18Value"/>" />
               	 		<input type="hidden" id="inputTemp_trisome18Value" name="inputTemp.trisome18Value" value="<s:property value="inputTemp.trisome18Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="trisome18Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome18ValueNew').value=document.getElementById('trisome18Value').options[document.getElementById('trisome18Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.trisome18Value"/>"><s:property value="sampleInputTemp.trisome18Value"/></option> 
									<option value="<s:property value="inputTemp.trisome18Value"/>"><s:property value="inputTemp.trisome18Value"/></option>
								</select> 
								<input type="text" name="inputTempNew.trisome18Value" id="trisome18ValueNew" value="<s:property value="inputTempNew.trisome18Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
<!-- 					<td class="label-title">18-三体（参考范围[-3,3]）</td> -->
<!-- 					<td class="requiredcolumn" nowrap width="10px"> -->
<%-- 						</td> --%>
<!-- 					<td align="left"> -->
<%-- 						<input type="text" size="20" maxlength="25" id="sampleInputTemp_reference18Range" name="sampleInputTemp.reference18Range" data="reference18Range" title="18-三体（参考范围）" value="<s:property value="sampleInputTemp.reference18Range"/>" /> --%>
<!-- 					</td> -->
					
					<td class="label-title">18-三体（参考范围[-3,3]）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reference18Range" name="sampleInputTemp.reference18Range" value="<s:property value="sampleInputTemp.reference18Range"/>" />
               	 		<input type="hidden" id="inputTemp_reference18Range" name="inputTemp.reference18Range" value="<s:property value="inputTemp.reference18Range"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="reference18Range" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reference18RangeNew').value=document.getElementById('reference18Range').options[document.getElementById('reference18Range').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reference18Range"/>"><s:property value="sampleInputTemp.reference18Range"/></option> 
									<option value="<s:property value="inputTemp.reference18Range"/>"><s:property value="inputTemp.reference18Range"/></option>
								</select> 
								<input type="text" name="inputTempNew.reference18Range" id="reference18RangeNew" value="<s:property value="inputTempNew.reference18Range"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
				</tr>
				<tr>
					<td class="label-title">13-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_trisome13Value" name="sampleInputTemp.trisome13Value" value="<s:property value="sampleInputTemp.trisome13Value"/>" />
               	 		<input type="hidden" id="inputTemp_trisome13Value" name="inputTemp.trisome13Value" value="<s:property value="inputTemp.trisome13Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="trisome13Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome13ValueNew').value=document.getElementById('trisome13Value').options[document.getElementById('trisome13Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.trisome13Value"/>"><s:property value="sampleInputTemp.trisome13Value"/></option> 
							<option value="<s:property value="inputTemp.trisome13Value"/>"><s:property value="inputTemp.trisome13Value"/></option>
						</select> 
						<input type="text" name="inputTempNew.trisome13Value" id="trisome13ValueNew" value="<s:property value="inputTempNew.trisome13Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
					<td class="label-title">13-三体（参考范围[-3,3]）</td>
					<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reference13Range" name="sampleInputTemp.reference13Range" value="<s:property value="sampleInputTemp.reference13Range"/>" />
               	 		<input type="hidden" id="inputTemp_reference13Range" name="inputTemp.reference13Range" value="<s:property value="inputTemp.reference13Range"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="reference13Range" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reference13RangeNew').value=document.getElementById('reference13Range').options[document.getElementById('reference13Range').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reference13Range"/>"><s:property value="sampleInputTemp.reference13Range"/></option> 
									<option value="<s:property value="inputTemp.reference13Range"/>"><s:property value="inputTemp.reference13Range"/></option>
								</select> 
								<input type="text" name="inputTempNew.reference13Range" id="reference13RangeNew" value="<s:property value="inputTempNew.reference13Range"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
				</tr>
				<tr>
					<td class="label-title">结果描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reason" name="sampleInputTemp.reason" value="<s:property value="sampleInputTemp.reason"/>" />
               	 		<input type="hidden" id="inputTemp_reason" name="inputTemp.reason" value="<s:property value="inputTemp.reason"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="reason" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reasonNew').value=document.getElementById('reason').options[document.getElementById('reason').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reason"/>"><s:property value="sampleInputTemp.reason"/></option> 
									<option value="<s:property value="inputTemp.reason"/>"><s:property value="inputTemp.reason"/></option>
								</select>
								<input type="text" name="inputTempNew.reason" id="reasonNew" value="<s:property value="inputTempNew.reason"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
	
					<td class="label-title">其他提示</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_badMotherhood" name="sampleInputTemp.badMotherhood" value="<s:property value="sampleInputTemp.badMotherhood"/>" />
               	 		<input type="hidden" id="inputTemp_badMotherhood" name="inputTemp.badMotherhood" value="<s:property value="inputTemp.badMotherhood"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="badMotherhood" style="width:152px;height:28px;" onChange="javascript:document.getElementById('badMotherhoodNew').value=document.getElementById('badMotherhood').options[document.getElementById('badMotherhood').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.badMotherhood"/>"><s:property value="sampleInputTemp.badMotherhood"/></option> 
							<option value="<s:property value="inputTemp.badMotherhood"/>"><s:property value="inputTemp.badMotherhood"/></option>
						</select> 
						<input type="text" name="inputTempNew.badMotherhood" id="badMotherhoodNew" value="<s:property value="inputTempNew.badMotherhood"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
				</tr>
				<tr>
					<td class="label-title">建议与解释</td>
					<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_medicalHistory" name="sampleInputTemp.medicalHistory" value="<s:property value="sampleInputTemp.medicalHistory"/>" />
               	 		<input type="hidden" id="inputTemp_medicalHistory" name="inputTemp.medicalHistory" value="<s:property value="inputTemp.medicalHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="medicalHistory" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicalHistoryNew').value=document.getElementById('medicalHistory').options[document.getElementById('medicalHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.medicalHistory"/>"><s:property value="sampleInputTemp.medicalHistory"/></option> 
							<option value="<s:property value="inputTemp.medicalHistory"/>"><s:property value="inputTemp.medicalHistory"/></option>
						</select> 
						<input type="text" name="inputTempNew.medicalHistory" id="medicalHistoryNew" value="<s:property value="inputTempNew.medicalHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showdetectionMan" title="选择检测者"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="detectionUserFun" hasSetFun="true"
						documentId="sampleInputTemp_detectionMan_id"
						documentName="sampleInputTemp_detectionMan_name" />

			<!--	<td class="label-title">检测者</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="15" readonly="readOnly" id="sampleInputTemp_detectionMan_name" name="sampleInputTemp.detectionMan.name" value="<s:property value="sampleInputTemp.detectionMan.name"/>" />
						<input type="hidden" id="sampleInputTemp_detectionMan_id" name="sampleInputTemp.detectionMan.id" value="<s:property value="sampleInputTemp.detectionMan.id"/>">
						<img alt='选择检测者' id='showdetectionMan' src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
 			-->
					<td class="label-title">检测者</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_detectionMan_id" name="sampleInputTemp.detectionMan.id"  value="<s:property value="sampleInputTemp.detectionMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleInputTemp_detectionMan_name" name="sampleInputTemp.detectionMan.name"  value="<s:property value="sampleInputTemp.detectionMan.name"/>" class="text input" />
  						<input type="hidden" id="inputTemp_detectionMan_id" name="inputTemp.detectionMan.id" value="<s:property value="inputTemp.detectionMan.id"/>">
  						<input type="hidden" id="inputTemp_detectionMan_name" name="inputTemp.detectionMan.name" value="<s:property value="inputTemp.detectionMan.name"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="detectionMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('detectionManNew').value=document.getElementById('detectionMan').options[document.getElementById('detectionMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.detectionMan.name"/>"><s:property value="sampleInputTemp.detectionMan.name"/></option> 
							<option value="<s:property value="inputTemp.detectionMan.name"/>"><s:property value="inputTemp.detectionMan.name"/></option>
						</select> 
						<input type="text" name="inputTempNew.detectionMan.name" id="detectionManNew" value="<s:property value="inputTempNew.detectionMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="inputTempNew.detectionMan.id" id="detectionManIdNew" value="<s:property value="inputTempNew.detectionMan.id"/>"/>
                   	</td>

					<g:LayOutWinTag buttonId="showreportMan" title="选择报告者"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="reportUserFun" hasSetFun="true"
						documentId="sampleInputTemp_reportMan_id"
						documentName="sampleInputTemp_reportMan_name" />

			<!-- 	<td class="label-title">报告者</td>
					<td class="requiredcolumn" nowrap width="10px">
						</td>
					<td align="left">
						<input type="text" size="15" readonly="readOnly" id="sampleInputTemp_reportMan_name" name="sampleInputTemp.reportMan.name" value="<s:property value="sampleInputTemp.reportMan.name"/>" />
						<input type="hidden" id="sampleInputTemp_reportMan_id" name="sampleInputTemp.reportMan.id"  value="<s:property value="sampleInputTemp.reportMan.id"/>">
						<img alt='选择报告者' id='showreportMan' src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
			 -->
					<td class="label-title">报告者</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reportMan_id" name="sampleInputTemp.reportMan.id"  value="<s:property value="sampleInputTemp.reportMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleInputTemp_reportMan_name" name="sampleInputTemp.reportMan.name"  value="<s:property value="sampleInputTemp.reportMan.name"/>" class="text input" />
  						<input type="hidden" id="inputTemp_reportMan_id" name="inputTemp.reportMan.id" value="<s:property value="inputTemp.reportMan.id"/>">
  						<input type="hidden" id="inputTemp_reportMan_name" name="inputTemp.reportMan.name" value="<s:property value="inputTemp.reportMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reportMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportManNew').value=document.getElementById('reportMan').options[document.getElementById('reportMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.reportMan.name"/>"><s:property value="sampleInputTemp.reportMan.name"/></option> 
							<option value="<s:property value="inputTemp.reportMan.name"/>"><s:property value="inputTemp.reportMan.name"/></option>
						</select> 
						<input type="text" name="inputTempNew.reportMan.name" id="reportManNew" value="<s:property value="inputTempNew.reportMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="inputTempNew.reportMan.id" id="reportManIdNew" value="<s:property value="inputTempNew.reportMan.id"/>"/>
                   	</td>

					<g:LayOutWinTag buttonId="showauditMan" title="选择审核者"
						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="auditUserFun" hasSetFun="true"
						documentId="sampleInputTemp_auditMan_id"
						documentName="sampleInputTemp_auditMan_name" />

			<!--  	<td class="label-title">审核者</td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left">
						<input type="text" size="15" readonly="readOnly" id="sampleInputTemp_auditMan_name" name="sampleInputTemp.auditMan.name" value="<s:property value="sampleInputTemp.auditMan.name"/>" />
						<input type="hidden" id="sampleInputTemp_auditMan_id" name="sampleInputTemp.auditMan.id" value="<s:property value="sampleInputTemp.auditMan.id"/>">
						<img alt='选择审核者' id='showauditMan' src='${ctx}/images/img_lookup.gif' class='detail' />
					</td>
			-->		
					<td class="label-title">审核者</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_auditMan_id" name="sampleInputTemp.auditMan.id"  value="<s:property value="sampleInputTemp.auditMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleInputTemp_auditMan_name" name="sampleInputTemp.auditMan.name"  value="<s:property value="sampleInputTemp.auditMan.name"/>" class="text input" />
  						<input type="hidden" id="inputTemp_auditMan_id" name="inputTemp.auditMan.id" value="<s:property value="inputTemp.auditMan.id"/>">
  						<input type="hidden" id="inputTemp_auditMan_name" name="inputTemp.auditMan.name" value="<s:property value="inputTemp.auditMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="auditMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('auditManNew').value=document.getElementById('auditMan').options[document.getElementById('auditMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.auditMan.name"/>"><s:property value="sampleInputTemp.auditMan.name"/></option> 
							<option value="<s:property value="inputTemp.auditMan.name"/>"><s:property value="inputTemp.auditMan.name"/></option>
						</select> 
						<input type="text" name="inputTempNew.auditMan.name" id="auditManNew" value="<s:property value="inputTempNew.auditMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="inputTempNew.auditMan.id" id="auditManIdNew" value="<s:property value="inputTempNew.auditMan.id"/>"/>
                   	</td>
					
				</tr>
				<tr>
					<td class="label-title">报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_reportDate" name="sampleInputTemp.reportDate" value="<s:property value="sampleInputTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_reportDate" name="inputTemp.reportDate"  value="<s:property value="inputTemp.reportDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reportDate"/>"><s:property value="sampleInputTemp.reportDate"/></option> 
									<option value="<s:property value="inputTemp.reportDate"/>"><s:property value="inputTemp.reportDate"/></option>
								</select> 
								
								<input type="text" name="inputTempNew.reportDate" id="reportDateNew" value="<s:property value="inputTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
					<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left"> 
                   		<select name="inputTempNew.nextStepFlow" id="sampleInputTemp_nextStepFlow">
							<option value="" <s:if test="inputTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="inputTempNew.nextStepFlow==0">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="inputTempNew.nextStepFlow==1">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
			</table>
			<input type="hidden" id="id_parent_hidden"
				value="<s:property value="sampleInputTemp.id"/>" />
		</form>
		<!-- <div id="tabs">
            <ul>
           	</ul>  -->
	</div>
</body>
</html>
