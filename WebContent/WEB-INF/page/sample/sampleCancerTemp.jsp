﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleCancerTemp.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" >订单号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_orderNumber"
                   	 name="orderNumber" searchField="true" title="订单号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >编码</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_id"
                   	 name="id" searchField="true" title="编码"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >姓名</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleCancerTemp_name"
                   	 name="name" searchField="true" title="姓名"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >性别</td>
                   	<td align="left"  >
					<select id="sampleCancerTemp_gender" name="gender" searchField="true" class="input-10-length" >
								<option value="1" <s:if test="sampleCancerTemp.gender==1">selected="selected"</s:if>>男</option>
								<option value="0" <s:if test="sampleCancerTemp.gender==0">selected="selected"</s:if>>女</option>
					</select>
 					
                   	</td>
               	 	<td class="label-title" >出生日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startbirthDate" name="startbirthDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="birthDate1" name="birthDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endbirthDate" name="endbirthDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="birthDate2" name="birthDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >确诊日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startdiagnosisDate" name="startdiagnosisDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="diagnosisDate1" name="diagnosisDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="enddiagnosisDate" name="enddiagnosisDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="diagnosisDate2" name="diagnosisDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >肿瘤类别及分期</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="250" id="sampleCancerTemp_tumorTypeAndStage"
                   	 name="tumorTypeAndStage" searchField="true" title="肿瘤类别及分期"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showinspectionDepartment" title="选择送检科室"
				hasHtmlFrame="true"
				html="${ctx}/sample/dicTypeSelect.action"
				isHasSubmit="false" functionName="DicTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_inspectionDepartment').value=rec.get('id');
				document.getElementById('sampleCancerTemp_inspectionDepartment_name').value=rec.get('name');" />
               	 	<td class="label-title" >送检科室</td>
                   	<td align="left"  >
 						<input type="text" size="50"   id="sampleCancerTemp_inspectionDepartment_name" searchField="true"  name="inspectionDepartment.name"  value="" class="text input" />
 						<input type="hidden" id="sampleCancerTemp_inspectionDepartment" name="sampleCancerTemp.inspectionDepartment.id"  value="" > 
 						<img alt='选择送检科室' id='showinspectionDepartment' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcrmProduct" title="选择检测项目"
				hasHtmlFrame="true"
				html="${ctx}/sample/productSelect.action"
				isHasSubmit="false" functionName="ProductFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_crmProduct').value=rec.get('id');
				document.getElementById('sampleCancerTemp_crmProduct_name').value=rec.get('name');" />
               	 	<td class="label-title" >检测项目</td>
                   	<td align="left"  >
 						<input type="text" size="50"   id="sampleCancerTemp_crmProduct_name" searchField="true"  name="crmProduct.name"  value="" class="text input" />
 						<input type="hidden" id="sampleCancerTemp_crmProduct" name="sampleCancerTemp.crmProduct.id"  value="" > 
 						<img alt='选择检测项目' id='showcrmProduct' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >采样日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startsamplingDate" name="startsamplingDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="samplingDate1" name="samplingDate##@@##1"  searchField="true" /> 
 						<input type="text" class="Wdate" readonly="readonly" id="endsamplingDate" name="endsamplingDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="samplingDate2" name="samplingDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showsamplingLocation" title="选择采样部位"
				hasHtmlFrame="true"
				html="${ctx}/sample/dicTypeSelect.action"
				isHasSubmit="false" functionName="DicTypeFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_samplingLocation').value=rec.get('id');
				document.getElementById('sampleCancerTemp_samplingLocation_name').value=rec.get('name');" />
               	 	<td class="label-title" >采样部位</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleCancerTemp_samplingLocation_name" searchField="true"  name="samplingLocation.name"  value="" class="text input" />
 						<input type="hidden" id="sampleCancerTemp_samplingLocation" name="sampleCancerTemp.samplingLocation.id"  value="" > 
 						<img alt='选择采样部位' id='showsamplingLocation' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_samplingNumber"
                   	 name="samplingNumber" searchField="true" title="样本编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >是否病理确认</td>
                   	<td align="left"  >
					<select id="sampleCancerTemp_pathologyConfirmed" name="pathologyConfirmed" searchField="true" class="input-10-length" >
								<option value="1" <s:if test="sampleCancerTemp.pathologyConfirmed==1">selected="selected"</s:if>>是</option>
								<option value="0" <s:if test="sampleCancerTemp.pathologyConfirmed==0">selected="selected"</s:if>>否</option>
					</select>
 					
                   	</td>
               	 	<td class="label-title" >血液采样时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startbloodSampleDate" name="startbloodSampleDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="bloodSampleDate1" name="bloodSampleDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endbloodSampleDate" name="endbloodSampleDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="bloodSampleDate2" name="bloodSampleDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >血浆分离时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startplasmapheresisDate" name="startplasmapheresisDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="plasmapheresisDate1" name="plasmapheresisDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endplasmapheresisDate" name="endplasmapheresisDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="plasmapheresisDate2" name="plasmapheresisDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcommissioner" title="选择负责人"
					hasHtmlFrame="true" hasSetFun="true"
					html="${ctx}/core/user/userSelect.action"
					width="document.body.clientWidth/1.5" isHasSubmit="false"
					functionName="dutyUserFun" 
					documentId="sampleCancerTemp_commissioner"
					documentName="sampleCancerTemp_commissioner_name" />
			
				
			
			
               	 	<td class="label-title" >销售代表</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleCancerTemp_commissioner_name"  value="<s:property value="sampleCancerTemp.commissioner.name"/>" class="text input readonlyfalse" readonly="readOnly"  />
 						<input type="hidden" id="sampleCancerTemp_commissioner" name="sampleCancerTemp.commissioner.id"  value="<s:property value="sampleCancerTemp.commissioner.id"/>" > 
 						<img alt='选择销售代表' id='showcommissioner' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >收样日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startreceivedDate" name="startreceivedDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receivedDate1" name="receivedDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endreceivedDate" name="endreceivedDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="receivedDate2" name="receivedDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >样本类型id</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleCancerTemp_sampleTypeId"
                   	 name="sampleTypeId" searchField="true" title="样本类型id"    />
                   	
					
 
                  
                   	
                   	  
 					<input type="hidden" id="sampleCancerTemp_sampleTypeId" name="sampleTypeId" searchField="true" value=""/>"
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >样本类型</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleCancerTemp_sampleTypeName"
                   	 name="sampleTypeName" searchField="true" title="样本类型"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showsampleOrder" title="选择关联订单"
				hasHtmlFrame="true"
				html="${ctx}/sample/sampleOrderSelect.action"
				isHasSubmit="false" functionName="SampleOrderFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_sampleOrder').value=rec.get('id');
				document.getElementById('sampleCancerTemp_sampleOrder_name').value=rec.get('name');" />
               	 	<td class="label-title" >关联订单</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleCancerTemp_sampleOrder_name" searchField="true"  name="sampleOrder.name"  value="" class="text input" />
 						<input type="hidden" id="sampleCancerTemp_sampleOrder" name="sampleCancerTemp.sampleOrder.id"  value="" > 
 						<img alt='选择关联订单' id='showsampleOrder' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >实验室内部样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleCancerTemp_sampleCode"
                   	 name="sampleCode" searchField="true" title="实验室内部样本编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >家属联系人</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_family"
                   	 name="family" searchField="true" title="家属联系人"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >家属联系人电话</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_familyPhone"
                   	 name="familyPhone" searchField="true" title="家属联系人电话"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >家属联系人地址</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleCancerTemp_familySite"
                   	 name="familySite" searchField="true" title="家属联系人地址"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >医疗机构</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_medicalInstitutions"
                   	 name="medicalInstitutions" searchField="true" title="医疗机构"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >医疗机构联系电话</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_medicalInstitutionsPhone"
                   	 name="medicalInstitutionsPhone" searchField="true" title="医疗机构联系电话"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >医疗机构联系地址</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleCancerTemp_medicalInstitutionsSite"
                   	 name="medicalInstitutionsSite" searchField="true" title="医疗机构联系地址"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >主治医生</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_attendingDoctor"
                   	 name="attendingDoctor" searchField="true" title="主治医生"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >主治医生联系电话</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_attendingDoctorPhone"
                   	 name="attendingDoctorPhone" searchField="true" title="主治医生联系电话"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >主治医生联系地址</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleCancerTemp_attendingDoctorSite"
                   	 name="attendingDoctorSite" searchField="true" title="主治医生联系地址"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="250" id="sampleCancerTemp_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true"
				html="${ctx}/sample/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_createUser').value=rec.get('id');
				document.getElementById('sampleCancerTemp_createUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="50"   id="sampleCancerTemp_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleCancerTemp_createUser" name="sampleCancerTemp.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >创建时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showconfirmUser" title="选择审批人"
				hasHtmlFrame="true"
				html="${ctx}/sample/userSelect.action"
				isHasSubmit="false" functionName="UserFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleCancerTemp_confirmUser').value=rec.get('id');
				document.getElementById('sampleCancerTemp_confirmUser_name').value=rec.get('name');" />
               	 	<td class="label-title" >审批人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleCancerTemp_confirmUser_name" searchField="true"  name="confirmUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleCancerTemp_confirmUser" name="sampleCancerTemp.confirmUser.id"  value="" > 
 						<img alt='选择审批人' id='showconfirmUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" >审批时间</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startconfirmDate" name="startconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate1" name="confirmDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endconfirmDate" name="endconfirmDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="confirmDate2" name="confirmDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_state"
                   	 name="state" searchField="true" title="状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >状态名称</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleCancerTemp_stateName"
                   	 name="stateName" searchField="true" title="状态名称"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleCancerTemp_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleCancerTemp_tree_page"></div>
</body>
</html>



