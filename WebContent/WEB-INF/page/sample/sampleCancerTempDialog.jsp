﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleCancerTempDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">订单号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_orderNumber" searchField="true" name="orderNumber"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">编码</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">姓名</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">性别</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="sampleCancerTemp_gender" searchField="true" name="gender"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">出生日期</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_birthDate" searchField="true" name="birthDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">确诊日期</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_diagnosisDate" searchField="true" name="diagnosisDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">肿瘤类别及分期</td>
               	<td align="left">
                    		<input type="text" maxlength="250" id="sampleCancerTemp_tumorTypeAndStage" searchField="true" name="tumorTypeAndStage"  class="input-20-length"></input>
               	</td>
               	<td class="label-title">分期</td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="sampleCancerTemp_sampleStage" searchField="true" name="sampleStage"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">送检科室</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_inspectionDepartment" searchField="true" name="inspectionDepartment"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">检测项目</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_crmProduct" searchField="true" name="crmProduct"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">采样日期</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_samplingDate" searchField="true" name="samplingDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">采样部位</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_samplingLocation" searchField="true" name="samplingLocation"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_samplingNumber" searchField="true" name="samplingNumber"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">是否病理确认</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_pathologyConfirmed" searchField="true" name="pathologyConfirmed"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">血液采样时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_bloodSampleDate" searchField="true" name="bloodSampleDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">血浆分离时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_plasmapheresisDate" searchField="true" name="plasmapheresisDate"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">销售代表</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_commissioner" searchField="true" name="commissioner"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">收样日期</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_receivedDate" searchField="true" name="receivedDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本类型id</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_sampleTypeId" searchField="true" name="sampleTypeId"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本类型</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_sampleTypeName" searchField="true" name="sampleTypeName"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">关联订单</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_sampleOrder" searchField="true" name="sampleOrder"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">实验室内部样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_sampleCode" searchField="true" name="sampleCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">家属联系人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_family" searchField="true" name="family"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">家属联系人电话</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_familyPhone" searchField="true" name="familyPhone"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">家属联系人地址</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_familySite" searchField="true" name="familySite"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">医疗机构</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_medicalInstitutions" searchField="true" name="medicalInstitutions"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">医疗机构联系电话</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_medicalInstitutionsPhone" searchField="true" name="medicalInstitutionsPhone"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">医疗机构联系地址</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_medicalInstitutionsSite" searchField="true" name="medicalInstitutionsSite"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">主治医生</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_attendingDoctor" searchField="true" name="attendingDoctor"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">主治医生联系电话</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_attendingDoctorPhone" searchField="true" name="attendingDoctorPhone"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">主治医生联系地址</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_attendingDoctorSite" searchField="true" name="attendingDoctorSite"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="250" id="sampleCancerTemp_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">创建人</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleCancerTemp_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">创建时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">审批人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_confirmUser" searchField="true" name="confirmUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">审批时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_confirmDate" searchField="true" name="confirmDate"  class="input-20-length"></input>
               	</td>
           	 	
			</tr>
			<tr>
				<td class="label-title">状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">状态名称</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleCancerTemp_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_sampleCancerTemp_div"></div>
   		
</body>
</html>



