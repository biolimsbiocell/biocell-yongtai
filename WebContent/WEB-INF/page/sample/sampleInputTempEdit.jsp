﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInfo&id=${sampleInputTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/js/sample/sampleInputTempEdit.js"></script>
	
	<s:if test="sampleInputTemp.sampleInfo.upLoadAccessory.id != null">
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputTempItemImg"><img onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
	</s:if>
	<s:if test="">
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputTempItemImg"><img src=""></div>
	</s:if>
	
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}"> 
		<input type="hidden" id="id" value="${requestScope.id}">
		<input type="hidden" id="path" value="${requestScope.path}"> 
		<input type="hidden" id="fname" value="${requestScope.fname}">
		<input type="hidden"  id="str" value="${requestScope.str}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" id="saveType" value="cq" >
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_id" name="sampleInputTemp.id" title="编号" value="<s:property value="sampleInputTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleInputTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   	</td>
                   	
                   	
					<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="inputTempNew.sampleInfo.code" title="样本编号" value="<s:property value="inputTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="inputTempNew.code" title="样本编号" value="<s:property value="inputTempNew.code"/>" />
                   	</td>
				
					<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="inputTempNew_name" name="inputTempNew.name" title="描述" value="<s:property value="inputTempNew.name"/>" class="sampleInputTemp_name"/>
                   	</td>
					
<!-- 					<td class="label-title" >检测项目</td> -->
<!--                    	<td class="requiredcolumn" nowrap width="10px" > -->
<%--                    		 --%>
<!--                    	</td>	 -->
<!--                    	<td align="left"  > -->
<!--  						<input type="hidden" size="20"   id="sampleInputTemp_productId" searchField="true" name="inputTempNew.productId"  value="" class="text input" /> -->
<%--  						<input type="text" id="sampleInputTemp_productName" name="inputTempNew.productName" value="<s:property value="inputTempNew.productName"/>" readonly="readonly" data="productName" >  --%>
<%--  						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		 --%>
<!--                    	</td> -->
<!--                     <tr> -->
                    
                    <td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_productId" name="sampleInputTemp.productId"  value="<s:property value="sampleInputTemp.productId"/>"/>
  						<input type="hidden" id="sampleInputTemp_productName" name="sampleInputTemp.productName" value="<s:property value="sampleInputTemp.productName"/>"/>
  						<input type="hidden" id="inputTemp_productId" name="inputTemp.productId" value="<s:property value="inputTemp.productId"/>">
  						<input type="hidden" id="inputTemp_productName" name="inputTemp.productName" value="<s:property value="inputTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.productName"/>"><s:property value="sampleInputTemp.productName"/></option> 
							<option value="<s:property value="inputTemp.productName"/>"><s:property value="inputTemp.productName"/></option>
						</select> 
						<input type="text" name="inputTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="inputTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="inputTempNew.productId" id="productIdNew" value="<s:property value="inputTempNew.productId"/>" />
                   	</td>
                </tr>
         		<tr>
         	
<!--                    	<td class="label-title">样本类型</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%-- 						<input type="text" size="20" readonly="readOnly" name="inputTempNew.sampleType.name" id="sampleInputTemp_sampleType_name" data="sampleType" value="<s:property value="inputTempNew.sampleType.name"/>" /> --%>
<%-- 						<s:hidden id="sampleInputTemp_sampleType_id" name="inputTempNew.sampleType.id"></s:hidden> --%>
<%-- 						<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_sampleType_id" name="sampleInputTemp.sampleType.id"  value="<s:property value="sampleInputTemp.sampleType.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleInputTemp_sampleType_name" name="sampleInputTemp.sampleType.name"  value="<s:property value="sampleInputTemp.sampleType.name"/>" class="text input" />
  						<input type="hidden" id="inputTemp_sampleType_id" name="inputTemp.sampleType.id" value="<s:property value="inputTemp.sampleType.id"/>">
  						<input type="hidden" id="inputTemp_sampleType_name" name="inputTemp.sampleType.name" value="<s:property value="inputTemp.sampleType.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.sampleType.name"/>"><s:property value="sampleInputTemp.sampleType.name"/></option> 
							<option value="<s:property value="inputTemp.sampleType.name"/>"><s:property value="inputTemp.sampleType.name"/></option>
						</select> 
						<input type="text" name="inputTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="inputTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
            			<input type="hidden" name="inputTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="inputTempNew.sampleType.id"/>" />
                   	</td>
                   	
	
<!--               		<td class="label-title">送检医院</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25"  data="hospital" id="sampleInputTemp_hospital" name="inputTempNew.hospital" title="送检医院" value="<s:property value="inputTempNew.hospital"/>" /> --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_hospital" name="sampleInputTemp.hospital"  value="<s:property value="sampleInputTemp.hospital"/>"/>
  						<input type="hidden" id="inputTemp_hospital" name="inputTemp.hospital" value="<s:property value="inputTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital" style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.hospital"/>"><s:property value="sampleInputTemp.hospital"/></option> 
							<option value="<s:property value="inputTemp.hospital"/>"><s:property value="inputTemp.hospital"/></option>
						</select> 
						<input type="text" name="inputTempNew.hospital" id="hospitalNew" value="<s:property value="inputTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
                   	
<!--                    	<td class="label-title">送检医生</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="doctor" id="sampleInputTemp_doctor" name="inputTempNew.doctor" title="送检医生" value="<s:property value="inputTempNew.doctor"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_doctor" name="sampleInputTemp.doctor"  value="<s:property value="sampleInputTemp.doctor"/>"/>
  						<input type="hidden" id="inputTemp_doctor" name="inputTemp.doctor" value="<s:property value="inputTemp.doctor"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="doctor" style="width:152px;height:28px;" onChange="javascript:document.getElementById('doctorNew').value=document.getElementById('doctor').options[document.getElementById('doctor').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.doctor"/>"><s:property value="sampleInputTemp.doctor"/></option> 
									<option value="<s:property value="inputTemp.doctor"/>"><s:property value="inputTemp.doctor"/></option>
								</select> 
								<input type="text" name="inputTempNew.doctor" id="doctorNew" value="<s:property value="inputTempNew.doctor"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
         		</tr>
         		<tr>
         		
         			<td class="label-title">审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<input type="hidden" size="20" maxlength="25" id="inputTempNew_createUser_id" name="inputTempNew.createUser.id" title="审核人" value="<s:property value="inputTempNew.createUser.id"/>" readonly="readonly" class="text input readonlytrue" />
	                   	<input type="text" size="20" maxlength="25" id="inputTempNew_createUser_name" name="inputTempNew.createUser.name" title="审核人" value="<s:property value="inputTempNew.createUser.name"/>" readonly="readonly" class="text input readonlytrue"/>
						<input type="hidden" size="20" maxlength="25" id="inputTempNew_createUser1" name="inputTempNew.createUser1" title="审核人1"  value="<s:property value="inputTempNew.createUser1"/>" />
                   	</td>
						
<!-- 					<td class="label-title">地区</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="area" id="sampleInputTemp_area" name="inputTempNew.area" title="地区" value="<s:property value="inputTempNew.area"/>" /> --%>
<!--                    	</td> -->
					
					<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_area" name="sampleInputTemp.area"  value="<s:property value="sampleInputTemp.area"/>"/>
  						<input type="hidden" id="inputTemp_area" name="inputTemp.area" value="<s:property value="inputTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.area"/>"><s:property value="sampleInputTemp.area"/></option> 
							<option value="<s:property value="inputTemp.area"/>"><s:property value="inputTemp.area"/></option>
						</select> 
						<input type="text" name="inputTempNew.area" id="areaNew" value="<s:property value="inputTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					

<!--               		<td class="label-title">接收日期</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<!--                    	 <input type="text" size="20" maxlength="25" data="acceptDate" id="sampleInputTemp_acceptDate" name="inputTempNew.acceptDate" title="接收日期" Class="Wdate" readonly="readonly" -->
<%-- 						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.acceptDate" format="yyyy-MM-dd" />" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_acceptDate" name="sampleInputTemp.acceptDate" value="<s:property value="sampleInputTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_acceptDate" name="inputTemp.acceptDate" value="<s:property value="inputTemp.acceptDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.acceptDate"/>"><s:property value="sampleInputTemp.acceptDate"/></option> 
							<option value="<s:property value="inputTemp.acceptDate"/>"><s:property value="inputTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="inputTempNew.acceptDate" id="acceptDateNew" value="<s:property value="inputTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                </tr>
               
                <tr>
<!--                 	<td class="label-title">取样时间</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<!--                    	 <input type="text" size="20" maxlength="25" data="sendDate" id="sampleInputTemp_sendDate" name="inputTempNew.sendDate" title="取样时间" Class="Wdate" readonly="readonly" -->
<%-- 						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:new Date()})" value="<s:date name="inputTempNew.sendDate" format="yyyy-MM-dd HH:mm:ss"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">取样时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_sendDate" name="sampleInputTemp.sendDate" value="<s:property value="sampleInputTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_sendDate" name="inputTemp.sendDate"  value="<s:property value="inputTemp.sendDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.sendDate"/>"><s:property value="sampleInputTemp.sendDate"/></option> 
									<option value="<s:property value="inputTemp.sendDate"/>"><s:property value="inputTemp.sendDate"/></option>
								</select> 
								
								<input type="text" name="inputTempNew.sendDate" id="sendDateNew" value="<s:property value="inputTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})" value="<s:date name="inputTempNew.sendDate" format="yyyy-MM-dd HH:mm:ss"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
              	<tr>
              	
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
					
				</tr>
               	<tr>
               		<td class="label-title">孕妇姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_patientName" name="sampleInputTemp.patientName" value="<s:property value="sampleInputTemp.patientName"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_patientName" name="inputTemp.patientName"  value="<s:property value="inputTemp.patientName"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="aabb" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ccdd').value=document.getElementById('aabb').options[document.getElementById('aabb').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<!--下面的option的样式是为了使字体为灰色，只是视觉问题，看起来像是注释一样--> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.patientName"/>"><s:property value="sampleInputTemp.patientName"/></option> 
									<option value="<s:property value="inputTemp.patientName"/>"><s:property value="inputTemp.patientName"/></option>
								</select> 
								<input type="text" name="inputTempNew.patientName" id="ccdd" value="<s:property value="inputTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>

               		
               	 	<td class="label-title">性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_gender" name="sampleInputTemp.gender" value="<s:property value="sampleInputTemp.gender"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_gender" name="inputTemp.gender"  value="<s:property value="inputTemp.gender"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.gender" id="inputTempNew_gender">
							<option value="" <s:if test="inputTempNew.gender==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.gender==0">selected="selected" </s:if>>女</option>
	    					<option value="1" <s:if test="inputTempNew.gender==1">selected="selected" </s:if>>男</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
<!--                    	<td class="label-title">年龄</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--  						<input type="text" size="20" id="sampleInputTemp_age" name="inputTempNew.age" data="age" value="<s:property value="inputTempNew.age"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_age" name="sampleInputTemp.age" value="<s:property value="sampleInputTemp.age"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_age" name="inputTemp.age"  value="<s:property value="inputTemp.age"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="age" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.age"/>"><s:property value="sampleInputTemp.age"/></option> 
									<option value="<s:property value="inputTemp.age"/>"><s:property value="inputTemp.age"/></option>
								</select> 
								<input type="text" name="inputTempNew.age" id="ageNew" value="<s:property value="inputTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
				</tr>
				<tr>
				
                   	<td class="label-title">出生日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_birthday" name="sampleInputTemp.birthday" value="<s:property value="sampleInputTemp.birthday"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_birthday" name="inputTemp.birthday"  value="<s:property value="inputTemp.birthday"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   	  	<input type="text" size="20" maxlength="25" id="birthday" name="inputTempNew.birthday" title="出生日期" Class="Wdate" readonly="readonly" 
                   	  		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.birthday" format="yyyy-MM-dd"/>" />
                   	</td>
                   	
                   	<td class="label-title">体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_weight" name="sampleInputTemp.weight" value="<s:property value="sampleInputTemp.weight"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_weight" name="inputTemp.weight"  value="<s:property value="inputTemp.weight"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="weight" style="width:152px;height:28px;" onChange="javascript:document.getElementById('weightNew').value=document.getElementById('weight').options[document.getElementById('weight').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.weight"/>"><s:property value="sampleInputTemp.weight"/></option> 
									<option value="<s:property value="inputTemp.weight"/>"><s:property value="inputTemp.weight"/></option>
								</select> 
								<input type="text" name="inputTempNew.weight" id="weightNew" value="<s:property value="inputTempNew.weight"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
                   	
<!--                    	</td> -->
<!--                    	<td class="label-title">体重</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="weight" id="sampleInputTemp_weight" name="inputTempNew.weight" title="体重" value="<s:property value="inputTempNew.weight"/>" /> --%>
<!--                    	</td> -->
					<td class="label-title">孕周</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_gestationalAge" name="sampleInputTemp.gestationalAge" value="<s:property value="sampleInputTemp.gestationalAge"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_gestationalAge" name="inputTemp.gestationalAge"  value="<s:property value="inputTemp.gestationalAge"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="gestationalAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('gestationalAgeNew').value=document.getElementById('gestationalAge').options[document.getElementById('gestationalAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.gestationalAge"/>"><s:property value="sampleInputTemp.gestationalAge"/></option> 
									<option value="<s:property value="inputTemp.gestationalAge"/>"><s:property value="inputTemp.gestationalAge"/></option>
								</select> 
								<input type="text" name="inputTempNew.gestationalAge" id="gestationalAgeNew" value="<s:property value="inputTempNew.gestationalAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
				<tr>
<!-- 					<td class="label-title">证件类型</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--  						<input type="text" size="20" readonly="readOnly" id="sampleInputTemp_voucherType_name" data="voucherType" name="inputTempNew.voucherType.name" value="<s:property value="inputTempNew.voucherType.name"/>" /> --%>
<%--  						<input type="hidden" id="sampleInputTemp_voucherType" name="inputTempNew.voucherType.id" value="<s:property value="inputTempNew.voucherType.id"/>">  --%>
<%--  						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()" class='detail' />                   		 --%>
<!--                    	</td> -->
                     
                   	<td class="label-title">证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_voucherType_name" name="sampleInputTemp.voucherType.name" value="<s:property value="sampleInputTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleInputTemp_voucherType_id"   name="sampleInputTemp.voucherType.id" value="<s:property value="sampleInputTemp.voucherType.id"/>"> 
               	 		<input type="hidden" id="inputTemp_voucherType_name" name="inputTemp.voucherType.name" value="<s:property value="inputTemp.voucherType.name"/>" />
 						<input type="hidden" id="inputTemp_voucherType_id"   name="inputTemp.voucherType.id" value="<s:property value="inputTemp.voucherType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="voucherType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherTypeNew').value=document.getElementById('voucherType').options[document.getElementById('voucherType').selectedIndex].value;this.nextSibling.value=this.value;c();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.voucherType.name"/>"><s:property value="sampleInputTemp.voucherType.name"/></option> 
							<option value="<s:property value="inputTemp.voucherType.name"/>"><s:property value="inputTemp.voucherType.name"/></option>
						</select> 
						<input type="text" name="inputTempNew.voucherType.name" id="voucherTypeNew" onfocus="voucherTypeFun()" value="<s:property value="inputTempNew.voucherType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="inputTempNew.voucherType.id" id="voucherTypeIdNew" value="<s:property value="inputTempNew.voucherType.id"/>">
                   	</td>
                   	
                   	
                   	<td class="label-title">证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_voucherCode"   name="sampleInputTemp.voucherCode" value="<s:property value="sampleInputTemp.voucherCode"/>"> 
               	 		<input type="hidden" id="inputTemp_voucherCode" name="inputTemp.voucherCode" value="<s:property value="inputTemp.voucherCode"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="voucherCode" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherCodeAgeNew').value=document.getElementById('voucherCode').options[document.getElementById('voucherCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.voucherCode"/>"><s:property value="sampleInputTemp.voucherCode"/></option> 
									<option value="<s:property value="inputTemp.voucherCode"/>"><s:property value="inputTemp.voucherCode"/></option>
								</select> 
								<input type="text" name="inputTempNew.voucherCode" id="voucherCodeAgeNew" value="<s:property value="inputTempNew.voucherCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
            		
            		<td class="label-title">手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_phoneNum"   name="sampleInputTemp.phoneNum" value="<s:property value="sampleInputTemp.phoneNum"/>"> 
               	 		<input type="hidden" id="inputTemp_phoneNum" name="inputTemp.phoneNum" value="<s:property value="inputTemp.phoneNum"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="phoneNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNumNew').value=document.getElementById('phoneNum').options[document.getElementById('phoneNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.phoneNum"/>"><s:property value="sampleInputTemp.phoneNum"/></option> 
									<option value="<s:property value="inputTemp.phoneNum"/>"><s:property value="inputTemp.phoneNum"/></option>
								</select> 
								<input type="text" name="inputTempNew.phoneNum" id="phoneNumNew" value="<s:property value="inputTempNew.phoneNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
			</tr>
			<tr>
                   	
<!--                    	<td class="label-title">家庭住址</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--                    	 	<input type="text" size="20" maxlength="25" data="address" id="sampleInputTemp_address" name="inputTempNew.address" title="家庭住址" onblur="checkAddress()" value="<s:property value="inputTempNew.address"/>" /> --%>
<%--                    	 	<img class='requiredimage' src='${ctx}/images/required.gif' /> --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_address"   name="sampleInputTemp.address" value="<s:property value="sampleInputTemp.address"/>"> 
               	 		<input type="hidden" id="inputTemp_address" name="inputTemp.address" value="<s:property value="inputTemp.address"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="address" style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.address"/>"><s:property value="sampleInputTemp.address"/></option> 
									<option value="<s:property value="inputTemp.address"/>"><s:property value="inputTemp.address"/></option>
								</select> 
								<input type="text" name="inputTempNew.address" id="addressNew" value="<s:property value="inputTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
<!--                    	<td class="label-title">病床号</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		 --%>
<!--                	 	</td>            	 	 -->
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="inHosNum" id="sampleInputTemp_inHosNum" name="inputTempNew.inHosNum" title="病床号" value="<s:property value="inputTempNew.inHosNum"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">病床号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_inHosNum"   name="sampleInputTemp.inHosNum" value="<s:property value="sampleInputTemp.inHosNum"/>"> 
               	 		<input type="hidden" id="inputTemp_inHosNum" name="inputTemp.inHosNum" value="<s:property value="inputTemp.inHosNum"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="inHosNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('inHosNumNew').value=document.getElementById('inHosNum').options[document.getElementById('inHosNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.inHosNum"/>"><s:property value="sampleInputTemp.inHosNum"/></option> 
									<option value="<s:property value="inputTemp.inHosNum"/>"><s:property value="inputTemp.inHosNum"/></option>
								</select> 
								<input type="text" name="inputTempNew.inHosNum" id="inHosNumNew" value="<s:property value="inputTempNew.inHosNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
<!--                    	<td class="label-title">推荐人</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		<input type="hidden" id="sampleInputTemp_linkman_name"   name="sampleInputTemp.linkman_name" value="<s:property value="sampleInputTemp.linkman_name"/>">  --%>
<%--                	 		<input type="hidden" id="sampleInputTemp_linkman_id"   name="sampleInputTemp.linkman_id" value="<s:property value="sampleInputTemp.linkman_id"/>">  --%>
<%--                	 		<input type="hidden" id="inputTemp_linkman_name" name="inputTemp.linkman_name" value="<s:property value="inputTemp.linkman_name"/>" /> --%>
<%--                	 		<input type="hidden" id="inputTemp_linkman_id" name="inputTemp.linkman_id" value="<s:property value="inputTemp.linkman_id"/>" /> --%>
<%--                	 		 --%>
<!--                	 	</td>  	 	 -->
<!--                    	<td align="left"> -->
<%--  						<input type="text" size="20" readonly="readOnly" id="inputTempNew_linkman_name" name="inputTempNew.linkman.name" value="<s:property value="inputTempNew.linkman.name"/>" readonly="readOnly" /> --%>
<%--  						<input type="hidden" id="inputTempNew_linkman_id" name="inputTempNew.linkman.id" value="<s:property value="inputTempNew.linkman.id"/>">  --%>
<%--  						<img alt='推荐人' id='showlinkman' src='${ctx}/images/img_lookup.gif' class='detail' />                   		 --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_linkman_name"   name="sampleInputTemp.linkman.name" value="<s:property value="sampleInputTemp.linkman.name"/>"> 
               	 		<input type="hidden" id="sampleInputTemp_linkman_id"   name="sampleInputTemp.linkman.id" value="<s:property value="sampleInputTemp.linkman.id"/>"> 
               	 		<input type="hidden" id="inputTemp_linkman_name" name="inputTemp.linkman.name" value="<s:property value="inputTemp.linkman.name"/>" />
               	 		<input type="hidden" id="inputTemp_linkman_id" name="inputTemp.linkman.id" value="<s:property value="inputTemp.linkman.id"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="linkman" style="width:152px;height:28px;" onChange="javascript:document.getElementById('linkmanNew').value=document.getElementById('linkman').options[document.getElementById('linkman').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.linkman.name"/>"><s:property value="sampleInputTemp.linkman.name"/></option> 
							<option value="<s:property value="inputTemp.linkman.name"/>"><s:property value="inputTemp.linkman.name"/></option>
						</select> 
						<input type="text" name="inputTempNew.linkman.name" id="linkmanNew" value="<s:property value="inputTempNew.linkman.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="inputTempNew.linkman.id" id="linkmanIdNew" value="<s:property value="inputTempNew.linkman.id"/>"/>
                   	</td>
                   	
			</tr>
			<tr>
                   	
<!--                    	<td class="label-title">联系方式</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!-- 	                   	<input type="text" size="20" maxlength="10" data="phone" -->
<!-- 						id="sampleInputTemp_phone" name="inputTempNew.phone" -->
<%-- 						title="联系方式" value="<s:property value="inputTempNew.phone"/>" /> --%>

<!--                    	</td> -->
                   	
                   	<td class="label-title">联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_phone"   name="sampleInputTemp.phone" value="<s:property value="sampleInputTemp.phone"/>"> 
               	 		<input type="hidden" id="inputTemp_phone" name="inputTemp.phone" value="<s:property value="inputTemp.phone"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="phone" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNew').value=document.getElementById('phone').options[document.getElementById('phone').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.phone"/>"><s:property value="sampleInputTemp.phone"/></option> 
									<option value="<s:property value="inputTemp.phone"/>"><s:property value="inputTemp.phone"/></option>
								</select>
								<input type="text" name="inputTempNew.phone" id="phoneNew" value="<s:property value="inputTempNew.phone"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
			</tr>
			<tr>
			
				<td colspan="9">
					<div class="standard-section-header type-title">
						<label>检查情况</label>
					</div>
				</td>
				
			</tr>
			<tr>
<!--                    	<td class="label-title">末次月经</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	 <input type="text" size="20" maxlength="25" data="endMenstruationDate" -->
<!-- 						id="sampleInputTemp_endMenstruationDate" -->
<!-- 						name="inputTempNew.endMenstruationDate" title="末次月经" -->
<!-- 						Class="Wdate" readonly="readonly" -->
<!-- 						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" -->
<%-- 						value="<s:date name="inputTempNew.endMenstruationDate" format="yyyy-MM-dd"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">末次月经</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" id="sampleInputTemp_endMenstruationDate" name="sampleInputTemp.endMenstruationDate" value="<s:property value="sampleInputTemp.endMenstruationDate"/>" />
               	 		<input type="hidden" id="inputTemp_endMenstruationDate" name="inputTemp.endMenstruationDate" value="<s:property value="inputTemp.endMenstruationDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
						<select id="endMenstruationDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('endMenstruationDateNew').value=document.getElementById('endMenstruationDate').options[document.getElementById('endMenstruationDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.endMenstruationDate"/>"><s:property value="sampleInputTemp.endMenstruationDate"/></option> 
							<option value="<s:property value="inputTemp.endMenstruationDate"/>"><s:property value="inputTemp.endMenstruationDate"/></option>
						</select> 
						<input type="text" name="inputTempNew.endMenstruationDate" id="endMenstruationDateNew" value="<s:property value="inputTempNew.endMenstruationDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.endMenstruationDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
<!--                    	<td class="label-title">孕几次</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	<input type="text" size="20" maxlength="1" data="pregnancyTime" -->
<!-- 						id="sampleInputTemp_pregnancyTime" -->
<!-- 						name="inputTempNew.pregnancyTime" title="孕几次" -->
<%-- 						value="<s:property value="inputTempNew.pregnancyTime"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">孕几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_pregnancyTime" name="sampleInputTemp.pregnancyTime" value="<s:property value="sampleInputTemp.pregnancyTime"/>" />
               	 		<input type="hidden" id="inputTemp_pregnancyTime" name="inputTemp.pregnancyTime" value="<s:property value="inputTemp.pregnancyTime"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="pregnancyTime" style="width:152px;height:28px;" onChange="javascript:document.getElementById('pregnancyTimeNew').value=document.getElementById('pregnancyTime').options[document.getElementById('pregnancyTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.pregnancyTime"/>"><s:property value="sampleInputTemp.pregnancyTime"/></option> 
									<option value="<s:property value="inputTemp.pregnancyTime"/>"><s:property value="inputTemp.pregnancyTime"/></option>
								</select> 
								<input type="text" name="inputTempNew.pregnancyTime" id="pregnancyTimeNew" value="<s:property value="inputTempNew.pregnancyTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   		
                   	</td>
                   	
                   	
                   	
<!--                    	<td class="label-title">产几次</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	<input type="text" size="20" maxlength="1" data="parturitionTime" -->
<!-- 						id="sampleInputTemp_parturitionTime" -->
<!-- 						name="inputTempNew.parturitionTime" title="产几次" -->
<%-- 						value="<s:property value="inputTempNew.parturitionTime"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">产几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_parturitionTime" name="sampleInputTemp.parturitionTime" value="<s:property value="sampleInputTemp.parturitionTime"/>" />
               	 		<input type="hidden" id="inputTemp_parturitionTime" name="inputTemp.parturitionTime" value="<s:property value="inputTemp.parturitionTime"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="parturitionTime" style="width:152px;height:28px;" onChange="javascript:document.getElementById('parturitionTimeNew').value=document.getElementById('parturitionTime').options[document.getElementById('parturitionTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.parturitionTime"/>"><s:property value="sampleInputTemp.parturitionTime"/></option> 
							<option value="<s:property value="inputTemp.parturitionTime"/>"><s:property value="inputTemp.parturitionTime"/></option>
						</select> 
						<input type="text" name="inputTempNew.parturitionTime" id="parturitionTimeNew" value="<s:property value="inputTempNew.parturitionTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
			</tr>
			<tr>
					<td class="label-title">IVF妊娠</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_gestationIVF" name="sampleInputTemp.gestationIVF" value="<s:property value="sampleInputTemp.gestationIVF"/>" />
               	 		<input type="hidden" id="inputTemp_gestationIVF" name="inputTemp.gestationIVF" value="<s:property value="inputTemp.gestationIVF"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.gestationIVF" id="inputTempNew_gestationIVF">
							<option value=""<s:if test="inputTempNew.gestationIVF==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.gestationIVF==0">selected="selected" </s:if>>否</option>
	    					<option value="1"<s:if test="inputTempNew.gestationIVF==1">selected="selected" </s:if>>是</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
<!--                    <td class="label-title">不良孕产史</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	<input type="text" size="20" maxlength="25" data="badMotherhood" -->
<!-- 						id="sampleInputTemp_badMotherhood" -->
<!-- 						name="inputTempNew.badMotherhood" title="不良孕产史" -->
<%-- 						value="<s:property value="inputTempNew.badMotherhood"/>" /> --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_badMotherhood" name="sampleInputTemp.badMotherhood" value="<s:property value="sampleInputTemp.badMotherhood"/>" />
               	 		<input type="hidden" id="inputTemp_badMotherhood" name="inputTemp.badMotherhood" value="<s:property value="inputTemp.badMotherhood"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="badMotherhood" style="width:152px;height:28px;" onChange="javascript:document.getElementById('badMotherhoodNew').value=document.getElementById('badMotherhood').options[document.getElementById('badMotherhood').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.badMotherhood"/>"><s:property value="sampleInputTemp.badMotherhood"/></option> 
							<option value="<s:property value="inputTemp.badMotherhood"/>"><s:property value="inputTemp.badMotherhood"/></option>
						</select> 
						<input type="text" name="inputTempNew.badMotherhood" id="badMotherhoodNew" value="<s:property value="inputTempNew.badMotherhood"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
<!--                    	<td class="label-title">简要病史（家族史）</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	<input type="text" size="20" maxlength="25" data="medicalHistory" -->
<!-- 						id="sampleInputTemp_medicalHistory" -->
<!-- 						name="inputTempNew.medicalHistory" title="简要病史（家族史）" -->
<%-- 						value="<s:property value="inputTempNew.medicalHistory"/>" /> --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">简要病史（家族史）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_medicalHistory" name="sampleInputTemp.medicalHistory" value="<s:property value="sampleInputTemp.medicalHistory"/>" />
               	 		<input type="hidden" id="inputTemp_medicalHistory" name="inputTemp.medicalHistory" value="<s:property value="inputTemp.medicalHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="medicalHistory" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicalHistoryNew').value=document.getElementById('medicalHistory').options[document.getElementById('medicalHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.medicalHistory"/>"><s:property value="sampleInputTemp.medicalHistory"/></option> 
									<option value="<s:property value="inputTemp.medicalHistory"/>"><s:property value="inputTemp.medicalHistory"/></option>
								</select> 
								<input type="text" name="inputTempNew.medicalHistory" id="medicalHistoryNew" value="<s:property value="inputTempNew.medicalHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
	               </td>
                   	
			</tr>
			<tr>
                   	<td class="label-title">器官移植</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_organGrafting" name="sampleInputTemp.organGrafting" value="<s:property value="sampleInputTemp.organGrafting"/>" />
               	 		<input type="hidden" id="inputTemp_organGrafting" name="inputTemp.organGrafting" value="<s:property value="inputTemp.organGrafting"/>" />
               	 		</td>
                   	<td align="left">
                   		<select name="inputTempNew.organGrafting" id="inputTempNew_organGrafting">
							<option value="" <s:if test="inputTempNew.organGrafting==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="inputTempNew.organGrafting==0">selected="selected" </s:if>>无</option>
    						<option value="1" <s:if test="inputTempNew.organGrafting==1">selected="selected" </s:if>>有</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                   	<td class="label-title">外源输血</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_outTransfusion" name="sampleInputTemp.outTransfusion" value="<s:property value="sampleInputTemp.outTransfusion"/>" />
               	 		<input type="hidden" id="inputTemp_outTransfusion" name="inputTemp.outTransfusion" value="<s:property value="inputTemp.outTransfusion"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.outTransfusion" id="inputTempNew_outTransfusion">
							<option value="" <s:if test="inputTempNew.outTransfusion==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.outTransfusion==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="inputTempNew.outTransfusion==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
<!--                    	<td class="label-title">最后一次外源输血时间</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	 <input type="text" size="20" maxlength="25" data="firstTransfusionDate" -->
<!-- 						id="sampleInputTemp_firstTransfusionDate" -->
<!-- 						name="inputTempNew.firstTransfusionDate" title="最后一次外源输血时间" -->
<!-- 						Class="Wdate" readonly="readonly" -->
<!-- 						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" -->
<%-- 						value="<s:date name="inputTempNew.firstTransfusionDate" format="yyyy-MM-dd"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">最后一次外源输血时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" id="sampleInputTemp_firstTransfusionDate" name="sampleInputTemp.firstTransfusionDate" value="<s:property value="sampleInputTemp.firstTransfusionDate"/>" />
               	 		<input type="hidden" id="inputTemp_firstTransfusionDate" name="inputTemp.firstTransfusionDate" value="<s:property value="inputTemp.firstTransfusionDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
						<select id="firstTransfusionDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('firstTransfusionDateNew').value=document.getElementById('firstTransfusionDate').options[document.getElementById('firstTransfusionDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.firstTransfusionDate"/>"><s:property value="sampleInputTemp.firstTransfusionDate"/></option> 
							<option value="<s:property value="inputTemp.firstTransfusionDate"/>"><s:property value="inputTemp.firstTransfusionDate"/></option>
						</select> 
						<input type="text" name="inputTempNew.firstTransfusionDate" id="firstTransfusionDateNew" value="<s:property value="inputTempNew.firstTransfusionDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
				    		value="<s:date name="inputTempNew.firstTransfusionDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
			</tr>
			<tr>
                   	<td class="label-title">干细胞治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_stemCellsCure" name="sampleInputTemp.stemCellsCure" value="<s:property value="sampleInputTemp.stemCellsCure"/>" />
               	 		<input type="hidden" id="inputTemp_stemCellsCure" name="inputTemp.stemCellsCure" value="<s:property value="inputTemp.stemCellsCure"/>" />
               	 		</td>
                   	<td align="left">
                   		<select name="inputTempNew.stemCellsCure" id="inputTempNew_stemCellsCure">
							<option value=""<s:if test="inputTempNew.stemCellsCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.stemCellsCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="inputTempNew.stemCellsCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">免疫治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_immuneCure" name="sampleInputTemp.immuneCure" value="<s:property value="sampleInputTemp.immuneCure"/>" />
               	 		<input type="hidden" id="inputTemp_immuneCure" name="inputTemp.immuneCure" value="<s:property value="inputTemp.immuneCure"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.immuneCure" id="inputTempNew_immuneCure">
							<option value="" <s:if test="inputTempNew.immuneCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.immuneCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="inputTempNew.immuneCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
<!--                    	<td class="label-title">最后一次免疫治疗时间</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<!--                    	 <input type="text" size="20" maxlength="25" data="endImmuneCureDate" -->
<!-- 						id="sampleInputTemp_endImmuneCureDate" -->
<!-- 						name="inputTempNew.endImmuneCureDate" title="最后一次免疫治疗时间" -->
<!-- 						Class="Wdate" readonly="readonly" -->
<!-- 						onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" -->
<%-- 						value="<s:date name="inputTempNew.endImmuneCureDate" format="yyyy-MM-dd"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">最后一次免疫治疗时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" id="sampleInputTemp_endImmuneCureDate" name="sampleInputTemp.endImmuneCureDate" value="<s:property value="sampleInputTemp.endImmuneCureDate"/>" />
               	 		<input type="hidden" id="inputTemp_endImmuneCureDate" name="inputTemp.endImmuneCureDate" value="<s:property value="inputTemp.endImmuneCureDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
								<select id="endImmuneCureDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('endImmuneCureDateNew').value=document.getElementById('endImmuneCureDate').options[document.getElementById('endImmuneCureDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.endImmuneCureDate"/>"><s:property value="sampleInputTemp.endImmuneCureDate"/></option> 
									<option value="<s:property value="inputTemp.endImmuneCureDate"/>"><s:property value="inputTemp.endImmuneCureDate"/></option>
								</select> 
								<input type="text" name="inputTempNew.endImmuneCureDate" id="endImmuneCureDateNew" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
							    	value="<s:date name="inputTempNew.endImmuneCureDate" format="yyyy-MM-dd"/>"  value="<s:property value="inputTempNew.endImmuneCureDate"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title">单/双/多胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_embryoType" name="sampleInputTemp.embryoType" value="<s:property value="sampleInputTemp.embryoType"/>" />
               	 		<input type="hidden" id="inputTemp_embryoType" name="inputTemp.embryoType" value="<s:property value="inputTemp.embryoType"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.embryoType" id="inputTempNew_embryoType">
							<option value="" <s:if test="inputTempNew.embryoType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.embryoType==0">selected="selected" </s:if>>单胎</option>
	    					<option value="1"<s:if test="inputTempNew.embryoType==1">selected="selected" </s:if>>双胎</option>
	    					<option value="2" <s:if test="inputTempNew.embryoType==2">selected="selected" </s:if>>多胎</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
<!--                    	<td class="label-title">NT值</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="NT" id="sampleInputTemp_NT" name="inputTempNew.NT" title="NT值" value="<s:property value="inputTempNew.NT"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">NT值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_NT" name="sampleInputTemp.NT" value="<s:property value="sampleInputTemp.NT"/>" />
               	 		<input type="hidden" id="inputTemp_NT" name="inputTemp.NT" value="<s:property value="inputTemp.NT"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="NT" style="width:152px;height:28px;" onChange="javascript:document.getElementById('NTNew').value=document.getElementById('NT').options[document.getElementById('NT').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.NT"/>"><s:property value="sampleInputTemp.NT"/></option> 
							<option value="<s:property value="inputTemp.NT"/>"><s:property value="inputTemp.NT"/></option>
						</select> 
						<input type="text" name="inputTempNew.NT" id="NTNew" value="<s:property value="inputTempNew.NT"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
<!--                    	<td class="label-title">异常结果描述</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--                    	<input type="text" size="20" maxlength="25" data="reason" id="sampleInputTemp_reason" name="inputTempNew.reason" title="NT值" value="<s:property value="inputTempNew.reason"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">异常结果描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reason" name="sampleInputTemp.reason" value="<s:property value="sampleInputTemp.reason"/>" />
               	 		<input type="hidden" id="inputTemp_reason" name="inputTemp.reason" value="<s:property value="inputTemp.reason"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="reason" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reasonNew').value=document.getElementById('reason').options[document.getElementById('reason').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reason"/>"><s:property value="sampleInputTemp.reason"/></option> 
									<option value="<s:property value="inputTemp.reason"/>"><s:property value="inputTemp.reason"/></option>
								</select>
								<input type="text" name="inputTempNew.reason" id="reasonNew" value="<s:property value="inputTempNew.reason"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
			</tr>
			<tr>
			
                   	<td class="label-title">筛查模式</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_testPattern" name="sampleInputTemp.testPattern" value="<s:property value="sampleInputTemp.testPattern"/>" />
               	 		<input type="hidden" id="inputTemp_testPattern" name="inputTemp.testPattern" value="<s:property value="inputTemp.testPattern"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.testPattern" id="inputTempNew_testPattern">
							<option value="" <s:if test="inputTempNew.testPattern==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.testPattern==0">selected="selected" </s:if>>未做</option>
	    					<option value="1"<s:if test="inputTempNew.testPattern==1">selected="selected" </s:if>>早孕期筛查</option>
	    					<option value="2"<s:if test="inputTempNew.testPattern==2">selected="selected" </s:if>>早中孕期联合筛查</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
<!-- 					<td class="label-title">21-三体比值</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="trisome21Value" id="sampleInputTemp_trisome21Value" name="inputTempNew.trisome21Value" title="21-三体比值" value="<s:property value="inputTempNew.trisome21Value"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">21-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_trisome21Value" name="sampleInputTemp.trisome21Value" value="<s:property value="sampleInputTemp.trisome21Value"/>" />
               	 		<input type="hidden" id="inputTemp_trisome21Value" name="inputTemp.trisome21Value" value="<s:property value="inputTemp.trisome21Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="trisome21Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome21ValueNew').value=document.getElementById('trisome21Value').options[document.getElementById('trisome21Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.trisome21Value"/>"><s:property value="sampleInputTemp.trisome21Value"/></option> 
									<option value="<s:property value="inputTemp.trisome21Value"/>"><s:property value="inputTemp.trisome21Value"/></option>
								</select> 
								<input type="text" name="inputTempNew.trisome21Value" id="trisome21ValueNew" value="<s:property value="inputTempNew.trisome21Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
<!-- 					<td class="label-title">18-三体比值</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%-- 	                   	<input type="text" size="20" maxlength="25" data="trisome18Value" id="sampleInputTemp_trisome18Value" name="inputTempNew.trisome18Value" title="18-三体比值" value="<s:property value="inputTempNew.trisome18Value"/>" /> --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">18-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_trisome18Value" name="sampleInputTemp.trisome18Value" value="<s:property value="sampleInputTemp.trisome18Value"/>" />
               	 		<input type="hidden" id="inputTemp_trisome18Value" name="inputTemp.trisome18Value" value="<s:property value="inputTemp.trisome18Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="trisome18Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome18ValueNew').value=document.getElementById('trisome18Value').options[document.getElementById('trisome18Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.trisome18Value"/>"><s:property value="sampleInputTemp.trisome18Value"/></option> 
									<option value="<s:property value="inputTemp.trisome18Value"/>"><s:property value="inputTemp.trisome18Value"/></option>
								</select> 
								<input type="text" name="inputTempNew.trisome18Value" id="trisome18ValueNew" value="<s:property value="inputTempNew.trisome18Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
			</tr>
			<tr>
<!--                    	<td class="label-title">临床诊断</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="diagnosis" id="sampleInputTemp_diagnosis" name="inputTempNew.diagnosis" title="临床诊断" value="<s:property value="inputTempNew.diagnosis"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_diagnosis" name="sampleInputTemp.diagnosis" value="<s:property value="sampleInputTemp.diagnosis"/>" />
               	 		<input type="hidden" id="inputTemp_diagnosis" name="inputTemp.diagnosis" value="<s:property value="inputTemp.diagnosis"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="diagnosis" style="width:152px;height:28px;" onChange="javascript:document.getElementById('diagnosisNew').value=document.getElementById('diagnosis').options[document.getElementById('diagnosis').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.diagnosis"/>"><s:property value="sampleInputTemp.diagnosis"/></option> 
									<option value="<s:property value="inputTemp.diagnosis"/>"><s:property value="inputTemp.diagnosis"/></option>
								</select> 
								<input type="text" name="inputTempNew.diagnosis" id="diagnosisNew" value="<s:property value="inputTempNew.diagnosis"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	<td class="label-title">夫妻双方染色体</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_coupleChromosome" name="sampleInputTemp.coupleChromosome" value="<s:property value="sampleInputTemp.coupleChromosome"/>" />
               	 		<input type="hidden" id="inputTemp_coupleChromosome" name="inputTemp.coupleChromosome" value="<s:property value="inputTemp.coupleChromosome"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.coupleChromosome" id="inputTempNew_coupleChromosome">
							<option value=""<s:if test="inputTempNew.coupleChromosome==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.coupleChromosome==0">selected="selected" </s:if>>未做</option>
	    					<option value="1"<s:if test="inputTempNew.coupleChromosome==1">selected="selected" </s:if>>正常</option>
	    					<option value="2"<s:if test="inputTempNew.coupleChromosome==2">selected="selected" </s:if>>异常</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
<!--                    	<td class="label-title">异常结果描述</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="reason2" id="sampleInputTemp_reason2" name="inputTempNew.reason2" title="异常结果描述" value="<s:property value="inputTempNew.reason2"/>" /> --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">异常结果描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reason2" name="sampleInputTemp.reason2" value="<s:property value="sampleInputTemp.reason2"/>" />
               	 		<input type="hidden" id="inputTemp_reason2" name="inputTemp.reason2" value="<s:property value="inputTemp.reason2"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="reason2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reason2New').value=document.getElementById('reason2').options[document.getElementById('reason2').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reason2"/>"><s:property value="sampleInputTemp.reason2"/></option> 
									<option value="<s:property value="inputTemp.reason2"/>"><s:property value="inputTemp.reason2"/></option>
								</select> 
								<input type="text" name="inputTempNew.reason2" id="reason2New" value="<s:property value="inputTempNew.reason2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
			</tr>
			<tr>
				<td colspan="9">
					<div class="standard-section-header type-title">
						<label>收费情况</label>
					</div>
				</td>
			</tr>
			
			<tr>
<!-- 					<td class="label-title">金额</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<%--                    	<td align="left"> <input type="text" size="20" maxlength="25" data="money" id="sampleInputTemp_money" name="inputTempNew.money" title="备注" value="<s:property value="inputTempNew.money"/>" /> --%>
<!--                    	</td> -->
					
					<td class="label-title">金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_money" name="sampleInputTemp.money" value="<s:property value="sampleInputTemp.money"/>" />
               	 		<input type="hidden" id="inputTemp_money" name="inputTemp.money" value="<s:property value="inputTemp.money"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="money" style="width:152px;height:28px;" onChange="javascript:document.getElementById('moneyNew').value=document.getElementById('money').options[document.getElementById('money').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.money"/>"><s:property value="sampleInputTemp.money"/></option> 
									<option value="<s:property value="inputTemp.money"/>"><s:property value="inputTemp.money"/></option>
								</select> 
								<input type="text" name="inputTempNew.money" id="moneyNew" value="<s:property value="inputTempNew.money"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
					
					
					<td class="label-title">是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_isFee" name="sampleInputTemp.isFee" value="<s:property value="sampleInputTemp.isFee"/>" />
               	 		<input type="hidden" id="inputTemp_isFee" name="inputTemp.isFee" value="<s:property value="inputTemp.isFee"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.isFee" id="inputTempNew_isFee">
							<option value=""<s:if test="inputTempNew.isFee==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1"<s:if test="inputTempNew.isFee==1">selected="selected" </s:if>>是</option>
	    					<option value="0"<s:if test="inputTempNew.isFee==0">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                   	<td class="label-title">优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_privilegeType" name="sampleInputTemp.privilegeType" value="<s:property value="sampleInputTemp.privilegeType"/>" />
               	 		<input type="hidden" id="inputTemp_privilegeType" name="inputTemp.privilegeType" value="<s:property value="inputTemp.privilegeType"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.privilegeType" id="inputTempNew_privilegeType">
							<option value=""<s:if test="inputTempNew.privilegeType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.privilegeType==0">selected="selected" </s:if>>否</option>
	    					<option value="1"<s:if test="inputTempNew.privilegeType==1">selected="selected" </s:if>>是</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
			</tr>
			<tr>
					<td class="label-title">是否开票</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_isInvoice" name="sampleInputTemp.isInvoice" value="<s:property value="sampleInputTemp.isInvoice"/>" />
               	 		<input type="hidden" id="inputTemp_isInvoice" name="inputTemp.isInvoice" value="<s:property value="inputTemp.isInvoice"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.isInvoice" id="inputTempNew_isInvoice">
							<option value="" <s:if test="inputTempNew.isInvoice==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.isInvoice==0">selected="selected" </s:if>>否</option>
	    					<option value="1" <s:if test="inputTempNew.isInvoice==1">selected="selected" </s:if>>是</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
<!--                    	<td class="label-title">开票单位</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--                    		<input type="text" size="20" maxlength="25" data="paymentUnit" id="sampleInputTemp_paymentUnit" name="inputTempNew.paymentUnit" title="开票单位" value="<s:property value="inputTempNew.paymentUnit"/>" /> --%>
<!--                    	</td> -->
                   	
                   	<td class="label-title">开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_paymentUnit" name="sampleInputTemp.paymentUnit" value="<s:property value="sampleInputTemp.paymentUnit"/>" />
               	 		<input type="hidden" id="inputTemp_paymentUnit" name="inputTemp.paymentUnit" value="<s:property value="inputTemp.paymentUnit"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="paymentUnit" style="width:152px;height:28px;" onChange="javascript:document.getElementById('paymentUnitNew').value=document.getElementById('paymentUnit').options[document.getElementById('paymentUnit').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.paymentUnit"/>"><s:property value="sampleInputTemp.paymentUnit"/></option> 
									<option value="<s:property value="inputTemp.paymentUnit"/>"><s:property value="inputTemp.paymentUnit"/></option>
								</select> 
								<input type="text" name="inputTempNew.paymentUnit" id="paymentUnitNew" value="<s:property value="inputTempNew.paymentUnit"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
                   	<td class="label-title">是否需要保险</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_isInsure" name="sampleInputTemp.isInsure" value="<s:property value="sampleInputTemp.isInsure"/>" />
               	 		<input type="hidden" id="inputTemp_isInsure" name="inputTemp.isInsure" value="<s:property value="inputTemp.isInsure"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.isInsure" id="inputTempNew_isInsure">
							<option value="" <s:if test="inputTempNew.isInsure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.isInsure==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="inputTempNew.isInsure==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
			</tr>
			<tr>
<!--                    	<td class="label-title">收据类型</td> -->
<!--                	 	<td class="requiredcolumn" nowrap width="10px"> -->
<%--                	 		</td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--  						<input type="text" size="20" readonly="readOnly" id="sampleInputTemp_receiptType_name" name="inputTempNew.receiptType.name" value="<s:property value="inputTempNew.receiptType.name"/>" /> --%>
<%--  						<input type="hidden" id="sampleInputTemp_receiptType" name="inputTempNew.receiptType.id" value="<s:property value="inputTempNew.receiptType.id"/>">  --%>
<%--  						<img alt='选择收据类型' src='${ctx}/images/img_lookup.gif' onClick="receiptTypeFun()" class='detail' />                   		 --%>
<!--                    	</td> -->
                   	
                   	
                   	<td class="label-title">收据类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_receiptType_id" name="sampleInputTemp.receiptType.id"  value="<s:property value="sampleInputTemp.receiptType.id"/>"/>
               	 		<input type="hidden" id="sampleInputTemp_receiptType_name" name="sampleInputTemp.receiptType.name"  value="<s:property value="sampleInputTemp.receiptType.name"/>"/>
  						<input type="hidden" id="inputTemp_receiptType_id" name="inputTemp.receiptType.id" value="<s:property value="inputTemp.receiptType.id"/>">
  						<input type="hidden" id="inputTemp_receiptType_name" name="inputTemp.receiptType.name" value="<s:property value="inputTemp.receiptType.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		
								<select id="receiptType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('receiptTypeNew').value=document.getElementById('receiptType').options[document.getElementById('receiptType').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.receiptType.name"/>"><s:property value="sampleInputTemp.receiptType.name"/></option> 
									<option value="<s:property value="inputTemp.receiptType.name"/>"><s:property value="inputTemp.receiptType.name"/></option>
								</select> 
								<input type="text" name="inputTempNew.receiptType.name" id="receiptTypeNew" onfocus="receiptTypeFun()" value="<s:property value="inputTempNew.receiptType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   				<input type="hidden" name="inputTempNew.receiptType.id" id="receiptTypeIdNew" value="<s:property value="inputTempNew.receiptType.id"/>" />
                   	</td>
                   	
					<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="inputTempNew_note" name="inputTempNew.note" title="备注" value="<s:property value="inputTempNew.note"/>" />
                   	</td>
                   	
					<td class="label-title">工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="inputTempNew_stateName" name="inputTempNew.stateName" title="工作流状态" readonly="readOnly" class="text input readonlytrue" value="<s:property value="inputTempNew.stateName"/>" />
                   		<input type="hidden" size="20" maxlength="25" id="inputTempNew_state" name="inputTempNew.state" title="工作流状态" readonly="readOnly" class="text input readonlytrue" value="<s:property value="inputTempNew.state"/>" />
                   	</td>
                   	
<!--                    	<td class="label-title">工作流状态ID</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
                   		
<!--                    	</td> -->
			</tr>
				<tr>
<!-- 					<td class="label-title">补充协议</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	 --%>
<!--                    	<td align="left"> -->
<%--                    	 	<input type="text" size="20" maxlength="25" data="suppleAgreement" id="sampleInputTemp_suppleAgreement" name="inputTempNew.suppleAgreement" title="补充协议" onblur="checkAddress()" value="<s:property value="inputTempNew.suppleAgreement"/>" /> --%>
<!-- 					</td> -->
					
					<td class="label-title">补充协议</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_suppleAgreement" name="sampleInputTemp.suppleAgreement" value="<s:property value="sampleInputTemp.suppleAgreement"/>" />
               	 		<input type="hidden" id="inputTemp_suppleAgreement" name="inputTemp.suppleAgreement" value="<s:property value="inputTemp.suppleAgreement"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="suppleAgreement" style="width:152px;height:28px;" onChange="javascript:document.getElementById('suppleAgreementNew').value=document.getElementById('suppleAgreement').options[document.getElementById('suppleAgreement').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.suppleAgreement"/>"><s:property value="sampleInputTemp.suppleAgreement"/></option> 
									<option value="<s:property value="inputTemp.suppleAgreement"/>"><s:property value="inputTemp.suppleAgreement"/></option>
								</select> 
								<input type="text" name="inputTempNew.suppleAgreement" id="suppleAgreementNew" value="<s:property value="inputTempNew.suppleAgreement"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
					
					
					<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left"> 
                   		<select name="inputTempNew.nextStepFlow" id="sampleInputTemp_nextStepFlow">
							<option value="" <s:if test="inputTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="inputTempNew.nextStepFlow==0">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="inputTempNew.nextStepFlow==1">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
				<tr>
					
					<td class="label-title">附件</td>
					<td></td>
					<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span
						class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
				</tr>
            </table>
		<input type="hidden" id="id_parent_hidden" value="<s:property value="sampleInputTemp.id"/>" /></form>
     		  <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
