<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="dicPriLib_label" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="/operfile/initFileList.action?modelType=dicPriLib&id=dicPriLib.id"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/dic/dicPriLibEdit.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleVaLeave.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	
                   	<td align="left">
                   	<input type="text" size="15" maxlength="36" id="dicPriLib_id" name="dicPriLib.id" title="<fmt:message key="biolims.common.primersListCode"/>" 
                   		 	class="text input"
                   		 value="<s:property value="dicPriLib.id"/>" 
                   	 />
                   	</td>
                    <td class="label-title"><fmt:message key="biolims.common.gene"/></td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	
                   	<td align="left">
                   	<input type="text" size="50" maxlength="100" id="dicPriLib_name" name="dicPriLib.name" title="<fmt:message key="biolims.common.gene"/>" 
                   		 	class="text input"
                   		 value="<s:property value="dicPriLib.name"/>" 
                   	 />
                   	</td>    	 
               	 	<td class="label-title"><fmt:message key="biolims.common.geneType"/></td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	
                   	<td align="left">
                   	<input type="text" size="50" maxlength="100" id="dicPriLib_geneName" name="dicPriLib.geneName" title="<fmt:message key="biolims.common.geneType"/>" 
                   		 	class="text input"
                   		 value="<s:property value="dicPriLib.geneName"/>" 
                   	 />
                   	</td>  
				</tr>
				<tr>
  	 
               	 	<td class="label-title"><fmt:message key="biolims.common.state"/></td>
               	 		<td class="requiredcolumn" nowrap width="10px"></td>            	 	
               	 	<td align="left"><select id="dicPriLib_state"
						name="dicPriLib.state" class="input-10-length">
							<option value="1"
								<s:if test="dicPriLib.state==1">selected="selected"</s:if>><fmt:message key="biolims.common.effective"/></option>
							<option value="0"
								<s:if test="dicPriLib.state==0">selected="selected"</s:if>><fmt:message key="biolims.common.invalid"/></option>
					</select>
					<td class="label-title"><fmt:message key="biolims.common.detectionItems"/></td>
               	 	<td></td>
						<td><input type="text" size="15" maxlength="200" id="dicPriLib_item_name" name="dicPriLib.item.name" title="<fmt:message key="biolims.common.state"/>" 
                   		 	class="text input"
                   		 value="<s:property value="dicPriLib.item.name"/>" 
                   	 />
                   	 <input type="hidden" size="15"  id="dicPriLib_item_id" name="dicPriLib.item.id" title="<fmt:message key="biolims.common.state"/>" 
                   		 	class="text input"  value="<s:property value="dicPriLib.item.id"/>" 
                   	 />
							<span id="regionType" onClick="showItemNumber()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
						</td>
				</tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="dicPriLib.id"/>" />
            </form>
			</div>
	</body>
	</html>
