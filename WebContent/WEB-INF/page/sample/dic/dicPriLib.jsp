﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/dic/dicPriLib.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table>
	
        </table>
		</form>
		</div>
		<div id="show_dicPriLib_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<input type="hidden" name="itemDataJson" id="itemDataJson" value="" />
		<div id="bat_upload_div" style="display: none">
		<input type="file" name="file" id="file-upload"><fmt:message key="biolims.common.uploadTheReceivingExcelFile"/>
	</div>
</body>
</html>



