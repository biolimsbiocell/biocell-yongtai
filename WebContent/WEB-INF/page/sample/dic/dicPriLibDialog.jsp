﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/dic/dicPriLibDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.primerOrderNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="36" id="dicPriLib_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.workingLiquidPosition"/>（5μm）</td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_twfPosition" searchField="true" name="twfPosition"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.storageOfLiquidLocation"/>（100μm-r）</td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_solPosition" searchField="true" name="solPosition"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.dryPlace"/></td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_dryPosition" searchField="true" name="dryPosition"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.geneName"/></td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_geneName" searchField="true" name="geneName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.chromosomeNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_chromosomeNumber" searchField="true" name="chromosomeNumber"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.site"/></td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_site" searchField="true" name="site"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.geneposNameOfThePrimer"/></td>
               	<td align="left">
                    		<input type="text" maxlength="100" id="dicPriLib_genePosPriName" searchField="true" name="genePosPriName"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.chrposNameOfThePrimer"/></td>
               	<td align="left">
                    		<input type="text" maxlength="100" id="dicPriLib_chrPosPriName" searchField="true" name="chrPosPriName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">ref-5'</td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_ref5" searchField="true" name="ref5"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">ref-3'</td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_ref3" searchField="true" name="ref3"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.state"/></td>
               	<td align="left">
                    		<input type="text" maxlength="200" id="dicPriLib_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_dicPriLib_div"></div>
   		
</body>
</html>



