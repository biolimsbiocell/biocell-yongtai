﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleInputDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">信息录入id</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_code" searchField="true" name="code"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">描述</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleInput_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">年龄</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_age" searchField="true" name="age"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">性别</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_gender" searchField="true" name="gender"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">联系人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_linkman" searchField="true" name="linkman"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">出生日期</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_birthday" searchField="true" name="birthday"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">联系方式</td>
               	<td align="left">
                    		<input type="text" maxlength="10" id="sampleInput_phone" searchField="true" name="phone"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">状态id</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">工作流状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInput_stateName" searchField="true" name="stateName"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_sampleInput_div"></div>
   		
</body>
</html>



