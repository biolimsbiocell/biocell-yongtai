
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBloodDiseaseTemp&id=${sampleBloodDiseaseTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleBloodDiseaseEdit.js"></script>
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBloodDiseaseTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<tr>
				<td class="label-title" style="color: red"><fmt:message key="biolims.common.clickToUploadPictures"/></td>
				<td></td>
				<td>
  					<input type="button" value="<fmt:message key="biolims.common.uploadTheInformationRecordedImages"/>" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleBloodDiseaseTemp.upLoadAccessory.id" value="<s:property value="sampleBloodDiseaseTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleBloodDiseaseTemp.upLoadAccessory.fileName" value="<s:property value="sampleBloodDiseaseTemp.upLoadAccessory.fileName"/>">
					<input type="hidden" id="saveType" value="xyb" >
				</td>
			</tr>
				<tr>
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
<%--                    		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleBloodDiseaseTemp.code" title="编号" value="<s:property value="sampleBloodDiseaseTemp.code"/>" /> --%>
<%-- 						<input type="hidden" name="sampleBloodDiseaseTemp.id" value="<s:property value="sampleBloodDiseaseTemp.id"/>" /> --%>
<%--                    		<input type="hidden"  id="upload_imga_name" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName"/>"> --%>
<%-- 						<input type="hidden"  id="upload_imga_id10" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id"/>"> --%>
                   		<input type="text" 	  id="sampleBloodDiseaseTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleBloodDiseaseTemp.sampleInfo.code" title="<fmt:message key="biolims.common.sampleCode"/>" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.code"/>" />
						<input type="hidden"  id="sampleBloodDiseaseTemp_id"  name="sampleBloodDiseaseTemp.sampleInfo.id"  value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.id"/>" />
						<input type="hidden"  name="sampleBloodDiseaseTemp.id" value="<s:property value="sampleBloodDiseaseTemp.id"/>" />
						<input type="hidden"  id="upload_imga_name" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.id"/>">
                  	</td>

                   	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_name" name="sampleBloodDiseaseTemp.name" title="<fmt:message key="biolims.common.describe"/>" value="<s:property value="sampleBloodDiseaseTemp.name"/>" />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.detectionItems"/></td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleBloodDiseaseTemp_product_name" searchField="true"  
 							name="sampleBloodDiseaseTemp.productName"  value="<s:property value="sampleBloodDiseaseTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_product_id" name="sampleBloodDiseaseTemp.productId"  
 							value="<s:property value="sampleBloodDiseaseTemp.productId"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectTheTestItems"/>' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                  </tr>
                <tr>   	
                   	<td class="label-title" ><fmt:message key="biolims.common.receivingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_acceptDate"
                   	 		name="sampleBloodDiseaseTemp.acceptDate" title="<fmt:message key="biolims.common.receivingDate"/>" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBloodDiseaseTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	    />
                   	</td>
              
                	<td class="label-title" ><fmt:message key="biolims.common.region"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_area"
                   	 		name="sampleBloodDiseaseTemp.area" title="<fmt:message key="biolims.common.region"/>"  onblur="checkAdd()" 
							value="<s:property value="sampleBloodDiseaseTemp.area"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.inspectionHospital"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_hospital"
                   	 		name="sampleBloodDiseaseTemp.hospital" title="<fmt:message key="biolims.common.inspectionHospital"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.hospital"/>"
                   	 	/>
                   	</td>
                   	
                   	  </tr>
                <tr>
                   	<td class="label-title" ><fmt:message key="biolims.common.inspectionDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_sendDate"
                   	 		name="sampleBloodDiseaseTemp.sendDate" title="<fmt:message key="biolims.common.inspectionDate"/>" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleBloodDiseaseTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	 	/>
                   	</td>
              
                	<td class="label-title" ><fmt:message key="biolims.common.shouldTheReportDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_reportDate"
                   	 		name="sampleBloodDiseaseTemp.reportDate" title="<fmt:message key="biolims.common.shouldTheReportDate"/>" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBloodDiseaseTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.sampleType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleBloodDiseaseTemp_sampleType_name"  value="<s:property value="sampleBloodDiseaseTemp.sampleType.name"/>"/>
						<s:hidden id="sampleBloodDiseaseTemp_sampleType_id" name="sampleBloodDiseaseTemp.sampleType.id"></s:hidden>
						<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>	
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.number"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBloodDiseaseTemp_confirmAgeOne" name="sampleBloodDiseaseTemp.confirmAgeOne"  
 							value="<s:property value="sampleBloodDiseaseTemp.confirmAgeOne"/>" />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.inspectionDoctor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_doctor"
                   	 		name="sampleBloodDiseaseTemp.doctor" title="<fmt:message key="biolims.common.inspectionDoctor"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.doctor"/>"
                   		 />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.doctorContact"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_phone"
                   	 		name="sampleBloodDiseaseTemp.phone" title="<fmt:message key="biolims.common.doctorContact"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.phone"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title"><fmt:message key="biolims.common.sampleCode"/></td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                    <td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_sampleNum" 
                   			name="sampleBloodDiseaseTemp.sampleNum" title="<fmt:message key="biolims.common.sampleCode"/>" value="<s:property value="sampleBloodDiseaseTemp.sampleNum"/>"
                   		/>
                   </td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.patientInformation"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.patientName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_patientName"
                   	 		name="sampleBloodDiseaseTemp.patientName" title="<fmt:message key="biolims.common.patientName"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.namePinyin"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_patientNameSpell"
                   	 		name="sampleBloodDiseaseTemp.patientNameSpell" title="<fmt:message key="biolims.common.namePinyin"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		
               	 	<td class="label-title" ><fmt:message key="biolims.common.gender"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.gender" id="sampleBloodDiseaseTemp_gender" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.gender==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.gender==0">selected="selected" </s:if>><fmt:message key="biolims.common.female"/></option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.gender==1">selected="selected" </s:if>><fmt:message key="biolims.common.male"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.age"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBloodDiseaseTemp_age" name="sampleBloodDiseaseTemp.age"  value="<s:property value="sampleBloodDiseaseTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.papersType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleBloodDiseaseTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleBloodDiseaseTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_voucherType" name="sampleBloodDiseaseTemp.voucherType.id"  value="<s:property value="sampleBloodDiseaseTemp.voucherType.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectPapersType"/>' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" ><fmt:message key="biolims.common.papersCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_voucherCode"
                   	 		name="sampleBloodDiseaseTemp.voucherCode" title="<fmt:message key="biolims.common.papersCode"/>"   onblur="checkFun()"
							value="<s:property value="sampleBloodDiseaseTemp.voucherCode"/>"
                   	 	/>
                   	</td>
                   	
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.contact"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleBloodDiseaseTemp_phoneNum2"
	                   		 name="sampleBloodDiseaseTemp.phoneNum2" title="<fmt:message key="biolims.common.contact"/>"  value="<s:property value="sampleBloodDiseaseTemp.phoneNum2"/>"
	                   	 />
                   	</td>

                   	<td class="label-title" ><fmt:message key="biolims.common.eamil"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_emailAddress"
                  	 		name="sampleBloodDiseaseTemp.emailAddress" title="<fmt:message key="biolims.common.eamil"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.emailAddress"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.correspondenceAddress"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_address"
                   	 		name="sampleBloodDiseaseTemp.address" title="<fmt:message key="biolims.common.correspondenceAddress"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.address"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.detectionDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_gestationalAge"
                   	 		name="sampleBloodDiseaseTemp.gestationalAge" title="<fmt:message key="biolims.common.detectionDate"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.gestationalAge"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.vitalSigns"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_reason"
                   	 		name="sampleBloodDiseaseTemp.reason" title="<fmt:message key="biolims.common.vitalSigns"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.reason"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   		<td class="label-title" ><fmt:message key="biolims.common.relevantMedicalHistory"/></td>
               	 		<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   		<td align="left"  >
                   			<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_reason2"
                   	 			name="sampleBloodDiseaseTemp.reason2" title="<fmt:message key="biolims.common.relevantMedicalHistory"/>"   
                   	 			value="<s:property value="sampleBloodDiseaseTemp.reason2"/>"
                   	 		/>
                   	 		<img class='requiredimage' src='${ctx}/images/required.gif' />
                   		</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.medicalHistory"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.chemotherap"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_tumorHistory"
                   	 		name="sampleBloodDiseaseTemp.tumorHistory" title="<fmt:message key="biolims.common.chemotherap"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.tumorHistory"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.lastTimeOfChemotherapy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_firstTransfusionDate"
                   	 		name="sampleBloodDiseaseTemp.firstTransfusionDate" title="<fmt:message key="biolims.common.shouldTheReportDate"/>" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBloodDiseaseTemp.firstTransfusionDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.recentlyUsedImmunosuppressant"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.medicineType" id="sampleBloodDiseaseTemp_medicineType" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.medicineType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.medicineType==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.medicineType==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.specificImmunosuppressiveDrugs"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_medicine"
                   	 		name="sampleBloodDiseaseTemp.medicine" title="<fmt:message key="biolims.common.specificImmunosuppressiveDrugs"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.medicine"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" ><fmt:message key="biolims.common.radiationTherapy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_radioactiveRays"
                   	 		name="sampleBloodDiseaseTemp.radioactiveRays" title="<fmt:message key="biolims.common.radiationTherapy"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.radioactiveRays"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   </td>
                   	<td class="label-title" ><fmt:message key="biolims.common.boneMarrowGrowthStimulatingFactor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_cadmium"
                   	 		name="sampleBloodDiseaseTemp.cadmium" title="<fmt:message key="biolims.common.boneMarrowGrowthStimulatingFactor"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.cadmium"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.peripheralBloodTestResults"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >WBC</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_trisome21Value"
                   	 		name="sampleBloodDiseaseTemp.trisome21Value" title="WBC"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.trisome21Value"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >RBC</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_trisome18Value"
                   	 		name="sampleBloodDiseaseTemp.trisome18Value" title="WBC"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.trisome18Value"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >Hb</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_trisome13Value"
                   	 		name="sampleBloodDiseaseTemp.trisome13Value" title="Hb"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.trisome13Value"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >PLT</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_confirmAgeTwo"
                   	 		name="sampleBloodDiseaseTemp.confirmAgeTwo" title="PLT"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.confirmAgeTwo"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" ><fmt:message key="biolims.common.originalCell"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_confirmAgeThree"
                   	 		name="sampleBloodDiseaseTemp.confirmAgeThree" title="<fmt:message key="biolims.common.originalCell"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.confirmAgeThree"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" ><fmt:message key="biolims.common.immatureCells"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_confirmAge"
                   	 		name="sampleBloodDiseaseTemp.confirmAge" title="<fmt:message key="biolims.common.immatureCells"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.confirmAge"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.otherCells"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_stemCellsCure"
                   	 		name="sampleBloodDiseaseTemp.stemCellsCure" title="<fmt:message key="biolims.common.otherCells"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.stemCellsCure"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                   	<td class="label-title" ><fmt:message key="biolims.common.boneMarrowExaminationResults"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_badMotherhood"
                   	 		name="sampleBloodDiseaseTemp.badMotherhood" title="<fmt:message key="biolims.common.boneMarrowExaminationResults"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.badMotherhood"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.clinicalDiagnosisOpinion"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_diagnosis"
                   	 		name="sampleBloodDiseaseTemp.diagnosis" title="<fmt:message key="biolims.common.clinicalDiagnosisOpinion"/>"   
                   	 		value="<s:property value="sampleBloodDiseaseTemp.diagnosis"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.chargeState"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.whetherTheCharge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.isFee" id="sampleBloodDiseaseTemp_isInsure" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.isInsure==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.isInsure==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.isInsure==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.preferentialType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.privilegeType" id="sampleBloodDiseaseTemp_privilegeType" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.privilegeType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.privilegeType==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.privilegeType==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.personOfRecommendations"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_linkman"
                   	 		name="sampleBloodDiseaseTemp.linkman" title="<fmt:message key="biolims.common.personOfRecommendations"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.linkman"/>"
                   		/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.whetherTheInvoice"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBloodDiseaseTemp.isInvoice" id="sampleBloodDiseaseTemp_isInvoice" >
							<option value="" <s:if test="sampleBloodDiseaseTemp.isInvoice==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    						<option value="0" <s:if test="sampleBloodDiseaseTemp.isInvoice==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    						<option value="1" <s:if test="sampleBloodDiseaseTemp.isInvoice==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                    <td class="label-title" ><fmt:message key="biolims.common.enterPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="sampleBloodDiseaseTemp.createUser" title="<fmt:message key="biolims.common.enterPerson"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.createUser"/>"/>
                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_note"
                   	 		name="sampleBloodDiseaseTemp.note" title="<fmt:message key="biolims.common.enterPerson"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.note"/>"
                   	 	/>
                   	</td>
                </tr>
                 <tr>
                	<td class="label-title" ><fmt:message key="biolims.common.money"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_money"
                   	 		name="sampleBloodDiseaseTemp.money" title="<fmt:message key="biolims.common.money"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_bloodHistory"
                   	 		name="sampleBloodDiseaseTemp.bloodHistory" title="SP"   
							value="<s:property value="sampleBloodDiseaseTemp.bloodHistory"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" ><fmt:message key="biolims.common.institutionsOfMakeOutAnInvoice"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_paymentUnit"
                   	 		name="sampleBloodDiseaseTemp.paymentUnit" title="<fmt:message key="biolims.common.institutionsOfMakeOutAnInvoice"/>"   
							value="<s:property value="sampleBloodDiseaseTemp.paymentUnit"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                <g:LayOutWinTag buttonId="showreportMan" title='<fmt:message key="biolims.common.selectingTheCheckPerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reportManUserFun" 
 				hasSetFun="true"
				documentId="sampleBloodDiseaseTemp_reportMan"
				documentName="sampleBloodDiseaseTemp_reportMan_name" />
			
			  	 	<td class="label-title" ><fmt:message key="biolims.common.checkThePeopleOne"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBloodDiseaseTemp_reportMan_name"  value="<s:property value="sampleBloodDiseaseTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_reportMan" name="sampleBloodDiseaseTemp.reportMan.id"  value="<s:property value="sampleBloodDiseaseTemp.reportMan.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectingTheCheckPerson"/>' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 <g:LayOutWinTag buttonId="showauditMan" title='<fmt:message key="biolims.common.selectingTheCheckPerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="auditManUserFun" 
 				hasSetFun="true"
				documentId="sampleBloodDiseaseTemp_auditMan"
				documentName="sampleBloodDiseaseTemp_auditMan_name" />
			
			  	 	<td class="label-title" ><fmt:message key="biolims.common.checkThePeopleTwo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBloodDiseaseTemp_auditMan_name"  value="<s:property value="sampleBloodDiseaseTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleBloodDiseaseTemp_auditMan" name="sampleBloodDiseaseTemp.auditMan.id"  value="<s:property value="sampleBloodDiseaseTemp.auditMan.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.selectingTheCheckPerson"/>' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                </tr>

            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBloodDisease.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
