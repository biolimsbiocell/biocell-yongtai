
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBreastCancerTemp&id=${sampleBreastCancerTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 20 && zoom < 400)
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleBreastCancerEditTwo.js"></script>
<s:if test="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id != ''">
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id}"></div>
</s:if>
<s:if test="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id == ''">
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src=""></div>
</s:if>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleBreastCancerTemp.upLoadAccessory.id" value="<s:property value="sampleBreastCancerTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleBreastCancerTemp.upLoadAccessory.fileName" value="<s:property value="sampleBreastCancerTemp.upLoadAccessory.fileName"/>">
				</td>
			</tr>
				<tr>
                   	<td class="label-title">编号</td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25"
							id="sampleBreastCancerTemp_code" class="text input readonlytrue" readonly="readonly"
							name="sampleBreastCancerTemp.code" title="编号"
							value="<s:property value="sampleBreastCancerTemp.code"/>" />
						<input type="hidden" name="sampleBreastCancerTemp.id" value="<s:property value="sampleBreastCancerTemp.id"/>" />
                   		<input type="hidden"  id="upload_imga_name" name="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.id"/>">
                   	</td>
                   
                   	
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_name"
                   	 		name="sampleBreastCancerTemp.name" title="描述"   
							value="<s:property value="sampleBreastCancerTemp.name"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleBreastCancerTemp_product_name" searchField="true"  
 							name="sampleBreastCancerTemp.productName"  value="<s:property value="sampleBreastCancerTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleBreastCancerTemp_product_id" name="sampleBreastCancerTemp.productId"  
 							value="<s:property value="sampleBreastCancerTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   
                </tr>
                <tr>
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_acceptDate"
                   	 		name="sampleBreastCancerTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
                   	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_area"
                   	 		name="sampleBreastCancerTemp.area" title="地区"  onblur="checkAdd()" 
							value="<s:property value="sampleBreastCancerTemp.area"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_hospital"
                   	 		name="sampleBreastCancerTemp.hospital" title="送检医院"   
							value="<s:property value="sampleBreastCancerTemp.hospital"/>"
                   		 />
                   	</td>
                </tr>
                <tr>
                   
                   	<td class="label-title" >病历号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_inHosNum"
                   	 		name="sampleBreastCancerTemp.inHosNum" title="病历号"   
							value="<s:property value="sampleBreastCancerTemp.inHosNum"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >亲属编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_relationNum"
                   	 		name="sampleBreastCancerTemp.relationNum" title="亲属编号"   
							value="<s:property value="sampleBreastCancerTemp.relationNum"/>"
                   	 	/>
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title" >亲属关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.relationship1" id="sampleBreastCancerTemp_relationship1" >
							<option value="" <s:if test="sampleBreastCancerTemp.relationship1==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.relationship1==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.relationship1==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sendDate"
                   	 		name="sampleBreastCancerTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleBreastCancerTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_reportDate"
                   	 		name="sampleBreastCancerTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleBreastCancerTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_doctor"
                   	 		name="sampleBreastCancerTemp.doctor" title="送检医生"   
							value="<s:property value="sampleBreastCancerTemp.doctor"/>"
                   	 />
                   	</td>
                   	<td class="label-title">样本编号</td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                    <td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sampleNum" 
                   			name="sampleBreastCancerTemp.sampleNum" title="样本编号" value="<s:property value="sampleBreastCancerTemp.sampleNum"/>"
                   		/>
                   </td>	
               </tr>
               <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientName"
                   	 		name="sampleBreastCancerTemp.patientName" title="姓名"   
							value="<s:property value="sampleBreastCancerTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
<!--                    	<td class="label-title" >姓名拼音</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientNameSpell" -->
<!--                    	 		name="sampleBreastCancerTemp.patientNameSpell" title="姓名拼音"    -->
<%-- 							value="<s:property value="sampleBreastCancerTemp.patientNameSpell"/>" --%>
<!--                    	 	/> -->
<!--                    	</td> -->
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gender" id="sampleBreastCancer_gender" >
							<option value="" <s:if test="sampleBreastCancerTemp.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_age" name="sampleBreastCancerTemp.age"  value="<s:property value="sampleBreastCancerTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" >籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nativePlace"
                   	 		name="sampleBreastCancerTemp.nativePlace" title="籍贯"   
							value="<s:property value="sampleBreastCancerTemp.nativePlace"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_nationality"
                   	 		name="sampleBreastCancerTemp.nationality" title="民族"   
							value="<s:property value="sampleBreastCancerTemp.nationality"/>"
                   	 	/>
                   	</td>
                </tr>
                   	<tr>
                   	<td class="label-title" >身高</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_heights"
                   	 		name="sampleBreastCancerTemp.heights" title="身高" 
							value="<s:property value="sampleBreastCancerTemp.heights"/>"
                   		 />
                   	</td>
                   	
                   	<td class="label-title" >体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_weight"
                   	 		name="sampleBreastCancerTemp.weight" title="体重" 
							value="<s:property value="sampleBreastCancerTemp.weight"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.maritalStatus" id="sampleBreastCancerTemp_maritalStatus" >
							<option value="" <s:if test="sampleBreastCancerTemp.maritalStatus==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.maritalStatus==0">selected="selected" </s:if>>已婚</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.maritalStatus==1">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                </td>
                </tr>
                   	<tr>
                   	<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleBreastCancerTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleBreastCancerTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_voucherType_id" name="sampleBreastCancerTemp.voucherType.id"  value="<s:property value="sampleBreastCancerTemp.voucherType.id"/>" > 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_voucherCode"
                   	 		name="sampleBreastCancerTemp.voucherCode" title="证件号码"   onblur="checkFun()"
							value="<s:property value="sampleBreastCancerTemp.voucherCode"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleBreastCancerTemp_phone"
	                   		 name="sampleBreastCancerTemp.phone" title="联系方式"  value="<s:property value="sampleBreastCancerTemp.phone"/>"
	                   	 />
	                   	 <img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_emailAddress"
                  	 		name="sampleBreastCancerTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleBreastCancerTemp.emailAddress"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_address"
                   	 		name="sampleBreastCancerTemp.address" title="通讯地址"   
							value="<s:property value="sampleBreastCancerTemp.address"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >初潮年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_menarcheAge" name="sampleBreastCancerTemp.menarcheAge"  
 							value="<s:property value="sampleBreastCancerTemp.menarcheAge"/>" />
                   	</td>
                   	<td class="label-title" >月经周期</td>
		            <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
		            <td align="left"  >
	 					<input type="text" size="20" id="sampleBreastCancerTemp_menstrualCycle" name="sampleBreastCancerTemp.menstrualCycle"  
	 						value="<s:property value="sampleBreastCancerTemp.menstrualCycle"/>" />
                   	</td> 
                 </tr>
                 <tr>	
                   	<td class="label-title" >首次妊娠年龄</td>
		            <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
		            <td align="left"  >
	 					<input type="text" size="20" id="sampleBreastCancerTemp_firstGestationAge" name="sampleBreastCancerTemp.firstGestationAge"  
	 						value="<s:property value="sampleBreastCancerTemp.firstGestationAge"/>" />
                   	</td> 	
                   	<td class="label-title" >孕次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pregnancyTime"
                   	 		name="sampleBreastCancerTemp.pregnancyTime" title="孕次数"   
							value="<s:property value="sampleBreastCancerTemp.pregnancyTime"/>"
                   		 />
                   	</td>
                   	
                   	<td class="label-title" >产次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_parturitionTime"
                   	 		name="sampleBreastCancerTemp.parturitionTime" title="产次数"   
							value="<s:property value="sampleBreastCancerTemp.parturitionTime"/>"
                   		 />
                   	</td>
                   		
                 </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>医疗信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title" >个人肿瘤病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorHistory" id="sampleBreastCancerTemp_tumorHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorHistory==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >肿瘤名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_ovaryTumorType"
                   	 		name="sampleBreastCancerTemp.ovaryTumorType" title="肿瘤名称"   
							value="<s:property value="sampleBreastCancerTemp.ovaryTumorType"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleBreastCancerTemp_confirmAgeTwo" name="sampleBreastCancerTemp.confirmAgeTwo"  
 							value="<s:property value="sampleBreastCancerTemp.confirmAgeTwo"/>" />
                   	</td> 
                </tr>
                <tr>
                   	<td class="label-title" >个人哺乳史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.otherBreastHistory" id="sampleBreastCancerTemp_otherBreastHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.otherBreastHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.otherBreastHistory==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.otherBreastHistory==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >哺乳次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_breastNum"
                   	 		name="sampleBreastCancerTemp.breastNum" title="哺乳次数"   
							value="<s:property value="sampleBreastCancerTemp.breastNum"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >绝经情况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.gestationIVF" id="sampleBreastCancerTemp_gestationIVF" >
							<option value="" <s:if test="sampleBreastCancerTemp.gestationIVF==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.gestationIVF==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.gestationIVF==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                </tr>
                <tr>
                	<td class="label-title" >绝经年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_menopauseAge"
                   	 		name="sampleBreastCancerTemp.menopauseAge" title="绝经年龄"   
							value="<s:property value="sampleBreastCancerTemp.menopauseAge"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >是否接受乳腺活检</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.tumorType" id="sampleBreastCancerTemp_tumorType" >
							<option value="" <s:if test="sampleBreastCancerTemp.tumorType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.tumorType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.tumorType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >接受次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_acceptNum"
                   	 		name="sampleBreastCancerTemp.acceptNum" title="接受次数"   
							value="<s:property value="sampleBreastCancerTemp.acceptNum"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >非典型乳腺增生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_isAtypicalBreast"
                   	 		name="sampleBreastCancerTemp.isAtypicalBreast" title="非典型乳腺增生"   
							value="<s:property value="sampleBreastCancerTemp.isAtypicalBreast"/>"
                   	 	/>
                    </td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>有毒、有害物质长期接触史</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cigarette"
                   	 		name="sampleBreastCancerTemp.cigarette" title="烟"   
							value="<s:property value="sampleBreastCancerTemp.cigarette"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_wine"
                   	 		name="sampleBreastCancerTemp.wine" title="酒"   
							value="<s:property value="sampleBreastCancerTemp.wine"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicine"
                   	 		name="sampleBreastCancerTemp.medicine" title="药物"   
							value="<s:property value="sampleBreastCancerTemp.medicine"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >药物类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicineType"
                   	 		name="sampleBreastCancerTemp.medicineType" title="药物类型"   
							value="<s:property value="sampleBreastCancerTemp.medicineType"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_radioactiveRays"
                   	 		name="sampleBreastCancerTemp.radioactiveRays" title="放射线"   
							value="<s:property value="sampleBreastCancerTemp.radioactiveRays"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_pesticide"
                   	 		name="sampleBreastCancerTemp.pesticide" title="农药"   
							value="<s:property value="sampleBreastCancerTemp.pesticide"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_plumbane"
                   	 		name="sampleBreastCancerTemp.plumbane" title="铅"   
							value="<s:property value="sampleBreastCancerTemp.plumbane"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_mercury"
                   	 		name="sampleBreastCancerTemp.mercury" title="汞"   
							value="<s:property value="sampleBreastCancerTemp.mercury"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_cadmium"
                   	 		name="sampleBreastCancerTemp.cadmium" title="镉"   
							value="<s:property value="sampleBreastCancerTemp.cadmium"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                    <td class="label-title" >其它有害物质</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_otherHazardousSubstance"
                   	 		name="sampleBreastCancerTemp.otherHazardousSubstance" title="其它有害物质"   
							value="<s:property value="sampleBreastCancerTemp.otherHazardousSubstance"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >乳房自检</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_breastExa"
                   	 		name="sampleBreastCancerTemp.breastExa" title="乳房自检"   
							value="<s:property value="sampleBreastCancerTemp.breastExa"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>家族信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title" >一级亲属女性患乳腺癌</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.cancer" id="sampleBreastCancerTemp_cancer" >
							<option value="" <s:if test="sampleBreastCancerTemp.cancer==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.cancer==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.cancer==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sickenNum"
                   	 		name="sampleBreastCancerTemp.sickenNum" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.sickenNum"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >初诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_firstAge"
                   	 		name="sampleBreastCancerTemp.firstAge" title="初诊年龄"   
							value="<s:property value="sampleBreastCancerTemp.firstAge"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >乳腺癌类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.breastType" id="sampleBreastCancerTemp_breastType" >
							<option value="" <s:if test="sampleBreastCancerTemp.breastType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.breastType==0">selected="selected" </s:if>>单侧</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.breastType==1">selected="selected" </s:if>>双侧</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note1"
                   	 		name="sampleBreastCancerTemp.note1" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.note1"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >二级亲属女性患乳腺癌</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.cancer2" id="sampleBreastCancerTemp_cancer2" >
							<option value="" <s:if test="sampleBreastCancerTemp.cancer2==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.cancer2==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.cancer2==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sickenNum2"
                   	 		name="sampleBreastCancerTemp.sickenNum2" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.sickenNum2"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >初诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_firstAge2"
                   	 		name="sampleBreastCancerTemp.firstAge2" title="初诊年龄"   
							value="<s:property value="sampleBreastCancerTemp.firstAge2"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >乳腺癌类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.breastType2" id="sampleBreastCancerTemp_breastType2" >
							<option value="" <s:if test="sampleBreastCancerTemp.breastType2==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.breastType2==0">selected="selected" </s:if>>单侧</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.breastType2==1">selected="selected" </s:if>>双侧</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note2"
                   	 		name="sampleBreastCancerTemp.note2" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.note2"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >三级亲属女性患乳腺癌</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.cancer3" id="sampleBreastCancerTemp_cancer3" >
							<option value="" <s:if test="sampleBreastCancerTemp.cancer3==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.cancer3==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.cancer3==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sickenNum3"
                   	 		name="sampleBreastCancerTemp.sickenNum3" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.sickenNum3"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >初诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_firstAge3"
                   	 		name="sampleBreastCancerTemp.firstAge3" title="初诊年龄"   
							value="<s:property value="sampleBreastCancerTemp.firstAge3"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >乳腺癌类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.breastType3" id="sampleBreastCancerTemp_breastType3" >
							<option value="" <s:if test="sampleBreastCancerTemp.breastType3==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.breastType3==0">selected="selected" </s:if>>单侧</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.breastType3==1">selected="selected" </s:if>>双侧</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note3"
                   	 		name="sampleBreastCancerTemp.note3" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.note3"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >一二三级亲属男性患乳腺癌</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.cancerMan" id="sampleBreastCancerTemp_cancerMan" >
							<option value="" <s:if test="sampleBreastCancerTemp.cancerMan==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.cancerMan==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.cancerMan==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >一级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_count"
                   	 		name="sampleBreastCancerTemp.count" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.count"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >二级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_count1"
                   	 		name="sampleBreastCancerTemp.count1" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.count1"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >三级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_count2"
                   	 		name="sampleBreastCancerTemp.count2" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.count2"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note4"
                   	 		name="sampleBreastCancerTemp.note4" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.note4"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >一二三级亲属女性患乳腺癌</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.cancerWoman" id="sampleBreastCancerTemp_cancerWoman" >
							<option value="" <s:if test="sampleBreastCancerTemp.cancerWoman==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.cancerWoman==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.cancerWoman==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >一级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_count3"
                   	 		name="sampleBreastCancerTemp.count3" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.count3"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >二级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_count4"
                   	 		name="sampleBreastCancerTemp.count4" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.count4"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >三级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_count5"
                   	 		name="sampleBreastCancerTemp.count5" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.count5"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note5"
                   	 		name="sampleBreastCancerTemp.note5" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.note5"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >一二三级亲属女性同时患乳腺癌和卵巢癌</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.cancerAll" id="sampleBreastCancerTemp_cancerAll" >
							<option value="" <s:if test="sampleBreastCancerTemp.cancerAll==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.cancerAll==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.cancerAll==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >一级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_toCount1"
                   	 		name="sampleBreastCancerTemp.toCount1" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.toCount1"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >二级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_toCount2"
                   	 		name="sampleBreastCancerTemp.toCount2" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.toCount2"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >三级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_toCount3"
                   	 		name="sampleBreastCancerTemp.toCount3" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.toCount3"/>"
                   	 	/>
                   	</td>
                   
                   	</tr>
                   	<tr>
                   		<td class="label-title" >家族遗传病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.medicalHistory" id="sampleBreastCancerTemp_medicalHistory" >
							<option value="" <s:if test="sampleBreastCancerTemp.medicalHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.medicalHistory==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.medicalHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >一级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_toCount4"
                   	 		name="sampleBreastCancerTemp.toCount4" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.toCount4"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >二级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_toCount5"
                   	 		name="sampleBreastCancerTemp.toCount5" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.toCount2"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title" >三级患病人数</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_toCount6"
                   	 		name="sampleBreastCancerTemp.toCount6" title="患病人数"   
							value="<s:property value="sampleBreastCancerTemp.toCount6"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >遗传病类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_medicalType"
                   	 		name="sampleBreastCancerTemp.medicalType" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.medicalType"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_note6"
                   	 		name="sampleBreastCancerTemp.note6" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.note6"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>费用信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isFee" id="sampleBreastCancerTemp_isFee" >
							<option value="" <s:if test="sampleBreastCancerTemp.isFee==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isFee==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isFee==1">selected="selected" </s:if>>否</option>
					    </select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.privilegeType" id="sampleBreastCancer_privilegeType" >
							<option value="" <s:if test="sampleBreastCancerTemp.privilegeType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.privilegeType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.privilegeType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_linkman_id"
                   	 		name="sampleBreastCancerTemp.linkman.id" title="推荐人ID"   
							value="<s:property value="sampleBreastCancerTemp.linkman.id"/>"/>
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_linkman_name"
                   	 		name="sampleBreastCancerTemp.linkman.name" title="推荐人"   
							value="<s:property value="sampleBreastCancerTemp.linkman.name"/>"/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleBreastCancerTemp.isInvoice" id="sampleBreastCancerTemp_isInvoice" >
							<option value="" <s:if test="sampleBreastCancerTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleBreastCancerTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleBreastCancerTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                    <td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_id"
                   	 		name="sampleBreastCancerTemp.createUser.id" title="录入人"   
                   		 />
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_name" class="text input readonlytrue" readonly="readonly"
                   	 		name="sampleBreastCancerTemp.createUser.name" title="录入人"   
							value="<s:property value="sampleBreastCancerTemp.createUser.name"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_reason"
                   	 		name="sampleBreastCancerTemp.reason" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.reason"/>"
                   	 	/>
                   	</td>
              	</tr>
                <tr>
                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_money"
                   	 		name="sampleBreastCancerTemp.money" title="备注"   
							value="<s:property value="sampleBreastCancerTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sp"
                   	 		name="sampleBreastCancerTemp.sp" title="SP"   
							value="<s:property value="sampleBreastCancerTemp.sp"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_paymentUnit"
                   	 		name="sampleBreastCancerTemp.paymentUnit" title="开票单位"   
							value="<s:property value="sampleBreastCancerTemp.paymentUnit"/>"
                   		 />
                   	</td>
                </tr>
                <tr>
                	<g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reportManUserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_reportMan"
				documentName="sampleBreastCancerTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_reportMan_name"  value="<s:property value="sampleBreastCancerTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_reportMan" name="sampleBreastCancerTemp.reportMan.id"  value="<s:property value="sampleBreastCancerTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 <g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="auditManUserFun" 
 				hasSetFun="true"
				documentId="sampleBreastCancerTemp_auditMan"
				documentName="sampleBreastCancerTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleBreastCancerTemp_auditMan_name"  value="<s:property value="sampleBreastCancerTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleBreastCancerTemp_auditMan" name="sampleBreastCancerTemp.auditMan.id"  value="<s:property value="sampleBreastCancerTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                </tr>
               
                
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBreastCancer.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
