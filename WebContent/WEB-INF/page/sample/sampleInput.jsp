﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleInput.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >信息录入id</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleInput_id"
                   	 name="id" searchField="true" title="信息录入id"   style="display:none"    />
                   	</td>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleInput_code"
                   	 name="code" searchField="true" title="编号"    />
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleInput_name"
                   	 name="name" searchField="true" title="描述"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >年龄</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleInput_age" searchField="true"  name="age"  value="" class="text input" />
                   	</td>
               	 	<td class="label-title" >性别</td>
                   	<td align="left"  >
                   		<select name="gender" id="sampleInput_gender" >
						<option value="" <s:if test="sampleInput.gender==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleInput.gender=='0'">selected="selected" </s:if>>女</option>
    					<option value="1" <s:if test="sampleInput.gender=='1'">selected="selected" </s:if>>男</option>
					</select>
               	 	<td class="label-title" >联系人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleInput_linkman_name" searchField="true"  name="linkman.name"  value="" class="text input" />
 						<input type="hidden" id="sampleInput_linkman" name="linkman.id"  value="" > 
 						<img alt='选择联系人' id='showlinkman' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >出生日期</td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startbirthday" name="startbirthday" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="birthday1" name="birthday##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endbirthday" name="endbirthday" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="birthday2" name="birthday##@@##2"  searchField="true" />
                   	</td>
               	 	<td class="label-title" >联系方式</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="10" id="sampleInput_phone"
                   	 name="phone" searchField="true" title="联系方式"    />
                   	</td>
               	 	<td class="label-title" >状态id</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleInput_state"
                   	 name="state" searchField="true" title="状态id"    />
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleInput_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleInput_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleInput_tree_page"></div>
<!-- 		<select id="temp" style="display: none"> -->
<!-- 			<option value="">请选择</option> -->
<!-- 			<option value="0">原始模板</option> -->
<!-- 			<option value="1">青岛市妇女儿童医院高通量基因测序产前筛查临床申请单</option> -->
<!-- 		</select> -->
</body>
</html>



