
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleChromosomeTemp&id=${sampleChromosomeTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleChromosomeTemoEdit.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleChromosomeTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
				
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
			<tr>
               	 	<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_id" name="sampleChromosomeTemp.id" title="编号" value="<s:property value="sampleChromosomeTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleChromosomeTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleChromosomeTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   		<input type ="hidden" id="saveType" value="rst" >
                   	</td>
                   
                   <td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_code" class="text input readonlytrue" readonly="readonly" name="chromosomeTempNew.sampleInfo.code" title="样本编号" value="<s:property value="chromosomeTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_code" class="text input readonlytrue" readonly="readonly" name="chromosomeTempNew.code" title="样本编号" value="<s:property value="chromosomeTempNew.code"/>" />
                   	</td>
                   	
                   	<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="chromosomeTempNew_name" name="chromosomeTempNew.name" title="描述" value="<s:property value="chromosomeTempNew.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_name" name="sampleChromosomeTemp.name" title="描述" value="<s:property value="sampleChromosomeTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_name" name="chromosomeTemp.name" title="描述" value="<s:property value="chromosomeTemp.name"/>"/>
                   	</td>
                   	
                   	<td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_productId" name="sampleChromosomeTemp.productId"  value="<s:property value="sampleChromosomeTemp.productId"/>"/>
  						<input type="hidden" id="sampleChromosomeTemp_productName" name="sampleChromosomeTemp.productName" value="<s:property value="sampleChromosomeTemp.productName"/>"/>
  						<input type="hidden" id="chromosomeTemp_productId" name="chromosomeTemp.productId" value="<s:property value="chromosomeTemp.productId"/>">
  						<input type="hidden" id="chromosomeTemp_productName" name="chromosomeTemp.productName" value="<s:property value="chromosomeTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.productName"/>"><s:property value="sampleChromosomeTemp.productName"/></option> 
							<option value="<s:property value="chromosomeTemp.productName"/>"><s:property value="chromosomeTemp.productName"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="chromosomeTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="chromosomeTempNew.productId" id="productIdNew" value="<s:property value="chromosomeTempNew.productId"/>" />
                   	</td>
                   		
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_acceptDate" name="sampleChromosomeTemp.acceptDate" value="<s:property value="sampleChromosomeTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_acceptDate" name="chromosomeTemp.acceptDate" value="<s:property value="chromosomeTemp.acceptDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.acceptDate"/>"><s:property value="sampleChromosomeTemp.acceptDate"/></option> 
							<option value="<s:property value="chromosomeTemp.acceptDate"/>"><s:property value="chromosomeTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.acceptDate" id="acceptDateNew" value="<s:property value="chromosomeTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="chromosomeTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_area" name="sampleChromosomeTemp.area"  value="<s:property value="sampleChromosomeTemp.area"/>"/>
  						<input type="hidden" id="chromosomeTemp_area" name="chromosomeTemp.area" value="<s:property value="chromosomeTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.area"/>"><s:property value="sampleChromosomeTemp.area"/></option> 
							<option value="<s:property value="chromosomeTemp.area"/>"><s:property value="chromosomeTemp.area"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.area" id="areaNew" value="<s:property value="chromosomeTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_hospital" name="sampleChromosomeTemp.hospital"  value="<s:property value="sampleChromosomeTemp.hospital"/>"/>
  						<input type="hidden" id="chromosomeTemp_hospital" name="chromosomeTemp.hospital" value="<s:property value="chromosomeTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital"style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.hospital"/>"><s:property value="sampleChromosomeTemp.hospital"/></option> 
							<option value="<s:property value="chromosomeTemp.hospital"/>"><s:property value="chromosomeTemp.hospital"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.hospital" id="hospitalNew" value="<s:property value="chromosomeTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_sendDate" name="sampleChromosomeTemp.sendDate" value="<s:property value="sampleChromosomeTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_sendDate" name="chromosomeTemp.sendDate" value="<s:property value="chromosomeTemp.sendDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.sendDate"/>"><s:property value="sampleChromosomeTemp.sendDate"/></option> 
							<option value="<s:property value="chromosomeTemp.sendDate"/>"><s:property value="chromosomeTemp.sendDate"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.sendDate" id="sendDateNew" value="<s:property value="chromosomeTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})" value="<s:date name="chromosomeTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_reportDate" name="sampleChromosomeTemp.reportDate" value="<s:property value="sampleChromosomeTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_reportDate" name="chromosomeTemp.reportDate" value="<s:property value="chromosomeTemp.reportDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.reportDate"/>"><s:property value="sampleChromosomeTemp.reportDate"/></option> 
							<option value="<s:property value="chromosomeTemp.reportDate"/>"><s:property value="chromosomeTemp.reportDate"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.reportDate" id="reportDateNew" value="<s:property value="chromosomeTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="chromosomeTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_doctor" name="sampleChromosomeTemp.doctor"  value="<s:property value="sampleChromosomeTemp.doctor"/>"/>
  						<input type="hidden" id="chromosomeTemp_doctor" name="chromosomeTemp.doctor" value="<s:property value="chromosomeTemp.doctor"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="doctor"style="width:152px;height:28px;" onChange="javascript:document.getElementById('doctorNew').value=document.getElementById('doctor').options[document.getElementById('doctor').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.doctor"/>"><s:property value="sampleChromosomeTemp.doctor"/></option> 
							<option value="<s:property value="chromosomeTemp.doctor"/>"><s:property value="chromosomeTemp.doctor"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.doctor" id="doctorNew" value="<s:property value="chromosomeTempNew.doctor"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
<!--                   	<td class="label-title" >样本编号</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_sampleNum" data ="sampleNum" -->
<!--                    	 		name="sampleChromosomeTemp.sampleNum" title="序号"    -->
<%-- 							value="<s:property value="sampleChromosomeTemp.sampleNum"/>" --%>
<!--                    	 	/> -->
<!--                     </td> -->
                   	
                   	<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_sampleType_name" name="sampleChromosomeTemp.sampleType.name"  value="<s:property value="sampleChromosomeTemp.sampleType.name"/>"/>
  						<input type="hidden" id="sampleChromosomeTemp_sampleType_id" name="sampleChromosomeTemp.sampleType.id" value="<s:property value="sampleChromosomeTemp.sampleType.id"/>"/>
  						<input type="hidden" id="chromosomeTemp_sampleType_name" name="chromosomeTemp.sampleType.name" value="<s:property value="chromosomeTemp.sampleType.name"/>">
  						<input type="hidden" id="chromosomeTemp_sampleType_id" name="chromosomeTemp.sampleType.id" value="<s:property value="chromosomeTemp.sampleType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.sampleType.name"/>"><s:property value="sampleChromosomeTemp.sampleType.name"/></option> 
							<option value="<s:property value="chromosomeTemp.sampleType.name"/>"><s:property value="chromosomeTemp.sampleType.name"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="chromosomeTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="chromosomeTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="chromosomeTempNew.sampleType.id"/>" />
                   	</td>
                   	
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title">姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_patientName" name="sampleChromosomeTemp.patientName"  value="<s:property value="sampleChromosomeTemp.patientName"/>"/>
  						<input type="hidden" id="chromosomeTemp_patientName" name="chromosomeTemp.patientName" value="<s:property value="chromosomeTemp.patientName"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientName"style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameNew').value=document.getElementById('patientName').options[document.getElementById('patientName').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.patientName"/>"><s:property value="sampleChromosomeTemp.patientName"/></option> 
							<option value="<s:property value="chromosomeTemp.patientName"/>"><s:property value="chromosomeTemp.patientName"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.patientName" id="patientNameNew" value="<s:property value="chromosomeTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
					<td class="label-title">证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_voucherType_name" name="sampleChromosomeTemp.voucherType.name"  value="<s:property value="sampleChromosomeTemp.voucherType.name"/>"/>
  						<input type="hidden" id="sampleChromosomeTemp_voucherType_id" name="sampleChromosomeTemp.voucherType.id" value="<s:property value="sampleChromosomeTemp.voucherType.id"/>"/>
  						<input type="hidden" id="chromosomeTemp_voucherType_name" name="chromosomeTemp.voucherType.name" value="<s:property value="chromosomeTemp.voucherType.name"/>">
  						<input type="hidden" id="chromosomeTemp_voucherType_id" name="chromosomeTemp.voucherType.id" value="<s:property value="chromosomeTemp.voucherType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="voucherType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherTypeNew').value=document.getElementById('voucherType').options[document.getElementById('voucherType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.voucherType.name"/>"><s:property value="sampleChromosomeTemp.voucherType.name"/></option> 
							<option value="<s:property value="chromosomeTemp.voucherType.name"/>"><s:property value="chromosomeTemp.voucherType.name"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.voucherType.name" id="voucherTypeNew" onfocus="sampleKind()" value="<s:property value="chromosomeTempNew.voucherType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="chromosomeTempNew.voucherType.id" id="voucherTypeIdNew" value="<s:property value="chromosomeTempNew.voucherType.id"/>" />
                   	</td>                   	
            	
            		<td class="label-title">证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_voucherCode" name="sampleChromosomeTemp.voucherCode"  value="<s:property value="sampleChromosomeTemp.voucherCode"/>"/>
  						<input type="hidden" id="chromosomeTemp_voucherCode" name="chromosomeTemp.voucherCode" value="<s:property value="chromosomeTemp.voucherCode"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="voucherCode"style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherCodeNew').value=document.getElementById('voucherCode').options[document.getElementById('voucherCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.voucherCode"/>"><s:property value="sampleChromosomeTemp.voucherCode"/></option> 
							<option value="<s:property value="chromosomeTemp.voucherCode"/>"><s:property value="chromosomeTemp.voucherCode"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.voucherCode" id="voucherCodeNew" value="<s:property value="chromosomeTempNew.voucherCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
            	
				  </tr>			
				  <tr>
                   	
                   	<td class="label-title">手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_phoneNum" name="sampleChromosomeTemp.phoneNum"  value="<s:property value="sampleChromosomeTemp.phoneNum"/>"/>
  						<input type="hidden" id="chromosomeTemp_phoneNum" name="chromosomeTemp.phoneNum" value="<s:property value="chromosomeTemp.phoneNum"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phoneNum"style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNumNew').value=document.getElementById('phoneNum').options[document.getElementById('phoneNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.phoneNum"/>"><s:property value="sampleChromosomeTemp.phoneNum"/></option> 
							<option value="<s:property value="chromosomeTemp.phoneNum"/>"><s:property value="chromosomeTemp.phoneNum"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.phoneNum" id="phoneNumNew" value="<s:property value="chromosomeTempNew.phoneNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">手机号码2</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_phoneNum2" name="sampleChromosomeTemp.phoneNum2"  value="<s:property value="sampleChromosomeTemp.phoneNum2"/>"/>
  						<input type="hidden" id="chromosomeTemp_phoneNum2" name="chromosomeTemp.phoneNum2" value="<s:property value="chromosomeTemp.phoneNum2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phoneNum2"style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNum2New').value=document.getElementById('phoneNum2').options[document.getElementById('phoneNum2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.phoneNum2"/>"><s:property value="sampleChromosomeTemp.phoneNum2"/></option> 
							<option value="<s:property value="chromosomeTemp.phoneNum2"/>"><s:property value="chromosomeTemp.phoneNum2"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.phoneNum2" id="phoneNum2New" value="<s:property value="chromosomeTempNew.phoneNum2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	
                   	<td class="label-title">代理人</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_agentMan" name="sampleChromosomeTemp.agentMan"  value="<s:property value="sampleChromosomeTemp.agentMan"/>"/>
  						<input type="hidden" id="chromosomeTemp_agentMan" name="chromosomeTemp.agentMan" value="<s:property value="chromosomeTemp.agentMan"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="agentMan"style="width:152px;height:28px;" onChange="javascript:document.getElementById('agentManNew').value=document.getElementById('agentMan').options[document.getElementById('agentMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.agentMan"/>"><s:property value="sampleChromosomeTemp.agentMan"/></option> 
							<option value="<s:property value="chromosomeTemp.agentMan"/>"><s:property value="chromosomeTemp.agentMan"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.agentMan" id="agentManNew" value="<s:property value="chromosomeTempNew.agentMan"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">代理人身份证</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_cardCode" name="sampleChromosomeTemp.cardCode"  value="<s:property value="sampleChromosomeTemp.cardCode"/>"/>
  						<input type="hidden" id="chromosomeTemp_cardCode" name="chromosomeTemp.cardCode" value="<s:property value="chromosomeTemp.cardCode"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="cardCode"style="width:152px;height:28px;" onChange="javascript:document.getElementById('cardCodeNew').value=document.getElementById('cardCode').options[document.getElementById('cardCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.cardCode"/>"><s:property value="sampleChromosomeTemp.cardCode"/></option> 
							<option value="<s:property value="chromosomeTemp.cardCode"/>"><s:property value="chromosomeTemp.cardCode"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.cardCode" id="cardCodeNew" value="<s:property value="chromosomeTempNew.cardCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_address" name="sampleChromosomeTemp.address"  value="<s:property value="sampleChromosomeTemp.address"/>"/>
  						<input type="hidden" id="chromosomeTemp_address" name="chromosomeTemp.address" value="<s:property value="chromosomeTemp.address"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="address"style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.address"/>"><s:property value="sampleChromosomeTemp.address"/></option> 
							<option value="<s:property value="chromosomeTemp.address"/>"><s:property value="chromosomeTemp.address"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.address" id="addressNew" value="<s:property value="chromosomeTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_emailAddress" name="sampleChromosomeTemp.emailAddress"  value="<s:property value="sampleChromosomeTemp.emailAddress"/>"/>
  						<input type="hidden" id="chromosomeTemp_emailAddress" name="chromosomeTemp.emailAddress" value="<s:property value="chromosomeTemp.emailAddress"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="emailAddress"style="width:152px;height:28px;" onChange="javascript:document.getElementById('emailAddressNew').value=document.getElementById('emailAddress').options[document.getElementById('emailAddress').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.emailAddress"/>"><s:property value="sampleChromosomeTemp.emailAddress"/></option> 
							<option value="<s:property value="chromosomeTemp.emailAddress"/>"><s:property value="chromosomeTemp.emailAddress"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.emailAddress" id="emailAddressNew" value="<s:property value="chromosomeTempNew.emailAddress"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">邮编</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_inHosNum" name="sampleChromosomeTemp.inHosNum"  value="<s:property value="sampleChromosomeTemp.inHosNum"/>"/>
  						<input type="hidden" id="chromosomeTemp_inHosNum" name="chromosomeTemp.inHosNum" value="<s:property value="chromosomeTemp.inHosNum"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="inHosNum"style="width:152px;height:28px;" onChange="javascript:document.getElementById('inHosNumNew').value=document.getElementById('inHosNum').options[document.getElementById('inHosNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.inHosNum"/>"><s:property value="sampleChromosomeTemp.inHosNum"/></option> 
							<option value="<s:property value="chromosomeTemp.inHosNum"/>"><s:property value="chromosomeTemp.inHosNum"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.inHosNum" id="inHosNumNew" value="<s:property value="chromosomeTempNew.inHosNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	
                   	<td class="label-title">送检原因</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_tumorHistory" name="sampleChromosomeTemp.tumorHistory"  value="<s:property value="sampleChromosomeTemp.tumorHistory"/>"/>
  						<input type="hidden" id="chromosomeTemp_tumorHistory" name="chromosomeTemp.tumorHistory" value="<s:property value="chromosomeTemp.tumorHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="tumorHistory"style="width:152px;height:28px;" onChange="javascript:document.getElementById('tumorHistoryNew').value=document.getElementById('tumorHistory').options[document.getElementById('tumorHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.tumorHistory"/>"><s:property value="sampleChromosomeTemp.tumorHistory"/></option> 
							<option value="<s:property value="chromosomeTemp.tumorHistory"/>"><s:property value="chromosomeTemp.tumorHistory"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.tumorHistory" id="tumorHistoryNew" value="<s:property value="chromosomeTempNew.tumorHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                  
                  <tr>
						<td colspan="9">
							<div class="standard-section-header type-title">
								<label>孕产史</label>
							</div>
						</td>
				  </tr>
                  
                  <tr>
                   	
                   	<td class="label-title">不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_badMotherhood" name="sampleChromosomeTemp.badMotherhood"  value="<s:property value="sampleChromosomeTemp.badMotherhood"/>"/>
  						<input type="hidden" id="chromosomeTemp_badMotherhood" name="chromosomeTemp.badMotherhood" value="<s:property value="chromosomeTemp.badMotherhood"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="badMotherhood"style="width:152px;height:28px;" onChange="javascript:document.getElementById('badMotherhoodNew').value=document.getElementById('badMotherhood').options[document.getElementById('badMotherhood').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.badMotherhood"/>"><s:property value="sampleChromosomeTemp.badMotherhood"/></option> 
							<option value="<s:property value="chromosomeTemp.badMotherhood"/>"><s:property value="chromosomeTemp.badMotherhood"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.badMotherhood" id="badMotherhoodNew" value="<s:property value="chromosomeTempNew.badMotherhood"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">孕几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_pregnancyTime" name="sampleChromosomeTemp.pregnancyTime"  value="<s:property value="sampleChromosomeTemp.pregnancyTime"/>"/>
  						<input type="hidden" id="chromosomeTemp_pregnancyTime" name="chromosomeTemp.pregnancyTime" value="<s:property value="chromosomeTemp.pregnancyTime"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="pregnancyTime"style="width:152px;height:28px;" onChange="javascript:document.getElementById('pregnancyTimeNew').value=document.getElementById('pregnancyTime').options[document.getElementById('pregnancyTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.pregnancyTime"/>"><s:property value="sampleChromosomeTemp.pregnancyTime"/></option> 
							<option value="<s:property value="chromosomeTemp.pregnancyTime"/>"><s:property value="chromosomeTemp.pregnancyTime"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.pregnancyTime" id="pregnancyTimeNew" value="<s:property value="chromosomeTempNew.pregnancyTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">产几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_parturitionTime" name="sampleChromosomeTemp.parturitionTime"  value="<s:property value="sampleChromosomeTemp.parturitionTime"/>"/>
  						<input type="hidden" id="chromosomeTemp_parturitionTime" name="chromosomeTemp.parturitionTime" value="<s:property value="chromosomeTemp.parturitionTime"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="parturitionTime"style="width:152px;height:28px;" onChange="javascript:document.getElementById('parturitionTimeNew').value=document.getElementById('parturitionTime').options[document.getElementById('parturitionTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.parturitionTime"/>"><s:property value="sampleChromosomeTemp.parturitionTime"/></option> 
							<option value="<s:property value="chromosomeTemp.parturitionTime"/>"><s:property value="chromosomeTemp.parturitionTime"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.parturitionTime" id="parturitionTimeNew" value="<s:property value="chromosomeTempNew.parturitionTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">此次第几胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_howManyfetus" name="sampleChromosomeTemp.howManyfetus"  value="<s:property value="sampleChromosomeTemp.howManyfetus"/>"/>
  						<input type="hidden" id="chromosomeTemp_howManyfetus" name="chromosomeTemp.howManyfetus" value="<s:property value="chromosomeTemp.howManyfetus"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="howManyfetus"style="width:152px;height:28px;" onChange="javascript:document.getElementById('howManyfetusNew').value=document.getElementById('howManyfetus').options[document.getElementById('howManyfetus').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.howManyfetus"/>"><s:property value="sampleChromosomeTemp.howManyfetus"/></option> 
							<option value="<s:property value="chromosomeTemp.howManyfetus"/>"><s:property value="chromosomeTemp.howManyfetus"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.howManyfetus" id="howManyfetusNew" value="<s:property value="chromosomeTempNew.howManyfetus"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">具体情况描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_reason2" name="sampleChromosomeTemp.reason2"  value="<s:property value="sampleChromosomeTemp.reason2"/>"/>
  						<input type="hidden" id="chromosomeTemp_reason2" name="chromosomeTemp.reason2" value="<s:property value="chromosomeTemp.reason2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reason2"style="width:152px;height:28px;" onChange="javascript:document.getElementById('reason2New').value=document.getElementById('reason2').options[document.getElementById('reason2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.reason2"/>"><s:property value="sampleChromosomeTemp.reason2"/></option> 
							<option value="<s:property value="chromosomeTemp.reason2"/>"><s:property value="chromosomeTemp.reason2"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.reason2" id="reason2New" value="<s:property value="chromosomeTempNew.reason2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">血型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_organGrafting" name="sampleChromosomeTemp.organGrafting"  value="<s:property value="sampleChromosomeTemp.organGrafting"/>"/>
  						<input type="hidden" id="chromosomeTemp_organGrafting" name="chromosomeTemp.organGrafting" value="<s:property value="chromosomeTemp.organGrafting"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="organGrafting"style="width:152px;height:28px;" onChange="javascript:document.getElementById('organGraftingNew').value=document.getElementById('organGrafting').options[document.getElementById('organGrafting').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.organGrafting"/>"><s:property value="sampleChromosomeTemp.organGrafting"/></option> 
							<option value="<s:property value="chromosomeTemp.organGrafting"/>"><s:property value="chromosomeTemp.organGrafting"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.organGrafting" id="organGraftingNew" value="<s:property value="chromosomeTempNew.organGrafting"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	
                   	<td class="label-title">受孕方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_outTransfusion" name="sampleChromosomeTemp.outTransfusion" value="<s:property value="sampleChromosomeTemp.outTransfusion"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_outTransfusion" name="chromosomeTemp.outTransfusion"  value="<s:property value="chromosomeTemp.outTransfusion"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.outTransfusion" id="chromosomeTempNew_outTransfusion">
							<option value="" <s:if test="chromosomeTempNew.outTransfusion==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.outTransfusion==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.outTransfusion==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">单胎/非单胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_embryoType" name="sampleChromosomeTemp.embryoType" value="<s:property value="sampleChromosomeTemp.embryoType"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_embryoType" name="chromosomeTemp.embryoType"  value="<s:property value="chromosomeTemp.embryoType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.embryoType" id="chromosomeTempNew_embryoType">
							<option value="" <s:if test="chromosomeTempNew.embryoType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.embryoType==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.embryoType==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">流产孕周</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_gestationalAge" name="sampleChromosomeTemp.gestationalAge"  value="<s:property value="sampleChromosomeTemp.gestationalAge"/>"/>
  						<input type="hidden" id="chromosomeTemp_gestationalAge" name="chromosomeTemp.gestationalAge" value="<s:property value="chromosomeTemp.gestationalAge"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="gestationalAge"style="width:152px;height:28px;" onChange="javascript:document.getElementById('gestationalAgeNew').value=document.getElementById('gestationalAge').options[document.getElementById('gestationalAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.gestationalAge"/>"><s:property value="sampleChromosomeTemp.gestationalAge"/></option> 
							<option value="<s:property value="chromosomeTemp.gestationalAge"/>"><s:property value="chromosomeTemp.gestationalAge"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.gestationalAge" id="gestationalAgeNew" value="<s:property value="chromosomeTempNew.gestationalAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_diagnosis" name="sampleChromosomeTemp.diagnosis"  value="<s:property value="sampleChromosomeTemp.diagnosis"/>"/>
  						<input type="hidden" id="chromosomeTemp_diagnosis" name="chromosomeTemp.diagnosis" value="<s:property value="chromosomeTemp.diagnosis"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="diagnosis"style="width:152px;height:28px;" onChange="javascript:document.getElementById('diagnosisNew').value=document.getElementById('diagnosis').options[document.getElementById('diagnosis').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.diagnosis"/>"><s:property value="sampleChromosomeTemp.diagnosis"/></option> 
							<option value="<s:property value="chromosomeTemp.diagnosis"/>"><s:property value="chromosomeTemp.diagnosis"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.diagnosis" id="diagnosisNew" value="<s:property value="chromosomeTempNew.diagnosis"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>其它信息</label>
						</div>
					</td>
				  </tr>
                  <tr>
                   	
                   	<td class="label-title">抗生素</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_stemCellsCure" name="sampleChromosomeTemp.stemCellsCure" value="<s:property value="sampleChromosomeTemp.stemCellsCure"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_stemCellsCure" name="chromosomeTemp.stemCellsCure"  value="<s:property value="chromosomeTemp.stemCellsCure"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.stemCellsCure" id="chromosomeTempNew_stemCellsCure">
							<option value="" <s:if test="chromosomeTempNew.stemCellsCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.stemCellsCure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.stemCellsCure==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">避孕药</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_immuneCure" name="sampleChromosomeTemp.immuneCure" value="<s:property value="sampleChromosomeTemp.immuneCure"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_immuneCure" name="chromosomeTemp.immuneCure"  value="<s:property value="chromosomeTemp.immuneCure"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.immuneCure" id="chromosomeTempNew_immuneCure">
							<option value="" <s:if test="chromosomeTempNew.immuneCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.immuneCure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.immuneCure==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">核型分析</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_coreAnalyze" name="sampleChromosomeTemp.coreAnalyze" value="<s:property value="sampleChromosomeTemp.coreAnalyze"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_coreAnalyze" name="chromosomeTemp.coreAnalyze"  value="<s:property value="chromosomeTemp.coreAnalyze"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.coreAnalyze" id="chromosomeTempNew_coreAnalyze">
							<option value="" <s:if test="chromosomeTempNew.coreAnalyze==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.coreAnalyze==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.coreAnalyze==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">其它</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_note" name="sampleChromosomeTemp.note" value="<s:property value="sampleChromosomeTemp.note"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_note" name="chromosomeTemp.note"  value="<s:property value="chromosomeTemp.note"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.note" id="chromosomeTempNew_note">
							<option value="" <s:if test="chromosomeTempNew.note==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.note==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.note==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">附加样本1（姓名、关系）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_attachedSample1" name="sampleChromosomeTemp.attachedSample1"  value="<s:property value="sampleChromosomeTemp.attachedSample1"/>"/>
  						<input type="hidden" id="chromosomeTemp_attachedSample1" name="chromosomeTemp.attachedSample1" value="<s:property value="chromosomeTemp.attachedSample1"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="attachedSample1"style="width:152px;height:28px;" onChange="javascript:document.getElementById('attachedSample1New').value=document.getElementById('attachedSample1').options[document.getElementById('attachedSample1').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.attachedSample1"/>"><s:property value="sampleChromosomeTemp.attachedSample1"/></option> 
							<option value="<s:property value="chromosomeTemp.attachedSample1"/>"><s:property value="chromosomeTemp.attachedSample1"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.attachedSample1" id="attachedSample1New" value="<s:property value="chromosomeTempNew.attachedSample1"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">附加样本2（姓名、关系）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_attachedSample2" name="sampleChromosomeTemp.attachedSample2"  value="<s:property value="sampleChromosomeTemp.attachedSample2"/>"/>
  						<input type="hidden" id="chromosomeTemp_attachedSample2" name="chromosomeTemp.attachedSample2" value="<s:property value="chromosomeTemp.attachedSample2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="attachedSample2"style="width:152px;height:28px;" onChange="javascript:document.getElementById('attachedSample2New').value=document.getElementById('attachedSample2').options[document.getElementById('attachedSample2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.attachedSample2"/>"><s:property value="sampleChromosomeTemp.attachedSample2"/></option> 
							<option value="<s:property value="chromosomeTemp.attachedSample2"/>"><s:property value="chromosomeTemp.attachedSample2"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.attachedSample2" id="attachedSample2New" value="<s:property value="chromosomeTempNew.attachedSample2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">特殊情况说明</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_specialCircumstances" name="sampleChromosomeTemp.specialCircumstances"  value="<s:property value="sampleChromosomeTemp.specialCircumstances"/>"/>
  						<input type="hidden" id="chromosomeTemp_specialCircumstances" name="chromosomeTemp.specialCircumstances" value="<s:property value="chromosomeTemp.specialCircumstances"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="specialCircumstances"style="width:152px;height:28px;" onChange="javascript:document.getElementById('specialCircumstancesNew').value=document.getElementById('specialCircumstances').options[document.getElementById('specialCircumstances').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.specialCircumstances"/>"><s:property value="sampleChromosomeTemp.specialCircumstances"/></option> 
							<option value="<s:property value="chromosomeTemp.specialCircumstances"/>"><s:property value="chromosomeTemp.specialCircumstances"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.specialCircumstances" id="specialCircumstancesNew" value="<s:property value="chromosomeTempNew.specialCircumstances"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_isInsure" name="sampleChromosomeTemp.isInsure" value="<s:property value="sampleChromosomeTemp.isInsure"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_isInsure" name="chromosomeTemp.isInsure"  value="<s:property value="chromosomeTemp.isInsure"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.isInsure" id="chromosomeTempNew_isInsure">
							<option value="" <s:if test="chromosomeTempNew.isInsure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.isInsure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.isInsure==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleChromosomeTemp_isInvoice" name="sampleChromosomeTemp.isInvoice" value="<s:property value="sampleChromosomeTemp.isInvoice"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="chromosomeTemp_isInvoice" name="chromosomeTemp.isInvoice"  value="<s:property value="chromosomeTemp.isInvoice"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.isInvoice" id="chromosomeTempNew_isInvoice">
							<option value="" <s:if test="chromosomeTempNew.isInvoice==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="chromosomeTempNew.isInvoice==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="chromosomeTempNew.isInvoice==1">selected="selected" </s:if>>无</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="chromosomeTempNew_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="chromosomeTempNew.createUser" title="审入人"   
							value="<s:property value="chromosomeTempNew.createUser"/>" />
                   	</td>
                   	
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_reason" name="sampleChromosomeTemp.reason"  value="<s:property value="sampleChromosomeTemp.reason"/>"/>
  						<input type="hidden" id="chromosomeTemp_reason" name="chromosomeTemp.reason" value="<s:property value="chromosomeTemp.reason"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reason"style="width:152px;height:28px;" onChange="javascript:document.getElementById('reasonNew').value=document.getElementById('reason').options[document.getElementById('reason').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.reason"/>"><s:property value="sampleChromosomeTemp.reason"/></option> 
							<option value="<s:property value="chromosomeTemp.reason"/>"><s:property value="chromosomeTemp.reason"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.reason" id="reasonNew" value="<s:property value="chromosomeTempNew.reason"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
              
                   	<td class="label-title">金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_money" name="sampleChromosomeTemp.money"  value="<s:property value="sampleChromosomeTemp.money"/>"/>
  						<input type="hidden" id="chromosomeTemp_money" name="chromosomeTemp.money" value="<s:property value="chromosomeTemp.money"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="money"style="width:152px;height:28px;" onChange="javascript:document.getElementById('moneyNew').value=document.getElementById('money').options[document.getElementById('money').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.money"/>"><s:property value="sampleChromosomeTemp.money"/></option> 
							<option value="<s:property value="chromosomeTemp.money"/>"><s:property value="chromosomeTemp.money"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.money" id="moneyNew" value="<s:property value="chromosomeTempNew.money"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">收据（S）/pos条（P）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_receipt" name="sampleChromosomeTemp.receipt"  value="<s:property value="sampleChromosomeTemp.receipt"/>"/>
  						<input type="hidden" id="chromosomeTemp_receipt" name="chromosomeTemp.receipt" value="<s:property value="chromosomeTemp.receipt"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="receipt"style="width:152px;height:28px;" onChange="javascript:document.getElementById('receiptNew').value=document.getElementById('receipt').options[document.getElementById('receipt').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.receipt"/>"><s:property value="sampleChromosomeTemp.receipt"/></option> 
							<option value="<s:property value="chromosomeTemp.receipt"/>"><s:property value="chromosomeTemp.receipt"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.receipt" id="receiptNew" value="<s:property value="chromosomeTempNew.receipt"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_paymentUnit" name="sampleChromosomeTemp.paymentUnit"  value="<s:property value="sampleChromosomeTemp.paymentUnit"/>"/>
  						<input type="hidden" id="chromosomeTemp_paymentUnit" name="chromosomeTemp.paymentUnit" value="<s:property value="chromosomeTemp.paymentUnit"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="paymentUnit"style="width:152px;height:28px;" onChange="javascript:document.getElementById('paymentUnitNew').value=document.getElementById('paymentUnit').options[document.getElementById('paymentUnit').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.paymentUnit"/>"><s:property value="sampleChromosomeTemp.paymentUnit"/></option> 
							<option value="<s:property value="chromosomeTemp.paymentUnit"/>"><s:property value="chromosomeTemp.paymentUnit"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.paymentUnit" id="paymentUnitNew" value="<s:property value="chromosomeTempNew.paymentUnit"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                	<g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="UserFun" 
 					hasSetFun="true"
					documentId="sampleChromosomeTemp_reportMan"
					documentName="sampleChromosomeTemp_reportMan_name" />
			
			  	 	<td class="label-title">核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_reportMan_id" name="sampleChromosomeTemp.reportMan.id"  value="<s:property value="sampleChromosomeTemp.reportMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleChromosomeTemp_reportMan_name" name="sampleChromosomeTemp.reportMan.name"  value="<s:property value="sampleChromosomeTemp.reportMan.name"/>" class="text input" />
  						<input type="hidden" id="chromosomeTemp_reportMan_id" name="chromosomeTemp.reportMan.id" value="<s:property value="chromosomeTemp.reportMan.id"/>">
  						<input type="hidden" id="chromosomeTemp_reportMan_name" name="chromosomeTemp.reportMan.name" value="<s:property value="chromosomeTemp.reportMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reportMan" style="width:152px;height:28px;margin-top: 8px;" onChange="javascript:document.getElementById('reportManNew').value=document.getElementById('reportMan').options[document.getElementById('reportMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.reportMan.name"/>"><s:property value="sampleChromosomeTemp.reportMan.name"/></option> 
							<option value="<s:property value="chromosomeTemp.reportMan.name"/>"><s:property value="chromosomeTemp.reportMan.name"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.reportMan.name" id="reportManNew" value="<s:property value="chromosomeTempNew.reportMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="chromosomeTempNew.reportMan.id" id="reportManIdNew" value="<s:property value="chromosomeTempNew.reportMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' style="margin-left: 20px;" class='detail' /> --%>
                   	</td>
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="UserFun" 
 					hasSetFun="true"
					documentId="sampleChromosomeTemp_auditMan"
					documentName="sampleChromosomeTemp_auditMan_name" />
			
			  	 	<td class="label-title">核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleChromosomeTemp_auditMan_id" name="sampleChromosomeTemp.auditMan.id"  value="<s:property value="sampleChromosomeTemp.auditMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleChromosomeTemp_auditMan_name" name="sampleChromosomeTemp.auditMan.name"  value="<s:property value="sampleChromosomeTemp.auditMan.name"/>" class="text input" />
  						<input type="hidden" id="chromosomeTemp_auditMan_id" name="chromosomeTemp.auditMan.id" value="<s:property value="chromosomeTemp.auditMan.id"/>">
  						<input type="hidden" id="chromosomeTemp_auditMan_name" name="chromosomeTemp.auditMan.name" value="<s:property value="chromosomeTemp.auditMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="auditMan" style="width:152px;height:28px;margin-top: 8px;" onChange="javascript:document.getElementById('auditManNew').value=document.getElementById('auditMan').options[document.getElementById('auditMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleChromosomeTemp.auditMan.name"/>"><s:property value="sampleChromosomeTemp.auditMan.name"/></option> 
							<option value="<s:property value="chromosomeTemp.auditMan.name"/>"><s:property value="chromosomeTemp.auditMan.name"/></option>
						</select> 
						<input type="text" name="chromosomeTempNew.auditMan.name" id="auditManNew" value="<s:property value="chromosomeTempNew.auditMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="chromosomeTempNew.auditMan.id" id="auditManIdNew" value="<s:property value="chromosomeTempNew.auditMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' style="margin-left: 20px;" class='detail' /> --%>
                   	</td>
                   	
                	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="chromosomeTempNew.nextStepFlow" id="chromosomeTempNew_nextStepFlow">
							<option value="" <s:if test="chromosomeTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="chromosomeTempNew.nextStepFlow==1">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="chromosomeTempNew.nextStepFlow==0">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                	
                	
                  </tr>
               
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleChromosome.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
