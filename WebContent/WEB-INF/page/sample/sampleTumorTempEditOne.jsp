
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleTumorTemp&id=${sampleTumorTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleTumorTempEditOne.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:640px;float:left;" id="sampleInputItemImg"><img onmousewheel="return changeimg(this)" id="upLoadImg" src="${ctx}/operfile/downloadById.action?id=${sampleTumorTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="path" value="${requestScope.path}">
            <input type="hidden" id="fname" value="${requestScope.fname}">
            <input type="hidden"  id="str" value="${requestScope.str}">
            <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
					
					<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_id" name="sampleTumorTemp.id" title="编号" value="<s:property value="sampleTumorTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleTumorTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleTumorTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   		<input type ="hidden" id="saveType" value="zl" >
                   	</td>
					
               	 	<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_code" class="text input readonlytrue" readonly="readonly" name="tumorTempNew.sampleInfo.code" title="样本编号" value="<s:property value="tumorTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="tumorTempNew_code" class="text input readonlytrue" readonly="readonly" name="tumorTempNew.code" title="样本编号" value="<s:property value="tumorTempNew.code"/>" />
                   	</td>
                   
                   	<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="tumorTempNew_name" name="tumorTempNew.name" title="描述" value="<s:property value="tumorTempNew.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_name" name="sampleTumorTemp.name" title="描述" value="<s:property value="sampleTumorTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="tumorTemp_name" name="tumorTemp.name" title="描述" value="<s:property value="tumorTemp.name"/>"/>
                   	</td>
                   	
                   	<td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_productId" name="sampleTumorTemp.productId"  value="<s:property value="sampleTumorTemp.productId"/>"/>
  						<input type="hidden" id="sampleTumorTemp_productName" name="sampleTumorTemp.productName" value="<s:property value="sampleTumorTemp.productName"/>"/>
  						<input type="hidden" id="tumorTemp_productId" name="tumorTemp.productId" value="<s:property value="tumorTemp.productId"/>">
  						<input type="hidden" id="tumorTemp_productName" name="tumorTemp.productName" value="<s:property value="tumorTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.productName"/>"><s:property value="sampleTumorTemp.productName"/></option> 
							<option value="<s:property value="tumorTemp.productName"/>"><s:property value="tumorTemp.productName"/></option>
						</select> 
						<input type="text" name="tumorTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="tumorTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="tumorTempNew.productId" id="productIdNew" value="<s:property value="tumorTempNew.productId"/>" />
                   	</td>
                   	
                  </tr>
                  <tr>  	
                   	<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_sampleType_id" name="sampleTumorTemp.sampleType.id"  value="<s:property value="sampleTumorTemp.sampleType.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleTumorTemp_sampleType_name" name="sampleTumorTemp.sampleType.name"  value="<s:property value="sampleTumorTemp.sampleType.name"/>" class="text input" />
  						<input type="hidden" id="tumorTemp_sampleType_id" name="tumorTemp.sampleType.id" value="<s:property value="tumorTemp.sampleType.id"/>">
  						<input type="hidden" id="tumorTemp_sampleType_name" name="tumorTemp.sampleType.name" value="<s:property value="tumorTemp.sampleType.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.sampleType.name"/>"><s:property value="sampleTumorTemp.sampleType.name"/></option> 
							<option value="<s:property value="tumorTemp.sampleType.name"/>"><s:property value="tumorTemp.sampleType.name"/></option>
						</select> 
						<input type="text" name="tumorTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="tumorTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
            			<input type="hidden" name="tumorTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="tumorTempNew.sampleType.id"/>" />
                   	</td>
                   	<td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_acceptDate" name="sampleTumorTemp.acceptDate" value="<s:property value="sampleTumorTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="tumorTemp_acceptDate" name="tumorTemp.acceptDate" value="<s:property value="tumorTemp.acceptDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.acceptDate"/>"><s:property value="sampleTumorTemp.acceptDate"/></option> 
							<option value="<s:property value="tumorTemp.acceptDate"/>"><s:property value="tumorTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="tumorTempNew.acceptDate" id="acceptDateNew" value="<s:property value="tumorTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="tumorTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_area" name="sampleTumorTemp.area"  value="<s:property value="sampleTumorTemp.area"/>"/>
  						<input type="hidden" id="tumorTemp_area" name="tumorTemp.area" value="<s:property value="tumorTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.area"/>"><s:property value="sampleTumorTemp.area"/></option> 
							<option value="<s:property value="tumorTemp.area"/>"><s:property value="tumorTemp.area"/></option>
						</select> 
						<input type="text" name="tumorTempNew.area" id="areaNew" value="<s:property value="tumorTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>	
                  <tr>
                  	<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_hospital" name="sampleTumorTemp.hospital"  value="<s:property value="sampleTumorTemp.hospital"/>"/>
  						<input type="hidden" id="tumorTemp_hospital" name="tumorTemp.hospital" value="<s:property value="tumorTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital" style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.hospital"/>"><s:property value="sampleTumorTemp.hospital"/></option> 
							<option value="<s:property value="tumorTemp.hospital"/>"><s:property value="tumorTemp.hospital"/></option>
						</select> 
						<input type="text" name="tumorTempNew.hospital" id="hospitalNew" value="<s:property value="tumorTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  	
                   	<td class="label-title">送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_sendDate" name="sampleTumorTemp.sendDate" value="<s:property value="sampleTumorTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="tumorTemp_sendDate" name="tumorTemp.sendDate" value="<s:property value="tumorTemp.sendDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.sendDate"/>"><s:property value="sampleTumorTemp.sendDate"/></option> 
							<option value="<s:property value="tumorTemp.sendDate"/>"><s:property value="tumorTemp.sendDate"/></option>
						</select> 
						<input type="text" name="tumorTempNew.sendDate" id="sendDateNew" value="<s:property value="tumorTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})" value="<s:date name="tumorTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_reportDate" name="sampleTumorTemp.reportDate" value="<s:property value="sampleTumorTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="tumorTemp_reportDate" name="tumorTemp.reportDate" value="<s:property value="tumorTemp.reportDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.reportDate"/>"><s:property value="sampleTumorTemp.reportDate"/></option> 
							<option value="<s:property value="tumorTemp.reportDate"/>"><s:property value="tumorTemp.reportDate"/></option>
						</select> 
						<input type="text" name="tumorTempNew.reportDate" id="reportDateNew" value="<s:property value="tumorTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="tumorTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   
                   	</tr>
                   	<tr>
                   	<td class="label-title">送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_doctor" name="sampleTumorTemp.doctor"  value="<s:property value="sampleTumorTemp.doctor"/>"/>
  						<input type="hidden" id="tumorTemp_doctor" name="tumorTemp.doctor" value="<s:property value="tumorTemp.doctor"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="doctor"style="width:152px;height:28px;" onChange="javascript:document.getElementById('doctorNew').value=document.getElementById('doctor').options[document.getElementById('doctor').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.doctor"/>"><s:property value="sampleTumorTemp.doctor"/></option> 
							<option value="<s:property value="tumorTemp.doctor"/>"><s:property value="tumorTemp.doctor"/></option>
						</select> 
						<input type="text" name="tumorTempNew.doctor" id="doctorNew" value="<s:property value="tumorTempNew.doctor"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">检测内容</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_detectionContent" name="sampleTumorTemp.detectionContent"  value="<s:property value="sampleTumorTemp.detectionContent"/>"/>
  						<input type="hidden" id="tumorTemp_detectionContent" name="tumorTemp.detectionContent" value="<s:property value="tumorTemp.detectionContent"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="detectionContent"style="width:152px;height:28px;" onChange="javascript:document.getElementById('detectionContentNew').value=document.getElementById('detectionContent').options[document.getElementById('detectionContent').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.detectionContent"/>"><s:property value="sampleTumorTemp.detectionContent"/></option> 
							<option value="<s:property value="tumorTemp.detectionContent"/>"><s:property value="tumorTemp.detectionContent"/></option>
						</select> 
						<input type="text" name="tumorTempNew.detectionContent" id="detectionContentNew" value="<s:property value="tumorTempNew.detectionContent"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
<!--                    	<td class="label-title" >样本编号</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_sampleNum" -->
<!--                    	 		name="sampleTumorTemp.sampleNum" title="序号"    -->
<%-- 							value="<s:property value="sampleTumorTemp.sampleNum"/>" --%>
<!--                    	 	/> -->
<!--                     </td> -->
                   	</tr>
                   	<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                   	<tr>
                   	<td class="label-title">姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_patientName" name="sampleTumorTemp.patientName"  value="<s:property value="sampleTumorTemp.patientName"/>"/>
  						<input type="hidden" id="tumorTemp_patientName" name="tumorTemp.patientName" value="<s:property value="tumorTemp.patientName"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientName"style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameNew').value=document.getElementById('patientName').options[document.getElementById('patientName').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.patientName"/>"><s:property value="sampleTumorTemp.patientName"/></option> 
							<option value="<s:property value="tumorTemp.patientName"/>"><s:property value="tumorTemp.patientName"/></option>
						</select> 
						<input type="text" name="tumorTempNew.patientName" id="patientNameNew" value="<s:property value="tumorTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
<!--                    	<td class="label-title" >姓名拼音</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_patientNameSpell" -->
<!--                    	 		name="sampleTumorTemp.patientNameSpell" title="姓名拼音"    -->
<%-- 							value="<s:property value="sampleTumorTemp.patientNameSpell"/>" --%>
<!--                    	 	/> -->
<!--                    	</td> -->
               		
                   	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_gender" name="sampleTumorTemp.gender" value="<s:property value="sampleTumorTemp.gender"/>" />
               	 		<input type="hidden" id="tumorTemp_gender" name="tumorTemp.gender" value="<s:property value="tumorTemp.gender"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.gender" id="tumorTempNew_gender" >
							<option value="" <s:if test="tumorTempNew.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="tumorTempNew.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title">年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_age" name="sampleTumorTemp.age"  value="<s:property value="sampleTumorTemp.age"/>"/>
  						<input type="hidden" id="tumorTemp_age" name="tumorTemp.age" value="<s:property value="tumorTemp.age"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="age"style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.age"/>"><s:property value="sampleTumorTemp.age"/></option> 
							<option value="<s:property value="tumorTemp.age"/>"><s:property value="tumorTemp.age"/></option>
						</select> 
						<input type="text" name="tumorTempNew.age" id="ageNew" value="<s:property value="tumorTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_nativePlace" name="sampleTumorTemp.nativePlace"  value="<s:property value="sampleTumorTemp.nativePlace"/>"/>
  						<input type="hidden" id="tumorTemp_nativePlace" name="tumorTemp.nativePlace" value="<s:property value="tumorTemp.nativePlace"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="nativePlace"style="width:152px;height:28px;" onChange="javascript:document.getElementById('nativePlaceNew').value=document.getElementById('nativePlace').options[document.getElementById('nativePlace').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.nativePlace"/>"><s:property value="sampleTumorTemp.nativePlace"/></option> 
							<option value="<s:property value="tumorTemp.nativePlace"/>"><s:property value="tumorTemp.nativePlace"/></option>
						</select> 
						<input type="text" name="tumorTempNew.nativePlace" id="nativePlaceNew" value="<s:property value="tumorTempNew.nativePlace"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_nationality" name="sampleTumorTemp.nationality"  value="<s:property value="sampleTumorTemp.nationality"/>"/>
  						<input type="hidden" id="tumorTemp_nationality" name="tumorTemp.nationality" value="<s:property value="tumorTemp.nationality"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="nationality"style="width:152px;height:28px;" onChange="javascript:document.getElementById('nationalityNew').value=document.getElementById('nationality').options[document.getElementById('nationality').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.nationality"/>"><s:property value="sampleTumorTemp.nationality"/></option> 
							<option value="<s:property value="tumorTemp.nationality"/>"><s:property value="tumorTemp.nationality"/></option>
						</select> 
						<input type="text" name="tumorTempNew.nationality" id="nationalityNew" value="<s:property value="tumorTempNew.nationality"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title" >婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_isInsure" name="sampleTumorTemp.isInsure" value="<s:property value="sampleTumorTemp.isInsure"/>" />
               	 		<input type="hidden" id="tumorTemp_isInsure" name="tumorTemp.isInsure" value="<s:property value="tumorTemp.isInsure"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.isInsure" id="tumorTempNew_isInsure" >
							<option value="" <s:if test="tumorTempNew.isInsure==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.isInsure==0">selected="selected" </s:if>>已婚</option>
    						<option value="1" <s:if test="tumorTempNew.isInsure==1">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">身高</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_heights" name="sampleTumorTemp.heights"  value="<s:property value="sampleTumorTemp.heights"/>"/>
  						<input type="hidden" id="tumorTemp_heights" name="tumorTemp.heights" value="<s:property value="tumorTemp.heights"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="heights"style="width:152px;height:28px;" onChange="javascript:document.getElementById('heightsNew').value=document.getElementById('heights').options[document.getElementById('heights').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.heights"/>"><s:property value="sampleTumorTemp.heights"/></option> 
							<option value="<s:property value="tumorTemp.heights"/>"><s:property value="tumorTemp.heights"/></option>
						</select> 
						<input type="text" name="tumorTempNew.heights" id="heightsNew" value="<s:property value="tumorTempNew.heights"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_weight" name="sampleTumorTemp.weight"  value="<s:property value="sampleTumorTemp.weight"/>"/>
  						<input type="hidden" id="tumorTemp_weight" name="tumorTemp.weight" value="<s:property value="tumorTemp.weight"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="weight"style="width:152px;height:28px;" onChange="javascript:document.getElementById('weightNew').value=document.getElementById('weight').options[document.getElementById('weight').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.weight"/>"><s:property value="sampleTumorTemp.weight"/></option> 
							<option value="<s:property value="tumorTemp.weight"/>"><s:property value="tumorTemp.weight"/></option>
						</select> 
						<input type="text" name="tumorTempNew.weight" id="weightNew" value="<s:property value="tumorTempNew.weight"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	</tr>
                   	<tr>
                   	<td class="label-title">血压</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_bloodPressure" name="sampleTumorTemp.bloodPressure"  value="<s:property value="sampleTumorTemp.bloodPressure"/>"/>
  						<input type="hidden" id="tumorTemp_bloodPressure" name="tumorTemp.bloodPressure" value="<s:property value="tumorTemp.bloodPressure"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="bloodPressure"style="width:152px;height:28px;" onChange="javascript:document.getElementById('bloodPressureNew').value=document.getElementById('bloodPressure').options[document.getElementById('bloodPressure').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.bloodPressure"/>"><s:property value="sampleTumorTemp.bloodPressure"/></option> 
							<option value="<s:property value="tumorTemp.bloodPressure"/>"><s:property value="tumorTemp.bloodPressure"/></option>
						</select> 
						<input type="text" name="tumorTempNew.bloodPressure" id="bloodPressureNew" value="<s:property value="tumorTempNew.bloodPressure"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">心电图</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_electrocardiogram" name="sampleTumorTemp.electrocardiogram"  value="<s:property value="sampleTumorTemp.electrocardiogram"/>"/>
  						<input type="hidden" id="tumorTemp_electrocardiogram" name="tumorTemp.electrocardiogram" value="<s:property value="tumorTemp.electrocardiogram"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="electrocardiogram"style="width:152px;height:28px;" onChange="javascript:document.getElementById('electrocardiogramNew').value=document.getElementById('electrocardiogram').options[document.getElementById('electrocardiogram').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.electrocardiogram"/>"><s:property value="sampleTumorTemp.electrocardiogram"/></option> 
							<option value="<s:property value="tumorTemp.electrocardiogram"/>"><s:property value="tumorTemp.electrocardiogram"/></option>
						</select> 
						<input type="text" name="tumorTempNew.electrocardiogram" id="electrocardiogramNew" value="<s:property value="tumorTempNew.electrocardiogram"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	</tr>
                   	<tr>
                   	
                   	<td class="label-title">证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_voucherType_id" name="sampleTumorTemp.voucherType.id"  value="<s:property value="sampleTumorTemp.voucherType.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleTumorTemp_voucherType_name" name="sampleTumorTemp.voucherType.name"  value="<s:property value="sampleTumorTemp.voucherType.name"/>" class="text input" />
  						<input type="hidden" id="tumorTemp_voucherType_id" name="tumorTemp.voucherType.id" value="<s:property value="tumorTemp.voucherType.id"/>">
  						<input type="hidden" id="tumorTemp_voucherType_name" name="tumorTemp.voucherType.name" value="<s:property value="tumorTemp.voucherType.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="voucherType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherTypeNew').value=document.getElementById('voucherType').options[document.getElementById('voucherType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.voucherType.name"/>"><s:property value="sampleTumorTemp.voucherType.name"/></option> 
							<option value="<s:property value="tumorTemp.voucherType.name"/>"><s:property value="tumorTemp.voucherType.name"/></option>
						</select> 
						<input type="text" name="tumorTempNew.voucherType.name" id="voucherTypeNew" onfocus="sampleKind()" value="<s:property value="tumorTempNew.voucherType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
            			<input type="hidden" name="tumorTempNew.voucherType.id" id="voucherTypeIdNew" value="<s:property value="tumorTempNew.voucherType.id"/>" />
                   	</td>
                   	
                   	<td class="label-title">证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_voucherCode" name="sampleTumorTemp.voucherCode"  value="<s:property value="sampleTumorTemp.voucherCode"/>"/>
  						<input type="hidden" id="tumorTemp_voucherCode" name="tumorTemp.voucherCode" value="<s:property value="tumorTemp.voucherCode"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="voucherCode"style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherCodeNew').value=document.getElementById('voucherCode').options[document.getElementById('voucherCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.voucherCode"/>"><s:property value="sampleTumorTemp.voucherCode"/></option> 
							<option value="<s:property value="tumorTemp.voucherCode"/>"><s:property value="tumorTemp.voucherCode"/></option>
						</select> 
						<input type="text" name="tumorTempNew.voucherCode" id="voucherCodeNew" value="<s:property value="tumorTempNew.voucherCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_phone" name="sampleTumorTemp.phone"  value="<s:property value="sampleTumorTemp.phone"/>"/>
  						<input type="hidden" id="tumorTemp_phone" name="tumorTemp.phone" value="<s:property value="tumorTemp.phone"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phone"style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNew').value=document.getElementById('phone').options[document.getElementById('phone').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.phone"/>"><s:property value="sampleTumorTemp.phone"/></option> 
							<option value="<s:property value="tumorTemp.phone"/>"><s:property value="tumorTemp.phone"/></option>
						</select> 
						<input type="text" name="tumorTempNew.phone" id="phoneNew" value="<s:property value="tumorTempNew.phone"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	</tr>
                
                   	<tr>
                   	<td class="label-title">电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_emailAddress" name="sampleTumorTemp.emailAddress"  value="<s:property value="sampleTumorTemp.emailAddress"/>"/>
  						<input type="hidden" id="tumorTemp_emailAddress" name="tumorTemp.emailAddress" value="<s:property value="tumorTemp.emailAddress"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="emailAddress"style="width:152px;height:28px;" onChange="javascript:document.getElementById('emailAddressNew').value=document.getElementById('emailAddress').options[document.getElementById('emailAddress').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.emailAddress"/>"><s:property value="sampleTumorTemp.emailAddress"/></option> 
							<option value="<s:property value="tumorTemp.emailAddress"/>"><s:property value="tumorTemp.emailAddress"/></option>
						</select> 
						<input type="text" name="tumorTempNew.emailAddress" id="emailAddressNew" value="<s:property value="tumorTempNew.emailAddress"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_address" name="sampleTumorTemp.address"  value="<s:property value="sampleTumorTemp.address"/>"/>
  						<input type="hidden" id="tumorTemp_address" name="tumorTemp.address" value="<s:property value="tumorTemp.address"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="address"style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.address"/>"><s:property value="sampleTumorTemp.address"/></option> 
							<option value="<s:property value="tumorTemp.address"/>"><s:property value="tumorTemp.address"/></option>
						</select> 
						<input type="text" name="tumorTempNew.address" id="addressNew" value="<s:property value="tumorTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人病史</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title">临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_diagnosis" name="sampleTumorTemp.diagnosis"  value="<s:property value="sampleTumorTemp.diagnosis"/>"/>
  						<input type="hidden" id="tumorTemp_diagnosis" name="tumorTemp.diagnosis" value="<s:property value="tumorTemp.diagnosis"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="diagnosis"style="width:152px;height:28px;" onChange="javascript:document.getElementById('diagnosisNew').value=document.getElementById('diagnosis').options[document.getElementById('diagnosis').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.diagnosis"/>"><s:property value="sampleTumorTemp.diagnosis"/></option> 
							<option value="<s:property value="tumorTemp.diagnosis"/>"><s:property value="tumorTemp.diagnosis"/></option>
						</select> 
						<input type="text" name="tumorTempNew.diagnosis" id="diagnosisNew" value="<s:property value="tumorTempNew.diagnosis"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">用药史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_pharmacy" name="sampleTumorTemp.pharmacy"  value="<s:property value="sampleTumorTemp.pharmacy"/>"/>
  						<input type="hidden" id="tumorTemp_pharmacy" name="tumorTemp.pharmacy" value="<s:property value="tumorTemp.pharmacy"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="pharmacy"style="width:152px;height:28px;" onChange="javascript:document.getElementById('pharmacyNew').value=document.getElementById('pharmacy').options[document.getElementById('pharmacy').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.pharmacy"/>"><s:property value="sampleTumorTemp.pharmacy"/></option> 
							<option value="<s:property value="tumorTemp.pharmacy"/>"><s:property value="tumorTemp.pharmacy"/></option>
						</select> 
						<input type="text" name="tumorTempNew.pharmacy" id="pharmacyNew" value="<s:property value="tumorTempNew.pharmacy"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">病例描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_caseDescribe" name="sampleTumorTemp.caseDescribe"  value="<s:property value="sampleTumorTemp.caseDescribe"/>"/>
  						<input type="hidden" id="tumorTemp_caseDescribe" name="tumorTemp.caseDescribe" value="<s:property value="tumorTemp.caseDescribe"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="caseDescribe"style="width:152px;height:28px;" onChange="javascript:document.getElementById('caseDescribeNew').value=document.getElementById('caseDescribe').options[document.getElementById('caseDescribe').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.caseDescribe"/>"><s:property value="sampleTumorTemp.caseDescribe"/></option> 
							<option value="<s:property value="tumorTemp.caseDescribe"/>"><s:property value="tumorTemp.caseDescribe"/></option>
						</select> 
						<input type="text" name="tumorTempNew.caseDescribe" id="caseDescribeNew" value="<s:property value="tumorTempNew.caseDescribe"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人医疗信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title">个人疾病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_bloodHistory" name="sampleTumorTemp.bloodHistory"  value="<s:property value="sampleTumorTemp.bloodHistory"/>"/>
  						<input type="hidden" id="tumorTemp_bloodHistory" name="tumorTemp.bloodHistory" value="<s:property value="tumorTemp.bloodHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="bloodHistory"style="width:152px;height:28px;" onChange="javascript:document.getElementById('bloodHistoryNew').value=document.getElementById('bloodHistory').options[document.getElementById('bloodHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.bloodHistory"/>"><s:property value="sampleTumorTemp.bloodHistory"/></option> 
							<option value="<s:property value="tumorTemp.bloodHistory"/>"><s:property value="tumorTemp.bloodHistory"/></option>
						</select> 
						<input type="text" name="tumorTempNew.bloodHistory" id="bloodHistoryNew" value="<s:property value="tumorTempNew.bloodHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note1" name="sampleTumorTemp.note1"  value="<s:property value="sampleTumorTemp.note1"/>"/>
  						<input type="hidden" id="tumorTemp_note1" name="tumorTemp.note1" value="<s:property value="tumorTemp.note1"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note1"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note1New').value=document.getElementById('note1').options[document.getElementById('note1').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note1"/>"><s:property value="sampleTumorTemp.note1"/></option> 
							<option value="<s:property value="tumorTemp.note1"/>"><s:property value="tumorTemp.note1"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note1" id="note1New" value="<s:property value="tumorTempNew.note1"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">个人生育史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_parturitionTime" name="sampleTumorTemp.parturitionTime"  value="<s:property value="sampleTumorTemp.note"/>"/>
  						<input type="hidden" id="tumorTemp_parturitionTime" name="tumorTemp.parturitionTime" value="<s:property value="tumorTemp.parturitionTime"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="parturitionTime"style="width:152px;height:28px;" onChange="javascript:document.getElementById('parturitionTimeNew').value=document.getElementById('parturitionTime').options[document.getElementById('parturitionTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.parturitionTime"/>"><s:property value="sampleTumorTemp.parturitionTime"/></option> 
							<option value="<s:property value="tumorTemp.parturitionTime"/>"><s:property value="tumorTemp.parturitionTime"/></option>
						</select> 
						<input type="text" name="tumorTempNew.parturitionTime" id="parturitionTimeNew" value="<s:property value="tumorTempNew.parturitionTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>	
                 	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note2" name="sampleTumorTemp.note2"  value="<s:property value="sampleTumorTemp.note2"/>"/>
  						<input type="hidden" id="tumorTemp_note2" name="tumorTemp.note2" value="<s:property value="tumorTemp.note2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note2"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note2New').value=document.getElementById('note2').options[document.getElementById('note2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note2"/>"><s:property value="sampleTumorTemp.note2"/></option> 
							<option value="<s:property value="tumorTemp.note2"/>"><s:property value="tumorTemp.note2"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note2" id="note2New" value="<s:property value="tumorTempNew.note2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                 	
                   	<td class="label-title">烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_cigarette" name="sampleTumorTemp.cigarette"  value="<s:property value="sampleTumorTemp.cigarette"/>"/>
  						<input type="hidden" id="tumorTemp_cigarette" name="tumorTemp.cigarette" value="<s:property value="tumorTemp.cigarette"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="cigarette"style="width:152px;height:28px;" onChange="javascript:document.getElementById('cigaretteNew').value=document.getElementById('cigarette').options[document.getElementById('cigarette').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.cigarette"/>"><s:property value="sampleTumorTemp.cigarette"/></option> 
							<option value="<s:property value="tumorTemp.cigarette"/>"><s:property value="tumorTemp.cigarette"/></option>
						</select> 
						<input type="text" name="tumorTempNew.cigarette" id="cigaretteNew" value="<s:property value="tumorTempNew.cigarette"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_wine" name="sampleTumorTemp.wine"  value="<s:property value="sampleTumorTemp.wine"/>"/>
  						<input type="hidden" id="tumorTemp_wine" name="tumorTemp.wine" value="<s:property value="tumorTemp.wine"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="wine"style="width:152px;height:28px;" onChange="javascript:document.getElementById('wineNew').value=document.getElementById('wine').options[document.getElementById('wine').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.wine"/>"><s:property value="sampleTumorTemp.wine"/></option> 
							<option value="<s:property value="tumorTemp.wine"/>"><s:property value="tumorTemp.wine"/></option>
						</select> 
						<input type="text" name="tumorTempNew.wine" id="wineNew" value="<s:property value="tumorTempNew.wine"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title">药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_medicine" name="sampleTumorTemp.medicine"  value="<s:property value="sampleTumorTemp.medicine"/>"/>
  						<input type="hidden" id="tumorTemp_medicine" name="tumorTemp.medicine" value="<s:property value="tumorTemp.medicine"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="medicine"style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicineNew').value=document.getElementById('medicine').options[document.getElementById('medicine').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.medicine"/>"><s:property value="sampleTumorTemp.medicine"/></option> 
							<option value="<s:property value="tumorTemp.medicine"/>"><s:property value="tumorTemp.medicine"/></option>
						</select> 
						<input type="text" name="tumorTempNew.medicine" id="medicineNew" value="<s:property value="tumorTempNew.medicine"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note3" name="sampleTumorTemp.note3"  value="<s:property value="sampleTumorTemp.note3"/>"/>
  						<input type="hidden" id="tumorTemp_note3" name="tumorTemp.note3" value="<s:property value="tumorTemp.note3"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note3"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note3New').value=document.getElementById('note3').options[document.getElementById('note3').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note3"/>"><s:property value="sampleTumorTemp.note3"/></option> 
							<option value="<s:property value="tumorTemp.note3"/>"><s:property value="tumorTemp.note3"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note3" id="note3New" value="<s:property value="tumorTempNew.note3"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  
                   	<td class="label-title">放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_radioactiveRays" name="sampleTumorTemp.radioactiveRays"  value="<s:property value="sampleTumorTemp.radioactiveRays"/>"/>
  						<input type="hidden" id="tumorTemp_radioactiveRays" name="tumorTemp.radioactiveRays" value="<s:property value="tumorTemp.radioactiveRays"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="radioactiveRays"style="width:152px;height:28px;" onChange="javascript:document.getElementById('radioactiveRaysNew').value=document.getElementById('radioactiveRays').options[document.getElementById('radioactiveRays').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.radioactiveRays"/>"><s:property value="sampleTumorTemp.radioactiveRays"/></option> 
							<option value="<s:property value="tumorTemp.radioactiveRays"/>"><s:property value="tumorTemp.radioactiveRays"/></option>
						</select> 
						<input type="text" name="tumorTempNew.radioactiveRays" id="radioactiveRaysNew" value="<s:property value="tumorTempNew.radioactiveRays"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
             </tr>
             <tr>
                  	<td class="label-title">农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_pesticide" name="sampleTumorTemp.pesticide"  value="<s:property value="sampleTumorTemp.pesticide"/>"/>
  						<input type="hidden" id="tumorTemp_pesticide" name="tumorTemp.pesticide" value="<s:property value="tumorTemp.pesticide"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="pesticide"style="width:152px;height:28px;" onChange="javascript:document.getElementById('pesticideNew').value=document.getElementById('pesticide').options[document.getElementById('pesticide').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.pesticide"/>"><s:property value="sampleTumorTemp.pesticide"/></option> 
							<option value="<s:property value="tumorTemp.pesticide"/>"><s:property value="tumorTemp.pesticide"/></option>
						</select> 
						<input type="text" name="tumorTempNew.pesticide" id="pesticideNew" value="<s:property value="tumorTempNew.pesticide"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_plumbane" name="sampleTumorTemp.plumbane"  value="<s:property value="sampleTumorTemp.plumbane"/>"/>
  						<input type="hidden" id="tumorTemp_plumbane" name="tumorTemp.plumbane" value="<s:property value="tumorTemp.plumbane"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="plumbane"style="width:152px;height:28px;" onChange="javascript:document.getElementById('plumbaneNew').value=document.getElementById('plumbane').options[document.getElementById('plumbane').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.plumbane"/>"><s:property value="sampleTumorTemp.plumbane"/></option> 
							<option value="<s:property value="tumorTemp.plumbane"/>"><s:property value="tumorTemp.plumbane"/></option>
						</select> 
						<input type="text" name="tumorTempNew.plumbane" id="plumbaneNew" value="<s:property value="tumorTempNew.plumbane"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_mercury" name="sampleTumorTemp.mercury"  value="<s:property value="sampleTumorTemp.mercury"/>"/>
  						<input type="hidden" id="tumorTemp_mercury" name="tumorTemp.mercury" value="<s:property value="tumorTemp.mercury"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="mercury"style="width:152px;height:28px;" onChange="javascript:document.getElementById('mercuryNew').value=document.getElementById('mercury').options[document.getElementById('mercury').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.mercury"/>"><s:property value="sampleTumorTemp.mercury"/></option> 
							<option value="<s:property value="tumorTemp.mercury"/>"><s:property value="tumorTemp.mercury"/></option>
						</select> 
						<input type="text" name="tumorTempNew.mercury" id="mercuryNew" value="<s:property value="tumorTempNew.mercury"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
              </tr>
              <tr>
                   	<td class="label-title">镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_cadmium" name="sampleTumorTemp.cadmium"  value="<s:property value="sampleTumorTemp.cadmium"/>"/>
  						<input type="hidden" id="tumorTemp_cadmium" name="tumorTemp.cadmium" value="<s:property value="tumorTemp.cadmium"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="cadmium"style="width:152px;height:28px;" onChange="javascript:document.getElementById('cadmiumNew').value=document.getElementById('cadmium').options[document.getElementById('cadmium').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.cadmium"/>"><s:property value="sampleTumorTemp.cadmium"/></option> 
							<option value="<s:property value="tumorTemp.cadmium"/>"><s:property value="tumorTemp.cadmium"/></option>
						</select> 
						<input type="text" name="tumorTempNew.cadmium" id="cadmiumNew" value="<s:property value="tumorTempNew.cadmium"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note4" name="sampleTumorTemp.note4"  value="<s:property value="sampleTumorTemp.note4"/>"/>
  						<input type="hidden" id="tumorTemp_note4" name="tumorTemp.note4" value="<s:property value="tumorTemp.note4"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note4"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note4New').value=document.getElementById('note4').options[document.getElementById('note4').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note4"/>"><s:property value="sampleTumorTemp.note4"/></option> 
							<option value="<s:property value="tumorTemp.note4"/>"><s:property value="tumorTemp.note4"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note4" id="note4New" value="<s:property value="tumorTempNew.note4"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   
                   	<td class="label-title" >外源输血史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_medicalHistory" name="sampleTumorTemp.medicalHistory" value="<s:property value="sampleTumorTemp.medicalHistory"/>" />
               	 		<input type="hidden" id="tumorTemp_medicalHistory" name="tumorTemp.medicalHistory" value="<s:property value="tumorTemp.medicalHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.medicalHistory" id="tumorTempNew_medicalHistory" >
							<option value="" <s:if test="tumorTempNew.medicalHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.medicalHistory==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="tumorTempNew.medicalHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
               </tr>
               <tr>
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note5" name="sampleTumorTemp.note5"  value="<s:property value="sampleTumorTemp.note5"/>"/>
  						<input type="hidden" id="tumorTemp_note5" name="tumorTemp.note5" value="<s:property value="tumorTemp.note5"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note5"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note5New').value=document.getElementById('note5').options[document.getElementById('note5').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note5"/>"><s:property value="sampleTumorTemp.note5"/></option> 
							<option value="<s:property value="tumorTemp.note5"/>"><s:property value="tumorTemp.note5"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note5" id="note5New" value="<s:property value="tumorTempNew.note5"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title" >近期是否做过检查</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_gestationIVF" name="sampleTumorTemp.gestationIVF" value="<s:property value="sampleTumorTemp.gestationIVF"/>" />
               	 		<input type="hidden" id="tumorTemp_gestationIVF" name="tumorTemp.gestationIVF" value="<s:property value="tumorTemp.gestationIVF"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.gestationIVF" id="tumorTempNew_gestationIVF" >
							<option value="" <s:if test="tumorTempNew.gestationIVF==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.gestationIVF==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.gestationIVF==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note6" name="sampleTumorTemp.note6"  value="<s:property value="sampleTumorTemp.note6"/>"/>
  						<input type="hidden" id="tumorTemp_note6" name="tumorTemp.note6" value="<s:property value="tumorTemp.note6"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note6"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note6New').value=document.getElementById('note6').options[document.getElementById('note6').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note6"/>"><s:property value="sampleTumorTemp.note6"/></option> 
							<option value="<s:property value="tumorTemp.note6"/>"><s:property value="tumorTemp.note6"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note6" id="note6New" value="<s:property value="tumorTempNew.note6"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
               	</tr>
               	<tr>
                   	<td class="label-title" >是否有过手术或治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_isSurgery" name="sampleTumorTemp.isSurgery" value="<s:property value="sampleTumorTemp.isSurgery"/>" />
               	 		<input type="hidden" id="tumorTemp_isSurgery" name="tumorTemp.isSurgery" value="<s:property value="tumorTemp.isSurgery"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.isSurgery" id="tumorTempNew_isSurgery" >
							<option value="" <s:if test="tumorTempNew.isSurgery==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.isSurgery==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.isSurgery==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">其他</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_other" name="sampleTumorTemp.other"  value="<s:property value="sampleTumorTemp.other"/>"/>
  						<input type="hidden" id="tumorTemp_other" name="tumorTemp.other" value="<s:property value="tumorTemp.other"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="other"style="width:152px;height:28px;" onChange="javascript:document.getElementById('otherNew').value=document.getElementById('other').options[document.getElementById('other').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.other"/>"><s:property value="sampleTumorTemp.other"/></option> 
							<option value="<s:property value="tumorTemp.other"/>"><s:property value="tumorTemp.other"/></option>
						</select> 
						<input type="text" name="tumorTempNew.other" id="otherNew" value="<s:property value="tumorTempNew.other"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title" >是否用过药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_isPharmacy" name="sampleTumorTemp.isPharmacy" value="<s:property value="sampleTumorTemp.isPharmacy"/>" />
               	 		<input type="hidden" id="tumorTemp_isPharmacy" name="tumorTemp.isPharmacy" value="<s:property value="tumorTemp.isPharmacy"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.isPharmacy" id="tumorTempNew_isPharmacy" >
							<option value="" <s:if test="tumorTempNew.isPharmacy==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.isPharmacy==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.isPharmacy==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
              </tr>
              <tr>
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note7" name="sampleTumorTemp.note7"  value="<s:property value="sampleTumorTemp.note7"/>"/>
  						<input type="hidden" id="tumorTemp_note7" name="tumorTemp.note7" value="<s:property value="tumorTemp.note7"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note7"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note7New').value=document.getElementById('note7').options[document.getElementById('note7').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note7"/>"><s:property value="sampleTumorTemp.note7"/></option> 
							<option value="<s:property value="tumorTemp.note7"/>"><s:property value="tumorTemp.note7"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note7" id="note7New" value="<s:property value="tumorTempNew.note7"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
              </tr>
              <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人健康状况及家族信息</label>
						</div>
					</td>
			  </tr>
              <tr>
                   	<td class="label-title" >是否存在下列疾病</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_isAilment" name="sampleTumorTemp.isAilment" value="<s:property value="sampleTumorTemp.isAilment"/>" />
               	 		<input type="hidden" id="tumorTemp_isAilment" name="tumorTemp.isAilment" value="<s:property value="tumorTemp.isAilment"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.isAilment" id="tumorTempNew_isAilment" >
							<option value="" <s:if test="tumorTempNew.isAilment==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.isAilment==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.isAilment==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note8" name="sampleTumorTemp.note8"  value="<s:property value="sampleTumorTemp.note8"/>"/>
  						<input type="hidden" id="tumorTemp_note8" name="tumorTemp.note8" value="<s:property value="tumorTemp.note8"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note8"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note8New').value=document.getElementById('note8').options[document.getElementById('note8').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note8"/>"><s:property value="sampleTumorTemp.note8"/></option> 
							<option value="<s:property value="tumorTemp.note8"/>"><s:property value="tumorTemp.note8"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note8" id="note8New" value="<s:property value="tumorTempNew.note8"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title" >直系亲属中是否患有肿瘤</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_isSickenTumour" name="sampleTumorTemp.isSickenTumour" value="<s:property value="sampleTumorTemp.isSickenTumour"/>" />
               	 		<input type="hidden" id="tumorTemp_isSickenTumour" name="tumorTemp.isSickenTumour" value="<s:property value="tumorTemp.isSickenTumour"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.isSickenTumour" id="tumorTempNew_isSickenTumour" >
							<option value="" <s:if test="tumorTempNew.isSickenTumour==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.isSickenTumour==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.isSickenTumour==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	</tr>
                   	<tr>
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note9" name="sampleTumorTemp.note9"  value="<s:property value="sampleTumorTemp.note9"/>"/>
  						<input type="hidden" id="tumorTemp_note9" name="tumorTemp.note9" value="<s:property value="tumorTemp.note9"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note9"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note9New').value=document.getElementById('note9').options[document.getElementById('note9').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note9"/>"><s:property value="sampleTumorTemp.note9"/></option> 
							<option value="<s:property value="tumorTemp.note9"/>"><s:property value="tumorTemp.note9"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note9" id="note9New" value="<s:property value="tumorTempNew.note9"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">与受检者关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_relationship" name="sampleTumorTemp.relationship"  value="<s:property value="sampleTumorTemp.relationship"/>"/>
  						<input type="hidden" id="tumorTemp_relationship" name="tumorTemp.relationship" value="<s:property value="tumorTemp.relationship"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="relationship"style="width:152px;height:28px;" onChange="javascript:document.getElementById('relationshipNew').value=document.getElementById('relationship').options[document.getElementById('relationship').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.relationship"/>"><s:property value="sampleTumorTemp.relationship"/></option> 
							<option value="<s:property value="tumorTemp.relationship"/>"><s:property value="tumorTemp.relationship"/></option>
						</select> 
						<input type="text" name="tumorTempNew.relationship" id="relationshipNew" value="<s:property value="tumorTempNew.relationship"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	</tr>
                   	 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>费用信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_isFee" name="sampleTumorTemp.isFee" value="<s:property value="sampleTumorTemp.isFee"/>" />
               	 		<input type="hidden" id="tumorTemp_isFee" name="tumorTemp.isFee" value="<s:property value="tumorTemp.isFee"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.isFee" id="tumorTempNew_isFee" >
							<option value="" <s:if test="tumorTempNew.isFee==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.isFee==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.isFee==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_privilegeType" name="sampleTumorTemp.privilegeType" value="<s:property value="sampleTumorTemp.privilegeType"/>" />
               	 		<input type="hidden" id="tumorTemp_privilegeType" name="tumorTemp.privilegeType" value="<s:property value="tumorTemp.privilegeType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.privilegeType" id="tumorTempNew_privilegeType" >
							<option value="" <s:if test="tumorTempNew.privilegeType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.privilegeType==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.privilegeType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_linkman" name="sampleTumorTemp.linkman"  value="<s:property value="sampleTumorTemp.linkman"/>"/>
  						<input type="hidden" id="tumorTemp_linkman" name="tumorTemp.linkman" value="<s:property value="tumorTemp.linkman"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="linkman"style="width:152px;height:28px;" onChange="javascript:document.getElementById('linkmanNew').value=document.getElementById('linkman').options[document.getElementById('linkman').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.linkman"/>"><s:property value="sampleTumorTemp.linkman"/></option> 
							<option value="<s:property value="tumorTemp.linkman"/>"><s:property value="tumorTemp.linkman"/></option>
						</select> 
						<input type="text" name="tumorTempNew.linkman" id="linkmanNew" value="<s:property value="tumorTempNew.linkman"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleTumorTemp_isInvoice" name="sampleTumorTemp.isInvoice" value="<s:property value="sampleTumorTemp.isInvoice"/>" />
               	 		<input type="hidden" id="tumorTemp_isInvoice" name="tumorTemp.isInvoice" value="<s:property value="tumorTemp.isInvoice"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="tumorTempNew.isInvoice" id="tumorTempNew_isInvoice" >
							<option value="" <s:if test="tumorTempNew.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="tumorTempNew.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="tumorTempNew.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="tumorTempNew_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="tumorTempNew.createUser" title="审入人"   
							value="<s:property value="tumorTempNew.createUser"/>" />
                   	</td>
                   	
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_note10" name="sampleTumorTemp.note10"  value="<s:property value="sampleTumorTemp.note10"/>"/>
  						<input type="hidden" id="tumorTemp_note10" name="tumorTemp.note10" value="<s:property value="tumorTemp.note10"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note10"style="width:152px;height:28px;" onChange="javascript:document.getElementById('note10New').value=document.getElementById('note10').options[document.getElementById('note10').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.note10"/>"><s:property value="sampleTumorTemp.note10"/></option> 
							<option value="<s:property value="tumorTemp.note10"/>"><s:property value="tumorTemp.note10"/></option>
						</select> 
						<input type="text" name="tumorTempNew.note10" id="note10New" value="<s:property value="tumorTempNew.note10"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
              	</tr>
                <tr>
               		<td class="label-title">金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_money" name="sampleTumorTemp.money"  value="<s:property value="sampleTumorTemp.money"/>"/>
  						<input type="hidden" id="tumorTemp_money" name="tumorTemp.money" value="<s:property value="tumorTemp.money"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="money"style="width:152px;height:28px;" onChange="javascript:document.getElementById('moneyNew').value=document.getElementById('money').options[document.getElementById('money').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.money"/>"><s:property value="sampleTumorTemp.money"/></option> 
							<option value="<s:property value="tumorTemp.money"/>"><s:property value="tumorTemp.money"/></option>
						</select> 
						<input type="text" name="tumorTempNew.money" id="moneyNew" value="<s:property value="tumorTempNew.money"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_sp" name="sampleTumorTemp.sp"  value="<s:property value="sampleTumorTemp.sp"/>"/>
  						<input type="hidden" id="tumorTemp_sp" name="tumorTemp.sp" value="<s:property value="tumorTemp.sp"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="sp"style="width:152px;height:28px;" onChange="javascript:document.getElementById('spNew').value=document.getElementById('sp').options[document.getElementById('sp').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.sp"/>"><s:property value="sampleTumorTemp.sp"/></option> 
							<option value="<s:property value="tumorTemp.sp"/>"><s:property value="tumorTemp.sp"/></option>
						</select> 
						<input type="text" name="tumorTempNew.sp" id="spNew" value="<s:property value="tumorTempNew.sp"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_paymentUnit" name="sampleTumorTemp.paymentUnit"  value="<s:property value="sampleTumorTemp.paymentUnit"/>"/>
  						<input type="hidden" id="tumorTemp_paymentUnit" name="tumorTemp.paymentUnit" value="<s:property value="tumorTemp.paymentUnit"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="paymentUnit"style="width:152px;height:28px;" onChange="javascript:document.getElementById('paymentUnitNew').value=document.getElementById('paymentUnit').options[document.getElementById('paymentUnit').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.paymentUnit"/>"><s:property value="sampleTumorTemp.paymentUnit"/></option> 
							<option value="<s:property value="tumorTemp.paymentUnit"/>"><s:property value="tumorTemp.paymentUnit"/></option>
						</select> 
						<input type="text" name="tumorTempNew.paymentUnit" id="paymentUnitNew" value="<s:property value="tumorTempNew.paymentUnit"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                
                 <g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="reportManUserFun" 
 					hasSetFun="true"
					documentId="sampleTumorTemp_reportMan"
					documentName="sampleTumorTemp_reportMan_name" />
                   	
                   	<td class="label-title">核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_reportMan_id" name="sampleTumorTemp.reportMan.id"  value="<s:property value="sampleTumorTemp.reportMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleTumorTemp_reportMan_name" name="sampleTumorTemp.reportMan.name"  value="<s:property value="sampleTumorTemp.reportMan.name"/>" class="text input" />
  						<input type="hidden" id="tumorTemp_reportMan_id" name="tumorTemp.reportMan.id" value="<s:property value="tumorTemp.reportMan.id"/>">
  						<input type="hidden" id="tumorTemp_reportMan_name" name="tumorTemp.reportMan.name" value="<s:property value="tumorTemp.reportMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reportMan" style="width:152px;height:28px;margin-top: 8px;" onChange="javascript:document.getElementById('reportManNew').value=document.getElementById('reportMan').options[document.getElementById('reportMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.reportMan.name"/>"><s:property value="sampleTumorTemp.reportMan.name"/></option> 
							<option value="<s:property value="tumorTemp.reportMan.name"/>"><s:property value="tumorTemp.reportMan.name"/></option>
						</select> 
						<input type="text" name="tumorTempNew.reportMan.name" id="reportManNew" value="<s:property value="tumorTempNew.reportMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="tumorTempNew.reportMan.id" id="reportManIdNew" value="<s:property value="tumorTempNew.reportMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' style="margin-left: 20px;" class='detail' /> --%>
                   	</td>
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="auditManUserFun" 
 					hasSetFun="true"
					documentId="sampleTumorTemp_auditMan"
					documentName="sampleTumorTemp_auditMan_name" />
			
                   	
                   	<td class="label-title">核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleTumorTemp_auditMan_id" name="sampleTumorTemp.auditMan.id"  value="<s:property value="sampleTumorTemp.auditMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleTumorTemp_auditMan_name" name="sampleTumorTemp.auditMan.name"  value="<s:property value="sampleTumorTemp.auditMan.name"/>" class="text input" />
  						<input type="hidden" id="tumorTemp_auditMan_id" name="tumorTemp.auditMan.id" value="<s:property value="tumorTemp.auditMan.id"/>">
  						<input type="hidden" id="tumorTemp_auditMan_name" name="tumorTemp.auditMan.name" value="<s:property value="tumorTemp.auditMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="auditMan" style="width:152px;height:28px;margin-top: 8px;" onChange="javascript:document.getElementById('auditManNew').value=document.getElementById('auditMan').options[document.getElementById('auditMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleTumorTemp.auditMan.name"/>"><s:property value="sampleTumorTemp.auditMan.name"/></option> 
							<option value="<s:property value="tumorTemp.auditMan.name"/>"><s:property value="tumorTemp.auditMan.name"/></option>
						</select> 
						<input type="text" name="tumorTempNew.auditMan.name" id="auditManNew" value="<s:property value="tumorTempNew.auditMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="tumorTempNew.auditMan.id" id="auditManIdNew" value="<s:property value="tumorTempNew.auditMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' style="margin-left: 20px;" class='detail' /> --%>
                   	</td>
                   	
                   	
                   	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="tumorTempNew.nextStepFlow" id="tumorTempNew_nextStepFlow">
							<option value="" <s:if test="tumorTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="tumorTempNew.nextStepFlow==1">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="tumorTempNew.nextStepFlow==0">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  	
                	
                </tr>
               

            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleTumor.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
