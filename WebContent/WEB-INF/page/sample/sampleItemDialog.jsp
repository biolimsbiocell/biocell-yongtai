﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%-- <%@ include file="/WEB-INF/page/include/common3.jsp"%> --%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleItemDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">样本id</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleReceiveItem_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleReceiveItem_code" searchField="true" name="code"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本情况</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleReceiveItem_condition" searchField="true" name="condition"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">存放区域</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleReceiveItem_location" searchField="true" name="location"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">处理方法</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleReceiveItem_method" searchField="true" name="method"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">处理意见</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleReceiveItem_advice" searchField="true" name="advice"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">信息录入是否完成</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleReceiveItem_isFull" searchField="true" name="isFull"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleReceiveItem_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_sampleItem_div"></div>
   		
</body>
</html>



