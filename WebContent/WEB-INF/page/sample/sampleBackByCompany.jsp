﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%-- <%@ include file="/WEB-INF/page/include/toolbar.jsp"%> --%>
<script type="text/javascript" src="${ctx}/js/sample/sampleBackByCompany.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
	            <td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
	            <td align="left"  >
					<input type="text" size="46" maxlength="30" id="sampleBackByCompany_sampleCode"
	                	 name="sampleCode" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    />
	            </td>
	        </tr>
			<tr>
	           	<td class="label-title" ><fmt:message key="biolims.common.returnDate"/></td>
	            <td align="left"  >
					<input type="text" class="Wdate" readonly="readonly" id="startreturnDate" name="startreturnDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
					<input type="hidden" id="writeDate1" name="writeDate##@@##1"  searchField="true" /> -
					<input type="text" class="Wdate" readonly="readonly" id="endreturnDate" name="endreturnDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
					<input type="hidden" id="writeDate2" name="writeDate##@@##2"  searchField="true" />
	            </td>
			</tr>
			<tr>
                <td class="label-title" ><span><fmt:message key="biolims.common.processingMethods"/></span></td>
               	<td><select id="sampleBackByCompany_method" name="method" searchField="true" style="width:100">
              		<option value=""><fmt:message key="biolims.common.pleaseSelect"/></option>
   					<option value="1"><fmt:message key="biolims.common.yes"/></option>
   					<option value="0"><fmt:message key="biolims.common.no"/></option>
					</select>
                </td>
			</tr>
            </table>          
		</form>
		</div>
		<div id="sampleBackByCompanydiv"></div>
	
</body>
</html>



