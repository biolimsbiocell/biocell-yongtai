
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleGeneTemp&id=${sampleGeneTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleGeneEdit.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleGeneTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
            <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleGeneTemp.upLoadAccessory.id" value="<s:property value="sampleGeneTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleGeneTemp.upLoadAccessory.fileName" value="<s:property value="sampleGeneTemp.upLoadAccessory.fileName"/>">
					<input type="hidden" id="saveType" value="djy" >
				</td>
			</tr>
				<tr>
                   	<td class="label-title">编号</td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
	                   	<input type="text" 	  id="sampleGeneTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleGeneTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleGeneTemp.sampleInfo.code"/>" />
						<input type="hidden"  id="sampleGeneTemp_id"  name="sampleGeneTemp.sampleInfo.id"  value="<s:property value="sampleGeneTemp.sampleInfo.id"/>" />
						<input type="hidden"  name="sampleGeneTemp.id" value="<s:property value="sampleGeneTemp.id"/>" />
						<input type="hidden"  id="upload_imga_name" name="sampleGeneTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleGeneTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleGeneTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleGeneTemp.sampleInfo.upLoadAccessory.id"/>">
					</td>
                   
                   	
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_name"
                   	 		name="sampleGeneTemp.name" title="描述"   
							value="<s:property value="sampleGeneTemp.name"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleGeneTemp_product_name" searchField="true"  
 							name="sampleGeneTemp.productName"  value="<s:property value="sampleGeneTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleGeneTemp_product_id" name="sampleGeneTemp.productId"  
 							value="<s:property value="sampleGeneTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   		
                   	
               </tr>
                <tr>     	
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_acceptDate"
                   	 		name="sampleGeneTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleGeneTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
               
                  	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_area"
                   	 		name="sampleGeneTemp.area" title="地区"  onblur="checkAdd()" 
							value="<s:property value="sampleGeneTemp.area"/>"
                   		 />
                   	</td>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_hospital"
                   	 		name="sampleGeneTemp.hospital" title="送检医院"   
							value="<s:property value="sampleGeneTemp.hospital"/>"
                   	 />
                   	</td>
                  </tr>
                  <tr>
                   	<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_sendDate"
                   	 		name="sampleGeneTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleGeneTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	</td>
               
                  	<td class="label-title" >应出报告日期</td>
               	    <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                    <td align="left"  >
                   	   <input type="text" size="20" maxlength="25" id="sampleGeneTemp_reportDate" 
                   	 		name="sampleGeneTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	 		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" 
                   	 		value="<s:date name="sampleGeneTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	   />
                    </td>
                   
                   	<td class="label-title" >检测内容</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_confirmAgeOne"
                   			name="sampleGeneTemp.confirmAgeOne" title="检测内容"   
                   	 		value="<s:property value="sampleGeneTemp.confirmAgeOne"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>		
                  </tr>
                  <tr>
                  	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleGeneTemp_sampleType_name"  value="<s:property value="sampleGeneTemp.sampleType.name"/>"/>
							<s:hidden id="sampleGeneTemp_sampleType_id" name="sampleGeneTemp.sampleType.id"></s:hidden>
							<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>
                   	<td class="label-title" >送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_doctor"
                   	 		name="sampleGeneTemp.doctor" title="送检医生"   
							value="<s:property value="sampleGeneTemp.doctor"/>"
                   	 />
                   	</td>
<!--                    	<td class="label-title">样本编号</td> -->
<%--                     <td class="requiredcolunm" nowrap width="10px" > </td> --%>
<!--                     <td align="left"> -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_sampleNum"  -->
<%--                    			name="sampleGeneTemp.sampleNum" title="样本编号" value="<s:property value="sampleGeneTemp.sampleNum"/>" --%>
<!--                    		/> -->
<!--                    </td>	 -->
               </tr>
               <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                  	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_patientName"
                   	 		name="sampleGeneTemp.patientName" title="姓名"   
							value="<s:property value="sampleGeneTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_patientNameSpell"
                   	 		name="sampleGeneTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleGeneTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleGeneTemp.gender" id="sampleGeneTemp_gender" >
							<option value="" <s:if test="sampleGeneTemp.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleGeneTemp.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="sampleGeneTemp.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleGeneTemp_age" name="sampleGeneTemp.age"  value="<s:property value="sampleGeneTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" >籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_nativePlace"
                   	 		name="sampleGeneTemp.nativePlace" title="籍贯"   
							value="<s:property value="sampleGeneTemp.nativePlace"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_nationality"
                   	 		name="sampleGeneTemp.nationality" title="民族"   
							value="<s:property value="sampleGeneTemp.nationality"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  		
                   	<td class="label-title" >婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleGeneTemp.isInsure" id="sampleGeneTemp_isInsure" >
							<option value="" <s:if test="sampleGeneTemp.isInsure==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="sampleGeneTemp.isInsure==1">selected="selected" </s:if>>已婚</option>
    						<option value="0" <s:if test="sampleGeneTemp.isInsure==0">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
              
                   	<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleGeneTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleGeneTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleGeneTemp_voucherType" name="sampleGeneTemp.voucherType.id"  value="<s:property value="sampleGeneTemp.voucherType.id"/>" > 
 							<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_voucherCode"
                   	 		name="sampleGeneTemp.voucherCode" title="证件号码"   onblur="checkFun()"
							value="<s:property value="sampleGeneTemp.voucherCode"/>"
                   	 	/>
                   	</td>
                   	
                  </tr>
                  <tr>
                  	<td class="label-title" >联系电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleGeneTemp_phone"
	                   		 name="sampleGeneTemp.phone" title="联系方式"  value="<s:property value="sampleGeneTemp.phone"/>"
	                   	 />
                   	</td>

                   	<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_emailAddress"
                  	 		name="sampleGeneTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleGeneTemp.emailAddress"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   		<td class="label-title" >通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_address"
                   	 		name="sampleGeneTemp.address" title="通讯地址"   
							value="<s:property value="sampleGeneTemp.address"/>"
                   	 	/>
                   	</td>
                  </tr>
                   <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人医疗信息</label>
						</div>
					</td>
				  </tr>
                  <tr>
                  		<td class="label-title" >个人疾病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleGeneTemp.bloodHistory" id="sampleGeneTemp_bloodHistory" >
							<option value="" <s:if test="sampleGeneTemp.bloodHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="sampleGeneTemp.bloodHistory==1">selected="selected" </s:if>>有</option>
    						<option value="0" <s:if test="sampleGeneTemp.bloodHistory==0">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_note"
                   	 		name="sampleGeneTemp.note" title="备注"   
							value="<s:property value="sampleGeneTemp.note"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >个人生育史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleGeneTemp.tumorHistory" id="sampleGeneTemp_tumorHistory" >
							<option value="" <s:if test="sampleGeneTemp.tumorHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="sampleGeneTemp.tumorHistory==1">selected="selected" </s:if>>有</option>
    						<option value="0" <s:if test="sampleGeneTemp.tumorHistory==0">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_note1"
                   	 		name="sampleGeneTemp.note1" title="备注"   
							value="<s:property value="sampleGeneTemp.note1"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                	<td class="label-title" >烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_cigarette"
                   	 	name="sampleGeneTemp.cigarette" title="烟"   
						value="<s:property value="sampleGeneTemp.cigarette"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_wine"
                   	 	name="sampleGeneTemp.wine" title="酒"   
						value="<s:property value="sampleGeneTemp.wine"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_medicine"
                   	 	name="sampleGeneTemp.medicine" title="药物"   
						value="<s:property value="sampleGeneTemp.medicine"/>"
                   	 />
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_note2"
                   	 		name="sampleGeneTemp.note2" title="备注"   
							value="<s:property value="sampleGeneTemp.note2"/>"
                   	 	/>
                   	</td>
                   		<td class="label-title" >放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_radioactiveRays"
                   	 	name="sampleGeneTemp.radioactiveRays" title="放射线"   
						value="<s:property value="sampleGeneTemp.radioactiveRays"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_pesticide"
                   	 	name="sampleGeneTemp.pesticide" title="农药"   
						value="<s:property value="sampleGeneTemp.pesticide"/>"
                   	 />
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_plumbane"
                   	 	name="sampleGeneTemp.plumbane" title="铅"   
						value="<s:property value="sampleGeneTemp.plumbane"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_mercury"
                   	 	name="sampleGeneTemp.mercury" title="汞"   
						value="<s:property value="sampleGeneTemp.mercury"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_cadmium"
                   	 	name="sampleGeneTemp.cadmium" title="镉"   
						value="<s:property value="sampleGeneTemp.cadmium"/>"
                   	 />
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_note3"
                   	 		name="sampleGeneTemp.note3" title="备注"   
							value="<s:property value="sampleGeneTemp.note3"/>"
                   	 	/>
                   	</td>
                   		<td class="label-title" >家族病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_medicalHistory"
                   	 		name="sampleGeneTemp.medicalHistory" title="姓名拼音"   
							value="<s:property value="sampleGeneTemp.medicalHistory"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >与受检者关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_relationship"
                   	 		name="sampleGeneTemp.relationship" title="与受检者关系"   
							value="<s:property value="sampleGeneTemp.relationship"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleGeneTemp.isFee" id="sampleGeneTemp_isFee" >
						<option value="" <s:if test="sampleGeneTemp.isFee==''">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="sampleGeneTemp.isFee==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="sampleGeneTemp.isFee==0">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleGeneTemp.privilegeType" id="sampleGeneTemp_privilegeType" >
						<option value="" <s:if test="sampleGeneTemp.privilegeType==''">selected="selected" </s:if>>请选择</option>
    					<option value="1" <s:if test="sampleGeneTemp.privilegeType==1">selected="selected" </s:if>>是</option>
    					<option value="0" <s:if test="sampleGeneTemp.privilegeType==0">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_linkman"
                   	 	name="sampleGeneTemp.linkman" title="推荐人"   
						value="<s:property value="sampleGeneTemp.linkman"/>"
                   	 />
                   	</td>
                </tr>
                 <tr>
                	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleGeneTemp.isInvoice" id="sampleGeneTemp_isInvoice" >
						<option value="" <s:if test="sampleGeneTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleGeneTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleGeneTemp.isInvoice==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   <td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_createUser" class="text input readonlytrue" readonly="readonly"
	                   	 	name="sampleGeneTemp.createUser" title="录入人"   
							value="<s:property value="sampleGeneTemp.createUser"/>" />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_note4"
                   	 	name="sampleGeneTemp.note4" title="备注"   
						value="<s:property value="sampleGeneTemp.note4"/>"
                   	 />
                   	</td>
              	</tr>
              	 <tr>
                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_money"
                   	 		name="sampleGeneTemp.money" title="金额"   
							value="<s:property value="sampleGeneTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_SP"
                   	 	name="sampleGeneTemp.SP" title="SP"   
						value="<s:property value="sampleGeneTemp.SP"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleGeneTemp_paymentUnit"
                   	 	name="sampleGeneTemp.paymentUnit" title="开票单位"   
						value="<s:property value="sampleGeneTemp.paymentUnit"/>"
                   	 />
                   	</td>
                </tr>
                  <tr>
                  <g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="reportManUserFun" 
 					hasSetFun="true"
					documentId="sampleGeneTemp_reportMan"
					documentName="sampleGeneTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleGeneTemp_reportMan_name"  value="<s:property value="sampleGeneTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleGeneTemp_reportMan" name="sampleGeneTemp.reportMan.id"  value="<s:property value="sampleGeneTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="auditManUserFun" 
 					hasSetFun="true"
					documentId="sampleGeneTemp_auditMan"
					documentName="sampleGeneTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleGeneTemp_auditMan_name"  value="<s:property value="sampleGeneTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleGeneTemp_auditMan" name="sampleGeneTemp.auditMan.id"  value="<s:property value="sampleGeneTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                  	
                  </tr>
               
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleGene.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
