
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleVisitTemp&id=${sampleVisitTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleVisitEditOne.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:640px;float:left;" id="sampleInputItemImg"><img id="upLoadImg" src="${ctx}/operfile/downloadById.action?id=${sampleVisitTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
               	 	<td class="label-title">编号</td>
                     	<td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_code" name="sampleVisitTemp.code" title="样本编号" value="<s:property value="sampleVisitTemp.code"/>" />
<%-- 						<input type="hidden" id="sampleVisitTemp_id" name="sampleVisitTemp.sampleInfo.id" value="<s:property value="sampleVisitTemp.sampleInfo.id"/>" /> --%>
						<input type="hidden" name="sampleVisitTemp.id" value="<s:property value="sampleVisitTemp.id"/>" />
					</td>
                   	
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleVisitTemp_name"
                   	 	name="sampleVisitTemp.name" title="描述"   
						value="<s:property value="sampleVisitTemp.name"/>"
                   	 />
                   	</td>
                   		<td class="label-title" >检测项目</td>
                   		<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleVisitTemp_product_name" searchField="true"  
 							name="sampleVisitTemp.productName"  value="<s:property value="sampleVisitTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleVisitTemp_product_id" name="sampleVisitTemp.productId"  
 							value="<s:property value="sampleVisitTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   	
                	</tr>
                   	<tr>   	
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleVisitTemp_acceptDate"
                   	 		name="sampleVisitTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleVisitTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
                    
              
                	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleVisitTemp_area"
                   	 	name="sampleVisitTemp.area" title="地区"  onblur="checkAdd()" 
						value="<s:property value="sampleVisitTemp.area"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleVisitTemp_hospital"
                   	 	name="sampleVisitTemp.hospital" title="送检医院"   
						value="<s:property value="sampleVisitTemp.hospital"/>"
                   	 />
                   	</td>
                   	  </tr>
                <tr>
                   	<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_sendDate"
                   	 		name="sampleVisitTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleVisitTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	    />
                   	</td>
               
                	<td class="label-title" >应出报告日期</td>
               	    <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                    <td align="left"  >
                   	   <input type="text" size="20" maxlength="25" id="sampleVisitTemp_reportDate" 
                   	 		name="sampleVisitTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	 		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" 
                   	 		value="<s:date name="sampleVisitTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	   />
                    </td>
                    <td class="label-title" >送检科室</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_inspectionDept"
                   	 		name="sampleVisitTemp.inspectionDept" title="送检科室"   
							value="<s:property value="sampleVisitTemp.inspectionDept"/>"
                   	 	/>
                   	</td>
                   	  </tr>
<!--                 <tr> -->
<!--                    	 <td class="label-title">样本编号</td> -->
<%--                     <td class="requiredcolunm" nowrap width="10px" > </td> --%>
<!--                     <td align="left"> -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_sampleNum"  -->
<%--                    			name="sampleVisitTemp.sampleNum" title="样本编号" value="<s:property value="sampleVisitTemp.sampleNum"/>" --%>
<!--                    		/> -->
<!--                    </td> -->
<!--                 </tr> -->
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
				<tr>
                	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_patientName"
                   	 		name="sampleVisitTemp.patientName" title="姓名"   
							value="<s:property value="sampleVisitTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_patientNameSpell"
                   	 		name="sampleVisitTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleVisitTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleVisitTemp.gender" id="sampleVisit_gender" >
							<option value="" <s:if test="sampleVisitTemp.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleVisitTemp.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="sampleVisitTemp.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleVisitTemp_age" name="sampleVisitTemp.age"  value="<s:property value="sampleVisitTemp.age"/>" />
                   	</td>
                   	<td class="label-title" >出生日期</td>
               	    <td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                    <td align="left"  >
                   	   <input type="text" size="20" maxlength="25" id="sampleVisitTemp_birthday" 
                   	 		name="sampleVisitTemp.birthday" title="出生日期" Class="Wdate" readonly="readonly" 
                   	 		onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" 
                   	 		value="<s:date name="sampleVisitTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	   />
                    </td>
                    <td class="label-title" >血型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleVisitTemp.bloodType" id="sampleVisitTemp_bloodType" >
							<option value="" <s:if test="sampleVisitTemp.bloodType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleVisitTemp.bloodType==0">selected="selected" </s:if>>A</option>
    						<option value="1" <s:if test="sampleVisitTemp.bloodType==1">selected="selected" </s:if>>B</option>
    						<option value="2" <s:if test="sampleVisitTemp.bloodType==2">selected="selected" </s:if>>AB</option>
    						<option value="3" <s:if test="sampleVisitTemp.bloodType==3">selected="selected" </s:if>>O</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleVisitTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleVisitTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleVisitTemp_voucherType" name="sampleVisitTemp.voucherType.id"  value="<s:property value="sampleVisitTemp.voucherType.id"/>" > 
 							<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_voucherCode"
                   	 		name="sampleVisitTemp.voucherCode" title="证件号码"   onblur="checkFun()"
							value="<s:property value="sampleVisitTemp.voucherCode"/>"
                   	 	/>
                   	</td>
		
					<td class="label-title" >手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_phoneNum"
                   	 		name="sampleVisitTemp.phoneNum" title="手机号码"   onblur="checkPhone()"
							value="<s:property value="sampleVisitTemp.phoneNum"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleVisitTemp_address"

                   	 	name="sampleVisitTemp.address" title="家庭住址"  

						value="<s:property value="sampleVisitTemp.address"/>"
                   	 />
                   	 <img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	 <td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleVisitTemp_sampleType_name"  value="<s:property value="sampleVisitTemp.sampleType.name"/>"/>
							<s:hidden id="sampleVisitTemp_sampleType_id" name="sampleVisitTemp.sampleType.id"></s:hidden>
							<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>
                   	<td class="label-title" >数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleVisitTemp_count" name="sampleVisitTemp.count"  
 						value="<s:property value="sampleVisitTemp.count"/>" />
                   	</td>
                   	               	
                </tr>
                <tr>
                   	<td class="label-title" >采集方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_collectionWay"
                   	 		name="sampleVisitTemp.collectionWay" title="采集方式"   
							value="<s:property value="sampleVisitTemp.collectionWay"/>"
                   	 	/>
                   	</td>
                   		<td class="label-title" >其它</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_reason2"
                   	 		name="sampleVisitTemp.reason2" title="其它"   
							value="<s:property value="sampleVisitTemp.reason2"/>"
                   	 	/>
                   	</td>
             	
                </tr>
                <tr>
                	<td class="label-title" >采集部位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_ confirmAge"
                   	 		name="sampleVisitTemp.confirmAge" title="采集部位"   
							value="<s:property value="sampleVisitTemp.confirmAge"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >病理推断</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_confirmAgeTwo"
                   	 		name="sampleVisitTemp.confirmAgeTwo" title="采集部位"   
							value="<s:property value="sampleVisitTemp.confirmAgeTwo"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_diagnosis"
                   	 		name="sampleVisitTemp.diagnosis" title="采集部位"   
							value="<s:property value="sampleVisitTemp.diagnosis"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >手术史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_bloodHistory"
                   	 		name="sampleVisitTemp.bloodHistory" title="手术史"   
							value="<s:property value="sampleVisitTemp.bloodHistory"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >用药史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_medicalHistory"
                   	 		name="sampleVisitTemp.medicalHistory" title="用药史"   
							value="<s:property value="sampleVisitTemp.medicalHistory"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >吸烟史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_tumorHistory"
                   	 		name="sampleVisitTemp.tumorHistory" title="吸烟史"   
							value="<s:property value="sampleVisitTemp.tumorHistory"/>"
                   	 	/>
                   	</td>
                
                </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>其它信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >特殊情况说明</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_serialNum"
                   	 		name="sampleVisitTemp.serialNum" title="吸烟史"   
							value="<s:property value="sampleVisitTemp.serialNum"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleVisitTemp.isInsure" id="sampleVisitTemp_isInsure" >
							<option value="" <s:if test="sampleVisitTemp.isInsure==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleVisitTemp.isInsure==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleVisitTemp.isInsure==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleVisitTemp.isInvoice" id="sampleVisitTemp_isInvoice" >
							<option value="" <s:if test="sampleVisitTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleVisitTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleVisitTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
                   	<td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_createUser" class="text input readonlytrue" readonly="readonly"
	                   	 	name="sampleVisitTemp.createUser" title="录入人"   
							value="<s:property value="sampleVisitTemp.createUser"/>" />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleVisitTemp_reason"
                   	 		name="sampleVisitTemp.reason" title="备注"   
							value="<s:property value="sampleVisitTemp.reason"/>"
                   	 	/>
                   	</td>
                </tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleVisit.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
