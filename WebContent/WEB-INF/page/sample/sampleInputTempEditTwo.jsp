
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInputTemp&id=${sampleInputTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript" src="${ctx}/js/sample/sampleInputTempEditTwo.js"></script>
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
    <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden" id="saveType" value="${requestScope.saveType}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
<!-- 				<td class="label-title" style="color: red">点击上传图片</td> -->
				<td></td>
				<td>
<!--   					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" /> -->
				</td>
			</tr>
				<tr>
<!--                	 	<td class="label-title" style="display: none">编号</td> -->
<!-- 					<td class="requiredcolunm" nowrap width="10px" style="display: none"></td> --%>
<!-- 					<td align="left" style="display: none"><input type="text" size="20" maxlength="25" -->
<!-- 						id="sampleInputTemp_id" -->
<!-- 						class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.id" title="编号" -->
<%-- 						value="<s:property value="sampleInputTemp.id"/>" /> --%>
<!-- 					</td> -->
                    <td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="inputTempNew.sampleInfo.code" title="样本编号" value="<s:property value="inputTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_code" class="text input readonlytrue" readonly="readonly" name="inputTempNew.code" title="样本编号" value="<s:property value="inputTempNew.code"/>" />
                   	</td>
                   
                   	
	                <td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_name" name="sampleInputTemp.name" title="描述" value="<s:property value="sampleInputTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="inputTemp_name" name="inputTemp.name" title="描述" value="<s:property value="inputTemp.name"/>"/>
                   		<input type="text" size="20" maxlength="25" id="inputTempNew_name" name="inputTempNew.name" title="描述" value="<s:property value="inputTempNew.name"/>" class="sampleInputTemp_name"/>
                   	</td>
	                   	
	                <td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_productId" name="sampleInputTemp.productId"  value="<s:property value="sampleInputTemp.productId"/>"/>
  						<input type="hidden" id="sampleInputTemp_productName" name="sampleInputTemp.productName" value="<s:property value="sampleInputTemp.productName"/>"/>
  						<input type="hidden" id="inputTemp_productId" name="inputTemp.productId" value="<s:property value="inputTemp.productId"/>">
  						<input type="hidden" id="inputTemp_productName" name="inputTemp.productName" value="<s:property value="inputTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.productName"/>"><s:property value="sampleInputTemp.productName"/></option> 
							<option value="<s:property value="inputTemp.productName"/>"><s:property value="inputTemp.productName"/></option>
						</select> 
						<input type="text" name="inputTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="inputTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="inputTempNew.productId" id="productIdNew" value="<s:property value="inputTempNew.productId"/>" />
                   	</td>
                   </tr>
                   <tr>
		                    <td class="label-title">送检医院</td>
	               	 		<td class="requiredcolumn" nowrap width="10px">
	               	 		<input type="hidden" id="sampleInputTemp_hospital" name="sampleInputTemp.hospital"  value="<s:property value="sampleInputTemp.hospital"/>"/>
	  						<input type="hidden" id="inputTemp_hospital" name="inputTemp.hospital" value="<s:property value="inputTemp.hospital"/>">
	               	 			
	               	 		</td>            	 	
	                   			<td align="left">
									<select id="hospital" style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
										<option value="" style="color:#c2c2c2;">---请选择---</option> 
										<option value="<s:property value="sampleInputTemp.hospital"/>"><s:property value="sampleInputTemp.hospital"/></option> 
										<option value="<s:property value="inputTemp.hospital"/>"><s:property value="inputTemp.hospital"/></option>
									</select> 
									<input type="text" name="inputTempNew.hospital" id="hospitalNew" value="<s:property value="inputTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
	                   		</td>
	                   	
	                   	
	                   	<td class="label-title" >门诊号</td>
	               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_inHosNum"   name="sampleInputTemp.inHosNum" value="<s:property value="sampleInputTemp.inHosNum"/>"> 
               	 		<input type="hidden" id="inputTemp_inHosNum" name="inputTemp.inHosNum" value="<s:property value="inputTemp.inHosNum"/>" />
               	 			
	               	 	</td>            	 	
	                   	<td align="left">
								<select id="inHosNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('inHosNumNew').value=document.getElementById('inHosNum').options[document.getElementById('inHosNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.inHosNum"/>"><s:property value="sampleInputTemp.inHosNum"/></option> 
									<option value="<s:property value="inputTemp.inHosNum"/>"><s:property value="inputTemp.inHosNum"/></option>
								</select> 
								<input type="text" name="inputTempNew.inHosNum" id="inHosNumNew" value="<s:property value="inputTempNew.inHosNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   		</td>
	                   	
	                   	
	                    <td class="label-title" >抽血时间</td>
	               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_sendDate" name="sampleInputTemp.sendDate" value="<s:property value="sampleInputTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_sendDate" name="inputTemp.sendDate"  value="<s:property value="inputTemp.sendDate"/>" />
	               	 	</td>            	 	
	                   	<td align="left">
                   		
								<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.sendDate"/>"><s:property value="sampleInputTemp.sendDate"/></option> 
									<option value="<s:property value="inputTemp.sendDate"/>"><s:property value="inputTemp.sendDate"/></option>
								</select> 
								
								<input type="text" name="inputTempNew.sendDate" id="sendDateNew" value="<s:property value="inputTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
	                   	</td>
	                    </tr>
					           
                    <tr>                	
	                    <td class="label-title">样本类型</td>
               	 		<td class="requiredcolumn" nowrap width="10px">
	               	 		<input type="hidden" id="sampleInputTemp_sampleType_id" name="sampleInputTemp.sampleType.id"  value="<s:property value="sampleInputTemp.sampleType.id"/>" class="text input" />
	               	 		<input type="hidden" id="sampleInputTemp_sampleType_name" name="sampleInputTemp.sampleType.name"  value="<s:property value="sampleInputTemp.sampleType.name"/>" class="text input" />
	  						<input type="hidden" id="inputTemp_sampleType_id" name="inputTemp.sampleType.id" value="<s:property value="inputTemp.sampleType.id"/>">
	  						<input type="hidden" id="inputTemp_sampleType_name" name="inputTemp.sampleType.name" value="<s:property value="inputTemp.sampleType.name"/>">
               	 			
	               	 	</td>            	 	
	                   	<td align="left">
							<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
								<option value="" style="color:#c2c2c2;">---请选择---</option> 
								<option value="<s:property value="sampleInputTemp.sampleType.name"/>"><s:property value="sampleInputTemp.sampleType.name"/></option> 
								<option value="<s:property value="inputTemp.sampleType.name"/>"><s:property value="inputTemp.sampleType.name"/></option>
							</select> 
							<input type="text" name="inputTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="inputTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 			<input type="hidden" name="inputTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="inputTempNew.sampleType.id"/>" />
                   		</td>
	                   	
	               	 	<td class="label-title">孕妇姓名</td>
               	 		<td class="requiredcolumn" nowrap width="10px">
	               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_patientName" name="sampleInputTemp.patientName" value="<s:property value="sampleInputTemp.patientName"/>" />
	               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_patientName" name="inputTemp.patientName"  value="<s:property value="inputTemp.patientName"/>" />
	               	 		
               	 		</td>            	 	
                   		<td align="left">
							<select id="aabb" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ccdd').value=document.getElementById('aabb').options[document.getElementById('aabb').selectedIndex].value;this.nextSibling.value=this.value;"> 
								<!--下面的option的样式是为了使字体为灰色，只是视觉问题，看起来像是注释一样--> 
								<option value="" style="color:#c2c2c2;">---请选择---</option> 
								<option value="<s:property value="sampleInputTemp.patientName"/>"><s:property value="sampleInputTemp.patientName"/></option> 
								<option value="<s:property value="inputTemp.patientName"/>"><s:property value="inputTemp.patientName"/></option>
							</select> 
							<input type="text" name="inputTempNew.patientName" id="ccdd" value="<s:property value="inputTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   		</td>
	                   	
						<td class="label-title">年龄</td>
               	 		<td class="requiredcolumn" nowrap width="10px">
	               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_age" name="sampleInputTemp.age" value="<s:property value="sampleInputTemp.age"/>" />
	               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_age" name="inputTemp.age"  value="<s:property value="inputTemp.age"/>" />
	               	 		
               	 		</td>            	 	
                   		<td align="left">
							<select id="age" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
								<option value="" style="color:#c2c2c2;">---请选择---</option> 
								<option value="<s:property value="sampleInputTemp.age"/>"><s:property value="sampleInputTemp.age"/></option> 
								<option value="<s:property value="inputTemp.age"/>"><s:property value="inputTemp.age"/></option>
							</select> 
							<input type="text" name="inputTempNew.age" id="ageNew" value="<s:property value="inputTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   		</td>
              		
                   </tr>
                   <tr>	
                  
					
                   	<td class="label-title">体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_weight" name="sampleInputTemp.weight" value="<s:property value="sampleInputTemp.weight"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_weight" name="inputTemp.weight"  value="<s:property value="inputTemp.weight"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="weight" style="width:152px;height:28px;" onChange="javascript:document.getElementById('weightNew').value=document.getElementById('weight').options[document.getElementById('weight').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.weight"/>"><s:property value="sampleInputTemp.weight"/></option> 
							<option value="<s:property value="inputTemp.weight"/>"><s:property value="inputTemp.weight"/></option>
						</select> 
						<input type="text" name="inputTempNew.weight" id="weightNew" value="<s:property value="inputTempNew.weight"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
					<td class="label-title">孕周</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_gestationalAge" name="sampleInputTemp.gestationalAge" value="<s:property value="sampleInputTemp.gestationalAge"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="inputTemp_gestationalAge" name="inputTemp.gestationalAge"  value="<s:property value="inputTemp.gestationalAge"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="gestationalAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('gestationalAgeNew').value=document.getElementById('gestationalAge').options[document.getElementById('gestationalAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.gestationalAge"/>"><s:property value="sampleInputTemp.gestationalAge"/></option> 
							<option value="<s:property value="inputTemp.gestationalAge"/>"><s:property value="inputTemp.gestationalAge"/></option>
						</select> 
						<input type="text" name="inputTempNew.gestationalAge" id="gestationalAgeNew" value="<s:property value="inputTempNew.gestationalAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_phoneNum"   name="sampleInputTemp.phoneNum" value="<s:property value="sampleInputTemp.phoneNum"/>"> 
               	 		<input type="hidden" id="inputTemp_phoneNum" name="inputTemp.phoneNum" value="<s:property value="inputTemp.phoneNum"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phoneNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNumNew').value=document.getElementById('phoneNum').options[document.getElementById('phoneNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.phoneNum"/>"><s:property value="sampleInputTemp.phoneNum"/></option> 
							<option value="<s:property value="inputTemp.phoneNum"/>"><s:property value="inputTemp.phoneNum"/></option>
						</select> 
						<input type="text" name="inputTempNew.phoneNum" id="phoneNumNew" value="<s:property value="inputTempNew.phoneNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
              	</tr>
              	<tr>
              		<td class="label-title" >家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_address"   name="sampleInputTemp.address" value="<s:property value="sampleInputTemp.address"/>"> 
               	 		<input type="hidden" id="inputTemp_address" name="inputTemp.address" value="<s:property value="inputTemp.address"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="address" style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.address"/>"><s:property value="sampleInputTemp.address"/></option> 
									<option value="<s:property value="inputTemp.address"/>"><s:property value="inputTemp.address"/></option>
								</select> 
								<input type="text" name="inputTempNew.address" id="addressNew" value="<s:property value="inputTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
              	</tr>
			<tr>
					<td class="label-title">末次月经</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" id="sampleInputTemp_endMenstruationDate" name="sampleInputTemp.endMenstruationDate" value="<s:property value="sampleInputTemp.endMenstruationDate"/>" />
               	 		<input type="hidden" id="inputTemp_endMenstruationDate" name="inputTemp.endMenstruationDate" value="<s:property value="inputTemp.endMenstruationDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
						<select id="endMenstruationDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('endMenstruationDateNew').value=document.getElementById('endMenstruationDate').options[document.getElementById('endMenstruationDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.endMenstruationDate"/>"><s:property value="sampleInputTemp.endMenstruationDate"/></option> 
							<option value="<s:property value="inputTemp.endMenstruationDate"/>"><s:property value="inputTemp.endMenstruationDate"/></option>
						</select> 
						<input type="text" name="inputTempNew.endMenstruationDate" id="endMenstruationDateNew" value="<s:property value="inputTempNew.endMenstruationDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="inputTempNew.endMenstruationDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	<td class="label-title" >孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_parturitionTime" name="sampleInputTemp.parturitionTime" value="<s:property value="sampleInputTemp.parturitionTime"/>" />
               	 		<input type="hidden" id="inputTemp_parturitionTime" name="inputTemp.parturitionTime" value="<s:property value="inputTemp.parturitionTime"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="parturitionTime" style="width:152px;height:28px;" onChange="javascript:document.getElementById('parturitionTimeNew').value=document.getElementById('parturitionTime').options[document.getElementById('parturitionTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.parturitionTime"/>"><s:property value="sampleInputTemp.parturitionTime"/></option> 
							<option value="<s:property value="inputTemp.parturitionTime"/>"><s:property value="inputTemp.parturitionTime"/></option>
						</select> 
						<input type="text" name="inputTempNew.parturitionTime" id="parturitionTimeNew" value="<s:property value="inputTempNew.parturitionTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                 </tr>
			     <tr>
			     	<td class="label-title">IVF妊娠</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_gestationIVF" name="sampleInputTemp.gestationIVF" value="<s:property value="sampleInputTemp.gestationIVF"/>" />
               	 		<input type="hidden" id="inputTemp_gestationIVF" name="inputTemp.gestationIVF" value="<s:property value="inputTemp.gestationIVF"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.gestationIVF" id="inputTempNew_gestationIVF">
							<option value=""<s:if test="inputTempNew.gestationIVF==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.gestationIVF==0">selected="selected" </s:if>>否</option>
	    					<option value="1"<s:if test="inputTempNew.gestationIVF==1">selected="selected" </s:if>>是</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_badMotherhood" name="sampleInputTemp.badMotherhood" value="<s:property value="sampleInputTemp.badMotherhood"/>" />
               	 		<input type="hidden" id="inputTemp_badMotherhood" name="inputTemp.badMotherhood" value="<s:property value="inputTemp.badMotherhood"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="badMotherhood" style="width:152px;height:28px;" onChange="javascript:document.getElementById('badMotherhoodNew').value=document.getElementById('badMotherhood').options[document.getElementById('badMotherhood').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.badMotherhood"/>"><s:property value="sampleInputTemp.badMotherhood"/></option> 
							<option value="<s:property value="inputTemp.badMotherhood"/>"><s:property value="inputTemp.badMotherhood"/></option>
						</select> 
						<input type="text" name="inputTempNew.badMotherhood" id="badMotherhoodNew" value="<s:property value="inputTempNew.badMotherhood"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
			</tr>
			<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>既往史</label>
						</div>
					</td>
			</tr>
			<tr>
					<td class="label-title">器官移植</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_organGrafting" name="sampleInputTemp.organGrafting" value="<s:property value="sampleInputTemp.organGrafting"/>" />
               	 		<input type="hidden" id="inputTemp_organGrafting" name="inputTemp.organGrafting" value="<s:property value="inputTemp.organGrafting"/>" />
               	 		</td>
                   	<td align="left">
                   		<select name="inputTempNew.organGrafting" id="inputTempNew_organGrafting">
							<option value="" <s:if test="inputTempNew.organGrafting==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="inputTempNew.organGrafting==0">selected="selected" </s:if>>无</option>
    						<option value="1" <s:if test="inputTempNew.organGrafting==1">selected="selected" </s:if>>有</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
					<td class="label-title">外源输血</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_outTransfusion" name="sampleInputTemp.outTransfusion" value="<s:property value="sampleInputTemp.outTransfusion"/>" />
               	 		<input type="hidden" id="inputTemp_outTransfusion" name="inputTemp.outTransfusion" value="<s:property value="inputTemp.outTransfusion"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.outTransfusion" id="inputTempNew_outTransfusion">
							<option value="" <s:if test="inputTempNew.outTransfusion==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.outTransfusion==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="inputTempNew.outTransfusion==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">最后一次外源输血时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" id="sampleInputTemp_firstTransfusionDate" name="sampleInputTemp.firstTransfusionDate" value="<s:property value="sampleInputTemp.firstTransfusionDate"/>" />
               	 		<input type="hidden" id="inputTemp_firstTransfusionDate" name="inputTemp.firstTransfusionDate" value="<s:property value="inputTemp.firstTransfusionDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
						<select id="firstTransfusionDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('firstTransfusionDateNew').value=document.getElementById('firstTransfusionDate').options[document.getElementById('firstTransfusionDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.firstTransfusionDate"/>"><s:property value="sampleInputTemp.firstTransfusionDate"/></option> 
							<option value="<s:property value="inputTemp.firstTransfusionDate"/>"><s:property value="inputTemp.firstTransfusionDate"/></option>
						</select> 
						<input type="text" name="inputTempNew.firstTransfusionDate" id="firstTransfusionDateNew" value="<s:property value="inputTempNew.firstTransfusionDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
				    		value="<s:date name="inputTempNew.firstTransfusionDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                </tr>
                <tr>
                   	<td class="label-title">干细胞治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_stemCellsCure" name="sampleInputTemp.stemCellsCure" value="<s:property value="sampleInputTemp.stemCellsCure"/>" />
               	 		<input type="hidden" id="inputTemp_stemCellsCure" name="inputTemp.stemCellsCure" value="<s:property value="inputTemp.stemCellsCure"/>" />
               	 		</td>
                   	<td align="left">
                   		<select name="inputTempNew.stemCellsCure" id="inputTempNew_stemCellsCure">
							<option value=""<s:if test="inputTempNew.stemCellsCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.stemCellsCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="inputTempNew.stemCellsCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">免疫治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_immuneCure" name="sampleInputTemp.immuneCure" value="<s:property value="sampleInputTemp.immuneCure"/>" />
               	 		<input type="hidden" id="inputTemp_immuneCure" name="inputTemp.immuneCure" value="<s:property value="inputTemp.immuneCure"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.immuneCure" id="inputTempNew_immuneCure">
							<option value="" <s:if test="inputTempNew.immuneCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="inputTempNew.immuneCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="inputTempNew.immuneCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">最后一次免疫治疗时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" id="sampleInputTemp_endImmuneCureDate" name="sampleInputTemp.endImmuneCureDate" value="<s:property value="sampleInputTemp.endImmuneCureDate"/>" />
               	 		<input type="hidden" id="inputTemp_endImmuneCureDate" name="inputTemp.endImmuneCureDate" value="<s:property value="inputTemp.endImmuneCureDate"/>" />
               	 	</td>            	 	
                   	<td align="left">
								<select id="endImmuneCureDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('endImmuneCureDateNew').value=document.getElementById('endImmuneCureDate').options[document.getElementById('endImmuneCureDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.endImmuneCureDate"/>"><s:property value="sampleInputTemp.endImmuneCureDate"/></option> 
									<option value="<s:property value="inputTemp.endImmuneCureDate"/>"><s:property value="inputTemp.endImmuneCureDate"/></option>
								</select> 
								<input type="text" name="inputTempNew.endImmuneCureDate" id="endImmuneCureDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"
							    	value="<s:date name="inputTempNew.endImmuneCureDate" format="yyyy-MM-dd"/>"  value="<s:property value="inputTempNew.endImmuneCureDate"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
			</tr>
			<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>辅助检查</label>
						</div>
					</td>
			</tr>

			<tr>
               	 	<td class="label-title">单/双/多胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_embryoType" name="sampleInputTemp.embryoType" value="<s:property value="sampleInputTemp.embryoType"/>" />
               	 		<input type="hidden" id="inputTemp_embryoType" name="inputTemp.embryoType" value="<s:property value="inputTemp.embryoType"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.embryoType" id="inputTempNew_embryoType">
							<option value="" <s:if test="inputTempNew.embryoType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.embryoType==0">selected="selected" </s:if>>单胎</option>
	    					<option value="1"<s:if test="inputTempNew.embryoType==1">selected="selected" </s:if>>双胎</option>
	    					<option value="2" <s:if test="inputTempNew.embryoType==2">selected="selected" </s:if>>多胎</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">NT值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_NT" name="sampleInputTemp.NT" value="<s:property value="sampleInputTemp.NT"/>" />
               	 		<input type="hidden" id="inputTemp_NT" name="inputTemp.NT" value="<s:property value="inputTemp.NT"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="NT" style="width:152px;height:28px;" onChange="javascript:document.getElementById('NTNew').value=document.getElementById('NT').options[document.getElementById('NT').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.NT"/>"><s:property value="sampleInputTemp.NT"/></option> 
							<option value="<s:property value="inputTemp.NT"/>"><s:property value="inputTemp.NT"/></option>
						</select> 
						<input type="text" name="inputTempNew.NT" id="NTNew" value="<s:property value="inputTempNew.NT"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
			</tr>
			<tr>
                   	<td class="label-title">筛查模式</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_testPattern" name="sampleInputTemp.testPattern" value="<s:property value="sampleInputTemp.testPattern"/>" />
               	 		<input type="hidden" id="inputTemp_testPattern" name="inputTemp.testPattern" value="<s:property value="inputTemp.testPattern"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.testPattern" id="inputTempNew_testPattern">
							<option value="" <s:if test="inputTempNew.testPattern==''">selected="selected" </s:if>>请选择</option>
	    					<option value="2"<s:if test="inputTempNew.testPattern==2">selected="selected" </s:if>>早中孕期联合筛查</option>
	    					<option value="0" <s:if test="inputTempNew.testPattern==0">selected="selected" </s:if>>未做</option>
	    					<option value="1"<s:if test="inputTempNew.testPattern==1">selected="selected" </s:if>>早孕期筛查</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
					<td class="label-title">21-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_trisome21Value" name="sampleInputTemp.trisome21Value" value="<s:property value="sampleInputTemp.trisome21Value"/>" />
               	 		<input type="hidden" id="inputTemp_trisome21Value" name="inputTemp.trisome21Value" value="<s:property value="inputTemp.trisome21Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="trisome21Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome21ValueNew').value=document.getElementById('trisome21Value').options[document.getElementById('trisome21Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.trisome21Value"/>"><s:property value="sampleInputTemp.trisome21Value"/></option> 
									<option value="<s:property value="inputTemp.trisome21Value"/>"><s:property value="inputTemp.trisome21Value"/></option>
								</select> 
								<input type="text" name="inputTempNew.trisome21Value" id="trisome21ValueNew" value="<s:property value="inputTempNew.trisome21Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					<td class="label-title">18-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_trisome18Value" name="sampleInputTemp.trisome18Value" value="<s:property value="sampleInputTemp.trisome18Value"/>" />
               	 		<input type="hidden" id="inputTemp_trisome18Value" name="inputTemp.trisome18Value" value="<s:property value="inputTemp.trisome18Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="trisome18Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome18ValueNew').value=document.getElementById('trisome18Value').options[document.getElementById('trisome18Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.trisome18Value"/>"><s:property value="sampleInputTemp.trisome18Value"/></option> 
									<option value="<s:property value="inputTemp.trisome18Value"/>"><s:property value="inputTemp.trisome18Value"/></option>
								</select> 
								<input type="text" name="inputTempNew.trisome18Value" id="trisome18ValueNew" value="<s:property value="inputTempNew.trisome18Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
			</tr>
			<tr>
					<td class="label-title">夫妻双方染色体</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_coupleChromosome" name="sampleInputTemp.coupleChromosome" value="<s:property value="sampleInputTemp.coupleChromosome"/>" />
               	 		<input type="hidden" id="inputTemp_coupleChromosome" name="inputTemp.coupleChromosome" value="<s:property value="inputTemp.coupleChromosome"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="inputTempNew.coupleChromosome" id="inputTempNew_coupleChromosome">
							<option value=""<s:if test="inputTempNew.coupleChromosome==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0"<s:if test="inputTempNew.coupleChromosome==0">selected="selected" </s:if>>未做</option>
	    					<option value="1"<s:if test="inputTempNew.coupleChromosome==1">selected="selected" </s:if>>正常</option>
	    					<option value="2"<s:if test="inputTempNew.coupleChromosome==2">selected="selected" </s:if>>异常</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">异常结果描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_reason2" name="sampleInputTemp.reason2" value="<s:property value="sampleInputTemp.reason2"/>" />
               	 		<input type="hidden" id="inputTemp_reason2" name="inputTemp.reason2" value="<s:property value="inputTemp.reason2"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="reason2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reason2New').value=document.getElementById('reason2').options[document.getElementById('reason2').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.reason2"/>"><s:property value="sampleInputTemp.reason2"/></option> 
									<option value="<s:property value="inputTemp.reason2"/>"><s:property value="inputTemp.reason2"/></option>
								</select> 
								<input type="text" name="inputTempNew.reason2" id="reason2New" value="<s:property value="inputTempNew.reason2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
               
                	<td class="label-title">临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_diagnosis" name="sampleInputTemp.diagnosis" value="<s:property value="sampleInputTemp.diagnosis"/>" />
               	 		<input type="hidden" id="inputTemp_diagnosis" name="inputTemp.diagnosis" value="<s:property value="inputTemp.diagnosis"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="diagnosis" style="width:152px;height:28px;" onChange="javascript:document.getElementById('diagnosisNew').value=document.getElementById('diagnosis').options[document.getElementById('diagnosis').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.diagnosis"/>"><s:property value="sampleInputTemp.diagnosis"/></option> 
							<option value="<s:property value="inputTemp.diagnosis"/>"><s:property value="inputTemp.diagnosis"/></option>
						</select> 
						<input type="text" name="inputTempNew.diagnosis" id="diagnosisNew" value="<s:property value="inputTempNew.diagnosis"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >简要病史（家族史）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_medicalHistory" name="sampleInputTemp.medicalHistory" value="<s:property value="sampleInputTemp.medicalHistory"/>" />
               	 		<input type="hidden" id="inputTemp_medicalHistory" name="inputTemp.medicalHistory" value="<s:property value="inputTemp.medicalHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="medicalHistory" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicalHistoryNew').value=document.getElementById('medicalHistory').options[document.getElementById('medicalHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.medicalHistory"/>"><s:property value="sampleInputTemp.medicalHistory"/></option> 
									<option value="<s:property value="inputTemp.medicalHistory"/>"><s:property value="inputTemp.medicalHistory"/></option>
								</select> 
								<input type="text" name="inputTempNew.medicalHistory" id="medicalHistoryNew" value="<s:property value="inputTempNew.medicalHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
	               </td>
              
               		<td class="label-title" >孕妇（签字）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_patientName" name="" title="孕妇（签字）" class="text input readonlytrue"readonly="readonly" value="<s:property value="inputTempNew.patientName"/>"/>
                   	</td>
                   	
                   	<td class="label-title" >孕妇证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_voucherType_name" name="sampleInputTemp.voucherType.name" value="<s:property value="sampleInputTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleInputTemp_voucherType_id"   name="sampleInputTemp.voucherType.id" value="<s:property value="sampleInputTemp.voucherType.id"/>"> 
               	 		<input type="hidden" id="inputTemp_voucherType_name" name="inputTemp.voucherType.name" value="<s:property value="inputTemp.voucherType.name"/>" />
 						<input type="hidden" id="inputTemp_voucherType_id"   name="inputTemp.voucherType.id" value="<s:property value="inputTemp.voucherType.id"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="voucherType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherTypeNew').value=document.getElementById('voucherType').options[document.getElementById('voucherType').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleInputTemp.voucherType.name"/>"><s:property value="sampleInputTemp.voucherType.name"/></option> 
							<option value="<s:property value="inputTemp.voucherType.name"/>"><s:property value="inputTemp.voucherType.name"/></option>
						</select> 
						<input type="text" name="inputTempNew.voucherType.name" id="voucherTypeNew" onfocus="voucherTypeFun()" value="<s:property value="inputTempNew.voucherType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="inputTempNew.voucherType.id" id="voucherTypeIdNew" value="<s:property value="inputTempNew.voucherType.id"/>">
                   	</td>
                </tr>
                <tr>
               	 	<td class="label-title">送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_doctor" name="sampleInputTemp.doctor"  value="<s:property value="sampleInputTemp.doctor"/>"/>
  						<input type="hidden" id="inputTemp_doctor" name="inputTemp.doctor" value="<s:property value="inputTemp.doctor"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="doctor" style="width:152px;height:28px;" onChange="javascript:document.getElementById('doctorNew').value=document.getElementById('doctor').options[document.getElementById('doctor').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.doctor"/>"><s:property value="sampleInputTemp.doctor"/></option> 
									<option value="<s:property value="inputTemp.doctor"/>"><s:property value="inputTemp.doctor"/></option>
								</select> 
								<input type="text" name="inputTempNew.doctor" id="doctorNew" value="<s:property value="inputTempNew.doctor"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title" >孕妇证件号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleInputTemp_voucherCode"   name="sampleInputTemp.voucherCode" value="<s:property value="sampleInputTemp.voucherCode"/>"> 
               	 		<input type="hidden" id="inputTemp_voucherCode" name="inputTemp.voucherCode" value="<s:property value="inputTemp.voucherCode"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
								<select id="voucherCode" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherCodeAgeNew').value=document.getElementById('voucherCode').options[document.getElementById('voucherCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
									<option value="" style="color:#c2c2c2;">---请选择---</option> 
									<option value="<s:property value="sampleInputTemp.voucherCode"/>"><s:property value="sampleInputTemp.voucherCode"/></option> 
									<option value="<s:property value="inputTemp.voucherCode"/>"><s:property value="inputTemp.voucherCode"/></option>
								</select> 
								<input type="text" name="inputTempNew.voucherCode" id="voucherCodeAgeNew" value="<s:property value="inputTempNew.voucherCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left"> 
                   		<select name="inputTempNew.nextStepFlow" id="sampleInputTemp_nextStepFlow">
							<option value="" <s:if test="inputTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="inputTempNew.nextStepFlow==0">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="inputTempNew.nextStepFlow==1">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
            </table>
<%--             <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleInputTemp.id"/>" /> --%>
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
