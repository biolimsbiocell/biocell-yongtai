<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/encod/orderEncodEdit.js"></script>
 <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="orderEncod_id"
                   	 name="orderEncod.id" title="<fmt:message key="biolims.common.serialNumber"/>" 
                   	   readonly="readOnly" class="text input readonlytrue"
	value="<s:property value="orderEncod.id"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="orderEncod_name"
                   	 name="orderEncod.name" title="<fmt:message key="biolims.common.describe"/>"
                   	   
	value="<s:property value="orderEncod.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.biggestCodeNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="orderEncod_maxCode"
                   	 name="orderEncod.maxCode" title="<fmt:message key="biolims.common.biggestCodeNumber"/>" readonly="readOnly" class="text input readonlytrue"
                   	   
	value="<s:property value="orderEncod.maxCode"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.number"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="orderEncod_num"
                   	 name="orderEncod.num" title="<fmt:message key="biolims.common.number"/>"
                   	   
	value="<s:property value="orderEncod.num"/>"
                   	  />
                   	  
                   	</td>
			
			
			
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="orderEncod_createUser_name"  value="<s:property value="orderEncod.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="orderEncod_createUser" name="orderEncod.createUser.id"  value="<s:property value="orderEncod.createUser.id"/>" > 
                   	</td>
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  
                   	  	<input type="text" size="20" maxlength="25" id="orderEncod_createDate"
                   	 name="orderEncod.createDate" title="<fmt:message key="biolims.common.commandTime"/>" readonly="readonly"
                   	     value="<s:date name="orderEncod.createDate" format="yyyy-MM-dd"/>" 
                   	     
                   	  />
                   	</td>
			</tr>
			<tr>
			
                   	<td style="display: none;"><input type="hidden"  id="orderEncod_state"  name="orderEncod.state"  value="<s:property value="orderEncod.state"/>"   /></td>
                   	  
			
			
               	 	<td class="label-title" ><fmt:message key="biolims.common.state"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="orderEncod_stateName"
                   	 name="orderEncod.stateName" title="<fmt:message key="biolims.common.stateDescription"/>" class="text input readonlytrue"
                   	   readonly="readOnly" 
	value="<s:property value="orderEncod.stateName"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="50" maxlength="250" id="orderEncod_note"
                   	 name="orderEncod.note" title="<fmt:message key="biolims.common.note"/>"
                   	   
	value="<s:property value="orderEncod.note"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			
			
            </table>
            <input type="hidden" name="orderEncodDetailsJson" id="orderEncodDetailsJson" value="" />
            <input type="hidden" name="orderEncodItemJson" id="orderEncodItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="orderEncod.id"/>" />
            </form>
            <div id="tabs">
            <ul>
            <li><a href="#orderEncodItempage"><fmt:message key="biolims.common.orderCodeDetail"/></a></li>
			<li><a href="#orderEncodDetailspage"><fmt:message key="biolims.common.orderCodePrintDetails"/></a></li>
			
           	</ul> 
			<div id="orderEncodDetailspage" width="100%" height:10px></div>
			<div id="orderEncodItempage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
