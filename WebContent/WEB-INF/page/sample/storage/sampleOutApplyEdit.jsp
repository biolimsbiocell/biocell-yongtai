﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
.dt-buttons {
	float: none;
}

.tablebtns {
	position: initial;
}
</style>
</head>

<body style="height: 94%">
<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>

	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
<!-- 		<input type="hidden" id="sampleOutApply_name" -->
<%-- 			value="<s:property value="sampleOutApply.name"/>">  --%>
			<input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.sampleOutApply"/> <small style="color: #fff;"><fmt:message key="biolims.sampleOutApply.id"/>:<span id="sampleOutApply_id"><s:property value="sampleOutApply.id" /></span>
								<fmt:message key="biolims.sampleOutApply.createUser"/>:<span userId="<s:property value="sampleOutApply.createUser.id"/>"
								id="sampleOutApply_createUser"><s:property value="sampleOutApply.createUser.name" /></span>
								<fmt:message key="biolims.sampleOutApply.createDate"/>:<span id="sampleOutApply_createDate"><s:date name=" sampleOutApply.createDate" format="yyyy-MM-dd "/></span> 
								 <fmt:message key="biolims.sampleOutApply.state"/>:<span state="<s:property value="sampleOutApply.state"/>"
								id="headStateName"><s:property value="sampleOutApply.stateName" /></span>
							</small>
						</h3>
					</div>
				<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.sampleOutApply.note"/>
											</span> <input type="text" size="20" maxlength="25"
												id="sampleOutApply_name" name="note"
												changelog="<s:property value="sampleOutApply.name"/>"
												class="form-control"
												value="<s:property value="sampleOutApply.name"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.sampleOutApply.selectType"/></span>

											<input type="text" size="20" readonly="readOnly" id="sampleOutApply_experimentType_name" changelog="<s:property value=" sampleOutApply.experimentType.name "/>" value="<s:property value=" sampleOutApply.experimentType.name "/>" class="form-control" />

											<input type="hidden" id="sampleOutApply_experimentType_id" name="experimentType-id" value="<s:property value=" sampleOutApply.experimentType.id "/>">

											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onClick="showType()">
													<i class="glyphicon glyphicon-search"></i>
												</span>
										</div>
									</div>
								</div>
							</form>
						</div>

	</br>






						<!--库存样本-->
						<div id="leftDiv" class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.sampleOutApply.storage"/></h3>
								</div>
							<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="sampleOutApplyAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--申请明细-->
						<div id="rightDiv" class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.sampleOutApply.applyItem"/></h3>
								</div>
							<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="sampleOutApplyItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		</section>
	</div>
	<div style="display: none" id="batch_data" class="input-group">
		<span class="input-group-addon bg-aqua">产物数量</span> <input
			type="number" id="productNum" class="form-control"
			placeholder="" value="">
	</div>
	<script type="text/javascript"
		src="${ctx}/js/sample/storage/sampleOutApplyAllLeft.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/sample/storage/sampleOutApplyItemRight.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>