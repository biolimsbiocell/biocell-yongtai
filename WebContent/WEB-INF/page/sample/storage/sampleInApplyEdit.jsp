﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInApply&id=${sampleInApply.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleInApplyEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
               	 	<td class="label-title"  style="display:none"  >样本入库id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
	                   	<input type="text" size="20" maxlength="25" id="sampleInApply_code"
	                   	 	name="sampleInApply.code" title="样本入库id" value="<s:property value="sampleInApply.code"/>"
	                   	  	style="display:none" />
                   	</td>
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="25" id="sampleInApply_id" name="sampleInApply.id" title="编号"
							readonly = "readOnly" class="text input readonlytrue"
							value="<s:property value="sampleInApply.id"/>" />
                   	</td>
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="50" id="sampleInApply_name" name="sampleInApply.name" title="描述"
	                   		value="<s:property value="sampleInApply.name"/>" />
                   	</td>
					<g:LayOutWinTag buttonId="showtaskCode" title="选择所属任务单"
						hasHtmlFrame="true"
						html="${ctx}/sample/storage/sampleInApply/mODELSelect.action"
						isHasSubmit="false" functionName="MODELFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('sampleInApply_taskCode').value=rec.get('id');
						document.getElementById('sampleInApply_taskCode_name').value=rec.get('name');" />
               	 	<td class="label-title" >所属任务单</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInApply_taskCode_name"  value="<s:property value="sampleInApply.taskCode.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleInApply_taskCode" name="sampleInApply.taskCode.id"  value="<s:property value="sampleInApply.taskCode.id"/>" > 
 						<img alt='选择所属任务单' id='showtaskCode' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
				</tr>
				<tr>
			<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="sampleInApply_createUser"
				documentName="sampleInApply_createUser_name"
			 /> --%>
			
			
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInApply_createUser_name"  value="<s:property value="sampleInApply.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleInApply_createUser" name="sampleInApply.createUser.id"  value="<s:property value="sampleInApply.createUser.id"/>" > 
                   	</td>
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInApply_createDate"
                   	 		name="sampleInApply.createDate" title="创建日期"
                   	   		readonly = "readOnly" class="text input readonlytrue"  
                   	 		value="<s:date name="sampleInApply.createDate" format="yyyy-MM-dd"/>"/>
                   	</td>
               	 	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" readonly="readOnly"  id="sampleInApply_acceptUser_name"  value="<s:property value="sampleInApply.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleInApply_acceptUser" name="sampleInApply.acceptUser.id"  value="<s:property value="sampleInApply.acceptUser.id"/>" > 
                   	</td>
				</tr>
				<tr>
               	 	<td class="label-title" >审核日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInApply_acceptDate"
                   	 name="sampleInApply.acceptDate" title="审核日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="sampleInApply.acceptDate" format="yyyy-MM-dd"/>" />
                   	</td>
					<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInApply_stateName"
                   	 	name="sampleInApply.stateName" title="工作流状态" readonly = "readOnly" class="text input readonlytrue"  
						value="<s:property value="sampleInApply.stateName"/>" />
                   	</td>
                   	<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
				</tr>
               	 	<%-- <td class="label-title" >状态id</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInApply_state"
                   	 name="sampleInApply.state" title="状态id"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleInApply.state"/>"
                   	  />
                   	</td> --%>
			
			
			
            </table>
            <input type="hidden" name="sampleInApplyItemJson" id="sampleInApplyItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleInApply.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
			<li><a href="#sampleInApplyItempage">入库申请明细</a></li>
           	</ul>  -->
			<div id="sampleInApplyItempage" width="100%" height:10px></div>
			</div>
	</body>
	</html>
