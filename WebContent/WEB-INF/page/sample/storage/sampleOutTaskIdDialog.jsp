﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleOutTaskIdDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
		<input type="hidden" id="sampleOutTaskId_type" value="<%=request.getParameter("type") %>"/>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">任务单号</td>
               	<td align="left">
                    <input type="text" maxlength="25" id="sampleOutTaskId_taskId" searchField="true" name="taskId"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		</div>
		<div id="show_dialog_sampleOutTaskId_div"></div>
   		
</body>
</html>



