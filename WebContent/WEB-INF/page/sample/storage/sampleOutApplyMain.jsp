<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="cs" uri="http://www.biolims.com/taglibs/constant"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><fmt:message key="biolims.common.mixPlanFormulation"/></title>
<head>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">
<script type="text/javascript" src="${ctx}/javascript/sample/pooling/mainframe.js"></script>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<cs:Constant var="WK_COMPLETE_2100_QPCR"/>
</head>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup">
		<input type="hidden" id="pooling_id" value="${requestScope.poolingId}">
		<input type="hidden" id="task_id" value="${requestScope.poolingId}">
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<input type="hidden" id="hid_state" value="${pageScope.WK_COMPLETE_2100_QPCR}">
		<input type="hidden" id="h_state" value="${pageScope.WK_COMPLETE_2100_QPCR}">
		<input type="hidden" name="bpmTaskId" id="bpmTaskId" value="<%=request.getParameter("bpmTaskId") %>" />
		<div  class="main-centent">
				<c:if test="${requestScope.workflowState==null||requestScope.workflowState=='3'}" >
			<div class="lefttab" id="left">
		</c:if>	
		<c:if test="${requestScope.workflowState!=null&&requestScope.workflowState!='3'}" >
			<div class="lefttab0" id="left">
		</c:if>
			</div>
			
			<c:if test="${requestScope.workflowState==null||requestScope.workflowState=='3'}" >
				<div  id="right" class="righttab">
			</c:if>
			<c:if test="${requestScope.workflowState!=null&&requestScope.workflowState!='3'}" >
				<div  id="right" class="righttab100">
			</c:if>
				<div id="pooling_task_list_div"></div>
				
				
				
				<div>
					<div id="tabs">
						<ul>
							<li><a href="#pooling_task_item_div"><fmt:message key="biolims.common.mixLibraryDetail"/></a></li>
							<li><a href="#sample-tabs-1"><fmt:message key="biolims.common.putReagent"/></a></li>
						
						</ul>
						<div id="pooling_task_item_div"></div>	
						<div id="sample-tabs-1"></div>
						
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</body>
</html>