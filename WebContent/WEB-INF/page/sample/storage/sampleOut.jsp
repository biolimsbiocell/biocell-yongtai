﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleOut.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"><fmt:message key="biolims.common.encrypt"/></td>
                   	<td align="left">
                  
					<input type="text" size="30" maxlength="25" id="sampleOut_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.outboundNumber"/>"/>
                   	</td>
<!--                	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleOut_code"
                   	 name="code" searchField="true" title="编号"    />
                   	</td> -->
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="50" id="sampleOut_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />

                   	</td>
			</tr>
			<tr>
<%--                	 	<td class="label-title" >出库申请单</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleOut_sampleOutApply_name" searchField="true"  name="sampleOutApply.name"  value="" class="text input" />
 						<input type="hidden" id="sampleOut_sampleOutApply" name="sampleOut.sampleOutApply.id"  value="" > 
 						<img alt='选择出库申请单' id='showsampleOutApply' src='${ctx}/images/img_lookup.gif' 	class='detail'     />                   		
                   	</td> --%>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="sampleOut_createUser"
				documentName="sampleOut_createUser_name"/>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleOut_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleOut_createUser" name="sampleOut.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseFromPeople"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<%-- <g:LayOutWinTag buttonId="showcreateDate" title="选择创建日期"
				hasHtmlFrame="true"
				html="${ctx}/sample/storage/sampleOut/dateSelect.action"
				isHasSubmit="false" functionName="DateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleOut_createDate').value=rec.get('id');
				document.getElementById('sampleOut_createDate_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleOut_createDate_name" searchField="true"  name="createDate.name"  value="" class="text input" />
 						<input type="hidden" id="sampleOut_createDate" name="sampleOut.createDate.id"  value="" > 
 						<img alt='选择创建日期' id='showcreateDate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
                   	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showacceptUser1" title='<fmt:message key="biolims.common.selectTheAuditor"/>'
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showacceptUser" 
				documentId="sampleOut_acceptUser"
				documentName="sampleOut_acceptUser_name"/>
				<td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleOut_acceptUser_name" searchField="true"  name="acceptUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleOut_acceptUser" name="sampleOut.acceptUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.selectTheAuditor"/>' id='showacceptUser1' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditDate"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startacceptDate" name="startacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate1" name="acceptDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endacceptDate" name="endacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate2" name="acceptDate##@@##2"  searchField="true" />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleOut_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleOut_tree_page"></div>
</body>
</html>