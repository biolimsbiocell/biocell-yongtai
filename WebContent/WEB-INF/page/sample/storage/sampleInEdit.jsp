﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img"
		title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleIn&id=${sampleIn.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
	<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
	<script type="text/javascript"
		src="${ctx}/js/sample/storage/sampleInEdit.js"></script>
	<div id="sampleInItemTempPage" style="width: 25%; float: left"></div>
	<div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass">
		<input type="hidden" id="handlemethod"
			value="${requestScope.handlemethod}"> <input type="hidden"
			id="id" value="${requestScope.id}">
		<form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
					<td class="label-title" style="display: none"><fmt:message
							key="biolims.common.sampleStorageId" /></td>
					<td class="requiredcolumn" nowrap width="10px"
						style="display: none"><img class='requiredimage'
						src='${ctx}/images/required.gif' /></td>
					<td align="left" style="display: none"><input type="text"
						size="20" maxlength="25" id="sampleIn_code" name="sampleIn.code"
						title="<fmt:message key="biolims.common.sampleStorageId"/>"
						value="<s:property value="sampleIn.code"/>" style="display: none" />
					</td>
					<td class="label-title"><fmt:message
							key="biolims.common.serialNumber" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="sampleIn_id" name="sampleIn.id"
						title="<fmt:message key="biolims.common.serialNumber"/>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="sampleIn.id"/>" /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.describe" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="50"
						id="sampleIn_name" name="sampleIn.name"
						title="<fmt:message key="biolims.common.describe"/>"
						value="<s:property value="sampleIn.name"/>" /></td>

					<td class="label-title"><fmt:message
							key="biolims.common.storageLocationHints" /></td>
					<td class="requiredcolumn" nowrap width="10px"><img
						class='requiredimage' src='${ctx}/images/required.gif' /></td>
					<td align="left"><input type="text" id="sampleIn_location"
						name="sampleIn.location"
						value="<s:property value="sampleIn.location"/>"> <img
						alt='<fmt:message key="biolims.common.selectLocation"/>'
						id='showlocation' src='${ctx}/images/img_lookup.gif'
						onClick='FrozenLocationFun();' class='detail' /></td>

				</tr>
				<tr>
					<!-- 	<g:LayOutWinTag buttonId="showlocation" title="选择储位提示"
						hasHtmlFrame="true"
						html="${ctx}/system/location/saveLocation/saveLocationSelect.action"
						isHasSubmit="false" functionName="MODELFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('sampleIn_location').value=rec.get('id');
						document.getElementById('sampleIn_location_name').value=rec.get('name');" />
               	 	<td class="label-title" >储位提示</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" id="sampleIn_location" name="sampleIn.location.id"  value="<s:property value="sampleIn.location.id"/>" > 
 						<img alt='选择储位提示' id='showlocation' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> 
                   	<td class="label-title" ></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleIn_location_name"  value="<s:property value="sampleIn.location.name"/>" class="text input readonlytrue" readonly="readOnly"  />
                   	</td>-->



				</tr>
				<tr>
					<td class="label-title"><fmt:message
							key="biolims.common.commandPerson" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="sampleIn_createUser_name"
						value="<s:property value="sampleIn.createUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="sampleIn_createUser"
						name="sampleIn.createUser.id"
						value="<s:property value="sampleIn.createUser.id"/>"></td>

					<td class="label-title"><fmt:message
							key="biolims.common.commandTime" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="sampleIn_createDate" name="sampleIn.createDate"
						title="<fmt:message key="biolims.common.commandTime"/>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="sampleIn.createDate" format="yyyy-MM-dd"/>" />
					</td>
					<td class="label-title"><fmt:message
							key="biolims.common.auditor" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20"
						readonly="readOnly" id="sampleIn_acceptUser_name"
						value="<s:property value="sampleIn.acceptUser.name"/>"
						class="text input readonlytrue" readonly="readOnly" /> <input
						type="hidden" id="sampleIn_acceptUser"
						name="sampleIn.acceptUser.id"
						value="<s:property value="sampleIn.acceptUser.id"/>"></td>

				</tr>
				<tr>
					<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="sampleIn_createUser"
				documentName="sampleIn_createUser_name"
			 /> --%>

					<td class="label-title"><fmt:message
							key="biolims.common.auditDate" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="sampleIn_acceptDate" name="sampleIn.acceptDate" title="审核日期"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:date name="sampleIn.acceptDate" format="yyyy-MM-dd"/>" />
					</td>

					<td class="label-title"><fmt:message
							key="biolims.common.workflowState" /></td>
					<td class="requiredcolumn" nowrap width="10px"></td>
					<td align="left"><input type="text" size="20" maxlength="25"
						id="sampleIn_stateName" name="sampleIn.stateName"
						title="<fmt:message key="biolims.common.workflowState"/>"
						readonly="readOnly" class="text input readonlytrue"
						value="<s:property value="sampleIn.stateName"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleIn_state"
						name="sampleIn.state"
						title="<fmt:message key="biolims.common.workflowState"/>"
						value="<s:property value="sampleIn.state"/>" /></td>
					<td class="label-title"><fmt:message
							key="biolims.common.attachment" /></td>
					<td></td>
					<td
						title="<fmt:message key="biolims.common.afterSaveCanLookAtTheAttachment"/>"
						id="doclinks_img"><span class="attach-btn"></span><span
						class="text label"><fmt:message
								key="biolims.common.aTotalOf" />&nbsp;&nbsp;${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
								key="biolims.common.attachment" /></span>
				</tr>
				<tr>
					<%-- <g:LayOutWinTag buttonId="showtype" title="选择检测方法"
						hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
						html="${ctx}/com/biolims/system/work/workType/workTypeSelect.action"
						isHasSubmit="false"		functionName="WorkTypeFun" 
						extRec="rec"
						extStr="document.getElementById('sampleIn_businessType').value=rec.get('id');
						document.getElementById('sampleIn_businessType_name').value=rec.get('name');" /> --%>
					<%-- <td class="label-title" >检测方法</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" readonly="readOnly"  id="sampleIn_businessType_name"  value="<s:property value="sampleIn.businessType.name"/>" />
 						<input type="hidden" id="sampleIn_businessType" name="sampleIn.businessType.id"  value="<s:property value="sampleIn.businessType.id"/>" > 
 						<img alt='选择检测方法' id='showtype' src='${ctx}/images/img_lookup.gif' onClick="WorkTypeFun()"	class='detail'    />
                   	</td> --%>


				</tr>



			</table>
			<input type="hidden" name="sampleInItemJson" id="sampleInItemJson"
				value="" /> <input type="hidden" id="id_parent_hidden"
				value="<s:property value="sampleIn.id"/>" />
		</form>
		<!-- <div id="tabs">
            <ul>
			<li><a href="#sampleInItempage">入库明细</a></li>
           	</ul>  -->
		<div id="sampleInItempage" width="100%" height:10px></div>
	</div>
</body>
</html>
