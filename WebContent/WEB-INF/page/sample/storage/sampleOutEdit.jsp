﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style type="text/css">
.dt-buttons {
	float: none;
}
.table th,
.table td {
	white-space: nowrap;
}

.tablebtns {
	position: initial;
}
.col-xs-4{
	margin-top: 5px;
}
</style>
</head>

<body style="height: 94%">
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div class="content-wrapper" id="content" style="margin-left: 0px;margin-top: 46px">
<!-- 		<input type="hidden" id="sampleOut_name" -->
<%-- 			value="<s:property value="sampleOut.name"/>">  --%>
			<input
			type="hidden" id="bpmTaskId" value="${requestScope.bpmTaskId}" />
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.sampleOut"/> <small style="color: #fff;"><fmt:message key="biolims.sampleOut.id"/>: <span id="sampleOut_id"><s:property value="sampleOut.id" /></span>
							<fmt:message key="biolims.sampleOut.createUser"/>: <span userId="<s:property value="sampleOut.createUser.id"/>"
								id="sampleOut_createUser"><s:property value="sampleOut.createUser.name" /></span>
								<fmt:message key="biolims.sampleOut.createDate"/>: <span id="sampleOut_createDate"><s:date name=" sampleOut.createDate" format="yyyy-MM-dd "/></span> 
								<fmt:message key="biolims.sampleOut.state"/>: <span state="<s:property value="sampleOut.state"/>"
								id="headStateName"><s:property value="sampleOut.stateName" /></span>
							</small>
						</h3>
					</div>
				<div class="box-body ipadmini">

						<div class="HideShowPanel">
							<form name="form1" class="layui-form" id="form1" method="post">
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.sampleOut.note"/>
											</span> <input type="text" size="20" maxlength="25"
												id="sampleOut_name" name="name"
												changelog="<s:property value="sampleOut.name"/>"
												class="form-control"
												value="<s:property value="sampleOut.name"/>" />

										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon"><fmt:message key="biolims.sampleOut.selectType"/></span>

											<input type="text" size="20" readonly="readOnly" id="sampleOut_outTypes_name" changelog="<s:property value=" sampleOut.outTypes.name "/>" value="<s:property value=" sampleOut.outTypes.name "/>" class="form-control" />

											<input type="hidden" id="sampleOut_outTypes_id" name="outTypes-id" value="<s:property value=" sampleOut.outTypes.id "/>">
										
											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onclick="showOutTypes()">
													<i class="glyphicon glyphicon-search"></i>
												</span>
										
										</div>
									</div>
									
									<div class="col-xs-4">
										<div class="input-group">
										
									<span class="input-group-addon" style="font-family: 黑体;">
											<fmt:message key="biolims.sampleOut.qyDate"/>
										</span> <input class="form-control" changelog="<s:property value="biolims.sampleOut.qyDate "/>" type="text" size="20"
											maxlength="25" id="sampleOut_fyDate"
											name="sampleOut.fyDate" title="<fmt:message key="biolims.sampleOut.qyDate"/>" class="Wdate"
											value="<s:date name=" sampleOut.fyDate " format="yyyy-MM-dd "/>" />
										
										<%-- 
											<span class="input-group-addon" style="font-family: 黑体;">
												<fmt:message key="biolims.sampleOut.qyDate"/>

											</span> <input class="form-control " type="text" size="20"
												maxlength="25" id="sampleOut_fyDate"
												name="fyDate" title=""
												value="<s:date name=" sampleOut.fyDate " format="yyyy-MM-dd "/>" />
 --%>
										</div>
									</div>
								</div>
								
									<div class="row">
									
							
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.sampleOut.applyUser"/></span>

											<input type="text" size="20" readonly="readOnly" id="sampleOut_applyUser_name" changelog="<s:property value=" sampleOut.applyUser.name "/>" value="<s:property value=" sampleOut.applyUser.name "/>" class="form-control" />

											<input type="hidden" id="sampleOut_applyUser_id" name="applyUser-id" value="<s:property value=" sampleOut.applyUser.id "/>">

											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onclick="showApplyUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>
										</div>
									</div>
									
									
									<div class="col-xs-4">
										<div class="input-group">

											<span class="input-group-addon"><fmt:message key="biolims.sampleOut.acceptUser"/>
											<img class='requiredimage'
											src='${ctx}/images/required.gif' /></span>

											<input type="text" size="20" readonly="readOnly" id="sampleOut_acceptUser_name" changelog="<s:property value=" sampleOut.acceptUser.name "/>" value="<s:property value=" sampleOut.acceptUser.name "/>"  class="form-control" />

											<input type="hidden" id="sampleOut_acceptUser_id" name="acceptUser-id" value="<s:property value=" sampleOut.acceptUser.id "/>">

											<span class="input-group-btn">
												<button class="btn btn-info" type="button" 
													onclick="showAcceptUser()">
													<i class="glyphicon glyphicon-search"></i>
												</span>
										</div>
									</div>
									
								</div>
								
								
								
							</form>
						</div>

	</br>






						<!--库存样本-->
						<div id="leftDiv" class="col-md-4 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.sampleOut.pendingSample"/></h3>
								</div>
							<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="sampleOutAlldiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
						<!--申请明细-->
						<div id="rightDiv" class="col-md-8 col-xs-12">
							<div class="box box-success">
								<div class="box-header with-border">
									<i class="glyphicon glyphicon-leaf"></i>
									<h3 class="box-title"><fmt:message key="biolims.sampleOut.outItem"/></h3>
								</div>
							<div class="box-body ipadmini">
									<table
										class="table table-hover table-striped table-bordered table-condensed"
										id="sampleOutItemdiv" style="font-size: 14px;"></table>
								</div>
							</div>
						</div>
					</div>

					</div>
			</div>

		</div>
		</section>
	</div>
	<div style="display: none" id="batch_data" class="input-group">
		<span class="input-group-addon bg-aqua"><fmt:message key="biolims.common.productNum"/></span> <input
			type="number" id="productNum" class="form-control"
			placeholder="" value="">
	</div>
	<script type="text/javascript"
		src="${ctx}/js/sample/storage/sampleOutAllLeft.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/sample/storage/sampleOutItemRight.js"></script>
	<!-- <script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script> -->
</body>

</html>