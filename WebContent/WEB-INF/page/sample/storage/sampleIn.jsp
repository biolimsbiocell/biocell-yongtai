﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleIn.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left">
                  
					<input type="text" size="30" maxlength="30" id="sampleIn_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"/>
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseFromPeople"/>'
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="sampleIn_createUser"
				documentName="sampleIn_createUser_name"
			 />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="30"   id="sampleIn_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleIn_createUser" name="sampleIn.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseFromPeople"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
            </tr>
			<tr>
					<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />
                   	</td>
               	 	
			</tr>
			<!-- <tr>
               	 	<td class="label-title" >审核人</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleIn_acceptUser"
                   	 name="acceptUser" searchField="true" title="审核人"    />
                   	</td>
               	 	<td class="label-title" >审核日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startacceptDate" name="startacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate1" name="acceptDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endacceptDate" name="endacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate2" name="acceptDate##@@##2"  searchField="true" />
                   	</td>
			</tr> -->
            </table>
		</form>
		</div>
		<div id="show_sampleIn_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleIn_tree_page"></div>
</body>
</html>



