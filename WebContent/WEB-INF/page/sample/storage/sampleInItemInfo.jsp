﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleInItemInfo.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>

<table>
<tr>
	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
                  	<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
					<input type="text" size="20" maxlength="25" id="code"
                   	 name="code" searchField="true" title='<fmt:message key="biolims.common.sampleCode"/>'    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.originalSampleCode"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="sampleCode"
                   	 name="sampleCode" searchField="true" title='<fmt:message key="biolims.common.name"/>'   />
                </td>
                
                	<td class="label-title" ><fmt:message key="biolims.common.storageLocation"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="location"
                   	 name="location" searchField="true" title='<fmt:message key="biolims.common.name"/>'   />
                   	 
                   	
                </td>
                <!-- 原始样本/DNA样本/文库样本 -->
                   	<td align="left"  style="display:none">
                  
					<input type="text" size="20" maxlength="50" id="dicSampleType"
                   	 name="dicSampleType" value="${requestScope.dicSampleType}"  />
                   	 
                   	
                </td>
                
                <td class="label-title" ><fmt:message key="biolims.common.note"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="location"
                   	 name="note" searchField="true" title='<fmt:message key="biolims.common.note"/>'   />
                   	</td>
                   
                   	 <td class="label-title" ><fmt:message key="biolims.common.sampleState"/></td>
                   	<td align="left"  >
                  		<select id="outState" name="outState" searchField="true">
                  			<option value="">All</option>
                  			<option value="0"><fmt:message key="biolims.common.experiment"/></option>
                  			<option value="1"><fmt:message key="biolims.common.returnToCustomer"/></option>
                  			<option value="2"><fmt:message key="biolims.common.outsourceOutStorage"/></option>
                  			<option value="3"><fmt:message key="biolims.common.sampleDestruction"/></option>
                  			<option value="4"><fmt:message key="biolims.common.inputStorage"/></option>
                  		</select>
                   	<td>
                   	 <input type="button" value="<fmt:message key="biolims.common.find"/>" onClick="cx();">
                </td>
                
                </tr>
                </table>   	
					
	<div id="sampleInItemInfodiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
</body>
</html>


