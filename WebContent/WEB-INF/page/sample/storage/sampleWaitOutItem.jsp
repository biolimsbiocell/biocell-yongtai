﻿	<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleWaitOutItem.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<input type="hidden" id="cid" value="${requestScope.cid}">
<input type="hidden" id="type" value="${requestScope.type}">
   
<table>
<tr>
			<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
                  	<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
					<input type="text" size="20" maxlength="25" id="code"
                   	 name="code" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    />
                   	 
              	</td>
              	 <td class="label-title" ><fmt:message key="biolims.common.customerName"/></td>
                   	<td align="left"  >
                   	<input type ="hidden"  id="sampleInfo_crmCustomer_id" name="sampleInfo.crmCustomer.id" >
                   	<input type="text" searchField="true" size="20" maxlength="50"  id="sampleInfo_crmDoctor_name"  name="sampleInfo.crmDoctor.name"  />
                </td>
               <c:if test="${requestScope.type!=0}">
	                <td class="label-title" ><fmt:message key="biolims.common.qcTask"/></td>
	                   	<td align="left"  >
						<input type="text" size="20" maxlength="50" id="techJkServiceTask_id"
	                   	 name="techJkServiceTask.id" searchField="true" title=""    />
	                </td>
                </c:if>
         </tr>
         <tr>
        	 	<td class="label-title" ><fmt:message key="biolims.common.outCode"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="50" id="sampleInfo_idCard"
                   	 name="sampleInfo.idCard" searchField="true" title=""    />
                </td>

                   		<td class="label-title" ><fmt:message key="biolims.common.sponsor"/></td>
                   	<td align="left"  >
                   	
                   	<input type ="hidden"  id="sampleInfo_crmCustomer_id" name="sampleInfo.crmCustomer.id" >
                   	<input type="text"  searchField="true" size="20" maxlength="50"  id="sampleInfo_crmCustomer_name"  name="sampleInfo.crmCustomer.name"  />
                   	</td>
                   	<%-- <c:if test="${requestScope.type!=0}">
	                   	<td class="label-title" >内部项目号</td>
	                   	<td align="left"  >
	                  
						<input type="text" size="20" maxlength="50" id="tjItem_inwardCode"
	                   	 name="tjItem.inwardCode" searchField="true" title="内部项目号"    />
	               	 	</td>
                   </c:if> --%>
                   	<!-- 	<td class="label-title" >销售</td>
                   	<td align="left"  >
                   	
                   	<input type ="hidden"  id="sampleInfo_sellPerson_id" name="sampleInfo.sellPerson.id"   value="<s:property value="sampleInfo.sellPerson.id"/>">
                   	<input type="text" searchField="true" size="20" maxlength="50"  id="sampleInfo_sellPerson_name"  name="sellPerson.name"   value="<s:property value="sampleInfo.sellPerson.name"/>" />
                   	</td>
                   	 -->
                <td colspan="2" align="center">
                   	 <input type="button" value="<fmt:message key="biolims.common.find"/>" onClick="searchGrid();">
                </td>


                   	</tr>
                 
</table>   	
					
	<div id="sampleWaitOutItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
</body>
</html>


