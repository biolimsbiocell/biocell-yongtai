﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleOutApply.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
<!-- 选择资源库 -->
<fmt:setBundle basename="ResouseInternational/msg" />
</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  ><fmt:message key="biolims.common.outboundNumber"/></td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleOutApply_id"
                   	 name="id" searchField="true" title="<fmt:message key="biolims.common.outboundNumber"/>"   style="display:none"    />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.serialNumber"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleOutApply_code"
                   	 name="code" searchField="true" title="<fmt:message key="biolims.common.serialNumber"/>"    />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.describe"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleOutApply_name"
                   	 name="name" searchField="true" title="<fmt:message key="biolims.common.describe"/>"    />
                   	</td>
			</tr>
			<tr>
			<%-- <g:LayOutWinTag buttonId="showtaskCode" title="选择所属任务单"
				hasHtmlFrame="true"
				html="${ctx}/com/biolims/system/work/workOrder/workOrderSelect.action"
				isHasSubmit="false" functionName="StringFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleOutApply_workOrder').value=rec.get('id');
				document.getElementById('sampleOutApply_workOrder_name').value=rec.get('name');" />
               	 	<td class="label-title" >所属任务单</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleOutApply_taskCode_name" searchField="true"  name="taskCode.name"  value="" class="text input" />
 						<input type="hidden" id="sampleOutApply_taskCode" name="sampleOutApply.taskCode.id"  value="" > 
 						<img alt='选择所属任务单' id='showtaskCode' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
                   <%-- 	<g:LayOutWinTag buttonId="showworkOrder" title='<fmt:message key="biolims.common.chooseTheTaskList"/>'
						hasHtmlFrame="true"
						html="${ctx}/com/biolims/system/work/workOrder/workOrderSelect.action"
						isHasSubmit="false" functionName="WorkOrderFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('sampleOutApply_workOrder').value=rec.get('id');
						document.getElementById('sampleOutApply_workOrder_name').value=rec.get('name');" />
               	 	<td class="label-title" ><fmt:message key="biolims.common.subordinateToTheTask"/></td>
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleOutApply_workOrder_name"  value="<s:property value="sampleOutApply.workOrder.name"/>" />
 						<input type="hidden" id="sampleOutApply_workOrder" name="sampleOutApply.workOrder.id"  value="<s:property value="sampleOutApply.workOrder.id"/>" > 
 						<img alt='<fmt:message key="biolims.common.chooseTheTaskList"/>' id='showworkOrder' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td> --%>
			<g:LayOutWinTag buttonId="showcreateUser" title='<fmt:message key="biolims.common.chooseTheTaskList"/>'
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="sampleOutApply_createUser"
				documentName="sampleOutApply_createUser_name"
			 />
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandPerson"/></td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleOutApply_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleOutApply_createUser" name="sampleOutApply.createUser.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseFromPeople"/>' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<%-- <g:LayOutWinTag buttonId="showcreateDate" title='<fmt:message key="biolims.common.chooseToCreateDate"/>'
				hasHtmlFrame="true"
				html="${ctx}/sample/storage/sampleOutApply/dateSelect.action"
				isHasSubmit="false" functionName="DateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleOutApply_createDate').value=rec.get('id');
				document.getElementById('sampleOutApply_createDate_name').value=rec.get('name');" /> --%>
               	 	<td class="label-title" ><fmt:message key="biolims.common.commandTime"/></td>
                   	<td align="left"  >
 						<%-- <input type="text" size="20"   id="sampleOutApply_createDate_name" searchField="true"  name="createDate.name"  value="" class="text input" />
 						<input type="hidden" id="sampleOutApply_createDate" name="sampleOutApply.createDate.id"  value="" > 
 						<img alt='<fmt:message key="biolims.common.chooseToCreateDate"/>' id='showcreateDate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />  --%>     
 						<input type="text" class="Wdate" readonly="readonly" id="startcreateDate" name="startcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate1" name="createDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endcreateDate" name="endcreateDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="createDate2" name="createDate##@@##2"  searchField="true" />             		
                   	</td>
			</tr>
			<tr>
               	 	<%-- <td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleOutApply_acceptUser"
                   	 name="acceptUser" searchField="true" title="<fmt:message key="biolims.common.auditor"/>"    />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.auditDate"/></td>
                   	<td align="left"  >
 						<input type="text" class="Wdate" readonly="readonly" id="startacceptDate" name="startacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate1" name="acceptDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endacceptDate" name="endacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate2" name="acceptDate##@@##2"  searchField="true" />
                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.stateID"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleOutApply_state"
                   	 name="state" searchField="true" title="状态id"    />
                   	</td> --%>
			</tr>
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.state"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleOutApply_stateName"
                   	 name="stateName" searchField="true" title="<fmt:message key="biolims.common.workflowState"/>"    />
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleOutApply_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleOutApply_tree_page"></div>
</body>
</html>



