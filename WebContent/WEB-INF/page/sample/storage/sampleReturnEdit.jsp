﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleReturn&id=${sampleReturn.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleReturnEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
               	 	<td class="label-title"  style="display:none"  >样本返库id</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
	                   	<input type="text" size="20" maxlength="25" id="sampleReturn_code" name="sampleReturn.code" title="样本返库id"
							value="<s:property value="sampleReturn.code"/>" style="display:none" />
                   	</td>
               	 	<td class="label-title" >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="25" id="sampleReturn_id" name="sampleReturn.id" title="编号"
							readonly = "readOnly" class="text input readonlytrue"
							value="<s:property value="sampleReturn.id"/>" />
                   	</td>
               	 	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="50" id="sampleReturn_name"
	                   	 	name="sampleReturn.name" title="描述" value="<s:property value="sampleReturn.name"/>" />
                   	</td>
               	 	<td class="label-title" >创建人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleReturn_createUser_name"  value="<s:property value="sampleReturn.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleReturn_createUser" name="sampleReturn.createUser.id"  value="<s:property value="sampleReturn.createUser.id"/>" > 
                   	</td>
				</tr>
				<tr>
					<g:LayOutWinTag buttonId="showlocation" title="选择储位提示"
						hasHtmlFrame="true" width="document.body.clientWidth/1.5" 
						html="${ctx}/system/location/saveLocation/saveLocationSelect.action"
						isHasSubmit="false" functionName="MODELFun" 
		 				hasSetFun="true"
						extRec="rec"
						extStr="document.getElementById('sampleReturn_location').value=rec.get('id');
						document.getElementById('sampleReturn_location_name').value=rec.get('name');" />
               	 	<td class="label-title" >储位提示</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" id="sampleReturn_location" name="sampleReturn.location.id"  value="<s:property value="sampleReturn.location.id"/>" > 
 						<img alt='选择储位提示' id='showlocation' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	<td class="label-title" ></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" readonly="readOnly"  id="sampleReturn_location_name"  value="<s:property value="sampleReturn.location.name"/>" class="text input readonlytrue" readonly="readOnly"  />
                   	</td>
					<%-- <g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
						hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false"		functionName="showcreateUserFun" 
						documentId="sampleReturn_createUser"
						documentName="sampleReturn_createUser_name" /> --%>
               	 	<td class="label-title" >创建日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleReturn_createDate"
	                   	 	name="sampleReturn.createDate" title="审核日期" readonly = "readOnly" class="text input readonlytrue"  
	                   	  	value="<s:date name="sampleReturn.createDate" format="yyyy-MM-dd"/>" />
                   	</td>
				</tr>
				<tr>
               	 	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  	<input type="text" size="20" readonly="readOnly"  id="sampleReturn_acceptUser_name"  value="<s:property value="sampleReturn.acceptUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleReturn_acceptUser" name="sampleReturn.acceptUser.id"  value="<s:property value="sampleReturn.acceptUser.id"/>" > 
                   	</td>
               	 	<td class="label-title" >审核日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="25" id="sampleReturn_acceptDate"
	                   	 	name="sampleReturn.acceptDate" title="审核日期" readonly = "readOnly" class="text input readonlytrue"  
	                   	  	value="<s:date name="sampleReturn.acceptDate" format="yyyy-MM-dd"/>" />
                   	</td>
					<td class="label-title" >工作流状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleReturn_stateName"
                   	 	name="sampleReturn.stateName" title="工作流状态" readonly = "readOnly" class="text input readonlytrue"  
						value="<s:property value="sampleReturn.stateName"/>" />
                   	</td>
			
               	 	<%-- <td class="label-title" >状态id</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleReturn_state"
                   		name="sampleReturn.state" title="状态id" readonly = "readOnly" class="text input readonlytrue"  
						value="<s:property value="sampleReturn.state"/>" />
                   	</td> --%>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="sampleReturnItemJson" id="sampleReturnItemJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleReturn.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
			<li><a href="#sampleReturnItempage">返库明细</a></li>
           	</ul>  -->
			<div id="sampleReturnItempage" width="100%" height:10px></div>
			</div>
	</body>
	</html>
