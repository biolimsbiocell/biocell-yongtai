﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style>

</style>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleOutTemp.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>
<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table" >
			<tr>
               	 	<td class="label-title" >样本编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="18" id="sampleOutTemp_code"
                   	 name="code" searchField="true" title="样本编号"    />

                   	</td>
               	 	<td class="label-title" ><fmt:message key="biolims.common.originalSampleCode"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="sampleOutTemp_sampleCode"
                   	 name="sampleCode" searchField="true" title="<fmt:message key="biolims.common.originalSampleCode"/>"    />

                   	</td>
			</tr>
			<tr >
               	 	<td class="label-title" >出库类型</td>
                   	<td align="left"  >
					<select id="sampleOutTemp_type" name="type" searchField="true" >
								<option value="" >请选择</option>
								<option value="0" ><fmt:message key="biolims.common.returnToExperiment"/></option>
								<option value="1" ><fmt:message key="biolims.common.returnToCustomer"/></option>
								<option value="2" >外包出库</option>
								<option value="3" >样本销毁</option>
					</select>
                   	</td>
                   	<td class="label-title" >实验任务单号</td>
                   	<td align="left"  >
                  
					<input type="text" size="30" maxlength="60" id="sampleOutTemp_taskId"
                   	 name="taskId" searchField="true" title="实验任务单号"    />

                   	</td>
			</tr>
            </table>          
		</form>
		</div> 
	<div id="show_sampleOutTemp_div"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="many_bat_div" style="display: none">
		<div class="ui-widget;">
			<div class="ui-state-highlight ui-corner-all jquery-ui-warning">
			</div>
		</div>
		<textarea id="many_bat_text" style="width:650px;height: 339px"></textarea>
	</div>
</body>
</html>


