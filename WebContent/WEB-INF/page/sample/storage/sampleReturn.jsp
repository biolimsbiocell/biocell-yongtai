﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleReturn.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >样本返库id</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleReturn_id"
                   	 name="id" searchField="true" title="样本返库id"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleReturn_code"
                   	 name="code" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleReturn_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showlocation" title="选择储位提示"
				hasHtmlFrame="true"
				html="${ctx}/sample/storage/sampleReturn/mODELSelect.action"
				isHasSubmit="false" functionName="MODELFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleReturn_location').value=rec.get('id');
				document.getElementById('sampleReturn_location_name').value=rec.get('name');" />
               	 	<td class="label-title" >储位提示</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleReturn_location_name" searchField="true"  name="location.name"  value="" class="text input" />
 						<input type="hidden" id="sampleReturn_location" name="sampleReturn.location.id"  value="" > 
 						<img alt='选择储位提示' id='showlocation' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="sampleReturn_createUser"
				documentName="sampleReturn_createUser_name"
			 />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleReturn_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleReturn_createUser" name="sampleReturn.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcreateDate" title="选择创建日期"
				hasHtmlFrame="true"
				html="${ctx}/sample/storage/sampleReturn/dateSelect.action"
				isHasSubmit="false" functionName="DateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleReturn_createDate').value=rec.get('id');
				document.getElementById('sampleReturn_createDate_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleReturn_createDate_name" searchField="true"  name="createDate.name"  value="" class="text input" />
 						<input type="hidden" id="sampleReturn_createDate" name="sampleReturn.createDate.id"  value="" > 
 						<img alt='选择创建日期' id='showcreateDate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >审核人</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleReturn_acceptUser"
                   	 name="acceptUser" searchField="true" title="审核人"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >审核日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startacceptDate" name="startacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate1" name="acceptDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endacceptDate" name="endacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate2" name="acceptDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >状态id</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleReturn_state"
                   	 name="state" searchField="true" title="状态id"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleReturn_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleReturn_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleReturn_tree_page"></div>
</body>
</html>



