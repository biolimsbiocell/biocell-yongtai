﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
.x-grid3-cell-inner, .x-grid3-hd-inner{
	overflow:hidden;
	-o-text-overflow: ellipsis;
	text-overflow: ellipsis;
    padding:3px 3px 3px 5px;
    white-space: nowrap;
}

</style>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleOutItem.js"></script>
<script type="text/javascript" src="${ctx}/javascript/lib/jquery.simple_csv.js"></script>
</head>
<body>

<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		
	<div id="sampleOutItemdiv"></div>
	<div id="bat_uploadcsv_div" style="display: none">
		<input type="file" name="file" id="file-uploadcsv"><fmt:message key="biolims.common.upLoadFile"/>
	</div>
	<div id="bat_isDepot_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.wetherToReturnLibrary"/></span></td>
                <td><select id="isDepot"  style="width:100">
    					<option value="2" <s:if test="isDepot==2">selected="selected" </s:if>><fmt:message key="biolims.common.returnLibrary"/></option>
    					<option value="0" <s:if test="isDepot==0">selected="selected" </s:if>><fmt:message key="biolims.common.notToReturnLibrary"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
	<div id="bat_expreceCode_div" style="display: none">
		<table>		
			<tr>
				<td class="label-title" ><span>快递单号</span></td>
                <td><input type="text" id="expreceCode"/></td>
			</tr>
		</table>
	</div>
</body>
</html>


