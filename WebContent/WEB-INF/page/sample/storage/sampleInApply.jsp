﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/storage/sampleInApply.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >样本入库id</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleInApply_id"
                   	 name="id" searchField="true" title="样本入库id"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >编号</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleInApply_code"
                   	 name="code" searchField="true" title="编号"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >描述</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleInApply_name"
                   	 name="name" searchField="true" title="描述"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
			<g:LayOutWinTag buttonId="showtaskCode" title="选择所属任务单"
				hasHtmlFrame="true"
				html="${ctx}/sample/storage/sampleInApply/mODELSelect.action"
				isHasSubmit="false" functionName="MODELFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleInApply_taskCode').value=rec.get('id');
				document.getElementById('sampleInApply_taskCode_name').value=rec.get('name');" />
               	 	<td class="label-title" >所属任务单</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleInApply_taskCode_name" searchField="true"  name="taskCode.name"  value="" class="text input" />
 						<input type="hidden" id="sampleInApply_taskCode" name="sampleInApply.taskCode.id"  value="" > 
 						<img alt='选择所属任务单' id='showtaskCode' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcreateUser" title="选择创建人"
				hasHtmlFrame="true" hasSetFun="true" width="document.body.clientWidth/1.5" 
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false"		functionName="showcreateUserFun" 
				documentId="sampleInApply_createUser"
				documentName="sampleInApply_createUser_name"
			 />
               	 	<td class="label-title" >创建人</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleInApply_createUser_name" searchField="true"  name="createUser.name"  value="" class="text input" />
 						<input type="hidden" id="sampleInApply_createUser" name="sampleInApply.createUser.id"  value="" > 
 						<img alt='选择创建人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			<g:LayOutWinTag buttonId="showcreateDate" title="选择创建日期"
				hasHtmlFrame="true"
				html="${ctx}/sample/storage/sampleInApply/dateSelect.action"
				isHasSubmit="false" functionName="DateFun" 
 				hasSetFun="true"
				extRec="rec"
				extStr="document.getElementById('sampleInApply_createDate').value=rec.get('id');
				document.getElementById('sampleInApply_createDate_name').value=rec.get('name');" />
               	 	<td class="label-title" >创建日期</td>
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleInApply_createDate_name" searchField="true"  name="createDate.name"  value="" class="text input" />
 						<input type="hidden" id="sampleInApply_createDate" name="sampleInApply.createDate.id"  value="" > 
 						<img alt='选择创建日期' id='showcreateDate' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >审核人</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleInApply_acceptUser"
                   	 name="acceptUser" searchField="true" title="审核人"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >审核日期</td>
                   	<td align="left"  >
                  
                   	
 						<input type="text" class="Wdate" readonly="readonly" id="startacceptDate" name="startacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate1" name="acceptDate##@@##1"  searchField="true" /> -
 						<input type="text" class="Wdate" readonly="readonly" id="endacceptDate" name="endacceptDate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})"  />
						<input type="hidden" id="acceptDate2" name="acceptDate##@@##2"  searchField="true" />
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >状态id</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleInApply_state"
                   	 name="state" searchField="true" title="状态id"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >工作流状态</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleInApply_stateName"
                   	 name="stateName" searchField="true" title="工作流状态"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleInApply_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleInApply_tree_page"></div>
</body>
</html>



