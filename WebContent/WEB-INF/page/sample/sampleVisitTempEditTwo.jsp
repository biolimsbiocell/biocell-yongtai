
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleVisitTemp&id=${sampleVisitTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleVisitTempEditTwo.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleVisitTemp.upLoadAccessory.id}"></div> 
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
               	 	<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_id" name="sampleVisitTemp.id" title="编号" value="<s:property value="sampleVisitTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleVisitTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleVisitTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   		<input type ="hidden" id="saveType" value="jytb" >
                   	</td>
					
					<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_code" class="text input readonlytrue" readonly="readonly" name="visitTempNew.sampleInfo.code" title="样本编号" value="<s:property value="visitTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_code" class="text input readonlytrue" readonly="readonly" name="visitTempNew.code" title="样本编号" value="<s:property value="visitTempNew.code"/>" />
                   	</td>
					
                   	<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="visitTempNew_name" name="visitTempNew.name" title="描述" value="<s:property value="visitTempNew.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_name" name="sampleVisitTemp.name" title="描述" value="<s:property value="sampleVisitTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="visitTemp_name" name="visitTemp.name" title="描述" value="<s:property value="visitTemp.name"/>"/>
                   	</td>
                   	
                   	<td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_productId" name="sampleVisitTemp.productId"  value="<s:property value="sampleVisitTemp.productId"/>"/>
  						<input type="hidden" id="sampleVisitTemp_productName" name="sampleVisitTemp.productName" value="<s:property value="sampleVisitTemp.productName"/>"/>
  						<input type="hidden" id="visitTemp_productId" name="visitTemp.productId" value="<s:property value="visitTemp.productId"/>">
  						<input type="hidden" id="visitTemp_productName" name="visitTemp.productName" value="<s:property value="visitTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.productName"/>"><s:property value="sampleVisitTemp.productName"/></option> 
							<option value="<s:property value="visitTemp.productName"/>"><s:property value="visitTemp.productName"/></option>
						</select> 
						<input type="text" name="visitTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="visitTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="visitTempNew.productId" id="productIdNew" value="<s:property value="visitTempNew.productId"/>" />
                   	</td>
                   		
                </tr>
                <tr>		
                   	
                   	<td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_acceptDate" name="sampleVisitTemp.acceptDate" value="<s:property value="sampleVisitTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_acceptDate" name="visitTemp.acceptDate" value="<s:property value="visitTemp.acceptDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.acceptDate"/>"><s:property value="sampleVisitTemp.acceptDate"/></option> 
							<option value="<s:property value="visitTemp.acceptDate"/>"><s:property value="visitTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="visitTempNew.acceptDate" id="acceptDateNew" value="<s:property value="visitTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="visitTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_area" name="sampleVisitTemp.area"  value="<s:property value="sampleVisitTemp.area"/>"/>
  						<input type="hidden" id="visitTemp_area" name="visitTemp.area" value="<s:property value="visitTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.area"/>"><s:property value="sampleVisitTemp.area"/></option> 
							<option value="<s:property value="visitTemp.area"/>"><s:property value="visitTemp.area"/></option>
						</select> 
						<input type="text" name="visitTempNew.area" id="areaNew" value="<s:property value="visitTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_hospital" name="sampleVisitTemp.hospital"  value="<s:property value="sampleVisitTemp.hospital"/>"/>
  						<input type="hidden" id="visitTemp_hospital" name="visitTemp.hospital" value="<s:property value="visitTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital"style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.hospital"/>"><s:property value="sampleVisitTemp.hospital"/></option> 
							<option value="<s:property value="visitTemp.hospital"/>"><s:property value="visitTemp.hospital"/></option>
						</select> 
						<input type="text" name="visitTempNew.hospital" id="hospitalNew" value="<s:property value="visitTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	  </tr>
                 <tr>
                   	<td class="label-title">送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_sendDate" name="sampleVisitTemp.sendDate" value="<s:property value="sampleVisitTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_sendDate" name="visitTemp.sendDate" value="<s:property value="visitTemp.sendDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.sendDate"/>"><s:property value="sampleVisitTemp.sendDate"/></option> 
							<option value="<s:property value="visitTemp.sendDate"/>"><s:property value="visitTemp.sendDate"/></option>
						</select> 
						<input type="text" name="visitTempNew.sendDate" id="sendDateNew" value="<s:property value="visitTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:new Date()})" value="<s:date name="visitTempNew.sendDate" format='yyyy-MM-dd HH:mm:ss'/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                    
                    <td class="label-title">应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_reportDate" name="sampleVisitTemp.reportDate" value="<s:property value="sampleVisitTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_reportDate" name="visitTemp.reportDate" value="<s:property value="visitTemp.reportDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.reportDate"/>"><s:property value="sampleVisitTemp.reportDate"/></option> 
							<option value="<s:property value="visitTemp.reportDate"/>"><s:property value="visitTemp.reportDate"/></option>
						</select> 
						<input type="text" name="visitTempNew.reportDate" id="reportDateNew" value="<s:property value="visitTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="visitTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">送检科室</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_inspectionDept" name="sampleVisitTemp.inspectionDept"  value="<s:property value="sampleVisitTemp.inspectionDept"/>"/>
  						<input type="hidden" id="visitTemp_inspectionDept" name="visitTemp.inspectionDept" value="<s:property value="visitTemp.inspectionDept"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="inspectionDept"style="width:152px;height:28px;" onChange="javascript:document.getElementById('inspectionDeptNew').value=document.getElementById('inspectionDept').options[document.getElementById('inspectionDept').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.inspectionDept"/>"><s:property value="sampleVisitTemp.inspectionDept"/></option> 
							<option value="<s:property value="visitTemp.inspectionDept"/>"><s:property value="visitTemp.inspectionDept"/></option>
						</select> 
						<input type="text" name="visitTempNew.inspectionDept" id="inspectionDeptNew" value="<s:property value="visitTempNew.inspectionDept"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                 <tr>
                   	
                   	<td class="label-title">姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_patientName" name="sampleVisitTemp.patientName"  value="<s:property value="sampleVisitTemp.patientName"/>"/>
  						<input type="hidden" id="visitTemp_patientName" name="visitTemp.patientName" value="<s:property value="visitTemp.patientName"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientName"style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameNew').value=document.getElementById('patientName').options[document.getElementById('patientName').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.patientName"/>"><s:property value="sampleVisitTemp.patientName"/></option> 
							<option value="<s:property value="visitTemp.patientName"/>"><s:property value="visitTemp.patientName"/></option>
						</select> 
						<input type="text" name="visitTempNew.patientName" id="patientNameNew" value="<s:property value="visitTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_gender" name="sampleVisitTemp.gender" value="<s:property value="sampleVisitTemp.gender"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_gender" name="visitTemp.gender"  value="<s:property value="visitTemp.gender"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="visitTempNew.gender" id="visitTempNew_gender">
							<option value="" <s:if test="visitTempNew.gender==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="visitTempNew.gender==0">selected="selected" </s:if>>女</option>
	    					<option value="1" <s:if test="visitTempNew.gender==1">selected="selected" </s:if>>男</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                 </tr>
                 <tr>
                   	<td class="label-title">年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_age" name="sampleVisitTemp.age"  value="<s:property value="sampleVisitTemp.age"/>"/>
  						<input type="hidden" id="visitTemp_age" name="visitTemp.age" value="<s:property value="visitTemp.age"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="age"style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.age"/>"><s:property value="sampleVisitTemp.age"/></option> 
							<option value="<s:property value="visitTemp.age"/>"><s:property value="visitTemp.age"/></option>
						</select> 
						<input type="text" name="visitTempNew.age" id="ageNew" value="<s:property value="visitTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                    
                    <td class="label-title">出生日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_birthday" name="sampleVisitTemp.birthday" value="<s:property value="sampleVisitTemp.birthday"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_birthday" name="visitTemp.birthday" value="<s:property value="visitTemp.birthday"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="birthday" style="width:152px;height:28px;" onChange="javascript:document.getElementById('birthdayNew').value=document.getElementById('birthday').options[document.getElementById('birthday').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.birthday"/>"><s:property value="sampleVisitTemp.birthday"/></option> 
							<option value="<s:property value="visitTemp.birthday"/>"><s:property value="visitTemp.birthday"/></option>
						</select> 
						<input type="text" name="visitTempNew.birthday" id="birthdayNew" value="<s:property value="visitTempNew.birthday"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="visitTempNew.birthday" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">血型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_bloodType" name="sampleVisitTemp.bloodType" value="<s:property value="sampleVisitTemp.bloodType"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_bloodType" name="visitTemp.bloodType"  value="<s:property value="visitTemp.bloodType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="visitTempNew.bloodType" id="visitTempNew_bloodType">
							<option value="" <s:if test="visitTempNew.bloodType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="visitTempNew.bloodType==0">selected="selected" </s:if>>A</option>
	    					<option value="1" <s:if test="visitTempNew.bloodType==1">selected="selected" </s:if>>B</option>
	    					<option value="2" <s:if test="visitTempNew.bloodType==2">selected="selected" </s:if>>AB</option>
	    					<option value="3" <s:if test="visitTempNew.bloodType==3">selected="selected" </s:if>>O</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                 </tr>
                 <tr>
                    <td class="label-title">证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_voucherType_name" name="sampleVisitTemp.voucherType.name"  value="<s:property value="sampleVisitTemp.voucherType.name"/>"/>
  						<input type="hidden" id="sampleVisitTemp_voucherType_id" name="sampleVisitTemp.voucherType.id" value="<s:property value="sampleVisitTemp.voucherType.id"/>"/>
  						<input type="hidden" id="visitTemp_voucherType_name" name="visitTemp.voucherType.name" value="<s:property value="visitTemp.voucherType.name"/>">
  						<input type="hidden" id="visitTemp_voucherType_id" name="visitTemp.voucherType.id" value="<s:property value="visitTemp.voucherType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="voucherType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherTypeNew').value=document.getElementById('voucherType').options[document.getElementById('voucherType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.voucherType.name"/>"><s:property value="sampleVisitTemp.voucherType.name"/></option> 
							<option value="<s:property value="visitTemp.voucherType.name"/>"><s:property value="visitTemp.voucherType.name"/></option>
						</select> 
						<input type="text" name="visitTempNew.voucherType.name" id="voucherTypeNew" onfocus="voucherTypeFun()" value="<s:property value="visitTempNew.voucherType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="visitTempNew.voucherType.id" id="voucherTypeIdNew" value="<s:property value="visitTempNew.voucherType.id"/>" />
                   	</td>  
                   	
                   	<td class="label-title">证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_voucherCode" name="sampleVisitTemp.voucherCode"  value="<s:property value="sampleVisitTemp.voucherCode"/>"/>
  						<input type="hidden" id="visitTemp_voucherCode" name="visitTemp.voucherCode" value="<s:property value="visitTemp.voucherCode"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="voucherCode"style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherCodeNew').value=document.getElementById('voucherCode').options[document.getElementById('voucherCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.voucherCode"/>"><s:property value="sampleVisitTemp.voucherCode"/></option> 
							<option value="<s:property value="visitTemp.voucherCode"/>"><s:property value="visitTemp.voucherCode"/></option>
						</select> 
						<input type="text" name="visitTempNew.voucherCode" id="voucherCodeNew" value="<s:property value="visitTempNew.voucherCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_phoneNum" name="sampleVisitTemp.phoneNum"  value="<s:property value="sampleVisitTemp.phoneNum"/>"/>
  						<input type="hidden" id="visitTemp_phoneNum" name="visitTemp.phoneNum" value="<s:property value="visitTemp.phoneNum"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phoneNum"style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNumNew').value=document.getElementById('phoneNum').options[document.getElementById('phoneNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.phoneNum"/>"><s:property value="sampleVisitTemp.phoneNum"/></option> 
							<option value="<s:property value="visitTemp.phoneNum"/>"><s:property value="visitTemp.phoneNum"/></option>
						</select> 
						<input type="text" name="visitTempNew.phoneNum" id="phoneNumNew" value="<s:property value="visitTempNew.phoneNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	<td class="label-title">家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_address" name="sampleVisitTemp.address"  value="<s:property value="sampleVisitTemp.address"/>"/>
  						<input type="hidden" id="visitTemp_address" name="visitTemp.address" value="<s:property value="visitTemp.address"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="address"style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.address"/>"><s:property value="sampleVisitTemp.address"/></option> 
							<option value="<s:property value="visitTemp.address"/>"><s:property value="visitTemp.address"/></option>
						</select> 
						<input type="text" name="visitTempNew.address" id="addressNew" value="<s:property value="visitTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
	                   	
                   	<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_sampleType_name" name="sampleVisitTemp.sampleType.name"  value="<s:property value="sampleVisitTemp.sampleType.name"/>"/>
  						<input type="hidden" id="sampleVisitTemp_sampleType_id" name="sampleVisitTemp.sampleType.id" value="<s:property value="sampleVisitTemp.sampleType.id"/>"/>
  						<input type="hidden" id="visitTemp_sampleType_name" name="visitTemp.sampleType.name" value="<s:property value="visitTemp.sampleType.name"/>">
  						<input type="hidden" id="visitTemp_sampleType_id" name="visitTemp.sampleType.id" value="<s:property value="visitTemp.sampleType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.sampleType.name"/>"><s:property value="sampleVisitTemp.sampleType.name"/></option> 
							<option value="<s:property value="visitTemp.sampleType.name"/>"><s:property value="visitTemp.sampleType.name"/></option>
						</select> 
						<input type="text" name="visitTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="visitTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="visitTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="visitTempNew.sampleType.id"/>" />
                   	</td>
                   	<td class="label-title">数量</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_amount" name="sampleVisitTemp.amount"  value="<s:property value="sampleVisitTemp.amount"/>"/>
  						<input type="hidden" id="visitTemp_amount" name="visitTemp.amount" value="<s:property value="visitTemp.amount"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="amount"style="width:152px;height:28px;" onChange="javascript:document.getElementById('amountNew').value=document.getElementById('amount').options[document.getElementById('amount').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.amount"/>"><s:property value="sampleVisitTemp.amount"/></option> 
							<option value="<s:property value="visitTemp.amount"/>"><s:property value="visitTemp.amount"/></option>
						</select> 
						<input type="text" name="visitTempNew.amount" id="amountNew" value="<s:property value="visitTempNew.amount"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	<td class="label-title">采集方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_collectionWay" name="sampleVisitTemp.collectionWay"  value="<s:property value="sampleVisitTemp.collectionWay"/>"/>
  						<input type="hidden" id="visitTemp_collectionWay" name="visitTemp.collectionWay" value="<s:property value="visitTemp.collectionWay"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="collectionWay"style="width:152px;height:28px;" onChange="javascript:document.getElementById('collectionWayNew').value=document.getElementById('collectionWay').options[document.getElementById('collectionWay').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.collectionWay"/>"><s:property value="sampleVisitTemp.collectionWay"/></option> 
							<option value="<s:property value="visitTemp.collectionWay"/>"><s:property value="visitTemp.collectionWay"/></option>
						</select> 
						<input type="text" name="visitTempNew.collectionWay" id="collectionWayNew" value="<s:property value="visitTempNew.collectionWay"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">其它</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_reason2" name="sampleVisitTemp.reason2"  value="<s:property value="sampleVisitTemp.reason2"/>"/>
  						<input type="hidden" id="visitTemp_reason2" name="visitTemp.reason2" value="<s:property value="visitTemp.reason2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reason2"style="width:152px;height:28px;" onChange="javascript:document.getElementById('reason2New').value=document.getElementById('reason2').options[document.getElementById('reason2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.reason2"/>"><s:property value="sampleVisitTemp.reason2"/></option> 
							<option value="<s:property value="visitTemp.reason2"/>"><s:property value="visitTemp.reason2"/></option>
						</select> 
						<input type="text" name="visitTempNew.reason2" id="reason2New" value="<s:property value="visitTempNew.reason2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	<td class="label-title">采集部位</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_gatherPart" name="sampleVisitTemp.gatherPart"  value="<s:property value="sampleVisitTemp.gatherPart"/>"/>
  						<input type="hidden" id="visitTemp_gatherPart" name="visitTemp.gatherPart" value="<s:property value="visitTemp.gatherPart"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="gatherPart"style="width:152px;height:28px;" onChange="javascript:document.getElementById('gatherPartNew').value=document.getElementById('gatherPart').options[document.getElementById('gatherPart').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.gatherPart"/>"><s:property value="sampleVisitTemp.gatherPart"/></option> 
							<option value="<s:property value="visitTemp.gatherPart"/>"><s:property value="visitTemp.gatherPart"/></option>
						</select> 
						<input type="text" name="visitTempNew.gatherPart" id="gatherPartNew" value="<s:property value="visitTempNew.gatherPart"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">病理推断</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_confirmAgeTwo" name="sampleVisitTemp.confirmAgeTwo"  value="<s:property value="sampleVisitTemp.confirmAgeTwo"/>"/>
  						<input type="hidden" id="visitTemp_confirmAgeTwo" name="visitTemp.confirmAgeTwo" value="<s:property value="visitTemp.confirmAgeTwo"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAgeTwo"style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeTwoNew').value=document.getElementById('confirmAgeTwo').options[document.getElementById('confirmAgeTwo').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.confirmAgeTwo"/>"><s:property value="sampleVisitTemp.confirmAgeTwo"/></option> 
							<option value="<s:property value="visitTemp.confirmAgeTwo"/>"><s:property value="visitTemp.confirmAgeTwo"/></option>
						</select> 
						<input type="text" name="visitTempNew.confirmAgeTwo" id="confirmAgeTwoNew" value="<s:property value="visitTempNew.confirmAgeTwo"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_diagnosis" name="sampleVisitTemp.diagnosis"  value="<s:property value="sampleVisitTemp.diagnosis"/>"/>
  						<input type="hidden" id="visitTemp_diagnosis" name="visitTemp.diagnosis" value="<s:property value="visitTemp.diagnosis"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="diagnosis"style="width:152px;height:28px;" onChange="javascript:document.getElementById('diagnosisNew').value=document.getElementById('diagnosis').options[document.getElementById('diagnosis').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.diagnosis"/>"><s:property value="sampleVisitTemp.diagnosis"/></option> 
							<option value="<s:property value="visitTemp.diagnosis"/>"><s:property value="visitTemp.diagnosis"/></option>
						</select> 
						<input type="text" name="visitTempNew.diagnosis" id="diagnosisNew" value="<s:property value="visitTempNew.diagnosis"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	<td class="label-title">手术史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_bloodHistory" name="sampleVisitTemp.bloodHistory"  value="<s:property value="sampleVisitTemp.bloodHistory"/>"/>
  						<input type="hidden" id="visitTemp_bloodHistory" name="visitTemp.bloodHistory" value="<s:property value="visitTemp.bloodHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="bloodHistory"style="width:152px;height:28px;" onChange="javascript:document.getElementById('bloodHistoryNew').value=document.getElementById('bloodHistory').options[document.getElementById('bloodHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.bloodHistory"/>"><s:property value="sampleVisitTemp.bloodHistory"/></option> 
							<option value="<s:property value="visitTemp.bloodHistory"/>"><s:property value="visitTemp.bloodHistory"/></option>
						</select> 
						<input type="text" name="visitTempNew.bloodHistory" id="bloodHistoryNew" value="<s:property value="visitTempNew.bloodHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">用药史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_medicalHistory" name="sampleVisitTemp.medicalHistory"  value="<s:property value="sampleVisitTemp.medicalHistory"/>"/>
  						<input type="hidden" id="visitTemp_medicalHistory" name="visitTemp.medicalHistory" value="<s:property value="visitTemp.medicalHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="medicalHistory"style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicalHistoryNew').value=document.getElementById('medicalHistory').options[document.getElementById('medicalHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.medicalHistory"/>"><s:property value="sampleVisitTemp.medicalHistory"/></option> 
							<option value="<s:property value="visitTemp.medicalHistory"/>"><s:property value="visitTemp.medicalHistory"/></option>
						</select> 
						<input type="text" name="visitTempNew.medicalHistory" id="medicalHistoryNew" value="<s:property value="visitTempNew.medicalHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">吸烟史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_tumorHistory" name="sampleVisitTemp.tumorHistory"  value="<s:property value="sampleVisitTemp.tumorHistory"/>"/>
  						<input type="hidden" id="visitTemp_tumorHistory" name="visitTemp.tumorHistory" value="<s:property value="visitTemp.tumorHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="tumorHistory"style="width:152px;height:28px;" onChange="javascript:document.getElementById('tumorHistoryNew').value=document.getElementById('tumorHistory').options[document.getElementById('tumorHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.tumorHistory"/>"><s:property value="sampleVisitTemp.tumorHistory"/></option> 
							<option value="<s:property value="visitTemp.tumorHistory"/>"><s:property value="visitTemp.tumorHistory"/></option>
						</select> 
						<input type="text" name="visitTempNew.tumorHistory" id="tumorHistoryNew" value="<s:property value="visitTempNew.tumorHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>其它信息</label>
						</div>
					</td>
				</tr>
				<tr>
                   	<td class="label-title">特殊情况说明</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_serialNum" name="sampleVisitTemp.serialNum"  value="<s:property value="sampleVisitTemp.serialNum"/>"/>
  						<input type="hidden" id="visitTemp_serialNum" name="visitTemp.serialNum" value="<s:property value="visitTemp.serialNum"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="serialNum"style="width:152px;height:28px;" onChange="javascript:document.getElementById('serialNumNew').value=document.getElementById('serialNum').options[document.getElementById('serialNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.serialNum"/>"><s:property value="sampleVisitTemp.serialNum"/></option> 
							<option value="<s:property value="visitTemp.serialNum"/>"><s:property value="visitTemp.serialNum"/></option>
						</select> 
						<input type="text" name="visitTempNew.serialNum" id="serialNumNew" value="<s:property value="visitTempNew.serialNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_isInsure" name="sampleVisitTemp.isInsure" value="<s:property value="sampleVisitTemp.isInsure"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_isInsure" name="visitTemp.isInsure"  value="<s:property value="visitTemp.isInsure"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="visitTempNew.isInsure" id="visitTempNew_isInsure">
							<option value="" <s:if test="visitTempNew.isInsure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="visitTempNew.isInsure==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="visitTempNew.isInsure==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleVisitTemp_isInvoice" name="sampleVisitTemp.isInvoice" value="<s:property value="sampleVisitTemp.isInvoice"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="visitTemp_isInvoice" name="visitTemp.isInvoice"  value="<s:property value="visitTemp.isInvoice"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="visitTempNew.isInvoice" id="visitTempNew_isInvoice">
							<option value="" <s:if test="visitTempNew.isInvoice==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="visitTempNew.isInvoice==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="visitTempNew.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                 </tr>
                 <tr>
                   	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="visitTempNew_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="visitTempNew.createUser" title="审入人"   
							value="<s:property value="visitTempNew.createUser"/>" />
                   	</td>
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleVisitTemp_reason" name="sampleVisitTemp.reason"  value="<s:property value="sampleVisitTemp.reason"/>"/>
  						<input type="hidden" id="visitTemp_reason" name="visitTemp.reason" value="<s:property value="visitTemp.reason"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reason"style="width:152px;height:28px;" onChange="javascript:document.getElementById('reasonNew').value=document.getElementById('reason').options[document.getElementById('reason').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleVisitTemp.reason"/>"><s:property value="sampleVisitTemp.reason"/></option> 
							<option value="<s:property value="visitTemp.reason"/>"><s:property value="visitTemp.reason"/></option>
						</select> 
						<input type="text" name="visitTempNew.reason" id="reasonNew" value="<s:property value="visitTempNew.reason"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  	
                  	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="visitTempNew.nextStepFlow" id="visitTempNew_nextStepFlow">
							<option value="" <s:if test="visitTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="visitTempNew.nextStepFlow==1">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="visitTempNew.nextStepFlow==0">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                 </tr>
               
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleVisit.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
