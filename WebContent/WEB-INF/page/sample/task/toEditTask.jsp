<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <title>任务</title> -->
<script>
	function xzCheckValue() {
		var coproductandefficiencyStr1 = document
				.getElementById("sck_checkBoxTest1").value;
		$("input[name='sck.checkedBoxTest']").each(function() {
			if (coproductandefficiencyStr1.indexOf($(this).val()) >= 0) {
				$(this).attr("checked", true);
			}
		});
	}
	function checkValue2() {
		var checks = "";
		var checkName = "";
		$("[id='sck_checkedBoxTest']:checked").each(function() {
			checks += $(this).val() + ","; //动态拼取选中的checkbox的值，用“|”符号分隔
			checkName += document.getElementById($(this).val()).innerText + ",";   
		});
		document.getElementById("sck_checkBoxTest1").value = checks;
		document.getElementById("sck_qcTestName").value = checkName;
	}

	function downFile(id) {
		window.open(window.ctx + '/operfile/downloadById.action?id=' + id, '',
				'');
	}
	//调用模板
function TemplateFun(){
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src="+window.ctx +"'/system/template/template/templateSelectByType.action?flag=TemplateFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
function setTemplateFun(rec){
		var code=$("#sck_template").val();
		if(code==""){
			document.getElementById('sck_template').value=rec.get('id');
			document.getElementById('sck_template_name').value=rec.get('name');
			var win = Ext.getCmp('TemplateFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/system/template/template/setTemplateItem.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						var ob = dnaTemplateItemGrid.getStore().recordType;
						dnaTemplateItemGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.id);
							p.set("name",obj.name);
							dnaTemplateItemGrid.getStore().add(p);							
						});
						
						dnaTemplateItemGrid.startEditing(0, 0);		
					} else {
						message("获取明细数据时发生错误！");
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateReagent.action", {
				code : id,
				}, function(data) {
					if (data.success) {	

						var ob = dnaTemplateReagentGrid.getStore().recordType;
						dnaTemplateReagentGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.id);
							p.set("name",obj.name);
							p.set("batch",obj.batch);
							p.set("isGood",obj.isGood);
							dnaTemplateReagentGrid.getStore().add(p);							
						});
						
						dnaTemplateReagentGrid.startEditing(0, 0);		
					} else {
						message("获取明细数据时发生错误！");
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateCos.action", {
				code : id,
				}, function(data) {
					if (data.success) {	

						var ob = dnaTemplateCosGrid.getStore().recordType;
						dnaTemplateCosGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.id);
							p.set("name",obj.name);
							p.set("isGood",obj.isGood);
							dnaTemplateCosGrid.getStore().add(p);							
						});			
						dnaTemplateCosGrid.startEditing(0, 0);		
					} else {
						message("获取明细数据时发生错误！");
					}
				}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
 				dnaTemplateItemGrid.store.removeAll();
  				dnaTemplateReagentGrid.store.removeAll();
 				dnaTemplateCosGrid.store.removeAll();
 				document.getElementById('sck_template').value=rec.get('id');
 				document.getElementById('sck_template_name').value=rec.get('name');
			 var win = Ext.getCmp('TemplateFun');
			 if(win){win.close();}
				var id = rec.get('id');
				ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = dnaTemplateItemGrid.getStore().recordType;
							dnaTemplateItemGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("code",obj.id);
								p.set("name",obj.name);
								dnaTemplateItemGrid.getStore().add(p);							
							});
							
							dnaTemplateItemGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = dnaTemplateReagentGrid.getStore().recordType;
							dnaTemplateReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("code",obj.id);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
								dnaTemplateReagentGrid.getStore().add(p);							
							});
							
							dnaTemplateReagentGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = dnaTemplateCosGrid.getStore().recordType;
							dnaTemplateCosGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("code",obj.id);
								p.set("name",obj.name);
								p.set("isGood",obj.isGood);
								dnaTemplateCosGrid.getStore().add(p);							
							});			
							dnaTemplateCosGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null);
			 }
			
 		}
		
}

/* function showTemplateItem(id){
ajax("post", "/system/template/template/setTemplateItem.action", {
	code : id,
	}, function(data) {
		if (data.success) {	

			var ob = dnaTemplateItemGrid.getStore().recordType;
			dnaTemplateItemGrid.stopEditing();
			
			$.each(data.data, function(i, obj) {
				var p = new ob({});
				p.isNew = true;
				alert(obj.code);
				p.set("code",obj.code);
				p.set("name",obj.name);
				dnaTemplateItemGrid.getStore().add(p);							
			});
			
			dnaTemplateItemGrid.startEditing(0, 0);		
		} else {
			message("获取明细数据时发生错误！");
		}
	}, null); 
ajax("post", "/system/template/template/setTemplateReagent.action", {
	code : id,
	}, function(data) {
		if (data.success) {	

			var ob = dnaTemplateReagentGrid.getStore().recordType;
			dnaTemplateReagentGrid.stopEditing();
			
			$.each(data.data, function(i, obj) {
				var p = new ob({});
				p.isNew = true;
				p.set("code",obj.code);
				p.set("name",obj.name);
				p.set("batch",obj.batch);
				p.set("isGood",obj.isGood);
				dnaTemplateReagentGrid.getStore().add(p);							
			});
			
			dnaTemplateReagentGrid.startEditing(0, 0);		
		} else {
			message("获取明细数据时发生错误！");
		}
	}, null); 
ajax("post", "/system/template/template/setTemplateCos.action", {
	code : id,
	}, function(data) {
		if (data.success) {	

			var ob = dnaTemplateCosGrid.getStore().recordType;
			dnaTemplateCosGrid.stopEditing();
			
			$.each(data.data, function(i, obj) {
				var p = new ob({});
				p.isNew = true;
				p.set("code",obj.code);
				p.set("name",obj.name);
				p.set("isGood",obj.isGood);
				dnaTemplateCosGrid.getStore().add(p);							
			});			
			dnaTemplateCosGrid.startEditing(0, 0);		
		} else {
			message("获取明细数据时发生错误！");
		}
	}, null); 
} */
</script>
<body>
	<g:LayOutWinTag buttonId="wzBtn" title="选择位置" hasHtmlFrame="true"
		html="${ctx}/storage/common/showStoragePositionTree.action?setStoragePostion=true"
		isHasSubmit="false" functionName="showStoragePositionFun"
		hasSetFun="true" documentId="sck_location_id"
		documentName="sck_location_name" />
	<g:LayOutWinTag buttonId="jsBtn" title="接收人"
		width="document.body.clientWidth/1.5" hasHtmlFrame="true"
		html="${ctx}/core/user/userSelect.action" isHasSubmit="false" 
		functionName="js" hasSetFun="true" documentId="accept_user_id"
		documentName="accept_user_name" />

	<g:LayOutWinTag buttonId="zjrBtn" title="质检人"
		width="document.body.clientWidth/1.5" hasHtmlFrame="true"
		html="${ctx}/core/user/userSelect.action" isHasSubmit="false" 
		functionName="zjr" hasSetFun="true" documentId="qc_user_id"
		documentName="qc_user_name" />
<s:if test='sck.id!="NEW"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=commonTask&id=${sck.id}"
		isHasSubmit="false" functionName="doc" />
	
	<g:LayOutWinTag buttonId="gwsm_img" title="岗位说明书" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=info&id=dnagwsm&view=true"
		isHasSubmit="false" functionName="doc" />
	<g:LayOutWinTag buttonId="sop_img" title="实验SOP" hasHtmlFrame="true"
		width="900" height="500"
		html="${ctx}/operfile/initFileList.action?modelType=info&id=dnasop&view=true"
		isHasSubmit="false" functionName="doc" />
		
	</s:if>

	<div>
		<s:form theme="simple" method="post" id="task_form">
			<input type="hidden" name="itemDataJson" id="item_data_json">
			<input type="hidden" name="storageDateJson" id="storage_date_json">
			<input type="hidden" name="successPlasmaJson"
				id="success_plasma_json">
			<input type="hidden" name="successDnaJson" id="success_Dna_json">
			<input type="hidden" name="successWKJson" id="success_WK_json">
			<input type="hidden" name="dnaTemplateItemJson" id="dnaTemplateItemJson" value="" />
        	<input type="hidden" name="dnaTemplateReagentJson" id="dnaTemplateReagentJson" value="" />
       	 	<input type="hidden" name="dnaTemplateCosJson" id="dnaTemplateCosJson" value="" />
			<table class="frame-table">
				<tr>
					<td class="label-title">任务单号</td>
					<td><s:textfield maxlength="32" id="task_id_form"
							name="sck.id" cssClass="input-20-length readonlytrue"
							readonly="true"></s:textfield> <img class='requiredimage'
						src='${ctx}/images/required.gif' /></td>
					<td class="label-title">描述</td>
					<td><s:textfield maxlength="100" id="note" name="sck.note"
							cssClass="input-40-length"></s:textfield></td>
					<td class="label-title">下达人</td>
					<td><s:textfield id="task_send_name" readonly="true"
							cssClass="readonlytrue" name="sck.sendUser.name"></s:textfield> <s:hidden
							id="task_send_id" name="sck.sendUser.id"></s:hidden></td>
				</tr>
				<tr>
					<td class="label-title">下达日期</td>
					<td><input type="text" class="readonlytrue"
						readonly="readonly" id="sck_sendDate" name="sck.sendDate"
						value='<s:date name="sck.sendDate" format="yyyy-MM-dd HH:mm:ss"/>'>
					</td>
					<td class="label-title">实验员</td>
					<td><input type="text" name="acceptUser" id="accept_user_name"
						readonly="readonly"
						value='<s:property value="sck.acceptUser.name"/>'> <s:hidden
							name="sck.acceptUser.id" id="accept_user_id" /> <span id="jsBtn"
						class="select-search-btn">&nbsp;&nbsp;&nbsp;</span> <img class='requiredimage'
						src='${ctx}/images/required.gif' /></td>
					<td class="label-title">接收日期</td>
					<td><input id="sck_acceptDate" type="text"
						class="readonlytrue" readonly="readonly" name="sck.acceptDate"
						value='<s:date name="sck.acceptDate" format="yyyy-MM-dd HH:mm"/>'>
					</td>
				</tr>
				<tr>
				<%if(!request.getParameter("taskType").equals("3")){%>
					<td class="label-title">下一步任务</td>
					<td>
						
							<select name="sck.nextStep" id="sck_nextStep" style="width:120px">
								
								<!-- <option value="" <s:if test="sck.nextStep==''">selected="selected" </s:if>>请选择</option> -->
								<%if(request.getParameter("taskType").equals("2")){%>
								<option value="notOutsource" <s:if test="sck.nextStep=='notOutsource'">selected="selected" </s:if>>DNA提取</option>
								<%} %>
								
								<%if(request.getParameter("taskType").equals("3")){%>
							<option value="two" <s:if test="sck.nextStep=='two'">selected="selected" </s:if>>二代</option>
								<option value="mlpa" <s:if test="sck.nextStep=='mlpa'">selected="selected"</s:if>>MLPA</option>
								<option value="mutation" <s:if test="sck.nextStep=='mutation'">selected="selected"</s:if>>动态突变</option>
								<option value="research" <s:if test="bo.nextStep=='research'">selected="selected"</s:if>>科研任务</option>
								<%} %>
								
							</select> <img class='requiredimage'
						src='${ctx}/images/required.gif' /></td>
						<%} %>
						<td class="label-title">工作流状态</td>
					<td><s:textfield name="sck.stateName" id="sck_stateName" cssClass="readonlytrue"
							readonly="true">
						</s:textfield> <s:hidden name="sck.state" id="sck_state">
						</s:hidden></td>
											<td class="label-title">附件</td>
					<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
						</td>
							<td class="label-title">岗位说明及SOP</td>
					<td><span id="gwsm_img" title="岗位说明书"
							class="attach-btn"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span id="sop_img" title="SOP" 
							class="attach-btn"></span>
					</td>		
				</tr>
				<%if(!request.getParameter("taskType").equals("3")){%>
				<tr>
				<td class="label-title">实验编号</td>
				<td><input type="text" name="sck.sampleCode" id="sck_sampleCode"
						readonly="readonly"
						value='<s:property value="sck.sampleCode"/>'> </td>
						<td class="label-title">提示存储位置</td>
				<td><input type="text" name="sck.location.name" id="sck_location_name"
						readonly="readonly"
						value='<s:property value="sck.location.name"/>'>
						<input type="hidden" name="sck.location.id" id="sck_location_id"
						readonly="readonly"
						value='<s:property value="sck.location.id"/>'>  </td>
				</tr>
				<%} %>
				
				<tr>
					<td colspan="6">
						<div class="standard-section-header type-title">
							<label>存储信息</label>
						</div>
					</td>
				</tr>
				<tr>
					<td class="label-title blood_show">DNA储位提示</td>
					<td class="blood_show" colspan="6">
						<input type="text"
							class="input_parts text input  false false"
							name="sck.location.id" id="sck_location_id"
							value='<s:property value="sck.location.id"/>'
							title="存储位置 " size="10"
							maxlength="30" readonly="readOnly" /> 
							<img alt='选择' id='wzBtn'
							name='show-btn' src='${ctx}/images/img_menu.gif'
							class='detail' /> 
						<input type="text" class="input_parts text input  readonlytrue"
							name="sck.location.name" id="sck_location_name"
							readonly="readonly"
							value='<s:property value="sck.location.name"/>'
							title="存储位置 "  size="30"
							maxlength="62" /> 
					</td>
				</tr>
				<tr>
					<td class="label-title" >选择模板</td>
                   	<td align="left"  >
 						<input type="hidden" size="40" readonly="readOnly"  id="sck_template_name"  value="<s:property value="sck.template.name"/>" />
 						<input type="text" id="sck_template" name="sck.template.id"  value="<s:property value="sck.template.id"/>" > 
 						<img alt='选择模板' id='showTemplateFun' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="TemplateFun()" />                   		
                   	</td>

				</tr>
				
				<%-- <tr>
					<td colspan="6">
						<div class="standard-section-header type-title">
							<label>质检方法 </label>
						</div>
					</td>
					
				</tr>

				
				<tr>
					<td class="label-title"></td>
					<td colspan="5"><c:forEach var="DicCountTable"
							items="${DicCountTableList}">
							
							<span id="${DicCountTable.id}" class="label-title">${DicCountTable.name}</span><input type="checkbox" name="sck.checkedBoxTest"
								id="sck_checkedBoxTest" value="${DicCountTable.id}" />&nbsp;&nbsp;&nbsp;&nbsp;
						</c:forEach>
					<input type="hidden" name="sck.checkBoxTest1" id="sck_checkBoxTest1"
					value="<s:property value="sck.checkedBoxTest"/>" />
					<input type="hidden" name="sck.qcTestName" id="sck_qcTestName"
					value="<s:property value="sck.qcTestName"/>" />
						</td>
				</tr>
				</table>
				<table style="display:none">
				<tr >	
						<td class="label-title">质检人</td>
					<td><input type="text" name="qcUser" id="qc_user_name"
						readonly="readonly"
						value='<s:property value="sck.qcUser.name"/>'> <s:hidden
							name="sck.qcUser.id" id="qc_user_id" /> <span id="zjrBtn"
						class="select-search-btn">&nbsp;&nbsp;&nbsp;</span> <img class='requiredimage'
						src='${ctx}/images/required.gif' /></td></td>
					<td class="label-title">质检日期</td>
					<td><input id="sck_qcDate" type="text"
						class="readonlytrue" readonly="readonly" name="sck.qcDate"
						value='<s:date name="sck.qcDate" format="yyyy-MM-dd HH:mm"/>'>
					</td>

				</tr> --%>
			</table>
		</s:form>
	</div>
</body>
</html>
<script>
	/* xzCheckValue();	 */
</script>