<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript"
	src="${ctx}/javascript/lib/jquery.fileupload.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/lib/jquery.iframe-transport.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/sample/task/sampleCommonUpload.js"></script>
<link rel=stylesheet type="text/css" href="${ctx}/css/sample.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
</head>
<body>
<div id="upload_file_div">
	<input type="hidden" value="${requestScope.moduleType}" id="module_type">
	<input type="hidden" value="${requestScope.isUpload }" id="is_upload">
	<input type="file" name="file11" id="file-upload11" >
	<input type="hidden" value="" id="file_id">
	<table class="frame-iefull-table" style="margin-top: 10px;">
			<tr>
				<td class="label-title td-nowrap"  style="width:60px">
					文件名称:
				</td>
				<td>
					<span id="file_name">
						<s:property value="fileInfo.fileName"/>
						&nbsp;
					</span>
				</td>
			</tr>
			<tr>
				<td class="label-title td-nowrap"  style="width:60px">
					上传时间:
				</td>
				<td>
					<span id="upload_time">
						<s:date name="fileInfo.uploadTime" format="yyyy-MM-dd HH:mm:ss"/>&nbsp;
					</span>
				</td>
			</tr>
		</table>
</div>
</body>
</html>