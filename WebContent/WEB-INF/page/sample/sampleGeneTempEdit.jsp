
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleGeneTemp&id=${sampleGeneTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleGeneTempEdit.js"></script>
	<s:if test="sampleGeneTemp.sampleInfo.upLoadAccessory.id != null">
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleGeneTemp.sampleInfo.upLoadAccessory.id}"></div>
	</s:if>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
            <input type="hidden" id="fname" value="${requestScope.fname}">
            <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
                   	<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleGeneTemp_id" name="sampleGeneTemp.id" title="编号" value="<s:property value="sampleGeneTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleGeneTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleGeneTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   		<input type ="hidden" id="saveType" value="djy" >
                   	</td>
                   
                   
                   	<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleGeneTemp_code" class="text input readonlytrue" readonly="readonly" name="geneTempNew.sampleInfo.code" title="样本编号" value="<s:property value="geneTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="geneTempNew_code" class="text input readonlytrue" readonly="readonly" name="geneTempNew.code" title="样本编号" value="<s:property value="geneTempNew.code"/>" />
                   	</td>
                   	
                   	<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="geneTempNew_name" name="geneTempNew.name" title="描述" value="<s:property value="geneTempNew.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="sampleGeneTemp_name" name="sampleGeneTemp.name" title="描述" value="<s:property value="sampleGeneTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="geneTemp_name" name="geneTemp.name" title="描述" value="<s:property value="geneTemp.name"/>"/>
                   	</td>
                   	
                   	<td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_productId" name="sampleGeneTemp.productId"  value="<s:property value="sampleGeneTemp.productId"/>"/>
  						<input type="hidden" id="sampleGeneTemp_productName" name="sampleGeneTemp.productName" value="<s:property value="sampleGeneTemp.productName"/>"/>
  						<input type="hidden" id="geneTemp_productId" name="geneTemp.productId" value="<s:property value="geneTemp.productId"/>">
  						<input type="hidden" id="geneTemp_productName" name="geneTemp.productName" value="<s:property value="geneTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.productName"/>"><s:property value="sampleGeneTemp.productName"/></option> 
							<option value="<s:property value="geneTemp.productName"/>"><s:property value="geneTemp.productName"/></option>
						</select> 
						<input type="text" name="geneTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="geneTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="geneTempNew.productId" id="productIdNew" value="<s:property value="geneTempNew.productId"/>" />
                   	</td>
                   		
                   	
               </tr>
                <tr>
                   	<td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleGeneTemp_acceptDate" name="sampleGeneTemp.acceptDate" value="<s:property value="sampleGeneTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="geneTemp_acceptDate" name="geneTemp.acceptDate" value="<s:property value="geneTemp.acceptDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.acceptDate"/>"><s:property value="sampleGeneTemp.acceptDate"/></option> 
							<option value="<s:property value="geneTemp.acceptDate"/>"><s:property value="geneTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="geneTempNew.acceptDate" id="acceptDateNew" value="<s:property value="geneTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="geneTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_area" name="sampleGeneTemp.area"  value="<s:property value="sampleGeneTemp.area"/>"/>
  						<input type="hidden" id="geneTemp_area" name="geneTemp.area" value="<s:property value="geneTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.area"/>"><s:property value="sampleGeneTemp.area"/></option> 
							<option value="<s:property value="geneTemp.area"/>"><s:property value="geneTemp.area"/></option>
						</select> 
						<input type="text" name="geneTempNew.area" id="areaNew" value="<s:property value="geneTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_hospital" name="sampleGeneTemp.hospital"  value="<s:property value="sampleGeneTemp.hospital"/>"/>
  						<input type="hidden" id="geneTemp_hospital" name="geneTemp.hospital" value="<s:property value="geneTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital" style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.hospital"/>"><s:property value="sampleGeneTemp.hospital"/></option> 
							<option value="<s:property value="geneTemp.hospital"/>"><s:property value="geneTemp.hospital"/></option>
						</select> 
						<input type="text" name="geneTempNew.hospital" id="hospitalNew" value="<s:property value="geneTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
					<td class="label-title">送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleGeneTemp_sendDate" name="sampleGeneTemp.sendDate" value="<s:property value="sampleGeneTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="geneTemp_sendDate" name="geneTemp.sendDate" value="<s:property value="geneTemp.sendDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.sendDate"/>"><s:property value="sampleGeneTemp.sendDate"/></option> 
							<option value="<s:property value="geneTemp.sendDate"/>"><s:property value="geneTemp.sendDate"/></option>
						</select> 
						<input type="text" name="geneTempNew.sendDate" id="sendDateNew" value="<s:property value="geneTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})" value="<s:date name="geneTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
					
                   <td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleGeneTemp_reportDate" name="sampleGeneTemp.reportDate" value="<s:property value="sampleGeneTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="geneTemp_reportDate" name="geneTemp.reportDate" value="<s:property value="geneTemp.reportDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.reportDate"/>"><s:property value="sampleGeneTemp.reportDate"/></option> 
							<option value="<s:property value="geneTemp.reportDate"/>"><s:property value="geneTemp.reportDate"/></option>
						</select> 
						<input type="text" name="geneTempNew.reportDate" id="reportDateNew" value="<s:property value="geneTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="geneTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   
                   	<td class="label-title">检测内容</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_confirmAgeOne" name="sampleGeneTemp.confirmAgeOne"  value="<s:property value="sampleGeneTemp.confirmAgeOne"/>"/>
  						<input type="hidden" id="geneTemp_confirmAgeOne" name="geneTemp.confirmAgeOne" value="<s:property value="geneTemp.confirmAgeOne"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAgeOne" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeOneNew').value=document.getElementById('confirmAgeOne').options[document.getElementById('confirmAgeOne').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.confirmAgeOne"/>"><s:property value="sampleGeneTemp.confirmAgeOne"/></option> 
							<option value="<s:property value="geneTemp.confirmAgeOne"/>"><s:property value="geneTemp.confirmAgeOne"/></option>
						</select> 
						<input type="text" name="geneTempNew.confirmAgeOne" id="confirmAgeOneNew" value="<s:property value="geneTempNew.confirmAgeOne"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   			
                  </tr>
                  <tr>
                  
                  	<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_sampleType_id" name="sampleGeneTemp.sampleType.id"  value="<s:property value="sampleGeneTemp.sampleType.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleGeneTemp_sampleType_name" name="sampleGeneTemp.sampleType.name"  value="<s:property value="sampleGeneTemp.sampleType.name"/>" class="text input" />
  						<input type="hidden" id="geneTemp_sampleType_id" name="geneTemp.sampleType.id" value="<s:property value="geneTemp.sampleType.id"/>">
  						<input type="hidden" id="geneTemp_sampleType_name" name="geneTemp.sampleType.name" value="<s:property value="geneTemp.sampleType.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.sampleType.name"/>"><s:property value="sampleGeneTemp.sampleType.name"/></option> 
							<option value="<s:property value="geneTemp.sampleType.name"/>"><s:property value="geneTemp.sampleType.name"/></option>
						</select> 
						<input type="text" name="geneTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="geneTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
            			<input type="hidden" name="geneTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="geneTempNew.sampleType.id"/>" />
                   	</td>
                   	
                   	<td class="label-title">送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_doctor" name="sampleGeneTemp.doctor"  value="<s:property value="sampleGeneTemp.doctor"/>"/>
  						<input type="hidden" id="geneTemp_doctor" name="geneTemp.doctor" value="<s:property value="geneTemp.doctor"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="doctor" style="width:152px;height:28px;" onChange="javascript:document.getElementById('doctorNew').value=document.getElementById('doctor').options[document.getElementById('doctor').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.doctor"/>"><s:property value="sampleGeneTemp.doctor"/></option> 
							<option value="<s:property value="geneTemp.doctor"/>"><s:property value="geneTemp.doctor"/></option>
						</select> 
						<input type="text" name="geneTempNew.doctor" id="doctorNew" value="<s:property value="geneTempNew.doctor"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
                   	
<!--                    	<td class="label-title">样本编号</td> -->
<%--                     <td class="requiredcolunm" nowrap width="10px" > </td> --%>
<!--                     <td align="left"> -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleGeneTemp_sampleNum"  -->
<%--                    			name="sampleGeneTemp.sampleNum" title="样本编号" value="<s:property value="sampleGeneTemp.sampleNum"/>" --%>
<!--                    		/> -->
<!--                    </td> -->
               </tr>
               <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	
                   	<td class="label-title">病人姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_patientName" name="sampleGeneTemp.patientName"  value="<s:property value="sampleGeneTemp.patientName"/>"/>
  						<input type="hidden" id="geneTemp_patientName" name="geneTemp.patientName" value="<s:property value="geneTemp.patientName"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameNew').value=document.getElementById('patientName').options[document.getElementById('patientName').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.patientName"/>"><s:property value="sampleGeneTemp.patientName"/></option> 
							<option value="<s:property value="geneTemp.patientName"/>"><s:property value="geneTemp.patientName"/></option>
						</select> 
						<input type="text" name="geneTempNew.patientName" id="patientNameNew" value="<s:property value="geneTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_patientNameSpell" name="sampleGeneTemp.patientNameSpell"  value="<s:property value="sampleGeneTemp.patientNameSpell"/>"/>
  						<input type="hidden" id="geneTemp_patientNameSpell" name="geneTemp.patientNameSpell" value="<s:property value="geneTemp.patientNameSpell"/>">
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientNameSpell" style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameSpellNew').value=document.getElementById('patientNameSpell').options[document.getElementById('patientNameSpell').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.patientNameSpell"/>"><s:property value="sampleGeneTemp.patientNameSpell"/></option> 
							<option value="<s:property value="geneTemp.patientNameSpell"/>"><s:property value="geneTemp.patientNameSpell"/></option>
						</select> 
						<input type="text" name="geneTempNew.patientNameSpell" id="patientNameSpellNew" value="<s:property value="geneTempNew.patientNameSpell"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
               		
               	 	<td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleGeneTemp_gender" name="sampleGeneTemp.gender" value="<s:property value="sampleGeneTemp.gender"/>" />
               	 		<input type="hidden" id="geneTemp_gender" name="geneTemp.gender" value="<s:property value="geneTemp.gender"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="geneTempNew.gender" id="geneTempNew_gender" >
							<option value="" <s:if test="geneTempNew.gender==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="geneTempNew.gender==0">selected="selected" </s:if>>女</option>
    						<option value="1" <s:if test="geneTempNew.gender==1">selected="selected" </s:if>>男</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_age" name="sampleGeneTemp.age"  value="<s:property value="sampleGeneTemp.age"/>"/>
  						<input type="hidden" id="geneTemp_age" name="geneTemp.age" value="<s:property value="geneTemp.age"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="age" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.age"/>"><s:property value="sampleGeneTemp.age"/></option> 
							<option value="<s:property value="geneTemp.age"/>"><s:property value="geneTemp.age"/></option>
						</select> 
						<input type="text" name="geneTempNew.age" id="ageNew" value="<s:property value="geneTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_nativePlace" name="sampleGeneTemp.nativePlace"  value="<s:property value="sampleGeneTemp.nativePlace"/>"/>
  						<input type="hidden" id="geneTemp_nativePlace" name="geneTemp.nativePlace" value="<s:property value="geneTemp.nativePlace"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="nativePlace" style="width:152px;height:28px;" onChange="javascript:document.getElementById('nativePlaceNew').value=document.getElementById('nativePlace').options[document.getElementById('nativePlace').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.nativePlace"/>"><s:property value="sampleGeneTemp.nativePlace"/></option> 
							<option value="<s:property value="geneTemp.nativePlace"/>"><s:property value="geneTemp.nativePlace"/></option>
						</select> 
						<input type="text" name="geneTempNew.nativePlace" id="nativePlaceNew" value="<s:property value="geneTempNew.nativePlace"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_nationality" name="sampleGeneTemp.nationality"  value="<s:property value="sampleGeneTemp.nationality"/>"/>
  						<input type="hidden" id="geneTemp_nationality" name="geneTemp.nationality" value="<s:property value="geneTemp.nationality"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="nationality" style="width:152px;height:28px;" onChange="javascript:document.getElementById('nationalityNew').value=document.getElementById('nationality').options[document.getElementById('nationality').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.nationality"/>"><s:property value="sampleGeneTemp.nationality"/></option> 
							<option value="<s:property value="geneTemp.nationality"/>"><s:property value="geneTemp.nationality"/></option>
						</select> 
						<input type="text" name="geneTempNew.nationality" id="nationalityNew" value="<s:property value="geneTempNew.nationality"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                  		
                   	<td class="label-title" >婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleGeneTemp_isInsure" name="sampleGeneTemp.isInsure" value="<s:property value="sampleGeneTemp.isInsure"/>" />
               	 		<input type="hidden" id="geneTemp_isInsure" name="geneTemp.isInsure" value="<s:property value="geneTemp.isInsure"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="geneTempNew.isInsure" id="geneTempNew_isInsure" >
							<option value="" <s:if test="geneTempNew.isInsure==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="geneTempNew.isInsure==1">selected="selected" </s:if>>已婚</option>
    						<option value="0" <s:if test="geneTempNew.isInsure==0">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
              
                   	<td class="label-title">证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_voucherType_name" name="sampleGeneTemp.voucherType.name" value="<s:property value="sampleGeneTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleGeneTemp_voucherType_id"   name="sampleGeneTemp.voucherType.id" value="<s:property value="sampleGeneTemp.voucherType.id"/>"> 
               	 		<input type="hidden" id="geneTemp_voucherType_name" name="geneTemp.voucherType.name" value="<s:property value="geneTemp.voucherType.name"/>" />
 						<input type="hidden" id="geneTemp_voucherType_id"   name="geneTemp.voucherType.id" value="<s:property value="geneTemp.voucherType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="voucherType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherTypeNew').value=document.getElementById('voucherType').options[document.getElementById('voucherType').selectedIndex].value;this.nextSibling.value=this.value;c();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.voucherType.name"/>"><s:property value="sampleGeneTemp.voucherType.name"/></option> 
							<option value="<s:property value="geneTemp.voucherType.name"/>"><s:property value="geneTemp.voucherType.name"/></option>
						</select> 
						<input type="text" name="geneTempNew.voucherType.name" id="voucherTypeNew" onfocus="voucherTypeFun()" value="<s:property value="geneTempNew.voucherType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="geneTempNew.voucherType.id" id="voucherTypeIdNew" value="<s:property value="geneTempNew.voucherType.id"/>">
                   	</td>
                   	<td class="label-title">证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_voucherCode" name="sampleGeneTemp.voucherCode"  value="<s:property value="sampleGeneTemp.voucherCode"/>"/>
  						<input type="hidden" id="geneTemp_voucherCode" name="geneTemp.voucherCode" value="<s:property value="geneTemp.voucherCode"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="voucherCode" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherCodeNew').value=document.getElementById('voucherCode').options[document.getElementById('voucherCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.voucherCode"/>"><s:property value="sampleGeneTemp.voucherCode"/></option> 
							<option value="<s:property value="geneTemp.voucherCode"/>"><s:property value="geneTemp.voucherCode"/></option>
						</select> 
						<input type="text" name="geneTempNew.voucherCode" id="voucherCodeNew" value="<s:property value="geneTempNew.voucherCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
					<td class="label-title">联系电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_phone" name="sampleGeneTemp.phone"  value="<s:property value="sampleGeneTemp.phone"/>"/>
  						<input type="hidden" id="geneTemp_phone" name="geneTemp.phone" value="<s:property value="geneTemp.phone"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phone" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNew').value=document.getElementById('phone').options[document.getElementById('phone').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.phone"/>"><s:property value="sampleGeneTemp.phone"/></option> 
							<option value="<s:property value="geneTemp.phone"/>"><s:property value="geneTemp.phone"/></option>
						</select> 
						<input type="text" name="geneTempNew.phone" id="phoneNew" value="<s:property value="geneTempNew.phone"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_emailAddress" name="sampleGeneTemp.emailAddress"  value="<s:property value="sampleGeneTemp.emailAddress"/>"/>
  						<input type="hidden" id="geneTemp_emailAddress" name="geneTemp.emailAddress" value="<s:property value="geneTemp.emailAddress"/>">
               	 	</td>            	 	
                   	<td align="left">
						<select id="emailAddress" style="width:152px;height:28px;" onChange="javascript:document.getElementById('emailAddressNew').value=document.getElementById('emailAddress').options[document.getElementById('emailAddress').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.emailAddress"/>"><s:property value="sampleGeneTemp.emailAddress"/></option> 
							<option value="<s:property value="geneTemp.emailAddress"/>"><s:property value="geneTemp.emailAddress"/></option>
						</select> 
						<input type="text" name="geneTempNew.emailAddress" id="emailAddressNew" value="<s:property value="geneTempNew.emailAddress"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_address" name="sampleGeneTemp.address"  value="<s:property value="sampleGeneTemp.address"/>"/>
  						<input type="hidden" id="geneTemp_address" name="geneTemp.address" value="<s:property value="geneTemp.address"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="address" style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.address"/>"><s:property value="sampleGeneTemp.address"/></option> 
							<option value="<s:property value="geneTemp.address"/>"><s:property value="geneTemp.address"/></option>
						</select> 
						<input type="text" name="geneTempNew.address" id="addressNew" value="<s:property value="geneTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                   <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人医疗信息</label>
						</div>
					</td>
				  </tr>
                  <tr>
                  		<td class="label-title" >个人疾病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleGeneTemp_bloodHistory" name="sampleGeneTemp.bloodHistory" value="<s:property value="sampleGeneTemp.bloodHistory"/>" />
               	 		<input type="hidden" id="geneTemp_bloodHistory" name="geneTemp.bloodHistory" value="<s:property value="geneTemp.bloodHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="geneTempNew.bloodHistory" id="geneTempNew_bloodHistory" >
							<option value="" <s:if test="geneTempNew.bloodHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="geneTempNew.bloodHistory==1">selected="selected" </s:if>>有</option>
    						<option value="0" <s:if test="geneTempNew.bloodHistory==0">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
<!--                    	<td class="label-title" >备注</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="geneTempNew_note" -->
<!--                    	 		name="geneTempNew.note" title="备注"    -->
<%-- 							value="<s:property value="geneTempNew.note"/>"/> --%>
<!--                    	</td> -->
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_note" name="sampleGeneTemp.note"  value="<s:property value="sampleGeneTemp.note"/>"/>
  						<input type="hidden" id="geneTemp_note" name="geneTemp.note" value="<s:property value="geneTemp.note"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note" style="width:152px;height:28px;" onChange="javascript:document.getElementById('noteNew').value=document.getElementById('note').options[document.getElementById('note').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.note"/>"><s:property value="sampleGeneTemp.note"/></option> 
							<option value="<s:property value="geneTemp.note"/>"><s:property value="geneTemp.note"/></option>
						</select> 
						<input type="text" name="geneTempNew.note" id="noteNew" value="<s:property value="geneTempNew.note"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                  	<td class="label-title" >个人生育史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleGeneTemp_tumorHistory" name="sampleGeneTemp.tumorHistory" value="<s:property value="sampleGeneTemp.tumorHistory"/>" />
               	 		<input type="hidden" id="geneTemp_tumorHistory" name="geneTemp.tumorHistory" value="<s:property value="geneTemp.tumorHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="geneTempNew.tumorHistory" id="geneTempNew_tumorHistory" >
							<option value="" <s:if test="geneTempNew.tumorHistory==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="geneTempNew.tumorHistory==1">selected="selected" </s:if>>有</option>
    						<option value="0" <s:if test="geneTempNew.tumorHistory==0">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_note1" name="sampleGeneTemp.note1"  value="<s:property value="sampleGeneTemp.note1"/>"/>
  						<input type="hidden" id="geneTemp_note1" name="geneTemp.note1" value="<s:property value="geneTemp.note1"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note1" style="width:152px;height:28px;" onChange="javascript:document.getElementById('note1New').value=document.getElementById('note1').options[document.getElementById('note1').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.note1"/>"><s:property value="sampleGeneTemp.note1"/></option> 
							<option value="<s:property value="geneTemp.note1"/>"><s:property value="geneTemp.note1"/></option>
						</select> 
						<input type="text" name="geneTempNew.note1" id="note1New" value="<s:property value="geneTempNew.note1"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  </tr>
                  <tr>
                   	<td class="label-title">烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_cigarette" name="sampleGeneTemp.cigarette"  value="<s:property value="sampleGeneTemp.cigarette"/>"/>
  						<input type="hidden" id="geneTemp_cigarette" name="geneTemp.cigarette" value="<s:property value="geneTemp.cigarette"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="cigarette" style="width:152px;height:28px;" onChange="javascript:document.getElementById('cigaretteNew').value=document.getElementById('cigarette').options[document.getElementById('cigarette').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.cigarette"/>"><s:property value="sampleGeneTemp.cigarette"/></option> 
							<option value="<s:property value="geneTemp.cigarette"/>"><s:property value="geneTemp.cigarette"/></option>
						</select> 
						<input type="text" name="geneTempNew.cigarette" id="cigaretteNew" value="<s:property value="geneTempNew.cigarette"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_wine" name="sampleGeneTemp.wine"  value="<s:property value="sampleGeneTemp.wine"/>"/>
  						<input type="hidden" id="geneTemp_wine" name="geneTemp.wine" value="<s:property value="geneTemp.wine"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="wine" style="width:152px;height:28px;" onChange="javascript:document.getElementById('wineNew').value=document.getElementById('wine').options[document.getElementById('wine').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.wine"/>"><s:property value="sampleGeneTemp.wine"/></option> 
							<option value="<s:property value="geneTemp.wine"/>"><s:property value="geneTemp.wine"/></option>
						</select> 
						<input type="text" name="geneTempNew.wine" id="wineNew" value="<s:property value="geneTempNew.wine"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_medicine" name="sampleGeneTemp.medicine"  value="<s:property value="sampleGeneTemp.medicine"/>"/>
  						<input type="hidden" id="geneTemp_medicine" name="geneTemp.medicine" value="<s:property value="geneTemp.medicine"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="medicine" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicineNew').value=document.getElementById('medicine').options[document.getElementById('medicine').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.medicine"/>"><s:property value="sampleGeneTemp.medicine"/></option> 
							<option value="<s:property value="geneTemp.medicine"/>"><s:property value="geneTemp.medicine"/></option>
						</select> 
						<input type="text" name="geneTempNew.medicine" id="medicineNew" value="<s:property value="geneTempNew.medicine"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                  	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_note2" name="sampleGeneTemp.note2" value="<s:property value="sampleGeneTemp.note2"/>"/>
  						<input type="hidden" id="geneTemp_note2" name="geneTemp.note2" value="<s:property value="geneTemp.note2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('note2New').value=document.getElementById('note2').options[document.getElementById('note2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.note2"/>"><s:property value="sampleGeneTemp.note2"/></option> 
							<option value="<s:property value="geneTemp.note2"/>"><s:property value="geneTemp.note2"/></option>
						</select> 
						<input type="text" name="geneTempNew.note2" id="note2New" value="<s:property value="geneTempNew.note2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_radioactiveRays" name="sampleGeneTemp.radioactiveRays"  value="<s:property value="sampleGeneTemp.radioactiveRays"/>"/>
  						<input type="hidden" id="geneTemp_radioactiveRays" name="geneTemp.radioactiveRays" value="<s:property value="geneTemp.radioactiveRays"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="radioactiveRays" style="width:152px;height:28px;" onChange="javascript:document.getElementById('radioactiveRaysNew').value=document.getElementById('radioactiveRays').options[document.getElementById('radioactiveRays').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.radioactiveRays"/>"><s:property value="sampleGeneTemp.radioactiveRays"/></option> 
							<option value="<s:property value="geneTemp.radioactiveRays"/>"><s:property value="geneTemp.radioactiveRays"/></option>
						</select> 
						<input type="text" name="geneTempNew.radioactiveRays" id="radioactiveRaysNew" value="<s:property value="geneTempNew.radioactiveRays"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_pesticide" name="sampleGeneTemp.pesticide"  value="<s:property value="sampleGeneTemp.pesticide"/>"/>
  						<input type="hidden" id="geneTemp_pesticide" name="geneTemp.pesticide" value="<s:property value="geneTemp.pesticide"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="pesticide" style="width:152px;height:28px;" onChange="javascript:document.getElementById('pesticideNew').value=document.getElementById('pesticide').options[document.getElementById('pesticide').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.pesticide"/>"><s:property value="sampleGeneTemp.pesticide"/></option> 
							<option value="<s:property value="geneTemp.pesticide"/>"><s:property value="geneTemp.pesticide"/></option>
						</select> 
						<input type="text" name="geneTempNew.pesticide" id="pesticideNew" value="<s:property value="geneTempNew.pesticide"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  </tr>
                  <tr>
                   	<td class="label-title">铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_plumbane" name="sampleGeneTemp.plumbane"  value="<s:property value="sampleGeneTemp.plumbane"/>"/>
  						<input type="hidden" id="geneTemp_plumbane" name="geneTemp.plumbane" value="<s:property value="geneTemp.plumbane"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="plumbane" style="width:152px;height:28px;" onChange="javascript:document.getElementById('plumbaneNew').value=document.getElementById('plumbane').options[document.getElementById('plumbane').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.plumbane"/>"><s:property value="sampleGeneTemp.plumbane"/></option> 
							<option value="<s:property value="geneTemp.plumbane"/>"><s:property value="geneTemp.plumbane"/></option>
						</select> 
						<input type="text" name="geneTempNew.plumbane" id="plumbaneNew" value="<s:property value="geneTempNew.plumbane"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_mercury" name="sampleGeneTemp.mercury"  value="<s:property value="sampleGeneTemp.mercury"/>"/>
  						<input type="hidden" id="geneTemp_mercury" name="geneTemp.mercury" value="<s:property value="geneTemp.mercury"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="mercury" style="width:152px;height:28px;" onChange="javascript:document.getElementById('mercuryNew').value=document.getElementById('mercury').options[document.getElementById('mercury').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.mercury"/>"><s:property value="sampleGeneTemp.mercury"/></option> 
							<option value="<s:property value="geneTemp.mercury"/>"><s:property value="geneTemp.mercury"/></option>
						</select> 
						<input type="text" name="geneTempNew.mercury" id="mercuryNew" value="<s:property value="geneTempNew.mercury"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_cadmium" name="sampleGeneTemp.cadmium"  value="<s:property value="sampleGeneTemp.cadmium"/>"/>
  						<input type="hidden" id="geneTemp_cadmium" name="geneTemp.cadmium" value="<s:property value="geneTemp.cadmium"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="cadmium" style="width:152px;height:28px;" onChange="javascript:document.getElementById('cadmiumNew').value=document.getElementById('cadmium').options[document.getElementById('cadmium').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.cadmium"/>"><s:property value="sampleGeneTemp.cadmium"/></option> 
							<option value="<s:property value="geneTemp.cadmium"/>"><s:property value="geneTemp.cadmium"/></option>
						</select> 
						<input type="text" name="geneTempNew.cadmium" id="cadmiumNew" value="<s:property value="geneTempNew.cadmium"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                  	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_note3" name="sampleGeneTemp.note3" value="<s:property value="sampleGeneTemp.note3"/>"/>
  						<input type="hidden" id="geneTemp_note3" name="geneTemp.note3" value="<s:property value="geneTemp.note3"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note3" style="width:152px;height:28px;" onChange="javascript:document.getElementById('note3New').value=document.getElementById('note3').options[document.getElementById('note3').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.note3"/>"><s:property value="sampleGeneTemp.note3"/></option> 
							<option value="<s:property value="geneTemp.note3"/>"><s:property value="geneTemp.note3"/></option>
						</select> 
						<input type="text" name="geneTempNew.note3" id="note3New" value="<s:property value="geneTempNew.note3"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">家族病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_medicalHistory" name="sampleGeneTemp.medicalHistory"  value="<s:property value="sampleGeneTemp.medicalHistory"/>"/>
  						<input type="hidden" id="geneTemp_medicalHistory" name="geneTemp.medicalHistory" value="<s:property value="geneTemp.medicalHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="medicalHistory" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicalHistoryNew').value=document.getElementById('medicalHistory').options[document.getElementById('medicalHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.medicalHistory"/>"><s:property value="sampleGeneTemp.medicalHistory"/></option> 
							<option value="<s:property value="geneTemp.medicalHistory"/>"><s:property value="geneTemp.medicalHistory"/></option>
						</select> 
						<input type="text" name="geneTempNew.medicalHistory" id="medicalHistoryNew" value="<s:property value="geneTempNew.medicalHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">与受检者关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_relationship" name="sampleGeneTemp.relationship"  value="<s:property value="sampleGeneTemp.relationship"/>"/>
  						<input type="hidden" id="geneTemp_relationship" name="geneTemp.relationship" value="<s:property value="geneTemp.relationship"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="relationship" style="width:152px;height:28px;" onChange="javascript:document.getElementById('relationshipNew').value=document.getElementById('relationship').options[document.getElementById('relationship').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.relationship"/>"><s:property value="sampleGeneTemp.relationship"/></option> 
							<option value="<s:property value="geneTemp.relationship"/>"><s:property value="geneTemp.relationship"/></option>
						</select> 
						<input type="text" name="geneTempNew.relationship" id="relationshipNew" value="<s:property value="geneTempNew.relationship"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleGeneTemp_isFee" name="sampleGeneTemp.isFee" value="<s:property value="sampleGeneTemp.isFee"/>" />
               	 		<input type="hidden" id="geneTemp_isFee" name="geneTemp.isFee" value="<s:property value="geneTemp.isFee"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="geneTempNew.isFee" id="geneTempNew_isFee" >
							<option value="" <s:if test="geneTempNew.isFee==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="geneTempNew.isFee==1">selected="selected" </s:if>>是</option>
    						<option value="0" <s:if test="geneTempNew.isFee==0">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>

                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" >
               	 		<input type="hidden" id="sampleGeneTemp_privilegeType" name="sampleGeneTemp.privilegeType" value="<s:property value="sampleGeneTemp.privilegeType"/>" />
               	 		<input type="hidden" id="geneTemp_privilegeType" name="geneTemp.privilegeType" value="<s:property value="geneTemp.privilegeType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left"  >
                   		<select name="geneTempNew.privilegeType" id="geneTempNew_privilegeType" >
							<option value="" <s:if test="geneTempNew.privilegeType==''">selected="selected" </s:if>>请选择</option>
    						<option value="1" <s:if test="geneTempNew.privilegeType==1">selected="selected" </s:if>>是</option>
    						<option value="0" <s:if test="geneTempNew.privilegeType==0">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_linkman_name"   name="sampleGeneTemp.linkman" value="<s:property value="sampleGeneTemp.linkman"/>"> 
<%--                	 		<input type="hidden" id="sampleGeneTemp_linkman_id"   name="sampleGeneTemp.linkman.id" value="<s:property value="sampleGeneTemp.linkman.id"/>">  --%>
               	 		<input type="hidden" id="geneTemp_linkman_name" name="geneTemp.linkman" value="<s:property value="geneTemp.linkman"/>" />
<%--                	 		<input type="hidden" id="geneTemp_linkman_id" name="geneTemp.linkman.id" value="<s:property value="geneTemp.linkman.id"/>" /> --%>
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="linkman" style="width:152px;height:28px;" onChange="javascript:document.getElementById('linkmanNew').value=document.getElementById('linkman').options[document.getElementById('linkman').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.linkman"/>"><s:property value="sampleGeneTemp.linkman"/></option> 
							<option value="<s:property value="geneTemp.linkman"/>"><s:property value="geneTemp.linkman"/></option>
						</select> 
						<input type="text" name="geneTempNew.linkman" id="linkmanNew" value="<s:property value="geneTempNew.linkman"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
<%--                 		<input type="hidden" name="geneTempNew.linkman.id" id="linkmanIdNew" value="<s:property value="geneTempNew.linkman.id"/>"/> --%>
                   	</td>
                   	
                </tr>
                 <tr>
                   	<td class="label-title">是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_isInvoice" name="sampleGeneTemp.isInvoice" value="<s:property value="sampleGeneTemp.isInvoice"/>" />
               	 		<input type="hidden" id="geneTemp_isInvoice" name="geneTemp.isInvoice" value="<s:property value="geneTemp.isInvoice"/>" />
               	 		</td>            	 	
                   	<td align="left">
                   		<select name="geneTempNew.isInvoice" id="geneTempNew_isInvoice">
							<option value="" <s:if test="geneTempNew.isInvoice==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="geneTempNew.isInvoice==0">selected="selected" </s:if>>否</option>
	    					<option value="1" <s:if test="geneTempNew.isInvoice==1">selected="selected" </s:if>>是</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	
                   	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="geneTempNew_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="geneTempNew.createUser" title="录入人"   
							value="<s:property value="geneTempNew.createUser"/>"/>
                   	</td>
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_note4" name="sampleGeneTemp.note4" value="<s:property value="sampleGeneTemp.note4"/>"/>
  						<input type="hidden" id="geneTemp_note4" name="geneTemp.note4" value="<s:property value="geneTemp.note4"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note4" style="width:152px;height:28px;" onChange="javascript:document.getElementById('note4New').value=document.getElementById('note4').options[document.getElementById('note4').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.note4"/>"><s:property value="sampleGeneTemp.note4"/></option> 
							<option value="<s:property value="geneTemp.note4"/>"><s:property value="geneTemp.note4"/></option>
						</select> 
						<input type="text" name="geneTempNew.note4" id="note4New" value="<s:property value="geneTempNew.note4"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
              	</tr>
              	 <tr>
                   	<td class="label-title">金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_money" name="sampleGeneTemp.money"  value="<s:property value="sampleGeneTemp.money"/>"/>
  						<input type="hidden" id="geneTemp_money" name="geneTemp.money" value="<s:property value="geneTemp.money"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="money" style="width:152px;height:28px;" onChange="javascript:document.getElementById('moneyNew').value=document.getElementById('money').options[document.getElementById('money').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.money"/>"><s:property value="sampleGeneTemp.money"/></option> 
							<option value="<s:property value="geneTemp.money"/>"><s:property value="geneTemp.money"/></option>
						</select> 
						<input type="text" name="geneTempNew.money" id="moneyNew" value="<s:property value="geneTempNew.money"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_SP" name="sampleGeneTemp.SP"  value="<s:property value="sampleGeneTemp.SP"/>"/>
  						<input type="hidden" id="geneTemp_SP" name="geneTemp.SP" value="<s:property value="geneTemp.SP"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="SP" style="width:152px;height:28px;" onChange="javascript:document.getElementById('SPNew').value=document.getElementById('SP').options[document.getElementById('SP').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.SP"/>"><s:property value="sampleGeneTemp.SP"/></option> 
							<option value="<s:property value="geneTemp.SP"/>"><s:property value="geneTemp.SP"/></option>
						</select> 
						<input type="text" name="geneTempNew.SP" id="SPNew" value="<s:property value="geneTempNew.SP"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_paymentUnit" name="sampleGeneTemp.paymentUnit"  value="<s:property value="sampleGeneTemp.paymentUnit"/>"/>
  						<input type="hidden" id="geneTemp_paymentUnit" name="geneTemp.paymentUnit" value="<s:property value="geneTemp.paymentUnit"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="paymentUnit" style="width:152px;height:28px;" onChange="javascript:document.getElementById('paymentUnitNew').value=document.getElementById('paymentUnit').options[document.getElementById('paymentUnit').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.paymentUnit"/>"><s:property value="sampleGeneTemp.paymentUnit"/></option> 
							<option value="<s:property value="geneTemp.paymentUnit"/>"><s:property value="geneTemp.paymentUnit"/></option>
						</select> 
						<input type="text" name="geneTempNew.paymentUnit" id="paymentUnitNew" value="<s:property value="geneTempNew.paymentUnit"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                  <tr>
                   	<td class="label-title">核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_reportMan_id" name="sampleGeneTemp.reportMan.id"  value="<s:property value="sampleGeneTemp.reportMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleGeneTemp_reportMan_name" name="sampleGeneTemp.reportMan.name"  value="<s:property value="sampleGeneTemp.reportMan.name"/>" class="text input" />
  						<input type="hidden" id="geneTemp_reportMan_id" name="geneTemp.reportMan.id" value="<s:property value="geneTemp.reportMan.id"/>">
  						<input type="hidden" id="geneTemp_reportMan_name" name="geneTemp.reportMan.name" value="<s:property value="geneTemp.reportMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reportMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportManNew').value=document.getElementById('reportMan').options[document.getElementById('reportMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.reportMan.name"/>"><s:property value="sampleGeneTemp.reportMan.name"/></option> 
							<option value="<s:property value="geneTemp.reportMan.name"/>"><s:property value="geneTemp.reportMan.name"/></option>
						</select> 
						<input type="text" name="geneTempNew.reportMan.name" id="reportManNew" value="<s:property value="geneTempNew.reportMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="geneTempNew.reportMan.id" id="reportManIdNew" value="<s:property value="geneTempNew.reportMan.id"/>"/>
                   	</td>
                   	
                   	<td class="label-title">核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleGeneTemp_auditMan_id" name="sampleGeneTemp.auditMan.id"  value="<s:property value="sampleGeneTemp.auditMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleGeneTemp_auditMan_name" name="sampleGeneTemp.auditMan.name"  value="<s:property value="sampleGeneTemp.auditMan.name"/>" class="text input" />
  						<input type="hidden" id="geneTemp_auditMan_id" name="geneTemp.auditMan.id" value="<s:property value="geneTemp.auditMan.id"/>">
  						<input type="hidden" id="geneTemp_auditMan_name" name="geneTemp.auditMan.name" value="<s:property value="geneTemp.auditMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="auditMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('auditManNew').value=document.getElementById('auditMan').options[document.getElementById('auditMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleGeneTemp.auditMan.name"/>"><s:property value="sampleGeneTemp.auditMan.name"/></option> 
							<option value="<s:property value="geneTemp.auditMan.name"/>"><s:property value="geneTemp.auditMan.name"/></option>
						</select> 
						<input type="text" name="geneTempNew.auditMan.name" id="auditManNew" value="<s:property value="geneTempNew.auditMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="geneTempNew.auditMan.id" id="auditManIdNew" value="<s:property value="geneTempNew.auditMan.id"/>"/>
                   	</td>
                   	
                   	
                   	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="geneTempNew.nextStepFlow" id="geneTempNew_nextStepFlow">
							<option value="" <s:if test="geneTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="geneTempNew.nextStepFlow==1">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="geneTempNew.nextStepFlow==0">selected="selected" </s:if>>反馈项目管理</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  	
                  </tr>
               
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleGene.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
