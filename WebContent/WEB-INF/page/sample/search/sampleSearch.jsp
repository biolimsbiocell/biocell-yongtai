<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<head>
		<title></title>
		<%@ include file="/WEB-INF/page/include/common.jsp"%>
		<style>
			.subTable .tablebtns,.subTable .col-sm-2,.subTable .col-sm-5{
				display: none !important;
            }
            .input-group{
                margin-bottom: 5px;
            }
            .y{
                margin-top: 10px;
            }
            .bg-green{
                background: rgb(0, 166, 90);
            }
			.bg-ccc{
                background: gray;
            }
            .message p {
                font-size: 16px;
				color:  #0073b7;
				
            }
			.flgg{
				font-size: 12px;
				width: auto;
				height: 20px;
				padding: 5px;
				border-radius: 10px;
				margin-left: 15px;
				color: white;
			}
			.flgc{
				color: #fff !important;
			}
			.wc{
				
				background: #0073b7 !important;
			}
			.zz{
				
				background: rgb(0, 166, 90) !important;
			}
			.wwc{
				background: #ccc !important;
			}
			.bhg{
				
				background: red !important;
			}
			
			.schight{
				height: 226px;
				overflow: hidden;
			}
			.y{
				display: none;
			}
			.remrmber{
				margin-top: 21px;
				display: none;
				color: white;
				background: rgb(0, 192, 239);
			}
			.remrmberimg{
				width: 50px;
				float: left;
				line-height: 40px;
			}
			.remrmberimg img{
				margin: 5px;
			}
			.remrmbertxt{
				padding-left: 50px;
				line-height: 40px;
				color: white;
			}
			.showTd .glyphicon-star{
				display: inline-block !important;
                color: red !important; 
                animation: flash .8s linear infinite;
			}
			.zzz div{
				float: left;
				margin-right: 20px;
				margin-bottom: 6px;
			}
			/* .showTd .glyphicon-star {
                display: inline-block !important;
                color: red !important; 
                animation: flash .8s linear infinite;
            } */
		</style>
	</head>

	<body>
		<!--toolbar按钮组-->
		<div>
			<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
		</div>
		<div>
			<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
		</div>
		<div style="height: 14px"></div>
		<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
		<div class="container-fluid" style="margin-top: 46px">
			<!--Form表单-->
			<div class="col-xs-12" style="padding: 0px">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
						样本信息
					</h3>

						<div class="box-tools pull-right" style="display: none;">
							<button type="button" class="btn btn-box-tool" id="tableRefresh" onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Action <span class="caret"></span>
							</button>
								<ul class="dropdown-menu">
									<li>
										<a href="####" onclick="$('.buttons-print').click();">
											<fmt:message key="biolims.common.print" />
										</a>
									</li>
									<li>
										<a href="#####" onclick="$('.buttons-copy').click();">
											<fmt:message key="biolims.common.copyData" />
										</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-excel').click();">Excel</a>
									</li>
									<li>
										<a href="####" onclick="$('.buttons-csv').click();">CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li>
										<a href="####" onclick="itemFixedCol(2)">
											<fmt:message key="biolims.common.lock2Col" />
										</a>
									</li>
									<li>
										<a href="####" id="unfixde" onclick="unfixde()">
											<fmt:message key="biolims.common.cancellock2Col" />
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="box-body ipadmini">
						<!--进度条-->
						
						<!--form表单-->
						<div class="HideShowPanel" style="position: relative">
								<div class="remrmber" style="height: 40px;width: 150px; position: absolute;right: 18px;">
									<div class="remrmberimg">
										<img src="../../../../images/jlwc2.png" alt="">
									</div>
									<div class="remrmbertxt">
										订单录入完成
									</div>
								</div>
							<form name="form1" class="layui-form" id="form1" method="post">
								<input type="hidden" id="bpmTaskId" value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
								<input type="hidden" size="20" maxlength="25" readonly="readonly" id="sampleInfo_sampleStyle" name="sampleInfo.sampleStyle" class="text input readonlytrue" value="<s:property value=" sampleInfo.sampleStyle "/>" />
								<input type="hidden" size="20" maxlength="25" readonly="readonly" id="sampleInfo_reportDate" name="sampleInfo.reportDate" class="text input readonlytrue" value="<s:property value=" sampleInfo.reportDate "/>" />
								<input type="hidden" size="20" maxlength="25" Class="Wdate" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd'})" id="sampleInfo_sendReportDate" name="sampleInfo.sendReportDate" value="<s:date name = " sampleInfo.sendReportDate "  format="yyyy-MM-dd "/>" />
								<input type=hidden id="sampleInfo_sampleType_id" name="sampleInfo.sampleType.id" value="<s:property value=" sampleInfo.sampleType.id "/>">
								<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="<fmt:message key=" biolims.common.sampleCode "/>" value="<s:property value=" sampleInfo.id " />"/>
								<input type="hidden" id="pch" value="${requestScope.searchnum}">
								<div style="width: 280px; margin: 0 auto; position: relative;" class="clearfix">
									<input type="text" size="30" maxlength="127" id="sampleInfo_samplecodee" style="margin-bottom:10px; height: 40px; float: left;" style="height: 40px;"    placeholder="请扫描细胞编号" />
									<%-- <span class="input-group-btn" style="float: left">
										<button class="btn btn-info ssearch1" type="button" onclick="qingkong();" style="height: 40px;">
											清空
										</button>
									</span> --%>
									<span class="input-group-btn" style="float: left">
										<button class="btn btn-info ssearch" type="button" style="height: 40px;">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
									<input type="text" size="30" maxlength="127" id="sampleInfo_codee" style="margin-bottom:10px; height: 40px; float: left;" style="height: 40px;"  placeholder="对应批号" /><!-- placeholder="请扫描产批次号" -->
									
								</div>
									<!-- 申请单信息 -->
									<div class="shenqing y" style="padding: 5px;border:1px solid #ccc">
											<div class="message clearfix" style="margin-bottom:10px;">
												<p style="float: left" style="font-size: 16px;">申请单信息</p>
												<div class="sqbutton" style="float: right">
													<button class="btn btn-info" type="button" onclick="showCrmPatient();">
														电子病历
													   </button>
													   <button class="btn btn-info" type="button" onclick="showSampleOrder();">
														 申请单详情
													   </button>
												</div>

											</div>
											<div class="row">
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">产品批号</span>
																<input type="text" size="50" maxlength="127" id="barcode" name="sampleOrder-id" changelog="<s:property value=" sampleInfo.sampleOrder.id "/>"  class="form-control readonlytrue" readonly="readOnly" value="<s:property value=" sampleInfo.sampleOrder.id "/>" />
																<input type="hidden" size="50" maxlength="127" id="sampleOrder"   class="form-control readonlytrue" readonly="readOnly"  />
                												<input type="hidden" size="50" maxlength="127" id="crmPatient"   class="form-control readonlytrue" readonly="readOnly"  />
															</div>
													</div>
													
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">姓名</span>
																<input type="text" id="name" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.name "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">采血轮次</span>
																<input type="text"  size="50" maxlength="127" id="round" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.round "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">产品名称</span>
																<input type="text" id="productName" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.productName "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">CCOI</span>
																<input type="text" id="ccoi" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">医院</span>
																<input type="text" size="50" id="crmCustomername" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">科室</span>
																<input type="text" size="50" id="inspectionDepartment" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">白细胞计数</span>
																<input type="text" id="whiteBloodCellNum" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">淋巴细胞百分比</span>
																<input type="text" id="percentageOfLymphocytes" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">预计回输开始日期</span>
																<input type="text" id="startBackTime" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">预计回输结束日期</span>
																<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
													</div>
											</div>
									</div>
                                    <!-- 申请单信息 结束 -->
                                    
                                    <!-- 样本接收记录 -->
                                    <div class="jeishou y" style="padding: 5px;border:1px solid #ccc">
											<div class="message clearfix" style="margin-bottom:10px;">
												<p style="float: left" style="font-size: 16px;">样本接收记录</p>
												<div class="sqbutton" style="float: right">
														<button class="btn btn-info" type="button" onclick="showSampleReceive();">
															 接收详情
														</button>
												</div>

											</div>
											<div class="row">
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">接收人</span>
																<input type="text" size="50" maxlength="127" id="acceptUsername" name="sampleOrder-id" changelog="<s:property value=" sampleInfo.sampleOrder.id "/>"  class="form-control readonlytrue" readonly="readOnly" value="<s:property value=" sampleInfo.sampleOrder.id "/>" />
																<input type="hidden" size="50" maxlength="127" id="sampleReceive"   class="form-control readonlytrue" readonly="readOnly"  />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">接受日期  </span>
																<input type="text" size="50" maxlength="127" id="acceptDate"  readonly="readOnly" class="form-control readonlytrue" value="<s:property value=" sampleInfo.sampleOrder.barcode" />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">运送设备编号</span>
																<input type="text" id="equimentNumber" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.name "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">运送设备温度</span>
																<input type="text" id="temperature" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.round "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">快递公司</span>
																<input type="text" id="expressCompanyname" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.productName "  />" />
															</div>
													</div>
													<div class="col-xs-4">
															<div class="input-group">
																<span class="input-group-addon">快递员</span>
																<input type="text" size="50" id="courier" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
															</div>
                                                    </div>
											</div>
                                    </div>
                                    <!-- 样本接收记录 -->
								<!-- <div class="row">
									
									
									
									
									
								</div> -->
								<!-- <div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">性别</span>
											<input type="text" size="50" maxlength="127" class="form-control"  
											<c:if test='${ sampleInfo.sampleOrder.gender eq "1"}'>value="男"</c:if> 
											<c:if test='${ sampleInfo.sampleOrder.gender eq "0"}'>value="女"</c:if>
											<c:if test='${ sampleInfo.sampleOrder.gender eq "3"}'>value="未知"</c:if>  />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">年龄</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.age "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">电子病历号</span>
											<input type="text" size="50" maxlength="127" id="medicalNumber" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.medicalNumber "  />" />
											<span class="input-group-btn">
												<button class="btn btn-info" type="button"  onclick="showMedicalNumberInfo()">
													<i class="glyphicon glyphicon-search"></i>
												</button>
											</span>
										</div>
									</div> -->
								<!-- </div>
								<div class="row">
									
									
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">筛选号</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.filtrateCode "  />" />
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">随机号</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.randomCode "  />" />
										</div>
									</div>
									
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">医疗机构</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.crmCustomer.name "  />" />
										</div>
									</div>	
								</div>
								<div class="row">
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">预计采血时间</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:date name=" sampleInfo.sampleOrder.drawBloodTime " format="yyyy-MM-dd HH:mm" />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">审核人</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.confirmUser.name "  />" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="input-group">
											<span class="input-group-addon">审核时间</span>
											<input type="text" size="50" maxlength="127" class="form-control" value="<s:date name=" sampleInfo.sampleOrder.confirmDate " format="yyyy-MM-dd" />"/>
										</div>
									</div>	
								</div> -->
								<!-- 
                                	作者：offline
                                	时间：2018-03-19
                                	描述：自定义字段
                                -->
								<!-- <div id="fieldItemDiv"></div>  -->

								<br>
								<input type="hidden" name="storageReagentBuySerialJson" id="storageReagentBuySerialJson" value="" />

								<input type="hidden" id="id_parent_hidden" value="<s:property value=" sampleInfo.id "/>" />
                            </form>·
                            <!-- 生产记录 -->
                            <div class="scjl y schight" style="padding: 5px;border:1px solid #ccc">
                                    <div class="message clearfix" style="margin-bottom:10px;">
                                            <p style="float: left" style="font-size: 16px;">生产记录 <span class="wc flgg">完成</span><span class="zz flgg">正在进行</span><span class="wwc flgg">未完成</span><span class="bhg  flgg">不合格</span></p>
                                            <div class="sqbutton" style="float: right">
                                                    
													   <button class="viewSubTable btn btn-warning btn-xs"><i class="glyphicon jiajian glyphicon-plus"></i></button>
                                            </div>

                                        </div>
                                    <ul class="timeline">
                                            <li class="time-label" id="parentWrap">
                                                <span class="bg-red"> </span>
                                            </li>
                                            <script type="text/html" id="templatee">
                                                {{each list}}
                                                <li>
                                                        {{if $value.state==0}}
                                                        <i class="fa wc flgc">{{$value.orderNum}}</i>
                                                        {{else if $value.state==1}}
														<i class="fa bhg flgc">{{$value.orderNum}}</i>
														{{else if $value.state==2}}
														<i class="fa zz flgc">{{$value.orderNum}}</i>
														{{else }}
                                                        <i class="fa wwc flgc">{{$value.orderNum}}</i>
                                                        {{/if}}
                                                    <div class="timeline-item">
                                                        <span class="time"><i class="fa fa-clock-o"></i></span>
    
                                                        <h3 class="timeline-header">
															<a href="####">{{$value.mc}}</a>
															
															{{if $value.unstar=='1'}}
															<span class="glyphicon glyphicon-star text-red" style="display:none"></span>
															{{/if}}
														</h3>
    
                                                        <div class="timeline-body table-responsive">
                                                            <table class="table table-hover" style="margin-bottom: 0;">
                                                                <tr class="row">
                                                                    <td class="col-md-3">操作员 ：{{$value.templeOperatorName}}</td>
                                                                    
                                                                    <td class="col-md-3">房间: {{$value.operatingRoomName}}</td>
                                                                    
                                                                    <td class="col-md-3">预计用时 : {{$value.estimatedDate}} 天</td>
                                                                    
                                                                    <td class="col-md-3">预计操作日期 : {{$value.planWorkDate}} </td>
                                                                </tr>
                                                                <tr class="row">
                                                                    <td class="col-md-3">操作开始时间 : {{$value.productStartTime}} </td>
                                                                    <td class="col-md-3">操作结束时间: {{$value.productEndTime}} </td>
																	<td class="col-md-3"><button class="btn btn-info" mc={{$value.mc}} cid={{$value.cid}} ordernum={{$value.orderNum}} type="button" onclick="printTempalte(this);" >
																		查看批记录
																   </button></td>
																	<td class="col-md-3"><button class="btn btn-info"  mc={{$value.mc}} cid={{$value.cid}} ordernum={{$value.orderNum}}  type="button" onclick="zhijianjieguo(this);" >
																		查看质检结果
																   </button></td>
																</tr>
																
                                                            </table>
                                                        </div>
                                                    </div>
                                                </li>
    
                                                {{/each}}
                                            </script>
                                            <li>
                                                <i class="fa fa-hourglass-3  bg-gray"></i>
                                            </li>
                                    </ul>
                            </div>
                                
                        <!-- 生产记录 -->
                        <!-- 放行审核 -->
                        <div class="jeishou y" style="padding: 5px;padding-left:20px;border:1px solid #ccc">
                                <div class="message clearfix" style="margin-bottom:10px;">
                                    <p style="float: left" style="font-size: 16px;">放行审核</p>
                                    <div class="sqbutton" style="float: right">
                                            
                                    </div>

                                </div>
                                <div class="row zzz">
								<div class="uuu" style="">
									<button id="cpbgdy" class="btn btn-info" type="button"
										onclick="showcpjyy(this);">成品检验报告单（一）</button>
								</div>
								<div class="">
									<button id="cpbgde" class="btn btn-info" type="button"
										onclick="showcpjye(this);">成品检验报告单（二）</button>
								</div>
								<div class="">
									<button id="zjcpjy" class="btn btn-info" type="button"
										onclick="showzjcpjy(this);">中间产品检验报告单</button>
								</div>
								<div class="">
									<button id="tzbtn" class="btn btn-info" type="button"
										onclick="showfxtz(this);">成品放行通知单</button>
								</div>
								<div class="uuu">
									<button id="shbtn" class="btn btn-info" type="button"
										onclick="showfxsh(this);">成品放行审核单</button>
								</div>
							</div>
                        </div>
                        <!-- 运输安排 -->
                        <div class="jeishou y" style="padding: 5px;border:1px solid #ccc">
                                <div class="message clearfix" style="margin-bottom:10px;">
                                    <p style="float: left" style="font-size: 16px;">运输安排</p>
                                    <div class="sqbutton" style="float: right">
                                            
                                    </div>

                                </div>
                                <div class="row">
                                        <div class="col-xs-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">是否发出</span>
                                                    <input type="text" size="50" id="transportState" maxlength="127" name="sampleOrder-id" changelog="<s:property value=" sampleInfo.sampleOrder.id "/>"  class="form-control readonlytrue" readonly="readOnly" value="<s:property value=" sampleInfo.sampleOrder.id "/>" />
                                                    
                                                </div>
                                        </div>
                                        <div class="col-xs-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">运输单号  </span>
                                                    <input type="text" size="50" maxlength="127" id="transportOrderid"  readonly="readOnly" class="form-control readonlytrue" value="<s:property value=" sampleInfo.sampleOrder.barcode" />" />
                                                </div>
                                        </div>
                                        <div class="col-xs-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">医院</span>
                                                    <input type="text" id="expresscrmCustomer" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.name "  />" />
                                                </div>
                                        </div>
                                        <div class="col-xs-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">出发时间</span>
                                                    <input type="text" size="50" id="transportDate" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.round "  />" />
                                                </div>
                                        </div>
                                        <div class="col-xs-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">预计到达时间</span>
                                                    <input type="text" id="ytransportDate" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.productName "  />" />
                                                </div>
                                        </div>
                                        <div class="col-xs-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">签收时间</span>
                                                    <input type="text" id="signConfirmationDate" size="50" maxlength="127" class="form-control" value="<s:property value=" sampleInfo.sampleOrder.ccoi "  />" />
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <!-- 偏差不合格 -->
						<!-- 我的实验！！！！！！！！！！！！！！！！！！！！1 -->
						<div class="jeishou y" style="padding: 5px;border:1px solid #ccc">
                                <div class="message clearfix" style="margin-bottom:10px;">
                                    <p style="float: left" style="font-size: 16px;">偏差</p>
                                   

								</div>
								<div id="my"></div>
								<script type="text/html" id="templateee">
									
									{{each list}}
								
									<div class="row">
											<div class="col-xs-4">
													<div class="input-group">
														<span class="input-group-addon">是否存在偏差</span>
														<input type="text" size="50" maxlength="127"  class="form-control readonlytrue" readonly="readOnly" value='存在变差' />
														
													</div>
											</div>
											<div class="col-xs-4">
													<div class="input-group">
														<span class="input-group-addon">偏差编号  </span>
														<input type="text" size="50" maxlength="127"   readonly="readOnly" class="form-control readonlytrue" value="{{$value.no}}" />
													</div>
											</div>
											<div class="col-xs-4">
													<div class="input-group">
														<span class="input-group-addon">偏差描述</span>
														<input type="text" size="50" maxlength="127" class="form-control" value="{{$value.note}}" />
													</div>
											</div>
											<div class="col-xs-4">
													<div class="input-group">
														<span class="input-group-addon">偏差类型</span>
														<input type="text" size="50" maxlength="127" class="form-control" value="{{$value.type}}" />
													</div>
											</div>
											<div class="col-xs-4">
													<div class="input-group">
														<span class="input-group-addon">发生日期</span>
														<input type="text" id="pcdata" size="50" maxlength="127" class="form-control" value="{{$value.fssj}}"  />
													</div>
											</div>
											<div class="col-xs-4">
													<div class="input-group">
														<span class="input-group-addon">发现人</span>
														<input type="text" size="50" maxlength="127" class="form-control" value="{{$value.fxr}}" />
													</div>
											</div>
											<div class="col-xs-4">
													<div class="input-group">
														<span class="input-group-addon">偏差处理进度</span>
														<input type="text" size="50" maxlength="127" class="form-control" value="{{$value.cljd}}" />
													</div>
											</div>
											<div class="col-xs-4">
													<div class="input-group">
															<button class="btn btn-info" type="button" pid={{$value.id}} onclick="showpccl(this);" >
																	偏差处理明细
															</button>
													</div>
											</div>
									</div>
									{{/each}}
								</script>
								<!-- <div class="col-xs-4">
													<div class="input-group">
															<button class="btn btn-info" type="button" pid={{$value.id}}>
																	偏差处理报告
															</button>
													</div>
											</div> -->
						</div>
						<!-- end------------------------ -->
						</div>
						<!-- <div class="HideShowPanel" style="display: none;">
							<div class="box box-info">
							<div class="box-header">
								<h3 class="box-title">样本状态预览</h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
									<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
								</div>
							</div>
							 /.box-header
							<div class="box-body ipadmini">
							

							</div>
						</div>

						</div> -->
						<!--table表格-->
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="sampleStateGrid" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="sampleInItemInfoGrid" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="sampleReceiveShow" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="cellPassageItemShow" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="cellPassageResultShow" style="font-size: 14px;">
							</table>
						</div>
						<div class="HideShowPanel" style="display: none;">
							<table class="table table-hover table-striped table-bordered table-condensed" id="qualityTestShow" style="font-size: 14px;">
							</table>
						</div>
					</div>
					<!-- <div class="box-footer">
						<div class="pull-right">
							<button type="button" class="btn btn-primary" id="pre">上一步</button>
							<button type="button" class="btn btn-primary" id="next">下一步</button>
						</div>
					</div> -->
				</div>
			</div>
		</div>
		<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
		<script src="${ctx}/lims/plugins/template/template.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleMainEdit.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleSearchSearch.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleState.js"></script>
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleInItem.js"></script>
		<!-- 样本接收子表 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/sampleReceiveShow.js"></script>
		<!-- 细胞生产明细 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/cellPassageItemShow.js"></script>
		<!-- 细胞生产结果 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/cellPassageResultShow.js"></script>
		<!-- 质检内容 -->
		<script type="text/javascript" src="${ctx}/js/system/sample/qualityTestShow.js"></script>
	</body>
</html>