
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title='<fmt:message key="biolims.common.attachment"/>'
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBloodDiseaseTemp&id=${sampleBloodDiseaseTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleBloodDiseaseTempEdit.js"></script>
	<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBloodDiseaseTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<tr>
				<td class="label-title" style="color: red"><fmt:message key="biolims.common.clickToUploadPictures"/></td>
				<td></td>
				<td>
  					 <input type="button" value="<fmt:message key="biolims.common.uploadTheInformationRecordedImages"/>" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
               	 	<td class="label-title" style="display:none"><fmt:message key="biolims.common.serialNumber"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_id" name="sampleBloodDiseaseTemp.id" title="编号" value="<s:property value="sampleBloodDiseaseTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBloodDiseaseTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   		<input type ="hidden" id="saveType" value="xyb" >
                   	</td>

					<td class="label-title"><fmt:message key="biolims.common.sampleCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_code" class="text input readonlytrue" readonly="readonly" name="bloodDiseaseTempNew.sampleInfo.code" title="样本编号" value="<s:property value="bloodDiseaseTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_code" class="text input readonlytrue" readonly="readonly" name="bloodDiseaseTempNew.code" title="样本编号" value="<s:property value="bloodDiseaseTempNew.code"/>" />
                   	</td>

                   	<td class="label-title"><fmt:message key="biolims.common.describe"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="bloodDiseaseTempNew_name" name="bloodDiseaseTempNew.name" title="描述" value="<s:property value="bloodDiseaseTempNew.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_name" name="sampleBloodDiseaseTemp.name" title="描述" value="<s:property value="sampleBloodDiseaseTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_name" name="bloodDiseaseTemp.name" title="描述" value="<s:property value="bloodDiseaseTemp.name"/>"/>
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.detectionItems"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_productId" name="sampleBloodDiseaseTemp.productId"  value="<s:property value="sampleBloodDiseaseTemp.productId"/>"/>
  						<input type="hidden" id="sampleBloodDiseaseTemp_productName" name="sampleBloodDiseaseTemp.productName" value="<s:property value="sampleBloodDiseaseTemp.productName"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_productId" name="bloodDiseaseTemp.productId" value="<s:property value="bloodDiseaseTemp.productId"/>">
  						<input type="hidden" id="bloodDiseaseTemp_productName" name="bloodDiseaseTemp.productName" value="<s:property value="bloodDiseaseTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.productName"/>"><s:property value="sampleBloodDiseaseTemp.productName"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.productName"/>"><s:property value="bloodDiseaseTemp.productName"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="bloodDiseaseTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="bloodDiseaseTempNew.productId" id="productIdNew" value="<s:property value="bloodDiseaseTempNew.productId"/>" />
                   	</td>
                   	
                  </tr>
                <tr>   	
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.receivingDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_acceptDate" name="sampleBloodDiseaseTemp.acceptDate" value="<s:property value="sampleBloodDiseaseTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_acceptDate" name="bloodDiseaseTemp.acceptDate" value="<s:property value="bloodDiseaseTemp.acceptDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.acceptDate"/>"><s:property value="sampleBloodDiseaseTemp.acceptDate"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.acceptDate"/>"><s:property value="bloodDiseaseTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.acceptDate" id="acceptDateNew" value="<s:property value="bloodDiseaseTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="bloodDiseaseTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.region"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_area" name="sampleBloodDiseaseTemp.area"  value="<s:property value="sampleBloodDiseaseTemp.area"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_area" name="bloodDiseaseTemp.area" value="<s:property value="bloodDiseaseTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.area"/>"><s:property value="sampleBloodDiseaseTemp.area"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.area"/>"><s:property value="bloodDiseaseTemp.area"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.area" id="areaNew" value="<s:property value="bloodDiseaseTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                	
                	<td class="label-title"><fmt:message key="biolims.common.inspectionHospital"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_hospital" name="sampleBloodDiseaseTemp.hospital"  value="<s:property value="sampleBloodDiseaseTemp.hospital"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_hospital" name="bloodDiseaseTemp.hospital" value="<s:property value="bloodDiseaseTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital" style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.hospital"/>"><s:property value="sampleBloodDiseaseTemp.hospital"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.hospital"/>"><s:property value="bloodDiseaseTemp.hospital"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.hospital" id="hospitalNew" value="<s:property value="bloodDiseaseTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                	   	
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.inspectionDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_sendDate" name="sampleBloodDiseaseTemp.sendDate" value="<s:property value="sampleBloodDiseaseTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_sendDate" name="bloodDiseaseTemp.sendDate" value="<s:property value="bloodDiseaseTemp.sendDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.sendDate"/>"><s:property value="sampleBloodDiseaseTemp.sendDate"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.sendDate"/>"><s:property value="bloodDiseaseTemp.sendDate"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.sendDate" id="sendDateNew" value="<s:property value="bloodDiseaseTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})" value="<s:date name="bloodDiseaseTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
					<td class="label-title"><fmt:message key="biolims.common.shouldTheReportDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_reportDate" name="sampleBloodDiseaseTemp.reportDate" value="<s:property value="sampleBloodDiseaseTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_reportDate" name="bloodDiseaseTemp.reportDate" value="<s:property value="bloodDiseaseTemp.reportDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.reportDate"/>"><s:property value="sampleBloodDiseaseTemp.reportDate"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.reportDate"/>"><s:property value="bloodDiseaseTemp.reportDate"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.reportDate" id="reportDateNew" value="<s:property value="bloodDiseaseTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="bloodDiseaseTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>                   	
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.sampleType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_sampleType_name" name="sampleBloodDiseaseTemp.sampleType.name"  value="<s:property value="sampleBloodDiseaseTemp.sampleType.name"/>"/>
  						<input type="hidden" id="sampleBloodDiseaseTemp_sampleType_id" name="sampleBloodDiseaseTemp.sampleType.id" value="<s:property value="sampleBloodDiseaseTemp.sampleType.id"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_sampleType_name" name="bloodDiseaseTemp.sampleType.name" value="<s:property value="bloodDiseaseTemp.sampleType.name"/>">
  						<input type="hidden" id="bloodDiseaseTemp_sampleType_id" name="bloodDiseaseTemp.sampleType.id" value="<s:property value="bloodDiseaseTemp.sampleType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.sampleType.name"/>"><s:property value="sampleBloodDiseaseTemp.sampleType.name"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.sampleType.name"/>"><s:property value="bloodDiseaseTemp.sampleType.name"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="bloodDiseaseTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="bloodDiseaseTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="bloodDiseaseTempNew.sampleType.id"/>" />
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.number"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_confirmAgeOne" name="sampleBloodDiseaseTemp.confirmAgeOne"  value="<s:property value="sampleBloodDiseaseTemp.confirmAgeOne"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_confirmAgeOne" name="bloodDiseaseTemp.confirmAgeOne" value="<s:property value="bloodDiseaseTemp.confirmAgeOne"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAgeOne" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeOneNew').value=document.getElementById('confirmAgeOne').options[document.getElementById('confirmAgeOne').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.confirmAgeOne"/>"><s:property value="sampleBloodDiseaseTemp.confirmAgeOne"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.confirmAgeOne"/>"><s:property value="bloodDiseaseTemp.confirmAgeOne"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.confirmAgeOne" id="confirmAgeOneNew" value="<s:property value="bloodDiseaseTempNew.confirmAgeOne"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.inspectionDoctor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_doctor" name="sampleBloodDiseaseTemp.doctor"  value="<s:property value="sampleBloodDiseaseTemp.doctor"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_doctor" name="bloodDiseaseTemp.doctor" value="<s:property value="bloodDiseaseTemp.doctor"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="doctor" style="width:152px;height:28px;" onChange="javascript:document.getElementById('doctorNew').value=document.getElementById('doctor').options[document.getElementById('doctor').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.doctor"/>"><s:property value="sampleBloodDiseaseTemp.doctor"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.doctor"/>"><s:property value="bloodDiseaseTemp.doctor"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.doctor" id="doctorNew" value="<s:property value="bloodDiseaseTempNew.doctor"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.doctorContact"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_phone" name="sampleBloodDiseaseTemp.phone"  value="<s:property value="sampleBloodDiseaseTemp.phone"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_phone" name="bloodDiseaseTemp.phone" value="<s:property value="bloodDiseaseTemp.phone"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phone" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNew').value=document.getElementById('phone').options[document.getElementById('phone').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.phone"/>"><s:property value="sampleBloodDiseaseTemp.phone"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.phone"/>"><s:property value="bloodDiseaseTemp.phone"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.phone" id="phoneNew" value="<s:property value="bloodDiseaseTempNew.phone"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                </tr>
                <tr>
<!--                 	<td class="label-title">样本编号</td> -->
<%--                     <td class="requiredcolunm" nowrap width="10px" > </td> --%>
<!--                     <td align="left"> -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_sampleNum"  -->
<%--                    			name="sampleBloodDiseaseTemp.sampleNum" title="样本编号" value="<s:property value="sampleBloodDiseaseTemp.sampleNum"/>" --%>
<!--                    		/> -->
<!--                    </td> -->
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.patientInformation"/></label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title"><fmt:message key="biolims.common.patientName"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_patientName" name="sampleBloodDiseaseTemp.patientName"  value="<s:property value="sampleBloodDiseaseTemp.patientName"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_patientName" name="bloodDiseaseTemp.patientName" value="<s:property value="bloodDiseaseTemp.patientName"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameNew').value=document.getElementById('patientName').options[document.getElementById('patientName').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.patientName"/>"><s:property value="sampleBloodDiseaseTemp.patientName"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.patientName"/>"><s:property value="bloodDiseaseTemp.patientName"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.patientName" id="patientNameNew" value="<s:property value="bloodDiseaseTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
<!--                    	<td class="label-title" >姓名拼音</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleBloodDiseaseTemp_patientNameSpell" -->
<!--                    	 		name="sampleBloodDiseaseTemp.patientNameSpell" title="姓名拼音"    -->
<%-- 							value="<s:property value="sampleBloodDiseaseTemp.patientNameSpell"/>" --%>
<!--                    	 	/> -->
<!--                    	</td> -->
               		
                   	<td class="label-title"><fmt:message key="biolims.common.gender"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_gender" name="sampleBloodDiseaseTemp.gender" value="<s:property value="sampleBloodDiseaseTemp.gender"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_gender" name="bloodDiseaseTemp.gender"  value="<s:property value="bloodDiseaseTemp.gender"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="bloodDiseaseTempNew.gender" id="bloodDiseaseTempNew_gender">
							<option value="" <s:if test="bloodDiseaseTempNew.gender==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="0" <s:if test="bloodDiseaseTempNew.gender==0">selected="selected" </s:if>><fmt:message key="biolims.common.female"/></option>
	    					<option value="1" <s:if test="bloodDiseaseTempNew.gender==1">selected="selected" </s:if>><fmt:message key="biolims.common.male"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.age"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_age" name="sampleBloodDiseaseTemp.age"  value="<s:property value="sampleBloodDiseaseTemp.age"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_age" name="bloodDiseaseTemp.age" value="<s:property value="bloodDiseaseTemp.age"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="age" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.age"/>"><s:property value="sampleBloodDiseaseTemp.age"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.age"/>"><s:property value="bloodDiseaseTemp.age"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.age" id="ageNew" value="<s:property value="bloodDiseaseTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.papersType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_voucherType_name" name="sampleBloodDiseaseTemp.voucherType.name"  value="<s:property value="sampleBloodDiseaseTemp.voucherType.name"/>"/>
  						<input type="hidden" id="sampleBloodDiseaseTemp_voucherType_id" name="sampleBloodDiseaseTemp.voucherType.id" value="<s:property value="sampleBloodDiseaseTemp.voucherType.id"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_voucherType_name" name="bloodDiseaseTemp.voucherType.name" value="<s:property value="bloodDiseaseTemp.voucherType.name"/>">
  						<input type="hidden" id="bloodDiseaseTemp_voucherType_id" name="bloodDiseaseTemp.voucherType.id" value="<s:property value="bloodDiseaseTemp.voucherType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="voucherType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherTypeNew').value=document.getElementById('voucherType').options[document.getElementById('voucherType').selectedIndex].value;this.nextSibling.value=this.value;c();"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.voucherType.name"/>"><s:property value="sampleBloodDiseaseTemp.voucherType.name"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.voucherType.name"/>"><s:property value="bloodDiseaseTemp.voucherType.name"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.voucherType.name" id="voucherTypeNew" onfocus="voucherTypeFun()" value="<s:property value="bloodDiseaseTempNew.voucherType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="bloodDiseaseTempNew.voucherType.id" id="voucherTypeIdNew" value="<s:property value="bloodDiseaseTempNew.voucherType.id"/>" />
                   	</td>
                     
                   	<td class="label-title"><fmt:message key="biolims.common.papersCode"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_voucherCode" name="sampleBloodDiseaseTemp.voucherCode"  value="<s:property value="sampleBloodDiseaseTemp.voucherCode"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_voucherCode" name="bloodDiseaseTemp.voucherCode" value="<s:property value="bloodDiseaseTemp.voucherCode"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="voucherCode" style="width:152px;height:28px;" onChange="javascript:document.getElementById('voucherCodeNew').value=document.getElementById('voucherCode').options[document.getElementById('voucherCode').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.voucherCode"/>"><s:property value="sampleBloodDiseaseTemp.voucherCode"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.voucherCode"/>"><s:property value="bloodDiseaseTemp.voucherCode"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.voucherCode" id="voucherCodeNew" value="<s:property value="bloodDiseaseTempNew.voucherCode"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.contact"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_phoneNum2" name="sampleBloodDiseaseTemp.phoneNum2"  value="<s:property value="sampleBloodDiseaseTemp.phoneNum2"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_phoneNum2" name="bloodDiseaseTemp.phoneNum2" value="<s:property value="bloodDiseaseTemp.phoneNum2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phoneNum2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNum2New').value=document.getElementById('phoneNum2').options[document.getElementById('phoneNum2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.phoneNum2"/>"><s:property value="sampleBloodDiseaseTemp.phoneNum2"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.phoneNum2"/>"><s:property value="bloodDiseaseTemp.phoneNum2"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.phoneNum2" id="phoneNum2New" value="<s:property value="bloodDiseaseTempNew.phoneNum2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.eamil"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_emailAddress" name="sampleBloodDiseaseTemp.emailAddress"  value="<s:property value="sampleBloodDiseaseTemp.emailAddress"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_emailAddress" name="bloodDiseaseTemp.emailAddress" value="<s:property value="bloodDiseaseTemp.emailAddress"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="emailAddress" style="width:152px;height:28px;" onChange="javascript:document.getElementById('emailAddressNew').value=document.getElementById('emailAddress').options[document.getElementById('emailAddress').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.emailAddress"/>"><s:property value="sampleBloodDiseaseTemp.emailAddress"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.emailAddress"/>"><s:property value="bloodDiseaseTemp.emailAddress"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.emailAddress" id="emailAddressNew" value="<s:property value="bloodDiseaseTempNew.emailAddress"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.correspondenceAddress"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_address" name="sampleBloodDiseaseTemp.address"  value="<s:property value="sampleBloodDiseaseTemp.address"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_address" name="bloodDiseaseTemp.address" value="<s:property value="bloodDiseaseTemp.address"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="address" style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.address"/>"><s:property value="sampleBloodDiseaseTemp.address"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.address"/>"><s:property value="bloodDiseaseTemp.address"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.address" id="addressNew" value="<s:property value="bloodDiseaseTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.detectionDate"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_gestationalAge" name="sampleBloodDiseaseTemp.gestationalAge"  value="<s:property value="sampleBloodDiseaseTemp.gestationalAge"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_gestationalAge" name="bloodDiseaseTemp.gestationalAge" value="<s:property value="bloodDiseaseTemp.gestationalAge"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="gestationalAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('gestationalAgeNew').value=document.getElementById('gestationalAge').options[document.getElementById('gestationalAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.gestationalAge"/>"><s:property value="sampleBloodDiseaseTemp.gestationalAge"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.gestationalAge"/>"><s:property value="bloodDiseaseTemp.gestationalAge"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.gestationalAge" id="gestationalAgeNew" value="<s:property value="bloodDiseaseTempNew.gestationalAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.vitalSigns"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_reason" name="sampleBloodDiseaseTemp.reason"  value="<s:property value="sampleBloodDiseaseTemp.reason"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_reason" name="bloodDiseaseTemp.reason" value="<s:property value="bloodDiseaseTemp.reason"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reason" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reasonNew').value=document.getElementById('reason').options[document.getElementById('reason').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.reason"/>"><s:property value="sampleBloodDiseaseTemp.reason"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.reason"/>"><s:property value="bloodDiseaseTemp.reason"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.reason" id="reasonNew" value="<s:property value="bloodDiseaseTempNew.reason"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   		
                   	<td class="label-title"><fmt:message key="biolims.common.relevantMedicalHistory"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_reason2" name="sampleBloodDiseaseTemp.reason2"  value="<s:property value="sampleBloodDiseaseTemp.reason2"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_reason2" name="bloodDiseaseTemp.reason2" value="<s:property value="bloodDiseaseTemp.reason2"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reason2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reason2New').value=document.getElementById('reason').options[document.getElementById('reason').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.reason2"/>"><s:property value="sampleBloodDiseaseTemp.reason2"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.reason2"/>"><s:property value="bloodDiseaseTemp.reason2"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.reason2" id="reason2New" value="<s:property value="bloodDiseaseTempNew.reason2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   		
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.medicalHistory"/></label>
						</div>
					</td>
				</tr>
                <tr>
                	
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.chemotherap"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_tumorHistory" name="sampleBloodDiseaseTemp.tumorHistory"  value="<s:property value="sampleBloodDiseaseTemp.tumorHistory"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_tumorHistory" name="bloodDiseaseTemp.tumorHistory" value="<s:property value="bloodDiseaseTemp.tumorHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="tumorHistory" style="width:152px;height:28px;" onChange="javascript:document.getElementById('tumorHistoryNew').value=document.getElementById('tumorHistory').options[document.getElementById('tumorHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.tumorHistory"/>"><s:property value="sampleBloodDiseaseTemp.tumorHistory"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.tumorHistory"/>"><s:property value="bloodDiseaseTemp.tumorHistory"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.tumorHistory" id="tumorHistoryNew" value="<s:property value="bloodDiseaseTempNew.tumorHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.lastTimeOfChemotherapy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_firstTransfusionDate" name="sampleBloodDiseaseTemp.firstTransfusionDate" value="<s:property value="sampleBloodDiseaseTemp.firstTransfusionDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_firstTransfusionDate" name="bloodDiseaseTemp.firstTransfusionDate" value="<s:property value="bloodDiseaseTemp.firstTransfusionDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="firstTransfusionDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('firstTransfusionDateNew').value=document.getElementById('firstTransfusionDate').options[document.getElementById('firstTransfusionDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.firstTransfusionDate"/>"><s:property value="sampleBloodDiseaseTemp.firstTransfusionDate"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.firstTransfusionDate"/>"><s:property value="bloodDiseaseTemp.firstTransfusionDate"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.firstTransfusionDate" id="firstTransfusionDateNew" value="<s:property value="bloodDiseaseTempNew.firstTransfusionDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="bloodDiseaseTempNew.firstTransfusionDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>  
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.recentlyUsedImmunosuppressant"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_medicineType" name="sampleBloodDiseaseTemp.medicineType" value="<s:property value="sampleBloodDiseaseTemp.medicineType"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_medicineType" name="bloodDiseaseTemp.medicineType"  value="<s:property value="bloodDiseaseTemp.medicineType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="bloodDiseaseTempNew.medicineType" id="bloodDiseaseTempNew_medicineType">
							<option value="" <s:if test="bloodDiseaseTempNew.medicineType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="0" <s:if test="bloodDiseaseTempNew.medicineType==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
	    					<option value="1" <s:if test="bloodDiseaseTempNew.medicineType==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.specificImmunosuppressiveDrugs"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_medicine" name="sampleBloodDiseaseTemp.medicine"  value="<s:property value="sampleBloodDiseaseTemp.medicine"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_medicine" name="bloodDiseaseTemp.medicine" value="<s:property value="bloodDiseaseTemp.medicine"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="medicine" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicineNew').value=document.getElementById('medicine').options[document.getElementById('medicine').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.medicine"/>"><s:property value="sampleBloodDiseaseTemp.medicine"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.medicine"/>"><s:property value="bloodDiseaseTemp.medicine"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.medicine" id="medicineNew" value="<s:property value="bloodDiseaseTempNew.medicine"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   
                   	<td class="label-title"><fmt:message key="biolims.common.radiationTherapy"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_radioactiveRays" name="sampleBloodDiseaseTemp.radioactiveRays"  value="<s:property value="sampleBloodDiseaseTemp.radioactiveRays"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_radioactiveRays" name="bloodDiseaseTemp.radioactiveRays" value="<s:property value="bloodDiseaseTemp.radioactiveRays"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="radioactiveRays" style="width:152px;height:28px;" onChange="javascript:document.getElementById('radioactiveRaysNew').value=document.getElementById('radioactiveRays').options[document.getElementById('radioactiveRays').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.radioactiveRays"/>"><s:property value="sampleBloodDiseaseTemp.radioactiveRays"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.radioactiveRays"/>"><s:property value="bloodDiseaseTemp.radioactiveRays"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.radioactiveRays" id="radioactiveRaysNew" value="<s:property value="bloodDiseaseTempNew.radioactiveRays"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.boneMarrowGrowthStimulatingFactor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_cadmium" name="sampleBloodDiseaseTemp.cadmium"  value="<s:property value="sampleBloodDiseaseTemp.cadmium"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_cadmium" name="bloodDiseaseTemp.cadmium" value="<s:property value="bloodDiseaseTemp.cadmium"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="cadmium" style="width:152px;height:28px;" onChange="javascript:document.getElementById('cadmiumNew').value=document.getElementById('cadmium').options[document.getElementById('cadmium').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.cadmium"/>"><s:property value="sampleBloodDiseaseTemp.cadmium"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.cadmium"/>"><s:property value="bloodDiseaseTemp.cadmium"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.cadmium" id="cadmiumNew" value="<s:property value="bloodDiseaseTempNew.cadmium"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.peripheralBloodTestResults"/></label>
						</div>
					</td>
				</tr>
                <tr>
                   	
                   	<td class="label-title">WBC</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_trisome21Value" name="sampleBloodDiseaseTemp.trisome21Value"  value="<s:property value="sampleBloodDiseaseTemp.trisome21Value"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_trisome21Value" name="bloodDiseaseTemp.trisome21Value" value="<s:property value="bloodDiseaseTemp.trisome21Value"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="trisome21Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome21ValueNew').value=document.getElementById('trisome21Value').options[document.getElementById('trisome21Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.trisome21Value"/>"><s:property value="sampleBloodDiseaseTemp.trisome21Value"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.trisome21Value"/>"><s:property value="bloodDiseaseTemp.trisome21Value"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.trisome21Value" id="trisome21ValueNew" value="<s:property value="bloodDiseaseTempNew.trisome21Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">RBC</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_trisome18Value" name="sampleBloodDiseaseTemp.trisome18Value"  value="<s:property value="sampleBloodDiseaseTemp.trisome18Value"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_trisome18Value" name="bloodDiseaseTemp.trisome18Value" value="<s:property value="bloodDiseaseTemp.trisome18Value"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="trisome18Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome18ValueNew').value=document.getElementById('trisome18Value').options[document.getElementById('trisome18Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.trisome18Value"/>"><s:property value="sampleBloodDiseaseTemp.trisome18Value"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.trisome18Value"/>"><s:property value="bloodDiseaseTemp.trisome18Value"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.trisome18Value" id="trisome18ValueNew" value="<s:property value="bloodDiseaseTempNew.trisome18Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">Hb</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_trisome13Value" name="sampleBloodDiseaseTemp.trisome13Value"  value="<s:property value="sampleBloodDiseaseTemp.trisome13Value"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_trisome13Value" name="bloodDiseaseTemp.trisome13Value" value="<s:property value="bloodDiseaseTemp.trisome13Value"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="trisome13Value" style="width:152px;height:28px;" onChange="javascript:document.getElementById('trisome13ValueNew').value=document.getElementById('trisome13Value').options[document.getElementById('trisome13Value').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.trisome13Value"/>"><s:property value="sampleBloodDiseaseTemp.trisome13Value"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.trisome13Value"/>"><s:property value="bloodDiseaseTemp.trisome13Value"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.trisome13Value" id="trisome13ValueNew" value="<s:property value="bloodDiseaseTempNew.trisome13Value"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title">PLT</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_confirmAgeTwo" name="sampleBloodDiseaseTemp.confirmAgeTwo"  value="<s:property value="sampleBloodDiseaseTemp.confirmAgeTwo"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_confirmAgeTwo" name="bloodDiseaseTemp.confirmAgeTwo" value="<s:property value="bloodDiseaseTemp.confirmAgeTwo"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAgeTwo" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeTwoNew').value=document.getElementById('confirmAgeTwo').options[document.getElementById('confirmAgeTwo').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.confirmAgeTwo"/>"><s:property value="sampleBloodDiseaseTemp.confirmAgeTwo"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.confirmAgeTwo"/>"><s:property value="bloodDiseaseTemp.confirmAgeTwo"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.confirmAgeTwo" id="confirmAgeTwoNew" value="<s:property value="bloodDiseaseTempNew.confirmAgeTwo"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.originalCell"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_confirmAgeThree" name="sampleBloodDiseaseTemp.confirmAgeThree"  value="<s:property value="sampleBloodDiseaseTemp.confirmAgeThree"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_confirmAgeThree" name="bloodDiseaseTemp.confirmAgeThree" value="<s:property value="bloodDiseaseTemp.confirmAgeThree"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAgeThree" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeThreeNew').value=document.getElementById('confirmAgeThree').options[document.getElementById('confirmAgeThree').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.confirmAgeThree"/>"><s:property value="sampleBloodDiseaseTemp.confirmAgeThree"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.confirmAgeThree"/>"><s:property value="bloodDiseaseTemp.confirmAgeThree"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.confirmAgeThree" id="confirmAgeThreeNew" value="<s:property value="bloodDiseaseTempNew.confirmAgeThree"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.immatureCells"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_confirmAge" name="sampleBloodDiseaseTemp.confirmAge"  value="<s:property value="sampleBloodDiseaseTemp.confirmAge"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_confirmAge" name="bloodDiseaseTemp.confirmAge" value="<s:property value="bloodDiseaseTemp.confirmAge"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeNew').value=document.getElementById('confirmAge').options[document.getElementById('confirmAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.confirmAge"/>"><s:property value="sampleBloodDiseaseTemp.confirmAge"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.confirmAge"/>"><s:property value="bloodDiseaseTemp.confirmAge"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.confirmAge" id="confirmAgeNew" value="<s:property value="bloodDiseaseTempNew.confirmAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   		
                   	<td class="label-title"><fmt:message key="biolims.common.otherCells"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_stemCellsCure" name="sampleBloodDiseaseTemp.stemCellsCure"  value="<s:property value="sampleBloodDiseaseTemp.stemCellsCure"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_stemCellsCure" name="bloodDiseaseTemp.stemCellsCure" value="<s:property value="bloodDiseaseTemp.stemCellsCure"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="stemCellsCure" style="width:152px;height:28px;" onChange="javascript:document.getElementById('stemCellsCureNew').value=document.getElementById('stemCellsCure').options[document.getElementById('stemCellsCure').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.stemCellsCure"/>"><s:property value="sampleBloodDiseaseTemp.stemCellsCure"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.stemCellsCure"/>"><s:property value="bloodDiseaseTemp.stemCellsCure"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.stemCellsCure" id="stemCellsCureNew" value="<s:property value="bloodDiseaseTempNew.stemCellsCure"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>	
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.boneMarrowExaminationResults"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_badMotherhood" name="sampleBloodDiseaseTemp.badMotherhood"  value="<s:property value="sampleBloodDiseaseTemp.badMotherhood"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_badMotherhood" name="bloodDiseaseTemp.badMotherhood" value="<s:property value="bloodDiseaseTemp.badMotherhood"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="badMotherhood" style="width:152px;height:28px;" onChange="javascript:document.getElementById('badMotherhoodNew').value=document.getElementById('badMotherhood').options[document.getElementById('badMotherhood').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.badMotherhood"/>"><s:property value="sampleBloodDiseaseTemp.badMotherhood"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.badMotherhood"/>"><s:property value="bloodDiseaseTemp.badMotherhood"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.badMotherhood" id="badMotherhoodNew" value="<s:property value="bloodDiseaseTempNew.badMotherhood"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.clinicalDiagnosisOpinion"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_diagnosis" name="sampleBloodDiseaseTemp.diagnosis"  value="<s:property value="sampleBloodDiseaseTemp.diagnosis"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_diagnosis" name="bloodDiseaseTemp.diagnosis" value="<s:property value="bloodDiseaseTemp.diagnosis"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="diagnosis" style="width:152px;height:28px;" onChange="javascript:document.getElementById('diagnosisNew').value=document.getElementById('diagnosis').options[document.getElementById('diagnosis').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.diagnosis"/>"><s:property value="sampleBloodDiseaseTemp.diagnosis"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.diagnosis"/>"><s:property value="bloodDiseaseTemp.diagnosis"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.diagnosis" id="diagnosisNew" value="<s:property value="bloodDiseaseTempNew.diagnosis"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label><fmt:message key="biolims.common.chargeState"/></label>
						</div>
					</td>
				</tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.whetherTheCharge"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_isInsure" name="sampleBloodDiseaseTemp.isInsure" value="<s:property value="sampleBloodDiseaseTemp.isInsure"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_isInsure" name="bloodDiseaseTemp.isInsure"  value="<s:property value="bloodDiseaseTemp.isInsure"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="bloodDiseaseTempNew.isInsure" id="bloodDiseaseTempNew_isInsure">
							<option value="" <s:if test="bloodDiseaseTempNew.isInsure==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="0" <s:if test="bloodDiseaseTempNew.isInsure==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
	    					<option value="1" <s:if test="bloodDiseaseTempNew.isInsure==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.preferentialType"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_privilegeType" name="sampleBloodDiseaseTemp.privilegeType" value="<s:property value="sampleBloodDiseaseTemp.privilegeType"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_privilegeType" name="bloodDiseaseTemp.privilegeType"  value="<s:property value="bloodDiseaseTemp.privilegeType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="bloodDiseaseTempNew.privilegeType" id="bloodDiseaseTempNew_privilegeType">
							<option value="" <s:if test="bloodDiseaseTempNew.privilegeType==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="0" <s:if test="bloodDiseaseTempNew.privilegeType==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
	    					<option value="1" <s:if test="bloodDiseaseTempNew.privilegeType==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.personOfRecommendations"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_linkman" name="sampleBloodDiseaseTemp.linkman"  value="<s:property value="sampleBloodDiseaseTemp.linkman"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_linkman" name="bloodDiseaseTemp.linkman" value="<s:property value="bloodDiseaseTemp.linkman"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="linkman" style="width:152px;height:28px;" onChange="javascript:document.getElementById('linkmanNew').value=document.getElementById('linkman').options[document.getElementById('linkman').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.linkman"/>"><s:property value="sampleBloodDiseaseTemp.linkman"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.linkman"/>"><s:property value="bloodDiseaseTemp.linkman"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.linkman" id="linkmanNew" value="<s:property value="bloodDiseaseTempNew.linkman"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.whetherTheInvoice"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBloodDiseaseTemp_isInvoice" name="sampleBloodDiseaseTemp.isInvoice" value="<s:property value="sampleBloodDiseaseTemp.isInvoice"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="bloodDiseaseTemp_isInvoice" name="bloodDiseaseTemp.isInvoice"  value="<s:property value="bloodDiseaseTemp.isInvoice"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="bloodDiseaseTempNew.isInvoice" id="bloodDiseaseTempNew_isInvoice">
							<option value="" <s:if test="bloodDiseaseTempNew.isInvoice==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="0" <s:if test="bloodDiseaseTempNew.isInvoice==0">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
	    					<option value="1" <s:if test="bloodDiseaseTempNew.isInvoice==1">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                    <td class="label-title" ><fmt:message key="biolims.common.auditor"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="bloodDiseaseTempNew_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="bloodDiseaseTempNew.createUser" title="<fmt:message key="biolims.common.auditor"/>"   
							value="<s:property value="bloodDiseaseTempNew.createUser"/>" />
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.note"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_note" name="sampleBloodDiseaseTemp.note"  value="<s:property value="sampleBloodDiseaseTemp.note"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_note" name="bloodDiseaseTemp.note" value="<s:property value="bloodDiseaseTemp.note"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="note" style="width:152px;height:28px;" onChange="javascript:document.getElementById('noteNew').value=document.getElementById('note').options[document.getElementById('note').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.note"/>"><s:property value="sampleBloodDiseaseTemp.note"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.note"/>"><s:property value="bloodDiseaseTemp.note"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.note" id="noteNew" value="<s:property value="bloodDiseaseTempNew.note"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                 <tr>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.money"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_money" name="sampleBloodDiseaseTemp.money"  value="<s:property value="sampleBloodDiseaseTemp.money"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_money" name="bloodDiseaseTemp.money" value="<s:property value="bloodDiseaseTemp.money"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="money" style="width:152px;height:28px;" onChange="javascript:document.getElementById('moneyNew').value=document.getElementById('money').options[document.getElementById('money').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.money"/>"><s:property value="sampleBloodDiseaseTemp.money"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.money"/>"><s:property value="bloodDiseaseTemp.money"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.money" id="moneyNew" value="<s:property value="bloodDiseaseTempNew.money"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_bloodHistory" name="sampleBloodDiseaseTemp.bloodHistory"  value="<s:property value="sampleBloodDiseaseTemp.bloodHistory"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_bloodHistory" name="bloodDiseaseTemp.bloodHistory" value="<s:property value="bloodDiseaseTemp.bloodHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="bloodHistory" style="width:152px;height:28px;" onChange="javascript:document.getElementById('bloodHistoryNew').value=document.getElementById('bloodHistory').options[document.getElementById('bloodHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.bloodHistory"/>"><s:property value="sampleBloodDiseaseTemp.bloodHistory"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.bloodHistory"/>"><s:property value="bloodDiseaseTemp.bloodHistory"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.bloodHistory" id="bloodHistoryNew" value="<s:property value="bloodDiseaseTempNew.bloodHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.institutionsOfMakeOutAnInvoice"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_paymentUnit" name="sampleBloodDiseaseTemp.paymentUnit"  value="<s:property value="sampleBloodDiseaseTemp.paymentUnit"/>"/>
  						<input type="hidden" id="bloodDiseaseTemp_paymentUnit" name="bloodDiseaseTemp.paymentUnit" value="<s:property value="bloodDiseaseTemp.paymentUnit"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="paymentUnit" style="width:152px;height:28px;" onChange="javascript:document.getElementById('paymentUnitNew').value=document.getElementById('paymentUnit').options[document.getElementById('paymentUnit').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.paymentUnit"/>"><s:property value="sampleBloodDiseaseTemp.paymentUnit"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.paymentUnit"/>"><s:property value="bloodDiseaseTemp.paymentUnit"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.paymentUnit" id="paymentUnitNew" value="<s:property value="bloodDiseaseTempNew.paymentUnit"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                <g:LayOutWinTag buttonId="showreportMan" title='<fmt:message key="biolims.common.selectingTheCheckPerson"/>'
				hasHtmlFrame="true"
				html="${ctx}/core/user/userSelect.action"
				isHasSubmit="false" functionName="reportManUserFun" 
 				hasSetFun="true"
				documentId="sampleBloodDiseaseTemp_reportMan"
				documentName="sampleBloodDiseaseTemp_reportMan_name" />
			
			  	 	<td class="label-title"><fmt:message key="biolims.common.checkThePeopleOne"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_reportMan_id" name="sampleBloodDiseaseTemp.reportMan.id"  value="<s:property value="sampleBloodDiseaseTemp.reportMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_reportMan_name" name="sampleBloodDiseaseTemp.reportMan.name"  value="<s:property value="sampleBloodDiseaseTemp.reportMan.name"/>" class="text input" />
  						<input type="hidden" id="bloodDiseaseTemp_reportMan_id" name="bloodDiseaseTemp.reportMan.id" value="<s:property value="bloodDiseaseTemp.reportMan.id"/>">
  						<input type="hidden" id="bloodDiseaseTemp_reportMan_name" name="bloodDiseaseTemp.reportMan.name" value="<s:property value="bloodDiseaseTemp.reportMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reportMan" style="width:152px;height:28px;margin-top: 8px;" onChange="javascript:document.getElementById('reportManNew').value=document.getElementById('reportMan').options[document.getElementById('reportMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.reportMan.name"/>"><s:property value="sampleBloodDiseaseTemp.reportMan.name"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.reportMan.name"/>"><s:property value="bloodDiseaseTemp.reportMan.name"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.reportMan.name" id="reportManNew" value="<s:property value="bloodDiseaseTempNew.reportMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="bloodDiseaseTempNew.reportMan.id" id="reportManIdNew" value="<s:property value="bloodDiseaseTempNew.reportMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' style="margin-left: 20px;" class='detail' /> --%>
                   	</td>
                   	<td></td>
                   	<td></td>
                   	<td></td>
                   	<td class="label-title"><fmt:message key="biolims.common.theNextStepTo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="bloodDiseaseTempNew.nextStepFlow" id="bloodDiseaseTempNew_nextStepFlow">
							<option value="" <s:if test="bloodDiseaseTempNew.nextStepFlow==''">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
	    					<option value="1" <s:if test="bloodDiseaseTempNew.nextStepFlow==1">selected="selected" </s:if>><fmt:message key="biolims.common.qualified"/></option>
	    					<option value="0" <s:if test="bloodDiseaseTempNew.nextStepFlow==0">selected="selected" </s:if>><fmt:message key="biolims.common.feedbackToTheProjectManagement"/></option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                 </tr>
                 <tr>
                 <g:LayOutWinTag buttonId="showauditMan" title='<fmt:message key="biolims.common.selectingTheCheckPerson"/>'
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="auditManUserFun" 
	 				hasSetFun="true"
					documentId="sampleBloodDiseaseTemp_auditMan"
					documentName="sampleBloodDiseaseTemp_auditMan_name" />
                   	
                   	<td class="label-title"><fmt:message key="biolims.common.checkThePeopleTwo"/></td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_auditMan_id" name="sampleBloodDiseaseTemp.auditMan.id"  value="<s:property value="sampleBloodDiseaseTemp.auditMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleBloodDiseaseTemp_auditMan_name" name="sampleBloodDiseaseTemp.auditMan.name"  value="<s:property value="sampleBloodDiseaseTemp.auditMan.name"/>" class="text input" />
  						<input type="hidden" id="bloodDiseaseTemp_auditMan_id" name="bloodDiseaseTemp.auditMan.id" value="<s:property value="bloodDiseaseTemp.auditMan.id"/>">
  						<input type="hidden" id="bloodDiseaseTemp_auditMan_name" name="bloodDiseaseTemp.auditMan.name" value="<s:property value="bloodDiseaseTemp.auditMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="auditMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('auditManNew').value=document.getElementById('auditMan').options[document.getElementById('auditMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---<fmt:message key="biolims.common.pleaseSelect"/>---</option> 
							<option value="<s:property value="sampleBloodDiseaseTemp.auditMan.name"/>"><s:property value="sampleBloodDiseaseTemp.auditMan.name"/></option> 
							<option value="<s:property value="bloodDiseaseTemp.auditMan.name"/>"><s:property value="bloodDiseaseTemp.auditMan.name"/></option>
						</select> 
						<input type="text" name="bloodDiseaseTempNew.auditMan.name" id="auditManNew" value="<s:property value="bloodDiseaseTempNew.auditMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="bloodDiseaseTempNew.auditMan.id" id="auditManIdNew" value="<s:property value="bloodDiseaseTempNew.auditMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail' /> --%>
                   	</td>
                </tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBloodDisease.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
