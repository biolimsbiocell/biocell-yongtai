﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript" src="${ctx}/js/sample/dicSampleTypeDialog1.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<input type="hidden" id="a" value="${requestScope.a}">
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.serialNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="18" id="dicSampleType_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.type"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicSampleType_name" searchField="true" name="name"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.tag"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicSampleType_code" searchField="true" name="code"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.explain"/></td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="dicSampleType_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title"><fmt:message key="biolims.common.sortingNumber"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicSampleType_orderNumber" searchField="true" name="orderNumber"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.type"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicSampleType_type" searchField="true" name="type"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.settings"/></td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="dicSampleType_func" searchField="true" name="func"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title"><fmt:message key="biolims.common.state"/></td>
               	<td align="left">
                    		<input type="text" maxlength="5" id="dicSampleType_state" searchField="true" name="state"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue"><fmt:message key="biolims.common.search"/></font></span>
		
		<div id="show_dialog_dicSampleType_div"></div>
   		
</body>
</html>



