﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInputTechnology&id=${sampleInputTechnology.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleInputTechnologyEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_id" name="sampleInputTechnology.id" title="编号"   
	value="<s:property value="sampleInputTechnology.id"/>" style="display:none"/>
                   	</td>
			
					<td class="label-title" >样本编码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_sampleCode"
                   	 name="sampleInputTechnology.sampleCode" title="样本编码"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleInputTechnology.sampleCode"/>"
                   	  />
                   	  
                   	</td>
			
					
					<td class="label-title" >合同编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="sampleInputTechnology_contractNum" name="sampleInputTechnology.contractNum" title="合同编号"
    value="<s:property value="sampleInputTechnology.contractNum"/>"
                   	  />
                   	  
                   	</td>
					
               	 	<td class="label-title" >项目名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="50" id="sampleInputTechnology_projectName"
                   	 name="sampleInputTechnology.projectName" title="项目名称"
                   	   
	value="<s:property value="sampleInputTechnology.projectName"/>"
                   	  />
                   	  
                   	</td>
			
			</tr>
			<tr>
					<td class="label-title" >客户名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_clientName"
                   	 name="sampleInputTechnology.clientName" title="客户名称"
	value="<s:property value="sampleInputTechnology.clientName"/>"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" >客户单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_clientUnit"
                   	 name="sampleInputTechnology.clientUnit" title="客户单位"
	value="<s:property value="sampleInputTechnology.clientUnit"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >样本状态</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_sampleState"
                   	 name="sampleInputTechnology.sampleState" title="样本状态"
	value="<s:property value="sampleInputTechnology.sampleState"/>"
                   	  />
                   	  
                   	</td>
			
			</tr>
			<tr>
					<td class="label-title" >物种</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_species"
                   	 name="sampleInputTechnology.species" title="物种"
	value="<s:property value="sampleInputTechnology.species"/>"
                   	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" >器官类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_organType"
                   	 name="sampleInputTechnology.organType" title="器官类型"
	value="<s:property value="sampleInputTechnology.organType"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >样本名称</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_sampleName"
                   	 name="sampleInputTechnology.sampleName" title="样本名称"
	value="<s:property value="sampleInputTechnology.sampleName"/>"
                   	  />
                   	  	<input type="hidden"   	 name="sampleInputTechnology.sampleInfo.id" value="<s:property value="sampleInputTechnology.sampleInfo.id"/>"  />
                   	</td>
			
			</tr>
			<tr>
					
               	 	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInputTechnology_sampleType_name"  value="<s:property value="sampleInputTechnology.sampleType.name"/>" readonly="readOnly"  />
 						<input type="hidden" id="sampleInputTechnology_sampleType" name="sampleInputTechnology.sampleType.id"  value="<s:property value="sampleInputTechnology.sampleType.id"/>" > 
 						<img alt='选择样本类型' id='showsampleType' src='${ctx}/images/img_lookup.gif' 	class='detail'  onClick="sampleKind()"   />                   		
                   	</td>
			
					<td class="label-title" >接受日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_sendDate"
                   	 name="sampleInputTechnology.sendDate" title="接受日期"
                   	   readonly = "readOnly" class="text input readonlytrue"  
    value="<s:date name="sampleInputTechnology.sendDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
               	 	<td class="label-title" >开箱接收人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_openBoxUser"
                   	 name="sampleInputTechnology.openBoxUser" title="开箱接收人"
	value="<s:property value="sampleInputTechnology.openBoxUser"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
					
<%-- 			<g:LayOutWinTag buttonId="showcreateUser" title="选择录入人" --%>
<%-- 						hasHtmlFrame="true" html="${ctx}/core/user/userSelect.action" --%>
<%-- 						isHasSubmit="false" functionName="detectionUserFun" hasSetFun="true" --%>
<%-- 						documentId="sampleInputTechnology_createUser" --%>
<%-- 						documentName="sampleInputTechnology_createUser_name" /> --%>
			
               	 	<td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleInputTechnology_createUser_name"  value="<s:property value="sampleInputTechnology.createUser.name"/>" class="text input readonlytrue" readonly="readOnly"  />
 						<input type="hidden" id="sampleInputTechnology_createUser" name="sampleInputTechnology.createUser.id"  value="<s:property value="sampleInputTechnology.createUser.id"/>" > 
<%--  						<img alt='选择录入人' id='showcreateUser' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		 --%>
                   	</td>
			
               	 	<td class="label-title" >录入时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_createDate"
                   	 name="sampleInputTechnology.createDate" title="录入时间"
                   	   readonly = "readOnly" class="text input readonlytrue"  
                   	  value="<s:date name="sampleInputTechnology.createDate" format="yyyy-MM-dd"/>"
                   	                      	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTechnology_note"
                   	 name="sampleInputTechnology.note" title="备注"
	value="<s:property value="sampleInputTechnology.note"/>"
                   	  />
                   	  
                   	</td>
			
			</tr>
			<tr>
                   	<td class="label-title">是否合格</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
                   		<select name="sampleInputTechnology.isQualified" id="sampleInputTechnology_isQualified" title="是否合格">
	    					<option value=""<s:if test="sampleInputTechnology.isQualified==''">selected="selected" </s:if>>--请选择--</option>
	    					<option value="1" <s:if test="sampleInputTechnology.isQualified==1">selected="selected" </s:if>>是</option>
	    					<option value="0" <s:if test="sampleInputTechnology.isQualified==0">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleInputTechnology.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul> 
			</div> -->
        	</div>
	</body>
	</html>
