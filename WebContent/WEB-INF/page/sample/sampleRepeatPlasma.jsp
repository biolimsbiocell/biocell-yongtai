﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleRepeatPlasma.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>

<div id="jstj" style="display: none">
	<input type="hidden" id="selectId"/>
	<input type="hidden" id="extJsonDataString" name="extJsonDataString">
			<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title" ><fmt:message key="biolims.common.sampleCode"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="dNAAbnormalBack_sampleCode"
                   	 name="sampleCode" searchField="true" title="<fmt:message key="biolims.common.sampleCode"/>"    />

                   	</td>
                   	<td class="label-title" ><fmt:message key="biolims.common.bloodCode"/></td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="dNAAbnormalBack_code"
                   	 name="code" searchField="true" title="<fmt:message key="biolims.common.bloodCode"/>"    />

                   	</td>
               	 	
            </tr>
            </table>
		</form>
		</div>
	<div id="bat_next_div" style="display: none">
		<table>
			<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.processingOpinion"/></span></td>
                <td>
                	<select id="method"  style="width:100">
                			<option value="" <s:if test="method==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="3" <s:if test="method==3">selected="selected" </s:if>><fmt:message key="biolims.common.drawBloodAgain"/></option>
    					<option value="4" <s:if test="method==4">selected="selected" </s:if>><fmt:message key="biolims.common.unqualified"/></option>
					</select>
                 </td>
			</tr>
		</table>
		</div>
		<div id="show_repeatPlasma_div" width="100%" height:10px></div>
		<div id="bat_submit_div" style="display: none">
		<table>
				<tr>
				<td class="label-title" ><span><fmt:message key="biolims.common.result"/></span></td>
                <td><select id="submit" style="width:100">
                		<option value="" <s:if test="submit==">selected="selected" </s:if>><fmt:message key="biolims.common.pleaseSelect"/></option>
    					<option value="1" <s:if test="submit==1">selected="selected" </s:if>><fmt:message key="biolims.common.yes"/></option>
    					<option value="0" <s:if test="submit==0">selected="selected" </s:if>><fmt:message key="biolims.common.no"/></option>
					</select>
                 </td>
			</tr>
		</table>
	</div>
</body>
</html>



