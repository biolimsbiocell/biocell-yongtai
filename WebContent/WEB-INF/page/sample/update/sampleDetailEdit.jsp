﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleDetail&id=${sampleDetail.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/update/sampleDetailEdit.js"></script>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"   ><img class='requiredimage' src='${ctx}/images/required.gif' /></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="sampleDetail_sampleId"
                   	 name="sampleDetail.sampleId" title="样本编号"
                   	   
	value="<s:property value="sampleDetail.sampleId"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="50" id="sampleDetail_name"
                   	 name="sampleDetail.name" title="姓名"
                   	   
	value="<s:property value="sampleDetail.name"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleDetail_sampleType"
                   	 name="sampleDetail.sampleType" title="样本类型"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleDetail.sampleType"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title"  style="display:none"  >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="sampleDetail_sex"
                   	 name="sampleDetail.sex" title="性别"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleDetail.sex"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >结果判定</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleDetail_resultDecide"
                   	 name="sampleDetail.resultDecide" title="结果判定"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleDetail.resultDecide"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title"  style="display:none"  >下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px"  style="display:none"  ></td>            	 	
                   	<td align="left"   style="display:none">
                   	<input type="text" size="20" maxlength="25" id="sampleDetail_nextFlow"
                   	 name="sampleDetail.nextFlow" title="下一步流向"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleDetail.nextFlow"/>"
                   	   style="display:none"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			
			
               	 	<td class="label-title" >处理意见</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleDetail_handleIdea"
                   	 name="sampleDetail.handleIdea" title="处理意见"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleDetail.handleIdea"/>"
                   	  />
                   	  
                   	</td>
			
			
               	 	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="50" maxlength="75" id="sampleDetail_note"
                   	 name="sampleDetail.note" title="备注"
                   	   readonly = "readOnly" class="text input readonlytrue"  
	value="<s:property value="sampleDetail.note"/>"
                   	  />
                   	  
                   	</td>
			</tr>
			<tr>
			<td class="label-title">附件</td><td></td>
						<td title="保存基本后,可以维护查看附件" id="doclinks_img"><span 
							class="attach-btn"></span><span class="text label">共有${requestScope.fileNum}个附件</span>
			</tr>
			
			
            </table>
            <input type="hidden" name="sampleUpdateRecordsJson" id="sampleUpdateRecordsJson" value="" />
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleDetail.id"/>" />
            </form>
            <div id="tabs">
            <ul>
			<li><a href="#sampleUpdateRecordspage">修改记录</a></li>
           	</ul> 
			<div id="sampleUpdateRecordspage" width="100%" height:10px></div>
			</div>
        	</div>
	</body>
	</html>
