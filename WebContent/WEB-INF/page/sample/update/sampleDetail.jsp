﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<%@ include file="/WEB-INF/page/include/toolbar.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/update/sampleDetail.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>

</head>
<body>
		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
               	 	<td class="label-title"  style="display:none"  >样本编号</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleDetail_sampleId"
                   	 name="sampleId" searchField="true" title="样本编号"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >姓名</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="50" id="sampleDetail_name"
                   	 name="name" searchField="true" title="姓名"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >样本类型</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleDetail_sampleType"
                   	 name="sampleType" searchField="true" title="样本类型"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title"  style="display:none"  >性别</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleDetail_sex"
                   	 name="sex" searchField="true" title="性别"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >结果判定</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleDetail_resultDecide"
                   	 name="resultDecide" searchField="true" title="结果判定"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title"  style="display:none"  >下一步流向</td>
                   	<td align="left"   style="display:none">
                  
					<input type="text" size="20" maxlength="25" id="sampleDetail_nextFlow"
                   	 name="nextFlow" searchField="true" title="下一步流向"   style="display:none"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
			<tr>
               	 	<td class="label-title" >处理意见</td>
                   	<td align="left"  >
                  
					<input type="text" size="20" maxlength="25" id="sampleDetail_handleIdea"
                   	 name="handleIdea" searchField="true" title="处理意见"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
               	 	<td class="label-title" >备注</td>
                   	<td align="left"  >
                  
					<input type="text" size="50" maxlength="75" id="sampleDetail_note"
                   	 name="note" searchField="true" title="备注"    />
                   	
					
 
                  
                   	
                   	  
                   	</td>
			</tr>
            </table>
		</form>
		</div>
		<div id="show_sampleDetail_div"></div>
   		<form name='excelfrm' action='/common/exportExcel.action' method='POST'>
		<input type='hidden' id='gridhtm' name='gridhtm' value=''/></form>
		<div id="show_sampleDetail_tree_page"></div>
</body>
</html>



