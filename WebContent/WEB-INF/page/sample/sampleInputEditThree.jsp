
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleInput&id=${sampleInput.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleInputEditThree.js"></script>
	<s:if test="sampleInputTemp.upLoadAccessory.id != ''">
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" class="img" src="${ctx}/operfile/downloadById.action?id=${sampleInputTemp.sampleInfo.upLoadAccessory.id}"></div>
	</s:if>
	<s:if test="sampleInputTemp.upLoadAccessory.id ==''"> 
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src=""></div>
	</s:if>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
            <input type="hidden" id="saveType" value="${requestScope.saveType}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red;font-size:larger;">上传图片</td>
				<td></td>
				<td>
  					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleInputTemp.upLoadAccessory.id" value="<s:property value="sampleInputTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleInputTemp.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.upLoadAccessory.fileName"/>">
				</td>
			</tr>
				<tr>
<!--                	 	<td class="label-title" style="display: none">编号</td> -->
<!-- 					<td class="requiredcolunm" nowrap width="10px" style="display: none"></td> --%>
<!-- 					<td align="left" style="display: none"><input type="text" size="20" maxlength="25" -->
<!-- 						id="sampleInputTemp_id" -->
<!-- 						class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.id" title="编号" -->
<%-- 						value="<s:property value="sampleInputTemp.id"/>" /> --%>
<!-- 					</td> -->
                    
  				<tr>
  					<td class="label-title">样本编号</td>
					<td class="requiredcolumn" nowrap width="10px">
						
					</td>
					<td align="left">
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_code" name="sampleInputTemp.sampleInfo.code" title="样本编号" class="text input readonlytrue" readonly="readonly" value="<s:property value="sampleInputTemp.sampleInfo.code"/>" />
						<input type="hidden" id="sampleInputTemp_id" name="sampleInputTemp.sampleInfo.id" value="<s:property value="sampleInputTemp.sampleInfo.id"/>" />
						<input type="hidden"  id="upload_imga" name="sampleInputTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleInputTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleInputTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleInputTemp.sampleInfo.upLoadAccessory.id"/>">
					</td>
                   	
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_name"
                   	 	name="sampleInputTemp.name" title="描述"   
						value="<s:property value="sampleInputTemp.name"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="hidden" size="20"   id="sampleInputTemp_productId" searchField="true"  
 							name="sampleInputTemp.productId"  value="<s:property value="sampleInputTemp.productId"/>" class="text input" />
 						<input type="text" id="sampleInputTemp_productName" readonly="readonly" name="sampleInputTemp.productName"  
 							value="<s:property value="sampleInputTemp.productName"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   	
  				</tr>
                
                <tr>
              		<td class="label-title" >户籍地</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_area"
                   	 	name="sampleInputTemp.area" title="户籍地"  onblur="checkAdd()" 
						value="<s:property value="sampleInputTemp.area"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >身份证号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_voucherCode"
                   	 	name="sampleInputTemp.voucherCode" title="身份证号码"   onblur="checkFun()"
						value="<s:property value="sampleInputTemp.voucherCode"/>"
                   	 />
                   	 <img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
				</tr>
				<tr>
					<td class="label-title" >抽血时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleInputTemp_sendDate"
                   	 		name="sampleInputTemp.sendDate" title="抽血时间" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleInputTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	 />
<!--                    	</td> -->
<!--                    		<td class="label-title">样本编号</td> -->
<!-- 					<td class="requiredcolumn" nowrap width="10px"></td> --%>
<!-- 					<td align="left"> -->
					
<!-- 					<input type="text" size="20" maxlength="25" -->
<!-- 						id="sampleInputTemp_code"  -->
<!-- 						name="sampleInputTemp.sampleInfo.code" title="样本编号" -->
<%-- 						value="<s:property value="sampleInputTemp.sampleInfo.code"/>" /> --%>
<!-- 						<input type="hidden"  -->
<!-- 						id="sampleInputTemp_id"  -->
<!-- 						name="sampleInputTemp.sampleInfo.id"  -->
<%-- 						value="<s:property value="sampleInputTemp.sampleInfo.id"/>" /> --%>
<!-- 						<input type="hidden" name="sampleInputTemp.id" -->
<%-- 						value="<s:property value="sampleInputTemp.id"/>" /> --%>
				</tr>
				<tr>
					<td class="label-title" >门诊号/住院号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_inHosNum"
                   	 	name="sampleInputTemp.inHosNum" title="门诊号/住院号"   
						value="<s:property value="sampleInputTemp.inHosNum"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >手机电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_phoneNum"
                   	 	name="sampleInputTemp.phoneNum" title="手机电话"   onblur="checkPhone()"
						value="<s:property value="sampleInputTemp.phoneNum"/>"
                   	 />
                   	</td>
				</tr>
				<tr>
					<td class="label-title" >家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_address"

                   	 	name="sampleInputTemp.address" title="家庭住址"   onblur="checkAddress()"

						value="<s:property value="sampleInputTemp.address"/>"
                   	 />
                   	 <img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
				<tr>
					<td class="label-title" >孕妇姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_patientName"
                   	 	name="sampleInputTemp.patientName" title="孕妇姓名"   
						value="<s:property value="sampleInputTemp.patientName"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >出生日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	  	<input type="text" size="20" maxlength="25" id="sampleInputTemp_birthday"
                   	 		name="sampleInputTemp.birthday" title="出生日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleInputTemp.birthday" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                   	<td class="label-title" >现年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleInputTemp_age" name="sampleInputTemp.age"  value="<s:property value="sampleInputTemp.age"/>" />
                   	</td>
				</tr>
				<tr>
	                <td class="label-title" >孕周</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="5" id="sampleInputTemp_gestationalAge"
                   	 	name="sampleInputTemp.gestationalAge" title="孕周"   
						value="<s:property value="sampleInputTemp.gestationalAge"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_weight"
                   	 	name="sampleInputTemp.weight" title="体重" 
						value="<s:property value="sampleInputTemp.weight"/>"
                   	 />
                   	</td>

                	<td class="label-title" >临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_diagnosis"
                   	 	name="sampleInputTemp.diagnosis" title="临床诊断"   
						value="<s:property value="sampleInputTemp.diagnosis"/>"
                   	 />
                   	</td>
             </tr>
             <tr>
                   	<td class="label-title">录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px"></td>            	 	
                   	<td align="left">
<%--                    	<input type="hidden" size="20" maxlength="25" id="sampleInputTemp_createUser_id" name="sampleInputTemp.createUser.id" title="录入人" value='<s:property value="sampleInputTemp.createUser.id" />' /> --%>
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_createUser1" class="text input readonlytrue" readonly="readonly" name="sampleInputTemp.createUser1" title="录入人" value="<s:property value="sampleInputTemp.createUser1"/>" />
                   	</td>
                   	
                </tr>
                <tr>
				<td colspan="9">
					<div class="standard-section-header type-title">
						<label>特殊既往史</label>
					</div>
				</td>
			</tr>
			<tr>
              		<td class="label-title" >异体输血</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.outTransfusion" id="sampleInputTemp_outTransfusion" >
<!-- 							<option value="" <s:if test="sampleInputTemp.outTransfusion==''">selected="selected" </s:if>>请选择</option> -->
	    					<option value="0" <s:if test="sampleInputTemp.outTransfusion==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.outTransfusion==1">selected="selected" </s:if>>有</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >移植手术</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.organGrafting" id="sampleInputTemp_organGrafting" >
<!-- 						<option value="" <s:if test="sampleInputTemp.organGrafting==''">selected="selected" </s:if>>请选择</option> -->
    					<option value="0" <s:if test="sampleInputTemp.organGrafting==0">selected="selected" </s:if>>无</option>
    					<option value="1" <s:if test="sampleInputTemp.organGrafting==1">selected="selected" </s:if>>有</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >干细胞治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.stemCellsCure" id="sampleInputTemp_stemCellsCure" >
<!-- 							<option value="" <s:if test="sampleInputTemp.stemCellsCure==''">selected="selected" </s:if>>请选择</option> -->
	    					<option value="0" <s:if test="sampleInputTemp.stemCellsCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.stemCellsCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
              	</tr>
              	<tr>
              		<td class="label-title" >免疫治疗</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.immuneCure" id="sampleInputTemp_immuneCure" >
<!-- 							<option value="" <s:if test="sampleInputTemp.immuneCure==''">selected="selected" </s:if>>请选择</option> -->
	    					<option value="0" <s:if test="sampleInputTemp.immuneCure==0">selected="selected" </s:if>>无</option>
	    					<option value="1" <s:if test="sampleInputTemp.immuneCure==1">selected="selected" </s:if>>有</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   
              	</tr>
              	<tr>
              	
              		<td class="label-title" >不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_badMotherhood"
                   	 	name="sampleInputTemp.badMotherhood" title="不良孕产史"   
						value="<s:property value="sampleInputTemp.badMotherhood"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >家族史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_medicalHistory"
                   	 	name="sampleInputTemp.medicalHistory" title="家族史"   
						value="<s:property value="sampleInputTemp.medicalHistory"/>"
                   	 />
                   	</td>
                   	</tr>
                   	<tr>
                   		<td class="label-title" >婚育史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_parturitionTime"
                   	 	name="sampleInputTemp.parturitionTime" title="婚育史"   
						value="<s:property value="sampleInputTemp.parturitionTime"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >IVF</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_gestationIVF"
                   	 	name="sampleInputTemp.gestationIVF" title="IVF妊娠"   
						value="<s:property value="sampleInputTemp.gestationIVF"/>"
                   	 />
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >植入日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleInputTemp_acceptDate"
                   	 		name="sampleInputTemp.acceptDate" title="植入日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleInputTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
              	</tr>
                <tr>
				  <td colspan="9">
					<div class="standard-section-header type-title">
						<label>辅助检查</label>
					</div>
				  </td>
				</tr>
				<tr>
					<td class="label-title" >B超</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.embryoType" id="sampleInputTemp_embryoType" onChange="change()" >
<!-- 							<option value="" <s:if test="sampleInputTemp.embryoType==''">selected="selected" </s:if>>请选择</option> -->
	    					<option value="0" <s:if test="sampleInputTemp.embryoType==0">selected="selected" </s:if>>单胎</option>
	    					<option value="1" <s:if test="sampleInputTemp.embryoType==1">selected="selected" </s:if>>双胎</option>
	    					<option value="2" <s:if test="sampleInputTemp.embryoType==2">selected="selected" </s:if>>多胎</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
						<input type="text" size="20" maxlength="25" id="sampleInputTemp_messages"
	                   	 	name="sampleInputTemp.messages" title="异常提醒" value="<s:property value="sampleInputTemp.messages"/>"
	                   	   	style="display:none"/>
                   	</td>
                   	<td class="label-title" >筛查模式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.testPattern" id="sampleInputTemp_testPattern" >
<!-- 							<option value="" <s:if test="sampleInputTemp.testPattern==''">selected="selected" </s:if>>请选择</option> -->
	    					<option value="2" <s:if test="sampleInputTemp.testPattern==2">selected="selected" </s:if>>早中孕期联合筛查</option>
	    					<option value="0" <s:if test="sampleInputTemp.testPattern==0">selected="selected" </s:if>>未做</option>
	    					<option value="1" <s:if test="sampleInputTemp.testPattern==1">selected="selected" </s:if>>早孕期筛查</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
				<tr>
					<td class="label-title" >21-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome21Value"
	                   	 	name="sampleInputTemp.trisome21Value" title="21-三体比值"   
							value="<s:property value="sampleInputTemp.trisome21Value"/>"
                   	 	/>
                   	</td>
					<td class="label-title" >18-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome18Value"
	                   	 	name="sampleInputTemp.trisome18Value" title="18-三体比值"   
							value="<s:property value="sampleInputTemp.trisome18Value"/>"
	                   	 />
                   	</td>
                   	<td class="label-title" >13-三体比值</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_trisome13Value"
	                   	 	name="sampleInputTemp.trisome13Value" title="13-三体比值"   
							value="<s:property value="sampleInputTemp.trisome13Value"/>"
	                   	 />
                   	</td>
				</tr>
				<tr>
					<td class="label-title" >夫妻双方染色体</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleInputTemp.coupleChromosome" id="sampleInputTemp_coupleChromosome" >
<!-- 							<option value="" <s:if test="sampleInputTemp.coupleChromosome==''">selected="selected" </s:if>>请选择</option> -->
	    					<option value="1" <s:if test="sampleInputTemp.coupleChromosome==1">selected="selected" </s:if>>正常</option>
	    					<option value="0" <s:if test="sampleInputTemp.coupleChromosome==0">selected="selected" </s:if>>未做</option>
	    					<option value="2" <s:if test="sampleInputTemp.coupleChromosome==2">selected="selected" </s:if>>异常</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
				</tr>
				<tr>
					<td class="label-title" >孕妇签名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_patientName"
                   	 	name="" title="孕妇签名"   class="text input readonlytrue"  readonly="readonly" 
						value="<s:property value="sampleInputTemp.patientName"/>"
                   	 />
                   	</td>
				</tr>
				<tr>
					<td class="label-title" >申请医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleInputTemp_doctor"
                   	 	name="sampleInputTemp.doctor" title="申请医生"   
						value="<s:property value="sampleInputTemp.doctor"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >申请日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleInputTemp_applicatioDate"
                   	 		name="sampleInputTemp.applicatioDate" title="申请日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleInputTemp.applicatioDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
				</tr>
            </table>
            	<input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleInputTemp.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
