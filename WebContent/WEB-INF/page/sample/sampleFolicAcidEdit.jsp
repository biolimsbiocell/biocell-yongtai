
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleFolicAcidTemp&id=${sampleFolicAcidTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleFolicAcidEdit.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleFolicAcidTemp.upLoadAccessory.id}"></div>  
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleFolicAcidTemp.upLoadAccessory.id" value="<s:property value="sampleFolicAcidTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleFolicAcidTemp.upLoadAccessory.fileName" value="<s:property value="sampleFolicAcidTemp.upLoadAccessory.fileName"/>">
					<input type="hidden" id="saveType" value="ys" >
				</td>
			</tr>
				<tr>
               	 	<td class="label-title">编号</td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
                   		<input type="text" 	  id="sampleFolicAcidTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleFolicAcidTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleFolicAcidTemp.sampleInfo.code"/>" />
						<input type="hidden"  id="sampleFolicAcidTemp_id"  name="sampleFolicAcidTemp.sampleInfo.id"  value="<s:property value="sampleFolicAcidTemp.sampleInfo.id"/>" />
						<input type="hidden"  name="sampleFolicAcidTemp.id" value="<s:property value="sampleFolicAcidTemp.id"/>" />
						<input type="hidden"  id="upload_imga_name" name="sampleFolicAcidTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleFolicAcidTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleFolicAcidTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleFolicAcidTemp.sampleInfo.upLoadAccessory.id"/>">
                   	</td>
  	
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_name"
                   	 		name="sampleFolicAcidTemp.name" title="描述"   
							value="<s:property value="sampleFolicAcidTemp.name"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleFolicAcidTemp_product_name" searchField="true"  
 							name="sampleFolicAcidTemp.productName"  value="<s:property value="sampleFolicAcidTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleFolicAcidTemp_product_id" name="sampleFolicAcidTemp.productId"  
 							value="<s:property value="sampleFolicAcidTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   		
                     </tr>
                <tr>		
                   	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_acceptDate"
                   	 		name="sampleFolicAcidTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleFolicAcidTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 	/>
                   	</td>
             
                	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_area"
                   	 		name="sampleFolicAcidTemp.area" title="地区"  onblur="checkAdd()" 
							value="<s:property value="sampleFolicAcidTemp.area"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_hospital"
                   	 		name="sampleFolicAcidTemp.hospital" title="送检医院"   
							value="<s:property value="sampleFolicAcidTemp.hospital"/>"
                   	 />
                   	</td>
                   	   </tr>
                <tr>
                   	<td class="label-title" >送检时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_sendDate"
                   	 		name="sampleFolicAcidTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"  value="<s:date name="sampleFolicAcidTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
               
                	<td class="label-title" >出报告时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		 <input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_reportDate"
                   	 		name="sampleFolicAcidTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleFolicAcidTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	 	/>
                   	
                   	</td>
                   
                   	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleFolicAcidTemp_sampleType_name"  value="<s:property value="sampleFolicAcidTemp.sampleType.name"/>"/>
							<s:hidden id="sampleFolicAcidTemp_sampleType_id" name="sampleFolicAcidTemp.sampleType.id"></s:hidden>
							<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_patientName"
                   	 		name="sampleFolicAcidTemp.patientName" title="姓名"   
							value="<s:property value="sampleFolicAcidTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_patientNameSpell"
                   	 		name="sampleFolicAcidTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleFolicAcidTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
               		<td class="label-title" >手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_phoneNum"
                   	 		name="sampleFolicAcidTemp.phoneNum" title="手机号码"   onblur="checkPhone()"
							value="<s:property value="sampleFolicAcidTemp.phoneNum"/>"
                   	 	/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25"
							id="sampleFolicAcidTemp_address"
							name="sampleFolicAcidTemp.address" title="家庭住址"
							value="<s:property value="sampleFolicAcidTemp.address"/>" />
							<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >个人情况</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_otherTumorHistory"
                   	 		name="sampleFolicAcidTemp.otherTumorHistory" title="个人情况"   
							value="<s:property value="sampleFolicAcidTemp.otherTumorHistory"/>"
                   	 	/>
                   		</td>
                   	<td class="label-title" >孕次</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_pregnancyTime"
                   	 		name="sampleFolicAcidTemp.pregnancyTime" title="孕几次"   
							value="<s:property value="sampleFolicAcidTemp.pregnancyTime"/>"
                   	 	/>
                   	</td>
                   	
                   	
                </tr>
                <tr>
                	<td class="label-title" >产次</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_parturitionTime"
                   	 		name="sampleFolicAcidTemp.parturitionTime" title="孕几次"   
							value="<s:property value="sampleFolicAcidTemp.parturitionTime"/>"
                   	 	/>
                   	</td>
            
                   	<td class="label-title" >第几次产胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_confirmAgeOne"
                   	 		name="sampleFolicAcidTemp.confirmAgeOne" title="第几次产胎"   
							value="<s:property value="sampleFolicAcidTemp.confirmAgeOne"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_badMotherhood"
                   	 		name="sampleFolicAcidTemp.badMotherhood" title="孕几次"   
							value="<s:property value="sampleFolicAcidTemp.badMotherhood"/>"
                   	 	/>
                   	</td>
                </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>收费情况</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleFolicAcidTemp.isPay" id="sampleFolicAcidTemp_isPay" >
							<option value="" <s:if test="sampleFolicAcidTemp.isPay==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleFolicAcidTemp.isPay==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleFolicAcidTemp.isPay==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleFolicAcidTemp.isInvoice" id="sampleFolicAcidTemp_isInvoice" >
							<option value="" <s:if test="sampleFolicAcidTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleFolicAcidTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleFolicAcidTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		
                   	<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_createUser" class="text input readonlytrue" readonly="readonly"
                   	 	name="sampleFolicAcidTemp.createUser" title="录入人"   
						value="<s:property value="sampleFolicAcidTemp.createUser"/>"/>
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_reason"
                   	 		name="sampleFolicAcidTemp.reason" title="备注"   
							value="<s:property value="sampleFolicAcidTemp.reason"/>"
                   	 />
                   	</td>

                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_money"
                   	 		name="sampleFolicAcidTemp.money" title="备注"   
							value="<s:property value="sampleFolicAcidTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >收据/pos</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_receipt"
                   	 		name="sampleFolicAcidTemp.receipt" title="收据/pos"   
							value="<s:property value="sampleFolicAcidTemp.receipt"/>"
                   	 />
                   	</td>
                   	
                </tr>
                <tr>
                	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleFolicAcidTemp_paymentUnit"
                   	 		name="sampleFolicAcidTemp.paymentUnit" title="开票单位"   
							value="<s:property value="sampleFolicAcidTemp.paymentUnit"/>"
                   	 />
                   	</td>
              
                	<g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="reportManUserFun" 
 					hasSetFun="true"
					documentId="sampleFolicAcidTemp_reportMan"
					documentName="sampleFolicAcidTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleFolicAcidTemp_reportMan_name"  value="<s:property value="sampleFolicAcidTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleFolicAcidTemp_reportMan" name="sampleFolicAcidTemp.reportMan.id"  value="<s:property value="sampleFolicAcidTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="auditManUserFun" 
 					hasSetFun="true"
					documentId="sampleFolicAcidTemp_auditMan"
					documentName="sampleFolicAcidTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleFolicAcidTemp_auditMan_name"  value="<s:property value="sampleFolicAcidTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleFolicAcidTemp_auditMan" name="sampleFolicAcidTemp.auditMan.id"  value="<s:property value="sampleFolicAcidTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                </tr>
              
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleFolicAcid.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
