
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleBreastCancerTemp&id=${sampleBreastCancerTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleBreastCancerTempEditThree.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleBreastCancerTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
               	 	<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_id" name="sampleBreastCancerTemp.id" title="编号" value="<s:property value="sampleBreastCancerTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleBreastCancerTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   		<input type ="hidden" id="saveType" value="rxa" >
                   	</td>
    
    				<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_code" class="text input readonlytrue" readonly="readonly" name="breastCancerTempNew.sampleInfo.code" title="样本编号" value="<s:property value="breastCancerTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_code" class="text input readonlytrue" readonly="readonly" name="breastCancerTempNew.code" title="样本编号" value="<s:property value="breastCancerTempNew.code"/>" />
                   	</td>
                   	
                   	<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="breastCancerTempNew_name" name="breastCancerTempNew.name" title="描述" value="<s:property value="breastCancerTempNew.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_name" name="sampleBreastCancerTemp.name" title="描述" value="<s:property value="sampleBreastCancerTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_name" name="breastCancerTemp.name" title="描述" value="<s:property value="breastCancerTemp.name"/>"/>
                   	</td>
                   	
                   	<td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_productId" name="sampleBreastCancerTemp.productId"  value="<s:property value="sampleBreastCancerTemp.productId"/>"/>
  						<input type="hidden" id="sampleBreastCancerTemp_productName" name="sampleBreastCancerTemp.productName" value="<s:property value="sampleBreastCancerTemp.productName"/>"/>
  						<input type="hidden" id="breastCancerTemp_productId" name="breastCancerTemp.productId" value="<s:property value="breastCancerTemp.productId"/>">
  						<input type="hidden" id="breastCancerTemp_productName" name="breastCancerTemp.productName" value="<s:property value="breastCancerTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.productName"/>"><s:property value="sampleBreastCancerTemp.productName"/></option> 
							<option value="<s:property value="breastCancerTemp.productName"/>"><s:property value="breastCancerTemp.productName"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="breastCancerTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="breastCancerTempNew.productId" id="productIdNew" value="<s:property value="breastCancerTempNew.productId"/>" />
                   	</td>
                   	
                    </tr>
                <tr> 	
                   	<td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_acceptDate" name="sampleBreastCancerTemp.acceptDate" value="<s:property value="sampleBreastCancerTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_acceptDate" name="breastCancerTemp.acceptDate" value="<s:property value="breastCancerTemp.acceptDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.acceptDate"/>"><s:property value="sampleBreastCancerTemp.acceptDate"/></option> 
							<option value="<s:property value="breastCancerTemp.acceptDate"/>"><s:property value="breastCancerTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.acceptDate" id="acceptDateNew" value="<s:property value="breastCancerTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="breastCancerTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_area" name="sampleBreastCancerTemp.area"  value="<s:property value="sampleBreastCancerTemp.area"/>"/>
  						<input type="hidden" id="breastCancerTemp_area" name="breastCancerTemp.area" value="<s:property value="breastCancerTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.area"/>"><s:property value="sampleBreastCancerTemp.area"/></option> 
							<option value="<s:property value="breastCancerTemp.area"/>"><s:property value="breastCancerTemp.area"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.area" id="areaNew" value="<s:property value="breastCancerTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>

                   	<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_hospital" name="sampleBreastCancerTemp.hospital"  value="<s:property value="sampleBreastCancerTemp.hospital"/>"/>
  						<input type="hidden" id="breastCancerTemp_hospital" name="breastCancerTemp.hospital" value="<s:property value="breastCancerTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital" style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.hospital"/>"><s:property value="sampleBreastCancerTemp.hospital"/></option> 
							<option value="<s:property value="breastCancerTemp.hospital"/>"><s:property value="breastCancerTemp.hospital"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.hospital" id="hospitalNew" value="<s:property value="breastCancerTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
          
                  <tr>
                   	
                   	<td class="label-title">病历号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_serialNum" name="sampleBreastCancerTemp.serialNum"  value="<s:property value="sampleBreastCancerTemp.serialNum"/>"/>
  						<input type="hidden" id="breastCancerTemp_serialNum" name="breastCancerTemp.serialNum" value="<s:property value="breastCancerTemp.serialNum"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="serialNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('serialNumNew').value=document.getElementById('serialNum').options[document.getElementById('serialNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.serialNum"/>"><s:property value="sampleBreastCancerTemp.serialNum"/></option> 
							<option value="<s:property value="breastCancerTemp.serialNum"/>"><s:property value="breastCancerTemp.serialNum"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.serialNum" id="serialNumNew" value="<s:property value="breastCancerTempNew.serialNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">亲属编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_relationNum" name="sampleBreastCancerTemp.relationNum"  value="<s:property value="sampleBreastCancerTemp.relationNum"/>"/>
  						<input type="hidden" id="breastCancerTemp_relationNum" name="breastCancerTemp.relationNum" value="<s:property value="breastCancerTemp.relationNum"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="relationNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('relationNumNew').value=document.getElementById('relationNum').options[document.getElementById('relationNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.relationNum"/>"><s:property value="sampleBreastCancerTemp.relationNum"/></option> 
							<option value="<s:property value="breastCancerTemp.relationNum"/>"><s:property value="breastCancerTemp.relationNum"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.relationNum" id="relationNumNew" value="<s:property value="breastCancerTempNew.relationNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">亲属关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_relationship1" name="sampleBreastCancerTemp.relationship1" value="<s:property value="sampleBreastCancerTemp.relationship1"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_relationship1" name="breastCancerTemp.relationship1"  value="<s:property value="breastCancerTemp.relationship1"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.relationship1" id="breastCancerTempNew_relationship1">
							<option value="" <s:if test="breastCancerTempNew.relationship1==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.relationship1==0">selected="selected" </s:if>>否</option>
	    					<option value="1" <s:if test="breastCancerTempNew.relationship1==1">selected="selected" </s:if>>是</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_sendDate" name="sampleBreastCancerTemp.sendDate" value="<s:property value="sampleBreastCancerTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_sendDate" name="breastCancerTemp.sendDate" value="<s:property value="breastCancerTemp.sendDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.sendDate"/>"><s:property value="sampleBreastCancerTemp.sendDate"/></option> 
							<option value="<s:property value="breastCancerTemp.sendDate"/>"><s:property value="breastCancerTemp.sendDate"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.sendDate" id="sendDateNew" value="<s:property value="breastCancerTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})" value="<s:date name="breastCancerTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                    <td class="label-title">应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_reportDate" name="sampleBreastCancerTemp.reportDate" value="<s:property value="sampleBreastCancerTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_reportDate" name="breastCancerTemp.reportDate" value="<s:property value="breastCancerTemp.reportDate"/>" />
               	 	</td>
                   	<td align="left">
						<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.reportDate"/>"><s:property value="sampleBreastCancerTemp.reportDate"/></option> 
							<option value="<s:property value="breastCancerTemp.reportDate"/>"><s:property value="breastCancerTemp.reportDate"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.reportDate" id="reportDateNew" value="<s:property value="breastCancerTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="breastCancerTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                    
                    
<!--                     <td class="label-title">样本编号</td> -->
<%--                     <td class="requiredcolunm" nowrap width="10px" > </td> --%>
<!--                     <td align="left"> -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_sampleNum"  -->
<%--                    			name="sampleBreastCancerTemp.sampleNum" title="样本编号" value="<s:property value="sampleBreastCancerTemp.sampleNum"/>" --%>
<!--                    		/> -->
<!--                    </td> -->
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
				 <tr>
				 
                   	<td class="label-title">姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_patientName" name="sampleBreastCancerTemp.patientName"  value="<s:property value="sampleBreastCancerTemp.patientName"/>"/>
  						<input type="hidden" id="breastCancerTemp_patientName" name="breastCancerTemp.patientName" value="<s:property value="breastCancerTemp.patientName"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameNew').value=document.getElementById('patientName').options[document.getElementById('patientName').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.patientName"/>"><s:property value="sampleBreastCancerTemp.patientName"/></option> 
							<option value="<s:property value="breastCancerTemp.patientName"/>"><s:property value="breastCancerTemp.patientName"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.patientName" id="patientNameNew" value="<s:property value="breastCancerTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	
<!--                    	<td class="label-title" >姓名拼音</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_patientNameSpell" -->
<!--                    	 		name="sampleBreastCancerTemp.patientNameSpell" title="姓名拼音"    -->
<%-- 							value="<s:property value="sampleBreastCancerTemp.patientNameSpell"/>" --%>
<!--                    	 	/> -->
<!--                    	</td> -->
               		
                   	<td class="label-title">性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_gender" name="sampleBreastCancerTemp.gender" value="<s:property value="sampleBreastCancerTemp.gender"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_gender" name="breastCancerTemp.gender"  value="<s:property value="breastCancerTemp.gender"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.gender" id="breastCancerTempNew_gender">
							<option value="" <s:if test="breastCancerTempNew.gender==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.gender==0">selected="selected" </s:if>>女</option>
	    					<option value="1" <s:if test="breastCancerTempNew.gender==1">selected="selected" </s:if>>男</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title">年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_age" name="sampleBreastCancerTemp.age"  value="<s:property value="sampleBreastCancerTemp.age"/>"/>
  						<input type="hidden" id="breastCancerTemp_age" name="breastCancerTemp.age" value="<s:property value="breastCancerTemp.age"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="age" style="width:152px;height:28px;" onChange="javascript:document.getElementById('ageNew').value=document.getElementById('age').options[document.getElementById('age').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.age"/>"><s:property value="sampleBreastCancerTemp.age"/></option> 
							<option value="<s:property value="breastCancerTemp.age"/>"><s:property value="breastCancerTemp.age"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.age" id="ageNew" value="<s:property value="breastCancerTempNew.age"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_nativePlace" name="sampleBreastCancerTemp.nativePlace"  value="<s:property value="sampleBreastCancerTemp.nativePlace"/>"/>
  						<input type="hidden" id="breastCancerTemp_nativePlace" name="breastCancerTemp.nativePlace" value="<s:property value="breastCancerTemp.nativePlace"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="nativePlace" style="width:152px;height:28px;" onChange="javascript:document.getElementById('nativePlaceNew').value=document.getElementById('nativePlace').options[document.getElementById('nativePlace').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.nativePlace"/>"><s:property value="sampleBreastCancerTemp.nativePlace"/></option> 
							<option value="<s:property value="breastCancerTemp.nativePlace"/>"><s:property value="breastCancerTemp.nativePlace"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.nativePlace" id="nativePlaceNew" value="<s:property value="breastCancerTempNew.nativePlace"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_nationality" name="sampleBreastCancerTemp.nationality"  value="<s:property value="sampleBreastCancerTemp.nationality"/>"/>
  						<input type="hidden" id="breastCancerTemp_nationality" name="breastCancerTemp.nationality" value="<s:property value="breastCancerTemp.nationality"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="nationality" style="width:152px;height:28px;" onChange="javascript:document.getElementById('nationalityNew').value=document.getElementById('nationality').options[document.getElementById('nationality').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.nationality"/>"><s:property value="sampleBreastCancerTemp.nationality"/></option> 
							<option value="<s:property value="breastCancerTemp.nationality"/>"><s:property value="breastCancerTemp.nationality"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.nationality" id="nationalityNew" value="<s:property value="breastCancerTempNew.nationality"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	
                   	<td class="label-title">身高</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_heights" name="sampleBreastCancerTemp.heights"  value="<s:property value="sampleBreastCancerTemp.heights"/>"/>
  						<input type="hidden" id="breastCancerTemp_heights" name="breastCancerTemp.heights" value="<s:property value="breastCancerTemp.heights"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="heights" style="width:152px;height:28px;" onChange="javascript:document.getElementById('heightsNew').value=document.getElementById('heights').options[document.getElementById('heights').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.heights"/>"><s:property value="sampleBreastCancerTemp.heights"/></option> 
							<option value="<s:property value="breastCancerTemp.heights"/>"><s:property value="breastCancerTemp.heights"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.heights" id="heightsNew" value="<s:property value="breastCancerTempNew.heights"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">体重</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_weight" name="sampleBreastCancerTemp.weight"  value="<s:property value="sampleBreastCancerTemp.weight"/>"/>
  						<input type="hidden" id="breastCancerTemp_weight" name="breastCancerTemp.weight" value="<s:property value="breastCancerTemp.weight"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="weight" style="width:152px;height:28px;" onChange="javascript:document.getElementById('weightNew').value=document.getElementById('weight').options[document.getElementById('weight').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.weight"/>"><s:property value="sampleBreastCancerTemp.weight"/></option> 
							<option value="<s:property value="breastCancerTemp.weight"/>"><s:property value="breastCancerTemp.weight"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.weight" id="weightNew" value="<s:property value="breastCancerTempNew.weight"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">婚姻状况</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_maritalStatus" name="sampleBreastCancerTemp.maritalStatus" value="<s:property value="sampleBreastCancerTemp.maritalStatus"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_maritalStatus" name="breastCancerTemp.maritalStatus"  value="<s:property value="breastCancerTemp.maritalStatus"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.maritalStatus" id="breastCancerTempNew_maritalStatus">
							<option value="" <s:if test="breastCancerTempNew.maritalStatus==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.maritalStatus==0">selected="selected" </s:if>>已婚</option>
	    					<option value="1" <s:if test="breastCancerTempNew.maritalStatus==1">selected="selected" </s:if>>未婚</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                </tr>
                  <tr>
                   	<td class="label-title">联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_phone" name="sampleBreastCancerTemp.phone"  value="<s:property value="sampleBreastCancerTemp.phone"/>"/>
  						<input type="hidden" id="breastCancerTemp_phone" name="breastCancerTemp.phone" value="<s:property value="breastCancerTemp.phone"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phone" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNew').value=document.getElementById('phone').options[document.getElementById('phone').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.phone"/>"><s:property value="sampleBreastCancerTemp.phone"/></option> 
							<option value="<s:property value="breastCancerTemp.phone"/>"><s:property value="breastCancerTemp.phone"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.phone" id="phoneNew" value="<s:property value="breastCancerTempNew.phone"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
               
                   	<td class="label-title">电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_emailAddress" name="sampleBreastCancerTemp.emailAddress"  value="<s:property value="sampleBreastCancerTemp.emailAddress"/>"/>
  						<input type="hidden" id="breastCancerTemp_emailAddress" name="breastCancerTemp.emailAddress" value="<s:property value="breastCancerTemp.emailAddress"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="emailAddress" style="width:152px;height:28px;" onChange="javascript:document.getElementById('emailAddressNew').value=document.getElementById('emailAddress').options[document.getElementById('emailAddress').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.emailAddress"/>"><s:property value="sampleBreastCancerTemp.emailAddress"/></option> 
							<option value="<s:property value="breastCancerTemp.emailAddress"/>"><s:property value="breastCancerTemp.emailAddress"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.emailAddress" id="emailAddressNew" value="<s:property value="breastCancerTempNew.emailAddress"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_address" name="sampleBreastCancerTemp.address"  value="<s:property value="sampleBreastCancerTemp.address"/>"/>
  						<input type="hidden" id="breastCancerTemp_address" name="breastCancerTemp.address" value="<s:property value="breastCancerTemp.address"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="address" style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.address"/>"><s:property value="sampleBreastCancerTemp.address"/></option> 
							<option value="<s:property value="breastCancerTemp.address"/>"><s:property value="breastCancerTemp.address"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.address" id="addressNew" value="<s:property value="breastCancerTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">入选者类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_choiceType" name="sampleBreastCancerTemp.choiceType" value="<s:property value="sampleBreastCancerTemp.choiceType"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_choiceType" name="breastCancerTemp.choiceType"  value="<s:property value="breastCancerTemp.choiceType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.choiceType" id="breastCancerTempNew_choiceType">
							<option value="" <s:if test="breastCancerTempNew.choiceType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.choiceType==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="breastCancerTempNew.choiceType==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人及家族肿瘤病史</label>
						</div>
					</td>
				</tr>
               
                <tr>
                   	
                   	<td class="label-title">个人乳腺肿瘤病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_tumorHistory" name="sampleBreastCancerTemp.tumorHistory" value="<s:property value="sampleBreastCancerTemp.tumorHistory"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_tumorHistory" name="breastCancerTemp.tumorHistory"  value="<s:property value="breastCancerTemp.tumorHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.tumorHistory" id="breastCancerTempNew_tumorHistory">
							<option value="" <s:if test="breastCancerTempNew.tumorHistory==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.tumorHistory==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="breastCancerTempNew.tumorHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">乳腺肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_ovaryTumorType" name="sampleBreastCancerTemp.ovaryTumorType" value="<s:property value="sampleBreastCancerTemp.ovaryTumorType"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_ovaryTumorType" name="breastCancerTemp.ovaryTumorType"  value="<s:property value="breastCancerTemp.ovaryTumorType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.ovaryTumorType" id="breastCancerTempNew_ovaryTumorType">
							<option value="" <s:if test="breastCancerTempNew.ovaryTumorType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.ovaryTumorType==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="breastCancerTempNew.ovaryTumorType==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
        	
                   	<td class="label-title">确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_confirmAgeOne" name="sampleBreastCancerTemp.confirmAgeOne"  value="<s:property value="sampleBreastCancerTemp.confirmAgeOne"/>"/>
  						<input type="hidden" id="breastCancerTemp_confirmAgeOne" name="breastCancerTemp.confirmAgeOne" value="<s:property value="breastCancerTemp.confirmAgeOne"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAgeOne" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeOneNew').value=document.getElementById('confirmAgeOne').options[document.getElementById('confirmAgeOne').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.confirmAgeOne"/>"><s:property value="sampleBreastCancerTemp.confirmAgeOne"/></option> 
							<option value="<s:property value="breastCancerTemp.confirmAgeOne"/>"><s:property value="breastCancerTemp.confirmAgeOne"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.confirmAgeOne" id="confirmAgeOneNew" value="<s:property value="breastCancerTempNew.confirmAgeOne"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                 <tr>
                   	
                   	<td class="label-title">个人其他肿瘤病史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_otherTumorHistory" name="sampleBreastCancerTemp.otherTumorHistory" value="<s:property value="sampleBreastCancerTemp.otherTumorHistory"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_otherTumorHistory" name="breastCancerTemp.otherTumorHistory"  value="<s:property value="breastCancerTemp.otherTumorHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.otherTumorHistory" id="breastCancerTempNew_otherTumorHistory">
							<option value="" <s:if test="breastCancerTempNew.otherTumorHistory==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.otherTumorHistory==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="breastCancerTempNew.otherTumorHistory==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_tumorType" name="sampleBreastCancerTemp.tumorType" value="<s:property value="sampleBreastCancerTemp.tumorType"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_tumorType" name="breastCancerTemp.tumorType"  value="<s:property value="breastCancerTemp.tumorType"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.tumorType" id="breastCancerTempNew_tumorType">
							<option value="" <s:if test="breastCancerTempNew.tumorType==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.tumorType==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="breastCancerTempNew.tumorType==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_confirmAgeTwo" name="sampleBreastCancerTemp.confirmAgeTwo"  value="<s:property value="sampleBreastCancerTemp.confirmAgeTwo"/>"/>
  						<input type="hidden" id="breastCancerTemp_confirmAgeTwo" name="breastCancerTemp.confirmAgeTwo" value="<s:property value="breastCancerTemp.confirmAgeTwo"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="confirmAgeTwo" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeTwoNew').value=document.getElementById('confirmAgeTwo').options[document.getElementById('confirmAgeTwo').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.confirmAgeTwo"/>"><s:property value="sampleBreastCancerTemp.confirmAgeTwo"/></option> 
							<option value="<s:property value="breastCancerTemp.confirmAgeTwo"/>"><s:property value="breastCancerTemp.confirmAgeTwo"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.confirmAgeTwo" id="confirmAgeTwoNew" value="<s:property value="breastCancerTempNew.confirmAgeTwo"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                </tr>
                <tr>
                   	
                   	<td class="label-title">是否有亲属患肿瘤</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_isInvoice" name="sampleBreastCancerTemp.isInvoice" value="<s:property value="sampleBreastCancerTemp.isInvoice"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_isInvoice" name="breastCancerTemp.isInvoice"  value="<s:property value="breastCancerTemp.isInvoice"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.isInvoice" id="breastCancerTempNew_isInvoice">
							<option value="" <s:if test="breastCancerTempNew.isInvoice==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.isInvoice==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="breastCancerTempNew.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">肿瘤类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_tumorTypeOne" name="sampleBreastCancerTemp.tumorTypeOne" value="<s:property value="sampleBreastCancerTemp.tumorTypeOne"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_tumorTypeOne" name="breastCancerTemp.tumorTypeOne"  value="<s:property value="breastCancerTemp.tumorTypeOne"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.tumorTypeOne" id="breastCancerTempNew_tumorTypeOne">
							<option value="" <s:if test="breastCancerTempNew.tumorTypeOne==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.tumorTypeOne==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="breastCancerTempNew.tumorTypeOne==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">亲属关系</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_relationship2" name="sampleBreastCancerTemp.relationship2" value="<s:property value="sampleBreastCancerTemp.relationship2"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_relationship2" name="breastCancerTemp.relationship2"  value="<s:property value="breastCancerTemp.relationship2"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.relationship2" id="breastCancerTempNew_relationship2">
							<option value="" <s:if test="breastCancerTempNew.relationship==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.relationship==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="breastCancerTempNew.relationship==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                </tr>
                 <tr>
                   	
                   	<td class="label-title">确诊年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_confirmAgeThree" name="sampleBreastCancerTemp.confirmAgeThree"  value="<s:property value="sampleBreastCancerTemp.confirmAgeThree"/>"/>
  						<input type="hidden" id="breastCancerTemp_confirmAgeThree" name="breastCancerTemp.confirmAgeThree" value="<s:property value="breastCancerTemp.confirmAgeThree"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="confirmAgeThree" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeThreeNew').value=document.getElementById('confirmAgeThree').options[document.getElementById('confirmAgeThree').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.confirmAgeThree"/>"><s:property value="sampleBreastCancerTemp.confirmAgeThree"/></option> 
							<option value="<s:property value="breastCancerTemp.confirmAgeThree"/>"><s:property value="breastCancerTemp.confirmAgeThree"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.confirmAgeThree" id="confirmAgeThreeNew" value="<s:property value="breastCancerTempNew.confirmAgeThree"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人其他信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	
                   	<td class="label-title">初潮年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_confirmAge" name="sampleBreastCancerTemp.confirmAge"  value="<s:property value="sampleBreastCancerTemp.confirmAge"/>"/>
  						<input type="hidden" id="breastCancerTemp_confirmAge" name="breastCancerTemp.confirmAge" value="<s:property value="breastCancerTemp.confirmAge"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="confirmAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeNew').value=document.getElementById('confirmAge').options[document.getElementById('confirmAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.confirmAge"/>"><s:property value="sampleBreastCancerTemp.confirmAge"/></option> 
							<option value="<s:property value="breastCancerTemp.confirmAge"/>"><s:property value="breastCancerTemp.confirmAge"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.confirmAge" id="confirmAgeNew" value="<s:property value="breastCancerTempNew.confirmAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">月经周期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_menstrualCycle" name="sampleBreastCancerTemp.menstrualCycle"  value="<s:property value="sampleBreastCancerTemp.menstrualCycle"/>"/>
  						<input type="hidden" id="breastCancerTemp_menstrualCycle" name="breastCancerTemp.menstrualCycle" value="<s:property value="breastCancerTemp.menstrualCycle"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="menstrualCycle" style="width:152px;height:28px;" onChange="javascript:document.getElementById('menstrualCycleNew').value=document.getElementById('menstrualCycle').options[document.getElementById('menstrualCycle').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.menstrualCycle"/>"><s:property value="sampleBreastCancerTemp.menstrualCycle"/></option> 
							<option value="<s:property value="breastCancerTemp.menstrualCycle"/>"><s:property value="breastCancerTemp.menstrualCycle"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.menstrualCycle" id="menstrualCycleNew" value="<s:property value="breastCancerTempNew.menstrualCycle"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>	
                   	
                   	<td class="label-title">首次妊娠年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_firstGestationAge" name="sampleBreastCancerTemp.firstGestationAge"  value="<s:property value="sampleBreastCancerTemp.firstGestationAge"/>"/>
  						<input type="hidden" id="breastCancerTemp_firstGestationAge" name="breastCancerTemp.firstGestationAge" value="<s:property value="breastCancerTemp.firstGestationAge"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="firstGestationAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('firstGestationAgeNew').value=document.getElementById('firstGestationAge').options[document.getElementById('firstGestationAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.firstGestationAge"/>"><s:property value="sampleBreastCancerTemp.firstGestationAge"/></option> 
							<option value="<s:property value="breastCancerTemp.firstGestationAge"/>"><s:property value="breastCancerTemp.firstGestationAge"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.firstGestationAge" id="firstGestationAgeNew" value="<s:property value="breastCancerTempNew.firstGestationAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">孕次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_pregnancyTime" name="sampleBreastCancerTemp.pregnancyTime"  value="<s:property value="sampleBreastCancerTemp.pregnancyTime"/>"/>
  						<input type="hidden" id="breastCancerTemp_pregnancyTime" name="breastCancerTemp.pregnancyTime" value="<s:property value="breastCancerTemp.pregnancyTime"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="pregnancyTime" style="width:152px;height:28px;" onChange="javascript:document.getElementById('pregnancyTimeNew').value=document.getElementById('pregnancyTime').options[document.getElementById('pregnancyTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.pregnancyTime"/>"><s:property value="sampleBreastCancerTemp.pregnancyTime"/></option> 
							<option value="<s:property value="breastCancerTemp.pregnancyTime"/>"><s:property value="breastCancerTemp.pregnancyTime"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.pregnancyTime" id="pregnancyTimeNew" value="<s:property value="breastCancerTempNew.pregnancyTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">产次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_parturitionTime" name="sampleBreastCancerTemp.parturitionTime"  value="<s:property value="sampleBreastCancerTemp.parturitionTime"/>"/>
  						<input type="hidden" id="breastCancerTemp_parturitionTime" name="breastCancerTemp.parturitionTime" value="<s:property value="breastCancerTemp.parturitionTime"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="parturitionTime" style="width:152px;height:28px;" onChange="javascript:document.getElementById('parturitionTimeNew').value=document.getElementById('parturitionTime').options[document.getElementById('parturitionTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.parturitionTime"/>"><s:property value="sampleBreastCancerTemp.parturitionTime"/></option> 
							<option value="<s:property value="breastCancerTemp.parturitionTime"/>"><s:property value="breastCancerTemp.parturitionTime"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.parturitionTime" id="parturitionTimeNew" value="<s:property value="breastCancerTempNew.parturitionTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                   	<td class="label-title">个人哺乳史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_otherBreastHistory" name="sampleBreastCancerTemp.otherBreastHistory" value="<s:property value="sampleBreastCancerTemp.otherBreastHistory"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_otherBreastHistory" name="breastCancerTemp.otherBreastHistory"  value="<s:property value="breastCancerTemp.otherBreastHistory"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.otherBreastHistory" id="breastCancerTempNew_otherBreastHistory">
							<option value="" <s:if test="breastCancerTempNew.otherBreastHistory==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.otherBreastHistory==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="breastCancerTempNew.otherBreastHistory==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">哺乳次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_gestationalAge" name="sampleBreastCancerTemp.gestationalAge"  value="<s:property value="sampleBreastCancerTemp.gestationalAge"/>"/>
  						<input type="hidden" id="breastCancerTemp_gestationalAge" name="breastCancerTemp.gestationalAge" value="<s:property value="breastCancerTemp.gestationalAge"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="gestationalAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('gestationalAgeNew').value=document.getElementById('gestationalAge').options[document.getElementById('gestationalAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.gestationalAge"/>"><s:property value="sampleBreastCancerTemp.gestationalAge"/></option> 
							<option value="<s:property value="breastCancerTemp.gestationalAge"/>"><s:property value="breastCancerTemp.gestationalAge"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.gestationalAge" id="gestationalAgeNew" value="<s:property value="breastCancerTempNew.gestationalAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                
                   	<td class="label-title">绝经情况</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_gestationIVF" name="sampleBreastCancerTemp.gestationIVF" value="<s:property value="sampleBreastCancerTemp.gestationIVF"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_gestationIVF" name="breastCancerTemp.gestationIVF"  value="<s:property value="breastCancerTemp.gestationIVF"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.gestationIVF" id="breastCancerTempNew_gestationIVF">
							<option value="" <s:if test="breastCancerTempNew.gestationIVF==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.gestationIVF==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="breastCancerTempNew.gestationIVF==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title">绝经年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_menopauseAge" name="sampleBreastCancerTemp.menopauseAge"  value="<s:property value="sampleBreastCancerTemp.menopauseAge"/>"/>
  						<input type="hidden" id="breastCancerTemp_menopauseAge" name="breastCancerTemp.menopauseAge" value="<s:property value="breastCancerTemp.menopauseAge"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="menopauseAge" style="width:152px;height:28px;" onChange="javascript:document.getElementById('menopauseAgeNew').value=document.getElementById('menopauseAge').options[document.getElementById('menopauseAge').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.menopauseAge"/>"><s:property value="sampleBreastCancerTemp.menopauseAge"/></option> 
							<option value="<s:property value="breastCancerTemp.menopauseAge"/>"><s:property value="breastCancerTemp.menopauseAge"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.menopauseAge" id="menopauseAgeNew" value="<s:property value="breastCancerTempNew.menopauseAge"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">是否接受乳腺活检</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_trisome21Value" name="sampleBreastCancerTemp.trisome21Value" value="<s:property value="sampleBreastCancerTemp.trisome21Value"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_trisome21Value" name="breastCancerTemp.trisome21Value"  value="<s:property value="breastCancerTemp.trisome21Value"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.trisome21Value" id="breastCancerTempNew_trisome21Value">
							<option value="" <s:if test="breastCancerTempNew.trisome21Value==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.trisome21Value==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="breastCancerTempNew.trisome21Value==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">接受次数</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_acceptNum" name="sampleBreastCancerTemp.acceptNum"  value="<s:property value="sampleBreastCancerTemp.acceptNum"/>"/>
  						<input type="hidden" id="breastCancerTemp_acceptNum" name="breastCancerTemp.acceptNum" value="<s:property value="breastCancerTemp.acceptNum"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="acceptNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptNumNew').value=document.getElementById('acceptNum').options[document.getElementById('acceptNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.acceptNum"/>"><s:property value="sampleBreastCancerTemp.acceptNum"/></option> 
							<option value="<s:property value="breastCancerTemp.acceptNum"/>"><s:property value="breastCancerTemp.acceptNum"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.acceptNum" id="acceptNumNew" value="<s:property value="breastCancerTempNew.acceptNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                    
                    <td class="label-title">是否非典型乳腺增生</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_serialNum" name="sampleBreastCancerTemp.serialNum"  value="<s:property value="sampleBreastCancerTemp.serialNum"/>"/>
  						<input type="hidden" id="breastCancerTemp_serialNum" name="breastCancerTemp.serialNum" value="<s:property value="breastCancerTemp.serialNum"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="serialNum" style="width:152px;height:28px;" onChange="javascript:document.getElementById('serialNumNew').value=document.getElementById('serialNum').options[document.getElementById('serialNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.serialNum"/>"><s:property value="sampleBreastCancerTemp.serialNum"/></option> 
							<option value="<s:property value="breastCancerTemp.serialNum"/>"><s:property value="breastCancerTemp.serialNum"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.serialNum" id="serialNumNew" value="<s:property value="breastCancerTempNew.serialNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                    
                 </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>有毒、有害物质长期接触史</label>
						</div>
					</td>
				</tr>
                <tr>
                   	<td class="label-title">烟</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_cigarette" name="sampleBreastCancerTemp.cigarette"  value="<s:property value="sampleBreastCancerTemp.cigarette"/>"/>
  						<input type="hidden" id="breastCancerTemp_cigarette" name="breastCancerTemp.cigarette" value="<s:property value="breastCancerTemp.cigarette"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="cigarette" style="width:152px;height:28px;" onChange="javascript:document.getElementById('cigaretteNew').value=document.getElementById('cigarette').options[document.getElementById('cigarette').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.cigarette"/>"><s:property value="sampleBreastCancerTemp.cigarette"/></option> 
							<option value="<s:property value="breastCancerTemp.cigarette"/>"><s:property value="breastCancerTemp.cigarette"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.cigarette" id="cigaretteNew" value="<s:property value="breastCancerTempNew.cigarette"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">酒</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_wine" name="sampleBreastCancerTemp.wine"  value="<s:property value="sampleBreastCancerTemp.wine"/>"/>
  						<input type="hidden" id="breastCancerTemp_wine" name="breastCancerTemp.wine" value="<s:property value="breastCancerTemp.wine"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="wine" style="width:152px;height:28px;" onChange="javascript:document.getElementById('wineNew').value=document.getElementById('wine').options[document.getElementById('wine').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.wine"/>"><s:property value="sampleBreastCancerTemp.wine"/></option> 
							<option value="<s:property value="breastCancerTemp.wine"/>"><s:property value="breastCancerTemp.wine"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.wine" id="wineNew" value="<s:property value="breastCancerTempNew.wine"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	<td class="label-title">药物</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_medicine" name="sampleBreastCancerTemp.medicine"  value="<s:property value="sampleBreastCancerTemp.medicine"/>"/>
  						<input type="hidden" id="breastCancerTemp_medicine" name="breastCancerTemp.medicine" value="<s:property value="breastCancerTemp.medicine"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="medicine" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicineNew').value=document.getElementById('medicine').options[document.getElementById('medicine').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.medicine"/>"><s:property value="sampleBreastCancerTemp.medicine"/></option> 
							<option value="<s:property value="breastCancerTemp.medicine"/>"><s:property value="breastCancerTemp.medicine"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.medicine" id="medicineNew" value="<s:property value="breastCancerTempNew.medicine"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title">药物类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_medicineType" name="sampleBreastCancerTemp.medicineType"  value="<s:property value="sampleBreastCancerTemp.medicineType"/>"/>
  						<input type="hidden" id="breastCancerTemp_medicineType" name="breastCancerTemp.medicineType" value="<s:property value="breastCancerTemp.medicineType"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="medicineType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('medicineTypeNew').value=document.getElementById('medicineType').options[document.getElementById('medicineType').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.medicineType"/>"><s:property value="sampleBreastCancerTemp.medicineType"/></option> 
							<option value="<s:property value="breastCancerTemp.medicineType"/>"><s:property value="breastCancerTemp.medicineType"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.medicineType" id="medicineTypeNew" value="<s:property value="breastCancerTempNew.medicineType"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">放射线</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_radioactiveRays" name="sampleBreastCancerTemp.radioactiveRays"  value="<s:property value="sampleBreastCancerTemp.radioactiveRays"/>"/>
  						<input type="hidden" id="breastCancerTemp_radioactiveRays" name="breastCancerTemp.radioactiveRays" value="<s:property value="breastCancerTemp.radioactiveRays"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="radioactiveRays" style="width:152px;height:28px;" onChange="javascript:document.getElementById('radioactiveRaysNew').value=document.getElementById('radioactiveRays').options[document.getElementById('radioactiveRays').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.radioactiveRays"/>"><s:property value="sampleBreastCancerTemp.radioactiveRays"/></option> 
							<option value="<s:property value="breastCancerTemp.radioactiveRays"/>"><s:property value="breastCancerTemp.radioactiveRays"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.radioactiveRays" id="radioactiveRaysNew" value="<s:property value="breastCancerTempNew.radioactiveRays"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">农药</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_pesticide" name="sampleBreastCancerTemp.pesticide"  value="<s:property value="sampleBreastCancerTemp.pesticide"/>"/>
  						<input type="hidden" id="breastCancerTemp_pesticide" name="breastCancerTemp.pesticide" value="<s:property value="breastCancerTemp.pesticide"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="pesticide" style="width:152px;height:28px;" onChange="javascript:document.getElementById('pesticideNew').value=document.getElementById('pesticide').options[document.getElementById('pesticide').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.pesticide"/>"><s:property value="sampleBreastCancerTemp.pesticide"/></option> 
							<option value="<s:property value="breastCancerTemp.pesticide"/>"><s:property value="breastCancerTemp.pesticide"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.pesticide" id="pesticideNew" value="<s:property value="breastCancerTempNew.pesticide"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title">铅</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_plumbane" name="sampleBreastCancerTemp.plumbane"  value="<s:property value="sampleBreastCancerTemp.plumbane"/>"/>
  						<input type="hidden" id="breastCancerTemp_plumbane" name="breastCancerTemp.plumbane" value="<s:property value="breastCancerTemp.plumbane"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="plumbane" style="width:152px;height:28px;" onChange="javascript:document.getElementById('plumbaneNew').value=document.getElementById('plumbane').options[document.getElementById('plumbane').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.plumbane"/>"><s:property value="sampleBreastCancerTemp.plumbane"/></option> 
							<option value="<s:property value="breastCancerTemp.plumbane"/>"><s:property value="breastCancerTemp.plumbane"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.plumbane" id="plumbaneNew" value="<s:property value="breastCancerTempNew.plumbane"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">汞</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_mercury" name="sampleBreastCancerTemp.mercury"  value="<s:property value="sampleBreastCancerTemp.mercury"/>"/>
  						<input type="hidden" id="breastCancerTemp_mercury" name="breastCancerTemp.mercury" value="<s:property value="breastCancerTemp.mercury"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="mercury" style="width:152px;height:28px;" onChange="javascript:document.getElementById('mercuryNew').value=document.getElementById('mercury').options[document.getElementById('mercury').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.mercury"/>"><s:property value="sampleBreastCancerTemp.mercury"/></option> 
							<option value="<s:property value="breastCancerTemp.mercury"/>"><s:property value="breastCancerTemp.mercury"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.mercury" id="mercuryNew" value="<s:property value="breastCancerTempNew.mercury"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">镉</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_cadmium" name="sampleBreastCancerTemp.cadmium"  value="<s:property value="sampleBreastCancerTemp.cadmium"/>"/>
  						<input type="hidden" id="breastCancerTemp_cadmium" name="breastCancerTemp.cadmium" value="<s:property value="breastCancerTemp.cadmium"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="cadmium" style="width:152px;height:28px;" onChange="javascript:document.getElementById('cadmiumNew').value=document.getElementById('cadmium').options[document.getElementById('cadmium').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.cadmium"/>"><s:property value="sampleBreastCancerTemp.cadmium"/></option> 
							<option value="<s:property value="breastCancerTemp.cadmium"/>"><s:property value="breastCancerTemp.cadmium"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.cadmium" id="cadmiumNew" value="<s:property value="breastCancerTempNew.cadmium"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title">其它有害物质</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_otherJunk" name="sampleBreastCancerTemp.otherJunk"  value="<s:property value="sampleBreastCancerTemp.otherJunk"/>"/>
  						<input type="hidden" id="breastCancerTemp_otherJunk" name="breastCancerTemp.otherJunk" value="<s:property value="breastCancerTemp.otherJunk"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="otherJunk" style="width:152px;height:28px;" onChange="javascript:document.getElementById('otherJunkNew').value=document.getElementById('otherJunk').options[document.getElementById('otherJunk').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.otherJunk"/>"><s:property value="sampleBreastCancerTemp.otherJunk"/></option> 
							<option value="<s:property value="breastCancerTemp.otherJunk"/>"><s:property value="breastCancerTemp.otherJunk"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.otherJunk" id="otherJunkNew" value="<s:property value="breastCancerTempNew.otherJunk"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">乳房自检</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_breastExa" name="sampleBreastCancerTemp.breastExa"  value="<s:property value="sampleBreastCancerTemp.breastExa"/>"/>
  						<input type="hidden" id="breastCancerTemp_breastExa" name="breastCancerTemp.breastExa" value="<s:property value="breastCancerTemp.breastExa"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="breastExa" style="width:152px;height:28px;" onChange="javascript:document.getElementById('breastExaNew').value=document.getElementById('breastExa').options[document.getElementById('breastExa').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.breastExa"/>"><s:property value="sampleBreastCancerTemp.breastExa"/></option> 
							<option value="<s:property value="breastCancerTemp.breastExa"/>"><s:property value="breastCancerTemp.breastExa"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.breastExa" id="breastExaNew" value="<s:property value="breastCancerTempNew.breastExa"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">病理类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_pathologyType" name="sampleBreastCancerTemp.pathologyType"  value="<s:property value="sampleBreastCancerTemp.pathologyType"/>"/>
  						<input type="hidden" id="breastCancerTemp_pathologyType" name="breastCancerTemp.pathologyType" value="<s:property value="breastCancerTemp.pathologyType"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="pathologyType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('pathologyTypeNew').value=document.getElementById('pathologyType').options[document.getElementById('pathologyType').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.pathologyType"/>"><s:property value="sampleBreastCancerTemp.pathologyType"/></option> 
							<option value="<s:property value="breastCancerTemp.pathologyType"/>"><s:property value="breastCancerTemp.pathologyType"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.pathologyType" id="pathologyTypeNew" value="<s:property value="breastCancerTempNew.pathologyType"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                	<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>TNM分期</label>
						</div>
					</td>
				</tr>	
                  <tr>
                   	<td class="label-title">T</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_t" name="sampleBreastCancerTemp.t"  value="<s:property value="sampleBreastCancerTemp.t"/>"/>
  						<input type="hidden" id="breastCancerTemp_t" name="breastCancerTemp.t" value="<s:property value="breastCancerTemp.t"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="t" style="width:152px;height:28px;" onChange="javascript:document.getElementById('tNew').value=document.getElementById('t').options[document.getElementById('t').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.t"/>"><s:property value="sampleBreastCancerTemp.t"/></option> 
							<option value="<s:property value="breastCancerTemp.t"/>"><s:property value="breastCancerTemp.t"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.t" id="tNew" value="<s:property value="breastCancerTempNew.t"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">N</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_n" name="sampleBreastCancerTemp.n"  value="<s:property value="sampleBreastCancerTemp.n"/>"/>
  						<input type="hidden" id="breastCancerTemp_n" name="breastCancerTemp.t" value="<s:property value="breastCancerTemp.n"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="n" style="width:152px;height:28px;" onChange="javascript:document.getElementById('nNew').value=document.getElementById('n').options[document.getElementById('n').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.n"/>"><s:property value="sampleBreastCancerTemp.n"/></option> 
							<option value="<s:property value="breastCancerTemp.n"/>"><s:property value="breastCancerTemp.n"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.n" id="nNew" value="<s:property value="breastCancerTempNew.n"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">M</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_m" name="sampleBreastCancerTemp.m"  value="<s:property value="sampleBreastCancerTemp.m"/>"/>
  						<input type="hidden" id="breastCancerTemp_m" name="breastCancerTemp.m" value="<s:property value="breastCancerTemp.m"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="m" style="width:152px;height:28px;" onChange="javascript:document.getElementById('mNew').value=document.getElementById('m').options[document.getElementById('m').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.m"/>"><s:property value="sampleBreastCancerTemp.m"/></option> 
							<option value="<s:property value="breastCancerTemp.m"/>"><s:property value="breastCancerTemp.m"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.m" id="mNew" value="<s:property value="breastCancerTempNew.m"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">分期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_staging" name="sampleBreastCancerTemp.staging"  value="<s:property value="sampleBreastCancerTemp.staging"/>"/>
  						<input type="hidden" id="breastCancerTemp_staging" name="breastCancerTemp.staging" value="<s:property value="breastCancerTemp.staging"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="staging" style="width:152px;height:28px;" onChange="javascript:document.getElementById('stagingNew').value=document.getElementById('staging').options[document.getElementById('staging').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.staging"/>"><s:property value="sampleBreastCancerTemp.staging"/></option> 
							<option value="<s:property value="breastCancerTemp.staging"/>"><s:property value="breastCancerTemp.staging"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.staging" id="stagingNew" value="<s:property value="breastCancerTempNew.staging"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>免疫组化验检查</label>
						</div>
					</td>
				</tr>	
                  <tr>
                   	
                   	<td class="label-title">ER</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_er" name="sampleBreastCancerTemp.er"  value="<s:property value="sampleBreastCancerTemp.er"/>"/>
  						<input type="hidden" id="breastCancerTemp_er" name="breastCancerTemp.er" value="<s:property value="breastCancerTemp.er"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="er" style="width:152px;height:28px;" onChange="javascript:document.getElementById('erNew').value=document.getElementById('er').options[document.getElementById('er').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.er"/>"><s:property value="sampleBreastCancerTemp.er"/></option> 
							<option value="<s:property value="breastCancerTemp.er"/>"><s:property value="breastCancerTemp.er"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.er" id="erNew" value="<s:property value="breastCancerTempNew.er"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">PR</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_pr" name="sampleBreastCancerTemp.pr"  value="<s:property value="sampleBreastCancerTemp.pr"/>"/>
  						<input type="hidden" id="breastCancerTemp_pr" name="breastCancerTemp.pr" value="<s:property value="breastCancerTemp.pr"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="pr" style="width:152px;height:28px;" onChange="javascript:document.getElementById('prNew').value=document.getElementById('pr').options[document.getElementById('pr').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.pr"/>"><s:property value="sampleBreastCancerTemp.pr"/></option> 
							<option value="<s:property value="breastCancerTemp.pr"/>"><s:property value="breastCancerTemp.pr"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.pr" id="prNew" value="<s:property value="breastCancerTempNew.pr"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">Her2</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_Her2" name="sampleBreastCancerTemp.Her2"  value="<s:property value="sampleBreastCancerTemp.Her2"/>"/>
  						<input type="hidden" id="breastCancerTemp_Her2" name="breastCancerTemp.Her2" value="<s:property value="breastCancerTemp.Her2"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="Her2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('Her2New').value=document.getElementById('Her2').options[document.getElementById('Her2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.Her2"/>"><s:property value="sampleBreastCancerTemp.Her2"/></option> 
							<option value="<s:property value="breastCancerTemp.Her2"/>"><s:property value="breastCancerTemp.Her2"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.Her2" id="Her2New" value="<s:property value="breastCancerTempNew.Her2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">FISH</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_fish" name="sampleBreastCancerTemp.fish"  value="<s:property value="sampleBreastCancerTemp.fish"/>"/>
  						<input type="hidden" id="breastCancerTemp_fish" name="breastCancerTemp.fish" value="<s:property value="breastCancerTemp.fish"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="fish" style="width:152px;height:28px;" onChange="javascript:document.getElementById('fishNew').value=document.getElementById('fish').options[document.getElementById('fish').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.fish"/>"><s:property value="sampleBreastCancerTemp.fish"/></option> 
							<option value="<s:property value="breastCancerTemp.fish"/>"><s:property value="breastCancerTemp.fish"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.fish" id="fishNew" value="<s:property value="breastCancerTempNew.fish"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">Ki67</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_ki" name="sampleBreastCancerTemp.ki"  value="<s:property value="sampleBreastCancerTemp.ki"/>"/>
  						<input type="hidden" id="breastCancerTemp_ki" name="breastCancerTemp.ki" value="<s:property value="breastCancerTemp.ki"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="ki" style="width:152px;height:28px;" onChange="javascript:document.getElementById('kiNew').value=document.getElementById('ki').options[document.getElementById('ki').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.ki"/>"><s:property value="sampleBreastCancerTemp.ki"/></option> 
							<option value="<s:property value="breastCancerTemp.ki"/>"><s:property value="breastCancerTemp.ki"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.ki" id="kiNew" value="<s:property value="breastCancerTempNew.ki"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">P53</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_p53" name="sampleBreastCancerTemp.p53"  value="<s:property value="sampleBreastCancerTemp.p53"/>"/>
  						<input type="hidden" id="breastCancerTemp_p53" name="breastCancerTemp.p53" value="<s:property value="breastCancerTemp.p53"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="p53" style="width:152px;height:28px;" onChange="javascript:document.getElementById('p53New').value=document.getElementById('p53').options[document.getElementById('p53').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.p53"/>"><s:property value="sampleBreastCancerTemp.p53"/></option> 
							<option value="<s:property value="breastCancerTemp.p53"/>"><s:property value="breastCancerTemp.p53"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.p53" id="p53New" value="<s:property value="breastCancerTempNew.p53"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                   	
                   	<td class="label-title">癌胚抗原（CEA）（ug/L）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_antigen1" name="sampleBreastCancerTemp.antigen1"  value="<s:property value="sampleBreastCancerTemp.antigen1"/>"/>
  						<input type="hidden" id="breastCancerTemp_antigen1" name="breastCancerTemp.antigen1" value="<s:property value="breastCancerTemp.antigen1"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="antigen1" style="width:152px;height:28px;" onChange="javascript:document.getElementById('antigen1New').value=document.getElementById('antigen1').options[document.getElementById('antigen1').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.antigen1"/>"><s:property value="sampleBreastCancerTemp.antigen1"/></option> 
							<option value="<s:property value="breastCancerTemp.antigen1"/>"><s:property value="breastCancerTemp.antigen1"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.antigen1" id="antigen1New" value="<s:property value="breastCancerTempNew.antigen1"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">癌抗原125（CA125）（U/ml）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_antigen2" name="sampleBreastCancerTemp.antigen2"  value="<s:property value="sampleBreastCancerTemp.antigen2"/>"/>
  						<input type="hidden" id="breastCancerTemp_antigen2" name="breastCancerTemp.antigen2" value="<s:property value="breastCancerTemp.antigen2"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="antigen2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('antigen2New').value=document.getElementById('antigen2').options[document.getElementById('antigen2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.antigen2"/>"><s:property value="sampleBreastCancerTemp.antigen2"/></option> 
							<option value="<s:property value="breastCancerTemp.antigen2"/>"><s:property value="breastCancerTemp.antigen2"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.antigen2" id="antigen2New" value="<s:property value="breastCancerTempNew.antigen2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">癌抗原15-3（CA15-3）（U/ml）</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_antigen3" name="sampleBreastCancerTemp.antigen3"  value="<s:property value="sampleBreastCancerTemp.antigen3"/>"/>
  						<input type="hidden" id="breastCancerTemp_antigen3" name="breastCancerTemp.antigen3" value="<s:property value="breastCancerTemp.antigen3"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="antigen3" style="width:152px;height:28px;" onChange="javascript:document.getElementById('antigen3New').value=document.getElementById('antigen3').options[document.getElementById('antigen3').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.antigen3"/>"><s:property value="sampleBreastCancerTemp.antigen3"/></option> 
							<option value="<s:property value="breastCancerTemp.antigen3"/>"><s:property value="breastCancerTemp.antigen3"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.antigen3" id="antigen3New" value="<s:property value="breastCancerTempNew.antigen3"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>影像学筛查</label>
						</div>
					</td>
				</tr>	
                  <tr>
                   	
                   	<td class="label-title">X线检查所见</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_roentgenFindings1" name="sampleBreastCancerTemp.roentgenFindings1"  value="<s:property value="sampleBreastCancerTemp.roentgenFindings1"/>"/>
  						<input type="hidden" id="breastCancerTemp_roentgenFindings1" name="breastCancerTemp.roentgenFindings1" value="<s:property value="breastCancerTemp.roentgenFindings1"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="roentgenFindings1" style="width:152px;height:28px;" onChange="javascript:document.getElementById('roentgenFindings1New').value=document.getElementById('roentgenFindings1').options[document.getElementById('roentgenFindings1').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.roentgenFindings1"/>"><s:property value="sampleBreastCancerTemp.roentgenFindings1"/></option> 
							<option value="<s:property value="breastCancerTemp.roentgenFindings1"/>"><s:property value="breastCancerTemp.roentgenFindings1"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.roentgenFindings1" id="roentgenFindings1New" value="<s:property value="breastCancerTempNew.roentgenFindings1"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">分级</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_rate1" name="sampleBreastCancerTemp.rate1"  value="<s:property value="sampleBreastCancerTemp.rate1"/>"/>
  						<input type="hidden" id="breastCancerTemp_rate1" name="breastCancerTemp.rate1" value="<s:property value="breastCancerTemp.rate1"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="rate1" style="width:152px;height:28px;" onChange="javascript:document.getElementById('rate1New').value=document.getElementById('rate1').options[document.getElementById('rate1').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.rate1"/>"><s:property value="sampleBreastCancerTemp.rate1"/></option> 
							<option value="<s:property value="breastCancerTemp.rate1"/>"><s:property value="breastCancerTemp.rate1"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.rate1" id="rate1New" value="<s:property value="breastCancerTempNew.rate1"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">B超检查所见</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_roentgenFindings2" name="sampleBreastCancerTemp.roentgenFindings2"  value="<s:property value="sampleBreastCancerTemp.roentgenFindings2"/>"/>
  						<input type="hidden" id="breastCancerTemp_roentgenFindings2" name="breastCancerTemp.roentgenFindings2" value="<s:property value="breastCancerTemp.roentgenFindings2"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="roentgenFindings2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('roentgenFindings2New').value=document.getElementById('roentgenFindings2').options[document.getElementById('roentgenFindings2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.roentgenFindings2"/>"><s:property value="sampleBreastCancerTemp.roentgenFindings2"/></option> 
							<option value="<s:property value="breastCancerTemp.roentgenFindings2"/>"><s:property value="breastCancerTemp.roentgenFindings2"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.roentgenFindings2" id="roentgenFindings2New" value="<s:property value="breastCancerTempNew.roentgenFindings2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                  </tr>
                  <tr>
                  	<td class="label-title">分级</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_rate2" name="sampleBreastCancerTemp.rate2"  value="<s:property value="sampleBreastCancerTemp.rate2"/>"/>
  						<input type="hidden" id="breastCancerTemp_rate2" name="breastCancerTemp.rate2" value="<s:property value="breastCancerTemp.rate2"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="rate2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('rate2New').value=document.getElementById('rate2').options[document.getElementById('rate2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.rate2"/>"><s:property value="sampleBreastCancerTemp.rate2"/></option> 
							<option value="<s:property value="breastCancerTemp.rate2"/>"><s:property value="breastCancerTemp.rate2"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.rate2" id="rate2New" value="<s:property value="breastCancerTempNew.rate2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
					<td class="label-title">MRI检查所见</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_roentgenFindings3" name="sampleBreastCancerTemp.roentgenFindings3"  value="<s:property value="sampleBreastCancerTemp.roentgenFindings3"/>"/>
  						<input type="hidden" id="breastCancerTemp_roentgenFindings3" name="breastCancerTemp.roentgenFindings3" value="<s:property value="breastCancerTemp.roentgenFindings3"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="roentgenFindings3" style="width:152px;height:28px;" onChange="javascript:document.getElementById('roentgenFindings3New').value=document.getElementById('roentgenFindings3').options[document.getElementById('roentgenFindings3').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.roentgenFindings3"/>"><s:property value="sampleBreastCancerTemp.roentgenFindings3"/></option> 
							<option value="<s:property value="breastCancerTemp.roentgenFindings3"/>"><s:property value="breastCancerTemp.roentgenFindings3"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.roentgenFindings3" id="roentgenFindings3New" value="<s:property value="breastCancerTempNew.roentgenFindings3"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>                   	
                   	
                   	<td class="label-title">分级</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_rate3" name="sampleBreastCancerTemp.rate3"  value="<s:property value="sampleBreastCancerTemp.rate3"/>"/>
  						<input type="hidden" id="breastCancerTemp_rate3" name="breastCancerTemp.rate3" value="<s:property value="breastCancerTemp.rate3"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="rate3" style="width:152px;height:28px;" onChange="javascript:document.getElementById('rate3New').value=document.getElementById('rate3').options[document.getElementById('rate3').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.rate3"/>"><s:property value="sampleBreastCancerTemp.rate3"/></option> 
							<option value="<s:property value="breastCancerTemp.rate3"/>"><s:property value="breastCancerTemp.rate3"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.rate3" id="rate3New" value="<s:property value="breastCancerTempNew.rate3"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>信息确认核对</label>
						</div>
					</td>
				</tr>	
                  <tr>
                   	
                   	<td class="label-title">填写人</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_signed" name="sampleBreastCancerTemp.signed"  value="<s:property value="sampleBreastCancerTemp.signed"/>"/>
  						<input type="hidden" id="breastCancerTemp_signed" name="breastCancerTemp.signed" value="<s:property value="breastCancerTemp.signed"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="signed" style="width:152px;height:28px;" onChange="javascript:document.getElementById('signedNew').value=document.getElementById('signed').options[document.getElementById('signed').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.signed"/>"><s:property value="sampleBreastCancerTemp.signed"/></option> 
							<option value="<s:property value="breastCancerTemp.signed"/>"><s:property value="breastCancerTemp.signed"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.signed" id="signedNew" value="<s:property value="breastCancerTempNew.signed"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td> 
                   	
                   	<td class="label-title">填写人电话</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_phoneNum2" name="sampleBreastCancerTemp.phoneNum2"  value="<s:property value="sampleBreastCancerTemp.phoneNum2"/>"/>
  						<input type="hidden" id="breastCancerTemp_phoneNum2" name="breastCancerTemp.phoneNum2" value="<s:property value="breastCancerTemp.phoneNum2"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="phoneNum2" style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNum2New').value=document.getElementById('phoneNum2').options[document.getElementById('phoneNum2').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.phoneNum2"/>"><s:property value="sampleBreastCancerTemp.phoneNum2"/></option> 
							<option value="<s:property value="breastCancerTemp.phoneNum2"/>"><s:property value="breastCancerTemp.phoneNum2"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.phoneNum2" id="phoneNum2New" value="<s:property value="breastCancerTempNew.phoneNum2"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_isFee" name="sampleBreastCancerTemp.isFee" value="<s:property value="sampleBreastCancerTemp.isFee"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="breastCancerTemp_isFee" name="breastCancerTemp.isFee"  value="<s:property value="breastCancerTemp.isFee"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.isFee" id="breastCancerTempNew_isFee">
							<option value="" <s:if test="breastCancerTempNew.isFee==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="breastCancerTempNew.isFee==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="breastCancerTempNew.isFee==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                  </tr>
                  <tr>
<!--                   	<td class="label-title" >录入人</td> -->
<%--                	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	 --%>
<!--                    	<td align="left"  > -->
<!--                    		<input type="hidden" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_id" -->
<!--                    	 		name="sampleBreastCancer.createUser.id" title="录入人"    -->
<!--                    	 	/> -->
<!--                    		<input type="text" size="20" maxlength="25" id="sampleBreastCancerTemp_createUser_name" class="text input readonlytrue" readonly="readonly" -->
<!--                    	 		name="sampleBreastCancerTemp.createUser.name" title="录入人"    -->
<%-- 							value="<s:property value="sampleBreastCancerTemp.createUser.name"/>" --%>
<!--                    	 	/> -->
<!--                    	</td> -->
                   	
                   	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="breastCancerTempNew_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="breastCancerTempNew.createUser" title="审核入人"   
							value="<s:property value="breastCancerTempNew.createUser"/>"/>
                   	</td>
                   	
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_note1" name="sampleBreastCancerTemp.note1"  value="<s:property value="sampleBreastCancerTemp.note1"/>"/>
  						<input type="hidden" id="breastCancerTemp_note1" name="breastCancerTemp.note1" value="<s:property value="breastCancerTemp.note1"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="note1" style="width:152px;height:28px;" onChange="javascript:document.getElementById('note1New').value=document.getElementById('note1').options[document.getElementById('note1').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.note1"/>"><s:property value="sampleBreastCancerTemp.note1"/></option> 
							<option value="<s:property value="breastCancerTemp.note1"/>"><s:property value="breastCancerTemp.note1"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.note1" id="note1New" value="<s:property value="breastCancerTempNew.note1"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
                	<g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
						hasHtmlFrame="true"
						html="${ctx}/core/user/userSelect.action"
						isHasSubmit="false" functionName="reportManUserFun" 
		 				hasSetFun="true"
						documentId="reportManIdNew"
						documentName="reportManNew" />
                   	
                   	<td class="label-title">核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_reportMan_id" name="sampleBreastCancerTemp.reportMan.id"  value="<s:property value="sampleBreastCancerTemp.reportMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleBreastCancerTemp_reportMan_name" name="sampleBreastCancerTemp.reportMan.name"  value="<s:property value="sampleBreastCancerTemp.reportMan.name"/>" class="text input" />
  						<input type="hidden" id="breastCancerTemp_reportMan_id" name="breastCancerTemp.reportMan.id" value="<s:property value="breastCancerTemp.reportMan.id"/>">
  						<input type="hidden" id="breastCancerTemp_reportMan_name" name="breastCancerTemp.reportMan.name" value="<s:property value="breastCancerTemp.reportMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reportMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportManNew').value=document.getElementById('reportMan').options[document.getElementById('reportMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.reportMan.name"/>"><s:property value="sampleBreastCancerTemp.reportMan.name"/></option> 
							<option value="<s:property value="breastCancerTemp.reportMan.name"/>"><s:property value="breastCancerTemp.reportMan.name"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.reportMan.name" id="reportManNew" value="<s:property value="breastCancerTempNew.reportMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="breastCancerTempNew.reportMan.id" id="reportManIdNew" value="<s:property value="breastCancerTempNew.reportMan.id"/>"/>
                   	</td>
                   	<th><img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    /></th>
                   	<td></td>
                   	<td></td>
                   	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="breastCancerTempNew.nextStepFlow" id="breastCancerTempNew_nextStepFlow">
							<option value="" <s:if test="breastCancerTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="breastCancerTempNew.nextStepFlow==1">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="breastCancerTempNew.nextStepFlow==0">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
				<tr>
					 <g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="auditManUserFun" 
	 				hasSetFun="true"
					documentId="auditManIdNew"
					documentName="auditManNew" />
			
                   	<td class="label-title">核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleBreastCancerTemp_auditMan_id" name="sampleBreastCancerTemp.auditMan.id"  value="<s:property value="sampleBreastCancerTemp.auditMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleBreastCancerTemp_auditMan_name" name="sampleBreastCancerTemp.auditMan.name"  value="<s:property value="sampleBreastCancerTemp.auditMan.name"/>" class="text input" />
  						<input type="hidden" id="breastCancerTemp_auditMan_id" name="breastCancerTemp.auditMan.id" value="<s:property value="breastCancerTemp.auditMan.id"/>">
  						<input type="hidden" id="breastCancerTemp_auditMan_name" name="breastCancerTemp.auditMan.name" value="<s:property value="breastCancerTemp.auditMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="auditMan" style="width:152px;height:28px;" onChange="javascript:document.getElementById('auditManNew').value=document.getElementById('auditMan').options[document.getElementById('auditMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleBreastCancerTemp.auditMan.name"/>"><s:property value="sampleBreastCancerTemp.auditMan.name"/></option> 
							<option value="<s:property value="breastCancerTemp.auditMan.name"/>"><s:property value="breastCancerTemp.auditMan.name"/></option>
						</select> 
						<input type="text" name="breastCancerTempNew.auditMan.name" id="auditManNew" value="<s:property value="breastCancerTempNew.auditMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="breastCancerTempNew.auditMan.id" id="auditManIdNew" value="<s:property value="breastCancerTempNew.auditMan.id"/>"/>
                   	</td>
                   	<th><img alt='选择核对人' id=showauditMan src='${ctx}/images/img_lookup.gif' 	class='detail'    /></th>
				</tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleBreastCancer.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
