
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleTumorTemp&id=${sampleTumorTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleTumorEditThree.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:640px;float:left;" id="sampleInputItemImg"><img id="upLoadImg" src="${ctx}/operfile/downloadById.action?id=${sampleTumorTemp.upLoadAccessory.id}"></div>
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
               	 	<td class="label-title">编号</td>
                     	<td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
	                   	<input type="text" size="20" maxlength="25"
							id="sampleTumorTemp_code" 
							name="sampleTumorTemp.code" title="样本编号"
							value="<s:property value="sampleTumorTemp.code"/>" />
<%-- 							<input type="text" id="sampleTumorTemp_id" name="sampleTumorTemp.sampleInfo.id" value="<s:property value="sampleTumorTemp.sampleInfo.id"/>" /> --%>
							<input type="hidden" name="sampleTumorTemp.id" value="<s:property value="sampleTumorTemp.id"/>" />
					</td>
                   	
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_name"
                   	 	name="sampleTumorTemp.name" title="描述"   
						value="<s:property value="sampleTumorTemp.name"/>"
                   	 />
                   	</td>
                   		
                   	 	<td class="label-title" >检测项目</td>
                   	 	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleTumorTemp_product_name" searchField="true"  
 							name="sampleTumorTemp.productName"  value="<s:property value="sampleTumorTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleTumorTemp_product_id" name="sampleTumorTemp.productId"  
 							value="<s:property value="sampleTumorTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   	
               	</tr>
                   	<tr>     	
                   	<td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleTumorTemp_sampleType_name"  value="<s:property value="sampleTumorTemp.sampleType.name"/>"/>
						<s:hidden id="sampleTumorTemp_sampleType_id" name="sampleTumorTemp.sampleType.id"></s:hidden>
						<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>
              
                   		<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleTumorTemp_acceptDate"
                   	 		name="sampleTumorTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleTumorTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 />
                   	</td>
                   	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_area"
                   	 	name="sampleTumorTemp.area" title="地区"  onblur="checkAdd()" 
						value="<s:property value="sampleTumorTemp.area"/>"
                   	 />
                   	</td>
                   	     	
                   	</tr>
                   	
                   	<tr>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_hospital"
                   	 	name="sampleTumorTemp.hospital" title="送检医院"   
						value="<s:property value="sampleTumorTemp.hospital"/>"
                   	 />
                   	</td>
                   
                   		<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleTumorTemp_sendDate"
                   	 		name="sampleTumorTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"   value="<s:date name="sampleTumorTemp.sendDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 <input type="text" size="20" maxlength="25" id="sampleTumorTemp_reportDate"
                   	 		name="sampleTumorTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleTumorTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	</td>
                   	
                   	</tr>
                   	<tr>
                   		<td class="label-title" >送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_doctor"
                   	 	name="sampleTumorTemp.doctor" title="送检医生"   
						value="<s:property value="sampleTumorTemp.doctor"/>"
                   	 />
                   	</td>
                   	
                   		<td class="label-title" >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_sampleNum"
                   	 		name="sampleTumorTemp.sampleNum" title="序号"   
							value="<s:property value="sampleTumorTemp.sampleNum"/>"
                   	 	/>
                    </td>
                   	</tr>
                   	<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
				<tr>
                	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_patientName"
                   	 		name="sampleTumorTemp.patientName" title="姓名"   
							value="<s:property value="sampleTumorTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >姓名拼音</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_patientNameSpell"
                   	 		name="sampleTumorTemp.patientNameSpell" title="姓名拼音"   
							value="<s:property value="sampleTumorTemp.patientNameSpell"/>"
                   	 	/>
                   	</td>
                   		<td class="label-title" >病历号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_serialNum"
                   	 		name="sampleTumorTemp.serialNum" title="病历号"   
							value="<s:property value="sampleTumorTemp.serialNum"/>"
                   	 	/>
                   	</td>
               		
               	 	
                </tr>
                <tr>
                <td class="label-title" >性别</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.gender" id="sampleTumorTemp_gender" >
						<option value="" <s:if test="sampleTumorTemp.gender==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.gender==0">selected="selected" </s:if>>女</option>
    					<option value="1" <s:if test="sampleTumorTemp.gender==1">selected="selected" </s:if>>男</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                	<td class="label-title" >年龄</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" id="sampleTumorTemp_age" name="sampleTumorTemp.age"  value="<s:property value="sampleTumorTemp.age"/>" />
                   	</td>
                   	
                   	<td class="label-title" >籍贯</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_nativePlace"
                   	 		name="sampleTumorTemp.nativePlace" title="籍贯"   
							value="<s:property value="sampleTumorTemp.nativePlace"/>"
                   	 	/>
                   	</td>
       			</tr>
                <tr>
                		<td class="label-title" >民族</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_nationality"
                   	 		name="sampleTumorTemp.nationality" title="民族"   
							value="<s:property value="sampleTumorTemp.nationality"/>"
                   	 	/>
                   	</td>
               
                   			<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleTumorTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleTumorTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleTumorTemp_voucherType" name="sampleTumorTemp.voucherType.id"  value="<s:property value="sampleTumorTemp.voucherType.id"/>" > 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_voucherCode"
                   	 	name="sampleTumorTemp.voucherCode" title="证件号码"   onblur="checkFun()"
						value="<s:property value="sampleTumorTemp.voucherCode"/>"
                   	 />
                   	</td>
                   	</tr>
                   	<tr>
                   		<td class="label-title" >联系方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleTumorTemp_phone"
	                   		 name="sampleTumorTemp.phone" title="联系方式"  value="<s:property value="sampleTumorTemp.phone"/>"
	                   	 />
                   	</td>

                   		<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_emailAddress"
                  	 		name="sampleTumorTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleTumorTemp.emailAddress"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   		<td class="label-title" >通讯地址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_address"
                   	 		name="sampleTumorTemp.address" title="通讯地址"   
							value="<s:property value="sampleTumorTemp.address"/>"
                   	 	/>
                   	</td>
                   	</tr>
                   	<tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>个人病史</label>
						</div>
					</td>
				   </tr>
                   	<tr>
                   	<td class="label-title" >临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_diagnosis"
                   	 	name="sampleTumorTemp.diagnosis" title="临床诊断"   
						value="<s:property value="sampleTumorTemp.diagnosis"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >用药史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_bloodHistory"
                   	 		name="sampleTumorTemp.bloodHistory" title="用药史"   
							value="<s:property value="sampleTumorTemp.bloodHistory"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >病历描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_note"
                   	 	name="sampleTumorTemp.note" title="病历描述"   
						value="<s:property value="sampleTumorTemp.note"/>"
                   	 />
                   	</td>
                   	</tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>费用信息</label>
						</div>
					</td>
				</tr>
                <tr>
                	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.linkman" id="sampleTumorTemp_isInsure" >
						<option value="" <s:if test="sampleTumorTemp.isInsure==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.isInsure==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.isInsure==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >优惠类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.privilegeType" id="sampleTumorTempr_privilegeType" >
						<option value="" <s:if test="sampleTumorTemp.privilegeType==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.privilegeType==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.privilegeType==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   		<td class="label-title" >推荐人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_linkman"
                   	 	name="sampleTumorTemp.linkman" title="推荐人"   
						value="<s:property value="sampleTumorTemp.linkman"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	<td class="label-title" >是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleTumorTemp.isInvoice" id="sampleTumorTemp_isInvoice" >
						<option value="" <s:if test="sampleTumorTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    					<option value="0" <s:if test="sampleTumorTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    					<option value="1" <s:if test="sampleTumorTemp.isInvoice==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   <td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="hidden" size="20" maxlength="25" id="sampleTumorTemp_createUser_id"
                   	 		name="sampleTumorTemp.createUser.id" title="录入人"   
                   	 />
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_createUser_name" class="text input readonlytrue" readonly="readonly"
                   	 	name="sampleTumorTemp.createUser.name" title="录入人"   
						value="<s:property value="sampleTumorTemp.createUser.name"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_reason"
                   	 	name="sampleTumorTemp.reason" title="备注"   
						value="<s:property value="sampleTumorTemp.reason"/>"
                   	 />
                   	</td>
              	</tr>
                <tr>
                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleTumorTemp_money"
                   	 		name="sampleTumorTemp.money" title="备注"   
							value="<s:property value="sampleTumorTemp.money"/>"
                   	 	/>
                   	</td>
               
                	<td class="label-title" >SP</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_gestationalAge"
                   	 	name="sampleTumorTemp.gestationalAge" title="SP"   
						value="<s:property value="sampleTumorTemp.gestationalAge"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	<input type="text" size="20" maxlength="25" id="sampleTumorTemp_paymentUnit"
                   	 	name="sampleTumorTemp.paymentUnit" title="开票单位"   
						value="<s:property value="sampleTumorTemp.paymentUnit"/>"
                   	 />
                   	</td>
                </tr>
                <tr>
                	 <g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="UserFun" 
 					hasSetFun="true"
					documentId="sampleTumorTemp_reportMan"
					documentName="sampleTumorTemp_reportMan_name" />
			
			  	 	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleTumorTemp_reportMan_name"  value="<s:property value="sampleTumorTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleTumorTemp_reportMan" name="sampleTumorTemp.reportMan.id"  value="<s:property value="sampleTumorTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="UserFun" 
 					hasSetFun="true"
					documentId="sampleTumorTemp_auditMan"
					documentName="sampleTumorTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleTumorTemp_auditMan_name"  value="<s:property value="sampleTumorTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleTumorTemp_auditMan" name="sampleTumorTemp.auditMan.id"  value="<s:property value="sampleTumorTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                  	
                </tr>
       
		
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleTumor.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
