
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleChromosomeTemp&id=${sampleChromosomeTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleChromosomeEdit.js"></script>
	<s:if test="sampleChromosomeTemp.sampleInfo.upLoadAccessory.id !=''">
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" class="img" src="${ctx}/operfile/downloadById.action?id=${sampleChromosomeTemp.sampleInfo.upLoadAccessory.id}"></div>
	</s:if>
	<s:if test="sampleChromosomeTemp..sampleInfo.upLoadAccessory.id ==''"> 
		<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" class="img" src=""></div>
	</s:if>



  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
				<tr>
				<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
					<input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg2()" style="width: 150px;height: 30px; font-size: 13px;" />
					<input type="hidden"  id="upload_imga_id" name="sampleChromosomeTemp.upLoadAccessory.id" value="<s:property value="sampleChromosomeTemp.upLoadAccessory.id"/>">
					<input type="hidden"  id="upload_imga_name11" name="sampleChromosomeTemp.upLoadAccessory.fileName" value="<s:property value="sampleChromosomeTemp.upLoadAccessory.fileName"/>">
					<input type="hidden" id="saveType" value="rst" >
				</td>
			</tr>
			<tr>
               	 	<td class="label-title">编号</td>
                    <td class="requiredcolunm" nowrap width="10px" > </td>
                   	<td align="left">
                   		<input type="text" 	  id="sampleChromosomeTemp_code" class="text input readonlytrue" readonly="readonly" name="sampleChromosomeTemp.sampleInfo.code" title="样本编号" value="<s:property value="sampleChromosomeTemp.sampleInfo.code"/>" />
						<input type="hidden"  id="sampleChromosomeTemp_id"  name="sampleChromosomeTemp.sampleInfo.id"  value="<s:property value="sampleChromosomeTemp.sampleInfo.id"/>" />
						<input type="hidden"  name="sampleChromosomeTemp.id" value="<s:property value="sampleChromosomeTemp.id"/>" />
						<input type="hidden"  id="upload_imga_name" name="sampleChromosomeTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleChromosomeTemp.sampleInfo.upLoadAccessory.fileName"/>">
						<input type="hidden"  id="upload_imga_id10" name="sampleChromosomeTemp.sampleInfo.upLoadAccessory.id" value="<s:property value="sampleChromosomeTemp.sampleInfo.upLoadAccessory.id"/>">
                   	</td>
                   
                   	
                   	<td class="label-title" >描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_name"
                   	 		name="sampleChromosomeTemp.name" title="描述"   
							value="<s:property value="sampleChromosomeTemp.name"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >检测项目</td>
                   	<td class="requiredcolumn" nowrap width="10px" ></td>	
                   	<td align="left"  >
 						<input type="text" size="20"   id="sampleChromosomeTemp_product_name" searchField="true"  
 							name="sampleChromosomeTemp.productName"  value="<s:property value="sampleChromosomeTemp.productName"/>" class="text input" />
 						<input type="hidden" id="sampleChromosomeTemp_product_id" name="sampleChromosomeTemp.productId"  
 							value="<s:property value="sampleChromosomeTemp.productId"/>" > 
 						<img alt='选择检测项目' id='showage' src='${ctx}/images/img_lookup.gif' onClick="voucherProductFun()" class='detail' />                   		
                   	</td>
                   		
                  </tr>
                  <tr>
                  	<td class="label-title" >接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_acceptDate"
                   	 		name="sampleChromosomeTemp.acceptDate" title="接收日期" Class="Wdate" readonly="readonly"
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleChromosomeTemp.acceptDate" format="yyyy-MM-dd" />" 
                   	 	/>
                   	</td>
                   	<td class="label-title" >地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_area"
                   	 		name="sampleChromosomeTemp.area" title="地区"  onblur="checkAdd()" 
							value="<s:property value="sampleChromosomeTemp.area"/>"
                   		 />
                   	</td>
                   	<td class="label-title" >送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_hospital"
                   	 		name="sampleChromosomeTemp.hospital" title="送检医院"   
							value="<s:property value="sampleChromosomeTemp.hospital"/>"
                   	 />
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >送检日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		 <input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_sendDate"
                   	 		name="sampleChromosomeTemp.sendDate" title="送检日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd HH:mm',maxDate:new Date()})"   value="<s:date name="sampleChromosomeTemp.sendDate" format="yyyy-MM-dd"/>" 
                   		 />
                   	</td>
                   	
                   	<td class="label-title" >应出报告日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   	 	<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_reportDate"
                   	 		name="sampleChromosomeTemp.reportDate" title="应出报告日期" Class="Wdate" readonly="readonly" 
                   	    	onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})"   value="<s:date name="sampleChromosomeTemp.reportDate" format="yyyy-MM-dd"/>" 
                   	 />
                   	
                   	</td>
                   	<td class="label-title" >送检医生</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_doctor"
                   	 		name="sampleChromosomeTemp.doctor" title="送检医生"   
							value="<s:property value="sampleChromosomeTemp.doctor"/>"
                   		 />
                   	</td>
                 </tr>
                 <tr>
                  	<td class="label-title" >样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_sampleNum"
                   	 		name="sampleChromosomeTemp.sampleNum" title="序号"   
							value="<s:property value="sampleChromosomeTemp.sampleNum"/>"
                   	 	/>
                    </td>
                    <td class="label-title" >样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
						<input type="text" size="20" readonly="readOnly"  id="sampleChromosomeTemp_sampleType_name"  value="<s:property value="sampleChromosomeTemp.sampleType.name"/>"/>
							<s:hidden id="sampleChromosomeTemp_sampleType_id" name="sampleChromosomeTemp.sampleType.id"></s:hidden>
							<span id="regionType" onClick="sampleKind()" class="select-search-btn">&nbsp;&nbsp;&nbsp;</span>
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                  	<td class="label-title" >姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_patientName"
                   	 		name="sampleChromosomeTemp.patientName" title="姓名"   
							value="<s:property value="sampleChromosomeTemp.patientName"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >证件类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="20" readonly="readOnly"  id="sampleChromosomeTemp_voucherType_name" onblur="checkType()" value="<s:property value="sampleChromosomeTemp.voucherType.name"/>" />
 						<input type="hidden" id="sampleChromosomeTemp_voucherType" name="sampleChromosomeTemp.voucherType.id"  value="<s:property value="sampleChromosomeTemp.voucherType.id"/>" > 
 						<img alt='选择证件类型' src='${ctx}/images/img_lookup.gif' onClick="voucherTypeFun()"	class='detail'    />                   		
                   	</td>
                     
                   	<td class="label-title" >证件号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_voucherCode"
                   	 		name="sampleChromosomeTemp.voucherCode" title="证件号码"   onblur="checkFun()"
							value="<s:property value="sampleChromosomeTemp.voucherCode"/>"
                   	 	/>
                   	</td>
            	
				  </tr>			
				  <tr>
			
					<td class="label-title" >手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_phoneNum"
                   	 		name="sampleChromosomeTemp.phoneNum" title="手机号码"   onblur="checkPhone()"
							value="<s:property value="sampleChromosomeTemp.phoneNum"/>"
                   		 />
                   	</td>
                   	<td class="label-title" >手机号码2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_phoneNum2"
                   	 		name="sampleChromosomeTemp.phoneNum2" title="手机号码2"   onblur="checkPhone()"
							value="<s:property value="sampleChromosomeTemp.phoneNum2"/>"
                   	 	/>
                   	</td>
                   	
                 </tr>
                 <tr>
                  	<td class="label-title" >代理人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_agentMan"
                   	 		name="sampleChromosomeTemp.agentMan" title="代理人"   
							value="<s:property value="sampleChromosomeTemp.agentMan"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >代理人身份证</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_cardCode"
                   	 		name="sampleChromosomeTemp.cardCode" title="证件号码"   onblur="checkFun()"
							value="<s:property value="sampleChromosomeTemp.cardCode"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_address"

                   	 		name="sampleChromosomeTemp.address" title="家庭住址"   onblur="checkAddress()"

							value="<s:property value="sampleChromosomeTemp.address"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >电子邮箱</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_emailAddress"
                  	 		name="sampleChromosomeTemp.emailAddress" title="电子邮箱"   
							value="<s:property value="sampleChromosomeTemp.emailAddress"/>"
                   	 	/>
                   	 	<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >邮编</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_inHosNum"
                   	 		name="sampleChromosomeTemp.inHosNum" title="邮编"   
							value="<s:property value="sampleChromosomeTemp.inHosNum"/>"
                   	 	/>
                   	</td>
                   	
                 </tr>
                 <tr>
                  	<td class="label-title" >送检原因</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_tumorHistory"
                   	 		name="sampleChromosomeTemp.tumorHistory" title="送检原因"   
							value="<s:property value="sampleChromosomeTemp.tumorHistory"/>"
                   	 />
                   	</td>
                 </tr>
                  
                  <tr>
						<td colspan="9">
							<div class="standard-section-header type-title">
								<label>孕产史</label>
							</div>
						</td>
				  </tr>
                  
                  <tr>
                  	<td class="label-title" >不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_badMotherhood"
                   	 		name="sampleChromosomeTemp.badMotherhood" title="不良孕产史"   
							value="<s:property value="sampleChromosomeTemp.badMotherhood"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >孕几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_pregnancyTime"
                   	 		name="sampleChromosomeTemp.pregnancyTime" title="孕几次"   
							value="<s:property value="sampleChromosomeTemp.pregnancyTime"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >产几次</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_parturitionTime"
                   	 		name="sampleChromosomeTemp.parturitionTime" title="产几次"   
							value="<s:property value="sampleChromosomeTemp.parturitionTime"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >此次第几胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_howManyfetus"
                   	 		name="sampleChromosomeTemp.howManyfetus" title="此次第几胎"   
							value="<s:property value="sampleChromosomeTemp.howManyfetus"/>"
                   	 />
                   	</td>
                   	
                   	<td class="label-title" >具体情况描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_reason2"
                   	 		name="sampleChromosomeTemp.reason2" title="具体情况描述"   
							value="<s:property value="sampleChromosomeTemp.reason2"/>"
                   	 />
                   	</td>
                   	<td class="label-title" >血型</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.organGrafting" id="sampleChromosomeTemp_organGrafting" >
							<option value="" <s:if test="sampleChromosomeTemp.organGrafting==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleChromosomeTemp.organGrafting==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleChromosomeTemp.organGrafting==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                 </tr>
                 <tr>
                   	<td class="label-title" >受孕方式</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.outTransfusion" id="sampleChromosomeTemp_outTransfusion" >
							<option value="" <s:if test="sampleChromosomeTemp.outTransfusion==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleChromosomeTemp.outTransfusion==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleChromosomeTemp.outTransfusion==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >单胎/非单胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.embryoType" id="sampleChromosomeTemp_embryoType" >
							<option value="" <s:if test="sampleChromosomeTemp.embryoType==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleChromosomeTemp.embryoType==0">selected="selected" </s:if>>单胎</option>
    						<option value="1" <s:if test="sampleChromosomeTemp.embryoType==1">selected="selected" </s:if>>非单胎</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >流产孕周</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
	                   	<input type="text" size="20" maxlength="11" id="sampleChromosomeTemp_gestationalAge"
	                   		 name="sampleChromosomeTemp.gestationalAge" title="流产孕周"  value="<s:property value="sampleChromosomeTemp.gestationalAge"/>"
	                   	 />
                   	</td>
                   	
                  </tr>
                  <tr>
                  	<td class="label-title" >临床诊断</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_diagnosis"
                   	 		name="sampleChromosomeTemp.diagnosis" title="临床诊断"   
							value="<s:property value="sampleChromosomeTemp.diagnosis"/>"
                   		 />
                   	</td>
                  </tr>
                  <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>其它信息</label>
						</div>
					</td>
				  </tr>
                  <tr>
                  	<td class="label-title" >抗生素</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.stemCellsCure" id="sampleChromosomeTemp_stemCellsCure" >
							<option value="" <s:if test="sampleChromosomeTemp.stemCellsCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="sampleChromosomeTemp.stemCellsCure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="sampleChromosomeTemp.stemCellsCure==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >避孕药</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.immuneCure" id="sampleChromosomeTemp_immuneCure" >
							<option value="" <s:if test="sampleChromosomeTemp.immuneCure==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="sampleChromosomeTemp.immuneCure==0">selected="selected" </s:if>>有</option>
	    					<option value="1" <s:if test="sampleChromosomeTemp.immuneCure==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >核型分析</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.coreAnalyze" id="sampleChromosomeTemp_coreAnalyze" >
							<option value="" <s:if test="sampleChromosomeTemp.coreAnalyze==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleChromosomeTemp.coreAnalyze==0">selected="selected" </s:if>>有</option>
    						<option value="1" <s:if test="sampleChromosomeTemp.coreAnalyze==1">selected="selected" </s:if>>无</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                  </tr>
                  <tr>
                  	<td class="label-title" >其它</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_note"
                   	 		name="sampleChromosomeTemp.note" title="其它"   
							value="<s:property value="sampleChromosomeTemp.note"/>"
                   		 />
                   	</td>
                   	<td class="label-title" >附加样本1（姓名、关系）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_attachedSample1"
                   	 		name="sampleChromosomeTemp.attachedSample1" title="附加样本1（姓名、关系）"   
							value="<s:property value="sampleChromosomeTemp.attachedSample1"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >附加样本2（姓名、关系）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_attachedSample2"
                   	 		name="sampleChromosomeTemp.attachedSample2" title="附加样本2（姓名、关系）"   
							value="<s:property value="sampleChromosomeTemp.attachedSample2"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >特殊情况说明</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_specialCircumstances"
                   	 		name="sampleChromosomeTemp.specialCircumstances" title="特殊情况说明"   
							value="<s:property value="sampleChromosomeTemp.specialCircumstances"/>"
                   	 	/>
                   	</td>
                   	<td class="label-title" >是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.isInsure" id="sampleChromosomeTemp_isInsure" >
							<option value="" <s:if test="sampleChromosomeTemp.isInsure==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleChromosomeTemp.isInsure==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleChromosomeTemp.isInsure==1">selected="selected" </s:if>>否</option>
					</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	<td class="label-title" >发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<select name="sampleChromosomeTemp.isInvoice" id="sampleChromosomeTempe_isInvoice" >
							<option value="" <s:if test="sampleChromosomeTemp.isInvoice==''">selected="selected" </s:if>>请选择</option>
    						<option value="0" <s:if test="sampleChromosomeTemp.isInvoice==0">selected="selected" </s:if>>是</option>
    						<option value="1" <s:if test="sampleChromosomeTemp.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >录入人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="sampleChromosomeTemp.createUser" title="录入人"   
							value="<s:property value="sampleChromosomeTemp.createUser"/>"/>
                   	</td>
                   	<td class="label-title" >备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_reason"
                   	 		name="sampleChromosomeTemp.reason" title="备注"   
							value="<s:property value="sampleChromosomeTemp.reason"/>"
                   		 />
                   	</td>
              
                	<td class="label-title" >金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_money"
                   	 		name="sampleChromosomeTemp.money" title="备注"   
							value="<s:property value="sampleChromosomeTemp.money"/>"
                   	 	/>
                   	</td>
                  </tr>
                  <tr>
                  	<td class="label-title" >收据（S）/pos条（P）</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_receipt"
                   	 		name="sampleChromosomeTemp.receipt" title="收据（S）/pos条（P）"   
							value="<s:property value="sampleChromosomeTemp.receipt"/>"
                   	 	/>
                   	</td>
                   	
                   	<td class="label-title" >开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="sampleChromosomeTemp_paymentUnit"
                   	 		name="sampleChromosomeTemp.paymentUnit" title="开票单位"   
							value="<s:property value="sampleChromosomeTemp.paymentUnit"/>"
                   		 />
                   	</td>
                </tr>
                <tr>
                	<g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="reportManUserFun" 
 					hasSetFun="true"
					documentId="sampleChromosomeTemp_reportMan"
					documentName="sampleChromosomeTemp_reportMan_name" />
                   	
                   	<td class="label-title" >核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleChromosomeTemp_reportMan_name"  value="<s:property value="sampleChromosomeTemp.reportMan.name"/>" />
 						<input type="hidden" id="sampleChromosomeTemp_reportMan" name="sampleChromosomeTemp.reportMan.id"  value="<s:property value="sampleChromosomeTemp.reportMan.id"/>" > 
 						<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="auditManUserFun" 
 					hasSetFun="true"
					documentId="sampleChromosomeTemp_auditMan"
					documentName="sampleChromosomeTemp_auditMan_name" />
			
			  	 	<td class="label-title" >核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
 						<input type="text" size="15" readonly="readOnly"  id="sampleChromosomeTemp_auditMan_name"  value="<s:property value="sampleChromosomeTemp.auditMan.name"/>" />
 						<input type="hidden" id="sampleChromosomeTemp_auditMan" name="sampleChromosomeTemp.auditMan.id"  value="<s:property value="sampleChromosomeTemp.auditMan.id"/>" > 
 						<img alt='选择核对人' id='showauditMan' src='${ctx}/images/img_lookup.gif' 	class='detail'    />                   		
                   	</td>
                	
                	
                  </tr>
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleChromosome.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
