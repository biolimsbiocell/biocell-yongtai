﻿<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<%@ include file="/WEB-INF/page/include/common.jsp"%>

<style type="text/css">
#box2, #box3, #box1 {
	border-radius: 10px;
	overflow: hidden;
	transition: all 1s;
	width: 80%;
	margin: 10px auto;
	color: #fff;
}

#box2 {
	background: #8FBAF3;
}

#box3 {
	background: #28CC9E;
}

#box1 {
	background: #878ECD;
}

#box3:hover {
	box-shadow: 15px 15px 15px #000;
}

#box2:hover {
	box-shadow: 15px 15px 15px #000;
}

#box1:hover {
	box-shadow: 15px 15px 15px #000;
}

.modal-body .box-body div {
	margin: -34px 13px;
	text-align: center;
}

.modal-body .box-body div a {
	color: #fff;
	display: block;
	margin-top: 23%;
	text-decoration: none;
}

.modal-body .box-body div i {
	font-size: 100px;
}

.modal-body .box-body div p {
	font-weight: 900;
}

.chosedType {
	border: 2px solid #BAB5F6 !important;
}

.input-group {
	margin-top: 10px;
}

.scientific {
	display: none;
}

.box-title small span {
	margin-right: 20px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarBindClick.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<!--选择接受类型的模态框-->
	<div class="modal fade" id="sampleReceiveModal" tabindex="-1"
		role="dialog" aria-hidden="flase" data-backdrop="false">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<button class="btn btn-warning btn-lg chosedType">
						<%-- <fmt:message key="biolims.receive.clinicalSample" /> --%>
						自体库样本
					</button>
					<%-- <button class="btn btn-warning btn-lg">
						<fmt:message key="biolims.receive.scientificResearchSample" />
						公共库样本
					</button> --%>
				</div>
				<!--<div>
					<a href="####" style="text-align: center;" onclick="downCsv()">
						<p><fmt:message key="biolims.receive.downloadCSV" /></p>
					</a>
				</div>-->
				<div class="modal-body" id="modal-body" style="min-height:87px;">
					<div class="row">
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box1">
								<div class="box-body" style="height: 170px;">
									<div id="scanReceive">
										<a href="###"> <i class="fa  fa-barcode"></i>
											<p>
												<fmt:message key="biolims.receive.scanCode" />
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box2">
								<div class="box-body" style="height: 170px;">
									<div id="uploadList">
										<a href="####"> <i class="fa  fa-list"></i>
											<p>
												<fmt:message key="biolims.common.upLoadFile" />
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-4 col-xs-12">
							<div class="box" id="box3">
								<div class="box-body" style="height: 170px;">
									<div id="newList">
										<a href="####"> <i class="fa fa-clipboard"></i>
											<p>
												<fmt:message key="biolims.receive.newSampleReceive" />
											</p>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12" id="scancode" style="display: none;">
						<input type="text" class="form-control input-lg" id="scanInput"
							autofocus>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 50px">
		<!--订单录入的form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<strong id="sampleReceiveTitle"
							type="<s:property value="sampleReceive.type"/>"><fmt:message
								key="biolims.common.clinicalSamplesInformation" /></strong> <small
							style="color: #fff;"> <fmt:message
								key="biolims.receive.sampleReceiveId" />: <i
							id="sampleReveice_id"><s:property value="sampleReceive.id " /></i>
							<fmt:message key="biolims.common.receiverName" />: <span
							id="sampleReceive_acceptUser"><s:property
									value=" sampleReceive.acceptUser.name " /></span> <!--接收日期: <span id="sampleReceive_acceptDate"><s:date name="sampleReceive.acceptDate" format="yyyy-MM-dd"/></span>-->
							<fmt:message key="biolims.common.state" />: <span
							id="sampleReceive_state"><s:property
									value="sampleReceive.stateName" /></span>  <fmt:message
								key="biolims.common.completionTime" /><span
							id="sampleReceive_confirmDate"><s:date
									name=" sampleReceive.confirmDate " format="yyyy-MM-dd " /></span> <fmt:message
								key="biolims.common.createDate" />:<span
							id="sampleReceive_createDate"><s:date
									name="sampleReceive.createDate" format="yyyy-MM-dd" /></span>
						</small>
					</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefresh()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
											key="biolims.common.print" /></a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
											key="biolims.common.copyData" /></a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();"><fmt:message
											key="biolims.common.export" /></a></li>
								<li><a href="####" onclick="$('.buttons-csv').click();"><fmt:message
											key="biolims.common.exportCSV" /></a></li>
								<li role="separator" class="divider"></li>
								<li><a href="####" id="fixdeLeft2" onclick="fixedCol(2)"><fmt:message
											key="biolims.common.lock2Col" /></a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
											key="biolims.common.cancellock2Col" /></a></li>
							</ul>
						</div>
					</div>

				</div>
				<div class="box-body">
					<form name="form1" id="form1" method="post">
						<input type="hidden" id="bpmTaskId"
							value="<%=request.getParameter("bpmTaskId")%>" />
						<!--保存时的拼的数据-->
						<input type="hidden" name="confirmDate"
							value="<s:date name=" sampleReceive.confirmDate " format="yyyy-MM-dd "/>" />
						<input type="hidden" name="createDate"
							value="<s:date name=" sampleReceive.createDate" format="yyyy-MM-dd "/>" />
						<input type="hidden" id="confirmUserId" name="confirmUser-id"
							value="<s:property value=" sampleReceive.confirmUser.id "/>" />
						<input type="hidden" name="state"
							value="<s:property value="sampleReceive.state"/>" /> <input
							type="hidden" name="stateName"
							value="<s:property value="sampleReceive.stateName"/>" /> <input
							type="hidden" id="sampleReceiveinputid" name="id"
							value="<s:property value="sampleReceive.id "/>" /> <input
							type="hidden" id="acceptUserid" name="acceptUser-id"
							value="<s:property value=" sampleReceive.acceptUser.id "/>" />
						<div id="Qchidden" class="row">
						<!-- 订单编号 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">订单编号</span> <input type="text" readonly="readonly"
										changelog="<s:property value="sampleReceive.sampleOrder"/>" size="20"
										maxlength="25" id="sampleReceive_sampleOrder"
										name="sampleOrder" class="form-control" title=""
										value="<s:property value=" sampleReceive.sampleOrder "/>" />
								</div>
							</div>
						<!-- 快递公司 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.sample.companyName" /></span><input
										changelog="<s:property value=" sampleReceive.expressCompany.id "/>"
										type="hidden" size="20" id="sampleReceive_expressCompany_id"
										name="expressCompany-id"
										value="<s:property value=" sampleReceive.expressCompany.id "/>"
										class="form-control" title="快递公司" /> <input
										changelog="<s:property value=" sampleReceive.expressCompany.name "/>"
										type="text" size="20" id="sampleReceive_expressCompany_name"
										name="expressCompany-name" readonly="readonly"
										value="<s:property value=" sampleReceive.expressCompany.name "/>"
										class="form-control" title="快递公司" /> <span
										class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showexpressCompany()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<!-- 快递员 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">
										快递员 </span> <input
										changelog="<s:property value=" sampleReceive.courier "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_courier" name="courier"
										value="<s:property value=" sampleReceive.courier "/>"
										title="快递员" />
								</div>
							</div>
							<!-- 快递单号 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.sample.expreceCode" /> </span> <input
										changelog="<s:property value=" sampleReceive.expressNum "/>"
										list="sampleIdOne" type="text" size="20" maxlength="25"
										id="sampleReceive_expressNum" name="expressNum"
										class="form-control" title="快递单号"
										value="<s:property value=" sampleReceive.expressNum "/>" />
								</div>
							</div>
							<!-- 样品运输方式 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.sampleTransportationMode" /></span><input
										changelog="<s:property value=" sampleReceive.ysfs.id "/>"
										 type="hidden" size="20" id="sampleReceive_ysfs_id"
										name="ysfs-id"
										value="<s:property value=" sampleReceive.ysfs.id "/>"
										class="form-control" title="样品运输方式" /> <input
										changelog="<s:property value=" sampleReceive.ysfs.name "/>"
										type="text" size="20" id="sampleReceive_ysfs_name"
										name="ysfs-name" readonly="readonly"
										value="<s:property value=" sampleReceive.ysfs.name "/>"
										class="form-control" title="样品运输方式" /> <span
										class="input-group-btn">
										<button class="btn btn-info" type="button" id='ypysfs'
											onclick="yunshufs()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<!-- 运送设备温度 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 运送设备温度 </span> <input
										changelog="<s:property value=" sampleReceive.temperature "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_temperature" name="temperature" onchange="findTemperature(this.value)"
										value="<s:property value=" sampleReceive.temperature "/>"
										title="运送设备温度" />
								</div>
							</div>
							<!-- 温度记录仪编号 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 温度记录仪编号 </span> <input
										changelog="<s:property value=" sampleReceive.recorder "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_recorder" name="recorder" onchange="findRecorder(this.value)"
										value="<s:property value=" sampleReceive.recorder "/>"
										title="温度记录仪编号" />
								</div>
							</div>
							<!-- 温度计开启日期 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 温度计开始时间</span> <input
										changelog="<s:property value="sampleReceive.thermometerTime"/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_thermometerTime" name="thermometerTime"
										readonly="readonly"
										title=""
										value="<s:date name="sampleReceive.thermometerTime" format="yyyy-MM-dd HH:mm"/>" />
								</div>
							</div>
							<!--温度计结束日期  -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 温度计结束时间 </span> <input
										changelog="<s:property value="sampleReceive.thermometerTime2"/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_thermometerTime2" name="thermometerTime2"
										readonly="readonly"
										title="温度计结束时间"
										value="<s:date name="sampleReceive.thermometerTime2" format="yyyy-MM-dd HH:mm"/>" />
								</div>
							</div>
							<!-- 最低温度 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 开启温度℃ </span> <input
										changelog="<s:property value=" sampleReceive.lowTemperature "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_lowTemperature" name="lowTemperature" onchange="findLowTemperature(this.value)"
										value="<s:property value=" sampleReceive.lowTemperature "/>"
										title="开启温度℃" />
								</div>
							</div>
							
							<!-- 最高温度 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">结束温度℃ </span> <input
										changelog="<s:property value=" sampleReceive.highTemperature "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_highTemperature" name="highTemperature" onchange="findHighTemperature(this.value)"
										value="<s:property value=" sampleReceive.highTemperature "/>"
										title="结束温度℃ " />
								</div>
							</div>
							
							<!-- 平均温度 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 平均温度 ℃</span> <input
										changelog="<s:property value=" sampleReceive.avgTemperature "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_avgTemperature" name="avgTemperature" onchange="findAvgTemperature(this.value)"
										value="<s:property value=" sampleReceive.avgTemperature "/>"
										title="平均温度" />
								</div>
							</div>
							<!--  CCOI-->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">CCOI</span> <input type="text" readonly="readonly"
										changelog="<s:property value="sampleReceive.ccoi"/>"
										size="20" maxlength="25" id="sampleReceive_ccoi" name="ccoi"
										class="form-control" title=""
										value="<s:property value=" sampleReceive.ccoi "/>" />
								</div>
							</div>
							<!-- 产品批号 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">产品批号</span> <input type="text" readonly="readonly"
										changelog="<s:property value=" sampleReceive.barcode "/>"
										size="20" maxlength="25" id="sampleReceive_barcode"
										name="barcode" class="form-control" title="产品批号"
										value="<s:property value=" sampleReceive.barcode "/>" />
								</div>
							</div>
							
							
							
							
							
							
							
							<!-- 运送设备编号 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 运送设备编号 </span> <input
										changelog="<s:property value=" sampleReceive.equimentNumber "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_equimentNumber" name="equimentNumber"
										value="<s:property value=" sampleReceive.equimentNumber "/>"
										title="运送设备编号" />
								</div>
							</div>
							
							<!-- 运输温度是否合格 -->
							<div class="col-md-4 col-sm-6 col-xs-12" style="display:none">
								<div class="input-group">
									<span class="input-group-addon">运输温度是否合格</span> <select
										class="form-control" lay-ignore=""
										id="sampleReceive_qualified" name="qualified">
										<option value="0"
											<s:if test="sampleReceive.qualified==0">selected="selected"</s:if>></option>
										<option value="1"
											<s:if test="sampleReceive.qualified==1">selected="selected"</s:if>>合格</option>
										<option value="2"
											<s:if test="sampleReceive.qualified==2">selected="selected"</s:if>>不合格</option>
									</select>
								</div>
							</div>
							<!-- 接收日期 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> <fmt:message
											key="biolims.common.receiptDate" /><img
										class='requiredimage' src='${ctx}/images/required.gif' />
									</span> <input
										changelog="<s:property value=" sampleReceive.acceptDate "/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_acceptDate" name="acceptDate" title="接受日期"
										value="<s:date name=" sampleReceive.acceptDate " format="yyyy-MM-dd HH:mm"/>" />
								</div>
							</div>
							<!-- 预计回输开始日期 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 预计回输开始日期<img
										class='requiredimage' src='${ctx}/images/required.gif' />
									</span> <input
										changelog="<s:property value="sampleReceive.startBackTime"/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_startBackTime" name="startBackTime" title=""
										readonly="readonly"
										value="<s:date name="sampleReceive.startBackTime" format="yyyy-MM-dd"/>" />
								</div>
							</div>
							<!--预计回输结束日期  -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> 预计回输结束日期<img
										class='requiredimage' src='${ctx}/images/required.gif' />
									</span> <input
										changelog="<s:property value="sampleReceive.feedBackTime"/>"
										class="form-control" type="text" size="20" maxlength="25"
										id="sampleReceive_feedBackTime" name="feedBackTime" title=""
										readonly="readonly"
										value="<s:date name="sampleReceive.feedBackTime" format="yyyy-MM-dd"/>" />
								</div>
							</div>
							<!-- 订单信息核实 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon">订单信息核实 </span> <select
										class="form-control" lay-ignore="" id="sampleReceive_verify"
										name="verify">
										<option value="0"
											<s:if test="sampleReceive.verify==0">selected="selected"</s:if>></option>
										<option value="1"
											<s:if test="sampleReceive.verify==1">selected="selected"</s:if>>已核实</option>
										<option value="2"
											<s:if test="sampleReceive.verify==2">selected="selected"</s:if>>未核实</option>
									</select>
								</div>
							</div>
							<!-- QA审核人 -->
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">QA通知人<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										changelog="<s:property value=" sampleReceive.approvalUser.id "/>"
										type="hidden" size="20" id="sampleReceive_approvalUser_id"
										name="approvalUser-id"
										value="<s:property value=" sampleReceive.approvalUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" sampleReceive.approvalUser.name "/>"
										type="text" readonly="readonly"
										id="sampleReceive_approvalUser_name"
										value="<s:property value=" sampleReceive.approvalUser.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showApprovalUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<!-- 生产审核人 -->
							<div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">生产通知人<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										changelog="<s:property value=" sampleReceive.productionUser.id "/>"
										type="hidden" size="20" id="sampleReceive_productionUser_id"
										name="productionUser-id"
										value="<s:property value=" sampleReceive.productionUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" sampleReceive.productionUser.name "/>"
										type="text" size="20" readonly="readonly"
										id="sampleReceive_productionUser_name"
										value="<s:property value=" sampleReceive.productionUser.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showProductionUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							
							<!-- QC通知人 -->						
                            <div class="col-md-4 col-sm-6 col-xs-12 ">
								<div class="input-group">
									<span class="input-group-addon">QC
									<img
										class='requiredimage' src='${ctx}/images/required.gif' /></span><input
										changelog="<s:property value=" sampleReceive.qualityCheckUser.id "/>"
										type="hidden" size="20" id="sampleReceive_qualityCheckUser_id"
										name="qualityCheckUser-id"
										value="<s:property value=" sampleReceive.qualityCheckUser.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" sampleReceive.qualityCheckUser.name "/>"
										type="text" readonly="readonly"
										id="sampleReceive_qualityCheckUser_name"
										value="<s:property value=" sampleReceive.qualityCheckUser.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showQualityCheckUser()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<!--描述 -->
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.describe" /></span> <input list="sampleIdOne"
										changelog="<s:property value=" sampleReceive.name "/>"
										type="text" size="20" maxlength="25" id="sampleReceive_name"
										name="name" class="form-control" title="描述"
										value="<s:property value=" sampleReceive.name "/>" />
								</div>
							</div>
							
							
							<div class="col-md-4 col-sm-6 col-xs-12 scientific">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.projectName" /></span><input
										changelog="<s:property value=" sampleReceive.project.name "/>"
										type="hidden" size="20" id="sampleReceive_project_id"
										name="project-id"
										value="<s:property value=" sampleReceive.project.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" sampleReceive.project.name "/>"
										type="text" size="20" id="sampleReceive_project_name"
										name="project-name" readonly="readonly"
										value="<s:property value=" sampleReceive.project.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button" id='ypysfs'
											onclick="showProject()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 scientific">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.selectTheCustomer" /></span><input
										changelog="<s:property value=" sampleReceive.crmCustomer.id "/>"
										type="hidden" size="20" id="sampleReceive_crmCustomer_id"
										name="crmCustomer-id"
										value="<s:property value=" sampleReceive.crmCustomer.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" sampleReceive.crmCustomer.name "/>"
										type="text" size="20" readonly="readonly"
										id="sampleReceive_crmCustomer_name" name="crmCustomer-name"
										value="<s:property value=" sampleReceive.crmCustomer.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button" onclick="showHos()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12 scientific">
								<div class="input-group">
									<span class="input-group-addon"><fmt:message
											key="biolims.common.selectTheDoctor" /></span><input
										changelog="<s:property value=" sampleReceive.crmDoctor.id "/>"
										type="hidden" size="20" id="sampleReceive_crmDoctor_id"
										name="crmDoctor-id"
										value="<s:property value=" sampleReceive.crmDoctor.id "/>"
										class="form-control" /> <input
										changelog="<s:property value=" sampleReceive.crmDoctor.name "/>"
										type="text" size="20" readonly="readonly"
										id="sampleReceive_crmDoctor_name" name="crmDoctor-name"
										value="<s:property value=" sampleReceive.crmDoctor.name "/>"
										class="form-control" /> <span class="input-group-btn">
										<button class="btn btn-info" type="button"
											onclick="showDoctors()">
											<i class="glyphicon glyphicon-search"></i>
										</button>
									</span>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="input-group">
									<button type="button" id="fileUpId" class="btn btn-info btn-sm"
										onclick="fileUp()">
										<fmt:message key="biolims.common.uploadAttachment" />
									</button>
									&nbsp;&nbsp;
									<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
									<button type="button" class="btn btn-info btn-sm"
										onclick="fileView()">
										<fmt:message key="biolims.report.checkFile" />
									</button>
								</div>
							</div>
						</div>
						<input type="hidden" id="scopeId" name="scopeId"
							value="<s:property value="sampleReceive.scopeId"/>"> <input
							type="hidden" id="scopeName" name="scopeName"
							value="<s:property value="sampleReceive.scopeName"/>"> <input
							type="hidden" id="changeLog" name="changeLog" />
					</form>
				</div>
				<div class="box-footer ipadmini"
					style="padding-top: 5px; min-height: 320px;">
					<table
						class="table table-hover table-striped table-bordered table-condensed"
						id="main" style="font-size: 14px;"></table>
				</div>

			</div>

		</div>

	</div>
	<input type="hidden" id="lan" value="${sessionScope.lan}">
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/sample/sampleReceiveEditNew.js"></script>
</body>
</html>