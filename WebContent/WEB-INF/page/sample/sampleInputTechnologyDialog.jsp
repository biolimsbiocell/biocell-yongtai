﻿
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleInputTechnologyDialog.js"></script>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>
<body>

		<div id="jstj" style="display: none">
		<input type="hidden" id="selectId"/>
		<input type="hidden" id="extJsonDataString" name="extJsonDataString">	
		<form id="searchForm">
		<table class="frame-table">
			<tr>
           	 	<td class="label-title">编号</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_id" searchField="true" name="id"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">项目名称</td>
               	<td align="left">
                    		<input type="text" maxlength="50" id="sampleInputTechnology_projectName" searchField="true" name="projectName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">客户名称</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_clientName" searchField="true" name="clientName"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">客户单位</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_clientUnit" searchField="true" name="clientUnit"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">样本状态</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_sampleState" searchField="true" name="sampleState"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">物种</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_species" searchField="true" name="species"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">器官类型</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_organType" searchField="true" name="organType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本名称</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_sampleName" searchField="true" name="sampleName"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">样本类型</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_sampleType" searchField="true" name="sampleType"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">样本编码</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_sampleCode" searchField="true" name="sampleCode"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">开箱接收人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_openBoxUser" searchField="true" name="openBoxUser"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">录入人</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_createUser" searchField="true" name="createUser"  class="input-20-length"></input>
               	</td>
			</tr>
			<tr>
           	 	<td class="label-title">录入时间</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_createDate" searchField="true" name="createDate"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">备注</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_note" searchField="true" name="note"  class="input-20-length"></input>
               	</td>
           	 	<td class="label-title">是否合格</td>
               	<td align="left">
                    		<input type="text" maxlength="25" id="sampleInputTechnology_isQualified" searchField="true" name="isQualified"  class="input-20-length"></input>
               	</td>
			</tr>
        </table>
		</form>
		</div>
		<span onclick="sc()" ><font color="blue">搜索</font></span>
		
		<div id="show_dialog_sampleInputTechnology_div"></div>
   		
</body>
</html>



