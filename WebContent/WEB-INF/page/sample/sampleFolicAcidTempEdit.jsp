
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<head>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
</head>
<style type="text/css">
	#upLoadImg{ padding:3px 12px; background:#04B5AF; color:#fff; border-radius:3px; box-shadow:0 1px 1px #ddd;cursor:pointer;border:0px;border-bottom-style:none;border-top-style:none;border-left-style:none;border-right-style:none;}
</style>
<s:if test='#request.handlemethod!="view"'>
	<g:LayOutWinTag buttonId="doclinks_img" title="附件"
		hasHtmlFrame="true" width="900" height="500"
		html="${ctx}/operfile/initFileList.action\?modelType=sampleFolicAcidTemp&id=${sampleFolicAcidTemp.id}"
		isHasSubmit="false" functionName="doc" />
</s:if>
<body>
<script language="javascript">
function changeimg(i)
{
	var zoom = parseInt(i.style.zoom,10)||100;
	zoom += event.wheelDelta / 12;
	if(zoom > 0 )
	i.style.zoom=zoom+'%';
	return false;
}
</script>
<%@ include file="/WEB-INF/page/include/toolbarNoTabBindClick.jsp"%>
<script type="text/javascript" src="${ctx}/js/sample/sampleFolicAcidTempEdit.js"></script>
<div style="overflow-y:auto;overflow-x:auto;width:500px;height:95%;float:left;" id="sampleInputItemImg"><img id="upLoadImg" onmousewheel="return changeimg(this)" src="${ctx}/operfile/downloadById.action?id=${sampleFolicAcidTemp.upLoadAccessory.id}"></div>  
  <div id="maintab" style="margin: 0 0 0 0"></div>
	<div id="markup" class="mainclass"> 
			<input type="hidden" id="handlemethod" value="${requestScope.handlemethod}">
            <input type="hidden" id="id" value="${requestScope.id}">
            <input type="hidden" id="path" value="${requestScope.path}">
             <input type="hidden" id="fname" value="${requestScope.fname}">
             <input type="hidden"  id="str" value="${requestScope.str}">
            <form name="form1" id="form1" method="post">
			<table class="frame-table">
			<tr>
				<td class="label-title" style="color: red">点击上传图片</td>
				<td></td>
				<td>
  					 <input type="button" value="上传信息录入图片" id="upLoadImg" onclick="upLoadImg1()" style="width: 150px;height: 30px; font-size: 13px;" />
				</td>
			</tr>
				<tr>
               	 	<td class="label-title" style="display:none">编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px" style="display:none">
               	 		
               	 	</td>            	 	
                   	<td align="left" style="display:none">
                   		<input type="hidden" size="20" maxlength="25" id="sampleInfo_id" name="sampleInfo.id" title="编号" value="<s:property value="sampleInfo.id"/>" style="display:none"/>
						<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_id" name="sampleFolicAcidTemp.id" title="编号" value="<s:property value="sampleFolicAcidTemp.id"/>" style="display:none"/>
						<input type="hidden"  id="upload_imga" name="sampleFolicAcidTemp.sampleInfo.upLoadAccessory.fileName" value="<s:property value="sampleFolicAcidTemp.sampleInfo.upLoadAccessory.fileName"/>">
                   		<input type ="hidden" id="saveType" value="ys" >
                   	</td>
  					
  					<td class="label-title">样本编号</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="sampleInfo_code" name="sampleInfo.code" readonly="readonly"  class="text input readonlytrue" title="样本编号" value="<s:property value="sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_code" class="text input readonlytrue" readonly="readonly" name="folicAcidTempNew.sampleInfo.code" title="样本编号" value="<s:property value="folicAcidTempNew.sampleInfo.code"/>" />
						<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_code" class="text input readonlytrue" readonly="readonly" name="folicAcidTempNew.code" title="样本编号" value="<s:property value="folicAcidTempNew.code"/>" />
                   	</td>
  					
                   	<td class="label-title">描述</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>
                   	<td align="left">
                   		<input type="text" size="20" maxlength="25" id="folicAcidTempNew_name" name="folicAcidTempNew.name" title="描述" value="<s:property value="folicAcidTempNew.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_name" name="sampleFolicAcidTemp.name" title="描述" value="<s:property value="sampleFolicAcidTemp.name"/>"/>
                   		<input type="hidden" size="20" maxlength="25" id="folicAcidTemp_name" name="folicAcidTemp.name" title="描述" value="<s:property value="folicAcidTemp.name"/>"/>
                   	</td>
                   	
                   	<td class="label-title">检测项目</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_productId" name="sampleFolicAcidTemp.productId"  value="<s:property value="sampleFolicAcidTemp.productId"/>"/>
  						<input type="hidden" id="sampleFolicAcidTemp_productName" name="sampleFolicAcidTemp.productName" value="<s:property value="sampleFolicAcidTemp.productName"/>"/>
  						<input type="hidden" id="folicAcidTemp_productId" name="folicAcidTemp.productId" value="<s:property value="folicAcidTemp.productId"/>">
  						<input type="hidden" id="folicAcidTemp_productName" name="folicAcidTemp.productName" value="<s:property value="folicAcidTemp.productName"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="productName" style="width:152px;height:28px;" onChange="javascript:document.getElementById('productNameNew').value=document.getElementById('productName').options[document.getElementById('productName').selectedIndex].value;this.nextSibling.value=this.value;a();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.productName"/>"><s:property value="sampleFolicAcidTemp.productName"/></option> 
							<option value="<s:property value="folicAcidTemp.productName"/>"><s:property value="folicAcidTemp.productName"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.productName" id="productNameNew" onfocus="voucherProductFun()" value="<s:property value="folicAcidTempNew.productName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="folicAcidTempNew.productId" id="productIdNew" value="<s:property value="folicAcidTempNew.productId"/>" />
                   	</td>
                   		
                </tr>
                <tr>		
                   	
                   	<td class="label-title">接收日期</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_acceptDate" name="sampleFolicAcidTemp.acceptDate" value="<s:property value="sampleFolicAcidTemp.acceptDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="folicAcidTemp_acceptDate" name="folicAcidTemp.acceptDate" value="<s:property value="folicAcidTemp.acceptDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="acceptDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('acceptDateNew').value=document.getElementById('acceptDate').options[document.getElementById('acceptDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.acceptDate"/>"><s:property value="sampleFolicAcidTemp.acceptDate"/></option> 
							<option value="<s:property value="folicAcidTemp.acceptDate"/>"><s:property value="folicAcidTemp.acceptDate"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.acceptDate" id="acceptDateNew" value="<s:property value="folicAcidTempNew.acceptDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="folicAcidTempNew.acceptDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">地区</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_area" name="sampleFolicAcidTemp.area"  value="<s:property value="sampleFolicAcidTemp.area"/>"/>
  						<input type="hidden" id="folicAcidTemp_area" name="folicAcidTemp.area" value="<s:property value="folicAcidTemp.area"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="area"style="width:152px;height:28px;" onChange="javascript:document.getElementById('areaNew').value=document.getElementById('area').options[document.getElementById('area').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.area"/>"><s:property value="sampleFolicAcidTemp.area"/></option> 
							<option value="<s:property value="folicAcidTemp.area"/>"><s:property value="folicAcidTemp.area"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.area" id="areaNew" value="<s:property value="folicAcidTempNew.area"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">送检医院</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_hospital" name="sampleFolicAcidTemp.hospital"  value="<s:property value="sampleFolicAcidTemp.hospital"/>"/>
  						<input type="hidden" id="folicAcidTemp_hospital" name="folicAcidTemp.hospital" value="<s:property value="folicAcidTemp.hospital"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="hospital"style="width:152px;height:28px;" onChange="javascript:document.getElementById('hospitalNew').value=document.getElementById('hospital').options[document.getElementById('hospital').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.hospital"/>"><s:property value="sampleFolicAcidTemp.hospital"/></option> 
							<option value="<s:property value="folicAcidTemp.hospital"/>"><s:property value="folicAcidTemp.hospital"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.hospital" id="hospitalNew" value="<s:property value="folicAcidTempNew.hospital"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
					<td class="label-title">送检时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_sendDate" name="sampleFolicAcidTemp.sendDate" value="<s:property value="sampleFolicAcidTemp.sendDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="folicAcidTemp_sendDate" name="folicAcidTemp.sendDate" value="<s:property value="folicAcidTemp.sendDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sendDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sendDateNew').value=document.getElementById('sendDate').options[document.getElementById('sendDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.sendDate"/>"><s:property value="sampleFolicAcidTemp.sendDate"/></option> 
							<option value="<s:property value="folicAcidTemp.sendDate"/>"><s:property value="folicAcidTemp.sendDate"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.sendDate" id="sendDateNew" value="<s:property value="folicAcidTempNew.sendDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="folicAcidTempNew.sendDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>                   	
                   	
                   	<td class="label-title">出报告时间</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_reportDate" name="sampleFolicAcidTemp.reportDate" value="<s:property value="sampleFolicAcidTemp.reportDate"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="folicAcidTemp_reportDate" name="folicAcidTemp.reportDate" value="<s:property value="folicAcidTemp.reportDate"/>" />
               	 		
               	 	</td>
                   	<td align="left">
						<select id="reportDate" style="width:152px;height:28px;" onChange="javascript:document.getElementById('reportDateNew').value=document.getElementById('reportDate').options[document.getElementById('reportDate').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.reportDate"/>"><s:property value="sampleFolicAcidTemp.reportDate"/></option> 
							<option value="<s:property value="folicAcidTemp.reportDate"/>"><s:property value="folicAcidTemp.reportDate"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.reportDate" id="reportDateNew" value="<s:property value="folicAcidTempNew.reportDate"/>" onfocus="WdatePicker({skin:'ext',dateFmt:'yyyy-MM-dd',maxDate:new Date()})" value="<s:date name="folicAcidTempNew.reportDate" format="yyyy-MM-dd"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td> 
                   	
                   	<td class="label-title">样本类型</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_sampleType_name" name="sampleFolicAcidTemp.sampleType.name"  value="<s:property value="sampleFolicAcidTemp.sampleType.name"/>"/>
  						<input type="hidden" id="sampleFolicAcidTemp_sampleType_id" name="sampleFolicAcidTemp.sampleType.id" value="<s:property value="sampleFolicAcidTemp.sampleType.id"/>"/>
  						<input type="hidden" id="folicAcidTemp_sampleType_name" name="folicAcidTemp.sampleType.name" value="<s:property value="folicAcidTemp.sampleType.name"/>">
  						<input type="hidden" id="folicAcidTemp_sampleType_id" name="folicAcidTemp.sampleType.id" value="<s:property value="folicAcidTemp.sampleType.id"/>">
               	 		
               	 	</td>
                   	<td align="left">
						<select id="sampleType" style="width:152px;height:28px;" onChange="javascript:document.getElementById('sampleTypeNew').value=document.getElementById('sampleType').options[document.getElementById('sampleType').selectedIndex].value;this.nextSibling.value=this.value;b();"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.sampleType.name"/>"><s:property value="sampleFolicAcidTemp.sampleType.name"/></option> 
							<option value="<s:property value="folicAcidTemp.sampleType.name"/>"><s:property value="folicAcidTemp.sampleType.name"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.sampleType.name" id="sampleTypeNew" onfocus="sampleKind()" value="<s:property value="folicAcidTempNew.sampleType.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                 		<input type="hidden" name="folicAcidTempNew.sampleType.id" id="sampleTypeIdNew" value="<s:property value="folicAcidTempNew.sampleType.id"/>" />
                   	</td>
                   	
                </tr>
                <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>病人信息</label>
						</div>
					</td>
				</tr>
                <tr>
                   	
                   	<td class="label-title">姓名</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_patientName" name="sampleFolicAcidTemp.patientName"  value="<s:property value="sampleFolicAcidTemp.patientName"/>"/>
  						<input type="hidden" id="folicAcidTemp_patientName" name="folicAcidTemp.patientName" value="<s:property value="folicAcidTemp.patientName"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="patientName"style="width:152px;height:28px;" onChange="javascript:document.getElementById('patientNameNew').value=document.getElementById('patientName').options[document.getElementById('patientName').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.patientName"/>"><s:property value="sampleFolicAcidTemp.patientName"/></option> 
							<option value="<s:property value="folicAcidTemp.patientName"/>"><s:property value="folicAcidTemp.patientName"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.patientName" id="patientNameNew" value="<s:property value="folicAcidTempNew.patientName"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">手机号码</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_phoneNum" name="sampleFolicAcidTemp.phoneNum"  value="<s:property value="sampleFolicAcidTemp.phoneNum"/>"/>
  						<input type="hidden" id="folicAcidTemp_phoneNum" name="folicAcidTemp.phoneNum" value="<s:property value="folicAcidTemp.phoneNum"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="phoneNum"style="width:152px;height:28px;" onChange="javascript:document.getElementById('phoneNumNew').value=document.getElementById('phoneNum').options[document.getElementById('phoneNum').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.phoneNum"/>"><s:property value="sampleFolicAcidTemp.phoneNum"/></option> 
							<option value="<s:property value="folicAcidTemp.phoneNum"/>"><s:property value="folicAcidTemp.phoneNum"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.phoneNum" id="phoneNumNew" value="<s:property value="folicAcidTempNew.phoneNum"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title">家庭住址</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_address" name="sampleFolicAcidTemp.address"  value="<s:property value="sampleFolicAcidTemp.address"/>"/>
  						<input type="hidden" id="folicAcidTemp_address" name="folicAcidTemp.address" value="<s:property value="folicAcidTemp.address"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="address"style="width:152px;height:28px;" onChange="javascript:document.getElementById('addressNew').value=document.getElementById('address').options[document.getElementById('address').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.address"/>"><s:property value="sampleFolicAcidTemp.address"/></option> 
							<option value="<s:property value="folicAcidTemp.address"/>"><s:property value="folicAcidTemp.address"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.address" id="addressNew" value="<s:property value="folicAcidTempNew.address"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">个人情况</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_otherTumorHistory" name="sampleFolicAcidTemp.otherTumorHistory"  value="<s:property value="sampleFolicAcidTemp.otherTumorHistory"/>"/>
  						<input type="hidden" id="folicAcidTemp_otherTumorHistory" name="folicAcidTemp.otherTumorHistory" value="<s:property value="folicAcidTemp.otherTumorHistory"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="otherTumorHistory"style="width:152px;height:28px;" onChange="javascript:document.getElementById('otherTumorHistoryNew').value=document.getElementById('otherTumorHistory').options[document.getElementById('otherTumorHistory').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.otherTumorHistory"/>"><s:property value="sampleFolicAcidTemp.otherTumorHistory"/></option> 
							<option value="<s:property value="folicAcidTemp.otherTumorHistory"/>"><s:property value="folicAcidTemp.otherTumorHistory"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.otherTumorHistory" id="otherTumorHistoryNew" value="<s:property value="folicAcidTempNew.otherTumorHistory"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">孕次</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_pregnancyTime" name="sampleFolicAcidTemp.pregnancyTime"  value="<s:property value="sampleFolicAcidTemp.pregnancyTime"/>"/>
  						<input type="hidden" id="folicAcidTemp_pregnancyTime" name="folicAcidTemp.pregnancyTime" value="<s:property value="folicAcidTemp.pregnancyTime"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="pregnancyTime"style="width:152px;height:28px;" onChange="javascript:document.getElementById('pregnancyTimeNew').value=document.getElementById('pregnancyTime').options[document.getElementById('pregnancyTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.pregnancyTime"/>"><s:property value="sampleFolicAcidTemp.pregnancyTime"/></option> 
							<option value="<s:property value="folicAcidTemp.pregnancyTime"/>"><s:property value="folicAcidTemp.pregnancyTime"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.pregnancyTime" id="pregnancyTimeNew" value="<s:property value="folicAcidTempNew.pregnancyTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title">产次</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_parturitionTime" name="sampleFolicAcidTemp.parturitionTime"  value="<s:property value="sampleFolicAcidTemp.parturitionTime"/>"/>
  						<input type="hidden" id="folicAcidTemp_parturitionTime" name="folicAcidTemp.parturitionTime" value="<s:property value="folicAcidTemp.parturitionTime"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="parturitionTime"style="width:152px;height:28px;" onChange="javascript:document.getElementById('parturitionTimeNew').value=document.getElementById('parturitionTime').options[document.getElementById('parturitionTime').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.parturitionTime"/>"><s:property value="sampleFolicAcidTemp.parturitionTime"/></option> 
							<option value="<s:property value="folicAcidTemp.parturitionTime"/>"><s:property value="folicAcidTemp.parturitionTime"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.parturitionTime" id="parturitionTimeNew" value="<s:property value="folicAcidTempNew.parturitionTime"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">第几次产胎</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_confirmAgeOne" name="sampleFolicAcidTemp.confirmAgeOne"  value="<s:property value="sampleFolicAcidTemp.confirmAgeOne"/>"/>
  						<input type="hidden" id="folicAcidTemp_confirmAgeOne" name="folicAcidTemp.confirmAgeOne" value="<s:property value="folicAcidTemp.confirmAgeOne"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="confirmAgeOne"style="width:152px;height:28px;" onChange="javascript:document.getElementById('confirmAgeOneNew').value=document.getElementById('confirmAgeOne').options[document.getElementById('confirmAgeOne').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.confirmAgeOne"/>"><s:property value="sampleFolicAcidTemp.confirmAgeOne"/></option> 
							<option value="<s:property value="folicAcidTemp.confirmAgeOne"/>"><s:property value="folicAcidTemp.confirmAgeOne"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.confirmAgeOne" id="confirmAgeOneNew" value="<s:property value="folicAcidTempNew.confirmAgeOne"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                  
                   	<td class="label-title">不良孕产史</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_badMotherhood" name="sampleFolicAcidTemp.badMotherhood"  value="<s:property value="sampleFolicAcidTemp.badMotherhood"/>"/>
  						<input type="hidden" id="folicAcidTemp_badMotherhood" name="folicAcidTemp.badMotherhood" value="<s:property value="folicAcidTemp.badMotherhood"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="badMotherhood"style="width:152px;height:28px;" onChange="javascript:document.getElementById('badMotherhoodNew').value=document.getElementById('badMotherhood').options[document.getElementById('badMotherhood').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.badMotherhood"/>"><s:property value="sampleFolicAcidTemp.badMotherhood"/></option> 
							<option value="<s:property value="folicAcidTemp.badMotherhood"/>"><s:property value="folicAcidTemp.badMotherhood"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.badMotherhood" id="badMotherhoodNew" value="<s:property value="folicAcidTempNew.badMotherhood"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                 </tr>
                 <tr>
					<td colspan="9">
						<div class="standard-section-header type-title">
							<label>收费情况</label>
						</div>
					</td>
				</tr>
                <tr>
                   	
                   	<td class="label-title">是否收费</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_isPay" name="sampleFolicAcidTemp.isPay" value="<s:property value="sampleFolicAcidTemp.isPay"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="folicAcidTemp_isPay" name="folicAcidTemp.isPay"  value="<s:property value="folicAcidTemp.isPay"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="folicAcidTempNew.isPay" id="folicAcidTempNew_isPay">
							<option value="" <s:if test="folicAcidTempNew.isPay==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="folicAcidTempNew.isPay==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="folicAcidTempNew.isPay==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title">是否开发票</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" size="20" maxlength="25" id="sampleFolicAcidTemp_isInvoice" name="sampleFolicAcidTemp.isInvoice" value="<s:property value="sampleFolicAcidTemp.isInvoice"/>" />
               	 		<input type="hidden" size="20" maxlength="25" id="folicAcidTemp_isInvoice" name="folicAcidTemp.isInvoice"  value="<s:property value="folicAcidTemp.isInvoice"/>" />
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="folicAcidTempNew.isInvoice" id="folicAcidTempNew_isInvoice">
							<option value="" <s:if test="folicAcidTempNew.isInvoice==''">selected="selected" </s:if>>请选择</option>
	    					<option value="0" <s:if test="folicAcidTempNew.isInvoice==0">selected="selected" </s:if>>是</option>
	    					<option value="1" <s:if test="folicAcidTempNew.isInvoice==1">selected="selected" </s:if>>否</option>
						</select>
					<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                   	
                   	<td class="label-title" >审核人</td>
               	 	<td class="requiredcolumn" nowrap width="10px" ></td>            	 	
                   	<td align="left"  >
                   		<input type="text" size="20" maxlength="25" id="folicAcidTempNew_createUser" class="text input readonlytrue" readonly="readonly"
                   	 		name="folicAcidTempNew.createUser" title="审入人" value="<s:property value="folicAcidTempNew.createUser"/>" />
                   	</td>
                   	
                </tr>
                <tr>
                   	
                   	<td class="label-title">备注</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_reason" name="sampleFolicAcidTemp.reason"  value="<s:property value="sampleFolicAcidTemp.reason"/>"/>
  						<input type="hidden" id="folicAcidTemp_reason" name="folicAcidTemp.reason" value="<s:property value="folicAcidTemp.reason"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reason"style="width:152px;height:28px;" onChange="javascript:document.getElementById('reasonNew').value=document.getElementById('reason').options[document.getElementById('reason').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.reason"/>"><s:property value="sampleFolicAcidTemp.reason"/></option> 
							<option value="<s:property value="folicAcidTemp.reason"/>"><s:property value="folicAcidTemp.reason"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.reason" id="reasonNew" value="<s:property value="folicAcidTempNew.reason"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>

                   	<td class="label-title">金额</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_money" name="sampleFolicAcidTemp.money"  value="<s:property value="sampleFolicAcidTemp.money"/>"/>
  						<input type="hidden" id="folicAcidTemp_money" name="folicAcidTemp.money" value="<s:property value="folicAcidTemp.money"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="money"style="width:152px;height:28px;" onChange="javascript:document.getElementById('moneyNew').value=document.getElementById('money').options[document.getElementById('money').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.money"/>"><s:property value="sampleFolicAcidTemp.money"/></option> 
							<option value="<s:property value="folicAcidTemp.money"/>"><s:property value="folicAcidTemp.money"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.money" id="moneyNew" value="<s:property value="folicAcidTempNew.money"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                   	<td class="label-title">收据/pos</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_receipt" name="sampleFolicAcidTemp.receipt"  value="<s:property value="sampleFolicAcidTemp.receipt"/>"/>
  						<input type="hidden" id="folicAcidTemp_receipt" name="folicAcidTemp.receipt" value="<s:property value="folicAcidTemp.receipt"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="receipt"style="width:152px;height:28px;" onChange="javascript:document.getElementById('receiptNew').value=document.getElementById('receipt').options[document.getElementById('receipt').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.receipt"/>"><s:property value="sampleFolicAcidTemp.receipt"/></option> 
							<option value="<s:property value="folicAcidTemp.receipt"/>"><s:property value="folicAcidTemp.receipt"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.receipt" id="receiptNew" value="<s:property value="folicAcidTempNew.receipt"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
                   	
                </tr>
                <tr>
                   	<td class="label-title">开票单位</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_paymentUnit" name="sampleFolicAcidTemp.paymentUnit"  value="<s:property value="sampleFolicAcidTemp.paymentUnit"/>"/>
  						<input type="hidden" id="folicAcidTemp_paymentUnit" name="folicAcidTemp.paymentUnit" value="<s:property value="folicAcidTemp.paymentUnit"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="paymentUnit"style="width:152px;height:28px;" onChange="javascript:document.getElementById('paymentUnitNew').value=document.getElementById('paymentUnit').options[document.getElementById('paymentUnit').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.paymentUnit"/>"><s:property value="sampleFolicAcidTemp.paymentUnit"/></option> 
							<option value="<s:property value="folicAcidTemp.paymentUnit"/>"><s:property value="folicAcidTemp.paymentUnit"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.paymentUnit" id="paymentUnitNew" value="<s:property value="folicAcidTempNew.paymentUnit"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                   	</td>
              
                	<g:LayOutWinTag buttonId="showreportMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="reportManUserFun" 
 					hasSetFun="true"
					documentId="sampleFolicAcidTemp_reportMan"
					documentName="sampleFolicAcidTemp_reportMan_name" />
                   	
                   	<td class="label-title">核对人1</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_reportMan_id" name="sampleFolicAcidTemp.reportMan.id"  value="<s:property value="sampleFolicAcidTemp.reportMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleFolicAcidTemp_reportMan_name" name="sampleFolicAcidTemp.reportMan.name"  value="<s:property value="sampleFolicAcidTemp.reportMan.name"/>" class="text input" />
  						<input type="hidden" id="folicAcidTemp_reportMan_id" name="folicAcidTemp.reportMan.id" value="<s:property value="folicAcidTemp.reportMan.id"/>">
  						<input type="hidden" id="folicAcidTemp_reportMan_name" name="folicAcidTemp.reportMan.name" value="<s:property value="folicAcidTemp.reportMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="reportMan" style="width:152px;height:28px;margin-top: 8px;" onChange="javascript:document.getElementById('reportManNew').value=document.getElementById('reportMan').options[document.getElementById('reportMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.reportMan.name"/>"><s:property value="sampleFolicAcidTemp.reportMan.name"/></option> 
							<option value="<s:property value="folicAcidTemp.reportMan.name"/>"><s:property value="folicAcidTemp.reportMan.name"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.reportMan.name" id="reportManNew" value="<s:property value="folicAcidTempNew.reportMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="folicAcidTempNew.reportMan.id" id="reportManIdNew" value="<s:property value="folicAcidTempNew.reportMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' style="margin-left: 20px;" class='detail' /> --%>
                   	</td>
                   	
                   	
                 	<g:LayOutWinTag buttonId="showauditMan" title="选择核对人"
					hasHtmlFrame="true"
					html="${ctx}/core/user/userSelect.action"
					isHasSubmit="false" functionName="auditManUserFun" 
 					hasSetFun="true"
					documentId="sampleFolicAcidTemp_auditMan"
					documentName="sampleFolicAcidTemp_auditMan_name" />
                   	
                   	<td class="label-title">核对人2</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		<input type="hidden" id="sampleFolicAcidTemp_auditMan_id" name="sampleFolicAcidTemp.auditMan.id"  value="<s:property value="sampleFolicAcidTemp.auditMan.id"/>" class="text input" />
               	 		<input type="hidden" id="sampleFolicAcidTemp_auditMan_name" name="sampleFolicAcidTemp.auditMan.name"  value="<s:property value="sampleFolicAcidTemp.auditMan.name"/>" class="text input" />
  						<input type="hidden" id="folicAcidTemp_auditMan_id" name="folicAcidTemp.auditMan.id" value="<s:property value="folicAcidTemp.auditMan.id"/>">
  						<input type="hidden" id="folicAcidTemp_auditMan_name" name="folicAcidTemp.auditMan.name" value="<s:property value="folicAcidTemp.auditMan.name"/>">
               	 		
               	 	</td>            	 	
                   	<td align="left">
						<select id="auditMan" style="width:152px;height:28px;margin-top: 8px;" onChange="javascript:document.getElementById('auditManNew').value=document.getElementById('auditMan').options[document.getElementById('auditMan').selectedIndex].value;this.nextSibling.value=this.value;"> 
							<option value="" style="color:#c2c2c2;">---请选择---</option> 
							<option value="<s:property value="sampleFolicAcidTemp.auditMan.name"/>"><s:property value="sampleFolicAcidTemp.auditMan.name"/></option> 
							<option value="<s:property value="folicAcidTemp.auditMan.name"/>"><s:property value="folicAcidTemp.auditMan.name"/></option>
						</select> 
						<input type="text" name="folicAcidTempNew.auditMan.name" id="auditManNew" value="<s:property value="folicAcidTempNew.auditMan.name"/>" style="width:128px;height:24px;border:0pt;margin-top: -26px;margin-left: 2px"> 
                		<input type="hidden" name="folicAcidTempNew.auditMan.id" id="reportManIdNew" value="<s:property value="folicAcidTempNew.auditMan.id"/>"/>
<%--                    		<img alt='选择核对人' id='showreportMan' src='${ctx}/images/img_lookup.gif' style="margin-left: 20px;" class='detail' /> --%>
                   	</td>
                   	
                </tr>
                <tr>
                	<td class="label-title">下一步流向</td>
               	 	<td class="requiredcolumn" nowrap width="10px">
               	 		
               	 	</td>            	 	
                   	<td align="left">
                   		<select name="folicAcidTempNew.nextStepFlow" id="folicAcidTempNew_nextStepFlow">
							<option value="" <s:if test="folicAcidTempNew.nextStepFlow==''">selected="selected" </s:if>>请选择</option>
	    					<option value="1" <s:if test="folicAcidTempNew.nextStepFlow==1">selected="selected" </s:if>>合格</option>
	    					<option value="0" <s:if test="folicAcidTempNew.nextStepFlow==0">selected="selected" </s:if>>反馈项目管理</option>
						</select>
						<img class='requiredimage' src='${ctx}/images/required.gif' />
                   	</td>
                </tr>
              
            </table>
            <input type="hidden"  id="id_parent_hidden" value="<s:property value="sampleFolicAcid.id"/>" />
            </form>
            <!-- <div id="tabs">
            <ul>
           	</ul>  -->
			</div>
	</body>
	</html>
