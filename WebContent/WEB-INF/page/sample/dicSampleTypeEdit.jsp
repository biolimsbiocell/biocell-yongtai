<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
<title></title>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${ctx}/css/sampleIcon/iconfont.css" />
<style>
.dataTables_scrollBody {
	min-height: 100px;
}

.site-doc-icon li:hover {
	background-color: #eee;
	cursor: pointer;
}

.site-doc-icon li {
	display: inline-block;
	vertical-align: middle;
	width: 100px;
	line-height: 16px;
	padding: 20px 0;
	margin-right: -1px;
	margin-bottom: -1px;
	border: 1px solid #e2e2e2;
	font-size: 12px;
	text-align: center;
	color: #666;
	transition: all .3s;
	-webkit-transition: all .3s;
}

.site-doc-icon li.chosed {
	box-shadow: 0 0 4px blue;
	color: #fff;
}

.site-doc-icon li.chosed .name {
	color: #fff;
}

.site-doc-icon li .iconfont {
	font-size: 30px;
}

.site-doc-icon li .name {
	color: #c2c2c2;
	padding-top: 7px;
}
</style>
</head>

<body>
	<!--toolbar按钮组-->
	<div>
		<%@ include file="/WEB-INF/page/include/newToolBarNoState.jsp"%>
	</div>
	<div>
		<%@ include file="/WEB-INF/page/include/fileInputModal.jsp"%>
	</div>
	<div style="height: 14px"></div>
	<input type="hidden" id="handlemethod"
		value="${requestScope.handlemethod}">
	<div class="container-fluid" style="margin-top: 46px">
		<!--Form表单-->
		<div class="col-xs-12" style="padding: 0px">
			<div class="box box-info box-solid">
				<div class="box-header with-border">
					<i class="fa fa-bell-o"></i>
					<h3 class="box-title">
						<fmt:message key="biolims.common.sampleType" />
					</h3>

					<div class="box-tools pull-right" style="display: none;">
						<button type="button" class="btn btn-box-tool" id="tableRefresh"
							onclick="tableRefreshItem()">
							<i class="glyphicon glyphicon-refresh"></i>
						</button>
						<div class="btn-group">
							<button type="button" class="btn btn-default dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false">
								Action <span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="####" onclick="$('.buttons-print').click();">
										<fmt:message key="biolims.common.print" />
								</a></li>
								<li><a href="#####" onclick="$('.buttons-copy').click();">
										<fmt:message key="biolims.common.copyData" />
								</a></li>
								<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
								</li>
								<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
								</li>
								<li role="separator" class="divider"></li>
								<li><a href="####" onclick="itemFixedCol(2)"> <fmt:message
											key="biolims.common.lock2Col" />
								</a></li>
								<li><a href="####" id="unfixde" onclick="unfixde()"> <fmt:message
											key="biolims.common.cancellock2Col" />
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-body ipadmini">
					<!--form表单-->
					<div class="HideShowPanel">
						<form name="form1" class="layui-form" id="form1" method="post">
							<input type="hidden" id="bpmTaskId"
								value="<%=request.getParameter(" bpmTaskId ")%>" /> <br>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.id" /> <img class='requiredimage'
											src='${ctx}/images/required.gif' /></span> <input type="text"
											readonly="readOnly" size="20" maxlength="25"
											id="dicSampleType_id" name="id"
											changelog="<s:property value=" dicSampleType.id"/>"
											class="form-control"
											value="<s:property value=" dicSampleType.id "/>" />
									</div>
								</div>

								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.state" /> </span> <input type="text"
											size="20" readonly="readOnly" id="dicSampleType_stateName"
											name="stateName"
											changelog="<s:property value=" dicSampleType.stateName "/>"
											value="<s:property value=" dicSampleType.stateName "/>"
											class="form-control" /> <input type="hidden"
											id="dicSampleType_state" name="state"
											value="<s:property value=" dicSampleType.state "/>">
										<%-- <span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showstate()">
												<i class="glyphicon glyphicon-search"></i>
										</span> --%>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.name" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="50" maxlength="50" id="dicSampleType_name"
											name="name"
											changelog="<s:property value=" dicSampleType.name "/>"
											class="form-control"
											value="<s:property value=" dicSampleType.name "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.type" /> </span> <input type="text"
											size="20" readonly="readOnly" id="dicSampleType_type_name"
											changelog="<s:property value=" dicSampleType.type.name "/>"
											value="<s:property value=" dicSampleType.type.name "/>"
											class="form-control" /> <input type="hidden"
											id="dicSampleType_type_id" name="type-id"
											value="<s:property value=" dicSampleType.type.id "/>">
										<span class="input-group-btn">
											<button class="btn btn-info" type="button"
												onClick="showtype()">
												<i class="glyphicon glyphicon-search"></i>
											</button>
										</span>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"> <fmt:message
												key="biolims.dicSampleType.first" />
										</span> <input type="hidden" name="first "
											changelog="<s:property value="dicSampleType.first"/>"
											id="dicSampleType_first "
											value="<s:property value=" dicSampleType.first " />" /> <select
											class="form-control" name="dicSampleType.first">
											<option value="1"
												<s:if test="dicSampleType.first==1">selected="selected"</s:if>>
												是</option>
											<option value="0"
												<s:if test="dicSampleType.first==0">selected="selected"</s:if>>
												否</option>
										</select>
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.note" /> </span> <input type="text"
											size="20" maxlength="25" id="dicSampleType_note" name="note"
											changelog="<s:property value=" dicSampleType.note"/>"
											class="form-control"
											value="<s:property value=" dicSampleType.note "/>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.picCode" /> <img
											class='requiredimage' src='${ctx}/images/required.gif' /> </span> <input
											type="text" size="20" maxlength="25"
											id="dicSampleType_picCode" readonly="readonly"
											placeholder="请选择下面的图标" name="picCode"
											changelog="<s:property value=" dicSampleType.picCode "/>"
											class="form-control"
											value="<s:property value=" dicSampleType.picCode "/>" />
									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon">样本名称代码<%-- <fmt:message
												key="biolims.dicSampleType.code" /> --%> </span> <input type="text"
											size="20" maxlength="25" id="dicSampleType_code" name="code"
											changelog="<s:property value=" dicSampleType.code "/>"
											class="form-control"
											value="<s:property value=" dicSampleType.code "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.ordeRnumber" /> </span> <input
											type="text" size="20" maxlength="25"
											id="dicSampleType_orderNumber" name="orderNumber"
											changelog="<s:property value=" dicSampleType.orderNumber "/>"
											class="form-control"
											value="<s:property value=" dicSampleType.orderNumber "/>" />

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">

										<span class="input-group-addon"><fmt:message
												key="biolims.dicSampleType.picCodeName" /> </span> <input
											type="text" size="20" maxlength="25"
											id="dicSampleType_picCodeName" name="picCodeName"
											changelog="<s:property value=" dicSampleType.picCodeName "/>"
											class="form-control"
											value="<s:property value=" dicSampleType.picCodeName "/>" />

									</div>
								</div>
								<div class="col-md-4 col-sm-6 col-xs-12">
									<div class="input-group">
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileUp()">
											<fmt:message key="biolims.common.uploadAttachment" />
										</button>
										&nbsp;&nbsp;
										<%-- <span class="text label"><fmt:message
											key="biolims.common.aTotalOf" />${requestScope.fileNum}&nbsp;&nbsp;<fmt:message
											key="biolims.common.attachment" /> --%>
										<button type="button" class="btn btn-info btn-sm"
											onclick="fileView()">
											<fmt:message key="biolims.report.checkFile" />
										</button>
									</div>
								</div>
							</div>
							<br> <input type="hidden" id="id_parent_hidden"
								value="<s:property value=" dicSampleType.id "/>" /> <input
								type="hidden" id="fieldContent" name="fieldContent"
								value="<s:property value=" dicSampleType.fieldContent "/>" />

						</form>
					</div>
					<!--table表格-->
				</div>
				<div class="box-footer">
					<div class="bs-glyphicons">
						<ul class="site-doc-icon">
							<li><i class="iconfont icon-zhijia"></i>
								<div class="name">
									<fmt:message key="biolims.common.nail" />
								</div>
								<div class="code">icon-zhijia</div></li>
							<li><i class="iconfont icon-xuexibao"></i>
								<div class="name">
									<fmt:message key="biolims.common.bloodCorpuscle" />
								</div>
								<div class="code">icon-xuexibao</div></li>
							<li><i class="iconfont icon-jingban"></i>
								<div class="name">
									<fmt:message key="biolims.common.fineSpot" />
								</div>
								<div class="code">icon-jingban</div></li>
							<li><i class="iconfont icon-mianyipian"></i>
								<div class="name">
									<fmt:message key="biolims.common.immunohistochemicalFilm" />
								</div>
								<div class="code">icon-zhijia</div></li>
							<li><i class="iconfont icon-kouqiang"></i>
								<div class="name">
									<fmt:message key="biolims.common.oralSwab" />
								</div>
								<div class="code">icon-kouqiang</div></li>
							<li><i class="iconfont icon-jingye"></i>
								<div class="name">
									<fmt:message key="biolims.common.semen" />
								</div>
								<div class="code">icon-jingye</div></li>
							<li><i class="iconfont icon-dna1"></i>
								<div class="name">DNA</div>
								<div class="code">icon-dna1</div></li>
							<li><i class="iconfont icon-xinzang"></i>
								<div class="name">
									<fmt:message key="biolims.common.heart" />
								</div>
								<div class="code">icon-xinzang</div></li>
							<li><i class="iconfont icon-xuechendian"></i>
								<div class="name">
									<fmt:message key="biolims.common.bloodSedimentation" />
								</div>
								<div class="code">icon-xuechendian</div></li>
							<li><i class="iconfont icon-qiepian"></i>
								<div class="name">
									<fmt:message key="biolims.common.HEPathologicalSection" />
								</div>
								<div class="code">icon-qiepian</div></li>
							<li><i class="iconfont icon-niaoye"></i>
								<div class="name">
									<fmt:message key="biolims.common.urine" />
								</div>
								<div class="code">icon-niaoye</div></li>
							<li><i class="iconfont icon-rna"></i>
								<div class="name">RNA</div>
								<div class="code">icon-rna</div></li>
							<li><i class="iconfont icon-shila"></i>
								<div class="name">
									<fmt:message key="biolims.common.paraffinWax" />
								</div>
								<div class="code">icon-shila</div></li>
							<li><i class="iconfont icon-paozuzhi"></i>
								<div class="name">
									<fmt:message key="biolims.common.faureMarinSoakedTissue" />
								</div>
								<div class="code">icon-paozuzhi</div></li>
							<li><i class="iconfont icon-gusui"></i>
								<div class="name">
									<fmt:message key="biolims.common.boneMarrow" />
								</div>
								<div class="code">icon-gusui</div></li>
							<li><i class="iconfont icon-tuoye"></i>
								<div class="name">
									<fmt:message key="biolims.common.saliva" />
								</div>
								<div class="code">icon-tuoye</div></li>
							<li><i class="iconfont icon-xiongshui"></i>
								<div class="name">
									<fmt:message key="biolims.common.pleuralEffusion" />
								</div>
								<div class="code">icon-xiongshui</div></li>
							<li><i class="iconfont icon-kangxuening"></i>
								<div class="name">
									<fmt:message key="biolims.common.anticoagulantEDTA" />
								</div>
								<div class="code">icon-kangxuening</div></li>
							<li><i class="iconfont icon-ffpe"></i>
								<div class="name">
									<fmt:message key="biolims.common.FFPEHundredTablets" />
								</div>
								<div class="code">icon-ffpe</div></li>
							<li><i class="iconfont icon-ffpe1"></i>
								<div class="name">
									<fmt:message key="biolims.common.FFPEFilm" />
								</div>
								<div class="code">icon-ffpe1</div></li>
							<li><i class="iconfont icon-waixuejiang"></i>
								<div class="name">
									<fmt:message key="biolims.common.ExternalPlasma" />
								</div>
								<div class="code">icon-waixuejiang</div></li>
							<li><i class="iconfont icon-neixuejiang"></i>
								<div class="name">
									<fmt:message key="biolims.common.internalSeparationPlasma" />
								</div>
								<div class="code">icon-neixuejiang</div></li>
							<li><i class="iconfont icon-baixibao"></i>
								<div class="name">
									<fmt:message key="biolims.common.leucocyte" />
								</div>
								<div class="code">icon-baixibao</div></li>
							<li><i class="iconfont icon-fushui"></i>
								<div class="name">
									<fmt:message key="biolims.common.ascites" />
								</div>
								<div class="code">icon-fushui</div></li>
							<li><i class="iconfont icon-naosui"></i>
								<div class="name">
									<fmt:message key="biolims.common.cerebrospinalFluid" />
								</div>
								<div class="code">icon-naosui</div></li>
							<li><i class="iconfont icon-xueye"></i>
								<div class="name">
									<fmt:message key="biolims.common.blood" />
								</div>
								<div class="code">icon-xueye</div></li>
							<li><i class="iconfont icon-zuzhi"></i>
								<div class="name">
									<fmt:message key="biolims.common.freshTissue" />
								</div>
								<div class="code">icon-zuzhi</div></li>


						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
	<script type="text/javascript"
		src="${ctx}/js/sample/dicSampleTypeEdit.js"></script>
	<script type="text/javascript"
		src="${ctx}/javascript/common/dataTablesExtend.js"></script>
</body>

</html>