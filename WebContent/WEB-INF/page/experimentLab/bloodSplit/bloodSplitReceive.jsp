<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<style type="text/css">
			.layui-layer-content{
				padding: 0 20px;
			}
		</style>
</head>
<body style="height: 94%">
	<div class="content-wrapper" id="content" style="margin-left: 0px;">
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title" id="titleNote">
							第一次审核
						</h3>
					</div>
					<input type="hidden" id="bloodSplitReceive_type" value="${requestScope.type}" />
					<div id="gu" class="box-body ipadmini">
									<table class="table table-hover table-striped table-bordered table-condensed" id="bloodSplitReceiveDiv" style="font-size: 14px;"></table>
					</div>
					<div class="box-footer">
						<div class="pull-right">
							<button type="button" class="btn btn-primary" id="save">
								<i class="glyphicon glyphicon-save"></i><fmt:message key="biolims.common.save"/>
							</button>
							<%-- <button type="button" class="btn btn-primary"
								onclick="executeAbnormal()">
								<i class="glyphicon glyphicon-saved"></i><fmt:message key="biolims.common.receive"/>
							</button> --%>
						</div>
					</div>
				</div>
			</div>

		</div>

		</section>
	</div>
	<script type="text/javascript"
		src="${ctx}/js/experimentLab/bloodSplit/bloodSplitReceive.js"></script>
</body>

</html>