﻿
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<style>
.dataTables_scrollBody {
	min-height: 100px;
}

.tablebtns {
	float: right;
}

.dt-buttons {
	margin: 0;
	float: right;
}

.chosed {
	background-color: #5AC8D8 !important;
	color: #fff;
}
</style>
</head>
<body>
	<!-- <div>
		<table class="table table-hover table-bordered table-condensed"
			id="addNextFlow" style="font-size: 12px;"></table>
	</div>
	<div id="show_dialog_nextFlowTab_div"></div> -->
	<div class="row">
		<div class="panel"
			style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
			<div class="panel-heading text-left">
				<h3 class="panel-title" style="font-family: 黑体;">
					<i class="glyphicon glyphicon-bookmark"></i>
					接样记录
				</h3>
			</div>
		</div>
	</div>
	<br>
	<div id="show_samplereceive_div">
		<!-- <a href="login.jsp">接样记录单</a> -->
	</div>
	<div class="row">
		<div class="panel"
			style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
			<div class="panel-heading text-left">
				<h3 class="panel-title" style="font-family: 黑体;">
					<i class="glyphicon glyphicon-bookmark"></i>
					批生产记录
				</h3>
			</div>
		</div>
	</div>
	<br>
	<div id="show_cellpassage_div">
		<!-- <a href="javascript:void(0)" onclick="dayin('CP1905080003','1');">外周血分离</a><br>
		<a href="javascript:void(0)">补液</a><br>
		<a href="login.jsp">装袋</a><br>
		<a href="login.jsp">收获</a><br> -->
	</div>
	<div class="row">
		<div class="panel"
			style="height: 30px; border-radius: 0; margin-bottom: 0px; padding: 0px">
			<div class="panel-heading text-left">
				<h3 class="panel-title" style="font-family: 黑体;">
					<i class="glyphicon glyphicon-bookmark"></i>
					产品质控记录
				</h3>
			</div>
		</div>
	</div>
	<br>
	<div id="show_cellpassageresult_div">
		<a onclick="showcpjyy()">成品检验报告单(一)</a><br>
		<a onclick="showcpjye()">成品检验报告单(二)</a><br>
	</div>
	<input type="hidden" id="ordernum" value="${requestScope.ordernum}" />
	
	<script type="text/javascript"
		src="${ctx}/js/experimentLab/bloodSplit/showPiChuLiDialog.js"></script>
</body>
</html>



