<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
</head>
<script type="text/javascript">
	function setvalue(rec){
		 <%if(request.getParameter("flag")!=null){%>
		 	<%
		 if(request.getParameter("funName")!=null){%>
		 	window.parent.set<%=request.getParameter("funName")%>(rec);
		 	<%
		  }else{%>
		  window.parent.set<%=request.getParameter("flag")%>(rec); 
			  
		  	<%}%>
		  <%}%>
	}
	
</script>
<body>
<input type="button" value=<fmt:message key="biolims.common.confirm"/> onclick="javascript:gridSetSelectionValue();">
<input type="button" value=<fmt:message key="biolims.common.empty"/> onclick="javascript:setNullValue();">
<script language = "javascript" >
function gridSetSelectionValue(){
var r = gridGrid.getSelectionModel().getSelected(); 
setvalue(r);
}
var gridGrid;
Ext.onReady(function(){
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name: 'id'},{name: 'name'},{name: 'note'},{name: 'sysCode'}],
proxy: new Ext.data.HttpProxy({url: '/dic/type/dicTypeSelectJson.action?flag=slx',method: 'POST'})
});
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
autoExpandColumn: 'note',
height:document.body.clientHeight-15,
title:'',
store: store,
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:[new Ext.grid.RowNumberer(),{header:'id',sortable: true,hidden:true,dataIndex: 'id',width: 100},{header:biolims.user.eduName1,sortable: true,dataIndex: 'name',width: 200},{id:'note',header:biolims.sample.note,sortable: true,dataIndex: 'note',width: 240},{id:'sysCode',header:biolims.common.systemID,sortable: true,hidden:true,dataIndex: 'sysCode',width: 100}],
stripeRows: true,
bbar: new Ext.PagingToolbar({
pageSize: parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1),
store: store,
displayInfo: true,
displayMsg:biolims.common.displayMsg,
beforePageText:biolims.common.page,
afterPageText:biolims.common.afterPageText,
emptyMsg:biolims.common.noData
})
});
gridGrid.render('grid');
gridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
});
gridGrid.on('rowdblclick',function(){gridSetSelectionValue();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1)}});
});
</script >
<input type='hidden'  id='extJsonDataString'  name='extJsonDataString' value=''/>
<div id='grid' ></div>



</body>
</html>