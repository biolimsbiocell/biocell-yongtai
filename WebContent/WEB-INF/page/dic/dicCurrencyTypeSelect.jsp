<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
</head>
<script type="text/javascript">
	function setvalue(id,name){
		<%if(request.getParameter("flag")==null){%>
		 window.parent.setvalue(id,name);
		 <%}else{%>
		 window.parent.set<%=request.getParameter("flag")%>(id,name);
		 <%}%>
	}
</script>
<body>
	<script language = "javascript" >
function gridSetSelectionValue(){
var r = gridGrid.getSelectionModel().getSelected(); 
setvalue(r.data.id,r.data.name);
}
var gridGrid;
Ext.onReady(function(){
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'note',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: ctx+'/dic/currency/currencyTypeSelectJson.action',method: 'POST'})
});
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
gridGrid = new Ext.grid.GridPanel({
autoWidth:true,
height:document.body.clientHeight,
title:'',
store: store,
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:[new Ext.grid.RowNumberer(),{dataIndex:'id',header: 'ID',width: 32,sortable: true,hidden: true},{dataIndex:'name',header: biolims.common.designation,width: 110,sortable: true,hidden: false},{dataIndex:'note',header: biolims.common.note,width: 80,sortable: true,hidden: false}],
stripeRows: true,
bbar: new Ext.PagingToolbar({
pageSize: parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1),
store: store,
displayInfo: true,
displayMsg:biolims.common.displayMsg,
beforePageText:biolims.common.page,
afterPageText:biolims.common.afterPageText,
emptyMsg:biolims.common.noData
})
});
gridGrid.render('grid');
gridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
});
gridGrid.on('rowdblclick',function(){gridSetSelectionValue();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1)}});
});
</script >
<input type='hidden'  id='extJsonDataString'  name='extJsonDataString' value=''/>
<div id='grid' ></div>


</body>
</html>