<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</style>
<%@ include file="/WEB-INF/page/include/common.jsp"%>
<link rel="stylesheet" href="${ctx}/css/experimentLab.css" />
<link rel="stylesheet" href="${ctx}/lims/css/autocomplete.css" />
</head>
<body>
	<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Modal title</h4>
				</div>
				<div class="modal-body">
					<textarea id="checkCounts" class="form-control hide"
						style="width: 99%; height: 99%;"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="saveModal()">确认</button>
				</div>
			</div>
		</div>
	</div>
	<div class="content-wrapper" id="content" style="margin-left: 0px;">
		<section class="content">
		<div class="row">
			<!--表格-->
			<div class="col-xs-12">
				<div class="box box-info box-solid">
					<div class="box-header with-border">
						<i class="fa fa-bell-o"></i>
						<h3 class="box-title">
							<fmt:message key="biolims.common.dicManageMent" />
						</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" id="tableRefresh"
								onclick="tableRefresh()">
								<i class="glyphicon glyphicon-refresh"></i>
							</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown" aria-haspopup="true"
									aria-expanded="false">
									Action <span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="####" onclick="$('.buttons-print').click();"><fmt:message
												key="biolims.common.print" /></a></li>
									<li><a href="#####" onclick="$('.buttons-copy').click();"><fmt:message
												key="biolims.common.copyData" /></a></li>
									<li><a href="####" onclick="$('.buttons-excel').click();">Excel</a>
									</li>
									<li><a href="####" onclick="$('.buttons-csv').click();">CSV</a>
									</li>
									<li role="separator" class="divider"></li>
									<li><a href="####" onclick="fixedCol(2)"><fmt:message
												key="biolims.common.lock2Col" /></a></li>
									<li><a href="####" id="unfixde" onclick="unfixde()"><fmt:message
												key="biolims.common.cancellock2Col" /></a></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="box-body">
						<div class="row">

							<div class="col-xs-4">
								<div id="type_names"></div>
								<select class="form-control" id="types">
									<option value=""
										<s:if test="tunitGroupNew.state==''">selected="selected"</s:if>>请选择类型查询
									</option>
								</select>
							</div>
						</div>
					</div>

					<div class="box-body ipadmini">
						<table
							class="table table-hover table-striped table-bordered table-condensed"
							id="showDicTypeDiv" style="font-size: 14px;">
						</table>
					</div>
				</div>
			</div>

		</div>

		</section>
	</div>
</body>
<script src="${ctx}\lims\plugins\layer\layer.js" type="text/javascript"
	charset="utf-8"></script>
<script src="${ctx}\lims\plugins\jQuery\autocomplete.js"
	type="text/javascript" charset="utf-8"></script>
<%-- <script type="text/javascript"
	src="${ctx}/lims/plugins/ckeditor/ckeditor.js"></script> --%>
<script type="text/javascript" src="${ctx}/js/workflow-common.js"></script>
<script type="text/javascript"
	src="${ctx}/javascript/dic/showDicTypeList.js"></script>
</html>



