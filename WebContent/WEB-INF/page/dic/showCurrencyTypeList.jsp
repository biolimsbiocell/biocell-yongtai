<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<%@ include file="/WEB-INF/page/include/common3.jsp"%>
<%@ include file="/WEB-INF/page/include/common1.jsp"%>
<script type="text/javascript"
	src="${ctx}/javascript/dic/showCurrencyTypeList.js"></script>
</head>
<g:HandleDataExtTag hasConfirm="false" paramsValue="" funName="modifyData" url="${ctx}/dic/currency/editDicCurrencyType.action"/>
<g:HandleDataExtTag funName="delData" url="${ctx}/dic/currency/delDicCurrencyType.action" />
<%@ include file="/WEB-INF/page/include/toolbarNoButton.jsp"%>
<body>
<input type="hidden" id = "ctx" value="${ctx }">
<input type="hidden" id="id" value="" />

</body>
<script language = "javascript" >
var gridGrid; 

Ext.onReady(function(){
 Ext.QuickTips.init();
Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name:'id',type: 'string'},{name:'name',type: 'string'},{name:'note',type: 'string'}],
proxy: new Ext.data.HttpProxy({url: ctx+'/dic/currency/showCurrencyTypeListJson.action',method: 'POST'})
});
gridGrid = new Ext.grid.EditorGridPanel({
autoWidth:true,
height:document.body.clientHeight-30,
title:biolims.common.currency,
autoExpandColumn: 'common',
store: store,
clicksToEdit:1,
columnLines:true,
selModel: new Ext.grid.RowSelectionModel({singleSelect:false}),
trackMouseOver:true,
disableSelection:true,
loadMask: true,
columns: [new Ext.grid.RowNumberer(),{dataIndex:'id',header: 'ID',width: 32,sortable: true,hidden: true},{dataIndex:'name',header:
	biolims.user.eduName1,width: 110,sortable: true,hidden: false,editable: true,tooltip: biolims.common.editable,editor: typeName},{dataIndex:'note',header: 
		biolims.common.note,width: 80,sortable: true,hidden: false,editable: true,tooltip: biolims.common.editable,editor: note}],
stripeRows: true,
viewConfig: {
forceFit:true,
enableRowBody:true
},
tbar: [
{text:biolims.common.fillDetail,
iconCls:'add',
handler : function(){
var Ob = gridGrid.getStore().recordType;
var p = new Ob({
});
gridGrid.stopEditing();
store.insert(0, p);
gridGrid.startEditing(0, 0);
}
}
,'-',
{id:'saveAllModify',text:biolims.common.save, handler : function(){
var record = gridGrid.store.getModifiedRecords();  if(!record || record==''||record.length==0){return true;} else {	var selData='' ;for ( var ij=0;ij<record.length;ij++){ if(validateFun(record[ij])==false){return false;}if(record[ij].get('id')!='-1000'){ var array = record[ij].data;selData = selData+Ext.util.JSON.encode(array)+',';}}if(selData.length>0){selData = selData.substring(0,selData.length-1);}
var data = '['+selData+']';
data = rpLsp(data);
var params = "data:'"+data+"'";
modifyData(params);}gridGrid.store.commitChanges();return true;}},'-',
{text:  biolims.common.uncheck, handler : function(){
var record = gridGrid.getSelectionModel().clearSelections();  
}},'-',
{text:biolims.common.delSelected,
      handler: function(){
gridGrid.stopEditing();
var record = gridGrid.getSelectionModel().getSelections();  if(!record || record==''||record.length==0){Ext.MessageBox.alert(biolims.common.prompt,biolims.common.pleaseSelectRecord, '');} else {
   Ext.MessageBox.show( {title :biolims.common.prompt,msg :biolims.common.confirm2Del+record.length+biolims.common.record,buttons : Ext.MessageBox.OKCANCEL,
closable : false,
	fn : function(btn) {
	if (btn == 'ok') {
 for ( var ij=0;ij<record.length;ij++){ 
var id =  record[ij].get('id');if(id==undefined){record[ij].set('id','-1000');store.remove(record[ij]);	}else{  store.remove(record[ij]);		var params = "id:'"+id+"'"; delData(params);}}}}})}
 }
},
{id:'showEditColumn',hidden:true,text: biolims.common.editableColAppear, handler : function(){
var i=0;	for (i=0;i<gridGrid.getColumnModel().getColumnCount();i++)	{		if(gridGrid.getColumnModel().getColumnTooltip(i)==biolims.common.editable ){		gridGrid.getView().getHeaderCell(i).style.backgroundColor=RECORD_INSERT_COLOR;		}	} 
}}
],
bbar: new Ext.PagingToolbar({
pageSize: parseInt((document.body.clientHeight-70)>25?(document.body.clientHeight-70)/25:0),
store: store,
displayInfo: true,
displayMsg:biolims.common.displayMsg,
beforePageText: biolims.common.page,
afterPageText: biolims.common.afterPageText,
emptyMsg: biolims.common.noData,
plugins : new Ext.ui.plugins.ComboPageSize({ addToItem: false, prefixText:biolims.common.show, postfixText: biolims.common.entris})
})
});
gridGrid.render('grid');

store.load({params:{start:0, limit:parseInt((document.body.clientHeight-70)>25?(document.body.clientHeight-70)/25:0)}}); 
setTimeout(" Ext.getCmp('showEditColumn').handler()",1000); 	  
});
</script >
<div id='grid' style='width: 100%;' ></div>

</html>