<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/page/include/taglib.jsp"%>
<%@ taglib prefix="g" uri="http://www.biolims.com/taglibs/grid"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bio-LIMS</title>
<script type="text/javascript" src="${ctx}/javascript/handleSearchForm.js"></script>
</head>

<body>
			
			
<input type="hidden" value="${flag}" id="dicTypeId">			

<script language = "javascript" >
function gridSetSelectionValue(){
var r = dicTypeDialogGridGrid.getSelectionModel().getSelected(); 
setvalue(r.data.id,r.data.name);
}
var dicTypeDialogGridGrid;
Ext.onReady(function(){
var store = new Ext.data.JsonStore({
root: 'results',
totalProperty: 'total',
remoteSort: true,
fields: [{name: 'id'},{name: 'name'},{name: 'note'},{name: 'sysCode'}],
proxy: new Ext.data.HttpProxy({url: ctx+'/dic/type/dicTypeSelectJson.action?flag='+$("#dicTypeId").val(),method: 'POST'})
});
	Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {	width : 30,	renderer:function(value, cellmeta, record, rowIndex, columnIndex, store){	return store.lastOptions.params.start + rowIndex + 1;}});
dicTypeDialogGridGrid = new Ext.grid.GridPanel({
autoWidth:true,
autoExpandColumn: 'note',
height:document.body.clientHeight-15,
title:'',
store: store,
columnLines:true,
trackMouseOver:true,
loadMask: true,
columns:[new Ext.grid.RowNumberer(),{header:'id',sortable: true,hidden:true,dataIndex: 'id',width: 100},{header:'名称',sortable: true,dataIndex: 'name',width: 200},{id:'note',header:'说明',sortable: true,dataIndex: 'note',width: 240},{id:'sysCode',header:'系统码',sortable: true,hidden:true,dataIndex: 'sysCode',width: 100}],
stripeRows: true,
bbar: new Ext.PagingToolbar({
pageSize: parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1),
store: store,
displayInfo: true,
displayMsg: '当前显示 {0} - {1} 行 共 {2} 行',
beforePageText: '页码：',
afterPageText: '页 共 {0} 页',
emptyMsg: '没有可以显示的数据 '
})
});
dicTypeDialogGridGrid.render('dicTypeDialogGridDiv');
dicTypeDialogGridGrid.getSelectionModel().on('rowselect',function(sm,rowIdx,r){
});
dicTypeDialogGridGrid.on('rowdblclick',function(){gridSetSelectionValue();
});
store.on('beforeload', function () { store.baseParams = {data: document.getElementById('extJsonDataString').value};  });
store.load({params:{start:0, limit:parseInt((document.body.clientHeight)>0?(document.body.clientHeight)/25:1)}});
});
</script >
<input type='hidden'  id='extJsonDataString'  name='extJsonDataString' value=''/>
<div id='dicTypeDialogGridDiv' ></div>

</body>
</html>