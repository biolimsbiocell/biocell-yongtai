var sampleFeedbackDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'productid',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	    fields.push({
		name:'abnomalType',
		type:"string"
	});
	    fields.push({
		name:'isgood',
		type:"string"
	});
	    fields.push({
		name:'nextflow',
		type:"string"
	});
	    fields.push({
		name:'advice',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'样本编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'productid',
		header:'检测项目',
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		header:'样本类型',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'abnomalType',
		header:'异常类型',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'isgood',
		header:'是否合格',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'nextflow',
		header:'下一步流向',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'advice',
		header:'处理意见',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showSampleFeedbackListJson.action";
	var opts={};
	opts.title="样本异常反馈";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleFeedbackFun(rec);
	};
	sampleFeedbackDialogGrid=gridTable("show_dialog_sampleFeedback_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(sampleFeedbackDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
