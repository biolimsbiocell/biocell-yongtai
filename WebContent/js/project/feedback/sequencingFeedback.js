var sequencingFeedbackGrid;
$(function(){
	var cols={};
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'fcCode',
		type:"string"
	});
	   fields.push({
		name:'laneCode',
		type:"string"
	});
	   fields.push({
		name:'machineCode',
		type:"string"
	});
	   fields.push({
		name:'molarity',
		type:"string"
	});
	   fields.push({
		name:'quantity',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'computerDate',
		type:"string"
	});
	   fields.push({
		name:'pfPercent',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date"
	});
	    fields.push({
		name:'idCard',
		type:"string"
	});
	    fields.push({
		name:'sequenceFun',
		type:"string"
	});
	    fields.push({
		name:'inspectDate',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'sequencing-id',
		type:"string"
	});
	    fields.push({
		name:'sequencing-name',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:'POOLING编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'样本描述',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'fcCode',
		hidden : false,
		header:'FC号',
		width:20*6
	});
	cm.push({
		dataIndex:'laneCode',
		hidden : false,
		header:'lane号',
		width:20*6,
	});
	cm.push({
		dataIndex:'machineCode',
		hidden : false,
		header:'机器号',
		width:20*6,
	});
	cm.push({
		dataIndex:'molarity',
		hidden : false,
		header:'摩尔浓度',
		width:20*6,
	});
	cm.push({
		dataIndex:'quantity',
		hidden : true,
		header:'上机定量',
		width:20*6,
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'总密度',
		width:20*6,
	});
	cm.push({
		dataIndex:'computerDate',
		hidden : false,
		header:'上机日期',
		width:20*6
		
	});
	cm.push({
		dataIndex:'pfPercent',
		hidden : false,
		header:'PF%',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:'单位',
		width:20*6,
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '不合格'
			},{
				id : '1',
				name : '合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),
//		editor: result
	});
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '文库构建' ], [ '1', '重测序' ],[ '2', '入库' ],[ '3', '反馈至项目组' ],[ '4', '终止' ] ]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:'下一步流向',
		width:20*6,
//		editor : nextFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '重测序' ], [ '1', '退费' ] ,[ '2', '合格' ],[ '3', '待反馈']]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:'处理意见',
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	cm.push({
		dataIndex:'reportDate',
		header:'应出报告日期',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:'身份证号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样日期',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'acceptDate',
		header:'接收日期',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:'手机号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:'患者姓名',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		hidden:true,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	///project/feedback/sequencingFeedback
	loadParam.url=ctx+"/project/feedback/sequencingFeedback/showSequencingFeedbackListJson.action";
	var opts={};
	opts.title="上机异常反馈";
	opts.height=document.body.clientHeight-34;
	opts.tbar=[];
	opts.tbar.push({
			text : "保存",
			handler : function() {
				var result = commonGetModifyRecords(sequencingFeedbackGrid);
				if (result.length > 0) {
					ajax("post", "/project/feedback/sequencingFeedback/saveSequencingFeedback.action", {
						itemDataJson : result,
					}, function(data) {
						if (data.success) {
							message("保存成功！");
							sequencingFeedbackGrid.getStore().commitChanges();
							sequencingFeedbackGrid.getStore().reload();
						} else {
							message("保存失败！");
						}
					}, null);
				}
			}
		});
	
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	sequencingFeedbackGrid=gridEditTable("show_sequencingFeedback_div",cols,loadParam,opts);
	$("#show_sequencingFeedback_div").data("sequencingFeedbackGrid", sequencingFeedbackGrid);
});
function add(){
		window.location=window.ctx+'/project/feedback/sequencingFeedback/editSequencingFeedback.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/project/feedback/sequencingFeedback/editSequencingFeedback.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/project/feedback/sequencingFeedback/viewSequencingFeedback.action?id=' + id;
}
function exportexcel() {
	sequencingFeedbackGrid.title = '导出列表';
	var vExportContent = sequencingFeedbackGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				
				commonSearchAction(sequencingFeedbackGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
function selectSequencingInfo(){
	commonSearchActionByMo(sequencingFeedbackGrid,"6");
	$("#sequencingFeedback_poolingCode").val("");
	$("#sequencingFeedback_fcCode").val("");
	$("#sequencingFeedback_method").val("");
}