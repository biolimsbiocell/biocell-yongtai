﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTechnology/editSampleInputTechnology.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/project/feedback/sampleFeedback/showSampleFeedbackCommonList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var sampleType = $("#sampleInputTechnology_sampleType_name").val();
	if(sampleType==""){
		message("请选择样本类型！");
		return;
	}
	var isQualified = $("#sampleInputTechnology_isQualified").val();
	if(isQualified==""){
		message("请选择是否合格！");
		return;
	}
	
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleInputTechnology", {
					userId : userId,
					userName : userName,
					formId : $("#sampleInputTechnology_id").val(),
					title : $("#sampleInputTechnology_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleInputTechnology_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/project/feedback/sampleFeedback/save.action?code="+$("#code").val();
		form1.submit();
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInputTechnology/copySampleInputTechnology.action?id=' + $("#sampleInputTechnology_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#sampleInputTechnology_id").val() + "&tableId=sampleInputTechnology");
}





//function checkSubmit() {
//	var mess = "";
//	var fs = new Array();
//	var nsc = new Array();
//	fs.push($("#sampleInputTechnology_id").val());
//	nsc.push("编号不能为空！");
//	mess = commonFieldsNotNullVerify(fs, nsc);
//	if (mess != "") {
//			message(mess);
//			return false;
//		}
//		return true;
//	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'科技服务异常项目管理',
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
//选择样本类型
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : '选择样本类型',
				layout : 'fit',
				width : 800,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	$("#sampleInputTechnology_sampleType_sys").val();
	document.getElementById("sampleInputTechnology_sampleType").value = id;
	document.getElementById("sampleInputTechnology_sampleType_name").value = name;
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}