var wkFeedbackGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
			name:'batch',
			type:"string"
		});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextflow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
   fields.push({
		name:'phone',
		type:"string"
	});
   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'isRun',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   
	   fields.push({
		name:'techTaskId',
		type:"string"
	});
    fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'wkCode',
		header:'文库编号',
		hidden:true,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6,
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : false,
		header:'身份证',
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	cm.push({
		dataIndex:'batch',
		hidden : true,
		header:'批次号',
		width:6*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : false,
		header:'检测方法',
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'unit',
		hidden : false,
		header:'单位',
		width:20*6,
		
	});
//	var result = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '1',
//				name : '合格'
//			}, {
//				id : '0',
//				name : '不合格'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'处理结果',
//		width:15*6,
//		renderer: Ext.util.Format.comboRenderer(result),
////		editor: result
//	});
//	var next = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '0',
//				name : '文库质控'
//			}, {
//				id : '1',
//				name : '2100 or Caliper'
//			}, {
//				id : '6',
//				name : 'QPCR质控'
//			}, {
//				id : '2',
//				name : '终止'
//			}, {
//				id : '3',
//				name : '入库'
//			}, {
//				id : '4',
//				name : '异常反馈至项目管理'
//			}, {
//				id : '5',
//				name : '暂停'
//			}, {
//				id : '7',
//				name : '重建库'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'nextflow',
//		hidden : false,
//		header:'下一步流向',
//		width:25*6,
//		renderer: Ext.util.Format.comboRenderer(next),
////		editor: next
//	});
//	var storemethodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '文库质控' ],[ '1', '2100 or Caliper' ],[ '6', 'QPCR质控' ],[ '2', '重建库' ],['3','终止'],['4','入库'],['5','暂停']]
//	});
//	var methodCob = new Ext.form.ComboBox({
//		store : storemethodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'method',
		header:'处理意见',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : '是'
			}, {
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(submit),
		editor: submit
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:40*6,
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

//	cm.push({
//		dataIndex:'wk-id',
//		hidden : true,
//		header:'相关主表ID',
//		width:20*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'wk-name',
//		hidden : true,
//		header:'相关主表',
//		width:20*10
//	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : true,
		header:'科技服务任务单',
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6,
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	var type="1";
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showWkFeedbackListJson.action?type="+type;
	var opts={};
	opts.title="文库异常反馈";
	opts.height=document.body.clientHeight-34;
	
	opts.tbar=[];
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : search
//	});
//	opts.tbar.push({
//		text : "批量处理",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_wkMethod_div"), "批量处理", null, {
//				"确定" : function() {
//					var records = wkFeedbackGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var method = $("#wkMethod").val();
//						wkFeedbackGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("method", method);
//						});
//						wkFeedbackGrid.startEditing(0, 0);
//					}else{
//						message("请先选择数据！");
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : "批量提交",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_wkSubmit_div"), "批量提交", null, {
				"确定" : function() {
					var records = wkFeedbackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var submit = $("#wkSubmit").val();
						wkFeedbackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("submit", submit);
						});
						wkFeedbackGrid.startEditing(0, 0);
					}else{
						message("请先选择数据！");
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'save',
			text : "保存",
			handler : function() {
				var result = commonGetModifyRecords(wkFeedbackGrid);
				if (result.length > 0) {
					ajax("post", "/project/feedback/sampleFeedback/saveWkFeedback.action", {
						itemDataJson : result,
					}, function(data) {
						if (data.success) {
							message("保存成功！");
							wkFeedbackGrid.getStore().commitChanges();
							wkFeedbackGrid.getStore().reload();
						} else {
							message("保存失败！");
						}
					}, null);
				}
			}
		});
	
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	wkFeedbackGrid=gridEditTable("show_wkFeedback_div",cols,loadParam,opts);
	$("#show_wkFeedback_div").data("wkFeedbackGrid", wkFeedbackGrid);
});
function selectWkInfo(){
	commonSearchActionByMo(wkFeedbackGrid,"2");
	$("#wkFeedback_wkCode").val("");
	$("#wkFeedback_sampleCode").val("");
	$("#wkFeedback_code").val("");
}
//function add(){
//		window.location=window.ctx+'/project/feedback/wkFeedback/editWkFeedback.action';
//	}
//function edit(){
//	var id="";
//	id=document.getElementById("selectId").value;
//	if (id==""||id==undefined){
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location=window.ctx+'/project/feedback/wkFeedback/editWkFeedback.action?id=' + id;
//}
//function view() {
//	var id = "";
//	id = document.getElementById("selectId").value;
//	if (id == "" || id == undefined) {
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location = window.ctx + '/project/feedback/wkFeedback/viewWkFeedback.action?id=' + id;
//}
//function exportexcel() {
//	wkFeedbackGrid.title = '导出列表';
//	var vExportContent = wkFeedbackGrid.getExcelXml();
//	var x = document.getElementById('gridhtm');
//	x.value = vExportContent;
//	document.excelfrm.submit();
//}
//$(function() {
//	$("#opensearch").click(function() {
//		var option = {};
//		option.width = 542;
//		option.height = 417;
//		loadDialogPage($("#jstj"), "搜索", null, {
//			"开始检索" : function() {
//			
//				
//				commonSearchAction(wkFeedbackGrid);
//				$(this).dialog("close");
//
//			},
//			"清空" : function() {
//				form_reset();
//
//			}
//		}, true, option);
//	});
//});
