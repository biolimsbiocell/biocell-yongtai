var qpcrFeedbackGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
    	name:'sampleCode',
    	type:"string"
    });
	    fields.push({
		name:'wkCode',
		type:"string"
	});
	    fields.push({
			name:'batch',
			type:"string"
		});
	    fields.push({
		name:'indexa',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'wkType',
		type:"string"
	});
//	    fields.push({
//		name:'length',
//		type:"string"
//	});
//	    fields.push({
//		name:'qualityConcentrer',
//		type:"string"
//	});
//	    fields.push({
//		name:'reason',
//		type:"string"
//	});
	    fields.push({
		name:'nextFlow',
		type:"string"
	});
	    fields.push({
		name:'result',
		type:"string"
	});
	    fields.push({
		name:'method',
		type:"string"
	});

	    fields.push({
	    name:'note',
	    type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'submit',
		type:"string"
	});
    fields.push({
		name:'isRun',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
//	    fields.push({
//		name:'wkQpcrSampleTask-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'wkQpcrSampleTask-name',
//		type:"string"
//	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:'样本编号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'原始样本编号',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'wkCode',
		header:'文库编号',
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		header:'样本类型',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	cm.push({
		dataIndex:'batch',
		hidden : true,
		header:'批次号',
		width:6*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:30*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:30*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:30*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:30*6,
	});	
	cm.push({
		dataIndex:'indexa',
		header:'INDEX',
		width:20*6,
		
		sortable:true
	});
//	cm.push({
//		dataIndex:'name',
//		header:'描述',
//		width:20*6,
//		sortable:true
//	});
	var wkTypeCobs = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '2', '2100 or Caliper' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCobs,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
//	cm.push({
//		dataIndex:'length',
//		header:'片段长度',
//		width:20*6,
//		
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'qualityConcentrer',
//		header:'浓度',
//		width:20*6,
//		
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'reason',
//		header:'异常原因',
//		width:20*6,
//		
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'qualityConcentrer',
//		header:'浓度',
//		width:20*6,
//		
//		sortable:true
//	});
//	var storenextFlowCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', 'Pooling' ], [ '1', '重抽血' ],[ '2', '重质检' ],['3','终止'] ,['4','异常反馈至项目管理'],['5','入库']]
//	});
//	var nextFlowCob = new Ext.form.ComboBox({
//		store : storenextFlowCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:25*6,
////		editor : nextFlowCob,
//		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
//	});
//	var storeresultCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '不合格' ], [ '1', '合格' ] ]
//	});
//	var resultCob = new Ext.form.ComboBox({
//		store : storeresultCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'result',
//		header:'处理结果',
//		hidden : false,
//		width:20*6,
//		sortable:true,
////		editor : resultCob,
//		renderer : Ext.util.Format.comboRenderer(resultCob)
//	});
//	var storemethodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', 'Pooling' ], [ '1', '重抽血' ],[ '2', '重质检' ],['3','终止'] ,['4','入库']]
//	});
//	var methodCob = new Ext.form.ComboBox({
//		store : storemethodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理意见',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		header:'是否提交',
		hidden : false,
		width:20*6,
		sortable:true,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*6,
		hidden:false,
		sortable:true
	});

	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
//	cm.push({
//		dataIndex:'wkQpcrSampleTask-id',
//		hidden : true,
//		header:'相关主表ID',
//		width:20*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'wkQpcrSampleTask-name',
//		hidden : true,
//		header:'相关主表',
//		width:20*10
//	});
//	
	cols.cm=cm;
	var loadParam={};
	var type="1";
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showQpcrFeedbackListJson.action?type="+type;
	var opts={};
	opts.title="QPCR质控异常反馈";
	opts.height=document.body.clientHeight;
	
	opts.tbar=[];
//	opts.tbar.push({
//		text : "批量处理",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_qcmethod_div"), "批量处理", null, {
//				"确定" : function() {
//					var records = qpcrFeedbackGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var method = $("#qcmethod").val();
//						qpcrFeedbackGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("method", method);
//						});
//						qpcrFeedbackGrid.startEditing(0, 0);
//					}else{
//						message("请先选择数据！");
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : "批量提交",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_qcsubmit_div"), "批量下一步", null, {
				"确定" : function() {
					var records = qpcrFeedbackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var submit = $("#qcsubmit").val();
						qpcrFeedbackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("submit", submit);
						});
						qpcrFeedbackGrid.startEditing(0, 0);
					}else{
						message("请先选择数据！");
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'save',
			text : "保存",
			handler : function() {
				var result = commonGetModifyRecords(qpcrFeedbackGrid);
				if (result.length > 0) {
					ajax("post", "/project/feedback/qualityFeedback/saveQpcrFeedback.action", {
						itemDataJson : result,
					}, function(data) {
						if (data.success) {
							message("保存成功！");
							qpcrFeedbackGrid.getStore().commitChanges();
							qpcrFeedbackGrid.getStore().reload();
						} else {
							message("保存失败！");
						}
					}, null);
				}
			}
		});
	
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	qpcrFeedbackGrid=gridEditTable("show_qpcrFeedback_div",cols,loadParam,opts);
	$("#show_qpcrFeedback_div").data("qpcrFeedbackGrid", qpcrFeedbackGrid);
});
//function add(){
//		window.location=window.ctx+'/project/feedback/qualityFeedback/editQualityFeedback.action';
//	}
//function edit(){
//	var id="";
//	id=document.getElementById("selectId").value;
//	if (id==""||id==undefined){
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location=window.ctx+'/project/feedback/qualityFeedback/editQualityFeedback.action?id=' + id;
//}
//function view() {
//	var id = "";
//	id = document.getElementById("selectId").value;
//	if (id == "" || id == undefined) {
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location = window.ctx + '/project/feedback/qualityFeedback/viewQualityFeedback.action?id=' + id;
//}
function exportexcel() {
	qpcrFeedbackGrid.title = '导出列表';
	var vExportContent = qpcrFeedbackGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function selectQcInfo(){
	commonSearchActionByMo(qpcrFeedbackGrid,"5");
	$("#qpcrFeedback_wkCode").val("");
	$("#qpcrFeedback_sampleCode").val("");
	$("#qpcrFeedback_code").val("");
}
//$(function() {
//	$("#opensearch").click(function() {
//		var option = {};
//		option.width = 542;
//		option.height = 417;
//		loadDialogPage($("#jstj"), "搜索", null, {
//			"开始检索" : function() {
//			
//				
//				commonSearchAction(qpcrFeedbackGrid);
//				$(this).dialog("close");
//
//			},
//			"清空" : function() {
//				form_reset();
//
//			}
//		}, true, option);
//	});
//});
