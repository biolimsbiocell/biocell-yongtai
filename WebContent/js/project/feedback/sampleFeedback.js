var sampleFeedbackGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
			name:'sampleCode',
			type:"string"
		});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
			name:'productName',
			type:"string"
		});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	    fields.push({
		name:'abnomalType',
		type:"string"
	});
	    fields.push({
		name:'isgood',
		type:"string"
	});
	    fields.push({
		name:'nextflow',
		type:"string"
	});
	    fields.push({
		name:'advice',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'submit',
		type:"string"
	});
	    fields.push({
		name:'sampleNum',
		type:"string"
	});
	    fields.push({
		    name:'method',
		    type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'样本编号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:'病人名称',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'abnomalType',
		header:'异常类型',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		header:'样本类型',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleNum',
		header:'样本数量',
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'isgood',
		header:'是否合格',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'nextflow',
		header:'下一步流向',
		width:20*6,
		hidden :true,
		sortable:true
	});
	cm.push({
		dataIndex:'advice',
		header:'处理意见',
		width:20*6,
		
		sortable:true
	});
	var storeconfirmCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var confirmCob = new Ext.form.ComboBox({
		store : storeconfirmCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		editor : confirmCob,
		renderer : Ext.util.Format.comboRenderer(confirmCob)
	});
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateId',
		header:'工作流状态ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
   
//    var storemethodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '重抽血' ], [ '1', '退费' ] ,[ '2', '合格' ],[ '3', '待反馈']]
//	});
//	var methodCob = new Ext.form.ComboBox({
//		store : storemethodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	var storeresultCob1 = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ],[ '0', '不合格' ]]
	});
	var resultCob1 = new Ext.form.ComboBox({
		store : storeresultCob1,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:'处理结果',
		width:20*6,
		sortable:true,
		editor : resultCob1,
		renderer : Ext.util.Format.comboRenderer(resultCob1)
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showSampleFeedbackListJson.action";
	var opts={};
	opts.title="样本异常反馈";
	opts.height=document.body.clientHeight-34;
	opts.tbar = [];
	opts.tbar.push({
		text : "批量提交",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
				"确定" : function() {
					var records = sampleFeedbackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var submit = $("#submit").val();
						sampleFeedbackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("submit", submit);
						});
						sampleFeedbackGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'save',
		text : "保存",
		handler : function() {
			var result = commonGetModifyRecords(sampleFeedbackGrid);
			if (result.length > 0) {
				ajax("post", "/project/feedback/sampleFeedback/saveSampleFeedback.action", {
					itemDataJson : result,
				}, function(data) {
					if (data.success) {
						message("保存成功！");
						sampleFeedbackGrid.getStore().commitChanges();
						sampleFeedbackGrid.getStore().reload();
					} else {
						message("保存失败！");
					}
				}, null);
			}else{
				message("没有需要保存的数据！");
			}
		}
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		editsample();
	};
	sampleFeedbackGrid=gridTable("show_sampleFeedback_div",cols,loadParam,opts);
	
});


//function add(){
//		window.location=window.ctx+'/project/feedback/sampleFeedback/editSampleFeedback.action';
//	}
function editsample(){
	var id="";
	id=document.getElementById("selectId").value;
	window.location=window.ctx+'/project/feedback/sampleFeedback/editSampleFeedback.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/project/feedback/sampleFeedback/viewSampleFeedback.action?id=' + id;
}
function exportexcel() {
	sampleFeedbackGrid.title = '导出列表';
	var vExportContent = sampleFeedbackGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function selectSampleFeedback(){
	commonSearchActionByMo(sampleFeedbackGrid,"33");
	$("#sampleFeedback_sampleCode").val("");
//	$("#dnaFeedback_sampleCode").val("");
//	$("#dnaFeedback_method").val("");
}

//$(function() {
//	$("#opensearch").click(function() {
//		var option = {};
//		option.width = 542;
//		option.height = 417;
//		loadDialogPage($("#jstj"), "搜索", null, {
//			"开始检索" : function() {
//			
//				
//				commonSearchAction(sampleFeedbackGrid);
//				$(this).dialog("close");
//
//			},
//			"清空" : function() {
//				form_reset();
//
//			}
//		}, true, option);
//	});
//});
