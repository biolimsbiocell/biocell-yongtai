var dnaFeedbackGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'batch',
			type:"string"
		});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
			name:'sampleName',
			type:"string"
		});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'od230',
		type:"string"
	});
	   fields.push({
		name:'od280',
		type:"string"
	});
	   fields.push({
		name:'concentrer',
		type:"string"
	});
	    fields.push({
		name:'result',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
			name:'productId',
			type:"string"
		});
	   fields.push({
			name:'productName',
			type:"string"
		});
	   fields.push({
			name:'sequenceFun',
			type:"string"
		});
	   fields.push({
			name:'inspectDate',
			type:"string"
		});
	   fields.push({
			name:'idCard',
			type:"string"
		});
	   fields.push({
			name:'reportDate',
			type:"string"
		});
	   fields.push({
			name:'phone',
			type:"string"
		});
	   fields.push({
			name:'orderId',
			type:"string"
		});
	   fields.push({
			name:'isExecute',
			type:"string"
		});
	   fields.push({
			name:'nextflow',
			type:"string"
		});

	   fields.push({
	   		name:'state',
	   		type:"string"
	   	});
	   
	   fields.push({
			name:'stateName',
			type:"string"
		});
	   fields.push({
			name:'method',
			type:"string"
		});
	   fields.push({
	   		name:'classify',
	   		type:"string"
	   	});
	   fields.push({
	   		name:'dnaCode',
	   		type:"string"
	   	});
	   fields.push({
	   		name:'sampleType',
	   		type:"string"
	   	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'dnaCode',
		hidden : false,
		header:'DNA编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleName',
		hidden : false,
		header:'样本名称',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6
	});
	cm.push({
		dataIndex:'od230',
		hidden : false,
		header:'od260/230',
		width:20*6
	});
	cm.push({
		dataIndex:'od280',
		hidden : false,
		header:'od260/280',
		width:20*6
	});
	cm.push({
		dataIndex:'concentrer',
		hidden : false,
		header:'浓度',
		width:20*6
	});
	
//	var storeresultCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
//	});
//	var resultCob = new Ext.form.ComboBox({
//		store : storeresultCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*6,
		//renderer : Ext.util.Format.comboRenderer(resultCob)
	});
//	var storenextFlowCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '文库构建' ], [ '1', '重新提取' ],[ '2', '入库' ],[ '3', '反馈至项目组' ],[ '4', '终止' ] ]
//	});
//	var nextFlowCob = new Ext.form.ComboBox({
//		store : storenextFlowCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'nextflow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
//		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
//	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'病人姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : true,
		header:'检测项目',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'batch',
		hidden : true,
		header:'批次号',
		width:6*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:15*10
	});
	cm.push({
		dataIndex:'method',
		header:'处理意见',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var storeisExecuteCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '是' ], [ '0', '否' ] ]
//	});
//	var isExecuteCob = new Ext.form.ComboBox({
//		store : storeisExecuteCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'isExecute',
//		hidden : false,
//		header:'确认执行',
//		width:20*6,
//		hidden : false,
//		editor : isExecuteCob,
//		renderer : Ext.util.Format.comboRenderer(isExecuteCob)
//	});
	cm.push({
		dataIndex : 'inspectDate',
		header : '取样时间',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});

	cm.push({
		dataIndex : 'idCard',
		header : '身份证号',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'reportDate',
		header : '应出报告日期',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'phone',
		header : '手机号',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderId',
		header : '任务单',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	var storeconfirmCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var confirmCob = new Ext.form.ComboBox({
		store : storeconfirmCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:'确认执行',
		width:20*6,
		editor : confirmCob,
		renderer : Ext.util.Format.comboRenderer(confirmCob)
	});
	cm.push({
		dataIndex : 'state',
		header : '状态',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '状态',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务 ',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showDnaFeedbackListJson.action";
	var opts={};
	opts.title="核酸提取异常反馈";
	opts.height=document.body.clientHeight-34;
	opts.tbar=[];
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : selectDnaInfo
//	});
	opts.tbar.push({
		text : "批量执行",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_isExecute_div"), "批量执行", null, {
				"确定" : function() {
					var records = dnaFeedbackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute").val();
						dnaFeedbackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", isExecute);
						});
						dnaFeedbackGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
			text : "保存",
			handler : function() {
				var result = commonGetModifyRecords(dnaFeedbackGrid);
				if (result.length > 0) {
					ajax("post", "/project/feedback/sampleFeedback/saveDnaFeedback.action", {
						itemDataJson : result,
					}, function(data) {
						if (data.success) {
							message("保存成功！");
							dnaFeedbackGrid.getStore().commitChanges();
							dnaFeedbackGrid.getStore().reload();
						} else {
							message("保存失败！");
						}
					}, null);
				}
			}
		});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	dnaFeedbackGrid=gridEditTable("show_dnaFeedback_div",cols,loadParam,opts);
	$("#show_dnaFeedback_div").data("dnaFeedbackGrid", dnaFeedbackGrid);
	//$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	//$(".x-panel-tbar").css("width", "100%");
});
//function exportexcel() {
//	dnaFeedbackGrid.title = '导出列表';
//	var vExportContent = dnaFeedbackGrid.getExcelXml();
//	var x = document.getElementById('gridhtm');
//	x.value = vExportContent;
//	document.excelfrm.submit();
//}
function selectDnaInfo(){
	commonSearchActionByMo(dnaFeedbackGrid,"1");
	$("#dnaFeedback_code").val("");
	$("#dnaFeedback_sampleCode").val("");
	$("#dnaFeedback_dnaCode").val("");
}
