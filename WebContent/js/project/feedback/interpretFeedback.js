var interpretFeedbackGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
	    name:'result1',
	    type:"string"
	});
	    fields.push({
		    name:'result2',
		    type:"string"
		});

	    fields.push({
		    name:'CNV',
		    type:"string"
		});

	    fields.push({
		    name:'interpretedResult',
		    type:"string"
		});

	    fields.push({
		    name:'note',
		    type:"string"
		});


	    fields.push({
		name:'isgood',
		type:"string"
	});
	    fields.push({
		name:'nextflow',
		type:"string"
	});
	    fields.push({
		name:'advice',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});

	    fields.push({
	    name:'method',
	    type:"string"
	});

	    fields.push({
		name:'mainID',
		type:"string"
	});

	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'文库编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:10*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'样本编号',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'result1',
		header:'Result1',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'result2',
		header:'Result2',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'CNV',
		header:'CNV',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'interpretedResult',
		header:'解读结果',
		width:20*6,
		
		sortable:true
	});
	
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '合格' ], [ '1', '不合格' ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isgood',
		header:'是否合格',
		hidden : false,
		width:20*6,
		sortable:true,
//		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	

	
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '1', 'QPCR质控' ],[ '2', '返库' ],['3','入库'] ,['4','异常反馈至项目管理']]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextflow',
		hidden : false,
		header:'下一步流向',
		width:20*6,
//		editor : nextFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	
	cm.push({
		dataIndex:'advice',
		hidden : false,
		header:'处理意见',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'mainID',
		header:'相关主表ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '重抽血' ], [ '1', '退费' ] ,[ '2', '合格' ],[ '3', '待反馈']]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:'处理结果',
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*6,
		
		sortable:true
	});

	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showInterpretFeedbackListJson.action";
	var opts={};
	opts.title="解读异常反馈";
	opts.height=document.body.clientHeight-34;
	
	opts.tbar=[];
	opts.tbar.push({
			text : "保存",
			handler : function() {
				var result = commonGetModifyRecords(interpretFeedbackGrid);
				if (result.length > 0) {
					ajax("post", "/project/feedback/sampleFeedback/saveInterpretFeedback.action", {
						itemDataJson : result,
					}, function(data) {
						if (data.success) {
							message("保存成功！");
							interpretFeedbackGrid.getStore().commitChanges();
							interpretFeedbackGrid.getStore().reload();
						} else {
							message("保存失败！");
						}
					}, null);
				}
			}
		});
	
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	interpretFeedbackGrid=gridEditTable("show_interpretFeedback_div",cols,loadParam,opts);
	$("#show_interpretFeedback_div").data("interpretFeedbackGrid", interpretFeedbackGrid);
});
//function add(){
//		window.location=window.ctx+'/project/feedback/interpretFeedback/editInterpretFeedback.action';
//	}
//function edit(){
//	var id="";
//	id=document.getElementById("selectId").value;
//	if (id==""||id==undefined){
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location=window.ctx+'/project/feedback/interpretFeedback/editInterpretFeedback.action?id=' + id;
//}
//function view() {
//	var id = "";
//	id = document.getElementById("selectId").value;
//	if (id == "" || id == undefined) {
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location = window.ctx + '/project/feedback/interpretFeedback/viewInterpretFeedback.action?id=' + id;
//}
function exportexcel() {
	interpretFeedbackGrid.title = '导出列表';
	var vExportContent = interpretFeedbackGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				
				commonSearchAction(interpretFeedbackGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
