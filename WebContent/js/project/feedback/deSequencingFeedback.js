var deSequencingFeedbackGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
			name:'code',
			type:"string"
		});
	    fields.push({
			name:'wkNum',
			type:"string"
		});
	    fields.push({
			name:'estimate',
			type:"string"
		});
	    fields.push({
			name:'backDate',
			type:"string"
		});
	    fields.push({
			name:'errorRate',
			type:"string"
		});
//	    fields.push({
//		name:'isgood',
//		type:"string"
//	});
	    fields.push({
		name:'nextflow',
		type:"string"
	});
//	    fields.push({
//		name:'advice',
//		type:"string"
//	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});

	    fields.push({
	    name:'method',
	    type:"string"
	});
	    fields.push({
		name:'mainID',
		type:"string"
	});
	    fields.push({
		    name:'submit',
		    type:"string"
		});
	    fields.push({
		    name:'result',
		    type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'文库编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sampleCode',
		header:'样本编号',
		width:20*6,
		hidden : true,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		header:'pooling号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:10*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'wkNum',
		header:'文库号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'estimate',
		header:'测试评估',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'backDate',
		header:'反馈时间',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'errorRate',
		header:'错误率',
		width:20*6,
		
		sortable:true
	});

//	var storeisGoodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
//	});
//	var isGoodCob = new Ext.form.ComboBox({
//		store : storeisGoodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'isgood',
//		header:'结果',
//		hidden : false,
//		width:20*6,
//		sortable:true,
////		editor : isGoodCob,
//		renderer : Ext.util.Format.comboRenderer(isGoodCob)
//	});
	
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result)
	});
	
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '反馈至建库组' ], [ '1', '合格' ],['2','待反馈']]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextflow',
		hidden : false,
		header:'下一步流向',
		width:20*6,
		editor : nextFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	
//	cm.push({
//		dataIndex:'advice',
//		hidden : false,
//		header:'处理意见',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'mainID',
		header:'相关主表ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '重抽血' ], [ '1', '退费' ] ,[ '2', '合格' ],[ '3', '待反馈']]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:'处理意见',
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showDeSequencingFeedbackListJson.action";
	var opts={};
	opts.title="下机异常反馈";
	opts.height=document.body.clientHeight-34;

	
	opts.tbar=[];
	opts.tbar.push({
			text : "保存",
			handler : function() {
				var result = commonGetModifyRecords(deSequencingFeedbackGrid);
				if (result.length > 0) {
					ajax("post", "/project/feedback/sampleFeedback/saveDeSequencingFeedback.action", {
						itemDataJson : result,
					}, function(data) {
						if (data.success) {
							message("保存成功！");
							deSequencingFeedbackGrid.getStore().commitChanges();
							deSequencingFeedbackGrid.getStore().reload();
						} else {
							message("保存失败！");
						}
					}, null);
				}
			}
		});
	
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	deSequencingFeedbackGrid=gridEditTable("show_deSequencingFeedback_div",cols,loadParam,opts);
	$("#show_deSequencingFeedback_div").data("deSequencingFeedbackGrid", deSequencingFeedbackGrid);
});
//function add(){
//		window.location=window.ctx+'/project/feedback/deSequencingFeedback/editDeSequencingFeedback.action';
//	}
//function edit(){
//	var id="";
//	id=document.getElementById("selectId").value;
//	if (id==""||id==undefined){
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location=window.ctx+'/project/feedback/deSequencingFeedback/editDeSequencingFeedback.action?id=' + id;
//}
//function view() {
//	var id = "";
//	id = document.getElementById("selectId").value;
//	if (id == "" || id == undefined) {
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location = window.ctx + '/project/feedback/deSequencingFeedback/viewDeSequencingFeedback.action?id=' + id;
//}
function exportexcel() {
	deSequencingFeedbackGrid.title = '导出列表';
	var vExportContent = deSequencingFeedbackGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				
				commonSearchAction(deSequencingFeedbackGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
