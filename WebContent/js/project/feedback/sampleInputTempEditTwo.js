﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/project/feedback/sampleFeedback/showSampleFeedbackCommonList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {

	var reg = $("#sampleTemplate_address").val();
	if (reg == "") {
		message("家庭住址不能为空！");
		return;
	}
	
	//最后一次外源输血时间验证
	var outTransFusion = $("#sampleInputTemp_outTransfusion").val();
	if(outTransFusion=="1"){
		if($("#sampleInputTemp_firstTransfusionDate").val()=="" || $("#sampleInputTemp_firstTransfusionDate").val()==null){
			message("请输入最后一次外源输血时间！");
			return;
		}
	}
	//最后一次免疫治疗时间验证
	var immuneCure = $("#sampleInputTemp_immuneCure").val();
	if(immuneCure=="1"){
		if($("#sampleInputTemp_endImmuneCureDate").val()=="" || $("#sampleInputTemp_endImmuneCureDate").val()==null){
			message("请输入最后一次免疫治疗时间");
			return;
		}
	}
	
	
	var nextStepFlow =$("#sampleInputTemp_nextStepFlow").val();
	if(nextStepFlow==""){
		message("请选择下一步流向");
		return;
	}

	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleTemplate_id").val(),
		title : $("#sampleTemplate_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTemplate_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/project/feedback/sampleFeedback/save.action";
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleTemplate_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleTemplate_id").val()) {
				commonChangeState("formId=" + $("#sampleTemplate_id").val()
						+ "&tableId=SampleTemplate");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInputTemp_code").val());
	nsc.push("信息录入编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : '高通量基因测序产前筛查与诊断临床申请单异常反馈',
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : '选择类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleTemplate_sampleType_id").value = id;
	document.getElementById("sampleTemplate_sampleType_name").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 证件类型
function voucherTypeFun() {
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
	var voucherTypeFun = new Ext.Window(
			{
				id : 'voucherTypeFun',
				modal : true,
				title : '选择证件类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						voucherTypeFun.close();
					}
				} ]
			});
	voucherTypeFun.show();
}
function setzjlx(id, name) {
	document.getElementById("sampleInputTemp_voucherType").value = id;
	document.getElementById("sampleInputTemp_voucherType_name").value = name;
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
}

function checkType() {
	var re = $("#sampleTemplate_voucherType_name").val();
	if (re == "") {
		message("证件类型不能为空！");
	}
}
// 证件号码验证
function checkFun() {
	var reg = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
	if (reg.test($("#sampleTemplate_voucherCode").val())) {
		var id = $("#sampleTemplate_voucherCode").val();
		ajax("post", "/sample/sampleInput/findIdentity.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("输入的身份证号重复！");
				}
			}
		}, null);
	} else {
		message("请输入正确的证件号码!");
	}
}
// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleTemplate_phoneNum").val())) {
		// return;
		var id = $("#sampleTemplate_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("输入的手机号重复！");
				}
			}
		}, null);
	} else {
		message("请输入正确的手机号码!");
	}
}
// B超异常提醒验证
function change() {
	var reg = $('#sampleTemplate_embryoType option:selected').val();
	if (reg == "2") {
		$("#sampleTemplate_messages").css("display", "");
	}
}

// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : '选择项目',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("sampleInputTemp_productId").value = id;
					document.getElementById("sampleInputTemp_productName").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}

//选择不同的免疫治疗下拉列表如期显示最后一次免疫治疗时间不同的样式
$(function(){
	var immuneCureVar = $("#sampleInputTemp_immuneCure").val();
	var fusionCureVar = $("#sampleInputTemp_outTransfusion").val();
	//最后一次免疫治疗时间设置失效
	if(immuneCureVar=='0'){
		$("#last input").css({"background-color": "#DEDEDE"});
		document.getElementById("sampleInputTemp_endImmuneCureDate").disabled =true;
	}
	//最后一次外源输血时间设置失效
	if(fusionCureVar=='0'){
		$("#last1 input").css("background-color", "#DEDEDE");
		document.getElementById("sampleInputTemp_firstTransfusionDate").disabled =true;
	}
});

function change(){
	var immuneCureVar = $("#sampleInputTemp_immuneCure").val();
	if(immuneCureVar=='0'){
		$("#last input").css("background-color", "#DEDEDE");
		$("#last input").val('');
		document.getElementById("sampleInputTemp_endImmuneCureDate").disabled =true;
	}else if(immuneCureVar=='1'){
		$("#last input").css("background-color", "white");
		document.getElementById("sampleInputTemp_endImmuneCureDate").disabled =false;
	}
}
function change1(){
	var fusionCureVar = $("#sampleInputTemp_outTransfusion").val();
	if(fusionCureVar=='0'){
		$("#last1 input").css("background-color", "#DEDEDE");
		$("#last1 input").val('');
		document.getElementById("sampleInputTemp_firstTransfusionDate").disabled =true;
	}else if(fusionCureVar=='1'){
		$("#last1 input").css("background-color", "white");
		document.getElementById("sampleInputTemp_firstTransfusionDate").disabled =false;
	}
}

