var techCheckFeedBackGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'dnaCode',
		type:"string"
	});
	   fields.push({
		name:'od260',
		type:"string"
	});
	   fields.push({
		name:'od230',
		type:"string"
	});
	   fields.push({
		name:'rin',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	    fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'sumNum',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'isRun',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'dnaCode',
		hidden : false,
		header:'核酸编号',
		width:30*6
	});
	cm.push({
		dataIndex:'od260',
		hidden : false,
		header:'od260/230',
		width:20*6
	});
	cm.push({
		dataIndex:'od230',
		hidden : false,
		header:'od260/280',
		width:20*6

	});
	cm.push({
		dataIndex:'rin',
		hidden : false,
		header:'RIN',
		width:20*6

	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度',
		width:20*6

	});
	cm.push({
		dataIndex:'sumNum',
		hidden : false,
		header:'总量',
		width:20*6

	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : '合格'
			}, {
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果判定',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result)
		//editor: result
	});
	var nextFlow = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '反馈到项目组'
			}, {
				id : '1',
				name : '出报告'
			}, {
				id : '2',
				name : '继续'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:'下一步流向',
		width:30*6,
		renderer: Ext.util.Format.comboRenderer(nextFlow)
		//editor: nextFlow
	});
	var method = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '出报告'
			}, {
				id : '1',
				name : '继续'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理意见',
		width:30*6,
		renderer: Ext.util.Format.comboRenderer(method),
		editor: method
	});
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : '是'
			}, {
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(submit),
		editor: submit
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showTechCheckFeedBackListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title="核酸检测反馈";
	opts.height=document.body.clientHeight;
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : search
//	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});

//	opts.tbar.push({
//		text : "批量操作",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_way_div"), "批量操作", null, {
//				"确定" : function() {
//					var records = goodsMaterialsWayGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var isSend = $("#isSend").val();
//						goodsMaterialsWayGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isSend", isSend);
//						});
//						goodsMaterialsWayGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	techCheckFeedBackGrid=gridEditTable("techCheckFeedBackdiv",cols,loadParam,opts);
	$("#techCheckFeedBackdiv").data("techCheckFeedBackGrid", techCheckFeedBackGrid);
});

//保存
function save(){	
	var itemJson = commonGetModifyRecords(techCheckFeedBackGrid);
	if(itemJson.length>0){
		ajax("post", "/project/feedback/sampleFeedback/saveTechCheckFeedBack.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				techCheckFeedBackGrid.getStore().commitChanges();
				techCheckFeedBackGrid.getStore().reload();
				message("保存成功！");
			} else {
				message("保存失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
	
}

//function search() {
//		var option = {};
//		option.width = 542;
//		option.height = 417;
//		loadDialogPage($("#jstj"), "搜索", null, {
//			"开始检索" : function() {
//			
//				
//				commonSearchAction(techCheckServiceExceptionGrid);
//				$(this).dialog("close");
//
//			},
//			"清空" : function() {
//				form_reset();
//
//			}
//		}, true, option);
//}
function selectTechCheckFeedBackInfo(){
	commonSearchAction(techCheckFeedBackGrid);
	$("#techCheckFeedBack_sampleCode").val("");
	$("#techCheckFeedBack_dnaCode").val("");
	$("#techCheckFeedBack_method").val("");
}

