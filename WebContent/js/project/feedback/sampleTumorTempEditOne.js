﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
//	if($("#upload_imga_id10").val()==''){
//		$(":input").addClass(".text input readonlytrue");
//		$("[type='text']").focus(function () 
//		{
//			message("请您先上传图片后进行录入！");
//			return; 
//		}) ;
//	}
});
function add() {
	window.location = window.ctx + "/sample/sampleInput/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");

$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/project/feedback/sampleFeedback/showSampleFeedbackCommonList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {
	//sampleInfo 的图片Id
//	var sampleInfoImg = $("#upload_imga_id10").val();
	//sampleInputTemp 的图片Id
//	var sampleBreastCancerTempImg = $("#upload_imga_id").val();
//	if($("#upload_imga_name11").val()!="" && sampleInfoImg!="" && sampleInfoImg == sampleBreastCancerTempImg){
		//检测项目验证
		var productName = $("#sampleTumorTemp_product_name").val();
		if (productName == "") {
			message("检测项目不能为空！");
			return;
		}
		var next = $("#sampleTumorTemp_nextStepFlow").val();
		if(next == ""){
			message("请选择下一步流向！");
			return;
		}
		
//	}
//	else {
//		if($("#upload_imga_id").val()==""){
//			message("请上传图片并录入信息后，进行保存！");
//			return;
//		}
//	}
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleTumorTemp_id").val(),
		title : $("#sampleTumorTemp_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTumorTemp_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});
/*
 * 上传图片
 */
function upLoadImg1(){
	var isUpload = true;
	var fId=$("#upload_imga_id11").val();
	var fName=$("#upload_imga_name").val();
	
	load("/system/template/template/toSampeUpload.action", { // 是否修改
		fileId:fId,
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			fName=data.fileName;
			fId=data.fileId;
			 document.getElementById('upload_imga_id').value=fId;
			 document.getElementById('upload_imga_name11').value=fName;
		});
	});
}

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/project/feedback/sampleFeedback/save.action?saveType="+$("#saveType").val();
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleTumorTemp_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleTumorTemp_id").val()) {
				commonChangeState("formId=" + $("#sampleTumorTemp_id").val()
						+ "&tableId=SampleTumorTemp");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleTumorTemp_code").val());
	nsc.push("信息录入编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : '信息录入',
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : '选择类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleTumorTemp_sampleType_id").value = id;
	document.getElementById("sampleTumorTemp_sampleType_name").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

//证件类型
function voucherTypeFun(){
	 var win = Ext.getCmp('voucherTypeFun');
	 if (win) {win.close();}
	 var voucherTypeFun= new Ext.Window({
	 id:'voucherTypeFun',modal:true,title:'选择证件类型',layout:'fit',width:600,height:700,closeAction:'close',
	 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	 collapsible: true,maximizable: true,
	 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
	 buttons: [
	 { text: '关闭',
	  handler: function(){
		  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
	  function setzjlx(id,name){
	  document.getElementById("voucherTypeIdNew").value = id;
	 document.getElementById("voucherTypeNew").value = name;
	 var win = Ext.getCmp('voucherTypeFun');
	 if(win){win.close();}
}

// 收据类型
function receiptTypeFun() {
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
	var receiptTypeFun = new Ext.Window(
			{
				id : 'receiptTypeFun',
				modal : true,
				title : '选择收据类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=sjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						receiptTypeFun.close();
					}
				} ]
			});
	receiptTypeFun.show();
}
function setsjlx(id, name) {
	document.getElementById("sampleTumorTemp_receiptType").value = id;
	document.getElementById("sampleTumorTemp_receiptType_name").value = name;
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
}

// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleTumorTemp_phoneNum").val())) {
		// return;
		var id = $("#sampleTumorTemp_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("输入的手机号重复！");
				}
			}
		}, null);
	} else {
		message("请输入正确的手机号码!");
	}
}
function change() {
	var selected = $('#sampleTumorTemp_embryoType option:selected').val();
	if (selected == "2") {
		$("#sampleTumorTemp_message").css("display", "");
	}
}

function checkChange() {
	var selected = $('#sampleTumorTemp_coupleChromosome option:selected').val();
	if (selected == "2") {
		$("#sampleTumorTemp_remind").css("display", "");
	}
}


// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : '选择项目',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("sampleTumorTemp_product_id").value = id;
					document.getElementById("sampleTumorTemp_product_name").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}
