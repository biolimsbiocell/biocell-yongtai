var poolingFeedbackGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
//	    fields.push({
//		name:'sampleCode',
//		type:"string"
//	});
	    fields.push({
	    name:'libraryType',
	    type:"string"
	});
	    fields.push({
		    name:'mixAmount',
		    type:"string"
		});
	    fields.push({
		    name:'mixVolume',
		    type:"string"
		});
	    fields.push({
		    name:'qpcrConcentration',
		    type:"string"
		});
	    fields.push({
		    name:'note',
		    type:"string"
		});
	    fields.push({
		name:'volume',
		type:"string"
	});
	    fields.push({
		name:'concentrer',
		type:"string"
	});
	    fields.push({
		name:'result',
		type:"string"
	});
	    fields.push({
		name:'method',
		type:"string"
	});
	    fields.push({
		name:'nextFlow',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    
    fields.push({
		name:'poolingCode',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'inspectDate',
		type:"string"
	});
	    
    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'idCard',
		type:"string"
	});
	    fields.push({
		name:'sequenceFun',
		type:"string"
	});
	    fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
    fields.push({
		name:'orderId',
		type:"string"
	});
    
    fields.push({
		name:'sequencingReadLong',
		type:"string"
	});
	    fields.push({
		name:'sequencingType',
		type:"string"
	});
	    fields.push({
		name:'sequencingPlatform',
		type:"string"
	});
	    fields.push({
		name:'totalAmount',
		type:"string"
	});
	    fields.push({
		name:'totalVolume',
		type:"string"
	});
    fields.push({
		name:'others',
		type:"string"
	});
    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
    fields.push({
		name:'classify',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'poolingCode',
		header:'Pooling编号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:10*6,
		
		sortable:true
	});
//	cm.push({
//		dataIndex:'sampleCode',
//		header:'样本编号',
//		width:20*6,
//		sortable:true,
//	});
	cm.push({
		dataIndex:'libraryType',
		header:'文库类型',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'mixAmount',
		header:'混合量',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'mixVolume',
		header:'混合体积',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'qpcrConcentration',
		header:'理论QPCR浓度',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'volume',
		header:'体积',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'concentrer',
		header:'浓度',
		width:20*6,
		
		sortable:true
	});
	var storeresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		header:'结果',
		width:20*6,
		sortable:true,
		editor : null,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:20*6,
		
		sortable:true
	});
	
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '重抽血' ], [ '1', '退费' ] ,[ '2', '合格' ],[ '3', '待反馈']]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:'处理结果',
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	
	
	
	cm.push({
		dataIndex:'patientName',
		header:'患者姓名',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样日期',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:'接收日期',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'idCard',
		header:'身份证号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		header:'应出报告日期',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:'手机号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sequencingReadLong',
		header:'测序读长',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sequencingType',
		header:'测序类型',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequencingPlatform',
		header:'测序平台',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'totalAmount',
		header:'总数据量',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'totalVolume',
		header:'总体积',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'others',
		header:'其他',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-id',
		header:'实验组',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:'实验组',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/poolingFeedback/showPoolingFeedbackListJson.action";
	var opts={};
	opts.title="Pooling异常反馈";
	opts.height=document.body.clientHeight-34;
	
	opts.tbar=[];
	opts.tbar.push({
			text : "保存",
			handler : function() {
				var result = commonGetModifyRecords(poolingFeedbackGrid);
				if (result.length > 0) {
					ajax("post", "/project/feedback/sampleFeedback/savePoolingFeedback.action", {
						itemDataJson : result,
					}, function(data) {
						if (data.success) {
							message("保存成功！");
							poolingFeedbackGrid.getStore().commitChanges();
							poolingFeedbackGrid.getStore().reload();
						} else {
							message("保存失败！");
						}
					}, null);
				}
			}
		});
	
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	poolingFeedbackGrid=gridEditTable("show_poolingFeedback_div",cols,loadParam,opts);
	$("#show_poolingFeedback_div").data("poolingFeedbackGrid", poolingFeedbackGrid);
});
function selectPoolingInfo(){
	commonSearchActionByMo(poolingFeedbackGrid,"3");
	$("#poolingFeedback_poolingCode").val("");
	//$("#poolingFeedback_sampleCode").val("");
	$("#poolingFeedback_method").val("");
}
//function add(){
//		window.location=window.ctx+'/project/feedback/poolingFeedback/editPoolingFeedback.action';
//	}
//function edit(){
//	var id="";
//	id=document.getElementById("selectId").value;
//	if (id==""||id==undefined){
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location=window.ctx+'/project/feedback/poolingFeedback/editPoolingFeedback.action?id=' + id;
//}
//function view() {
//	var id = "";
//	id = document.getElementById("selectId").value;
//	if (id == "" || id == undefined) {
//		message("请选择一条记录!");
//		return false;
//	}
//	window.location = window.ctx + '/project/feedback/poolingFeedback/viewPoolingFeedback.action?id=' + id;
//}
function exportexcel() {
	poolingFeedbackGrid.title = '导出列表';
	var vExportContent = poolingFeedbackGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				
				commonSearchAction(poolingFeedbackGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
