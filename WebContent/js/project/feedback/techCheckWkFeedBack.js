﻿
var techCheckWkFeedBackGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'pdSzie',
		type:"string"
	});
	   fields.push({
		name:'massCon',
		type:"string"
	});
	   fields.push({
		name:'molarCon',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	    fields.push({
		name:'pdResult',
		type:"string"
	});
	   fields.push({
		name:'molarResult',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
//	   fields.push({
//		name:'itemId',
//		type:"string"
//	});
	   fields.push({
		name:'isRun',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:'文库编号',
		width:30*6
	});
//	cm.push({
//		dataIndex:'itemId',
//		hidden : true,
//		header:'左侧表ID'
//	});
	cm.push({
		dataIndex:'pdSzie',
		hidden : false,
		header:'片段大小',
		width:20*6
	});
	cm.push({
		dataIndex:'massCon',
		hidden : false,
		header:'质量浓度',
		width:20*6
	});
	cm.push({
		dataIndex:'molarCon',
		hidden : false,
		header:'摩尔浓度',
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'检测后体积',
		width:20*6
	});
	cm.push({
		dataIndex:'pdResult',
		hidden : false,
		header:'片段大小判定',
		width:20*6
	});
	cm.push({
		dataIndex:'molarResult',
		hidden : false,
		header:'摩尔浓度判定',
		width:20*6
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : '合格'
			}, {
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果判定',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result)
		//editor: result
	});
	var nextFlow = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '反馈到项目组'
			}, {
				id : '1',
				name : '文库质控'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:'下一步流向',
		width:30*6,
		renderer: Ext.util.Format.comboRenderer(nextFlow)
		//editor: nextFlow
	});
	var method = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '文库质控'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理意见',
		width:30*6,
		renderer: Ext.util.Format.comboRenderer(method),
		editor: method
	});
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : '是'
			}, {
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(submit),
		editor: submit
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showTechCheckWkFeedBackListJson.action";
	var opts={};
	opts.title="文库检测结果";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	techCheckWkFeedBackGrid=gridEditTable("techCheckWkFeedBackdiv",cols,loadParam,opts);
	$("#techCheckWkFeedBackdiv").data("techCheckWkFeedBackGrid", techCheckWkFeedBackGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//保存
function save(){	
	var itemJson = commonGetModifyRecords(techCheckWkFeedBackGrid);
	if(itemJson.length>0){
		ajax("post", "/project/feedback/sampleFeedback/saveTechCheckWkFeedBack.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				techCheckWkFeedBackGrid.getStore().commitChanges();
				techCheckWkFeedBackGrid.getStore().reload();
				message("保存成功！");
			} else {
				message("保存失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
	
}
function selectTechCheckWkFeedBackInfo(){
	commonSearchAction(techCheckWkFeedBackGrid);
	$("#techCheckWkFeedBack_sampleCode").val("");
	$("#techCheckWkFeedBack_wkCode").val("");
	$("#techCheckWkFeedBack_method").val("");
}
