var plasmaFeedbackGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
//	   fields.push({
//			name:'tempId',
//			type:"string"
//		});
	   fields.push({
		name:'code',
		type:"string"
	});
   fields.push({
		name:'sampleCode',
		type:"string"
	});
   fields.push({
		name:'productId',
		type:"string"
	});
  fields.push({
		name:'productName',
		type:"string"
	});
  fields.push({
		name:'patientName',
		type:"string"
	});
fields.push({
		name:'idCard',
		type:"string"
	});
fields.push({
		name:'sequenceFun',
		type:"string"
	});
fields.push({
		name:'inspectDate',
		type:"string"
	});
fields.push({
	name:'orderId',
	type:"string"
});
fields.push({
	name:'phone',
	type:"string"
});
fields.push({
	name:'reportDate',
	type:"string"
});
//	   fields.push({
//		name:'volume',
//		type:"string"
//	});
//	   fields.push({
//		name:'unit',
//		type:"string"
//	});
	   fields.push({
		name:'result',
		type:"string"
	});
//	   fields.push({
//		name:'nextFlow',
//		type:"string"
//	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
   fields.push({
		name:'state',
		type:"string"
	});
//	    fields.push({
//		name:'bloodSampleTask-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'bloodSampleTask-name',
//		type:"string"
//	});

   fields.push({
		name:'submit',
		type:"string"
	});
   
  
 fields.push({
		name:'acceptDate',
		type:"string"
	});
 
// fields.push({
//		name:'reason',
//		type:"string"
//	});
//fields.push({
//		name:'concentration',
//		type:"string"
//	});
		fields.push({
		name:'classify',
		type:"string"
	});
		fields.push({
			name:'sampleType',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
//	cm.push({
//		dataIndex:'tempId',
//		hidden : true,
//		header:'临时表id',
//		width:40*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:10*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : false,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : false,
		header:'手机号',
		width:30*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : false,
		header:'检测方法',
		width:20*6
	});
	
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : false,
		header:'取样日期',
		width:30*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单Id',
		width:20*6
	});
//	cm.push({
//		dataIndex:'concentration',
//		hidden : false,
//		header:'浓度',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'volume',
//		hidden : false,
//		header:'体积',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'unit',
//		hidden : false,
//		header:'单位',
//		width:20*6
//	});

	var storeisresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeisresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});

//	var storenextFlowCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '文库构建' ], [ '1', '重抽血' ],[ '2', '入库' ],[ '3', '反馈至项目组' ],[ '4', '终止' ],[ '5', '暂停' ]]
//	});
//	var nextFlowCob = new Ext.form.ComboBox({
//		store : storenextFlowCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
//		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
//	});
//	var storemethodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '文库构建' ], [ '1', '入库' ],[ '2', '终止' ],[ '3', '重抽血' ],[ '4', '暂停' ]]
//	});
//	var methodCob = new Ext.form.ComboBox({
//		store : storemethodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'method',
		header:'处理意见',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});

//	cm.push({
//		dataIndex:'reason',
//		hidden : false,
//		header:'原因',
//		width:20*6
//	});
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
//	cm.push({
//		dataIndex:'bloodSampleTask-id',
//		hidden : true,
//		header:'相关主表ID',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'bloodSampleTask-name',
//		hidden : true,
//		header:'相关主表',
//		width:15*10
//	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	var type="1";
	loadParam.url=ctx+"/project/feedback/sampleFeedback/showPlasmaFeedbackListJson.action?type="+type;
	var opts={};
	opts.title="样本处理异常反馈";
	opts.height=document.body.clientHeight-34;
	opts.tbar=[];
	
	opts.tbar.push({
		text : "批量提交",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_bloodSubmit_div"), "批量提交", null, {
				"确定" : function() {
					var records = plasmaFeedbackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var submit = $("#bloodSubmit").val();
						plasmaFeedbackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("submit", submit);
						});
						plasmaFeedbackGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
//	opts.rowselect=function(id){
//		$("#selectId").val(id);
//	};
	plasmaFeedbackGrid=gridEditTable("show_plasmaFeedback_div",cols,loadParam,opts);
	$("#show_plasmaFeedback_div").data("plasmaFeedbackGrid", plasmaFeedbackGrid);
});
//function exportexcel() {
//	plasmaFeedbackGrid.title = '导出列表';
//	var vExportContent = plasmaFeedbackGrid.getExcelXml();
//	var x = document.getElementById('gridhtm');
//	x.value = vExportContent;
//	document.excelfrm.submit();
//}
function selectPlasmaInfo(){
	commonSearchAction(plasmaFeedbackGrid);
	$("#plasmaFeedback_code").val("");
	$("#plasmaFeedback_sampleCode").val("");
}
//function searchPlasma(){
//		var option = {};
//		option.width = 542;
//		option.height = 417;
//		loadDialogPage($("#jstj"), "搜索", null, {
//			"开始检索" : function() {
//				commonSearchAction(plasmaFeedbackGrid);
//				$(this).dialog("close");
//			},
//			"清空" : function() {
//				form_reset();
//			}
//		}, true, option);
//}
function save() {
	var result = commonGetModifyRecords(plasmaFeedbackGrid);
	if (result.length > 0) {
		ajax("post", "/project/feedback/sampleFeedback/savePlasmaFeedback.action", {
			itemDataJson : result,
		}, function(data) {
			if (data.success) {
				message("保存成功！");
				plasmaFeedbackGrid.getStore().commitChanges();
				plasmaFeedbackGrid.getStore().reload();
			} else {
				message("保存失败！");
			}
		}, null);
	}else{
		message("没有需要保存的数据！");
	}
}