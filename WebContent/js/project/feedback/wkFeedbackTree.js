function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/project/feedback/wkFeedback/editWkFeedback.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/project/feedback/wkFeedback/viewWkFeedback.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : '文库编号',
		dataIndex : 'id',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : '描述',
		dataIndex : 'name',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '样本编号',
		dataIndex : 'sampleCode',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '体积',
		dataIndex : 'bluk',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : 'index',
		dataIndex : 'indexs',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '浓度',
		dataIndex : 'concentrer',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '指控类型',
		dataIndex : 'qctype',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '是否合格',
		dataIndex : 'isgood',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '下一步流向',
		dataIndex : 'nextflow',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '处理意见',
		dataIndex : 'advice',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '状态id',
		dataIndex : 'state',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : '工作流状态',
		dataIndex : 'stateName',
		width:20*6,
		hidden:false
	}, 
	
			
			
			
			{
				header : '上级编码',
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#wkFeedbackTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
