var reportConfirmDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
//	    fields.push({
//		name:'age-id',
//		type:"string"
//	});
	    fields.push({
		name:'age',
		type:"string"
	});
	    fields.push({
		name:'sex',
		type:"string"
	});
	    fields.push({
		name:'linkMan',
		type:"string"
	});
	    fields.push({
		name:'linkManWay',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'样本编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'姓名',
		width:50*10,
		sortable:true
	});
//		cm.push({
//		dataIndex:'age-id',
//		header:'年龄ID',
//		width:20*10,
//		sortable:true
//		});
		cm.push({
		dataIndex:'age',
		header:'年龄',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'sex',
		header:'性别',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'linkMan',
		header:'联系人',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'linkManWay',
		header:'联系人方式',
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		header:'样本类型',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/report/reportConfirm/showReportConfirmListJson.action";
	var opts={};
	opts.title="报告审核";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setReportConfirmFun(rec);
	};
	reportConfirmDialogGrid=gridTable("show_dialog_reportConfirm_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(reportConfirmDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
