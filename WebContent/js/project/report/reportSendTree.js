function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/project/report/reportSend/editReportSend.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/project/report/reportSend/viewReportSend.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : '报告编号',
		dataIndex : 'id',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : '样本编号',
		dataIndex : 'sampleId',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : '姓名',
		dataIndex : 'name-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : '检测项目',
		dataIndex : 'testItem',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '是否发送',
		dataIndex : 'isSend',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '寄送地址',
		dataIndex : 'sendAddress',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '寄送人',
		dataIndex : 'sender',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '联系方式',
		dataIndex : 'linkWay',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : '快递信息',
		dataIndex : 'expressNews-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : '发送日期',
		dataIndex : 'sendDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '备注',
		dataIndex : 'note',
		width:50*6,
		hidden:false
	}, 
	
			
			
			
			{
				header : '上级编码',
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#reportSendTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
