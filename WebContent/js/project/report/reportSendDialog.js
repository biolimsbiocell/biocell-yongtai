var reportSendDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'sampleId',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
//	    fields.push({
//		name:'name-name',
//		type:"string"
//	});
	    fields.push({
		name:'testItem',
		type:"string"
	});
	    fields.push({
		name:'isSend',
		type:"string"
	});
	    fields.push({
		name:'sendAddress',
		type:"string"
	});
	    fields.push({
		name:'sender',
		type:"string"
	});
	    fields.push({
		name:'linkWay',
		type:"string"
	});
//	    fields.push({
//		name:'expressNews-id',
//		type:"string"
//	});
	    fields.push({
		name:'expressNews',
		type:"string"
	});
	    fields.push({
		name:'sendDate',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'报告编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleId',
		header:'样本编号',
		width:50*10,
		sortable:true
	});
//		cm.push({
//		dataIndex:'name-id',
//		header:'姓名ID',
//		width:20*10,
//		sortable:true
//		});
		cm.push({
		dataIndex:'name',
		header:'姓名',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'testItem',
		header:'检测项目',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'isSend',
		header:'是否发送',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sendAddress',
		header:'寄送地址',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sender',
		header:'寄送人',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'linkWay',
		header:'联系方式',
		width:50*10,
		sortable:true
	});
//		cm.push({
//		dataIndex:'expressNews-id',
//		header:'快递信息ID',
//		width:20*10,
//		sortable:true
//		});
		cm.push({
		dataIndex:'expressNews',
		header:'快递信息',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'sendDate',
		header:'发送日期',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/report/reportSend/showReportSendListJson.action";
	var opts={};
	opts.title="报告发送";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setReportSendFun(rec);
	};
	reportSendDialogGrid=gridTable("show_dialog_reportSend_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(reportSendDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
