var abnormalFeedbackHandleDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
//	    fields.push({
//		name:'plasmaId',
//		type:"string"
//	});
	    fields.push({
		name:'bluk',
		type:"string"
	});
//	    fields.push({
//		name:'bluk',
//		type:"string"
//	});
	    fields.push({
		name:'resultDecide-id',
		type:"string"
	});
	    fields.push({
		name:'resultDecide-name',
		type:"string"
	});
//	    fields.push({
//		name:'resultDecide-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'resultDecide-name',
//		type:"string"
//	});
	    fields.push({
		name:'nextFlow',
		type:"string"
	});
//	    fields.push({
//		name:'nextFlow',
//		type:"string"
//	});
	    fields.push({
		name:'handleIdea',
		type:"string"
	});
//	    fields.push({
//		name:'handleIdea',
//		type:"string"
//	});
	    fields.push({
		name:'note',
		type:"string"
	});
//	    fields.push({
//		name:'note',
//		type:"string"
//	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'血浆编号',
		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'plasmaId',
//		header:'血浆编号',
//		width:20*10,
//		sortable:true
//	});
	cm.push({
		dataIndex:'bluk',
		header:'体积',
		width:50*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'bluk',
//		header:'体积',
//		width:50*10,
//		sortable:true
//	});
		cm.push({
		dataIndex:'resultDecide-id',
		header:'结果判定ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'resultDecide-name',
		header:'结果判定',
		width:20*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'resultDecide-id',
//		header:'结果判定ID',
//		width:20*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'resultDecide-name',
//		header:'结果判定',
//		width:20*10,
//		sortable:true
//		});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'nextFlow',
//		header:'下一步流向',
//		width:20*10,
//		sortable:true
//	});
	cm.push({
		dataIndex:'handleIdea',
		header:'处理意见',
		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'handleIdea',
//		header:'处理意见',
//		width:20*10,
//		sortable:true
//	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'note',
//		header:'备注',
//		width:50*10,
//		sortable:true
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/project/excpt/abnormalFeedbackHandle/showAbnormalFeedbackHandleListJson.action";
	var opts={};
	opts.title="异常反馈处理";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setAbnormalFeedbackHandleFun(rec);
	};
	abnormalFeedbackHandleDialogGrid=gridTable("show_dialog_abnormalFeedbackHandle_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(abnormalFeedbackHandleDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
