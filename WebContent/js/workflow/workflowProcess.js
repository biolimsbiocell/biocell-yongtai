$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"visible":false,
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.name,
	});
	colOpts.push({
		"data": "createUser-name",
		"title": biolims.sample.createUserName,
	});
	colOpts.push({
		"data": "createDate",
		"title": biolims.sample.createDate,
	});
	colOpts.push({
		"data": "orderBlockName",
		"title": biolims.common.applicationName,
	});
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.state,
	});
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
	});
	var tbarOpts = [];
	var options = table(true,"",
		"/workflow/process/showWorkflowProcessJson.action",colOpts, tbarOpts)
	myTable= renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(myTable);
	})
});
function add(){
		window.location=window.ctx+'/workflow/process/editWorkflowProcess.action';
	}
function edit(){
	var id="";
	var id = $(".selected").find("input").val();
	if (id==""||id==undefined){
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/workflow/process/editWorkflowProcess.action?id=' + id ;
}
function view() {
	var id = "";
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/workflow/process/viewWorkflowProcess.action?id=' + id;
}

//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "描述",
			"type": "input",
			"searchName": "name"
		},
		{
			"txt":"创建人",
			"type":"input",
			"searchName":"createUser-name"
		},
		{
			"txt": "创建时间",
			"type": "dataTime",
			"searchName": "createDate##@@##",
			"mark": "c##@@##",
		},
		{
			"txt": "创建时间（开始）",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt":"创建时间（结束）",
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"type": "table",
			"table": myTable
		}
	];
}
