$(function() {
	initFlowchart();
})
var FlowchartArr = [];
function getFlowchartJson(textareaa) {
		top.layer.open({
			title: biolims.common.selectProcessNode,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/workflow/def/selectWorkflowDef.action?orderBlock="+$("#workflowProcess_orderBlock").val(), ''],
			yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addWorkflowDef .chosed").children("td").eq(0).text();
				var cls = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addWorkflowDef .chosed").children("td").eq(1).text();
				var oper = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addWorkflowDef .chosed").children("td").eq(2).text();
				var state = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addWorkflowDef .chosed").children("td").eq(3).text();
				var stateName = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addWorkflowDef .chosed").children("td").eq(4).text();
				top.layer.close(index)
				FlowchartArr.push(name + "-" + cls + "-" + oper + "-" + state + "-" + stateName);
				textareaa.value = name;
			},
		})
}

function initFlowchart() {
	if(window.goSamples) goSamples(); // init for these samples -- you don't need to call this
	var $ = go.GraphObject.make; // for conciseness in defining templates

	myDiagram =
		$(go.Diagram, "myDiagramDiv", // must name or refer to the DIV HTML element
			{
				initialContentAlignment: go.Spot.Center,
				allowDrop: true, // must be true to accept drops from the Palette
				"LinkDrawn": showLinkLabel, // this DiagramEvent listener is defined below
				"LinkRelinked": showLinkLabel,
				scrollsPageOnFocus: false,
				"undoManager.isEnabled": true // enable undo & redo
			});

	// when the document is modified, add a "*" to the title and enable the "Save" button
	myDiagram.addDiagramListener("Modified", function(e) {
		var button = document.getElementById("SaveButton");
		if(button) button.disabled = !myDiagram.isModified;
		var idx = document.title.indexOf("*");
		if(myDiagram.isModified) {
			if(idx < 0) document.title += "*";
		} else {
			if(idx >= 0) document.title = document.title.substr(0, idx);
		}
	});

	// helper definitions for node templates

	function nodeStyle() {
		return [
			// The Node.location comes from the "loc" property of the node data,
			// converted by the Point.parse static method.
			// If the Node.location is changed, it updates the "loc" property of the node data,
			// converting back using the Point.stringify static method.
			new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
			{
				// the Node.location is at the center of each node
				locationSpot: go.Spot.Center,
				//isShadowed: true,
				//shadowColor: "#888",
				// handle mouse enter/leave events to show/hide the ports
				mouseEnter: function(e, obj) {
					showPorts(obj.part, true);
				},
				mouseLeave: function(e, obj) {
					showPorts(obj.part, false);
				}
			}
		];
	}

	// Define a function for creating a "port" that is normally transparent.
	// The "name" is used as the GraphObject.portId, the "spot" is used to control how links connect
	// and where the port is positioned on the node, and the boolean "output" and "input" arguments
	// control whether the user can draw links from or to the port.
	function makePort(name, spot, output, input) {
		// the port is basically just a small circle that has a white stroke when it is made visible
		return $(go.Shape, "Circle", {
			fill: "transparent",
			stroke: null, // this is changed to "white" in the showPorts function
			desiredSize: new go.Size(8, 8),
			alignment: spot,
			alignmentFocus: spot, // align the port on the main Shape
			portId: name, // declare this object to be a "port"
			fromSpot: spot,
			toSpot: spot, // declare where links may connect at this port
			fromLinkable: output,
			toLinkable: input, // declare whether the user may draw links to/from here
			cursor: "pointer" // show a different cursor to indicate potential link point

		});
	}

	// define the Node templates for regular nodes

	var lightText = 'whitesmoke';

	myDiagram.nodeTemplateMap.add("", // the default category
		$(go.Node, "Spot", nodeStyle(),
			// the main object is a Panel that surrounds a TextBlock with a rectangular Shape
			$(go.Panel, "Auto",
				$(go.Shape, "Rectangle", {
						fill: "#00A9C9",
						stroke: null
					},
					new go.Binding("figure", "figure")),
				$(go.TextBlock, {
						font: "bold 11pt Helvetica, Arial, sans-serif",
						stroke: lightText,
						margin: 8,
						maxSize: new go.Size(160, NaN),
						wrap: go.TextBlock.WrapFit,
						editable: true,
					},
					new go.Binding("text").makeTwoWay())
			),
			// four named ports, one on each side:
			makePort("T", go.Spot.Top, true, true),
			makePort("L", go.Spot.Left, true, true),
			makePort("R", go.Spot.Right, true, true),
			makePort("B", go.Spot.Bottom, true, true)
		));

	myDiagram.nodeTemplateMap.add("Start",
		$(go.Node, "Spot", nodeStyle(),
			$(go.Panel, "Auto",
				$(go.Shape, "Circle", {
					minSize: new go.Size(40, 40),
					fill: "#79C900",
					stroke: null
				}),
				$(go.TextBlock, "Start", {
						font: "bold 11pt Helvetica, Arial, sans-serif",
						stroke: lightText
					},
					new go.Binding("text"))
			),
			// three named ports, one on each side except the top, all output only:
			makePort("L", go.Spot.Left, true, false),
			makePort("R", go.Spot.Right, true, false),
			makePort("B", go.Spot.Bottom, true, false)
		));
	myDiagram.nodeTemplateMap.add("End",
		$(go.Node, "Spot", nodeStyle(),
			$(go.Panel, "Auto",
				$(go.Shape, "Circle", {
					minSize: new go.Size(40, 40),
					fill: "#DC3C00",
					stroke: null
				}),
				$(go.TextBlock, "End", {
						font: "bold 11pt Helvetica, Arial, sans-serif",
						stroke: lightText
					},
					new go.Binding("text"))
			),
			// three named ports, one on each side except the bottom, all input only:
			makePort("T", go.Spot.Top, false, true),
			makePort("L", go.Spot.Left, false, true),
			makePort("R", go.Spot.Right, false, true)
		));
	myDiagram.nodeTemplateMap.add("Comment",
		$(go.Node, "Auto", nodeStyle(),
			$(go.Shape, "File", {
				fill: "#EFFAB4",
				stroke: null
			}),
			$(go.TextBlock, {
					margin: 5,
					maxSize: new go.Size(200, NaN),
					wrap: go.TextBlock.WrapFit,
					textAlign: "center",
					editable: true,
					font: "bold 12pt Helvetica, Arial, sans-serif",
					stroke: '#454545'
				},
				new go.Binding("text").makeTwoWay())
			// no ports, because no links are allowed to connect with a comment
		));

	// replace the default Link template in the linkTemplateMap
	myDiagram.linkTemplate =
		$(go.Link, // the whole link panel
			{
				routing: go.Link.AvoidsNodes,
				curve: go.Link.JumpOver,
				corner: 5,
				toShortLength: 4,
				relinkableFrom: true,
				relinkableTo: true,
				reshapable: true,
				resegmentable: true,
				// mouse-overs subtly highlight links:
				mouseEnter: function(e, link) {
					link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)";
				},
				mouseLeave: function(e, link) {
					link.findObject("HIGHLIGHT").stroke = "transparent";
				}
			},
			new go.Binding("visible", "visible").makeTwoWay(),
			$(go.Shape, // the highlight shape, normally transparent
				{
					isPanelMain: true,
					strokeWidth: 8,
					stroke: "transparent",
					name: "HIGHLIGHT"
				}),
			$(go.Shape, // the link path shape
				{
					isPanelMain: true,
					stroke: "gray",
					strokeWidth: 2
				}),
			$(go.Shape, // the arrowhead
				{
					toArrow: "standard",
					stroke: null,
					fill: "gray"
				}),
			$(go.TextBlock, "text", // the label
				{
					textAlign: "center",
					font: "11pt helvetica, arial, sans-serif",
					stroke: "#333333",
					editable: true
				},
				new go.Binding("text").makeTwoWay())
			/*	$(go.Panel, "Auto", // the link label, normally not visible
					{
						visible: false,
						name: "LABEL",
						segmentIndex: 2,
						segmentFraction: 0.5
					},
					new go.Binding("visible", "visible").makeTwoWay(),
					$(go.Shape, "RoundedRectangle", // the label shape
						{
							fill: "#F8F8F8",
							stroke: null
						}),
					$(go.TextBlock, "Yes", // the label
						{
							textAlign: "center",
							font: "10pt helvetica, arial, sans-serif",
							stroke: "#333333",
							editable: true
						},
						new go.Binding("text").makeTwoWay())
				)*/
		);

	// Make link labels visible if coming out of a "conditional" node.
	// This listener is called by the "LinkDrawn" and "LinkRelinked" DiagramEvents.
	function showLinkLabel(e) {
		var label = e.subject.findObject("LABEL");
		if(label !== null) label.visible = (e.subject.fromNode.data.figure === "Diamond");
	}

	// temporary links used by LinkingTool and RelinkingTool are also orthogonal:
	myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
	myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;
	if(document.getElementById("workflowProcess_id").value == "") {
		myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModelnew").value);
	} else {
		myDiagram.model = go.Model.fromJson(document.getElementById("contentData").value);
	}

	// initialize the Palette that is on the left side of the page
	myPalette =
		$(go.Palette, "myPaletteDiv", // must name or refer to the DIV HTML element
			{
				scrollsPageOnFocus: false,
				nodeTemplateMap: myDiagram.nodeTemplateMap, // share the templates used by myDiagram
				model: new go.GraphLinksModel([ // specify the contents of the Palette
					{
						category: "Start",
						text: biolims.common.start
					},
					{
						text: biolims.common.judge,
						figure: "Diamond"
					},
					{
						text: biolims.common.stepName
					},
					{
						category: "End",
						text: biolims.common.end
					}
				])
			});
} // end init

// Make all ports on a node visible when the mouse is over the node
function showPorts(node, show) {
	var diagram = node.diagram;
	if(!diagram || diagram.isReadOnly || !diagram.allowLink) return;
	node.ports.each(function(port) {
		port.stroke = (show ? "white" : null);
	});
}
// Show the diagram's model in JSON format that the user may edit
function changeState() {
	top.layer.confirm(biolims.common.confirmReleaseWorkflow, {
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	}, function(index) {
		var id = $("#workflowProcess_id").val();
		if(id == "" || id == null) {
			var workOrderFlowchartJson = myDiagram.model.toJson();
			var workOrderFlowchartArr = JSON.parse(workOrderFlowchartJson)
			workOrderFlowchartArr.nodeDataArray.forEach(function(v, i) {
				FlowchartArr.forEach(function(vv, ii) {
					var name = vv.split("-")[0];
					var cls = vv.split("-")[1];
					var oper = vv.split("-")[2];
					if(name == v.text) {
						v.cls = cls;
						v.oper = oper;
					}
				});
			});
			workOrderFlowchartArr.linkDataArray.forEach(function(a, j) {
				FlowchartArr.forEach(function(aa, jj) {
					var name = aa.split("-")[0];
					var cls = aa.split("-")[1];
					var oper = aa.split("-")[2];
					var state = aa.split("-")[3];
					var stateName = aa.split("-")[4];
					if(name == a.text) {
						a.cls = cls;
						a.oper = oper;
						a.state = state;
						a.stateName = stateName;
					}
				});
			});
		} else {
			var workOrderFlowchartOldArr = JSON.parse($("#contentData").val());
			workOrderFlowchartOldArr.nodeDataArray.forEach(function(v, i) {
				if(v.text == biolims.common.start) {
					return true;
				}
				FlowchartArr.push(v.name + "-" + v.cls + "-" + v.oper);
			});
			workOrderFlowchartOldArr.linkDataArray.forEach(function(a, j) {
				FlowchartArr.push(a.name + "-" + a.cls + "-" + a.oper+"-"+a.state+"-"+a.stateName);
			});
			var workOrderFlowchartJson = myDiagram.model.toJson();
			var workOrderFlowchartArr = JSON.parse(workOrderFlowchartJson)
			workOrderFlowchartArr.nodeDataArray.forEach(function(v, i) {
				FlowchartArr.forEach(function(vv, ii) {
					var name = vv.split("-")[0];
					var cls = vv.split("-")[1];
					var oper = vv.split("-")[2];
					if(name == v.text) {
						v.cls = cls;
						v.oper = oper;
					}
				});
			});
			workOrderFlowchartArr.linkDataArray.forEach(function(b, k) {
				FlowchartArr.forEach(function(bb, kk) {
					var name = bb.split("-")[0];
					var cls = bb.split("-")[1];
					var oper = bb.split("-")[2];
					var state = bb.split("-")[3];
					var stateName = bb.split("-")[4];
					if(name == b.text) {
					b.cls = cls;
					b.oper = oper;
					b.state = state;
					b.stateName = stateName;
					}
				});
			});
		}
		var xmlJson = genarateXML(workOrderFlowchartArr);
		var xml = '<?xml version=\"1.0\" encoding=\"GBK\"?><definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:activiti=\"http://activiti.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc = \"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi = \"http://www.omg.org/spec/DD/20100524/DI\" typeLanguage = \"http://www.w3.org/2001/XMLSchema\" expressionLanguage = \"http://www.w3.org/1999/XPath\" targetNamespace = \"http://www.activiti.org/test\" >' +
			'<process id=\"' + $("#workflowProcess_orderBlock").val() + '\" name=\"' + $("#workflowProcess_orderBlockName").val() + '\">' +
			'<extensionElements>' +
			/*'<activiti:executionListener event=\"start\" class=\"com.biolims.workflow.listener.ProcessStartBusinessStateListener\"></activiti:executionListener>' +*/
			'<activiti:executionListener event=\"end\" class=\"com.biolims.workflow.listener.ProcessEndEventListener\"></activiti:executionListener>' +
			/*'<activiti:executionListener event=\"end\" class=\"com.biolims.workflow.listener.ProcessEndBusinessStateListener\"></activiti:executionListener>' +*/
			'</extensionElements>' +
			xmlJson +
			'</process>' +
			'</definitions>';
		console.log(xml);
		var svg = makeImage();
		var src = svg.src.split(",")[1]
		$.ajax({
		type:"post", 
		url:"/workflow/processdefinition/deploy.action",
		dataType:"json",
		data:{                      
			id: id,
			xmlJson: xml,
			svg: src,
			tableId: $("#workflowProcess_orderBlock").val()
		}
		,success: function(data) {
			if(data.success) {
				top.layer.msg(biolims.common.deploymentSuccess)
			} else {
				top.layer.msg(biolims.common.deploymentFailure)
			}
			top.layer.close(index);
		}
		});
	});
}

function genarateXML(json) {
	var jsonXML = "";
	json.nodeDataArray.forEach(function(v, i) {
		if(!v.category && !v.figure) {
			jsonXML +=
				'<userTask id=\"biolims' + v.key + '\" name=\"' + v.text + '\">' +
				'<extensionElements>' +
				'<activiti:taskListener event=\"create\" class=\"' + v.cls + '\"></activiti:taskListener>' +
				'</extensionElements>' +
				'</userTask>';
		} else if(v.category == 'Start') {
			jsonXML +=
				'<startEvent id=\"biolims' + v.key + '\" name=\"Start\"></startEvent>';
		} else if(v.category == 'End') {
			jsonXML +=
				'<endEvent id=\"biolims' + v.key + '\" name=\"End\"></endEvent>';
		} else if(v.figure) {
			jsonXML +=
				'<exclusiveGateway id=\"biolims' + v.key + '" name=\"' + v.text + '\"></exclusiveGateway>';
		}

	})
	json.linkDataArray.forEach(function(vv, ii) {
		var flow = 1 + ii;
		if(vv.state != "" && vv.state != undefined) {
			jsonXML +=
			'<sequenceFlow id=\"flow' + flow + '\" name=\"' + vv.text + '\" sourceRef=\"biolims' + vv.from + '\" targetRef=\"biolims' + vv.to + '\">' +
				'<extensionElements>' +
				'<activiti:executionListener event=\"take\" class=\"'+vv.cls+'\">' +
				'<activiti:field name=\"state\">' +
				'<activiti:string>'+vv.state+'</activiti:string>' +
				'</activiti:field>' +
				'<activiti:field name=\"stateName\">' +
				'<activiti:string>'+vv.stateName+'</activiti:string>' +
				'</activiti:field>' +
				'</activiti:executionListener>' +
				'</extensionElements>' +
				'</sequenceFlow>'
		} else if(vv.oper != "" && vv.oper != undefined&& vv.oper != 2) {
			jsonXML +=
				'<sequenceFlow id=\"flow' + flow + '\" name=\"' + vv.text + '\" sourceRef=\"biolims' + vv.from + '\" targetRef=\"biolims' + vv.to + '\">' +
				'<extensionElements>' +
				'<activiti:executionListener event=\"take\" class=\"' + vv.cls + '\"></activiti:executionListener>' +
				'</extensionElements>' +
				'<conditionExpression xsi:type=\"tFormalExpression\"><![CDATA[${oper==\"' + vv.oper + '\"}]]></conditionExpression>' +
				'</sequenceFlow>';
		} else if(vv.cls && vv.oper == 2) {
			jsonXML +=
				'<sequenceFlow id=\"flow' + flow + '\" name=\"' + vv.text + '\" sourceRef=\"biolims' + vv.from + '\" targetRef=\"biolims' + vv.to + '\">' +
				'<extensionElements>' +
				'<activiti:executionListener event=\"take\" class=\"' + vv.cls + '\"></activiti:executionListener>' +
				'</extensionElements>' +
				'</sequenceFlow>';
		} else {
			jsonXML +=
				'<sequenceFlow id=\"flow' + flow + '\" name=\"' + vv.text + '\" sourceRef=\"biolims' + vv.from + '\" targetRef=\"biolims' + vv.to + '\"></sequenceFlow>';
		}
	})
	return jsonXML;
}

function save() {
	var tableId = $("#workflowProcess_orderBlock").val();
	if(tableId == null || tableId == "") {
		top.layer.msg(biolims.common.selectModule);
		return
	}
	var id = $("#workflowProcess_id").val();
	if(id == "" || id == null) {
		var workOrderFlowchartJson = myDiagram.model.toJson();
		var workOrderFlowchartArr = JSON.parse(workOrderFlowchartJson)
		workOrderFlowchartArr.nodeDataArray.forEach(function(v, i) {
			FlowchartArr.forEach(function(vv, ii) {
				var name = vv.split("-")[0];
				var cls = vv.split("-")[1];
				var oper = vv.split("-")[2];
				if(name == v.text) {
					v.cls = cls;
					v.oper = oper;
				}
			});
		});
		workOrderFlowchartArr.linkDataArray.forEach(function(a, j) {
			FlowchartArr.forEach(function(aa, jj) {
				var name = aa.split("-")[0];
				var cls = aa.split("-")[1];
				var oper = aa.split("-")[2];
				var state = aa.split("-")[3];
				var stateName = aa.split("-")[4];
				if(name == a.text) {
					a.cls = cls;
					a.oper = oper;
					a.state = state;
					a.stateName = stateName;
				}
			});
		});
	} else {
		var workOrderFlowchartOldArr = JSON.parse($("#contentData").val());
		workOrderFlowchartOldArr.nodeDataArray.forEach(function(v, i) {
			if(v.text == biolims.common.start) {
				return true;
			}
			FlowchartArr.push(v.name + "-" + v.cls + "-" + v.oper);
		});
		workOrderFlowchartOldArr.linkDataArray.forEach(function(a, j) {
			FlowchartArr.push(a.name + "-" + a.cls + "-" + a.oper);
		});
		var workOrderFlowchartJson = myDiagram.model.toJson();
		var workOrderFlowchartArr = JSON.parse(workOrderFlowchartJson)
		workOrderFlowchartArr.nodeDataArray.forEach(function(v, i) {
			FlowchartArr.forEach(function(vv, ii) {
				var name = vv.split("-")[0];
				var cls = vv.split("-")[1];
				var oper = vv.split("-")[2];
				if(name == v.text) {
					v.cls = cls;
					v.oper = oper;
				}
			});
		});
		workOrderFlowchartArr.linkDataArray.forEach(function(b, k) {
			FlowchartArr.forEach(function(bb, kk) {
				var name = bb.split("-")[0];
				var cls = bb.split("-")[1];
				var oper = bb.split("-")[2];
				var state = bb.split("-")[3];
				var stateName = bb.split("-")[4];
				if(name == b.text) {
					b.cls = cls;
					b.oper = oper;
					b.state = state;
					b.stateName = stateName;
				}
			});
		});
	}
	console.log(JSON.stringify(workOrderFlowchartArr))
	$("#contentData").val(JSON.stringify(workOrderFlowchartArr));
	top.layer.load(4, {shade:0.3}); 
	form1.action = window.ctx + "/workflow/process/save.action";
	form1.submit();
	top.layer.closeAll();
}

// add an SVG rendering of the diagram at the end of this page
function makeImage() {
	var img = myDiagram.makeImage({
		//		scale: 0.5
	});
	return img;
}

function list() {
	window.location = window.ctx + '/workflow/process/showWorkflowProcess.action';
}

function add() {
	window.location = window.ctx + '/workflow/process/editWorkflowProcess.action';
}

function choseQK() {
		top.layer.open({
			title: biolims.common.selectModule,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/applicationTypeTable/showDialogApplicationTypeTable.action?flag=2", ''],
			yes: function(index, layer) {
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addApplicationTypeTable .chosed").children("td").eq(0).text();
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addApplicationTypeTable .chosed").children("td").eq(1).text();
				top.layer.close(index)
				$("#workflowProcess_orderBlock").val(id);
				$("#workflowProcess_orderBlockName").val(name);
			},
		})
}