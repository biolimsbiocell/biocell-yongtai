$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : "id",
		field : "id"
	});
	
	   
	fields.push({
		title : "描述",
		field : "name"
	});
	
	  fields.push({
		title : "创建人",
		field : "createUser.name"
	});
	   
	fields.push({
		title : "创建时间",
		field : "createDate"
	});
	
	   
	fields.push({
		title : "状态",
		field : "state"
	});
	
	   
	fields.push({
		title : "状态名称",
		field : "stateName"
	});
	
	   
	fields.push({
		title : "发生部门",
		field : "happenDepartment"
	});
	
	   
	fields.push({
		title : "发生时间",
		field : "happenDate"
	});
	
	   
	fields.push({
		title : "发生地点",
		field : "happenAddress"
	});
	
	  fields.push({
		title : "发现人",
		field : "discoverer.name"
	});
	   
	fields.push({
		title : "紧急纠正措施id",
		field : "correctiveActionId"
	});
	
	   
	fields.push({
		title : "紧急纠正措施",
		field : "correctiveActionName"
	});
	
	   
	fields.push({
		title : "偏差种类id",
		field : "kindId"
	});
	
	   
	fields.push({
		title : "偏差种类",
		field : "kindName"
	});
	
	  fields.push({
		title : "偏差报告人",
		field : "reportUser.name"
	});
	   
	fields.push({
		title : "偏差报告日期",
		field : "reportDate"
	});
	
	  fields.push({
		title : "部门负责人",
		field : "departmentUser.name"
	});
	   
	fields.push({
		title : "部门负责日期",
		field : "departmentDate"
	});
	
	  fields.push({
		title : "监控人",
		field : "monitor.name"
	});
	   
	fields.push({
		title : "监控日期",
		field : "monitoringDate"
	});
	
	   
	fields.push({
		title : "影响",
		field : "influence"
	});
	
	   
	fields.push({
		title : "偏差分类",
		field : "type"
	});
	
	  fields.push({
		title : "调查组长",
		field : "groupLeader.name"
	});
	   
	fields.push({
		title : "调查组组员",
		field : "groupMembers"
	});
	
	   
	fields.push({
		title : "偏差编号",
		field : "no"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/deviation/deviationHandlingReport/showDeviationHandlingReportListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/deviation/deviationHandlingReport/editDeviationHandlingReport.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/deviationHandlingReport/editDeviationHandlingReport.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/deviationHandlingReport/viewDeviationHandlingReport.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : "id",
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "描述",
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : '创建人',
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "创建时间",
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态",
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态名称",
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "发生部门",
		"searchName" : 'happenDepartment',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "发生时间",
		"searchName" : 'happenDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "发生地点",
		"searchName" : 'happenAddress',
		"type" : "input"
	});
	
	   fields.push({
		header : '发现人',
		"searchName" : 'discoverer.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "紧急纠正措施id",
		"searchName" : 'correctiveActionId',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "紧急纠正措施",
		"searchName" : 'correctiveActionName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "偏差种类id",
		"searchName" : 'kindId',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "偏差种类",
		"searchName" : 'kindName',
		"type" : "input"
	});
	
	   fields.push({
		header : '偏差报告人',
		"searchName" : 'reportUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "偏差报告日期",
		"searchName" : 'reportDate',
		"type" : "input"
	});
	
	   fields.push({
		header : '部门负责人',
		"searchName" : 'departmentUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "部门负责日期",
		"searchName" : 'departmentDate',
		"type" : "input"
	});
	
	   fields.push({
		header : '监控人',
		"searchName" : 'monitor.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "监控日期",
		"searchName" : 'monitoringDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "影响",
		"searchName" : 'influence',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "偏差分类",
		"searchName" : 'type',
		"type" : "input"
	});
	
	   fields.push({
		header : '调查组长',
		"searchName" : 'groupLeader.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "调查组组员",
		"searchName" : 'groupMembers',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "偏差编号",
		"searchName" : 'no',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

