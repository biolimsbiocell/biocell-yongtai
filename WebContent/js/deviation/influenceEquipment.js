var influenceEquipmentTable;
var oldinfluenceEquipmentChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#deviationHandlingReport_id").val();
	var colOpts = [];
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "编号",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
	});
	   colOpts.push({
		"data":"name",
		"title": "描述",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "edit"
	});
	   colOpts.push({
		   "data":"equipmentId",
		   "title": "设备ID",
		   "createdCell": function(td) {
			   $(td).attr("saveName", "equipmentId");
		   },
	   });
	   colOpts.push({
		   "data":"equipmentName",
		   "title": "设备名称",
		   "createdCell": function(td) {
			   $(td).attr("saveName", "equipmentName");
		   },
	   });
	   colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#influenceEquipmentTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveInfluenceEquipment($("#influenceEquipmentTable"));
		}
	});
	tbarOpts.push({
		text: "选择设备",
		action: function() {
			selectEquipment();
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#influenceEquipmentTable"))
		}
	});
	}
	
	var influenceEquipmentOptions = table(true,
		id,
		'/deviation/deviationHandlingReport/showInfluenceEquipmentTableJson.action', colOpts, tbarOpts)
	influenceEquipmentTable = renderData($("#influenceEquipmentTable"), influenceEquipmentOptions);
	influenceEquipmentTable.on('draw', function() {
		oldinfluenceEquipmentChangeLog = influenceEquipmentTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});


function selectEquipment(){
	var rows = $("#influenceEquipmentTable .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/equipment/main/showInstrumentDialogList.action", ''],
		yes: function(index, layer) {
			rows.addClass("editagain");
			var ids = [];
			var names = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addInstrument .selected").each(function (i, v){
				ids.push($(v).children("td").eq(1).text());
				names.push($(v).children("td").eq(2).text());
			})
			rows.find("td[savename='equipmentId']").text(ids.join(","));
			rows.find("td[savename='equipmentName']").text(names.join(","));
			top.layer.close(index);
		}
	})
}

// 保存
function saveInfluenceEquipment(ele) {
	var data = saveInfluenceEquipmentjson(ele);
	var ele=$("#influenceEquipmentTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog3(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/deviation/deviationHandlingReport/saveInfluenceEquipmentTable.action',
		data: {
			id: $("#deviationHandlingReport_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				influenceEquipmentTable.ajax.reload();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveInfluenceEquipmentjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog3(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldinfluenceEquipmentChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
