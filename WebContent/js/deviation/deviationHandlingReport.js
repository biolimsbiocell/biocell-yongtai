var deviationHandlingReportTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":"编号",
	});
	
	    fields.push({
		"data":"name",
		"title":"偏差描述",
	});
	
	    fields.push({
		"data":"createUser-id",
		"visible":false,
		"title":"创建人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":"创建人",
	});
	
	    fields.push({
		"data":"createDate",
		"title":"创建时间",
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	
	    fields.push({
		"data":"state",
		"visible":false,
		"title":"状态码",
	});
	
	    fields.push({
		"data":"stateName",
		"title":"状态名称",
	});
	
	    fields.push({
		"data":"happenDepartment",
		"title":"发生部门",
	});
	
	    fields.push({
		"data":"happenDate",
		"title":"发生时间",
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	
	    fields.push({
		"data":"happenAddress",
		"title":"发生地点",
	});
	
	    fields.push({
		"data":"discoverer-id",
		"visible":false,
		"title":"发现人ID"
	});
	    fields.push({
		"data":"discoverer-name",
		"title":"发现人"
	});
	
	   /* fields.push({
		"data":"correctiveActionId",
		"title":biolims.deviationHandlingReport.correctiveActionId,
		"visible":false,
	});
	
	    fields.push({
		"data":"correctiveActionName",
		"visible":false,
		"title":biolims.deviationHandlingReport.correctiveActionName,
	});*/
	
	    /*fields.push({
		"data":"kindId",
		"title":biolims.deviationHandlingReport.kindId,
	});
	
	    fields.push({
		"data":"kindName",
		"title":biolims.deviationHandlingReport.kindName,
	});*/
	
	    fields.push({
		"data":"reportUser-id",
		"visible":false,
		"title":"偏差报告人ID"
	});
	    fields.push({
		"data":"reportUser-name",
		"title":"报告人"
	});
	
	    fields.push({
		"data":"reportDate",
		"title":"报告日期",
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	
	    fields.push({
		"data":"departmentUser-id",
		"visible":false,
		"title":"部门负责人ID"
	});
	    fields.push({
		"data":"departmentUser-name",
		"title":"部门负责人"
	});
	
	    fields.push({
		"data":"departmentDate",
		"title":"负责日期",
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	
	    fields.push({
		"data":"monitor-id",
		"visible":false,
		"title":"监控人ID"
	});
	    fields.push({
		"data":"monitor-name",
		"title":"监控人"
	});
	
	    fields.push({
		"data":"monitoringDate",
		"title":"监控日期",
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	
	    fields.push({
		"data":"influence",
		"title":"偏差影响",
	});
	
	    fields.push({
		"data":"type",
		"title":"偏差分类",
	});
	
	    fields.push({
		"data":"groupLeader-id",
		"visible":false,
		"title":"调查组长ID"
	});
	    fields.push({
		"data":"groupLeader-name",
		"title":"调查组组长"
	});
	
	    fields.push({
		"data":"groupMembers",
		"title":"组员",
	});
	
	    fields.push({
		"data":"no",
		"title":"偏差编号",
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "DeviationHandlingReport"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});

	var options = table(true, "","/deviation/deviationHandlingReport/showDeviationHandlingReportTableJson.action",
	 fields, null)
	deviationHandlingReportTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(deviationHandlingReportTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/deviation/deviationHandlingReport/editDeviationHandlingReport.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/deviationHandlingReport/editDeviationHandlingReport.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/deviationHandlingReport/viewDeviationHandlingReport.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":"编号"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":"偏差描述"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":"创建人"
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":"创建日期"
		});
	fields.push({
			"txt": "创建时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":"状态"
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":"状态名称"
		});
	   fields.push({
		    "searchName":"happenDepartment",
			"type":"input",
			"txt":"发生部门"
		});
	   fields.push({
		    "searchName":"happenDate",
			"type":"input",
			"txt":"发生时间"
		});
	fields.push({
			"txt": "发生时间(Start)",
			"type": "dataTime",
			"searchName": "happenDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "发生时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "happenDate##@@##2"
		});
	   fields.push({
		    "searchName":"happenAddress",
			"type":"input",
			"txt":"发生地点"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"discoverer.id",
		"txt":"发现人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"discoverer.name",
		"txt":"发现人"
	});
//	   fields.push({
//		    "searchName":"correctiveActionId",
//			"type":"input",
//			"txt":""
//		});
//	   fields.push({
//		    "searchName":"correctiveActionName",
//			"type":"input",
//			"txt":""
//		});
//	   fields.push({
//		    "searchName":"kindId",
//			"type":"input",
//			"txt":""
//		});
//	   fields.push({
//		    "searchName":"kindName",
//			"type":"input",
//			"txt":""
//		});
/*	fields.push({
	    "type":"input",
		"searchName":"reportUser.id",
		"txt":"偏差报告人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"reportUser.name",
		"txt":"报告人"
	});
	   fields.push({
		    "searchName":"reportDate",
			"type":"input",
			"txt":"报告日期"
		});
	fields.push({
			"txt": "报告日期(Start)",
			"type": "dataTime",
			"searchName": "reportDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "报告日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "reportDate##@@##2"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"departmentUser.id",
		"txt":"部门负责人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"departmentUser.name",
		"txt":"部门负责人"
	});
	   fields.push({
		    "searchName":"departmentDate",
			"type":"input",
			"txt":"负责日期"
		});
	fields.push({
			"txt": "负责日期(Start)",
			"type": "dataTime",
			"searchName": "departmentDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "负责日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "departmentDate##@@##2"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"monitor.id",
		"txt":"监控人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"monitor.name",
		"txt":"监控人"
	});
	   fields.push({
		    "searchName":"monitoringDate",
			"type":"input",
			"txt":"监控日期"
		});
	fields.push({
			"txt": "监控日期(Start)",
			"type": "dataTime",
			"searchName": "monitoringDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "监控日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "monitoringDate##@@##2"
		});
	   fields.push({
		    "searchName":"influence",
			"type":"input",
			"txt":"偏差影响"
		});
	   fields.push({
		    "searchName":"type",
			"type":"input",
			"txt":"偏差分类"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"groupLeader.id",
		"txt":"调查组长ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"groupLeader.name",
		"txt":"调查组长"
	});
	   fields.push({
		    "searchName":"groupMembers",
			"type":"input",
			"txt":"组员"
		});
	   fields.push({
		    "searchName":"no",
			"type":"input",
			"txt":"偏差编号"
		});
	
	fields.push({
		"type":"table",
		"table":deviationHandlingReportTable
	});
	return fields;
}
