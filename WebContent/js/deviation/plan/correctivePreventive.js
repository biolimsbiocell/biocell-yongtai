var correctivePreventiveTable;
var oldcorrectivePreventiveChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#deviationSurveyPlan_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "编号",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
	});
	   colOpts.push({
		"data":"number",
		"title": "序号",
		"createdCell": function(td) {
			$(td).attr("saveName", "number");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"excuteWork",
		"title": "执行工作",
		"createdCell": function(td) {
			$(td).attr("saveName", "excuteWork");
	    },
		"className": "edit"
	});
	   colOpts.push({
		   "data":"completeDate",
		   "title": "完成期限",
		   "className":"date",
		   "createdCell": function(td) {
			   $(td).attr("saveName", "completeDate");
		   },
	   });
	   colOpts.push({
		"data":"chargeUser-id",
		"title": "责任人ID",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "chargeUser-id");
	    },
	});
	   colOpts.push({
		   "data":"chargeUser-name",
		   "title": "责任人",
		   "createdCell": function(td ,data, rowData) {
			   $(td).attr("saveName", "chargeUser-name");
			   $(td).attr("chargeUser-id", rowData['chargeUser-id']);
		   },
	   });
	   colOpts.push({ 
			"title": "上传附件",
			"width": "100px",
			"data": null,
			"createdCell": function(td, data) {},
			"render": function() { 
				return '<input type="button" value="上传附件" onClick="javascript:fileUp1(this);" >' +
					'<input type="button" value="查看附件" onClick="javascript:fileView1(this);">'

			},

		});
	  
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#correctivePreventiveTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveCorrectivePreventive($("#correctivePreventiveTable"));
		}
	});
	tbarOpts.push({
		text: "选择责任人",
		action: function() {
			showUser();
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#correctivePreventiveTable"))
		}
	});
	}
	
	var correctivePreventiveOptions = table(true,
		id,
		'/deviation/plan/deviationSurveyPlan/showCorrectivePreventiveTableJson.action', colOpts, tbarOpts)
	correctivePreventiveTable = renderData($("#correctivePreventiveTable"), correctivePreventiveOptions);
	correctivePreventiveTable.on('draw', function() {
		oldcorrectivePreventiveChangeLog = correctivePreventiveTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});
//报告上传
function fileUp1(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	if(id==""){
		top.layer.msg("请先保存数据，再上传附件！");
		return;
	}
	$("#uploadFileValx").fileinput({
		uploadUrl: ctx + "/attachmentUpload?useType=1&modelType=correctivePreventive&contentId=" + id,
		uploadAsync: true,
		language: 'zh',
		showPreview: true,
		enctype: 'multipart/form-data',
		elErrorContainer: '#kartik-file-errors',
	});
	$("#uploadFilex").modal("show");
}
//报告查看
function fileView1(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: ["650px", "500px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=correctivePreventive&id=" + id,
		cancel: function(index, layero) {
			top.layer.close(index)
		}
	})
}

function showUser() {
	
	var rows = $("#correctivePreventiveTable .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			rows.addClass("editagain");
			top.layer.close(index);
			rows.find("td[savename='chargeUser-name']").attr("chargeUser-id",id);
			rows.find("td[savename='chargeUser-name']").text(name);
		}
	})
}

// 保存
function saveCorrectivePreventive(ele) {
	var data = saveCorrectivePreventivejson(ele);
	var ele=$("#correctivePreventiveTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog1(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/deviation/plan/deviationSurveyPlan/saveCorrectivePreventiveTable.action',
		data: {
			id: $("#deviationSurveyPlan_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				correctivePreventiveTable.ajax.reload();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveCorrectivePreventivejson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			if(k == "chargeUser-name") {
				json["chargeUser-id"] = $(tds[j]).attr("chargeUser-id");
				continue;
			}
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog1(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldcorrectivePreventiveChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
