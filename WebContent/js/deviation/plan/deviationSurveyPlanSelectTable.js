var deviationSurveyPlanTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.deviationSurveyPlan.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.deviationSurveyPlan.name,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":"创建人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.deviationSurveyPlan.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.deviationSurveyPlan.createDate,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.deviationSurveyPlan.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.deviationSurveyPlan.stateName,
	});
	
	    fields.push({
		"data":"no",
		"title":biolims.deviationSurveyPlan.no,
	});
	
	    fields.push({
		"data":"serialNumber",
		"title":biolims.deviationSurveyPlan.serialNumber,
	});
	
	    fields.push({
		"data":"reason",
		"title":biolims.deviationSurveyPlan.reason,
	});
	
	    fields.push({
		"data":"groupMembers",
		"title":biolims.deviationSurveyPlan.groupMembers,
	});
	
	    fields.push({
		"data":"groupLeader-id",
		"title":"组长ID"
	});
	    fields.push({
		"data":"groupLeader-name",
		"title":biolims.deviationSurveyPlan.groupLeader
	});
	
	    fields.push({
		"data":"departmentUser-id",
		"title":"部门负责人ID"
	});
	    fields.push({
		"data":"departmentUser-name",
		"title":biolims.deviationSurveyPlan.departmentUser
	});
	
	    fields.push({
		"data":"auditOpinion",
		"title":biolims.deviationSurveyPlan.auditOpinion,
	});
	
	    fields.push({
		"data":"auditDate",
		"title":biolims.deviationSurveyPlan.auditDate,
	});
	
	    fields.push({
		"data":"auditUser-id",
		"title":"审核人ID"
	});
	    fields.push({
		"data":"auditUser-name",
		"title":biolims.deviationSurveyPlan.auditUser
	});
	
	    fields.push({
		"data":"approvalOpinion",
		"title":biolims.deviationSurveyPlan.approvalOpinion,
	});
	
	    fields.push({
		"data":"approvalUser-id",
		"title":"批准人ID"
	});
	    fields.push({
		"data":"approvalUser-name",
		"title":biolims.deviationSurveyPlan.approvalUser
	});
	
	    fields.push({
		"data":"approvalDate",
		"title":biolims.deviationSurveyPlan.approvalDate,
	});
	
	var options = table(true, "","/deviation/plan/deviationSurveyPlan/showDeviationSurveyPlanTableJson.action",
	 fields, null)
	deviationSurveyPlanTable = renderData($("#addDeviationSurveyPlanTable"), options);
	$('#addDeviationSurveyPlanTable').on('init.dt', function() {
		recoverSearchContent(deviationSurveyPlanTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.name
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.deviationSurveyPlan.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.createDate
		});
	fields.push({
			"txt": "创建时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.stateName
		});
	   fields.push({
		    "searchName":"no",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.no
		});
	   fields.push({
		    "searchName":"serialNumber",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.serialNumber
		});
	   fields.push({
		    "searchName":"reason",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.reason
		});
	   fields.push({
		    "searchName":"groupMembers",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.groupMembers
		});
	fields.push({
	    "type":"input",
		"searchName":"groupLeader.id",
		"txt":"组长ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"groupLeader.name",
		"txt":biolims.deviationSurveyPlan.groupLeader
	});
	fields.push({
	    "type":"input",
		"searchName":"departmentUser.id",
		"txt":"部门负责人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"departmentUser.name",
		"txt":biolims.deviationSurveyPlan.departmentUser
	});
	   fields.push({
		    "searchName":"auditOpinion",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.auditOpinion
		});
	   fields.push({
		    "searchName":"auditDate",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.auditDate
		});
	fields.push({
			"txt": "审核日期(Start)",
			"type": "dataTime",
			"searchName": "auditDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "审核日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "auditDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"auditUser.id",
		"txt":"审核人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"auditUser.name",
		"txt":biolims.deviationSurveyPlan.auditUser
	});
	   fields.push({
		    "searchName":"approvalOpinion",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.approvalOpinion
		});
	fields.push({
	    "type":"input",
		"searchName":"approvalUser.id",
		"txt":"批准人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"approvalUser.name",
		"txt":biolims.deviationSurveyPlan.approvalUser
	});
	   fields.push({
		    "searchName":"approvalDate",
			"type":"input",
			"txt":biolims.deviationSurveyPlan.approvalDate
		});
	fields.push({
			"txt": "批准日期(Start)",
			"type": "dataTime",
			"searchName": "approvalDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "批准日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "approvalDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":deviationSurveyPlanTable
	});
	return fields;
}
