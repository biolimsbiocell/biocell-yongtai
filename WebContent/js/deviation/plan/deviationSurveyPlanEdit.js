﻿
$(function() {
	
	$(".datePicker").datepicker({
		language: "zh-CN",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	var mainFileInput=fileInput ('1','deviationSurveyPlan',$("#deviationSurveyPlan_id").val());
//	fieldCustomFun();
	
	stepViewChange();
	
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });
	
})	

//部门负责人
function showdepartmentUser() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=DP", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index);
			$("#deviationSurveyPlan_departmentUser_id").val(id);
			$("#deviationSurveyPlan_departmentUser_name").val(name);
		},
	})
}

//选择偏差
function selectPC() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/deviation/plan/deviationSurveyPlan/selectDeviationHandlingReportTable.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(2).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
					3).text();
			var ms = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
					1).text();
			top.layer.close(index);
			$("#deviationSurveyPlan_deviationHr_id").val(id);
			$("#deviationSurveyPlan_deviationHr_no").val(name);
			$("#deviationSurveyPlan_deviationHr_name").val(ms);
			if(type=="微小偏差"){
				$("#deviationSurveyPlan_deviationHr_type").val("1");
			} else if(type=="一般偏差"){
				$("#deviationSurveyPlan_deviationHr_type").val("2");
			} else if(type=="重大偏差"){
				$("#deviationSurveyPlan_deviationHr_type").val("3");
			} else {
				$("#deviationSurveyPlan_deviationHr_type").val("");
			}
		},
	})
}
//组长
function showgroupLeader() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			top.layer.close(index);
			$("#deviationSurveyPlan_groupLeader_id").val(id);
			$("#deviationSurveyPlan_groupLeader_name").val(name);
		},
	})
}

//审核人
function showauditUser() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=QA003", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			top.layer.close(index);
			$("#deviationSurveyPlan_auditUser_id").val(id);
			$("#deviationSurveyPlan_auditUser_name").val(name);
		},
	})
}

//审核人
function showauditUser1() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=QA005", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			top.layer.close(index);
			$("#deviationSurveyPlan_auditQaUser_id").val(id);
			$("#deviationSurveyPlan_auditQaUser_name").val(name);
		},
	})
}

//产品质量负责人
function showapprovalUser() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=QC003", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			top.layer.close(index);
			$("#deviationSurveyPlan_approvalUser_id").val(id);
			$("#deviationSurveyPlan_approvalUser_name").val(name);
		},
	})
}

//批准人
function showApproverUser() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=QA004", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			top.layer.close(index);
			$("#deviationSurveyPlan_approverUser_id").val(id);
			$("#deviationSurveyPlan_approverUser_name").val(name);
		},
	})
}

//组员
function showGroupMembers() {
	$("#form1").data("changed",true);  
	var names = [];
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUsersTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .selected").each(function(i, v){
				var name = $(v).children("td").eq(2).text();
				names.push(name);
			});
			
			top.layer.close(index);
			$("#deviationSurveyPlan_groupMembers").val(names.join(","));
		},
	})
}
//选择小组
function showUserGroup(){
	top.layer.open({
		title: "选择人员组",
		type: 2,
		area: [document.body.clientWidth - 300,document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx +"/deviation/plan/deviationSurveyPlan/selUserGroupList.action",''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			$("#deviationSurveyPlan_userGroup_id").val(id);
			$("#deviationSurveyPlan_userGroup_name").val(name);
			top.layer.close(index)	
		},
	});
}
function change(id) {
	$("#" + id).css({
		"background-color" : "white",
		"color" : "black"
	});
}
function add() {
	window.location = window.ctx + "/deviation/plan/deviationSurveyPlan/editDeviationSurveyPlan.action";
}

function list() {
	window.location = window.ctx + '/deviation/plan/deviationSurveyPlan/showDeviationSurveyPlanList.action';
}

function tjsp() {
	 if($("#form1").data("changed") ||$("#deviationSurveyPlan_id").val()=="NEW"){
		    top.layer.msg("请保存后提交!");
		    return false;
		   }
	 
	 
		if($("#deviationSurveyPlan_departmentUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
			top.layer.msg(("创建人与部门负责人相同请重新选择"));
			return false;
		}
		
		if($("#deviationSurveyPlan_auditUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
			top.layer.msg(("创建人与QA负责人相同请重新选择"));
			return false;
		}
		
		if($("#deviationSurveyPlan_approvalUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
			top.layer.msg(("创建人与产品质量负责人相同请重新选择"));
			return false;
		}
		
		if($("#deviationSurveyPlan_auditQaUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
			top.layer.msg(("创建人与QA相同请重新选择"));
			return false;
		}
	
			top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {icon: 3, title:biolims.common.prompt}, function(index){
				top.layer.open({
					  title: biolims.common.submit,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toStartView.action?formName=DeviationSurveyPlan",
					  yes: function(index, layero) {
						 var datas={
									userId : userId,
									userName : userName,
									formId : $("#deviationSurveyPlan_id").val(),
									title : $("#deviationSurveyPlan_name").val(),
									formName : 'DeviationSurveyPlan'
								}
							ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
								if (data.success) {
									top.layer.msg(biolims.common.submitSuccess);
									if (typeof callback == 'function') {
										callback(data);
									}
//									dialogWin.dialog("close");
									window.open(window.ctx
									+ "/main/toPortal.action",
									'_parent');
									top.layer.close(index);
								} else {
									top.layer.msg(biolims.common.submitFail);
								}
							}, null);
						 	top.layer.close(index);
						},
						cancel: function(index, layero) {
							top.layer.close(index);
						}
				
				});     
				top.layer.close(index);
				});
}
function sp(){
	
		var taskId = $("#bpmTaskId").val();
		var formId = $("#deviationSurveyPlan_id").val();
		
					top.layer.open({
						  title: biolims.common.handle,
						  type:2,
						  anim: 2,
						  area: ['800px','500px']
						  ,btn: biolims.common.selected,
						  content: window.ctx+"/workflow/processinstance/toCompleteTaskView.action?taskId="+taskId+"&formId="+formId,
						  yes: function(index, layero) {
							  var operVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
							  var opinionVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();
							  var opinion = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinion").val();
								if(!operVal){
									top.layer.msg(biolims.common.pleaseSelectOper);
									return false;
								}
								if (operVal == "2") {
									_trunTodoTask(taskId, callback, dialogWin);
								} else {
									var paramData = {};
									paramData.oper = operVal;
									paramData.info = opinion;
					
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									}
									ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
										if (data.success) {
											top.layer.msg(biolims.common.submitSuccess);
											top.layer.closeAll();
											if (typeof callback == 'function') {
											}
											location.href =window.ctx+"/lims/pages/dashboard/dashboard.jsp";
										} else {
											top.layer.msg(biolims.common.submitFail);
										}
									}, null);
								}
							},
							cancel: function(index, layero) {
								top.layer.closeAll();
							}
					
					});     
}

function ck(){
			top.layer.open({
					  title: biolims.common.checkFlowChart,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toTraceProcessInstanceView.action?formId=" + $("#deviationSurveyPlan_id").val(),
					  yes: function(index, layero) {
						  top.layer.close(index)
					   },
						cancel: function(index, layero) {
							top.layer.close(index)
						}
				});     
}

function save() {
if(checkSubmit()==true){
	
	if($("#deviationSurveyPlan_departmentUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
		top.layer.msg(("创建人与部门负责人相同请重新选择"));
		return false;
	}
	
	if($("#deviationSurveyPlan_auditUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
		top.layer.msg(("创建人与QA负责人相同请重新选择"));
		return false;
	}
	
	if($("#deviationSurveyPlan_approvalUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
		top.layer.msg(("创建人与产品质量负责人相同请重新选择"));
		return false;
	}
	
	if($("#deviationSurveyPlan_auditQaUser_name").val()==$("#deviationSurveyPlan_createUser_name").val()){
		top.layer.msg(("创建人与QA相同请重新选择"));
		return false;
	}

		//自定义字段
		//拼自定义字段（实验记录）
		var inputs = $("#fieldItemDiv input");
		var options = $("#fieldItemDiv option");
		var contentData = {};
		var checkboxArr = [];
		$("#fieldItemDiv .checkboxs").each(function(i, v) {
			$(v).find("input").each(function(ii, inp) {
				var k = inp.name;
				if(inp.checked == true) {
					checkboxArr.push(inp.value);
					contentData[k] = checkboxArr;
				}
			});
		});
		inputs.each(function(i, inp) {
			var k = inp.name;
			if(inp.type != "checkbox") {
				contentData[k] = inp.value;
			}
		});
		options.each(function(i, opt) {
			if(opt.selected == true) {
				var k = opt.getAttribute("name");
				contentData[k] = opt.value;
			}
		});
		document.getElementById("fieldContent").value = JSON.stringify(contentData);
		
		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		var jsonStr = JSON.stringify($("#form1").serializeObject());  
		$.ajax({
			url: ctx + '/deviation/plan/deviationSurveyPlan/save.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				
				correctivePreventiveJson : saveCorrectivePreventive($("#correctivePreventiveTable")),
				
//				planInfluenceProductJson : savePlanInfluenceProductjson($("#planInfluenceProductTable")),
//				
//				planInfluenceMaterielJson : savePlanInfluenceMaterieljson($("#planInfluenceMaterielTable")),
//				
//				planInfluenceEquipmentJson : savePlanInfluenceEquipmentjson($("#planInfluenceEquipmentTable")),
				
				bpmTaskId : $("#bpmTaskId").val()
			},
			success: function(data) {
				//关闭加载层
				//layer.close(index);
				
				if(data.success) {
					var url = "/deviation/plan/deviationSurveyPlan/editDeviationSurveyPlan.action?id="+data.id;
					
					if(data.bpmTaskId){
						url = url +"&bpmTaskId="+data.bpmTaskId;
					}
					
					window.location.href=url;
					
					
				}else{
					top.layer.msg(biolims.common.saveFailed);
				
				}
				}
			
		});
		
	}
}		

$.fn.serializeObject = function() {
	   var o = {};  
	    var a = this.serializeArray();  
	    $.each(a, function() {  
	        if (o[this.name]) {  
	            if (!o[this.name].push) {  
	                o[this.name] = [ o[this.name] ];  
	            }  
	            o[this.name].push(this.value || '');  
	        } else {  
	            o[this.name] = this.value || '';  
	        }  
	    });  
	    return o;  
};



function editCopy() {
	window.location = window.ctx + '/deviation/plan/deviationSurveyPlan/copyDeviationSurveyPlan.action?id=' + $("#deviationSurveyPlan_id").val();
}
function changeState() {
	var paraStr="formId=" + $("#deviationSurveyPlan_id").val()
	+ "&tableId=DeviationSurveyPlan";
	layer.open({
		  title: biolims.common.changeState,
		  type:2,
		  anim: 2,
		  area: ['400px','400px']
		  ,btn: biolims.common.selected,
		  content: window.ctx
			+ "/applicationTypeAction/applicationTypeActionLook.action?" + paraStr
			+ "&flag=changeState'",
		  yes: function(index, layero) {
			  layer.confirm(biolims.common.toSubmit, {icon: 3, title:biolims.common.prompt}, function(index){
				 ajax("post","/applicationTypeAction/exeFun.action",{
					 applicationTypeActionId:deviationSurveyPlan,
					 formId:$("#deviationSurveyPlan_id").val()
				 },function(response){
					 var respText = response.message;
					 if (respText == '') {
							window.location.reload();
						} else {
							layer.msg(respText);
						}
				 },null)
				 layer.close(index);
			 })
			},
			cancel: function(index, layero) {
				layer.close(index)
			}
	
	});   
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#deviationSurveyPlan_id").val());
	nsc.push("id"+biolims.common.describeEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			layer.msg(mess);
			return false;
		}
		return true;
}
function fileUp(){
	$("#uploadFile").modal("show");
}
function fileView(){
	top.layer.open({
		title:biolims.common.attachment,
		type:2,
		skin: 'layui-layer-lan',
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		content:window.ctx+"/operfile/initFileList.action?flag=1&modelType=deviationSurveyPlan&id="+$("#deviationSurveyPlan_id").val(),
		cancel: function(index, layero) {
			top.layer.close(index)
		}
	})
}
function settextreadonly() {
    jQuery(":text, textarea").each(function() {
	var _vId = jQuery(this).attr('id');
	jQuery(this).css("background-color","#B4BAB5").attr("readonly", "readOnly");
	if (_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}
function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}


//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

		//查找自定义列表的订单模块的此检测项目的相关内容
		$.ajax({
			type: "post",
			url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
			data: {
				moduleValue: "DeviationSurveyPlan"
			},
			async: false,
			success: function(data) {
				var objValue = JSON.parse(data);
				if(objValue.success) {
					$.each(objValue.data, function(i, n) {
						var inputs = '';
						var disabled = n.readOnly ? ' ' : "disabled";
						var defaultValue = n.defaultValue ? n.defaultValue : ' ';
						if(n.fieldType == "checkbox") {
							var checkboxs = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
						} else if(n.fieldType == "radio") {
							var options = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
						} else if(n.fieldType == "date") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + ' required=' + n.isRequired + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}

						$("#fieldItemDiv").append(inputs);
					});

				} else {
					layer.msg("自定义列表更新失败！");
				}
			}
		});
	

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}
