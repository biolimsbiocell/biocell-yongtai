var deviationSurveyPlanTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":"编号",
	});
	
	    fields.push({
		"data":"name",
		"title":"描述",
	});
	
	    fields.push({
		"data":"createUser-id",
		"visible":false,
		"title":"创建人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":"创建人"
	});
	
	    fields.push({
		"data":"createDate",
		"title":"创建时间",
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	
	    fields.push({
		"data":"state",
		"visible":false,
		"title":"状态码",
	});
	
	    fields.push({
		"data":"stateName",
		"title":"状态名称",
	});
	
	    fields.push({
		"data":"deviationHr-no",
		"title":"偏差编号",
	});
	    
    fields.push({
		"data":"deviationHr-name",
		"title":"偏差描述",
	});
    
    
    fields.push({
		"data" : "deviationHr-type",
		"title" : "偏差分类",
//		"className" : "select",
		"name" : "|微小偏差|一般偏差|重大偏差",
		"createdCell" : function(td) {
			$(td).attr("saveName", "deviationHr-type");
			$(td).attr("selectOpt" , "|微小偏差|一般偏差|重大偏差");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "微小偏差";
			}
			if (data == "2") {
				return "一般偏差";
			}
			if (data == "3") {
				return "重大偏差";
			} else {
				return '';
			}
		}
	})
	
//	    fields.push({
//		"data":"serialNumber",
//		"title":"序列号",
//	});
//	
//	    fields.push({
//		"data":"reason",
//		"title":"原因调查",
//	});
	
//	    fields.push({
//		"data":"groupMembers",
//		"title":"组员",
//	});
//	
//	    fields.push({
//		"data":"groupLeader-id",
//		"visible":false,
//		"title":"组长ID"
//	});
//	    fields.push({
//		"data":"groupLeader-name",
//		"title":"组长"
//	});
	
	    fields.push({
		"data":"departmentUser-id",
		"visible":false,
		"title":"部门负责人ID"
	});
	    fields.push({
		"data":"departmentUser-name",
		"title":"部门负责人"
	});
	
	
	    fields.push({
		"data":"auditUser-id",
		"visible":false,
		"title":"审核人ID"
	});
	    fields.push({
		"data":"auditUser-name",
		"title":"QA负责人"
	});
	
	   
	    fields.push({
			"data":"auditOpinion",
			"title":"QA审批意见",
		});
		
		    fields.push({
			"data":"auditDate",
			"title":"QA审批日期",
			"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
		});
	
	    fields.push({
		"data":"approvalUser-id",
		"visible":false,
		"title":"批准人ID"
	});
	    fields.push({
		"data":"approvalUser-name",
		"title":"产品质量负责人"
	});
	    fields.push({
			"data":"approvalOpinion",
			"title":"产品质量负责人审批意见",
		});
	    fields.push({
		"data":"approvalDate",
		"title":"产品质量负责人审批日期",
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "DeviationSurveyPlan"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});

	var options = table(true, "","/deviation/plan/deviationSurveyPlan/showDeviationSurveyPlanTableJson.action",
	 fields, null)
	deviationSurveyPlanTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(deviationSurveyPlanTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/deviation/plan/deviationSurveyPlan/editDeviationSurveyPlan.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/plan/deviationSurveyPlan/editDeviationSurveyPlan.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/plan/deviationSurveyPlan/viewDeviationSurveyPlan.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":"编号"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":"描述"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":"创建人"
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":"创建日期"
		});
	fields.push({
			"txt": "创建时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
/*	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":"状态"
		});*/
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":"状态名称"
		});
//	   fields.push({
//		    "searchName":"no",
//			"type":"input",
//			"txt":"编号"
//		});
	   fields.push({
		    "searchName":"deviationHr.no",
			"type":"input",
			"txt":"偏差编号"
		});
	   fields.push({
		    "searchName":"deviationHr.name",
			"type":"input",
			"txt":"偏差描述"
		});
//	   fields.push({
//		    "searchName":"groupMembers",
//			"type":"input",
//			"txt":""
//		});
/*	fields.push({
	    "type":"input",
		"searchName":"groupLeader.id",
		"txt":"组长ID"
	});*/
/*	fields.push({
	    "type":"input",
		"searchName":"groupLeader.name",
		"txt":"组长"
	});*/
/*	fields.push({
	    "type":"input",
		"searchName":"departmentUser.id",
		"txt":"部门负责人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"departmentUser.name",
		"txt":"部门负责人"
	});
	   fields.push({
		    "searchName":"auditOpinion",
			"type":"input",
			"txt":"QA审批意见"
		});
	   fields.push({
		    "searchName":"auditDate",
			"type":"input",
			"txt":"QA审批日期"
		});
	fields.push({
			"txt": "QA审批日期(Start)",
			"type": "dataTime",
			"searchName": "auditDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "QA审批日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "auditDate##@@##2"
		});
/*	fields.push({
	    "type":"input",
		"searchName":"auditUser.id",
		"txt":"审核人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"auditUser.name",
		"txt":"QA负责人"
	});
//	   fields.push({
//		    "searchName":"approvalOpinion",
//			"type":"input",
//			"txt":""
//		});
/*	fields.push({
	    "type":"input",
		"searchName":"approvalUser.id",
		"txt":"批准人ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"approvalUser.name",
		"txt":"产品质量负责人"
	});
	   fields.push({
		    "searchName":"approvalDate",
			"type":"input",
			"txt":"产品质量负责人审批日期"
		});
	fields.push({
			"txt": "产品质量负责人审批日期(Start)",
			"type": "dataTime",
			"searchName": "approvalDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "产品质量负责人审批日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "approvalDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":deviationSurveyPlanTable
	});
	return fields;
}
