$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.user.eduName1,
	});
	var tbarOpts = [];
	var addDicTypeOptions = table(false, null,
			'/deviation/plan/deviationSurveyPlan/selUserGroupTableJson.action',
			colOpts, tbarOpts)//传值
	var DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
	$("#addDicTypeTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
			DicTypeTable.on('draw', function() {
				trs = $("#addDicTypeTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})