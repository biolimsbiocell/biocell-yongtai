var planInfluenceMaterielTable;
var oldplanInfluenceMaterielChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
//	var id = $("#deviationSurveyPlan_id").val();
	var id = $("#deviationSurveyPlan_deviationHr_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "编码",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
	});
	   colOpts.push({
		"data":"name",
		"title": "描述",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "edit"
	   });
	   colOpts.push({
		   "data":"materielId",
		   "title": "物料编号",
		   "createdCell": function(td) {
			   $(td).attr("saveName", "materielId");
		   },
	   });
	   colOpts.push({
		   "data":"materielName",
		   "title": "物料名称",
		   "createdCell": function(td) {
			   $(td).attr("saveName", "materielName");
		   },
	   });
	   colOpts.push({
		   "data":"batchNumber",
		   "title": "批号",
		   "createdCell": function(td) {
			   $(td).attr("saveName", "batchNumber");
		   },
	   });
	   colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
//	   colOpts.push({
//		"data":"other",
//		"title": "其他",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "other");
//	    },
//		"className": "edit"
//	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
//	tbarOpts.push({
//		text: biolims.common.fillDetail,
//		action: function() {
//			addItem($("#planInfluenceMaterielTable"))
//	}
//	});
//	
//	tbarOpts.push({
//		text: biolims.common.save,
//		action: function() {
//			savePlanInfluenceMateriel($("#planInfluenceMaterielTable"));
//		}
//	});
//	
//	tbarOpts.push({
//		text: "选择物料",
//		action: function() {
//			selectMateriel();
//		}
//	});
//	tbarOpts.push({
//		text: "选择批次",
//		action: function() {
//			selectMaterielpc();
//		}
//	});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#planInfluenceMaterielTable"))
			}
		});
	}
	
	var planInfluenceMaterielOptions = table(true,
		id,
		'/deviation/plan/deviationSurveyPlan/showPlanInfluenceMaterielTableJson.action', colOpts, tbarOpts)
	planInfluenceMaterielTable = renderData($("#planInfluenceMaterielTable"), planInfluenceMaterielOptions);
	planInfluenceMaterielTable.on('draw', function() {
		oldplanInfluenceMaterielChangeLog = planInfluenceMaterielTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});
//选择物料
function selectMateriel(){
	var rows = $("#planInfluenceMaterielTable .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/getStorage.action", ''],
		yes: function(index, layer) {
			rows.addClass("editagain");
			var productname = $('.layui-layer-iframe', parent.document).find("iframe").
			contents().find("#addStorage .chosed").children("td").eq(1).text();
			var code = $('.layui-layer-iframe', parent.document).find("iframe").
			contents().find("#addStorage .chosed").children("td").eq(0).text();
			rows.find("td[savename='materielName']").text(productname);
			rows.find("td[savename='materielId']").text(code);
			top.layer.close(index);
		},
	})
//	top.layer.open({
//		title: "请选择",
//		type: 2,
//		area: ["650px", "400px"],
//		btn: biolims.common.selected,
//		content: [window.ctx + "/storage/showStorageDialogList.action", ''],
//		yes: function(index, layer) {
//			rows.addClass("editagain");
//			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().
//				find("#addStorage .chosed").children("td").eq(0).text();
//			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().
//				find("#addStorage .chosed").children("td").eq(1).text();
//			rows.find("td[savename='materielId']").text(id);
//			rows.find("td[savename='materielName']").text(name);
//			top.layer.close(index);
//			top.layer.open({
//				title: biolims.common.batch,
//				type: 2,
//				area: ["650px", "400px"],
//				btn: biolims.common.selected,
//				content: [window.ctx + "/storage/selStorageBatch.action?id="+id, ''],
//				yes: function(index, layer) {
//					var batchNumbers = [];
//					$('.layui-layer-iframe', parent.document).find("iframe").contents().
//						find("#addStorageBatchTable .selected").each(function(i ,v){
//							batchNumbers.push($(v).children("td").eq(2).text());
//					})
//					rows.find("td[savename='batchNumber']").text(batchNumbers.join(","));
//					top.layer.close(index);
//				},
//			})
//		}
//	})
}
//选择物料
function selectMaterielpc(){
	var rows = $("#planInfluenceMaterielTable .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
	rows.each(function(i, v) {
		infoid = $(v).find("td[savename='materielId']").text();
	})
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		offset: ['10%', '10%'],
		area: [document.body.clientWidth - 300,
			document.body.clientHeight - 100
		],
		btn: biolims.common.selected,
		content: [
			window.ctx + "/experiment/qaAudit/qaAudit/getStrogeReagent.action?id=" + infoid + "",
			""
		],
		yes: function(index, layero) {
			rows.addClass("editagain");
			var batch = $(".layui-layer-iframe", parent.document)
				.find("iframe").contents().find("#addReagent .chosed")
				.children("td").eq(2).text();
			rows.find("td[savename='batchNumber']").text(batch);
			top.layer.close(index);
		},
	});
}

// 保存
function savePlanInfluenceMateriel(ele) {
	var data = savePlanInfluenceMaterieljson(ele);
	var ele=$("#planInfluenceMaterielTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog3(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/deviation/plan/deviationSurveyPlan/savePlanInfluenceMaterielTable.action',
		data: {
			id: $("#deviationSurveyPlan_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				planInfluenceMaterielTable.ajax.reload();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function savePlanInfluenceMaterieljson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "deviationSurveyPlan-name") {
				json["deviationSurveyPlan-id"] = $(tds[j]).attr("deviationSurveyPlan-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog3(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldplanInfluenceMaterielChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
