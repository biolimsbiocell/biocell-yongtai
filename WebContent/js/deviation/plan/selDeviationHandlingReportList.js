$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "编号",
	});
	colOpts.push({
		"data" : "name",
		"title" : "偏差描述",
	});
	colOpts.push({
		"data" : "no",
		"title" : "偏差编号",
	});
	colOpts.push({
		"data" : "type",
		"title" : "偏差分类",
//		"className" : "select",
		"name" : "|微小偏差|一般偏差|重大偏差",
		"createdCell" : function(td) {
			$(td).attr("saveName", "type");
			$(td).attr("selectOpt","|微小偏差|一般偏差|重大偏差");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "微小偏差";
			}
			if (data == "2") {
				return "一般偏差";
			}
			if (data == "3") {
				return "重大偏差";
			} else {
				return '';
			}
		}
	});
	var tbarOpts = [];
	var addDicTypeOptions = table(false, null,
			'/deviation/plan/deviationSurveyPlan/selectDeviationHandlingReportTableJson.action',
			colOpts, tbarOpts);//传值
	var DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
	$("#addDicTypeTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
			DicTypeTable.on('draw', function() {
				trs = $("#addDicTypeTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
});