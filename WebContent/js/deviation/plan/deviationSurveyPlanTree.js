$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : "id",
		field : "id"
	});
	
	   
	fields.push({
		title : "描述",
		field : "name"
	});
	
	  fields.push({
		title : "创建人",
		field : "createUser.name"
	});
	   
	fields.push({
		title : "创建时间",
		field : "createDate"
	});
	
	   
	fields.push({
		title : "状态",
		field : "state"
	});
	
	   
	fields.push({
		title : "状态名称",
		field : "stateName"
	});
	
	   
	fields.push({
		title : "偏差编号",
		field : "no"
	});
	
	   
	fields.push({
		title : "序列号",
		field : "serialNumber"
	});
	
	   
	fields.push({
		title : "原因调查",
		field : "reason"
	});
	
	   
	fields.push({
		title : "组员",
		field : "groupMembers"
	});
	
	  fields.push({
		title : "组长",
		field : "groupLeader.name"
	});
	  fields.push({
		title : "部门负责人",
		field : "departmentUser.name"
	});
	   
	fields.push({
		title : "审核意见",
		field : "auditOpinion"
	});
	
	   
	fields.push({
		title : "审核日期",
		field : "auditDate"
	});
	
	  fields.push({
		title : "审核人",
		field : "auditUser.name"
	});
	   
	fields.push({
		title : "批准意见",
		field : "approvalOpinion"
	});
	
	  fields.push({
		title : "批准人",
		field : "approvalUser.name"
	});
	   
	fields.push({
		title : "批准日期",
		field : "approvalDate"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/deviation/plan/deviationSurveyPlan/showDeviationSurveyPlanListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/deviation/plan/deviationSurveyPlan/editDeviationSurveyPlan.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/plan/deviationSurveyPlan/editDeviationSurveyPlan.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/deviation/plan/deviationSurveyPlan/viewDeviationSurveyPlan.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : "id",
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "描述",
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : '创建人',
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "创建时间",
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态",
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态名称",
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "偏差编号",
		"searchName" : 'no',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "序列号",
		"searchName" : 'serialNumber',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "原因调查",
		"searchName" : 'reason',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "组员",
		"searchName" : 'groupMembers',
		"type" : "input"
	});
	
	   fields.push({
		header : '组长',
		"searchName" : 'groupLeader.name',
		"type" : "input"
	});
	   fields.push({
		header : '部门负责人',
		"searchName" : 'departmentUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "审核意见",
		"searchName" : 'auditOpinion',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "审核日期",
		"searchName" : 'auditDate',
		"type" : "input"
	});
	
	   fields.push({
		header : '审核人',
		"searchName" : 'auditUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "批准意见",
		"searchName" : 'approvalOpinion',
		"type" : "input"
	});
	
	   fields.push({
		header : '批准人',
		"searchName" : 'approvalUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "批准日期",
		"searchName" : 'approvalDate',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

