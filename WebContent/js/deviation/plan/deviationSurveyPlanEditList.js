var deviationSurveyPlanTable;
var olddeviationSurveyPlanChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.deviationSurveyPlan.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.deviationSurveyPlan.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "创建人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.deviationSurveyPlan.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.deviationSurveyPlan.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.deviationSurveyPlan.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.deviationSurveyPlan.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"no",
		"title": biolims.deviationSurveyPlan.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "no");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"serialNumber",
		"title": biolims.deviationSurveyPlan.serialNumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "serialNumber");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"reason",
		"title": biolims.deviationSurveyPlan.reason,
		"createdCell": function(td) {
			$(td).attr("saveName", "reason");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"groupMembers",
		"title": biolims.deviationSurveyPlan.groupMembers,
		"createdCell": function(td) {
			$(td).attr("saveName", "groupMembers");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "groupLeader-id",
		"title": "组长ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "groupLeader-id");
		}
	});
	colOpts.push( {
		"data": "groupLeader-name",
		"title": biolims.deviationSurveyPlan.groupLeader,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "groupLeader-name");
			$(td).attr("groupLeader-id", rowData['groupLeader-id']);
		}
	});
	colOpts.push( {
		"data": "departmentUser-id",
		"title": "部门负责人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "departmentUser-id");
		}
	});
	colOpts.push( {
		"data": "departmentUser-name",
		"title": biolims.deviationSurveyPlan.departmentUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "departmentUser-name");
			$(td).attr("departmentUser-id", rowData['departmentUser-id']);
		}
	});
	   colOpts.push({
		"data":"auditOpinion",
		"title": biolims.deviationSurveyPlan.auditOpinion,
		"createdCell": function(td) {
			$(td).attr("saveName", "auditOpinion");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"auditDate",
		"title": biolims.deviationSurveyPlan.auditDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "auditDate");
	    },
		"className": "date"
	});
	colOpts.push( {
		"data": "auditUser-id",
		"title": "审核人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "auditUser-id");
		}
	});
	colOpts.push( {
		"data": "auditUser-name",
		"title": biolims.deviationSurveyPlan.auditUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "auditUser-name");
			$(td).attr("auditUser-id", rowData['auditUser-id']);
		}
	});
	   colOpts.push({
		"data":"approvalOpinion",
		"title": biolims.deviationSurveyPlan.approvalOpinion,
		"createdCell": function(td) {
			$(td).attr("saveName", "approvalOpinion");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "approvalUser-id",
		"title": "批准人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "approvalUser-id");
		}
	});
	colOpts.push( {
		"data": "approvalUser-name",
		"title": biolims.deviationSurveyPlan.approvalUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "approvalUser-name");
			$(td).attr("approvalUser-id", rowData['approvalUser-id']);
		}
	});
	   colOpts.push({
		"data":"approvalDate",
		"title": biolims.deviationSurveyPlan.approvalDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "approvalDate");
	    },
		"className": "date"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#deviationSurveyPlanTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#deviationSurveyPlanTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#deviationSurveyPlanTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#deviationSurveyPlan_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/deviation/plan/deviationSurveyPlan/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 deviationSurveyPlanTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveDeviationSurveyPlan($("#deviationSurveyPlanTable"));
		}
	});
	}
	
	var deviationSurveyPlanOptions = 
	table(true, "","/deviation/plan/deviationSurveyPlan/showDeviationSurveyPlanTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	deviationSurveyPlanTable = renderData($("#deviationSurveyPlanTable"), deviationSurveyPlanOptions);
	deviationSurveyPlanTable.on('draw', function() {
		olddeviationSurveyPlanChangeLog = deviationSurveyPlanTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveDeviationSurveyPlan(ele) {
	var data = saveDeviationSurveyPlanjson(ele);
	var ele=$("#deviationSurveyPlanTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/deviation/plan/deviationSurveyPlan/saveDeviationSurveyPlanTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveDeviationSurveyPlanjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			
			if(k == "groupLeader-name") {
				json["groupLeader-id"] = $(tds[j]).attr("groupLeader-id");
				continue;
			}
			
			if(k == "departmentUser-name") {
				json["departmentUser-id"] = $(tds[j]).attr("departmentUser-id");
				continue;
			}
			
			if(k == "auditUser-name") {
				json["auditUser-id"] = $(tds[j]).attr("auditUser-id");
				continue;
			}
			
			if(k == "approvalUser-name") {
				json["approvalUser-id"] = $(tds[j]).attr("approvalUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		olddeviationSurveyPlanChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
