var planInfluenceProductTable;
var oldplanInfluenceProductChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
//	var id = $("#deviationSurveyPlan_id").val();
	var id = $("#deviationSurveyPlan_deviationHr_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "编码",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
	   });
	   colOpts.push({
		"data":"name",
		"title": "描述",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
//		"className": "edit"
	   });
	   colOpts.push({
		   "data":"orderCode",
		   "title": "产品批次",
		   "createdCell": function(td) {
			   $(td).attr("saveName", "orderCode");
		   },
	   });
	   colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
//		"className": "textarea"
	   });
//	   colOpts.push({
//		"data":"other",
//		"title": "其他",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "other");
//	    },
//		"className": "edit"
//	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
//	tbarOpts.push({
//		text: biolims.common.fillDetail,
//		action: function() {
//			addItem($("#planInfluenceProductTable"))
//	}
//	});
//	
//	tbarOpts.push({
//		text: biolims.common.save,
//		action: function() {
//			savePlanInfluenceProduct($("#planInfluenceProductTable"));
//		}
//	});
//	
//	tbarOpts.push({
//		text: "选择产品",
//		action: function() {
//			selectProduct();
//		}
//	});
	}
	
	var planInfluenceProductOptions = table(true,
		id,
		'/deviation/plan/deviationSurveyPlan/showPlanInfluenceProductTableJson.action', colOpts, tbarOpts)
	planInfluenceProductTable = renderData($("#planInfluenceProductTable"), planInfluenceProductOptions);
	planInfluenceProductTable.on('draw', function() {
		oldplanInfluenceProductChangeLog = planInfluenceProductTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});
//选择产品
function selectProduct(){
	var rows = $("#planInfluenceProductTable .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
//	top.layer.open({
//		title: "请选择",
//		type: 2,
//		area: ["650px", "400px"],
//		btn: biolims.common.selected,
//		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
//		yes: function(index, layer) {
//			rows.addClass("editagain");
//			var orderCode = $('.layui-layer-iframe', parent.document).find("iframe").contents().
//				find("#addSampleOrder .chosed").children("td").eq(0).text();
//			rows.find("td[savename='orderCode']").text(orderCode);
//			top.layer.close(index);
//		}
//	})
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/experiment/qaAudit/qaAudit/showSampleOrderDialogList.action", ''],
		yes: function(index, layer) {
			rows.addClass("editagain");
			var orderCode = $('.layui-layer-iframe', parent.document).find("iframe").contents().
			find("#addSampleOrder .chosed").children("td").eq(2).text();
			rows.find("td[savename='orderCode']").text(orderCode);
			top.layer.close(index);
		}
	})
}

// 保存
function savePlanInfluenceProduct(ele) {
	var data = savePlanInfluenceProductjson(ele);
	var ele=$("#planInfluenceProductTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog2(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/deviation/plan/deviationSurveyPlan/savePlanInfluenceProductTable.action',
		data: {
			id: $("#deviationSurveyPlan_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				planInfluenceProductTable.ajax.reload();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function savePlanInfluenceProductjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "deviationSurveyPlan-name") {
				json["deviationSurveyPlan-id"] = $(tds[j]).attr("deviationSurveyPlan-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog2(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldplanInfluenceProductChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
