var deviationHandlingReportTable;
var olddeviationHandlingReportChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.deviationHandlingReport.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.deviationHandlingReport.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "创建人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.deviationHandlingReport.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.deviationHandlingReport.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.deviationHandlingReport.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.deviationHandlingReport.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"happenDepartment",
		"title": biolims.deviationHandlingReport.happenDepartment,
		"createdCell": function(td) {
			$(td).attr("saveName", "happenDepartment");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"happenDate",
		"title": biolims.deviationHandlingReport.happenDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "happenDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"happenAddress",
		"title": biolims.deviationHandlingReport.happenAddress,
		"createdCell": function(td) {
			$(td).attr("saveName", "happenAddress");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "discoverer-id",
		"title": "发现人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "discoverer-id");
		}
	});
	colOpts.push( {
		"data": "discoverer-name",
		"title": biolims.deviationHandlingReport.discoverer,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "discoverer-name");
			$(td).attr("discoverer-id", rowData['discoverer-id']);
		}
	});
	   colOpts.push({
		"data":"correctiveActionId",
		"title": biolims.deviationHandlingReport.correctiveActionId,
		"createdCell": function(td) {
			$(td).attr("saveName", "correctiveActionId");
	    },
		"visible": false,	
		"className": "edit"
	});
	   colOpts.push({
		"data":"correctiveActionName",
		"title": biolims.deviationHandlingReport.correctiveActionName,
		"createdCell": function(td) {
			$(td).attr("saveName", "correctiveActionName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"kindId",
		"title": biolims.deviationHandlingReport.kindId,
		"createdCell": function(td) {
			$(td).attr("saveName", "kindId");
	    },
		"visible": false,	
		"className": "edit"
	});
	   colOpts.push({
		"data":"kindName",
		"title": biolims.deviationHandlingReport.kindName,
		"createdCell": function(td) {
			$(td).attr("saveName", "kindName");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "reportUser-id",
		"title": "偏差报告人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "reportUser-id");
		}
	});
	colOpts.push( {
		"data": "reportUser-name",
		"title": biolims.deviationHandlingReport.reportUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "reportUser-name");
			$(td).attr("reportUser-id", rowData['reportUser-id']);
		}
	});
	   colOpts.push({
		"data":"reportDate",
		"title": biolims.deviationHandlingReport.reportDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "reportDate");
	    },
		"className": "date"
	});
	colOpts.push( {
		"data": "departmentUser-id",
		"title": "部门负责人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "departmentUser-id");
		}
	});
	colOpts.push( {
		"data": "departmentUser-name",
		"title": biolims.deviationHandlingReport.departmentUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "departmentUser-name");
			$(td).attr("departmentUser-id", rowData['departmentUser-id']);
		}
	});
	   colOpts.push({
		"data":"departmentDate",
		"title": biolims.deviationHandlingReport.departmentDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "departmentDate");
	    },
		"className": "date"
	});
	colOpts.push( {
		"data": "monitor-id",
		"title": "监控人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "monitor-id");
		}
	});
	colOpts.push( {
		"data": "monitor-name",
		"title": biolims.deviationHandlingReport.monitor,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "monitor-name");
			$(td).attr("monitor-id", rowData['monitor-id']);
		}
	});
	   colOpts.push({
		"data":"monitoringDate",
		"title": biolims.deviationHandlingReport.monitoringDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "monitoringDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"influence",
		"title": biolims.deviationHandlingReport.influence,
		"createdCell": function(td) {
			$(td).attr("saveName", "influence");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"type",
		"title": biolims.deviationHandlingReport.type,
		"createdCell": function(td) {
			$(td).attr("saveName", "type");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "groupLeader-id",
		"title": "调查组长ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "groupLeader-id");
		}
	});
	colOpts.push( {
		"data": "groupLeader-name",
		"title": biolims.deviationHandlingReport.groupLeader,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "groupLeader-name");
			$(td).attr("groupLeader-id", rowData['groupLeader-id']);
		}
	});
	   colOpts.push({
		"data":"groupMembers",
		"title": biolims.deviationHandlingReport.groupMembers,
		"createdCell": function(td) {
			$(td).attr("saveName", "groupMembers");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"no",
		"title": biolims.deviationHandlingReport.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "no");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#deviationHandlingReportTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#deviationHandlingReportTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#deviationHandlingReportTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#deviationHandlingReport_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/deviation/deviationHandlingReport/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 deviationHandlingReportTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveDeviationHandlingReport($("#deviationHandlingReportTable"));
		}
	});
	}
	
	var deviationHandlingReportOptions = 
	table(true, "","/deviation/deviationHandlingReport/showDeviationHandlingReportTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	deviationHandlingReportTable = renderData($("#deviationHandlingReportTable"), deviationHandlingReportOptions);
	deviationHandlingReportTable.on('draw', function() {
		olddeviationHandlingReportChangeLog = deviationHandlingReportTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveDeviationHandlingReport(ele) {
	var data = saveDeviationHandlingReportjson(ele);
	var ele=$("#deviationHandlingReportTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/deviation/deviationHandlingReport/saveDeviationHandlingReportTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveDeviationHandlingReportjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			
			if(k == "discoverer-name") {
				json["discoverer-id"] = $(tds[j]).attr("discoverer-id");
				continue;
			}
			
			if(k == "reportUser-name") {
				json["reportUser-id"] = $(tds[j]).attr("reportUser-id");
				continue;
			}
			
			if(k == "departmentUser-name") {
				json["departmentUser-id"] = $(tds[j]).attr("departmentUser-id");
				continue;
			}
			
			if(k == "monitor-name") {
				json["monitor-id"] = $(tds[j]).attr("monitor-id");
				continue;
			}
			
			if(k == "groupLeader-name") {
				json["groupLeader-id"] = $(tds[j]).attr("groupLeader-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		olddeviationHandlingReportChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
