var deviationHandlingReportTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.deviationHandlingReport.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.deviationHandlingReport.name,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":"创建人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.deviationHandlingReport.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.deviationHandlingReport.createDate,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.deviationHandlingReport.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.deviationHandlingReport.stateName,
	});
	
	    fields.push({
		"data":"happenDepartment",
		"title":biolims.deviationHandlingReport.happenDepartment,
	});
	
	    fields.push({
		"data":"happenDate",
		"title":biolims.deviationHandlingReport.happenDate,
	});
	
	    fields.push({
		"data":"happenAddress",
		"title":biolims.deviationHandlingReport.happenAddress,
	});
	
	    fields.push({
		"data":"discoverer-id",
		"title":"发现人ID"
	});
	    fields.push({
		"data":"discoverer-name",
		"title":biolims.deviationHandlingReport.discoverer
	});
	
	    fields.push({
		"data":"correctiveActionId",
		"title":biolims.deviationHandlingReport.correctiveActionId,
	});
	
	    fields.push({
		"data":"correctiveActionName",
		"title":biolims.deviationHandlingReport.correctiveActionName,
	});
	
	    fields.push({
		"data":"kindId",
		"title":biolims.deviationHandlingReport.kindId,
	});
	
	    fields.push({
		"data":"kindName",
		"title":biolims.deviationHandlingReport.kindName,
	});
	
	    fields.push({
		"data":"reportUser-id",
		"title":"偏差报告人ID"
	});
	    fields.push({
		"data":"reportUser-name",
		"title":biolims.deviationHandlingReport.reportUser
	});
	
	    fields.push({
		"data":"reportDate",
		"title":biolims.deviationHandlingReport.reportDate,
	});
	
	    fields.push({
		"data":"departmentUser-id",
		"title":"部门负责人ID"
	});
	    fields.push({
		"data":"departmentUser-name",
		"title":biolims.deviationHandlingReport.departmentUser
	});
	
	    fields.push({
		"data":"departmentDate",
		"title":biolims.deviationHandlingReport.departmentDate,
	});
	
	    fields.push({
		"data":"monitor-id",
		"title":"监控人ID"
	});
	    fields.push({
		"data":"monitor-name",
		"title":biolims.deviationHandlingReport.monitor
	});
	
	    fields.push({
		"data":"monitoringDate",
		"title":biolims.deviationHandlingReport.monitoringDate,
	});
	
	    fields.push({
		"data":"influence",
		"title":biolims.deviationHandlingReport.influence,
	});
	
	    fields.push({
		"data":"type",
		"title":biolims.deviationHandlingReport.type,
	});
	
	    fields.push({
		"data":"groupLeader-id",
		"title":"调查组长ID"
	});
	    fields.push({
		"data":"groupLeader-name",
		"title":biolims.deviationHandlingReport.groupLeader
	});
	
	    fields.push({
		"data":"groupMembers",
		"title":biolims.deviationHandlingReport.groupMembers,
	});
	
	    fields.push({
		"data":"no",
		"title":biolims.deviationHandlingReport.no,
	});
	
	var options = table(true, "","/deviation/deviationHandlingReport/showDeviationHandlingReportTableJson.action",
	 fields, null)
	deviationHandlingReportTable = renderData($("#addDeviationHandlingReportTable"), options);
	$('#addDeviationHandlingReportTable').on('init.dt', function() {
		recoverSearchContent(deviationHandlingReportTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.deviationHandlingReport.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.deviationHandlingReport.name
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.deviationHandlingReport.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.deviationHandlingReport.createDate
		});
	fields.push({
			"txt": "创建时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.deviationHandlingReport.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.deviationHandlingReport.stateName
		});
	   fields.push({
		    "searchName":"happenDepartment",
			"type":"input",
			"txt":biolims.deviationHandlingReport.happenDepartment
		});
	   fields.push({
		    "searchName":"happenDate",
			"type":"input",
			"txt":biolims.deviationHandlingReport.happenDate
		});
	fields.push({
			"txt": "发生时间(Start)",
			"type": "dataTime",
			"searchName": "happenDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "发生时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "happenDate##@@##2"
		});
	   fields.push({
		    "searchName":"happenAddress",
			"type":"input",
			"txt":biolims.deviationHandlingReport.happenAddress
		});
	fields.push({
	    "type":"input",
		"searchName":"discoverer.id",
		"txt":"发现人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"discoverer.name",
		"txt":biolims.deviationHandlingReport.discoverer
	});
	   fields.push({
		    "searchName":"correctiveActionId",
			"type":"input",
			"txt":biolims.deviationHandlingReport.correctiveActionId
		});
	   fields.push({
		    "searchName":"correctiveActionName",
			"type":"input",
			"txt":biolims.deviationHandlingReport.correctiveActionName
		});
	   fields.push({
		    "searchName":"kindId",
			"type":"input",
			"txt":biolims.deviationHandlingReport.kindId
		});
	   fields.push({
		    "searchName":"kindName",
			"type":"input",
			"txt":biolims.deviationHandlingReport.kindName
		});
	fields.push({
	    "type":"input",
		"searchName":"reportUser.id",
		"txt":"偏差报告人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"reportUser.name",
		"txt":biolims.deviationHandlingReport.reportUser
	});
	   fields.push({
		    "searchName":"reportDate",
			"type":"input",
			"txt":biolims.deviationHandlingReport.reportDate
		});
	fields.push({
			"txt": "偏差报告日期(Start)",
			"type": "dataTime",
			"searchName": "reportDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "偏差报告日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "reportDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"departmentUser.id",
		"txt":"部门负责人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"departmentUser.name",
		"txt":biolims.deviationHandlingReport.departmentUser
	});
	   fields.push({
		    "searchName":"departmentDate",
			"type":"input",
			"txt":biolims.deviationHandlingReport.departmentDate
		});
	fields.push({
			"txt": "部门负责日期(Start)",
			"type": "dataTime",
			"searchName": "departmentDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "部门负责日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "departmentDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"monitor.id",
		"txt":"监控人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"monitor.name",
		"txt":biolims.deviationHandlingReport.monitor
	});
	   fields.push({
		    "searchName":"monitoringDate",
			"type":"input",
			"txt":biolims.deviationHandlingReport.monitoringDate
		});
	fields.push({
			"txt": "监控日期(Start)",
			"type": "dataTime",
			"searchName": "monitoringDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "监控日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "monitoringDate##@@##2"
		});
	   fields.push({
		    "searchName":"influence",
			"type":"input",
			"txt":biolims.deviationHandlingReport.influence
		});
	   fields.push({
		    "searchName":"type",
			"type":"input",
			"txt":biolims.deviationHandlingReport.type
		});
	fields.push({
	    "type":"input",
		"searchName":"groupLeader.id",
		"txt":"调查组长ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"groupLeader.name",
		"txt":biolims.deviationHandlingReport.groupLeader
	});
	   fields.push({
		    "searchName":"groupMembers",
			"type":"input",
			"txt":biolims.deviationHandlingReport.groupMembers
		});
	   fields.push({
		    "searchName":"no",
			"type":"input",
			"txt":biolims.deviationHandlingReport.no
		});
	
	fields.push({
		"type":"table",
		"table":deviationHandlingReportTable
	});
	return fields;
}
