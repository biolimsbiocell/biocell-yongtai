var bloodSplitTab;
$(function() {
	var options = table(true, "",
		"/experiment/plasma/bloodSplit/showBloodSplitTableByStateJson.action", [{
			"data": "id",
			"title": "操作",
			"width": "60px",
			"render": function(data) {
				return "<button id=" + data + " class='btn btn-info btn-xs plus' onclick='showSubTable(this)'>展开</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
			},
			"createdCell": function(td, data, rowdata) {
				if(rowdata.state == 3) {
					if(rowdata.opUser) {
						rowdata.opUser.split(",").forEach(function(v, i) {
							if(v == window.userName) {
								$(td).addClass("showTd");
							}
						});
					}
				}
			}
		}, {
			"data": "id",
			"title": biolims.common.id,
		}, {
			/*"data": "name",
			"title": biolims.common.name,
		}, {*/
			"data": "createUser-name",
			"title": biolims.sample.createUserName,
		}, {
			"data": "createDate",
			"title": biolims.sample.createDate,
		}, {
			/*"data": "confirmDate",
			"title": biolims.common.confirmDate,
		}, {*/
			"data": "testUserOneName",
			"title": biolims.common.testUserName,
		}, {
			"data": "template-name",
			"title": biolims.common.experimentModule,
		}, {
			"data": "scopeName",
			"title": biolims.purchase.costCenter,
		}, {
			"data": "stateName",
			"title": biolims.common.stateName,
		}], null);
	bloodSplitTab = renderRememberData($("#bloodSplit"), options);
	$('#bloodSplit').on('init.dt', function() {
		recoverSearchContent(bloodSplitTab);
	})
});


//展示样本步骤
function showSubTable(that) {
	if(!$(that).parents("tr").next(".subTable").length) {
		$(that).parents("tr").after('<tr class="subTable" style="display:none"><td colspan="11"><table class="table table-bordered table-condensed" style="font-size: 12px;"></table></td></tr>');
	}
	var corspanTr = $(that).parents("tr").next(".subTable");
	if($(that).hasClass("plus")) { //展开
		$(that).removeClass("plus").addClass("minus");
		that.innerText = "关闭";
		corspanTr.slideDown();
		var elemen = corspanTr.find("table");
		if(!elemen.find("tr").length) {
			var id = that.id;
			renderSubTable(elemen, id);
		}
	} else { //关闭
		corspanTr.slideUp();
		that.innerText = "展开";
		$(that).removeClass("minus").addClass("plus");
	}
}

function renderSubTable(elemen, id) {
	colData = [{
		"data": "endTime",
		"title": "操作",
		"render": function(data) {
			if(data) {
				return "<button id=" + id + " class='btn btn-warning btn-xs' onclick='viewStrps(this)'>查看详情</button>";
			} else {
				return "<button id=" + id + " class='btn btn-info btn-xs' onclick='viewStrps(this)'>操作</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
			}
		},
		"createdCell": function(td, data, rowdata) {
			if(rowdata.testUserList) {
				rowdata.testUserList.split(",").forEach(function(v, i) {
					if(v == window.userName) {
						$(td).addClass("showTd");
					}
				});
			}
		}

	}, {
		"data": 'name',
		"title": "工序",
	}, {
		"data": 'name',
		"title": "步骤名称",
	}, {
		"data": 'testUserList',
		"title": "操作人",
	}, {
		"data": 'estimatedDate',
		"title": "预计操作日期",
	}];
	var options = table(false, id,
		"/experiment/plasma/bloodSplit/findPlasmaTaskTemplateList.action", colData, null);
	renderData(elemen, options);
}

function viewStrps(that) {
	var id = that.id;
	var orderNum = $(that).parent("td").parent("tr").index() + 1;
	sessionStorage.removeItem("dijibu");
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/plasma/bloodSplit/showBloodSplitSteps.action?id=" + id + "&orderNum=" + orderNum;
}