/* 
* 文件名称 :cosTemplateDialogTable.js
* 创建者 : 郭恒开
* 创建日期: 2018/01/25
* 文件描述:选择实验页面设备的datatables
* 
*/
$(function() {
	
	//接样单打印
	$.ajax({
		type : "post",
		data : {
			id : $("#ordernum").val()
		},
		url : ctx+ "/experiment/plasma/sampleReceiveEx/findsamplereceive.action",
		success : function(data1) {
			var data = JSON.parse(data1);
			if (data.success) {
				if(data.sris!=undefined){
					var newdiv = document.getElementById("show_samplereceive_div");
					newdiv.innerHTML = "<a href=\"javascript:void(0)\" onclick=\"dayin1('"+data.sris[0].sampleCode+"');\">接样打印单</a><br>";
//					newdiv.innerHTML = "<a href=\"javascript:void(0)\" onclick=\"dayin1('"+data.sris[0].id+"');\">接样打印单</a><br>";
				}
//				myTable.ajax.reload();
			} else {
//				top.layer.msg("生成失败！")
			}

		}
	});
	
	//批生产记录打印
	$.ajax({
		type : "post",
		data : {
			id : $("#ordernum").val()
		},
		url : ctx+ "/experiment/plasma/sampleReceiveEx/findExeperiment.action",
		success : function(data1) {
			var data = JSON.parse(data1);
			if (data.success) {
				if(data.cpid!=undefined){
					if(data.cpt!=undefined){
						var ziduan="";
						var newdiv = document.getElementById("show_cellpassage_div");
						for(var z=0;z<data.cpt.length;z++){
							console.log(data.cpt[z].chongfu)
							debugger
							if((data.cpt[z].chongfu==null||data.cpt[z].chongfu=="")&&data.cpt[z].orderNum!=""){
								ziduan = ziduan + "<a href=\"javascript:void(0)\" onclick=\"dayin('"+data.cpid+"','"+data.cpt[z].orderNum+"',2);\">生产批记录</a><br>";
							}else{
								ziduan = ziduan + "<a href=\"javascript:void(0)\" onclick=\"dayin('"+data.cpid+"','"+data.cpt[z].orderNum+"',1);\">生产批记录</a><br>";
							}
						}
						newdiv.innerHTML = ziduan;
					}
				}
			} else {
				
			}

		}
	});
	
	
//	生产产品质检打印
//	$.ajax({
//		type : "post",
//		data : {
//			id : $("#ordernum").val()
//		},
//		url : ctx+ "/experiment/plasma/sampleReceiveEx/findExeperimentResult.action",
//		success : function(data1) {
//			var data = JSON.parse(data1);
//			if (data.success) {
//				if(data.qt!=undefined){
//					var ziduan="";
//					var newdiv = document.getElementById("show_cellpassageresult_div");
//					for(var z=0;z<data.qt.length;z++){
//						ziduan = ziduan + "<a href=\"javascript:void(0)\" onclick=\"dayin2('"+data.qt[z].id+"','"+data.qt[z].sampleDeteyion.name+"','"+data.qt[z].qualityTest.createDate+"');\">"+"质检单号"+data.qt[z].qualityTest.id+"</a><br>";
//					}
//					newdiv.innerHTML = ziduan;
//				}
//			} else {
//				
//			}
//
//		}
//	});

});
//产品质检报告打印打印
function dayin2(id,jiance,createDate){
	if(jiance.indexOf("成品外观") != -1){
		var url = '__report=cpwgjc2.rptdesign&zjId='+id+'&id=' + id;//$("#qualityTest_id").text();
		commonPrint(url);
	}else if(jiance.indexOf("成品检验") != -1
			||jiance.indexOf("中间产品安全性检验") != -1){
		var url = '__report=cpaqxjc.rptdesign&zjId='+id+'&id=' + id;//$("#qualityTest_id").text();
		commonPrint(url);
	}else{
		$.ajax({
			type : "post",
			data : {
				id : $("#sampleReveice_id").text(),
				confirmDate : createDate,
				modelId:"dyzjjg"
			},
			url : ctx
					+ "/stamp/birtVersion/selectBirt.action",
			success : function(data) {
				var data = JSON.parse(data)
				if (data.reportN) {
					var url = '__report='+data.reportN+'&zjId='+id+'&id=' + id;
					commonPrint(url);
				} else {
					top.layer.msg("没有报表信息！");
				}
			}
		});
		
//		var url = '__report=cpjybgd.rptdesign&zjId='+id+'&id=' + id;//$("#qualityTest_id").text();
//		commonPrint(url);
	}
//	var url = '__report=cpjybgd.rptdesign&id=' + id+'&zjId=' + id;
//	commonPrint(url);
}
//接样打印
function dayin1(sampleCode){
	var url = '__report=sample_receive.rptdesign&id=' + sampleCode;
	commonPrint(url);
}
//批处理打印
function dayin(danao,num,chongfu){
	if(chongfu==2){
		$.ajax({
			type : "post",
			data : {
				id : danao,
			},
			url : ctx
					+ "/experiment/cell/passage/cellPassage/selectDate.action",
			success : function(data) {
				var data = JSON.parse(data)
				if (data.da) {
					$.ajax({
						type : "post",
						data : {
							id : $("#sampleReveice_id").text(),
							confirmDate : data.da,
							modelId:"scrwlb",
							oNum:num
						},
						url : ctx
								+ "/stamp/birtVersion/selectBirt.action",
						success : function(data) {
							var data = JSON.parse(data)
							if (data.reportN) {
								var url = '__report='+data.reportN+'&id=' + danao;
								commonPrint(url);
							} else {
								top.layer.msg("没有报表信息！");
							}
						}
					});
					
				
				}
					
//				} else {
//					top.layer.msg("！");
//				}
			}
		});
		
		
//		var url = '__report=templateitem.rptdesign&id=' + danao+'&ordernum='+num;
//		commonPrint(url);
	}else{
		$.ajax({
			type : "post",
			data : {
				id : danao,
			},
			url : ctx
					+ "/experiment/cell/passage/cellPassage/selectDate.action",
			success : function(data) {
				var data = JSON.parse(data)
				if (data.da) {
					$.ajax({
						type : "post",
						data : {
							id : $("#sampleReveice_id").text(),
							confirmDate : data.da,
							modelId:"scrwlbcf"
						},
						url : ctx
								+ "/stamp/birtVersion/selectBirt.action",
						success : function(data) {
							var data = JSON.parse(data)
							if (data.reportN) {
								var url = '__report='+data.reportN+'&id=' + danao+'&ordernum='+num;
								commonPrint(url);
							} else {
								top.layer.msg("没有报表信息！");
							}
						}
					});
					
				
				}
					
//				} else {
//					top.layer.msg("！");
//				}
			}
		});
		
//		var url = '__report=templateitemYC.rptdesign&id=' + danao+'&ordernum='+num;
//		commonPrint(url);
	}
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '', '');
}

function showcpjyy(){
	var id=$("#ordernum").val();
	$.ajax({
		type : "post",
		data : {
			modelId:"yccpjybgdone",
			id:id,
			
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirtv.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				var url = '__report='+data.reportN+'&batch=' + id;
				commonPrint(url);
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
}


function showcpjye(){
	var id=$("#ordernum").val();
	$.ajax({
		type : "post",
		data : {
			modelId:"yccpjybgdtwo",
			id:id,
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirtv.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				var url = '__report='+data.reportN+'&batch=' + id;
				commonPrint(url);
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
}