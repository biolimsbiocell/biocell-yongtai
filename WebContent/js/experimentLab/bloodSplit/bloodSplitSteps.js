/* 
 * 文件名称 :bloodSplitSteps.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/01/23
 * 文件描述: 实验步骤相关js函数
 * 
 */
var bloodSplit_id = $("#bloodSplit_id").text();
var isSeparate = $("#bloodSplit_template_isSeparate").val();
var changelogRea, changelogCos;
var flg = false;
if(isSeparate == 1) {
	flg = true;
}
$(function() {
	//请求步骤的数据
	$.ajax({
		type: "post",
		data: {
			id: bloodSplit_id
		},
		async: false,
		url: ctx + "/experiment/plasma/bloodSplit/showBloodSplitStepsJson.action",
		success: function(data) {
			var data = JSON.parse(data);
			var list = data.template;
			var reagent = changelogRea = data.reagent;
			var cos = changelogCos = data.cos;
			var plate = data.plate;
			var zhiJian = data.zhiJian;
			if(zhiJian){
				zhiJian.forEach(function (vvv,i) {
					$("#zjType").text(vvv.typeName);
					$("#zjType").attr("zjId",vvv.typeId);
					renderZhijian(vvv);
				});
			}else{
				$("#zjType").text("");
				$("#zjType").attr("");
			}
			//生成步骤
			var stepsLi = "<li><input class='content hide' value=" + list[0].content + "><input class='contentdata hide' value=" + list[0].contentData + "><em class='hide'>" + list[0].note + "</em><a class='selected step' stepid=" + list[0].id + "><span class='step_no'>" + list[0].orderNum + "</span><p class='step_descr'> " + list[0].name + "</p><p class='estimatedDate'> 预计用时:"+list[0].estimatedDate+"天</p></a></li>";
			list.forEach(function(v, i) {
				if(i > 0) {
					var index = i + 1;
					stepsLi += "<li><input class='content hide' value=" + v.content + "><input class='contentdata hide' value=" + v.contentData + "><em class='hide'>" + v.note + "</em><a class='disabled step' stepid=" + v.id + "><span class='step_no'>" + v.orderNum + "</span><p class='step_descr'> " + v.name + "</p><p class='estimatedDate'>预计用时:"+v.estimatedDate+"天</p></a></li>";
				}
			});
			$(".wizard_steps").append(stepsLi);
			//默认渲染第一个步骤名称
			$("#steptiele").text(list[0].name);
			//实验员
			$("#testUserList").val(list[0].testUserList);
			if(list[0].sampleDeteyion!=null&&list[0].sampleDeteyion!=""&&list[0].sampleDeteyion!=undefined){
			$("#sample_detecyion_name").val(list[0].sampleDeteyion.name);
			$("#sample_detecyion_id").val(list[0].sampleDeteyion.id);
			}else{
				$("#sample_detecyion_name").val("");
				$("#sample_detecyion_id").val("");
			}
			$("#estimated_time").val(list[0].estimatedTime);
			//默认渲染第一个步骤步骤明细（HTML代码）
			$("#stepcontent").html(list[0].note);
			$("#testUserList").html(list[0].testUserList);
			$("#planEndDate").html(list[0].planEndDate).attr("changelog", list[0].planEndDate);
			$("#startTime").html(list[0].startTime).attr("changelog", list[0].startTime);
			$("#endTime").html(list[0].endTime).attr("changelog", list[0].endTime);
			//默认渲染第一个步骤实验记录（自定义字段）
			renderContentData(list[0].content, list[0].contentData);
			//默认渲染第一个步骤原辅料
			renderReagentAndCos(reagent, cos);
			//生成孔板的box
			plate.forEach(function(v, i) {
				if(i > 0) {
					var clonePlateDiv = $("#plateDiv").eq(0).clone();
					clonePlateDiv.find(".box-titlem").text(v[0]);
					clonePlateDiv.find(".box-titlesub").text(v[1]);
					$("#plateModal").append(clonePlateDiv);
				} else {
					$("#plateDiv").find(".box-titlem").text(v[0]);
					$("#plateDiv").find(".box-titlesub").text(v[1]);
				}
			});
			renderPlate();
			//为每个步骤注册点击事件
			$(".wizard_steps .step").click(function() {
				$(".wizard_steps .step").removeClass("selected").addClass("disabled");
				$(this).removeClass("disabled").addClass("selected");
				//变步骤名称
				$("#steptiele").text($(this).children(".step_descr").text());
				//$("#steptiele").text($(this).children(".step_descr").text());
				//变步骤详情（HTML代码）
				$("#stepcontent").html($(this).siblings("em").html());
				//变实验记录

				renderContentData($(this).siblings(".content").val(), $(this).siblings(".contentdata").val());
				//变原辅料
				$.ajax({
					type: "post",
					data: {
						orderNum: $(this).children(".step_no").text(),
						id: bloodSplit_id
					},
					url: ctx + "/experiment/plasma/bloodSplit/showBloodSplitStepsJson.action",
					success: function(data) {

						var data = JSON.parse(data);
						var reagent = changelogRea = data.reagent;
						var cos = changelogCos = data.cos;
						var list = data.template;
						
						var zhiJian=data.zhiJian;
						$("#zhijianBody").empty();
						if(zhiJian){
							zhiJian.forEach(function (vvv,i) {
								$("#zjType").text(vvv.typeName);
								$("#zjType").attr("zjId",vvv.typeId);
								renderZhijian(vvv);
							});
						}else{
							$("#zjType").text("");
							$("#zjType").attr("");
						}
						
						$("#testUserList").html(list[0].testUserList);
						$("#planEndDate").html(list[0].planEndDate).attr("changelog", list[0].planEndDate);
						$("#startTime").html(list[0].startTime).attr("changelog", list[0].startTime);
						$("#endTime").html(list[0].endTime).attr("changelog", list[0].endTime);

						renderReagentAndCos(reagent, cos);
					}
				});
				//变设备
			});

			//为步骤明细的HTML设置动画
			$("#stepContentBtn").click(function() {
				if($(this).hasClass("xxx")) {
					$(this).removeClass("xxx");
					$("#stepContentModer").animate({
						"height": "10%"
					}, 800, "swing", function() {
						$(this).animate({
							"width": "0%",
							"height": "0%",
						}, 600, "linear");
					});
				} else {
					$(this).addClass("xxx");
					$("#stepContentModer").animate({
						"width": "100%",
						"height": "10%"
					}, 500, "swing", function() {
						$(this).animate({
							"height": "90%"
						}, 600, "linear");
					});
				}
			});
		}
	});
	preAndNext();
	//原辅料的扫码
	ReagentScanCode();
	//设备的扫码
	cosScanCode();
	bpmTask($("#bpmTaskId").val());
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view" || $("#bloodSplit_state").text() == "Complete") {
		settextreadonly();
		$("#save").hide()
		$("#tjsp").hide()
	}
	//判断如果是列表页进来的，直接跳到当前步骤
	renderOrderNums();
});
//渲染实验记录（自定义字段）
function renderContentData(content, contentData) {

	var flag = true;
	var inputs = '';
	inputs+='<div class="col-xs-12"><div class="input-group"><span class="input-group-addon">操作员</span><input type="text" class="form-control" name="testUserList" id="testUserList" /><span class="input-group-btn"><button class="btn btn-info" type="button" onClick="showdutyManId()"> <i class="glyphicon glyphicon-search"></i></button></span></div></div><div class="col-xs-12"><div class="input-group"><span class="input-group-addon">生产样本</span><input type="text" name="sampleCodes" required=false   class="form-control"  value=""></div></div>';
	var content = JSON.parse(content);
	content.forEach(function(v, i) {
		var disabled = v.readOnly == "true" ? "disabled" : ' ';
		var defaultValue = v.defaultValue ? v.defaultValue : ' ';
//		groupIds.split(",").forEach(function(v, i) {
//			if(v == "QC") {
//				flag = true; //质检组
//			}
//		});
		if(flag) { 
			//代表质检组的人只能看自定义字段标记为是否质检选择“是”的字段
				if(v.type == "checkbox") {
					var checkboxs = '';
					v.singleOption.forEach(function(vv, jj) {
						checkboxs += '<input type="checkbox" name=' + v.fieldName + ' value=' + vv.itemValue + ' title=' + vv.itemName + '>' + vv.itemName + '';
					});
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group checkboxs" changelog=""><span class="input-group-addon">' + v.label + '</span>' + checkboxs + '</div></div>';
				} else if(v.type == "radio") {
					var options = '';
					v.singleOption.forEach(function(vv, jj) {
						options += '<option name=' + v.fieldName + ' value=' + vv.itemValue + '>' + vv.itemName + '</option>';
					});
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><select class="form-control">' + options + '</select></div></div>';
				} else if(v.type == "date") {
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><input type="text" name=' + v.fieldName + ' required=' + v.required + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
				} else {
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><input type=' + v.type + ' name=' + v.fieldName + ' required=' + v.required + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
				}
		} else { //不是质检组的人只能看到自定义字段标记为是否质检选择“否”的字段

			if(v.qualityChecking == "false") {
				if(v.type == "checkbox") {
					var checkboxs = '';
					v.singleOption.forEach(function(vv, jj) {
						checkboxs += '<input type="checkbox" name=' + v.fieldName + ' value=' + vv.itemValue + ' title=' + vv.itemName + '>' + vv.itemName + '';
					});
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group checkboxs" changelog=""><span class="input-group-addon">' + v.label + '</span>' + checkboxs + '</div></div>';
				} else if(v.type == "radio") {
					var options = '';
					v.singleOption.forEach(function(vv, jj) {
						options += '<option name=' + v.fieldName + ' value=' + vv.itemValue + '>' + vv.itemName + '</option>';
					});
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><select class="form-control">' + options + '</select></div></div>';
				} else if(v.type == "date") {
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><input type="text" name=' + v.fieldName + ' required=' + v.required + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
				} else {
					inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><input type=' + v.type + ' name=' + v.fieldName + ' required=' + v.required + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
				}
			}

		}
	});
	$("#contentData").html(inputs);
	//日期格式化
	$("#contentData").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//赋值哟
	if(contentData && contentData != "null") {
		var contentData = JSON.parse(contentData);
		for(var k in contentData) {
			$("#contentData input").each(function(i, inp) {
				if(inp.name == k) {
					if(inp.type == "checkbox") {
						if(contentData[k].indexOf(inp.value) != -1) {
							$(inp).parents(".input-group").attr("changelog", contentData[k]);
							inp.setAttribute("checked", true);
						}
					} else {
						inp.value = contentData[k];
						inp.setAttribute("changelog", contentData[k]);
						return false;
					}
				}
			});
			$("#contentData option").each(function(i, val) {
				if(k == val.getAttribute("name")) {
					if(val.value == contentData[k]) {
						$(val).parent("select").attr("changelog", contentData[k]);
						val.setAttribute("selected", true);
						return false;
					}
				}
			});
		};
	}

	//多选框 格式化
	$("#contentData").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
}
//渲染原辅料和设备
function renderReagentAndCos(reagent, cos) {
	if(reagent) {
		var reagentLis = "";
		reagent.forEach(function(v, i) {
			var batch = v.batch ? v.batch : "";
			var sn = v.sn ? v.sn : "";
			var code = v.code ? v.code : "";
			reagentLis += '<li class="reagli" reagentid=' + v.id + ' code=' + code + ' itemid=' + v.itemId + ' ><span><i class="fa fa-search-plus"></i></span><i class="fa fa-trash pull-right"></i><i class="fa fa-copy pull-right" style="margin-right: 5px;"></i><p class="text">' + v.name + '</p><small class="label label-primary">批次:<span>' + batch + '</span></small><small class="label label-info">sn:<span>' + sn + '</span></small></li>'
		});
		$("#reagentBody").html(reagentLis);
	}
	if(cos) {
		var cosLis = "";
		cos.forEach(function(v, i) {
			var name = v.name ? v.name : "";
			var code = v.code ? v.code : "";
			cosLis += '<li class="cosli" cosid=' + v.id + ' itemid=' + v.itemId + '  typeid=' + v.type.id + ' code=' + code + '  ><span><i class="fa fa-search-plus"></i></span><i class="fa fa-trash pull-right"></i><i class="fa fa-copy pull-right" style="margin-right: 5px;"></i><p class="text">' + v.type.name + '</p><small class="label label-default">' + name + '</small></li>'
		});
		$("#cosBody").html(cosLis);
	}
	//原辅料和设备复制操作
	reagentAndCosCopy();
	//原辅料和设备删除操作
	reagentAndCosRemove();
	//选择原辅料和设备
	reagentAndCosChose();
}
//原辅料和设备复制操作
function reagentAndCosCopy() {
	$(".fa-copy").unbind("click").click(function() {
		var li = $(this).parent("li").clone();
		//复制的是原辅料
		if(li.hasClass("reagli")) {
			li.attr("reagentid", ""); //id为空
			li.find(".label-primary span").text(""); //批次为空
			li.find(".label-info span").text(""); //sn为空
		}
		//复制的是设备
		if(li.hasClass("cosli")) {
			li.attr({
				"cosid": "",
				"code": ""
			}); //id为空
			li.children(".label-default").text(""); //设备名称为空
		}
		$(this).parent("li").after(li);
		reagentAndCosCopy();
		reagentAndCosRemove();
		reagentAndCosChose();
	});
}

//原辅料和设备删除操作
function reagentAndCosRemove() {
	$(".fa-trash").unbind("click").click(function() {
		var li = $(this).parent("li");
		if(li.hasClass("reagli")) {
			var id = li.attr("reagentid");
			if(id) {
				top.layer.confirm(biolims.common.confirm2Del, {
					icon: 3,
					title: biolims.common.prompt,
					btn: biolims.common.selected,
				}, function(index) {
					$.ajax({
						type: "post",
						url: ctx + "/experiment/plasma/bloodSplit/delBloodSplitReagent.action",
						data: {
							id: id,
							bloodSplit_id: $("#bloodSplit_id").text(),
							del: "样本处理实验步骤：" + $(".wizard_steps .selected").find(".step_descr").text() + "删除原辅料："
						},
						success: function(data) {
							var data = JSON.parse(data);
							if(data.success) {
								li.remove();
								top.layer.msg(biolims.common.deleteSuccess);
							}
						}
					});

					top.layer.close(index);
				});

			} else {
				top.layer.msg(biolims.common.deleteSuccess);
				li.remove();
			}
		}
		if(li.hasClass("cosli")) {
			var id = li.attr("cosid");
			if(id) {
				top.layer.confirm(biolims.common.confirm2Del, function(index) {
					$.ajax({
						type: "post",
						url: ctx + "/experiment/plasma/bloodSplit/delBloodSplitCos.action",
						data: {
							id: id,
							bloodSplit_id: $("#bloodSplit_id").text(),
							del: "样本处理实验步骤：" + $(".wizard_steps .selected").find(".step_descr").text() + "删除设备："
						},
						success: function(data) {
							var data = JSON.parse(data);
							if(data.success) {
								li.remove();
								top.layer.msg(biolims.common.deleteSuccess);
							}
						}
					});

					top.layer.close(index);
				});

			} else {
				top.layer.msg(biolims.common.deleteSuccess);
				li.remove();
			}
		}
	});
}

//原辅料和设备选择操作
function reagentAndCosChose() {
	$(".fa-search-plus").unbind("click").click(function() {
		var item = $(this).parents("li");
		if(item.hasClass("reagli")) {
			addReagent(item.attr("code"), item);
		}
		if(item.hasClass("cosli")) {
			addCos(item.attr("typeid"), item);
		}
	});
}

//选择原辅料批次
function addReagent(id, item) {
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		offset: ['10%', '10%'],
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/getStrogeReagent.action?id=" + id + "", ""],
		yes: function(index, layer) {
			var batch = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addReagent .chosed").children("td").eq(2).text();
			var sn = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addReagent .chosed").children("td").eq(4).text();
			item.find(".label-primary span").text(batch);
			item.find(".label-info span").text(sn);
			top.layer.close(index)
		},
	});
}
//选择设备批次
function addCos(id, item) {
	top.layer.open({
		title: biolims.common.selectInstrument,
		type: 2,
		offset: ['10%', '10%'],
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/equipment/main/selectCos.action?typeId=" + id + "", ""],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCos .chosed").children("td")
				.eq(1).text();
			var code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCos .chosed").children("td").eq(
				0).text();
			item.find(".label-default").text(name);
			item.attr("code", code);
			top.layer.close(index)
		},
	})
}

//原辅料的扫码
function ReagentScanCode() {
	$("#reagentScanInput").keypress(function(e) {
		var e = e || window.event;
		if(e.keyCode == "13") {
			$.ajax({
				type: "post",
				data: {
					sn: this.value
				},
				url: ctx + "/storage/getStorageBySn.action",
				success: function(data) {
					var data = JSON.parse(data).soi;
					if(data) {
						var batch = data.code;
						var sn = data.note;
						var id = data.storage.id;
						$(".reagli").each(function(i, v) {
							if(v.getAttribute('code') == id) {
								if(!$(v).find(".label-primary span").text()) {
									$(v).find(".label-primary span").text(batch);
									$(v).find(".label-info span").text(sn);
									return false;
								}
							}
						});
					} else {
						top.layer.msg(biolims.dna.noMatch);
					}

				}
			});
		}
	})

}
//设备的扫码
function cosScanCode() {
	var e = e || window.event;
	$("#cosScanInput").keypress(function(e) {
		var e = e || window.event;
		if(e.keyCode == "13") {
			$.ajax({
				type: "post",
				data: {
					id: this.value
				},
				url: ctx + "/equipment/main/getCosById.action",
				success: function(data) {
					var data = JSON.parse(data).ins;
					if(data) {
						var code = data.id;
						var name = data.name;
						var id = data.mainType.id;
						$(".cosli").each(function(i, v) {
							if(v.getAttribute('typeid') == id) {
								if(!$(v).find(".label-default").text()) {
									$(v).find(".label-default").text(name);
									v.setAttribute("code", code);
									return false;
								}
							}
						});
					} else {
						top.layer.msg(biolims.dna.noMatch);
					}

				}
			});
		}
	})
}

//获取样本并布置到孔板   
function renderPlate() {
	var overlay = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
	$(".showPlate").click(function() {
		if($(this).children(".fa").hasClass("fa-plus")) {
			var plateDiv = $(this).parents("#plateDiv");
			var ele = plateDiv.find(".plate");
			if(!ele.children("tr").length) {
				var ele = plateDiv.find(".plate");
				var eletable = plateDiv.find("#bloodSplitSample");
				plateDiv.append(overlay);
				var counts = plateDiv.find('.box-titlem').text();
				$.ajax({
					type: "post",
					data: {
						id: bloodSplit_id,
						counts: counts
					},
					url: ctx + "/experiment/plasma/bloodSplit/plateSample.action",
					success: function(data) {
						var data = JSON.parse(data);
						var list = data.list;
						showSamoleTable(counts, eletable);
						if(data.rowNum) {
							createShelf(data.rowNum, data.colNum, ele);
							showPlate(list, ele);
						}
						ele.parents("#plateDiv").find(".overlay").remove();
					}
				});
			}
		}
	});
	$(".addSamp").click(function() {
		var plateDiv = $(this).parents("#plateDiv");
		var eletr = plateDiv.find("#bloodSplitSample").find("tbody").find("tr");
		if(!eletr.length) {
			return false;
		}
		var titlePlate = plateDiv.find(".box-titlem").text();
		var arr = [];
		eletr.each(function(i, tr) {
			if(titlePlate) {
				arr.push(titlePlate + "-" + $(tr).children("td[savename='code']").text());
			} else {
				arr.push($(tr).children("td[savename='code']").text());
			}
		});
		$("#contentData").find("input[name='sampleCodes']").val(arr.join(","));
	});
}

/** 
 * 根据要求生成要求规格的架子或盒子
 * @param  m => 行
 * @param  n => 列
 */
function createShelf(m, n, element) {
	element.empty();
	var m = parseInt(m);
	var n = parseInt(n);
	//$(element).empty();
	var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
	var tr0 = document.createElement("tr");
	var num = "";
	for(var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for(var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for(var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m) + ' coord=' + arr[i] + jj + '></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for(var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}
}

//渲染已保存到孔板上的样本
function showPlate(list, ele) {
	for(var i = 0; i < list.length; i++) {
		var platePoint = ele.find("div[coord='" + list[i].posId + "']")[0];
		var color = list[i].color ? list[i].color : "#007BB6";
		platePoint.style.backgroundColor = color;
		platePoint.setAttribute("title", list[i].code);
		platePoint.innerText = list[i].blendCode;
	}
}
//渲染样本的datatables
function showSamoleTable(counts, eletable) {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "dicSampleTypeId",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dicSampleTypeId");
		},
	})
	colOpts.push({
		"data": "dicSampleTypeName",
		"title": biolims.common.dicSampleTypeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleTypeName");
			$(td).attr("dicSampleTypeId", rowData['dicSampleTypeId']);
		}
	})
	colOpts.push({
		"data": "blendCode",
		"title": biolims.common.mixNumber,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "blendCode");
		},
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})

	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
		//"className": "edit",
		"createdCell": function(td, data, rowdata) {
			$(td).parent("tr").css("background-color", rowdata.color);
			$(td).attr("saveName", "counts");
		},
	})
	colOpts.push({
		"data": "chromosomalLocation",
		"title": biolims.sanger.chromosomalLocation,
		"visible": flg,
		"createdCell": function(td) {
			$(td).attr("saveName", "chromosomalLocation");
		}
	})
	colOpts.push({
		"data": "nextId",
		"title": "下一步流向ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "nextId");
		}
	})
	colOpts.push({
		"data": "nextName",
		"title": "下一步流向",
		"createdCell": function(td,data,rowData) {
			$(td).attr("saveName", "nextName");
//			$(td).attr("nextId", rowData['nextId']);
		}
	})
	var tbarOpts = [];

	//下一步流向选择质检
	/*tbarOpts.push({
		text:biolims.common.nextFlow,
		action:function(){
			var rows = $("#bloodSplitSample .selected");
			if(!rows.length){
				top.layer.msg(biolims.common.pleaseSelectData);
				return false;
			}
			top.layer.open({
				title: biolims.common.selectNextFlow,
				type: 2,
				area: ["650px", "400px"],
				btn: biolims.common.selected,
				content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model="+'ZLJC', ''],
				yes: function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
						.eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
						0).text();
					rows.addClass("editagain");
					rows.find("td[savename='nextName']").text(name);
					rows.find("td[savename='nextId']").text(id);

					top.layer.close(index);
				},
			})
		}
	})*/

	//选择下一步流向之后保存保存
	/*tbarOpts.push({
		text:biolims.common.save,
		action:function(){
			var itemJson = saveItemjson($("#bloodSplitSample"));
			$.ajax({
				type: "post",
				data: {
					id:bloodSplit_id,
					itemJson:itemJson
				},
				url: ctx + "/experiment/plasma/bloodSplit/saveBloodSplitItem.action",
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.msg(biolims.common.saveSuccess);
						
					} else {
						top.layer.msg(biolims.common.saveFailed);
					}

				}
			});
		}
	})*/
	
	//提交质检按钮 成功后直接将数据带去质检模块
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-upload"></i>'+biolims.common.submissionQuality,
		action: function() {
			var rows = $("#bloodSplitSample .selected");
			if(!rows.length){
				top.layer.msg(biolims.common.pleaseSelectData);
				return false;
			}
			
			var tr = $(this[0].node).parents("#plateDiv").find("#bloodSplitSample").find("tbody").find("tr");
			var idList=[];
			tr.each(function(i,v){
				idList.push($(v).find(".icheck").val());
			});
		
			$.ajax({
				type: "post",
				data: {
					stepNum:$(".wizard_steps .selected").children(".step_no").text(),
					idList:idList,
					id:bloodSplit_id,
					zj:$("#zjType").attr("zjId"),
					mark:"BloodSplit"
				},
				url: ctx + "/experiment/plasma/bloodSplit/submitToQualityTest.action",
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.msg(biolims.common.submissionofSuccess);
						
					} else {
						top.layer.msg(biolims.common.submissionFailure);
					}

				}
			});
		}
	});
	/*tbarOpts.push({
		text: '<i class="glyphicon glyphicon-th"></i>'+"展示质检结果",
		action: function() {
		var sname=	$("#sample_detecyion_name").val();
		var sid=	$("#sample_detecyion_id").val();
		var es =$(".wizard_steps .selected").children(".step_descr").text();
	      	sampleDeteyionName=  encodeURI(encodeURI(sname));
	      	sampleDeteyion=  encodeURI(encodeURI(sid));
	      	experimentalSteps=  encodeURI(encodeURI(es));
			top.layer.open({
				
				title: biolims.common.pleaseChoose,
				type: 2,
				area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
				btn: biolims.common.selected,
				content: [window.ctx + "/experiment/quality/qualityTest/showSampledetecyionDialog.action?id="+bloodSplit_id+"&sampleDeteyionName="+sampleDeteyionName+"&sampleDeteyion="+sampleDeteyion+"&experimentalSteps="+experimentalSteps, ''],
				yes: function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .chosed").children("td").eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .chosed").children("td").eq(0).text();
					top.layer.close(index);
					$("#sample_detecyion_id").val(id);
					$("#sample_detecyion_name").val(name);
				},
			})
			
			
		}
	});*/
	var bloodSplitMakeUpAfOps = table(true, bloodSplit_id, "/experiment/plasma/bloodSplit/plateSampleTable.action?counts=" + counts + "", colOpts, tbarOpts);
	var bloodSplitMakeUpAfTab = renderData(eletable, bloodSplitMakeUpAfOps);
}

//获取明细表中的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			var kValue=json[k];
			json[k] = $(tds[j]).text();
			/*if(k == "nextName"){
				json["nextId"] = $(tds[j]).attr("nextId");
				alert(json["nextId"]);
				continue;
			}*/
			
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

//保存数据
function saveStepItem() {
	var changeLog = "样本处理-实验步骤：";
	//拼自定义字段儿（实验记录）
	var inputs = $("#contentData input");
	var options = $("#contentData option");
	var contentData = {};
	var checkboxArr = [];
	$("#contentData .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	//拼原辅料的数据
	var reagentli = $("#reagentBody li");
	var reagent = [];
	reagentli.each(function(i, val) {
		var reagentItem = {};
		reagentItem.id = val.getAttribute("reagentid");
		reagentItem.name = $(val).children(".text").text();
		reagentItem.code = val.getAttribute("code");
		reagentItem.itemId = val.getAttribute("itemid");
		reagentItem.batch = $(val).children(".label-primary").children("span").text();
		reagentItem.sn = $(val).children(".label-info").children("span").text();
		reagent.push(reagentItem);
	});
	//拼设备的数据
	var cosli = $("#cosBody li");
	var cos = [];
	cosli.each(function(i, val) {
		var cosItem = {};
		cosItem.id = val.getAttribute("cosid");
		cosItem.name = $(val).children(".label-default").text();
		cosItem.code = val.getAttribute("code");
		cosItem.itemId = val.getAttribute("itemid");
		cosItem.typeId = val.getAttribute("typeid");
		cos.push(cosItem);
	});
	template = {
		id: $(".wizard_steps .selected").attr("stepid"),
		contentData: contentData,
		startTime: $("#startTime").text(),
		endTime: $("#endTime").text(),
		testUserList: $("#testUserList").val()
	};
	//修改日志
	changeLog = getChangeLog(changeLog, reagent, cos);
	var changeLogs = "";
	if(changeLog != "样本处理-实验步骤：") {
		changeLogs = changeLog;
	}
	template = JSON.stringify(template);
	reagent = JSON.stringify(reagent);
	cos = JSON.stringify(cos);
	top.layer.load(4, {
		shade: 0.3
	});
	$.ajax({
		type: "post",
		url: ctx + "/experiment/plasma/bloodSplit/saveSteps.action",
		data: {
			id: $("#bloodSplit_id").text(),
			templateJson: template,
			reagentJson: reagent,
			cosJson: cos,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				sessionStorage.setItem("dijibu",$(".wizard_steps .selected").children(".step_no").text());
				window.location.reload();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	});

}
//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/plasma/bloodSplit/showBloodSplitItemTable.action?id=" + bloodSplit_id + "&bpmTaskId=" + $("#bpmTaskId").val();
	});
	//下一步操作
	$("#next").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/plasma/bloodSplit/showBloodSplitResultTable.action?id=" + bloodSplit_id + "&bpmTaskId=" + $("#bpmTaskId").val();
	});
}
/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/09
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(changeLog, reagent, cos) {
	//开始时间和结束时间
	$(".stepsTime").each(function(i, v) {
		var oldtxt = v.getAttribute("changelog") ? v.getAttribute("changelog") : "";
		var newtxt = v.innerText;
		var title = $(v).prev("button").text();
		if(oldtxt != newtxt) {
			changeLog += '"' + title + '"由"' + oldtxt + '"变为"' + newtxt + '";';
		}
	});

	//console.log(JSON.stringify(changelogCos));
	//原辅料
	reagent.forEach(function(v, i) {
		var id = v.id;
		var reagentFlag = true;
		changelogRea.forEach(function(vv, ii) {
			if(vv.id == id) {
				batch = vv.batch ? vv.batch : "";
				sn = vv.sn ? vv.sn : "";
				if(v.batch != batch || v.sn != sn) {
					changeLog += '原辅料名称"' + v.name + '":';
					if(v.batch != batch) {
						changeLog += '批次由"' + batch + '"变为"' + v.batch + '";';
					}
					if(v.sn != sn) {
						changeLog += 'sn由"' + sn + '"变为"' + v.sn + '";';
					}
				}
				reagentFlag = false;
			}
		});
		if(reagentFlag) {
			changeLog += '新增原辅料"' + v.name;
			if(v.batch) {
				changeLog += '":' + '批次为"' + v.batch + '";';
			}
			if(v.sn) {
				changeLog += '":' + 'sn为"' + v.sn + '";';
			} else {
				changeLog += '";';
			}
		}
	});
	changelogRea.forEach(function(v, i) {
		var id = v.id;
		var reagentRemoveFlag = true;
		reagent.forEach(function(vv, ii) {
			if(vv.id == id) {
				reagentRemoveFlag = false;
			}
		});
		if(reagentRemoveFlag) {
			changeLog += '删除原辅料:"' + v.name + '";';
		}
	});

	//设备
	cos.forEach(function(v, i) {
		var id = v.id;
		var cosFlag = true;
		changelogCos.forEach(function(vv, ii) {
			if(vv.id == id) {
				name = vv.name ? vv.name : "";
				code = vv.code ? vv.code : "";
				if(v.name != name || v.code != code) {
					changeLog += '设备"' + vv.type.name + '":';
					if(v.name != name) {
						changeLog += '名称由"' + name + '"变为"' + v.name + '";';
					}
					if(v.code != code) {
						changeLog += '编码由"' + code + '"变为"' + v.code + '";';
					}
				}
				cosFlag = false;
			}
		});
		if(cosFlag) {
			changeLog += '新增设备"' + v.name;
			if(v.name) {
				changeLog += '";' + '名称为"' + v.name + '";';
			}
			if(v.code) {
				changeLog += '";' + '编码为"' + v.code + '";';
			} else {
				changeLog += '";';
			}
		}
	});
	changelogCos.forEach(function(v, i) {
		var id = v.id;
		var cosRemoveFlag = true;
		cos.forEach(function(vv, ii) {
			if(vv.id == id) {
				cosRemoveFlag = false;
			}
		});
		if(cosRemoveFlag) {
			changeLog += '删除设备："' + v.type.name + '";';
		}
	});
	//自定义字段儿
	$("#contentData .form-control").each(function(i, v) {
		if(v.localName == "text") {
			var oldtxt = v.getAttribute("changelog");
			var newtxt = v.value;
			if(oldtxt != newtxt) {
				changeLog += '实验记录："' + $(v).prev("span").text() + '"由"' + oldtxt + '"变为"' + newtxt + '";';
			}
		}
		if(v.localName == "select") {
			var oldtxt = v.getAttribute("changelog");
			var newtxt = $(v).find("option:selected").val();
			if(oldtxt != newtxt) {
				var oldtitle = $(v).find("option[value=" + oldtxt + "]").text();
				var newtitle = $(v).find("option[value=" + newtxt + "]").text();
				changeLog += '实验记录："' + $(v).prev("span").text() + '"由"' + oldtitle + '"变为"' + newtitle + '";';
			}

		}
	});
	$("#contentData .checkboxs").each(function(i, v) {
		var oldarr = v.getAttribute("changelog").split(",");
		var newarr = [];
		var newtitle = [];
		var oldtitle = [];
		var flag = 1;
		$(v).find("input").each(function(ii, inp) {
			if(inp.checked == true) {
				newarr.push(inp.value);
				newtitle.push(inp.title);
			}
		});
		if(oldarr[0]) {
			oldarr.forEach(function(vv, ii) {
				oldtitle.push($(v).find("input[value=" + vv + "]").attr("title"));
			});
		}
		newarr.forEach(function(vvv, iii) {
			if(vvv !== oldarr[iii]) {
				flag++;
				if(flag == 2) {
					changeLog += '实验记录："' + $(v).children(".input-group-addon").text() + '"由"' + oldtitle + '"变为"' + newtitle + '";';
				}
			}
		});
	});
	if(changeLog) {
		changeLog = $(".wizard_steps .selected").find(".step_descr").text() + ":" + changeLog;
	}
	return changeLog;
}

function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#bloodSplit_id").text();

	top.layer.open({
		title: biolims.common.detailedExaminationAndApproval,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/workflow/processinstance/toCompleteTaskView.action?taskId=" +
			taskId + "&formId=" + formId,
		yes: function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
				.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
				.find("#opinionVal").val();

			if(!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if(operVal == "1") {
				top.layer.msg(biolims.dna.pleaseFinish);
				return false;
			}
			if(operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data: JSON.stringify(paramData),
					formId: formId,
					taskId: taskId,
					userId: window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
					reqData,
					function(data) {
						if(data.success) {
							top.layer.msg(biolims.common.submitSuccess);
							if(typeof callback == 'function') {}
						} else {
							top.layer.msg(biolims.common.submitFail);
						}
					}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}
//选择实验员
function showdutyManId() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#testUserList").val(name);
		},
	})
}
//选择检测项
function showSampledetecyion() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/detecyion/sampleDeteyion/showSampledetecyion.action?", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#sample_detecyion_id").val(id);
			$("#sample_detecyion_name").val(name);
		},
	})
}

//判断如果是列表页进来的，直接跳到当前步骤
function renderOrderNums() {
	var dijibu=sessionStorage.getItem("dijibu");
	if(dijibu){
		var orderNums =dijibu;
	}else{
		var orderNums = $("#orderNumBy").val();
	}
	if(orderNums && orderNums != 1) {
		$(".wizard_steps .step").each(function(i, v) {
			if($(v).children(".step_no").text() == orderNums) {
				$(v).click();
			}
		});
	}
}

//渲染质检
function renderZhijian(zhijian) {
	var zhijianLis = "";
	var id = zhijian.id ? zhijian.id : "";
	var code = zhijian.code ? zhijian.code : "";
	var name = zhijian.name ? zhijian.name : "";
	console.log(name);
	var itemId = zhijian.itemId ? zhijian.itemId : "";
	var nextId = zhijian.nextId ? zhijian.nextId : "";
	zhijianLis += '<li class="zhijianli zhijianOld" id=' + id + '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span name=\''+name+'\' nextid=\''+nextId+'\' zhijianid=\'' + code + '\' class="zhijianName">' + name + '</span></small><small><i testId=\''+code+'\' stepNum=\''+itemId+'\' onclick="showResult(this)" class="fa fa-search-plus pull-right"></i></small></li>';
	$("#zhijianBody").append(zhijianLis);
}

//展示质检提交到哪个质检的结果内容
function showResult(self){
	var stepNum = $(self).attr("stepNum");
	var testId = $(self).attr("testId");
	var mark = "BloodSplit";
	top.layer.open({
		title: "质检结果",
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/experiment/quality/qualityTest/showQualityTestResultListForzj.action?" +
				"stepNum="+stepNum+"&testId="+testId+"&mark="+mark, ''],
		yes: function(index, layer) {
			top.layer.closeAll();
		},
	})
}