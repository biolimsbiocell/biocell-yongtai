var bloodSplitReceiveTab;
var oldChangeLog;
$(function() {
	var cols = [];
	cols.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
//	cols.push({
//		"data": "sampleCode",
//		"title": biolims.common.sampleCode,
//		"createdCell": function(td, data, rowData) {
//			$(td).attr("saveName", "sampleCode");
//			$(td).attr("releaseAuditId", rowData['releaseAuditId']);
//		}
//	})
	cols.push({
		"data": "sampleCode",
		"title": "样本编号",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleCode");
			$(td).attr("releaseAuditId", rowData['releaseAuditId']);
			$(td).attr("batchNumber", rowData['batchNumber']);
			$(td).attr("sampleOrder-id", rowData['sampleOrder-id']);
		}
	});
	cols.push({
		"data": "batchNumber",
		"title": "产品批号",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "batchNumber");
		}
	});
	cols.push({
		"data": "inspectorOne",
		"title": "第一次审核人",
		"createdCell": function(td) {
			$(td).attr("saveName", "inspectorOne");
		}
	})
	cols.push({
		"data" : "testingDateOne",
		"title" : "第一次审核日期",
		"createdCell" : function(td) {
			$(td).attr("saveName", "testingDateOne");
		}
	})
	cols.push({
		"data": "auditPassOne",
		"title": "第一次审核结果",
		"name": "通过|不通过",
		"createdCell": function(td) {
			$(td).attr("saveName", "auditPassOne");
			$(td).attr("selectOpt", "通过|不通过");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "不通过";
			}
			if(data == "1") {
				return "通过";
			} else {
				return '';
			}
		}
	});
	cols.push({
		"data": "inspectorTwo",
		"title": "第二次审核人",
		"createdCell": function(td) {
			$(td).attr("saveName", "inspectorTwo");
		}
	})
	cols.push({
		"data" : "testingDateTwo",
		"title" : "第二次审核日期",
		"createdCell" : function(td) {
			$(td).attr("saveName", "testingDateTwo");
		}
	})
	cols.push({
		"data": "auditPassTwo",
		"title": "第二次审核结果",
		"name": "通过|不通过",
		"createdCell": function(td) {
			$(td).attr("saveName", "auditPassTwo");
			$(td).attr("selectOpt", "通过|不通过");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "不通过";
			}
			if(data == "1") {
				return "通过";
			} else {
				return '';
			}
		}
	});
	cols.push({
		"data": "inspectorThree",
		"title": "第三次审核人",
		"createdCell": function(td) {
			$(td).attr("saveName", "inspectorThree");
		}
	})
	cols.push({
		"data" : "testingDateThree",
		"title" : "第三次审核日期",
		"createdCell" : function(td) {
			$(td).attr("saveName", "testingDateThree");
		}
	})
	cols.push({
		"data": "auditPassThree",
		"title": "第三次审核结果",
		"name": "通过|不通过",
		"createdCell": function(td) {
			$(td).attr("saveName", "auditPassThree");
			$(td).attr("selectOpt", "通过|不通过");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "不通过";
			}
			if(data == "1") {
				return "通过";
			} else {
				return '';
			}
		}
	});
//	cols.push({
//		"data": "productId",
//		"title": biolims.master.productId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "productId");
//		}
//	})
//	cols.push({
//		"data": "productName",
//		"title": biolims.master.product,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "productName");
//		}
//	})
//	cols.push({
//		"data": "sampleOrder-id",
//		"title":"关联订单",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "sampleOrder-id");
//		}
//	});
//	cols.push({
//		"data": "bagsNum",
//		"title": "回输袋数",
//		"className": "edit",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "bagsNum");
//		}
//	})
//	cols.push({
//		"data": "specifications",
//		"title": "规格（ml/袋）",
//		"className": "edit",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "specifications");
//		}
//	})
//	cols.push({
//		"data": "result",
//		"title": "肉眼观察是否合格",
//		"className": "select",
//		"name": "是|否",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "result");
//			$(td).attr("selectOpt", "是|否");
//		},
//		"render": function(data, type, full, meta) {
//			if(data == "0") {
//				return "否";
//			}
//			if(data == "1") {
//				return "是";
//			} else {
//				return '';
//			}
//		}
//	})
//	cols.push({
//		"data": "packageResult",
//		"title": "外包装是否完好，无渗漏",
//		"className": "select",
//		"name": "是|否",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "packageResult");
//			$(td).attr("selectOpt", "是|否");
//		},
//		"render": function(data, type, full, meta) {
//			if(data == "0") {
//				return "否";
//			}
//			if(data == "1") {
//				return "是";
//			} else {
//				return '';
//			}
//		}
//	})
	cols.push({
		"data": "cellNumber",
		"title": "(细胞数量)*10^9个",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "cellNumber");
		}
	});
	cols.push({
		"data": "specifications",
		"title": "规格（ml/袋）",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "specifications");
		}
	})
	cols.push({
		"data": "sampleOrder-crmCustomer-name",
		"title": "医院",
//		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-crmCustomer-name");
		}
	});
	cols.push({
		"data": "sampleOrder-inspectionDepartment-name",
		"title": "科室",
//		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-inspectionDepartment-name");
		}
	});
	cols.push({
		"data": "inspector",
		"title": "检验人",
//		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "inspector");
		}
	})
	cols.push({
		"data" : "testingDate",
		"title" : "检测日期",
		"className" : "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "testingDate");
		}
	})
	cols.push({
		"data": "reviewer",
		"title": "复核人",
//		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "reviewer");
		}
	})
	cols.push({
		"data" : "reviewDate",
		"title" : "复核日期",
		"className" : "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "reviewDate");
		}
	})
//	cols.push({
//		"data": "nextFlowId",
//		"title": biolims.common.nextFlowId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "nextFlowId");
//		}
//	})
//	cols.push({
//		"data": "nextFlow",
//		"title": biolims.common.nextFlow,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "nextFlow");
//		}
//	})
	cols.push({
		"data": "note",
		"title": biolims.common.note,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	});
	cols.push({
		"data": "auditPass",
		"title": "放行审核是否通过",
//		"className": "select",
		"name": "是|否",
		"createdCell": function(td) {
			$(td).attr("saveName", "auditPass");
			$(td).attr("selectOpt", "是|否");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "否";
			}
			if(data == "1") {
				return "是";
			} else {
				return '';
			}
		}
	})
	 cols.push({
		"title": "放行审核",
		"width": "100px",
		"data": null,
		"createdCell": function(td, data) {
		},
		"render": function(data, type, row, meta) {
			if(row["releaseAuditId"]!=null&row["releaseAuditId"]!=""){
				return '<input type="button" value="'+row["releaseAuditId"]+'" onClick="fangxing(this);">'
			}else{
				return '<input type="button" value="生成放行审核" onClick="fangxing(this);">'
			}
		},
	});
	cols.push({
		"title": "打印放行审核单",
		"width": "100px",
		"data": null,
		"createdCell": function(td, data) {
		},
		"render": function(data, type, row, meta) {
			if(row["sampleCode"]!=null&row["sampleCode"]!=""){
				var sampleCode=row["sampleCode"];
				var auditPassOne=row["auditPassOne"];
				var inspectorOne=row["inspectorOne"];
				var testingDateOne=row["testingDateOne"];
				return '<input type="button" auditPassOne="'+auditPassOne+'"  inspectorOne="'+inspectorOne+'"  testingDateOne="'+testingDateOne+'"  code="'+sampleCode+'"  value="打印" onClick="dayinShD(this);">'
			}
		},
	});
	var tbarOpts = [];
	tbarOpts.push({
		text: "第一次审核",//"未确认",
		action:function(){
			showtables(1);
		}
	});
	tbarOpts.push({
		text: "第二次审核",//"已确认",
		action:function(){
			showtables(2);
		}
	});
	tbarOpts.push({
		text: "第三次审核",
		action:function(){
			showtables(3);
		}
	});
	tbarOpts.push({
		text: "已通过",
		action:function(){
			showtables(4);
		}
	});
	tbarOpts.push({
		text: "未通过",
		action:function(){
			showtables(0);
		}
	});
	
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-search"></i> ' + biolims.common.search,
		action: function() {
			search()
		}
	});
	tbarOpts.push({
		text: "选择检验人",
		action:function(){
			showProductionUser2();
		}
	});
	tbarOpts.push({
		text: "选择复核人",
		action:function(){
			showProductionUser();
		}
	});
//	tbarOpts.push({
//		text: '<i class="fa fa-ioxhost"></i> ' + biolims.common.batchResult,
//		className: 'btn btn-sm btn-success resultsBatchBtn',
//	});
//	tbarOpts.push({
//		text: '<i class="fa fa-paypal"></i> ' + biolims.common.selectNextFlow,
//		action: function() {
//			nextFlow();
//		}
//	});
//	tbarOpts.push({
//		text: '<i class="fa fa-paypal"></i> ' + "提交质检",
//		action: function() {
//			nextFlow();
//		}
//	});
//	tbarOpts.push({
//		text: "打印成品外观检查记录",
//		action: function() {
//			dayin();
//		}
//	});
	tbarOpts.push({
		text: "打印成品放行审核",
		action: function() {
			dayin2();
		}
	});
	tbarOpts.push({
		text: "打印成品放行通知单",
		action: function() {
			dayin3();
		}
	});
	tbarOpts.push({
		text: "批量打印记录",
		action: function() {
			dayinpijilu();
		}
	});
	tbarOpts.push({
		text: "审核通过",
		action: function() {
			queren();
		}
	});
	tbarOpts.push({
		text: "审核不通过",
		action: function() {
			querenbu();
		}
	});
	 tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#bloodSplitReceiveDiv"))
			}
	});
	var bloodSplitReceiveOpts = table(true, null, "/experiment/plasma/sampleReceiveEx/showSampleReceiveItemTableJson.action?type="+$("#bloodSplitReceive_type").val(), cols, tbarOpts)
	//渲染样本的table

	bloodSplitReceiveTab=renderData($("#bloodSplitReceiveDiv"),bloodSplitReceiveOpts);
    //dwb 日志功能 2018-05-11 12:31:59
	bloodSplitReceiveTab.on('draw', function() {
		oldChangeLog = bloodSplitReceiveTab.ajax.json();
	});
//	//批量结果
//	btnChangeDropdown($('#bloodSplitReceiveDiv'), $(".resultsBatchBtn"), [biolims.common.qualified, biolims.common.disqualified], "result");
	//恢复搜索状态
	$('#bloodSplitReceiveDiv').on('init.dt', function() {
		recoverSearchContent(bloodSplitReceiveTab);
	})
});
function showtables(aa){
	bloodSplitReceiveTab.settings()[0].ajax.data = {
		type:aa
	};
	$("#bloodSplitReceive_type").val(aa);
	bloodSplitReceiveTab.ajax.url("/experiment/plasma/sampleReceiveEx/showSampleReceiveItemTableJson.action").load();
	oldChangeLog = bloodSplitReceiveTab.ajax.json();
	if(aa=="1"){
		$("#titleNote").text("第一次审核");
	}
	if(aa=="2"){
		$("#titleNote").text("第二次审核");
	}
	if(aa=="3"){
		$("#titleNote").text("第三次审核");
	}
	if(aa=="4"){
		$("#titleNote").text("审核通过");
	}
	if(aa=="0"){
		$("#titleNote").text("审核未通过");
	}
//	if(aa=="2"){
//		$("#titleNote").text("待放行审核");
//	}
//	if(aa=="1"){
//		$("#titleNote").text("已放行审核");
//	}
//	if(aa=="0"){
//		$("#titleNote").text("放行审核未通过");
//	}
}
//放行审核不通过
function querenbu() {
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	if($("#titleNote").text()=="审核未通过"
		||$("#titleNote").text()=="审核通过"){
		top.layer.msg("该状态不允许点击该按钮！");
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find(".icheck").val());
	});
	var zt = "";
	if($("#titleNote").text()=="第一次审核"){
		zt = "1";
	}else if($("#titleNote").text()=="第二次审核"){
		zt = "2";
	}else if($("#titleNote").text()=="第三次审核"){
		zt = "3";
	}
	$.ajax({
		type : "post",
		data : {
			ids : ids,
			zt : zt,
		},
		url : ctx+ "/experiment/plasma/sampleReceiveEx/confirmReinfusionbu.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.success) {
				bloodSplitReceiveTab.ajax.reload();
				top.layer.msg("确认审核不通过成功！");
			} else {
				top.layer.msg("不通过失败！");
			}

		}
	});
}
//确认放行审核
function queren() {
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	if($("#titleNote").text()=="审核未通过"
		||$("#titleNote").text()=="审核通过"){
		top.layer.msg("该状态不允许点击该按钮！");
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find(".icheck").val());
	});
	var zt = "";
	if($("#titleNote").text()=="第一次审核"){
		zt = "1";
	}else if($("#titleNote").text()=="第二次审核"){
		zt = "2";
	}else if($("#titleNote").text()=="第三次审核"){
		zt = "3";
	}
	$.ajax({
		type : "post",
		data : {
			ids : ids,
			zt : zt,
		},
		url : ctx+ "/experiment/plasma/sampleReceiveEx/confirmReinfusion.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.success) {
				bloodSplitReceiveTab.ajax.reload();
				top.layer.msg("确认成功！");
			} else {
				top.layer.msg("确认失败！");
			}

		}
	});
}
function dayinpijilu(){
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(length!=1) {
		top.layer.msg("请选择一条数据");
		return false;
	}
	//订单编号
	var ordernum = "";
	$.each(rows, function(j, k) {
		ordernum = $(k).find("td[savename='sampleCode']").text();
	});
	top.layer.open({
		title: "打印批记录",
		type: 2,
		area: ["650px", "400px"],
		btn: "关闭",
		content: [window.ctx + "/experiment/plasma/sampleReceiveEx/showPiChuLi.action?ordernum=" +
		          ordernum, ''
		],
		yes: function(index, layer) {
//			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
//				.eq(1).text();
//			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
//				0).text();
//			rows.addClass("editagain");
//			rows.find("td[savename='nextFlow']").text(name);
//			rows.find("td[savename='nextFlowId']").text(id);

			top.layer.close(index)
		},
	})
}
function fangxing(that){
	var itemid = $(that).parent("td").parent("tr").children("td").find(".icheck").eq(0).val();
	var code = $(that).parent("td").parent("tr").children("td").eq(1).text();
	if($(that).val()=="生成放行审核"){
		$.ajax({
			type : "post",
			data : {
				id : "NEW",
				itemId : itemid,
				code : code
			},
			url : ctx+ "/experiment/plasma/sampleReceiveEx/toEditReleaseAudit.action",
			success : function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					$("#maincontentframe",
							window.parent.document)[0].src = window.ctx
							+ "/experiment/ReleaseAudit/ReleaseAudit/toEditReleaseAudit.action?id="
							+ data.id;
//					myTable.ajax.reload();
				} else {
					top.layer.msg("生成失败！")
				}

			}
		});
	}else{
		$.ajax({
			type : "post",
			data : {
				id : $(that).val(),
				itemId : itemid,
				code : code
			},
			url : ctx+ "/experiment/plasma/sampleReceiveEx/toEditReleaseAudit.action",
			success : function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					$("#maincontentframe",
							window.parent.document)[0].src = window.ctx
							+ "/experiment/ReleaseAudit/ReleaseAudit/toEditReleaseAudit.action?id="
							+ data.id;
//					myTable.ajax.reload();
				} else {
					top.layer.msg("生成失败！")
				}

			}
		});
	}
}
function dayinShD(that){
	
	
	var sampleCode =$(that).attr("code")
	var testingDateOne =$(that).attr("testingDateOne")
	var auditPassOne =$(that).attr("auditPassOne")
	var inspectorOne =$(that).attr("inspectorOne")
	
	if((inspectorOne!=""&&inspectorOne!=null)&&(testingDateOne!=""&&testingDateOne!=null)){
		url= '__report=fangxingshenhe2.rptdesign&id=' + sampleCode;	
//	}else if($(that).attr("auditPassOne")){
//		top.layer.msg("没有第一次审核结果")
	}else{
		url= '__report=fangxingshenhe1.rptdesign&id=' + sampleCode;	
	}
	
	commonPrint(url);
	
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '', '');
}
$("#save").click(function() {
	
	var reg =/^[0-9]*$/;
	var flag1 = true
	var flag2 = true
	$("#main tbody tr").each(function(i,val){
    	var tds = $(val).find("td[savename='cellNumber']").text();
    	if(!reg.test(tds) && tds!=""){
    		top.layer.msg("请输入正确的细胞数量(数字)!");
    		flag1 = false;
    		return false;
    	}
    });
	if(flag1 == false){
		top.layer.msg("请输入正确的细胞数量(数字)!");
		return false;
	}
	
	$("#main tbody tr").each(function(i,val){
    	var tds = $(val).find("td[savename='specifications']").text();
    	if(!reg.test(tds) && tds!=""){
    		top.layer.msg("请输入正确的规格(数字)!");
    		flag2 = false;
    		return false;
    	}
    });
	if(flag2 == false){
		top.layer.msg("请输入正确的规格(数字)!");
		return false;
	}
	
	if(	$("#titleNote").text()=="已放行审核"){
		top.layer.msg("已放行审核数据不能保存！");
	}else if(	$("#titleNote").text()=="放行审核未通过"){
		top.layer.msg("放行审核未通过数据不能保存！");
	}else {
		var data = saveItemjson($("#bloodSplitReceiveDiv"));
		var changeLog = "待交接样本：";
		changeLog = getChangeLog(data, $("#bloodSplitReceiveDiv"), changeLog);
		var changeLogs = "";
		if(changeLog != "待交接样本："){
			changeLogs=changeLog;
		}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
			type: 'post',
			url: ctx + "/experiment/plasma/sampleReceiveEx/saveBloodSplitReceive.action",
			data: {
				dataJson: data,
				changeLog:changeLogs
			},
			success: function(data) {
				var data = JSON.parse(data);
				console.log(data)
				if(data.success) {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveSuccess);
				}
			}
		})
	}
	
});
//dwb 日志功能  2018-05-11 12:35:26
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断男女并转换为数字
			if(k == "result") {
				var result = $(tds[j]).text();
				if(result == "否") {
					json[k] = "0";
				} else if(result == "是") {
					json[k] = "1";
				}
				continue;
			}
			if(k == "packageResult") {
				var packageResult = $(tds[j]).text();
				if(packageResult == "否") {
					json[k] = "0";
				} else if(packageResult == "是") {
					json[k] = "1";
				}
				continue;
			}
			if(k == "auditPass") {
				var auditPass = $(tds[j]).text();
				if(auditPass == "否") {
					json[k] = "0";
				} else if(auditPass == "是") {
					json[k] = "1";
				} else{
					json[k] = "";
				}
				continue;
			}
			
			if(k == "sampleCode") {
				json["releaseAuditId"] = $(tds[j]).attr("releaseAuditId");
				json["batchNumber"] = $(tds[j]).attr("batchNumber");
				json["sampleOrder-id"] = $(tds[j]).attr("sampleOrder-id");
				
			}
			
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//下一步操作
$("#next").click(function() {
	var bloodSplit_id = $("#bloodSplit_id").text();
	if(bloodSplit_id == "NEW") {
		top.layer.msg(biolims.common.pleaseSaveRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/plasma/bloodSplit/showBloodSplitItemTable.action?id=" + bloodSplit_id;
	//	window.location.href=ctx+"/experiment/plasma/bloodSplit/showWellPlate.action?id="+bloodSplit_id;
});
//下一步流向
function nextFlow() {
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var productId = "";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
	});
	top.layer.open({
		title: biolims.common.selectNextFlow,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=PlasmaReceiveItem&productId=" +
			productIds, ''
		],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='nextFlow']").text(name);
			rows.find("td[savename='nextFlowId']").text(id);

			top.layer.close(index)
		},
	})
}

function executeAbnormal() {
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find(".icheck").val());
	});
	$.ajax({
		type: 'post',
		url: ctx + "/experiment/plasma/sampleReceiveEx/executeReceive.action",
		data: {
			ids: ids
		},
		success: function(data) {
			var data = JSON.parse(data);
			console.log(data)
			if(data.success) {
				top.layer.msg(biolims.common.handleok);
			}
		}
	})
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "产品批号",
			"type": "input",
			"searchName": "sampleCode"
		}
	,{
		"type":"table",
		"table":bloodSplitReceiveTab
	}];

}

function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layer) {
			var arr = $("#checkCode",parent.document).val().split("\n");
//			var codes=JSON.stringify(arr);
			bloodSplitReceiveTab.settings()[0].ajax.data = {"codes":arr};
			bloodSplitReceiveTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
function dayin2(){
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(length!=1) {
		top.layer.msg("请选择一条数据");
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("td[savename='sampleCode']").attr("releaseAuditId"));
	});	

	var id = ids.join(',');
	$.ajax({
		type : "post",
		data : {
			id : id,
		},
		url : ctx
				+ "/experiment/ReleaseAudit/ReleaseAudit/selectDate.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.da) {
				$.ajax({
					type : "post",
					data : {
						id : $("#sampleReveice_id").text(),
						confirmDate : data.da,
						modelId:"cpfxsh"
					},
					url : ctx
							+ "/stamp/birtVersion/selectBirt.action",
					success : function(data) {
						var data = JSON.parse(data)
						if (data.reportN) {
							var url = '__report='+data.reportN+'&id=' + id;	
							commonPrint(url);
						} else {
							top.layer.msg("没有报表信息！");
						}
					}
				});
				
				
				
			} else {
				top.layer.msg("审核结论日期没有选择！");
			}
		}
	});
	
//	var url = '__report=cpfxshd.rptdesign&id=' + id;
//	commonPrint(url);
}
function dayin3(){
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(length!=1) {
		top.layer.msg("请选择一条数据");
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("td[savename='sampleCode']").attr("releaseAuditId"));
	});
	var id = ids.join(',');
	$.ajax({
		type : "post",
		data : {
			id : id,
		},
		url : ctx
				+ "/experiment/ReleaseAudit/ReleaseAudit/selectDate1.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.da) {
				$.ajax({
					type : "post",
					data : {
						id : $("#sampleReveice_id").text(),
						confirmDate : data.da,
						modelId:"cpfxtzd"
					},
					url : ctx
							+ "/stamp/birtVersion/selectBirt.action",
					success : function(data) {
						var data = JSON.parse(data)
						if (data.reportN) {
							var url = '__report='+data.reportN+'&id=' + id;	
							commonPrint(url);
						} else {
							top.layer.msg("没有报表信息！");
						}
					}
				});
				
				
				
			} else {
				top.layer.msg("审批结论日期没有选择！");
			}
		}
	});
//	var url = '__report=cpfxtzd.rptdesign&id=' + id;
//	commonPrint(url);
}
function dayin(){
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find(".icheck").val());
	});
	var id = ids.join(',');
	var url = '__report=cpwgjc2.rptdesign&id=' + id;
	commonPrint(url);
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '_blank', '');
}
//选择检验人
function showProductionUser2() {
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer.open({
		title : "请选择检验人",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=QC001",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			
			top.layer.close(index);
			rows.addClass("editagain");
			rows.find("td[savename='inspector']").text(name);
			top.layer.close(index)
		},
	})
}
//选择复核人
function showProductionUser() {
	var rows = $("#bloodSplitReceiveDiv .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer.open({
		title : "请选择复核人",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=QA006",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			
			top.layer.close(index);
			rows.addClass("editagain");
			rows.find("td[savename='reviewer']").text(name);
			top.layer.close(index)
		},
	})
}


