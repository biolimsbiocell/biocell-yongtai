var bloodSplitManageTab
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "bloodSampleTask-id",
		"title": biolims.common.relatedMainTableId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "bloodSampleTask-id");
		},
	})
	colOpts.push({
		"data": "bloodSampleTask-name",
		"title": biolims.common.relatedMainTableName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "bloodSampleTask-name");
			$(td).attr("bloodSampleTask-id", rowData['bloodSampleTask-id']);
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.master.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "dicSampleTypeId",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dicSampleTypeId");
		},
	})
	colOpts.push({
		"data": "dicSampleTypeName",
		"title": biolims.common.dicSampleTypeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleTypeName");
			$(td).attr("dicSampleTypeId", rowData['dicSampleTypeId']);
		}
	})
	colOpts.push({
		"data": "productNum",
		"title": biolims.common.productNum,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productNum");
		},
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "counts");
		},
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.find,
		action: function() {
			search();
		}
	});
	tbarOpts.push({
		text: biolims.common.order,
		action: function() {
			viewBloodSplit();
		}
	});
	tbarOpts.push({
		text: biolims.common.batchInLib,
		action: function() {
			ruku();
		}
	});
	var bloodSplitManageOps = table(true, null, "/experiment/plasma/plasmaManage/showBloodSplitManageJson.action", colOpts, tbarOpts);
	bloodSplitManageTab = renderData($("#bloodSplitManageTab"), bloodSplitManageOps);
	//上一步下一步操作
	preAndNext();
});

//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/plasma/bloodSplit/showBloodSplitSteps.action?id=" + $("#bloodSplit_id").text();
	});
	//上一步操作
	$("#finish").click(function() {
	var paraStr = "formId=" + $("#bloodSplit_id").text() +
		"&tableId=PlasmaTask";
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: '13',
					formId: $("#bloodSplit_id").text()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self'); 
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.close(index);
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});
});
}
function ruku(){
	var rows = $("#bloodSplitManageTab .selected");
	if(rows.length>0){
		var ids=[]; 
		$.each(rows,function(i,j){
			ids.push($(j).find(".icheck").val());
		});
		console.log(ids)
		ajax("post", "/experiment/plasma/plasmaManage/plasmaManageItemRuku.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				tableRefresh();
				top.layer.msg(biolims.plasma.waitingInstorage)
			} else{
				top.layer.msg(biolims.plasma.warehousingFailed)
			}
		}, null);
	}else{
		top.layer.msg(biolims.common.pleaseSelectData);
	}
}
//查看任务单
function viewBloodSplit(){
	var id = $(".selected").find("td[savename='bloodSampleTask-id']").text();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/plasma/bloodSplit/showBloodSplitSteps.action?id=" + id;
}

//弹框模糊查询参数
function searchOptions() {
	return [
		{
			"txt": biolims.common.sampleCode,
			"type": "input",
			"searchName": "sampleCode"
		},{
			"txt": biolims.common.code,
			"type": "input",
			"searchName": "code"
		},
		{
			"txt": biolims.common.name,
			"type": "input",
			"searchName": "bloodSampleTask-name",
		},
		{
			"txt": biolims.common.relatedMainTableId,
			"type": "input",
			"searchName": "bloodSampleTask-id",
		},
	{
		"type":"table",
		"table":bloodSplitManageTab
	}];
}