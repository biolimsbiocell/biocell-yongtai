function confirmMsg(msg, title, callback) {
	if (title)
		title = "请确认";
	var result = false;
	Ext.MessageBox.confirm(title, msg, function(button, text) {
		if (button == "yes") {
			callback();
		}
	});
}

/**
 * 消息提示
 * 
 * @return
 */
function message(messg) {
	if (messg.length > 0) {
		Ext.MessageBox.show({
			title : '消息',
			msg : messg,
			buttons : Ext.MessageBox.OK,
			icon : Ext.MessageBox.INFO
		});
		return;
	}
}

/**
 * 提交审批
 */
window.submitWorkflow = function(formName, datas, callback) {
	var option = {};
	option.width = 929;
	option.height = 534;
	var url = "/workflow/processinstance/toStartView.action?formName=" + formName;
	var dialogWin = loadDialogPage(null, "提交审批", url, {
		"提交" : function() {
			datas.formName = formName;
			ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
				if (data.success) {
					message("提交成功！");
					if (typeof callback == 'function') {
						callback(data);
					}
					dialogWin.dialog("close");
				} else {
					message("提交失败！");
				}
			}, null);
		}
	}, true, option);
};
window.submitWorkflowByProcessKey = function(formName,processKey, datas, callback) {
	var option = {};
	option.width = 929;
	option.height = 534;
	var url = "/workflow/processinstance/toStartViewByProcessKey.action?processKey=" + processKey;
	var dialogWin = loadDialogPage(null, "提交审批", url, {
		"提交" : function() {
			datas.formName = formName;
			ajax("post", "/workflow/processinstance/startByProcessKey.action", datas, function(data) {
				if (data.success) {
					message("提交成功！");
					if (typeof callback == 'function') {
						callback(data);
					}
					dialogWin.dialog("close");
				} else {
					message("提交失败！");
				}
			}, null);
		}
	}, true, option);
};
/**
 * 签收审批任务
 */
window.claimTask = function(datas, callback) {
	confirmMessage("您确定签收当前审批任务吗？", function() {
		ajax("post", "/workflow/processinstance/claimTask.action", datas, function(data) {
			if (data.success) {
				message("提交成功！");
				if (typeof callback == 'function') {
					callback(data);
				}
			} else {
				message("提交失败！"+data.error);
			}
		}, null);
	});
};

window.completeTask = function(formId, taskId, callback, datas) {
	var options = {};
	options.width = 929;
	options.height = 534;
	
	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}

	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, "审批任务", url, {
		"确定" : function() {
			
			var operVal = $("#oper").val();
			if(!operVal){
				message("请选择操作！");
				return false;
			}
			
			
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = datas || {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				
				
				_complete(reqData, callback, dialogWin);
			}
		},
		"查看流程图" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
};

function _trunTodoTask(taskId, callback, dialogWin) {
	if (!$("#shift_handle").val()) {
		message("请选择转办人!");
		return;
	}
	ajax("post", "/workflow/processinstance/shiftHandleTask.action", {
		taskId : taskId,
		shiftUserId : $("#shift_handle").val()
	}, function(data) {
		if (data.success) {
			message("转办成功！");
			if (typeof callback == 'function') {
				callback(data);
			}
			dialogWin.dialog("close");
		} else {
			message("提交失败！");
		}
	}, null);
}

function _complete(paramData, callback, dialogWin) {
	confirmMsg("您确定继续操作吗？", null, function() {

		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		ajax("post", "/workflow/processinstance/completeTask.action", paramData, function(data) {
			if (data.success) {
				loadMarsk.hide();
				message("办理成功！");
				if (typeof callback == 'function') {
					callback(data);
				}
				dialogWin.dialog("close");
			} else {
				loadMarsk.hide();
				message("办理失败！"+data.error);
			}
		}, null);
	});
}
window.scanHistory = function(fromId) {
	var options = {};
	options.width = 600;
	options.height = 496;
	options.data = {
		formId : fromId
	};
	loadDialogPage(null, "审批历史", "/workflow/history/showTaskVariablesByFormIdList.action", {
		"查看流程图" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, false, options);
}
