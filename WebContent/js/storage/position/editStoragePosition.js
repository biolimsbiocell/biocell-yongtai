Ext.onReady(function() {
	var sysCode = document.getElementById("sp.type.id").getAttribute("sysCode");
	if (sysCode == '3d') {
		// $("#store_obj_div").hide();
		var url = "/materials/container/scanPositionContainer.action";
		var data = {};
		data.posiId = $("#sp_id").val();
		data.contId = $("#storage_container_id").val();
		if (data.contId) {
			load(url, data, $("#store_obj_div"), null);
		}
	}
});

function add() {
	var id = "";
	id = document.getElementById("id").value;
	window.location = window.ctx + '/storage/position/toEditStoragePosition.action?upId=' + id;

}

function getInfo() {
	var trs = $("#cont_table tr");
	var infos = [];
	$.each(trs, function(i, tr) {
		var tds = $(tr).children();
		$.each(tds, function(i, td) {
			var info = {};
			info.itemId = $(td).attr("itemId");
			if (info.itemId) {
				info.objName = $(td).attr("objName");
				info.ldDate = $(td).attr("ldDate");
				info.itemName = $(td).attr("itemName");
				info.num = $(td).attr("num");
				info.cellNum = $(td).attr("cellNum");
				infos.push(info);
			}
		});
	});
	console.log(infos);
	return infos;
}

function newSave() {
	if (checkSubmit() == false) {
		return false;

	}
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : window.ctx + '/common/hasId.action',
		method : 'POST',
		params : {
			id : $("#sp_id").val(),
			obj : 'StoragePosition'
		},
		success : function(response) {
			var respText = Ext.util.JSON.decode(response.responseText);
			myMask.hide();
			if (respText.message == '') {
				save();
			} else {
				message(respText.message);

			}

		},
		failure : function(response) {
		}
	});
}
function checkSubmit() {
	var mess = "";
	var fs = [ document.getElementById("sp_id").value, document.getElementById("sp.type.name").value ];
	var nsc = [ biolims.common.IdEmpty, biolims.common.typeIsEmpty ];
	mess = commonFieldsNotNullVerify(fs, nsc);

	if (mess != "") {
		message(mess);
		return false;
	}

	return true;

}
function save() {
	if (!checkSubmit()) {
		return;
	}

	var sysCode = document.getElementById("sp.type.id").getAttribute("sysCode");
	if (sysCode == '3d') {
		$("#container_info").val(JSON.stringify(getInfo()));
		var delItemIds = $("#delItemIds").val();
		if (delItemIds) {
			$("#container_del").val("'" + delItemIds + "'");
		}
	}
	$("#toolbarbutton_save").hide();
	document.getElementById("storagePositionForm").submit();
}

function list() {
	window.location = window.ctx + '/storage/position/showStoragePositionTree.action';
}
$(function() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "modify") {
		settextreadonly("sp_id");
	}
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});
Ext.onReady(function() {
	var tabs = new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-30,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.common.storageLocalName,
			contentEl : 'markup'

		} ]
	});

});
