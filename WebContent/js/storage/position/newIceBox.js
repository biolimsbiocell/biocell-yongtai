$(function() {
	
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	var mainFileInput=fileInput ('1','instrument',$("#instrument_id").val());
	fieldCustomFun();
	
})	
function change(id) {
	$("#" + id).css({
		"background-color" : "white",
		"color" : "black"
	});
}
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
function retu() {
	window.location = window.ctx + "/storage/newPosition/sampleManager.action";
}


function save() {
if(checkSubmit()==true){
	
	var abc = top.layer.load(4, {time: "",shade:0.3});   

		//自定义字段
		//拼自定义字段（实验记录）
		var inputs = $("#fieldItemDiv input");
		var options = $("#fieldItemDiv option");
		var contentData = {};
		var checkboxArr = [];
		$("#fieldItemDiv .checkboxs").each(function(i, v) {
			$(v).find("input").each(function(ii, inp) {
				var k = inp.name;
				if(inp.checked == true) {
					checkboxArr.push(inp.value);
					contentData[k] = checkboxArr;
				}
			});
		});
		inputs.each(function(i, inp) {
			var k = inp.name;
			if(inp.type != "checkbox") {
				contentData[k] = inp.value;
			}
		});
		options.each(function(i, opt) {
			if(opt.selected == true) {
				var k = opt.getAttribute("name");
				contentData[k] = opt.value;
			}
		});
		document.getElementById("fieldContent").value = JSON.stringify(contentData);
		
		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		var jsonStr = JSON.stringify($("#form1").serializeObject());  
		$.ajax({
			url: ctx + '/storage/newPosition/save.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
			},
			success: function(data) {
				if(data.success) {
					top.layer.msg("保存成功");
					top.layer.close(abc);
					var url = "/storage/newPosition/sampleManager.action";
					window.location.href=url;
				}else{
					top.layer.msg(biolims.common.saveFailed);
				}
				}
			
		});
		
	}
}		

$.fn.serializeObject = function() {
	   var o = {};  
	    var a = this.serializeArray();  
	    $.each(a, function() {  
	        if (o[this.name]) {  
	            if (!o[this.name].push) {  
	                o[this.name] = [ o[this.name] ];  
	            }  
	            o[this.name].push(this.value || '');  
	        } else {  
	            o[this.name] = this.value || '';  
	        }  
	    });  
	    return o;  
};

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sp_id").val());
	nsc.push("位置编号"+biolims.common.describeEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			top.layer.msg(mess);
			return false;
		}
		return true;
}
function settextreadonly() {
    jQuery(":text, textarea").each(function() {
	var _vId = jQuery(this).attr('id');
	jQuery(this).css("background-color","#B4BAB5").attr("readonly", "readOnly");
	if (_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}
function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}


function showType(){
	top.layer.open({
		title: "选择位置类型",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=freezeType", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
		$("#sp_type_id").val(id)
		$("#sp_type_name").val(name)
		},
	})
	
}
//状态
function showState(){
		top.layer.open({
		title:"选择冰箱状态",
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/dic/state/dicStateSelectTable.action?flag=storagePosition",''],
		yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(0).text();
				top.layer.close(index)
			
		$("#sp_state_id").val(id)
		$("#sp_state_name").val(name)
		},
	})
}
function showdepartment(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/dic/type/dicTypeSelectTable.action?flag=device",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDepartmentTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDepartmentTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#instrument_department").val(id)
		$("#instrument_department_name").val(name)
		},
	})
}
//容器
function showStorageContainer(){
		top.layer.open({
		title:'选择容器 ',
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/storage/container/showContainerTable.action",''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
		$("#sp_storageContainer_id").val(id)
		$("#sp_storageContainer_name").val(name)
		},
	})
}

//盒子
function showSubStorageContainer(){
		top.layer.open({
		title:'选择盒子 ',
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/storage/container/showContainerTable.action",''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
		$("#sp_subStorageContainer_id").val(id)
		$("#sp_subStorageContainer_name").val(name)
		},
	})
}
function showproducts(){
		top.layer.open({
		title:'选择生产商',
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/dic/type/dicTypeSelectTable.action?flag=scs",''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
		$("#instrument_products_id").val(id)
		$("#instrument_products_name").val(name)
		},
	})
}
//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

		//查找自定义列表的订单模块的此检测项目的相关内容
		$.ajax({
			type: "post",
			url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
			data: {
				moduleValue: "Instrument"
			},
			async: false,
			success: function(data) {
				var objValue = JSON.parse(data);
				if(objValue.success) {
					$.each(objValue.data, function(i, n) {
						var inputs = '';
						var disabled = n.readOnly ? ' ' : "disabled";
						var defaultValue = n.defaultValue ? n.defaultValue : ' ';
						if(n.fieldType == "checkbox") {
							var checkboxs = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
						} else if(n.fieldType == "radio") {
							var options = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
						} else if(n.fieldType == "date") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + ' required=' + n.isRequired + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}

						$("#fieldItemDiv").append(inputs);
					});

				} else {
					top.layer.msg("自定义列表更新失败！");
				}
			}
		});
	

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}
//成本中心
function showScope() {
	top.layer.open({
		title: biolims.user.selectCostCentre,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: window.ctx + "/core/user/showCostCenter.action",
		yes: function(index, layer) {
			var id = [],
				name = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#costCenter input").each(function(i, v) {
				if($(v).is(':checked')) {
					id.push(v.value);
					name.push(v.title);
				}
			});
			$("#sp_scopeName").val(name.join(","));
			$("#sp_scopeId").val(id.join(","));
			top.layer.close(index)
		}
	})
}
//
////编辑
//function edit() {
//	var id = $(".selected").find("input").val();
//	if(id == "" || id == undefined) {
//		top.layer.msg(biolims.common.selectRecord);
//		return false;
//	}
//	window.location = window.ctx +
//		"/equipment/main/editInstrument.action?id=" + id;
//}
