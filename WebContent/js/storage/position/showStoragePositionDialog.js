$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.user.eduName1,
	});
	var tbarOpts = [];
	var options = table(false, null,
		'/storage/newPosition/showStoragePositionDialogListJson.action', colOpts, null)
	var addStoragePasition = renderData($("#addStoragePasition"), options);
	$("#addStoragePasition").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addStoragePasition_wrapper .dt-buttons").empty();
			$('#addStoragePasition_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addStoragePasition tbody tr");
			addStoragePasition.ajax.reload();
			addStoragePasition.on('draw', function() {
				trs = $("#addStoragePasition tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});
})