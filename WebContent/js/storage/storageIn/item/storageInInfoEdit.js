$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/storage/storageIn/item/storageInInfo/editStorageInInfo.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/storage/storageIn/item/storageInInfo/showStorageInInfoList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#storageInInfo", {
					userId : userId,
					userName : userName,
					formId : $("#storageInInfo_id").val(),
					title : $("#storageInInfo_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#storageInInfo_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
	    var storageInInfoItemDivData = $("#storageInInfoItemdiv").data("storageInInfoItemGrid");
		document.getElementById('storageInInfoItemJson').value = commonGetModifyRecords(storageInInfoItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/storage/storageIn/item/storageInInfo/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/storage/storageIn/item/storageInInfo/copyStorageInInfo.action?id=' + $("#storageInInfo_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#storageInInfo_id").val() + "&tableId=storageInInfo");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#storageInInfo_id").val());
	nsc.push("原辅料编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'入库原辅料',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/storage/storageIn/item/storageInInfo/showStorageInInfoItemList.action", {
				id : $("#storageInInfo_id").val()
			}, "#storageInInfoItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);