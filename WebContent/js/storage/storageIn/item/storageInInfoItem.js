var storageInInfoItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'spec',
		type:"string"
	});
	   fields.push({
		name:'jdeCode',
		type:"string"
	});
	   fields.push({
		name:'serial',
		type:"string"
	});
	   fields.push({
		name:'expireDate',
		type : 'string'
	});
	   fields.push({
		name:'reactionNum',
		type:"string"
	});
	   fields.push({
		name:'qcPass',
		type:"string"
	});
	    fields.push({
		name:'position-id',
		type:"string"
	});
	    fields.push({
		name:'position-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'storageInInfo-id',
		type:"string"
	});
	    fields.push({
		name:'storageInInfo-name',
		type:"string"
	});
	    fields.push({
			name:'storageInId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'spec',
		hidden : false,
		header:'型号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'jdeCode',
		hidden : false,
		header:'jde编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'serial',
		hidden : false,
		header:'批号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var expireDate = new Ext.form.DateField({
//		format : 'Y-m-d'
//	});
	cm.push({
		dataIndex:'expireDate',
		hidden : false,
		header:'过期日期',
		width:20*6
	});
	cm.push({
		dataIndex:'reactionNum',
		hidden : false,
		header:'反应数',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcPass',
		hidden : false,
		header:'qc状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'position-id',
		hidden : true,
		header:'存储位置ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'position-name',
		hidden : false,
		header:'存储位置',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storageInId',
		hidden : true,
		header:'入库单号',
		width:20*6
	});
	cm.push({
		dataIndex:'storageInInfo-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storageInInfo-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/storageIn/item/storageInInfo/showStorageInInfoItemListJson.action?id="+ $("#id_parent_hidden").val()+"&serial="+$("#serial").val()+"&storageInId="+$("#storageInId").val();
	loadParam.limit=999;
	var opts={};
	opts.title="入库原辅料明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//    	   alert(ids);
//		ajax("post", "/storage/storageIn/item/storageInInfo/delStorageInInfoItem.action", {
//			ids : ids
//		}, function(data) {
//			alert();
//			if (data.success) {
//				storageInInfoItemGrid.getStore().commitChanges();
//				storageInInfoItemGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text :"打印标签",
		handler : print
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	storageInInfoItemGrid=gridEditTable("storageInInfoItemdiv",cols,loadParam,opts);
	$("#storageInInfoItemdiv").data("storageInInfoItemGrid", storageInInfoItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//打印
function print(){
	if(storageInInfoItemGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var selRecords = storageInInfoItemGrid.getSelectionModel().getSelections(); 
	if(selRecords.length>0){
		var options = {};
		options.width = document.body.clientWidth-400;
		options.height = document.body.clientHeight-40;
		loadDialogPage(null, "选择打印机", "/system/syscode/codeMain/codeMainSelect.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_codeMain_div").data("codeMainDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
					    id = obj.get("id");
					    var ids=[];
						for(var i=0;i<selRecords.length;i++){
							ids.push(selRecords[i].get("id"));
						}
						ajax("post", "/storage/storageIn/item/storageInInfo/makeCode.action", {
							ids:ids,
							id:id
						}, function() {
							
						}, null);
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}else{
		message("请选择数据！");
	}
	
}