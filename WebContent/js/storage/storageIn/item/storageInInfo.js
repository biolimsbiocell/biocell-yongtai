var storageInInfoGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'kit-id',
		type:"string"
	});
	    fields.push({
		name:'kit-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'原辅料编号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:40*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'kit-id',
		hidden:true,
		header:'原辅料盒ID',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'kit-name',
		header:'原辅料盒',
		
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建时间',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态描述',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:30*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/storage/storageIn/item/storageInInfo/showStorageInInfoListJson.action";
	var opts={};
	opts.title="入库原辅料";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	storageInInfoGrid=gridTable("show_storageInInfo_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/storage/storageIn/item/storageInInfo/editStorageInInfo.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/storage/storageIn/item/storageInInfo/editStorageInInfo.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/storage/storageIn/item/storageInInfo/viewStorageInInfo.action?id=' + id;
}
function exportexcel() {
	storageInInfoGrid.title = '导出列表';
	var vExportContent = storageInInfoGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(storageInInfoGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
