﻿var goodsMaterialsSendDetailsGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
   fields.push({
		name:'packCode',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-name',
		type:"string"
	});
	    //=============
	    fields.push({
	    	name:'validity',
	    	type:"date",
			dateFormat:"Y-m-d"
	    });
	    fields.push({
	    	name:'state',
	    	type:"string"
	    });
	    //=============
	    fields.push({
	    	name:'useState',
	    	type:"string"
	    });
	    fields.push({
	    	name:'outState',
	    	type:"string"
	    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.goods.materialId,
		width:40*6
	});
	cm.push({
		dataIndex:'packCode',
		hidden : true,
		header:biolims.master.materialPackageId,
		width:40*6
	});
	//===========
	cm.push({
		dataIndex:'validity',
		hidden : false,
		header:biolims.sanger.expiryDate,
		width:30*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'state',
		hidden:true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'useState',
		hidden:true,
		header:biolims.sanger.state,
		width:20*6
	});
	cm.push({
		dataIndex:'outState',
		hidden:true,
		header:biolims.goods.outState,
		width:20*6
	});
	//===========

	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.printingQuantity,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	//var type="1";
	loadParam.url=ctx+"/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendDetailsListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.materialPackageDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state=$("#goodsMaterialsSend_stateName").val();
	if(state!=biolims.common.finish){
		opts.delSelect = function(ids) {
			ajax("post", "/goods/mate/goodsMaterialsSend/delGoodsMaterialsReadyDetails.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					scpProToGrid.getStore().commitChanges();
					scpProToGrid.getStore().reload();
					message(biolims.common.deleteSuccess);
				} else {
					message(biolims.common.deleteFailed);
				}
			}, null);
		};
		opts.tbar.push({
			text : biolims.common.batchOper,
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_send_div"), biolims.common.batchOper, null, {
					"Confirm" : function() {
						var records = goodsMaterialsSendDetailsGrid.getSelectRecord();
						if (records && records.length > 0) {
							var date = $("#date").val();
							date = date.replace(/-/g,"/");
							var rdate = new Date(date);
							goodsMaterialsSendDetailsGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("validity", rdate);
							});
							goodsMaterialsSendDetailsGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
	opts.tbar.push({
		text : biolims.goods.printId,
		handler : function() {
			var records = inItemGrid.getSelectRecord();
			var str = "";
			if (records) {
				
				$.each(records, function(i, obj) {
				
				
					ajax("post", "/goods/mate/goodsMaterialsReady/makeCode.action", {
						id : obj.get("code"),
						//name: obj.get("patientName"),
						//打印机固定IP
						ip : '192.168.1.14'
					}, function() {
					
					}, null);
					
				});
				
			}
			message(str);
}
	});
//	opts.tbar.push({
//		text : '可重用样本号',
//		handler : reCode
//	});
       
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectgoodsMaterialsReadyFun
//		});
//	
//	
//	
	
	
	
	
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = goodsMaterialsSendDetailsGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
                			goodsMaterialsSendDetailsGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		text : '填加明细',
//		handler : null
//	});
	}
	goodsMaterialsSendDetailsGrid=gridEditTable("goodsMaterialsSendDetailsdiv",cols,loadParam,opts);
	$("#goodsMaterialsSendDetailsdiv").data("goodsMaterialsSendDetailsGrid", goodsMaterialsSendDetailsGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//--------------------
//function reCode(){
//	var options = {};
//	options.width = 900;
//	options.height = 460;
//	var url="/system/organize/officeApply/showOfficeApplyItemList.action?flag="+"101";
//	loadDialogPage(null, "选择明细", url, {
//		 "确定": function() {
//			 var off=officeApplyItemGrid.getSelectionModel();
//			 var gmrd=goodsMaterialsSendDetailsGrid.store;
//			 if(off.getSelections().length > 0){
//				 $.each(off.getSelections(), function(i, obj) {
//					 var isRepeat = false;
//						for(var j=0;j<gmrd.getCount();j++){
//							var oldv = gmrd.getAt(j).get("code");
//							if(oldv == obj.get("code")){
//								isRepeat = true;
//								message("数据重复，请重新选择！");
//								break;
//							}
//						}
//					 if(!isRepeat){
//						 var ob=goodsMaterialsSendDetailsGrid.getStore().recordType;
//						 goodsMaterialsSendDetailsGrid.stopEditing();
//						 var p=new ob({});
//						p.isNew=true;
//						p.set("code",obj.get("code"));
//						goodsMaterialsSendDetailsGrid.getStore().add(p);
//						goodsMaterialsSendDetailsGrid.startEditing(0,0);
//					 }
//				 });
//			 }else{
//						message("请先选择数据！");
//						return;
//			 }
//			/* if(ids.length>0){
//				 $.each(ids, function(i, obj) {
//					 var code = obj.get("id");
//					 ajax("post", "/goods/sample/sampleInfoMain/simpleInfoDetails.action", {
//					 },function(){
//						 
//					 },null);
//				 });
//			 }*/
//			 options.close();
//		}
//	}, true, options);
//}
//-----------------------
function selectgoodsMaterialsReadyFun(){
	var win = Ext.getCmp('selectgoodsMaterialsReady');
	if (win) {win.close();}
	var selectgoodsMaterialsReady= new Ext.Window({
	id:'selectgoodsMaterialsReady',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsReadySelect.action?flag=goodsMaterialsReady' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsMaterialsReady.close(); }  }]  });     selectgoodsMaterialsReady.show(); }
	function setgoodsMaterialsReady(id,name){
		var gridGrid = $("#goodsMaterialsSendDetailsdiv").data("goodsMaterialsSendDetailsGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('goodsMaterialsSend-id',id);
			obj.set('goodsMaterialsSend-name',name);
		});
		var win = Ext.getCmp('selectgoodsMaterialsReady');
		if(win){
			win.close();
		}
	}
	
