﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state=$("#goodsMaterialsPost_state").val();
	if(state=="3"){
		$("#showgoodsMaterialsSend").css("display","none");
		$("#showtype").css("display","none");
		$("#doclinks_img").css("display","none");
	}
	
});
function add() {
	window.location = window.ctx + "/goods/mate/goodsMaterialsPost/editGoodsMaterialsPost.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/goods/mate/goodsMaterialsPost/showGoodsMaterialsPostList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#goodsMaterialsPost", {
					userId : userId,
					userName : userName,
					formId : $("#goodsMaterialsPost_id").val(),
					title : $("#goodsMaterialsPost_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#goodsMaterialsPost_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var goodsMaterialsPostItemDivData = $("#goodsMaterialsPostItemdiv").data("goodsMaterialsPostItemGrid");
		document.getElementById('goodsMaterialsPostItemJson').value = commonGetModifyRecords(goodsMaterialsPostItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/goods/mate/goodsMaterialsPost/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/goods/mate/goodsMaterialsPost/copyGoodsMaterialsPost.action?id=' + $("#goodsMaterialsPost_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#goodsMaterialsPost_id").val() + "&tableId=goodsMaterialsPost");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#goodsMaterialsPost_id").val()){
		commonChangeState("formId=" + $("#goodsMaterialsPost_id").val() + "&tableId=GoodsMaterialsPost");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#goodsMaterialsPost_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.goods.express2Send,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/goods/mate/goodsMaterialsPost/showGoodsMaterialsPostItemList.action", {
				id : $("#goodsMaterialsPost_id").val()
			}, "#goodsMaterialsPostItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
function GoodsMaterialsSendFun(){
		var win = Ext.getCmp('GoodsMaterialsSendFun');
		if (win) {win.close();}
		var GoodsMaterialsSendFun= new Ext.Window({
		id:'GoodsMaterialsSendFun',modal:true,title:biolims.goods.selectMaterialIssue,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/goods/mate/goodsMaterialsSend/goodsMaterialsSendSelectByState.action?flag=GoodsMaterialsSendFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 GoodsMaterialsSendFun.close(); }  }]  });     GoodsMaterialsSendFun.show(); }

function setGoodsMaterialsSendFun(rec){
	var code=$("#goodsMaterialsPost_goodsMaterialsSend").val();
	 if(code==""){
		 document.getElementById('goodsMaterialsPost_goodsMaterialsSend').value=rec.get('id');
		 document.getElementById('goodsMaterialsPost_goodsMaterialsSend_name').value=rec.get('name');
//		 var address=rec.get('goodsMaterialsReady-goodsMaterialsApply-address-name');
//		 var receiver=rec.get('goodsMaterialsReady-goodsMaterialsApply-receiveUser');
//		 var company=rec.get('company-name');
		 var win = Ext.getCmp('GoodsMaterialsSendFun');
		 if(win){win.close();}
			id = rec.get('id');
			name= rec.get('name');
			ajax("post", "/goods/mate/goodsMaterialsPost/showGoodsMaterialsSendExpressList.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						var ob = goodsMaterialsPostItemGrid.getStore().recordType;
						goodsMaterialsPostItemGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							
							p.set("expressOrder",obj.code);
							p.set("address",obj.address);
							p.set("company",obj.company);
							p.set("receiver",obj.receiver);
							//p.set("expectDate",obj.expectDate);
							goodsMaterialsPostItemGrid.getStore().add(p);							
						});
						
						goodsMaterialsPostItemGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
	 }else{
			if(rec.get('id')==code){
				var win = Ext.getCmp('GoodsMaterialsSendFun');
				if(win){win.close();}
			 }else{
			//==================2015-12-01 ly=========================
				 var ob1=goodsMaterialsPostItemGrid.store;
				 if(ob1.getCount() > 0){
					 for(var j=0;j<=ob1.getCount();j++){
						 var no=ob1.getAt(j).get("id");
						 //根据ID删除
						 if(no!=null){
						 ajax("post", "/goods/mate/goodsMaterialsPost/delGoodsMaterialsPostOne.action", {
							 ids : no
						 }, function(data) {
							 if (data.success) {
								 message(biolims.common.deleteSuccess);
							 } else {
								 message(biolims.common.deleteFailed);
							 }
						 }, null);
						 }else{
							 goodsMaterialsPostItemGrid.store.removeAll();
						 }
						 goodsMaterialsPostItemGrid.store.removeAll();
					 }
				 }
			 //================================================
			 document.getElementById('goodsMaterialsPost_goodsMaterialsSend').value=rec.get('id');
			 document.getElementById('goodsMaterialsPost_goodsMaterialsSend_name').value=rec.get('name');
//			 var address=rec.get('goodsMaterialsReady-goodsMaterialsApply-address-name');
//			 var receiver=rec.get('goodsMaterialsReady-goodsMaterialsApply-receiveUser');
//			 var company=rec.get('company-name');
			 var win = Ext.getCmp('GoodsMaterialsSendFun');
			 if(win){win.close();}
				id = rec.get('id');
				name= rec.get('name');
				ajax("post", "/goods/mate/goodsMaterialsPost/showGoodsMaterialsSendExpressList.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = goodsMaterialsPostItemGrid.getStore().recordType;
							goodsMaterialsPostItemGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								
								p.set("expressOrder",obj.code);
								p.set("address",obj.address);
								p.set("company",obj.company);
								p.set("receiver",obj.receiver);
								p.set("expectDate",obj.expectDate);
								goodsMaterialsPostItemGrid.getStore().add(p);							
							});
							
							goodsMaterialsPostItemGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
			
	}
			}
	 }

