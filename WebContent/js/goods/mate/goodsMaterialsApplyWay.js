﻿
var goodsMaterialsApplyWayGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
//	   fields.push({
//		name:'expressOrder',
//		type:"string"
//	});
//	   fields.push({
//		name:'expressCompany',
//		type:"string"
//	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'way',
		type:"string"
	});
	   fields.push({
		name:'isReceive',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-name',
		type:"string"
	});
    fields.push({
		name:'goodsMaterialsSendExpress-id',
		type:"string"
	});
    fields.push({
		name:'goodsMaterialsSendExpress-expressOrder',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSendExpress-company',
		type:"string"
	});
    fields.push({
    	name:'predictDate',
    	type:"string"
//    	type:"date",
//		dateFormat:"Y-m-d"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSendExpress-expressOrder',
		hidden : false,
		header:biolims.sample.expreceCode,
		width:30*6
	});
	cm.push({
		dataIndex:'goodsMaterialsSendExpress-company',
		hidden : false,
		header:biolims.sample.companyName,
		width:30*6
	});
	cm.push({
		dataIndex:'predictDate',
		hidden : false,
		header:biolims.goods.predictDate,
		width:30*6
		//renderer: formatDate,
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.goods.state,
		width:20*6
	});
	cm.push({
		dataIndex:'way',
		hidden : false,
		header:biolims.goods.way,
		width:30*10
	});
	var isReceive = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name : biolims.common.no
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isReceive',
		hidden : false,
		header:biolims.goods.isReceive,
		width:25*6,
		renderer: Ext.util.Format.comboRenderer(isReceive),editor: isReceive
	});
//	cm.push({
//		dataIndex:'isReceive',
//		hidden : false,
//		header:'签收确认',
//		width:40*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	
	cm.push({
		dataIndex:'goodsMaterialsApply-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cm.push({
		dataIndex:'goodsMaterialsSendExpress-id',
		hidden : true,
		header:biolims.goods.sendExpressId,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsApply/showGoodsMaterialsApplyWayListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.goods.MaterialApplicationTracking;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];	
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.delSelected,
			handler : null
		});
				
	
	   opts.delSelect = function(ids) {
		   ajax("post", "/goods/mate/goodsMaterialsApply/delGoodsMaterialsApplyWay.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					scpProToGrid.getStore().commitChanges();
					scpProToGrid.getStore().reload();
					message(biolims.common.deleteSuccess);
				} else {
					message(biolims.common.deleteFailed);
				}
			}, null);
	   	};
		opts.tbar.push({
			text : biolims.common.batchSign,
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_way_div"), biolims.common.batchSign, null, {
					"Confirm" : function() {
						var records = goodsMaterialsApplyWayGrid.getSelectRecord();
						if (records && records.length > 0) {
							var isReceive = $("#isReceive").val();
							goodsMaterialsApplyWayGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("isReceive", isReceive);
							});
							goodsMaterialsApplyWayGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
		opts.tbar.push({
			text : biolims.common.save,
			handler : saveWay
		});
	

//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = goodsMaterialsApplyWayGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 6-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//							
//							goodsMaterialsApplyWayGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}

	goodsMaterialsApplyWayGrid=gridEditTable("goodsMaterialsApplyWaydiv",cols,loadParam,opts);
	$("#goodsMaterialsApplyWaydiv").data("goodsMaterialsApplyWayGrid", goodsMaterialsApplyWayGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//保存物流信息
function saveWay(){
	var selectRecord = goodsMaterialsApplyWayGrid.store;
	var inItemGrid = $("#goodsMaterialsApplyWaydiv").data("goodsMaterialsApplyWayGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	//if(selectRecord.length>0){
		//$.each(selectRecord, function(i, obj) {
			ajax("post", "/goods/mate/goodsMaterialsApply/setToWay.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#goodsMaterialsApplyWaydiv").data("goodsMaterialsApplyWayGrid").getStore().commitChanges();
					$("#goodsMaterialsApplyWaydiv").data("goodsMaterialsApplyWayGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		//});
	//}
}
//function selectwayFun(){
//	var win = Ext.getCmp('selectway');
//	if (win) {win.close();}
//	var selectway= new Ext.Window({
//	id:'selectway',modal:true,title:'选择快递轨迹',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/StringSelect.action?flag=way' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectway.close(); }  }]  });     selectway.show(); }
//	function setway(id,name){
//		var gridGrid = $("#goodsMaterialsApplyWaydiv").data("goodsMaterialsApplyWayGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('way-id',id);
//			obj.set('way-name',name);
//		});
//		var win = Ext.getCmp('selectway')
//		if(win){
//			win.close();
//		}
//	}
//	
//function selectgoodsMaterialsApplyFun(){
//	var win = Ext.getCmp('selectgoodsMaterialsApply');
//	if (win) {win.close();}
//	var selectgoodsMaterialsApply= new Ext.Window({
//	id:'selectgoodsMaterialsApply',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsApplySelect.action?flag=goodsMaterialsApply' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectgoodsMaterialsApply.close(); }  }]  });     selectgoodsMaterialsApply.show(); }
//	function setgoodsMaterialsApply(id,name){
//		var gridGrid = $("#goodsMaterialsApplyWaydiv").data("goodsMaterialsApplyWayGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('goodsMaterialsApply-id',id);
//			obj.set('goodsMaterialsApply-name',name);
//		});
//		var win = Ext.getCmp('selectgoodsMaterialsApply')
//		if(win){
//			win.close();
//		}
//	}
	
