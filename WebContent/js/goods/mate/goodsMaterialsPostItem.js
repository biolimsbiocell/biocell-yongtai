﻿var goodsMaterialsPostItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'expressOrder',
		type:"string"
	});
	   fields.push({
		name:'checked',
		type:"string"
	});
	   fields.push({
		name:'address',
		type:"string"
	});
	   fields.push({
		name:'company',
		type:"string"
	});
	   fields.push({
		name:'expectDate',
		type:"string"
	});
	   fields.push({
		name:'realDate',
		type:"string"
//		type:"date",
//		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'way',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
		name:'goodsMaterialsSendExpress-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-name',
		type:"string"
	});
	fields.push({
			name:'receiver',
			type:"string"
	});
	fields.push({
		name:'submit',
		type:"string"
	});
//	fields.push({
//			name:'dateOfLodgment',
//			type:"date",
//			dateFormat:"Y-m-d"
//			
//	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'expressOrder',
		hidden : false,
		header:biolims.sample.expreceCode,
		width:40*6
	});
	cm.push({
		dataIndex:'checked',
		hidden : false,
		header:biolims.common.checked,
		width:40*6
	});
	cm.push({
		dataIndex:'receiver',
		hidden : false,
		header:biolims.common.recipient,
		width:20*6
	});
	cm.push({
		dataIndex:'address',
		hidden : false,
		header:biolims.common.consigneeAddress,
		width:40*6
	});
	cm.push({
		dataIndex:'company',
		hidden : false,
		header:biolims.sample.companyName,
		width:20*6
	});
	cm.push({
		dataIndex:'expectDate',
		hidden : true,
		header:biolims.goods.predictDate,
		width:15*6
	});

//	cm.push({
//		dataIndex:'dateOfLodgment',
//		hidden : false,
//		header:'签收日期',
//		width:15*6,
//		
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
//	});
	
	cm.push({
		dataIndex:'realDate',
		hidden : true,
		header:biolims.goods.realDate,
		width:30*6
		
		//renderer: formatDate,
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	var storePutCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var putCob = new Ext.form.ComboBox({
		store : storePutCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.toSubmit,
		width:20*6,
		editor : putCob,
		renderer : Ext.util.Format.comboRenderer(putCob)
	});
	cm.push({
		dataIndex:'way',
		hidden : false,
		header:biolims.goods.way,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSendExpress-id',
		hidden : true,
		header:biolims.master.relatedTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsPost/showGoodsMaterialsPostItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.goods.express2SendDetail;
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	var state=$("#goodsMaterialsPost_stateName").val();
	if(state!=biolims.common.finish){
//       opts.delSelect = function(ids) {
//		ajax("post", "/goods/mate/goodsMaterialsPost/delGoodsMaterialsPostItem.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				goodsMaterialsPostItemGrid.getStore().commitChanges();
//				goodsMaterialsPostItemGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		text : biolims.common.batchOper,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			var records = goodsMaterialsPostItemGrid.getSelectRecord();
			if (records && records.length > 0) {
				loadDialogPage($("#bat_submit_div"), biolims.common.batchOper, null, {
					"Confirm" : function() {
							var submit = $("#submit").val();
							goodsMaterialsPostItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("submit", submit);
							});
							goodsMaterialsPostItemGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = goodsMaterialsPostItemGrid.getAllRecord();
							var store = goodsMaterialsPostItemGrid.store;

							var isOper = true;
							var buf = [];
							goodsMaterialsPostItemGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("expressOrder")){
										buf.push(store.indexOfId(obj1.get("id")));
										obj1.set("checked",obj1.get("expressOrder"));
										isOper = true;
									}else{
										isOper = false;
									}
								});
//								if(isOper==false){
//									message("样本号核对不符，请检查！");
//								}else{
//								    message("样本号核对完毕！");
//								}
							});
							goodsMaterialsPostItemGrid.getSelectionModel().selectRows(buf);
							
							goodsMaterialsPostItemGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	}
	goodsMaterialsPostItemGrid=gridEditTable("goodsMaterialsPostItemdiv",cols,loadParam,opts);
	$("#goodsMaterialsPostItemdiv").data("goodsMaterialsPostItemGrid", goodsMaterialsPostItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function save(){	
	var itemJson = commonGetModifyRecords(goodsMaterialsPostItemGrid);
	var records = goodsMaterialsPostItemGrid.getSelectionModel();
	var flag=true;
	$.each(records.getSelections(), function(i, obj) {
		if(obj.get("checked")!=null && obj.get("checked")!="" && obj.get("checked")!=undefined){
			flag = true;
		}else{
			flag = false;
		}
	});
	if(itemJson.length>0){
		if(flag){
			ajax("post", "/goods/mate/goodsMaterialsPost/saveGoodsMaterialsPostItemGrid.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					goodsMaterialsPostItemGrid.getStore().commitChanges();
					goodsMaterialsPostItemGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		}
	}else{
		message(biolims.common.noData2Save);
	}
	
}
function search() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(goodsMaterialsPostItemGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
}