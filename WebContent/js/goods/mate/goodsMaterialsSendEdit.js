﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	setTimeout(function(){
		loadBox();
    }, 500);
	var state=$("#goodsMaterialsSend_stateName").val();
	if(state==biolims.common.finish){
		$("#showgoodsMaterialsReady").css("display","none");
		$("#showcompany").css("display","none");
	}
});
function add() {
	window.location = window.ctx + "/goods/mate/goodsMaterialsSend/editGoodsMaterialsSend.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var flag=true;
	for(var i=0;i<goodsMaterialsSendExpressGrid.store.getCount();i++){
		if(goodsMaterialsSendExpressGrid.store.getAt(i).get("code")!=null&&
				goodsMaterialsSendExpressGrid.store.getAt(i).get("code")!="" &&
				goodsMaterialsSendExpressGrid.store.getAt(i).get("code")!=undefined){
			flag=true;
		}else{
			flag=false;
			message(biolims.goods.courierNumberIsEmpty);
			break;
		}
	}
	if(flag){
		save();
	}
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#goodsMaterialsSend", {
					userId : userId,
					userName : userName,
					formId : $("#goodsMaterialsSend_id").val(),
					title : $("#goodsMaterialsSend_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#goodsMaterialsSend_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

//function saveToApply(){
//	var id=$("#goodsMaterialsSend_id").val();
//	ajax("post", "/goods/mate/goodsMaterialsSend/setExpressToWay.action", {
//		code : id
//	}, function(data) {
//		if (data.success) {
//			message("");
//		} else {
//			message("保存失败！");
//		}
//	}, null);
//}
//计算运输箱
function sumNum(){	
	var sendBox = goodsMaterialsSendBoxGrid.store;
		$.each(sendBox, function(i, obj) {
			//var isRepeat = false;
			for(var j=0;j<sendBox.getCount();j++){
				var num = sendBox.getAt(j).get("transBox-num");
			}
		});
}
//	//var gridGrid = //$("#goodsMaterialsApplyWaydiv").data("goodsMaterialsApplyWayGrid");
//	var sendExpress = goodsMaterialsSendExpressGrid.store;
//	var applyWay = goodsMaterialsApplyWayGrid.store;
//		$.each(sendExpress.store, function(i, obj) {
//			var isRepeat = false;
//			for(var j=0;j<applyWay.getCount();j++){
//				var apply = applyWay.getAt(j).get("expressOrder");
//				if(apply == obj.get("code")){
//					isRepeat = true;
//					message("数据重复，请重新选择！");
//					break;
//				}else{
//					
//				}
//			}
//		});
//}



function save() {
	
	if(checkSubmit()==true){   
		Ext.MessageBox.show({ msg : biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
//	    var goodsMaterialsSendItemDivData = $("#goodsMaterialsSendItemdiv").data("goodsMaterialsSendItemGrid");
//		document.getElementById('goodsMaterialsSendItemJson').value = commonGetModifyRecords(goodsMaterialsSendItemDivData);
		
		var goodsMaterialsSendItem2divData = goodsMaterialsSendItem2Grid;
		document.getElementById('goodsMaterialsSendItemTwoJson').value = commonGetModifyRecords(goodsMaterialsSendItem2divData);
	    var goodsMaterialsSendBoxDivData = goodsMaterialsSendBoxGrid;
		document.getElementById('goodsMaterialsSendBoxJson').value = commonGetModifyRecords(goodsMaterialsSendBoxDivData);
	    var goodsMaterialsSendExpressDivData = goodsMaterialsSendExpressGrid;
		document.getElementById('goodsMaterialsSendExpressJson').value = commonGetModifyRecords(goodsMaterialsSendExpressDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/goods/mate/goodsMaterialsSend/save.action";
		form1.submit();
	}
	
}		
function editCopy() {
	window.location = window.ctx + '/goods/mate/goodsMaterialsSend/copyGoodsMaterialsSend.action?id=' + $("#goodsMaterialsSend_id").val();
}

$("#toolbarbutton_status").click(function(){
	var selRecord=goodsMaterialsSendExpressGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		if(selRecord.getAt(j).get("code")==""){
			message(biolims.goods.courierNumberIsEmpty);
			return;
		}
	}
	if ($("#goodsMaterialsSend_id").val()){
		commonChangeState("formId=" + $("#goodsMaterialsSend_id").val() + "&tableId=GoodsMaterialsSend");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
//	fs.push($("#goodsMaterialsSend_goodsMaterialsApply").val());
//	nsc.push("物料申请不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.goods.materialIssue,
	    	   contentEl:'markup'
	       } ]
	   });
});

//load("/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendItemList.action", {
//				id : $("#goodsMaterialsSend_id").val()
//			}, "#goodsMaterialsSendItempage");
load("/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendItemListTwo.action", {
	id : $("#goodsMaterialsSend_id").val()
}, "#goodsMaterialsSendItempage2");
load("/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendBoxList.action", {
				id : $("#goodsMaterialsSend_id").val()
			}, "#goodsMaterialsSendBoxpage");
load("/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendExpressList.action", {
				id : $("#goodsMaterialsSend_id").val()
			}, "#goodsMaterialsSendExpresspage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
function GoodsMaterialsApplyFun(){
		var win = Ext.getCmp('GoodsMaterialsApplyFun');
		if (win) {win.close();}
		var GoodsMaterialsApplyFun= new Ext.Window({
		id:'GoodsMaterialsApplyFun',modal:true,title:biolims.goods.selectMaterialApplication,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/goods/mate/goodsMaterialsApply/goodsMaterialsApplySelectByState.action?flag=GoodsMaterialsApplyFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
			 GoodsMaterialsApplyFun.close(); }  }]  });     GoodsMaterialsApplyFun.show(); }

function setGoodsMaterialsApplyFun(rec){
	 var code=$("#goodsMaterialsSend_goodsMaterialsApply_name").val();
	 if(code==null || code==""){
		 document.getElementById('goodsMaterialsSend_goodsMaterialsApply').value=rec.get('id');
		 document.getElementById('goodsMaterialsSend_goodsMaterialsApply_name').value=rec.get('name');
		 document.getElementById('goodsMaterialsSend_goodsMaterialsApply_applyOrganize_name').value=rec.get('goodsMaterialsApply-applyOrganize-name');
		 var win = Ext.getCmp('GoodsMaterialsApplyFun');
		 if(win){win.close();}
			id = rec.get('id');
			name= rec.get('name');
			ajax("post", "/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendItemList.action", {
				code : id,
				}, function(data) {
					if (data.success) {	

						var ob = goodsMaterialsSendItemGrid.getStore().recordType;
						goodsMaterialsSendItemGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("goodsMaterialsApplyItem-id", obj.mid);
							p.set("goodsMaterialsApplyItem-goodsMaterialsPack-id", obj.pid);
							p.set("goodsMaterialsApplyItem-goodsMaterialsPack-name", obj.pname);
//							p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-code", obj.materialsId);
//							p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-name", obj.materialsName);
							p.set("goodsMaterialsApplyItem-applyNum", obj.applyNum);
//							p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name", obj.unitName);
							p.set("goodsMaterialsApplyItem-sendNum", obj.sendName);
							p.set("goodsMaterialsApplyItem-wayNum", obj.wayNum);
							p.set("goodsMaterialsApplyItem-unuseNum", obj.unuseNum);							
							p.set("goodsMaterialsApplyItem-usedNum", obj.usedNum);
							//p.set("goodsMaterialsApplyItem-state", obj.state);
							p.set("goodsMaterialsApply-id", id);
							p.set("goodsMaterialsApply-name", name);
//							p.set("goodsMaterialsApplyItem-goodsMaterialsApply-receiveUser", obj.linkMan);
//							p.set("goodsMaterialsApplyItem-goodsMaterialsApply-address-name", obj.address);							
//							p.set("goodsMaterialsApplyItem-goodsMaterialsApply-company-name", obj.company);
							
							goodsMaterialsSendItemGrid.getStore().add(p);							
						});
						
						goodsMaterialsSendItemGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
	 }else{
			if(rec.get('id')==code){
				var win = Ext.getCmp('GoodsMaterialsApplyFun');
				if(win){win.close();}
			 }else{
			//=========2015-12-01 ly===========
				 var ob1=goodsMaterialsSendItemGrid.store;
				 if(ob1.getCount() > 0){
					 for(var j=0;j<ob1.getCount();j++){
						 var no=ob1.getAt(j).get("id");
						 //根据ID删除
						 if(no!=null){
						 ajax("post", "/goods/mate/goodsMaterialsSend/delGoodsMaterialsSendOne.action", {
							 ids : no
						 }, function(data) {
							 if (data.success) {
								 message(biolims.common.deleteSuccess);
							 } else {
								 message(biolims.common.deleteFailed);
							 }
						 }, null);
						 }else{
							 goodsMaterialsSendItemGrid.store.removeAll();
						 }
					 }
					 //清屏
					 goodsMaterialsSendItemGrid.store.removeAll();
				 }
			//====================
			 
			 document.getElementById('goodsMaterialsSend_goodsMaterialsApply').value=rec.get('id');
			 document.getElementById('goodsMaterialsSend_goodsMaterialsApply_name').value=rec.get('name');
			 document.getElementById('goodsMaterialsSend_goodsMaterialsApply_applyOrganize_name').value=rec.get('goodsMaterialsReady-goodsMaterialsApply-applyOrganize-name');
			 var win = Ext.getCmp('GoodsMaterialsApplyFun');
			 if(win){win.close();}
				id = rec.get('id');
				name= rec.get('name');
				ajax("post", "/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendItemList.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = goodsMaterialsSendItemGrid.getStore().recordType;
							goodsMaterialsSendItemGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								
								p.set("goodsMaterialsApplyItem-id", obj.mid);
								p.set("goodsMaterialsApplyItem-goodsMaterialsPack-id", obj.pid);
								p.set("goodsMaterialsApplyItem-goodsMaterialsPack-name", obj.pname);
//								p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-code", obj.materialsId);
//								p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-name", obj.materialsName);
								p.set("goodsMaterialsApplyItem-applyNum", obj.applyNum);
//								p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name", obj.unitName);
								p.set("goodsMaterialsApplyItem-sendNum", obj.sendName);
								p.set("goodsMaterialsApplyItem-wayNum", obj.wayNum);
								p.set("goodsMaterialsApplyItem-unuseNum", obj.unuseNum);							
								p.set("goodsMaterialsApplyItem-usedNum", obj.usedNum);
								//p.set("goodsMaterialsReadyItem-state", obj.state);
								p.set("goodsMaterialsApply-id", id);
								p.set("goodsMaterialsApply-name", name);
//								p.set("goodsMaterialsApplyItem-goodsMaterialsApply-receiveUser", obj.linkMan);
//								p.set("goodsMaterialsApplyItem-goodsMaterialsApply-address-name", obj.address);							
//								p.set("goodsMaterialsApplyItem-goodsMaterialsApply-company-name", obj.company);
								
								goodsMaterialsSendItemGrid.getStore().add(p);							
							});
							
							goodsMaterialsSendItemGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
		 }
			
	}
}

//加载运输箱
//function loadBox(){
//	var code=$("#goodsMaterialsSend_id").val();
//	if(code=="NEW"){
//	ajax("post", "/goods/mate/goodsMaterialsSend/showTransBoxList.action", null, function(data) {
//		if (data.success) {
//			var ob = goodsMaterialsSendBoxGrid.getStore().recordType;
//			goodsMaterialsSendBoxGrid.stopEditing();
//			$.each(data.data, function(i, obj) {
//				var p = new ob({});
//				p.isNew = true;
//				p.set("transBox-id", obj.id);
//				p.set("transBox-name", obj.name);
//				p.set("boxSize", obj.boxSize);
//				p.set("num", "0");
//				
//				goodsMaterialsSendBoxGrid.getStore().add(p);				
//			});	
//			goodsMaterialsSendBoxGrid.startEditing(0, 0);	
//		} else {
//			message(biolims.common.anErrorOccurred);
//		}
//	}, null);
//	}
//}
