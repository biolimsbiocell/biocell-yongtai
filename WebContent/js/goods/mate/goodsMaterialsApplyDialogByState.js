var goodsMaterialsApplyByStateDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'applyOrganize-id',
		type:"string"
	});
	    fields.push({
		name:'applyOrganize-name',
		type:"string"
	});
//	    fields.push({
//		name:'applyDept-id',
//		type:"string"
//	});
	    fields.push({
		name:'applyDept',
		type:"string"
	});
	    fields.push({
		name:'address-id',
		type:"string"
	});
	    fields.push({
			name:'address-name',
			type:"string"
		});
//	    fields.push({
//		name:'type-id',
//		type:"string"
//	});
	    fields.push({
		name:'type',
		type:"string"
	});
//	    fields.push({
//		name:'receiveUser-id',
//		type:"string"
//	});
	    fields.push({
		name:'receiveUser',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
    fields.push({
		name:'sendDate',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
			name:'company-id',
			type:"string"
		});
		    fields.push({
			name:'company-name',
			type:"string"
		});
	    fields.push({
			name:'priority',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:30*10,
		sortable:true
	});
		cm.push({
		dataIndex:'applyOrganize-id',
		header:biolims.common.provincesId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'applyOrganize-name',
		header:biolims.common.provinces,
		width:15*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'applyDept-id',
//		header:'单位名称ID',
//		width:15*10,
//		hidden:true,
//		sortable:true
//		});
		cm.push({
		dataIndex:'applyDept',
		header:biolims.common.dept,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'address-id',
		header:biolims.common.consigneeAddressId,
		width:30*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'address-name',
		header:biolims.common.consigneeAddress,
		width:30*10,
		sortable:true
	});
//		cm.push({
//		dataIndex:'type-id',
//		header:'申请类型ID',
//		width:15*10,
//		hidden:true,
//		sortable:true
//		});
		cm.push({
		dataIndex:'type',
		header:biolims.common.dept,
		width:15*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'receiveUser-id',
//		header:'收件人ID',
//		width:15*10,
//		hidden:true,
//		sortable:true
//		});
		cm.push({
		dataIndex:'receiveUser',
		header:biolims.common.recipient,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.recipientPhone,
		width:30*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:15*10,
		sortable:true
	});
	var gridPriority = new Ext.form.ComboBox({
		transform : "grid_priority",
		width : 50,
		triggerAction : 'all',
		lazyRender : true
	});
	cm.push({
		dataIndex:'priority',
		header:biolims.goods.priority,
		width:15*6,
		sortable:true,
		editor : gridPriority,
		renderer : function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "0") {
				return biolims.goods.normal;
			} else if (value == "1") {
				return biolims.goods.high;
			}
		}
	});
	cm.push({
		dataIndex:'company-id',
		hidden:true,
		header:biolims.sample.companyId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'company-name',
		header:biolims.sample.companyName,
		
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sendDate',
		header:biolims.common.startDate,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsApply/showDialogGoodsMaterialsApplyListByStateJson.action";
	var opts={};
	opts.title=biolims.goods.materialApplication;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setGoodsMaterialsApplyFun(rec);
	};
	goodsMaterialsApplyByStateDialogGrid=gridTable("show_dialog_goodsMaterialsApplyByState_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"),biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(goodsMaterialsApplyByStateDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
