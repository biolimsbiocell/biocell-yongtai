var goodsMaterialsReadyGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-applyOrganize-name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-receiveUser',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-phone',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-address-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createUserName,
		width:15*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'goodsMaterialsApply-id',
		hidden:true,
		header:biolims.goods.ApplicationOrderId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'goodsMaterialsApply-name',
		header:biolims.goods.ApplicationOrder,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'goodsMaterialsApply-applyOrganize-name',
		header:biolims.common.provinces,
		width:40*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-receiveUser',
		header:biolims.common.recipient,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-phone',
		header:biolims.common.recipientPhone,
		width:40*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-address-name',
		header:biolims.common.consigneeAddress,
		width:40*6,
		
		sortable:true
	});

		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:biolims.wk.confirmUserId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.confirmUserName,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.wk.confirmDate,
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:40*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:40*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyListJson.action";
	var opts={};
	opts.title=biolims.goods.materialPreparation;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	goodsMaterialsReadyGrid=gridTable("show_goodsMaterialsReady_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/goods/mate/goodsMaterialsReady/editGoodsMaterialsReady.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/goods/mate/goodsMaterialsReady/editGoodsMaterialsReady.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/goods/mate/goodsMaterialsReady/viewGoodsMaterialsReady.action?id=' + id;
}
function exportexcel() {
	goodsMaterialsReadyGrid.title = biolims.common.exportList;
	var vExportContent = goodsMaterialsReadyGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				
				
				commonSearchAction(goodsMaterialsReadyGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
