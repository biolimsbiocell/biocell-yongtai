﻿var goodsMaterialsApplyItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'materials-id',
		type:"string"
	});
	   fields.push({
		name:'materials-name',
		type:"string"
	});
	   ////
   fields.push({
		name:'goodsMaterialsDetails-code',
		type:"string"
	});
//	   fields.push({
//		name:'goodsMaterialsDetails-name',
//		type:"string"
//	});
	   fields.push({
			name:'goodsMaterialsDetails-id',
			type:"string"
		});
		   fields.push({
			name:'goodsMaterialsDetails-name',
			type:"string"
		});
   fields.push({
		name:'goodsMaterialsPack-id',
		type:"string"
	});
	   fields.push({
		name:'goodsMaterialsPack-name',
		type:"string"
	});
	   fields.push({
			name:'goodsMaterialsPack-num',
			type:"string"
		});
	    fields.push({
			name:'unitName',
			type:"string"
		});
	    ////
	   fields.push({
		name:'applyNum',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsDetails-unit-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsDetails-unit-name',
		type:"string"
	});
	   fields.push({
		name:'wayNum',
		type:"string"
	});
	   fields.push({
		name:'unuseNum',
		type:"string"
	});
	   fields.push({
		name:'usedNum',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsPack-id',
		hidden : false,
		header:biolims.master.materialPackageId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsPack-name',
		hidden : false,
		header:biolims.master.materialPackageName,
		width:15*10
	});
	cm.push({
		dataIndex:'goodsMaterialsPack-num',
		hidden : true,
		header:biolims.master.materialPackageNum,
		width:15*10
	});
	cm.push({
		dataIndex:'goodsMaterialsDetails-code',
		hidden : true,
		header:biolims.master.materialDetailId,
		width:40*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'goodsMaterialsDetails-name',
		hidden : true,
		header:biolims.master.materialDetail,
		width:40*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'goodsMaterialsDetails-id',
		hidden : true,
		header:biolims.master.materialDetailId,
		width:40*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
//	cm.push({
//		dataIndex:'goodsMaterialsDetails-name',
//		hidden : true,
//		header:'物料明细名称',
//		width:40*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
	dataIndex:'goodsMaterialsDetails-unit-name',
	hidden : true,
	header:biolims.common.unit,
	width:15*10
//	editor : new Ext.form.TextField({
//		allowBlank : true
//	})
});
	cm.push({
		dataIndex:'applyNum',
		hidden : false,
		header:biolims.goods.applyNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'wayNum',
		hidden : false,
		header:biolims.goods.wayNum,
		width:20*6
	});
	//鼠标聚焦时触发事件 
	var code =new Ext.form.TextField({
            allowBlank: false
    });
	code.on('focus', function() {
		var selectRecord = goodsMaterialsApplyItemGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("goodsMaterialsPack-id");
				loadMaterials(code);
			});
		}
	});
//	cm.push({
//		dataIndex:'unuseNum',
//		hidden : false,
//		header:'未使用数量',
//		width:20*6,
//		editor : code
//	});
	//鼠标聚焦时触发事件 
//	var code1 =new Ext.form.TextField({
//            allowBlank: false
//    });
//	code1.on('focus', function() {
//		var selectRecord = goodsMaterialsApplyItemGrid.getSelectionModel();
//		if (selectRecord.getSelections().length > 0) {
//			$.each(selectRecord.getSelections(), function(i, obj) {
//				var code=obj.get("goodsMaterialsPack-id");
//				loadMaterialsOut(code);
//			});
//		}
//	});
//	cm.push({
//		dataIndex:'usedNum',
//		hidden : false,
//		header:'过期数量',
//		width:20*6,
//		editor:code1
////		
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
//	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsApply/showGoodsMaterialsApplyItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.goods.materialApplicationDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state=$("#goodsMaterialsApply_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/goods/mate/goodsMaterialsApply/delGoodsMaterialsApplyItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				goodsMaterialsApplyItemGrid.getStore().commitChanges();
				goodsMaterialsApplyItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.goods.selectMaterial,
		handler : loadMaterialsPack
	});	
//	opts.tbar.push({
//			text : '选择单位',
//			handler : selectunitFun
//		});
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectgoodsMaterialsApplyFun
//		});
//	
//	
//	
//	
//	
//	
//	

//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = goodsMaterialsApplyItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							goodsMaterialsApplyItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
//	opts.tbar.push({
//		text : '选择物料',
//		handler : showStorageList
//	});
	}
	goodsMaterialsApplyItemGrid=gridEditTable("goodsMaterialsApplyItemdiv",cols,loadParam,opts);
	$("#goodsMaterialsApplyItemdiv").data("goodsMaterialsApplyItemGrid", goodsMaterialsApplyItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//加载库存主数据
function showStorageList(){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, biolims.common.theInventoryMasterData, url, {
		"Confirm" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					goodsMaterialsApplyItemGrid.stopEditing();
					var ob = goodsMaterialsApplyItemGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("goodsMaterialsPack-id",obj.get("id"));
					p.set("goodsMaterialsPack-name",obj.get("name"));
				
					goodsMaterialsApplyItemGrid.getStore().add(p);	
				});
				goodsMaterialsApplyItemGrid.startEditing(0, 0);
				options.close();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	}, true, options);
}
function loadMaterialsPack(){
	var options = {};
	options.width = 500;
	options.height = 460;
	var url="/goods/pack/goodsMaterialsPack/goodsMaterialsPackSelectByState.action";
	loadDialogPage(null, biolims.common.selectedDetail, url, {
		 "Confirm": function() {
				var selectRecord=goodsMaterialsPackDialogGridByState.getSelectionModel();
				var ob = goodsMaterialsApplyItemGrid.getStore().recordType;
				if(selectRecord.getSelections().length>0){
					$.each(selectRecord.getSelections(), function(i, obj) {
						var code = obj.get("id");
						goodsMaterialsApplyItemGrid.stopEditing();	
						var selRecord = goodsMaterialsApplyItemGrid.store;
						var isRepeat = false;
						for(var j=0;j<selRecord.getCount();j++){
							var oldv = selRecord.getAt(j).get("goodsMaterialsPack-id");
							if(oldv == obj.get("id")){
								isRepeat = true;
								message(biolims.common.haveDuplicate);
								break;
							}
						}
						if(!isRepeat){
							var p = new ob({});
							p.isNew = true;
							p.set("goodsMaterialsPack-id", obj.get("id"));
							ajax("post", "/goods/mate/goodsMaterialsApply/selectUnseNum.action", {
								code : code,
								}, function(data) {
									if (data.success) {
										p.set("unuseNum", data.data);
										p.set("usedNum", data.data1);
									}
								});
							p.set("goodsMaterialsPack-name", obj.get("name"));
							p.set("goodsMaterialsPack-num", obj.get("num"));
							goodsMaterialsApplyItemGrid.getStore().add(p);							
							goodsMaterialsApplyItemGrid.startEditing(0, 0);
						}
						options.close();
					});
				}else {
					message(biolims.common.anErrorOccurred);
				}
					
		 }
	}, true, options);
}
////物料明细
//function setGoodsMaterialsPackFun(rec){
//			var id = rec.get('id');		
//			ajax("post", "/goods/pack/goodsMaterialsPack/showPack.action", {
//				code : id,
//				}, function(data) {
//					if (data.success) {	
//						var ob = goodsMaterialsApplyItemGrid.getStore().recordType;
//						goodsMaterialsApplyItemGrid.stopEditing();						
//						var selRecord = goodsMaterialsApplyItemGrid.store;
//						var isRepeat = false;
//						for(var j=0;j<selRecord.getCount();j++){
//							var oldv = selRecord.getAt(j).get("goodsMaterialsPack-id");
//							if(oldv == id){
//								isRepeat = true;
//								message("数据重复，请重新选择！");
//								break;
//							}
//						}
//						if(!isRepeat){
//						$.each(data.data, function(i, obj) {
//							var p = new ob({});
//							p.isNew = true;
//							
//							p.set("goodsMaterialsDetails-id", obj.id);
//							p.set("goodsMaterialsDetails-code", obj.code);
//							p.set("goodsMaterialsDetails-name", obj.name);
//							p.set("goodsMaterialsDetails-unit-name", obj.unit);
//							p.set("goodsMaterialsPack-id", obj.mid);
//							p.set("goodsMaterialsPack-name", obj.mname);
//							
//							goodsMaterialsApplyItemGrid.getStore().add(p);							
//						});
//						
//						goodsMaterialsApplyItemGrid.startEditing(0, 0);
//						}
//						
//					} else {
//						message("获取明细数据时发生错误！");
//					}
//					
//				}, null);
//			var win = Ext.getCmp('GoodsMaterialsPackFun');
//			if(win){
//				win.close();
//			}
//	 }




function selectunitFun(){
	var win = Ext.getCmp('selectunit');
	if (win) {win.close();}
	var selectunit= new Ext.Window({
	id:'selectunit',modal:true,title:biolims.common.selectUnit,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=unit' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectunit.close(); }  }]  });     selectunit.show(); }
	function setunit(id,name){
		var gridGrid = $("#goodsMaterialsApplyItemdiv").data("goodsMaterialsApplyItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('unit-id',id);
			obj.set('unit-name',name);
		});
		var win = Ext.getCmp('selectunit');
		if(win){
			win.close();
		}
	}
//	function selectmaterialsFun(){
//		var win = Ext.getCmp('selectmaterials');
//		if (win) {win.close();}
//		var selectmaterials= new Ext.Window({
//		id:'selectmaterials',modal:true,title:'选择物料包',layout:'fit',width:500,height:500,closeAction:'close',
//		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//		collapsible: true,maximizable: true,
//		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=materials' frameborder='0' width='100%' height='100%' ></iframe>"}),
//		buttons: [
//		{ text: '关闭',
//		 handler: function(){
//			 selectmaterials.close(); }  }]  });     selectmaterials.show(); }
//		function setmaterials(id,name){
//			var gridGrid = $("#goodsMaterialsApplyItemdiv").data("goodsMaterialsApplyItemGrid");
//			var selRecords = gridGrid.getSelectionModel().getSelections(); 
//			$.each(selRecords, function(i, obj) {
//				obj.set('materials-id',id);
//				obj.set('materials-name',name);
//			});
//			var win = Ext.getCmp('selectmaterials')
//			if(win){
//				win.close();
//			}
//		}
function selectgoodsMaterialsApplyFun(){
	var win = Ext.getCmp('selectgoodsMaterialsApply');
	if (win) {win.close();}
	var selectgoodsMaterialsApply= new Ext.Window({
	id:'selectgoodsMaterialsApply',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsApplySelect.action?flag=goodsMaterialsApply' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsMaterialsApply.close(); }  }]  });     selectgoodsMaterialsApply.show(); }
	function setgoodsMaterialsApply(id,name){
		var gridGrid = $("#goodsMaterialsApplyItemdiv").data("goodsMaterialsApplyItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('goodsMaterialsApply-id',id);
			obj.set('goodsMaterialsApply-name',name);
		});
		var win = Ext.getCmp('selectgoodsMaterialsApply');
		if(win){
			win.close();
		}
	}
	//根据物料包Id加载未使用物料明细
	function loadMaterials(code){
			var options = {};
			options.width = 400;
			options.height = 460;
			var url="/goods/mate/goodsMaterialsReady/showDetailsListIsNotUse.action?code="+code;
			loadDialogPage(null, biolims.goods.unusedBloodVesselDetail, url, {
				 "Confirm": function() {
					 options.close();
				}
			}, true, options);
	}
	//根据物料包Id加载过期的物料明细
	function loadMaterialsOut(code){
			var options = {};
			options.width = 400;
			options.height = 460;
			var url="/goods/mate/goodsMaterialsReady/showDetailsListOut.action?code="+code;
			loadDialogPage(null, biolims.goods.expiredBloodVesselDetail, url, {
				 "Confirm": function() {
					 options.close();
				}
			}, true, options);
	}	
