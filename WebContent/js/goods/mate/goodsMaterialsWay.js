var goodsMaterialsWayGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'expressOrder',
		type:"string"
	});
	   fields.push({
		name:'checked',
		type:"string"
	});
	   fields.push({
		name:'address',
		type:"string"
	});
	   fields.push({
		name:'company',
		type:"string"
	});
	   fields.push({
		name:'expectDate',
		type:"string",
		//dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'realDate',
		type:"string",
		//dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'way',
		type:"string"
	});
   fields.push({
		name:'isSend',
		type:"string"
	});
   fields.push({
		name:'sendDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsPostItem-id',
		type:"string"
	});
//	    fields.push({
//		name:'goodsMaterialsPost-name',
//		type:"string"
//	});
	fields.push({
			name:'receiver',
			type:"string"
	});
//	fields.push({
//			name:'dateOfLodgment',
//			type:"date",
//			dateFormat:"Y-m-d"
//	});
	fields.push({
		name:'tackDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	fields.push({
		name:'roundTackDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'expressOrder',
		hidden : false,
		header:biolims.sample.expreceCode,
		width:40*6
	});
	var isSend = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isSend',
		hidden : false,
		header:biolims.goods.isSend,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(isSend),editor: isSend
	});
	cm.push({
		dataIndex:'sendDate',
		hidden : false,
		header:biolims.goods.sendDate,
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'checked',
		hidden : false,
		header:biolims.common.checked,
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'address',
		hidden : false,
		header:biolims.common.consigneeAddress,
		width:40*6
	});
	cm.push({
		dataIndex:'company',
		hidden : false,
		header:biolims.sample.companyName,
		width:20*6
	});
	cm.push({
		dataIndex:'receiver',
		hidden : false,
		header:biolims.common.recipient,
		width:20*6
	});
	cm.push({
		dataIndex:'tackDate',
		hidden : false,
		header:biolims.goods.receiveDate,
		width:25*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'roundTackDate',
		hidden : false,
		header:biolims.goods.roundTackDate,
		width:25*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'expectDate',
		hidden : false,
		header:biolims.goods.predictDate,
		width:25*6
//		
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});

	cm.push({
		dataIndex:'realDate',
		hidden : false,
		header:biolims.goods.realDate,
		width:25*6
//		
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'way',
		hidden : false,
		header:biolims.goods.way,
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.goods.sendDetailId,
		width:15*10
	});
	cm.push({
		dataIndex:'goodsMaterialsPostItem-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10
	});
//	cm.push({
//		dataIndex:'goodsMaterialsPost-name',
//		hidden : true,
//		header:'相关主表',
//		width:15*10
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsWay/showGoodsMaterialsWayListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.goods.expressList;
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : search
//	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexcel
	});
	opts.tbar.push({
		text : biolims.common.batchOper,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_way_div"), biolims.common.batchOper, null, {
				"Confirm" : function() {
					var records = goodsMaterialsWayGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isSend = $("#isSend").val();
						goodsMaterialsWayGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isSend", isSend);
						});
						goodsMaterialsWayGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchDate,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_date_div"), biolims.common.batchDate, null, {
				"Confirm" : function() {
					var records = goodsMaterialsWayGrid.getSelectRecord();
					if (records && records.length > 0) {
						var sendDate = $("#sendDate").val();
						sendDate = sendDate.replace(/-/g,"/");
						var date = new Date(sendDate);
						goodsMaterialsWayGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("sendDate", date);
						});
						goodsMaterialsWayGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	goodsMaterialsWayGrid=gridEditTable("show_goodsMaterialsWay_div",cols,loadParam,opts);
	$("#show_goodsMaterialsWay_div").data("goodsMaterialsWayGrid", goodsMaterialsWayGrid);
});

//保存
function save(){	
	//var selectRecord = goodsMaterialsWayGrid.getSelectionModel();
	//var inItemGrid = $("#show_goodsMaterialsWay_div").data("goodsMaterialsWayGrid");
	var itemJson = commonGetModifyRecords(goodsMaterialsWayGrid);
	//if(selectRecord.getSelections().length>0){
		//$.each(selectRecord.getSelections(), function(i, obj) {
	if(itemJson.length>0){
			ajax("post", "/goods/mate/goodsMaterialsWay/setMaterialsToWay.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					//$("#show_goodsMaterialsWay_div").data("goodsMaterialsWayGrid").getStore().commitChanges();
					//$("#show_goodsMaterialsWay_div").data("goodsMaterialsWayGrid").getStore().reload();
					goodsMaterialsWayGrid.getStore().commitChanges();
					goodsMaterialsWayGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		//});
	}else{
		message(biolims.common.noData2Save);
	}
	
}
function exportexcel() {
	goodsMaterialsWayGrid.title = biolims.common.exportList;
	var vExportContent = goodsMaterialsWayGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function search() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(goodsMaterialsWayGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
}
function selectInfo(){
	commonSearchAction(goodsMaterialsWayGrid);
	$("#goodsMaterialsWay_expressOrder").val("");
	$("#goodsMaterialsWay_company").val("");
	$("#goodsMaterialsWay_address").val("");
	$("#goodsMaterialsWay_receiver").val("");
}

