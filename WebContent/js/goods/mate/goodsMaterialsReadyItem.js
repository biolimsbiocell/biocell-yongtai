﻿var goodsMaterialsReadyItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
//   fields.push({
//		name:'goodsMaterialsApplyItem-goodsMaterialsDetails-code',
//		type:"string"
//	});
//  fields.push({
//		name:'goodsMaterialsApplyItem-goodsMaterialsDetails-name',
//		type:"string"
//	});
  fields.push({
		name:'goodsMaterialsApplyItem-goodsMaterialsPack-id',
		type:"string"
	});
fields.push({
		name:'goodsMaterialsApplyItem-goodsMaterialsPack-name',
		type:"string"
	});
fields.push({
	name:'goodsMaterialsApplyItem-goodsMaterialsPack-num',
	type:"string"
});
	   fields.push({
		name:'goodsMaterialsApplyItem-applyNum',
		type:"string"
	});
//	   fields.push({
//		name:'goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name',
//		type:"string"
//	});
	   fields.push({
		name:'sendNum',
		type:"string"
	});
	   fields.push({
		name:'goodsMaterialsApplyItem-wayNum',
		type:"string"
	});
	   fields.push({
		name:'goodsMaterialsApplyItem-unuseNum',
		type:"string"
	});
	   fields.push({
		name:'goodsMaterialsApplyItem-usedNum',
		type:"string"
	});
   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsReady-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsReady-name',
		type:"string"
	});
//	    fields.push({
//			name:'goodsMaterialsApply-id',
//			type:"string"
//		});
//		    fields.push({
//			name:'goodsMaterialsApply-name',
//			type:"string"
//		});
    fields.push({
		name:'goodsMaterialsApplyItem-id',
		type:"string"
	});
    //===========
    fields.push({
    	name:'note',
    	type:"string"
    });
    //===========
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//鼠标聚焦时触发事件 
	var code =new Ext.form.TextField({
            allowBlank: false
    });
	code.on('focus', function() {
		var selectRecord = goodsMaterialsReadyItemGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-id");
				loadMaterials(code);
			});
		}
	});
	cm.push({
		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsPack-id',
		hidden : false,
		header:biolims.master.materialPackageId,
		width:20*6,

		editor : code
	});
	cm.push({
		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsPack-num',
		hidden : false,
		header:biolims.goods.materialPackageDefaultNumber,
		width:20*6,

		editor : code
	});
	cm.push({
		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsPack-name',
		hidden : false,
		header:biolims.master.materialPackageName,
		width:40*6

	});
//	cm.push({
//		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsDetails-code',
//		hidden : false,
//		header:'物料编号',
//		width:40*6
//
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsDetails-name',
//		hidden : false,
//		header:'物料名称',
//		width:40*6
//
//	});
	cm.push({
		dataIndex:'goodsMaterialsApplyItem-applyNum',
		hidden : false,
		header:biolims.goods.applyNum,
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
//	cm.push({
//		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name',
//		hidden : false,
//		header:'单位',
//		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'sendNum',
		hidden : false,
		header:biolims.goods.sendNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'goodsMaterialsApplyItem-wayNum',
		hidden : false,
		header:'在途数量',
		width:20*6
	});*/
	/*cm.push({
		dataIndex:'goodsMaterialsApplyItem-unuseNum',
		hidden : false,
		header:'未使用数量',
		width:20*6
	});*/
	/*cm.push({
		dataIndex:'goodsMaterialsApplyItem-usedNum',
		hidden : false,
		header:'过期数量',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:20*6
	});
	//================
	cm.push({
		dataIndex:'note',
		hidden:false,
		header:biolims.common.note,
		width:50*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//================
	cm.push({
		dataIndex:'goodsMaterialsReady-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsReady-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
//	cm.push({
//		dataIndex:'goodsMaterialsApply-id',
//		hidden : true,
//		header:'准备主表ID',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsApply-name',
//		hidden : true,
//		header:'准备主表',
//		width:15*10
//	});
	cm.push({
		dataIndex:'goodsMaterialsApplyItem-id',
		hidden : true,
		header:biolims.master.relatedTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'goodsMaterialsApplyItem-name',
//		hidden : true,
//		header:'准备子表',
//		width:15*10
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.goods.materialPreparationDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state=$("#goodsMaterialsReady_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/goods/mate/goodsMaterialsReady/delGoodsMaterialsReadyItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				goodsMaterialsReadyItemGrid.getStore().commitChanges();
				goodsMaterialsReadyItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectgoodsMaterialsReadyFun
//		});
//	
//	
//	
//	
	
	
	
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = goodsMaterialsReadyItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							goodsMaterialsReadyItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		text : '删除选中',
//		handler : null
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.goods.copyDetail,
		handler : function(){
			var selectRecord = goodsMaterialsReadyItemGrid.getSelectionModel();
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					var ob = goodsMaterialsReadyItemGrid.getStore().recordType;
					goodsMaterialsReadyItemGrid.stopEditing();
					var p = new ob({});
					p.isNew = true;
					p.set("goodsMaterialsApplyItem-goodsMaterialsPack-id",obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-id"));			
					p.set("goodsMaterialsApplyItem-goodsMaterialsPack-name",obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-name"));			
//					p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-code",obj.get("goodsMaterialsApplyItem-goodsMaterialsDetails-code"));			
//					p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-name",obj.get("goodsMaterialsApplyItem-goodsMaterialsDetails-name"));			
					p.set("goodsMaterialsApplyItem-applyNum",obj.get("goodsMaterialsApplyItem-applyNum"));			
//					p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name",obj.get("goodsMaterialsApplyItem-goodsMaterialsDetails-unit-name"));			
//					p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-name",obj.get("goodsMaterialsApplyItem-goodsMaterialsDetails-name"));			
//					p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-name",obj.get("goodsMaterialsApplyItem-goodsMaterialsDetails-name"));			
//					p.set("goodsMaterialsApplyItem-goodsMaterialsDetails-name",obj.get("goodsMaterialsApplyItem-goodsMaterialsDetails-name"));			
					p.set("sendNum",obj.get("sendNum"));			
					p.set("state",obj.get("state"));			
					p.set("note",obj.get("note"));
					//goodsMaterialsReady-name
					p.set("goodsMaterialsReady-name",obj.get("goodsMaterialsReady-name"));
					p.set("goodsMaterialsReady-id",obj.get("goodsMaterialsReady-id"));
					p.set("goodsMaterialsApplyItem-id",obj.get("goodsMaterialsApplyItem-id"));
										
					goodsMaterialsReadyItemGrid.getStore().add(p);
					goodsMaterialsReadyItemGrid.startEditing(0, 0);
				});
			}
		}
	});
	opts.tbar.push({
		text : biolims.goods.generateMaterialDetailsBarcode,
		handler : editDetails
	});
	}
	goodsMaterialsReadyItemGrid=gridEditTable("goodsMaterialsReadyItemdiv",cols,loadParam,opts);
	$("#goodsMaterialsReadyItemdiv").data("goodsMaterialsReadyItemGrid", goodsMaterialsReadyItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function editDetails(){
	var selectRecord = goodsMaterialsReadyItemGrid.getSelectionModel();
	var selRecord = goodsMaterialsReadyDetailsGrid.store;
	var flag=false;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			var pCode=obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-id");
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("packCode");
				if(oldv == pCode){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;
				}
			}
			//ajax查询物料包是否要生成条码
			ajax("post", "/goods/pack/goodsMaterialsPack/selectSamplePack.action", {
				code : pCode
			}, function(data) {
				if(data.data[0].isCode==1)
					flag=true;
				if(flag){
					if(!isRepeat){
						var num=obj.get("sendNum");
						//ajax查询统一物料包的办事处申请的重用编码
						ajax("post", "/system/organize/officeApply/getReCode.action", {
							code : pCode,createUser:$("#goodsMaterialsReady_goodsMaterialsApply").val()
						}, function(data) {
							if (data.success){
								for(var i=0;i<num-data.size;i++){
									var ob = goodsMaterialsReadyDetailsGrid.getStore().recordType;
									goodsMaterialsReadyDetailsGrid.stopEditing();
									var p = new ob({});
									p.isNew = true;
									p.set("num",obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-num"));				
									p.set("packCode",obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-id"));				
									goodsMaterialsReadyDetailsGrid.getStore().add(p);
									goodsMaterialsReadyDetailsGrid.startEditing(0, 0);
								}
								for(var j=0;j<=data.size;j++){
									var ob1 = goodsMaterialsReadyDetailsGrid.getStore().recordType;
									goodsMaterialsReadyDetailsGrid.stopEditing();
									var p1 = new ob1({});
									//alert(data.size);
									//alert(data.date);
									//alert(data.date[j]);
									p1.isNew = true;
									p1.set("num","1");				
									p1.set("code",data.date[j]);
									p1.set("packCode",obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-id"));
									goodsMaterialsReadyDetailsGrid.getStore().add(p1);
									goodsMaterialsReadyDetailsGrid.startEditing(0, 0);
								}
								message(biolims.goods.generateDetailsBarcodeSuccess);
							}
						});
						
					}
				}
				
				
				
			});
		
			
		});
		
	}else{
		message(biolims.common.pleaseSelect);
		return;
	}
	
}
function selectgoodsMaterialsReadyFun(){
	var win = Ext.getCmp('selectgoodsMaterialsReady');
	if (win) {win.close();}
	var selectgoodsMaterialsReady= new Ext.Window({
	id:'selectgoodsMaterialsReady',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsReadySelect.action?flag=goodsMaterialsReady' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsMaterialsReady.close(); }  }]  });     selectgoodsMaterialsReady.show(); }
	function setgoodsMaterialsReady(id,name){
		var gridGrid = $("#goodsMaterialsReadyItemdiv").data("goodsMaterialsReadyItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('goodsMaterialsReady-id',id);
			obj.set('goodsMaterialsReady-name',name);
		});
		var win = Ext.getCmp('selectgoodsMaterialsReady');
		if(win){
			win.close();
		}
	}
//根据物料包Id加载物料明细
function loadMaterials(code){
		var options = {};
		options.width = 900;
		options.height = 460;
		var url="/goods/pack/goodsMaterialsPack/showGoodsMaterialsDetailsListById.action?code="+code;
		loadDialogPage(null, biolims.goods.checkDetail, url, {
			 "Confirm": function() {
				 options.close();
			}
		}, true, options);
}
