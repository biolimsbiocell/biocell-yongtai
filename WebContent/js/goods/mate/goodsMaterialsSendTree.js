function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/goods/mate/goodsMaterialsSend/editGoodsMaterialsSend.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/goods/mate/goodsMaterialsSend/viewGoodsMaterialsSend.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : biolims.common.id,
		dataIndex : 'id',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.name,
		dataIndex : 'name',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : biolims.sample.createUserName,
		dataIndex : 'createUser-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : biolims.sample.createDate,
		dataIndex : 'createDate',
		width:15*6,
		hidden:false
	}, 
	
	    {
		header : biolims.goods.materialPreparation,
		dataIndex : 'goodsMaterialsReady-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : biolims.goods.applyOrganization,
		dataIndex : 'goodsMaterialsApply',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.recipient,
		dataIndex : 'receiveUser',
		width:40*6,
		hidden:false
	}, 
	
	    {
		header : biolims.common.recipientPhone,
		dataIndex : 'phone-name',
		width:40*6,
		hidden:false
	}, 
	   
	{
		header : biolims.common.consigneeAddress,
		dataIndex : 'address',
		width:40*6,
		hidden:false
	}, 
	
	    {
		header : biolims.sample.companyName,
		dataIndex : 'company-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : biolims.storage.carryingCaseSummation,
		dataIndex : 'num',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.wk.confirmUserName,
		dataIndex : 'confirmUser-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : biolims.wk.confirmDate,
		dataIndex : 'confirmDate',
		width:15*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.workFlowState,
		dataIndex : 'state',
		width:40*6,
		hidden:true
	}, 
	
	   
	{
		header : biolims.common.workFlowStateName,
		dataIndex : 'stateName',
		width:40*6,
		hidden:false
	}, 
	
			
			
			
			{
				header : biolims.common.upId,
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#goodsMaterialsSendTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
