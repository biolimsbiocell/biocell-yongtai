﻿var goodsMaterialsSendItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});

	    fields.push({
		name:'goodsMaterialsApplyItem-applyNum',
		type:"string"
	});
	   
    fields.push({
		name:'goodsMaterialsApplyItem-wayNum',
		type:"string"
	});
    fields.push({
		name:'goodsMaterialsApplyItem-unuseNum',
		type:"string"
	});
    fields.push({
		name:'goodsMaterialsApplyItem-usedNum',
		type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'goodsMaterialsApplyItem-id',
		type:"string"
	});
    fields.push({
		name:'goodsMaterialsApply-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-name',
		type:"string"
	});
	   
	    fields.push({
			name:'goodsMaterialsApplyItem-goodsMaterialsPack-id',
			type:"string"
		});
	fields.push({
			name:'goodsMaterialsApplyItem-goodsMaterialsPack-name',
			type:"string"
		});

	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//鼠标聚焦时触发事件 
	var code =new Ext.form.TextField({
            allowBlank: false
    });
	code.on('focus', function() {
		var selectRecord = goodsMaterialsSendItemGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("goodsMaterialsApplyItem-goodsMaterialsPack-id");
				loadMaterials(code);
			});
		}
		
	});
	cm.push({
		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsPack-id',
		hidden : false,
		header:biolims.master.materialPackageId,
		width:20*6,

		editor : code
	});

	cm.push({
		dataIndex:'goodsMaterialsApplyItem-goodsMaterialsPack-name',
		hidden : false,
		header:biolims.master.materialPackageName,
		width:20*6
	});

	cm.push({
		dataIndex:'goodsMaterialsApplyItem-applyNum',
		hidden : false,
		header:biolims.goods.applyNum,
		width:20*10
	});

	cm.push({
		dataIndex:'goodsMaterialsApplyItem-sendNum',
		hidden : false,
		header:biolims.goods.sendNum,
		width:20*10
	});
//	cm.push({
//		dataIndex:'goodsMaterialsReadyItem-goodsMaterialsApplyItem-wayNum',
//		hidden : false,
//		header:'在途数量',
//		width:20*10
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsReadyItem-goodsMaterialsApplyItem-unuseNum',
//		hidden : false,
//		header:'未使用数量',
//		width:20*10
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsValidity',
//		hidden : false,
//		header:'有效期',
//		width:15*10,
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsReadyItem-goodsMaterialsApplyItem-usedNum',
//		hidden : false,
//		header:'过期数量',
//		width:20*10
//	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:20*10
	});
	cm.push({
		dataIndex:'goodsMaterialsApplyItem-id',
		hidden : true,
		header:biolims.goods.materialPreparationDetailId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'goodsMaterialsReadyItem-goodsMaterialsApplyItem-goodsMaterialsApply-receiveUser',
//		hidden : true,
//		header:'联系人',
//		width:20*10
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsReadyItem-goodsMaterialsApplyItem-goodsMaterialsApply-address-name',
//		hidden : true,
//		header:'联系地址',
//		width:20*10
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsReadyItem-goodsMaterialsApplyItem-goodsMaterialsApply-company-name',
//		hidden : true,
//		header:'快递公司',
//		width:20*10
//	});
//	cm.push({
//		dataIndex:'goodsMaterialsReadyItem-name',
//		hidden : true,
//		header:'物料准备明细',
//		width:15*10
//	});
	cm.push({
		dataIndex:'goodsMaterialsApply-id',
		hidden : true,
		header:biolims.goods.materialPreparationId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-name',
		hidden : true,
		header:biolims.goods.materialPreparation,
		width:15*10
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendItemListJson.action?id="+ $("#id_parent_hidden").val()+"&appId="+ $("#goodsMaterialsSend_goodsMaterialsApply").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.goods.materialIssueDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state=$("#goodsMaterialsSend_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/goods/mate/goodsMaterialsSend/delGoodsMaterialsSendItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				goodsMaterialsSendItemGrid.getStore().commitChanges();
				goodsMaterialsSendItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//	opts.tbar.push({
//		text : '生成快递单',
//		handler : editDetails
//	});
//	opts.tbar.push({
//			text : '选择运输箱种类',
//			handler : selecttypeFun
//		});
//    
//	opts.tbar.push({
//			text : '选择数量',
//			handler : selectnumFun
//		});
//    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectgoodsMaterialsSendFun
//		});
//	
	
	
	
	
	
	
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = goodsMaterialsSendItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							goodsMaterialsSendItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	}
	goodsMaterialsSendItemGrid=gridEditTable("goodsMaterialsSendItemdiv",cols,loadParam,opts);
	$("#goodsMaterialsSendItemdiv").data("goodsMaterialsSendItemGrid", goodsMaterialsSendItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

//function selecttypeFun(){
//	var win = Ext.getCmp('selecttype');
//	if (win) {win.close();}
//	var selecttype= new Ext.Window({
//	id:'selecttype',modal:true,title:'选择运输箱种类',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.ItemComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=type' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selecttype.close(); }  }]  });     selecttype.show(); }
//	function settype(id,name){
//		var gridGrid = $("#goodsMaterialsSendItemdiv").data("goodsMaterialsSendItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('type-id',id);
//			obj.set('type-name',name);
//		});
//		var win = Ext.getCmp('selecttype')
//		if(win){
//			win.close();
//		}
//	}
	
//function selectnumFun(){
//	var win = Ext.getCmp('selectnum');
//	if (win) {win.close();}
//	var selectnum= new Ext.Window({
//	id:'selectnum',modal:true,title:'选择数量',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.ItemComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/IntegerSelect.action?flag=num' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectnum.close(); }  }]  });     selectnum.show(); }
//	function setnum(id,name){
//		var gridGrid = $("#goodsMaterialsSendItemdiv").data("goodsMaterialsSendItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('num-id',id);
//			obj.set('num-name',name);
//		});
//		var win = Ext.getCmp('selectnum')
//		if(win){
//			win.close();
//		}
//	}
//function saveToApply(){
//	var id=$("#goodsMaterialsSend_id").val();
//	ajax("post", "/goods/mate/goodsMaterialsSend/setExpressToWay.action", {
//		code : id
//	}, function(data) {
//		if (data.success) {
//			message("保存成功！");
//		} else {
//			message("保存失败！");
//		}
//	}, null);
//}
//生成快递单
function editDetails(){
	var id=$("#goodsMaterialsSend_id").val();
	var selectRecord = goodsMaterialsSendItemGrid.getSelectionModel();
	var selRecord = goodsMaterialsSendExpressGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("code");
				if(oldv == obj.get("goodsMaterialsReadyItem-goodsMaterialsApplyItem-goodsMaterialsDetails-code")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;
				}
			}
			if(!isRepeat){
			var ob = goodsMaterialsSendExpressGrid.getStore().recordType;
			goodsMaterialsSendExpressGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("code", obj.get("goodsMaterialsReadyItem-goodsMaterialsApplyItem-goodsMaterialsDetails-code"));
			goodsMaterialsSendExpressGrid.getStore().add(p);			
			message(biolims.goods.generatedExpressOrderSuccess);
			}
		});
		goodsMaterialsSendExpressGrid.startEditing(0, 0);
	}else{
		message(biolims.common.pleaseSelect);
		return;
		
	}
	
}

function selectgoodsMaterialsSendFun(){
	var win = Ext.getCmp('selectgoodsMaterialsSend');
	if (win) {win.close();}
	var selectgoodsMaterialsSend= new Ext.Window({
	id:'selectgoodsMaterialsSend',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.ItemComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsSendSelect.action?flag=goodsMaterialsSend' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsMaterialsSend.close(); }  }]  });     selectgoodsMaterialsSend.show(); }
	function setgoodsMaterialsSend(id,name){
		var gridGrid = $("#goodsMaterialsSendItemdiv").data("goodsMaterialsSendItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('goodsMaterialsSend-id',id);
			obj.set('goodsMaterialsSend-name',name);
		});
		var win = Ext.getCmp('selectgoodsMaterialsSend')
		if(win){
			win.close();
		}
	}
	//根据物料包Id加载物料明细
	function loadMaterials(code){
			var options = {};
			options.width = 900;
			options.height = 460;
			var url="/goods/pack/goodsMaterialsPack/showGoodsMaterialsDetailsListById.action?code="+code;
			loadDialogPage(null, biolims.common.selectedDetail, url, {
				 "Confirm": function() {
					 options.close();
				}
			}, true, options);
	}
	
