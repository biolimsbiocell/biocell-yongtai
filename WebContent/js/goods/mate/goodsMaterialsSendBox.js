﻿var goodsMaterialsSendBoxGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'transBox-id',
		type:"string"
	});
	    fields.push({
		name:'transBox-name',
		type:"string"
	});
	   fields.push({
		name:'boxSize',
		type:"string"
	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-name',
		type:"string"
	});
	    fields.push({
			name:'goodsMaterialsSend-id',
			type:"string"
		});
	    fields.push({
			name:'goodsMaterialsSend-name',
			type:"string"
		});
	    
	    
	    
	    fields.push({
			name:'boxType',
			type:"string"
		});
	    fields.push({
			name:'boxName',
			type:"string"
		});
	    fields.push({
			name:'boxSpec',
			type:"string"
		});
	    fields.push({
			name:'boxNum',
			type:"string"
		});
	    
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'boxType',
		hidden : false,
		header:biolims.goods.transBox,
		width:15*10
	});
	cm.push({
		dataIndex:'boxName',
		hidden : false,
		header:biolims.storage.carryingCaseName,
		width:15*10
	});
	cm.push({
		dataIndex:'boxSpec',
		hidden : false,
		header:biolims.storage.carryingCaseSpecifications,
		width:15*10
	});
	cm.push({
		dataIndex:'boxNum',
		hidden : false,
		header:biolims.common.count,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
//	cm.push({
//		dataIndex:'transBox-id',
//		hidden : true,
//		header:biolims.goods.transBoxId,
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'transBox-name',
//		hidden : false,
//		header:biolims.goods.transBox,
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'boxSize',
//		hidden : false,
//		header:biolims.equipment.spec,
//		width:40*6
//	});
////	cm.push({
////		dataIndex:'num-id',
////		hidden : true,
////		header:'数量ID',
////		width:20*10,
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
////	});
//	cm.push({
//		dataIndex:'num',
//		hidden : false,
//		header:biolims.common.count,
//		width:20*10,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'goodsMaterialsSend-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	}); 
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendBoxListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.goods.materialCarryingCasesDetail;
	opts.height =  document.body.clientHeight-260;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.goods.generatedExpressList,
		handler : editDetails
	});
       opts.delSelect = function(ids) {
		ajax("post", "/goods/mate/goodsMaterialsSend/delGoodsMaterialsSendBox.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择运输箱种类',
//			handler : selecttypeFun
//		});
//    
//	opts.tbar.push({
//			text : '选择数量',
//			handler : selectnumFun
//		});
//    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectgoodsMaterialsSendFun
//		});
//	
	
	
	
	
	
	
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = goodsMaterialsSendBoxGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							goodsMaterialsSendBoxGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : loadBox1
	});
	goodsMaterialsSendBoxGrid=gridEditTable("goodsMaterialsSendBoxdiv",cols,loadParam,opts);
	$("#goodsMaterialsSendBoxdiv").data("goodsMaterialsSendBoxGrid", goodsMaterialsSendBoxGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


//选择运输箱
function loadBox1(){
	var options = {}; 
	options.width = document.body.clientWidth-600;
	options.height = document.body.clientHeight-100;
	loadAdvance=loadDialogPage(null, '', "/system/box/box/transBoxSelect.action", {
		"Confirm" : function() {
			var operGrid = transBoxDialogGrid;
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				var ob = goodsMaterialsSendBoxGrid.getStore().recordType;
				goodsMaterialsSendBoxGrid.stopEditing();
				$.each(selectRecord, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("boxType", obj.id);
					p.set("boxName", obj.get("name"));
					p.set("boxSpec", obj.get("boxSize"));
					p.set("boxNum", "0");
					
					goodsMaterialsSendBoxGrid.getStore().add(p);
					//$("#sampleOrder_primary").val( obj.get("id"));
					//$("#sampleOrder_primary_name").val( obj.get("name"));
				});
				goodsMaterialsSendBoxGrid.startEditing(0, 0);
			}else{
				message(biolims.common.selectYouWant);
				return;
			}
			options.close();
		}
	}, true, options);
}

//function setNextFlow(){
//	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
//	var selectRecord = operGrid.getSelectionModel().getSelections();
//	var records = goodsMaterialsSendBoxGrid.getSelectRecord();
//	if (selectRecord.length > 0) {
//		$.each(records, function(i, obj) {
//			$.each(selectRecord, function(a, b) {
//				obj.set("nextFlowId", b.get("id"));
//				obj.set("nextFlow", b.get("name"));
//			});
//		});
//	}else{
//		message(biolims.common.selectYouWant);
//		return;
//	}
//	loadNextFlow.dialog("close");
//}	
//	






function selecttypeFun(){
	var win = Ext.getCmp('selecttype');
	if (win) {win.close();}
	var selecttype= new Ext.Window({
	id:'selecttype',modal:true,title:biolims.goods.selectTransBox,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=type' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selecttype.close(); }  }]  });     selecttype.show(); }
	function settype(id,name){
		var gridGrid = $("#goodsMaterialsSendBoxdiv").data("goodsMaterialsSendBoxGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('type-id',id);
			obj.set('type-name',name);
		});
		var win = Ext.getCmp('selecttype');
		if(win){
			win.close();
		}
	}
	
function selectnumFun(){
	var win = Ext.getCmp('selectnum');
	if (win) {win.close();}
	var selectnum= new Ext.Window({
	id:'selectnum',modal:true,title:biolims.goods.selectNum,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/IntegerSelect.action?flag=num' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectnum.close(); }  }]  });     selectnum.show(); }
	function setnum(id,name){
		var gridGrid = $("#goodsMaterialsSendBoxdiv").data("goodsMaterialsSendBoxGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('num-id',id);
			obj.set('num-name',name);
		});
		var win = Ext.getCmp('selectnum');
		if(win){
			win.close();
		}
	}
	
function selectgoodsMaterialsSendFun(){
	var win = Ext.getCmp('selectgoodsMaterialsSend');
	if (win) {win.close();}
	var selectgoodsMaterialsSend= new Ext.Window({
	id:'selectgoodsMaterialsSend',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsSendSelect.action?flag=goodsMaterialsSend' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsMaterialsSend.close(); }  }]  });     selectgoodsMaterialsSend.show(); }
	function setgoodsMaterialsSend(id,name){
		var gridGrid = $("#goodsMaterialsSendBoxdiv").data("goodsMaterialsSendBoxGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('goodsMaterialsSend-id',id);
			obj.set('goodsMaterialsSend-name',name);
		});
		var win = Ext.getCmp('selectgoodsMaterialsSend');
		if(win){
			win.close();
		}
	}
		function editDetails(){
		var id=$("#goodsMaterialsSend_id").val();
		var appid=$("#goodsMaterialsSend_goodsMaterialsApply").val();
	
		//ajax开始
		ajax("post", "/goods/mate/goodsMaterialsSend/showGoodsMaterialsApplyListJsonById.action", {
			appid : appid
			}, function(data) {
				receiveUser =  data.data.receiveUser ;
				phone =  data.data.phone ;
				address =  data.address ;
				company =  data.company ;
				//alert(address+company);
		
				
				//var selectRecord = goodsMaterialsSendBoxGrid.getSelectionModel();
				var store = goodsMaterialsSendBoxGrid.store;
				var selRecord = goodsMaterialsSendExpressGrid.store;
						var isRepeat = false;
						var boxNum = 0;
						for(var j=0;j<store.getCount();j++){
							var oldv = parseInt(store.getAt(j).get("boxNum"));
							boxNum +=oldv; 
						}
							//var itemStore = goodsMaterialsSendItemGrid.store;
							if(!isRepeat){
								var ob = goodsMaterialsSendExpressGrid.getStore().recordType;
								goodsMaterialsSendExpressGrid.stopEditing();
								for(var ij=0;ij<boxNum;ij++){
									var p = new ob({});
									p.isNew = true;
									
									goodsMaterialsSendExpressGrid.getStore().add(p);
									selRecord.getAt(ij).set("address",address);
									selRecord.getAt(ij).set("company",company);
						
									
								}
								message(biolims.goods.generatedExpressOrderSuccess);
								goodsMaterialsSendExpressGrid.startEditing(0, 0);
							}
				
			}); 
		//ajax结束		
	}	
