var goodsMaterialsPostDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'type-id',
		header:biolims.goods.sendTypeId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'type-name',
		header:biolims.goods.sendType,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'goodsMaterialsSend-id',
		header:biolims.goods.materialIssueId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'goodsMaterialsSend-name',
		header:biolims.goods.materialIssue,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:40*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:40*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsPost/showGoodsMaterialsPostListJson.action";
	var opts={};
	opts.title=biolims.goods.express2Send;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setGoodsMaterialsPostFun(rec);
	};
	goodsMaterialsPostDialogGrid=gridTable("show_dialog_goodsMaterialsPost_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(goodsMaterialsPostDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
