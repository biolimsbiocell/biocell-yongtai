var goodsMaterialsSendGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-applyOrganize-name',
		type:"string"
	});
//	    fields.push({
//		name:'goodsMaterialsReady-goodsMaterialsApply-receiveUser',
//		type:"string"
//	});
//	    fields.push({
//		name:'goodsMaterialsReady-goodsMaterialsApply-phone',
//		type:"string"
//	});
//	    fields.push({
//		name:'goodsMaterialsReady-goodsMaterialsApply-address-name',
//		type:"string"
//	});
//	    fields.push({
//		name:'company-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'company-name',
//		type:"string"
//	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:15*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'goodsMaterialsApply-id',
		hidden:true,
		header:biolims.goods.materialApplicationId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'goodsMaterialsApply-name',
		header:biolims.goods.materialApplication,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'goodsMaterialsApply-applyOrganize-name',
		header:biolims.common.provinces,
		width:40*6,
		
		sortable:true
	});
//	cm.push({
//		dataIndex:'goodsMaterialsReady-goodsMaterialsApply-receiveUser',
//		header:'收件人',
//		width:40*6,
//		
//		sortable:true
//	});
//
//	cm.push({
//		dataIndex:'goodsMaterialsReady-goodsMaterialsApply-phone',
//		header:'收件人电话',
//		
//		width:40*10,
//		sortable:true
//		});
//	cm.push({
//		dataIndex:'goodsMaterialsReady-goodsMaterialsApply-address-name',
//		header:'收货地址',
//		width:40*6,
//		
//		sortable:true
//	});
//		cm.push({
//		dataIndex:'company-id',
//		hidden:true,
//		header:'快递公司ID',
//		width:15*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'company-name',
//		header:'快递公司',
//		
//		width:15*10,
//		sortable:true
//		});
	cm.push({
		dataIndex:'num',
		header:biolims.storage.carryingCaseSummation,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:biolims.wk.confirmUserId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.confirmUserName,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.wk.approverDate,
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:40*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowState,
		width:40*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendListJson.action";
	var opts={};
	opts.title=biolims.goods.materialIssue;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	goodsMaterialsSendGrid=gridTable("show_goodsMaterialsSend_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/goods/mate/goodsMaterialsSend/editGoodsMaterialsSend.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/goods/mate/goodsMaterialsSend/editGoodsMaterialsSend.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/goods/mate/goodsMaterialsSend/viewGoodsMaterialsSend.action?id=' + id;
}
function exportexcel() {
	goodsMaterialsSendGrid.title = biolims.common.exportList;
	var vExportContent = goodsMaterialsSendGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				
				
				commonSearchAction(goodsMaterialsSendGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
