var goodsMaterialsSendDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-organize-name',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-receiveUser',
		type:"string"
	});
//	    fields.push({
//		name:'phone-id',
//		type:"string"
//	});
	    fields.push({
		name:'goodsMaterialsApply-phone',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsApply-address-name',
		type:"string"
	});
	    fields.push({
		name:'company-id',
		type:"string"
	});
	    fields.push({
		name:'company-name',
		type:"string"
	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'goodsMaterialsApply-id',
		header:biolims.goods.materialApplicationId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'goodsMaterialsApply-name',
		header:biolims.goods.materialApplication,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'goodsMaterialsApply-organize-name',
		header:biolims.common.provinces,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'goodsMaterialsApply-receiveUser',
		header:biolims.common.recipient,
		width:40*10,
		sortable:true
	});
//		cm.push({
//		dataIndex:'phone-id',
//		header:'收件人电话ID',
//		width:40*10,
//		hidden:true,
//		sortable:true
//		});
		cm.push({
		dataIndex:'goodsMaterialsApply-phone',
		header:biolims.common.recipientPhone,
		width:40*10,
		sortable:true
		});
	cm.push({
		dataIndex:'goodsMaterialsApply-address-name',
		header:biolims.common.consigneeAddress,
		width:40*10,
		sortable:true
	});
		cm.push({
		dataIndex:'company-id',
		header:biolims.sample.companyId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'company-name',
		header:biolims.sample.companyName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'num',
		header:biolims.storage.carryingCaseSummation,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		header:biolims.wk.confirmUserId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.confirmUserName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.wk.confirmDate,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:40*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:40*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendListJson.action";
	var opts={};
	opts.title=biolims.goods.materialIssue;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setGoodsMaterialsSendFun(rec);
	};
	goodsMaterialsSendDialogGrid=gridTable("show_dialog_goodsMaterialsSend_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(goodsMaterialsSendDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
