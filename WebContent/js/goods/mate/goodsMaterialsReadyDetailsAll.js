﻿var goodsMaterialsReadyDetailsGrid1;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
   fields.push({
		name:'packCode',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsReady-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsReady-name',
		type:"string"
	});
	    //=============
	    fields.push({
	    	name:'validity',
	    	type:"date",
			dateFormat:"Y-m-d"
	    });
	    //=============
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.master.materialPackageId,
		width:40*6
	});
	//===========
	cm.push({
		dataIndex:'validity',
		hidden : false,
		header:biolims.sanger.expiryDate,
		width:40*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	//===========
	cm.push({
		dataIndex:'packCode',
		hidden : true,
		header:biolims.master.materialPackageId,
		width:40*6
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.printingQuantity,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsReady-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsReady-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsReady/showDialogGoodsMaterialsDeListJson.action?flag="+$("#flag").val();
	var opts={};
	opts.title=biolims.master.materialPackageDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];


	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = goodsMaterialsReadyDetailsGrid1.getAllRecord();
							var store = goodsMaterialsReadyDetailsGrid1.store;

							var isOper = true;
							var buf = [];
							goodsMaterialsReadyDetailsGrid1.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							goodsMaterialsReadyDetailsGrid1.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message(biolims.common.samplecodeComparison);
								
							}
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	goodsMaterialsReadyDetailsGrid1=gridEditTable("goodsMaterialsReadyDetailsdiv1",cols,loadParam,opts);
	$("#goodsMaterialsReadyDetailsdiv1").data("goodsMaterialsReadyDetailsGrid1", goodsMaterialsReadyDetailsGrid1);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});