﻿
var goodsMaterialsReadyDetailsOutGrid;
$(function(){
	var cols={};
	//cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
   fields.push({
		name:'packCode',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsReady-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsReady-name',
		type:"string"
	});
	    //=============
	    fields.push({
	    	name:'validity',
	    	type:"date",
			dateFormat:"Y-m-d"
	    });
	    fields.push({
	    	name:'state',
	    	type:"string"
	    });
	    //=============
	    fields.push({
	    	name:'useState',
	    	type:"string"
	    });
	    fields.push({
	    	name:'outState',
	    	type:"string"
	    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.master.materialPackageId,
		width:30*6
	});
//	//===========
//	cm.push({
//		dataIndex:'validity',
//		hidden : false,
//		header:'有效期',
//		width:30*6,
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
//	});
//	cm.push({
//		dataIndex:'state',
//		hidden:true,
//		header:'状态',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'useState',
//		hidden:true,
//		header:'使用状态',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'outState',
//		hidden:true,
//		header:'超期状态',
//		width:20*6
//	});
	//===========
	cm.push({
		dataIndex:'packCode',
		hidden : false,
		header:biolims.master.materialPackageId,
		width:30*6
	});
//	cm.push({
//		dataIndex:'num',
//		hidden : false,
//		header:'打印数量',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'goodsMaterialsReady-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsReady-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyDetailsList2Json.action?code="+ $("#code").val();
	var opts={};
	opts.title=biolims.goods.expiredBloodVesselDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];

	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	goodsMaterialsReadyDetailsOutGrid=gridEditTable("goodsMaterialsReadyDetailsOutdiv",cols,loadParam,opts);
	$("#goodsMaterialsReadyDetailsOutediv").data("goodsMaterialsReadyDetailsOutGrid", goodsMaterialsReadyDetailsOutGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

	
