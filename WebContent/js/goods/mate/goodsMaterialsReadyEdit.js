﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
//	showitem();
//	var state=$("#goodsMaterialsReady_stateName").val();
//	if(state=="新建"){
//		load("/goods/mate/goodsMaterialsApply/showGoodsMaterialsApplyList1.action", {id:""}, "#goodsMaterialsApplypage");
//	}
	if($("#goodsMaterialsReady_stateName").val()==biolims.common.finish){
		$("#showgoodsMaterialsApply").css("display","none");
	}
});	
function showitem(){
	if($("#goodsMaterialsReady_stateName").val()==biolims.common.finish){
		load("/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyItemList1.action", {
			id : $("#goodsMaterialsReady_id").val()
		}, "#goodsMaterialsReadyItempage");
	}else{		
		load("/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyItemList.action", {
			id : $("#goodsMaterialsReady_id").val()
		}, "#goodsMaterialsReadyItempage");
	}
}


function add() {
	window.location = window.ctx + "/goods/mate/goodsMaterialsReady/editGoodsMaterialsReady.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
	var selectRecord=goodsMaterialsApplyGrid.getSelectionModel().getSelections();
	var selRecord=goodsMaterialsReadyDetailsGrid.store;
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("code");
				if(oldv == obj.get("id")){
					goodsMaterialsApplyGrid.store.remove(selectRecord);
				}
			}
		});
	}
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#goodsMaterialsReady", {
					userId : userId,
					userName : userName,
					formId : $("#goodsMaterialsReady_id").val(),
					title : $("#goodsMaterialsReady_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#goodsMaterialsReady_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var goodsMaterialsReadyDetailsDivData = $("#goodsMaterialsReadyDetailsdiv").data("goodsMaterialsReadyDetailsGrid");
		document.getElementById('goodsMaterialsReadyDetailsJson').value = commonGetModifyRecords(goodsMaterialsReadyDetailsDivData);
	    var goodsMaterialsReadyItemDivData = $("#goodsMaterialsReadyItemdiv").data("goodsMaterialsReadyItemGrid");
		document.getElementById('goodsMaterialsReadyItemJson').value = commonGetModifyRecords(goodsMaterialsReadyItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/goods/mate/goodsMaterialsReady/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/goods/mate/goodsMaterialsReady/copyGoodsMaterialsReady.action?id=' + $("#goodsMaterialsReady_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#goodsMaterialsReady_id").val() + "&tableId=goodsMaterialsReady");
//}
$("#toolbarbutton_status").click(function(){
	var selRecord = goodsMaterialsReadyDetailsGrid.store;
	var flag=true;
	for(var j=0;j<selRecord.getCount();j++){
		var code = selRecord.getAt(j).get("code");
		if(!code){
			flag=false;
		}
	}
	if(flag==false){
		message(biolims.goods.pleaseGenerateMaterialPackageId);
		return;
	}
	if ($("#goodsMaterialsReady_id").val()){
		commonChangeState("formId=" + $("#goodsMaterialsReady_id").val() + "&tableId=GoodsMaterialsReady");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#goodsMaterialsReady_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.goods.materialPreparation,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyDetailsList.action", {
				id : $("#goodsMaterialsReady_id").val()
			}, "#goodsMaterialsReadyDetailspage");
load("/goods/mate/goodsMaterialsReady/showGoodsMaterialsReadyItemList.action", {
				id : $("#goodsMaterialsReady_id").val()
			}, "#goodsMaterialsReadyItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
function GoodsMaterialsApplyFun(){
		var win = Ext.getCmp('GoodsMaterialsApplyFun');
		if (win) {win.close();}
		var GoodsMaterialsApplyFun= new Ext.Window({
		id:'GoodsMaterialsApplyFun',modal:true,title:biolims.goods.selectApplicationOrder,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/goods/mate/goodsMaterialsApply/goodsMaterialsApplySelectByState.action?flag=GoodsMaterialsApplyFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 GoodsMaterialsApplyFun.close(); }  }]  }); 
		 GoodsMaterialsApplyFun.show(); 
}
function setGoodsMaterialsApplyFun(rec){
		var code=$("#goodsMaterialsReady_goodsMaterialsApply").val();
		 if(code==""){
			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply').value=rec.get('id');
			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_name').value=rec.get('name');
			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_applyOrganize_name').value=rec.get('applyOrganize-name');
			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_address_name').value=rec.get('address-name');
			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_receiveUser').value=rec.get('receiveUser');
			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_phone').value=rec.get('phone');
			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_sendDate').value=rec.get('sendDate');
				 
			 var win = Ext.getCmp('GoodsMaterialsApplyFun');
			 if(win){win.close();}
				var id = rec.get('id');
				//var name= rec.get('name');
				ajax("post", "/goods/mate/goodsMaterialsReady/showGoodsMaterialsApplyItemList.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = goodsMaterialsReadyItemGrid.getStore().recordType;
							goodsMaterialsReadyItemGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								
								p.set("goodsMaterialsApplyItem-goodsMaterialsPack-id", obj.mid);
								p.set("goodsMaterialsApplyItem-goodsMaterialsPack-num", obj.mnum);
								p.set("goodsMaterialsApplyItem-goodsMaterialsPack-name", obj.mname);
								p.set("goodsMaterialsApplyItem-applyNum", obj.applyNum);
								if(obj.lackNum==""){
									p.set("sendNum", obj.applyNum);
								}else if(obj.lackNum=="0"){
									p.set("sendNum", obj.lackNum);
								}
								//p.set("sendNum", obj.lackNum);
								p.set("goodsMaterialsApplyItem-wayNum", obj.wayNum);
								p.set("goodsMaterialsApplyItem-unuseNum", obj.unuseNum);							
								p.set("goodsMaterialsApplyItem-usedNum", obj.usedNum);
								p.set("goodsMaterialsApplyItem-id", obj.aid);
								p.set("goodsMaterialsApplyItem-usedNum", obj.usedNum);
//								p.set("state", obj.state);
								goodsMaterialsReadyItemGrid.getStore().add(p);							
							});
							
							goodsMaterialsReadyItemGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
		 }else{
				if(rec.get('id')==code){
	 				var win = Ext.getCmp('GoodsMaterialsApplyFun');
	 				if(win){win.close();}
	 			 }else{
	 				var ob1 = goodsMaterialsReadyItemGrid.store;
	 				if (ob1.getCount() > 0) {
						for(var j=0;j<ob1.getCount();j++){
							var oldv = ob1.getAt(j).get("id");
							//=====================2015-11-30 ly======================//
							//根据ID删除
							if(oldv!=null){
							ajax("post", "/goods/mate/goodsMaterialsReady/delGoodsMaterialsReadyOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null);
							}else{
								goodsMaterialsReadyItemGrid.store.removeAll();
							}
						}
						goodsMaterialsReadyItemGrid.store.removeAll();
						}
	 				//===========================================//
					var ob2 = goodsMaterialsReadyDetailsGrid.store;
					if (ob2.getCount() > 0) {
						for(var j=0;j<ob2.getCount();j++){
							var oldv = ob2.getAt(j).get("id");
							if(oldv!=null){
								//=====================2015-11-30 ly======================//
								//根据ID删除
								if(oldv!=null){
								ajax("post", "/goods/mate/goodsMaterialsReady/delGoodsMaterialsReadyDetailsOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null);
							}else{
								goodsMaterialsReadyDetailsGrid.store.removeAll();
							}
						}
	 				}
						goodsMaterialsReadyDetailsGrid.store.removeAll();
					}
	 			 //goodsMaterialsReadyItemGrid.store.removeAll();
	 			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply').value=rec.get('id');
	 			 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_name').value=rec.get('name');
 				 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_applyOrganize_name').value=rec.get('applyOrganize-name');
 				 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_address_name').value=rec.get('address-name');
 				 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_receiveUser').value=rec.get('receiveUser');
 				 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_phone').value=rec.get('phone');
 				 document.getElementById('goodsMaterialsReady_goodsMaterialsApply_sendDate').value=rec.get('sendDate');
				 var win = Ext.getCmp('GoodsMaterialsApplyFun');
				 if(win){win.close();}
					var id = rec.get('id');
					ajax("post", "/goods/mate/goodsMaterialsReady/showGoodsMaterialsApplyItemList.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = goodsMaterialsReadyItemGrid.getStore().recordType;
								goodsMaterialsReadyItemGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									
									p.set("goodsMaterialsApplyItem-goodsMaterialsPack-num", obj.mnum);
									p.set("goodsMaterialsApplyItem-goodsMaterialsPack-id", obj.mid);
									p.set("goodsMaterialsApplyItem-goodsMaterialsPack-name", obj.mname);
									p.set("goodsMaterialsApplyItem-applyNum", obj.applyNum);
									//p.set("sendNum", obj.applyNum);
									if(obj.lackNum==""){
										p.set("sendNum", obj.applyNum);
									}else if(obj.lackNum=="0"){
										p.set("sendNum", obj.lackNum);
									}
									p.set("goodsMaterialsApplyItem-wayNum", obj.wayNum);
									p.set("goodsMaterialsApplyItem-unuseNum", obj.unuseNum);							
									p.set("goodsMaterialsApplyItem-usedNum", obj.usedNum);
									p.set("goodsMaterialsApplyItem-id", obj.aid);
									p.set("goodsMaterialsApplyItem-usedNum", obj.usedNum);
									
									goodsMaterialsReadyItemGrid.getStore().add(p);							
								});
								
								goodsMaterialsReadyItemGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null);
		 }
	}
}


