﻿
var goodsMaterialsSendExpressGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
			name:'id',
			type:"string"
		});
		   fields.push({
			name:'code',
			type:"string"
		});
	   fields.push({
			name:'linkUser',
			type:"string"
		});
		   fields.push({
			name:'phone',
			type:"string"
		});
	   fields.push({
			name:'receiveUser',
			type:"string"
		});
		   fields.push({
			name:'receiveDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'planDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
		   fields.push({
			name:'realDate',
			type:"string"
//			type:"date",
//			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'note',
			type:"string"
		});
		   fields.push({
			name:'address',
			type:"string"
		});
//	   fields.push({
//			name:'company-id',
//			type:"string"
//		});
		   fields.push({
			name:'company',
			type:"string"
		});
	   fields.push({
			name:'port',
			type:"string"
		});
	    fields.push({
		name:'goodsMaterialsSend-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsSend-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.sample.expreceCode,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'linkUser',
		hidden : false,
		header:biolims.common.linkmanName,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'phone',
		hidden : false,
		header:biolims.common.phone,
		width:25*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'address',
		hidden : false,
		header:biolims.common.consigneeAddress,
		width:30*6
	});
//	cm.push({
//		dataIndex:'company-id',
//		hidden : true,
//		header:'快递公司ID',
//		width:20*6
//	});
	cm.push({
		dataIndex:'company',
		hidden : false,
		header:biolims.sample.companyName,
		width:25*6
	});
//	cm.push({
//		dataIndex:'receiveUser',
//		hidden : false,
//		header:'取件人',
//		width:40*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'receiveDate',
		hidden : true,
		header:biolims.goods.receiveDate,
		width:25*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'planDate',
		hidden : true,
		header:biolims.goods.predictDate,
		width:25*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'realDate',
		hidden : false,
		header:biolims.goods.realDate,
		width:30*6
		//renderer: formatDate,
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'port',
		hidden : false,
		header:biolims.goods.port,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10
	});
	cm.push({
		dataIndex:'goodsMaterialsSend-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/mate/goodsMaterialsSend/showGoodsMaterialsSendExpressListJson.action?id="+ $("#id_parent_hidden").val()+"&appId="+ $("#goodsMaterialsSend_goodsMaterialsApply").val();
	var opts={};
	opts.title=biolims.goods.materialDeliveryDetail;
	opts.height =  document.body.clientHeight-260;
	opts.tbar = [];
//	var state=$("#goodsMaterialsSend_stateName").val();
//	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/goods/mate/goodsMaterialsSend/delGoodsMaterialsSendExpress.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = goodsMaterialsSendExpressGrid.getAllRecord();
							var store = goodsMaterialsSendExpressGrid.store;

							var isOper = true;
							var buf = [];
							goodsMaterialsSendExpressGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
										obj1.set("code",array[i]);
								});
							});
							goodsMaterialsSendExpressGrid.getSelectionModel().selectRows(buf);
							
							goodsMaterialsSendExpressGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		text : biolims.common.batchCourierNum,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			var records = goodsMaterialsSendExpressGrid.getSelectRecord();
			if (records && records.length > 0) {
				loadDialogPage($("#bat_code_div"), biolims.common.batchCourierNum, null, {
					"Confirm" : function() {
							var code = $("#code").val();
							goodsMaterialsSendExpressGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("code", code);
							});
							goodsMaterialsSendExpressGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});

	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = goodsMaterialsSendExpressGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							goodsMaterialsSendExpressGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		text : '删除选中',
//		handler : null
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.goods.printExpressOrder,
		handler : printOrder
	});
	goodsMaterialsSendExpressGrid=gridEditTable("goodsMaterialsSendExpressdiv",cols,loadParam,opts);
	$("#goodsMaterialsSendExpressdiv").data("goodsMaterialsSendExpressGrid", goodsMaterialsSendExpressGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//打印快递单
function printOrder(){
	var cid=$("#goodsMaterialsSend_companyId").val();
	var selectRecord = goodsMaterialsSendExpressGrid.getSelectionModel();
	if (selectRecord.getSelections().length > 0) {
		var codes="";
		$.each(selectRecord.getSelections(), function(i, obj) {
			var code=obj.get("code");
			codes += "'"+code+"',";
		});
		var str=codes.substring(0, codes.length-1);
		ajax("post", "/goods/mate/goodsMaterialsSend/selectPrintPath.action", {
			code : cid
		}, function(data) {
			if (data.success) {
				//var url = '__report=expressOrder.rptdesign&ID=' + $("#GoodsMaterialsSend_id").val()+"&codes="+str;
				var url ='__report='+data.data+'&ID=' + $("#GoodsMaterialsSend_id").val()+"&codes="+str;
				commonPrint(url);
			}
		});
	}else{
		message(biolims.goods.pleaseSelectExpressOrder);
		return;
	}
}

function selectgoodsMaterialsSendFun(){
	var win = Ext.getCmp('selectgoodsMaterialsSend');
	if (win) {win.close();}
	var selectgoodsMaterialsSend= new Ext.Window({
	id:'selectgoodsMaterialsSend',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsSendSelect.action?flag=goodsMaterialsSend' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsMaterialsSend.close(); }  }]  });     selectgoodsMaterialsSend.show(); }
	function setgoodsMaterialsSend(id,name){
		var gridGrid = $("#goodsMaterialsSendExpressdiv").data("goodsMaterialsSendExpressGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('goodsMaterialsSend-id',id);
			obj.set('goodsMaterialsSend-name',name);
		});
		var win = Ext.getCmp('selectgoodsMaterialsSend');
		if(win){
			win.close();
		}
	}
	
	function selectcompanyFun(){
		var win = Ext.getCmp('selectcompanyFun');
		if (win) {win.close();}
		var selectcompanyFun= new Ext.Window({
		id:'selectcompanyFun',modal:true,title:biolims.goods.selectExpress,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=company' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
			 selectcompanyFun.close(); }  }]  });     selectcompanyFun.show(); }
		function setcompany(id,name){
			var gridGrid = $("#goodsMaterialsSendExpressdiv").data("goodsMaterialsSendExpressGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('company-id',id);
				obj.set('company-name',name);
			});
			var win = Ext.getCmp('selectcompanyFun');
			if(win){
				win.close();
			}
		}
	
