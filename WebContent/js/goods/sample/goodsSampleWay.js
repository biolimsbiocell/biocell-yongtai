var goodsSampleWayGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'packUser',
		type:"string"
	});
	    fields.push({
		name:'isQualified',
		type:"string"
	});
	    fields.push({
		name:'expressOrder',
		type:"string"
	});
	    fields.push({
		name:'expressCompany',
		type:"string"
	});
	    fields.push({
		name:'sendTime',
		type:"string"
		//dateFormat:'Y-m-d h:i:s'
	});
	    fields.push({
		name:'isReceive',
		type:"string"
	});
	    fields.push({
		name:'stateId',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.goods.samplePackageCode,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'packUser',
		header:biolims.goods.packUser,
		width:20*6,
		
		sortable:true
	});
	var isReceive = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isReceive',
		hidden : false,
		header:biolims.goods.isReceive,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(isReceive),editor: isReceive
	});
	var isQualified = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isQualified',
		hidden : false,
		header:biolims.common.isQualified,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(isQualified),editor: isQualified
	});

	cm.push({
		dataIndex:'expressOrder',
		header:biolims.sample.expreceCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'expressCompany',
		header:biolims.sample.companyName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sendTime',
		header:biolims.sample.sendOutTime,
		width:20*6
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d H:i'})
	});

	cm.push({
		dataIndex:'stateId',
		header:biolims.goods.stateId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.goods.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:40*6,		
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/sample/goodsSampleWay/showGoodsSampleWayListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.goods.logisticsTracking;
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.print,
		handler : exportexcel
	});
	opts.tbar.push({
		text : biolims.common.batchOper,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_way_div"), biolims.common.batchOper, null, {
				"Confirm" : function() {
					var records = goodsSampleWayGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isQualified = $("#isQualified").val();
						//var isReceive = $("#isReceive").val();
						goodsSampleWayGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isQualified", isQualified);
							//obj.set("isReceive", isReceive);
						});
						goodsSampleWayGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchSign,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), biolims.common.batchSign, null, {
				"Confirm" : function() {
					var records = goodsSampleWayGrid.getSelectRecord();
					if (records && records.length > 0) {
						//var isQualified = $("#isQualified").val();
						var isReceive = $("#isReceive").val();
						goodsSampleWayGrid.stopEditing();
						$.each(records, function(i, obj) {
							//obj.set("isQualified", isQualified);
							obj.set("isReceive", isReceive);
						});
						goodsSampleWayGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	goodsSampleWayGrid=gridEditTable("show_goodsSampleWay_div",cols,loadParam,opts);
	$("#show_goodsSampleWay_div").data("goodsSampleWayGrid", goodsSampleWayGrid);
});
//保存之后把是否签收反馈到取样打包页面
//function saveToSample(){	
//	if(selectRecord.getSelections().length>0){
//		for(var i=0;i<selectRecord.getSelections().length;i++){
//			//获取选中行的签收字段
//			var receive = selectRecord.getSelections()[i].get("isReceive");
//			var code = selectRecord.getSelections()[i].get("code");
//			if(receive!=null){
//				$.each(selectRecord.getSelections(), function(i, obj) {
//					ajax("post", "/goods/sample/goodsSampleWay/setIsReceiveOfWayToSample.action", {
//						code : code
//					}, function(data) {
//						if (data.success) {
//							message("保存成功！");
//						} else {
//							message("保存失败！");
//						}
//					}, null);			
//				});		
//			}
//		}
//		
//	}else{
//		message("没有需要保存的数据！");
//	}
//}

//保存
function save(){	
	//var selectRecord = goodsSampleWayGrid.getSelectionModel();
	//var inItemGrid = $("#show_goodsSampleWay_div").data("goodsSampleWayGrid");
	var itemJson = commonGetModifyRecords(goodsSampleWayGrid);
	if(itemJson.length>0){
		//$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/goods/sample/goodsSampleWay/setSampleToWay.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					//$("#show_goodsSampleWay_div").data("goodsSampleWayGrid").getStore().commitChanges();
					//$("#show_goodsSampleWay_div").data("goodsSampleWayGrid").getStore().reload();
					goodsSampleWayGrid.getStore().commitChanges();
					goodsSampleWayGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		//});
	}else{
		message(biolims.common.noData2Save);
	}
	
}
function add(){
		window.location=window.ctx+'/goods/sample/goodsSampleWay/editGoodsSampleWay.action';
	}

function exportexcel() {
	goodsSampleWayGrid.title = biolims.common.exportList;
	var vExportContent = goodsSampleWayGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function search() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(goodsSampleWayGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
}
