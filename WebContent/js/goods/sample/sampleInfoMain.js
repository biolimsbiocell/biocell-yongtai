var sampleInfoMainGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
//	    fields.push({
//		name:'hospital',
//		type:"string"
//	});
	    
	  //录入人
		   fields.push({
				name:'createUser-id',
				type:"string"
			});
			   fields.push({
				name:'createUser-name',
				type:"string"
			});
	    
	    fields.push({
		name:'project-id',
		type:"string"
	});
	    fields.push({
		name:'project-name',
		type:"string"
	});
	    fields.push({
		name:'sampleUser',
		type:"string"
	});
	    fields.push({
		name:'sampleTime',
		type:"string"
//		dateFormat:"Y-m-d H:i"
	});
	    fields.push({
		name:'sendTime',
		type:"string"
	});
	    fields.push({
		name:'company-id',
		type:"string"
	});
	    fields.push({
		name:'company-name',
		type:"string"
	});
	    fields.push({
		name:'expressCode',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
		name:'isQualified',
		type:"string"
	});
	fields.push({
		name:'isReceive',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:40*6,
		
		sortable:true
	});
//	cm.push({
//		dataIndex:'hospital',
//		header:'送检医院',
//		width:50*6,
//		
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'project-id',
//		hidden:true,
//		header:'项目ID',
//		width:15*10,
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'project-name',
//		header:'项目',
//		
//		width:15*10,
//		sortable:true
//	});
	cm.push({
		dataIndex:'sampleUser',
		header:biolims.sample.sampleUser,
		width:20*6,
		
		sortable:true
	});
//	cm.push({
//		dataIndex:'sampleTime',
//		header:'取样时间',
//		width:30*6,
//		
//		sortable:true
////		renderer: formatDate,
////		editor: new Ext.form.DateField({format: 'Y-m-d H:i'})
//	});
	cm.push({
		dataIndex:'sendTime',
		header:biolims.sample.sendOutTime,
		width:30*6,
		sortable:true
	});
	
	//录入人
	cm.push({
		dataIndex:'createUser-id',
		hidden : true,
		header:biolims.sample.createUserId,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'createUser-name',
		hidden : false,
		header:biolims.sample.createUserName,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
		cm.push({
		dataIndex:'company-id',
		hidden:true,
		header:biolims.sample.companyId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'company-name',
		header:biolims.sample.companyName,
		
		width:25*10,
		sortable:true
		});
	cm.push({
		dataIndex:'expressCode',
		header:biolims.sample.expreceCode,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:40*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:40*6,
		
		sortable:true
	});
	var isQualified = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isQualified',
		hidden : false,
		header:biolims.common.isQualified,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(isQualified),editor: isQualified
	});
	var isReceive = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isReceive',
		hidden : false,
		header:biolims.goods.isReceive,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(isReceive),editor: isReceive
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/sample/sampleInfoMain/showSampleInfoMainListJson.action";
	var opts={};
	opts.title=biolims.goods.samplePackage;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sampleInfoMainGrid=gridTable("show_sampleInfoMain_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/goods/sample/sampleInfoMain/editSampleInfoMain.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/goods/sample/sampleInfoMain/editSampleInfoMain.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/goods/sample/sampleInfoMain/viewSampleInfoMain.action?id=' + id;
}
function exportexcel() {
	sampleInfoMainGrid.title = biolims.common.exportList;
	var vExportContent = sampleInfoMainGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
	

}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(sampleInfoMainGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});


function ExpressCompanyTwoFun(){
	
	var win = Ext.getCmp('ExpressCompanyTwoFun');
	if (win) {win.close();}
	var ExpressCompanyFun= new Ext.Window({
	id:'ExpressCompanyTwoFun',modal:true,title:biolims.goods.selectExpress,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/express/expressCompany/expressCompanySelect.action?flag=ExpressCompanyFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
	 ExpressCompanyFun.close(); }  }]  });  
	ExpressCompanyFun.show(); }
function setExpressCompanyFun(rec){
	//alert(rec.get('name'));
	document.getElementById("sampleInfoMain_company").value = rec.get('id');
	document.getElementById("sampleInfoMain_company_name").value = rec.get('name');
	var win = Ext.getCmp('ExpressCompanyTwoFun');
	if(win){win.close();}
	}