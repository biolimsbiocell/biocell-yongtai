﻿var sampleInfoGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	
	fields.push({
		name : 'name',
		type : "string"
	});
	
	fields.push({
		name : 'sampleInfoMain',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	
	//
	
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : false,
		header : biolims.common.orderCode,
		width : 30 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	
	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.projectId,
		width : 25 * 6
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.projectName,
		width : 25 * 6
	});
	cm.push({
		dataIndex : 'name',
		hidden : false,
		header : biolims.common.patientName,
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	
	cm.push({
				hidden:true,
				xtype : 'actioncolumn',
				width : 80,
				header : biolims.common.uploadAttachment,
				items : [ {
					icon : window.ctx + '/images/img_attach.gif',
					tooltip : biolims.common.attachment,
					handler : function(grid, rowIndex, colIndex) {
						var rec = grid.getStore().getAt(rowIndex);
						if (rec.get('id') && rec.get('id') != 'NEW') {
							var win = Ext.getCmp('doc');
							if (win) {
								win.close();
							}
							var doc = new Ext.Window(
									{
										id : 'doc',
										modal : true,
										title : biolims.common.attachment,
										layout : 'fit',
										width : 900,
										height : 500,
										closeAction : 'close',
										plain : true,
										bodyStyle : 'padding:5px;',
										buttonAlign : 'center',
										collapsible : true,
										maximizable : true,
										items : new Ext.BoxComponent(
												{
													id : 'maincontent',
													region : 'center',
													html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?flag=doc&id="
															+ rec.get('id')
															+ "' frameborder='0' width='100%' height='100%' ></iframe>"
												}),
										buttons : [ {
											text : biolims.common.close,
											handler : function() {
												doc.close();
											}
										} ]
									});
							doc.show();
						} else {
							message(biolims.common.pleaseSaveRecord);
						}
					}
				} ]
			});
	
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : biolims.common.state,
		width : 15 * 6
	});
	cm.push({
		dataIndex : 'stateName',
		hidden : false,
		header : biolims.common.stateName,
		width : 15 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoMain',
		hidden : true,
		header : biolims.common.relatedMainTableId,
		width : 40 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/goods/sample/sampleInfoMain/showSampleInfoListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.goods.orderDetail;
	opts.height = document.body.clientHeight - 200;
	opts.tbar = [];
	if ($("#sampleInfoMain_stateName").val() != biolims.common.finish) {
		opts.delSelect = function(ids) {
			ajax("post", "/goods/sample/sampleInfoMain/delSampleInfo.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					scpProToGrid.getStore().commitChanges();
					scpProToGrid.getStore().reload();
					message(biolims.common.deleteSuccess);
				} else {
					message(biolims.common.deleteFailed);
				}
			}, null);
		};

	
		opts.tbar.push({
			text : biolims.QPCR.selectProject,
			handler : selectProduct
		});
	

		function goInExcelcsv() {
			var file = document.getElementById("file-uploadcsv").files[0];
			var n = 0;
			var ob = sampleInfoItemGrid.getStore().recordType;
			var reader = new FileReader();
			reader.readAsText(file, 'GB2312');
			reader.onload = function(f) {
				var csv_data = $.simple_csv(this.result);
				$(csv_data).each(function() {
					if (n > 0) {
						if (this[0]) {
							var p = new ob({});
							p.isNew = true;
							var o;
							o = 0 - 1;
							p.set("po.fieldName", this[o]);

							o = 1 - 1;
							p.set("po.fieldName", this[o]);

							o = 2 - 1;
							p.set("po.fieldName", this[o]);

							o = 3 - 1;
							p.set("po.fieldName", this[o]);

							o = 4 - 1;
							p.set("po.fieldName", this[o]);

							o = 5 - 1;
							p.set("po.fieldName", this[o]);

							o = 6 - 1;
							p.set("po.fieldName", this[o]);

							o = 7 - 1;
							p.set("po.fieldName", this[o]);

							sampleInfoItemGrid.getStore().insert(0, p);
						}
					}
					n = n + 1;
				});
			};
		}
		//上传单个图片的按钮
		opts.tbar.push({
			text : biolims.goods.uploadInformation,
			handler : function() {
				var isUpload = true;
				var record = sampleInfoGrid.getSelectOneRecord();
				load("/goods/sample/sampleInfoMain/toSampeInfoUpload.action", { // 是否修改
					fileId : record.get("upLoadAccessory-id"),
					isUpload : isUpload
				}, null, function() {
					$("#upload_file_div").data("callback", function(data) {
						record.set("upLoadAccessory-fileName", data.fileName);
						record.set("upLoadAccessory-id", data.fileId);
					});
				});
			}
		});
		
		

		
		
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});

		// opts.tbar.push({
		// text : '填加明细',
		// handler : null
		// });

	}
	sampleInfoGrid = gridEditTable("sampleInfodiv", cols, loadParam, opts);
	$("#sampleInfodiv").data("sampleInfoGrid", sampleInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});




function selecttype() {
	var win = Ext.getCmp('selecttype');
	if (win) {
		win.close();
	}
	var selecttype = new Ext.Window(
			{
				id : 'selecttype',
				modal : true,
				title : biolims.goods.selectPayWay,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src="
									+ window.ctx
									+ "'/dic/type/dicTypeSelect.action?flag=payType' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						selecttype.close();
					}
				} ]
			});
	selecttype.show();
}
function setpayType(id, name) {
	// var gridGrid = $("#goodsMaterialsDetailsdiv").data("sampleInfoItemGrid");
	var selRecords = sampleInfoGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('type-id', id);
		obj.set('type-name', name);
	});
	var win = Ext.getCmp('selecttype');
	if (win) {
		win.close();
	}
}

function selectsampleType() {
	var win = Ext.getCmp('selectsampleType');
	if (win) {
		win.close();
	}
	var selectsampleType = new Ext.Window(
			{
				id : 'selectsampleType',
				modal : true,
				title : biolims.common.sampleType,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						selectsampleType.close();
					}
				} ]
			});
	selectsampleType.show();
}
// ========2015-12-07 ly 物料包明细===========
function packDetails() {
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/goods/mate/goodsMaterialsReady/goodsMaterialsReadyDeSelect.action";
	loadDialogPage(null, biolims.common.selectedDetail, url, {
		"Confirm" : function() {
			var ids = goodsMaterialsReadyDetailsGrid1.getSelectionModel();
			var simpleInfo = sampleInfoGrid.store;
			var idArray = new Array();
			var marks = new Array();
			var isRepeat = false;
			if (ids.getSelections().length > 0) {
				$.each(ids.getSelections(), function(i, obj) {
					var code = obj.get("code");
					idArray.push(code);
				});
					var code2 = idArray[0].substr(0,4);
					ajax("post", "/com/biolims/system/product/findProductByMark.action", {
						code : code2
					}, function(data) {
						if (data.success) {	
							var ob = sampleInfoSystemGrid.getStore().recordType;
							sampleInfoSystemGrid.stopEditing();
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
//								p.set("id",obj.id);
								p.set("name",obj.name);
								p.set("num",obj.num);
								p.set("scope",obj.scope);
								sampleInfoSystemGrid.getStore().add(p);							
							});
							sampleInfoSystemGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
					for(var f=0;f<idArray.length;f++){
						var code1 = idArray[f].substr(0,4);
						ajax("post", "/com/biolims/system/product/findProductByMark.action", {
							code : code1
							}, function(data) {
								if (data.success) {	
									$.each(data.data, function(i, obj) {
										marks.push(obj.mark);
									});
									if(marks.length>1){
										for(var k=0;k<marks.length;k++ ){
											var mark =marks[k];
											if(mark != marks[0]){
												isRepeat = true;
												message(biolims.goods.businessTypesInconsistent);
											}
										}
									}
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null);
					}
					$.each(ids.getSelections(), function(i, obj) {
						//获取物料包编号
						var temp=obj.get("code");
						//获取有效期
						var d1=obj.get("validity");
						//比较有效期和系统当前日期大小
						var d2=new Date();
//						DateTime a = DateTime.parse(d1);
//						DateTime b = DateTime.parse(d2);
						var a = new Date(d1).getTime();
						var b = new Date(d2).getTime();
						if(b>=a){
							//当前日期大于有效期，返回后台改变准备明细中采血管的过期状态
							ajax("post", "/goods/mate/goodsMaterialsReady/setOutState.action", {
								code : temp
								}, function(data) {
									if (data.success) {
									message(biolims.goods.sampleOverDue);
									return;	
								}
							});
						}else{
							//如果没有超期就可以选择使用
							for ( var j = 0; j < simpleInfo.getCount(); j++) {
								var oldv = simpleInfo.getAt(j).get("code");
								if (oldv == obj.get("code")) {
									isRepeat = true;
									message(biolims.common.haveDuplicate);
									break;
								}
							}
							if (!isRepeat) {
								var ob = sampleInfoGrid.getStore().recordType;
								sampleInfoGrid.stopEditing();
								var p = new ob({});
								p.isNew = true;
								p.set("code", obj.get("code"));
								sampleInfoGrid.getStore().add(p);
								sampleInfoGrid.startEditing(0, 0);
							}
						}
					});
			} else {
				message(biolims.common.pleaseSelect);
				return;
			}
			options.close();
		}
	}, true, options);
}
// ========================================

function setyblx(id, name) {
	// var gridGrid = $("#goodsMaterialsDetailsdiv").data("sampleInfoItemGrid");
	var selRecords = sampleInfoGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('sampleType-id', id);
		obj.set('sampleType-name', name);
	});
	var win = Ext.getCmp('selectsampleType');
	if (win) {
		win.close();
	}
}

function selectProduct() {
	var selRecord = sampleInfoGrid.store;
	// 获取第一次输入的样本的标识码
	var win = Ext.getCmp('selectProject');
	if (win) {
		win.close();
	}
	var selectProduct = new Ext.Window(
			{
				id : 'selectProduct',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 600,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ window.ctx
									+ "/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						selectProduct.close();
					}
				} ]
			});
	selectProduct.show();
}
function setProductFun(id,name) {
	var selRecords = sampleInfoGrid.getSelectionModel().getSelections();
	var productName = "";
//	ajax("post", "/com/biolims/system/product/findProductToSample.action", {
//		code : id
//	}, function(data) {
//		if (data.success) {
//			$.each(data.data, function(i, obj) {
//				productName += obj.name + ",";
//			});
//			
//		}
//	}, null);
	$.each(selRecords, function(i, obj) {
		obj.set('productId', id);
		obj.set('productName', name);
	});
	var win = Ext.getCmp('selectProduct');
	if (win) {
		win.close();
	}
}
function upimg () {
	var isUpload = true;
	var record = sampleInputGrid.getSelectOneRecord();
	load("/goods/sample/sampleInfoMain/toSampeInfoUpload.action", { // 是否修改
		fileId : record.get("upLoadAccessory-id"),
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			record.set("upLoadAccessory-fileName", data.fileName);
			record.set("upLoadAccessory-id", data.fileId);
		});
	});
}
