﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleInfoMain_stateName").val()==biolims.common.finish){
		$("#showcompany").css("display","none");
	}
});	
function add() {
	window.location = window.ctx + "/goods/sample/sampleInfoMain/editSampleInfoMain.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/goods/sample/sampleInfoMain/showSampleInfoMainList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	if(!$("#sampleInfoMain_company_name").val()){
		message(biolims.goods.expressCompanyEmpty);
		return;
	}
	if(!$("#sampleInfoMain_expressCode").val()){
		message(biolims.goods.expressCodeEmpty);
		return;
	}
//获取当前系统时间传到取样时间中
//	var d = new Date();
//	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
//	var selRecord=sampleInfoGrid.store;
//	for(var j=0;j<selRecord.getCount();j++){
//		if(selRecord.getAt(j).get("sampleTime")==""){
//			selRecord.getAt(j).set("sampleTime",str);
//		}
//	}
	var selRecord = sampleInfoGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		for(var i=0;i<selRecord.getCount();i++){
			if(selRecord.getAt(j).get("id") == selRecord.getAt(i).get("id") && i!=j){
				message(biolims.goods.IdRepeat);
				return;
			}
		}
//		var oldv = selRecord.getAt(j).get("name");
//		if(!oldv){
//			message("样本名称不能为空");
//			return;
//		}
//		var patientName = selRecord.getAt(j).get("patientName");
//		if(!patientName){
//			message("病人姓名不能为空");
//			return;
//		}
//		var idCard = selRecord.getAt(j).get("idCard");
//		if(!idCard){
//			message("身份证号不能为空");
//			return;
//		}
//		var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
//		if(!reg.test(idCard)){
//			message("请输入正确的证件号码!");
//			return;
//		}
//		var sampleTypeId = selRecord.getAt(j).get("sampleType-id");
//		if(!sampleTypeId){
//			message("样本类型不能为空");
//			return;
//		}
	}
	save();
});	
//检查子表名称是否为空
//function checkNull(){
//	var selRecord = sampleInfoItemGrid.store;
//	for(var j=0;j<selRecord.getCount();j++){
//		var oldv = selRecord.getAt(j).get("name");
//		if( oldv == ""){
//			message("样本名称不能为空");
//		}
//	}
//}
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleInfoMain", {
					userId : userId,
					userName : userName,
					formId : $("#sampleInfoMain_id").val(),
					title : $("#sampleInfoMain_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleInfoMain_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

//function saveToApply(){
//	var id=$("#sampleInfo_id").val();
//	ajax("post", "/goods/sample/sampleInfo/setExpressToWay.action", {
//		code : id
//	}, function(data) {
//		if (data.success) {
//			message("保存成功！");
//		} else {
//			message("保存失败！");
//		}
//	}, null);
//}




function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var sampleInfoSystemDivData = $("#sampleInfoSystemdiv").data("sampleInfoSystemGrid");
		document.getElementById('sampleInfoSystemJson').value = commonGetModifyRecords(sampleInfoSystemDivData);
	    var sampleInfoDivData = $("#sampleInfodiv").data("sampleInfoGrid");
		document.getElementById('sampleInfoJson').value = commonGetModifyRecords(sampleInfoDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/goods/sample/sampleInfoMain/save.action";
		form1.submit();
		//saveToApply();
		}
}		
function editCopy() {
	window.location = window.ctx + '/goods/sample/sampleInfoMain/copySampleInfoMain.action?id=' + $("#sampleInfoMain_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#sampleInfo_id").val() + "&tableId=sampleInfo");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleInfoMain_id").val()){
		commonChangeState("formId=" + $("#sampleInfoMain_id").val() + "&tableId=SampleInfoMain");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfoMain_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.goods.samplePackage,
	    	   contentEl:'markup'
	       } ]
	   });
	
});
load("/goods/sample/sampleInfoMain/showSampleInfoSystemList.action", {
				id : $("#sampleInfoMain_id").val()
			}, "#sampleInfoSystempage");
load("/goods/sample/sampleInfoMain/showSampleInfoList.action", {
				id : $("#sampleInfoMain_id").val()
			}, "#sampleInfopage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
function ExpressCompanyFun(){
		var win = Ext.getCmp('ExpressCompanyFun');
		if (win) {win.close();}
		var ExpressCompanyFun= new Ext.Window({
		id:'ExpressCompanyFun',modal:true,title:biolims.goods.selectExpress,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/express/expressCompany/expressCompanySelect.action?flag=ExpressCompanyFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 ExpressCompanyFun.close(); }  }]  });     ExpressCompanyFun.show(); }
function setExpressCompanyFun(rec){
		document.getElementById("sampleInfoMain_company").value = rec.get('id');
		document.getElementById("sampleInfoMain_company_name").value = rec.get('name');
		var win = Ext.getCmp('ExpressCompanyFun');
		if(win){win.close();}
		}
