var goodsSampleWayDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'packUser',
		type:"string"
	});
	    fields.push({
		name:'isQualified',
		type:"string"
	});
	    fields.push({
		name:'order',
		type:"string"
	});
	    fields.push({
		name:'company',
		type:"string"
	});
	    fields.push({
		name:'sendTime',
		type:"string"
	});
	    fields.push({
		name:'isReceive',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.goods.samplePackageCode,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'packUser',
		header:biolims.goods.packUser,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'isQualified',
		header:biolims.common.isQualified,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'order',
		header:biolims.sample.expreceCode,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'company',
		header:biolims.sample.companyName,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sendTime',
		header:biolims.sample.sendOutTime,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'isReceive',
		header:biolims.goods.isReceive,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.goods.stateId,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.goods.state,
		width:40*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/sample/goodsSampleWay/showGoodsSampleWayListJson.action";
	var opts={};
	opts.title=biolims.goods.logisticsTracking;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setGoodsSampleWayFun(rec);
	};
	goodsSampleWayDialogGrid=gridTable("show_dialog_goodsSampleWay_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(goodsSampleWayDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
