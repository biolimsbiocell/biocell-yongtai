﻿var goodsMaterialsDetailsGrid1;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
   fields.push({
		name:'code',
		type:"string"
	});
   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'count',
		type:"string"
	});
	   fields.push({
		name:'mateType-id',
		type:"string"
	});
   fields.push({
		name:'mateType-name',
		type:"string"
	});
   fields.push({
		name:'unit-id',
		type:"string"
	});
  fields.push({
		name:'unit-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsPack-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsPack-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.master.serialNum,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.id,
		width:30*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:30*6
	});
	cm.push({
		dataIndex:'mateType-id',
		hidden : true,
		header:biolims.master.mateTypeId,
		width:20*6
	});
	cm.push({
		dataIndex:'mateType-name',
		hidden : false,
		header:biolims.master.mateTypeName,
		width:20*6
	});
	cm.push({
		dataIndex:'unit-id',
		hidden : true,
		header:biolims.common.unitId,
		width:20*6
	});
	cm.push({
		dataIndex:'unit-name',
		hidden : false,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'count',
		hidden : false,
		header:biolims.common.count,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:40*6
	});
	cm.push({
		dataIndex:'goodsMaterialsPack-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'goodsMaterialsPack-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/pack/goodsMaterialsPack/showGoodsMaterialsDetailsListJsonById.action?code="+ $("#code").val();
	var opts={};
	opts.title=biolims.master.materialDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	goodsMaterialsDetailsGrid1=gridEditTable("goodsMaterialsDetailsdiv1",cols,loadParam,opts);
});

