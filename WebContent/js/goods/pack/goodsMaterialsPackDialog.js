var goodsMaterialsPackDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
			name:'product-id',
			type:"string"
		});
	    fields.push({
			name:'product-name',
			type:"string"
		});
	    fields.push({
			name:'sampleType-id',
			type:"string"
		});
	    fields.push({
			name:'sampleType-name',
			type:"string"
		});
	    fields.push({
		name:'bloodTube-id',
		type:"string"
	});
	    fields.push({
			name:'bloodTube-name',
			type:"string"
		});
	    fields.push({
	    	name:'num',
	    	type:'string'
	    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'num',
		header:biolims.master.defaultNum,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-id',
		header:biolims.common.sampleTypeId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-name',
		header:biolims.common.sampleType,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/pack/goodsMaterialsPack/showGoodsMaterialsPackListJson.action";
	var opts={};
	opts.title=biolims.master.materialPackage;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setGoodsMaterialsPackFun(rec);
		//setGoodsMaterialsPackFun(rec);
	};
	goodsMaterialsPackDialogGrid=gridTable("show_dialog_goodsMaterialsPack_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(goodsMaterialsPackDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
