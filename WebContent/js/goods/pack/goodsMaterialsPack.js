var goodsMaterialsPackGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
			name:'isCode',
			type:"string"
		});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"String"
	});
	    fields.push({
		name:'sampleType-id',
		type:"string"
	});
    fields.push({
		name:'sampleType-name',
		type:"string"
	});
    fields.push({
		name:'product-id',
		type:"string"
	});
    fields.push({
		name:'product-name',
		type:"string"
	});
    fields.push({
		name:'bloodTube-id',
		type:"string"
	});
    fields.push({
		name:'bloodTube-name',
		type:"string"
	});
    fields.push({
    	name:'num',
    	type:"string"
    });
    fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'num',
		header:biolims.master.defaultNum,
		wodth:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.wk.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.wk.productName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-id',
		header:biolims.common.sampleTypeId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-name',
		header:biolims.common.sampleType,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'bloodTube-id',
		header:biolims.plasma.bloodTubeId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'bloodTube-name',
		header:biolims.plasma.bloodTube,
		width:20*6,
		sortable:true
	});
	var isCodestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ]]
	});
	
	var isCodeComboxFun = new Ext.form.ComboBox({
		store : isCodestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isCode',
		header:biolims.master.isCode,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(isCodeComboxFun),				
		sortable:true
	});
	var statestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.master.valid ], [ '0', biolims.master.invalid ]]
	});
	
	var stateComboxFun = new Ext.form.ComboBox({
		store : statestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(stateComboxFun),				
		sortable:true
	});
//	cm.push({
//		dataIndex:'state',
//		header:'状态',
//		width:20*6,			
//		sortable:true
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/pack/goodsMaterialsPack/showGoodsMaterialsPackListJson.action";
	var opts={};
	opts.title=biolims.master.materialPackage;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	goodsMaterialsPackGrid=gridTable("show_goodsMaterialsPack_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/goods/pack/goodsMaterialsPack/editGoodsMaterialsPack.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/goods/pack/goodsMaterialsPack/editGoodsMaterialsPack.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/goods/pack/goodsMaterialsPack/viewGoodsMaterialsPack.action?id=' + id;
}
function exportexcel() {
	goodsMaterialsPackGrid.title = biolims.common.exportList;
	var vExportContent = goodsMaterialsPackGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(goodsMaterialsPackGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
