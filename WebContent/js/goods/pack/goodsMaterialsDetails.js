﻿var goodsMaterialsDetailsGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
   fields.push({
		name:'code',
		type:"string"
	});
   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'count',
		type:"string"
	});
	   fields.push({
		name:'mateType-id',
		type:"string"
	});
   fields.push({
		name:'mateType-name',
		type:"string"
	});
   fields.push({
		name:'unit-id',
		type:"string"
	});
  fields.push({
		name:'unit-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsPack-id',
		type:"string"
	});
	    fields.push({
		name:'goodsMaterialsPack-name',
		type:"string"
	});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.master.serialNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.id,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mateType-id',
		hidden : true,
		header:biolims.master.mateTypeId,
		width:20*6
	});
	cm.push({
		dataIndex:'mateType-name',
		hidden : false,
		header:biolims.master.mateTypeName,
		width:20*6
	});
	cm.push({
		dataIndex:'unit-id',
		hidden : true,
		header:biolims.common.unitId,
		width:20*6
	});
	cm.push({
		dataIndex:'unit-name',
		hidden : false,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'count',
		hidden : false,
		header:biolims.common.count,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'state',
//		hidden : false,
//		header:'状态',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsPack-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'goodsMaterialsPack-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/goods/pack/goodsMaterialsPack/showGoodsMaterialsDetailsListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.materialDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/goods/pack/goodsMaterialsPack/delGoodsMaterialsDetails.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				goodsMaterialsDetailsGrid.getStore().commitChanges();
				goodsMaterialsDetailsGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteSuccess);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler:showStorageList
	});
	opts.tbar.push({
		text : biolims.common.selectUnit,
		handler : selectunit
	});
//    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectgoodsMaterialsDetailsFun
//		});
//	
	
	
	
	
	
	
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = goodsMaterialsDetailsGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							goodsMaterialsDetailsGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	goodsMaterialsDetailsGrid=gridEditTable("goodsMaterialsDetailsdiv",cols,loadParam,opts);
	$("#goodsMaterialsDetailsdiv").data("goodsMaterialsDetailsGrid", goodsMaterialsDetailsGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//加载库存主数据
function showStorageList(){
	var options = {};
	options.width = 900;
	options.height = 600;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, biolims.common.theInventoryMasterData, url, {
		"Confirm" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					goodsMaterialsDetailsGrid.stopEditing();
					var ob = goodsMaterialsDetailsGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
				
					goodsMaterialsDetailsGrid.getStore().add(p);	
				});
				goodsMaterialsDetailsGrid.startEditing(0, 0);
				options.close();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	}, true, options);
}
function selectgoodsMaterialsDetailsFun(){
	var win = Ext.getCmp('selectgoodsMaterialsDetails');
	if (win) {win.close();}
	var selectgoodsMaterialsDetails= new Ext.Window({
	id:'selectgoodsMaterialsDetails',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GoodsMaterialsDetailsSelect.action?flag=goodsMaterialsDetails' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsMaterialsDetails.close(); }  }]  });     selectgoodsMaterialsDetails.show(); }
	function setgoodsMaterialsDetails(id,name){
		var gridGrid = $("#goodsMaterialsDetailsdiv").data("goodsMaterialsDetailsGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('goodsMaterialsDetails-id',id);
			obj.set('goodsMaterialsDetails-name',name);
		});
		var win = Ext.getCmp('selectgoodsMaterialsDetails');
		if(win){
			win.close();
		}
	}
	
//选择种类	
function selecttype(){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, biolims.common.theInventoryMasterData, url, {
		"Confirm" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					reagentItemGrid.stopEditing();
					var ob = reagentItemGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
//					p.set("itemId",code);
				
					reagentItemGrid.getStore().add(p);	
				});
				reagentItemGrid.startEditing(0, 0);
				
				options.close();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	}, true, options);
}

//选择单位
function selectunit(){
	var win = Ext.getCmp('selectunit');
	if (win) {win.close();}
	var selectunit= new Ext.Window({
	id:'selectunit',modal:true,title:biolims.common.selectUnit,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/dic/unit/dicUnitSelect.action?flag=weight' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectunit.close(); }  }]  });     selectunit.show(); }
function setweight(id,name){
		var gridGrid = $("#goodsMaterialsDetailsdiv").data("goodsMaterialsDetailsGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('unit-id',id);
			obj.set('unit-name',name);
		});
		var win = Ext.getCmp('selectunit');
		if(win){
			win.close();
		}
	}