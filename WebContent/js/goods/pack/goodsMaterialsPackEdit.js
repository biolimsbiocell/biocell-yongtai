﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/goods/pack/goodsMaterialsPack/editGoodsMaterialsPack.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/goods/pack/goodsMaterialsPack/showGoodsMaterialsPackList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var productId = $("#goodsMaterialsPack_productId").val();
	if (!productId) {
		message(biolims.wk.productIdIsEmpty);
		return;
	}
	
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#goodsMaterialsPack", {
					userId : userId,
					userName : userName,
					formId : $("#goodsMaterialsPack_id").val(),
					title : $("#goodsMaterialsPack_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#goodsMaterialsPack_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var goodsMaterialsDetailsDivData = $("#goodsMaterialsDetailsdiv").data("goodsMaterialsDetailsGrid");
		document.getElementById('goodsMaterialsDetailsJson').value = commonGetModifyRecords(goodsMaterialsDetailsDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/goods/pack/goodsMaterialsPack/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/goods/pack/goodsMaterialsPack/copyGoodsMaterialsPack.action?id=' + $("#goodsMaterialsPack_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#goodsMaterialsPack_id").val() + "&tableId=goodsMaterialsPack");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#goodsMaterialsPack_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.master.materialPackage,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/goods/pack/goodsMaterialsPack/showGoodsMaterialsDetailsList.action", {
				id : $("#goodsMaterialsPack_id").val()
			}, "#goodsMaterialsDetailspage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
function productFun(){
		var win = Ext.getCmp('productFun');
		if (win) {win.close();}
		var productFun= new Ext.Window({
		id:'productFun',modal:true,title:biolims.pooling.selectBusinessType,layout:'fit',width:600,height:600,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
			 productFun.close(); }  }]  });     productFun.show(); }

function setProductFun(id,name){
//		var productName = "";
//		ajax(
//				"post",
//				"/com/biolims/system/product/findProductToSample.action",
//				{
//					code : id,
//				},
//				function(data) {
//
//					if (data.success) {
//						$.each(data.data, function(i, obj) {
//							productName += obj.name + ",";
//						});
//						document.getElementById("goodsMaterialsPack_product").value = id;
//						document.getElementById("goodsMaterialsPack_product_name").value = productName;
//					}
//				}, null);
		document.getElementById("goodsMaterialsPack_productId").value = id;
		document.getElementById("goodsMaterialsPack_productName").value = name;
		var win = Ext.getCmp('productFun');
		if(win){win.close();}
		}
function sampleTypeFun(){
	var win = Ext.getCmp('sampleTypeFun');
	if (win) {win.close();}
	var sampleTypeFun= new Ext.Window({
	id:'sampleTypeFun',modal:true,title:biolims.common.selectSampleType,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 sampleTypeFun.close(); }  }]  });     sampleTypeFun.show(); }

function setyblx(id,name){
	document.getElementById("goodsMaterialsPack_sampleType").value =id;
	document.getElementById("goodsMaterialsPack_sampleType_name").value = name;
	var win = Ext.getCmp('sampleTypeFun');
	if(win){win.close();}
	}
function bloodTubeFun(){
	var win = Ext.getCmp('bloodTubeFun');
	if (win) {win.close();}
	var bloodTubeFun= new Ext.Window({
	id:'bloodTubeFun',modal:true,title:biolims.plasma.selectBloodTube,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=bloodTube' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 bloodTubeFun.close(); }  }]  });     bloodTubeFun.show(); }

function setbloodTube(id,name){
	document.getElementById("goodsMaterialsPack_bloodTube").value = id;
	document.getElementById("goodsMaterialsPack_bloodTube_name").value = name;
	var win = Ext.getCmp('bloodTubeFun');
	if(win){win.close();}
	}