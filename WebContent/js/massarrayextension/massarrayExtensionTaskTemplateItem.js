var massarrayExtensionTaskTemplateItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({    	  
	    name:'id',
	    type:"string"
    });
    fields.push({
	    name:'code',
	    type:"string"
    });
    fields.push({
		name:'testUser-id',
		type:"string"
	});
    fields.push({
		name:'testUser-name',
		type:"string"
	});
    fields.push({
		name:'tItem',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
	    name:'startTime',
		type:"string"
	});
	fields.push({
		name:'endTime',
		type:"string"
	});
    fields.push({
		name:'sampleCodes',
		type:"string"
	});
    fields.push({
	    name:'state',
	    type:"string"
	});
    fields.push({
	    name:'massarrayExtensionTask-id',
	    type:"string"
	});
	fields.push({
		name:'massarrayExtensionTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'步骤id',
		width:20*6
	});	
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'步骤编号',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'步骤名称',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'testUser-id',
		hidden : true,
		header:'实验员',
		width:20*6		
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUser-name',
		hidden : false,
		header:'实验员',
		width:20*6,		
		editor : testUser
	});	
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:30*6
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:'关联样本',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'状态',
		width:20*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/massarrayExtension/massarrayExtensionTask/showTemplateItemListJson.action?id="+$("#id_parent_hidden").val();
	var opts={};
	opts.title="实验步骤";
	opts.height =  document.body.clientHeight*0.5;
	opts.tbar = [];
	var state=$("#massarrayExtensionTask_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/massarrayExtension/massarrayExtensionTask/delTemplateItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				massarrayExtensionTaskTemplateItemGrid.getStore().commitChanges();
				massarrayExtensionTaskTemplateItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_print',
		text : '打印执行单',
		handler : stampOrder
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : '填加明细',
		handler : templateSelect
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : '开始执行',
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : '执行结束',
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : '生成结果',
		handler : addSuccess
	});
	}
	massarrayExtensionTaskTemplateItemGrid=gridEditTable("massarrayExtensionTaskTemplateItemdiv",cols,loadParam,opts);
	$("#massarrayExtensionTaskTemplateItemdiv").data("massarrayExtensionTaskTemplateItemGrid",massarrayExtensionTaskTemplateItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");	
});

//打印执行单
function stampOrder(){
}

function addSuccess(){
	var getRecord = massarrayExtensionTaskItemGrid.getAllRecord();
	var selectRecord = massarrayExtensionTaskTemplateItemGrid.getSelectionModel().getSelections();
	var selRecord = massarrayExtensionTaskResultGrid.store;
	if(selectRecord.length>0){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("Codes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<selRecord.getCount();j1++){
							var getv = scode[i1];
							var setv = selRecord.getAt(j1).get("massarrayExtensionTaskCode");
							if(getv == setv){
								isRepeat = false;
								message("有重复的数据，请重新选择！");
								break;					
							}
						}
							if(isRepeat){
								$.each(getRecord,function(a,b){
									if(b.get("code")==scode[i1]){
									var productNum=b.get("productNum");
									if(b.get("dicType-name")==null||b.get("dicType-name")==""){
										message("请填写中间产物");
										return;
									}
										if(productNum==null||productNum==""||productNum==0){
											message("请填写中间产物数量");
										}else{
											for(var k=1;k<=productNum;k++){
												var ob = massarrayExtensionTaskResultGrid.getStore().recordType;
												massarrayExtensionTaskResultGrid.stopEditing();
												var p = new ob({});
												p.isNew = true;
												p.set("tempId",b.get("tempId"));
												p.set("sampleCode",b.get("sampleCode"));
												p.set("code",b.get("code"));
												p.set("dicSampleType-id",b.get("dicSampleType-id"));
												p.set("dicSampleType-name",b.get("dicSampleType-name"));
												p.set("tempId",b.get("tempId"));
												p.set("concentration",b.get("concentration"));
												p.set("patientName",b.get("patientName"));
												p.set("idCard",b.get("idCard"));
												p.set("phone",b.get("phone"));
												p.set("sequenceFun",b.get("sequenceFun"));
												p.set("productName",b.get("productName"));
												p.set("productId",b.get("productId"));
												p.set("inspectDate",b.get("inspectDate"));
												p.set("reportDate",b.get("reportDate"));
												p.set("orderId",b.get("orderId"));
												p.set("result","1");												
												p.set("projectId",b.get("projectId"));//项目编号
												p.set("contractId",b.get("contractId"));//合同编号
												p.set("orderType",b.get("orderType"));//任务单类型
												p.set("taskId",b.get("taskId"));//科技服务任务单
												p.set("classify",b.get("classify"));//科技服务任务单
												p.set("sampleType",b.get("sampleType"));
												p.set("labCode",b.get("labCode"));
												ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
													model : "MassarrayExtensionTask",productId:b.get("productId")
												}, function(data) {
													p.set("nextFlowId",data.dnextId);
													p.set("nextFlow",data.dnextName);
												}, null);
												message("生成结果成功！");
												massarrayExtensionTaskResultGrid.getStore().add(p);
												massarrayExtensionTaskResultGrid.startEditing(0,0);
											}
										}
									}
								});								
							}													
					}										
				});			
	}else{
		var selRecord = massarrayExtensionTaskResultGrid.store;
		var flag;		
		var getRecord = massarrayExtensionTaskItemGrid.getAllRecord();
		$.each(getRecord,function(a,b){
			flag = true;
			for(var j1=0;j1<selRecord.getCount();j1++){
				var getv = b.get("code");
				var setv = selRecord.getAt(j1).get("massarrayExtensionTaskCode");
				if(getv == setv){
					flag = false;
					message("有重复的数据，请重新选择！");
					break;					
				}
			}
			if(flag==true){
			var productNum=b.get("productNum");
			if(b.get("dicSampleType-name")==null||b.get("dicSampleType-name")==""){
				message("请填写中间产物");
				return;
			}
				if(productNum==null||productNum==""||productNum==0){
					message("请填写中间产物数量");
				}else{
					for(var k=1;k<=productNum;k++){
						var ob = massarrayExtensionTaskResultGrid.getStore().recordType;
						massarrayExtensionTaskResultGrid.stopEditing();
						var p = new ob({});
						p.isNew = true;
						p.set("tempId",b.get("tempId"));
						p.set("sampleCode",b.get("sampleCode"));
						p.set("code",b.get("code"));
						p.set("dicSampleType-id",b.get("dicSampleType-id"));
						p.set("dicSampleType-name",b.get("dicSampleType-name"));
						p.set("tempId",b.get("tempId"));
						p.set("concentration",b.get("concentration"));
						p.set("patientName",b.get("patientName"));
						p.set("idCard",b.get("idCard"));
						p.set("phone",b.get("phone"));
						p.set("sequenceFun",b.get("sequenceFun"));
						p.set("productName",b.get("productName"));
						p.set("productId",b.get("productId"));
						p.set("inspectDate",b.get("inspectDate"));
						p.set("reportDate",b.get("reportDate"));
						p.set("orderId",b.get("orderId"));
						p.set("result","1");						
						p.set("projectId",b.get("projectId"));//项目编号
						p.set("contractId",b.get("contractId"));//合同编号
						p.set("orderType",b.get("orderType"));//任务单类型
						p.set("taskId",b.get("taskId"));//科技服务任务单
						p.set("classify",b.get("classify"));//科技服务任务单
						p.set("sampleType",b.get("sampleType"));
						p.set("labCode",b.get("labCode"));
						ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
							model : "MassarrayExtensionTask",productId:b.get("productId")
						}, function(data) {
							p.set("nextFlowId",data.dnextId);
							p.set("nextFlow",data.dnextName);
						}, null);
						message("生成结果成功！");
						massarrayExtensionTaskResultGrid.getStore().add(p);
						massarrayExtensionTaskResultGrid.startEditing(0,0);
					}
				}
			}
		});		
	}
}

//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=massarrayExtensionTaskTemplateItemGrid.getSelectionModel();
	var setNum = massarrayExtensionTaskTemplateReagentGrid.store;
	var selectRecords = massarrayExtensionTaskItemGrid.getSelectionModel();
	if(selectRecords.getSelections().length>0){
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			obj.set("startTime",str);
			//将所选样本的数量，放到原辅料样本数量处
			for(var i=0; i<setNum.getCount();i++){
				var num = setNum.getAt(i).get("itemId");
				if(num==obj.get("code")){
					setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
				}
			}
		});		
	}else{
		message("请选择实验步骤");
	}
	var selectRecord=massarrayExtensionTaskItemGrid.getSelectionModel();
	var selRecord=massarrayExtensionTaskTemplateItemGrid.getSelectRecord();
	var sampleCodes = "";
	$.each(selectRecord.getSelections(), function(i, obj) {
		sampleCodes += obj.get("code")+",";
	});
	$.each(selRecord, function(i, obj) {
		obj.set("Codes", Codes);
	});
	}else{
	    message("请选择实验样本");
    }
}

//获取停止时的时间
function getEndTime(){
		var setRecord=massarrayExtensionTaskItemGrid.store;
		var d = new Date();
		var getIndex= massarrayExtensionTaskTemplateItemGrid.store;
		var getIndexs = massarrayExtensionTaskTemplateItemGrid.getSelectionModel().getSelections();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=massarrayExtensionTaskTemplateItemGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("endTime",str);
				var codes = obj.get("Codes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("Code")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("Codes",codes);
			});
		}
		else{
			message("请选择实验步骤");
		}
}

//选择实验步骤
function templateSelect(){
	var option = {};
	option.width = 605;
	option.height = 558;
	loadDialogPage(null, "选择实验步骤", "/experiment/massarrayExtension/massarrayExtensionTask/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
		"确定" : function() {
			var operGrid = $("#template_wait_grid_div").data("grid");
			var ob = massarrayExtensionTaskTemplateItemGrid.getStore().recordType;
			massarrayExtensionTaskTemplateItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code", obj.get("code"));
						p.set("name", obj.get("name"));	
						massarrayExtensionTaskTemplateItemGrid.getStore().add(p);
				});
				massarrayExtensionTaskTemplateItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message("请选择您要选择的数据");
				return;
			}
		}
	}, true, option);	
}

//查询实验员
function loadTestUser(){
	var win = Ext.getCmp('loadTestUser');
	if (win) {win.close();}
	var loadTestUser= new Ext.Window({
	id:'loadTestUser',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=loadTestUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadTestUser.close(); }  }]  });     loadTestUser.show(); }
	function setloadTestUser(id,name){
		var gridGrid = $("#massarrayExtensionTaskTemplateItemdiv").data("massarrayExtensionTaskTemplateItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('testUser-id',id);
			obj.set('testUser-name',name);
		});
		var win = Ext.getCmp('loadTestUser');
		if(win){
			win.close();
		}
}
