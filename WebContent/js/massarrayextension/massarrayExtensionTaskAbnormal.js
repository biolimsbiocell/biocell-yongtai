var massarrayExtensionTaskAbnormalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
    fields.push({	   
	    name:'sampleCode',
		type:"string"
    });
    fields.push({
	    name:'code',
	    type:"string"
    }); 
    fields.push({
		name:'patientName',
		type:"string"
    });
    fields.push({
		name:'productName',
		type:"string"
    });
    fields.push({
		name:'productId',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
    fields.push({
		name:'inspectDate',
		type:"string"
	});
    fields.push({
		name : 'idCard',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	}); 
    fields.push({
		name : 'orderId',
		type : "string"
	});
    fields.push({
	    name:'sampleType',
	    type:"string"
	});
    fields.push({
		name:'sampleCondition',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'isExecute',
		type:"string"
	});
	fields.push({
		name:'feedbackTime',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'classify',
		type:"string"
	});
	fields.push({
		name:'sampleNum',
		type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
	});	
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		hidden : false,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		hidden : true,
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'关联任务单',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'Condition',
		hidden : true,
		header:'样本情况',
		width:20*6
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),
		editor: result
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理意见',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeconfirmCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var confirmCob = new Ext.form.ComboBox({
		store : storeconfirmCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:'确认执行',
		width:20*6,
		hidden : false,
		editor : confirmCob,
		renderer : Ext.util.Format.comboRenderer(confirmCob)
	});	
	cm.push({
		dataIndex:'feedbackTime',
		hidden : false,
		header:'异常反馈时间',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务 ',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/massarrayExtension/massarrayExtensionTaskAbnormal/showMassarrayExtensionTaskAbnormalListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title="MassarrayExtension任务异常";
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		text : "批量结果",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = massarrayExtensionTaskAbnormalGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						massarrayExtensionTaskAbnormalGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						massarrayExtensionTaskAbnormalGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "批量下一步",
		handler : function() {
			var records = massarrayExtensionTaskAbnormalGrid.getSelectRecord();
			if(records.length>0){
				if(records.length>2){
					var productId = new Array();
					$.each(records, function(j, k) {
						productId[j]=k.get("productId");
					});
					for(var i=0;i<records.length;i++){
						if(i!=0&&productId[i]!=productId[i-1]){
							message("检测项目不同！");
							return;
						}
					}
					loadTestNextFlowCob();
				}else{
					loadTestNextFlowCob();
				}				
			}else{
				message("请选择数据!");
			}
		}
	});
	opts.tbar.push({
		text : "批量执行",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), "批量执行", null, {
				"确定" : function() {
					var records = massarrayExtensionTaskAbnormalGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute").val();
						massarrayExtensionTaskAbnormalGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", isExecute);
						});
						massarrayExtensionTaskAbnormalGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	massarrayExtensionTaskAbnormalGrid=gridEditTable("show_massarrayExtensionTaskAbnormal_div",cols,loadParam,opts);
	$("#show_massarrayExtensionTaskAbnormal_div").data("massarrayExtensionTaskAbnormalGrid", massarrayExtensionTaskAbnormalGrid);
});

//保存
function save(){
	var itemJson = commonGetModifyRecords(massarrayExtensionTaskAbnormalGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/massarrayExtension/massarrayExtensionTaskAbnormal/saveMassarrayExtensionTaskAbnormal.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					massarrayExtensionTaskAbnormalGrid.getStore().commitChanges();
					massarrayExtensionTaskAbnormalGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);					
	}else{
		message("没有需要保存的数据！");
	}
}

//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
			commonSearchAction(massarrayExtensionTaskAbnormalGrid);
			$(this).dialog("close");
		},
		"清空" : function() {
			form_reset();
		}
	}, true, option);
}

function selectInfo(){
	massarrayExtensionTaskAbnormalGrid.store.removeAll();
	var code=$("#massarrayExtensionTaskAbnormal_code").val();
	var Code=$("#massarrayExtensionTaskAbnormal_Code").val();
	ajax("post", "/experiment/massarrayExtension/massarrayExtensionTaskAbnormal/selectAbnormal.action", {
		code : code,Code:Code
	}, function(data) {
		if (data.success) {	
			var ob = massarrayExtensionTaskAbnormalGrid.getStore().recordType;
			massarrayExtensionTaskAbnormalGrid.stopEditing();
			$.each(data.data, function(i, obj) {
				var p = new ob({});
				p.isNew = true;				
				p.set("id", obj.id);
				p.set("code", obj.code);
				p.set("Code", obj.Code);
				p.set("Type", obj.Type);
				p.set("method", obj.method);
				p.set("nextFlow", obj.nextFlow);
				p.set("note", obj.note);
				p.set("methods", obj.methods);
				p.set("feedbackTime", obj.feedbackTime);
				p.set("isExecute", obj.isExecute);
				massarrayExtensionTaskAbnormalGrid.getStore().add(p);							
			});
			massarrayExtensionTaskAbnormalGrid.startEditing(0, 0);		
		} else {
			message("获取明细数据时发生错误！");
		}
	}, null);
	$("#massarrayExtensionTaskAbnormal_code").val("");
	$("#massarrayExtensionTaskAbnormal_Code").val("");
}

//下一步流向
function loadTestNextFlowCob(){
	var records1 = massarrayExtensionTaskAbnormalGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=MassarrayExtensionTask&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = massarrayExtensionTaskAbnormalGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}