function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/technology/wk/techJkServiceTask/editTechJkServiceTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/technology/wk/techJkServiceTask/viewTechJkServiceTask.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : biolims.common.id,
		dataIndex : 'id',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.name,
		dataIndex : 'name',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : biolims.common.projectId,
		dataIndex : 'projectId-name',
		width:20*6,
		hidden:false
	}, 
	    {
		header : biolims.common.contractId,
		dataIndex : 'contractId-name',
		width:20*6,
		hidden:false
	}, 
	    {
		header : biolims.common.orderTypeName,
		dataIndex : 'orderType-name',
		width:20*6,
		hidden:false
	}, 
	    {
		header : biolims.wk.productId,
		dataIndex : 'productId-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.common.dataType,
		dataIndex : 'dataType',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.sequenceBillDate,
		dataIndex : 'sequenceBillDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.sequenceBillName,
		dataIndex : 'sequenceBillName',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.sequenceType,
		dataIndex : 'sequenceType',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.sequenceLength,
		dataIndex : 'sequenceLength',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.sequencePlatform,
		dataIndex : 'sequencePlatform',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.sampleNum,
		dataIndex : 'sampeNum',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.species,
		dataIndex : 'species',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.experimentPeriod,
		dataIndex : 'experimentPeriod',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.experimentEndTime,
		dataIndex : 'experimentEndTime',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.experimentRemarks,
		dataIndex : 'experimentRemarks',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.analysisCycle,
		dataIndex : 'analysisCycle',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.analysisEndTime,
		dataIndex : 'analysisEndTime',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.analysisRemarks,
		dataIndex : 'anaysisRemarks',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.projectDifference,
		dataIndex : 'projectDifference',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.mixRatio,
		dataIndex : 'mixRatio',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.coefficient,
		dataIndex : 'coefficient',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : 'readsone',
		dataIndex : 'readsOne',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : 'readstwo',
		dataIndex : 'readsTwo',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : 'index1',
		dataIndex : 'index1',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : 'index2',
		dataIndex : 'index2',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.wk.density,
		dataIndex : 'density',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.wk.orders,
		dataIndex : 'orders-name',
		width:20*6,
		hidden:false
	}, 
	    {
		header : biolims.common.projectLeader,
		dataIndex : 'projectLeader-name',
		width:20*6,
		hidden:false
	}, 
	    {
		header : biolims.common.testLeader,
		dataIndex : 'experimentLeader-name',
		width:20*6,
		hidden:false
	}, 
	    {
		header : biolims.common.analysisLeader,
		dataIndex : 'analysisLeader-name',
		width:20*6,
		hidden:false
	}, 
	    {
		header : biolims.common.relatedMainPro,
		dataIndex : 'techProjectTask-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.common.note,
		dataIndex : 'note',
		width:50*6,
		hidden:true
	}, 
	
	    {
		header : biolims.sample.createUserName,
		dataIndex : 'createUser-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.sample.createDate,
		dataIndex : 'createDate',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.wk.approver,
		dataIndex : 'confirmUser-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.wk.approverDate,
		dataIndex : 'confirmDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.state,
		dataIndex : 'state',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.stateName,
		dataIndex : 'stateName',
		width:20*6,
		hidden:false
	}, 
	
			
			
			
			{
				header : biolims.common.upId,
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#techJkServiceTaskTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
