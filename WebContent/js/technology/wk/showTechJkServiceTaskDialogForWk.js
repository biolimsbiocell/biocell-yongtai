var showTechJkServiceTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'projectId-id',
		type:"string"
	});
	    fields.push({
		name:'projectId-name',
		type:"string"
	});
	    fields.push({
			name:'acceptUser-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
		});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
			name:'stateName',
			type:"string"
		});
	    fields.push({
			name:'proId',
			type:"string"
		});
	    fields.push({
			name:'proName',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'proId',
//		header:biolims.common.productId,
//		width:20*10,
//		hidden:true,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'proName',
//		header:biolims.common.productName,
//		width:20*6,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'projectId-id',
//		header:biolims.common.projectId,
//		width:20*10,
//		hidden:true,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'projectId-name',
//		header:biolims.common.projectName,
//		width:20*10,
//		hidden:true,
//		sortable:true
//		});
//		cm.push({
//			dataIndex:'acceptUser-id',
//			header:"实验组ID",
//			width:20*10,
//			sortable:true
//		});
//		cm.push({
//			dataIndex:'acceptUser-name',
//			header:"实验组",
//			width:20*10,
//			sortable:true
//		});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.state,
		width:20*10,
		hidden:false
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogForWkListJson.action?type="+$("#jktype").val()+"&state="+$("#state").val();
	var opts={};
	opts.title=biolims.common.scienceServiceTest;
	opts.width = document.body.clientWidth - 550;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect=function(id){
		$("#selectId1").val(id);
	};
	opts.rowdblclick=function(id,rec){
		setTechService();
	};
	showTechJkServiceTaskDialogGrid=gridTable("show_dialog_techJkServiceTask_div1",cols,loadParam,opts);
	$("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid", showTechJkServiceTaskDialogGrid);
});

function sc1(){
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj1"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(showTechJkServiceTaskDialogGrid);
			$(this).dialog("close");

		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}
