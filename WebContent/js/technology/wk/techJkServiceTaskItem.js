var techJkServiceTaskItemGrid;

$(function(){
	var flg=true;
	var jk=true;
	var sj=true;
	var sx=true;
	var type=$("#type2").val();
	if(type=="0"){//质检任务单
		$(".jk").addClass("hide");
		$(".sx").addClass("hide");
		flg=false;
	}else if(type=="1"){//建库任务单
		$(".jk").removeClass("hide");
		$(".sx").addClass("hide");
		jk=false;
	}else if(type=="2"){//上机任务单
		$(".jk").addClass("hide");
		$(".sx").addClass("hide");
		sj=false;
	}else if(type=="3"){//生信任务单
		$(".jk").addClass("hide");
		sx=false;
	}
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		   name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	    fields.push({
		name:'experimentUser-id',
		type:"string"
	});
	    fields.push({
		name:'experimentUser-name',
		type:"string"
	});
	   fields.push({
		name:'endDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'species',
		type:"string"
	});
	    fields.push({
		name:'techJkServiceTask-id',
		type:"string"
	});
	    fields.push({
		name:'techJkServiceTask-name',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
		name:'dataType',
		type:"string"
	});
	   fields.push({
		name:'dataNum',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
			name:'nextFlowId',
			type:"string"
		});
		   fields.push({
			name:'nextFlow',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
		   fields.push({
			name:'productId',
			type:"string"
		});
	   fields.push({
			name:'productName',
			type:"string"
		});
		   fields.push({
			name:'acceptDate',
			type:"string"
		});
	   fields.push({
			name:'reportDate',
			type:"string"
		});
		   fields.push({
			name:'sampleNum',
			type:"string"
		});
		   fields.push({
				name:'code',
				type:"string"
			});
		   fields.push({
				name:'tempId',
				type:"string"
			});
		   fields.push({
				name:'wgcId',
				type:"string"
			});
		   fields.push({
				name:'tissueId',
				type:"string"
			});
		   fields.push({
				name:'sampeType',
				type:"string"
			});
		   fields.push({
				name:'extraction',
				type:"string"
			});
		   fields.push({
				name:'qcProtocol',
				type:"string"
			});
		   fields.push({
				name:'sequencingApplication',
				type:"string"
			});
		   fields.push({
				name:'sampleReceivedDate',
				type:"string"
			});
		   fields.push({
				name:'specialRequirements',
				type:"string"
			});
		   fields.push({
				name:'location',
				type:"string"
			});
		   fields.push({
				name:'concentration',
				type:"string"
			});
		   fields.push({
				name:'volume',
				type:"string"
			});
		   fields.push({
				name : 'project-id',
				type : "string"
			});
			fields.push({
				name : 'project-name',
				type : "string"
			});
			fields.push({
				name : 'crmCustomer-id',
				type : "string"
			});
			fields.push({
				name : 'crmCustomer-name',
				type : "string"
			});
			fields.push({
				name : 'crmDoctor-id',
				type : "string"
			});
			fields.push({
				name : 'crmDoctor-name',
				type : "string"
			});
			fields.push({
				name : 'inwardCode',
				type : "string"
			});
			fields.push({
				name : 'sellPerson-id',
				type : "string"
			});
			fields.push({
				name : 'sellPerson-name',
				type : "string"
			});
		   
			fields.push({
				name : 'blendLane',
				type : "string"
			});
			fields.push({
				name : 'blendFc',
				type : "string"
			});
			fields.push({
				name : 'sampleInfoIn-id',
				type : "string"
			});
			
			fields.push({
				name : 'khysConcentration',
				type : "string"
			});
			fields.push({
				name : 'khysVolume',
				type : "string"
			});
			fields.push({
				name : 'isStain',
				type : "string"
			});
			fields.push({
				name : 'isNeedStain',
				type : "string"
			});
			fields.push({
				name : 'probeCode-id',
				type : "string"
			});
			fields.push({
				name : 'probeCode-name',
				type : "string"
			});
			fields.push({
				name : 'gc',
				type : "string"
			});
			fields.push({
				name : 'phix',
				type : "string"
			});
			fields.push({
				name : 'q30',
				type : "string"
			});	
			fields.push({
				name : 'orderId',
				type : "string"
			});
			fields.push({
				name : 'note',
				type : "string"
			});
			fields.push({
				name : 'groupNo',
				type : "string"
			});
			fields.push({
				name : 'barCode',
				type : "string"
			});
			fields.push({
				name : 'yunKangCode',
				type : "string"
			});
			
			fields.push({
				name : 'orderCode',
				type : "string"
			});
			fields.push({
				name : 'projectId',
				type : "string"
			});
			fields.push({
				name : 'sampleID',
				type : "string"
			});
			fields.push({
				name : 'sxProductName',
				type : "string"
			});
			fields.push({
				name : 'runId',
				type : "string"
			});
			fields.push({
				name : 'laneId',
				type : "string"
			});
			fields.push({
				name : 'sxSampleCode',
				type : "string"
			});
			fields.push({
				name : 'indexa',
				type : "string"
			});
			fields.push({
				name : 'dataTraffic',
				type : "string"
			});
			
			
	cols.fields=fields;
	var cm=[];
	
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*7
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:"中间表id",
		width:20*10,
	});
	cm.push({
		dataIndex:'sampleInfoIn-id',
		hidden : true,
		header:biolims.sample.id,
		width:20*10,
	});
	if(sx){
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*7,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*7,
	});
	}
	cm.push({
		dataIndex:'groupNo',
		hidden : flg,
		header:biolims.common.packetNumber,
		width:20*6
	});
	
	
	cm.push({
		dataIndex:'indexa',
		hidden : sj,
		header:"index",
		width:20*6
	});
	if(sx){
		
	
	cm.push({
		dataIndex:'barCode',
		hidden : true,
		header:biolims.common.barCode,
		width:20*6
	});
	
	cm.push({
		dataIndex:'wgcId',
		hidden : false,
		header:biolims.common.poolingGroup,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'project-id',
		header:biolims.common.contractCodeId,
		width:20*6,
		hidden:false
	});
	cm.push({
		dataIndex:'project-name',
		header:biolims.common.contractCodeId,
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.sample.orderNum,
		width:20*6,
		hidden:false
	});
	cm.push({
		dataIndex:'inwardCode',
		header:"内部项目号",
		width:20*6,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'species',
		hidden : false,
		header:biolims.wk.species,
		width:20*6
	});
	}
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', "DNA抽提" ], [ '2', "RNA抽提" ], [ '3', "DNA QC" ], [ '4', "RNA QC" ], [ '5', "LIBRARY QC" ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'orderType',
		hidden : flg,
		header:biolims.common.qcType,
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	if(sx){
		
		cm.push({
			dataIndex:'dataTraffic',
			hidden : false,
			header:biolims.common.throughput,
			width:20*6
		});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	}
	var isStainGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', "是" ], [ '0', "否" ] ]
	});
	var isStainCob = new Ext.form.ComboBox({
		store : isStainGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isStain',
		hidden : flg,
		header:biolims.common.ifThereStain,
		width:20*6,
		editor : isStainCob,
		renderer : Ext.util.Format.comboRenderer(isStainCob)
	});
	var isNeedStainGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', "是" ], [ '0', "否" ] ]
	});
	var isNeedStainCob = new Ext.form.ComboBox({
		store : isNeedStainGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isNeedStain',
		hidden : flg,
		header:biolims.common.ifThereNeedStain,
		width:20*6,
		editor : isNeedStainCob,
		renderer : Ext.util.Format.comboRenderer(isNeedStainCob)
	});
	cm.push({
		dataIndex:'probeCode-id',
		hidden : true,
		header:'探针id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'probeCode-name',
		hidden : jk,
		header:biolims.pooling.probeCodeName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'gc',
		hidden : jk,
		header:'GC%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'phix',
		hidden : jk,
		header:'phix%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'q30',
		hidden : jk,
		header:'Q30',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataNum',
		hidden : jk,
		header:biolims.common.dataNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	if(sx){
	cm.push({
		dataIndex:'crmCustomer-id',
		header:biolims.sample.customerId,
		width:20*10,
		hidden:true
	});
	
		cm.push({
			dataIndex:'crmCustomer-name',
			header:biolims.sample.customerName,
			width:20*10,
			hidden:false
		});
	
	
	cm.push({
		dataIndex:'crmDoctor-id',
		header:"客户姓名id",
		width:20*10,
		hidden:true
	});
	cm.push({
		dataIndex:'crmDoctor-name',
		header:biolims.common.customName,
		width:20*6,
		hidden:false
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:biolims.common.location,
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6
	});
	
	
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:20*6
	});
	
	cm.push({
		dataIndex:'experimentUser-id',
		hidden : true,
		header:biolims.wk.experimentUserId,
		width:20*10
	});
	cm.push({
		dataIndex:'experimentUser-name',
		hidden : true,
		header:biolims.wk.experimentUserName,
		width:20*10
	});
	cm.push({
		dataIndex:'endDate',
		hidden : true,
		header:biolims.common.endDate,
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});

	
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cm.push({
		dataIndex:'dataType',
		hidden : true,
		header:biolims.common.dataType,
		width:20*6
	});
	
	}
	cm.push({
		dataIndex:'blendLane',
		hidden : jk,
		header:biolims.sequencing.mixLane,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'blendFc',
		hidden : jk,
		header:biolims.sequencing.mixFlowcell,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	if(sx){
		
	
	cm.push({
		dataIndex:'khysConcentration',
		hidden : true,
		header:'客户原始浓度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'khysVolume',
		hidden : true,
		header:'客户原始体积',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	cm.push({
		dataIndex:'tissueId',
		hidden : true,
		header:'Tissue ID',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'extraction',
		hidden : true,
		header:'Extraction',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcProtocol',
		hidden : true,
		header:'QC Protocol',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencingApplication',
		hidden : true,
		header:'Sequencing Application',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleReceivedDate',
		hidden : true,
		header:biolims.common.receiveDate,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'specialRequirements',
		hidden : true,
		header:'Special Requirements',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	}
	
	if(!sx){
		cm.push({
			dataIndex:'orderCode',
			hidden : sx,
			header:biolims.common.orderCode,
			width:20*6,
		});
		cm.push({
			dataIndex:'projectId',
			hidden : sx,
			header:biolims.common.projectId,
			width:20*6,
			
		});
		cm.push({
			dataIndex:'sampleID',
			hidden : sx,
			header:biolims.wk.wkCode,
			width:20*6,
			
		});
		cm.push({
			dataIndex:'sxProductName',
			hidden : sx,
			header:biolims.common.productName,
			width:20*6,
		});
		cm.push({
			dataIndex:'runId',
			hidden : sx,
			header:"RUN_ID",
			width:20*6,
		});
		cm.push({
			dataIndex:'laneId',
			hidden : sx,
			header:"laneId",
			width:20*6,
			editor : new Ext.form.TextField({
				allowBlank : true
			})
		});
		cm.push({
			dataIndex:'sxSampleCode',
			hidden : true,
			header:"原始样本号",
			width:20*6,
		});
		
	}
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/wk/techJkServiceTask/showTechJkServiceTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.scienceServiceTestDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/technology/wk/techJkServiceTask/delTechJkServiceTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//			text : '核酸结果样本',
//			handler : selectsampleCodeDialogFun
//		});
	if(type=="1"){//建库任务单
		opts.tbar.push({
			text :biolims.sample.inventorySamples,
			handler : selectsampleInItem
		});
	}
	
//	opts.tbar.push({
//		text : '检测项目',
//		handler : ProductFun
//	});
	if(type=="0"){//质检任务单
		opts.tbar.push({
			text : biolims.sample.specimenClaimed,
			handler : selectItemDialog
		});
	}
	if(type=="1"){//建库任务单
		opts.tbar.push({
			text : biolims.sample.waitingLibrarySample,
			handler : selectsampleStateByType
		});
	}
	if(type=="2"){//上机任务单
		opts.tbar.push({
			text : biolims.sequencing.forSequencingSamples,
			handler : selectThreeType
		});
	}
	if(type=="3"){//生信任务单
		opts.tbar.push({
			text : biolims.sample.waitSXSample,
			handler : selectFourType
		});
	}
	if(type!=3){
		opts.tbar.push({
			text : biolims.common.productName,
			handler : ProductFun
		});
		opts.tbar.push({
			text : biolims.common.contractName,
			handler : projectFun
		});
		opts.tbar.push({
			text : biolims.common.batchNextStep,
			handler : function() {
				var records = techJkServiceTaskItemGrid.getSelectRecord();
				if(records.length>0){
						loadTestNextFlowCob();
				}else{
					message(biolims.common.pleaseSelect);
				}
			}
		});
	}
	
	opts.tbar.push({
		text : "内部项目号",
		hidden:true,
		handler : function() {
			var records = techJkServiceTaskItemGrid.getSelectRecord();
			if (records && records.length > 0) {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_inwardCode_div"), "内部项目号", null, {
					"Confirm" : function() {
						var inwardCode = $("#inwardCode").val();
						techJkServiceTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("inwardCode", inwardCode);
						});
						techJkServiceTaskItemGrid.startEditing(0, 0);
						
						$(this).dialog("close");
					}
				}, true, options);
			}else{
				message(biolims.common.pleaseSelectData);
			}
		}
	});
	if(type=="0"){//核酸任务单
		opts.tbar.push({
			text : biolims.common.qcType,
			handler : function() {
				var records = techJkServiceTaskItemGrid.getSelectRecord();
				if (records && records.length > 0) {
					var options = {};
					options.width = 400;
					options.height = 300;
					loadDialogPage($("#bat_orderType_div"), "质检类型", null, {
						"Confirm" : function() {
							var orderType = $("#orderType").val();
							techJkServiceTaskItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("orderType", orderType);
							});
							techJkServiceTaskItemGrid.startEditing(0, 0);
							
							$(this).dialog("close");
						}
					}, true, options);
				}else{
					message(biolims.common.pleaseSelectData);
				}
			}
		});
	}
	if(type=="1"){//建库任务单
		opts.tbar.push({
			text :biolims.common.batchData,
			handler : function() {
				var records = techJkServiceTaskItemGrid.getSelectRecord();
				if (records && records.length > 0) {
					var options = {};
					options.width = 400;
					options.height = 300;
					loadDialogPage($("#bat_lane_div"), "批量数据", null, {
						"Confirm" : function() {
							var gc = $("#gc").val();
							var phix = $("#phix").val();
							var dataNum = $("#dataNum").val();
							var q30 = $("#q30").val();
							var blendLane = $("#blendLane").val();
							var blendFc = $("#blendFc").val();
							techJkServiceTaskItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								if(gc!=""){
									obj.set("gc", gc);
								}
								if(phix!=""){
									obj.set("phix", phix);
								}
								if(dataNum!=""){
									obj.set("dataNum", dataNum);
								}
								if(q30!=""){
									obj.set("q30", q30);
								}
								if(blendLane!=""){
									obj.set("blendLane", blendLane);
								}
								if(blendFc!=""){
									obj.set("blendFc", blendFc);
								}
							
							});
							techJkServiceTaskItemGrid.startEditing(0, 0);
							
							$(this).dialog("close");
						}
					}, true, options);
				}else{
					message(biolims.common.pleaseSelectData);
				}
			}
		});
	}

	if($("#techJkServiceTask_id").val()&&$("#techJkServiceTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : save
		});
	}
//	
//	if(type=="0"){//核酸任务单
//		opts.tbar.push({
//			text : "选择样本",
//			handler : function() {
//				var records = techJkServiceTaskItemGrid.getSelectRecord();
//				if (records && records.length > 0) {
//					var options = {};
//					options.width = 400;
//					options.height = 300;
//					loadDialogPage($("#bat_orderType_div"), "样本类型", null, {
//						"Confirm" : function() {
//							var orderType = $("#orderType").val();
//							techJkServiceTaskItemGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								obj.set("orderType", orderType);
//							});
//							techJkServiceTaskItemGrid.startEditing(0, 0);
//							
//							$(this).dialog("close");
//						}
//					}, true, options);
//				}else{
//					message("请选择数据！");
//				}
//			}
//		});
//	}
	
	
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
	techJkServiceTaskItemGrid=gridEditTable("techJkServiceTaskItemdiv",cols,loadParam,opts);
	$("#techJkServiceTaskItemdiv").data("techJkServiceTaskItemGrid", techJkServiceTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");	
});

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialogAll.action", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = techJkServiceTaskItemGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = techJkServiceTaskItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}


function selectsampleCodeDialogFun(){ 
	
		var title = '';
		var url = '';
		title = biolims.sample.selectSample;
		var projectId=$("#techJkServiceTask_projectId").val();
		if(projectId==null||projectId==""){
			url = "/technology/wk/techJkServiceTask/SampleDnaInfo.action?projectId="+projectId;
//			message("项目不能为空！");
//			return;
		}else{
			url = "/technology/wk/techJkServiceTask/SampleDnaInfo.action?projectId="+projectId;
		}
		
		var option = {};
		option.width = document.body.clientWidth-200;
		option.height = document.body.clientHeight-80;
		loadDialogPage(null, title, url, {
			"Confirm" : function() {
				var operGrid = $("#template_wait_grid_div").data("sampleDnaInfoDialogGrid");
				var ob = techJkServiceTaskItemGrid.getStore().recordType;
				techJkServiceTaskItemGrid.stopEditing();
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code", obj.get("code"));
							p.set("sampleCode", obj.get("sampleCode"));
							p.set("sampleType", obj.get("sampleType"));
							p.set("productId", obj.get("productId"));
							p.set("productName", obj.get("productName"));
							p.set("reportDate", obj.get("reportDate"));
							p.set("sampleNum", obj.get("sampleNum"));
					
							p.set("state", "1");
							techJkServiceTaskItemGrid.getStore().add(p);
					});
					techJkServiceTaskItemGrid.startEditing(0, 0);
					$(this).dialog("close");
					$(this).dialog("remove");
				} else {
					message(biolims.common.selectYouWant);
					return;
				}
			}
		}, true, option);
	}
function ProductFun() {
	var win = Ext.getCmp('productFun');
	if (win) {
		win.close();
	}
	var productFun = new Ext.Window(
			{
				id : 'productFun',
				modal : true,
				title : biolims.common.selectProduct,
				layout : 'fit',
				width : 600,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						productFun.close();
					}
				} ]
			});
	productFun.show();
}

function setProductFun(id,name) {
	var selRecords = techJkServiceTaskItemGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('productId',id);
		obj.set('productName',name);
	});
	var win = Ext.getCmp('productFun');
	if (win) {
		win.close();
	}
	
}



//待建库样本
function selectsampleStateByType(){ 
	var title = '';
	var url = '';
	title = biolims.sample.selectInventorySamples;
	url = "/sample/storage/sampleIn/showSampleWaitOutItemList.action?type=2";//type=1 表示在待建库样本
	var option = {};
	option.width = document.body.clientWidth-50;
	option.height = document.body.clientHeight-50;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			var operGrid = $("#sampleWaitOutItemdiv").data("sampleWaitOutItemGrid");
			var ob = techJkServiceTaskItemGrid.getStore().recordType;
			techJkServiceTaskItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						var store=techJkServiceTaskItemGrid.store;
						for(var a=0;a<store.getCount();a++){
							if(obj.get("id")==store.getAt(a).get("sampleInfoIn-id")){
								message(biolims.common.haveDuplicate);
								return;
							}
						}
						p.set("code", obj.get("code"));
						p.set("sampleCode", obj.get("sampleCode"));
						p.set("sampleType", obj.get("sampleType"));
						p.set("productId", obj.get("sampleInfo-productId"));
						p.set("productName", obj.get("sampleInfo-productName"));
						p.set("project-id",obj.get("sampleInfo-project-id"));
						p.set("project-name",obj.get("sampleInfo-project-name"));
						p.set("name",obj.get("sampleInfo-patientName"));
						p.set("location", obj.get("location"));
						p.set("concentration", obj.get("concentration"));
						p.set("volume", obj.get("volume"));
						p.set("sampleNum", obj.get("sumTotal"));
						p.set("tempId", obj.get("id"));
						p.set("sampleInfoIn-id", obj.get("id"));
						p.set("crmCustomer-id",obj.get("customer-id"));
						p.set("crmCustomer-name",obj.get("customer-name"));
						p.set("crmDoctor-id",obj.get("sampleInfo-crmDoctor-id"));
						p.set("crmDoctor-name",obj.get("sampleInfo-crmDoctor-name"));
						p.set("state", "1");
						p.set("inwardCode",obj.get("tjItem-inwardCode"));
						p.set("orderId",obj.get("tjItem-orderId"));
						p.set("groupNo", obj.get("sampleInfo-name"));
						p.set("barCode", obj.get("barCode"));
						p.set("yunKangCode", obj.get("yunKangCode"));
						p.set("dataTraffic", obj.get("dataTraffic"));
						techJkServiceTaskItemGrid.getStore().add(p);
				});
				techJkServiceTaskItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}
//待生信样本
var logsa;
function selectFourType(){ 
	
	var projectId =  $("#techJkServiceTask_projectId").val();
	if(projectId==null || projectId==""){
		message(biolims.common.pleaseSelectProduct);
		return;
	}
	var title = '';
	var url = '';
	title = biolims.sample.selectInventorySamples;
	url = "/analysis/desequencing/deSequencingItem/deSequencingItemSelect.action?projectId="+projectId+"";
	var option = {};
	option.width = document.body.clientWidth-50;
	option.height = document.body.clientHeight-50;
	logsa = loadDialogPage(null, title, url, {
		"Confirm" : function() {
			var operGrid = $("#show_dialog_deSequencingItem_div").data("deSequencingItemDialogGrid");
			var ob = techJkServiceTaskItemGrid.getStore().recordType;
			techJkServiceTaskItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
//						var store=techJkServiceTaskItemGrid.store;
//						for(var a=0;a<store.getCount();a++){
//							if(obj.get("id")==store.getAt(a).get("sampleInfoIn-id")){
//								message(biolims.common.haveDuplicate);
//								return;
//							}
//						}
						p.set("orderCode", obj.get("orderCode"));
						p.set("projectId", obj.get("projectId"));
						p.set("sampleID", obj.get("sampleID"));
						
						p.set("sxProductName", obj.get("productName"));
						p.set("runId", obj.get("runId"));
//						p.set("laneId", obj.get("laneId"));
						
						p.set("tempId", obj.get("id"));
						p.set("state", "1");
						
						techJkServiceTaskItemGrid.getStore().add(p);
				});
				techJkServiceTaskItemGrid.startEditing(0, 0);
				logsa.dialog("close");
				logsa.dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}


//待上机样本
function selectThreeType(){ 
	var title = '';
	var url = '';
	title = biolims.sample.selectInventorySamples;
	url = "/sample/storage/sampleIn/showSampleWaitOutItemList.action?type=3";//type=3.则完成文库构建等待上机。
	var option = {};
	option.width = document.body.clientWidth-50;
	option.height = document.body.clientHeight-50;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			var operGrid = $("#sampleWaitOutItemdiv").data("sampleWaitOutItemGrid");
			var ob = techJkServiceTaskItemGrid.getStore().recordType;
			techJkServiceTaskItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						var store=techJkServiceTaskItemGrid.store;
						for(var a=0;a<store.getCount();a++){
							if(obj.get("id")==store.getAt(a).get("sampleInfoIn-id")){
								message(biolims.common.haveDuplicate);
								return;
							}
						}
						p.set("code", obj.get("code"));
						p.set("sampleCode", obj.get("sampleCode"));
						p.set("sampleType", obj.get("sampleType"));
						p.set("productId", obj.get("sampleInfo-productId"));
						p.set("productName", obj.get("sampleInfo-productName"));
						p.set("project-id",obj.get("sampleInfo-project-id"));
						p.set("project-name",obj.get("sampleInfo-project-name"));
						p.set("name",obj.get("sampleInfo-patientName"));
						p.set("location", obj.get("location"));
						p.set("concentration", obj.get("concentration"));
						p.set("volume", obj.get("volume"));
						p.set("sampleNum", obj.get("sumTotal"));
						p.set("tempId", obj.get("id"));
						p.set("sampleInfoIn-id", obj.get("id"));
						p.set("crmCustomer-id",obj.get("customer-id"));
						p.set("crmCustomer-name",obj.get("customer-name"));
						p.set("crmDoctor-id",obj.get("sampleInfo-crmDoctor-id"));
						p.set("crmDoctor-name",obj.get("sampleInfo-crmDoctor-name"));
						p.set("state", "1");
						p.set("inwardCode",obj.get("tjItem-inwardCode"));
						p.set("orderId",obj.get("tjItem-orderId"));
						p.set("groupNo", obj.get("sampleInfo-name"));
						p.set("barCode", obj.get("barCode"));
						p.set("yunKangCode", obj.get("yunKangCode"));
						p.set("indexa", obj.get("indexa"));
						p.set("dataTraffic", obj.get("dataTraffic"));
						techJkServiceTaskItemGrid.getStore().add(p);
				});
				techJkServiceTaskItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}






//库存样本
//var loadSampleIn;
function selectsampleInItem(){ 
	
	var title = '';
	var url = '';
	title = biolims.sample.selectInventorySamples;
	url = "/sample/storage/sampleIn/showSampleWaitOutItemList.action";
	var option = {};
	option.width = document.body.clientWidth-50;
	option.height = document.body.clientHeight-50;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			var operGrid = $("#sampleWaitOutItemdiv").data("sampleWaitOutItemGrid");
			var ob = techJkServiceTaskItemGrid.getStore().recordType;
			techJkServiceTaskItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						var store=techJkServiceTaskItemGrid.store;
						for(var a=0;a<store.getCount();a++){
							if(store.getAt(a).get("tempId")==obj.get("id")){
								message(biolims.common.haveDuplicate);
								return;
							}
						}
						p.set("code", obj.get("code"));
						p.set("sampleCode", obj.get("sampleCode"));
						p.set("sampleType", obj.get("sampleType"));
						p.set("productId", obj.get("sampleInfo-productId"));
						p.set("productName", obj.get("sampleInfo-productName"));
						p.set("project-id",obj.get("sampleInfo-project-id"));
						p.set("project-name",obj.get("sampleInfo-project-name"));
						p.set("name",obj.get("sampleInfo-patientName"));
						p.set("location", obj.get("location"));
						p.set("concentration", obj.get("concentration"));
						p.set("volume", obj.get("volume"));
						p.set("sampleNum", obj.get("sumTotal"));
						p.set("tempId", obj.get("id"));
						p.set("sampleInfoIn-id", obj.get("id"));
						p.set("crmCustomer-id",obj.get("customer-id"));
						p.set("crmCustomer-name",obj.get("customer-name"));
						p.set("crmDoctor-id",obj.get("sampleInfo-crmDoctor-id"));
						p.set("crmDoctor-name",obj.get("sampleInfo-crmDoctor-name"));
						p.set("state", "1");
						p.set("inwardCode",obj.get("tjItem-inwardCode"));
						p.set("orderId",obj.get("tjItem-orderId"));
						p.set("groupNo", obj.get("sampleInfo-name"));
						p.set("barCode", obj.get("barCode"));
						p.set("yunKangCode", obj.get("yunKangCode"));
						p.set("indexa", obj.get("indexa"));
						p.set("dataTraffic", obj.get("dataTraffic"));
						techJkServiceTaskItemGrid.getStore().add(p);
				});
				techJkServiceTaskItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}



//下达样本
function selectItemDialog(){ 
	var title = '';
	var url = '';
	title = "";
	url = "/technology/dna/techDnaServiceTask/showItemDialogList.action";
	var option = {};
	option.width = document.body.clientWidth-50;
	option.height = document.body.clientHeight-50;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			var operGrid = showItemDialogGrid;
			var ob = techJkServiceTaskItemGrid.getStore().recordType;
			techJkServiceTaskItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						var store=techJkServiceTaskItemGrid.store;
						for(var a=0;a<store.getCount();a++){
							if(store.getAt(a).get("tempId")==obj.get("id")){
								message(biolims.common.haveDuplicate);
								return;
							}
						}
						p.set("name", obj.get("sampleInfoIn-sampleInfo-patientName"));
						p.set("code", obj.get("sampleInfoIn-code"));
						p.set("sampleCode", obj.get("sampleCode"));
						p.set("sampleType", obj.get("sampleType"));
						p.set("productId", obj.get("sampleInfoIn-sampleInfo-productId"));
						p.set("productName", obj.get("sampleInfoIn-sampleInfo-productName"));
						p.set("project-id",obj.get("sampleInfoIn-sampleInfo-project-id"));
						p.set("project-name",obj.get("sampleInfoIn-sampleInfo-project-name"));
						p.set("location", obj.get("sampleInfoIn-location"));
						p.set("concentration", obj.get("sampleInfoIn-concentration"));
						p.set("volume", obj.get("sampleInfoIn-volume"));
						p.set("sampleNum", obj.get("sampleInfoIn-sumTotal"));
						p.set("sellPerson-id", obj.get("sampleInfoIn-sellPerson-id"));
						p.set("sellPerson-name", obj.get("sampleInfoIn-sellPerson-name"));
						p.set("tempId", obj.get("id"));
						p.set("sampleInfoIn-id", obj.get("sampleInfoIn-id"));
						p.set("crmCustomer-id",obj.get("sampleInfoIn-customer-id"));
						p.set("crmCustomer-name",obj.get("sampleInfoIn-customer-name"));
						p.set("crmDoctor-id",obj.get("sampleInfoIn-sampleInfo-crmDoctor-id"));
						p.set("crmDoctor-name",obj.get("sampleInfoIn-sampleInfo-crmDoctor-name"));
						p.set("species", obj.get("sampleInfoIn-sampleInfo-species"));
						p.set("state", "1");
						p.set("project-id", obj.get("project-id"));
						p.set("project-name", obj.get("project-name"));
						p.set("orderId", obj.get("orderId"));
						p.set("groupNo", obj.get("groupNo"));
						p.set("barCode", obj.get("barCode"));
						p.set("yunKangCode", obj.get("yunKangCode"));
						p.set("dataTraffic", obj.get("dataTraffic"));
						techJkServiceTaskItemGrid.getStore().add(p);
				});
				techJkServiceTaskItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}



//科研项目
function projectFun() {
	var win = Ext.getCmp('projectFun');
	if (win) {
		win.close();
	}
	var projectFun = new Ext.Window(
			{
				id : 'projectFun',
				modal : true,
				title : biolims.common.selectProduct,
				layout : 'fit',
				width : 600,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/crm/project/showProjectSelectList.action?flag=ProjectFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						projectFun.close();
					}
				} ]
			});
	projectFun.show();
}

function setProjectFun(rec) {
	var selRecords = techJkServiceTaskItemGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('project-id',rec.get("id"));
		obj.set('project-name',rec.get("name"));
	});
	var win = Ext.getCmp('projectFun');
	if (win) {
		win.close();
	}
	
}