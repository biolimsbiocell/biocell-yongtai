/* 
 * 文件名称 :sampleReceiveEdit.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/01/30
 * 文件描述: 样本接收的相关页面函数
 * 
 */

var flg = true;
$(function() {
	//如果状态完成,只显示右侧表
	if($("#headStateName").text()=="完成"){
		$("#leftDiv").hide();
		$("#rightDiv").removeClass().addClass("col-md-12 col-xs-12");
	}
	$("#techJkServiceTask_type").val("0");
	//接收样本了类型的名称
//	var sampleTypeTitle = $("#techJkServiceTaskTitle").attr("type");
	var sampleTypeTitle = "0";
	if(sampleTypeTitle == "0") {
//		$("#techJkServiceTaskTitle").text(biolims.common.qcTask );
		$(".zjrwd").show();
		$(".jkrwd").hide();
		$(".sjrwd").hide();
		$(".sjrwd").hide();
		$("#cxxx").hide();
		$("#sxfx").hide();
//		$("#techJkServiceTask_typeName").val(biolims.common.qcTask );
	}
	if(sampleTypeTitle == "1") {
		$("#techJkServiceTaskTitle").text(biolims.common.libraryTask );
		$(".zjrwd").hide();
		$(".jkrwd").show();
		$(".sjrwd").hide();
		$(".sjrwd").hide();
		$("#cxxx").show();
		$("#sxfx").hide();
		$("#techJkServiceTask_typeName").val(biolims.common.libraryTask );
	}
	if(sampleTypeTitle == "2") {
		$("#techJkServiceTaskTitle").text(biolims.common.sequenceTask);
		$(".zjrwd").hide();
		$(".jkrwd").hide();
		$(".sjrwd").show();
		$(".sjrwd").hide();
		$("#cxxx").hide();
		$("#sxfx").hide();
		$("#techJkServiceTask_typeName").val(biolims.common.sequenceTask);
	}
	if(sampleTypeTitle == "3") {
		$("#techJkServiceTaskTitle").text(biolims.common.bioinfoTask);
		$(".zjrwd").hide();
		$(".jkrwd").hide();
		$(".sjrwd").hide();
		$(".sjrwd").show();
		$("#cxxx").hide();
		$("#sxfx").show();
		$("#techJkServiceTask_typeName").val(biolims.common.bioinfoTask);
	}
//	cxTypeChange();
	if($("#techJkServiceTask_id").text() == "NEW") {
		$("#techJkServiceTaskModal").modal("show");
		//选择 新建任务单类型
		choseClinicalOrScientific();
		bpmTask($("#bpmTaskId").val());
		var handlemethod = $("#handlemethod").val();
		if(handlemethod == "view"||$("#techJkServiceTask_state").text()==biolims.common.finish){
			settextreadonly();
			$("#save").hide();
			$("#changeState").hide();
		}
	}else{
		bpmTask($("#bpmTaskId").val());
		var handlemethod = $("#handlemethod").val();
		if(handlemethod == "view"||$("#techJkServiceTask_state").text()=="Complete"){
			settextreadonly();
			$("#save").hide();
			$("#changeState").hide();
		}
	}
	//日期格式化
	$("#techJkServiceTask_sequenceBillDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
//	renderDatatables(flg);
	//批量结果的按钮变为下拉框
//	btnChangeDropdown($('#main'), $(".resultsBatchBtn"), [biolims.common.qualified, biolims.common.disqualified], "method");
//	//// 上传附件
//	fileInput('1', 'sampleReceive', $("#sampleReveice_id").text());
});
function cxTypeChange(){
	var type=$("#techJkServiceTask_cxType").val();
	if(type=="1"){
		$(".cx1").hide();
		$(".cx2").hide();
		$(".cx3").hide();
	}else if(type=="2"||type=="3"){
		$(".cx1").show();
		$(".cx2").hide();
		$(".cx3").hide();
	}else if(type=="4"){
		$(".cx1").hide();
		$(".cx2").show();
		$(".cx3").hide();
	}else if(type=="5"||type=="6"){
		$(".cx1").hide();
		$(".cx2").hide();
		$(".cx3").show();
	}else{
		$(".cx1").hide();
		$(".cx2").hide();
		$(".cx3").hide();
	}
}
//选择临床样本或者科研样本
function choseClinicalOrScientific() {
	//扫码、上传、新建的不同流向
	$("#modal-body .col-xs-12").click(function() {
		var index = $(this).index();
		//建库
		if(index == 1) {
			$("#techJkServiceTaskModal").modal("hide");
			$("#techJkServiceTaskTitle").text(biolims.common.libraryTask);
			$(".jkrwd").show();
			$(".zjrwd").hide();
			$(".sjrwd").hide();
			$(".sxrwd").hide();
			$("#cxxx").show();
			$("#sxfx").hide();
			$("#techJkServiceTask_type").val("1");
			$("#techJkServiceTask_typeName").val(biolims.common.libraryTask);
		}
		//质检
		if(index == 0) {
			$("#techJkServiceTaskModal").modal("hide");
			$("#techJkServiceTaskTitle").text(biolims.common.qcTask);
			$(".jkrwd").hide();
			$(".zjrwd").show();
			$(".sjrwd").hide();
			$(".sxrwd").hide();
			$("#cxxx").hide();
			$("#sxfx").hide();
			$("#techJkServiceTask_type").val("0");
			$("#techJkServiceTask_typeName").val(biolims.common.qcTask);
		}
		//上机
		if(index == 2) {
			$("#techJkServiceTaskModal").modal("hide");
			$("#techJkServiceTaskTitle").text(biolims.common.sequenceTask);
			$(".jkrwd").hide();
			$(".zjrwd").hide();
			$(".sjrwd").show();
			$(".sxrwd").hide();
			$("#cxxx").hide();
			$("#sxfx").hide();
			$("#techJkServiceTask_type").val("2");
			$("#techJkServiceTask_typeName").val(biolims.common.sequenceTask);
		}
		//生信
		if(index == 3) {
			$("#techJkServiceTaskModal").modal("hide");
			$("#techJkServiceTaskTitle").text(biolims.common.bioinfoTask);
			$(".jkrwd").hide();
			$(".zjrwd").hide();
			$(".sjrwd").hide();
			$(".sxrwd").show();
			$("#cxxx").hide();
			$("#sxfx").show();
			$("#techJkServiceTask_type").val("3");
			$("#techJkServiceTask_typeName").val(biolims.common.bioinfoTask);
		}
		//不管选什么都要刷新左侧和右侧列表
		changeTables();
		changeright();
	});
}

function xiangmu() {
	top.layer.open({
		title: biolims.crm.selectContract,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/crm/project/showProjectSelectNewList.action", ''],
		yes: function(index, layero) {
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(
				0).text();
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(1).text();
			$("#techJkServiceTask_projectId_id").val(id);
			$("#techJkServiceTask_projectId_name").val(name);
			top.layer.close(index)
		},
	})
}

function shiyanzu() {
	top.layer.open({
		title: biolims.common.selectGroup,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/core/userGroup/userGroupSelTable.action", ''],
		yes: function(index, layero) {
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(
				0).text();
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(1).text();
			$("#techJkServiceTask_acceptUser_id").val(id);
			$("#techJkServiceTask_acceptUser_name").val(name);
			top.layer.close(index)
		},
	})
}

function shiyanyuan() {
	var rows = $("#techJkServiceTask_acceptUser_id").val();
	if(rows==""||rows==null||rows==undefined) {
		top.layer.msg(biolims.common.pleaseSelectGroup);
		return false;
	}
	top.layer.open({
		title:biolims.common.chooseTester,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId="+$("#techJkServiceTask_acceptUser_id").val(), ''],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			$("#techJkServiceTask_testUserOneId").val(id);
			$("#techJkServiceTask_testUserOneName").val(name);
			top.layer.close(index)
		},
	})
}

function showInstrument() {
	top.layer.open({
		title: biolims.common.selectInstrument,
		type: 2,
		offset: ['10%', '10%'],
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/equipment/main/selectCos.action", ""],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addCos .chosed").children("td")
				.eq(1).text();
			var code = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addCos .chosed").children("td").eq(
				0).text();
			$("#techJkServiceTask_instrument_id").val(code);
			$("#techJkServiceTask_instrument_name").val(name);
			top.layer.close(index)
		},
	});
}

