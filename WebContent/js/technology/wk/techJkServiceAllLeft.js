/*//待入库样本-即库存主数据(树状带批次)
var techDnaServiceAllTab;
hideLeftDiv();
$(function() {
	$("#techJkServiceTask_type").val("0");
	if($("#techJkServiceTask_type").val()=="1"){
		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.sample.id,
			"visible": false
		});
		colOpts.push({
			"data" : "code",
			"title" : biolims.common.code
		});
		
		colOpts.push({
			"data": "barCode",
			"title": biolims.common.barCode
		});
		
		colOpts.push({
			"data" : "sampleCode",
			"title" : biolims.common.sampleCode,
			"visible": false
		});
		
		colOpts.push({
			"data" : "dataTraffic",
			"title" : biolims.common.throughput,
		});
		
		
		
		colOpts.push({
			"data" : "productId",
			"title" : biolims.common.productId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "productName",
			"title" : biolims.common.testProject,
		});
		
		colOpts.push({
			"data" : "samplingDate",
			"title" : biolims.sample.samplingDate,
		});
		
		colOpts.push({
			"data" : "sampleInfo-idCard",
			"title" : biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "location",
			"title" : biolims.common.location,
		});
		
		colOpts.push({
			"data" : "qpcrConcentration",
			"title" : biolims.QPCR.concentration,
		});
		
		colOpts.push({
			"data" : "indexa",
			"title" : 'index',
		});
		
		colOpts.push({
			"data" : "concentration",
			"title" : biolims.common.concentration,
		});
		
		colOpts.push({
			"data" : "volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfo-receiveDate",
			"title" : biolims.common.receiveDate,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-id",
			"title" : biolims.common.contractCodeId,
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-name",
			"title" : biolims.common.contractName,
		});
		
		colOpts.push({
			"data" : "sellPerson-id",
			"title" : biolims.common.salesId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sellPerson-name",
			"title" : biolims.common.sales,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-id",
			"title" : biolims.common.customId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-name",
			"title" : biolims.common.custom,
		});
		colOpts.push({
			"data" : "scopeId",
			"title" : biolims.purchase.costCenterId,
		});
		
		colOpts.push({
			"data" : "scopeName",
			"title" : biolims.purchase.costCenter,
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-id",
			"title" : biolims.sample.crmDoctorId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-name",
			"title" : biolims.sample.crmDoctorName,
		});
		
		colOpts.push({
			"data": "isRp",
			"title": biolims.common.ifThereStain,
			"name": biolims.common.no+"|"+biolims.common.yes,
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return biolims.common.no;
				}
				if(data == "1") {
					return biolims.common.yes;
				}else {
					return '';
				}
			}
		});
		
		colOpts.push({
			"data" : "note",
			"title" : biolims.common.note,
		});
		
		colOpts.push({
			"data" : "techJkServiceTask-id",
			"title" : biolims.common.qcTask,
		});
		
		colOpts.push({
			"data" : "sampleInfo-name",
			"title" : biolims.common.packetNumber,
		});
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		//添加明细按钮
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});

				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, "",
				"/sample/storage/sampleIn/showSampleWaitOutItemListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	}else if($("#techJkServiceTask_type").val()=="0"){
		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.common.id,
			"visible": false
		});
		colOpts.push({
			"data" : "sampleInfoIn-code",
			"title" : biolims.common.code,
		});
		
		colOpts.push({
			"data": "dataTraffic",
			"title": biolims.common.throughput,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-idCard",
			"title" :biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "groupNo",
			"title" : biolims.common.SampleCollectionPacketNumber,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "project-id",
			"title" : biolims.common.contractId,
		});
		
		colOpts.push({
			"data" : "project-name",
			"title" : biolims.common.contractName+"1111",
		});
		colOpts.push({
			"data" : "scopeId",
			"title" : biolims.purchase.costCenterId,
		});
		
		colOpts.push({
			"data" : "scopeName",
			"title" : biolims.purchase.costCenter,
		});
		
		colOpts.push({
			"data" : "orderId",
			"title" : biolims.sample.orderNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-concentration",
			"title" : biolims.common.concentration,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-customer-name",
			"title" : biolims.sample.customerName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-crmDoctor-name",
			"title" : biolims.common.customName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-species",
			"title" : biolims.wk.species,
		});
		
		colOpts.push({
			"data" : "sampleOrder-id",
			"title" : "关联订单",
		});
		
		
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});

				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, "",
				"/technology/dna/techDnaServiceTask/showItemDialogNewListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	}else if($("#techJkServiceTask_type").val()=="2"){

		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.sample.id,
			"visible": false
		});
		colOpts.push({
			"data" : "code",
			"title" : biolims.common.code
		});
		
		colOpts.push({
			"data": "barCode",
			"title": biolims.common.barCode
		});
		
		colOpts.push({
			"data" : "sampleCode",
			"title" : biolims.common.sampleCode,
			"visible": false
		});
		
		colOpts.push({
			"data" : "dataTraffic",
			"title" : biolims.common.throughput,
		});
		
		
		
		colOpts.push({
			"data" : "productId",
			"title" : biolims.common.productId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "productName",
			"title" : biolims.common.testProject,
		});
		
		colOpts.push({
			"data" : "scopeId",
			"title" : biolims.purchase.costCenterId,
//			"visible": false
		});
		
		colOpts.push({
			"data" : "scopeName",
			"title" : biolims.purchase.costCenter,
//			"visible": false
		});
		
		colOpts.push({
			"data" : "samplingDate",
			"title" : biolims.sample.samplingDate,
		});
		
		colOpts.push({
			"data" : "sampleInfo-idCard",
			"title" : biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "location",
			"title" : biolims.common.location,
		});
		
		colOpts.push({
			"data" : "qpcrConcentration",
			"title" : biolims.QPCR.concentration,
		});
		
		colOpts.push({
			"data" : "indexa",
			"title" : 'index',
		});
		
		colOpts.push({
			"data" : "concentration",
			"title" : biolims.common.concentration,
		});
		
		colOpts.push({
			"data" : "volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfo-receiveDate",
			"title" : biolims.common.receiveDate,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-id",
			"title" : biolims.common.contractCodeId,
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-name",
			"title" : biolims.common.contractName,
		});
		
		colOpts.push({
			"data" : "sellPerson-id",
			"title" : biolims.common.salesId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sellPerson-name",
			"title" : biolims.common.sales,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-id",
			"title" : biolims.common.customId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-name",
			"title" : biolims.common.custom,
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-id",
			"title" : biolims.sample.crmDoctorId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-name",
			"title" : biolims.sample.crmDoctorName,
		});
		
		colOpts.push({
			"data": "isRp",
			"title": biolims.common.ifThereStain,
//			"className": "select",
			"name": biolims.common.no+"|"+biolims.common.yes,
//			"createdCell": function(td) {
//				$(td).attr("saveName", "isRp");
//				$(td).attr("selectOpt", biolims.common.no+"|"+biolims.common.yes);
//			},
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return biolims.common.no;
				}
				if(data == "1") {
					return biolims.common.yes;
				}else {
					return '';
				}
			}
		});
		
		colOpts.push({
			"data" : "note",
			"title" : biolims.common.note,
		});
		
		colOpts.push({
			"data" : "techJkServiceTask-id",
			"title" : biolims.common.qcTask,
		});
		
		colOpts.push({
			"data" : "sampleInfo-name",
			"title" : biolims.common.packetNumber,
		});
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});
	
				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, "",
				"/sample/storage/sampleIn/showSampleWaitOutItemListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	}else if($("#techJkServiceTask_type").val()=="3"){

		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.sample.id,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleCode",
			"title" : "样本编号",
		});
		
		colOpts.push({
			"data" : "orderCode",
			"title" : "订单编号",
		});
		
		colOpts.push({
			"data": "projectId",
			"title": biolims.common.projectId,
		});
		
		colOpts.push({
			"data" : "sampleID",
			"title" : biolims.wk.wkCode,
		});
		
		colOpts.push({
			"data" : "productName",
			"title" : biolims.common.productName,
		});
		colOpts.push({
			"data" : "scopeId",
			"title" : biolims.purchase.costCenterId,
//			"visible": false
		});
		
		colOpts.push({
			"data" : "scopeName",
			"title" : biolims.purchase.costCenter,
//			"visible": false
		});
		
		
		
		colOpts.push({
			"data" : "runId",
			"title" : "RUN_ID",
		});
		
		colOpts.push({
			"data" : "gcContent",
			"title" : biolims.analysis.gcContent,
		});
		
		colOpts.push({
			"data" : "nContent",
			"title" : biolims.analysis.nContent,
		});
		
		colOpts.push({
			"data" : "qTwentyPCT",
			"title" : biolims.analysis.qTwentyPCT,
		});
		
		colOpts.push({
			"data" : "qThirtyPCT",
			"title" : biolims.analysis.qThirtyPCT,
		});
		
		colOpts.push({
			"data" : "ratioOfLane",
			"title" : biolims.analysis.ratioOfLane,
		});
		
		colOpts.push({
			"data" : "raw_reads",
			"title" : biolims.analysis.raw_reads,
		});
		
		colOpts.push({
			"data" : "cleanReads",
			"title" : biolims.analysis.cleanReads,
		});
		
		colOpts.push({
			"data" : "ratioOfReads",
			"title" : biolims.analysis.ratioOfReads,
		});
		
		colOpts.push({
			"data" : "raw_size",
			"title" : biolims.analysis.raw_size,
		});
		
		colOpts.push({
			"data" : "cleanBases",
			"title" : biolims.analysis.cleanBases,
		});
		
		colOpts.push({
			"data" : "ratioOfBases",
			"title" : biolims.analysis.ratioOfBases,
		});
		
		colOpts.push({
			"data" : "hq_reads",
			"title" :biolims.master.hq_reads,
		});
		
		colOpts.push({
			"data" : "medianInsert",
			"title" : biolims.analysis.medianInsert,
		});
		
		colOpts.push({
			"data" : "duplicate",
			"title" : biolims.analysis.duplicate,
		});
		
		colOpts.push({
			"data" : "onTarget_raw",
			"title" : biolims.analysis.onTarget_raw,
		});
		
		colOpts.push({
			"data" : "onTarget_add",
			"title" : biolims.analysis.onTarget_add,
		});
		
		colOpts.push({
			"data" : "mappingPct",
			"title" : biolims.analysis.mappingPct,
		});
		
		colOpts.push({
			"data" : "oriDepth",
			"title" : biolims.analysis.oriDepth,
		});
		
		colOpts.push({
			"data" : "oneXcoverage",
			"title" : biolims.analysis.oneXcoverage,
		});
		
		colOpts.push({
			"data" : "tenXcoverage",
			"title" : biolims.analysis.tenXcoverage,
		});
		
		colOpts.push({
			"data" : "twentyXcoverage",
			"title" : biolims.analysis.twentyXcoverage,
		});
		
		colOpts.push({
			"data" : "fiftyXcoverage",
			"title" : biolims.analysis.fiftyXcoverage,
		});
		colOpts.push({
			"data" : "twentyPctMeancoverage",
			"title" : biolims.analysis.twentyPctMeancoverage,
		});
		colOpts.push({
			"data" : "meanDepthDedup",
			"title" : biolims.analysis.meanDepthDedup,
		});
		colOpts.push({
			"data" : "oneCoverageDedup",
			"title" : biolims.analysis.oneCoverageDedup,
		});
		colOpts.push({
			"data" : "tenCoverageDedup",
			"title" : biolims.analysis.tenCoverageDedup,
		});
		
		colOpts.push({
			"data" : "twentyCoverageDedup",
			"title" : biolims.analysis.twentyCoverageDedup,
		});
		
		colOpts.push({
			"data" : "fiftyCoverageDedup",
			"title" : biolims.analysis.fiftyCoverageDedup,
		});
		
		colOpts.push({
			"data" : "twentyMeanCoverageDedup",
			"title" : biolims.analysis.twentyMeanCoverageDedup,
		});
		
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});
	
				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, $("#techJkServiceTask_projectId").val(),
				"/analysis/desequencing/deSequencingItem/showDialogDeSequencingItemNewListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	
	}else{

		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.common.id,
			"visible": false
		});
		colOpts.push({
			"data" : "sampleInfoIn-code",
			"title" : biolims.common.code,
		});
		
		colOpts.push({
			"data": "dataTraffic",
			"title": biolims.common.throughput,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-idCard",
			"title" :biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "groupNo",
			"title" : biolims.common.SampleCollectionPacketNumber,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "project-id",
			"title" : biolims.common.contractId,
		});
		
		colOpts.push({
			"data" : "project-name",
			"title" : biolims.common.contractName,
		});
		colOpts.push({
			"data" : "scopeId",
			"title" : biolims.purchase.costCenterId,
//			"visible": false
		});
		
		colOpts.push({
			"data" : "scopeName",
			"title" : biolims.purchase.costCenter,
//			"visible": false
		});
		colOpts.push({
			"data" : "orderId",
			"title" : biolims.sample.orderNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-concentration",
			"title" : biolims.common.concentration,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-customer-name",
			"title" : biolims.sample.customerName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-crmDoctor-name",
			"title" : biolims.common.customName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-species",
			"title" : biolims.wk.species,
		});
		
		
		var tbarOpts = [];

		var storageOutAllOps = table(true, "",
				"/technology/dna/techDnaServiceTask/showItemDialogNewListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	
	}
	
//	bpmTask($("#bpmTaskId").val());
});



//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.custom,
		"type" : "input",
		"searchName" : "sampleInfo-crmDoctor-name",
	},{
		"txt" : biolims.common.outCode,
		"type" : "input",
		"searchName" : "sampleInfo-idCard",
	},{
		"txt" : biolims.common.custom+biolims.common.unit,
		"type" : "input",
		"searchName" : "sampleInfo-crmCustomer-name",
	},{
		"txt" : biolims.common.qcTask,
		"type" : "input",
		"searchName" : "techJkServiceTask-id",
	}, {
		"type" : "table",
		"table" : techDnaServiceAllTab
	} ];
}

//大保存
function save() {
		var changeLog = "任务单：";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
	    //dwb 2018-05-11 11:45:00
		var jsonStr = JSON.stringify($("#form1").serializeObject());
		var dataItemJson = saveItemjson($("#storageOutItemdiv"));
		var ele = $("#storageOutItemdiv");
		var changeLogItemf = biolims.common.claimSampleTask+"：";
		var type = $("#techJkServiceTask_type").val();
		if(type=="0"){//质检任务单
			changeLogItemf += "质检任务单-";
		}else if(type=="1"){//建库任务单
			changeLogItemf += "建库任务单-";
		}else if(type=="2"){//上机任务单
			changeLogItemf += "上机任务单-";
		}else if(type=="3"){//生信任务单
			changeLogItemf += "生信任务单-";
		}
		var changeLogItem = getChangeLog(dataItemJson, ele, changeLogItemf);
		var changeLogItems = "";
		if(changeLogItem != changeLogItemf){
			changeLogItems = changeLogItem;
		}
		var name = $("#techJkServiceTask_name").val();
		var note = $("#techJkServiceTask_note").val();
		var createUser = $("#techJkServiceTask_createUser").attr("userId");
		var createDate = $("#techJkServiceTask_createDate").text();
		var state = $("#headStateName").attr("state");
		var stateName = $("#headStateName").text();
		var id = $("#techJkServiceTask_id").text();
		//必填验证
		var requiredField=requiredFilter();
			if(!requiredField){
				return false;
			}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
			url: ctx + '/technology/wk/techJkServiceTask/saveStorageOutAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItems,
				id:id,
				note:note,
				name:name,
				createUser:createUser,
				createDate:createDate,
				state:state,
				stateName:stateName
			},
			success: function(data) {
				if(data.success) {
					top.layer.closeAll();
					var url = "/technology/wk/techJkServiceTask/editTechJkServiceTask.action?id=" + data.id;

//					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.closeAll();
					top.layer.msg(data.msg);
				}
			}
		});
}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

function changeState() {
    var	id=$("#techJkServiceTask_id").text();
	var paraStr = "formId=" + id +
		"&tableId=TechJkServiceTask";
	console.log(paraStr);
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layero) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();

			})

		}
	});

	
}

var mainFileInput;
function fileUp() {
	if($("#techJkServiceTask_id").text()=="NEW"){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('1', 'techJkServiceTask', $("#techJkServiceTask_id").text());
	mainFileInput.off("fileuploaded");
}
function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=techJkServiceTask&id=" + $("#techJkServiceTask_id").text(),
		cancel: function(index, layero) {
			top.layer.close(index);
		}
	});
}
function fileUp2() {
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('2', 'techJkServiceTask', $("#techJkServiceTask_id").text());
	mainFileInput.off("fileuploaded");
}
function fileView2() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		content: window.ctx + "/operfile/initFileList.action?flag=2&modelType=techJkServiceTask&id=" + $("#techJkServiceTask_id").text(),
		cancel: function(index, layero) {
			top.layer.close(index);
		}
	});
}

function changeTables(){
	$("#lefttable").html('');
	$("#lefttable").html('<table '+
			' class="table table-hover table-striped table-bordered table-condensed" '+
				' id="storageOutAlldiv" style="font-size: 14px;"></table>');


	if($("#techJkServiceTask_type").val()=="1"){
		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.sample.id,
			"visible": false
		});
		colOpts.push({
			"data" : "code",
			"title" : biolims.common.code
		});
		
		colOpts.push({
			"data": "barCode",
			"title": biolims.common.barCode
		});
		
		colOpts.push({
			"data" : "sampleCode",
			"title" : biolims.common.sampleCode,
			"visible": false
		});
		
		colOpts.push({
			"data" : "dataTraffic",
			"title" : biolims.common.throughput,
		});
		
		
		
		colOpts.push({
			"data" : "productId",
			"title" : biolims.common.productId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "productName",
			"title" : biolims.common.testProject,
		});
		
		colOpts.push({
			"data" : "samplingDate",
			"title" : biolims.sample.samplingDate,
		});
		
		colOpts.push({
			"data" : "sampleInfo-idCard",
			"title" : biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "location",
			"title" : biolims.common.location,
		});
		
		colOpts.push({
			"data" : "qpcrConcentration",
			"title" : biolims.QPCR.concentration,
		});
		
		colOpts.push({
			"data" : "indexa",
			"title" : 'index',
		});
		
		colOpts.push({
			"data" : "concentration",
			"title" : biolims.common.concentration,
		});
		
		colOpts.push({
			"data" : "volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfo-receiveDate",
			"title" : biolims.common.receiveDate,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-id",
			"title" : biolims.common.contractCodeId,
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-name",
			"title" : biolims.common.contractName,
		});
		
		colOpts.push({
			"data" : "sellPerson-id",
			"title" : biolims.common.salesId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sellPerson-name",
			"title" : biolims.common.sales,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-id",
			"title" : biolims.common.customId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-name",
			"title" : biolims.common.custom,
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-id",
			"title" : biolims.sample.crmDoctorId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-name",
			"title" : biolims.sample.crmDoctorName,
		});
		
		colOpts.push({
			"data": "isRp",
			"title": biolims.common.ifThereStain,
			"name": biolims.common.no+"|"+biolims.common.yes,
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return biolims.common.no;
				}
				if(data == "1") {
					return biolims.common.yes;
				}else {
					return '';
				}
			}
		});
		
		colOpts.push({
			"data" : "note",
			"title" : biolims.common.note,
		});
		
		colOpts.push({
			"data" : "techJkServiceTask-id",
			"title" : biolims.common.qcTask,
		});
		
		colOpts.push({
			"data" : "sampleInfo-name",
			"title" : biolims.common.packetNumber,
		});
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});

				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, "",
				"/sample/storage/sampleIn/showSampleWaitOutItemListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	}else if($("#techJkServiceTask_type").val()=="0"){
		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.common.id,
			"visible": false
		});
		colOpts.push({
			"data" : "sampleInfoIn-code",
			"title" : biolims.common.code,
		});
		
		colOpts.push({
			"data": "dataTraffic",
			"title": biolims.common.throughput,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-idCard",
			"title" :biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "groupNo",
			"title" : biolims.common.SampleCollectionPacketNumber,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "project-id",
			"title" : biolims.common.contractId,
		});
		
		colOpts.push({
			"data" : "project-name",
			"title" : biolims.common.contractName,
		});
		
		colOpts.push({
			"data" : "orderId",
			"title" : biolims.sample.orderNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-concentration",
			"title" : biolims.common.concentration,
		});
		colOpts.push({
			"data" : "scopeId",
			"title" : biolims.purchase.costCenterId,
		});
		colOpts.push({
			"data" : "scopeName",
			"title" : biolims.purchase.costCenter,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-customer-name",
			"title" : biolims.sample.customerName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-crmDoctor-name",
			"title" : biolims.common.customName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-species",
			"title" : biolims.wk.species,
		});
		
		
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});

				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, "",
				"/technology/dna/techDnaServiceTask/showItemDialogNewListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	}else if($("#techJkServiceTask_type").val()=="2"){

		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.sample.id,
			"visible": false
		});
		colOpts.push({
			"data" : "code",
			"title" : biolims.common.code
		});
		
		colOpts.push({
			"data": "barCode",
			"title": biolims.common.barCode
		});
		
		colOpts.push({
			"data" : "sampleCode",
			"title" : biolims.common.sampleCode,
			"visible": false
		});
		
		colOpts.push({
			"data" : "dataTraffic",
			"title" : biolims.common.throughput,
		});
		
		
		
		colOpts.push({
			"data" : "productId",
			"title" : biolims.common.productId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "productName",
			"title" : biolims.common.testProject,
		});
		
		colOpts.push({
			"data" : "samplingDate",
			"title" : biolims.sample.samplingDate,
		});
		
		colOpts.push({
			"data" : "sampleInfo-idCard",
			"title" : biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "location",
			"title" : biolims.common.location,
		});
		
		colOpts.push({
			"data" : "qpcrConcentration",
			"title" : biolims.QPCR.concentration,
		});
		
		colOpts.push({
			"data" : "indexa",
			"title" : 'index',
		});
		
		colOpts.push({
			"data" : "concentration",
			"title" : biolims.common.concentration,
		});
		
		colOpts.push({
			"data" : "volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfo-receiveDate",
			"title" : biolims.common.receiveDate,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-id",
			"title" : biolims.common.contractCodeId,
		});
		
		colOpts.push({
			"data" : "sampleInfo-project-name",
			"title" : biolims.common.contractName,
		});
		
		colOpts.push({
			"data" : "sellPerson-id",
			"title" : biolims.common.salesId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sellPerson-name",
			"title" : biolims.common.sales,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-id",
			"title" : biolims.common.customId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmCustomer-name",
			"title" : biolims.common.custom,
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-id",
			"title" : biolims.sample.crmDoctorId,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleInfo-crmDoctor-name",
			"title" : biolims.sample.crmDoctorName,
		});
		
		colOpts.push({
			"data": "isRp",
			"title": biolims.common.ifThereStain,
//			"className": "select",
			"name": biolims.common.no+"|"+biolims.common.yes,
//			"createdCell": function(td) {
//				$(td).attr("saveName", "isRp");
//				$(td).attr("selectOpt", biolims.common.no+"|"+biolims.common.yes);
//			},
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return biolims.common.no;
				}
				if(data == "1") {
					return biolims.common.yes;
				}else {
					return '';
				}
			}
		});
		
		colOpts.push({
			"data" : "note",
			"title" : biolims.common.note,
		});
		
		colOpts.push({
			"data" : "techJkServiceTask-id",
			"title" : biolims.common.qcTask,
		});
		
		colOpts.push({
			"data" : "sampleInfo-name",
			"title" : biolims.common.packetNumber,
		});
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});
	
				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, "",
				"/sample/storage/sampleIn/showSampleWaitOutItemListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	}else if($("#techJkServiceTask_type").val()=="3"){

		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.sample.id,
			"visible": false
		});
		
		colOpts.push({
			"data" : "sampleCode",
			"title" : "样本编号",
		});
		
		colOpts.push({
			"data" : "orderCode",
			"title" : "订单编号",
		});
		
		colOpts.push({
			"data": "projectId",
			"title": "项目编号",
		});
		
		colOpts.push({
			"data" : "sampleID",
			"title" : "文库编号",
		});
		
		colOpts.push({
			"data" : "productName",
			"title" :biolims.common.productName,
		});
		
		
		
		colOpts.push({
			"data" : "runId",
			"title" : "RUN_ID",
		});
		
		colOpts.push({
			"data" : "gcContent",
			"title" : biolims.analysis.gcContent,
		});
		
		colOpts.push({
			"data" : "nContent",
			"title" : biolims.analysis.nContent,
		});
		
		colOpts.push({
			"data" : "qTwentyPCT",
			"title" : biolims.analysis.qTwentyPCT,
		});
		
		colOpts.push({
			"data" : "qThirtyPCT",
			"title" : biolims.analysis.qThirtyPCT,
		});
		
		colOpts.push({
			"data" : "ratioOfLane",
			"title" : biolims.analysis.ratioOfLane,
		});
		
		colOpts.push({
			"data" : "raw_reads",
			"title" : biolims.analysis.raw_reads,
		});
		
		colOpts.push({
			"data" : "cleanReads",
			"title" : biolims.analysis.cleanReads,
		});
		
		colOpts.push({
			"data" : "ratioOfReads",
			"title" : biolims.analysis.ratioOfReads,
		});
		
		colOpts.push({
			"data" : "raw_size",
			"title" : biolims.analysis.raw_size,
		});
		
		colOpts.push({
			"data" : "cleanBases",
			"title" : biolims.analysis.cleanBases,
		});
		
		colOpts.push({
			"data" : "ratioOfBases",
			"title" : biolims.analysis.ratioOfBases,
		});
		
		colOpts.push({
			"data" : "hq_reads",
			"title" :biolims.master.hq_reads,
		});
		
		colOpts.push({
			"data" : "medianInsert",
			"title" : biolims.analysis.medianInsert,
		});
		
		colOpts.push({
			"data" : "duplicate",
			"title" : biolims.analysis.duplicate,
		});
		
		colOpts.push({
			"data" : "onTarget_raw",
			"title" : biolims.analysis.onTarget_raw,
		});
		
		colOpts.push({
			"data" : "onTarget_add",
			"title" : biolims.analysis.onTarget_add,
		});
		
		colOpts.push({
			"data" : "mappingPct",
			"title" : biolims.analysis.mappingPct,
		});
		
		colOpts.push({
			"data" : "oriDepth",
			"title" : biolims.analysis.oriDepth,
		});
		
		colOpts.push({
			"data" : "oneXcoverage",
			"title" : biolims.analysis.oneXcoverage,
		});
		
		colOpts.push({
			"data" : "tenXcoverage",
			"title" : biolims.analysis.tenXcoverage,
		});
		
		colOpts.push({
			"data" : "twentyXcoverage",
			"title" : biolims.analysis.twentyXcoverage,
		});
		
		colOpts.push({
			"data" : "fiftyXcoverage",
			"title" : biolims.analysis.fiftyXcoverage,
		});
		colOpts.push({
			"data" : "twentyPctMeancoverage",
			"title" : biolims.analysis.twentyPctMeancoverage,
		});
		colOpts.push({
			"data" : "meanDepthDedup",
			"title" : biolims.analysis.meanDepthDedup,
		});
		colOpts.push({
			"data" : "oneCoverageDedup",
			"title" : biolims.analysis.oneCoverageDedup,
		});
		colOpts.push({
			"data" : "tenCoverageDedup",
			"title" : biolims.analysis.tenCoverageDedup,
		});
		
		colOpts.push({
			"data" : "twentyCoverageDedup",
			"title" : biolims.analysis.twentyCoverageDedup,
		});
		
		colOpts.push({
			"data" : "fiftyCoverageDedup",
			"title" : biolims.analysis.fiftyCoverageDedup,
		});
		
		colOpts.push({
			"data" : "twentyMeanCoverageDedup",
			"title" : biolims.analysis.twentyMeanCoverageDedup,
		});
		
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});
	
				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							var techJkServiceRight = $("#storageOutItemdiv")
								.DataTable();
							techJkServiceRight.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techJkServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
		

		var storageOutAllOps = table(true, $("#techJkServiceTask_projectId").val(),
				"/analysis/desequencing/deSequencingItem/showDialogDeSequencingItemNewListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	
	}else{

		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : biolims.common.id,
			"visible": false
		});
		colOpts.push({
			"data" : "sampleInfoIn-code",
			"title" : biolims.common.code,
		});
		
		colOpts.push({
			"data": "dataTraffic",
			"title": biolims.common.throughput,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-idCard",
			"title" :biolims.common.outCode,
		});
		
		colOpts.push({
			"data" : "groupNo",
			"title" : biolims.common.SampleCollectionPacketNumber,
		});
		
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
		});
		
		colOpts.push({
			"data" : "project-id",
			"title" : biolims.common.contractId,
		});
		
		colOpts.push({
			"data" : "orderId",
			"title" : biolims.sample.orderNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-concentration",
			"title" : biolims.common.concentration,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-volume",
			"title" : biolims.common.volume,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sumTotal",
			"title" : biolims.common.sumNum,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-customer-name",
			"title" : biolims.sample.customerName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-crmDoctor-name",
			"title" : biolims.common.customName,
		});
		
		colOpts.push({
			"data" : "sampleInfoIn-sampleInfo-species",
			"title" : biolims.wk.species,
		});
		
		
		var tbarOpts = [];
		
//		tbarOpts.push({
//			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
//			action:function(){
//				search()
//			}
//		});
//		tbarOpts.push({
//			text: biolims.common.fillDetail,
//			action: function() {
//				var sampleId = [];
//				$("#storageOutAlldiv .selected").each(function(i, val) {
//					sampleId.push($(val).children("td").eq(0).find("input").val());
//				});
//
//				// 先保存主表信息
//				var name = $("#techDnaServiceTask_name").val();
//				var note = $("#techDnaServiceTask_note").val();
//				var createUser = $("#techDnaServiceTask_createUser").attr("userId");
//				var createDate = $("#techDnaServiceTask_createDate").text();
//				var state = $("#techDnaServiceTask_state").attr("state");
//				var stateName = $("#techDnaServiceTask_state").text();
//				var id = $("#techDnaServiceTask_id").text();
//				$.ajax({
//					type: 'post',
//					url: '/technology/dna/techDnaServiceTask/addPurchaseApplyMakeUp.action',
//					data: {
//						ids: sampleId,
//						id: id,
//						note: note,
//						createUser: createUser,
//						createDate: createDate,
//						name: name,
//						state:state,
//						stateName:stateName
//					},
//					success: function(data) {
//						var data = JSON.parse(data)
//						if(data.success) {
//							$("#techDnaServiceTask_id").text(data.data);
//							var param = {
//								id: data.data
//							};
//							var techJkServiceRight = $("#storageOutItemdiv")
//								.DataTable();
//							techJkServiceRight.settings()[0].ajax.data = param;
//							techJkServiceRight.ajax.reload();
//						} else {
//							top.layer.msg(biolims.common.addToDetailFailed);
//						};
//					}
//				})
//			}
//		});
		
		

		var storageOutAllOps = table(true, "",
				"/technology/dna/techDnaServiceTask/showItemDialogNewListJson.action", colOpts, tbarOpts);
		techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

		// 选择数据并提示
		techDnaServiceAllTab.on('draw', function() {
			var index = 0;
			$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
				if ($(this).is(':checked')) {
					index++;
				} else {
					index--;
				}
				top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
			});
		});
	
	}
	
//	bpmTask($("#bpmTaskId").val());

}
*/
var techDnaServiceAllTab;
$(function() {
	
	var colOpts = [];
	
	colOpts.push({
		"data": "id",
		"title": biolims.common.orderCode,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "gender",
		"title": biolims.common.gender,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "age",
		"title": biolims.common.age,
	});
	colOpts.push({
		"data": "productName",
		"title": "产品名称",
	});
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.state,
	});
		
		var tbarOpts = [];
		
		tbarOpts.push({
			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
			action:function(){
				search()
			}
		});
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				var sampleId = [];
				$("#storageOutAlldiv .selected").each(function(i, val) {
					sampleId.push($(val).children("td").eq(0).find("input").val());
				});
	
				// 先保存主表信息
				var name = $("#techJkServiceTask_name").val();
				var note = $("#techJkServiceTask_note").val();
				var createUser = $("#techJkServiceTask_createUser").attr("userId");
				var createDate = $("#techJkServiceTask_createDate").text();
				var state = $("#headStateName").attr("state");
				var stateName = $("#headStateName").text();
				var id = $("#techJkServiceTask_id").text();
				var type = $("#techJkServiceTask_type").val();
				var jsonStr = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/technology/wk/techJkServiceTask/addPurchaseApplyMakeUp.action',
					data: {
						dataValue: jsonStr,
						ids: sampleId,
						id: id,
						note: note,
						createUser: createUser,
						createDate: createDate,
						name: name,
						state:state,
						stateName:stateName,
						type:type
					},
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							$("#techJkServiceTask_id").text(data.data);
							var param = {
								id: data.data
							};
							techDnaServiceAllTab.settings()[0].ajax.data = param;
							techDnaServiceAllTab.ajax.reload();
							techDnaServiceRight.settings()[0].ajax.data = param;
							techDnaServiceRight.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				})
			}
		});
		
	var options = table(true, "",
		"/system/sample/sampleOrder/showSampleOrderDialogListJson.action",colOpts, tbarOpts)
	techDnaServiceAllTab = renderRememberData($("#storageOutAlldiv"), options);
	
	// 选择数据并提示
	techDnaServiceAllTab.on('draw', function() {
		var index = 0;
		$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//恢复之前查询的状态
	$('#storageOutAlldiv').on('init.dt', function() {
		recoverSearchContent(techDnaServiceAllTab);
	})
});

//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.custom,
		"type" : "input",
		"searchName" : "sampleInfo-crmDoctor-name",
	},{
		"txt" : biolims.common.outCode,
		"type" : "input",
		"searchName" : "sampleInfo-idCard",
	},{
		"txt" : biolims.common.custom+biolims.common.unit,
		"type" : "input",
		"searchName" : "sampleInfo-crmCustomer-name",
	},{
		"txt" : biolims.common.qcTask,
		"type" : "input",
		"searchName" : "techJkServiceTask-id",
	}, {
		"type" : "table",
		"table" : techDnaServiceAllTab
	} ];
}

//大保存
function save() {
		var changeLog = "任务单：";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
	    //dwb 2018-05-11 11:45:00
		var jsonStr = JSON.stringify($("#form1").serializeObject());
		var dataItemJson = saveItemjson($("#storageOutItemdiv"));
		var ele = $("#storageOutItemdiv");
		var changeLogItemf = biolims.common.claimSampleTask+"：";
		var type = $("#techJkServiceTask_type").val();
		
		changeLogItemf += "任务单-";
		
		var changeLogItem = getChangeLog(dataItemJson, ele, changeLogItemf);
		var changeLogItems = "";
		if(changeLogItem != changeLogItemf){
			changeLogItems = changeLogItem;
		}
		var name = $("#techJkServiceTask_name").val();
		var note = $("#techJkServiceTask_note").val();
		var createUser = $("#techJkServiceTask_createUser").attr("userId");
		var createDate = $("#techJkServiceTask_createDate").text();
		var state = $("#headStateName").attr("state");
		var stateName = $("#headStateName").text();
		var id = $("#techJkServiceTask_id").text();
		//必填验证
		var requiredField=requiredFilter();
			if(!requiredField){
				return false;
			}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
			url: ctx + '/technology/wk/techJkServiceTask/saveStorageOutAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItems,
				id:id,
				note:note,
				name:name,
				createUser:createUser,
				createDate:createDate,
				state:state,
				stateName:stateName
			},
			success: function(data) {
				if(data.success) {
					top.layer.closeAll();
					var url = "/technology/wk/techJkServiceTask/editTechJkServiceTask.action?id=" + data.id;

//					url = url + "&bpmTaskId=" + $("#bpmTaskId").val() + "&p_type=" + $("#p_type").val();

					window.location.href = url;
				} else {
					top.layer.closeAll();
					top.layer.msg(data.msg);
				}
			}
		});
}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

function changeState() {
    var	id=$("#techJkServiceTask_id").text();
	var paraStr = "formId=" + id +
		"&tableId=TechJkServiceTask";
	console.log(paraStr);
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layero) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();

			})

		}
	});

	
}

var mainFileInput;
function fileUp() {
	if($("#techJkServiceTask_id").text()=="NEW"){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('1', 'techJkServiceTask', $("#techJkServiceTask_id").text());
	mainFileInput.off("fileuploaded");
}
function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=techJkServiceTask&id=" + $("#techJkServiceTask_id").text(),
		cancel: function(index, layero) {
			top.layer.close(index);
		}
	});
}
function fileUp2() {
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('2', 'techJkServiceTask', $("#techJkServiceTask_id").text());
	mainFileInput.off("fileuploaded");
}
function fileView2() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		content: window.ctx + "/operfile/initFileList.action?flag=2&modelType=techJkServiceTask&id=" + $("#techJkServiceTask_id").text(),
		cancel: function(index, layero) {
			top.layer.close(index);
		}
	});
}