var techServiceTaskGrid;
$(function() {
	var options = table(true, "",
			"/technology/wk/techJkServiceTask/showTechJkServiceTaskNewListJson.action?type="+$("#type").val(),
			[ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "name",
				"title" : biolims.common.name,
			}, {
				"data": "acceptUser-name",
				"title": biolims.user.personnelGroup,
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "confirmUser-name",
				"title" : biolims.wk.confirmUserName,
			}, {
				"data" : "confirmDate",
				"title" : biolims.wk.confirmDate,
			}, {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	techServiceTaskGrid = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(techServiceTaskGrid);
	})
});

function add() {
	window.location = window.ctx
			+ '/technology/wk/techJkServiceTask/editTechJkServiceTask.action?type='+$("#type").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/wk/techJkServiceTask/editTechJkServiceTask.action?id=' + id+"&type="+$("#type").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/technology/wk/techJkServiceTask/viewTechJkServiceTask.action?id="
			+ id+"&type="+$("#type").val();
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.sample.createUserName,
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"type" : "table",
		"table" : techServiceTaskGrid
	} ];
}
