var sampleDnaInfoDialogGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
		   fields.push({
			name:'productName',
			type:"string"
		});
	   fields.push({
			name:'productId',
			type:"string"
		});
		fields.push({
			name : 'reportDate',
			type : "string"
		});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
			name:'sampleNum',
			type:"string"
		});
	
	   fields.push({
			name:'dicSampleType-id',
			type:"string"
		});
	   fields.push({
			name:'dicSampleType-name',
			type:"string"
		});
	   fields.push({
			name:'dnaCode',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dnaCode',
		hidden : true,
		header:biolims.dna.dnaCode,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden:true,
		header:biolims.common.dicSampleTypeId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/technology/wk/techJkServiceTask/showSampleDnaInfoJson.action?id="+$("#id").val();
	loadParam.limit = 200;
	var opts = {};
	opts.title = biolims.sample.selectSample;
	opts.width = document.body.clientWidth - 250;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect = function(id) {
		$("#id").val(id);
	};
	opts.rowdblclick = function(id) {
		$("#id").val(id);
		view();
	};
	sampleDnaInfoDialogGrid = gridTable("template_wait_grid_div", cols, loadParam, opts);
	$("#template_wait_grid_div").data("sampleDnaInfoDialogGrid", sampleDnaInfoDialogGrid);
});