var techDnaServiceRight, oldChangeLog;
//出库明细表
$(function() {
	var flag = false;
	if($("#techJkServiceTask_state").val()=="1"){
		$("#btn_task").show();
		flag = true;
	}
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title":  biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false
	});
	colOpts.push({
		"data": "sampleOrder-id",
		"title":  "订单编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	colOpts.push({
		"data": "sampleOrder-name",
		"title":  "姓名",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-name");
		}
	});
	colOpts.push({
		"data": "sampleOrder-gender",
		"title":  "性别",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-gender");
		}
	});
	colOpts.push({
		"data": "sampleOrder-age",
		"title":  "年龄",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-age");
		}
	});
	colOpts.push({
		"data": "sampleOrder-productName",
		"title":  "产品名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-productName");
		}
	});
	colOpts.push({
		"data": "sampleOrder-newTask",
		"title":  "最近执行单号",
		"visible":flag,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-newTask");
		}
	});
	var tbarOpts = [];
	
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			/*removeChecked($("#storageOutItemdiv"),
				"/technology/wk/techJkServiceTask/delTechJkServiceTaskItem.action");*/
			removeData($("#storageOutItemdiv"));
		}
	});
/*	var flg=false;
	var jk=false;
	var sj=false;
	var sx=false;
	var type = $("#techJkServiceTask_type").val();
	if(type=="0"){//质检任务单
		flg=true;
	}else if(type=="1"){//建库任务单
		jk=true;
	}else if(type=="2"){//上机任务单
		sj=true;
	}else if(type=="3"){//生信任务单
		sx=true;
	}
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title":  biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false
	});
	if(!sx){
		colOpts.push({
			"data": "code",
			"title":  biolims.common.code,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "code");
				$(td).attr("sampleCode", rowData['sampleCode']);
			},
		});
	}
	
	if(sx){
		colOpts.push({
			"data": "sampleCode",
			"title":  biolims.common.code,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sampleCode");
			},
		});
	}
	
	colOpts.push({
		"data": "groupNo",
		"title": biolims.common.packetNumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "groupNo");
		},
		"visible": flg
	});
	
	colOpts.push({
		"data": "indexa",
		"title": "index",
		"createdCell": function(td) {
			$(td).attr("saveName", "indexa");
		},
		"visible": sj
	});
	colOpts.push({
		"data": "scopeId",
		"title":biolims.purchase.costCenterId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "scopeId");
		},
	});
	
	
	colOpts.push({
		"data": "scopeName",
		"title":biolims.purchase.costCenter,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "scopeName");
		},
	});
	
	if(!sx){
		colOpts.push({
			"data": "wgcId",
			"className":"edit",
			"title": biolims.common.poolingGroup,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "wgcId");
				$(td).attr("barCode", rowData['barCode']);
			},
		});
		colOpts.push({
			"data": "project-id",
			"title": biolims.common.contractCodeId,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "project-id");
				$(td).attr("project-name", rowData['project-name']);
			},
		});
		colOpts.push({
			"data": "sampleOrder-id",
			"title":"关联订单",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "sampleOrder-id");
			}
		});
		colOpts.push({
			"data": "sampleInfoIn-scopeId",
			"title":biolims.purchase.costCenterId,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "sampleInfoIn-scopeId");
			},
			"visible": false
		});
		
		
		colOpts.push({
			"data": "sampleInfoIn-scopeName",
			"title":biolims.purchase.costCenter,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "sampleInfoIn-scopeName");
			},
			"visible": false
		});
		colOpts.push({
			"data": "orderId",
			"title": biolims.sample.orderNum,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "orderId");
				$(td).attr("inwardCode", rowData['inwardCode']);
			},
		});
		colOpts.push({
			"data": "sampleType",
			"title": biolims.common.sampleType,
			"createdCell": function(td) {
				$(td).attr("saveName", "sampleType");
			},
		});
		colOpts.push({
			"data": "species",
			"title": biolims.wk.species,
			"createdCell": function(td) {
				$(td).attr("saveName", "species");
			},
		});
	}
	colOpts.push({
		"data": "orderType",
		"title": biolims.common.qcType,
		"className": "select",
		"name": "DNA抽提|RNA抽提|DNA QC|RNA QC|LIBRARY QC",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderType");
			$(td).attr("selectOpt", "DNA抽提|RNA抽提|DNA QC|RNA QC|LIBRARY QC");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "DNA抽提";
			}else if(data == "2") {
				return "RNA抽提";
			}else if(data == "3") {
				return "DNA QC";
			}else if(data == "4") {
				return "RNA QC";
			}else if(data == "5") {
				return "LIBRARY QC";
			}else {
				return '';
			}
		},
		"visible": flg
	});
	if(!sx){
		colOpts.push({
			"data": "dataTraffic",
			"title": biolims.common.throughput,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "dataTraffic");
			},
		});
		colOpts.push({
			"data": "productName",
			"title": biolims.common.productName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "productName");
				$(td).attr("sampleInfoIn-id",rowData['sampleInfoIn-id']);
				$(td).attr("tempId", rowData['tempId']);
				$(td).attr("productId", rowData['productId']);
			},
		});
	}
	colOpts.push({
		"data": "isStain",
		"title": biolims.common.ifThereStain,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isStain");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}else if(data == "0") {
				return biolims.common.no;
			}else {
				return '';
			}
		},
		"visible": flg
	});
	colOpts.push({
		"data": "isNeedStain",
		"title": biolims.common.ifThereNeedStain,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isNeedStain");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}else if(data == "0") {
				return biolims.common.no;
			}else {
				return '';
			}
		},
		"visible": flg
	});
	
	colOpts.push({
		"data": "probeCode-name",
		"title": biolims.pooling.probeCodeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "probeCode-name");
			$(td).attr("probeCode-id",rowData['probeCode-id']);
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "gc",
		"title": 'GC%',
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "gc");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "phix",
		"title": 'phix%',
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "phix");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "q30",
		"title": 'Q30',
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "q30");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "dataNum",
		"title": biolims.common.dataNum,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dataNum");
		},
		"visible": jk
	});
	
	if(!sx){
		colOpts.push({
			"data": "crmCustomer-name",
			"title": biolims.sample.customerName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "crmCustomer-name");
				$(td).attr("crmCustomer-id",rowData['crmCustomer-id']);
			},
		});
		
		colOpts.push({
			"data": "crmDoctor-name",
			"title": biolims.common.customName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "crmDoctor-name");
				$(td).attr("crmDoctor-id",rowData['crmDoctor-id']);
			},
		});
		
		colOpts.push({
			"data": "concentration",
			"title": biolims.common.concentration,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "concentration");
				$(td).attr("name",rowData['name']);
				$(td).attr("location",rowData['location']);
			},
		});
		
		colOpts.push({
			"data": "volume",
			"title": biolims.common.volume,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "volume");
			},
		});
		
		colOpts.push({
			"data": "sampleNum",
			"title": biolims.common.sumNum,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sampleNum");
				$(td).attr("acceptDate",rowData['acceptDate']);
				$(td).attr("reportDate",rowData['reportDate']);
				$(td).attr("experimentUser-id",rowData['experimentUser-id']);
				$(td).attr("experimentUser-name",rowData['experimentUser-name']);
				$(td).attr("endDate",rowData['endDate']);
				$(td).attr("classify",rowData['classify']);
				$(td).attr("dataType",rowData['dataType']);
			},
		});
	}
	colOpts.push({
		"data": "blendLane",
		"title": biolims.sequencing.mixLane,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "blendLane");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "blendFc",
		"title": biolims.sequencing.mixFlowcell,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "blendFc");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "techJkServiceTask-name",
		"title": biolims.common.relatedMainTableName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "techJkServiceTask-name");
			$(td).attr("techJkServiceTask-id",rowData['techJkServiceTask-id']);
		},
	});
	
	if(!sx){
		colOpts.push({
			"data": "nextFlow",
			"title": biolims.common.nextFlow,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "nextFlow");
				$(td).attr("nextFlowId",rowData['nextFlowId']);
			},
		});
		colOpts.push({
			"data": "note",
			"title": biolims.common.note,
			"className":"edit",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "note");
				$(td).attr("khysConcentration",rowData['khysConcentration']);
				$(td).attr("khysVolume",rowData['khysVolume']);
				$(td).attr("tissueId",rowData['tissueId']);
				$(td).attr("extraction",rowData['extraction']);
				$(td).attr("qcProtocol",rowData['qcProtocol']);
				$(td).attr("sequencingApplication",rowData['sequencingApplication']);
				$(td).attr("sampleReceivedDate",rowData['sampleReceivedDate']);
				$(td).attr("specialRequirements",rowData['specialRequirements']);
				$(td).attr("state",rowData['state']);
				$(td).attr("stateName",rowData['stateName']);
			},
		});
		
	}
	
	if(sx){
		colOpts.push({
			"data": "orderCode",
			"title": biolims.common.orderCode,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "orderCode");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "projectId",
			"title": biolims.common.projectId,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "projectId");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "sampleID",
			"title": biolims.wk.wkCode,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sampleID");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "sxProductName",
			"title": biolims.common.productName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sxProductName");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "runId",
			"title": "RUN_ID",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "runId");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "laneId",
			"title": "laneId",
			"className":"edit",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "laneId");
				$(td).attr("sxSampleCode",rowData['sxSampleCode']);
			},
			"visible": sx
		});
		
	}
	
	
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#storageOutItemdiv"),
				"/technology/wk/techJkServiceTask/delTechJkServiceTaskItem.action");
		}
	});
	if(sx){
		tbarOpts.push({
			text:biolims.common.volumeContract,
			action: function() {
				var rows = $("#storageOutItemdiv .selected");
				var length = rows.length;
				if(!length) {
					top.layer.msg(biolims.common.pleaseSelect);
					return false;
				}
				top.layer.open({
					title: biolims.crm.selectContract,
					type: 2,
					area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
					btn: biolims.common.confirmSelected,
					content: [window.ctx + "/crm/project/showProjectSelectNewList.action", ''],
					yes: function(index, layero) {
						var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(
							0).text();
						var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(1).text();
						rows.addClass("editagain");
						rows.find("td[savename='projectId']").text(id);
						top.layer.close(index)
					},
				})
			}
		});
	}else{
		tbarOpts.push({
			text:biolims.common.productName,
			action: function() {
				var rows = $("#storageOutItemdiv .selected");
				var length = rows.length;
				if(!length) {
					top.layer.msg(biolims.common.pleaseSelect);
					return false;
				}
				top.layer.open({
					title: biolims.crm.selectProduct,
					type: 2,
					area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
					btn: biolims.common.selected,
					content: window.ctx +
						"/com/biolims/system/product/showProductSelTree.action",
					yes: function(index, layero) {
						var name=[],id=[];
						$(".layui-layer-iframe", parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function (i,v) {
							name.push($(v).children("td").eq(2).text());
							id.push($(v).children("td").eq(1).text());
						});
						rows.addClass("editagain");
						rows.find("td[savename='productName']").attr(
							"productId", id.join(",")).text(name.join(","));
						rows.find("td[savename='productId']").text(id.join(","));
						top.layer.close(index);
					},
					cancel: function(index, layero) {
						top.layer.close(index);
					}
				})
			}
		});
		tbarOpts.push({
			text:biolims.common.volumeContract,
			action: function() {
				var rows = $("#storageOutItemdiv .selected");
				var length = rows.length;
				if(!length) {
					top.layer.msg(biolims.common.pleaseSelect);
					return false;
				}
				top.layer.open({
					title: biolims.crm.selectContract,
					type: 2,
					area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
					btn: biolims.common.confirmSelected,
					content: [window.ctx + "/crm/project/showProjectSelectNewList.action", ''],
					yes: function(index, layero) {
						var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(
							0).text();
						var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(1).text();
						rows.addClass("editagain");
						rows.find("td[savename='project-name']").attr(
							"project-id", id).text(name);
						rows.find("td[savename='project-id']").text(id);
						top.layer.close(index)
					},
				})
			}
		});
		if(flg){
			tbarOpts.push({
				text:biolims.common.batchNextStep,
				action: function() {
					var rows = $("#storageOutItemdiv .selected");
					var length = rows.length;
					if(!length) {
						top.layer.msg(biolims.common.pleaseSelect);
						return false;
					}
					top.layer.open({
						title: biolims.common.selectNextFlow,
						type: 2,
						area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
						btn: biolims.common.confirmSelected,
						content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=JKRWD", ''],
						yes: function(index, layero) {
							var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
								0).text();
							var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
							rows.addClass("editagain");
							rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(name);
							rows.find("td[savename='nextFlowId']").text(id);
							top.layer.close(index);
						},
					})
				}
			});
		}else if(jk){
			tbarOpts.push({
				text:biolims.common.batchNextStep,
				action: function() {
					var rows = $("#storageOutItemdiv .selected");
					var length = rows.length;
					if(!length) {
						top.layer.msg(biolims.common.pleaseSelect);
						return false;
					}
					top.layer.open({
						title: biolims.common.selectNextFlow,
						type: 2,
						area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
						btn: biolims.common.confirmSelected,
						content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=JKRWD", ''],
						yes: function(index, layero) {
							var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
								0).text();
							var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
							rows.addClass("editagain");
							rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(name);
							rows.find("td[savename='nextFlowId']").text(id);
							top.layer.close(index);
						},
					})
				}
			});
		}else if(sj){
			tbarOpts.push({
				text:biolims.common.batchNextStep,
				action: function() {
					var rows = $("#storageOutItemdiv .selected");
					var length = rows.length;
					if(!length) {
						top.layer.msg(biolims.common.pleaseSelect);
						return false;
					}
					top.layer.open({
						title: biolims.common.selectNextFlow,
						type: 2,
						area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
						btn: biolims.common.confirmSelected,
						content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=SJRWD", ''],
						yes: function(index, layero) {
							var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
								0).text();
							var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
							rows.addClass("editagain");
							rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(id);
							rows.find("td[savename='nextFlow']").text(name);
							top.layer.close(index);
						},
					})
				}
			});
		}
	}*/

	var storageOutItemOps = table(true, $("#techJkServiceTask_id").text(), "/technology/wk/techJkServiceTask/showTechJkServiceTaskItemNewListJson.action", colOpts, tbarOpts);
	techDnaServiceRight = renderData($("#storageOutItemdiv"), storageOutItemOps);
	//选择数据并提示
	techDnaServiceRight.on('draw', function() {
		var index = 0;
		$("#storageOutItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		
		oldChangeLog = techDnaServiceRight.ajax.json();
	});
});

//查看任务
function viewTask(){
	var ele = $("#storageOutItemdiv");
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}else if(length>1){
		top.layer.msg("只能选择一条数据");
		return false;
	}
	var sampleOrder = rows.children("td").eq(1).text();
	window.location = window.ctx
	+ '/system/sample/sampleOrder/viewSampleOrderKanban.action?id='+sampleOrder;
}

function removeData(ele){
	var ids = [];
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.confirm2Del + length + biolims.common.record,
			{
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	},function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			if(id) {
				ids.push(id);
			} else {
				$(val).remove();
			}
		});
		if(ids.length) {
			$.ajax({
				type: "post",
				url: ctx + "/technology/wk/techJkServiceTask/delTechJkServiceTaskItem.action",
				data: {
					ids: ids
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.msg(biolims.common.deleteSuccess);
						techDnaServiceAllTab.ajax.reload();
						techDnaServiceRight.settings()[0].ajax.data ={id: $("#techJkServiceTask_id").text()};
						techDnaServiceRight.ajax.reload();
					}
				}
			});
		}
	});
}

// 保存
function savetechDnaServiceRight() {
    var ele=$("#storageOutItemdiv");
	var changeLog = biolims.common.claimSampleTask+":";
	var type = $("#techJkServiceTask_type").val();
	if(type=="0"){//质检任务单
		changeLog += "质检任务单-";
	}else if(type=="1"){//建库任务单
		changeLog += "建库任务单-";
	}else if(type=="2"){//上机任务单
		changeLog += "上机任务单-";
	}else if(type=="3"){//生信任务单
		changeLog += "生信任务单-";
	}
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/storage/out/saveStorageOutItemTable.action',
		data: {
			id: $("#storageOut_id").text(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if($("#techJkServiceTask_type").val()=="3"){
				
			}else{
				if(k == "code") {
					json["sampleCode"] = $(tds[j]).attr("sampleCode");
					continue;
				}
			}
			if(k == "wgcId") {
				json["barCode"] = $(tds[j]).attr("barCode");
				continue;
			}
			if(k == "orderId") {
				json["inwardCode"] = $(tds[j]).attr("inwardCode");
				continue;
			}
			if(k == "orderType") {
				var gender = $(tds[j]).text();
				if(gender == "DNA抽提") {
					json[k] = "1";
				} else if(gender == "RNA抽提") {
					json[k] = "2";
				} else if(gender == "DNA QC") {
					json[k] = "3";
				} else if(gender == "RNA QC") {
					json[k] = "4";
				} else if(gender == "LIBRARY QC") {
					json[k] = "5";
				} else {
					json[k] = "";
				}
				continue;
			}
			if(k == "productName") {
				json["sampleInfoIn-id"] = $(tds[j]).attr("sampleInfoIn-id");
				json["tempId"] = $(tds[j]).attr("tempId");
				json["productId"] = $(tds[j]).attr("productId");
				continue;
			}
			if(k == "isStain") {
				var gender = $(tds[j]).text();
				if(gender == biolims.common.yes) {
					json[k] = "1";
				} else if(gender == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			if(k == "isNeedStain") {
				var gender = $(tds[j]).text();
				if(gender == biolims.common.yes) {
					json[k] = "1";
				} else if(gender == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			if(k == "probeCode-name") {
				json["probeCode-id"] = $(tds[j]).attr("probeCode-id");
				continue;
			}
			if(k == "crmCustomer-name") {
				json["crmCustomer-id"] = $(tds[j]).attr("crmCustomer-id");
				continue;
			}
			if(k == "crmDoctor-name") {
				json["crmDoctor-id"] = $(tds[j]).attr("crmDoctor-id");
				continue;
			}
			if(k == "concentration") {
				json["name"] = $(tds[j]).attr("name");
				json["location"] = $(tds[j]).attr("location");
				continue;
			}
			if(k == "sampleNum") {
				json["acceptDate"] = $(tds[j]).attr("acceptDate");
				json["reportDate"] = $(tds[j]).attr("reportDate");
				json["experimentUser-id"] = $(tds[j]).attr("experimentUser-id");
				json["experimentUser-name"] = $(tds[j]).attr("experimentUser-name");
				json["endDate"] = $(tds[j]).attr("endDate");
				json["classify"] = $(tds[j]).attr("classify");
				json["dataType"] = $(tds[j]).attr("dataType");
				continue;
			}
			if(k == "techJkServiceTask-name") {
				json["techJkServiceTask-id"] = $(tds[j]).attr("techJkServiceTask-id");
				continue;
			}
			if(k == "nextFlow") {
				json["nextFlowId"] = $(tds[j]).attr("nextFlowId");
				continue;
			}
			if(k == "note") {
				json["khysConcentration"] = $(tds[j]).attr("khysConcentration");
				json["khysVolume"] = $(tds[j]).attr("khysVolume");
				json["tissueId"] = $(tds[j]).attr("tissueId");
				json["extraction"] = $(tds[j]).attr("extraction");
				json["qcProtocol"] = $(tds[j]).attr("qcProtocol");
				json["sequencingApplication"] = $(tds[j]).attr("sequencingApplication");
				json["sampleReceivedDate"] = $(tds[j]).attr("sampleReceivedDate");
				json["specialRequirements"] = $(tds[j]).attr("specialRequirements");
				json["state"] = $(tds[j]).attr("state");
				json["stateName"] = $(tds[j]).attr("stateName");
				continue;
			}
			if(k == "laneId") {
				json["sxSampleCode"] = $(tds[j]).attr("sxSampleCode");
				continue;
			}
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += 'ID为"' + v.num + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
//样本数量
function sampleNum() {
	var rows = $("#storageOutItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var num = 1;
	top.layer.open({
		title: biolims.common.batchOrderNumber,
		type: 1,
		area: ["30%", "30%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: ' <span class=""></span><input type="text"  class="form-control" id="sampleNum">',
//		success: function() {
//			$("#subSampleNum").click(function() {
//				num = parseInt($("#sampleNum").val()) - 1;
//				$("#sampleNum").val(num);
//			});
//			$("#addSampleNum").click(function() {
//				num = parseInt($("#sampleNum").val()) + 1;
//				$("#sampleNum").val(num);
//			});
//		},
		yes: function(index, layero) {
			rows.find("td[savename='orderId']").text(parseInt($("#sampleNum").val()));
			top.layer.close(index)
		},
	})

}

/*function changeright(){
	$("#righttable").html('');
	$("#righttable").html('<table '+
			' class="table table-hover table-striped table-bordered table-condensed" '+
				' id="storageOutItemdiv" style="font-size: 14px;"></table>');
	var flg=false;
	var jk=false;
	var sj=false;
	var sx=false;
	var type = $("#techJkServiceTask_type").val();
	if(type=="0"){//质检任务单
		flg=true;
	}else if(type=="1"){//建库任务单
		jk=true;
	}else if(type=="2"){//上机任务单
		sj=true;
	}else if(type=="3"){//生信任务单
		sx=true;
	}
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title":  biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false
	});
	if(!sx){
		colOpts.push({
			"data": "code",
			"title":  biolims.common.code,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "code");
				$(td).attr("sampleCode", rowData['sampleCode']);
			},
		});
	}
	
	if(sx){
		colOpts.push({
			"data": "sampleCode",
			"title":  biolims.common.code,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sampleCode");
			},
		});
	}
	
	colOpts.push({
		"data": "groupNo",
		"title": biolims.common.packetNumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "groupNo");
		},
		"visible": flg
	});
	
	colOpts.push({
		"data": "indexa",
		"title": "index",
		"createdCell": function(td) {
			$(td).attr("saveName", "indexa");
		},
		"visible": sj
	});
	
	colOpts.push({
		"data": "scopeId",
		"title":biolims.purchase.costCenterId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "scopeId");
		},
		"visible": true
	});
	
	
	colOpts.push({
		"data": "scopeName",
		"title":biolims.purchase.costCenter,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "scopeName");
		},
		"visible": true
	});
	if(!sx){
		colOpts.push({
			"data": "wgcId",
			"className":"edit",
			"title": biolims.common.poolingGroup,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "wgcId");
				$(td).attr("barCode", rowData['barCode']);
			},
		});
		colOpts.push({
			"data": "project-id",
			"title": biolims.common.contractCodeId,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "project-id");
				$(td).attr("project-name", rowData['project-name']);
			},
		});
		colOpts.push({
			"data": "orderId",
			"title": biolims.sample.orderNum,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "orderId");
				$(td).attr("inwardCode", rowData['inwardCode']);
			},
		});
		colOpts.push({
			"data": "sampleType",
			"title": biolims.common.sampleType,
			"createdCell": function(td) {
				$(td).attr("saveName", "sampleType");
			},
		});
		colOpts.push({
			"data": "species",
			"title": biolims.wk.species,
			"createdCell": function(td) {
				$(td).attr("saveName", "species");
			},
		});
	}
	colOpts.push({
		"data": "orderType",
		"title": biolims.common.qcType,
		"className": "select",
		"name": "DNA抽提|RNA抽提|DNA QC|RNA QC|LIBRARY QC",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderType");
			$(td).attr("selectOpt", "DNA抽提|RNA抽提|DNA QC|RNA QC|LIBRARY QC");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "DNA抽提";
			}else if(data == "2") {
				return "RNA抽提";
			}else if(data == "3") {
				return "DNA QC";
			}else if(data == "4") {
				return "RNA QC";
			}else if(data == "5") {
				return "LIBRARY QC";
			}else {
				return '';
			}
		},
		"visible": flg
	});
	if(!sx){
		colOpts.push({
			"data": "dataTraffic",
			"title": biolims.common.throughput,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "dataTraffic");
			},
		});
		colOpts.push({
			"data": "productName",
			"title": biolims.common.productName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "productName");
				$(td).attr("sampleInfoIn-id",rowData['sampleInfoIn-id']);
				$(td).attr("tempId", rowData['tempId']);
				$(td).attr("productId", rowData['productId']);
			},
		});
	}
	colOpts.push({
		"data": "isStain",
		"title": biolims.common.ifThereStain,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isStain");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}else if(data == "0") {
				return biolims.common.no;
			}else {
				return '';
			}
		},
		"visible": flg
	});
	colOpts.push({
		"data": "isNeedStain",
		"title": biolims.common.ifThereNeedStain,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isNeedStain");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}else if(data == "0") {
				return biolims.common.no;
			}else {
				return '';
			}
		},
		"visible": flg
	});
	
	colOpts.push({
		"data": "probeCode-name",
		"title": biolims.pooling.probeCodeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "probeCode-name");
			$(td).attr("probeCode-id",rowData['probeCode-id']);
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "gc",
		"title": 'GC%',
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "gc");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "phix",
		"title": 'phix%',
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "phix");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "q30",
		"title": 'Q30',
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "q30");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "dataNum",
		"title": biolims.common.dataNum,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dataNum");
		},
		"visible": jk
	});
	
	if(!sx){
		colOpts.push({
			"data": "crmCustomer-name",
			"title": biolims.sample.customerName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "crmCustomer-name");
				$(td).attr("crmCustomer-id",rowData['crmCustomer-id']);
			},
		});
		
		colOpts.push({
			"data": "crmDoctor-name",
			"title": biolims.common.customName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "crmDoctor-name");
				$(td).attr("crmDoctor-id",rowData['crmDoctor-id']);
			},
		});
		
		colOpts.push({
			"data": "concentration",
			"title": biolims.common.concentration,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "concentration");
				$(td).attr("name",rowData['name']);
				$(td).attr("location",rowData['location']);
			},
		});
		
		colOpts.push({
			"data": "volume",
			"title": biolims.common.volume,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "volume");
			},
		});
		
		colOpts.push({
			"data": "sampleNum",
			"title": biolims.common.sumNum,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sampleNum");
				$(td).attr("acceptDate",rowData['acceptDate']);
				$(td).attr("reportDate",rowData['reportDate']);
				$(td).attr("experimentUser-id",rowData['experimentUser-id']);
				$(td).attr("experimentUser-name",rowData['experimentUser-name']);
				$(td).attr("endDate",rowData['endDate']);
				$(td).attr("classify",rowData['classify']);
				$(td).attr("dataType",rowData['dataType']);
			},
		});
	}
	
	colOpts.push({
		"data": "blendLane",
		"title": biolims.sequencing.mixLane,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "blendLane");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "blendFc",
		"title": biolims.sequencing.mixFlowcell,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "blendFc");
		},
		"visible": jk
	});
	
	colOpts.push({
		"data": "techJkServiceTask-name",
		"title": biolims.common.relatedMainTableName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "techJkServiceTask-name");
			$(td).attr("techJkServiceTask-id",rowData['techJkServiceTask-id']);
		},
	});
	
	if(!sx){
		colOpts.push({
			"data": "nextFlow",
			"title": biolims.common.nextFlow,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "nextFlow");
				$(td).attr("nextFlowId",rowData['nextFlowId']);
			},
		});
		colOpts.push({
			"data": "note",
			"title": biolims.common.note,
			"className":"edit",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "note");
				$(td).attr("khysConcentration",rowData['khysConcentration']);
				$(td).attr("khysVolume",rowData['khysVolume']);
				$(td).attr("tissueId",rowData['tissueId']);
				$(td).attr("extraction",rowData['extraction']);
				$(td).attr("qcProtocol",rowData['qcProtocol']);
				$(td).attr("sequencingApplication",rowData['sequencingApplication']);
				$(td).attr("sampleReceivedDate",rowData['sampleReceivedDate']);
				$(td).attr("specialRequirements",rowData['specialRequirements']);
				$(td).attr("state",rowData['state']);
				$(td).attr("stateName",rowData['stateName']);
			},
		});
		
	}
	
	if(sx){
		colOpts.push({
			"data": "orderCode",
			"title": biolims.common.orderCode,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "orderCode");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "projectId",
			"title": biolims.common.projectId,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "projectId");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "sampleID",
			"title": biolims.wk.wkCode,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sampleID");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "sxProductName",
			"title": biolims.common.productName,
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "sxProductName");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "runId",
			"title": "RUN_ID",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "runId");
			},
			"visible": sx
		});
		colOpts.push({
			"data": "laneId",
			"title": "laneId",
			"className":"edit",
			"createdCell": function(td, data, rowData) {
				$(td).attr("saveName", "laneId");
				$(td).attr("sxSampleCode",rowData['sxSampleCode']);
			},
			"visible": sx
		});
		
	}
	
	
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#storageOutItemdiv"),
				"/technology/wk/techJkServiceTask/delTechJkServiceTaskItem.action");
		}
	});
	if(sx){
		tbarOpts.push({
			text:biolims.common.volumeContract,
			action: function() {
				var rows = $("#storageOutItemdiv .selected");
				var length = rows.length;
				if(!length) {
					top.layer.msg(biolims.common.pleaseSelect);
					return false;
				}
				top.layer.open({
					title: biolims.crm.selectContract,
					type: 2,
					area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
					btn: biolims.common.confirmSelected,
					content: [window.ctx + "/crm/project/showProjectSelectNewList.action", ''],
					yes: function(index, layero) {
						var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(
							0).text();
						var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(1).text();
						rows.addClass("editagain");
						rows.find("td[savename='projectId']").text(id);
						top.layer.close(index)
					},
				})
			}
		});
	}else{
		tbarOpts.push({
			text:biolims.common.productName,
			action: function() {
				var rows = $("#storageOutItemdiv .selected");
				var length = rows.length;
				if(!length) {
					top.layer.msg(biolims.common.pleaseSelect);
					return false;
				}
				top.layer.open({
					title: biolims.crm.selectProduct,
					type: 2,
					area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
					btn: biolims.common.selected,
					content: window.ctx +
						"/com/biolims/system/product/showProductSelTree.action",
					yes: function(index, layero) {
						var name=[],id=[];
						$(".layui-layer-iframe", parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function (i,v) {
							name.push($(v).children("td").eq(2).text());
							id.push($(v).children("td").eq(1).text());
						});
						rows.addClass("editagain");
						rows.find("td[savename='productName']").attr(
							"productId", id.join(",")).text(name.join(","));
						rows.find("td[savename='productId']").text(id.join(","));
						top.layer.close(index);
					},
					cancel: function(index, layero) {
						top.layer.close(index);
					}
				})
			}
		});
		tbarOpts.push({
			text:biolims.common.volumeContract,
			action: function() {
				var rows = $("#storageOutItemdiv .selected");
				var length = rows.length;
				if(!length) {
					top.layer.msg(biolims.common.pleaseSelect);
					return false;
				}
				top.layer.open({
					title: biolims.crm.selectContract,
					type: 2,
					area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
					btn: biolims.common.confirmSelected,
					content: [window.ctx + "/crm/project/showProjectSelectNewList.action", ''],
					yes: function(index, layero) {
						var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(
							0).text();
						var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(1).text();
						rows.addClass("editagain");
						rows.find("td[savename='project-name']").attr(
							"project-id", id).text(name);
						rows.find("td[savename='project-id']").text(id);
						top.layer.close(index)
					},
				})
			}
		});
		if(flg){
			tbarOpts.push({
				text:biolims.common.batchNextStep,
				action: function() {
					var rows = $("#storageOutItemdiv .selected");
					var length = rows.length;
					if(!length) {
						top.layer.msg(biolims.common.pleaseSelect);
						return false;
					}
					top.layer.open({
						title: biolims.common.selectNextFlow,
						type: 2,
						area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
						btn: biolims.common.confirmSelected,
						content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=ZJRWD", ''],
						yes: function(index, layero) {
							var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
								0).text();
							var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
							rows.addClass("editagain");
							rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(id);
							rows.find("td[savename='nextFlow']").text(name);
							top.layer.close(index);
						},
					})
				}
			});
		}else if(jk){
			tbarOpts.push({
				text:biolims.common.batchNextStep,
				action: function() {
					var rows = $("#storageOutItemdiv .selected");
					var length = rows.length;
					if(!length) {
						top.layer.msg(biolims.common.pleaseSelect);
						return false;
					}
					top.layer.open({
						title: biolims.common.selectNextFlow,
						type: 2,
						area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
						btn: biolims.common.confirmSelected,
						content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=JKRWD", ''],
						yes: function(index, layero) {
							var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
								0).text();
							var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
							rows.addClass("editagain");
							rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(id);
							rows.find("td[savename='nextFlow']").text(name);
							top.layer.close(index);
						},
					})
				}
			});
		}else if(sj){
			tbarOpts.push({
				text:biolims.common.batchNextStep,
				action: function() {
					var rows = $("#storageOutItemdiv .selected");
					var length = rows.length;
					if(!length) {
						top.layer.msg(biolims.common.pleaseSelect);
						return false;
					}
					top.layer.open({
						title: biolims.common.selectNextFlow,
						type: 2,
						area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
						btn: biolims.common.confirmSelected,
						content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=SJRWD", ''],
						yes: function(index, layero) {
							var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
								0).text();
							var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
							rows.addClass("editagain");
							rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(id);
							rows.find("td[savename='nextFlow']").text(name);
							top.layer.close(index);
						},
					})
				}
			});
		}
	}*/
//	tbarOpts.push({
//		text: biolims.common.Editplay,
//		action: function() {
//			editItemLayer($("#storageOutItemdiv"));
//		}
//	});
//	tbarOpts.push({
//		text: biolims.common.batchOrderNumber ,
//		action: sampleNum
//	});
//	tbarOpts.push({
//		text:biolims.common.batchOrderNumber,
//		action: function() {
//			var rows = $("#storageOutItemdiv .selected");
//			var length = rows.length;
//			if(!length) {
//				layer.msg(biolims.common.pleaseSelect);
//				return false;
//			}
////			$("#storageOutItemdiv").find(".selected").children("td").eq(
////					3).text();
//			
//			layer.open({
//				title: biolims.crm.selectOrderId,
//				type: 2,
//				area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
//				btn: biolims.common.confirmSelected,
//				content: [window.ctx + "/crm/project/showProjectObjectivesDialogList.actionid="+$("#storageOutItemdiv").find(".selected").children("td").eq(
//						3).text(), ''],
//				yes: function(index, layero) {
//					var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(
//						0).text();
//					rows.find("td[savename='orderId']").text(id);
//					layer.close(index)
//				},
//			})
//		}
//	});
//	tbarOpts.push({
//		text: biolims.common.save,
//		action: function() {
//			savetechDnaServiceRight();
//		}
//	})
//	tbarOpts.push({
//		text : "产物数量",
//		action : function() {
//			var rows = $("#storageOutItemdiv .selected");
//			var length = rows.length;
//			if (!length) {
//				layer.msg("请选择数据");
//				return false;
//			}
//			layer.open({
//				type : 1,
//				title : "产物数量",
//				content : $('#batch_data'),
//				area:[document.body.clientWidth-600,document.body.clientHeight-200],
//				btn: biolims.common.selected,
//				yes : function(index, layero) {
//					var productNum = $("#productNum").val();
//					rows.addClass("editagain");
//					rows.find("td[savename='productNum']").text(productNum);
//					layer.close(index);
//				}
//			});
//		}
//	})
	/*var storageOutItemOps = table(true, $("#techJkServiceTask_id").text(), "/technology/wk/techJkServiceTask/showTechJkServiceTaskItemNewListJson.action", colOpts, tbarOpts);
	techDnaServiceRight = renderData($("#storageOutItemdiv"), storageOutItemOps);
	//选择数据并提示
	techDnaServiceRight.on('draw', function() {
		var index = 0;
		$("#storageOutItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		
		oldChangeLog = techDnaServiceRight.ajax.json();
	});

}*/
