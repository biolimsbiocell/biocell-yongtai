$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	cxTypeChange();
});

function add() {
	window.location = window.ctx + "/technology/wk/techJkServiceTask/editTechJkServiceTask.action?type="+$("#type2").val();
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/technology/wk/techJkServiceTask/showTechJkServiceTaskList.action?type='+$("#type2").val();
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#techJkServiceTask", {
					userId : userId,
					userName : userName,
					formId : $("#techJkServiceTask_id").val(),
					title : $("#techJkServiceTask_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#techJkServiceTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});



function testUser(){
	var gid=$("#techJkServiceTask_acceptUser").val();
	if(gid!=""){
		var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			"Confirm" : function() {
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					var id=[];
					var name=[];
					for(var i=0;i<selectRecord.length;i++){
						id.push(selectRecord[i].get("user-id"));
						name.push(selectRecord[i].get("user-name"));
					}
						$("#techJkServiceTask_testUserOneId").val(id);
						$("#techJkServiceTask_testUserOneName").val(name);
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}else{
		message(biolims.common.pleaseSelectGroup);
	}
	
}





function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
//	    var techJkServiceTaskItemDivData = $("#techJkServiceTaskItemdiv").data("techJkServiceTaskItemGrid");
		document.getElementById('techJkServiceTaskItemJson').value = commonGetModifyRecords(techJkServiceTaskItemGrid);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/technology/wk/techJkServiceTask/save.action?type="+$("#type2").val();
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		

//function saveItem() {
//	var id = $("#techJkServiceTask_id").val();
//	if (!id) {
//		return;
//	}
//	
////	   var techJkServiceTaskItemDivData = $("#techJkServiceTaskItemdiv").data("techJkServiceTaskItemGrid");
//		var techJkServiceTaskItemJson = commonGetModifyRecords(techJkServiceTaskItemGrid);
//	
//	
//	ajax("post", "/technology/wk/techJkServiceTask/saveAjax.action", {
//		techJkServiceTaskItemJson:techJkServiceTaskItemJson,
//		id : id
//	}, function(data) {
//		if (data.success) {
//			techJkServiceTaskItemDivData.getStore().reload();
//		} else {
//			message(biolims.common.saveFailed);
//		}
//	}, null);
//}

function editCopy() {
	window.location = window.ctx + '/technology/wk/techJkServiceTask/copyTechJkServiceTask.action?id=' + $("#techJkServiceTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#techJkServiceTask_id").val() + "&tableId=TechJkServiceTask");
}
$("#toolbarbutton_status").click(function(){
	var records = techJkServiceTaskItemGrid.store;
	if($("#type2").val()!=3){
		for(var i=0;i<records.getCount();i++){
			if(records.getAt(i).get("nextFlow")==""){
				message(biolims.common.notFillNextStep);
				return;
			}
			
		}
	}

	commonChangeState("formId=" + $("#techJkServiceTask_id").val() + "&tableId=TechJkServiceTask");
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#techJkServiceTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#techJkServiceTask_name").val());
	nsc.push(biolims.common.nameNotEmpty);
	fs.push($("#techJkServiceTask_acceptUser_name").val());
	nsc.push(biolims.common.createUserEmpty);
	fs.push($("#techJkServiceTask_testUserOneName").val());
	nsc.push(biolims.common.testUserEmptyEmpty);
	fs.push($("#techJkServiceTask_sequenceBillDate").val());
	nsc.push(biolims.common.stopDateNotEmpty);
	if($("#techJkServiceTask_type").val()=="1"){
		fs.push($("#techJkServiceTask_cxType").val());
		nsc.push(biolims.common.platformTypeNotEmpty);
	}
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.scienceServiceTest,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/technology/wk/techJkServiceTask/showTechJkServiceTaskItemList.action", {
				id : $("#techJkServiceTask_id").val()
			}, "#techJkServiceTaskItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '改变状态'
						});
	item.on('click', changeState);
	
	
	//选择设备	
	function selectinstrument(){ 
			var options = {};
		 options.width = 900;
		 options.height = 500;
		 loadDialogPage(null, "选择设备", "/equipment/main/instrumentSelect.action?p_type=1", {
		 "Confirm" : function() {
		 var operGrid = $("#show_dialog_instrument_div").data("instrumentDialogGrid");
		 var selectRecord = operGrid.getSelectionModel().getSelections();
		 if (selectRecord.length > 0) {
			 $.each(selectRecord, function(i, obj) {
				 document.getElementById("techJkServiceTask_instrument").value=obj.get("id");
				 document.getElementById("techJkServiceTask_instrument_name").value=obj.get("name");
			 });	
		 }else{
		 message(biolims.common.selectYouWant);
		 return;}
		 $(this).dialog("close");}
		 }, true, options);
	}
	//选择实验组用户
	function showexperimentLeader(){
		var type=$("#techJkServiceTask_type").val();
		var gid="";
		if(type==0){
			gid="QC";
		}else{
			gid="LR";
		}
		var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			"Confirm" : function() {
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$("#techJkServiceTask_experimentLeader").val(selectRecord[0].get("user-id"));
					$("#techJkServiceTask_experimentLeader_name").val(selectRecord[0].get("user-name"));
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
		
	}
	
function cxTypeChange(){
	var type=$("#techJkServiceTask_cxType").val();
	if(type=="1"){
		$(".cx1").addClass("hide");
		$(".cx2").addClass("hide");
		$(".cx3").addClass("hide");
	}else if(type=="2"||type=="3"){
		$(".cx1").removeClass("hide");
		$(".cx2").addClass("hide");
		$(".cx3").addClass("hide");
	}else if(type=="4"){
		$(".cx1").addClass("hide");
		$(".cx2").removeClass("hide");
		$(".cx3").addClass("hide");
	}else if(type=="5"||type=="6"){
		$(".cx1").addClass("hide");
		$(".cx2").addClass("hide");
		$(".cx3").removeClass("hide");
	}
}


function showResultFile() {
	var techJkServiceTaskId = $("#techJkServiceTask_id").val();
	if(techJkServiceTaskId == "NEW"){
		message(biolims.common.pleaseSaveTask);
		return
	}
	var win = Ext.getCmp('result_img');
	if (win) {
		win.close();
	}

	var resultFun = new Ext.Window(
			{
				id : 'resultdoc',
				modal : true,
				title : '',
				layout : 'fit',
				width : 900,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action\?modelType=ResultTask&id="+techJkServiceTaskId+"' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						resultFun.close();
					}
				} ]
			});
	resultFun.show();
}

function xmCheck(){
	var win = Ext.getCmp('UserFun');
	if (win) {win.close();}
	var UserFun= new Ext.Window({
	id:'UserFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='/crm/project/showProjectSelectList.action?flag=xmCheng' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 UserFun.close(); }  }]  
	});     
	UserFun.show(); 
}
 function setxmCheng(rec){
	document.getElementById('techJkServiceTask_projectId').value=rec.get('id');
	document.getElementById('techJkServiceTask_projectId_name').value=rec.get('name');
	var win = Ext.getCmp('UserFun')
	if(win){win.close();}
}