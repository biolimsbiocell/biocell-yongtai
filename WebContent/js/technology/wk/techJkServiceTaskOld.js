var techJkServiceTaskGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'projectId-id',
		type : "string"
	});
	fields.push({
		name : 'projectId-name',
		type : "string"
	});
	fields.push({
		name : 'contractId-id',
		type : "string"
	});
	fields.push({
		name : 'contractId-name',
		type : "string"
	});
	fields.push({
		name : 'type',
		type : "string"
	});

	fields.push({
		name : 'productId-id',
		type : "string"
	});
	fields.push({
		name : 'productId-name',
		type : "string"
	});
	fields.push({
		name : 'dataType',
		type : "string"
	});
	fields.push({
		name : 'sequenceBillDate',
		type : "string"
	});
	fields.push({
		name : 'sequenceBillName',
		type : "string"
	});
	fields.push({
		name : 'sequenceType',
		type : "string"
	});
	fields.push({
		name : 'sequenceLength',
		type : "string"
	});
	fields.push({
		name : 'sequencePlatform',
		type : "string"
	});
	fields.push({
		name : 'sampeNum',
		type : "string"
	});
	fields.push({
		name : 'species',
		type : "string"
	});
	fields.push({
		name : 'experimentPeriod',
		type : "string"
	});
	fields.push({
		name : 'experimentEndTime',
		type : "string"
	});
	fields.push({
		name : 'experimentRemarks',
		type : "string"
	});
	fields.push({
		name : 'analysisCycle',
		type : "string"
	});
	fields.push({
		name : 'analysisEndTime',
		type : "string"
	});
	fields.push({
		name : 'anaysisRemarks',
		type : "string"
	});
	fields.push({
		name : 'projectDifference',
		type : "string"
	});
	fields.push({
		name : 'mixRatio',
		type : "string"
	});
	fields.push({
		name : 'coefficient',
		type : "string"
	});
	fields.push({
		name : 'readsOne',
		type : "string"
	});
	fields.push({
		name : 'readsTwo',
		type : "string"
	});
	fields.push({
		name : 'index1',
		type : "string"
	});
	fields.push({
		name : 'index2',
		type : "string"
	});
	fields.push({
		name : 'density',
		type : "string"
	});
	fields.push({
		name : 'orders-id',
		type : "string"
	});
	fields.push({
		name : 'orders-name',
		type : "string"
	});
	fields.push({
		name : 'projectLeader-id',
		type : "string"
	});
	fields.push({
		name : 'projectLeader-name',
		type : "string"
	});
	fields.push({
		name : 'experimentLeader-id',
		type : "string"
	});
	fields.push({
		name : 'experimentLeader-name',
		type : "string"
	});
	fields.push({
		name : 'analysisLeader-id',
		type : "string"
	});
	fields.push({
		name : 'analysisLeader-name',
		type : "string"
	});
	fields.push({
		name : 'techProjectTask-id',
		type : "string"
	});
	fields.push({
		name : 'techProjectTask-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-id',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-name',
		type : "string"
	});
	fields.push({
		name : 'confirmDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 20 * 7,

		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.name,
		width : 20 * 10,

		sortable : true
	});
	cm.push({
		dataIndex : 'projectId-id',
		hidden : true,
		header : biolims.common.projectId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectId-name',
		header : biolims.common.projectName,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'contractId-id',
		hidden : true,
		header : biolims.common.contractCodeId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'contractId-name',
		header : biolims.common.contractId,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});

	var jcstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '测序任务单' ], [ '0', '质检任务单' ],[ '2', '上机任务单' ],[ '3', '生信任务单' ] ]
	});

	var jcComboxFun = new Ext.form.ComboBox({
		store : jcstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});

	cm.push({
		dataIndex : 'type',
		hidden : false,
		header : biolims.common.orderType,
		width : 20 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(jcComboxFun)
	});

	cm.push({
		dataIndex : 'productId-id',
		hidden : true,
		header : biolims.wk.productId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'productId-name',
		header : biolims.wk.productName,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'dataType',
		header : biolims.common.dataType,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sequenceBillDate',
		header : biolims.wk.sequenceBillDate,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sequenceBillName',
		header : biolims.wk.sequenceBillName,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sequenceType',
		header : biolims.wk.sequenceType,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sequenceLength',
		header : biolims.wk.sequenceLength,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sequencePlatform',
		header : biolims.wk.sequencePlatform,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sampeNum',
		header : biolims.common.sampleNum,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'species',
		header : biolims.wk.species,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'experimentPeriod',
		header : biolims.wk.experimentPeriod,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'experimentEndTime',
		header : biolims.wk.experimentEndTime,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'experimentRemarks',
		header : biolims.wk.experimentRemarks,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'analysisCycle',
		header : biolims.wk.analysisCycle,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'analysisEndTime',
		header : biolims.wk.analysisEndTime,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'anaysisRemarks',
		header : biolims.wk.analysisRemarks,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectDifference',
		header : biolims.wk.projectDifference,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'mixRatio',
		header : biolims.wk.mixRatio,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'coefficient',
		header : biolims.wk.coefficient,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'readsOne',
		header : 'readsone',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'readsTwo',
		header : 'readstwo',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'index1',
		header : 'index1',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'index2',
		header : 'index2',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'density',
		header : biolims.wk.density,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'orders-id',
		hidden : true,
		header : biolims.wk.ordersId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'orders-name',
		header : biolims.wk.orders,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectLeader-id',
		hidden : true,
		header : biolims.common.projectLeaderId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectLeader-name',
		header : biolims.common.projectLeader,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'experimentLeader-id',
		hidden : true,
		header : biolims.common.testLeaderId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'experimentLeader-name',
		header : biolims.common.testLeader,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'analysisLeader-id',
		hidden : true,
		header : biolims.common.analysisLeaderId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'analysisLeader-name',
		header : biolims.common.analysisLeader,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'techProjectTask-id',
		hidden : true,
		header : biolims.common.relatedMainProId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'techProjectTask-name',
		header : biolims.common.relatedMainPro,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 50 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : biolims.sample.createUserId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : biolims.sample.createUserName,

		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.sample.createDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-id',
		hidden : true,
		header : biolims.wk.confirmUserId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-name',
		header : biolims.wk.confirmUserName,

		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmDate',
		header : biolims.wk.confirmDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : biolims.common.stateName,
		width : 20 * 6,

		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/technology/wk/techJkServiceTask/showTechJkServiceTaskListJson.action?type="
			+ $("#type").val();
	var opts = {};
	if ($("#type").val() == 0) {
		opts.title = biolims.common.qcTask;
	} else if ($("#type").val() == 1) {
		opts.title =biolims.common.libraryTask;
	} else if ($("#type").val() == 2) {
		opts.title =biolims.common.sequenceTask;
	} else if ($("#type").val() == 3) {
		opts.title =biolims.common.bioinfoTask;
	}

	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	techJkServiceTaskGrid = gridTable("show_techJkServiceTask_div", cols,
			loadParam, opts);
})
function add() {
	window.location = window.ctx
			+ '/technology/wk/techJkServiceTask/editTechJkServiceTask.action?type='
			+ $("#type").val();
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/wk/techJkServiceTask/editTechJkServiceTask.action?id='
			+ id + '&type=' + $("#type").val();
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/wk/techJkServiceTask/viewTechJkServiceTask.action?id='
			+ id + '&type=' + $("#type").val();
}
function exportexcel() {
	techJkServiceTaskGrid.title = biolims.common.exportList;
	var vExportContent = techJkServiceTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"),biolims.common.search,null,
				{"开始检索(Start retrieve)" : function() {
						if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
							var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
							$("#confirmDate1").val(startconfirmDatestr);
						}
						if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
							var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();
							$("#confirmDate2").val(endconfirmDatestr);
						}				
						
						commonSearchAction(techJkServiceTaskGrid);
						$(this).dialog("close");
	
					},
					"清空(Empty)" : function() {
						form_reset();
	
					}
				}, true, option);
	});
});
