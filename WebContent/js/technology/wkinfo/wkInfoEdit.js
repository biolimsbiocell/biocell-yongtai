$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/technology/wkinfo/wkInfo/editWkInfo.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/technology/wkinfo/wkInfo/showWkInfoList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#wkInfo", {
					userId : userId,
					userName : userName,
					formId : $("#wkInfo_id").val(),
					title : $("#wkInfo_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#wkInfo_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
	    var wkInfoItemDivData = $("#wkInfoItemdiv").data("wkInfoItemGrid");
		document.getElementById('wkInfoItemJson').value = commonGetModifyRecords(wkInfoItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/technology/wkinfo/wkInfo/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/technology/wkinfo/wkInfo/copyWkInfo.action?id=' + $("#wkInfo_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#wkInfo_id").val() + "&tableId=wkInfo");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wkInfo_id").val());
	nsc.push(biolims.wk.wkIdIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.wk.libraryInfo,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/technology/wkinfo/wkInfo/showWkInfoItemList.action", {
				id : $("#wkInfo_id").val()
			}, "#wkInfoItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);