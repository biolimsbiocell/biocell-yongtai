var wkInfoItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	   fields.push({
		name:'indexI7',
		type:"string"
	});
	   fields.push({
		name:'indexI5',
		type:"string"
	});
	   fields.push({
		name:'fwType',
		type:"string"
	});
	   fields.push({
		name:'species',
		type:"string"
	});
	   fields.push({
		name:'barcodeNo',
		type:"string"
	});
	   fields.push({
		name:'barcode',
		type:"string"
	});
	   fields.push({
		name:'wkSize',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'hywkConcentration',
		type:"string"
	});
	   fields.push({
		name:'sampleVolume',
		type:"string"
	});
	   fields.push({
		name:'dataNum',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'wkInfo-id',
		type:"string"
	});
	    fields.push({
		name:'wkInfo-name',
		type:"string"
	});
	    
	    fields.push({
			name:'wkCode',
			type:"string"
		});
	    fields.push({
			name:'outCode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:biolims.wk.wkCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'outCode',
		hidden : false,
		header:biolims.common.outCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.subWK,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkType',
		hidden : false,
		header:biolims.wk.wkType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexI7',
		hidden : false,
		header:'index i7',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexI5',
		hidden : false,
		header:'index i5',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fwType',
		hidden : true,
		header:biolims.common.serviceType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'species',
		hidden : true,
		header:biolims.wk.species,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'barcodeNo',
		hidden : true,
		header:'BARCODE NO.',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'barcode',
		hidden : true,
		header:'BARCODE',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkSize',
		hidden : true,
		header:'文库大小(bp,联川)',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'送样体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'送样浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'hywkConcentration',
		hidden : true,
		header:'混样文库每个终浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:'取样体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'dataNum',
		hidden : true,
		header:'所需数据量要求',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkInfo-id',
		hidden : true,
		header:'文库信息ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkInfo-name',
		hidden : true,
		header:biolims.wk.libraryInfo,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/wkinfo/wkInfo/showWkInfoItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.subWK;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/technology/wkinfo/wkInfo/delWkInfoItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wkInfoItemGrid.getStore().commitChanges();
				wkInfoItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.downloadCsvTemplet,
		handler : function(){
			window.location.href= "/js/technology/wkinfo/uploadFileRoot/WkInfoTemplet.csv";
		}
	})
	opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),biolims.common.batchUpload,null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = wkInfoItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o=0;
                			p.set("wkCode",this[o++]);
                			p.set("outCode",this[o++]);
                			p.set("name",this[o++]);
                			p.set("wkType",this[o++]);
                			p.set("indexI7",this[o++]);
                			p.set("indexI5",this[o++]);
                			p.set("note",this[o++]);
							wkInfoItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	wkInfoItemGrid=gridEditTable("wkInfoItemdiv",cols,loadParam,opts);
	$("#wkInfoItemdiv").data("wkInfoItemGrid", wkInfoItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


