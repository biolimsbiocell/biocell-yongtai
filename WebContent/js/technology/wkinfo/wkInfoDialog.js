var wkInfoDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'文库编号1',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:'创建人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建时间',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态描述',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/wkinfo/wkInfo/showWkInfoListJson.action";
	var opts={};
	opts.title="文库信息";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setWkInfoFun(rec);
	};
	wkInfoDialogGrid=gridTable("show_dialog_wkInfo_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索111", null, {
			"开始检索" : function() {
				commonSearchAction(wkInfoDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
