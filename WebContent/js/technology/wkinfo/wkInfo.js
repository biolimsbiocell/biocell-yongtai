var wkInfoGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.wk.wkCode,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.name,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : biolims.sample.createUserId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header :biolims.sample.createUserName,

		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.sample.createDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : biolims.common.stateDescribe,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header :biolims.common.note,
		width : 50 * 6,

		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/technology/wkinfo/wkInfo/showWkInfoListJson.action";
	var opts = {};
	opts.title = biolims.wk.libraryInfo;
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	wkInfoGrid = gridTable("show_wkInfo_div", cols, loadParam, opts);
})
function add() {
	window.location = window.ctx
			+ '/technology/wkinfo/wkInfo/editWkInfo.action';
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/wkinfo/wkInfo/editWkInfo.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/wkinfo/wkInfo/viewWkInfo.action?id=' + id;
}
function exportexcel() {
	wkInfoGrid.title =biolims.common.exportList;
	var vExportContent = wkInfoGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(
			function() {
				var option = {};
				option.width = 542;
				option.height = 417;
				loadDialogPage($("#jstj"),biolims.common.search, null, {
					"开始检索(Start retrieve)" : function() {

//						if (($("#startcreateDate").val() != undefined)
//								&& ($("#startcreateDate").val() != '')) {
//							var startcreateDatestr = ">=##@@##"
//									+ $("#startcreateDate").val();
//							$("#createDate1").val(startcreateDatestr);
//						}
//						if (($("#endcreateDate").val() != undefined)
//								&& ($("#endcreateDate").val() != '')) {
//							var endcreateDatestr = "<=##@@##"
//									+ $("#endcreateDate").val();
//
//							$("#createDate2").val(endcreateDatestr);
//
//						}
						if (($("#startcreateDate").val() != undefined)
								&& ($("#startcreateDate").val() != '')
								&& ($("#endcreateDate").val() != undefined)
								&& ($("#endcreateDate").val() != '')) {
							var startcreateDate= $("#startcreateDate").val();
							var endcreateDate= $("#endcreateDate").val();
							//模糊搜索查询
							Ext.onReady(function() {
								wkInfoGrid.store.proxy.setUrl('/technology/wkinfo/wkInfo/showtechnologyWkinfoListBySearchJson.action');
							var o = {
									startcreateDate : startcreateDate,
									endcreateDate : endcreateDate,
								};
							
							wkInfoGrid.store.load({
									params : o
								});
							
							wkInfoGrid.render();
							});
						} else {

							commonSearchAction(wkInfoGrid);
						}

						$(this).dialog("close");

					},
					"清空(empty)" : function() {
						form_reset();

					}
				}, true, option);
			});
});
