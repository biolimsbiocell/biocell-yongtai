//待入库样本-即库存主数据(树状带批次)
var techDnaServiceAllTab;
hideLeftDiv();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.sample.id,
		"visible": false
	});
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code
	});
	
	colOpts.push({
		"data": "barCode",
		"title": biolims.common.barCode
	});
	
	colOpts.push({
		"data": "pronoun",
		"title": "代次"
	});
	
	colOpts.push({
		"data" : "sampleCode",
		"title" : biolims.common.sampleCode,
		"visible": false
	});
	
	colOpts.push({
		"data" : "dataTraffic",
		"title" : biolims.common.throughput,
	});
	colOpts.push({
		"data" : "productId",
		"title" : biolims.common.productId,
		"visible": false
	});
	
	colOpts.push({
		"data" : "productName",
		"title" : biolims.common.testProject,
	});
	
	colOpts.push({
		"data" : "samplingDate",
		"title" : biolims.sample.samplingDate,
	});
	
	colOpts.push({
		"data" : "sampleInfo-idCard",
		"title" : biolims.common.outCode,
	});
	
	colOpts.push({
		"data" : "sampleType",
		"title" : biolims.common.sampleType,
	});
	
	colOpts.push({
		"data" : "location",
		"title" : biolims.common.location,
	});
	
	colOpts.push({
		"data" : "qpcrConcentration",
		"title" : biolims.QPCR.concentration,
	});
	
	colOpts.push({
		"data" : "indexa",
		"title" : 'index',
	});
	
	colOpts.push({
		"data" : "concentration",
		"title" : biolims.common.concentration,
	});
	
	colOpts.push({
		"data" : "volume",
		"title" : biolims.common.volume,
	});
	
	colOpts.push({
		"data" : "sumTotal",
		"title" : biolims.common.sumNum,
	});
	
	colOpts.push({
		"data" : "sampleInfo-receiveDate",
		"title" : biolims.common.receiveDate,
		"visible": false
	});
	
	colOpts.push({
		"data" : "sampleInfo-project-id",
		"title" : biolims.common.contractCodeId,
	});
	
	colOpts.push({
		"data" : "sampleInfo-project-name",
		"title" : biolims.common.contractName,
	});
	
	colOpts.push({
		"data" : "sellPerson-id",
		"title" : biolims.common.salesId,
		"visible": false
	});
	
	colOpts.push({
		"data" : "sellPerson-name",
		"title" : biolims.common.sales,
		"visible": false
	});
	
	colOpts.push({
		"data" : "sampleInfo-crmCustomer-id",
		"title" : biolims.common.customId,
		"visible": false
	});
	
	colOpts.push({
		"data" : "sampleInfo-crmCustomer-name",
		"title" : biolims.common.custom,
	});
	
	colOpts.push({
		"data" : "sampleInfo-crmDoctor-id",
		"title" : biolims.sample.crmDoctorId,
		"visible": false
	});
	
	colOpts.push({
		"data" : "sampleInfo-crmDoctor-name",
		"title" : biolims.sample.crmDoctorName,
	});
	
	colOpts.push({
		"data": "isRp",
		"title": biolims.common.ifThereStain,
		"name": biolims.common.no+"|"+biolims.common.yes,
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.no;
			}
			if(data == "1") {
				return biolims.common.yes;
			}else {
				return '';
			}
		}
	});
	
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
	});
	
	colOpts.push({
		"data" : "techJkServiceTask-id",
		"title" : biolims.common.qcTask,
	});
	
	colOpts.push({
		"data" : "sampleInfo-name",
		"title" : biolims.common.packetNumber,
	});
	colOpts.push({
		"data" : "scopeId",
		"title" : biolims.purchase.costCenterId,
//		"visible": false
	});
	colOpts.push({
		"data" : "scopeName",
		"title" : biolims.purchase.costCenter,
//		"visible": false
	});
	var tbarOpts = [];
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	//添加明细按钮
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			var sampleId = [];
			$("#storageOutAlldiv .selected").each(function(i, val) {
				sampleId.push($(val).children("td").eq(0).find("input").val());
			});

			// 先保存主表信息
			var name = $("#techDnaServiceTask_name").val();
			var note = $("#techDnaServiceTask_note").val();
			var createUser = $("#techDnaServiceTask_createUser").attr("userId");
			var createDate = $("#techDnaServiceTask_createDate").text();
			var state = $("#techDnaServiceTask_state").attr("state");
			var stateName = $("#techDnaServiceTask_state").text();
			var id = $("#techDnaServiceTask_id").text();
			$.ajax({
				type: 'post',
				url: '/technology/dna/techDnaServiceTask/addPurchaseApplyMakeUp.action',
				data: {
					ids: sampleId,
					id: id,
					note: note,
					createUser: createUser,
					createDate: createDate,
					name: name,
					state:state,
					stateName:stateName
				},
				success: function(data) {
					var data = JSON.parse(data)
					if(data.success) {
						$("#techDnaServiceTask_id").text(data.data);
						var param = {
							id: data.data
						};
						var techDnaServiceRight = $("#storageOutItemdiv")
							.DataTable();
						techDnaServiceRight.settings()[0].ajax.data = param;
						techDnaServiceRight.ajax.reload();
						techDnaServiceAllTab.ajax.reload();
					} else {
						top.layer.msg(biolims.common.addToDetailFailed);
					};
				}
			})
		}
	});
	
	

	var storageOutAllOps = table(true, "",
			"/sample/storage/sampleIn/showSampleWaitOutItemListJson.action", colOpts, tbarOpts);
	techDnaServiceAllTab = renderData($("#storageOutAlldiv"), storageOutAllOps);

	// 选择数据并提示
	techDnaServiceAllTab.on('draw', function() {
		var index = 0;
		$("#storageOutAlldiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#techDnaServiceTask_state").text()=="Complete"){
		settextreadonly();
		$("#save").hide();
		$("#changeState").hide();
	}
});



//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.custom,
		"type" : "input",
		"searchName" : "sampleInfo-crmDoctor-name",
	},{
		"txt" : biolims.common.outCode,
		"type" : "input",
		"searchName" : "sampleInfo-idCard",
	},{
		"txt" : biolims.common.custom+biolims.common.unit,
		"type" : "input",
		"searchName" : "sampleInfo-crmCustomer-name",
	},{
		"txt" : biolims.common.qcTask,
		"type" : "input",
		"searchName" : "techJkServiceTask-id",
	}, {
		"type" : "table",
		"table" : techDnaServiceAllTab
	} ];
}

//大保存
function save() {
		var changeLog = "认领任务单:";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		console.log($("#form1"));
		var jsonStr = JSON.stringify($("#form1").serializeObject());

		var dataItemJson = saveItemjson($("#storageOutItemdiv"));
		var ele = $("#storageOutItemdiv");
		var changeLogItem = biolims.common.claimSampleTask+":";
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItem);
		var changeLogItems = "";
		if(changeLogItem != (biolims.common.claimSampleTask+":")){
			changeLogItems = changeLogItem;
		}
		var name = $("#techDnaServiceTask_name").val();
		var note = $("#techDnaServiceTask_note").val();
		var createUser = $("#techDnaServiceTask_createUser").attr("userId");
		var createDate = $("#techDnaServiceTask_createDate").text();
		var state = $("#techDnaServiceTask_state").attr("state");
		var stateName = $("#techDnaServiceTask_state").text();
		var id = $("#techDnaServiceTask_id").text();
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
			url: ctx + '/technology/dna/techDnaServiceTask/saveStorageOutAndItem.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				ImteJson: dataItemJson,
				bpmTaskId: $("#bpmTaskId").val(),
				changeLogItem: changeLogItems,
				id:id,
				note:note,
				name:name,
				createUser:createUser,
				createDate:createDate,
				state:state,
				stateName:stateName
			},
			success: function(data) {
				if(data.success) {
					top.layer.closeAll();
					var url = "/technology/dna/techDnaServiceTask/editTechDnaServiceTask.action?id=" + data.id;


					window.location.href = url;
				} else {
					top.layer.closeAll();
					top.layer.msg(data.msg);

				}
			}

		});

}

$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

function changeState() {
    var	id=$("#techDnaServiceTask_id").text();
	var paraStr = "formId=" + id +
		"&tableId=TechDnaServiceTask";
	console.log(paraStr);
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});

	
}

var mainFileInput;
function fileUp2() {
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('1', 'techDnaServiceTask', $("#techDnaServiceTask_id").text());
	mainFileInput.off("fileuploaded");
}
function fileView2() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=techDnaServiceTask&id=" + $("#techDnaServiceTask_id").text(),
		cancel: function(index, layer) {
			top.layer.close(index);
		}
	});
}
