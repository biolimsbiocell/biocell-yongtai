var techDnaServiceTaskGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'projectId-id',
		type : "string"
	});
	fields.push({
		name : 'projectId-name',
		type : "string"
	});
	fields.push({
		name : 'createUserOneName',
		type : "string"
	});
	fields.push({
		name : 'contractId-id',
		type : "string"
	});
	fields.push({
		name : 'contractId-name',
		type : "string"
	});
	fields.push({
		name : 'orderType-id',
		type : "string"
	});
	fields.push({
		name : 'orderType-name',
		type : "string"
	});
	fields.push({
		name : 'projectLeader-id',
		type : "string"
	});
	fields.push({
		name : 'projectLeader-name',
		type : "string"
	});
	fields.push({
		name : 'experimentLeader-id',
		type : "string"
	});
	fields.push({
		name : 'experimentLeader-name',
		type : "string"
	});
	fields.push({
		name : 'analysisLeader-id',
		type : "string"
	});
	fields.push({
		name : 'analysisLeader-name',
		type : "string"
	});
	fields.push({
		name : 'techProjectTask-id',
		type : "string"
	});
	fields.push({
		name : 'techProjectTask-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-id',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-name',
		type : "string"
	});
	fields.push({
		name : 'confirmDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 20 * 10,

		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.name,
		width : 20 * 10,

		sortable : true
	});
	cm.push({
		dataIndex : 'projectId-id',
		hidden : true,
		header : biolims.common.projectIdID,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectId-name',
		header : biolims.common.projectId,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'contractId-id',
		hidden : true,
		header : biolims.common.contractCodeId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'contractId-name',
		header : biolims.common.contractId,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderType-id',
		hidden : true,
		header : biolims.common.orderTypeId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderType-name',
		header : biolims.common.orderType,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectLeader-id',
		hidden : true,
		header : biolims.common.projectLeaderId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectLeader-name',
		header : biolims.common.projectLeader,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'experimentLeader-id',
		hidden : true,
		header : biolims.common.testLeaderId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'experimentLeader-name',
		header : biolims.common.testLeader,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'analysisLeader-id',
		hidden : true,
		header : biolims.common.analysisLeaderId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'analysisLeader-name',
		header : biolims.common.analysisLeader,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'techProjectTask-id',
		hidden : true,
		header : biolims.common.relatedMainProId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'techProjectTask-name',
		header : biolims.common.relatedMainPro,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 50 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : biolims.sample.createUserId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : biolims.sample.createUserName,

		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.sample.createDate,
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-id',
		hidden : true,
		header : biolims.wk.approverId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-name',
		header : biolims.wk.approver,
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmDate',
		header : biolims.wk.approverDate,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : biolims.common.stateName,
		width : 20 * 6,
		hidden : false,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/technology/dna/techDnaServiceTask/showTechDnaServiceTaskListJson.action";
	var opts = {};
	opts.title = biolims.common.claimTask;
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	techDnaServiceTaskGrid = gridTable("show_techDnaServiceTask_div", cols,
			loadParam, opts);
});
function add() {
	window.location = window.ctx
			+ '/technology/dna/techDnaServiceTask/editTechDnaServiceTask.action';
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/dna/techDnaServiceTask/editTechDnaServiceTask.action?id='
			+ id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/dna/techDnaServiceTask/viewTechDnaServiceTask.action?id='
			+ id;
}
function exportexcel() {
	techDnaServiceTaskGrid.title = biolims.common.exportList;
	var vExportContent = techDnaServiceTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {

				// if (($("#startconfirmDate").val() != undefined) &&
				// ($("#startconfirmDate").val() != '')) {
				// var startconfirmDatestr = ">=##@@##" +
				// $("#startconfirmDate").val();
				// $("#confirmDate1").val(startconfirmDatestr);
				// }
				// if (($("#endconfirmDate").val() != undefined) &&
				// ($("#endconfirmDate").val() != '')) {
				// var endconfirmDatestr = "<=##@@##" +
				// $("#endconfirmDate").val();
				//
				// $("#confirmDate2").val(endconfirmDatestr);
				//
				// }
				// if (($("#startcreateDate").val() != undefined)
				// || ($("#startcreateDate").val() != '')
				// || ($("#endcreateDate").val() != undefined)
				// || ($("#endcreateDate").val() != '')
				// ||($("#startconfirmDate").val() != undefined)
				// || ($("#startconfirmDate").val() != '')
				// || ($("#endconfirmDate").val() != undefined)
				// || ($("#endconfirmDate").val() != '')) {
				// var startcreateDate= $("#startcreateDate").val();
				// var endcreateDate= $("#endcreateDate").val();
				// var startconfirmDate= $("#startconfirmDate").val();
				// var endconfirmDate= $("#endconfirmDate").val();
				// //模糊搜索查询
				// Ext.onReady(function() {
				// techDnaServiceTaskGrid.store.proxy.setUrl('/technology/dna/techDnaServiceTask/showtechnologyTechDnaServiceTaskListBySearchJson.action');
				// var o = {
				// startcreateDate : startcreateDate,
				// endcreateDate : endcreateDate,
				// startconfirmDate : startconfirmDate,
				// endconfirmDate : endconfirmDate
				// };
				//					
				// techDnaServiceTaskGrid.store.load({
				// params : o
				// });
				//					
				// techDnaServiceTaskGrid.render();
				// });
				// } else {
				commonSearchAction(techDnaServiceTaskGrid);
				// }
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
