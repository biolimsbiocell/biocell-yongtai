var showTechDnaServiceTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'projectId-id',
		type:"string"
	});
	    fields.push({
		name:'projectId-name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.describe,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'projectId-id',
		header:biolims.common.projectIdID,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'projectId-name',
		header:biolims.common.projectId,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/dna/techDnaServiceTask/showTechDnaServiceTaskDialogJson.action";
	var opts={};
	opts.title=biolims.common.selectExtract;
	opts.width = document.body.clientWidth - 550;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setTechDnaServiceTaskFun(rec);
	};
	showTechDnaServiceTaskDialogGrid=gridTable("show_dialog_techDnaServiceTask_div1",cols,loadParam,opts);
	$("#show_dialog_techDnaServiceTask_div1").data("showTechDnaServiceTaskDialogGrid", showTechDnaServiceTaskDialogGrid);
});
//function sc(){
//		var option = {};
//		option.width = 542;
//		option.height = 417;
//		loadDialogPage($("#jstj"), "搜索", null, {
//			"开始检索" : function() {
//				commonSearchAction(techDnaServiceTaskDialogGrid);
//				$(this).dialog("close");
//
//			},
//			"清空" : function() {
//				form_reset();
//
//			}
//		}, true, option);
//	}
