var sampleInfoDialogGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'sampleType2',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'receiveDate',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'id',
		width : 120,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'code',
		header : biolims.common.code,
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.sampleName,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'sampleType2',
		header : biolims.common.sampleType,
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'productId',
		header : biolims.common.projectIdID,
		width : 120,
		hidden:true,
		sortable : true
	});
	cm.push({
		dataIndex : 'productName',
		header : biolims.common.projectId,
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'receiveDate',
		header : biolims.common.receiveDate,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'reportDate',
		header : biolims.common.reportDate,
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'sampleNum',
		header : biolims.common.sampleNum,
		width : 120,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/technology/dna/techDnaServiceTask/showSampleInfoListJson.action?id="+$("#id").val();
	loadParam.limit = 200;
	var opts = {};
	opts.title = biolims.common.selectSampleCode;
	opts.width = document.body.clientWidth - 250;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect = function(id) {
		$("#id").val(id);
	};
	opts.rowdblclick = function(id) {
		$("#id").val(id);
		view();
	};
	sampleInfoDialogGrid = gridTable("template_wait_grid_div", cols, loadParam, opts);
	$("#template_wait_grid_div").data("sampleInfoDialogGrid", sampleInfoDialogGrid);
	opts.tbar = [];
});