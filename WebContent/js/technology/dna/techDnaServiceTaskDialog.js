var techDnaServiceTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'projectId-id',
		type:"string"
	});
	    fields.push({
		name:'projectId-name',
		type:"string"
	});
	    fields.push({
		name:'contractId-id',
		type:"string"
	});
	    fields.push({
		name:'contractId-name',
		type:"string"
	});
	    fields.push({
		name:'orderType-id',
		type:"string"
	});
	    fields.push({
		name:'orderType-name',
		type:"string"
	});
	    fields.push({
		name:'projectLeader-id',
		type:"string"
	});
	    fields.push({
		name:'projectLeader-name',
		type:"string"
	});
	    fields.push({
		name:'experimentLeader-id',
		type:"string"
	});
	    fields.push({
		name:'experimentLeader-name',
		type:"string"
	});
	    fields.push({
		name:'analysisLeader-id',
		type:"string"
	});
	    fields.push({
		name:'analysisLeader-name',
		type:"string"
	});
	    fields.push({
		name:'techProjectTask-id',
		type:"string"
	});
	    fields.push({
		name:'techProjectTask-name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'projectId-id',
		header:biolims.common.projectIdID,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'projectId-name',
		header:biolims.common.projectId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'contractId-id',
		header:biolims.common.contractCodeId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'contractId-name',
		header:biolims.common.contractId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'orderType-id',
		header:biolims.common.orderTypeId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'orderType-name',
		header:biolims.common.orderType,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'projectLeader-id',
		header:biolims.common.projectLeaderId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'projectLeader-name',
		header:biolims.common.projectLeader,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'experimentLeader-id',
		header:biolims.common.testLeaderId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'experimentLeader-name',
		header:biolims.common.testLeader,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'analysisLeader-id',
		header:biolims.common.analysisLeaderId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'analysisLeader-name',
		header:biolims.common.analysisLeader,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'techProjectTask-id',
		header:biolims.common.relatedMainProId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'techProjectTask-name',
		header:biolims.common.relatedMainPro,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		header:biolims.wk.approverId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.approver,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.wk.approverDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/dna/techDnaServiceTask/showTechDnaServiceTaskListJson.action";
	var opts={};
	opts.title=biolims.common.scienceServiceDNA;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setTechDnaServiceTaskFun(rec);
	};
	techDnaServiceTaskDialogGrid=gridTable("show_dialog_techDnaServiceTask_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(techDnaServiceTaskDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)": function() {
				form_reset();

			}
		}, true, option);
	}
