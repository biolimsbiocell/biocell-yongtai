var techDnaServiceRight, oldChangeLog;
//出库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title":  biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "sampleInfoIn-code",
		"title": biolims.common.code,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleInfoIn-code");
			$(td).attr("sampleInfoIn-id",rowData['sampleInfoIn-id']);
		},
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		},
		"visible": false
	});
	colOpts.push({
		"data": "groupNo",
		"title": biolims.common.packetNumber,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "groupNo");
		},
	});
	colOpts.push({
		"data": "barCode",
		"title": biolims.common.barCode,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "barCode");
		},
		"visible": false
	});
	colOpts.push({
		"data": "project-id",
		"title": biolims.common.contractCodeId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "project-id");
		},
	});
	
	colOpts.push({
		"data": "project-name",
		"title":  biolims.common.contractName,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "project-name");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "orderId",
		"title":  biolims.purchase.orderNum,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "orderId");
		},
		"className":"edit",
	});
	
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleType");
		},
	});
	colOpts.push({
		"data": "sampleInfoIn-sampleInfo-species",
		"title": biolims.wk.species,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sampleInfo-species");
		},
	});
	
	colOpts.push({
		"data": "sampleInfoIn-customer-id",
		"title": biolims.common.unitId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-customer-id");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "sampleInfoIn-customer-name",
		"title": biolims.common.unit,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-customer-name");
		},
	});
	colOpts.push({
		"data": "sampleInfoIn-sampleInfo-crmDoctor-id",
		"title":biolims.common.customId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sampleInfo-crmDoctor-id");
		},
		"visible": false
	});
	colOpts.push({
		"data": "sampleInfoIn-sampleInfo-crmDoctor-name",
		"title": biolims.common.custom,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sampleInfo-crmDoctor-name");
		},
	});
	colOpts.push({
		"data": "sampleReceiveDate",
		"title":biolims.common.receiveDate,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleReceiveDate");
		},
	});
	
		colOpts.push({
		"data": "sonWkName",
		"title":biolims.common.subWK,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sonWkName");
		},
		"visible": false
	});
		colOpts.push({
		"data": "indexi7",
		"title":"index i7",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "indexi7");
		},
		"visible": false
	});
		colOpts.push({
		"data": "indexi5",
		"title":"index i5",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "indexi5");
		},
		"visible": false
	});
		colOpts.push({
		"data": "sampleInfoIn-location",
		"title":biolims.common.location,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-location");
		},
		"visible": false
	});
		colOpts.push({
		"data": "sampleInfoIn-concentration",
		"title":biolims.common.concentration,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-concentration");
		},
		"visible": false
	});
		colOpts.push({
		"data": "sampleInfoIn-volume",
		"title":biolims.common.volume,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-volume");
		},
		"visible": false
	});
		colOpts.push({
		"data": "sampleInfoIn-sumTotal",
		"title":biolims.common.sumNum,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sumTotal");
		},
		"visible": false
	});
		colOpts.push({
		"data": "sampleInfoIn-customer-street",
		"title":biolims.sample.customerStreet,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-customer-street");
		},
		"visible": false
	});
		colOpts.push({
		"data": "sampleInfoIn-customer-postcode",
		"title":biolims.sample.customerPostcode,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-customer-postcode");
		},
		"visible": false
	});
		colOpts.push({
			"data": "scopeId",
			"title":biolims.purchase.costCenterId,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "scopeId");
			},
//			"visible": false
		});
		
		
		colOpts.push({
			"data": "scopeName",
			"title":biolims.purchase.costCenter,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "scopeName");
			},
//			"visible": false
		});
		colOpts.push({
		"data": "sampleInfoIn-sampleInfo-project-id",
		"title":biolims.sample.projectId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sampleInfo-project-id");
		},
		"visible": false
	});
	
	
	colOpts.push({
		"data": "sampleInfoIn-sampleInfo-project-name",
		"title":biolims.sample.projectName,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sampleInfo-project-name");
		},
		"visible": false
	});
	colOpts.push({
		"data": "sampleInfoIn-sellPerson-id",
		"title":biolims.common.salesId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sellPerson-id");
		},
		"visible": false
	});
	colOpts.push({
		"data": "sampleInfoIn-sellPerson-name",
		"title":biolims.common.sales,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-sellPerson-name");
		},
		"visible": false
	});
	colOpts.push({
		"data": "sampleInfoIn-tempId",
		"title":biolims.sample.inStorageTempId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleInfoIn-tempId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "productId",
		"title":biolims.common.productId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "productName",
		"title":biolims.common.productName,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productName");
		},
		"visible": false
	});
	colOpts.push({
		"data": "acceptDate",
		"title":biolims.common.acceptDate,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "acceptDate");
		},
		"visible": false
	});
	colOpts.push({
		"data": "reportDate",
		"title":biolims.common.reportDate,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "reportDate");
		},
		"visible": false
	});
	colOpts.push({
		"data": "sampleNum",
		"title":biolims.common.sampleNum,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleNum");
		},
		"visible": false
	});
	colOpts.push({
		"data": "techDnaServiceTask-id",
		"title":biolims.common.relatedMainTableId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "techDnaServiceTask-id");
		},
		"visible": false
	});
	colOpts.push({
		"data": "techDnaServiceTask-name",
		"title":biolims.common.relatedMainTableName,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "techDnaServiceTask-name");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "orderType",
		"title":biolims.common.orderType,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "orderType");
		},
		"visible": false
	});
	colOpts.push({
		"data": "dataTraffic",
		"title":biolims.common.throughput,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dataTraffic");
		},
	});
	colOpts.push({
		"data": "nextFlowId",
		"title":biolims.common.nextFlowId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "nextFlowId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "nextFlow",
		"title":biolims.common.nextFlow,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "nextFlow");
		},
		"visible": false
	});
	colOpts.push({
		"data": "classify",
		"title":biolims.common.classify,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "classify");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "state",
		"title":biolims.common.state,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "state");
		}
	});
	
	colOpts.push({
		"data": "name",
		"title":biolims.common.name,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "name");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "sampleOrder-id",
		"title":"关联订单",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	
	colOpts.push({
		"data": "note",
		"title":biolims.common.note,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "note");
		},
		"className":"edit"
	});
	
	
	
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#storageOutItemdiv"),
				"/technology/dna/techDnaServiceTask/delTechDnaServiceTaskItem.action");
		}
	});
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#storageOutItemdiv"));
		}
	});
	tbarOpts.push({
		text:biolims.common.volumeContract,
		action: function() {
			var rows = $("#storageOutItemdiv .selected");
			var length = rows.length;
			if(!length) {
				top.layer.msg(biolims.common.pleaseSelect);
				return false;
			}
			top.layer.open({
				title: biolims.crm.selectContract,
				type: 2,
				area: ["650px", "400px"],
				btn: biolims.common.confirmSelected,
				content: [window.ctx + "/crm/project/showProjectSelectNewList.action", ''],
				yes: function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(
						0).text();
					var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmProjectTable .chosed").children("td").eq(1).text();
					rows.addClass("editagain");
					rows.find("td[savename='project-name']").text(name);
					rows.find("td[savename='project-id']").text(id);
					top.layer.close(index)
				},
			})
		}
	});
	tbarOpts.push({
		text: "选择订单",
		action: function() {
			selectOrder();
		}
	});

	var storageOutItemOps = table(true, $("#techDnaServiceTask_id").text(), "/technology/dna/techDnaServiceTask/showTechDnaServiceTaskItemListJson.action", colOpts, tbarOpts);
	techDnaServiceRight = renderData($("#storageOutItemdiv"), storageOutItemOps);
	//选择数据并提示
	techDnaServiceRight.on('draw', function() {
		var index = 0;
		$("#storageOutItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		
		oldChangeLog = techDnaServiceRight.ajax.json();
	});
});

//选择订单
function selectOrder(){
	var rows = $("#storageOutItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
			.eq(0).text();
			rows.find("td[savename='sampleOrder-id']").text(id);
			rows.addClass("editagain");
			top.layer.closeAll();
		},
	})
}

// 保存
function savetechDnaServiceRight() {
    var ele=$("#storageOutItemdiv");
	var changeLog = biolims.common.claimSampleTask+":";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/storage/out/saveStorageOutItemTable.action',
		data: {
			id: $("#storageOut_id").text(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if(k == "sampleInfoIn-code") {
				json["sampleInfoIn-id"] = $(tds[j]).attr("sampleInfoIn-id");
				continue;
			}
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += 'ID为"' + id  + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
//样本数量
function sampleNum() {
	var rows = $("#storageOutItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var num = 1;
	top.layer.open({
		title: biolims.common.batchOrderNumber,
		type: 1,
		area: ["30%", "30%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: ' <span class=""></span><input type="text"  class="form-control" id="sampleNum">',
//		success: function() {
//			$("#subSampleNum").click(function() {
//				num = parseInt($("#sampleNum").val()) - 1;
//				$("#sampleNum").val(num);
//			});
//			$("#addSampleNum").click(function() {
//				num = parseInt($("#sampleNum").val()) + 1;
//				$("#sampleNum").val(num);
//			});
//		},
		yes: function(index, layer) {
			rows.find("td[savename='orderId']").text(parseInt($("#sampleNum").val()));
			top.layer.close(index)
		},
	})

}
