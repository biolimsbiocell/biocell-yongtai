var techDnaServiceTaskItemGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'yunKangCode',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'techDnaServiceTask-id',
		type : "string"
	});
	fields.push({
		name : 'techDnaServiceTask-name',
		type : "string"
	});
	fields.push({
		name : 'orderType',
		type : "string"
	});
	fields.push({
		name : 'dataTraffic',
		type : "string"
	});
	fields.push({
		name : 'classify',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'nextFlowId',
		type : "string"
	});
	fields.push({
		name : 'nextFlow',
		type : "string"
	});
	fields.push({
		name : 'sampleType',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'acceptDate',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});

	fields.push({
		name : 'sampleInfoIn-id',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-code',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-location',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-tempId',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-concentration',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-volume',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sumTotal',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-customer-id',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-customer-name',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-customer-street',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-customer-postcode',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sampleInfo-project-id',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sampleInfo-project-name',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sampleInfo-crmDoctor-id',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sampleInfo-crmDoctor-name',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sampleInfo-species',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sellPerson-id',
		type : "string"
	});
	fields.push({
		name : 'sampleInfoIn-sellPerson-name',
		type : "string"
	});
	fields.push({
		name : 'contractId',
		type : "string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'sampleReceiveDate',
		type : "string"
	});
	fields.push({
		name : 'sonWkName',
		type : "string"
	});
	fields.push({
		name : 'indexi7',
		type : "string"
	});
	fields.push({
		name : 'indexi5',
		type : "string"
	});
	fields.push({
		name : 'project-id',
		type : "string"
	});
	fields.push({
		name : 'project-name',
		type : "string"
	});
	fields.push({
		name : 'groupNo',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'barCode',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.common.id,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleInfoIn-id',
		hidden : true,
		header : biolims.common.tempId,
		width : 20 * 7
	});
	cm.push({
		dataIndex : 'sampleInfoIn-code',
		hidden : false,
		header : biolims.common.code,
		width : 20 * 7
	});

	cm.push({
		dataIndex : 'sampleCode',
		hidden : true,
		header : biolims.common.sampleCode,
		width : 20 * 6
	});
	
	cm.push({
		dataIndex : 'groupNo',
		hidden : false,
		header : biolims.common.packetNumber,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'barCode',
		hidden : true,
		header : biolims.common.barCode,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'project-id',
		header :biolims.common.contractCodeId,
		width : 20 * 6,
		hidden : false
	});
	cm.push({
		dataIndex : 'project-name',
		header :biolims.common.contractName,
		width : 20 * 6,
		hidden : true,
		hidden : true
	});
	cm.push({
		dataIndex : 'orderId',
		hidden : false,
		header : biolims.purchase.orderNum,
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleType',
		hidden : false,
		header : biolims.common.sampleType,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-sampleInfo-species',
		hidden : false,
		header :biolims.wk.species,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-customer-id',
		hidden : true,
		header : biolims.common.unitId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-customer-name',
		hidden : false,
		header : biolims.common.unit,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-sampleInfo-crmDoctor-id',
		hidden : true,
		header : biolims.common.customId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-sampleInfo-crmDoctor-name',
		hidden : false,
		header : biolims.common.custom,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleReceiveDate',
		hidden : false,
		header :biolims.common.receiveDate,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sonWkName',
		hidden : true,
		header : biolims.common.subWK,
		width : 20 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'indexi7',
		hidden : true,
		header : "index i7",
		width : 20 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'indexi5',
		hidden : true,
		header : "index i5",
		width : 20 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleInfoIn-location',
		hidden : true,
		header : biolims.common.location,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-concentration',
		hidden : true,
		header : biolims.common.concentration,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-volume',
		hidden : true,
		header : biolims.common.volume,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-sumTotal',
		hidden : true,
		header : biolims.common.sumNum,
		width : 20 * 6
	});

	cm.push({
		dataIndex : 'sampleInfoIn-customer-street',
		hidden : true,
		header : biolims.sample.customerStreet,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-customer-postcode',
		hidden : true,
		header : biolims.sample.customerPostcode,
		width : 20 * 6
	});

	cm.push({
		dataIndex : 'sampleInfoIn-sampleInfo-project-id',
		hidden : true,
		header : biolims.sample.projectId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-sampleInfo-project-name',
		hidden : true,
		header : biolims.sample.projectName,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-sellPerson-id',
		hidden : true,
		header : biolims.common.salesId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-sellPerson-name',
		hidden : true,
		header :biolims.common.sales,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleInfoIn-tempId',
		hidden : true,
		header : biolims.sample.inStorageTempId,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.productId,
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'productName',
		hidden : true,
		header : biolims.common.productName,
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'acceptDate',
		hidden : true,
		header : biolims.common.acceptDate,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'reportDate',
		hidden : true,
		header : biolims.common.reportDate,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum',
		hidden : true,
		header : biolims.common.sampleNum,
		width : 10 * 6
	});
	cm.push({
		dataIndex : 'techDnaServiceTask-id',
		hidden : true,
		header : biolims.common.relatedMainTableId,
		width : 20 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'techDnaServiceTask-name',
		hidden : true,
		header : biolims.common.relatedMainTableName,
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'orderType',
		hidden : true,
		header : biolims.common.orderType,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'dataTraffic',
		header : biolims.common.throughput,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'nextFlowId',
		hidden : true,
		header : biolims.common.nextFlowId,
		width : 15 * 10,
		sortable : true
	});
	var nextFlowCob = new Ext.form.TextField({
		allowBlank : false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex : 'nextFlow',
		header : biolims.common.nextFlow,
		width : 15 * 10,
		sortable : true,
		hidden : true,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex : 'classify',
		hidden : true,
		header : biolims.common.classify,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : biolims.common.state,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		hidden : true,
		header : biolims.common.name,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : biolims.common.note,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/technology/dna/techDnaServiceTask/showTechDnaServiceTaskItemListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.common.claimSampleTask;
	opts.height = document.body.clientHeight - 100;
	opts.tbar = [];
	if ($("#techDnaServiceTask_state").val() != 1) {
		opts.delSelect = function(ids) {
			ajax(
					"post",
					"/technology/dna/techDnaServiceTask/delTechDnaServiceTaskItem.action",
					{
						ids : ids
					}, function(data) {
						if (data.success) {
							techDnaServiceTaskItemGrid.getStore()
									.commitChanges();
							techDnaServiceTaskItemGrid.getStore().reload();
							message(biolims.common.deleteSuccess);
						} else {
							message(biolims.common.deleteFailed);
						}
					}, null);
		};
		opts.tbar.push({
			text :biolims.common.sampleToBeClaim,
			handler : selectsampleState
		});
		opts.tbar.push({
			text : biolims.sample.inventorySamples,
			handler : selectsampleInItem
		});
		opts.tbar.push({
			text : biolims.common.volumeContract,
			handler : projectFun
		});
		opts.tbar.push({
			text : biolims.common.batchOrderNumber,
			handler : projectorderCodeFun
		});
		opts.tbar.push({
			text : biolims.wk.libraryInfo,
			handler : wkInfo
		});

		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
	}
	techDnaServiceTaskItemGrid = gridEditTable("techDnaServiceTaskItemdiv",
			cols, loadParam, opts);
	$("#techDnaServiceTaskItemdiv").data("techDnaServiceTaskItemGrid",
			techDnaServiceTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

// 待认领样本
function selectsampleState() {
	var title = '';
	var url = '';
	title = biolims.sample.selectInventorySamples;
	url = "/sample/storage/sampleIn/showSampleWaitOutItemList.action?type=1";// type=1
																				// 表示在待认领样本
	var option = {};
	option.width = document.body.clientWidth - 50;
	option.height = document.body.clientHeight - 50;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			var operGrid = $("#sampleWaitOutItemdiv").data(
					"sampleWaitOutItemGrid");
			var ob = techDnaServiceTaskItemGrid.getStore().recordType;
			techDnaServiceTaskItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					var store = techDnaServiceTaskItemGrid.store;
					for ( var a = 0; a < store.getCount(); a++) {
						if (obj.get("id") == store.getAt(a).get(
								"sampleInfoIn-id")) {
							message(biolims.common.haveDuplicate);
							return;
						}
					}
					p.set("sampleInfoIn-code", obj.get("code"));
					p.set("sampleCode", obj.get("sampleCode"));
					p.set("sampleType", obj.get("sampleType"));
					p.set("dataTraffic", obj.get("dataTraffic"));
					p.set("sampleInfoIn-location", obj.get("location"));
					p.set("sampleInfoIn-concentration", obj
							.get("concentration"));
					p.set("sampleInfoIn-volume", obj.get("volume"));
					p.set("sampleInfoIn-sumTotal", obj.get("sumTotal"));
					p.set("name", obj.get("note"));
					p.set("sampleInfoIn-tempId", obj.get("id"));
					p.set("sampleInfoIn-id", obj.get("id"));
					p.set("sampleInfoIn-sampleInfo-project-id", obj
							.get("sampleInfo-project-id"));
					p.set("sampleInfoIn-sampleInfo-project-name", obj
							.get("sampleInfo-project-name"));
					p.set("sampleInfoIn-sampleInfo-crmDoctor-name", obj
							.get("sampleInfo-crmDoctor-name"));
					p.set("sampleInfoIn-sampleInfo-crmDoctor-id", obj
							.get("sampleInfo-crmDoctor-id"));

					p.set("sampleInfoIn-customer-name", obj
							.get("sampleInfo-crmCustomer-name"));
					p.set("sampleInfoIn-customer-id", obj
							.get("sampleInfo-crmCustomer-id"));
					p.set("sampleInfoIn-sellPerson-id", obj
							.get("sellPerson-id"));
					p.set("sampleInfoIn-sellPerson-name", obj
							.get("sellPerson-name"));
					p.set("state", "1");
					p.set("project-id", obj.get("sampleInfo-project-id"));
					p.set("project-name", obj.get("sampleInfo-project-name"));
					p.set("sampleReceiveDate", obj
							.get("sampleInfo-receiveDate"));
					p.set("yunKangCode", obj.get("yunKangCode"));
					p.set("groupNo", obj.get("sampleInfo-name"));

					p.set("barCode", obj.get("barCode"));
					techDnaServiceTaskItemGrid.getStore().add(p);
				});
				techDnaServiceTaskItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}

// 库存样本
function selectsampleInItem() {
	var title = '';
	var url = '';
	title = biolims.sample.selectInventorySamples;
	url = "/sample/storage/sampleIn/showSampleWaitOutItemList.action?type=0";// type=0
																				// 表示在认领任务单是存库样本没有一些搜索项
	var option = {};
	option.width = document.body.clientWidth - 50;
	option.height = document.body.clientHeight - 50;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			var operGrid = $("#sampleWaitOutItemdiv").data(
					"sampleWaitOutItemGrid");
			var ob = techDnaServiceTaskItemGrid.getStore().recordType;
			techDnaServiceTaskItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					var store = techDnaServiceTaskItemGrid.store;
					for ( var a = 0; a < store.getCount(); a++) {
						if (obj.get("id") == store.getAt(a).get(
								"sampleInfoIn-id")) {
							message(biolims.common.haveDuplicate);
							return;
						}
					}
					p.set("sampleInfoIn-code", obj.get("code"));
					p.set("sampleCode", obj.get("sampleCode"));
					p.set("sampleType", obj.get("sampleType"));
					p.set("dataTraffic", obj.get("dataTraffic"));
					p.set("sampleInfoIn-location", obj.get("location"));
					p.set("sampleInfoIn-concentration", obj
							.get("concentration"));
					p.set("sampleInfoIn-volume", obj.get("volume"));
					p.set("sampleInfoIn-sumTotal", obj.get("sumTotal"));
					p.set("name", obj.get("note"));
					p.set("sampleInfoIn-tempId", obj.get("id"));
					p.set("sampleInfoIn-id", obj.get("id"));
					p.set("sampleInfoIn-sampleInfo-project-id", obj
							.get("sampleInfo-project-id"));
					p.set("sampleInfoIn-sampleInfo-project-name", obj
							.get("sampleInfo-project-name"));
					p.set("sampleInfoIn-sampleInfo-crmDoctor-name", obj
							.get("sampleInfo-crmDoctor-name"));
					p.set("sampleInfoIn-sampleInfo-crmDoctor-id", obj
							.get("sampleInfo-crmDoctor-id"));
					p.set("sampleInfoIn-customer-name", obj
							.get("sampleInfo-crmCustomer-name"));
					p.set("sampleInfoIn-customer-id", obj
							.get("sampleInfo-crmCustomer-id"));
					p.set("sampleInfoIn-sellPerson-id", obj
							.get("sellPerson-id"));
					p.set("sampleInfoIn-sellPerson-name", obj
							.get("sellPerson-name"));
					p.set("state", "1");
					p.set("project-id", obj.get("sampleInfo-project-id"));
					p.set("project-name", obj.get("sampleInfo-project-name"));
					p.set("sampleReceiveDate", obj
							.get("sampleInfo-receiveDate"));
					p.set("groupNo", obj.get("sampleInfo-name"));
					p.set("yunKangCode", obj.get("yunKangCode"));
					p.set("barCode", obj.get("barCode"));
					techDnaServiceTaskItemGrid.getStore().add(p);
				});
				techDnaServiceTaskItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}

function wkInfo() {
	var selectRecord = techDnaServiceTaskItemGrid.getSelectionModel()
			.getSelections();
	var id = "";
	if (selectRecord.length > 0) {
		id = selectRecord[0].get("sampleInfoIn-code");
		window.open('/technology/wkinfo/wkInfo/editWkInfo.action?id=' + id, '',
				'');
	} else {
		message(biolims.common.pleaseSelectAPieceOfData);
	}
}

// 科研项目
function projectFun() {

	var selRecords = techDnaServiceTaskItemGrid.getSelectionModel()
			.getSelections();
	for ( var i = 0; i < selRecords.length; i++) {
		var id = selRecords[0].get("sampleInfoIn-customer-id");
		var ids = selRecords[i].get("sampleInfoIn-customer-id");
		if (id != ids) {
			message(biolims.common.customerNotSelect);
			return;
		}
	}
	var crmCus = selRecords[0].get("sampleInfoIn-customer-id");
	var win = Ext.getCmp('projectFun');
	if (win) {
		win.close();
	}
	var projectFun = new Ext.Window(
			{
				id : 'projectFun',
				modal : true,
				title : biolims.crm.selectContract,
				layout : 'fit',
				width : 600,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/project/showProjectSelectList.action?flag=ProjectFun&crmCus="
									+ crmCus
									+ "' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						projectFun.close();
					}
				} ]
			});
	projectFun.show();
}

function setProjectFun(rec) {
	var selRecords = techDnaServiceTaskItemGrid.getSelectionModel()
			.getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('project-id', rec.get("id"));
		obj.set('project-name', rec.get("name"));
	});
	var win = Ext.getCmp('projectFun');
	if (win) {
		win.close();
	}

}

// 选择订单号
function projectorderCodeFun() {
	var selectRecord = techDnaServiceTaskItemGrid.getSelectionModel()
			.getSelections();
	if (selectRecord.length > 0) {
		var title =biolims.crm.selectOrderId;
		var url = '';
		url = "/crm/project/showProjectObjectivesDialogList.action?id="
				+ selectRecord[0].get("project-id");
		var option = {};
		option.width = document.body.clientWidth - 400;
		option.height = document.body.clientHeight - 50;
		loadDialogPage(null, title, url, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_projectObjectives_div").data(
						"projectObjectivesDialogGrid");
				var selectRecord1 = operGrid.getSelectionModel()
						.getSelections();
				if (selectRecord1.length > 0) {
					$.each(selectRecord1, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							b.set("orderId", obj.get("orderCode"));
						});
					});
					$(this).dialog("close");
				} else {
					message(biolims.common.selectYouWant);
					return;
				}
			}
		}, true, option);
		$(this).dialog("close");
	} else {
		message(biolims.common.selectYouWant);
		return;
	}
}
