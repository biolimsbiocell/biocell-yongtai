﻿
var showItemDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		   name:'dataTraffic',
		   type:"string"
	   });
	   fields.push({
		   name:'yunKangCode',
		   type:"string"
	   });
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'techDnaServiceTask-id',
		type:"string"
	});
	    fields.push({
		name:'techDnaServiceTask-name',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
			name:'nextFlowId',
			type:"string"
		});
		   fields.push({
			name:'nextFlow',
			type:"string"
		});
		   fields.push({
				name:'sampleType',
				type:"string"
			});
			   fields.push({
				name:'productId',
				type:"string"
			});
		   fields.push({
				name:'productName',
				type:"string"
			});
			   fields.push({
				name:'acceptDate',
				type:"string"
			});
		   fields.push({
				name:'reportDate',
				type:"string"
			});
			   fields.push({
				name:'sampleNum',
				type:"string"
			});
			   
			   fields.push({
					name:'sampleInfoIn-id',
					type:"string"
				}); 
		   fields.push({
				name:'sampleInfoIn-code',
				type:"string"
			}); 
		   fields.push({
				name:'sampleInfoIn-location',
				type:"string"
			}); 
		   fields.push({
				name:'sampleInfoIn-tempId',
				type:"string"
			});   
		   fields.push({
				name:'sampleInfoIn-concentration',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-volume',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sumTotal',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-customer-id',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-customer-name',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-customer-street',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-customer-postcode',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-project-id',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-project-name',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-productId',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-productName',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-crmDoctor-id',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-crmDoctor-name',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-species',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sellPerson-id',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sellPerson-name',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-patientName',
				type:"string"
			});
		   fields.push({
				name:'sampleInfoIn-sampleInfo-idCard',
				type:"string"
			});
		   fields.push({
				name:'project-id',
				type:"string"
			});
		   fields.push({
				name:'project-name',
				type:"string"
			});
		   fields.push({
				name:'orderId',
				type:"string"
			});
		   fields.push({
				name:'groupNo',
				type:"string"
			});
		   fields.push({
				name:'barCode',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleInfoIn-code',
		hidden : false,
		header:biolims.common.code,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		header:biolims.common.throughput,
		width:20*6
	});
	
	cm.push({
		dataIndex:'barCode',
		hidden : true,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sampleInfo-idCard',
		hidden : false,
		header:biolims.common.outCode,
		width:20*6
	});
	cm.push({
		dataIndex:'groupNo',
		hidden : false,
		header:biolims.common.SampleCollectionPacketNumber,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'project-id',
		hidden : false,
		header:biolims.common.contractId,
		width:20*6
	});
	cm.push({
		dataIndex:'project-name',
		hidden : true,
		header:biolims.common.contractName,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:biolims.sample.orderNum,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-location',
		hidden : true,
		header:biolims.common.location,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-customer-id',
		hidden : true,
		header:biolims.sample.customerId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-customer-name',
		hidden : false,
		header:biolims.sample.customerName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-customer-street',
		hidden : true,
		header:biolims.sample.customerStreet,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-customer-postcode',
		hidden : true,
		header:biolims.sample.customerPostcode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sampleInfo-crmDoctor-id',
		hidden : true,
		header:"客户姓名id",
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sampleInfo-crmDoctor-name',
		hidden : false,
		header:biolims.common.customName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sampleInfo-species',
		hidden : false,
		header:biolims.wk.species,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sellPerson-id',
		hidden : true,
		header:"销售id",
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sellPerson-name',
		hidden : true,
		header:"销售",
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-id',
		hidden : true,
		header:"入库Id",
		width:30*6
	});
	cm.push({
		dataIndex:'sampleInfoIn-sampleInfo-productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*10
	});
	cm.push({
		dataIndex:'sampleInfoIn-sampleInfo-productName',
		hidden : true,
		header:biolims.common.productName,
		width:20*10
	});
	cm.push({
		dataIndex:'sampleInfoIn-sampleInfo-patientName',
		hidden : true,
		header:"姓名/名称",
		width:20*10
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:biolims.common.sampleNum,
		width:10*6
	});
	cm.push({
		dataIndex:'techDnaServiceTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techDnaServiceTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		sortable:true,
		hidden:true,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/dna/techDnaServiceTask/showItemDialogListJson.action";
	var opts={};
	opts.title=biolims.common.selectGiveSample;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];

	showItemDialogGrid=gridEditTable("showItemDialogdiv",cols,loadParam,opts);
	$("#showItemDialogdiv").data("showItemDialogGrid", showItemDialogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function searchGrid(){
	
	commonSearchAction(showItemDialogGrid);
	
}
