$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/technology/dna/techDnaServiceTask/editTechDnaServiceTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/technology/dna/techDnaServiceTask/showTechDnaServiceTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#techDnaServiceTask", {
					userId : userId,
					userName : userName,
					formId : $("#techDnaServiceTask_id").val(),
					title : $("#techDnaServiceTask_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#techDnaServiceTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){ 
	Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var techDnaServiceTaskItemDivData = $("#techDnaServiceTaskItemdiv").data("techDnaServiceTaskItemGrid");
		document.getElementById('techDnaServiceTaskItemJson').value = commonGetModifyRecords(techDnaServiceTaskItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/technology/dna/techDnaServiceTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/technology/dna/techDnaServiceTask/copyTechDnaServiceTask.action?id=' + $("#techDnaServiceTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#techDnaServiceTask_id").val() + "&tableId=TechDnaServiceTask");
}
$("#toolbarbutton_status").click(function(){
//	var grid=techDnaServiceTaskItemGrid.store;
//	for(var i=0;i<grid.getCount();i++){
//		var next=grid.getAt(i).get("nextFlowId");
//		if(next==""||next==null){
//			message(biolims.common.pleaseSelectNextFlow);
//			return;
//		}
//	}
	commonChangeState("formId=" + $("#techDnaServiceTask_id").val() + "&tableId=TechDnaServiceTask");
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#techDnaServiceTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#techDnaServiceTask_name").val());
	nsc.push(biolims.common.nameNotEmpty);
//	fs.push($("#techDnaServiceTask_projectId").val());
//	nsc.push("项目编号不能为空！");
//	fs.push($("#techDnaServiceTask_contractId").val());
//	nsc.push("合同编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.claimTask,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/technology/dna/techDnaServiceTask/showTechDnaServiceTaskItemList.action", {
				id : $("#techDnaServiceTask_id").val()
			}, "#techDnaServiceTaskItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
	text:biolims.common.changeState
	});
item.on('click', changeState);