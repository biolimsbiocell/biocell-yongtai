var techDnaServiceTaskGrid;
$(function() {
	var options = table(true, "",
			"/technology/dna/techDnaServiceTask/showTechDnaServiceTaskListJson.action", [ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "name",
				"title" : biolims.common.name,
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			}  ], null)
	techDnaServiceTaskGrid = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(techDnaServiceTaskGrid);
	})
});

function add() {
	window.location = window.ctx
			+ '/technology/dna/techDnaServiceTask/editTechDnaServiceTask.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/technology/dna/techDnaServiceTask/editTechDnaServiceTask.action?id=' + id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/technology/dna/techDnaServiceTask/viewTechDnaServiceTask.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.sample.createUserName,
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"type" : "table",
		"table" : techDnaServiceTaskGrid
	} ];
}
