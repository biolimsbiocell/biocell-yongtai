var techServiceTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
			name:'acceptUser-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
		});
	    fields.push({
			name:'projectId-id',
			type:"string"
		});
	    fields.push({
			name:'projectId-name',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-id',
		header:"实验组ID",
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:"实验组",
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/technology/techServiceTask/showTechServiceTaskListJson.action";
	var opts={};
	opts.title=biolims.common.scienceService;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setTechServiceTaskFun(rec);
	};
	techServiceTaskDialogGrid=gridTable("show_dialog_techServiceTask_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(techServiceTaskDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)": function() {
				form_reset();

			}
		}, true, option);
	}
