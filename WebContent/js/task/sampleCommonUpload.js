$(function() {
	$("#upload_file_div").dialog({
		modal : true,
		title : biolims.common.uploadResult,
		closeText : "",
		width : 380,
		height : 400,
		resizable : false,
		buttons : {
			"Confirm" : function() {
				var callback = $("#upload_file_div").data("callback");
				if ((typeof callback) != "function") {
					return;
				}
				var data = {};
				data.fileId = $("#file_id").val();
				data.fileName = $("#file_name").html();
				callback(data);
				$(this).dialog("close");
			},
			"Cancel" : function() {
				$(this).dialog("close");
			}
		},
		close : function(event, ui) {
			$(this).dialog("destroy");
			$(this).remove();
		}
	});
	$("#file-upload11").fileupload({
		forceIframeTransport : true,
		autoUpload : true,
		type : 'POST',
		dataType : "json",
		url : ctx + "/sample/task/ajaxImportExcel.action?modelType=" + $("#module_type").val(),
		always : function(e, data) {
			if (data != "error") {
				var result = data.result;
				$("#file_id").val(result.fileId);
				$("#file_name").html(result.fileName);
				$("#upload_time").html(result.uploadDate);
			} else {
				// TODO 文件上传失败提醒
			}
		}
	});
});
