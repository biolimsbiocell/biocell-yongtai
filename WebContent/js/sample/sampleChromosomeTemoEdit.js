﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});

	if($("#sampleChromosomeTemp_name").val() != ""){
		$("#chromosomeTempNew_name").val($("#sampleChromosomeTemp_name").val());
	}
	if($("#chromosomeTemp_name").val() != ""){
		$("#chromosomeTempNew_name").val($("#chromosomeTemp_name").val());
	}
	
	if($("#sampleChromosomeTemp_productName").val()!=$("#chromosomeTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleChromosomeTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleChromosomeTemp_productId").val());
		$("#productNameNew").val($("#sampleChromosomeTemp_productName").val());
	}
	
	if($("#sampleChromosomeTemp_sampleType_id").val()!=$("#chromosomeTemp_sampleType_id").val()){
		$("#sampleType").css({"background-color":"red","color":"white"});
	}else{
		$("#sampleTypeIdNew").val($("#sampleChromosomeTemp_sampleType_id").val());
		$("#sampleTypeNew").val($("#sampleChromosomeTemp_sampleType_name").val());
	}
	
	if($("#sampleChromosomeTemp_voucherType_id").val()!=$("#chromosomeTemp_voucherType_id").val()){
		$("#voucherType").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherTypeIdNew").val($("#sampleChromosomeTemp_voucherType_id").val());
		$("#voucherTypeNew").val($("#sampleChromosomeTemp_voucherType_name").val());
	}
	
	if($("#sampleChromosomeTemp_acceptDate").val()!=$("#chromosomeTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleChromosomeTemp_acceptDate").val());
	}
	
	if($("#sampleChromosomeTemp_area").val()!=$("#chromosomeTemp_area").val()){
		$("#area").css({"background-color":"red","color":"white"});
	}else{
		$("#areaNew").val($("#sampleChromosomeTemp_area").val());
	}
	
	if($("#sampleChromosomeTemp_hospital").val()!=$("#chromosomeTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleChromosomeTemp_hospital").val());
	}
	
	if($("#sampleChromosomeTemp_sendDate").val()!=$("#chromosomeTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleChromosomeTemp_sendDate").val());
	}
	
	if($("#sampleChromosomeTemp_reportDate").val()!=$("#chromosomeTemp_reportDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleChromosomeTemp_reportDate").val());
	}
	
	if($("#sampleChromosomeTemp_doctor").val()!=$("#chromosomeTemp_doctor").val()){
		$("#doctor").css({"background-color":"red","color":"white"});
	}else{
		$("#doctorNew").val($("#sampleChromosomeTemp_doctor").val());
	}
	
	if($("#sampleChromosomeTemp_patientName").val()!=$("#chromosomeTemp_patientName").val()){
		$("#patientName").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameNew").val($("#sampleChromosomeTemp_patientName").val());
	}
	
	if($("#sampleChromosomeTemp_voucherCode").val()!=$("#chromosomeTemp_voucherCode").val()){
		$("#voucherCode").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherCodeNew").val($("#sampleChromosomeTemp_voucherCode").val());
	}
	
	if($("#sampleChromosomeTemp_phoneNum").val()!=$("#chromosomeTemp_phoneNum").val()){
		$("#phoneNum").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNumNew").val($("#sampleChromosomeTemp_phoneNum").val());
	}
	
	if($("#sampleChromosomeTemp_phoneNum2").val()!=$("#chromosomeTemp_phoneNum2").val()){
		$("#phoneNum2").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNum2New").val($("#sampleChromosomeTemp_phoneNum2").val());
	}
	
	if($("#sampleChromosomeTemp_agentMan").val()!=$("#chromosomeTemp_agentMan").val()){
		$("#agentMan").css({"background-color":"red","color":"white"});
	}else{
		$("#agentManNew").val($("#sampleChromosomeTemp_agentMan").val());
	}
	
	if($("#sampleChromosomeTemp_cardCode").val()!=$("#chromosomeTemp_cardCode").val()){
		$("#cardCode").css({"background-color":"red","color":"white"});
	}else{
		$("#cardCodeNew").val($("#sampleChromosomeTemp_cardCode").val());
	}
	
	if($("#sampleChromosomeTemp_address").val()!=$("#chromosomeTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleChromosomeTemp_address").val());
	}
	
	if($("#sampleChromosomeTemp_emailAddress").val()!=$("#chromosomeTemp_emailAddress").val()){
		$("#emailAddress").css({"background-color":"red","color":"white"});
	}else{
		$("#emailAddressNew").val($("#sampleChromosomeTemp_emailAddress").val());
	}
	
	if($("#sampleChromosomeTemp_inHosNum").val()!=$("#chromosomeTemp_inHosNum").val()){
		$("#inHosNum").css({"background-color":"red","color":"white"});
	}else{
		$("#inHosNumNew").val($("#sampleChromosomeTemp_inHosNum").val());
	}
	
	if($("#sampleChromosomeTemp_tumorHistory").val()!=$("#chromosomeTemp_tumorHistory").val()){
		$("#tumorHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorHistoryNew").val($("#sampleChromosomeTemp_tumorHistory").val());
	}
	
	if($("#sampleChromosomeTemp_badMotherhood").val()!=$("#chromosomeTemp_badMotherhood").val()){
		$("#badMotherhood").css({"background-color":"red","color":"white"});
	}else{
		$("#badMotherhoodNew").val($("#sampleChromosomeTemp_badMotherhood").val());
	}
	
	if($("#sampleChromosomeTemp_pregnancyTime").val()!=$("#chromosomeTemp_pregnancyTime").val()){
		$("#pregnancyTime").css({"background-color":"red","color":"white"});
	}else{
		$("#pregnancyTimeNew").val($("#sampleChromosomeTemp_pregnancyTime").val());
	}
	
	if($("#sampleChromosomeTemp_parturitionTime").val()!=$("#chromosomeTemp_parturitionTime").val()){
		$("#parturitionTime").css({"background-color":"red","color":"white"});
	}else{
		$("#parturitionTimeNew").val($("#sampleChromosomeTemp_parturitionTime").val());
	}
	
	if($("#sampleChromosomeTemp_howManyfetus").val()!=$("#chromosomeTemp_howManyfetus").val()){
		$("#howManyfetus").css({"background-color":"red","color":"white"});
	}else{
		$("#howManyfetusNew").val($("#sampleChromosomeTemp_howManyfetus").val());
	}
	
	if($("#sampleChromosomeTemp_reason2").val()!=$("#chromosomeTemp_reason2").val()){
		$("#reason2").css({"background-color":"red","color":"white"});
	}else{
		$("#reason2New").val($("#sampleChromosomeTemp_reason2").val());
	}
	
	if($("#sampleChromosomeTemp_organGrafting").val()!=$("#chromosomeTemp_organGrafting").val()){
		$("#organGrafting").css({"background-color":"red","color":"white"});
	}else{
		$("#organGraftingNew").val($("#sampleChromosomeTemp_organGrafting").val());
	}
	
	if($("#sampleChromosomeTemp_outTransfusion").val()!=$("#chromosomeTemp_outTransfusion").val()){
		$("#chromosomeTempNew_outTransfusion").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_outTransfusion").val($("#sampleChromosomeTemp_outTransfusion").val());
	}
	
	if($("#sampleChromosomeTemp_embryoType").val()!=$("#chromosomeTemp_embryoType").val()){
		$("#chromosomeTempNew_embryoType").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_embryoType").val($("#sampleChromosomeTemp_embryoType").val());
	}
	
	if($("#sampleChromosomeTemp_gestationalAge").val()!=$("#chromosomeTemp_gestationalAge").val()){
		$("#gestationalAge").css({"background-color":"red","color":"white"});
	}else{
		$("#gestationalAgeNew").val($("#sampleChromosomeTemp_gestationalAge").val());
	}
	
	if($("#sampleChromosomeTemp_diagnosis").val()!=$("#chromosomeTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleChromosomeTemp_diagnosis").val());
	}
	
	if($("#sampleChromosomeTemp_stemCellsCure").val()!=$("#chromosomeTemp_stemCellsCure").val()){
		$("#chromosomeTempNew_stemCellsCure").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_stemCellsCure").val($("#sampleChromosomeTemp_stemCellsCure").val());
	}
	
	if($("#sampleChromosomeTemp_immuneCure").val()!=$("#chromosomeTemp_immuneCure").val()){
		$("#chromosomeTempNew_immuneCure").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_immuneCure").val($("#sampleChromosomeTemp_immuneCure").val());
	}
	
	if($("#sampleChromosomeTemp_coreAnalyze").val()!=$("#chromosomeTemp_coreAnalyze").val()){
		$("#chromosomeTempNew_coreAnalyze").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_coreAnalyze").val($("#sampleChromosomeTemp_coreAnalyze").val());
	}
	
	if($("#sampleChromosomeTemp_note").val()!=$("#chromosomeTemp_note").val()){
		$("#chromosomeTempNew_note").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_note").val($("#sampleChromosomeTemp_note").val());
	}
	
	if($("#sampleChromosomeTemp_attachedSample1").val()!=$("#chromosomeTemp_attachedSample1").val()){
		$("#attachedSample1").css({"background-color":"red","color":"white"});
	}else{
		$("#attachedSample1New").val($("#sampleChromosomeTemp_attachedSample1").val());
	}
	
	if($("#sampleChromosomeTemp_attachedSample2").val()!=$("#chromosomeTemp_attachedSample2").val()){
		$("#attachedSample2").css({"background-color":"red","color":"white"});
	}else{
		$("#attachedSample2New").val($("#sampleChromosomeTemp_attachedSample2").val());
	}
	
	if($("#sampleChromosomeTemp_specialCircumstances").val()!=$("#chromosomeTemp_specialCircumstances").val()){
		$("#specialCircumstances").css({"background-color":"red","color":"white"});
	}else{
		$("#specialCircumstancesNew").val($("#sampleChromosomeTemp_specialCircumstances").val());
	}
	
	if($("#sampleChromosomeTemp_isInsure").val()!=$("#chromosomeTemp_isInsure").val()){
		$("#chromosomeTempNew_isInsure").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_isInsure").val($("#sampleChromosomeTemp_isInsure").val());
	}
	
	if($("#sampleChromosomeTemp_isInvoice").val()!=$("#chromosomeTemp_isInvoice").val()){
		$("#chromosomeTempNew_isInvoice").css({"background-color":"red","color":"white"});
	}else{
		$("#chromosomeTempNew_isInvoice").val($("#sampleChromosomeTemp_isInvoice").val());
	}
	
	if($("#sampleChromosomeTemp_money").val()!=$("#chromosomeTemp_money").val()){
		$("#money").css({"background-color":"red","color":"white"});
	}else{
		$("#moneyNew").val($("#sampleChromosomeTemp_money").val());
	}
	
	if($("#sampleChromosomeTemp_receipt").val()!=$("#chromosomeTemp_receipt").val()){
		$("#receipt").css({"background-color":"red","color":"white"});
	}else{
		$("#receiptNew").val($("#sampleChromosomeTemp_receipt").val());
	}
	
	if($("#sampleChromosomeTemp_paymentUnit").val()!=$("#chromosomeTemp_paymentUnit").val()){
		$("#paymentUnit").css({"background-color":"red","color":"white"});
	}else{
		$("#paymentUnitNew").val($("#sampleChromosomeTemp_paymentUnit").val());
	}
	
	if($("#sampleChromosomeTemp_reportMan_id").val()!=$("#chromosomeTemp_reportMan_id").val()){
		$("#reportMan").css({"background-color":"red","color":"white"});
	}else{
		$("#reportManIdNew").val($("#sampleChromosomeTemp_reportMan_id").val());
		$("#reportManNew").val($("#sampleChromosomeTemp_reportMan_name").val());
	}
	
	if($("#sampleChromosomeTemp_auditMan_id").val()!=$("#chromosomeTemp_auditMan_id").val()){
		$("#auditMan").css({"background-color":"red","color":"white"});
	}else{
		$("#auditManIdNew").val($("#sampleChromosomeTemp_auditMan_id").val());
		$("#auditManNew").val($("#sampleChromosomeTemp_auditMan_name").val());
	}
	
	if($("#sampleChromosomeTemp_reason").val()!=""){
		$("#reasonNew").val($("#sampleChromosomeTemp_reason").val());
	}
	if($("#chromosomeTemp_reason").val()!=""){
		$("#reasonNew").val($("#chromosomeTemp_reason").val());
	}
	if($("#sampleChromosomeTemp_reason").val()!="" && $("#chromosomeTemp_reason").val()!=""){
		$("#reasonNew").val($("#sampleChromosomeTemp_reason").val());
	}
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {

	var nextStepFlow =$("#sampleChromosomeTemp_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.pleaseSelectNextFlow);
		return;
	}
	
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleChromosomeTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleChromosomeTemp_id").val(),
					title : $("#sampleChromosomeTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleChromosomeTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action?saveType="+$("#saveType").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInputTemp/copySampleInput.action?id=' + $("#sampleChromosomeTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleChromosomeTemp_id").val()){
		commonChangeState("formId=" + $("#sampleChromosomeTemp_id").val() + "&tableId=SampleChromosomeTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.sampleChromosomeTemo,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
//
//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 800,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleChromosomeTemp_sampleType_id").value = id;
		 document.getElementById("sampleChromosomeTemp_sampleType_name").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:500,height:500,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("sampleChromosomeTemp_voucherType").value = id;
			 document.getElementById("sampleChromosomeTemp_voucherType_name").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleChromosomeTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleChromosomeTemp_voucherCode").val())){
				var id = $("#sampleChromosomeTemp_voucherCode").val();
				ajax("post", "/sample/sampleInputTemp/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleChromosomeTemp_phoneNum").val())){
				//return;
				var id = $("#sampleChromosomeTemp_phoneNum").val();
				ajax("post", "/sample/sampleInputTemp/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

		//家庭住址验证	 
		function checkAddress(){
			var address = $("#sampleChromosomeTemp_address").val();
			if(address==""){
				message(biolims.sample.addressIsEmpty);
		}
}
		//B超异常提醒验证
		function change(){
			var reg = $('#sampleChromosomeTemp_embryoType option:selected').val();
			if(reg =="2"){
				$("#sampleChromosomeTemp_messages").css("display","");
			}
		}
		
		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 500,
						height : 500,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("sampleChromosomeTemp_product_id").value = id;
							document.getElementById("sampleChromosomeTemp_product_name").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}
		
		
		
		function upLoadImg1(){
			var win = Ext.getCmp('upLoadImg1');
			if (win) {
				win.close();
			}
			var upLoadImg1 = new Ext.Window(
					{
						id : 'upLoadImg1',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 500,
						height : 500,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/goods/sample/sampleInfoMain/toSampeInfoUpload.action?flag=sctp' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								upLoadImg1.close();
							}
						} ]
					});
//			var isUpload = true;
//			load("/goods/sample/sampleInfoMain/toSampeInfoUpload.action", { // 是否修改
//				fileId : record.get("upLoadAccessory-id"),
//				isUpload : isUpload
//			}, null, function() {
//				$("#upload_file_div").data("callback", function(data) {
//					record.set("upLoadAccessory-fileName", data.fileName);
//					record.set("upLoadAccessory-id", data.fileId);
//				});
//			});
			upLoadImg1.show();
		}

		function setsctp(id, name) {
			document.getElementById("sampleTumorTemp_upLoadAccessory_id").value = id;
			document.getElementById("sampleTumorTemp_upLoadAccessory_fileName").value = name;
			var win = Ext.getCmp('upLoadImg1');
			if (win) {
				win.close();
			}
		}		