﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleTumorTemp_name").val() != ""){
		$("#tumorTempNew_name").val($("#sampleTumorTemp_name").val());
	}
	if($("#tumorTemp_name").val() != ""){
		$("#tumorTempNew_name").val($("#tumorTemp_name").val());
	}
	
	if($("#sampleTumorTemp_productName").val()!=$("#tumorTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleTumorTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleTumorTemp_productId").val());
		$("#productNameNew").val($("#sampleTumorTemp_productName").val());
	}
	
	if($("#sampleTumorTemp_sampleType_id").val()!=$("#tumorTemp_sampleType_id").val()){
		$("#sampleType").css({"background-color":"red","color":"white"});
	}else{
		$("#sampleTypeIdNew").val($("#sampleTumorTemp_sampleType_id").val());
		$("#sampleTypeNew").val($("#sampleTumorTemp_sampleType_name").val());
	}
	
	if($("#sampleTumorTemp_voucherType_id").val()!=$("#tumorTemp_voucherType_id").val()){
		$("#voucherType").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherTypeIdNew").val($("#sampleTumorTemp_voucherType_id").val());
		$("#voucherTypeNew").val($("#sampleTumorTemp_voucherType_name").val());
	}
	
	if($("#sampleTumorTemp_acceptDate").val()!=$("#tumorTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleTumorTemp_acceptDate").val());
	}
	
	if($("#sampleTumorTemp_area").val()!=$("#tumorTemp_area").val()){
		$("#area").css({"background-color":"red","color":"white"});
	}else{
		$("#areaNew").val($("#sampleTumorTemp_area").val());
	}
	
	if($("#sampleTumorTemp_hospital").val()!=$("#tumorTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleTumorTemp_hospital").val());
	}
	
	if($("#sampleTumorTemp_sendDate").val()!=$("#tumorTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleTumorTemp_sendDate").val());
	}
	
	if($("#sampleTumorTemp_reportDate").val()!=$("#tumorTemp_reportDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleTumorTemp_reportDate").val());
	}
	
	if($("#sampleTumorTemp_doctor").val()!=$("#tumorTemp_doctor").val()){
		$("#doctor").css({"background-color":"red","color":"white"});
	}else{
		$("#doctorNew").val($("#sampleTumorTemp_doctor").val());
	}
	
	if($("#sampleTumorTemp_detectionContent").val()!=$("#tumorTemp_detectionContent").val()){
		$("#detectionContent").css({"background-color":"red","color":"white"});
	}else{
		$("#detectionContentNew").val($("#sampleTumorTemp_detectionContent").val());
	}
	
	if($("#sampleTumorTemp_patientName").val()!=$("#tumorTemp_patientName").val()){
		$("#patientName").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameNew").val($("#sampleTumorTemp_patientName").val());
	}
	
	if($("#sampleTumorTemp_gender").val()!=$("#tumorTemp_gender").val()){
		$("#tumorTempNew_gender").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_gender").val($("#sampleTumorTemp_gender").val());
	}
	
	if($("#sampleTumorTemp_age").val()!=$("#tumorTemp_age").val()){
		$("#age").css({"background-color":"red","color":"white"});
	}else{
		$("#ageNew").val($("#sampleTumorTemp_age").val());
	}
	
	if($("#sampleTumorTemp_nativePlace").val()!=$("#tumorTemp_nativePlace").val()){
		$("#nativePlace").css({"background-color":"red","color":"white"});
	}else{
		$("#nativePlaceNew").val($("#sampleTumorTemp_nativePlace").val());
	}
	
	if($("#sampleTumorTemp_nationality").val()!=$("#tumorTemp_nationality").val()){
		$("#nationality").css({"background-color":"red","color":"white"});
	}else{
		$("#nationalityNew").val($("#sampleTumorTemp_nationality").val());
	}
	
	if($("#sampleTumorTemp_isInsure").val()!=$("#tumorTemp_isInsure").val()){
		$("#tumorTempNew_isInsure").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_isInsure").val($("#sampleTumorTemp_isInsure").val());
	}
	
	if($("#sampleTumorTemp_heights").val()!=$("#tumorTemp_heights").val()){
		$("#heights").css({"background-color":"red","color":"white"});
	}else{
		$("#heightsNew").val($("#sampleTumorTemp_heights").val());
	}
	
	if($("#sampleTumorTemp_weight").val()!=$("#tumorTemp_weight").val()){
		$("#weight").css({"background-color":"red","color":"white"});
	}else{
		$("#weightNew").val($("#sampleTumorTemp_weight").val());
	}
	
	if($("#sampleTumorTemp_bloodPressure").val()!=$("#tumorTemp_bloodPressure").val()){
		$("#bloodPressure").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodPressureNew").val($("#sampleTumorTemp_bloodPressure").val());
	}
	
	if($("#sampleTumorTemp_electrocardiogram").val()!=$("#tumorTemp_electrocardiogram").val()){
		$("#electrocardiogram").css({"background-color":"red","color":"white"});
	}else{
		$("#electrocardiogramssureNew").val($("#sampleTumorTemp_electrocardiogram").val());
	}
	
	if($("#sampleTumorTemp_voucherCode").val()!=$("#tumorTemp_voucherCode").val()){
		$("#voucherCode").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherCodeNew").val($("#sampleTumorTemp_voucherCode").val());
	}
	
	if($("#sampleTumorTemp_phone").val()!=$("#tumorTemp_phone").val()){
		$("#phone").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNew").val($("#sampleTumorTemp_phone").val());
	}
	
	if($("#sampleTumorTemp_emailAddress").val()!=$("#tumorTemp_emailAddress").val()){
		$("#emailAddress").css({"background-color":"red","color":"white"});
	}else{
		$("#emailAddressNew").val($("#sampleTumorTemp_emailAddress").val());
	}
	
	if($("#sampleTumorTemp_address").val()!=$("#tumorTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleTumorTemp_address").val());
	}
	
	if($("#sampleTumorTemp_diagnosis").val()!=$("#tumorTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleTumorTemp_diagnosis").val());
	}
	
	if($("#sampleTumorTemp_pharmacy").val()!=$("#tumorTemp_pharmacy").val()){
		$("#pharmacy").css({"background-color":"red","color":"white"});
	}else{
		$("#pharmacyNew").val($("#sampleTumorTemp_pharmacy").val());
	}
	
	if($("#sampleTumorTemp_caseDescribe").val()!=$("#tumorTemp_caseDescribe").val()){
		$("#caseDescribe").css({"background-color":"red","color":"white"});
	}else{
		$("#caseDescribeNew").val($("#sampleTumorTemp_caseDescribe").val());
	}
	
	if($("#sampleTumorTemp_bloodHistory").val()!=$("#tumorTemp_bloodHistory").val()){
		$("#bloodHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodHistoryNew").val($("#sampleTumorTemp_bloodHistory").val());
	}
	
	if($("#sampleTumorTemp_parturitionTime").val()!=$("#tumorTemp_parturitionTime").val()){
		$("#parturitionTime").css({"background-color":"red","color":"white"});
	}else{
		$("#parturitionTimeNew").val($("#sampleTumorTemp_parturitionTime").val());
	}
	
	if($("#sampleTumorTemp_cigarette").val()!=$("#tumorTemp_cigarette").val()){
		$("#cigarette").css({"background-color":"red","color":"white"});
	}else{
		$("#cigaretteNew").val($("#sampleTumorTemp_cigarette").val());
	}
	
	if($("#sampleTumorTemp_wine").val()!=$("#tumorTemp_wine").val()){
		$("#wine").css({"background-color":"red","color":"white"});
	}else{
		$("#wineNew").val($("#sampleTumorTemp_wine").val());
	}
	
	if($("#sampleTumorTemp_medicine").val()!=$("#tumorTemp_medicine").val()){
		$("#medicine").css({"background-color":"red","color":"white"});
	}else{
		$("#medicineNew").val($("#sampleTumorTemp_medicine").val());
	}
	
	if($("#sampleTumorTemp_radioactiveRays").val()!=$("#tumorTemp_radioactiveRays").val()){
		$("#radioactiveRays").css({"background-color":"red","color":"white"});
	}else{
		$("#radioactiveRaysNew").val($("#sampleTumorTemp_radioactiveRays").val());
	}
	
	if($("#sampleTumorTemp_pesticide").val()!=$("#tumorTemp_pesticide").val()){
		$("#pesticide").css({"background-color":"red","color":"white"});
	}else{
		$("#pesticideNew").val($("#sampleTumorTemp_pesticide").val());
	}
	
	if($("#sampleTumorTemp_plumbane").val()!=$("#tumorTemp_plumbane").val()){
		$("#plumbane").css({"background-color":"red","color":"white"});
	}else{
		$("#plumbaneNew").val($("#sampleTumorTemp_plumbane").val());
	}
	
	if($("#sampleTumorTemp_mercury").val()!=$("#tumorTemp_mercury").val()){
		$("#mercury").css({"background-color":"red","color":"white"});
	}else{
		$("#mercuryNew").val($("#sampleTumorTemp_mercury").val());
	}
	
	if($("#sampleTumorTemp_cadmium").val()!=$("#tumorTemp_cadmium").val()){
		$("#cadmium").css({"background-color":"red","color":"white"});
	}else{
		$("#cadmiumNew").val($("#sampleTumorTemp_cadmium").val());
	}
	
	if($("#sampleTumorTemp_medicalHistory").val()!=$("#tumorTemp_medicalHistory").val()){
		$("#tumorTempNew_medicalHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_medicalHistory").val($("#sampleTumorTemp_medicalHistory").val());
	}
	
	if($("#sampleTumorTemp_gestationIVF").val()!=$("#tumorTemp_gestationIVF").val()){
		$("#tumorTempNew_gestationIVF").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_gestationIVF").val($("#sampleTumorTemp_gestationIVF").val());
	}
	
	if($("#sampleTumorTemp_isSurgery").val()!=$("#tumorTemp_isSurgery").val()){
		$("#tumorTempNew_isSurgery").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_isSurgery").val($("#sampleTumorTemp_isSurgery").val());
	}
	
	if($("#sampleTumorTemp_other").val()!=$("#tumorTemp_other").val()){
		$("#other").css({"background-color":"red","color":"white"});
	}else{
		$("#otherNew").val($("#sampleTumorTemp_other").val());
	}
	
	if($("#sampleTumorTemp_isPharmacy").val()!=$("#tumorTemp_isPharmacy").val()){
		$("#tumorTempNew_isPharmacy").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_isPharmacy").val($("#sampleTumorTemp_isPharmacy").val());
	}
	
	if($("#sampleTumorTemp_isAilment").val()!=$("#tumorTemp_isAilment").val()){
		$("#tumorTempNew_isAilment").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_isAilment").val($("#sampleTumorTemp_isAilment").val());
	}
	
	if($("#sampleTumorTemp_isSickenTumour").val()!=$("#tumorTemp_isSickenTumour").val()){
		$("#tumorTempNew_isSickenTumour").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_isSickenTumour").val($("#sampleTumorTemp_isSickenTumour").val());
	}
	
	if($("#sampleTumorTemp_relationship").val()!=$("#tumorTemp_relationship").val()){
		$("#relationship").css({"background-color":"red","color":"white"});
	}else{
		$("#relationshipNew").val($("#sampleTumorTemp_relationship").val());
	}
	
	if($("#sampleTumorTemp_isFee").val()!=$("#tumorTemp_isFee").val()){
		$("#tumorTempNew_isFee").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_isFee").val($("#sampleTumorTemp_isFee").val());
	}
	
	if($("#sampleTumorTemp_privilegeType").val()!=$("#tumorTemp_privilegeType").val()){
		$("#tumorTempNew_privilegeType").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_privilegeType").val($("#sampleTumorTemp_privilegeType").val());
	}
	
	if($("#sampleTumorTemp_linkman").val()!=$("#tumorTemp_linkman").val()){
		$("#linkman").css({"background-color":"red","color":"white"});
	}else{
		$("#linkmanNew").val($("#sampleTumorTemp_linkman").val());
	}
	
	if($("#sampleTumorTemp_isInvoice").val()!=$("#tumorTemp_isInvoice").val()){
		$("#tumorTempNew_isInvoice").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorTempNew_isInvoice").val($("#sampleTumorTemp_isInvoice").val());
	}
	
	if($("#sampleTumorTemp_money").val()!=$("#tumorTemp_money").val()){
		$("#money").css({"background-color":"red","color":"white"});
	}else{
		$("#moneyNew").val($("#sampleTumorTemp_money").val());
	}
	if($("#sampleTumorTemp_sp").val()!=$("#tumorTemp_sp").val()){
		$("#sp").css({"background-color":"red","color":"white"});
	}else{
		$("#spNew").val($("#sampleTumorTemp_sp").val());
	}
	
	if($("#sampleTumorTemp_paymentUnit").val()!=$("#tumorTemp_paymentUnit").val()){
		$("#paymentUnit").css({"background-color":"red","color":"white"});
	}else{
		$("#paymentUnitNew").val($("#sampleTumorTemp_paymentUnit").val());
	}
	
	if($("#sampleTumorTemp_reportMan_name").val()!=$("#tumorTemp_reportMan_name").val()){
		$("#reportMan").css({"background-color":"red","color":"white"});
		$("#reportManIdNew").val($("#sampleTumorTemp_reportMan_id").val());
	}else{
		$("#reportManIdNew").val($("#sampleTumorTemp_reportMan_id").val());
		$("#reportManNew").val($("#sampleTumorTemp_reportMan_name").val());
	}
	
	if($("#sampleTumorTemp_auditMan_name").val()!=$("#tumorTemp_auditMan_name").val()){
		$("#auditMan").css({"background-color":"red","color":"white"});
		$("#auditManIdNew").val($("#sampleTumorTemp_auditMan_id").val());
	}else{
		$("#auditManIdNew").val($("#sampleTumorTemp_auditMan_id").val());
		$("#auditManNew").val($("#sampleTumorTemp_auditMann_name").val());
	}
	
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");

$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {
	
	var nextStepFlow =$("#tumorTempNew_nextStepFlow").val();
	if(nextStepFlow==""){
		message("请选择下一步流向");
		return;
	}

	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleTumorTemp_id").val(),
		title : $("#sampleTumorTemp_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTumorTemp_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action?saveType="+$("#saveType").val();
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleTumorTemp_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleTumorTemp_id").val()) {
				commonChangeState("formId=" + $("#sampleTumorTemp_id").val()
						+ "&tableId=SampleTumorTemp");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push("信息录入编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : '肿瘤信息录入审核',
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : '选择类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.QPCR.selectProject,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleTumorTemp_sampleType_id").value = id;
	document.getElementById("sampleTumorTemp_sampleType_name").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 收据类型
function receiptTypeFun() {
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
	var receiptTypeFun = new Ext.Window(
			{
				id : 'receiptTypeFun',
				modal : true,
				title : '选择收据类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=sjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						receiptTypeFun.close();
					}
				} ]
			});
	receiptTypeFun.show();
}
function setsjlx(id, name) {
	document.getElementById("sampleTumorTemp_receiptType").value = id;
	document.getElementById("sampleTumorTemp_receiptType_name").value = name;
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
}

// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleTumorTemp_phoneNum").val())) {
		// return;
		var id = $("#sampleTumorTemp_phoneNum").val();
		ajax("post", "/sample/sampleInputTemp/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("输入的手机号重复！");
				}
			}
		}, null);
	} else {
		message("请输入正确的手机号码!");
	}
}
function change() {
	var selected = $('#sampleTumorTemp_embryoType option:selected').val();
	if (selected == "2") {
		$("#sampleTumorTemp_message").css("display", "");
	}
}

function checkChange() {
	var selected = $('#sampleTumorTemp_coupleChromosome option:selected').val();
	if (selected == "2") {
		$("#sampleTumorTemp_remind").css("display", "");
	}
}


// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.QPCR.selectProject,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("sampleTumorTemp_product_id").value = id;
					document.getElementById("sampleTumorTemp_product_name").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}
