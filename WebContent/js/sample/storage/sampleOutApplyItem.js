﻿
var sampleOutApplyItemGrid;
$(function(){
	var flg=true;
	var xh=true;
	if($("#sampleOutApply_type").val()=="1"){//返回客户
		flg=false;
	}
	if($("#sampleOutApply_type").val()=="3"){//样本销毁
		xh=false;
	}
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'sampleInItemId',
			type:"string"
		});
	   fields.push({
			name:'unitGroup-id',
			type:"string"
		});
		fields.push({
			name:'unitGroup-name',
			type:"string"
		});
   
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
			name:'code',
			type:"string"
		});
	   fields.push({
			name:'num',
			type:"string"
		});
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'sampleOutApply-id',
		type:"string"
	});
	    fields.push({
		name:'sampleOutApply-name',
		type:"string"
	});
	    fields.push({
			name:'sendTime',
			type:"date"
	});
	    fields.push({
			name:'patientName',
			type:"string",
    });
	    fields.push({
		name:'location',
		type:"string"
	});
	    fields.push({
			name:'concentration',
			type:"string"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'sumTotal',
			type:"string"
		});
	   fields.push({
			name:'ystj',
			type:"string"
		});
	   fields.push({
			name:'fyVolume',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-createUser-name',
			type:"string"
		});
	   fields.push({
			name:'barCode',
			type:"string"
		});
	   fields.push({
		   name:'dataTraffic',
		   type:"string"
	   });
	   fields.push({
			name:'productId',
			type:"string"
	    });
		   fields.push({
			name:'productName',
			type:"string"
	    });
		   fields.push({
			name:'samplingDate',
			type:"string",
	    });
		   fields.push({
				name:'sampleStyle',
				type:"string",
		    });
		   
		   fields.push({
				name:'qpcrConcentration',
				type:"indexa",
		    });
		   fields.push({
				name:'samplingDate',
				type:"string",
		    });
		   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleStyle',
		hidden : true,
		header:biolims.master.productType,
		width:20*6
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.sample.applicationDetailId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleReceiveItem-id',
		header:biolims.common.sampleId,
		hidden:true,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex:'num',
		header:biolims.sample.inventory,
		hidden : true,
		width:12*10
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.testProject,
		width:20*6,
		hidden : false,
//		sortable:true
	});
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		hidden : false,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:10*10
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:biolims.common.location,
		width:20*6
	});
	
	cm.push({
		dataIndex:'qpcrConcentration',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInItemId',
		header:biolims.sample.warehousingDetail,
		hidden:true,
		width:10*10
	});
	
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:biolims.common.unitGroupId,
		width:15*10,
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: biolims.common.unitGroup,
		width:15*10,
		hidden : true
//		sortable:true,
//		editor : testUnitGroup
	});
	
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ystj',
		hidden : flg,
		header:biolims.common.transportConditions,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fyVolume',
		hidden : flg,
		header:biolims.common.sampleBackVolume,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : xh,
		header:biolims.pooling.kyId,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-createUser-name',
		hidden : xh,
		header:biolims.common.kyNextPerson,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleOutApply-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleOutApply-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:10*10
	});
	cm.push({
		dataIndex:'sendTime',
		hidden : true,
		header:biolims.sample.sendTime,
		width:10*10,

		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleOutApply/showSampleOutApplyItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.outboundApplyDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state=$("#sampleOutApply_state").val();
	if(state!="1"){
       opts.delSelect = function(ids) {
		ajax("post", "/sample/storage/sampleOutApply/delSampleOutApplyItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//    if($("#sampleOutApply_sampleType_id").val()=='blood'){
    	opts.tbar.push({
    		text : biolims.sample.selectSample,
    		handler : function() {
    			var cid=$("#sampleOutApply_customer").val();
    			var title = '';
    			var url = '';
				title = biolims.sample.selectSample;
				url = "/sample/storage/sampleIn/showSampleWaitOutItemList.action?cid="+cid;
    			var option = {};
    			option.width = document.body.clientWidth-50;
    			option.height = document.body.clientHeight-50;
    			loadDialogPage(null, title, url, {
    				"Confirm" : function() {
    						selValwk(this);
    				}
    			}, true, option);
    		}
    	});
    	var selValwk = function(win) {
    		var operGrid = sampleWaitOutItemGrid;
    		var selectRecord = operGrid.getSelectionModel().getSelections();
    		if (selectRecord.length > 0) {
//    			var datas = gridToObj(sampleOutApplyItemGrid, "sampleReceiveItem-id") || {};
    			var ob = sampleOutApplyItemGrid.getStore().recordType;
    			sampleOutApplyItemGrid.stopEditing();
    			$.each(selectRecord, function(i, obj) {	
    				var p = new ob({});
    					p.isNew = true;
    					p.set("code", obj.get("code"));
    					p.set("sampleCode", obj.get("sampleCode"));
    					p.set("sampleType", obj.get("sampleType"));
    					p.set("sampleInItemId", obj.get("id"));
    					p.set("location", obj.get("location"));
    					p.set("patientName", obj.get("patientName"));
    					p.set("barCode",obj.get("barCode"));
    					p.set("dataTraffic",obj.get("dataTraffic"));
    					p.set("productId",obj.get("productId"));
    					p.set("patientName",obj.get("sampleInfo-patientName"));
    					p.set("productName",obj.get("productName"));
    					p.set("samplingDate",obj.get("samplingDate"));
    					p.set("concentration", obj.get("concentration"));
    					p.set("volume", obj.get("volume"));
    					p.set("sumTotal", obj.get("sumTotal"));
    					p.set("note",obj.get("note"));
    					p.set("sampleStyle",obj.get("sampleStyle"));
    					
    					p.set("qpcrConcentration", obj.get("qpcrConcentration"));
    					p.set("indexa", obj.get("indexa"));
    					p.set("techJkServiceTask-id",obj.get("techJkServiceTask-id"));
    					p.set("techJkServiceTask-createUser-name",obj.get("techJkServiceTask-createUser-name"));
    					$("#sampleOutApply_customer").val(obj.get("customer-id"));
    					$("#sampleOutApply_customer_name").val(obj.get("customer-name"));
    					$("#sampleOutApply_customer_street").val(obj.get("customer-street"));
    					$("#sampleOutApply_customer_postcode").val(obj.get("customer-postcode"));
    					sampleOutApplyItemGrid.getStore().add(p);
    			});
    			sampleOutApplyItemGrid.startEditing(0, 0);
    			
    			$(win).dialog("close");
    			$(win).dialog("remove");
    		} else {
    			message(biolims.common.selectYouWant);
    			return;
    		}
    	};
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	}
	sampleOutApplyItemGrid=gridEditTable("sampleOutApplyItemdiv",cols,loadParam,opts);
	$("#sampleOutApplyItemdiv").data("sampleOutApplyItemGrid", sampleOutApplyItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
	
