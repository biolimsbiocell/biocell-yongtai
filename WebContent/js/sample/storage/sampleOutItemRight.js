var sampleOutItemTab, oldChangeLog;
// 出库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.sample.outId,
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "code");
			
			$(td).attr("infoFrom", rowData['infoFrom']);
			$(td).attr("infoFromId", rowData['infoFromId']);
		}
	})
	colOpts.push({
		"data" : "sampleCode",
		"title" : biolims.common.sampleCode,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleCode");
		},
	});

	/*colOpts.push({
		"data" : "barCode",
		"title" : biolims.common.barCode,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "barCode");
		},
	});*/
	/*colOpts.push({
		"data" : "dataTraffic",
		"title" : biolims.common.throughput,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "dataTraffic");
		},
	});*/
	colOpts.push({
		"data" : "productId",
		"title" : biolims.master.productId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "productId");
		},
	});
	colOpts.push({
		"data" : "productName",
		"title" : biolims.master.product,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "productName");
		},
	});
	colOpts.push({
		"data" : "sampleDeteyionName",
		"title" : "检测项",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "sampleDeteyionName");
			$(td).attr("sampleDeteyionId", rowData['sampleDeteyionId']);
		},
	});
	colOpts.push({
		"data" : "sampleOrder-id",
		"title" : "关联订单号",
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		},
	});
	colOpts.push({
		"data" : "samplingDate",
		"title" : biolims.sample.samplingDate,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "samplingDate");
		},
	});
	colOpts.push({
		"data" : "sampleType",
		"title" : biolims.common.sampleType,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleType");
		},
	});
	colOpts.push({
		"data" : "location",
		"title" : biolims.common.location,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "location");
		},
	});
	/*colOpts.push({
		"data" : "qpcrConcentration",
		"title" : biolims.QPCR.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "qpcrConcentration");
		},
	});
	colOpts.push({
		"data" : "indexa",
		"title" : 'index',
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "indexa");
		},
	});*/
	/*colOpts.push({
		"data" : "concentration",
		"title" : biolims.common.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "concentration");
		},
	});
	colOpts.push({
		"data" : "volume",
		"title" : biolims.common.volume,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "volume");
		},
	});
	colOpts.push({
		"data" : "sumTotal",
		"title" : biolims.common.sumNum,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sumTotal");
		},
	});*/
	colOpts.push({
		"data" : "applyUser-id",
		"title" : biolims.sample.applyUserId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "applyUser-id");
		},
	});
	colOpts.push({
		"data" : "applyUser-name",
		"title" : biolims.sample.applyUserName,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "applyUser-name");
		},
	});
	
	
	colOpts.push({
		"data" : "nextFlowId",
		"title" : biolims.common.nextFlowId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "nextFlowId");
		},
	});
	colOpts.push({
		"data" : "nextFlow",
		"title" : biolims.common.nextFlow,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "nextFlow");
		},
	});
	
	colOpts.push({
		"data" : "isDepot",
		"title" : biolims.wk.ifBackLib,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "isDepot");
		},
	});
	colOpts.push({
		"data" : "pronoun",
		"title" : biolims.common.pronoun,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "pronoun");
		},
	});
	colOpts.push({
		"data" : "tempId",
		"title" : biolims.common.tempId,
		"visible":false,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "tempId");
		},
	});
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"className": "edit",
		"width":"150px",
		"createdCell" : function(td, data,rowData) {
			$(td).attr("saveName", "note");
			$(td).attr("tempId", rowData['tempId']);
			
		},
		
	});

	var tbarOpts = [];
	
	var state = $("#sampleOut_state").text();
	if(state!="Complete"){
		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				//刷新左侧表
				var sampleOutAllTabs = $("#sampleOutAlldiv")
						.DataTable();
				removeChecked(
						$("#sampleOutItemdiv"),
						"/sample/storage/sampleOut/delSampleOutItems.action",
						"删除出库申请明细数据：", $("#sampleOut_id").text(),sampleOutAllTabs);
				
//				sampleOutAllTabs.ajax.reload();

			}
		});
tbarOpts.push({
	text : biolims.common.Editplay,
	action : function() {
		editItemLayer($("#sampleOutItemdiv"))
	}
})
tbarOpts.push({
	text : biolims.common.save,
	action : function() {
		saveSampleOutItemTab();
	}
})
	}
	
	
	
	tbarOpts.push({
		text: '<i class="fa fa-paypal"></i> '+biolims.common.nextFlow,
		action: function() {
			nextFlow();
		}
	});
//	tbarOpts.push({
//		text : "产物数量",
//		action : function() {
//			var rows = $("#sampleOutItemdiv .selected");
//			var length = rows.length;
//			if (!length) {
//				top.layer.msg("请选择数据");
//				return false;
//			}
//			top.layer.open({
//				type : 1,
//				title : "产物数量",
//				content : $('#batch_data'),
//				area : [ document.body.clientWidth - 600,
//						document.body.clientHeight - 200 ],
//				btn : biolims.common.selected,
//				yes : function(index, layer) {
//					var productNum = $("#productNum").val();
//					rows.addClass("editagain");
//					rows.find("td[savename='productNum']").text(productNum);
//					top.layer.close(index);
//				}
//			});
//		}
//	})
	var sampleOutItemOps = table(
			true,
			$("#sampleOut_id").text(),
			"/sample/storage/sampleOut/showSampleOutItemTableJson.action",
			colOpts, tbarOpts);
	sampleOutItemTab = renderData($("#sampleOutItemdiv"),
			sampleOutItemOps);
	// 选择数据并提示
	sampleOutItemTab.on('draw', function() {
		var index = 0;
		$("#sampleOutItemdiv .icheck").on(
				'ifChanged',
				function(event) {
					if ($(this).is(':checked')) {
						index++;
						$("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "2px solid #000");
					} else {
						var tt = $("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "1px solid gainsboro");
						index--;
					}
					top.layer.msg(biolims.common.Youselect + index
							+ biolims.common.data);
				});
		oldChangeLog = sampleOutItemTab.ajax.json();

	});
});



//下一步流向
function nextFlow() {
	var rows = $("#sampleOutItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var productId = "";
	var sampleType="";
	$.each(rows, function(j, k) {
		sampleType = $(k).find("td[savename='sampleType']").text();
		productId = $(k).find("td[savename='productId']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=SampleOut&sampleType="+encodeURIComponent(encodeURIComponent(sampleType))+"&productId="+productId,''],
		yes: function(index, layero) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}


// 保存
function saveSampleOutItemTab() {
	var ele = $("#sampleOutItemdiv");
	var changeLog = "库存主数据出库:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type : 'post',
		url : '/sample/storage/sampleOut/saveSampleOutItemTable.action',
		data : {
			id : $("#sampleOut_id").text(),
			dataJson : data,
			logInfo : changeLog
		},
		success : function(data) {
			var data = JSON.parse(data)
			if (data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.msg(biolims.common.saveFailed)
			}
			;
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for ( var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			 if(k == "note") {
				 json["tempId"] = $(tds[j]).attr("tempId");
				 continue;
			 }
			 if(k == "sampleDeteyionName") {
				 json["sampleDeteyionId"] = $(tds[j]).attr("sampleDeteyionId");
			 }
			 if(k == "code") {
				 json["infoFrom"] = $(tds[j]).attr("infoFrom");
				 json["infoFromId"] = $(tds[j]).attr("infoFromId");
			 } 
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开 创建日期: 2018/03/08 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += 'ID为"' + id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
