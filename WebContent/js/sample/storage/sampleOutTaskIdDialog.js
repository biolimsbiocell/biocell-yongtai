var sampleOutDialogTaskIdGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'taskId',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'type',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:"id",
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'taskId',
		header:biolims.common.orderId,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.order,
		width:20*6,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*5,
		sortable:true
		});
		cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*7,
		sortable:true
		});
		var outTypec = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [{
					id : '0',
					name : biolims.common.returnExperiment
				}, {
					id : '1',
					name :biolims.common.returnCustom
				}, {
					id : '2',
					name : biolims.common.outsourceOutStorage
				}, {
					id : '3',
					name : biolims.common.sampleDestruction	
				}]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
		cm.push({
			dataIndex:'type',
			hidden : false,
			header:biolims.storage.outType,
			width:20*6,
			renderer: Ext.util.Format.comboRenderer(outTypec)
		});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*4,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleOut/showsampleOutTaskIdDialogListJson.action?type="+$("#sampleOutTaskId_type").val();
	var opts={};
	opts.title=biolims.common.missionList;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleOutTaskIdFun(rec);
	};
	sampleOutDialogTaskIdGrid=gridTable("show_dialog_sampleOutTaskId_div",cols,loadParam,opts);
	$("#show_dialog_sampleOutTaskId_div").data("sampleOutDialogTaskIdGrid", sampleOutDialogTaskIdGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleOutDialogTaskIdGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
