var sampleOutApplyDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'workOrder-id',
		type:"string"
	});
	    fields.push({
		name:'workOrder-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
		});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:'ID',
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'workOrder-id',
		header:biolims.common.workOrderId,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'workOrder-name',
		header:biolims.common.workOrderName,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'acceptUser-id',
		header:biolims.wk.confirmUserId,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.wk.confirmUserName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.wk.confirmDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleOutApply/showDialogSampleOutApplyListJson.action?outType="+$("#outType").val();
	var opts={};
	opts.title=biolims.sample.outboundApply;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleOutApplyFun(rec);
	};
	sampleOutApplyDialogGrid=gridTable("show_dialog_sampleOutApply_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 500;
		option.height = 300;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleOutApplyDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
