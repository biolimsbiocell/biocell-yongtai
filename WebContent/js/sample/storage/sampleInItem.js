﻿
var sampleInItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'checked',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'dicSampleType-name',
			type:"string"
		});
	   fields.push({
			name:'classify',
			type:"string"
		});
	   fields.push({
			name:'upLocation',
			type:"string"
		});
	   
	   
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});	   
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
    });
	   fields.push({
		name:'productName',
		type:"string"
    });
	   fields.push({
		name:'samplingDate',
		type:"string",
    });
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
		name:'sampleIn-id',
		type:"string"
	});
	    fields.push({
		name:'sampleIn-name',
		type:"string"
	});
	    fields.push({
			name:'sampleTypeId',
			type:"string"
		});  
	    fields.push({
			name:'dicSampleType-id',
			type:"string"
		});
	   fields.push({
			name:'infoFrom',
			type:"string"
		});  
	   fields.push({
			name:'patientName',
			type:"string"
		});
	   fields.push({
			name:'concentration',
			type:"string"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'sumTotal',
			type:"string"
		});
	   fields.push({
			name:'customer-id',
			type:"string"
		});
	   fields.push({
			name:'customer-name',
			type:"string"
		});
	   fields.push({
			name:'customer-street',
			type:"string"
		});
	   fields.push({
			name:'sellPerson-id',
			type:"string"
		});
	   fields.push({
			name:'sellPerson-name',
			type:"string"
		});
	   fields.push({
			name:'unitGroup-id',
			type:"string"
		});
		fields.push({
			name:'unitGroup-name',
			type:"string"
		});
	  fields.push({
			name:'fkUser-id',
			type:"string"
		});
		fields.push({
			name:'fkUser-name',
			type:"string"
		});
		fields.push({
			name:'isRp',
			type:"string"
		});
		  fields.push({
				name:'techJkServiceTask-id',
				type:"string"
			});
		   fields.push({
				name:'techJkServiceTask-name',
				type:"string"
			});
		   fields.push({
				name:'tjItem-id',
				type:"string"
			});
		   fields.push({
				name:'tjItem-inwardCode',
				type:"string"
			});
		   fields.push({
				name:'groupNo',
				type:"string"
			});
		   fields.push({
				name:'barCode',
				type:"string"
			});
		   fields.push({
			   name:'dataTraffic',
			   type:"string"
		   });
		   
		   fields.push({
				name:'sampleState',
				type:"string"
			});
		   fields.push({
				name:'type',
				type:"string"
			});
		   fields.push({
				name:'sampleStyle',
				type:"string"
			});
			  fields.push({
					name:'qpcrConcentration',
					type:"string",
			   });
			   fields.push({
					name:'indexa',
					type:"string",
			   });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleStyle',
		hidden : true,
		header:biolims.master.productType,
		width:20*6
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.sample.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleState',
		hidden : true,
		header:biolims.common.IfClaim,
		width:20*6
	});
	cm.push({
		dataIndex:'type',
		hidden : true,
		header:biolims.sample.sampleSource,
		width:20*6
	});
	cm.push({
		dataIndex:'groupNo',
		hidden : false,
		header:biolims.common.SampleCollectionPacketNumber,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'checked',
		hidden : true,
		header:biolims.common.checked,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.originalSampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.describe,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		header:biolims.sample.num,
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'sellPerson-id',
		header:biolims.common.salesId,
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'sellPerson-name',
		header:biolims.common.sales,
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:biolims.common.location,
		width:20*8,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.testProject,
		width:20*6,
		hidden : false,
//		sortable:true
	});
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		hidden : false,
		width:30*6,
		sortable:true
	});
	
	/*cm.push({
		dataIndex:'qpcrConcentration',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:biolims.common.unitGroupId,
		width:15*10,
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: biolims.common.unitGroup,
		width:15*10,
		hidden : true
//		sortable:true,
//		editor : testUnitGroup
	});

	cm.push({
		dataIndex:'customer-id',
		hidden : true,
		header:biolims.sample.customerId,
		width:20*6
	});
	cm.push({
		dataIndex:'customer-name',
		hidden : true,
		header:biolims.sample.customerName,
		width:20*6
	});
	cm.push({
		dataIndex:'customer-street',
		hidden : true,
		header:biolims.sample.customerStreet,
		width:20*6
	});
	cm.push({
		dataIndex:'upLocation',
		hidden : true,
		header:biolims.sample.upLocation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.sample.inStorageTempId,
		width:30*6
	});
	cm.push({
		dataIndex:'fkUser-id',
		hidden : true,
		header:biolims.common.redepositorId,
		width:20*6
	});
	cm.push({
		dataIndex:'fkUser-name',
		hidden : false,
		header:biolims.common.redepositor,
		width:20*6
	});
	var storeRp = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes] ]
	});
	var rpCob = new Ext.form.ComboBox({
		store : storeRp,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isRp',
		hidden : false,
		header:biolims.common.ifThereStain,
		width:20*6,
		editor : rpCob,
		renderer : Ext.util.Format.comboRenderer(rpCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleIn-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleIn-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	//原始样本类型
	cm.push({
		dataIndex:'sampleTypeId',
		hidden : true,
		header:biolims.common.originalSampleTypeId,
		width:20*6
	});
	//样本类型
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.sampleTypeId,
		width:20*6
	});
	cm.push({
		dataIndex:'infoFrom',
		hidden : true,
		header:biolims.sample.infoFrom,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		header: biolims.common.scienceServiceId,
		width:15*10,
		hidden : true
	});
	cm.push({
		dataIndex:'tjItem-id',
		header: biolims.wk.scienceServiceDetailId,
		width:15*10,
		hidden : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleIn/showSampleInItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit=1000;
	var opts={};
	opts.title=biolims.sample.warehousingDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
//	if($("#sampleIn_stateName").val()!=biolims.common.finish){
//       opts.delSelect = function(ids) {
//		ajax("post", "/sample/storage/sampleIn/delSampleInItem.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				sampleInItemTempGrid.getStore().commitChanges();
//				sampleInItemTempGrid.getStore().reload();
//				sampleInItemGrid.getStore().commitChanges();
//				sampleInItemGrid.getStore().reload();
//				message(biolims.common.deleteSuccess);
//			} else {
//				message(biolims.common.deleteFailed);
//			}
//		}, null);
//	};
//    
//	opts.tbar.push({
//		text : biolims.sample.selectLocation,
//		handler :FrozenLocationFun
//	});
//	
//	opts.tbar.push({
//		text : biolims.sample.selectLocation+"(tree)",
//		handler : showStoragePosition
//	});
//	opts.tbar.push({
//		text : biolims.common.selectBox,
//		handler : selectBox
//	});
//	opts.tbar.push({
//		text : biolims.common.redepositor,
//		handler : loadTestUser
//	});
//	opts.tbar.push({
//	text : biolims.common.batchIfThereStain,
//	handler : function() {
//		var records = sampleInItemGrid.getSelectRecord();
//		if (records && records.length > 0) {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_isRp_div"), biolims.common.batchIfThereStain, null, {
//				"确定(confirm)" : function() {
//					
//						var isRp = $("#isRp").val();
//						sampleInItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isRp", isRp);
//						});
//						sampleInItemGrid.startEditing(0, 0);
//					
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}else{
//			message(biolims.common.pleaseSelect);
//		}
//	}
//});
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		handler : null
//	});
//	opts.tbar.push({
//		text : biolims.common.editableColAppear,
//		handler : null
//	});
//	opts.tbar.push({
//		text : biolims.common.uncheck,
//		handler : null
//	});
//	}
	sampleInItemGrid=gridEditTable("sampleInItemdiv",cols,loadParam,opts);
	$("#sampleInItemdiv").data("sampleInItemGrid", sampleInItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
		//选择位置
		function FrozenLocationFun(){
			var selRecords = sampleInItemGrid.getSelectionModel().getSelections(); 
//			for(var i=0;i<selRecords.length;i++){
//				if(selRecords[i].get("id")==null){
//					message("请先保存新添加的数据然后再设置存储位置。");
//					return ;
//				}
//			}
			 var win = Ext.getCmp('FrozenLocationFun');
			 if (win) {win.close();}
			 var ProjectFun= new Ext.Window({
			 id:'FrozenLocationFun',modal:true,title:biolims.sample.selectLocation,layout:'fit',width:900,height:600,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='yes' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog.action?typeId="+$("#sampleIn_sampleType").val()+"'  frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
			  ProjectFun.close(); }  }]  });     ProjectFun.show(); }
		function setFrozenLocationFun(str,upStr){
			
			var ids = str.split("-");
			var str1="";
			var strId="";
			if(ids.length==3){
				if(ids[2].length==2){
					strId = ids[2].substr(0,1)+"0"+ids[2].substr(1);
				}else{
					strId = ids[2];
				}
				str1 = ids[0]+"-"+ids[1]+"-"+strId;
			}else if(ids.length==4){
				if(ids[3].length==2){
					strId = ids[3].substr(0,1)+"0"+ids[3].substr(1);
				}else{
					strId = ids[3];
				}
				str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+strId;
			}else if(ids.length==5){
				if(ids[4].length==2){
					strId = ids[4].substr(0,1)+"0"+ids[4].substr(1);
				}else{
					strId = ids[4];
				}
				str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+ids[3]+"-"+strId;
			}
			 	var gridGrid = $("#sampleInItemdiv").data("sampleInItemGrid");
//				var selRecords = gridGrid.getSelectOneRecord(); 
			 	var selRecords=gridGrid.getSelectionModel().getSelections(); 
				if(selRecords.length==1){
					$.each(selRecords, function(i, obj) {
						obj.set('location',str1);
					});
//					$("#sampleIn_location").val(str1);
				}else{
					$("#sampleIn_location").val(str1);
				}
//				if(selRecords){
//					$("#sampleIn_location").val(str1);
//				}
				 var win = Ext.getCmp('FrozenLocationFun');
				 if(win){win.close();}
		 }	
		 function showStoragePosition(){
			 var win = Ext.getCmp('showStoragePosition');
			 if (win) {win.close();}
			 var showStoragePosition= new Ext.Window({
			 id:'showStoragePosition',modal:true,title:biolims.common.pleaseSelectLocation,layout:'fit',width:500,height:500,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog2.action?setStoragePostion=true&flag=showStoragePosition' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
			  showStoragePosition.close(); }  }]  });     showStoragePosition.show(); }
			
			  function setshowStoragePosition(id){
				 
//				  var selRecords = sampleInItemGrid.getSelectOneRecord();
				 	var selRecords=sampleInItemGrid.getSelectionModel().getSelections(); 
//					selRecords[0].set('location',str);
//					selRecords[0].set('upLocation',upStr);
					
				//	var ads = str.split(",");
				//	alert(ads);
					$.each(selRecords, function(i, obj) {
						obj.set('location',id);
					});
					$("#sampleIn_location").val(id);
//					if(selRecords){
//						selRecords.set('location',id);
//					}
			 var win = Ext.getCmp('showStoragePosition');
			 if(win){win.close();}
			 }
	//选择盒子
	  function selectBox(){
//		  var win = Ext.getCmp('StorageBoxFun');
//			 if (win) {win.close();}
//			 var StorageBoxFun= new Ext.Window({
//			 id:'StorageBoxFun',modal:true,title:biolims.sample.selectLocation,layout:'fit',width:900,height:660,closeAction:'close',
//			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//			 collapsible: true,maximizable: true,
//			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//			 html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/storage/storageBox/storageBoxSelect.action?flag=StorageBoxFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
//			 buttons: [
//			 { text: biolims.common.close,
//			  handler: function(){
//				  StorageBoxFun.close(); }  }]  });     StorageBoxFun.show(); 
			  
		 var url = "/system/storage/storageBox/storageBoxSelect.action?flag=StorageBoxFun";
		var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.selectBox, url, {
			"Confirm" : function() {
				
				var record = storageBoxDialogGrid.getSelectionModel().getSelections();
				
				ajax("post", "/system/storage/storageBox/setStorageBox.action", {
					spId : n,
					id : record[0].get("id")
				}, function(data) {
					if (data.success) {
						
						message(biolims.common.setSuccess);
					}
				});
				gridContainerBoxGrid.getStore().reload();
				$(this).dialog("close");
			}
		}, false, options);
		}
//	  function setStorageBoxFun(str,type){
//			var ids = str.split("-");
//			var str1="";
//			var strId="";
//			if(ids.length==3){
//				if(ids[2].length==2){
//					strId = ids[2].substr(0,1)+"0"+ids[2].substr(1);
//				}else{
//					strId = ids[2];
//				}
//				str1 = ids[0]+"-"+ids[1]+"-"+strId;
//			}else if(ids.length==4){
//				if(ids[3].length==2){
//					strId = ids[3].substr(0,1)+"0"+ids[3].substr(1);
//				}else{
//					strId = ids[3];
//				}
//				str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+strId;
//			}else if(ids.length==5){
//				if(ids[4].length==2){
//					strId = ids[4].substr(0,1)+"0"+ids[4].substr(1);
//				}else{
//					strId = ids[4];
//				}
//				str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+ids[3]+"-"+strId;
//			}
//			 	var gridGrid = $("#sampleInItemdiv").data("sampleInItemGrid");
//				var selRecords = gridGrid.getSelectOneRecord(); 
////			 	var selRecords=gridGrid.getSelectionModel().getSelections(); 
////				selRecords[0].set('location',str);
////				selRecords[0].set('upLocation',upStr);
//				
//			//	var ads = str.split(",");
//			//	alert(ads);
//			//	$.each(selRecords, function(i, obj) {
//			//		obj.set('location',ads[i]);
//			//	});
//				if(selRecords){
//					selRecords.set('location',str1);
//				}else{
//					$("#sampleIn_location").val(str1);
//				}
//				 
//				 var win = Ext.getCmp('StorageBoxFun')
//				 if(win){win.close();}
//		 }	
	  
	  //返库人
	  function loadTestUser(){
			var win = Ext.getCmp('loadTestUser');
			if (win) {win.close();}
			var loadTestUser= new Ext.Window({
			id:'loadTestUser',modal:true,title:biolims.common.chooseTester,layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center',
			html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: biolims.common.close,
			 handler: function(){
				 loadTestUser.close(); }  }]  });     loadTestUser.show(); 
		}
		function setreciveUserFun(id,name){
			var selRecords = sampleInItemGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('fkUser-id',id);
				obj.set('fkUser-name',name);
			});
			var win = Ext.getCmp('loadTestUser');
			if(win){
				win.close();
			}
		}