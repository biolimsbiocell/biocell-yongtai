var sampleOutApplyGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});

	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
			name:'sampleType-id',
			type:"string"
	});
	    fields.push({
	    	name:'sampleType-name',
	    	type:"string"
	});
	    fields.push({
			name:'businessType-id',
			type:"string"
	});
	    fields.push({
	    	name:'businessType-name',
	    	type:"string"
	});
	    fields.push({
			name:'workOrder-id',
			type:"string"
	});
	    fields.push({
	    	name:'workOrder-name',
	    	type:"string"
	});
	    fields.push({
	    	name:'type',
	    	type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'code',
		header:biolims.sample.sampleOutApplyId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*7,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:30*6,
		
		sortable:true
	});
	var outType = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : biolims.common.returnExperiment
			}, {
				id : '1',
				name : biolims.common.returnCustom
			}, {
				id : '2',
				name : biolims.common.outsourceOutStorage
			}, {
				id : '3',
				name : biolims.common.sampleDestruction
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'type',
		hidden : false,
		header:biolims.goods.type,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(outType)
	});
		cm.push({
			dataIndex:'workOrder-id',
			hidden:true,
			header:biolims.common.workOrderId,
			width:20*10,
			sortable:true
		});
		cm.push({
			dataIndex:'workOrder-name',
			header:biolims.common.workOrderName,
			hidden:true,
			width:20*10,
			sortable:true
		});
		cm.push({
			dataIndex:'sampleType-id',
			header:biolims.common.sampleTypeId,
			width:20*6,
			hidden:true,
			sortable:true
		});
		cm.push({
			dataIndex:'sampleType-name',
			header:biolims.common.sampleTypeName,
			width:20*6,
			hidden:true,
			sortable:true
		});
		cm.push({
			dataIndex:'businessType-id',
			header:biolims.sample.businessTypeId,
			width:20*6,
			hidden:true,
			sortable:true
		});
		cm.push({
			dataIndex:'businessType-name',
			header:biolims.sample.businessTypeName,
			width:20*6,
			hidden:true,
			sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'acceptUser-id',
		header:biolims.wk.confirmUserId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.wk.confirmUserName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.wk.confirmDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleOutApply/showSampleOutApplyListJson.action";
	var opts={};
	opts.title=biolims.sample.outboundApply;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sampleOutApplyGrid=gridTable("show_sampleOutApply_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/sample/storage/sampleOutApply/editSampleOutApply.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/sample/storage/sampleOutApply/editSampleOutApply.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/storage/sampleOutApply/viewSampleOutApply.action?id=' + id;
}
function exportexcel() {
	sampleOutApplyGrid.title = biolims.common.exportList;
	var vExportContent = sampleOutApplyGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startacceptDate").val() != undefined) && ($("#startacceptDate").val() != '')) {
					var startacceptDatestr = ">=##@@##" + $("#startacceptDate").val();
					$("#acceptDate1").val(startacceptDatestr);
				}
				if (($("#endacceptDate").val() != undefined) && ($("#endacceptDate").val() != '')) {
					var endacceptDatestr = "<=##@@##" + $("#endacceptDate").val();

					$("#acceptDate2").val(endacceptDatestr);

				}
				
				
				commonSearchAction(sampleOutApplyGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
