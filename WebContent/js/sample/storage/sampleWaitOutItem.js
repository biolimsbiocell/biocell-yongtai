﻿
var sampleWaitOutItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'checked',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});	   
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'tempId',
		type:"string"
	});
	    fields.push({
			name:'sampleType',
			type:"string"
		});
	    fields.push({
			name:'concentration',
			type:"string"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'sumTotal',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-project-name',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-crmCustomer-name',
			type:"string"
		});  
	   fields.push({
			name:'sampleInfo-crmDoctor-name',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-project-id',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-crmCustomer-id',
			type:"string"
		});  
	   fields.push({
			name:'sampleInfo-crmDoctor-id',
			type:"string"
		});
	   fields.push({
			name:'sellPerson-id',
			type:"string"
		});
	   fields.push({
			name:'sellPerson-name',
			type:"string"
		});
	   
	   fields.push({
			name:'sampleInfo-productId',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-productName',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-patientName',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-receiveDate',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-name',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-idCard',
			type:"string"
		});
	   fields.push({
			name:'isRp',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-createUser-name',
			type:"string"
		});
	   fields.push({
			name:'tjItem-id',
			type:"string"
		});
	   fields.push({
			name:'tjItem-inwardCode',
			type:"string"
		});
	   fields.push({
			name:'tjItem-orderId',
			type:"string"
		});
	   fields.push({
			name:'barCode',
			type:"string"
		});
	   fields.push({
		   name:'dataTraffic',
		   type:"string"
	   });
	   fields.push({
			name:'productId',
			type:"string"
	    });
		   fields.push({
			name:'productName',
			type:"string"
	    });
		   fields.push({
			name:'samplingDate',
			type:"string",
	    });
		   fields.push({
				name:'sampleStyle',
				type:"string",
		    });
		   
		   fields.push({
				name:'qpcrConcentration',
				type:"string",
		   });
		   fields.push({
				name:'indexa',
				type:"string",
		   });
		   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleStyle',
		hidden : true,
		header:"产品类型",
		width:20*6
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.sample.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*7
	});
	
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex:'checked',
		hidden : true,
		header:biolims.common.checked,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.testProject,
		width:20*6,
		hidden : false,
//		sortable:true
	});
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		hidden : false,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleInfo-idCard',
		hidden : false,
		header:biolims.common.outCode,
		width:20*7
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:biolims.sample.num,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:biolims.common.location,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qpcrConcentration',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-project-id',
		hidden : true,
		header:biolims.common.contractCodeId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-project-name',
		hidden : false,
		header:biolims.common.contractName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-crmCustomer-id',
		hidden : true,
		header:biolims.common.customId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-crmCustomer-name',
		hidden : false,
		header:biolims.common.custom,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-crmDoctor-id',
		hidden : true,
		header:biolims.sample.crmDoctorId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-crmDoctor-name',
		hidden : false,
		header:biolims.sample.crmDoctorName,
		width:20*6
	});
	cm.push({
		dataIndex:'sellPerson-id',
		hidden : true,
		header:biolims.common.salesId,
		width:20*6
	});
	cm.push({
		dataIndex:'sellPerson-name',
		hidden : true,
		header:biolims.common.sales,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-productName',
		hidden : true,
		header:biolims.common.productName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*10
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : true,
		header:"样本接收时间",
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.sample.inStorageTempId,
		width:30*6
	});
	var storeRp = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes] ]
	});
	var rpCob = new Ext.form.ComboBox({
		store : storeRp,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isRp',
		hidden : false,
		header:biolims.common.ifThereStain,
		width:20*6,
		editor : rpCob,
		renderer : Ext.util.Format.comboRenderer(rpCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		header: biolims.common.qcTask,
		width:15*10,
		hidden : false
	});
	cm.push({
		dataIndex:'tjItem-id',
		header: "科技服务明细id",
		width:15*10,
		hidden : true
	});
	cm.push({
		dataIndex:'tjItem-inwardCode',
		header: "内部项目号",
		width:15*10,
		hidden : true
	});
	cm.push({
		dataIndex:'sampleInfo-name',
		header: biolims.common.packetNumber,
		width:15*10,
		hidden : false
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleIn/showSampleWaitOutItemListJson.action?cid="+ $("#cid").val()+"&type="+$("#type").val();
	var opts={};
	opts.title=biolims.sample.outBoundDetail;
	opts.height =  document.body.clientHeight-180;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/sample/storage/sampleIn/delSampleInItem.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				scpProToGrid.getStore().commitChanges();
//				scpProToGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
    

	sampleWaitOutItemGrid=gridTable("sampleWaitOutItemdiv",cols,loadParam,opts);
	$("#sampleWaitOutItemdiv").data("sampleWaitOutItemGrid", sampleWaitOutItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function searchGrid(){
	
	commonSearchAction(sampleWaitOutItemGrid);
	
}
