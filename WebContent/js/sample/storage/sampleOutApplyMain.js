﻿$(function() {
	load("/sample/wk/showWaitSampleWKInfoList.action", {
		state : $("#hid_state").val(),
		title : biolims.sample.libCaptureScheme,
		sort : 'wk.qpcrMoerNd',
		dir : 'ASC'
	}, $("#left"), null);
	$("#tabs").tabs();
	
	load("/sample/pooling/toEditPoolingTask.action", {
		id : $("#pooling_id").val(),
		taskType : '5',
		bpmTaskId : $("#bpmTaskId").val()
	}, $("#pooling_task_list_div"), null);
	setTimeout(function() {
	load("/sample/pooling/showPoolingTaskItemList.action", null, $("#pooling_task_item_div"), null);
	
	load("/sample/storage/showSampleStorageList.action", null, "#sample-tabs-1", null);
	},400);
	var handlemethod = $("#handlemethod").val();
	if ('view' === handlemethod) {
		setTimeout(function() {
			settextreadonlyByAllJquery();
			$("#bat_text").removeClass();
			$("#bat_text").attr("readOnly", false);
		}, 1000);
	} else if ('modify' === handlemethod) {
		setTimeout(function() {
			settextreadonlyJquery($("#spt_id"));
		}, 1000);
	}
	$("#toolbarbutton_status").click(function() {
		commonChangeState("formId=" + $("#pooling_id").val() + "&tableId=pooling");
	});
	
	$("#toolbarbutton_tjsp").click(
			function() {
				//commonWorkflowSubmit("applicationTypeTableId=plasmaTask&formId=" + $("#task_id").val() + "&title="
				//		+ encodeURI(encodeURI($("#note").val())));
				if (poolingTaskGrid.getAllRecord().length > 0) {
					
					if(poolingTaskGrid.getModifyRecord().length > 0){
						
						message(biolims.common.pleaseSaveRecord);
						return;
						
					}
				submitWorkflow("pooling", {
					userId : userId,
					userName : userName,
					formId : $("#pooling_id").val(),
					title : $("#spt_note").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				}else{
					message(biolims.common.addAndSave);
					return;
				}	
			});
	$("#toolbarbutton_sp").click(function() {
		//commonWorkflowLook("applicationTypeTableId=plasmaTask&formId=" + $("#task_id").val());
		
		if($("#spt_stateName").val()==biolims.sample.tested){
			
			if(!$("#spt_ration").val()){
				
				message(biolims.sample.pleaseFillGroup);
				return;
			}	
			if(!$("#spt_isGood").val()){
				
				message(biolims.sample.pleaseSelectQC);
				return;
			}	
			
			
		}
		
		
		completeTask($("#task_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
	});

	$("#toolactions_ck").click(function() {
		commonWorkflowLook("applicationTypeTableId=plasmaTask&formId=" + $("#task_id").val());
	});
	
	$("#toolbarbutton_add").click(function() {
		window.location = window.ctx + '/sample/pooling/toMainframe.action?reqMethodType=add';
	});

	$("#toolbarbutton_list").click(function() {
		window.location = window.ctx + '/sample/pooling/showPoolingTaskList.action';
	});
	$("#toolbarbutton_save").click(function() {
		var id = $("#spt_id").val();
		checkValue2();
		if (!id) {
			message(biolims.sample.MixIsEmpty);
			return;
		}
		if(!$("#spt_acceptUser_id").val()){
			message(biolims.sample.pleaseSelectExperimenter);
			return;
		}
		if(!$("#spt_x").val()){
			message(biolims.sample.pleaseFillX);
			return;
		}
		if(!$("#spt_Lane").val()){
			message(biolims.sample.pleaseFillGroupNo);
			return;
		}
		
		if (!$("#pooling_id").val()) {
			ajax("post", "/sample/task/isIdAreThere.action", {
				objName : "SamplePoolingTask",
				objProperty : "id",
				state : "",
				vals : "'" + id + "'"
			}, function(data) {
				if (!data.success) {
					message(biolims.sample.systemError);
				} else if (data.isSave && data.isSave.length > 0) {
					message(biolims.sample.MixIsUsed);
				} else {
					var poolingTaskGrid = $("#pooling_task_item_div").data("poolingTaskGrid");
					$("#item_data_json").val(commonGetModifyRecords(poolingTaskGrid));
					
					$("#toolbarbutton_save").hide();
					$("#pooling_task_form").attr("action", "/sample/pooling/save.action").submit();
				}
			}, null);
		} else {
			var getPoolingTaskItemDate = $("#pooling_task_item_div").data("getPoolingTaskItemData");
			var poolingTaskGrid = $("#pooling_task_item_div").data("poolingTaskGrid");
			$("#item_data_json").val(commonGetModifyRecords(poolingTaskGrid));
			$("#toolbarbutton_save").hide();
			$("#pooling_task_form").attr("action", "/sample/pooling/save.action").submit();
		}
	});
	$("#toolbarbutton_print").click(function() {
		var url = '__report=WKBuHuoRWD.rptdesign&id=' + $("#spt_id").val();
		commonPrint(url);
	});
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.sample.orderPrint
			});
		item.on('click', ckcrk);
		});
	function ckcrk(){
		var url = '__report=poolingItemcheck.rptdesign&id=' + $("#pooling_id").val();
		commonPrint(url);
	}
	new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-29,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.sample.libCaptureScheme,
			autoWidth : true,
			contentEl : 'markup'
		} ]
	});

});