var sampleOutApplyItemTab, oldChangeLog;
// 出库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.sample.applicationDetailId,
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "code");
			
			$(td).attr("sumTotal", rowData['sumTotal']);
			$(td).attr("sampleDeteyionId", rowData['sampleDeteyionId']);
			$(td).attr("sampleDeteyionName", rowData['sampleDeteyionName']);
			$(td).attr("infoFrom", rowData['infoFrom']);
			$(td).attr("infoFromId", rowData['infoFromId']);
		}
	})
	colOpts.push({
		"data" : "sampleCode",
		"title" : biolims.common.sampleCode,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleCode");
		},
	});

	/*colOpts.push({
		"data" : "barCode",
		"title" : biolims.common.barCode,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "barCode");
		},
	});*/
	/*colOpts.push({
		"data" : "dataTraffic",
		"title" : biolims.common.throughput,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "dataTraffic");
		},
	});*/
	colOpts.push({
		"data" : "productId",
		"title" : biolims.common.productId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "productId");
		},
	});
	colOpts.push({
		"data" : "productName",
		"title" : biolims.common.testProject,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "productName");
		},
	});
	colOpts.push({
		"data" : "sampleOrder-id",
		"title" : "关联订单号",
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		},
	});
	colOpts.push({
		"data" : "samplingDate",
		"title" : biolims.sample.samplingDate,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "samplingDate");
		},
	});
	colOpts.push({
		"data" : "sampleType",
		"title" : biolims.common.sampleType,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleType");
		},
	});
	colOpts.push({
		"data" : "location",
		"title" : biolims.common.location,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "location");
		},
	});
	/*colOpts.push({
		"data" : "qpcrConcentration",
		"title" : biolims.QPCR.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "qpcrConcentration");
		},
	});*/
	/*colOpts.push({
		"data" : "indexa",
		"title" : 'index',
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "indexa");
		},
	});*/
	/*colOpts.push({
		"data" : "concentration",
		"title" : biolims.common.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "concentration");
		},
	});
	colOpts.push({
		"data" : "volume",
		"title" : biolims.common.volume,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "volume");
		},
	});
	colOpts.push({
		"data" : "sumTotal",
		"title" : biolims.common.sumNum,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sumTotal");
		},
	});*/
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"width":"150px",
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "note");
		},
	});

	var tbarOpts = [];
	
	var state = $("#sampleOutApply_state").text();
	if(state!="Complete"){
		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				//刷新左侧表
				var sampleOutApplyAllTabs = $("#sampleOutApplyAlldiv")
						.DataTable();
				removeChecked(
						$("#sampleOutApplyItemdiv"),
						"/sample/storage/sampleOutApply/delSampleOutApplyItems.action",
						"删除出库申请明细数据：", $("#sampleOutApply_id").text(),sampleOutApplyAllTabs);
				
//				sampleOutApplyAllTabs.ajax.reload();

			}
		});
		tbarOpts.push({
			text : biolims.common.Editplay,
			action : function() {
				editItemLayer($("#sampleOutApplyItemdiv"))
			}
		})
		tbarOpts.push({
			text : "绑定订单",
			action : function() {
				bindOrder();
			}
		})
	}
	

//	tbarOpts.push({
//		text : biolims.common.save,
//		action : function() {
//			saveSampleOutApplyItemTab();
//		}
//	})
//	tbarOpts.push({
//		text : "产物数量",
//		action : function() {
//			var rows = $("#sampleOutApplyItemdiv .selected");
//			var length = rows.length;
//			if (!length) {
//				top.layer.msg("请选择数据");
//				return false;
//			}
//			top.layer.open({
//				type : 1,
//				title : "产物数量",
//				content : $('#batch_data'),
//				area : [ document.body.clientWidth - 600,
//						document.body.clientHeight - 200 ],
//				btn : biolims.common.selected,
//				yes : function(index, layer) {
//					var productNum = $("#productNum").val();
//					rows.addClass("editagain");
//					rows.find("td[savename='productNum']").text(productNum);
//					top.layer.close(index);
//				}
//			});
//		}
//	})
	var sampleOutApplyItemOps = table(
			true,
			$("#sampleOutApply_id").text(),
			"/sample/storage/sampleOutApply/showSampleOutApplyItemTableJson.action",
			colOpts, tbarOpts);
	sampleOutApplyItemTab = renderData($("#sampleOutApplyItemdiv"),
			sampleOutApplyItemOps);
	// 选择数据并提示
	sampleOutApplyItemTab.on('draw', function() {
		var index = 0;
		$("#sampleOutApplyItemdiv .icheck").on(
				'ifChanged',
				function(event) {
					if ($(this).is(':checked')) {
						index++;
						$("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "2px solid #000");
					} else {
						var tt = $("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "1px solid gainsboro");
						index--;
					}
					top.layer.msg(biolims.common.Youselect + index
							+ biolims.common.data);
				});
		oldChangeLog = sampleOutApplyItemTab.ajax.json();

	});
});

function bindOrder(){
	var rows = $("#sampleOutApplyItemdiv .selected");
	if(!rows.length){
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action?mark="+"SampleOutApply", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td").eq(
				0).text();
			top.layer.close(index);
			rows.addClass("editagain");
			rows.find("td[savename=sampleOrder-id]").text(id);
			
		},
	})
}

// 保存
//function saveSampleOutApplyItemTab() {
//	var ele = $("#sampleOutApplyItemdiv");
//	var changeLog = "库存主数据出库:";
//	var data = saveItemjson(ele);
//	changeLog = getChangeLog(data, ele, changeLog);
//
//	$.ajax({
//		type : 'post',
//		url : '/storage/out/saveSampleOutApplyItemTable.action',
//		data : {
//			id : $("#sampleOutApply_id").text(),
//			dataJson : data,
//			logInfo : changeLog
//		},
//		success : function(data) {
//			var data = JSON.parse(data)
//			if (data.success) {
//				top.layer.msg(biolims.common.saveSuccess);
//			} else {
//				top.layer.msg(biolims.common.saveFailed)
//			}
//			;
//		}
//	})
//}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for ( var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
			// if(k == "dicSampleTypeName") {
			// json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
			// continue;
			// }
			 if(k == "code") {
				 json["sumTotal"] = $(tds[j]).attr("sumTotal");
				 json["sampleDeteyionId"] = $(tds[j]).attr("sampleDeteyionId");
				 json["sampleDeteyionName"] = $(tds[j]).attr("sampleDeteyionName");
				 json["infoFrom"] = $(tds[j]).attr("infoFrom");
				 json["infoFromId"] = $(tds[j]).attr("infoFromId");
				 continue;
			 }
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开 创建日期: 2018/03/08 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '数量为"' + v.num + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
