var sampleOutGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'sampleOutApply-id',
		type:"string"
	});
	    fields.push({
		name:'sampleOutApply-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
		fields.push({
			name:'applyUser-id',
			type:"string"
		});
		fields.push({
			name:'applyUser-name',
			type:"string"
		});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
			name:'outType',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'code',
		header:biolims.sample.sampleOutStorageId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*7,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:30*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'sampleOutApply-id',
		header:biolims.sample.sampleOutApplyId,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'sampleOutApply-name',
		header:biolims.sample.sampleOutApplyName,
		hidden:true,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'acceptUser-id',
		header:biolims.wk.confirmUserId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.wk.confirmUserName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.confirmDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'applyUser-id',
		header:biolims.sample.applyUserId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'applyUser-name',
		header:biolims.sample.applyUserName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	var outType1 = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : biolims.common.outsourceOutStorage
			}, {
				id : '1',
				name :biolims.common.returnCustom
			}, {
				id : '2',
				name : biolims.common.outsourceOutStorage
			}, {
				id : '3',
				name : biolims.common.sampleDestruction
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'outType',
		hidden : false,
		header:biolims.storage.outType,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(outType1)
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleOut/showSampleOutListJson.action";
	var opts={};
	opts.title=biolims.sample.sampleOutStorage;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		console.log($("#selectId").val(id));
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sampleOutGrid=gridTable("show_sampleOut_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/sample/storage/sampleOut/editSampleOut.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/sample/storage/sampleOut/editSampleOut.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/storage/sampleOut/viewSampleOut.action?id=' + id;
}
function exportexcel() {
	sampleOutGrid.title = biolims.common.exportList;
	var vExportContent = sampleOutGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startacceptDate").val() != undefined) && ($("#startacceptDate").val() != '')) {
					var startacceptDatestr = ">=##@@##" + $("#startacceptDate").val();
					$("#acceptDate1").val(startacceptDatestr);
				}
				if (($("#endacceptDate").val() != undefined) && ($("#endacceptDate").val() != '')) {
					var endacceptDatestr = "<=##@@##" + $("#endacceptDate").val();

					$("#acceptDate2").val(endacceptDatestr);

				}
				
				
				commonSearchAction(sampleOutGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
function sampleOutApply(){
	var win = Ext.getCmp('sampleOutApply');
	if (win) {win.close();}
	var sampleOutApply= new Ext.Window({
	id:'sampleOutApply',modal:true,title:biolims.sample.selectOutApply,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/storage/sampleOutApply/sampleOutApplySelect.action?flag=sampleOutApply' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 sampleOutApply.close(); }  }]  });     sampleOutApply.show(); }
	 function setsampleOutApply(rec){
	document.getElementById('sampleOut_sampleOutApply').value=rec.get('id');
	document.getElementById('sampleOut_sampleOutApply_name').value=rec.get('name');
	var win = Ext.getCmp('sampleOutApply')
	if(win){win.close();}
	}

