var sampleOutApplyTab;
$(function() {
	var options = table(true, "",
			"/sample/storage/sampleOutApply/showSampleOutApplyTableJson.action", [ {
				"data" : "id",
				"title" : biolims.sampleOutApply.id,
			}, {
				"data" : "createUser-id",
				"title" : biolims.sampleOutApply.createUserId,
			}, {
				"data" : "createUser-name",
				"title" : biolims.sampleOutApply.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sampleOutApply.createDate,
			}, {
				"data" : "name",
				"title" : biolims.sampleOutApply.note,
			}, {
				"data" : "type-id",
				"title" : biolims.sampleOutApply.typeId
			}, {
				"data" : "type-name",
				"title" : biolims.sampleOutApply.type
			} ,{
				"data" : "stateName",
				"title" : biolims.sampleOutApply.stateName
			} ], null)
	sampleOutApplyTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleOutApplyTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/sample/storage/sampleOutApply/toEditSampleOutApply.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/sample/storage/sampleOutApply/toEditSampleOutApply.action?id='+ id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/sample/storage/sampleOutApply/toEditSampleOutApply.action?id='+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" :  biolims.sampleOutApply.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.sampleOutApply.createUserId,
		"type" : "input",
		"searchName" : "createUser-id",
	}, {
		"txt" :  biolims.sampleOutApply.createUserName,
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"txt" :biolims.sampleOutApply.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sampleOutApply.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"type" : "table",
		"table" : sampleOutApplyTab
	} ];
}
