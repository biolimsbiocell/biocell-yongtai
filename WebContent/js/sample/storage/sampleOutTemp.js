var sampleOutTempGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
			name:'num',
			type:"string"
		});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
			name:'sampleInItemId',
			type:"string"
		});
	    fields.push({
			name:'location',
			type:"string"
		});
	    fields.push({
		name:'method',
		type:"string"
	});
	    fields.push({
			name:'sampleType',
			type:"string"
		});
	    fields.push({
			name:'concentration',
			type:"string"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'sumTotal',
			type:"string"
		});
	   fields.push({
			name:'applyUser-id',
			type:"string"
		});
		    fields.push({
			name:'applyUser-name',
			type:"string"
		});
		    fields.push({
				name:'crmCustomer-id',
				type:"string"
			});
			    fields.push({
				name:'crmCustomer-name',
				type:"string"
			});
		    fields.push({
				name:'crmCustomer-street',
				type:"string"
			});
			    fields.push({
				name:'crmCustomer-postcode',
				type:"string"
			});
			    fields.push({
					name:'type',
					type:"string"
				});
			    fields.push({
					name:'supplier-id',
					type:"string"
				});
			    fields.push({
					name:'supplier-name',
					type:"string"
				});
			    fields.push({
					name:'supplier-linkMan',
					type:"string"
				});
			    fields.push({
					name:'supplier-linkTel',
					type:"string"
				});
			    fields.push({
					name:'taskId',
					type:"string"
				});
			    fields.push({
					name:'infoFrom',
					type:"string"
				}); 
			    fields.push({
					name:'barCode',
					type:"string"
				});
			    fields.push({
			    	name:'dataTraffic',
			    	type:"string"
			    });
			    fields.push({
					name:'productId',
					type:"string"
			    });
				   fields.push({
					name:'productName',
					type:"string"
			    });
				   fields.push({
					name:'samplingDate',
					type:"string",
			    });
				   fields.push({
					name:'patientName',
					type:"string",
			    });
				   fields.push({
						name:'sampleStyle',
						type:"string",
				    });
				   
				   fields.push({
						name:'qpcrConcentration',
						type:"string",
				   });
				   fields.push({
						name:'indexa',
						type:"string",
				   });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleStyle',
		header:biolims.master.productType,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*7,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'barCode',
		hidden : true,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.testProject,
		width:20*6,
		hidden : true,
//		sortable:true
	});
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		hidden : true,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'num',
		header:biolims.sample.inventory,
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'location',
		header:biolims.common.location,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'qpcrConcentration',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*4
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*4
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*4
	});
	cm.push({
		dataIndex:'applyUser-id',
		hidden : true,
		header:biolims.sample.applyUserId,
		width:20*10,
	});
	cm.push({
		dataIndex:'applyUser-name',
		hidden : false,
		header:biolims.sample.applyUserName,
		width:20*4
	});
	
	cm.push({
		dataIndex:'sampleInItemId',
		header:biolims.sample.warehousingDetail,
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'crmCustomer-id',
		header:biolims.sample.customerId,
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'crmCustomer-name',
		header:biolims.sample.customerName,
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'crmCustomer-street',
		header:biolims.sample.customerStreet,
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'crmCustomer-postcode',
		header:biolims.sample.customerPostcode,
		width:30*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'method',
		header:biolims.common.method,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'type',
		header:biolims.sample.selectType,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'taskId',
		header:biolims.common.orderId,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'supplier-id',
		header:biolims.common.supplierId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'supplier-name',
		header:biolims.common.supplier,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'supplier-linkMan',
		header:biolims.equipment.supplierLinkMan,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'supplier-linkTel',
		header:biolims.equipment.supplierLinkTel,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'infoFrom',
		hidden : true,
		header:biolims.sample.sampleSource,
		width:15*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleOut/showSampleOutTempListJson.action";
	loadParam.limit=999;
	var opts={};
	opts.title=biolims.sample.toOutboundSample;
	opts.height=document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.addToDetail,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : checked
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	sampleOutTempGrid=gridEditTable("show_sampleOutTemp_div",cols,loadParam,opts);
	$("#show_sampleOutTemp_div").data("sampleOutTempGrid", sampleOutTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			$("#sampleOut_outType").val($("#sampleOutTemp_type").val());
			commonSearchLimit(sampleOutTempGrid,999);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();
		}
	}, true, option);
}

function checked() {
	$("#many_bat_text").val("");
	var options = {};
	options.width = 474;
	options.height = 480;
	loadDialogPage(
			$("#many_bat_div"),
			biolims.common.checkCode,
			null,
			{
				"Confirm" : function() {
					var positions = $("#many_bat_text").val();
					if (!positions) {
						message(biolims.common.fillBarcode);
						return;
					}
					var array = positions.split("\n");
					var record=sampleOutTempGrid.getAllRecord();
					var store = sampleOutTempGrid.store;
					var isOper = true;
					var buf = [];
					sampleOutTempGrid.stopEditing();
					$.each(array,function(i, obj) {
						$.each(record, function(i, obj1) {
							if(obj==obj1.get("code")){
								isOper = true;
								buf.push(store.indexOfId(obj1.get("id")));
							}
//							else{
//								isOper = false; 
//							}
						});
					});
					//判断那些样本没有匹配到
					var nolist = new Array();
					var templist = new Array();
					$.each(record, function(i, obj1) {
						templist.push(obj1.get("code"));
					});
					$.each(array,function(i, obj) {
						if(templist.indexOf(obj) == -1){
							nolist.push(obj);
						}
					});
					if(nolist!="" && nolist.length>0){
						message(biolims.common.noMatchSample+nolist);
					}
					
					
					sampleOutTempGrid.getSelectionModel().selectRows(buf);
					if(isOper=true){
						addItem();
					}
//					else{
//						message("样本号核对不符，请检查！");
//					}
					sampleOutTempGrid.startEditing(0, 0);
					$(this).dialog("close");
				}
			}, true, options);

}
function addItem(){
	var selectRecord=sampleOutTempGrid.getSelectionModel();
	var selRecord=sampleOutItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;					
				}
			}
			if(!isRepeat){
			var ob = sampleOutItemGrid.getStore().recordType;
			sampleOutItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("code",obj.get("code"));
			p.set("location",obj.get("location"));
			p.set("tempId",obj.get("id"));
			p.set("sampleInItemId",obj.get("sampleInItemId"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("barCode",obj.get("barCode"));
			p.set("dataTraffic",obj.get("dataTraffic"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("samplingDate",obj.get("samplingDate"));
			p.set("concentration",obj.get("concentration"));
			p.set("patientName",obj.get("patientName"));
			p.set("volume",obj.get("volume"));
			p.set("sumTotal",obj.get("sumTotal"));
			p.set("applyUser-id",obj.get("applyUser-id"));
			p.set("applyUser-name",obj.get("applyUser-name"));
			p.set("crmCustomer-id",obj.get("crmCustomer-id"));
			p.set("crmCustomer-name",obj.get("crmCustomer-name"));
			p.set("crmCustomer-street",obj.get("crmCustomer-street"));
			p.set("crmCustomer-postcode",obj.get("crmCustomer-postcode"));
			p.set("supplier-id",obj.get("supplier-id"));
			p.set("supplier-name",obj.get("supplier-name"));
			p.set("supplier-linkMan",obj.get("supplier-linkMan"));
			p.set("supplier-linkTel",obj.get("supplier-linkTel"));
			p.set("infoFrom",obj.get("infoFrom"));
			p.set("sampleStyle",obj.get("sampleStyle"));
			p.set("qpcrConcentration",obj.get("qpcrConcentration"));
			p.set("indexa",obj.get("indexa"));
			if(obj.get("infoFrom")=="SampleOutApply"){
				$("#sampleOut_sampleOutApply").val(obj.get("taskId"));
			}
			if(obj.get("infoFrom")=="DnaTaskItem"){//核酸提取
				p.set("nextFlowId","0017");
				p.set("nextFlow","核酸提取");
			}
			if(obj.get("infoFrom")=="PlasmaTaskItem"){//处理
				p.set("nextFlowId","0016");
				p.set("nextFlow","样本处理");
			}
			if(obj.get("infoFrom")=="TechCheckServiceTaskItem"){//纯化质检
				p.set("nextFlowId","0019");
				p.set("nextFlow","纯化质检");		
			}
			if(obj.get("infoFrom")=="WkTaskItem"){//文库构建
				p.set("nextFlowId","0018");
				p.set("nextFlow","文库构建");		
			}
			if(obj.get("infoFrom")=="WkBlendTaskItem"){//文库混合
				p.set("nextFlowId","0032");
				p.set("nextFlow","文库混合");		
			}
//			ajax("post", "/sample/storage/sampleIn/setSampleInItemByid.action", {
//				id : obj.get("sampleInItemId")
//			}, function(data) {
//				if (data.success) {
////					alert(data.data);
//					p.set("patientName",data.data.patientName);
//				} 
//			}, null);
			sampleOutItemGrid.getStore().add(p);
		}
			
		});
		sampleOutItemGrid.startEditing(0, 0);
	}
	
}