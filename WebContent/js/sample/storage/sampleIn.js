var sampleInGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'location',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
			name:'sampleInApply-id',
			type:"string"
	});
//	fields.push({
//			name:'sampleInApply-name',
//			type:"string"
//	});
	fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
			name:'businessType-id',
			type:"string"
	});
	fields.push({
		name:'workOrder-id',
		type:"string"
	});
	fields.push({
		name:'workOrder-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
//	cm.push({
//		dataIndex:'code',
//		header:'样本入库id',
//		width:20*6,
//		hidden:true,
//		sortable:true
//	});
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleInApply-id',
		hidden:true,
		header:biolims.sample.sampleInApplyId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'workOrder-id',
		header:biolims.common.workOrderId,
		hidden:true,
		width:20*10,
		sortable:true
		});
		cm.push({
			dataIndex:'workOrder-name',
			header:biolims.common.workOrderName,
			width:20*10,
			hidden:true,
			sortable:true
			});
		cm.push({
		dataIndex:'location',
		header:biolims.common.location,
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'acceptUser-id',
		header:biolims.wk.confirmUserId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.wk.confirmUserName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.wk.confirmDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		
		sortable:true
	});
//	cm.push({
//		dataIndex:'sampleType-id',
//		header:'样本类型ID',
//		width:20*6,
//		hidden:true,
//		sortable:true
//	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'businessType-id',
		header:biolims.sample.businessTypeId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'businessType-name',
		header:biolims.sample.businessTypeName,
		width:20*6,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleIn/showSampleInListJson.action";
	var opts={};
	opts.title=biolims.sample.sampleStorage;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sampleInGrid=gridTable("show_sampleIn_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/sample/storage/sampleIn/editSampleIn.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/sample/storage/sampleIn/editSampleIn.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/storage/sampleIn/viewSampleIn.action?id=' + id;
}
function exportexcel() {
	sampleInGrid.title = biolims.common.exportList;
	var vExportContent = sampleInGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startacceptDate").val() != undefined) && ($("#startacceptDate").val() != '')) {
					var startacceptDatestr = ">=##@@##" + $("#startacceptDate").val();
					$("#acceptDate1").val(startacceptDatestr);
				}
				if (($("#endacceptDate").val() != undefined) && ($("#endacceptDate").val() != '')) {
					var endacceptDatestr = "<=##@@##" + $("#endacceptDate").val();

					$("#acceptDate2").val(endacceptDatestr);

				}
				
				
				commonSearchAction(sampleInGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
