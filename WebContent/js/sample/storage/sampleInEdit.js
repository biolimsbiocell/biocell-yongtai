﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleIn_state").val()=="3"){
		load("/sample/storage/sampleIn/showSampleInItemTempList.action", { }, "#sampleInItemTempPage");
		$("#markup").css("width","75%");
	}
});	
function add() {
	window.location = window.ctx + "/sample/storage/sampleIn/editSampleIn.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/storage/sampleIn/showSampleInList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleIn", {
					userId : userId,
					userName : userName,
					formId : $("#sampleIn_id").val(),
					title : $("#sampleIn_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleIn_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});


$("#toolbarbutton_print").click(function() {
	var url = '__report=SampleIn.rptdesign&id=' + $("#sampleIn_id").val();
	commonPrint(url);
});




function save() {
	
	var local = $("#sampleIn_location").val();
	if(local==""){
		message(biolims.common.storageNotNull);
		return;
	}
	
	

if(checkSubmit()==true){
		var myMask = new Ext.LoadMask(Ext.getBody(), {
			msg : biolims.common.pleaseWait
		});
		myMask.show();
	    var sampleInItemDivData = $("#sampleInItemdiv").data("sampleInItemGrid");
		document.getElementById('sampleInItemJson').value = commonGetModifyRecords(sampleInItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/storage/sampleIn/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/storage/sampleIn/copySampleIn.action?id=' + $("#sampleIn_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#sampleIn_id").val() + "&tableId=sampleIn");
//}
$("#toolbarbutton_status").click(function(){
//	var selRecord = sampleInItemGrid.store;
//	for(var j=0;j<selRecord.getCount();j++){
//		var oldv = selRecord.getAt(j).get("checked");
//		if(!oldv){
//			message("核对结果不能为空！");
//			return;
//		}
//	}
	if ($("#sampleIn_id").val()){
		commonChangeState("formId=" + $("#sampleIn_id").val() + "&tableId=SampleIn");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleIn_id").val());
//	fs.push($("#sampleIn_location").val());
	nsc.push(biolims.sample.sampleStorageCodeIsEmpty);
//	nsc.push(biolims.sample.locationIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.sampleStorage,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/sample/storage/sampleIn/showSampleInItemList.action", {
				id : $("#sampleIn_id").val()
			}, "#sampleInItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	function sampleInKind() {
		var win = Ext.getCmp('sampleInKind');
		if (win) {
			win.close();
		}
		var sampleInKind = new Ext.Window(
				{
					id : 'sampleInKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 800,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleInKind.close();
						}
					} ]
				});
		sampleInKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleIn_sampleType_id").value = id;
		 document.getElementById("sampleIn_sampleType_name").value = name;

		var win = Ext.getCmp('sampleInKind')
		if(win){win.close();}
	}
	//选择检测方法
	 function WorkTypeFun(){
		 var win = Ext.getCmp('WorkTypeFun');
		 if (win) {win.close();}
		 var WorkTypeFun= new Ext.Window({
		 id:'WorkTypeFun',modal:true,title:biolims.sample.selectSequenceMethod,layout:'fit',width:document.body.clientWidth/1.5,height:500,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		 html:"<iframe scrolling='no' name='maincontentframe' src='/com/biolims/system/work/workType/workTypeSelect.action?flag=WorkTypeFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: biolims.common.close,
		  handler: function(){
			  WorkTypeFun.close(); }  }]  });     WorkTypeFun.show(); }
		  function setWorkTypeFun(rec){
		 document.getElementById('sampleIn_businessType').value=rec.get('id');
		 document.getElementById('sampleIn_businessType_name').value=rec.get('name');
		 var win = Ext.getCmp('WorkTypeFun')
		 if(win){win.close();}
		 }
