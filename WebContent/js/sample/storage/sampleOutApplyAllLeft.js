//样本出库申请左侧待出库样本
var sampleOutApplyAllTab;
hideLeftDiv();
var sampleOutApply_id = $("#sampleOutApply_id").text();
$("#save").hide();
$("#tjsp").hide();
$("#sp").hide();
$("#changeState").hide();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	});
	/*colOpts.push({
		"data" : "barCode",
		"title" : biolims.common.barCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "barCode");
		}
	});*/
	colOpts.push({
		"data" : "productId",
		"title" : biolims.common.productId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "productId");
		}
	});
	colOpts.push({
		"data" : "productName",
		"title" : biolims.common.testProject,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "productName");
		},
	});

	colOpts.push({
		"data" : "samplingDate",
		"title" : biolims.sample.samplingDate,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "samplingDate");
		},
	});
	colOpts.push({
		"data" : "sampleInfo-idCard",
		"title" : biolims.common.outCode,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleInfo-idCard");
		},
	});
	colOpts.push({
		"data" : "sampleType",
		"title" : biolims.common.sampleType,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleType");
		},
	});
	colOpts.push({
		"data" : "location",
		"title" : biolims.common.location,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "location");
		},
	});

	/*colOpts.push({
		"data" : "qpcrConcentration",
		"title" : biolims.QPCR.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "qpcrConcentration");
		},
	});
	colOpts.push({
		"data" : "indexa",
		"title" : 'index',
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "indexa");
		},
	});
	colOpts.push({
		"data" : "concentration",
		"title" : biolims.common.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "concentration");
		},
	});
	colOpts.push({
		"data" : "volume",
		"title" : biolims.common.volume,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "volume");
		},
	});
	colOpts.push({
		"data" : "sumTotal",
		"title" : biolims.common.sumNum,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sumTotal");
		},
	});*/

	var tbarOpts = [];
	var state = $("#sampleOutApply_state").text();
	if(state!="Complete"){
		tbarOpts
		.push({
			text : biolims.common.addToDetail,
			action : function() {
				var sampleId = [];
				$("#sampleOutApplyAlldiv .selected").each(
						function(i, val) {
							sampleId.push($(val).children("td").eq(0).find(
									"input").val());
						});

				// 先保存主表信息
				var note = $("#sampleOutApply_name").val();
				var createUser = $("#sampleOutApply_createUser").attr(
						"userId");
				var createDate = $("#sampleOutApply_createDate").text();
				var type = $("#sampleOutApply_experimentType_id").val();
				var id = $("#sampleOutApply_id").text();
				$
						.ajax({
							type : 'post',
							url : '/sample/storage/sampleOutApply/addSampleOutApplyItem.action',
							data : {
								ids : sampleId,
								note : note,
								id : id,
								createDate : createDate,
								createUser : createUser,
								type : type
							},
							success : function(data) {
								var data = JSON.parse(data)
								if (data.success) {
									$("#sampleOutApply_id").text(data.data);
									var param = {
										id : data.data
									};
									var sampleOutApplyItemTabs = $(
											"#sampleOutApplyItemdiv")
											.DataTable();
									sampleOutApplyItemTabs.settings()[0].ajax.data = param;
									sampleOutApplyItemTabs.ajax.reload();
									var sampleOutApplyAllTabs = $(
											"#sampleOutApplyAlldiv")
											.DataTable();
									sampleOutApplyAllTabs.ajax.reload();
								} else {
									top.layer.msg('添加失败');
								}
								;
							}
						});
			}
		});
		
		$("#save").show();
		$("#tjsp").show();
		$("#sp").show();
		$("#changeState").show();
	}
	


	tbarOpts.push({
		text : '<i class="fa fa-ioxhost"></i>' + biolims.common.search,
		action : function() {
			search()
		}
	});

	var sampleOutApplyAllOps = table(true, sampleOutApply_id,
			"/sample/storage/sampleOutApply/showSampleInfoInTableJson.action",
			colOpts, tbarOpts);
	sampleOutApplyAllTab = renderData($("#sampleOutApplyAlldiv"),
			sampleOutApplyAllOps);

	// 选择数据并提示
	sampleOutApplyAllTab.on('draw', function() {
		var index = 0;
		$("#sampleOutApplyAlldiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
	
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });
});

// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	},  {
		"txt" : biolims.common.productId,
		"type" : "input",
		"searchName" : "productId",
	}, {
		"txt" :biolims.common.testProject,
		"type" : "input",
		"searchName" : "productName",
	}/*, {
		"txt" :"库存类型",
		"type" : "select",
		"options": biolims.common.pleaseChoose+"|"+"自体细胞库"+"|"+"公共细胞库"+"|"+"样本库",
		"changeOpt": "''|0|1|2",
		"searchName" : "stockType",
	}*/,  {
		"txt" : biolims.common.sampleType,
		"type" : "input",
		"searchName" : "sampleType",
	}, {
		"txt" : biolims.common.location,
		"type" : "input",
		"searchName" : "location",
	}, {
		"type" : "table",
		"table" : sampleOutApplyAllTab
	} ];
}

function list(){
	window.location= ctx+"/sample/storage/sampleOutApply/showSampleOutApplyTable.action";
}
// 大保存
function save() {
	
	var changeLog = "出库申请:";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	var jsonStr = JSON.stringify($("#form1").serializeObject());

	var dataItemJson = saveItemjson($("#sampleOutApplyItemdiv"));
	var ele = $("#sampleOutApplyItemdiv");
	var changeLogItems = "出库申请明细：";
	var changeLogItem = "";
	if(changeLogItems != "出库申请明细："){
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItems);
	}
	var id = $("#sampleOutApply_id").text();
	var type = $("#sampleOutApply_experimentType_id").val();
	var note = $("#sampleOutApply_name").val();
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		url : ctx + '/sample/storage/sampleOutApply/saveSampleOutApplyAndItem.action',
		dataType : 'json',
		type : 'post',
		data : {
			dataValue : jsonStr,
			changeLog : changeLog,
			ImteJson : dataItemJson,
			bpmTaskId : $("#bpmTaskId").val(),
			changeLogItem : changeLogItem,
			id : id,
			type : type,
			note : note
		},
		success : function(data) {
			if (data.success) {
				var url = "/sample/storage/sampleOutApply/toEditSampleOutApply.action?id=" + data.id;

				url = url + "&bpmTaskId=" + $("#bpmTaskId").val();

				window.location.href = url;
				top.layer.closeAll();
			} else {
				top.layer.msg(data.msg);
				top.layer.closeAll();
			}
		}

	});

}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
function tjsp() {
	var a = $("#sampleOutApply_id").text();
	if(a=="NEW"){
		  top.layer.msg("请保存后提交!");
		  return false;
	}
	if($("#form1").data("changed")){ 
	    top.layer.msg("请保存后提交!");
	    return false;
	   }

	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=SampleOutApply",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : sampleOutApply_id,
											title : $("#sampleOutApply_name")
													.val(),
											formName : "SampleOutApply"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = sampleOutApply_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}



//改变状态
function changeState() {
  var	id=$("#sampleOutApply_id").text()
	var paraStr = "formId=" + id +
		"&tableId=SampleOutApply";
	console.log(paraStr)
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			var flag=1;
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				if(flag==1){
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				flag++;
				top.layer.closeAll();
				}
				
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});

	
}

function showType() {
	$("#form1").data("changed",true);  
	top.layer.open({
		title : biolims.user.selectedApplicationType,
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [
				window.ctx
						+ "/dic/type/dicTypeSelectTable.action?flag=outApply",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#sampleOutApply_experimentType_id").val(id);
			$("#sampleOutApply_experimentType_name").val(name);
		},
	})

}
