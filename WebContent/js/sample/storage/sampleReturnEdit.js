﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/sample/storage/sampleReturn/editSampleReturn.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/storage/sampleReturn/showSampleReturnList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleReturn", {
					userId : userId,
					userName : userName,
					formId : $("#sampleReturn_id").val(),
					title : $("#sampleReturn_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleReturn_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var sampleReturnItemDivData = $("#sampleReturnItemdiv").data("sampleReturnItemGrid");
		document.getElementById('sampleReturnItemJson').value = commonGetModifyRecords(sampleReturnItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/storage/sampleReturn/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/storage/sampleReturn/copySampleReturn.action?id=' + $("#sampleReturn_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleReturn_id").val()){
		commonChangeState("formId=" + $("#sampleReturn_id").val() + "&tableId=SampleReturn");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleReturn_id").val());
	nsc.push("样本返库编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'样本返库',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/sample/storage/sampleReturn/showSampleReturnItemList.action", {
				id : $("#sampleReturn_id").val()
			}, "#sampleReturnItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);