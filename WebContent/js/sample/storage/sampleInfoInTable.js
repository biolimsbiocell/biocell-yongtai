var sampleInfoInTab;
$(function() {
	var tbarOpts = [];
	tbarOpts.push({
	text : '<i class="fa fa-ioxhost"></i>' + biolims.common.search,
	action : function() {
		search();
	}
});
	var options = table(true, "",
			"/sample/storage/sampleInInfo/showSampleInInfoTableListJson.action", [ 
			{
				"data" : "code",
				"title" : biolims.common.code,
			}, {
				"data" : "sampleCode",
				"title" : biolims.common.sampleCode,
			}, {
				"data" : "sampleType",
				"title" :biolims.common.originalSampleType,
			}
			, {
				"data" : "dicSampleType-id",
				"title" :biolims.common.sampleTypeId,
			}
			, {
				"data" : "dicSampleType-name",
				"title" :biolims.common.sampleTypeName,
			},  {
				/*"data" : "qpcrConcentration",
				"title" : biolims.QPCR.concentration,
			}, {
				"data" : "indexa",
				"title" :'index',
			}, {
				"data" : "concentration",
				"title" : biolims.common.concentration,
			}, {
				"data" : "volume",
				"title" : biolims.common.volume,
			}, {*/
				"data" : "location",
				"title" : biolims.common.location,
			}, {
				/*"data" : "sumTotal",
				"title" : biolims.common.sumNum,
			}, {*/
				"data" : "customer-id",
				"title" : biolims.sample.customerId,
			}, {
				"data" : "customer-name",
				"title" : biolims.sample.customerName,
			}, {
				"data" : "customer-street",
				"title" :biolims.sample.customerStreet,
			}, {
				"data" : "sampleInfo-idCard",
				"title" :biolims.common.outCode,
			}, {
				"data" : "sampleInfo-cardNumber",
				"title" : biolims.common.actualExternalNumber,
			}, {
				"data" : "note",
				"title" : biolims.common.note,
			}, {
				"data" : "techJkServiceTask-id",
				"title" :biolims.common.scienceServiceId,
			}, {
				"data" : "tjItem-id",
				"title" :biolims.wk.scienceServiceDetailId,
			}], tbarOpts);
			
	sampleInfoInTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleInfoInTab);
	})
	
	
	
});



// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	},{
		"txt" :biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.originalSampleType,
		"type" : "input",
		"searchName" : "sampleType",
	},{
		"txt" : biolims.common.sampleTypeId,
		"type" : "input",
		"searchName" : "dicSampleType-id",
	},{
		"txt" :biolims.common.sampleType,
		"type" : "input",
		"searchName" : "dicSampleType-name",
	},{
		"txt" : biolims.common.location,
		"type" : "input",
		"searchName" : "location",
	},/*{
		"txt" : biolims.QPCR.concentration,
		"type" : "input",
		"searchName" : "qpcrConcentration",
	},{
		"txt" :'index',
		"type" : "input",
		"searchName" : "indexa",
	},{
		"txt" :biolims.common.concentration,
		"type" : "input",
		"searchName" : "concentration",
	},{
		"txt" : biolims.common.volume,
		"type" : "input",
		"searchName" : "volume",
	},{
		"txt" : biolims.common.sumNum,
		"type" : "input",
		"searchName" : "sumTotal",
	},*/{
		"txt" :biolims.sample.customerId,
		"type" : "input",
		"searchName" : "customer-id",
	},{
		"txt" : biolims.sample.customerName,
		"type" : "input",
		"searchName" : "customer-name",
	},{
		"txt" : biolims.sample.customerStreet,
		"type" : "input",
		"searchName" : "customer-street",
	},{
		"txt" :biolims.common.outCode,
		"type" : "input",
		"searchName" : "sampleInfo-idCard",
	},{
		"txt" : biolims.common.actualExternalNumber,
		"type" : "input",
		"searchName" : "sampleInfo-cardNumber",
	}/*,{
		"txt" : biolims.common.sampleInventoryState,
		"type" : "input",
		"searchName" : "outState",
	}*/,{
		"txt" : biolims.common.note,
		"type" : "input",
		"searchName" : "note",
	},{
		"type" : "table",
		"table" : sampleInfoInTab
	} ];
}
