var sampleOutTab;
$(function() {
	var options = table(true, "",
			"/sample/storage/sampleOut/showSampleOutTableJson.action", [ {
				"data" : "id",
				"title" : biolims.sampleOut.id,
			}, {
				"data" : "createUser-id",
				"title" : biolims.sampleOut.createUserId,
			}, {
				"data" : "createUser-name",
				"title" : biolims.sampleOut.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sampleOut.createDate,
			}, {
				"data" : "name",
				"title" : biolims.sampleOut.note,
			}, {
				"data" : "outTypes-id",
				"title" : biolims.sampleOut.typeId,
			}, {
				"data" : "outTypes-name",
				"title" : biolims.sampleOut.type,
			}, {
				"data" : "fyDate",
				"title" : biolims.sampleOut.fyDate,
			}, {
				"data" : "applyUser-id",
				"title" :biolims.sampleOut.applyUserId,
			}, {
				"data" : "applyUser-name",
				"title" :biolims.sampleOut.applyUserName,
			}, {
				"data" : "acceptUser-id",
				"title" : biolims.sampleOut.acceptUserId
			}, {
				"data" : "acceptUser-name",
				"title" : biolims.sampleOut.acceptUserName
			},  {
				"data" : "stateName",
				"title" : biolims.sampleOutApply.stateName
			}], null)
	sampleOutTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleOutTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/sample/storage/sampleOut/toEditSampleOut.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/sample/storage/sampleOut/toEditSampleOut.action?id=' + id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/sample/storage/sampleOut/toEditSampleOut.action?id=' + id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.sampleOut.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.sampleOut.createUserId,
		"type" : "input",
		"searchName" : "createUser-id",
	}, {
		"txt" : biolims.sampleOut.createUserName,
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"txt" : biolims.sampleOut.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sampleOut.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, , {
		"txt" : biolims.sampleOut.fyDateStart,
		"type" : "dataTime",
		"searchName" : "fyDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sampleOut.fyDateEnd,
		"type" : "dataTime",
		"searchName" : "fyDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : biolims.sampleOut.applyUserId,
		"type" : "input",
		"searchName" : "applyUser-id",
	}, {
		"txt" : biolims.sampleOut.applyUserName,
		"type" : "input",
		"searchName" : "applyUser-name",
	},, {
		"txt" : biolims.sampleOut.acceptUserId,
		"type" : "input",
		"searchName" : "acceptUser-id",
	}, {
		"txt" : biolims.sampleOut.acceptUserName,
		"type" : "input",
		"searchName" : "acceptUser-name",
	},{
		"type" : "table",
		"table" : sampleOutTab
	} ];
}
