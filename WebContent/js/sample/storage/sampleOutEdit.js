﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleOut_state").val()=="3"){
		load("/sample/storage/sampleOut/showSampleOutTempList.action", null, "#sampleOutTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#sampleOutTemppage").remove();
	}
});
function add() {
	window.location = window.ctx + "/sample/storage/sampleOut/editSampleOut.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/storage/sampleOut/showSampleOutList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
//	var selRecord = sampleOutItemGrid.store;
//	for(var j=0;j<selRecord.getCount();j++){
//		var oldv = selRecord.getAt(j).get("checked");
//		if(!oldv){
//			message("核对结果不能为空！");
//			return;
//		}
//	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
	if($("#sampleOut_acceptUser_name").val()==""){
		message(biolims.common.confirmUserEmpty);
		return;
	}
				submitWorkflow("SampleOut", {
					userId : userId,
					userName : userName,
					formId : $("#sampleOut_id").val(),
					title : $("#sampleOut_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var selRecord = sampleOutItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var nextFlowId = selRecord.getAt(j).get("nextFlowId");
		if(nextFlowId==""||nextFlowId==null){ 
			message(biolims.common.nextStepNotEmpty);
			return;
		}
	}
		completeTask($("#sampleOut_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
	
	var selRecord = sampleOutItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var type = selRecord.getAt(j).get("sampleType");
		var productId = selRecord.getAt(j).get("productName");
		if(type==""||type==null){ 
			message(biolims.sample.sampleTypeIsEmpty);
			return;
		}
//		if(productId==""||productId==null){
//			message("检测项目不能为空!");
//			return;
//		}
	}
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    //var sampleOutItemDivData = sampleOutItemGrid;
	    var sampleOutItemDivData = $("#sampleOutItemdiv").data("sampleOutItemGrid");;
		document.getElementById('sampleOutItemJson').value = commonGetModifyRecords(sampleOutItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/storage/sampleOut/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/storage/sampleOut/copySampleOut.action?id=' + $("#sampleOut_id").val();
}
$("#toolbarbutton_status").click(function(){
	var selRecord = sampleOutItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var nextFlowId = selRecord.getAt(j).get("nextFlowId");
		if(nextFlowId==""||nextFlowId==null){ 
			message(biolims.common.nextStepNotEmpty);
			return;
		}
	}
	if ($("#sampleOut_id").val()){
		commonChangeState("formId=" + $("#sampleOut_id").val() + "&tableId=SampleOut");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleOut_id").val());
	nsc.push(biolims.sample.sampleOutStorageCodeIsEmpty);
	fs.push($("#sampleOut_outType").val());
	nsc.push(biolims.common.outStorageNotEmpty);
	if($("#sampleOut_outType").val()=="0"){
		fs.push($("#sampleOut_applyUser_name").val());
		nsc.push(biolims.common.samplerNotEmpty);
	}
	
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.sampleOutStorage,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/sample/storage/sampleOut/showSampleOutItemList.action", {
				id : $("#sampleOut_id").val()
			}, "#sampleOutItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: biolims.common.copy
//						});
//	item.on('click', editCopy);
	
	
	//选择出库申请单
	function SampleOutApplyFun(){
		var outType=$("#sampleOut_outType").val();
		var win = Ext.getCmp('SampleOutApplyFun');
		if (win) {win.close();}
		var SampleOutApplyFun= new Ext.Window({
		id:'SampleOutApplyFun',modal:true,title:biolims.sample.selectOutApply,layout:'fit',width:800,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/storage/sampleOutApply/sampleOutApplySelect.action?flag=SampleOutApplyFun&outType="+outType+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 SampleOutApplyFun.close(); }  }]  });     SampleOutApplyFun.show(); }
		 
	function setSampleOutApplyFun(rec){
		 $("#sampleOutTemp_taskId").val(rec.get('id'));
		 commonSearchLimit(sampleOutTempGrid,999);
		var code=$("#sampleOut_sampleOutApply").val();
		if(code=="" ){
			document.getElementById('sampleOut_sampleOutApply').value=rec.get('id');
			document.getElementById('sampleOut_sampleOutApply_name').value=rec.get('name');
//			document.getElementById('sampleOut_applyUser').value=rec.get('createUser-id');
//			document.getElementById('sampleOut_applyUser_name').value=rec.get('createUser-name');
			var win = Ext.getCmp('SampleOutApplyFun');
			if(win){win.close();}
			id = rec.get('id');
			
			ajax("post", "/sample/storage/sampleOutApply/showSampleOutItemList.action", {
				code : id,
			}, function(data) {
				if (data.success) {
//					var ob = sampleOutItemGrid.getStore().recordType;
//					sampleOutItemGrid.stopEditing();
					var record=sampleOutTempGrid.getAllRecord();
					var store = sampleOutTempGrid.store;
					var buf = [];
					$.each(data.data, function(i, obj) {
						$.each(record, function(a, obj1) {
							if(obj1.get("code")==obj.code){
								buf.push(store.indexOfId(obj1.get("id")));
							}
						});
//						var p = new ob({});
//						p.isNew = true;
//						p.set("code", obj.code);
//						p.set("sampleCode", obj.sampleCode);
//						p.set("sampleNum", obj.sampleNum);
//						p.set("sampleType", obj.sampleType);
//						p.set("location",obj.location);
//						p.set("sampleInItemId",obj.id);
//						
//						sampleOutItemGrid.getStore().add(p);							
					});
					sampleOutTempGrid.getSelectionModel().selectRows(buf);
					addItem();
//					sampleOutItemGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('SampleOutApplyFun');
 				if(win){win.close();}
 			 }else{
 				sampleOutItemGrid.store.removeAll();
 				document.getElementById('sampleOut_sampleOutApply').value=rec.get('id');
 				document.getElementById('sampleOut_sampleOutApply_name').value=rec.get('name');
// 				document.getElementById('sampleOut_applyUser').value=rec.get('createUser-id');
// 				document.getElementById('sampleOut_applyUser_name').value=rec.get('createUser-name');
 				var win = Ext.getCmp('SampleOutApplyFun');
			 if(win){win.close();}
				id = rec.get('id');
				name= rec.get('name');
				ajax("post", "/sample/storage/sampleOutApply/showSampleOutItemList.action", {
					code : id,
					}, function(data) {
						if (data.success) {	
							var record=sampleOutTempGrid.getAllRecord();
							var store = sampleOutTempGrid.store;
							var buf = [];
							$.each(data.data, function(i, obj) {
								$.each(record, function(a, obj1) {
									if(obj1.get("code")==obj.code){
										buf.push(store.indexOfId(obj1.get("id")));
									}
								});
							});
							sampleOutTempGrid.getSelectionModel().selectRows(buf);
							addItem();
							
//							var ob = sampleOutItemGrid.getStore().recordType;
//							sampleOutItemGrid.stopEditing();
//							
//							$.each(data.data, function(i, obj) {
//								var p = new ob({});
//								p.isNew = true;
//								p.set("code", obj.code);
//								p.set("sampleType", obj.sampleType);
//								p.set("location",obj.location);
//								sampleOutItemGrid.getStore().add(p);							
//							});
							
							sampleOutItemGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
			 }
		}
	}
	
	
//科技服务
var loadTechService;
function selectTechJkServiceTask(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
	 	loadTechService = loadDialogPage(null, biolims.common.selectExtract, "/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogList.action?out=out", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$("#sampleOut_techJkServiceTask_name").val(selectRecord[0].get("name"));
					$("#sampleOut_techJkServiceTask").val(selectRecord[0].get("id"));
					ajax("post", "/technology/wk/techJkServiceTask/selTechJkserverTaskItem.action", {
						id : selectRecord[0].get("id"),
						}, function(data) {
							if (data.success) {	
								var ob = sampleOutItemGrid.getStore().recordType;
								sampleOutItemGrid.stopEditing();
								$.each(data.data, function(i, obj) {
									var grid=sampleOutItemGrid.store;
									for(var a=0;a<grid.getCount();a++){
										if(obj.code==grid.getAt(a).get("code")){
											message(biolims.common.dataRepeat);
											return;
										}
									}
									var p = new ob({});
									p.isNew = true;
									p.set("code", obj.code);
									p.set("sampleCode", obj.sampleCode);
									p.set("patientName", obj.name);
									p.set("productId", obj.productId);
									p.set("productName", obj.productName);
									p.set("sampleInItemId", obj.sampleInfoIn.id);
									p.set("sampleType", obj.sampleType);
									p.set("location", obj.location);
									p.set("concentration", obj.concentration);
									p.set("volume", obj.volume);
									p.set("sumTotal", obj.sampleNum);
									p.set("applyUser-id", obj.techJkServiceTask.createUser.id);
									p.set("applyUser-name", obj.techJkServiceTask.createUser.name);
									p.set("nextFlowId", obj.nextFlowId);
									p.set("nextFlow", obj.nextFlow);
									p.set("isDepot", "2");
									sampleOutItemGrid.getStore().add(p);							
								});
								
								sampleOutItemGrid.startEditing(0, 0);		
							} 
						}, null); 
					
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}


//选择科研任务单
function setTechService(){
	var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
		$("#sampleOut_techJkServiceTask_name").val(selectRecord[0].get("name"));
		$("#sampleOut_techJkServiceTask").val(selectRecord[0].get("id"));
		ajax("post", "/technology/wk/techJkServiceTask/selTechJkserverTaskItem.action", {
			id : selectRecord[0].get("id"),
			}, function(data) {
				if (data.success) {	
					var ob = sampleOutItemGrid.getStore().recordType;
					sampleOutItemGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var grid=sampleOutItemGrid.store;
						for(var a=0;a<grid.getCount();a++){
							if(obj.code==grid.getAt(a).get("code")){
								message(biolims.common.dataRepeat);
								return;
							}
						}
						var p = new ob({});
						p.isNew = true;
						p.set("code", obj.code);
						p.set("sampleCode", obj.sampleCode);
						p.set("patientName", obj.name);
						p.set("productId", obj.productId);
						p.set("productName", obj.productName);
						p.set("sampleInItemId", obj.sampleInfoIn.id);
						p.set("sampleType", obj.sampleType);
						p.set("location", obj.location);
						p.set("concentration", obj.concentration);
						p.set("volume", obj.volume);
						p.set("sumTotal", obj.sampleNum);
						p.set("applyUser-id", obj.techJkServiceTask.createUser.id);
						p.set("applyUser-name", obj.techJkServiceTask.createUser.name);
						p.set("nextFlowId", obj.nextFlowId);
						p.set("nextFlow", obj.nextFlow);
						p.set("isDepot", "2");
						sampleOutItemGrid.getStore().add(p);							
					});
					
					sampleOutItemGrid.startEditing(0, 0);	
				} 
			}, null); 
		
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadTechService.dialog("close");

}

//选择审核人（样本组的人员）
function confirmUser(){
	var options = {};
	options.width = 500;
	options.height = 500;
	loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid=SA", {
		"Confirm" : function() {
			var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$("#sampleOut_acceptUser").val(selectRecord[0].get("user-id"));
				$("#sampleOut_acceptUser_name").val(selectRecord[0].get("user-name"));
			}else{
				message(biolims.common.selectYouWant);
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
	
}

function selectoutType(){
	var type=$("#sampleOut_outType").val();
	$("#sampleOutTemp_type").val(type);
	 commonSearchLimit(sampleOutTempGrid,999);
}

//回车搜索实验任务单号
function disableEnter(event){
	var e = event || window.event || arguments.callee.caller.arguments[0];
    if(e && e.keyCode==13){ // enter 键
    	$("#sampleOutTemp_taskId").val($("#sampleOut_taskId").val());//把主表的实验任务单号给左侧表搜索框
    	commonSearchLimit(sampleOutTempGrid,999);
//    	ajax("post", "/sample/storage/sampleOutApply/showSampleOutItemList.action", {
//			code : $("#sampleOut_taskId").val()
//		}, function(data) {
//			if (data.success) {
//				var record= sampleOutTempGrid.getAllRecord();
//				var store = sampleOutTempGrid.store;
//				var buf = [];
//				$.each(data.data, function(i, obj) {
//					$.each(record, function(a, obj1) {
//						if(obj1.get("code")==obj.code){
//							buf.push(store.indexOfId(obj1.get("id")));
//						}
//					});
//				});
//				sampleOutTempGrid.getSelectionModel().selectRows(buf);
//				addItem();
//			} else {
//				message(biolims.common.anErrorOccurred);
//			}
//		}, null);
    	
	}
}
//选择任务单号
function SampleOutTaskIdFun(){
	var outType=$("#sampleOut_outType").val();
	var win = Ext.getCmp('SampleOutTaskIdFun');
	if (win) {win.close();}
	var SampleOutTaskIdFun= new Ext.Window({
	id:'SampleOutTaskIdFun',modal:true,title:"Select task number",layout:'fit',width:800,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/storage/sampleOut/sampleOutTaskIdDialog.action?flag=SampleOutTaskIdFun&type="+outType+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 SampleOutTaskIdFun.close(); }  }]  });     SampleOutTaskIdFun.show(); 
}
function setSampleOutTaskIdFun(rec){
	$("#sampleOut_taskId").val(rec.get('taskId'));
	$("#sampleOutTemp_taskId").val(rec.get('taskId'));
	commonSearchLimit(sampleOutTempGrid,999);
//	ajax("post", "/sample/storage/sampleOutApply/showSampleOutItemList.action", {
//		code : rec.get('taskId')
//	}, function(data) {
//		if (data.success) {
//			var record=sampleOutTempGrid.getAllRecord();
//			var store = sampleOutTempGrid.store;
//			var buf = [];
//			$.each(data.data, function(i, obj) {
//				$.each(record, function(a, obj1) {
//					alert(obj.code);
//					if(obj1.get("code")==obj.code){
//						alert();
//						buf.push(store.indexOfId(obj1.get("id")));
//					}
//				});
//			});
//			sampleOutTempGrid.getSelectionModel().selectRows(buf);
//			addItem();
//		} else {
//			message(biolims.common.anErrorOccurred);
//		}
//	}, null);

	var win = Ext.getCmp('SampleOutTaskIdFun');
	if(win){
		win.close();
	}
}