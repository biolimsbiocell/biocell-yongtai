﻿//function buildSelect(x){
//	if($("#sampleOutApply_sampleType_name").val()){
//		document.getElementById('to').style.display = "block";
//		$("#tabs").tabs();
//		load("/sample/storage/sampleOutApply/showSampleOutApplyList.action", null, $("#sample-tabs-0"), null);
//	}else{
//		document.getElementById('to').style.display = "none";
//	}
//}    
//if($("#sampleOutApply_sampleType_name").val()){
//
//}else{
//	document.getElementById('to').style.display = "none";
//}
$(function() {
	changeType();
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});

function changeType(){
	var selected = $('#sampleOutApply_type') .val();
	if(selected=="1"){
		$(".tr1").removeClass("aa");
		$(".tr2").addClass("aa");
	}else if(selected=="0"){
		$(".tr1").addClass("aa");
		$(".tr2").addClass("aa");
	}else if(selected=="2"){
		$(".tr1").addClass("aa");
		$(".tr2").removeClass("aa");
	}else if(selected=="3"){
		$(".tr1").addClass("aa");
		$(".tr2").addClass("aa");
	}
//	getSampleType(selected);
//	changeInfo();
}

function add() {
	window.location = window.ctx + "/sample/storage/sampleOutApply/editSampleOutApply.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/storage/sampleOutApply/showSampleOutApplyList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleOutApply", {
					userId : userId,
					userName : userName,
					formId : $("#sampleOutApply_id").val(),
					title : $("#sampleOutApply_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleOutApply_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var sampleOutApplyItemDivData = sampleOutApplyItemGrid;
		document.getElementById('sampleOutApplyItemJson').value = commonGetModifyRecords(sampleOutApplyItemDivData);
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/storage/sampleOutApply/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/storage/sampleOutApply/copySampleOutApply.action?id=' + $("#sampleOutApply_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleOutApply_id").val()){
		commonChangeState("formId=" + $("#sampleOutApply_id").val() + "&tableId=SampleOutApply");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleOutApply_id").val());
	nsc.push(biolims.sample.sampleOutApplyCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.outboundApply,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/sample/storage/sampleOutApply/showSampleOutApplyItemList.action", {
				id : $("#sampleOutApply_id").val()
			}, "#sampleOutApplyItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 800,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleOutApply_sampleType_id").value = id;
		 document.getElementById("sampleOutApply_sampleType_name").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}