﻿var sampleOutItemGrid;

$(function(){
//	selectoutType();

	var cols={};
	var loadParam={};
	var opts={};
	var flg=true;
	var fla=true;
	var flag=true;
	var type=$("#sampleOut_outType").val();
//	$("#sampleOutTemp_type").val(type);
//	if($("#sampleOut_state").val()=="3"){
//		var TempGrid=$("#show_sampleOutTemp_div").data("sampleOutTempGrid");
//		commonSearchAction(TempGrid);
//	}
	
	if(type=="1"){//客户
		flag=false;
		flg=false;
	}else if(type=="2"){//外包
		flag=false;
		fla=false;
	}
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
			name:'code',
			type:"string"
		});
	   fields.push({
			name:'num',
			type:"string"
		});
	   fields.push({
			name:'sampleNum',
			type:"string"
		});
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'tempId',
		type:"string"
	});
	    fields.push({
		name:'sampleOut-id',
		type:"string"
	});
	    fields.push({
		name:'sampleOut-name',
		type:"string"
	});
	    fields.push({
			name:'nextFlow',
			type:"string"
	});
	    fields.push({
			name:'nextFlowId',
			type:"string"
	});
	    
	    fields.push({
			name:'sampleInItemId',
			type:"string"
	});

	    fields.push({
			name:'orderCode',
			type:"string"
	});
	    fields.push({
			name:'productId',
			type:"string"
	});
	    fields.push({
			name:'productName',
			type:"string"
	});
	    fields.push({
			name:'patientName',
			type:"string"
	});
	    fields.push({
			name:'concentration',
			type:"string"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'sumTotal',
			type:"string"
		});
	   fields.push({
			name:'crmCustomer-id',
			type:"string"
		});
	   fields.push({
			name:'crmCustomer-name',
			type:"string"
		});
	   fields.push({
			name:'crmCustomer-street',
			type:"string"
		});
	   fields.push({
			name:'supplier-id',
			type:"string"
		});
	   fields.push({
			name:'supplier-name',
			type:"string"
		});
	   fields.push({
			name:'supplier-linkMan',
			type:"string"
		});
	   fields.push({
			name:'supplier-linkTel',
			type:"string"
		});
	   fields.push({
			name:'crmCustomer-postcode',
			type:"string"
		});
	   fields.push({
			name:'expreceCode',
			type:"string"
		});
	   
	   fields.push({
			name:'applyUser-id',
			type:"string"
		});
		    fields.push({
			name:'applyUser-name',
			type:"string"
		});
	    fields.push({
			name:'isDepot',
			type:"string"
		});
	    fields.push({
			name:'expressCompany-id',
			type:"string"
		});
		fields.push({
			name:'expressCompany-name',
			type:"string"
		});
	    fields.push({
			name:'infoFrom',
			type:"string"
		}); 
	    fields.push({
			name:'barCode',
			type:"string"
		});
	    fields.push({
	    	name:'dataTraffic',
	    	type:"string"
	    });
	    fields.push({
			name:'productId',
			type:"string"
	    });
		   fields.push({
			name:'productName',
			type:"string"
	    });
		   fields.push({
			name:'samplingDate',
			type:"string",
	    });
		   fields.push({
				name:'sampleStyle',
				type:"string",
		    });
		   
		   fields.push({
				name:'qpcrConcentration',
				type:"string",
		   });
		   fields.push({
				name:'indexa',
				type:"string",
		   });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleStyle',
		hidden : true,
		header:biolims.master.productType,
		width:20*6
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.sample.outId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*7
		
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6
		
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		hidden : true,
		width:20*6
		
	});
	cm.push({
		dataIndex:'orderCode',
		header:biolims.common.orderCode,
		width:30*6,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'samplingDate',
		hidden : false,
		header:biolims.sample.samplingDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleNum',
		header:biolims.sample.inventory,
		hidden:true,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleInItemId',
		header:biolims.sample.id,
		hidden:true,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6
	});
	
	cm.push({
		dataIndex:'num',
		header:biolims.sample.num,
		hidden:true,
		width:30*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:biolims.common.location,
		width:30*6
	});
	cm.push({
		dataIndex:'qpcrConcentration',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*5
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*5
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*5
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'applyUser-id',
		hidden : true,
		header:biolims.sample.applyUserId,
		width:20*6,
	});
	cm.push({
		dataIndex:'applyUser-name',
		hidden : false,
		header:biolims.sample.applyUserName,
		width:20*6
	});
	
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.sample.tempId,
		width:30*6
	});
	
	cm.push({
		dataIndex:'sampleOut-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleOut-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'crmCustomer-id',
		hidden : true,
		header:biolims.sample.customerId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmCustomer-name',
		hidden : flg,
		header:biolims.sample.customerName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmCustomer-street',
		hidden : flg,
		header:biolims.sample.customerStreet,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmCustomer-postcode',
		hidden : flg,
		header:biolims.sample.customerPostcode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'supplier-id',
		hidden : true,
		header:biolims.common.supplierId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'supplier-name',
		hidden : fla,
		header:biolims.common.supplier,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'supplier-linkMan',
		hidden : fla,
		header:biolims.common.linkmanName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'supplier-linkTel',
		hidden : fla,
		header:biolims.common.linkMan_telNumber,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'expreceCode',
		hidden : flg,
		header:biolims.sample.expreceCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'expressCompany-id',
		hidden : true,
		header:biolims.sample.companyId,
		width:20*6
	});
	cm.push({
		dataIndex:'expressCompany-name',
		hidden : flag,
		header:biolims.sample.companyName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeisDepotCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '2', biolims.wk.backLib ], [ '0', biolims.wk.notBackLib ] ]
	});
	var isDepotCob = new Ext.form.ComboBox({
		store : storeisDepotCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isDepot',
		hidden : false,
		header:biolims.wk.ifBackLib,
		width:20*6,
		editor : isDepotCob,
		renderer : Ext.util.Format.comboRenderer(isDepotCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'infoFrom',
		hidden : true,
		header:biolims.master.source,
		width:15*6
	});
	cols.cm=cm;
	loadParam.url=ctx+"/sample/storage/sampleOut/showSampleOutItemListJson.action?id="+$("#id_parent_hidden").val();
	opts.title=biolims.sample.outBoundDetail;
	opts.height =  document.body.clientHeight-159;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/sample/storage/sampleOut/delSampleOutItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				sampleOutTempGrid.getStore().commitChanges();
				sampleOutTempGrid.getStore().reload();
				sampleOutItemGrid.getStore().commitChanges();
				sampleOutItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//		text : biolims.common.sampleType,
//		handler : loadTestDicSampleType
//	});
	opts.tbar.push({
		text : biolims.common.testProject,
		handler : ProductFun
	});
	opts.tbar.push({
		text : biolims.wk.ifBackLib,
		handler : function() {
			var records = sampleOutItemGrid.getSelectRecord();
			if (records && records.length > 0) {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_isDepot_div"), biolims.wk.ifBackLib, null, {
					"Confirm" : function() {
						
							var isDepot = $("#isDepot").val();
							sampleOutItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("isDepot", isDepot);
							});
							sampleOutItemGrid.startEditing(0, 0);
						
						$(this).dialog("close");
					}
				}, true, options);
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});
	if(!flag){
	opts.tbar.push({
		text : "快递单号",
		handler : function() {
			var records = sampleOutItemGrid.getSelectRecord();
			if (records && records.length > 0) {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_expreceCode_div"), "快递单号", null, {
					"Confirm" : function() {
							var expreceCode = $("#expreceCode").val();
							sampleOutItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("expreceCode", expreceCode);
							});
							sampleOutItemGrid.startEditing(0, 0);
						
						$(this).dialog("close");
					}
				}, true, options);
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});
	opts.tbar.push({
		text : "快递公司",
		handler : ExpressCompanyFun
	});
	}
	opts.tbar.push({
		text : biolims.common.nextFlow,
		handler : function() {
			var records = sampleOutItemGrid.getSelectRecord();
			if(records.length>0){
				loadTestNextFlowCob();
			}else{
				message(biolims.common.pleaseSelect);
			}
			
		}
	});
	opts.tbar.push({
		text :biolims.common.exportList,
		handler : exportexcel
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
sampleOutItemGrid=gridEditTable("sampleOutItemdiv",cols,loadParam,opts);
$("#sampleOutItemdiv").data("sampleOutItemGrid", sampleOutItemGrid);
$(".x-panel-bbar,.x-toolbar").css("width", "100%");
$(".x-panel-tbar").css("width", "100%");
});
function exportexcel() {
	sampleOutItemGrid.title = "导出列表";
	var vExportContent = sampleOutItemGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

var loadDicSampleType;
//查询样本类型
function loadTestDicSampleType(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDicSampleType=loadDialogPage(null, biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = sampleOutItemGrid.getSelectRecord();
				
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("sampleType", b.get("name"));
							
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setDicSampleType(){

	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = sampleOutItemGrid.getSelectRecord();
	
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("sampleType", b.get("name"));
				
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadDicSampleType.dialog("close");

}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
//	if(sampleOutItemGrid.getModifyRecord().length > 0){
//		message("请先保存记录！");
//		return;
//	}
	var records1 = sampleOutItemGrid.getSelectRecord();
	var sampleType="";
	$.each(records1, function(j, k) {
		sampleType=k.get("sampleType");
	});
	var sampleTypeId="";
	ajax("post", "/sample/dicSampleType/setDicSampleTypeByname.action", {
		sampleType : sampleType
	}, function(data) {
		if (data.success) {
			sampleTypeId=data.data.id;
		}
	}, null);
			 var options = {};
				options.width = 500;
				options.height = 500;
				loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialogBysampleTypeId.action?sampleTypeId="+sampleTypeId, {
					"Confirm" : function() {
						var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
						var selectRecord = operGrid.getSelectionModel().getSelections();
						var records = sampleOutItemGrid.getSelectRecord();
						if (selectRecord.length > 0) {
							$.each(records, function(i, obj) {
								$.each(selectRecord, function(a, b) {
									obj.set("nextFlowId", b.get("id"));
									obj.set("nextFlow", b.get("name"));
								});
							});
						}else{
							message(biolims.common.selectYouWant);
							return;
						}
						$(this).dialog("close");
					}
				}, true, options);
		
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = sampleOutItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}	
function ProductFun() {
	var win = Ext.getCmp('productFun');
	if (win) {
		win.close();
	}
	var productFun = new Ext.Window(
			{
				id : 'productFun',
				modal : true,
				title : biolims.common.selectProduct,
				layout : 'fit',
				width : 600,
				height : 600,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						productFun.close();
					}
				} ]
			});
	productFun.show();
}

function setProductFun(id,name) {
	var productName = "";
	var productId = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
						productId += obj.id + ",";
					});
					//var gridGrid = $("#sampleReceiveItemdiv").data("sampleOutItemGrid");
					var selRecords = sampleOutItemGrid.getSelectionModel().getSelections(); 
					$.each(selRecords, function(i, obj) {
						obj.set('productId',id);
						obj.set('productName',productName);
					});
				}
			}, null);
	var win = Ext.getCmp('productFun');
	if (win) {
		win.close();
	}
	
}

//快递公司
function ExpressCompanyFun(){
	var win = Ext.getCmp('ExpressCompanyFun');
	if (win) {win.close();}
	var ExpressCompanyFun= new Ext.Window({
	id:'ExpressCompanyFun',modal:true,title:biolims.common.chooseTester,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/express/expressCompany/expressCompanySelect.action?flag=ExpressCompanyFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 ExpressCompanyFun.close(); }  }]  });     ExpressCompanyFun.show(); 
}
function setExpressCompanyFun(rec){
	var selRecords = sampleOutItemGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('expressCompany-id',rec.get("id"));
		obj.set('expressCompany-name',rec.get("name"));
	});
	var win = Ext.getCmp('ExpressCompanyFun');
	if(win){
		win.close();
	}
}

