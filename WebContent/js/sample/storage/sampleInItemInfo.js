﻿var sampleInItemInfoGrid;
var ids = "";
$(function() {
	var cols = {};
	cols.sm = false;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'checked',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'num',
		type : "string"
	});
	fields.push({
		name : 'location',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'tempId',
		type : "string"
	});
	fields.push({
		name : 'sampleType',
		type : "string"
	});
	fields.push({
		name : 'dicSampleType-name',
		type : "string"
	});
	fields.push({
		name : 'dicSampleType-id',
		type : "string"
	});
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'concentration',
		type : "string"
	});
	fields.push({
		name : 'volume',
		type : "string"
	});
	fields.push({
		name : 'sumTotal',
		type : "string"
	});
	fields.push({
		name : 'customer-id',
		type : "string"
	});
	fields.push({
		name : 'customer-name',
		type : "string"
	});
	fields.push({
		name : 'customer-street',
		type : "string"
	});
	fields.push({
		name : 'customer-street',
		type : "string"
	});
	fields.push({
		name : 'unitGroup-id',
		type : "string"
	});
	fields.push({
		name : 'unitGroup-name',
		type : "string"
	});
	 fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	   fields.push({
			name:'tjItem-id',
			type:"string"
		});
	   fields.push({
			name:'tjItem-inwardCode',
			type:"string"
		});

	 fields.push({
		 name:'sampleInfo-idCard',
		 type:"string"
	 });
	 fields.push({
		 name:'sampleInfo-cardNumber',
		 type:"string"
	 }); fields.push({
		 name:'outState',
		 type:"string"
	 });
	 fields.push({
		 name:'sampleStyle',
		 type:"string"
	 });
	 fields.push({
		 name:'qpcrConcentration',
		 type:"string"
	 });
	 fields.push({
		 name:'indexa',
		 type:"string"
	 });
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'sampleStyle',
		hidden : true,
		header : biolims.master.productType,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.sample.id,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'code',
		hidden : false,
		header : biolims.common.code,
		width : 20 * 7
	});
	cm.push({
		dataIndex : 'sampleCode',
		hidden : false,
		header : biolims.common.sampleCode,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'checked',
		hidden : true,
		header : biolims.common.checked,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'patientName',
		hidden : true,
		header : biolims.common.patientName,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'name',
		hidden : true,
		header : biolims.common.name,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'num',
		hidden : true,
		header : biolims.sample.num,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleType',
		hidden : false,
		header :biolims.common.originalSampleType,
		width : 20 * 4
	});
	//样本类型
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*4,
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.sampleTypeId,
		width:20*6
	});
	cm.push({
		dataIndex : 'location',
		hidden : false,
		header : biolims.common.location,
		width : 20 * 6
	});
	cm.push({
		dataIndex:'qpcrConcentration',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex : 'concentration',
		hidden : false,
		header : biolims.common.concentration,
		width : 20 * 4
	});
	cm.push({
		dataIndex : 'volume',
		hidden : false,
		header : biolims.common.volume,
		width : 20 * 4
	});
	cm.push({
		dataIndex : 'unitGroup-id',
		hidden : true,
		header : biolims.common.unitGroupId,
		width : 15 * 10,
	// sortable:true
	});
	var testUnitGroup = new Ext.form.TextField({
		allowBlank : false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex : 'unitGroup-name',
		header : biolims.common.unitGroupId,
		hidden : true,
		width : 15 * 10
		// sortable:true,
		//editor : testUnitGroup
	});
	cm.push({
		dataIndex : 'sumTotal',
		hidden : false,
		header : biolims.common.sumNum,
		width : 20 * 6
	});

	cm.push({
		dataIndex : 'customer-id',
		hidden : true,
		header : biolims.sample.customerId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'customer-name',
		hidden : false,
		header : biolims.sample.customerName,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'customer-street',
		hidden : false,
		header : biolims.sample.customerStreet,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : biolims.common.state,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'tempId',
		hidden : true,
		header : biolims.sample.inStorageTempId,
		width : 30 * 6
	});
	 cm.push({
		 dataIndex:'sampleInfo-idCard',
		 hidden : false,
		 header:biolims.common.outCode,
		 width:20*6
	 });
	 cm.push({
		 dataIndex:'sampleInfo-cardNumber',
		 hidden : false,
		 header:biolims.common.actualExternalNumber,
		 width:20*6
	 });
	 var storeoutState = new Ext.data.ArrayStore({
			fields : [ 'id', 'name' ],
			data : [ [ '0', biolims.common.experiment ], [ '1',biolims.common.returnToCustomer ], [ '2',biolims.common.outsourceOutStorage ]
			, [ '3',biolims.common.sampleDestruction], [ '4', biolims.common.inputStorage] ]
	});
	var outStateCob = new Ext.form.ComboBox({
		store : storeoutState,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'outState',
		hidden : false,
		header:biolims.common.sampleInventoryState,
		width:20*6,
//		editor : outStateCob,
		renderer : Ext.util.Format.comboRenderer(outStateCob)
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : biolims.common.note,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		header: biolims.common.scienceServiceId,
		width:15*10,
		hidden : true
	});
	cm.push({
		dataIndex:'tjItem-id',
		header:biolims.wk.scienceServiceDetailId,
		width:15*10,
		hidden : true
	});
	
	cols.cm = cm;
	var loadParam = {};
	if($("#dicSampleType").val()==""){
		loadParam.url = ctx
				+ "/sample/storage/sampleInInfo/showSampleInInfoListJson.action";
	}else{
		loadParam.url = ctx
		+ "/sample/storage/sampleInInfo/showSampleInInfoListJson.action"+"?dicSampleType="+$("#dicSampleType").val();
	}
	var opts = {};
	opts.title = biolims.sample.inventoryDetail;
	opts.height = document.body.clientHeight - 26;
	opts.tbar = [];
//	opts.tbar.push({
//		iconCls : '原始样本',
//		text :biolims.common.originalSample ,
//		handler :  function(){
//			getsampleidByname("DNA","dnaid");
//			getsampleidByname("文库","wkid");
//			window.location.href="/sample/storage/sampleInInfo/showSampleInInfoList.action?dicSampleType="+ids;
//			
//		}
//	});
//	opts.tbar.push({
//		iconCls : 'DNA样本',
//		text : biolims.common.DNAsample,
//		handler : function(){getsampleidByname("DNA","");}
//	});
//	opts.tbar.push({
//		iconCls : '文库样本样本',
//		text : biolims.common.WKsample,
//		handler : function(){getsampleidByname("文库","");}
//	});
	sampleInItemInfoGrid = gridTable("sampleInItemInfodiv", cols, loadParam,
			opts);
	$("#sampleInItemInfodiv")
			.data("sampleInItemInfoGrid", sampleInItemInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function cx() {

	commonSearchAction(sampleInItemInfoGrid);

}

function getsampleidByname(name,type){
	//样本类型
	ajax("post", "/sample/sampleOrder/GetsampleTypeIdByName.action", {
		name : name
	}, function(data) {
		if (data.success) {
			if(type != ""){
				if(ids == ""){
					ids = data.sampleTypeid;
				}else{
					ids = ids+","+ data.sampleTypeid;
				}
			}else{
				window.location.href="/sample/storage/sampleInInfo/showSampleInInfoList.action?dicSampleType="+data.sampleTypeid;
						
			}
		} else {
			message(biolims.common.SampleSelectFail);
			return "";
		}
	}, null);
}
	
//// 查询单位组
//function loadUnitGroup() {
//	var win = Ext.getCmp('loadUnitGroup');
//if (win) {win.close();}
//var loadUnitGroup= new Ext.Window({
//						id : 'loadUnitGroup',
//				modal : true,
//				title : '选择单位组',
//				layout : 'fit',
//				width : 600,
//				height : 600,
//				closeAction : 'close',
//				plain : true,
//				bodyStyle : 'padding:5px;',
//				buttonAlign : 'center',
//				collapsible : true,
//				maximizable : true,
//				items : new Ext.BoxComponent(
//						{
//							id : 'maincontent',
//							region : 'center',
//							html : "<iframe scrolling='no' name='maincontentframe' src='"
//									+ ctx
//									+ "/dic/type/dicTypeSelect.action?flag=unitGroup' frameborder='0' width='100%' height='100%' ></iframe>"
//						}),
//				buttons : [ {
//					text: biolims.common.close,
//					handler : function() {
//						loadUnitGroup.close();
//					}
//				} ]
//			});
//	loadUnitGroup.show();
//}
//
//function setunitGroup(id, name) {
//	var gridGrid = $("#sampleInItemInfodiv").data("sampleInItemInfoGrid");
//	var selRecords = gridGrid.getSelectionModel().getSelections();
//	$.each(selRecords, function(i, obj) {
//		obj.set('unitGroup-id', id);
//		obj.set('unitGroup-name', name);
//	});
//	var win = Ext.getCmp('loadUnitGroup');
//	if (win) {
//		win.close();
//	}
//}
