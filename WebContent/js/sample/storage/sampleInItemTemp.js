﻿
var sampleInItemTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
			name:'unitGroup-id',
			type:"string"
		});
		fields.push({
			name:'unitGroup-name',
			type:"string"
		});

	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	  
   fields.push({
		name:'sampleType',
		type:"string"
	});
   fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	}); 
	   fields.push({
			name:'sampleTypeId',
			type:"string"
		}); 
	   fields.push({
			name:'dicSampleType-id',
			type:"string"
		});
	   fields.push({
			name:'infoFrom',
			type:"string"
		});  
	   fields.push({
			name:'patientName',
			type:"string"
		});
	   fields.push({
			name:'concentration',
			type:"string"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'sumTotal',
			type:"string"
		});
	   fields.push({
			name:'sellPerson-id',
			type:"string"
		});
	   fields.push({
			name:'sellPerson-name',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	   fields.push({
			name:'tjItem-id',
			type:"string"
		});
	   fields.push({
			name:'tjItem-inwardCode',
			type:"string"
		});
	   fields.push({
			name:'groupNo',
			type:"string"
		});
	   fields.push({
			name:'isRp',
			type:"string"
		});
	   fields.push({
			name:'barCode',
			type:"string"
	   }); 
	   fields.push({
		   name:'dataTraffic',
		   type:"string"
	   }); 
	   fields.push({
			name:'sampleState',
			type:"string"
	   });
	   fields.push({
			name:'type',
			type:"string"
	   });
	   fields.push({
			name:'productId',
			type:"string"
	   });
	   fields.push({
			name:'productName',
			type:"string"
	   });
	   fields.push({
			name:'samplingDate',
			type:"string",
	   });
	   fields.push({
			name:'sampleStyle',
			type:"string",
	   });
	   
	   fields.push({
			name:'qpcrConcentration',
			type:"string",
	   });
	   fields.push({
			name:'indexa',
			type:"string",
	   });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleStyle',
		hidden : true,
		header:biolims.master.productType,
		width:20*6
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex:'groupNo',
		hidden : false,
		header:biolims.common.SampleCollectionPacketNumber,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleState',
		hidden : true,
		header:biolims.common.IfClaim,
		width:20*6
	});
	cm.push({
		dataIndex:'type',
		hidden : true,
		header:biolims.sample.sampleSource,
		width:20*6
	});
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:biolims.sample.num,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6
	});
	cm.push({
		dataIndex:'sellPerson-id',
		hidden : true,
		header:biolims.common.salesId,
		width:20*6
	});
	cm.push({
		dataIndex:'sellPerson-name',
		hidden : true,
		header:biolims.common.sales,
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:biolims.common.location,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.originalSampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	
	cm.push({
		dataIndex:'qpcrConcentration',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:20*6
	});
	//原始样本类型
	cm.push({
		dataIndex:'sampleTypeId',
		hidden : true,
		header:biolims.common.originalSampleTypeId,
		width:20*6
	});
	//样本类型
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.sampleTypeId,
		width:20*6
	});
	cm.push({
		dataIndex:'infoFrom',
		hidden : true,
		header:biolims.sample.infoFrom,
		width:20*6
	});
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:biolims.common.unitGroupId,
		width:15*10,
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: biolims.common.unitGroup,
		width:15*10,
		hidden : true
//		sortable:true,
//		editor : testUnitGroup
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		header:biolims.common.scienceServiceId,
		width:15*10,
		hidden : true
	});
	cm.push({
		dataIndex:'tjItem-id',
		header:biolims.wk.scienceServiceDetailId,
		width:15*10,
		hidden : true
	});
	var storeisRpCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes] ]
	});
	var isRpCob = new Ext.form.ComboBox({
		store : storeisRpCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isRp',
		hidden : false,
		header:biolims.common.ifThereStain,
		width:20*5,
		editor : isRpCob,
		renderer : Ext.util.Format.comboRenderer(isRpCob)
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden : true,
//		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.testProject,
		width:20*6,
		hidden : true,
//		sortable:true
	});
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		hidden : true,
		width:30*6,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleIn/showSampleInItemTempListJson.action";
	loadParam.limit=1000;
	var opts={};
	opts.title=biolims.sample.toWarehousingSample;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
    

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.addToDetail,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm": function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = sampleInItemTempGrid.getAllRecord();
							var store = sampleInItemTempGrid.store;

							var isOper = true;
							var buf = [];
							sampleInItemTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										//obj1.set("checked",obj1.get("code"));
										isOper = true;
										//alert(obj1.get("code"));
										//AP12AB00010-2
									}
								});
							});
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
//								alert(biolims.common.noMatchSample+nolist);
							}
							sampleInItemTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message(biolims.common.samplecodeComparison);
							}else{
								addItem();
							}
							sampleInItemTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	sampleInItemTempGrid=gridEditTable("sampleInItemTempdiv",cols,loadParam,opts);
	$("#sampleInItemTempdiv").data("sampleInItemTempGrid", sampleInItemTempGrid);

});

function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(sampleInItemTempGrid);
			$(this).dialog("close");

		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}
function addItem(){
	var selectRecord=sampleInItemTempGrid.getSelectionModel();
	var selRecord=sampleInItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;					
				}
			}
			if(!isRepeat){
			var ob = sampleInItemGrid.getStore().recordType;
			sampleInItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("type",obj.get("type"));
			p.set("sampleState",obj.get("sampleState"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("note",obj.get("note"));
			p.set("code",obj.get("code"));
			p.set("num",obj.get("num"));
			p.set("orderId",obj.get("orderId"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("dicSampleType-name",obj.get("dicSampleType-name"));
			p.set("tempId",obj.get("id"));
			p.set("barCode",obj.get("barCode"));
			p.set("dataTraffic",obj.get("dataTraffic"));
			p.set("infoFrom",obj.get("infoFrom"));
			p.set("sampleTypeId",obj.get("sampleTypeId"));
			p.set("dicSampleType-id",obj.get("dicSampleType-id"));
			p.set("patientName",obj.get("patientName"));
			p.set("state","0");//添加到明细状态为0   完成后状态为1 (库存样本查询的是明细样本状态为1)
			p.set("concentration",obj.get("concentration"));
			p.set("volume",obj.get("volume"));
			p.set("sumTotal",obj.get("sumTotal"));
			p.set("techJkServiceTask-id",obj.get("techJkServiceTask-id"));
			p.set("tjItem-id",obj.get("tjItem-id"));
			p.set("groupNo",obj.get("groupNo"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("samplingDate",obj.get("samplingDate"));
			p.set("sampleStyle",obj.get("sampleStyle"));
			p.set("qpcrConcentration",obj.get("qpcrConcentration"));
			p.set("indexa",obj.get("indexa"));
			
			ajax("post", "/sample/storage/sampleIn/setSampleInItemByCode.action", {
				code : obj.get("code"),
				sampleCode : obj.get("sampleCode")
			}, function(data) {
				if (data.success) {
					if(data.data!=null){
						p.set("location",data.data.location);
					}
//					if(data.map.sellPerson!=null){
//						p.set("sellPerson-id",data.map.sellPerson.id);
//						p.set("sellPerson-name",data.map.sellPerson.name);
//					}
//					if(data.map.customer!=null){
//						p.set("customer-id",data.map.customer.id);
//						p.set("customer-name",data.map.customer.name);
//						p.set("customer-street",data.map.customer.street);
//					}
				} 
			}, null);
			sampleInItemGrid.getStore().add(p);
		}
			
		});
		sampleInItemGrid.startEditing(0, 0);
	}
	
}
