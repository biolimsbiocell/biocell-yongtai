var sampleInApplyDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'taskCode-id',
		type:"string"
	});
	    fields.push({
		name:'taskCode-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
		});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:'ID',
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'taskCode-id',
		header:'所属任务单ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'taskCode-name',
		header:'所属任务单',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		header:'创建人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'acceptUser-id',
		header:'审核人ID',
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:'审核人',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:'审核日期',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/storage/sampleInApply/showSampleInApplyListJson.action";
	var opts={};
	opts.title="入库申请";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleInApplyFun(rec);
	};
	sampleInApplyDialogGrid=gridTable("show_dialog_sampleInApply_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(sampleInApplyDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
