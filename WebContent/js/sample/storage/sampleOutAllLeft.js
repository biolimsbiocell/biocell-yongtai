//样本出库申请左侧待出库样本
var sampleOutAllTab;
hideLeftDiv();
var sampleOut_id = $("#sampleOut_id").text();
var state = $("#sampleOut_state").text();
$("#save").hide();
$("#tjsp").hide();
$("#sp").hide();
$("#changeState").hide();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	});
	colOpts.push({
		"data" : "sampleCode",
		"title" :biolims.common.sampleCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	/*colOpts.push({
		"data" : "dataTraffic",
		"title" :biolims.common.throughput,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dataTraffic");
		}
	});*/
	colOpts.push({
		"data" : "productId",
		"title" : biolims.master.productId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "productId");
		}
	});
	colOpts.push({
		"data" : "productName",
		"title" : biolims.master.product,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "productName");
		},
	});
	colOpts.push({
		"data" : "sampleOrder-id",
		"title" : "关联订单号",
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		},
	});

	colOpts.push({
		"data" : "samplingDate",
		"title" : biolims.sample.samplingDate,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "samplingDate");
		},
	});
	colOpts.push({
		"data" : "location",
		"title" : biolims.common.location,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "location");
		},
	});
	colOpts.push({
		"data" : "sampleType",
		"title" : biolims.common.sampleType,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleType");
		},
	});
	

	/*colOpts.push({
		"data" : "qpcrConcentration",
		"title" : biolims.QPCR.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "qpcrConcentration");
		},
	});
	colOpts.push({
		"data" : "indexa",
		"title" : 'index',
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "indexa");
		},
	});*/
	/*colOpts.push({
		"data" : "concentration",
		"title" : biolims.common.concentration,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "concentration");
		},
	});
	colOpts.push({
		"data" : "volume",
		"title" : biolims.common.volume,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "volume");
		},
	});
	

	colOpts.push({
		"data" : "sumTotal",
		"title" : biolims.common.sumNum,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sumTotal");
		},
	});*/
	colOpts.push({
		"data" : "applyUser-id",
		"title" : biolims.sample.applyUserId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "applyUser-id");
		},
	});
	colOpts.push({
		"data" : "applyUser-name",
		"title" : biolims.sample.applyUserName,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "applyUser-name");
		},
	});
	colOpts.push({
		"data" : "taskId",
		"title" : biolims.common.orderId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "taskId");
		},
	});
	
	
	var tbarOpts = [];

if(state!="Complete"){
	tbarOpts.push({
		text :  biolims.common.addToDetail,
		action : function() {
			//左侧表id
			var sampleId = [];
			$("#sampleOutAlldiv .selected").each(
					function(i, val) {
						sampleId.push($(val).children("td").eq(0).find(
								"input").val());
					});

			// 先保存主表信息
			//描述
			var note = $("#sampleOut_name").val();
			//创建人
			var createUser = $("#sampleOut_createUser").attr("userId");
			//创建日期
			var createDate = $("#sampleOut_createDate").text();
			//出库类型
			var outTypes = $("#sampleOut_outTypes_id").val();
			//出库ID
			var id = $("#sampleOut_id").text();
			//期望反库时间
			var fyDate = $("#sampleOut_fyDate").val();
			//领用人
			var applyUser = $("#sampleOut_applyUser_id").val();
			//审核人
			var acceptUser = $("#sampleOut_acceptUser_id").val();
			$.ajax({
						type : 'post',
						url : '/sample/storage/sampleOut/addSampleOutItem.action',
						data : {
							ids : sampleId,
							note : note,
							createUser : createUser,
							createDate : createDate,
							outTypes:outTypes,
							id : id,
							fyDate:fyDate,
							applyUser:applyUser,
							acceptUser:acceptUser,
						},
						success : function(data) {
							var data = JSON.parse(data)
							if (data.success) {
								$("#sampleOut_id").text(data.data);
								var param = {
									id : data.data
								};
								var sampleOutItemTabs = $(
										"#sampleOutItemdiv")
										.DataTable();
								sampleOutItemTabs.settings()[0].ajax.data = param;
								sampleOutItemTabs.ajax.reload();
								var sampleOutAllTabs = $(
										"#sampleOutAlldiv")
										.DataTable();
								sampleOutAllTabs.ajax.reload();
							} else {
								top.layer.msg(biolims.common.addFailure);
							}
							;
						}
					});
		}
	});
	
	$("#save").show();
	$("#tjsp").show();
	$("#sp").show();
	$("#changeState").show();
	
	
}	
	
	

	tbarOpts.push({
		text : '<i class="fa fa-ioxhost"></i>' + biolims.common.search,
		action : function() {
			search()
		}
	});

	var sampleOutAllOps = table(true, sampleOut_id,
			"/sample/storage/sampleOut/showSampleOutTempTableJson.action",
			colOpts, tbarOpts);
	sampleOutAllTab = renderData($("#sampleOutAlldiv"),
			sampleOutAllOps);

	// 选择数据并提示
	sampleOutAllTab.on('draw', function() {
		var index = 0;
		$("#sampleOutAlldiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });
});

// 弹框模糊查询参数
function searchOptions() {
	return [  {
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" :"sampleCode",
	}, {
		"txt" : biolims.common.productId,
		"type" : "input",
		"searchName" : "productId",
	}, {
		"txt" :biolims.common.testProject,
		"type" : "input",
		"searchName" : "productName",
	}/*, {
		"txt" :"库存类型",
		"type" : "select",
		"options": biolims.common.pleaseChoose+"|"+"自体细胞库"+"|"+"公共细胞库"+"|"+"样本库",
		"changeOpt": "''|0|1|2",
		"searchName" : "stockType",
	}*/, {
		"txt" : biolims.common.location,
		"type" : "input",
		"searchName" : "location",
	}, {
		"txt" : biolims.common.sampleType,
		"type" : "input",
		"searchName" : "sampleType",
	/*}, {
		"txt" :biolims.sample.applyUserId,
		"type" : "input",
		"searchName" : "applyUser.id",
	}, {
		"txt" :biolims.sample.applyUserName,
		"type" : "input",
		"searchName" : "applyUser.name",*/
	}, {
		"txt" :biolims.common.orderId,
		"type" : "input",
		"searchName" : "taskId",
	},{
		"type" : "table",
		"table" : sampleOutAllTab
	} ];
}

function list(){
	window.location= ctx+"/sample/storage/sampleOut/showSampleOutTable.action";
}

// 大保存
function save() {
	var changeLog = "样本出库:";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	console.log($("#form1"));
	var jsonStr = JSON.stringify($("#form1").serializeObject());

	var dataItemJson = saveItemjson($("#sampleOutItemdiv"));
	var ele = $("#sampleOutItemdiv");
	changeLogItem = getChangeLog(dataItemJson, ele, changeLogItems);
	var changeLogItems = "";
	if(changeLogItem != "库存主数据出库:"){
		changeLogItems = changeLogItem;
	}
	//描述
	var note = $("#sampleOut_name").val();
	//出库类型
	var outTypes = $("#sampleOut_outTypes_id").val();
	//出库ID
	var id = $("#sampleOut_id").text();
	//期望反库时间
	var fyDate = $("#sampleOut_fyDate").val();
	//领用人
	var applyUser = $("#sampleOut_applyUser_id").val();
	//审核人
	var acceptUser = $("#sampleOut_acceptUser_id").val();
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		url : ctx + '/sample/storage/sampleOut/saveSampleOutAndItem.action',
		dataType : 'json',
		type : 'post',
		data : {
			dataValue : jsonStr,
			changeLog : changeLog,
			ImteJson : dataItemJson,
			bpmTaskId : $("#bpmTaskId").val(),
			changeLogItem : changeLogItems,
			note : note,
			outTypes : outTypes,
			id : id,
			fyDate:fyDate,
			applyUser:applyUser,
			acceptUser:acceptUser
		},
		success : function(data) {
			if (data.success) {
				var url = "/sample/storage/sampleOut/toEditSampleOut.action?id=" + data.id;
				url = url + "&bpmTaskId=" + $("#bpmTaskId").val();
				window.location.href = url;
				top.layer.closeAll();
			} else {
				top.layer.msg(data.msg);
				top.layer.closeAll();
			}
		}

	});
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
function tjsp() {
	if($("#form1").data("changed")){ 
	    top.layer.msg("请保存后提交!");
	    return false;
	   }
	//必填验证
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=SampleOut",
									yes : function(index, layero) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : sampleOut_id,
											title : $("#sampleOut_name")
													.val(),
											formName : "SampleOut"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layero) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = sampleOut_id;
	console.log(taskId)

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layero) {
			var operVal = $(".layui-layer-iframe", parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $(".layui-layer-iframe", parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll()
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}
	});
}


//日期格式化
$("#sampleOut_fyDate").datepicker({
	language: "zh-TW",
	autoclose: true, //选中之后自动隐藏日期选择框
	format: "yyyy-mm-dd" //日期格式，详见 
});

function showOutTypes() {
	$("#form1").data("changed",true);
	top.layer.open({
		title : biolims.user.selectionOutgoingType,
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [
				window.ctx
						+ "/dic/type/dicTypeSelectTable.action?flag=outApply",
				'' ],
		yes : function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#sampleOut_outTypes_id").val(id);
			$("#sampleOut_outTypes_name").val(name);
		},
	})

}

function showApplyUser(){
	$("#form1").data("changed",true); 
	top.layer.open({
		title: biolims.user.selectionLeader,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
			$("#sampleOut_applyUser_name").val(name);
			$("#sampleOut_applyUser_id").val(id);
			top.layer.close(index);
		},
	})
}
function showAcceptUser(){
	$("#form1").data("changed",true);
	top.layer.open({
		title: biolims.user.selectionAuditor,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
			$("#sampleOut_acceptUser_name").val(name);
			$("#sampleOut_acceptUser_id").val(id);
			top.layer.close(index);
		},
	})
}


function changeState() {
    var	id=$("#sampleOut_id").text()
	var paraStr = "formId=" + id +
		"&tableId=SampleOut";
	console.log(paraStr)
	top.layer.open({
		title: biolims.common.approvalProcess,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layero) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index);
		}

	});

	
}
