var dicPriLibDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'twfPosition',
		type:"string"
	});
	    fields.push({
		name:'solPosition',
		type:"string"
	});
	    fields.push({
		name:'dryPosition',
		type:"string"
	});
	    fields.push({
		name:'geneName',
		type:"string"
	});
	    fields.push({
		name:'chromosomeNumber',
		type:"string"
	});
	    fields.push({
		name:'site',
		type:"string"
	});
	    fields.push({
		name:'genePosPriName',
		type:"string"
	});
	    fields.push({
		name:'chrPosPriName',
		type:"string"
	});
	    fields.push({
		name:'ref5',
		type:"string"
	});
	    fields.push({
		name:'ref3',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.master.primerOrderNo,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'twfPosition',
		header:biolims.master.twfPosition,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'solPosition',
		header:biolims.master.solPosition,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'dryPosition',
		header:biolims.master.dryPosition,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'geneName',
		header:biolims.master.geneName,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'chromosomeNumber',
		header:biolims.master.chromosomeNumber,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'site',
		header:biolims.sanger.chromosomalLocation,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'genePosPriName',
		header:biolims.master.genePosPriName,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'chrPosPriName',
		header:biolims.master.chrPosPriName,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'ref5',
		header:'ref-5',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'ref3',
		header:'ref-3',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:15*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/dic/showDicPriLibListJson.action";
	var opts={};
	opts.title=biolims.master.primersDictionary;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setDicPriLibFun(rec);
	};
	dicPriLibDialogGrid=gridTable("show_dialog_dicPriLib_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(dicPriLibDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
