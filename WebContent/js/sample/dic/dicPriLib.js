var dicPriLibGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'twfPosition',
		type : "string"
	});
	fields.push({
		name : 'solPosition',
		type : "string"
	});
	fields.push({
		name : 'dryPosition',
		type : "string"
	});
	fields.push({
		name : 'geneName',
		type : "string"
	});
	fields.push({
		name : 'chromosomeNumber',
		type : "string"
	});
	fields.push({
		name : 'site',
		type : "string"
	});
	fields.push({
		name : 'genePosPriName',
		type : "string"
	});
	fields.push({
		name : 'chrPosPriName',
		type : "string"
	});
	fields.push({
		name : 'ref5',
		type : "string"
	});
	fields.push({
		name : 'item-name',
		type : "string"
	});
	fields.push({
		name : 'item-id',
		type : "string"
	});
	fields.push({
		name : 'ref3',
		type : "string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'orderDate',
		type : "string"
	});
	fields.push({
		name : 'orderCompany',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 15 * 10,
		hidden:true,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.master.gene,
		width : 15 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'geneName',
		header : biolims.QPCR.genotype,
		width : 50 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 15 * 10,
		hidden:true,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/sample/dic/showDicPriLibListJson.action";
	var opts = {};
	opts.title = biolims.master.genotypeInfo;
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post",
				"/sample/dic/delItem.action", {
					ids : ids
				}, function(data) {
					if (data.success) {
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);

	};
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});

	
	opts.tbar.push({
		text : "<font color='red'>"+biolims.common.save+"</font>",
		handler : function() {
			var result = commonGetModifyRecords(dicPriLibGrid);
			alert(result);
			if (result.length > 0) {
				ajax("post", "/sample/dic/saveItem.action", {
					itemDataJson : result
				}, function(data) {
					if (data.success) {
						message(biolims.common.saveSuccess);
						dicPriLibGrid.getStore().reload();
					} else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.batchUpload,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_upload_div"), biolims.common.batchUpload, null, {
				"Confirm" : function() {
					goInExcel();
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	dicPriLibGrid = gridEditTable("show_dicPriLib_div", cols, loadParam, opts);
})
function add() {
	window.location = window.ctx + '/sample/dic/editDicPriLib.action';
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/dic/editDicPriLib.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/dic/viewDicPriLib.action?id=' + id;
}
function exportexcel() {
	dicPriLibGrid.title = biolims.common.exportList;
	var vExportContent = dicPriLibGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 742;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(dicPriLibGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});


var idTmr = "";
function goInExcel(){
		 //iniloading();  // 等待遮罩层
		 //var filePath = $("#file-upload").val();
		  var filePath=getPath(document.getElementById("file-upload"));
			 //$("#upload").val();
		  if (filePath== ""){
		    alert(biolims.master.pleaseUploadExcel);
		        return false;
		  	}
		  var oXL = new ActiveXObject("Excel.application");
		  if (oXL == null) {
		                alert(biolims.master.createFailed);
		                return;
		  }
		  var oWB = oXL.Workbooks.open(filePath);
		  //  oXL.DisplayAlerts = false;    是否显示打开文件，如添加则会闪现Excel打开
		 //oXL.Visible = true;
		  oWB.worksheets(1).select();
		  var oSheet = oWB.ActiveSheet;
		  var row=null
		  row=oSheet.usedrange.rows.count;
		  var a = oSheet.Rows.Length;
		  try{
		      
		       
		       var cellRownum;
		       var ob = dicPriLibGrid.getStore().recordType;
				for(cellRownum=2;cellRownum<=row;cellRownum++){
					var p = new ob({});
					p.isNew = true;				
					p.set("id",oSheet.Cells(cellRownum,1).value);
					p.set("name",oSheet.Cells(cellRownum,2).value);
					p.set("geneName",oSheet.Cells(cellRownum,3).value);
					p.set("item-id",oSheet.Cells(cellRownum,5).value);
					dicPriLibGrid.getStore().insert(0, p);
					
				}
		        
		 }catch(e){
		  }
		   oXL.Quit();  
		   
		   idTmr = window.setInterval("Cleanup();",1000); 
	    
	}
function getPath(obj) {
	  if (obj) {
	  if (window.navigator.userAgent.indexOf("MSIE") >= 1) {
	  obj.select(); return document.selection.createRange().text;
	  }
	  else if (window.navigator.userAgent.indexOf("Firefox") >= 1) {
	  if (obj.files) {
	  return obj.files.item(0).getAsDataURL();
	  }
	  return obj.value;
	  }
	  return obj.value;
	  }
	  }  

function Cleanup() { 
	window.clearInterval(idTmr); 
	CollectGarbage(); 
	//Closeloading(); //关闭遮罩层
	}