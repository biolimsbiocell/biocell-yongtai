$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/sample/dic/editDicPriLib.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/dic/showDicPriLibList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
function save() {
if(checkSubmit()==true){
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/dic/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/dic/copyDicPriLib.action?id=' + $("#dicPriLib_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#dicPriLib_id").val() + "&tableId=dicPriLib");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.master.genotypeInfo,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
function showItemNumber() {
	var win = Ext.getCmp('showItemNumber');
	if (win) {
		win.close();
	}
	var showItemNumber = new Ext.Window(
			{
				id : 'showItemNumber',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : document.body.clientWidth / 2,
				height : document.body.clientHeight / 1.5,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/service/product/showProjectSelectList.action?flag=xzcs1' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showItemNumber.close();
					}
				} ]
			});
	showItemNumber.show();
}
function setxzcs1(rec){
	 document.getElementById("dicPriLib_item_id").value = rec.id;
	document.getElementById("dicPriLib_item_name").value = rec.name;
	var win = Ext.getCmp('showItemNumber')
	if(win){win.close();}
	}