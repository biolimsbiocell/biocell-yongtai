﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleInputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#inputTemp_name").val());
	}else if($("#inputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}else if($("#sampleInputTemp_name").val()!="" && $("#inputTemp_name").val()!=""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}
	
	if($("#sampleInputTemp_productName").val()!=$("#inputTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
	}	
	else{
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
		$("#productNameNew").val($("#sampleInputTemp_productName").val());
	}
	if($("#sampleInputTemp_area").val()!=$("#inputTemp_area").val())
		$("#area").css({"background-color":"red","color":"white"});
	else
		$("#areaNew").val($("#sampleInputTemp_area").val());
	
	if($("#sampleInputTemp_voucherCode").val()!=$("#inputTemp_voucherCode").val())
		$("#voucherCode").css({"background-color":"red","color":"white"});
	else
		$("#voucherCodeAgeNew").val($("#sampleInputTemp_voucherCode").val());
	
	if($("#sampleInputTemp_sendDate").val()!=$("#inputTemp_sendDate").val())
		$("#sendDate").css({"background-color":"red","color":"white"});
	else
		$("#sendDateNew").val($("#sampleInputTemp_sendDate").val());
	
	if($("#sampleInputTemp_inHosNum").val()!=$("#inputTemp_inHosNum").val())
		$("#inHosNum").css({"background-color":"red","color":"white"});
	else
		$("#inHosNumNew").val($("#sampleInputTemp_inHosNum").val());
	
	if($("#sampleInputTemp_phoneNum").val()!=$("#inputTemp_phoneNum").val())
		$("#phoneNum").css({"background-color":"red","color":"white"});
	else
		$("#phoneNumNew").val($("#sampleInputTemp_phoneNum").val());
	
	if($("#sampleInputTemp_address").val()!=$("#inputTemp_address").val())
		$("#address").css({"background-color":"red","color":"white"});
	else
		$("#addressNew").val($("#sampleInputTemp_address").val());
	
	if($("#sampleInputTemp_patientName").val()!=$("#inputTemp_patientName").val())
		$("#aabb").css({"background-color":"red","color":"white"});
	else
		$("#ccdd").val($("#sampleInputTemp_patientName").val());
	
	if($("#sampleInputTemp_birthday").val()!=$("#inputTemp_birthday").val()){
		$("#birthday").css({"background-color":"red","color":"white"});
	}else{
		var bri = $("#sampleInputTemp_birthday").val();
	    var date =bri.toString();
	    var str = date.substring(0,7);
	    if(date.substring(0,1)<=9 && date.substring(0,1)>=6)
	    	str="19"+date.substring(0,7);
	    else if(str=="0")
	    	str="20"+date.substring(0,7);
		$("#birthday").val(str);
	}
	if($("#sampleInputTemp_gestationalAge").val()!=$("#inputTemp_gestationalAge").val()){
		$("#gestationalAge").css({"background-color":"red","color":"white"});
	}else{
		$("#gestationalAgeNew").val($("#sampleInputTemp_gestationalAge").val());
	}
	if($("#sampleInputTemp_weight").val()!=$("#inputTemp_weight").val()){
		$("#weight").css({"background-color":"red","color":"white"});
	}else{
		$("#weightNew").val($("#sampleInputTemp_weight").val());
	}
	if($("#sampleInputTemp_diagnosis").val()!=$("#inputTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleInputTemp_diagnosis").val());
	}
	if($("#sampleInputTemp_outTransfusion").val()!=$("#inputTemp_outTransfusion").val()){
		$("#inputTempNew_outTransfusion").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_outTransfusion").val($("#sampleInputTemp_outTransfusion").val());
	}
	if($("#sampleInputTemp_organGrafting").val()!=$("#inputTemp_organGrafting").val()){
		$("#inputTempNew_organGrafting").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_organGrafting").val($("#sampleInputTemp_organGrafting").val());
	}
	if($("#sampleInputTemp_stemCellsCure").val()!=$("#inputTemp_stemCellsCure").val()){
		$("#inputTempNew_stemCellsCure").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_stemCellsCure").val($("#sampleInputTemp_stemCellsCure").val());
	}
	if($("#sampleInputTemp_immuneCure").val()!=$("#inputTemp_immuneCure").val()){
		$("#inputTempNew_immuneCure").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_immuneCure").val($("#sampleInputTemp_immuneCure").val());
	}
	if($("#sampleInputTemp_badMotherhood").val()!=$("#inputTemp_badMotherhood").val()){
		$("#badMotherhood").css({"background-color":"red","color":"white"});
	}else{
		$("#badMotherhoodNew").val($("#sampleInputTemp_badMotherhood").val());
	}
	if($("#sampleInputTemp_medicalHistory").val()!=$("#inputTemp_medicalHistory").val()){
		$("#medicalHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#medicalHistoryNew").val($("#sampleInputTemp_medicalHistory").val());
	}
	if($("#sampleInputTemp_parturitionTime").val()!=$("#inputTemp_parturitionTime").val()){
		$("#parturitionTime").css({"background-color":"red","color":"white"});
	}else{
		$("#parturitionTimeNew").val($("#sampleInputTemp_parturitionTime").val());
	}
	if($("#sampleInputTemp_gestationIVF").val()!=$("#inputTemp_gestationIVF").val()){
		$("#inputTempNew_gestationIVF").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_gestationIVF").val($("#sampleInputTemp_gestationIVF").val());
	}
	if($("#sampleInputTemp_acceptDate").val()!=$("#inputTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleInputTemp_acceptDate").val());
	}
	if($("#sampleInputTemp_embryoType").val()!=$("#inputTemp_embryoType").val()){
		$("#inputTempNew_embryoType").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_embryoType").val($("#sampleInputTemp_embryoType").val());
	}
	if($("#sampleInputTemp_testPattern").val()!=$("#inputTemp_testPattern").val()){
		$("#inputTempNew_testPattern").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_testPattern").val($("#sampleInputTemp_testPattern").val());
	}
	if($("#sampleInputTemp_trisome21Value").val()!=$("#inputTemp_trisome21Value").val()){
		$("#trisome21Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome21ValueNew").val($("#sampleInputTemp_trisome21Value").val());
	}
	if($("#sampleInputTemp_trisome18Value").val()!=$("#inputTemp_trisome18Value").val()){
		$("#trisome18Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome18ValueNew").val($("#sampleInputTemp_trisome18Value").val());
	}
	if($("#sampleInputTemp_trisome13Value").val()!=$("#inputTemp_trisome13Value").val()){
		$("#trisome13Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome13ValueNew").val($("#sampleInputTemp_trisome13Value").val());
	}
	if($("#sampleInputTemp_coupleChromosome").val()!=$("#inputTemp_coupleChromosome").val()){
		$("#inputTempNew_coupleChromosome").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_coupleChromosome").val($("#sampleInputTemp_coupleChromosome").val());
	}
	if($("#sampleInputTemp_doctor").val()!=$("#inputTemp_doctor").val())
		$("#doctor").css({"background-color":"red","color":"white"});
	else
		$("#doctorNew").val($("#sampleInputTemp_doctor").val());
	
	if($("#sampleInputTemp_applicatioDate").val()!=$("#inputTemp_applicatioDate").val()){
		$("#applicatioDate").css({"background-color":"red","color":"white"});
	}else{
		$("#applicatioDateNew").val($("#sampleInputTemp_applicatioDate").val());
	}
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}

$("#toolbarbutton_save").click(function() {
	var productNameNew = $("#productNameNew").val();
	if(productNameNew==""){
		message(biolims.sample.productNameIsEmpty);
		return;
	}
//	var reg = $("#addressNew").val();
//	if (reg == "") {
//		message("家庭住址不能为空！");
//		return;
//	}
	
	var nextStepFlow =$("#sampleInputTemp_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.pleaseSelectNextFlow);
		return;
	}

	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleTemplate_id").val(),
		title : $("#sampleTemplate_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTemplate_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action";
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleTemplate_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleTemplate_id").val()) {
				commonChangeState("formId=" + $("#sampleTemplate_id").val()
						+ "&tableId=SampleTemplate");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : biolims.sample.maternalPeripheral,
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : biolims.sample.selectType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleTemplate_sampleType_id").value = id;
	document.getElementById("sampleTemplate_sampleType_name").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 证件类型
function voucherTypeFun() {
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
	var voucherTypeFun = new Ext.Window(
			{
				id : 'voucherTypeFun',
				modal : true,
				title : biolims.sample.selectCertificateType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherTypeFun.close();
					}
				} ]
			});
	voucherTypeFun.show();
}
function setzjlx(id, name) {
	document.getElementById("sampleTemplate_voucherType").value = id;
	document.getElementById("sampleTemplate_voucherType_name").value = name;
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
}

function checkType() {
	var re = $("#sampleTemplate_voucherType_name").val();
	if (re == "") {
		message(biolims.sample.voucherTypeIsEmpty);
	}
}
// 证件号码验证
function checkFun() {
	var reg = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
	if (reg.test($("#sampleTemplate_voucherCode").val())) {
		var id = $("#sampleTemplate_voucherCode").val();
		ajax("post", "/sample/sampleInput/findIdentity.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.IdRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputRightId);
	}
}
// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleTemplate_phoneNum").val())) {
		// return;
		var id = $("#sampleTemplate_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.phoneRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputPhone);
	}
}
// B超异常提醒验证
function change() {
	var reg = $('#sampleTemplate_embryoType option:selected').val();
	if (reg == "2") {
		$("#sampleTemplate_messages").css("display", "");
	}
}

//选择模板
function Template() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleInput.action?id='
			+ $("#sampleInputTemp_id").val();
} 
function TemplateOne() {
	// var tid = $("#sampleInputTemp_id").val();
	// document.getElementById("sampleInputTemp_id").value=tid;
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleInputOne.action?id='
			+ $("#sampleInputTemp_id").val();
}
function TemplateTwo() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleInputTwo.action?tid='
			+ $("#sampleInputTemp_id").val();
}
function TemplateThree() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleInputThree.action?sid='
			+ $("#sampleInputTemp_id").val();
}
function TemplateFour() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleInputFour.action?aid='
			+ $("#sampleInputTemp_id").val();
}

function TumorOne() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleTumorOne.action?aid='
			+ $("#sampleTumorTemp_id").val();
}

function TumorTwo() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleTumorTwo.action?aid='
			+ $("#sampleTumorTemp_id").val();
}
function TumorThree() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleTumorThree.action?aid='
			+ $("#sampleTumorTemp_id").val();
}

function VisitOne() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleVisitOne.action?aid='
			+ $("#sampleVisitTemp_id").val();
}
function VisitTwo() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleVisitTwo.action?aid='
			+ $("#sampleVisitTemp_id").val();
}


function BloodDisease() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleBloodDisease.action?aid='
			+ $("#sampleBloodDiseaseTemp_id").val();
}

function BreastCancerOne() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleBreastCancerOne.action?aid='
			+ $("#sampleBloodDiseaseTemp_id").val();
}


function BreastCancerTwo() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleBreastCancerTwo.action?aid='
			+ $("#sampleBloodDiseaseTemp_id").val();
}

function BreastCancerThree() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleBreastCancerThree.action?aid='
			+ $("#sampleBloodDiseaseTemp_id").val();
}

function Chromosome() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleChromosome.action?aid='
			+ $("#sampleChromosomeTemp_id").val();
}

function FolicAcid() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleFolicAcid.action?aid='
			+ $("#sampleFolicAcidTemp_id").val();
}


function Gene() {
	window.location = window.ctx
			+ '/sample/sampleInput/editSampleGene.action?aid='
			+ $("#sampleGeneTemp_id").val();
}


var item = menu.add({
	text : '原始模板'
});
item.on('click', Template);
var item = menu.add({
	text : '青岛市妇女儿童医院高通量基因测序产前筛查临床申请单'
});
item.on('click', TemplateOne);
var item = menu.add({
	text : '高通量基因测序产前筛查与诊断临床申请单 '
});
item.on('click', TemplateTwo);
var item = menu.add({
	text : '孕妇外周血胎儿染色体非整倍体筛查（NIPT）临床申请单'
});
item.on('click', TemplateThree);
var item = menu.add({
	text : '高通量基因测序产前检测临床报告单'
});
item.on('click', TemplateFour);
var item = menu.add({
	text : '遗传性肿瘤基因检测'
});
item.on('click', TumorOne);

var item = menu.add({
	text : '肿瘤个体化用药基因检测样本信息录入表'
});
item.on('click', TumorTwo);

var item = menu.add({
	text : '肿瘤化疗用药基因检测样本信息录入表'
});
item.on('click', TumorThree);

var item = menu.add({
	text : 'KRAS基因突变检测样本信息录入表'
});
item.on('click', VisitOne);

var item = menu.add({
	text : 'EGFR基因突变检测样本信息录入表'
});
item.on('click', VisitTwo);

var item = menu.add({
	text : '血液病基因检测样本信息录入表'
});
item.on('click', BloodDisease);

var item = menu.add({
	text : '奥拉帕尼伴随诊断信息录入表'
});
item.on('click', BreastCancerOne);

var item = menu.add({
	text : '乳腺癌基因检测样本信息录入'
});
item.on('click', BreastCancerTwo);

var item = menu.add({
	text : '乳腺癌高风险人群基因筛查课题-科研样本'
});
item.on('click', BreastCancerThree);

var item = menu.add({
	text : '染色体异常检测样本信息录入表'
});
item.on('click', Chromosome);

var item = menu.add({
	text : '叶酸利用能力基因检测信息录入表'
});
item.on('click', FolicAcid);


var item = menu.add({
	text : '诺康无忧基因体检（好生100）样本信息录入表'
});
item.on('click', Gene);



// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("sampleInputTemp_productId").value = id;
					document.getElementById("sampleInputTemp_productName").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}

function a(){
	var productName = $("#productName").val();
	var id1 = $("#sampleInputTemp_productId").val();
	var name1= $("#sampleInputTemp_productName").val();
	var id2= $("#inputTemp_productId").val();
	var name2 = $("#inputTemp_productName").val();
	if(productName==name1){
		$("#productIdNew").val(id1);
	}else if(productName==name2){
		$("#productIdNew").val(id2);
	}else if(productName==""){
		$("#productIdNew").val("");
	}
}