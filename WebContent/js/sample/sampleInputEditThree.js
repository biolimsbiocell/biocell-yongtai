﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	//验证有无图片如果没有就进行上传图片
	if($("#upload_imga_id").val()==''){
		$(":input").addClass(".text input readonlytrue");
		$("[type='text']").focus(function ()
		{
			message(biolims.sample.pleaseUploadImage);
			return; 
		}) ;
	}
});
function add() {
	window.location = window.ctx + "/sample/sampleInput/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/sample/sampleInput/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}

$("#toolbarbutton_save").click(function() {
	//sampleInfo 的图片Id
	var sampleInfoImg = $("#upload_imga_id10").val();
	//sampleInputTemp 的图片Id
	var sampleInputTempimg = $("#upload_imga_id").val();
	if($("#upload_imga_name11").val()!="" && $("#upload_imga_id10").val()!="" && sampleInfoImg == sampleInputTempimg){
		
//		//检测项目验证
		var productName = $("#sampleInputTemp_productName").val();
		if (productName == "") {
			message(biolims.sample.productNameIsEmpty);
			return;
		}
//		//证件号码验证 ps:输入正确的证件号码后自动计算出年龄，性别
//		//身份证号的正则
//		var idCodeReg = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
//		//性别
//		var voucherCode =$("#sampleInputTemp_voucherCode").val();
//		if(voucherCode!=""){
//			if(idCodeReg.test(voucherCode)){//自动获取年龄
//				var year = voucherCode.substring(6,10);
//				var currentTime =new Date();
//				var birthday =voucherCode.substring(6,14);
//				var b=birthday.replace(/^(\d{4})(\d{2})(\d{2})$/, "$1-$2-$3");//把字符串转换成日期yyyy-MM-dd
//				console.log(b);
//				$("#sampleInputTemp_birthday").val(b);//出身日期
//				var ages = currentTime.getFullYear().toString() -year;
//				if(ages>=16 && ages<=50){
//					$("#sampleInputTemp_age").val(ages);
//				}else{
//					message("请输入正确的证件号码！");
//					return;
//				}
//			}else{
//				message("请输入正确的证件号码！");
//				return;
//			}
//			if(idCodeReg.test(voucherCode)){//自动获取性别
//				var genderNum = voucherCode.substring(16,17);
//				if(genderNum=='1' || genderNum=='3' || genderNum=='5'|| genderNum=='7'|| genderNum=='9'){
//					$("#sampleInputTemp_gender").val(1);
//				}else{
//					$("#sampleInputTemp_gender").val(0);
//				}
//			}else{
//				message("请输入正确的证件号码！");
//				return;
//			}
//		}else {
//			message("证件号码不能为空");
//			return;
//		}
//		
//		
//		//家庭住址验证
//		var address = $("#sampleInputTemp_address").val();
//		if (address == "") {
//			message("家庭住址不能为空！");
//			return;
//		}
//		//	孕周的验证
//			var gestationalAge =$("#sampleInputTemp_gestationalAge").val();
//			var gestationalAgeReg = /^$|^[0-2][0-9][w]\+[1-7]$|^[0-2][0-9][w]$/;
//			if(gestationalAgeReg.test(gestationalAge)){
//				alert(0);
//				var numAge = gestationalAge.substring(0,2);
//				var sumAge =gestationalAge.substring(4);
//				if(numAge>27){
//					if(confirm("此孕周超过送检孕周要求,是否录入？")){
//					}else {
//						$("#sampleInputTemp_gestationalAge").val("");
//						document.getElementById("sampleInputTemp_gestationalAge").focus(); 
//						return;
//					}
//				}else if(sumAge>7 && numAge<=11 && sumAge!=""){
//					alert(2);
//					if(confirm("此孕周超过送检孕周要求,是否录入？")){
//					}else{
//						$("#sampleInputTemp_gestationalAge").val("");
//						document.getElementById("sampleInputTemp_gestationalAge").focus(); 
//						return;
//					}
//				}
//			}else{
//				alert(3);
//				message("请输入正确的孕周！");
//				return;
//			}
//		}
//		
//		//体重验证
//		var weight =$("#sampleInputTemp_weight").val();
//		if(weight>100){
//			if(confirm("根据体重系统检测出该孕妇为无创DNA检查慎用人群,是否录入?")){
//				
//			}else {
//				$("#sampleInputTemp_weight").val("");
//				document.getElementById("sampleInputTemp_weight").focus(); 
//				return;
//			}
//		
	}else {
		if($("#upload_imga_id").val()==""){
			message(biolims.sample.pleaseUploadImage2Save);
			return;
		}
	}
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleInputTemp_id").val(),
		title : $("#sampleInputTemp_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTemplate_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInput/save.action?imgId="+$("#upload_imga_id").val()+"&saveType="+$("#saveType").val();
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleInputTemp_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleInputTemp_code").val()) {
				commonChangeState("formId=" + $("#sampleInputTemp_id").val()
						+ "&tableId=SampleInputTemp");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInputTemp_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : biolims.sample.infoInput,
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : biolims.sample.selectType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleInputTemp_sampleType_id").value = id;
	document.getElementById("sampleInputTemp_sampleType_name").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 证件类型
function voucherTypeFun() {
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
	var voucherTypeFun = new Ext.Window(
			{
				id : 'voucherTypeFun',
				modal : true,
				title : biolims.sample.selectCertificateType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherTypeFun.close();
					}
				} ]
			});
	voucherTypeFun.show();
}
function setzjlx(id, name) {
	document.getElementById("sampleInputTemp_voucherType").value = id;
	document.getElementById("sampleInputTemp_voucherType_name").value = name;
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
}

function checkType() {
	var re = $("#sampleInputTemp_voucherType_name").val();
	if (re == "") {
		message(biolims.sample.voucherTypeIsEmpty);
	}
}
// 证件号码验证
function checkFun() {
	var reg = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
	if (reg.test($("#sampleInputTemp_voucherCode").val())) {
		var id = $("#sampleInputTemp_voucherCode").val();
		ajax("post", "/sample/sampleInput/findIdentity.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.IdRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputRightId);
	}
}
// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleInputTemp_phoneNum").val())) {
		// return;
		var id = $("#sampleInputTemp_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.IdRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputRightId);
	}
}

// 家庭住址验证
function checkAddress() {
	var address = $("#sampleInputTemp_address").val();
	if (address == "") {
		message(biolims.sample.addressIsEmpty);
	}
}
// B超异常提醒验证
function change() {
	var reg = $('#sampleTemplate_embryoType option:selected').val();
	if (reg == "2") {
		$("#sampleInputTemp_messages").css("display", "");
	}
}

/*
 * 上传图片
 */
function upLoadImg1(){
	var isUpload = true;
	var id=$("#template_testType").val();
	var fId=$("#upload_imga_id11").val();
	var fName=$("#upload_imga_name").val();
	
	load("/system/template/template/toSampeUpload.action", { // 是否修改
		fileId:fId,
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			fName=data.fileName;
			fId=data.fileId;
//				$("#template_fileInfo").val(fId);
//				$("#template_fileInfo_name").val(fName);
			 document.getElementById('upload_imga_id').value=fId;
			 document.getElementById('upload_imga_name11').value=fName;
		});
	});
}

// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("sampleInputTemp_productId").value = id;
					document.getElementById("sampleInputTemp_productName").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}
//计算天数差的函数，通用  
function  DateDiff(sDate1,  sDate2){    //sDate1和sDate2是yyyy-MM-dd格式  
    var  aDate,  oDate1,  oDate2,  iDays ;
    aDate  =  sDate1.split("-");
    oDate1 = new Date(aDate[0] , aDate[1] ,aDate[2]); //转换为12-18-2006格式 
    aDate = sDate2.split("-") ;
    oDate2 = new Date(aDate[0] , aDate[1] , aDate[2]);
    iDays  =  parseInt(Math.abs(oDate1  -  oDate2)/1000/60/60/24);//把相差的毫秒数转换为天数  

    return  iDays;
}