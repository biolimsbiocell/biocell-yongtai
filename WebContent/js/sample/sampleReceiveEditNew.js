﻿﻿﻿﻿﻿﻿/* 
 * 文件名称 :sampleReceiveEdit.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/01/30
 * 文件描述: 样本接收的相关页面函数
 * 
 */
var flg = true;
$(function() {
//	if($("#sampleReceive_state").text()=="已下达"){
//		$("#btn_save").hide();
//	}
	
	var nameId =sessionStorage.getItem("userId");
//    
	if($("#sampleReceive_qualityCheckUser_id").val()==nameId){
		if($("#sampleReceive_state").text()=="已下达"){	
			$("#Qchidden").hide();
		}
	
    }
	if($("#sampleReceive_state").text() == "已下达"&&($("#bpmTaskId").val()=="null"||$("#bpmTaskId").val()=="")){
		$("#btn_save").hide();
		$("#btn_submit").hide();
		$("#btn_changeState").hide();
		
		
		
	}
	if($("#sampleReceive_state").text()=="完成"){
		$("#Qchidden").show();
		$("#btn_changeState").show();
		
	}
	
	if($("#sampleReceive_state").text()=="作废"){
	$("#btn_save").hide();
	$("#btn_submit").hide();
    }
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });
	
	
	$("#sampleReceive_acceptDate").datetimepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd hh:ii" // 日期格式，详见
	});
	$("#sampleReceive_startBackTime").datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd" // 日期格式，详见
	});
	$("#sampleReceive_feedBackTime").datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd" // 日期格式，详见
	});
	$("#sampleReceive_thermometerTime").datetimepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd hh:ii" // 日期格式，详见
	});
	$("#sampleReceive_thermometerTime2").datetimepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd hh:ii" // 日期格式，详见
	});
	// 接收样本了类型的名称
	var sampleTypeTitle = $("#sampleReceiveTitle").attr("type");
	if (sampleTypeTitle == "1") {
		$("#sampleReceiveTitle").text(biolims.common.kySample);
		$(".scientific").show();
	}
	if ($("#sampleReveice_id").text() == "NEW") {
		$("#sampleReceiveModal").modal("show");
		// 选择临床样本或者科研样本
		choseClinicalOrScientific();
	}
	
//	$("#btn_changeState").hide();
	
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view"
			|| $("#sampleReceive_state").text() == "Complete"
			|| $("#sampleReceive_state").text() == "完成") {
		settextreadonly();
		$("#btn_save").hide();
		$("#btn_submit").hide();
		
		$("#fileUpId").hide();
	}
//	if ($("#sampleReceive_state").text() != "新建") {
//		$("#btn_submit").hide();
//	}else{
//		$("#btn_submit").hide();
//	}
	renderDatatables(flg);
	// 批量结果的按钮变为下拉框
	btnChangeDropdown($('#main'), $(".resultsBatchBtn"), [
			biolims.common.qualified, biolims.common.disqualified ], "method");
	// // 上传附件
	fileInput('1', 'sampleReceive', $("#sampleReveice_id").text());
});
// 选择临床样本或者科研样本
function choseClinicalOrScientific() {
	$(".modal-header .btn-lg").click(
			function() {
				var index = $(this).index();
				$(this).siblings(".btn-lg").removeClass("chosedType");
				$(this).addClass("chosedType");
				// 临床样本
				if (index == 1) {
					$("#modal-body #scancode").hide();
					$("#modal-body .col-xs-12").show();
					$("#modal-body .col-xs-12").removeClass("col-sm-6")
							.addClass("col-sm-4").eq(0).show();
					$(".scientific").hide();
					$("#sampleReceiveTitle")
							.text(biolims.master.clinicalSample).attr("type",
									"3");
					flg = true;
				}
				// 科研样本
				if (index == 2) {
					$("#modal-body #scancode").hide();
					$("#modal-body .col-xs-12").show();
					$("#modal-body .col-xs-12").removeClass("col-sm-4")
							.addClass("col-sm-6").eq(0).hide();
					$(".scientific").show();
					$("#sampleReceiveTitle").text(biolims.common.kySample)
							.attr("type", "1");
					flg = false;
				}
			});
	// 扫码、上传、新建的不同流向
	$("#modal-body .col-xs-12").click(function() {
		var index = $(this).index();
		// 扫码
		if (index == 0) {
			$("#modal-body .col-xs-12").hide();
			$("#modal-body #scancode").show();
			scanCode();
		}
		// 上传
		if (index == 1) {
			$("#sampleReceiveModal").modal("hide");
			$("#uploadCsv").modal("show");
			// $(".fileinput-remove").click();
			uploadCsv();
		}
		// 新建
		if (index == 2) {
			$("#sampleReceiveModal").modal("hide");
		}
	});
}


// 渲染的datatables
var myTablel;
var oldChangeLog;
function renderDatatables(flg) {
	var colOpts = [];
	var colOpts1 = [];
//	if(){
//		
//	}else{
//		
//	}
	//第一
	colOpts.push({
		"data" : 'id',
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "receiveState",
		"title" : "是否已接收",
		// "className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "receiveState");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			}
			if (data == "1") {
				return "是";
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : 'name',
		"visible" : false,
		"title" : biolims.common.packetNumber,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	})
	colOpts.push({
		"data" : "batch",
		"title" : "产品批号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "batch");
		}
	})
	colOpts.push({
		"data" : "barCode",
		"title" : biolims.common.barCode,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "barCode");
			
			
			
			$(td).attr("sampleCode", rowData['sampleCode']);
			$(td).attr("sampleNum", rowData['sampleNum']);
			$(td).attr("orderCode", rowData['orderCode']);
			$(td).attr("gender", rowData['gender']);
			$(td).attr("patientName", rowData['patientName']);
			$(td).attr("abbreviation", rowData['abbreviation']);
			$(td).attr("dicSampleType-id", rowData['dicSampleType-id']);
			$(td).attr("samplingDate", rowData['samplingDate']);
			$(td).attr("EMRId", rowData['EMRId']);
			
			$(td).attr("bloodVolume", rowData['bloodVolume']);
			$(td).attr("bloodColourTest", rowData['bloodColourTest']);
			$(td).attr("bloodColourNotes", rowData['bloodColourNotes']);
			$(td).attr("coagulationTest", rowData['coagulationTest']);
			$(td).attr("coagulationNotes", rowData['coagulationNotes']);
			$(td).attr("sealingBloodVessels", rowData['sealingBloodVessels']);
			$(td).attr("sealingBloodNotes", rowData['sealingBloodNotes']);
			$(td).attr("bloodVesselTime", rowData['bloodVesselTime']);
			$(td).attr("bloodTime", rowData['bloodTime']);
			$(td).attr("other", rowData['other']);
			$(td).attr("method", rowData['method']);
			$(td).attr("otherInformationNotes", rowData['otherInformationNotes']);
			$(td).attr("nextFlow", rowData['nextFlow']);
			$(td).attr("nextFlowId", rowData['nextFlowId']);
			$(td).attr("note", rowData['note']);
			

			
		}
	})
	colOpts.push({
		"data" : "sequenceFun",
		"title" : biolims.common.sequencingFun,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sequenceFun");
		}
	})
	colOpts.push({
		"data" : "productId",
		"title" : biolims.master.productId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data" : "productName",
		"title" : biolims.master.product,
		"width" : "100px",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "productName");
			$(td).attr("productId", rowData['productId']);
		}
	})
	if($("#sampleReceive_state").text()!="完成"){
		
	if($("#sampleReceive_qualityCheckUser_id").val()!=sessionStorage.getItem("userId")){
		
	
	colOpts.push({
		"data" : "gender",
		"title" : biolims.common.gender,
		"className" : "select",
		"name" : biolims.common.male + "|" + biolims.common.female + "|"
				+ biolims.common.unknown,
		"createdCell" : function(td) {
			$(td).attr("saveName", "gender");
			$(td).attr(
					"selectOpt",
					biolims.common.male + "|" + biolims.common.female + "|"
							+ biolims.common.unknown);
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return biolims.common.female;
			}
			if (data == "1") {
				return biolims.common.male;
			}
			if (data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "patientName",
		"title" : biolims.common.patientName,
//				+ '<img src="/images/required.gif"/>',
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientName");
		}
	})
	colOpts.push({
		"data" : "abbreviation",
		"title" : "姓名缩写",
//				+ '<img src="/images/required.gif"/>',
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "abbreviation");
		}
	})
	colOpts.push({
		"data" : "dicSampleType-id",
		"title" : "样本名称编号",
		"visible": false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data" : "dicSampleType-name",
		"title" : "样本名称"
				+ '<img src="/images/required.gif"/>',
		"width" : "80px",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleType-name");
			$(td).attr("dicSampleType-id", rowData['dicSampleType-id']);
		}
	})
	
	colOpts.push({
		"data" : "samplingDate",
		"title" : "采血时间",
		"className" : "date",
		width : "200px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "samplingDate");
		}
	})
	colOpts.push({
		"data" : "dicTypeName",
		"title" : biolims.sample.samplingLocationName,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dicTypeName");
		}
	})
	colOpts.push({
		"data" : "analysisType",
		"title" : biolims.common.analysisType,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "analysisType");
		}
	})
	colOpts.push({
		"data" : "unitNote",
		"title" : biolims.common.unitNote,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unitNote");
		}
	})
	colOpts.push({
		"data" : "idCard",
		"title" : biolims.common.outCode,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "idCard");
		}
	})
	colOpts.push({
		"data" : "EMRId",
		"title" : biolims.sample.medicalNumber,
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "EMRId");
		}
	})
	}
	if($("#sampleReceive_qualityCheckUser_id").val()==sessionStorage.getItem("userId")){
		if($("#sampleReceive_state").text()=="已下达"){
			
		
	colOpts.push({
		"data" : "acceptDate",
		"title" : biolims.common.acceptDate,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "acceptDate");
		}
	})
	colOpts.push({
		"data" : "bloodVolume",
		"title" : "血量",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodVolume");
			$(td).attr("onChange","findBloodVolume(this)")
		}
	})
	colOpts.push({
		"data" : "bloodColourTest",
		"title" : "血样颜色检验 ",
		"className" : "select",
		"name" : "" + "|" + "鲜红" + "|" + "暗红",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodColourTest");
			$(td).attr("selectOpt", "" + "|" + "鲜红" + "|" + "暗红");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "";
			}
			if (data == "1") {
				return "鲜红";
			} else if (data == "2") {
				return '暗红';
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "bloodColourNotes",
		"title" : "备注 ",
		"className" : "edit",
		// "visible": false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodColourNotes");
		}
	})
	colOpts.push({
		"data" : "coagulationTest",
		"title" : "是否凝血 ",
		"className" : "select",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "coagulationTest");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "coagulationNotes",
		"title" : "备注 ",
		"className" : "edit",
		// "visible": false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "coagulationNotes");
		}
	})
	colOpts.push({
		"data" : "sealingBloodVessels",
		"title" : "采血管是否发送渗漏 ",
		"className" : "select",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sealingBloodVessels");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "sealingBloodNotes",
		"title" : "备注 ",
		"className" : "edit",
		// "visible": false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sealingBloodNotes");
		},
	})
	colOpts.push({
		"data" : "bloodVesselTime",
		"title" : "采血管是否在有效期内 ",
		"className" : "select",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodVesselTime");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "bloodTime",
		"title" : "采血管有效期 ",
		"className" : "day",
		// "visible": false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodTime");
		}
	})

	colOpts.push({
		"data" : "other",
		"title" : "其他 ",
		"className" : "edit",
		// "visible": false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "other");
		}
	})

	colOpts.push({
		"data" : "method",
		"title" : biolims.common.result + '<img src="/images/required.gif"/>',
		"className" : "select",
		"name" : biolims.common.qualified + "|" + biolims.common.disqualified
				+ "|让步放行" + "|备用空管",
		"createdCell" : function(td) {
			$(td).attr("saveName", "method");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified + "|让步放行" + "|备用空管");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return biolims.common.qualified;
			}
			if (data == "0") {
				return biolims.common.disqualified;
			}
			if (data == "2") {
				return "让步放行";
			}
			if (data == "3") {
				return "备用空管";
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "otherInformationNotes",
		"title" : "其他情况说明 ",
		"className" : "edit",
		// "visible": false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "otherInformationNotes");
		}
	})

	colOpts
			.push({
				"data" : "nextFlow",
				"title" : biolims.common.nextFlow
						+ '<img src="/images/required.gif"/>',
				"width" : "170px",
				"createdCell" : function(td, data, rowData) {
					$(td).attr("saveName", "nextFlow");
					$(td).attr("nextFlowId", rowData['nextFlowId']);
				}
			})
	colOpts.push({
		"data" : "unusual-id",
		"title" : biolims.sample.unusualName,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unusual-id");
		}
	})
	colOpts.push({
		"data" : "location",
		"visible" : false,
		"title" : biolims.sample.expreceCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "location");
		}
	})
	colOpts.push({
		"data" : "advice",
		"visible" : false,
		"title" : biolims.common.method,
		"createdCell" : function(td) {
			$(td).attr("saveName", "advice");
		}
	})
	colOpts.push({
		"data" : "isFull",
		"visible" : false,
		"title" : biolims.sample.isFull,
		"createdCell" : function(td) {
			$(td).attr("saveName", "isFull");
		}
	})
	colOpts.push({
		"data" : "note",
		"className" : "edit",
		"title" : biolims.common.note,
		"width" : "170px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	})
	}else{
		
		colOpts.push({
			"data" : "gender",
			"title" : biolims.common.gender,
			"className" : "select",
			"name" : biolims.common.male + "|" + biolims.common.female + "|"
					+ biolims.common.unknown,
			"createdCell" : function(td) {
				$(td).attr("saveName", "gender");
				$(td).attr(
						"selectOpt",
						biolims.common.male + "|" + biolims.common.female + "|"
								+ biolims.common.unknown);
			},
			"render" : function(data, type, full, meta) {
				if (data == "0") {
					return biolims.common.female;
				}
				if (data == "1") {
					return biolims.common.male;
				}
				if (data == "3") {
					return biolims.common.unknown;
				} else {
					return '';
				}
			}
		})
		colOpts.push({
			"data" : "patientName",
			"title" : biolims.common.patientName,
//					+ '<img src="/images/required.gif"/>',
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "patientName");
			}
		})
		colOpts.push({
			"data" : "abbreviation",
			"title" : "姓名缩写",
//					+ '<img src="/images/required.gif"/>',
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "abbreviation");
			}
		})
		colOpts.push({
			"data" : "dicSampleType-id",
			"title" : "样本名称编号",
			"visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "dicSampleType-id");
			}
		})
		colOpts.push({
			"data" : "dicSampleType-name",
			"title" : "样本名称"
					+ '<img src="/images/required.gif"/>',
			"width" : "80px",
			"createdCell" : function(td, data, rowData) {
				$(td).attr("saveName", "dicSampleType-name");
				$(td).attr("dicSampleType-id", rowData['dicSampleType-id']);
			}
		})
		
		colOpts.push({
			"data" : "samplingDate",
			"title" : "采血时间",
			"className" : "date",
			width : "200px",
			"createdCell" : function(td) {
				$(td).attr("saveName", "samplingDate");
			}
		})
		colOpts.push({
			"data" : "dicTypeName",
			"title" : biolims.sample.samplingLocationName,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "dicTypeName");
			}
		})
		colOpts.push({
			"data" : "analysisType",
			"title" : biolims.common.analysisType,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "analysisType");
			}
		})
		colOpts.push({
			"data" : "unitNote",
			"title" : biolims.common.unitNote,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "unitNote");
			}
		})
		colOpts.push({
			"data" : "idCard",
			"title" : biolims.common.outCode,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "idCard");
			}
		})
		colOpts.push({
			"data" : "EMRId",
			"title" : biolims.sample.medicalNumber,
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "EMRId");
			}
		})
	}
	}
	}else{
		colOpts.push({
			"data" : "gender",
			"title" : biolims.common.gender,
			"className" : "select",
			"name" : biolims.common.male + "|" + biolims.common.female + "|"
					+ biolims.common.unknown,
			"createdCell" : function(td) {
				$(td).attr("saveName", "gender");
				$(td).attr(
						"selectOpt",
						biolims.common.male + "|" + biolims.common.female + "|"
								+ biolims.common.unknown);
			},
			"render" : function(data, type, full, meta) {
				if (data == "0") {
					return biolims.common.female;
				}
				if (data == "1") {
					return biolims.common.male;
				}
				if (data == "3") {
					return biolims.common.unknown;
				} else {
					return '';
				}
			}
		})
		colOpts.push({
			"data" : "patientName",
			"title" : biolims.common.patientName,
//					+ '<img src="/images/required.gif"/>',
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "patientName");
			}
		})
		colOpts.push({
			"data" : "abbreviation",
			"title" : "姓名缩写",
//					+ '<img src="/images/required.gif"/>',
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("abbreviation", "abbreviation");
			}
		})
		colOpts.push({
			"data" : "dicSampleType-id",
			"title" : "样本名称编号",
			"visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "dicSampleType-id");
			}
		})
		colOpts.push({
			"data" : "dicSampleType-name",
			"title" : "样本名称"
					+ '<img src="/images/required.gif"/>',
			"width" : "80px",
			"createdCell" : function(td, data, rowData) {
				$(td).attr("saveName", "dicSampleType-name");
				$(td).attr("dicSampleType-id", rowData['dicSampleType-id']);
			}
		})
		
		colOpts.push({
			"data" : "samplingDate",
			"title" : "采血时间",
			"className" : "date",
			width : "200px",
			"createdCell" : function(td) {
				$(td).attr("saveName", "samplingDate");
			}
		})
		colOpts.push({
			"data" : "dicTypeName",
			"title" : biolims.sample.samplingLocationName,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "dicTypeName");
			}
		})
		colOpts.push({
			"data" : "analysisType",
			"title" : biolims.common.analysisType,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "analysisType");
			}
		})
		colOpts.push({
			"data" : "unitNote",
			"title" : biolims.common.unitNote,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "unitNote");
			}
		})
		colOpts.push({
			"data" : "idCard",
			"title" : biolims.common.outCode,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "idCard");
			}
		})
		colOpts.push({
			"data" : "EMRId",
			"title" : biolims.sample.medicalNumber,
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "EMRId");
			}
		})
		
		
		colOpts.push({
			"data" : "acceptDate",
			"title" : biolims.common.acceptDate,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "acceptDate");
			}
		})
		colOpts.push({
			"data" : "bloodVolume",
			"title" : "血量",
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "bloodVolume");
			}
		})
		colOpts.push({
			"data" : "bloodColourTest",
			"title" : "血样颜色检验 ",
			"className" : "select",
			"name" : "" + "|" + "鲜红" + "|" + "暗红",
			"createdCell" : function(td) {
				$(td).attr("saveName", "bloodColourTest");
				$(td).attr("selectOpt", "" + "|" + "鲜红" + "|" + "暗红");
			},
			"render" : function(data, type, full, meta) {
				if (data == "0") {
					return "";
				}
				if (data == "1") {
					return "鲜红";
				} else if (data == "2") {
					return '暗红';
				} else {
					return '';
				}
			}
		})
		colOpts.push({
			"data" : "bloodColourNotes",
			"title" : "备注 ",
			"className" : "edit",
			// "visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "bloodColourNotes");
			}
		})
		colOpts.push({
			"data" : "coagulationTest",
			"title" : "是否凝血 ",
			"className" : "select",
			"name" : biolims.common.yes + "|" + biolims.common.no,
			"createdCell" : function(td) {
				$(td).attr("saveName", "coagulationTest");
				$(td).attr("selectOpt",
						biolims.common.yes + "|" + biolims.common.no);
			},
			"render" : function(data, type, full, meta) {
				if (data == biolims.common.yes) {
					return biolims.common.yes;
				}
				if (data == biolims.common.no) {
					return biolims.common.no;
				} else {
					return '';
				}
			}
		})
		colOpts.push({
			"data" : "coagulationNotes",
			"title" : "备注 ",
			"className" : "edit",
			// "visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "coagulationNotes");
			}
		})
		colOpts.push({
			"data" : "sealingBloodVessels",
			"title" : "采血管是否发送渗漏 ",
			"className" : "select",
			"name" : biolims.common.yes + "|" + biolims.common.no,
			"createdCell" : function(td) {
				$(td).attr("saveName", "sealingBloodVessels");
				$(td).attr("selectOpt",
						biolims.common.yes + "|" + biolims.common.no);
			},
			"render" : function(data, type, full, meta) {
				if (data == biolims.common.yes) {
					return biolims.common.yes;
				}
				if (data == biolims.common.no) {
					return biolims.common.no;
				} else {
					return '';
				}
			}
		})
		colOpts.push({
			"data" : "sealingBloodNotes",
			"title" : "备注 ",
			"className" : "edit",
			// "visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "sealingBloodNotes");
			},
		})
		colOpts.push({
			"data" : "bloodVesselTime",
			"title" : "采血管是否在有效期内 ",
			"className" : "select",
			"name" : biolims.common.yes + "|" + biolims.common.no,
			"createdCell" : function(td) {
				$(td).attr("saveName", "bloodVesselTime");
				$(td).attr("selectOpt",
						biolims.common.yes + "|" + biolims.common.no);
			},
			"render" : function(data, type, full, meta) {
				if (data == biolims.common.yes) {
					return biolims.common.yes;
				}
				if (data == biolims.common.no) {
					return biolims.common.no;
				} else {
					return '';
				}
			}
		})
		colOpts.push({
			"data" : "bloodTime",
			"title" : "采血管有效期 ",
			"className" : "day",
			// "visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "bloodTime");
			}
		})

		colOpts.push({
			"data" : "other",
			"title" : "其他 ",
			"className" : "edit",
			// "visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "other");
			}
		})

		colOpts.push({
			"data" : "method",
			"title" : biolims.common.result + '<img src="/images/required.gif"/>',
			"className" : "select",
			"name" : biolims.common.qualified + "|" + biolims.common.disqualified
					+ "|让步放行" + "|备用空管",
			"createdCell" : function(td) {
				$(td).attr("saveName", "method");
				$(td).attr(
						"selectOpt",
						biolims.common.qualified + "|"
								+ biolims.common.disqualified + "|让步放行" + "|备用空管");
			},
			"render" : function(data, type, full, meta) {
				if (data == "1") {
					return biolims.common.qualified;
				}
				if (data == "0") {
					return biolims.common.disqualified;
				}
				if (data == "2") {
					return "让步放行";
				}
				if (data == "3") {
					return "备用空管";
				} else {
					return '';
				}
			}
		})
		colOpts.push({
			"data" : "otherInformationNotes",
			"title" : "其他情况说明 ",
			"className" : "edit",
			// "visible": false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "otherInformationNotes");
			}
		})

		colOpts
				.push({
					"data" : "nextFlow",
					"title" : biolims.common.nextFlow
							+ '<img src="/images/required.gif"/>',
					"width" : "170px",
					"createdCell" : function(td, data, rowData) {
						$(td).attr("saveName", "nextFlow");
						$(td).attr("nextFlowId", rowData['nextFlowId']);
					}
				})
		colOpts.push({
			"data" : "unusual-id",
			"title" : biolims.sample.unusualName,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "unusual-id");
			}
		})
		colOpts.push({
			"data" : "location",
			"visible" : false,
			"title" : biolims.sample.expreceCode,
			"createdCell" : function(td) {
				$(td).attr("saveName", "location");
			}
		})
		colOpts.push({
			"data" : "advice",
			"visible" : false,
			"title" : biolims.common.method,
			"createdCell" : function(td) {
				$(td).attr("saveName", "advice");
			}
		})
		colOpts.push({
			"data" : "isFull",
			"visible" : false,
			"title" : biolims.sample.isFull,
			"createdCell" : function(td) {
				$(td).attr("saveName", "isFull");
			}
		})
		colOpts.push({
			"data" : "note",
			"className" : "edit",
			"title" : biolims.common.note,
			"width" : "170px",
			"createdCell" : function(td) {
				$(td).attr("saveName", "note");
			}
		})
		
	}

	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view"
			&& $("#sampleReceive_state").text() != "Complete"
			&& $("#sampleReceive_state").text() != "完成") {
		console.log($("#bpmTaskId").val());
		if($("#sampleReceive_state").text() == "已下达"&&($("#bpmTaskId").val()!="null"||$("#bpmTaskId").val()!=null)){
		tbarOpts.push({
			text : '扫描条码',
			action : function() {
				xianshisaomiao();
			}
		});
		tbarOpts.push({
			text : biolims.common.fillDetail,
			action : function() {
				addItem($("#main"));
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#main"))
			}
		});

//		tbarOpts.push({
//			text : biolims.common.delSelected,
//			action : function() {
//				removeChecked($("#main"),
//						"/sample/sampleReceive/delSampleReceiveItem.action");
//			}
//		});
		tbarOpts.push({
			text : '<i class="fa fa-h-square"></i> '
					+ biolims.common.selectProducts,
			action : detectionProject,
		});
		tbarOpts.push({
			text : '<i class="fa fa-yelp"></i> ' + "样本名称",
			action : sampleType,
		});
		tbarOpts.push({
			text : '<i class="fa fa-paypal"></i> ' + biolims.common.nextFlow,
			action : function() {
				nextFlow();
			}
		});

		tbarOpts.push({
			text : '<i class="fa fa-ioxhost"></i> '
					+ biolims.common.batchResult,
			className : 'btn btn-sm btn-success resultsBatchBtn',
		});
		tbarOpts.push({
			   text : '打印接收单',
			   action : function() {
			    dayin();
			   }
			  });
		tbarOpts.push({
			   text : '病人订单',
			   action : function() {
			    skip();
			   }
		})
		}
		if($("#sampleReceive_state").text() == "新建"){
			tbarOpts.push({
				text : '扫描条码',
				action : function() {
					xianshisaomiao();
				}
			});
			tbarOpts.push({
				text : biolims.common.fillDetail,
				action : function() {
					addItem($("#main"));
				}
			});
			tbarOpts.push({
				text : biolims.common.editwindow,
				action : function() {
					editItemLayer($("#main"))
				}
			});

//			tbarOpts.push({
//				text : biolims.common.delSelected,
//				action : function() {
//					removeChecked($("#main"),
//							"/sample/sampleReceive/delSampleReceiveItem.action");
//				}
//			});
			tbarOpts.push({
				text : '<i class="fa fa-h-square"></i> '
						+ biolims.common.selectProducts,
				action : detectionProject,
			});
			tbarOpts.push({
				text : '<i class="fa fa-yelp"></i> ' + "样本名称",
				action : sampleType,
			});
			tbarOpts.push({
				text : '<i class="fa fa-paypal"></i> ' + biolims.common.nextFlow,
				action : function() {
					nextFlow();
				}
			});

			tbarOpts.push({
				text : '<i class="fa fa-ioxhost"></i> '
						+ biolims.common.batchResult,
				className : 'btn btn-sm btn-success resultsBatchBtn',
			});
			tbarOpts.push({
				text : '打印接收单',
				action : function() {
					dayin();
				}
			});
			tbarOpts.push({
				text : '病人订单',
				action : function() {
					skip();
				}
			});
//			tbarOpts.push({
//				text: "打印生产条码",
//				action: function() {
//					downCsvProduct()
//				}
//			});
//			tbarOpts.push({
//				text: "打印质检条码",
//				action: function() {
//					downCsv()
//				}
//			});
		}
	}else{
		tbarOpts.push({
			text : '打印接收单',
			action : function() {
				dayin();
			}
		});
//		tbarOpts.push({
//			text: "打印生产条码",
//			action: function() {
//				downCsvProduct()
//			}
//		});
//		tbarOpts.push({
//			text: "打印质检条码",
//			action: function() {
//				downCsv()
//			}
//		});
	}
	console.log(colOpts)
    var options;
	options = table(true, $("#sampleReveice_id").text(),
			"/sample/sampleReceive/showSampleReceiveItemTableJson.action",
			colOpts, tbarOpts);
	myTable = renderData($("#main"), options);
	myTable.on('draw', function() {
		oldChangeLog = myTable.ajax.json();
	});
}


// 点击病人订单跳转
function skip() {
	var orderId = document.getElementById('sampleReceive_sampleOrder').value;
	window.open("/system/sample/sampleOrder/editSampleOrder.action?id="
			+ orderId);
}
function xianshisaomiao() {
	$("#sampleReceiveModal").modal("show");
	// 选择临床样本或者科研样本
	choseClinicalOrScientific();
	$("#modal-body .col-xs-12").hide();
	$("#modal-body #scancode").show();
	scanCode();
}
function dayin() {
	var rows = $("#main .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("td[savename='barCode']").text());
	});
	
//	if( $("#sampleReceive_confirmDate").text()==""){
//		top.layer.msg("样本接收单没有保存！，不能打印！");
//		return false;
//	}
	$.ajax({
		type : "post",
		data : {
			id : $("#sampleReveice_id").text(),
			confirmDate : $("#sampleReceive_acceptDate").val(),
			modelId:"ybjslb"
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirt.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				
				
				var id = ids.join('');
				var url = '__report='+data.reportN+'&id=' + id;
				commonPrint(url);
				
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
	
	
	
	
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '_blank', '');
}
// 扫码
function scanCode() {
	$("#scanInput")
			.keypress(
					function(e) {
						var e = e || window.event;
						if (e.keyCode == "13") {
							var sampleCode = this.value;
							if (sampleCode) {
								$
										.ajax({
											type : "post",
											async : false,
											data : {
												id : $("#sampleReveice_id")
														.text(),
												sampleCode : sampleCode,
												type : $("#sampleReceiveTitle")
														.attr("type")
											},
											url : ctx
													+ "/sample/sampleReceive/scanCodeInsertpanduan.action",
											success : function(data2) {
												var data3 = JSON.parse(data2);
												if (data3.success) {
													if (data3.state) {
														if (data3.sfdqdd) {
															if (data3.jszt) {
																top.layer
																		.msg("该条形码已经接收！");
																myTable.ajax
																		.reload();
															} else {
																top.layer
																		.msg("接收成功！");
																$("#scanInput")
																		.val("");
																myTable.ajax
																		.reload();
															}
														} else {
															top.layer
																	.msg("该条形码已经在其他接样单中！");
														}
													} else {
														if (data3.sfdqdd) {
															$
																	.ajax({
																		type : "post",
																		async : false,
																		data : {
																			id : $(
																					"#sampleReveice_id")
																					.text(),
																			sampleCode : sampleCode,
																			type : $(
																					"#sampleReceiveTitle")
																					.attr(
																							"type"),
																			sj : $(
																					"#sampleReceive_acceptDate")
																					.val(),
																		},
																		url : ctx
																				+ "/sample/sampleReceive/scanCodeInsert.action",
																		success : function(
																				data4) {
																			var data = JSON
																					.parse(data4);
																			var id = data.id;
																			if(data.sorry=="2"){
																		           
																		           top.layer.msg("订单状态不是预订单！不能接收！");
																						
																					}
																			if (data.id) {
																				$("#sampleReveice_id").text(
																								data.id);
																				$("#sampleReceiveinputid").val(data.id);
																				if (data.sampleOrder) {
																					$(
																							"#sampleReceive_sampleOrder")
																							.val(
																									data.sampleOrder);
																				}
																				if (data.startBackTime) {
																					$(
																							"#sampleReceive_startBackTime")
																							.val(
																									data.startBackTime);
																				}
																				if (data.feedBackTime) {
																					$(
																							"#sampleReceive_feedBackTime")
																							.val(
																									data.feedBackTime);
																				}
																				if (data.ccoi) {
																					$("#sampleReceive_ccoi").val(data.ccoi);
																				}
																				if (data.barcode) {
																					$("#sampleReceive_barcode").val(data.barcode);
																				}
																				top.layer
																						.msg(data.prompt);
																				myTable
																						.settings()[0].ajax.data = {
																					id : id,
																				};
																				myTable.ajax
																						.reload();
																				if (data.prompt == 1) {
																					// top.layer.msg(biolims.common.saveSuccess);
																					$(
																					"#scanInput")
																							.val(
																									"");
																					top.layer
																							.msg("接收成功！");
																				} else {
																					top.layer
																							.msg(biolims.dna.noMatch);
																				}
																				;
																			} else {
																			}
																			$(
																					"#scanInput")
																					.val(
																							"");
																		}
																	});
														} else {
															top.layer
																	.msg("该条形码不在当前接样单的订单中！");
														}

													}
													;
												} else {
													top.layer.msg("条形码查重错误！");
												}
											}
										});
							}
							;
						}
						;
					});
		
}
// 上传
function uploadCsv() {
	var csvFileInput = fileInputCsv("");
	csvFileInput
			.on(
					"fileuploaded",
					function(event, data, previewId, index) {
						$
								.ajax({
									type : "post",
									data : {
										id : $("#sampleReveice_id").text(),
										type : $("#sampleReceiveTitle").attr(
												"type"),
										fileId : data.response.fileId
									},
									url : ctx
											+ "/sample/sampleReceive/uploadCsvFile.action",
									success : function(data) {
										var data = JSON.parse(data)
										if (data.success) {
											if (data.prompt != "") {
											}
											$("#maincontentframe",
													window.parent.document)[0].src = window.ctx
													+ "/sample/sampleReceive/editSampleReceive.action?id="
													+ data.id;
											myTable.ajax.reload();
										} else {
											top.layer
													.msg(biolims.common.uploadFailed)
										}
									}
								});
					});
}
// 检测项目
function detectionProject() {
	var rows = $("#main .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title : biolims.crm.selectProduct,
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/com/biolims/system/product/showProductSelTree.action",
		yes : function(index, layer) {
			var name = [], id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#mytreeGrid .chosed").each(function(i, v) {
						name.push($(v).children("td").eq(1).text());
						id.push($(v).children("td").eq(0).text());
					});
			rows.addClass("editagain");
			rows.find("td[savename='productName']").attr("productId",
					id.join(",")).text(name.join(","));
			rows.find("td[savename='productId']").text(id.join(","));
			top.layer.close(index)
		},
		cancel : function(index, layer) {
			top.layer.close(index)
		}
	})
}
// 样本类型
function sampleType() {
	var rows = $("#main .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer
			.open({
				title : "选择样本名称",
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/sample/dicSampleType/selectDicSampleTypeOne.action",
						'' ],
				yes : function(index, layer) {
					var type = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(1)
							.text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(0)
							.text();
					rows.addClass("editagain");
					rows.find("td[savename='dicSampleType-name']").attr(
							"dicSampleType-id", id).text(type);
					rows.find("td[savename='dicSampleType-id']").text(id);

					top.layer.close(index)
				},
			})

}
// 送检医院
function sendHospital() {
	var rows = $("#main .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer
			.open({
				title : biolims.sample.hospital,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/crm/customer/customer/crmCustomerSelectTable.action",
						'' ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addcrmCustomerTable .chosed").children("td")
							.eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addcrmCustomerTable .chosed").children("td")
							.eq(0).text();

					rows.find("td[savename='crmCustomer-name']").attr(
							"crmCustomer-id", id).text(name);
					rows.find("td[savename='crmCustomer-id']").text(id);
					top.layer.close(index)
				},
			})
}
// 样本数量
function sampleNum() {
	var rows = $("#main .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var num = 1;
	top.layer
			.open({
				title : biolims.common.sampleNum,
				type : 1,
				area : [ "30%", "30%" ],
				btn : biolims.common.selected,
				btnAlign : 'c',
				content : ' <div class="input-group"><span class="input-group-btn"><button class="btn btn-default" type="button" id="subSampleNum"> - </button></span><input type="text" value="1" class="form-control" id="sampleNum"><span class="input-group-btn"><button class="btn btn-default" id="addSampleNum" type="button"> + </button></span></div>',
				success : function() {
					console.log($("#subSampleNum", parent.document));
					$("#subSampleNum", parent.document).click(
							function() {
								num = parseInt($("#sampleNum", parent.document)
										.val()) - 1;
								$("#sampleNum", parent.document).val(num);
							});
					$("#addSampleNum", parent.document).click(
							function() {
								num = parseInt($("#sampleNum", parent.document)
										.val()) + 1;
								$("#sampleNum", parent.document).val(num);
							});
				},
				yes : function(index, layer) {
					rows.addClass("editagain");
					rows.find("td[savename='sampleNum']").text(num);
					top.layer.close(index)
				},
			})

}
// 批量单位组
function unitBatch() {
	var rows = $("#main .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer
			.open({
				title : biolims.common.batchUnit,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/com/biolims/system/work/unitGroupNew/showWorkTypeDialogList.action",
						'' ],
				yes : function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addUnitGroup .chosed")
							.children("td").eq(0).text();
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addUnitGroup .chosed")
							.children("td").eq(1).text();
					/*
					 * var cycle = $('.layui-layer-iframe',
					 * parent.document).find("iframe").contents().find("#addUnitGroup
					 * .chosed").children("td") .eq(2).text();
					 */
					var mark2Name = $('.layui-layer-iframe', parent.document)
							.find("iframe").contents().find(
									"#addUnitGroup .chosed").children("td").eq(
									4).text();
					rows.addClass("editagain");
					rows.find("td[savename='unitGroup-name']").attr(
							"unitGroup-id", id);
					rows.find("td[savename='unitGroup-name']").text(name);
					rows.find("td[savename='unit']").text(mark2Name);
					top.layer.close(index)
				},
			})
}
// 下一步流向
function nextFlow() {
	var rows = $("#main .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	// 判断先选产品
	var productIds = "";
	var sampleType = "";
	$.each(rows, function(j, k) {
		productId = $(k).find("td[savename='productName']").attr("productId");
		sampleType = $(k).find("td[savename='dicSampleType-id']").text();
	});
	if (!productId) {
		top.layer.msg(biolims.common.pleaseSelectProduct);
		return false;
	}
	top.layer
			.open({
				title : biolims.common.selectNextFlow,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=SampleReceive&productId="
								+ productId + "&sampleType=" + sampleType, '' ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addNextFlow .chosed")
							.children("td").eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#addNextFlow .chosed")
							.children("td").eq(0).text();
					rows.addClass("editagain");
					rows.find("td[savename='nextFlow']").attr("nextFlowId", id)
							.text(name);
					rows.find("td[savename='nextFlowId']").text(id);
					top.layer.close(index)
				},
			})
}
// 保存
function save() {
	
	
	
	var flag = true;
	
	var tabledata = saveItemjson($("#main"));
	var requiredField = requiredFilter();
	if (!requiredField) {
		flag = false;
		return false;
	}
	if($("#sampleReceive_approvalUser_name").val()==$("#sampleReceive_acceptUser").text()){
		top.layer.msg(("创建人与审核人相同请重新选择"));
		return false;
	}
 
	if($("#sampleReceive_productionUser_name").val()==$("#sampleReceive_acceptUser").text()){
		top.layer.msg(("创建人与审核人相同请重新选择"));
		return false;
	}
	if($("#sampleReceive_qualityCheckUser_name").val()==$("#sampleReceive_acceptUser").text()){
		top.layer.msg(("创建人与审核人相同请重新选择"));
		return false;
	}
	/**
	 * 限制数字,小数
	 */
	var reg = /^\d+(\.\d{1,2})?$/;
	//运送设备温度
	if(!reg.test($("#sampleReceive_temperature").val()) && $("#sampleReceive_temperature").val()!=""){
		top.layer.msg("请输入正确的运送设备温度(数字,小数)!");
		return false;
	}
	//开启温度
	if(!reg.test($("#sampleReceive_lowTemperature").val()) && $("#sampleReceive_lowTemperature").val()!=""){
		top.layer.msg("请输入正确的开启温度(数字,小数)!");
		return false;
	}
	//结束温度
	if(!reg.test($("#sampleReceive_highTemperature").val()) && $("#sampleReceive_highTemperature").val()!=""){
		top.layer.msg("请输入正确的结束温度(数字,小数)!");
		return false;
	}
	//平均温度
	if(!reg.test($("#sampleReceive_avgTemperature").val()) && $("#sampleReceive_avgTemperature").val()!=""){
		top.layer.msg("请输入正确的平均温度(数字,小数)!");
		return false;
	}
	
	/**
	 * 数字,字母
	 */
	//快递单号
	var reg1 = /^\w+$/;
	if(!reg1.test($("#sampleReceive_expressNum").val()) && $("#sampleReceive_expressNum").val()!=""){
		top.layer.msg("请输入正确的快递单号(数字,字母)!");
		return false;
	}
	//温度记录仪编号
	if(!reg1.test($("#sampleReceive_recorder").val()) && $("#sampleReceive_recorder").val()!=""){
		top.layer.msg("请输入正确的温度记录仪编号(数字,字母)!");
		return false;
	}
	
	//血量
	var xl = /^[0-9]*$/;
	var xl1 = true
	$("#main tbody tr").each(function(i,val){
    	var tds = $(val).find("td[savename='bloodVolume']").text();
    	if(!xl.test(tds) && tds!=""){
    		top.layer.msg("请输入正确的血量(数字)!");
    		xl1 = false;
    		return false;
    	}
    });
	if(xl1 == false){
		top.layer.msg("请输入正确的血量(数字)!");
		return false;
	}
	
	var jsonn = {};
	
	// 判断开始时间是否小于结束时间 
    var sDate = new Date($("#sampleReceive_startBackTime").val().replace("/\-/g", "\/"));
    var eDate = new Date($("#sampleReceive_feedBackTime").val().replace("/\-/g", "\/"));
    if(sDate > eDate) {
      flag = false;
      top.layer.msg("结束日期不能小于开始日期！");
      return false;
    }
    var jsDate = new Date($("#sampleReceive_acceptDate").val()).getTime();
    var arr = [];
    $("#main tbody tr").each(function(i,val){
    	var tds = $(val).find("td[savename='samplingDate']").text();
    	arr.push(tds);
    });
   for(var i = 0; i<arr.length;i++){
	  var cxDate = new Date(arr[i]).getTime();
	  if(cxDate-jsDate>28800000){
		  top.layer.msg("采血时间和接收日期中间差值大于八小时！");
		  return false;
	  }
   }
   
   //判断是否全部接收
   debugger
   var cxg = true;
   $("#main tbody tr").each(function(i,v){
		var tds = $(v).find("td[savename='receiveState']").text();
		if(tds!="是"){
			cxg = false;
		}
	})
   if(cxg==false){
	    top.layer.msg("采血管请全部接收完后再保存!");
		return false;
   }
   
    var trss = $("#main").find("tbody tr");
  	trss.each(function(i, val) {
  		var json = {};
  		var tds = $(val).children("td");
  		for (var j = 1; j < tds.length; j++) {
  			var k = $(tds[j]).attr("savename");
  			json[k] = $(tds[j]).text();
  			if (k == "nextFlow") {
  				console.log(json[k]);
  				if (!json[k]) {
  					flag = false;
  					top.layer.msg("请选择下一步流向！");
  					return false;
  				}
  			}
  			
  			if (k == "method") {
  				console.log(json[k]);
  				if (!json[k]) {
  					flag = false;
  					top.layer.msg("请选择结果！");
  					return false;
  				}
  			}
  			
  			if (k == "dicSampleType-name") {
  				console.log(json[k]);
  				if (!json[k]) {
  					flag = false;
  					top.layer.msg("请选择样本名称！");
  					return false;
  				}
  			}
  			
  			
  		}
  	});
  	if(flag){
  		$("#form1 input").each(function(i, v) {
  			var k = v.name;
  			jsonn[k] = v.value;
  		});
  		$("#form1 select").each(function(i, v) {
  			var k = v.name;
  			jsonn[k] =$(v).val();
  		});
  		
  		jsonn["type"] = $("#sampleReceiveTitle").attr("type");
  		console.log(JSON.stringify(jsonn))
  		
  		var datas = saveItemjson($("#main"));
  		var changeLogItem = "样本接收明细：";
  		changeLogItem = getChangeLog(datas, $("#main"), changeLogItem);
  		var changeLogItemLast = "";
  		if (changeLogItem != "样本接收明细：") {
  			changeLogItemLast = changeLogItem
  		}
  		var changeLog = "样本接收：";
  		$('input[class="form-control"]').each(
  				function(i, v) {
  					var valnew = $(v).val();
  					var val = $(v).attr("changelog");
  					if (val !== valnew) {
  						changeLog += $(v).prev("span").text() + ':由"' + val
  								+ '"变为"' + valnew + '";';
  					}
  				});
  		var changeLogs = "";
  		if(changeLog != "样本接收：") {
  			changeLogs = changeLog;
  			$("#changelog").val(changeLogs);
  		}
  		top.layer.load(4, {
  			shade : 0.3
  		});
  		var data = {
  	  			main : JSON.stringify(jsonn),
  	  			itemJson : tabledata,
  	  			changeLog:changeLog,
  	  			changeLogItem:changeLogItemLast,
  	  		};
  		$.ajax({
  			type : "post",
  			url : ctx + "/sample/sampleReceive/saveSampleReceive.action?ids="
  					+ $("#sampleReveice_id").text(),
  			data : data,
  			async : false,
  			success : function(data) {
  				var data = JSON.parse(data);
  				if (data.success) {
  					top.layer.closeAll();
  					top.layer.msg(biolims.common.saveSuccess);
  					$("#sampleReveice_id").text(data.id);
  					$("#sampleReceiveinputid").val(data.id);
//  					myTable.settings()[0].ajax.data = {
//  						"id" : data.id
//  					};
//  					myTable.ajax.reload();
  					window.location=window.ctx+'/sample/sampleReceive/editSampleReceive.action?id=' + $("#sampleReveice_id").text()+"&bpmTaskId="+$("#bpmTaskId").val();
  				}
  			}
  		});
  		
//  		window.location.reload();
  	}else{
  		
  	}
	
	
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag = true;
	
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");

			
			
			
			// 判断是否正常并转换为数字
//			if (k == "isGood") {
//				var isGood = $(tds[j]).text();
//				if (isGood == biolims.common.yes) {
//					json[k] = "1";
//				} else if (isGood == biolims.common.no) {
//					json[k] = "0";
//				} else {
//					json[k] = "";
//				}
//				continue;
//			}
			if (k == "receiveState") {
				var receiveState = $(tds[j]).text();
				if (receiveState == "是") {
					json[k] = "1";
				} else if (receiveState == "否") {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			
			
			if (k == "productName") {
				json["productId"] = $(tds[j]).attr("productId");
			}
//			if (k == "crmCustomer-name") {
//				json["crmCustomer-id"] = $(tds[j]).attr("crmCustomer-id");
//				continue;
//			}
			
			
//			if (k == "unitGroup-name") {
//				json["unitGroup-id"] = $(tds[j]).attr("unitGroup-id");
//				continue;
//			}

			
			
			if($("#sampleReceive_qualityCheckUser_id").val()!=sessionStorage.getItem("userId")){
				if (k == "barCode") {
					json["sampleCode"] = $(tds[j]).attr("sampleCode");
					json["sampleNum"] = $(tds[j]).attr("sampleNum");
					json["orderCode"] = $(tds[j]).attr("orderCode");
					json["bloodVolume"] = $(tds[j]).attr("bloodVolume");
					json["bloodColourTest"] = $(tds[j]).attr("bloodColourTest");
					json["bloodColourNotes"] = $(tds[j]).attr("bloodColourNotes");
					json["coagulationTest"] = $(tds[j]).attr("coagulationTest");
					json["coagulationNotes"] = $(tds[j]).attr("coagulationNotes");
					json["sealingBloodVessels"] = $(tds[j]).attr("sealingBloodVessels");
					json["sealingBloodNotes"] = $(tds[j]).attr("sealingBloodNotes");
					json["bloodVesselTime"] = $(tds[j]).attr("bloodVesselTime");
					json["bloodTime"] = $(tds[j]).attr("bloodTime");
					json["other"] = $(tds[j]).attr("other");
					json["method"] = $(tds[j]).attr("method");
					json["otherInformationNotes"] = $(tds[j]).attr("otherInformationNotes");
					json["nextFlow"] = $(tds[j]).attr("nextFlow");
					json["nextFlowId"] = $(tds[j]).attr("nextFlowId");
					json["note"] = $(tds[j]).attr("note");

				
				}
				// 判断男女并转换为数字
				if (k == "gender") {
					var gender = $(tds[j]).text();
					if (gender == biolims.common.male) {
						json[k] = "1";
					} else if (gender == biolims.common.female) {
						json[k] = "0";
					} else if (gender == biolims.common.unknown) {
						json[k] = "3";
					} else {
						json[k] = "";
					}
					continue;
				}
				// 添加样本类型ID
				if (k == "dicSampleType-name") {
					json["dicSampleType-id"] = $(tds[j]).attr("dicSampleType-id");
					continue;
				}
			}
			if($("#sampleReceive_qualityCheckUser_id").val()==sessionStorage.getItem("userId")){
				if (k == "barCode") {
					json["sampleCode"] = $(tds[j]).attr("sampleCode");
					json["sampleNum"] = $(tds[j]).attr("sampleNum");
					json["orderCode"] = $(tds[j]).attr("orderCode");
					if($("#sampleReceive_state").text()=="已下达"){
						json["gender"] = $(tds[j]).attr("gender");
						json["patientName"] = $(tds[j]).attr("patientName");
						json["abbreviation"] = $(tds[j]).attr("abbreviation");
						
						json["dicSampleType-id"] = $(tds[j]).attr("dicSampleType-id");
						json["samplingDate"] = $(tds[j]).attr("samplingDate");
						json["EMRId"] = $(tds[j]).attr("EMRId");
					}else{
						json["bloodVolume"] = $(tds[j]).attr("bloodVolume");
						json["bloodColourTest"] = $(tds[j]).attr("bloodColourTest");
						json["bloodColourNotes"] = $(tds[j]).attr("bloodColourNotes");
						json["coagulationTest"] = $(tds[j]).attr("coagulationTest");
						json["coagulationNotes"] = $(tds[j]).attr("coagulationNotes");
						json["sealingBloodVessels"] = $(tds[j]).attr("sealingBloodVessels");
						json["sealingBloodNotes"] = $(tds[j]).attr("sealingBloodNotes");
						json["bloodVesselTime"] = $(tds[j]).attr("bloodVesselTime");
						json["bloodTime"] = $(tds[j]).attr("bloodTime");
						json["other"] = $(tds[j]).attr("other");
						json["method"] = $(tds[j]).attr("method");
						json["otherInformationNotes"] = $(tds[j]).attr("otherInformationNotes");
//						json["nextFlow"] = $(tds[j]).attr("nextFlow");
						json["note"] = $(tds[j]).attr("note");
					}
				}
				if (k == "nextFlow") {
					json["nextFlowId"] = $(tds[j]).attr("nextFlowId");
				}
				

				if (k == "method") {
					var method = $(tds[j]).text();
					if (method == biolims.common.qualified) {
						json[k] = "1";
					} else if (method == biolims.common.disqualified) {
						json[k] = "0";
					} else if (method == "让步放行") {
						json[k] = "2";
					} else if(method == "备用空管") {
						json[k] = "3";
					}else{
						json[k] = ""
					}
					continue;
				}
				if (k == "bloodColourTest") {
					var bloodColourTest = $(tds[j]).text();
					if (bloodColourTest == "") {
						json[k] = "0";
					} else if (bloodColourTest == "鲜红") {
						json[k] = "1";
					} else if (bloodColourTest == "暗红") {
						json[k] = "2";
					} else {
						json[k] = "";
					}
					continue;
				}
			}
//			if (k == "barCode") {
//				json["sampleCode"] = $(tds[j]).attr("sampleCode");
//				json["sampleNum"] = $(tds[j]).attr("sampleNum");
//				json["orderCode"] = $(tds[j]).attr("orderCode");
//				if($("#sampleReceive_qualityCheckUser_id").val()!=sessionStorage.getItem("userId")){
//					json["bloodVolume"] = $(tds[j]).attr("bloodVolume");
//					json["bloodColourTest"] = $(tds[j]).attr("bloodColourTest");
//					json["bloodColourNotes"] = $(tds[j]).attr("bloodColourNotes");
//					json["coagulationTest"] = $(tds[j]).attr("coagulationTest");
//					json["coagulationNotes"] = $(tds[j]).attr("coagulationNotes");
//					json["sealingBloodVessels"] = $(tds[j]).attr("sealingBloodVessels");
//					json["sealingBloodNotes"] = $(tds[j]).attr("sealingBloodNotes");
//					json["bloodVesselTime"] = $(tds[j]).attr("bloodVesselTime");
//					json["bloodTime"] = $(tds[j]).attr("bloodTime");
//					json["other"] = $(tds[j]).attr("other");
//					json["method"] = $(tds[j]).attr("method");
//					json["otherInformationNotes"] = $(tds[j]).attr("otherInformationNotes");
//					json["nextFlow"] = $(tds[j]).attr("nextFlow");
//					json["note"] = $(tds[j]).attr("note");
//
//				
//				}
//
//				if($("#sampleReceive_qualityCheckUser_id").val()==sessionStorage.getItem("userId")){
//					if($("#sampleReceive_state").text()=="已下达"){
//						json["gender"] = $(tds[j]).attr("gender");
//						json["patientName"] = $(tds[j]).attr("patientName");
//						json["dicSampleType-id"] = $(tds[j]).attr("dicSampleType-id");
//						json["samplingDate"] = $(tds[j]).attr("samplingDate");
//						json["EMRId"] = $(tds[j]).attr("EMRId");
//					}else{
//						json["bloodVolume"] = $(tds[j]).attr("bloodVolume");
//						json["bloodColourTest"] = $(tds[j]).attr("bloodColourTest");
//						json["bloodColourNotes"] = $(tds[j]).attr("bloodColourNotes");
//						json["coagulationTest"] = $(tds[j]).attr("coagulationTest");
//						json["coagulationNotes"] = $(tds[j]).attr("coagulationNotes");
//						json["sealingBloodVessels"] = $(tds[j]).attr("sealingBloodVessels");
//						json["sealingBloodNotes"] = $(tds[j]).attr("sealingBloodNotes");
//						json["bloodVesselTime"] = $(tds[j]).attr("bloodVesselTime");
//						json["bloodTime"] = $(tds[j]).attr("bloodTime");
//						json["other"] = $(tds[j]).attr("other");
//						json["method"] = $(tds[j]).attr("method");
//						json["otherInformationNotes"] = $(tds[j]).attr("otherInformationNotes");
//						json["nextFlow"] = $(tds[j]).attr("nextFlow");
//						json["note"] = $(tds[j]).attr("note");
//					}
//						
//
//					
//				}
//				
//				
//			}
		
			

			json[k] = $(tds[j]).text();

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
/*
 * 创建者 : dwb 创建日期: 2018-05-10 17:13:12 文件描述: 修改日志 保存增加日志
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

function add() {
	window.location = window.ctx
			+ "/sample/sampleReceive/editSampleReceive.action?type="
			+ $("#type2").val();
}

function list() {
	window.location = window.ctx
			+ '/sample/sampleReceive/showSampleReceiveTable.action?type='
			+ $("#type2").val();
}
// 提交审批
//乐观锁
var functionLock = true;
//乐观锁确定弹窗
var functionLockQueding = true;
//乐观锁提交最后的确定
var functionLockQuedingEnd = true;
function tjsp() {
	// var mustField = mustAddField();
	// if (!mustField) {
	// return false;
	// }
	if(functionLock){
		functionLock = false;
		 if($("#form1").data("changed")){ 
			    top.layer.msg("请保存后提交");
			    return false;
			   }
		 
		 var cxg = true;
		   $("#main tbody tr").each(function(i,v){
				var tds = $(v).find("td[savename='receiveState']").text();
				if(tds!="是"){
					cxg = false;
				}
			})
		   if(cxg==false){
			    top.layer.msg("采血管请全部接收完后在保存!");
				return false;
		   }
		 
		 
		 if($("#sampleReceive_approvalUser_name").val()==$("#sampleReceive_acceptUser").text()){
				top.layer.msg(("创建人与审核人相同请重新选择"));
				functionLock = true;
				return false;
			}
		 
		 if($("#sampleReceive_productionUser_name").val()==$("#sampleReceive_acceptUser").text()){
				top.layer.msg(("创建人与审核人相同请重新选择"));
				functionLock = true;
				return false;
			}
		 
		 if($("#sampleReceive_qualityCheckUser_name").val()==$("#sampleReceive_acceptUser").text()){
				top.layer.msg(("创建人与审核人相同请重新选择"));
				functionLock = true;
				return false;
			}
	 
			if ($("#sampleReceive_approvalUser_id").val() == ""
				|| $("#sampleReceive_approvalUser_id").val() == null) {
			top.layer.msg("请选择QA审核人并保存！");
			functionLock = true;
			return false;
		}
		$.ajax({
			type : "post",
			url : "/sample/sampleReceive/changeSampleOrderState.action",
			async : false,
			data : {
				id : $("#sampleReveice_id").text(),
			},
			success : function(dataBack) {
				functionLock = true;
				var data = JSON.parse(dataBack);
				if (data.success) {
					top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit,
							{
								icon : 3,
								title : biolims.common.prompt,
								btn : biolims.common.selected
							}, function(index1) {
								splc();
								top.layer.close(index1);
							});
				}
			}
		})
	
	
		
	}else{
		top.layer.msg("请勿连续点击！");
	}
	

	// top.layer.open({
	// title : biolims.sample.pleaseSelectReviewer,
	// type : 2,
	// area : [ "650px", "400px" ],
	// btn : biolims.common.selected,
	// content : [
	// window.ctx
	// + "/core/user/selectUserTable.action?groupId=admin",
	// '' ],
	// yes : function(index, layer) {
	// var name = $('.layui-layer-iframe',
	// parent.document).find("iframe")
	// .contents()
	// .find("#addUserTable .chosed")
	// .children("td").eq(1).text();
	// var id = $('.layui-layer-iframe',
	// parent.document).find("iframe")
	// .contents()
	// .find("#addUserTable .chosed")
	// .children("td").eq(0).text();
	// $("#confirmUserName").text(name);
	// $("#confirmUserId").val(id);
	// save();
	// top.layer.close(index);
	// },
	// end : function() {
	// splc();
	// }
	// })

}
// 审批流程图
function splc() {
	if(functionLockQueding){
		functionLockQueding=false;
		top.layer
		.open({
			title : biolims.common.approvalProcess,
			type : 2,
			anim : 2,
			area : [ '800px', '500px' ],
			btn : biolims.common.selected,
			content : window.ctx
					+ "/workflow/processinstance/toStartView.action?formName=SampleReceive",
			yes : function(index2, layer) {
				functionLockQueding=true;
				
				if(functionLockQuedingEnd){
					functionLockQuedingEnd = false;
					var datas = {
							userId : userId,
							userName : userName,
							formId : $("#sampleReveice_id").text(),
							title : $("#sampleReceive_name").val(),
							formName : "SampleReceive"
						}
						ajax(
								"post",
								"/workflow/processinstance/start.action",
								datas,
								function(data) {
									if (data.success) {
										functionLockQuedingEnd = true;
										top.layer.msg(biolims.common.submitSuccess);
										window.open(window.ctx
												+ "/main/toPortal.action",
												'_parent');
										top.layer.close(index);
										if (typeof callback == 'function') {
											callback(data);
										}
									} else {
										functionLockQuedingEnd = true;
										top.layer.msg(biolims.common.submitFail);
									}
								}, null);
				}else{
					top.layer.msg("请勿重复点击");
				}
				
			},
			cancel: function(){
				functionLockQueding=true;
				top.layer.closeAll();
			}

		});
	}else{
		top.layer.msg("请勿重复点击");
	}
}

function sp() {
	if ($("#sampleReceive_productionUser_id").val() == ""
			|| $("#sampleReceive_productionUser_id").val() == null) {
		top.layer.msg("请选择生产审核人!");
		return false;
	} else {
		$.ajax({
					type : "post",
					url : "/sample/sampleReceive/saveProductionUser.action",
					async : false,
					data : {
						id : $("#sampleReveice_id").text(),
						userId : $("#sampleReceive_productionUser_id").val(),
					},
					success : function(dataBack) {
						var data = JSON.parse(dataBack);
						if (data.success) {
							var taskId = $("#bpmTaskId").val();
							var formId = $("#sampleReveice_id").text();
							top.layer
									.open({
										title : biolims.common.approvalProcess,
										type : 2,
										anim : 2,
										shade: 0.3,
										area : [ '800px', '500px' ],
										btn : biolims.common.selected,
										content : window.ctx
												+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
												+ taskId + "&formId=" + formId,
										yes : function(index, layer) {
											var operVal = $(
													'.layui-layer-iframe',
													parent.document).find(
													"iframe").contents().find(
													"#oper").val();
											var opinionVal = $(
													'.layui-layer-iframe',
													parent.document).find(
													"iframe").contents().find(
													"#opinionVal").val();
											var opinion = $(
													'.layui-layer-iframe',
													parent.document).find(
													"iframe").contents().find(
													"#opinion").val();
											if (!operVal) {
												top.layer
														.msg(biolims.common.pleaseSelectOper);
												return false;
											}
											if (operVal == "2") {
												_trunTodoTask(taskId, callback,
														dialogWin);
											} else {
												var paramData = {};
												paramData.oper = operVal;
												paramData.info = opinion;

												var reqData = {
													data : JSON
															.stringify(paramData),
													formId : formId,
													taskId : taskId,
													userId : window.userId
												}
												ajax(
														"post",
														"/workflow/processinstance/completeTask.action",
														reqData,
														function(data) {
															if (data.success) {
																$
																		.ajax({
																			type : "post",
																			url : window.ctx
																					+ "/sample/sampleReceive/changeSampleOrderReceive.action",
																			async : false,
																			data : {
																				zid : formId,
																			},
																			success : function(
																					dataBack) {
																				var data = JSON
																						.parse(dataBack);
																			}
																		});
																top.layer
																		.msg(biolims.common.submitSuccess);
																if (typeof callback == 'function') {
																}
															} else {
																top.layer
																		.msg(biolims.common.submitFail);
															}
														}, null);
											}
											top.layer.closeAll();
											location.href = window.ctx
													+ "/lims/pages/dashboard/dashboard.jsp";
										}
									});
						}
					}
				})
	}
}
// 送检医院与主治医生添加关联性
function showDoctors() {

	var sampleReceive_crmCustomer_id = $("#sampleReceive_crmCustomer_id").val();
	if (sampleReceive_crmCustomer_id == null
			|| sampleReceive_crmCustomer_id == "") {
		top.layer.msg(biolims.common.pleaseSelectKH);
		return

	}

	top.layer
			.open({
				title : biolims.master.selectDoctor,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/crm/doctor/showDialogCrmPatientTable.action",
						'' ],
				yes : function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addcrmPatientTable .chosed").children("td").eq(0)
							.text();
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addcrmPatientTable .chosed").children("td").eq(1)
							.text();
					top.layer.close(index)
					$("#sampleReceive_crmDoctor_id").val(id)
					$("#sampleReceive_crmDoctor_name").val(name)
				},
			})
}
// 医疗机构
function showHos() {
	top.layer
			.open({
				title : biolims.common.selectedDoctor,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/crm/customer/customer/crmCustomerSelectTable.action",
						'' ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addcrmCustomerTable .chosed").children("td")
							.eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addcrmCustomerTable .chosed").children("td")
							.eq(0).text();
					top.layer.close(index)
					$("#sampleReceive_crmCustomer_id").val(id)
					$("#sampleReceive_crmCustomer_name").val(name)
				},
			})
}
// 运输方式
function yunshufs() {
	$("#form1").data("changed",true); 
	top.layer.open({
		title : biolims.order.ysfs,
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/dic/type/dicTypeSelectTable.action?flag=ysfs",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addDicTypeTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addDicTypeTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index)
			$("#sampleReceive_ysfs_id").val(id)
			$("#sampleReceive_ysfs_name").val(name)
			if($("#sampleReceive_ysfs_name").val()=="常温"){
				$("#sampleReceive_temperature").val ("15-25℃")
			}else if($("#sampleReceive_ysfs_name").val()=="蓝冰"){
				$("#sampleReceive_temperature").val ("2-8℃");
			}else{
				$("#sampleReceive_temperature").val ("");
			}
		},
	})
}
// 选择快递公司
function showexpressCompany() {
	$("#form1").data("changed",true); 
	top.layer
			.open({
				title : biolims.goods.selectExpress,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/system/express/expressCompany/expressCompanySelectTable.action",
						'' ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addExpressCompanyTable .chosed").children("td")
							.eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addExpressCompanyTable .chosed").children("td")
							.eq(0).text();
					top.layer.close(index)
					$("#sampleReceive_expressCompany_id").val(id)
					$("#sampleReceive_expressCompany_name").val(name)
				},
			})
}
// 选择合同
function showProject() {
	top.layer.open({
		title : biolims.crm.selectContract,
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [ window.ctx + "/crm/project/showProject.action", '' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addProject .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addProject .chosed").children("td")
					.eq(0).text();
			top.layer.close(index)
			$("#sampleReceive_project_id").val(id)
			$("#sampleReceive_project_name").val(name)
		},
	})
}
// 上传附件
function fileUp() {
	if ($("#sampleReveice_id").text() == "NEW") {
		top.layer.msg(biolims.common.pleaseSave)
		return false;
	}
	$("#uploadFile").modal("show");
}
// 查看附件
function fileView() {
	top.layer
			.open({
				title : biolims.common.attachment,
				type : 2,
				skin : 'layui-top.layer-lan',
				area : [ "650px", "400px" ],
				content : window.ctx
						+ "/operfile/initFileList.action?flag=1&modelType=sampleReceive&id="
						+ $("#sampleReveice_id").text(),
				cancel : function(index, layer) {
					top.layer.close(index)
				}
			})
}
// 改变状态
function changeState() {
	if($("#form1").data("changed")){ 
		top.layer.msg("请保存后完成");
		return false;
	}
//	var mustField = mustAddField();
//	if (!mustField) {
//		return false;
//	}
	var id = $("#sampleReveice_id").text()
	var paraStr = "formId=" + id + "&tableId=SampleReceive";
	console.log(paraStr)
	top.layer
			.open({
				title : biolims.common.approvalProcess,
				type : 2,
				anim : 2,
				area : [ '400px', '400px' ],
				btn : biolims.common.selected,
				content : window.ctx
						+ "/applicationTypeAction/applicationTypeActionLook.action?"
						+ paraStr + "&flag=changeState'",
				yes : function(indexx, layer) {
					top.layer
							.confirm(
									biolims.common.approve,
									{
										icon : 3,
										title : biolims.common.prompt,
										btn : biolims.common.selected
									},
									function(index) {
										ajax(
												"post",
												"/applicationTypeAction/exeFun.action",
												{
													applicationTypeActionId : $(
															'.layui-layer-iframe',
															parent.document)
															.find("iframe")
															.contents()
															.find(
																	"input:checked")
															.val(),
													formId : id
												},
												function(response) {
													var respText = response.message;
													$
															.ajax({
																type : "post",
																url : window.ctx
																		+ "/sample/sampleReceive/changeSampleOrderReceive.action",
																async : false,
																data : {
																	zid : id,
																},
																success : function(
																		dataBack) {
																	var data = JSON
																			.parse(dataBack);
																}
															});
													if (respText == '') {
														window.location
																.reload();
														top.layer.closeAll()

													} else {
													
														top.layer.msg(respText);
														window.setTimeout ('test()',1000)
													}
												}, null)
//										top.layer.closeAll();
									})
				},

			});
}
function test(){
	top.layer.closeAll()
}
// 复制行,只能复制一行
function copyItem(ele) {
	var rows = ele.find(".selected");
	var length = rows.length;
	if (length == 1) {
		var row = rows.clone(true);
		row.find("td").eq(1).text("");
		row.addClass("editagain")
		row.find(".icheck").iCheck('uncheck').val("");
		rows.after(row);
		checkall(ele);
	} else if (!length || length < 1) {
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
	} else {
		top.layer.msg(biolims.common.onlyOne);
	}
}

//function downCsv() {
//	var lan = $("#lan").val();
//	if (lan == "en") {
//		window.location.href = ctx
//				+ "/js/sample/uploadFileRoot/sampleReceiveItem_En.csv";
//	} else {
//		window.location.href = ctx
//				+ "/js/sample/uploadFileRoot/sampleReceiveItem.csv";
//	}
//}

//质检下载csv
function downCsv(){
	//window.location.href=ctx+"/js/experiment/enmonitor/dust/cleanAreaDustItem.csv";
	var mainTableId=$("#sampleReveice_id").text();
	if(mainTableId!=""&&mainTableId!='NEW'){
		
		var sampleId= $("#sampleReceive_sampleOrder").val();
		window.location.href=ctx+"/sample/sampleReceive/downloadCsvFileTwo.action?id="+mainTableId+"&sampleId="+sampleId;
	}else{
		top.layer.msg("请保存后再下载模板！");
	}
}
//生产下载csv
function downCsvProduct(){
	//window.location.href=ctx+"/js/experiment/enmonitor/dust/cleanAreaDustItem.csv";
	var mainTableId=$("#sampleReveice_id").text();
	if(mainTableId!=""&&mainTableId!='NEW'){
		
		var sampleId= $("#sampleReceive_sampleOrder").val();
		window.location.href=ctx+"/sample/sampleReceive/downloadCsvFile.action?id="+mainTableId+"&sampleId="+sampleId;
	}else{
		top.layer.msg("请保存后再下载模板！");
	}
}
// 必填字段验证
function mustAddField() {
	var trs = $("#main").find("tbody").children("tr");
	var flag = true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if (k == "patientName") {
				if (!json[k]) {
					flag = false;
					top.layer.msg(biolims.common.pleaseFillName);
					return false;
				}
			}
			if (k == "abbreviation") {
				if (!json[k]) {
					flag = false;
					top.layer.msg("请填写姓名缩写");
					return false;
				}
			}
			if (k == "dicSampleType-name") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg(biolims.common.pleaseSelectSampleType);
					return false;
				}
			}
			if (k == "method") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg(biolims.common.chooseTaskResult);
					return false;
				}
			}
			if (k == "nextFlow") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg(biolims.common.pleaseSelectNextFlow);
					return false;
				}
			}
//			if (k == "sampleNum") {
//				if (!json[k]) {
//					console.log(json[k]);
//					flag = false;
//					top.layer.msg("请输入样本数量");
//					return false;
//				}
//			}

		}
	});
	return flag;
}
function showApprovalUser() {
	$("#form1").data("changed",true); 
	top.layer.open({
		title : "请选择QA审核人",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=QA001",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#sampleReceive_approvalUser_id").val(id);
			$("#sampleReceive_approvalUser_name").val(name);
		},
	})
}
function showProductionUser() {
	$("#form1").data("changed",true); 
	top.layer.open({
		title : "请选择生产审核人",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=SC001",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#sampleReceive_productionUser_id").val(id);
			$("#sampleReceive_productionUser_name").val(name);
		},
	})
}


function showQualityCheckUser() {
	$("#form1").data("changed",true); 
	top.layer.open({
		title : "请选择QC通知人",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=QC001",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#sampleReceive_qualityCheckUser_id").val(id);
			$("#sampleReceive_qualityCheckUser_name").val(name);
		},
	})
}
function ck() {
	top.layer
			.open({
				title : biolims.common.checkFlowChart,
				type : 2,
				anim : 2,
				area : [ '800px', '500px' ],
				btn : biolims.common.selected,
				content : window.ctx
						+ "/workflow/processinstance/toTraceProcessInstanceView.action?formId="
						+ $("#sampleReveice_id").text(),
				yes : function(index, layero) {
					top.layer.close(index)
				},
				cancel : function(index, layero) {
					top.layer.close(index)
				}
			});
}

/**
 * 验证温度设备编号
 * @param v
 * @returns
 */
function findRecorder(v){
	var reg = /^\w+$/;
	if(!v.match(reg)) {
		top.layer.msg(("请输入正确的温度记录仪编号!"));
		return false
	}
}
/**
 * 开启温度
 * @param v
 * @returns
 */
function findLowTemperature(v){
	var reg = /^\d+(\.\d{1,2})?$/;
	if(!v.match(reg)) {
		top.layer.msg(("请输入正确的开启温度!"));
		return false
	}
}
/**
 * 结束温度
 * @param v
 * @returns
 */
function findHighTemperature(v){
	var reg = /^\d+(\.\d{1,2})?$/;
	if(!v.match(reg)) {
		top.layer.msg(("请输入正确的结束温度!"));
	}
}
/**
 * 平均温度
 * @param v
 * @returns
 */
function findAvgTemperature(v){
	var reg = /^\d+(\.\d{1,2})?$/;
	if(!v.match(reg)) {
		top.layer.msg(("请输入正确的平均温度!"));
	}
}
/**
 * 运送设备温度
 * @param v
 * @returns
 */
function findTemperature(v){
	var reg = /^\d+(\.\d{1,2})?$/;
	if(!v.match(reg)) {
		top.layer.msg(("请输入正确的运送设备温度!"));
	}
}
/**
 * 血量
 * @param v
 * @returns
 */
function findBloodVolume(that) {
	var aa = $(that).children().val()
	var reg = /^\d+(\.\d+)?$/
	if(!aa.match(reg)){
		top.layer.msg(("血量只能输入数字！"));
	}
}