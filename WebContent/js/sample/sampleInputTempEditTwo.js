﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleInputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#inputTemp_name").val());
	}else if($("#inputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}else if($("#sampleInputTemp_name").val()!="" && $("#inputTemp_name").val()!=""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}
	if($("#sampleInputTemp_productName").val()!=$("#inputTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
		$("#productNameNew").val($("#sampleInputTemp_productName").val());
	}
	if($("#sampleInputTemp_hospital").val()!=$("#inputTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleInputTemp_hospital").val());
	}
	if($("#sampleInputTemp_inHosNum").val()!=$("#inputTemp_inHosNum").val()){
		$("#inHosNum").css({"background-color":"red","color":"white"});
	}else{
		$("#inHosNumNew").val($("#sampleInputTemp_inHosNum").val());
	}
	if($("#sampleInputTemp_sendDate").val()!=$("#inputTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleInputTemp_sendDate").val());
	}
	if($("#sampleInputTemp_sampleType_id").val()!=$("#inputTemp_sampleType_id").val()){
		$("#sampleType").css({"background-color":"red","color":"white"});
	}else{
		$("#sampleTypeNew").val($("#sampleInputTemp_sampleType_name").val());
	}
	if($("#sampleInputTemp_patientName").val()!=$("#inputTemp_patientName").val()){
		$("#aabb").css({"background-color":"red","color":"white"});
	}else{
		$("#ccdd").val($("#sampleInputTemp_patientName").val());
	}
	if($("#sampleInputTemp_age").val()!=$("#inputTemp_age").val()){
		$("#age").css({"background-color":"red","color":"white"});
	}else{
		$("#ageNew").val($("#sampleInputTemp_age").val());
	}
	if($("#sampleInputTemp_weight").val()!=$("#inputTemp_weight").val()){
		$("#weight").css({"background-color":"red","color":"white"});
	}else{
		$("#weightNew").val($("#sampleInputTemp_weight").val());
	}
	if($("#sampleInputTemp_gestationalAge").val()!=$("#inputTemp_gestationalAge").val()){
		$("#gestationalAge").css({"background-color":"red","color":"white"});
	}else{
		$("#gestationalAgeNew").val($("#sampleInputTemp_gestationalAge").val());
	}
	if($("#sampleInputTemp_phoneNum").val()!=$("#inputTemp_phoneNum").val()){
		$("#phoneNum").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNumNew").val($("#sampleInputTemp_phoneNum").val());
	}
	if($("#sampleInputTemp_address").val()!=$("#inputTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleInputTemp_address").val());
	}
	if($("#sampleInputTemp_endMenstruationDate").val()!=$("#inputTemp_endMenstruationDate").val()){
		$("#endMenstruationDate").css({"background-color":"red","color":"white"});
	}else{
		$("#endMenstruationDateNew").val($("#sampleInputTemp_endMenstruationDate").val());
	}
	if($("#sampleInputTemp_parturitionTime").val()!=$("#inputTemp_parturitionTime").val()){
		$("#parturitionTime").css({"background-color":"red","color":"white"});
	}else{
		$("#parturitionTimeNew").val($("#sampleInputTemp_parturitionTime").val());
	}
	if($("#sampleInputTemp_gestationIVF").val()!=$("#inputTemp_gestationIVF").val()){
		$("#inputTempNew_gestationIVF").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_gestationIVF").val($("#sampleInputTemp_gestationIVF").val());
	}
	if($("#sampleInputTemp_badMotherhood").val()!=$("#inputTemp_badMotherhood").val()){
		$("#badMotherhood").css({"background-color":"red","color":"white"});
	}else{
		$("#badMotherhoodNew").val($("#sampleInputTemp_badMotherhood").val());
	}
	if($("#sampleInputTemp_organGrafting").val()!=$("#inputTemp_organGrafting").val()){
		$("#inputTempNew_organGrafting").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_organGrafting").val($("#sampleInputTemp_organGrafting").val());
	}
	if($("#sampleInputTemp_outTransfusion").val()!=$("#inputTemp_outTransfusion").val()){
		$("#inputTempNew_outTransfusion").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_outTransfusion").val($("#sampleInputTemp_outTransfusion").val());
	}
	if($("#sampleInputTemp_firstTransfusionDate").val()!=$("#inputTemp_firstTransfusionDate").val()){
		$("#firstTransfusionDate").css({"background-color":"red","color":"white"});
	}else{
		$("#firstTransfusionDateNew").val($("#sampleInputTemp_firstTransfusionDate").val());
	}
	if($("#sampleInputTemp_stemCellsCure").val()!=$("#inputTemp_stemCellsCure").val()){
		$("#inputTempNew_stemCellsCure").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_stemCellsCure").val($("#sampleInputTemp_stemCellsCure").val());
	}
	if($("#sampleInputTemp_immuneCure").val()!=$("#inputTemp_immuneCure").val()){
		$("#inputTempNew_immuneCure").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_immuneCure").val($("#sampleInputTemp_immuneCure").val());
	}
	if($("#sampleInputTemp_endImmuneCureDate").val()!=$("#inputTemp_endImmuneCureDate").val()){
		$("#endImmuneCureDate").css({"background-color":"red","color":"white"});
	}else{
		$("#endImmuneCureDateNew").val($("#sampleInputTemp_endImmuneCureDate").val());
	}
	if($("#sampleInputTemp_embryoType").val()!=$("#inputTemp_embryoType").val()){
		$("#inputTempNew_embryoType").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_embryoType").val($("#sampleInputTemp_embryoType").val());
	}
	if($("#sampleInputTemp_NT").val()!=$("#inputTemp_NT").val()){
		$("#NT").css({"background-color":"red","color":"white"});
	}else{
		$("#NTNew").val($("#sampleInputTemp_NT").val());
	}
	if($("#sampleInputTemp_testPattern").val()!=$("#inputTemp_testPattern").val()){
		$("#inputTempNew_testPattern").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_testPattern").val($("#sampleInputTemp_testPattern").val());
	}
	if($("#sampleInputTemp_trisome21Value").val()!=$("#inputTemp_trisome21Value").val()){
		$("#trisome21Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome21ValueNew").val($("#sampleInputTemp_trisome21Value").val());
	}
	if($("#sampleInputTemp_trisome18Value").val()!=$("#inputTemp_trisome18Value").val()){
		$("#trisome18Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome18ValueNew").val($("#sampleInputTemp_trisome18Value").val());
	}
	if($("#sampleInputTemp_coupleChromosome").val()!=$("#inputTemp_coupleChromosome").val()){
		$("#inputTempNew_coupleChromosome").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_coupleChromosome").val($("#sampleInputTemp_coupleChromosome").val());
	}
	if($("#sampleInputTemp_reason2").val()!=$("#inputTemp_reason2").val()){
		$("#reason2").css({"background-color":"red","color":"white"});
	}else{
		$("#reason2New").val($("#sampleInputTemp_reason2").val());
	}
	if($("#sampleInputTemp_diagnosis").val()!=$("#inputTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleInputTemp_diagnosis").val());
	}
	if($("#sampleInputTemp_medicalHistory").val()!=$("#inputTemp_medicalHistory").val()){
		$("#medicalHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#medicalHistoryNew").val($("#sampleInputTemp_medicalHistory").val());
	}
	if($("#sampleInputTemp_voucherType_name").val()!=$("#inputTemp_voucherType_name").val()){
		$("#voucherType").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherTypeNew").val($("#sampleInputTemp_voucherType_name").val());
		$("#voucherTypeIdNew").val($("#sampleInputTemp_voucherType_id").val());
	}
	if($("#sampleInputTemp_doctor").val()!=$("#inputTemp_doctor").val()){
		$("#doctor").css({"background-color":"red","color":"white"});
	}else{
		$("#doctorNew").val($("#sampleInputTemp_doctor").val());
	}
	if($("#sampleInputTemp_voucherCode").val()!=$("#inputTemp_voucherCode").val()){
		$("#voucherCode").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherCodeAgeNew").val($("#sampleInputTemp_voucherCode").val());
	}
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {
	//检测项目验证
	var productNameNew = $("#productNameNew").val();
	if(productNameNew==""){
		message("检测项目不能为空！");
		return;
	}
//	
//	//样本类型验证
//	var sampleTypeNew =$("#sampleTypeNew").val();
//	if(sampleTypeNew==""){
//		message("样本类型不能为空！");
//		return;
//	}
//	
//	//家庭地址的验证
//	var address = $("#addressNew").val();
//	if (address == "") {
//		message("家庭地址不能为空！");
//		return;
//	};
//	
//	//证件类型验证
//	var voucherTypeNew = $("#voucherTypeNew").val();
//	if(voucherTypeNew==""){
//		message("证件类型不能为空！");
//		return;
//	}

	var nextStepFlow =$("#sampleInputTemp_nextStepFlow").val();
	if(nextStepFlow==""){
		message("请选择下一步流向");
		return;
	}

	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleTemplate_id").val(),
		title : $("#sampleTemplate_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTemplate_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action";
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleTemplate_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleTemplate_id").val()) {
				commonChangeState("formId=" + $("#sampleTemplate_id").val()
						+ "&tableId=SampleTemplate");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push("信息录入编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : '高通量基因测序产前筛查与诊断临床申请单审核',
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : '选择样本类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleTypeIdNew").value = id;
	document.getElementById("sampleTypeNew").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 证件类型
function voucherTypeFun() {
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
	var voucherTypeFun = new Ext.Window(
			{
				id : 'voucherTypeFun',
				modal : true,
				title : '选择证件类型',
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherTypeFun.close();
					}
				} ]
			});
	voucherTypeFun.show();
}
function setzjlx(id, name) {
	document.getElementById("voucherTypeIdNew").value = id;
	document.getElementById("voucherTypeNew").value = name;
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
}

function checkType() {
	var re = $("#sampleTemplate_voucherType_name").val();
	if (re == "") {
		message("证件类型不能为空！");
	}
}
// 证件号码验证
function checkFun() {
	var reg = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
	if (reg.test($("#sampleTemplate_voucherCode").val())) {
		var id = $("#sampleTemplate_voucherCode").val();
		ajax("post", "/sample/sampleInput/findIdentity.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("输入的身份证号重复！");
				}
			}
		}, null);
	} else {
		message("请输入正确的证件号码!");
	}
}
// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleTemplate_phoneNum").val())) {
		// return;
		var id = $("#sampleTemplate_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("输入的手机号重复！");
				}
			}
		}, null);
	} else {
		message("请输入正确的手机号码!");
	}
}
// B超异常提醒验证
function change() {
	var reg = $('#sampleTemplate_embryoType option:selected').val();
	if (reg == "2") {
		$("#sampleTemplate_messages").css("display", "");
	}
}


// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("productIdNew").value = id;
					document.getElementById("productNameNew").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}

function a(){
	var productName = $("#productName").val();
	var id1 = $("#sampleInputTemp_productId").val();
	var name1= $("#sampleInputTemp_productName").val();
	var id2= $("#inputTemp_productId").val();
	var name2 = $("#inputTemp_productName").val();
	if(productName==name1){
		$("#productIdNew").val(id1);
	}else if(productName==name2){
		$("#productIdNew").val(id2);
	}else if(productName==""){
		$("#productIdNew").val("");
	}
}

function b(){
	var sampleType = $("#sampleType").val();
	var id1 = $("#sampleInputTemp_sampleType_id").val();
	var name1 = $("#sampleInputTemp_sampleType_name").val();
	var id2 = $("#inputTemp_sampleType_id").val();
	var name2 = $("#inputTemp_sampleType_name").val();
	if(sampleType==name1){
		$("#sampleTypeIdNew").val(id1);
	}else if(sampleType==name2){
		$("#sampleTypeIdNew").val(id2);
	}else if(sampleType==""){
		$("#sampleTypeIdNew").val("");
	}
}