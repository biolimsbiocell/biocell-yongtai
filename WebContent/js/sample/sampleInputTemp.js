	var sampleInputTempGrid;
$(function() {
	var cols = {};
	var fields = [];
	  fields.push({
			name:'id',
			type:"string"
		});
		    fields.push({
			name:'code',
			type:"string"
		});
		    fields.push({
			name:'name',
			type:"string"
		});
		    
	    fields.push({
			name:'upLoadAccessory-fileName',
			type:"string"
		});
	    fields.push({
			name:'upLoadAccessory-id',
			type:"string"
		});
		    fields.push({
			name:'patientName',
			type:"string"
		});
		    fields.push({
			name:'idCard',
			type:"string"
		});
		    fields.push({
			name:'phone',
			type:"string"
		});
		    fields.push({
			name:'sampleType',
			type:"string"
		});
		    fields.push({
			name:'businessType',
			type:"string"
		});
		    fields.push({
			name:'businessType',
			type:"string"
		});
		    fields.push({
			name:'hospital',
			type:"string"
		});
		    fields.push({
			name:'price',
			type:"string"
		});
		    fields.push({
		    name:'suppleAgreement',
		    type:"string"
		});
	   //审核人
	    fields.push({
			name:'createUser-id',
			type:"string"
		});
		   fields.push({
			name:'createUser-name',
			type:"string"
		});
		    fields.push({
			name:'type-id',
			type:"string"
		});
		    fields.push({
			name:'type-name',
			type:"string"
		});
		    fields.push({
			name:'productName',
			type:"string"
		});
		    fields.push({
			name:'productId',
			type:"string"
		});
		    fields.push({
			name:'state',
			type:"string"
		});
		    fields.push({
			name:'stateName',
			type:"string"
		});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		hidden:true,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'suppleAgreement',
		header:biolims.sample.suppleAgreement,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.idCard,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'price',
		header:biolims.sample.price,
		hidden:true,
		width:50*6,
		
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	//录入人
	cm.push({
		dataIndex:'createUser-id',
		hidden : true,
		header:biolims.wk.confirmUserId,
		width:20*6
	});
	
	cm.push({
		dataIndex:'createUser-name',
		hidden : false,
		header:biolims.wk.confirmUserName,
		width:20*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'type-id',
		header:biolims.sample.payTypeId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'type-name',
		header:biolims.sample.payTypeName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.projectName,
		width:20*6,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.projectId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header: biolims.common.workFlowState,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'upLoadAccessory-fileName',
		header:biolims.sample.upLoadAccessoryName,
		width:20*6,
		hidden:false,
		sortable:true
	});
	
	cm.push({
		dataIndex:'upLoadAccessory-id',
		header:biolims.sample.upLoadAccessoryId,
		width:20*6,
		hidden:true,
		sortable:true
	});

	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/sample/sampleInputTemp/showSampleInputCheckListJson.action";
	var opts = {};
	opts.tbar = [];
	opts.title = biolims.sample.InfoAudit;
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
//	opts.tbar.push({
//		text : '显示可编辑列',
//		handler : null
//	});
//	opts.tbar.push({
//		text : '填加明细',
//		handler : null
//	});
//	opts.tbar.push({
//		text : '取消选中',
//		handler : null
//	});
//	opts.tbar.push({
//		text : '删除选中',
//		handler : null
//	});
	sampleInputTempGrid = gridEditTable("show_sampleInputTemp_div", cols, loadParam,opts);
	$("#show_sampleInputTemp_div").data("sampleInputTempGrid", sampleInputTempGrid);
});
function add() {
	window.location = window.ctx
			+ '/sample/sampleInputTemp/editSampleInputTemp.action';
}
function edit() {
	
	var id = "";
	id = document.getElementById("selectId").value;

	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/sample/sampleInputTemp/editSampleInputTemp.action?id=' + id;
}


function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/sample/sampleInputTemp/viewSampleInputTemp.action?id=' + id;
}

function exportexcel() {
	sampleInputGrid.title = biolims.common.exportList;
	var vExportContent = sampleInputTempGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startbirthday").val() != undefined) && ($("#startbirthday").val() != '')) {
					var startbirthdaystr = ">=##@@##" + $("#startbirthday").val();
					$("#birthday1").val(startbirthdaystr);
				}
				if (($("#endbirthday").val() != undefined) && ($("#endbirthday").val() != '')) {
					var endbirthdaystr = "<=##@@##" + $("#endbirthday").val();

					$("#birthday2").val(endbirthdaystr);

				}
				
				
				commonSearchAction(sampleInputTempGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});