﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTempTemp/editSampleInput.action";
}

load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");

$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {	
	var nextStepFlow =$("#sampleBreastCancerTemp_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.pleaseSelectNextFlow);
		return;
	}
	

	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleBreastCancerTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleBreastCancerTemp_id").val(),
					title : $("#sampleBreastCancerTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleBreastCancerTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/saveToSampleBreastCanerOne.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInputTemp/copySampleInput.action?id=' + $("#sampleBreastCancerTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleBreastCancerTemp_id").val()){
		commonChangeState("formId=" + $("#sampleBreastCancerTemp_id").val() + "&tableId=SampleBreastCancerTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleBreastCancerTemp_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.EGFRAudit,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 800,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleBreastCancerTemp_sampleType_id").value = id;
		 document.getElementById("sampleBreastCancerTemp_sampleType_name").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:500,height:500,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("sampleBreastCancerTemp_voucherType").value = id;
			 document.getElementById("sampleBreastCancerTemp_voucherType_name").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleBreastCancerTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleBreastCancerTemp_voucherCode").val())){
				var id = $("#sampleBreastCancerTemp_voucherCode").val();
				ajax("post", "/sample/sampleInputTemp/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleBreastCancerTemp_phoneNum").val())){
				//return;
				var id = $("#sampleBreastCancerTemp_phoneNum").val();
				ajax("post", "/sample/sampleInputTemp/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 500,
						height : 500,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("sampleBreastCancerTemp_product_id").value = id;
							document.getElementById("sampleBreastCancerTemp_product_name").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}

	