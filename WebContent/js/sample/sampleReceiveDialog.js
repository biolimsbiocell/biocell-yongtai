var sampleReceiveDialogGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'businessType-id',
		type:"string"
	});
	    fields.push({
		name:'businessType-name',
		type:"string"
	});
	    fields.push({
		name:'expreceCode',
		type:"string"
	});
//	    fields.push({
//		name:'expreceCode-name',
//		type:"string"
//	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
			name:'state',
			type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.openBoxInspectionId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'businessType-id',
		header:biolims.sample.businessTypeId,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'businessType-name',
		header:biolims.common.sequencingFun,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'expreceCode',
		header:biolims.sample.expreceCode,
		width:20*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'expreceCode-name',
//		header:'快递单号',
//		width:20*10,
//		sortable:true
//		});
		cm.push({
		dataIndex:'acceptUser-id',
		header:biolims.common.receiverId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.receiverName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.acceptDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowState, //工作流状态
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'project-id',
		hidden:true,
		header:biolims.sample.projectId,
		width:20*10,
		sortable:true
		});
	cm.push({
	dataIndex:'project-name',
	header:biolims.sample.projectName,
	
	width:20*10,
	sortable:true
	});
		
	cm.push({
		dataIndex:'classify',
		header:biolims.common.sampleTag,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleReceive/showSampleReceiveListJson.action";
	var opts={};
	opts.title=biolims.common.openBoxInspection;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleReceiveFun(rec);
	};
	sampleReceiveDialogGrid=gridTable("show_dialog_sampleReceive_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleReceiveDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
