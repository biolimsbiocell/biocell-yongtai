var sjbb = [];
$(function() {
	$("#starttime").datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd " // 日期格式，详见
	});
	$("#endtime").datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd " // 日期格式，详见
	});
	if($("#starttime").val()==""){
		var myDate = new Date();
		var year = myDate.getFullYear();
		var month = myDate.getMonth()+1;
		var date = myDate.getDate();
		if(month>=10){
			var thedate = year+"-"+month+"-"+date;
		}else{
			var thedate = year+"-0"+month+"-"+date;
		}
		$("#starttime").val(thedate)
	}
	if($("#endtime").val()==""){
		var myDate1 = new Date();
		var year1 = myDate1.getFullYear();
		var month1 = myDate1.getMonth()+1;
		var date1 = myDate1.getDate();
		if(month1>=10){
			var thedate1 = year1+"-"+month1+"-"+date1;
		}else{
			var thedate1 = year1+"-0"+month1+"-"+date1;
		}
		$("#endtime").val(thedate1)
	}
	var colOpts = [];
	colOpts.push({
		"data" : 'id',
		"title" : "筛选号/时间",
	});
	colOpts.push({
		"data" : 'sampleState',
		"title" : "状态",
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "在组";
			}
			if (data == "1") {
				return "剔除";
			}
			if (data == "2") {
				return "脱落";
			}
			if (data == "3") {
				return "复发";
			} 
			if (data == "4") {
				return "死亡";
			} 
			if (data == "5") {
				return "其它";
			}else {
				return "";
			}
		}
	});
	$.ajax({
			type: "post",
			url: ctx+"/sample/sampleFutureFind/chaxunsj.action",
			data: {
				kssj: $("#starttime").val(),// kssj,
				jssj: $("#endtime").val(),// jssj,
			},
			async : false,
			datatype: "json",
			success: function(data) {
				var obj = JSON.parse(data);
				if(obj["success"]) {
					if(obj["sj"]!=""){
						$.each(obj["sj"], function(j, k) {
							colOpts.push({
								"data" : obj["sj"][j]["sampleTime"],
								"title" : obj["sj"][j]["sampleTime"]
							});
							sjbb.push(obj["sj"][j]["sampleTime"]);
						});
					}
				} else{ 
// top.layer.msg("验证失败！");
				} 
			}
		});
	
	var tbarOpts = [];
// tbarOpts.push({
// text : '查询',
// action : function() {
// xianshisaomiao();
// }
// });
// tbarOpts.push({
// text : '添加新人员',
// action : function() {
// xinrenyuan();
// }
// });
// tbarOpts.push({
// text : '保存',
// action : function() {
// saveShuju();
// }
// });
	var options = table(true, "",
			"/sample/sampleFutureFind/showSampleFutureFindsjListJson.action?sjbb="+sjbb,
			colOpts, tbarOpts);
	myTable = renderData($("#main"), options);
	// myTable= renderRememberData($("#main"), options);
// myTable= renderData($("#main"), options);
	// 恢复之前查询的状态
// $('#main').on('init.dt', function() {
// recoverSearchContent(myTable);
// });
});
function chaxun(){

// $("#main").dataTable().fnDestroy();
	var oldTable = $('#main').dataTable();
    oldTable.fnClearTable(); // 清空一下table
    oldTable.fnDestroy(); // 还原初始化了的dataTable
    $('#main').empty();
	var colOpts1 = [];
	colOpts1.push({
		"data" : 'id',
		"title" : "筛选号/时间",
		"width" : "100px"
	});
	colOpts1.push({
		"data" : 'sampleState',
		"title" : "状态",
		"width" : "100px",
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "在组";
			}
			if (data == "1") {
				return "剔除";
			}
			if (data == "2") {
				return "脱落";
			}
			if (data == "3") {
				return "复发";
			} 
			if (data == "4") {
				return "死亡";
			} 
			if (data == "5") {
				return "其它";
			}else {
				return "";
			}
		}
	});
	$.ajax({
			type: "post",
			url: ctx+"/sample/sampleFutureFind/chaxunsj.action",
			data: {
				kssj: $("#starttime").val(),// kssj,
				jssj: $("#endtime").val(),// jssj,
			},
			async : false,
			datatype: "json",
			success: function(data) {
				var obj = JSON.parse(data);
				if(obj["success"]) {
					if(obj["sj"]!=""){
						$.each(obj["sj"], function(j, k) {
							colOpts1.push({
								"data" : obj["sj"][j]["sampleTime"],
								"title" : obj["sj"][j]["sampleTime"],
								"width" : "100px"
							});
							sjbb.push(obj["sj"][j]["sampleTime"]);
						});
					}
				} else{ 
// top.layer.msg("验证失败！");
				} 
			}
		});
	
	var tbarOpts1 = [];
	var options1 = table(true, "",
			"/sample/sampleFutureFind/showSampleFutureFindsjListJson.action?sjbb="+sjbb,
			colOpts1, tbarOpts1);
	myTable = renderData($("#main"), options1);
	// 恢复之前查询的状态
// $('#main').on('init.dt', function() {
// recoverSearchContent(myTable);
// });
}
// function add(){
// window.location=window.ctx+'/sample/sampleReceive/editSampleReceive.action?type='+$("#type").val();
// }
// function edit(){
// var id="";
// var id = $(".selected").find("input").val();
// if (id==""||id==undefined){
// message(biolims.common.selectRecord);
// return false;
// }
// window.location=window.ctx+'/sample/sampleReceive/editSampleReceive.action?id='
// + id +'&type='+$("#type").val();
// }
// function view() {
// var id = "";
// var id = $(".selected").find("input").val();
// if (id == "" || id == undefined) {
// message(biolims.common.selectRecord);
// return false;
// }
// window.location = window.ctx +
// '/sample/sampleReceive/viewSampleReceive.action?id=' + id
// +'&type='+$("#type").val();
// }
//
function xinrenyuan(){
	top.layer.open({
		  type: 1,
		  title: "添加新人员",
		  closeBtn: 1,
		  offset: '100px',
		  shadeClose: true,
		  skin: 'yourclass',
		  btn: ['验证'],
		  content: `
		            <div class="form-group has-feedback">
			  			<div class="input-group">
			  				<span class="input-group-addon">筛选号</span>
							<input type="text" class="form-control name" placeholder="筛选号">
						</div>
					</div>
					<div class="form-group has-feedback">
						<div class="input-group">
			  				<span class="input-group-addon">初次采血时间</span>
							<input class="form-control" type="text" size="20" maxlength="25"
									id="cxsj"	 format="yyyy-MM-dd" />
						</div>
					</div>
					<div class="col-xs-12">
					<button class="btn btn-primary btn-block" id="checkU">确定</button>
					<script>
					$("#cxsj").datepicker({
						language : "zh-CN",
						autoclose : true, // 选中之后自动隐藏日期选择框
						format : "yyyy-mm-dd " // 日期格式，详见
					});
					$("#checkU").click(function() {
		        	var user = $(".name").val();
					var pad = $(".psd").val();
		 $.ajax({
			type: "post",
			url: ctx+"/main/checkU.action",
			data: {
				user: user,
				pad: pad,
			},
			datatype: "json",
			success: function(data) {
				var obj = JSON.parse(data);
				if(obj["success"] == 1) {
				top.layer.msg("验证成功！");
			    $("#templeReviewer-name").val(user);
			     $("#templeReviewer").val(obj.id);
			    layer.closeAll(); //疯狂模式，关闭所有层
				} else{ 
				top.layer.msg("验证失败！");
				} 
			}
		});
		        });
					
				</script>
				</div>` ,
		 success: function(layero){
		        var btn = layero.find('.layui-layer-btn');
		        btn.find('.layui-layer-btn0').remove()
    }
		}); 
}

// 跳转页面
function hrefFunc(that){
	var round = that.getAttribute("id");
	var idCode = $(that).attr("class");
	console.log($(that).attr("class"))
	console.log(round)
	var id = $(that).parents("tr").find("input[type=checkbox]").val();
	 $.ajax({
		 type : "post",
		 data: {
			 filtrateCode:id,
			 round :round,
		 },
		 url: ctx+"/sample/sampleFutureFind/findSampleOrderId.action",
		 success:function(dataBack){
			 var data = JSON.parse(dataBack)
			 console.log(data)
			 if(data.success){
				 if(data.idCode !=null && data.idCode != undefined && data.idCode !=""){
					 window.location = window.ctx +
					 	'/system/sample/sampleOrder/findSampleOrder.action?id=' +data.id+'&type='+0+'&round='+data.round+'&filtrateCode='+data.filtrateCode+'&idCode='+data.idCode+'&sjh=1';
				 }else{
					 window.location = window.ctx +
					 	'/system/sample/sampleOrder/findSampleOrder.action?id=' +data.id+'&type='+0+'&round='+data.round+'&filtrateCode='+data.filtrateCode+'&sjh=1';
				 }
			 }else{
				 top.layer.msg("不能跨轮次创建订单!")
				 return false;
			 }
		 }
	 })
}
// 弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "筛选号",
			"type": "input",
			"searchName": "filtrateCode"
		},{
			"txt": "轮次",
			"type": "input",
			"searchName": "round"
		},
		{
			"txt": "时间（开始）",
			"type": "dataTime",
			"searchName": "sampleTime##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "时间（结束）",
			"type": "dataTime",
			"searchName": "sampleTime##@@##2",
			"mark": "e##@@##",
		},
		{
			"type": "table",
			"table": myTable
		}
	];
}
