$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : 'id',
		"title" : "编码",
		"visible" : false,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : 'filtrateCode',
		"title" : "筛选号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "filtrateCode");
			$(td).attr("timeState", rowData['timeState']);
			$(td).attr("timeShowState", rowData['timeShowState']);
		}
	});
	colOpts.push({
		"data" : 'sampleTime',
		"title" : "时间",
//		"className" : "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleTime");
		}
	});
	colOpts.push({
		"data" : 'round',
		"title" : "轮次",
//		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "round");
		}
	});
	colOpts.push({
		"data" : "happenEvent",
		"title" : "事件",
//		"className" : "select",
		"name" : "|采血|回输",
		"createdCell" : function(td) {
			$(td).attr("saveName", "happenEvent");
			$(td).attr("selectOpt", "|采血|回输");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "采血";
			}
			if (data == "2") {
				return "回输";
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : 'sampleState',
		"title" : "状态",
		"createdCell" : function(td) {
			$(td).attr("saveName", "samplState");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "在组";
			}
			if (data == "1") {
				return "剔除";
			}
			if (data == "2") {
				return "脱落";
			}
			if (data == "3") {
				return "复发";
			} 
			if (data == "4") {
				return "死亡";
			} 
			if (data == "5") {
				return "其它";
			}else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : 'dingDanCode',
		"title" : "订单编号",
//		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "dingDanCode");
		}

	});
	colOpts.push({
		"data" : 'stateName',
		"title" : "订单状态",
		"createdCell" : function(td) {
			$(td).attr("saveName", "stateName");
		}
	});
	var tbarOpts = [];
	tbarOpts.push({
		text : '查询',
		action : search
	});
//	tbarOpts.push({
//		text : '添加新人员',
//		action : function() {
//			xinrenyuan();
//		}
//	});
//	tbarOpts.push({
//		text : '弹窗修改',
//		action : function() {
//			xiugai($("#main"));
//		}
//	});
//	tbarOpts.push({
//		text : '保存',
//		action : function() {
//			saveShuju();
//		}
//	});
	var options = table(true, "",
			"/sample/sampleFutureFind/showSampleFutureFindListJson.action",
			colOpts, tbarOpts);
	myTable = renderData($("#main"), options);
	//myTable= renderRememberData($("#main"), options);
//	myTable= renderData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(myTable);
	})
});
//弹框编辑
function xiugai(ele) {
	var row = ele.find(".selected");
	var length = row.length;
	if(!length || length > 1) {
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
		return false;
	}
	var ths = ele.find("th");
	var tds = row.children("td");
	//获取能被修改的列的表头并生成输入框
	$("body").append('<div id="editItemLaye" class="hide"><div id="editItemLayer" class="row"></div></div>');
	var str = "";
	ths.each(function(i, val) {
		if(i != 0) {
			var edit = $(val).attr("key");
			var txt = $(val).text();
			var val = $(tds[i]).text();
			if(txt == "时间") {
				str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="date" class="form-control editItemLayerInput editItemLayerDate" value=' + val + '></div></div>';
			} else{
				if(edit == "select") {
					var selectopt = $(tds[i]).attr("selectopt");
					var selectOptArr = selectopt.split("|");
					var opt = "";
					selectOptArr.forEach(function(v, i) {
						if(v == val) {
							opt += "<option selected>" + v + "</option>";
						} else {
							opt += "<option>" + v + "</option>";
						}
					});
					str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><select class="form-control editItemLayerInput addItemLayerSelect">' + opt + '</select></div></div>';
				} else if(edit == "date") {
					str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="date" class="form-control editItemLayerInput editItemLayerDate" value=' + val + '></div></div>';
				} else if(edit == "edit") {
					str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput" value=' + val + '></div></div>';
				} else {
					str += '<div class="col-sm-6" style="margin-top: 10px;"><div class="input-group"><span class="input-group-addon">' + txt + '</span><input type="text" class="form-control editItemLayerInput" disabled value=' + val + '></div></div>';
				}
			}
		}
	});
	$("#editItemLayer").append(str);
	top.layer.open({
		type: 1,
		content: $("#editItemLaye").html(),
		anim: '1',
		area: ['650px', "300px"],
		shade: 0,
		title: biolims.common.vim,
		btn: ['<<', biolims.common.selected, '>>'],
		yes: function(index, layero) {
			row.find(".icheck").iCheck('uncheck');
			row = row.prev("tr");
			if(!row.length) {
				top.layer.msg(biolims.common.noPreData);
				top.layer.close(index);
				return false;
			}
			row.find(".icheck").iCheck('check');
			tds = row.children("td");
			var thisTextArr = [];
			tds.each(function(i, val) {
				if(i != 0) {
					thisTextArr.push($(val).text());
				}
			});
			$(".editItemLayerInput").attr('type','date')
			$(".editItemLayerInput", parent.document).each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					$(val).children("option").each(function(j, opt) {
						if($(opt).val() == thisTextArr[i]) {
							$(opt).prop("selected", true);
						} else {
							$(opt).prop("selected", false);
						}
					});
				} else {
					$(val).val(thisTextArr[i]);
				}
			});
			return false;
		},
		btn2: function(index, layero) {
			//获取添加的数据
			var addItemLayerValue = [];
			$(".editItemLayerInput", parent.document).each(function(i, val) {
				addItemLayerValue.push($(val).val());
			});
			//回填tr
			for(var i = 1; i < tds.length; i++) {
				var width = $(tds[i]).width();
				$(tds[i]).css({
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
				});
				$(tds[i]).text(addItemLayerValue[i - 1]);
			}
			row.addClass("editagain");
			top.layer.msg(biolims.main.success);
			top.layer.close(index);
			return false;
		},
		btn3: function(index, layero) {
			row.find(".icheck").iCheck('uncheck');
			row = row.next("tr");
			if(!row.length) {
				top.layer.msg(biolims.common.noNextData);
				top.layer.close(index);
				return false;
			}
			row.find(".icheck").iCheck('check');
			tds = row.children("td");
			var thisTextArr = [];
			tds.each(function(i, val) {
				if(i != 0) {
					thisTextArr.push($(val).text());
				}
			});
			$(".editItemLayerInput", parent.document).each(function(i, val) {
				if($(val).hasClass("addItemLayerSelect")) {
					$(val).children("option").each(function(j, opt) {
						if($(opt).val() == thisTextArr[i]) {
							$(opt).prop("selected", true);
						} else {
							$(opt).prop("selected", false);
						}
					});
				} else {
					$(val).val(thisTextArr[i]);
				}
			});
			return false;
		},
		btnAlign: 'c',
		success: function(index, layero) {
			$(".editItemLayerDate").datepicker({
				language: "zh-TW",
				autoclose: true, //选中之后自动隐藏日期选择框
				format: "yyyy-mm-dd" //日期格式，详见 
			});
		},
		end: function() {
			$("#editItemLaye").remove();
		}
	});

}
function saveShuju(){
	var datas = saveItemjson($("#main"));
		$.ajax({
  			type : "post",
  			url : ctx + "/sample/sampleFutureFind/saveSampleFutureFind.action",
  			data : 
  			{
  				datas:datas
  				},
  			async : false,
  			success : function(data) {
  				var data = JSON.parse(data);
  				if (data.success) {
  					top.layer.closeAll();
  					top.layer.msg(biolims.common.saveSuccess);
//  					myTable.settings()[0].ajax.data = {
//  						"id" : data.id
//  					};
  					myTable.ajax.reload();
  				}
  			}
  		});
}

//获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag = true;
	
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");

			// 判断男女并转换为数字
			if (k == "happenEvent") {
				var happenEvent = $(tds[j]).text();
				if (happenEvent == "采血") {
					json[k] = "1";
				} else if (happenEvent == "回输") {
					json[k] = "2";
				} else {
					json[k] = "";
				}
				continue;
			}
			json[k] = $(tds[j]).text();

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function xinrenyuan(){
	top.layer.open({
		  type: 1,
		  title: "添加新人员",
		  closeBtn: 1,
		  offset: '100px',
		  shadeClose: true,
		  skin: 'yourclass',
		  btn: ['确定'],
		  content: `
		            <div class="form-group has-feedback">
			  			<div class="input-group">
			  				<span class="input-group-addon">筛选号</span>
							<input type="text" id="sxh" class="form-control name" placeholder="筛选号">
						</div>
					</div>
					<div class="form-group has-feedback">
						<div class="input-group">
			  				<span class="input-group-addon">初次采血时间</span>
							<input class="form-control" type="text" size="20" maxlength="25"
									id="cxsj"	 format="yyyy-MM-dd" />
						</div>
					</div>
					<div class="col-xs-12">
					<button class="btn btn-primary btn-block" id="checkU">确定</button>
					<script>
					$("#cxsj").datepicker({
						language : "zh-CN",
						autoclose : true, // 选中之后自动隐藏日期选择框
						format : "yyyy-mm-dd " // 日期格式，详见
					});
					$("#checkU").click(function() {
			        	var sxh = $("#sxh").val();
						var cxsj = $("#cxsj").val();
						if(sxh.length!=5){
						layer.msg("筛选号长度是5位!");
						return false;
						}
						 $.ajax({
							type: "post",
							url: ctx+"/sample/sampleFutureFind/checkU.action",
							data: {
								sxh: sxh,
								cxsj: cxsj,
							},
							datatype: "json",
							success: function(data) {
								var obj = JSON.parse(data);
								if(obj["success"]) {
									if(obj["cz"]){
										top.layer.msg("创建失败，该筛选号已被使用！");
									}else{
										layer.closeAll(); //疯狂模式，关闭所有层
										top.layer.msg("创建成功！");
									}
								} else{
									top.layer.msg("创建失败！");
								} 
							}
						});
		        	});
					
				</script>
				</div>` ,
		 success: function(layero){
		        var btn = layero.find('.layui-layer-btn');
		        btn.find('.layui-layer-btn0').remove()
    }
		}); 
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "筛选号",
			"type": "input",
			"searchName": "filtrateCode"
		},{
			"txt": "轮次",
			"type": "input",
			"searchName": "round"
		},
		{
			"txt": "时间（开始）",
			"type": "dataTime",
			"searchName": "sampleTime##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "时间（结束）",
			"type": "dataTime",
			"searchName": "sampleTime##@@##2",
			"mark": "e##@@##",
		},
		{
			"type": "table",
			"table": myTable
		}
	];
}

