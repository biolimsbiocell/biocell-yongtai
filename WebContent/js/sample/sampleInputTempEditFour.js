﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	if($("#sampleInputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#inputTemp_name").val());
	}else if($("#inputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}else if($("#sampleInputTemp_name").val()!="" && $("#inputTemp_name").val()!=""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}
	if($("#sampleInputTemp_productName").val()!=$("#inputTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
		$("#productNameNew").val($("#sampleInputTemp_productName").val());
	}
	if($("#sampleInputTemp_patientName").val()!=$("#inputTemp_patientName").val())
		$("#aabb").css({"background-color":"red","color":"white"});
	else
		$("#ccdd").val($("#sampleInputTemp_patientName").val());
	
	if($("#sampleInputTemp_inHosNum").val()!=$("#inputTemp_inHosNum").val())
		$("#inHosNum").css({"background-color":"red","color":"white"});
	else
		$("#inHosNumNew").val($("#sampleInputTemp_inHosNum").val());
	
	if($("#sampleInputTemp_age").val()!=$("#inputTemp_age").val())
		$("#age").css({"background-color":"red","color":"white"});
	else
		$("#ageNew").val($("#sampleInputTemp_age").val());
	
	if($("#sampleInputTemp_endMenstruationDate").val()!=$("#inputTemp_endMenstruationDate").val()){
		$("#endMenstruationDate").css({"background-color":"red","color":"white"});
	}else{
		$("#endMenstruationDateNew").val($("#sampleInputTemp_endMenstruationDate").val());
	}
	if($("#sampleInputTemp_sampleType_id").val()!=$("#inputTemp_sampleType_id").val())
		$("#sampleType").css({"background-color":"red","color":"white"});
	else
		$("#sampleTypeNew").val($("#sampleInputTemp_sampleType_name").val());
	
	if($("#sampleInputTemp_diagnosis").val()!=$("#inputTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleInputTemp_diagnosis").val());
	}
	if($("#sampleInputTemp_gestationIVF").val()!=$("#inputTemp_gestationIVF").val()){
		$("#inputTempNew_gestationIVF").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_isInsure").val()!=$("#inputTemp_isInsure").val()){
		$("#inputTempNew_isInsure").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_hospital").val()!=$("#inputTemp_hospital").val())
		$("#hospital").css({"background-color":"red","color":"white"});
	else
		$("#hospitalNew").val($("#sampleInputTemp_hospital").val());
	
	if($("#sampleInputTemp_doctor").val()!=$("#inputTemp_doctor").val())
		$("#doctor").css({"background-color":"red","color":"white"});
	else
		$("#doctorNew").val($("#sampleInputTemp_doctor").val());
	
	if($("#sampleInputTemp_sendDate").val()!=$("#inputTemp_sendDate").val())
		$("#sendDate").css({"background-color":"red","color":"white"});
	else
		$("#sendDateNew").val($("#sampleInputTemp_sendDate").val());
	
	if($("#sampleInputTemp_trisome21Value").val()!=$("#inputTemp_trisome21Value").val()){
		$("#trisome21Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome21ValueNew").val($("#sampleInputTemp_trisome21Value").val());
	}
	
	if($("#sampleInputTemp_reference21Range").val()!=$("#inputTemp_reference21Range").val()){
		$("#reference21Range").css({"background-color":"red","color":"white"});
	}else{
		$("#reference21RangeNew").val($("#sampleInputTemp_reference21Range").val());
	}
	
	if($("#sampleInputTemp_trisome18Value").val()!=$("#inputTemp_trisome18Value").val()){
		$("#trisome18Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome18ValueNew").val($("#sampleInputTemp_trisome18Value").val());
	}
	
	if($("#sampleInputTemp_reference18Range").val()!=$("#inputTemp_reference18Range").val()){
		$("#reference18Range").css({"background-color":"red","color":"white"});
	}else{
		$("#reference18RangeNew").val($("#sampleInputTemp_reference18Range").val());
	}
	
	if($("#sampleInputTemp_trisome13Value").val()!=$("#inputTemp_trisome13Value").val()){
		$("#trisome13Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome13ValueNew").val($("#sampleInputTemp_trisome13Value").val());
	}
	
	if($("#sampleInputTemp_reference13Range").val()!=$("#inputTemp_reference13Range").val()){
		$("#reference13Range").css({"background-color":"red","color":"white"});
	}else{
		$("#reference13RangeNew").val($("#sampleInputTemp_reference13Range").val());
	}
	if($("#sampleInputTemp_reason").val()!=$("#inputTemp_reason").val()){
		$("#reason").css({"background-color":"red","color":"white"});
	}else{
		$("#reasonNew").val($("#sampleInputTemp_reason").val());
	}
	if($("#sampleInputTemp_badMotherhood").val()!=$("#inputTemp_badMotherhood").val()){
		$("#badMotherhood").css({"background-color":"red","color":"white"});
	}else{
		$("#badMotherhoodNew").val($("#sampleInputTemp_badMotherhood").val());
	}
	
	if($("#sampleInputTemp_medicalHistory").val()!=$("#inputTemp_medicalHistory").val()){
		$("#medicalHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#medicalHistoryNew").val($("#sampleInputTemp_medicalHistory").val());
	}
	
	if($("#sampleInputTemp_detectionMan_id").val()!=$("#inputTemp_detectionMan_id").val()){
		$("#detectionMan").css({"background-color":"red","color":"white"});
	}else{
		$("#detectionManNew").val($("#sampleInputTemp_detectionMan_id").val());
	}
	
	if($("#sampleInputTemp_reportMan_id").val()!=$("#inputTemp_reportMan_id").val()){
		$("#reportMan").css({"background-color":"red","color":"white"});
	}else{
		$("#reportManNew").val($("#sampleInputTemp_reportManMan_id").val());
	}
	
	if($("#sampleInputTemp_auditMan_id").val()!=$("#inputTemp_auditMan_id").val()){
		$("#auditMan").css({"background-color":"red","color":"white"});
	}else{
		$("#auditManNew").val($("#sampleInputTemp_auditMan_id").val());
	}
	
	if($("#sampleInputTemp_reportDate").val()!=$("#inputTemp_reportDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleInputTemp_reportDate").val());
	}
	
	
	
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {
	//sampleInfo 的图片Id
//	var sampleInfoImg = $("#upload_imga_id10").val();
	//sampleInputTemp 的图片Id
//	var sampleInputTempimg = $("#upload_imga_id").val();
//	if($("#upload_imga_name11").val()!="" && $("#upload_imga_id10").val()!="" && sampleInfoImg == sampleInputTempimg){
//		
//		
//		
//		
//		
//		
//	}else {
//		if($("#upload_imga_id").val()==""){
//			message("请上传图片并录入信息后，进行保存！");
//			return;
//		}
//	}
	//检测项目的验证
	var productName = $("#productNameNew").val();
	if(productName==""){
		message(biolims.sample.productNameIsEmpty);
		return;
	}
//	//样本类型的验证
//	var sampleType = $("#sampleTypeNew").val();
//	if(sampleType==""){
//		message("样本类型不能为空！");
//		return;
//	}
	
	//下一步流向的验证
	var nextStepFlow = $("#sampleInputTemp_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.pleaseSelectNextFlow);
		return;
	}
	
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleInputTemp", {
		userId : userId,
		userName : userName,
		formId : $("#sampleInputTemp_id").val(),
		title : $("#sampleInputTemp_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleInputTemp_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action";
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleTemplate_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleInputTemp_id").val()) {
				commonChangeState("formId=" + $("#sampleInputTemp_id").val()
						+ "&tableId=SampleInputTemp");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : biolims.sample.high_throughputAudit,
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : biolims.sample.selectType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleTypeIdNew").value = id;
	document.getElementById("sampleTypeNew").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 证件类型
function voucherTypeFun() {
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
	var voucherTypeFun = new Ext.Window(
			{
				id : 'voucherTypeFun',
				modal : true,
				title : biolims.sample.selectCertificateType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherTypeFun.close();
					}
				} ]
			});
	voucherTypeFun.show();
}
function setzjlx(id, name) {
	document.getElementById("sampleInputTemp_voucherType").value = id;
	document.getElementById("sampleInputTemp_voucherType_name").value = name;
	var win = Ext.getCmp('voucherTypeFun');
	if (win) {
		win.close();
	}
}

function checkType() {
	var re = $("#sampleInputTemp_voucherType_name").val();
	if (re == "") {
		message(biolims.sample.voucherTypeIsEmpty);
	}
}
// 证件号码验证
function checkFun() {
	var reg = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
	if (reg.test($("#sampleInputTemp_voucherCode").val())) {
		var id = $("#sampleInputTemp_voucherCode").val();
		ajax("post", "/sample/sampleInput/findIdentity.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.IdRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputRightId);
	}
}
// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleInputTemp_phoneNum").val())) {
		// return;
		var id = $("#sampleInputTemp_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.phoneRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputRightId);
	}
}

// 家庭住址验证
function checkAddress() {
	var address = $("#sampleInputTemp_address").val();
	if (address == "") {
		message(biolims.sample.addressIsEmpty);
	}
}

/*
 * 上传图片
 */
function upLoadImg1(){
	var isUpload = true;
	var id=$("#template_testType").val();
	var fId=$("#upload_imga_id11").val();
	var fName=$("#upload_imga_name").val();
	
	load("/system/template/template/toSampeUpload.action", { // 是否修改
		fileId:fId,
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			fName=data.fileName;
			fId=data.fileId;
//				$("#template_fileInfo").val(fId);
//				$("#template_fileInfo_name").val(fName);
			 document.getElementById('upload_imga_id').value=fId;
			 document.getElementById('upload_imga_name11').value=fName;
		});
	});
}

// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("productIdNew").value = id;
					document.getElementById("productNameNew").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}

function a(){
	var productName = $("#productName").val();
	var id1 = $("#sampleInputTemp_productId").val();
	var name1= $("#sampleInputTemp_productName").val();
	var id2= $("#inputTemp_productId").val();
	var name2 = $("#inputTemp_productName").val();
	if(productName==name1){
		$("#productIdNew").val(id1);
	}else if(productName==name2){
		$("#productIdNew").val(id2);
	}else if(productName==""){
		$("#productIdNew").val("");
	}
}
function b(){
	var sampleType = $("#sampleType").val();
	var id1 = $("#sampleInputTemp_sampleType_id").val();
	var name1 = $("#sampleInputTemp_sampleType_name").val();
	var id2 = $("#inputTemp_sampleType_id").val();
	var name2 = $("#inputTemp_sampleType_name").val();
	if(sampleType==name1){
		$("#sampleTypeIdNew").val(id1);
	}else if(sampleType==name2){
		$("#sampleTypeIdNew").val(id2);
	}else if(sampleType==""){
		$("#sampleTypeIdNew").val("");
	}
}