$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/sample/encod/orderEncod/editOrderEncod.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/encod/orderEncod/showOrderEncodList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#orderEncod", {
					userId : userId,
					userName : userName,
					formId : $("#orderEncod_id").val(),
					title : $("#orderEncod_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#orderEncod_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
	//判断数量是否为纯数字
	var str = $("#orderEncod_num").val();
	var regexp = /^\d+$/;
	if(!regexp.test(str)){	
		message(biolims.common.NumMustBeNum);
		return;
	}
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
		
	    var orderEncodDetailsDivData = $("#orderEncodDetailsdiv").data("orderEncodDetailsGrid");
		document.getElementById('orderEncodDetailsJson').value = commonGetModifyRecords(orderEncodDetailsDivData);
	    var orderEncodItemDivData = $("#orderEncodItemdiv").data("orderEncodItemGrid");
		document.getElementById('orderEncodItemJson').value = commonGetModifyRecords(orderEncodItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/sample/encod/orderEncod/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/sample/encod/orderEncod/copyOrderEncod.action?id=' + $("#orderEncod_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#orderEncod_id").val() + "&tableId=orderEncod");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#orderEncod_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.orderCode,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/sample/encod/orderEncod/showOrderEncodDetailsList.action", {
				id : $("#orderEncod_id").val()
			}, "#orderEncodDetailspage");
load("/sample/encod/orderEncod/showOrderEncodItemList.action", {
				id : $("#orderEncod_id").val()
			}, "#orderEncodItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);