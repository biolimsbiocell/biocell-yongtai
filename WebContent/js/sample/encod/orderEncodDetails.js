var orderEncodDetailsGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'codeDetails',
		type:"string"
	});
	    fields.push({
		name:'orderEncod-id',
		type:"string"
	});
	    fields.push({
		name:'orderEncod-name',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'codeDetails',
		hidden : false,
		header:biolims.common.codeDetails,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderEncod-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderEncod-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.printingQuantity,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/encod/orderEncod/showOrderEncodDetailsListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.codeDetails;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/sample/encod/orderEncod/delOrderEncodDetails.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
			text : biolims.common.printingQuantity,
				handler : selectorderEncodDialogFun
		});
	opts.tbar.push({
		text : biolims.common.print,
			handler : dayin
	});
	/*opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});*/
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = orderEncodDetailsGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
							orderEncodDetailsGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	orderEncodDetailsGrid=gridEditTable("orderEncodDetailsdiv",cols,loadParam,opts);
	$("#orderEncodDetailsdiv").data("orderEncodDetailsGrid", orderEncodDetailsGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectorderEncodFun(){
	var win = Ext.getCmp('selectorderEncod');
	if (win) {win.close();}
	var selectorderEncod= new Ext.Window({
	id:'selectorderEncod',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectorderEncod.close(); }  }]  }) });  
    selectorderEncod.show(); }
	function setorderEncod(rec){
		var gridGrid = $("#orderEncodDetailsdiv").data("orderEncodDetailsGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('orderEncod-id',rec.get('id'));
			obj.set('orderEncod-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectorderEncod');
		if(win){
			win.close();
		}
	}
	function selectorderEncodDialogFun(){
			var title = '';
			title = biolims.common.printingQuantity;
			var option = {};
			option.width = 400;
			option.height = 300;
			loadDialogPage($("#print_num_div"), title, null, {
				"Confirm" : function() {
					var gridGrid = $("#orderEncodDetailsdiv").data("orderEncodDetailsGrid");
					var selectRecord = gridGrid.getSelectionModel().getSelections(); 
				
						var num=$("#num").val();
						if(selectRecord.length > 0){
							$.each(selectRecord, function(i, obj) {
								obj.set('num',num);
							});
						}else {
							message(biolims.common.selectYouWant);
							return;
						}
						$(this).dialog("close");
				}
			}, true, option);
		}
	function dayin(){
		
		var selectRecord = orderEncodDetailsGrid.getSelectionModel().getSelections();
		if(selectRecord.length>0){
			var str="";
			$.each(selectRecord, function(i, obj) {
				var num=obj.get("num");
				
				for(var i=0;i<num;i++){
					str+=obj.get("codeDetails")+"<br>";
				}
				
			});
			message(str);
		}else{
			message(biolims.common.selectYouWant);
		}
		
	}
	var selorderEncodVal = function(win) {
		var operGrid = orderEncodDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#orderEncodDetailsdiv").data("orderEncodDetailsGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('orderEncod-id',rec.get('id'));
				obj.set('orderEncod-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
