﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleInputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#inputTemp_name").val());
	}else if($("#inputTemp_name").val()==""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}else if($("#sampleInputTemp_name").val()!="" && $("#inputTemp_name").val()!=""){
		$("#inputTempNew_name").val($("#sampleInputTemp_name").val());
	}
	if($("#sampleInputTemp_productName").val()!=$("#inputTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleInputTemp_productId").val());
		$("#productNameNew").val($("#sampleInputTemp_productName").val());
	}
	if($("#sampleInputTemp_sendDate").val()!=$("#inputTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleInputTemp_sendDate").val());
	}
	if($("#sampleInputTemp_inHosNum").val()!=$("#inputTemp_inHosNum").val()){
		$("#inHosNum").css({"background-color":"red","color":"white"});
	}else{
		$("#inHosNumNew").val($("#sampleInputTemp_inHosNum").val());
	}
	if($("#sampleInputTemp_phoneNum").val()!=$("#inputTemp_phoneNum").val()){
		$("#phoneNum").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNumNew").val($("#sampleInputTemp_phoneNum").val());
	}
	if($("#sampleInputTemp_address").val()!=$("#inputTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleInputTemp_address").val());
	}
	if($("#sampleInputTemp_patientName").val()!=$("#inputTemp_patientName").val()){
		$("#aabb").css({"background-color":"red","color":"white"});
	}else{
		$("#ccdd").val($("#sampleInputTemp_patientName").val());
	}
	if($("#sampleInputTemp_age").val()!=$("#inputTemp_age").val()){
		$("#age").css({"background-color":"red","color":"white"});
	}else{
		$("#ageNew").val($("#sampleInputTemp_age").val());
	}
	if($("#sampleInputTemp_endMenstruationDate").val()!=$("#inputTemp_endMenstruationDate").val()){
		$("#endMenstruationDate").css({"background-color":"red","color":"white"});
	}else{
		$("#endMenstruationDateNew").val($("#sampleInputTemp_endMenstruationDate").val());
	}
	if($("#sampleInputTemp_gestationalAge").val()!=$("#inputTemp_gestationalAge").val()){
		$("#gestationalAge").css({"background-color":"red","color":"white"});
	}else{
		$("#gestationalAgeNew").val($("#sampleInputTemp_gestationalAge").val());
	}
	if($("#sampleInputTemp_weight").val()!=$("#inputTemp_weight").val()){
		$("#weight").css({"background-color":"red","color":"white"});
	}else{
		$("#weightNew").val($("#sampleInputTemp_weight").val());
	}
	if($("#sampleInputTemp_diagnosis").val()!=$("#inputTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleInputTemp_diagnosis").val());
	}
	if($("#sampleInputTemp_medicalHistory").val()!=$("#inputTemp_medicalHistory").val()){
		$("#medicalHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#medicalHistoryNew").val($("#sampleInputTemp_medicalHistory").val());
	}
	if($("#sampleInputTemp_parturitionTime").val()!=$("#inputTemp_parturitionTime").val()){
		$("#parturitionTime").css({"background-color":"red","color":"white"});
	}else{
		$("#parturitionTimeNew").val($("#sampleInputTemp_parturitionTime").val());
	}
	if($("#sampleInputTemp_gestationIVF").val()!=$("#inputTemp_gestationIVF").val()){
		$("#inputTempNew_gestationIVF").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_badMotherhood").val()!=$("#inputTemp_badMotherhood").val()){
		$("#badMotherhood").css({"background-color":"red","color":"white"});
	}else{
		$("#badMotherhoodNew").val($("#sampleInputTemp_badMotherhood").val());
	}
	if($("#sampleInputTemp_outTransfusion").val()!=$("#inputTemp_outTransfusion").val()){
		$("#inputTempNew_outTransfusion").css({"background-color":"red","color":"white"});
	}else{
		$("#inputTempNew_outTransfusion").val();
	}
	if($("#sampleInputTemp_organGrafting").val()!=$("#inputTemp_organGrafting").val()){
		$("#inputTempNew_organGrafting").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_stemCellsCure").val()!=$("#inputTemp_stemCellsCure").val()){
		$("#inputTempNew_stemCellsCure").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_immuneCure").val()!=$("#inputTemp_immuneCure").val()){
		$("#inputTempNew_immuneCure").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_embryoType").val()!=$("#inputTemp_embryoType").val()){
		$("#inputTempNew_embryoType").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_testPattern").val()!=$("#inputTemp_testPattern").val()){
		$("#inputTempNew_testPattern").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_trisome21Value").val()!=$("#inputTemp_trisome21Value").val()){
		$("#trisome21Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome21ValueNew").val($("#sampleInputTemp_trisome21Value").val());
	}
	if($("#sampleInputTemp_trisome18Value").val()!=$("#inputTemp_trisome18Value").val()){
		$("#trisome18Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome18ValueNew").val($("#sampleInputTemp_trisome18Value").val());
	}
	if($("#sampleInputTemp_NT").val()!=$("#inputTemp_NT").val()){
		$("#NT").css({"background-color":"red","color":"white"});
	}else{
		$("#NTNew").val($("#sampleInputTemp_NT").val());
	}
	if($("#sampleInputTemp_coupleChromosome").val()!=$("#inputTemp_coupleChromosome").val()){
		$("#inputTempNew_coupleChromosome").css({"background-color":"red","color":"white"});
	}
	if($("#sampleInputTemp_doctor").val()!=$("#inputTemp_doctor").val()){
		$("#doctor").css({"background-color":"red","color":"white"});
	}else{
		$("#doctorNew").val($("#sampleInputTemp_doctor").val());
	}
	if($("#sampleInputTemp_applicatioDate").val()!=$("#inputTemp_applicatioDate").val()){
		$("#applicatioDate").css({"background-color":"red","color":"white"});
	}else{
		$("#applicatioDateNew").val($("#sampleInputTemp_applicatioDate").val());
	}
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {
	
	//检测项目验证
	var productNameNew = $("#productNameNew").val();
	if(productNameNew==""){
		message(biolims.sample.productNameIsEmpty);
		return;
	}
//	//家庭住址的验证
//	var addressNew = $("#addressNew").val();
//	if(addressNew==""){
//		message("家庭住址不能为空！");
//		return;
//	}
	
	var nextStepFlow =$("#sampleInputTemp_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.pleaseSelectNextFlow);
		return;
	}
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleTemplate_id").val(),
		title : $("#sampleTemplate_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTemplate_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action";
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleInputTemp_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleInputTemp_id").val()) {
				commonChangeState("formId=" + $("#sampleInputTemp_id").val()
						+ "&tableId=SampleInputTemp");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : '青岛市妇女儿童医院高通量基因测序产前筛查临床申请单审核',
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : biolims.common.selectSampleType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleInputTemp_sampleType_id").value = id;
	document.getElementById("sampleInputTemp_sampleType_name").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 收据类型
function receiptTypeFun() {
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
	var receiptTypeFun = new Ext.Window(
			{
				id : 'receiptTypeFun',
				modal : true,
				title : biolims.sample.selectReceiptType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=sjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						receiptTypeFun.close();
					}
				} ]
			});
	receiptTypeFun.show();
}
function setsjlx(id, name) {
	document.getElementById("sampleInputTemp_receiptType").value = id;
	document.getElementById("sampleInputTemp_receiptType_name").value = name;
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
}

// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleInputTemp_phoneNum").val())) {
		// return;
		var id = $("#sampleInputTemp_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.phoneRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputPhone);
	}
}
function change() {
	var selected = $('#sampleInputTemp_embryoType option:selected').val();
	if (selected == "2") {
		$("#sampleInputTemp_message").css("display", "");
	}
}

function checkChange() {
	var selected = $('#sampleInputTemp_coupleChromosome option:selected').val();
	if (selected == "2") {
		$("#sampleInputTemp_remind").css("display", "");
	}
}


// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("productIdNew").value = id;
					document.getElementById("productNameNew").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}

function a(){
	var productName = $("#productName").val();
	var id1 = $("#sampleInputTemp_productId").val();
	var name1= $("#sampleInputTemp_productName").val();
	var id2= $("#inputTemp_productId").val();
	var name2 = $("#inputTemp_productName").val();
	if(productName==name1){
		$("#productIdNew").val(id1);
	}else if(productName==name2){
		$("#productIdNew").val(id2);
	}else if(productName==""){
		$("#productIdNew").val("");
	}
}