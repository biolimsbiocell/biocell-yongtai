$(function() {
	
	viewSampleType();
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	

function add() {
	window.location = window.ctx + "/sample/sampleCancerTemp/editSampleCancerTemp.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleCancerTemp/showSampleCancerTempList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleCancerTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleCancerTemp_id").val(),
					title : $("#sampleCancerTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleCancerTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
		makeSampleType();
//	    var sampleCancerTempPersonnelDivData = $("#sampleCancerTempPersonneldiv").data("sampleCancerTempPersonnelGrid");
//		document.getElementById('sampleCancerTempPersonnelJson').value = commonGetModifyRecords(sampleCancerTempPersonnelDivData);
//	    var sampleCancerTempItemDivData = $("#sampleCancerTempItemdiv").data("sampleCancerTempItemGrid");
//		document.getElementById('sampleCancerTempItemJson').value = commonGetModifyRecords(sampleCancerTempItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/sample/sampleCancerTemp/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		


function makeSampleType(){
	
	var checks = "";
	$("[id='sck_checkedBoxTest']:checked").each(function() {
		checks += $(this).val() + ",";
	});
	 
	document.getElementById("sampleCancerTemp_sampleTypeId").value = checks;
	
	
}
/**
 * 复选框选中方法
 */
function viewSampleType() {
	var str1 = document
	.getElementById("sampleCancerTemp_sampleTypeId").value;
	$("input[id='sck_checkedBoxTest']").each(function() {
	if (str1.indexOf($(this).val()) >= 0) {
		$(this).attr("checked", true);
		}
	});
}

function editCopy() {
	window.location = window.ctx + '/sample/sampleCancerTemp/copySampleCancerTemp.action?id=' + $("#sampleCancerTemp_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#sampleCancerTemp_id").val() + "&tableId=sampleCancerTemp");
}


function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleCancerTemp_orderNumber").val());
	nsc.push(biolims.sample.orderNumIsEmpty);
	fs.push($("#sampleCancerTemp_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#sampleCancerTemp_name").val());
	nsc.push(biolims.common.nameIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.testOrder,
	    	   contentEl:'markup'
	       } ]
	   });
});

//加载明细款的主要load
//load("/sample/sampleCancerTemp/showSampleCancerTempPersonnelList.action", {
//				id : $("#sampleCancerTemp_id").val()
//			}, "#sampleCancerTempPersonnelpage");
//load("/sample/sampleCancerTemp/showSampleCancerTempItemList.action", {
//				id : $("#sampleCancerTemp_id").val()
//			}, "#sampleCancerTempItempage");


var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	// 选择检查项目
	function voucherProductFun() {
		var win = Ext.getCmp('voucherProductFun');
		if (win) {
			win.close();
		}
		var voucherProductFun = new Ext.Window(
				{
					id : 'voucherProductFun',
					modal : true,
					title : biolims.QPCR.selectProject,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							voucherProductFun.close();
						}
					} ]
				});
		voucherProductFun.show();
	}
	/*
	 * ajax 异步
	 * */
	function setProductFun(id, name) {
		var productName = "";
		ajax(
				"post",
				"/com/biolims/system/product/findProductToSample.action",
				{
					code : id
				},
				function(data) {

					if (data.success) {
						$.each(data.data, function(i, obj) {
							productName += obj.name + ",";
						});
						document.getElementById("sampleCancerTemp_productId").value = id;
						document.getElementById("sampleCancerTemp_productName").value = productName;
					}
				}, null);
		var win = Ext.getCmp('voucherProductFun');
		if (win) {
			win.close();
		}
	}
	/**
	 * 选择肿瘤类型
	 */
	
	function WeiChatCancerTypeFun() {
		var win = Ext.getCmp('WeiChatCancerTypeFun');
		if (win) {
			win.close();
		}
		var WeiChatCancerTypeFun = new Ext.Window(
				{
					id : 'WeiChatCancerTypeFun',
					modal : true,
					title : biolims.QPCR.selectProject,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sample/sampleCancerTemp/weiChatCancerTypeSelect.action?flag=WeiChatCancerTypeFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							WeiChatCancerTypeFun.close();
						}
					} ]
				});
		WeiChatCancerTypeFun.show();
	}
	/*
	 * 
	 * */
	function setWeiChatCancerTypeFun(rec) {
	
	document.getElementById("sampleCancerTemp_cancerType_Id").value = rec.get("id");
	document.getElementById("sampleCancerTemp_cancerType_cancerTypeName").value = rec.get("cancerTypeName");
		var win = Ext.getCmp('WeiChatCancerTypeFun');
		if (win) {
			win.close();
		}
	}
	
	/**
	 * 选择肿瘤类型1
	 */
	
	function WeiChatCancerTypeOneFun() {
		if($("#sampleCancerTemp_cancerType_Id").val()==""||$("#sampleCancerTemp_cancerType_Id").val()==null){
			
			message(biolims.sample.pleaseSelectTumorType);
			Ext.getCmp('WeiChatCancerTypeOneFun').close();
		}
		var win = Ext.getCmp('WeiChatCancerTypeOneFun');
		if (win) {
			win.close();
		}
		var WeiChatCancerTypeOneFun = new Ext.Window(
				{
					id : 'WeiChatCancerTypeOneFun',
					modal : true,
					title : biolims.QPCR.selectProject,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/sampleCancerTemp/weiChatCancerTypeSelectOne.action?code="+$("#sampleCancerTemp_cancerType_Id").val()+"&flag=WeiChatCancerTypeOneFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							WeiChatCancerTypeOneFun.close();
						}
					} ]
				});
		WeiChatCancerTypeOneFun.show();
	}
	/*
	 * 
	 * */
	function setWeiChatCancerTypeOneFun(rec) {
	
	document.getElementById("sampleCancerTemp_cancerTypeSeedOne_Id").value = rec.get("id");
	document.getElementById("sampleCancerTemp_cancerTypeSeedOne_cancerTypeName").value = rec.get("cancerTypeName");
		var win = Ext.getCmp('WeiChatCancerTypeOneFun');
		if (win) {
			win.close();
		}
	}
	
	/**
	 * 选择肿瘤类型2
	 */
	
	function WeiChatCancerTypeTwoFun() {
		if($("#sampleCancerTemp_cancerType_Id").val()==""||$("#sampleCancerTemp_cancerType_Id").val()==null){
			message(biolims.sample.pleaseSelectTumorType);
			Ext.getCmp('WeiChatCancerTypeTwoFun').close();
		}
		var win = Ext.getCmp('WeiChatCancerTypeTwoFun');
		if (win) {
			win.close();
		}
		var WeiChatCancerTypeTwoFun = new Ext.Window(
				{
					id : 'WeiChatCancerTypeTwoFun',
					modal : true,
					title : biolims.QPCR.selectProject,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/sampleCancerTemp/weiChatCancerTypeSelectTwo.action?code="+$("#sampleCancerTemp_cancerType_Id").val()+"&flag=WeiChatCancerTypeTwoFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							WeiChatCancerTypeTwoFun.close();
						}
					} ]
				});
		WeiChatCancerTypeTwoFun.show();
	}
	/*
	 * 
	 * */
	function setWeiChatCancerTypeTwoFun(rec) {
	
	document.getElementById("sampleCancerTemp_cancerTypeSeedTwo_Id").value = rec.get("id");
	document.getElementById("sampleCancerTemp_cancerTypeSeedTwo_cancerTypeName").value = rec.get("cancerTypeName");
		var win = Ext.getCmp('WeiChatCancerTypeTwoFun');
		if (win) {
			win.close();
		}
	}
	
	
	/**
	 * 选择电子病历编号
	 */
	
	function crmPatientTypeTwoFun() {
		//alert(encodeURIComponent($("#sampleCancerTemp_name").val()));
		var win = Ext.getCmp('crmPatientTypeTwoFun');
		if (win) {
			win.close();
		}
		var crmPatientTypeTwoFun = new Ext.Window(
				{
					id : 'crmPatientTypeTwoFun',
					modal : true,
					title : biolims.QPCR.selectProject,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/crm/customer/patient/crmPatientSelectTwo.action?code="+encodeURIComponent(encodeURIComponent($("#sampleCancerTemp_name").val()))+"&flag=CrmPatientTwoFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							crmPatientTypeTwoFun.close();
						}
					} ]
				});
		crmPatientTypeTwoFun.show();
	}
	/*
	 * 
	 * */
	function setCrmPatientTwoFun(rec) {
	
	document.getElementById("sampleCancerTemp_medicalNumber").value = rec.get("id");
		var win = Ext.getCmp('crmPatientTypeTwoFun');
		if (win) {
			win.close();
		}
	}
	
	
	
	