﻿
var sampleSystemItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
//	   fields.push({
//		name:'temperature',
//		type:"string"
//	});
	    fields.push({
		name:'sampleReceive-id',
		type:"string"
	});
	    fields.push({
		name:'sampleReceive-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.value,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'temperature',
//		hidden : false,
//		header:'温度',
//		width:40*6,
//		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
//	});
	cm.push({
		dataIndex:'sampleReceive-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleReceive-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleReceive/showsampleSystemItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.packagingSystem;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	if($("#sampleReceive_stateName").val()!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/sample/sampleReceive/delsampleSystemItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				sampleSystemItemGrid.getStore().commitChanges();
				sampleSystemItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectgoodsSamplePackFun
//		});
//	
//	
//	
//	
//	
//	
//	
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
//
//	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = sampleSystemItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
                			sampleSystemItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
	sampleSystemItemGrid=gridEditTable("sampleSystemItemdiv",cols,loadParam,opts);
	$("#sampleSystemItemdiv").data("sampleSystemItemGrid", sampleSystemItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectsampleInfoFun(){
	var win = Ext.getCmp('selectsampleInfo');
	if (win) {win.close();}
	var selectsampleInfo= new Ext.Window({
	id:'selectgoodsSamplePack',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/SampleReceiveSelect.action?flag=goodsSamplePack' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectgoodsSamplePack.close(); }  }]  });     selectgoodsSamplePack.show(); }
	function setgoodsSamplePack(id,name){
		var gridGrid = $("#sampleSystemItemdiv").data("sampleSystemItemdiv");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('sampleReceive-id',id);
			obj.set('sampleReceive-name',name);
		});
		var win = Ext.getCmp('selectgoodsSamplePack');
		if(win){
			win.close();
		}
	}
	
