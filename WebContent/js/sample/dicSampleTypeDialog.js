var dicSampleTypeDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'orderNumber',
		type:"string"
	});
	    fields.push({
		name:'type',
		type:"string"
	});
	    fields.push({
			name:'nextFlow',
			type:"string"
		});
		    fields.push({
			name:'nextFlowId',
			type:"string"
		});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
			name:'stateName',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.sampleTypeId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sampleType,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.sample.code,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.sample.note,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderNumber',
		header:biolims.common.orderNumber,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'type',
		header:biolims.common.type,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'nextFlowId',
		header:biolims.common.nextFlowId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:70*6,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:10*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:10*10,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/dicSampleType/showDicSampleTypeListJson.action";
	var opts={};
	opts.title=biolims.common.sampleType;
	opts.width = document.body.clientWidth - 260;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	var a=$("#a").val();
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setDicSampleTypeFun(rec);
		if(a==2){
			setDicSampleType2();
			$("#a").val("");
		}else{
			setDicSampleType();
		}
		
	};
	//dicSampleTypeDialogGrid=gridTable("show_dialog_dicSampleType_div",cols,loadParam,opts);
	dicSampleTypeDialogGrid=gridTable("show_dialog_dicSampleType_div",cols,loadParam,opts);
	$("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid", dicSampleTypeDialogGrid);

});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(dicSampleTypeDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
