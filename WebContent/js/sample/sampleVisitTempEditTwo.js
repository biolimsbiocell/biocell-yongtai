﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleVisitTemp_name").val() != ""){
		$("#visitTempNew_name").val($("#sampleVisitTemp_name").val());
	}
	if($("#visitTemp_name").val() != ""){
		$("#visitTempNew_name").val($("#visitTemp_name").val());
	}
	
	if($("#sampleVisitTemp_productName").val()!=$("#visitTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleVisitTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleVisitTemp_productId").val());
		$("#productNameNew").val($("#sampleVisitTemp_productName").val());
	}
	
	if($("#sampleVisitTemp_sampleType_id").val()!=$("#visitTemp_sampleType_id").val()){
		$("#sampleType").css({"background-color":"red","color":"white"});
	}else{
		$("#sampleTypeIdNew").val($("#sampleVisitTemp_sampleType_id").val());
		$("#sampleTypeNew").val($("#sampleVisitTemp_sampleType_name").val());
	}
	
	if($("#sampleVisitTemp_voucherType_id").val()!=$("#visitTemp_voucherType_id").val()){
		$("#voucherType").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherTypeIdNew").val($("#sampleVisitTemp_voucherType_id").val());
		$("#voucherTypeNew").val($("#sampleVisitTemp_voucherType_name").val());
	}
	
	if($("#sampleVisitTemp_acceptDate").val()!=$("#visitTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleVisitTemp_acceptDate").val());
	}
	
	if($("#sampleVisitTemp_area").val()!=$("#visitTemp_area").val()){
		$("#area").css({"background-color":"red","color":"white"});
	}else{
		$("#areaNew").val($("#sampleVisitTemp_area").val());
	}
	
	if($("#sampleVisitTemp_hospital").val()!=$("#visitTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleVisitTemp_hospital").val());
	}
	
	if($("#sampleVisitTemp_sendDate").val()!=$("#visitTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleVisitTemp_sendDate").val());
	}
	
	if($("#sampleVisitTemp_reportDate").val()!=$("#visitTemp_reportDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleVisitTemp_reportDate").val());
	}
	
	if($("#sampleVisitTemp_inspectionDept").val()!=$("#visitTemp_inspectionDept").val()){
		$("#inspectionDept").css({"background-color":"red","color":"white"});
	}else{
		$("#inspectionDeptNew").val($("#sampleVisitTemp_inspectionDept").val());
	}
	
	if($("#sampleVisitTemp_patientName").val()!=$("#visitTemp_patientName").val()){
		$("#patientName").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameNew").val($("#sampleVisitTemp_patientName").val());
	}
	
	if($("#sampleVisitTemp_gender").val()!=$("#visitTemp_gender").val()){
		$("#visitTempNew_gender").css({"background-color":"red","color":"white"});
	}else{
		$("#visitTempNew_gender").val($("#sampleVisitTemp_gender").val());
	}
	
	if($("#sampleVisitTemp_age").val()!=$("#visitTemp_age").val()){
		$("#age").css({"background-color":"red","color":"white"});
	}else{
		$("#ageNew").val($("#sampleVisitTemp_age").val());
	}
	
	if($("#sampleVisitTemp_birthday").val()!=$("#visitTemp_birthday").val()){
		$("#birthday").css({"background-color":"red","color":"white"});
	}else{
		$("#birthdayNew").val($("#sampleVisitTemp_birthday").val());
	}
	
	if($("#sampleVisitTemp_bloodType").val()!=$("#visitTemp_bloodType").val()){
		$("#visitTempNew_bloodType").css({"background-color":"red","color":"white"});
	}else{
		$("#visitTempNew_bloodType").val($("#sampleVisitTemp_bloodType").val());
	}
	
	if($("#sampleVisitTemp_voucherCode").val()!=$("#visitTemp_voucherCode").val()){
		$("#voucherCode").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherCodeNew").val($("#sampleVisitTemp_voucherCode").val());
	}
	
	if($("#sampleVisitTemp_phoneNum").val()!=$("#visitTemp_phoneNum").val()){
		$("#phoneNum").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNumNew").val($("#sampleVisitTemp_phoneNum").val());
	}
	
	if($("#sampleVisitTemp_address").val()!=$("#visitTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleVisitTemp_address").val());
	}
	
	if($("#sampleVisitTemp_amount").val()!=$("#visitTemp_amount").val()){
		$("#amount").css({"background-color":"red","color":"white"});
	}else{
		$("#amountNew").val($("#sampleVisitTemp_amount").val());
	}
	
	if($("#sampleVisitTemp_collectionWay").val()!=$("#visitTemp_collectionWay").val()){
		$("#collectionWay").css({"background-color":"red","color":"white"});
	}else{
		$("#collectionWayNew").val($("#sampleVisitTemp_collectionWay").val());
	}
	
	if($("#sampleVisitTemp_reason2").val()!=$("#visitTemp_reason2").val()){
		$("#reason2").css({"background-color":"red","color":"white"});
	}else{
		$("#reason2New").val($("#sampleVisitTemp_reason2").val());
	}
	
	if($("#sampleVisitTemp_gatherPart").val()!=$("#visitTemp_gatherPart").val()){
		$("#gatherPart").css({"background-color":"red","color":"white"});
	}else{
		$("#gatherPartNew").val($("#sampleVisitTemp_gatherPart").val());
	}
	
	if($("#sampleVisitTemp_confirmAgeTwo").val()!=$("#visitTemp_confirmAgeTwo").val()){
		$("#confirmAgeTwo").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeTwoNew").val($("#sampleVisitTemp_confirmAgeTwo").val());
	}
	
	if($("#sampleVisitTemp_diagnosis").val()!=$("#visitTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleVisitTemp_diagnosis").val());
	}
	
	if($("#sampleVisitTemp_bloodHistory").val()!=$("#visitTemp_bloodHistory").val()){
		$("#bloodHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodHistoryNew").val($("#sampleVisitTemp_bloodHistory").val());
	}
	
	if($("#sampleVisitTemp_medicalHistory").val()!=$("#visitTemp_medicalHistory").val()){
		$("#medicalHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#medicalHistoryNew").val($("#sampleVisitTemp_medicalHistory").val());
	}
	
	if($("#sampleVisitTemp_tumorHistory").val()!=$("#visitTemp_tumorHistory").val()){
		$("#tumorHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorHistoryNew").val($("#sampleVisitTemp_tumorHistory").val());
	}
	
	if($("#sampleVisitTemp_serialNum").val()!=$("#visitTemp_serialNum").val()){
		$("#serialNum").css({"background-color":"red","color":"white"});
	}else{
		$("#serialNumNew").val($("#sampleVisitTemp_serialNum").val());
	}
	
	if($("#sampleVisitTemp_isInsure").val()!=$("#visitTemp_isInsure").val()){
		$("#visitTempNew_isInsure").css({"background-color":"red","color":"white"});
	}else{
		$("#visitTempNew_isInsure").val($("#sampleVisitTemp_isInsure").val());
	}
	
	if($("#sampleVisitTemp_isInvoice").val()!=$("#visitTemp_isInvoice").val()){
		$("#visitTempNew_isInvoice").css({"background-color":"red","color":"white"});
	}else{
		$("#visitTempNew_isInvoice").val($("#sampleVisitTemp_isInvoice").val());
	}
	
	if($("#sampleVisitTemp_reason").val()!=""){
		$("#reasonNew").val($("#sampleVisitTemp_reason").val());
	}
	if($("#visitTemp_reason").val()!=""){
		$("#reasonNew").val($("#visitTemp_reason").val());
	}
	if($("#sampleVisitTemp_reason").val()!="" && $("#visitTemp_reason").val()!=""){
		$("#reasonNew").val($("#sampleVisitTemp_reason").val());
	}
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}

load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");

$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {	

//	var reg =$("#sampleVisitTemp_address").val();
//	if(reg==""){
//		message("家庭住址不能为空！");
//		return;
//	}
//	
	var nextStepFlow =$("#visitTempNew_nextStepFlow").val();
	if(nextStepFlow==""){
		message("请选择下一步流向");
		return;
	}

	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleVisitTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleVisitTemp_id").val(),
					title : $("#sampleVisitTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleVisitTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});


function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action?saveType="+$("#saveType").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInputTemp/copySampleInput.action?id=' + $("#sampleVisitTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleVisitTemp_id").val()){
		commonChangeState("formId=" + $("#sampleVisitTemp_id").val() + "&tableId=SampleVisitTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push("信息录入编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'基因突变信息录入审核',
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : '选择类型',
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleTypeIdNew").value = id;
		 document.getElementById("sampleTypeNew").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:'选择证件类型',layout:'fit',width:600,height:700,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("voucherTypeIdNew").value = id;
			 document.getElementById("voucherTypeNew").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleVisitTemp_voucherType_name").val();
		if(re==""){
			message("证件类型不能为空！");
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleVisitTemp_voucherCode").val())){
				var id = $("#sampleVisitTemp_voucherCode").val();
				ajax("post", "/sample/sampleInputTemp/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message("输入的身份证号重复！");
						}
					} 
				}, null);
			}else{
				message("请输入正确的证件号码!");
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleVisitTemp_phoneNum").val())){
				//return;
				var id = $("#sampleVisitTemp_phoneNum").val();
				ajax("post", "/sample/sampleInputTemp/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message("输入的手机号重复！");
						}
					} 
				}, null);
			}else{
				message("请输入正确的手机号码!");
			}
		}

		//家庭住址验证	 
		function checkAddress(){
			var address = $("#sampleVisitTemp_address").val();
			if(address==""){
				message("家庭地址不能为空！");
		}
}
		//B超异常提醒验证
		function change(){
			var reg = $('#sampleVisitTemp_embryoType option:selected').val();
			if(reg =="2"){
				$("#sampleVisitTemp_messages").css("display","");
			}
		}
		
		
		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 600,
						height : 700,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("productIdNew").value = id;
							document.getElementById("productNameNew").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}		
