var dicSampleTypeTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.dicSampleType.id,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.dicSampleType.stateName,
	});
	
	/*    fields.push({
		"data":"nextFlowId",
		"title":biolims.dicSampleType.nextFlowId,
	});
	
	    fields.push({
		"data":"nextFlow",
		"title":biolims.dicSampleType.nextFlow,
	});*/
	
	    fields.push({
		"data":"type-id",
		"title":biolims.common.typeID
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.dicSampleType.type
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.dicSampleType.name,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.dicSampleType.note,
	});
	
	    fields.push({
		"data":"code",
		"title":biolims.dicSampleType.code,
	});
	
	    fields.push({
		"data":"orderNumber",
		"title":biolims.dicSampleType.ordeRnumber,
	});
	
	    fields.push({
		"data":"first",
		"title":biolims.dicSampleType.first,
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.no;
			}
			if(data == "1") {
				return biolims.common.yes;
			} else {
				return '';
			}
		}
	});
	   
	    fields.push({
		"data":"picCode",
		"title":biolims.dicSampleType.picCode,
	});
	
	    fields.push({
		"data":"picCodeName",
		"title":biolims.dicSampleType.picCodeName,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.common.state+"ID"
	});
	 
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "DicSampleType"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/sample/dicSampleType/showDicSampleTypeTableJson.action",
	 fields, null)
	dicSampleTypeTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(dicSampleTypeTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/sample/dicSampleType/editDicSampleType.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/sample/dicSampleType/editDicSampleType.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/sample/dicSampleType/viewDicSampleType.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.dicSampleType.id
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.dicSampleType.stateName
		});
	
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.common.typeID
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.dicSampleType.type
	});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.dicSampleType.name
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.dicSampleType.note
		});
	   fields.push({
		    "searchName":"code",
			"type":"input",
			"txt":biolims.dicSampleType.code
		});
	   fields.push({
		    "searchName":"ordeRnumber",
			"type":"input",
			"txt":biolims.dicSampleType.ordeRnumber
		});
	   fields.push({
		    "searchName":"first",
			"type":"input",
			"txt":biolims.dicSampleType.first
		});
	   fields.push({
		    "searchName":"picCode",
			"type":"input",
			"txt":biolims.dicSampleType.picCode
		});
	   fields.push({
		    "searchName":"picCodeName",
			"type":"input",
			"txt":biolims.dicSampleType.picCodeName
		});
	fields.push({
	    "type":"input",
		"searchName":"state",
		"txt":biolims.dicSampleType.state+"ID"
	});
	fields.push({
		"type":"table",
		"table":dicSampleTypeTable
	});
	return fields;
}
