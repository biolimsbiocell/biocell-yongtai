var showDicUnusualMethodGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"String"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'sysCode',
		type:"string"
	});
	    fields.push({
		name:'type',
		type:"string"
	});
    fields.push({
		name:'orderNumber',
		type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'name',
		header:biolims.sample.unusualMethod,
		width:50*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'code',
		header:'功能码',
		hidden : true,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'note',
		header:biolims.sample.note,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sysCode',
		header:'系统标识',
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'type',
		header:biolims.common.type,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'orderNumber',
		header:biolims.user.sortingNumber,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	/*var model=$("#model").val();
	var sequencingPlatform = $("#sequencingPlatform").val();
	var productId=$("#productId").val();
	var sampleTypeId=$("#sampleTypeId").val();*/
	var loadParam={};
	
	loadParam.url=ctx+"/sample/sampleAbnormal/showshowDicUnusualMethodAllJson.action";
	
	var opts={};
	opts.title=biolims.common.unusualMethod;
	
	opts.height=400;
	opts.width=450;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	var n=$("#n").val();
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setNextFlowFun(rec);
		if(n==2){
			setNextFlow2();
		}else if(n==3){
			setNextFlow3();
		}else if(n==4){
			setNextFlow4();
		}else{
			setNextFlow();
		}
		
	};
	showDicUnusualMethodGrid=gridTable("show_DicUnusualMethod_div1",cols,loadParam,opts);
	$("#show_DicUnusualMethod_div1").data("showDicUnusualMethodGrid", showDicUnusualMethodGrid);
	
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "biolims.common.search", null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(shownextFlowDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
