﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#sampleBloodDiseaseTemp_name").val() != ""){
		$("#bloodDiseaseTempNew_name").val($("#sampleBloodDiseaseTemp_name").val());
	}
	if($("#bloodDiseaseTemp_name").val() != ""){
		$("#bloodDiseaseTempNew_name").val($("#bloodDiseaseTemp_name").val());
	}
	
	if($("#sampleBloodDiseaseTemp_productName").val()!=$("#bloodDiseaseTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleBloodDiseaseTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleBloodDiseaseTemp_productId").val());
		$("#productNameNew").val($("#sampleBloodDiseaseTemp_productName").val());
	}
	
	if($("#sampleBloodDiseaseTemp_sampleType_id").val()!=$("#bloodDiseaseTemp_sampleType_id").val()){
		$("#sampleType").css({"background-color":"red","color":"white"});
	}else{
		$("#sampleTypeIdNew").val($("#sampleBloodDiseaseTemp_sampleType_id").val());
		$("#sampleTypeNew").val($("#sampleBloodDiseaseTemp_sampleType_name").val());
	}
	
	if($("#sampleBloodDiseaseTemp_voucherType_id").val()!=$("#bloodDiseaseTemp_voucherType_id").val()){
		$("#voucherType").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherTypeIdNew").val($("#sampleBloodDiseaseTemp_voucherType_id").val());
		$("#voucherTypeNew").val($("#sampleBloodDiseaseTemp_voucherType_name").val());
	}
	
	if($("#sampleBloodDiseaseTemp_acceptDate").val()!=$("#bloodDiseaseTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleBloodDiseaseTemp_acceptDate").val());
	}
	
	if($("#sampleBloodDiseaseTemp_area").val()!=$("#bloodDiseaseTemp_area").val()){
		$("#area").css({"background-color":"red","color":"white"});
	}else{
		$("#areaNew").val($("#sampleBloodDiseaseTemp_area").val());
	}
	
	if($("#sampleBloodDiseaseTemp_hospital").val()!=$("#bloodDiseaseTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleBloodDiseaseTemp_hospital").val());
	}
	
	if($("#sampleBloodDiseaseTemp_sendDate").val()!=$("#bloodDiseaseTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleBloodDiseaseTemp_sendDate").val());
	}
	
	if($("#sampleBloodDiseaseTemp_reportDate").val()!=$("#bloodDiseaseTemp_reportDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleBloodDiseaseTemp_reportDate").val());
	}
	
	if($("#sampleBloodDiseaseTemp_confirmAgeOne").val()!=$("#bloodDiseaseTemp_confirmAgeOne").val()){
		$("#confirmAgeOne").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeOneNew").val($("#sampleBloodDiseaseTemp_confirmAgeOne").val());
	}
	
	if($("#sampleBloodDiseaseTemp_doctor").val()!=$("#bloodDiseaseTemp_doctor").val()){
		$("#doctor").css({"background-color":"red","color":"white"});
	}else{
		$("#doctorNew").val($("#sampleBloodDiseaseTemp_doctor").val());
	}
	
	if($("#sampleBloodDiseaseTemp_phone").val()!=$("#bloodDiseaseTemp_phone").val()){
		$("#phone").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNew").val($("#sampleBloodDiseaseTemp_phone").val());
	}
	
	if($("#sampleBloodDiseaseTemp_patientName").val()!=$("#bloodDiseaseTemp_patientName").val()){
		$("#patientName").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameNew").val($("#sampleBloodDiseaseTemp_patientName").val());
	}
	
	if($("#sampleBloodDiseaseTemp_gender").val()!=$("#bloodDiseaseTemp_gender").val()){
		$("#bloodDiseaseTempNew_gender").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodDiseaseTempNew_gender").val($("#sampleBloodDiseaseTemp_gender").val());
	}
	
	if($("#sampleBloodDiseaseTemp_age").val()!=$("#bloodDiseaseTemp_age").val()){
		$("#age").css({"background-color":"red","color":"white"});
	}else{
		$("#ageNew").val($("#sampleBloodDiseaseTemp_age").val());
	}
	
	if($("#sampleBloodDiseaseTemp_voucherCode").val()!=$("#bloodDiseaseTemp_voucherCode").val()){
		$("#voucherCode").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherCodeNew").val($("#sampleBloodDiseaseTemp_voucherCode").val());
	}
	
	if($("#sampleBloodDiseaseTemp_phoneNum2").val()!=$("#bloodDiseaseTemp_phoneNum2").val()){
		$("#phoneNum2").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNum2New").val($("#sampleBloodDiseaseTemp_voucherCode").val());
	}
	
	if($("#sampleBloodDiseaseTemp_emailAddress").val()!=$("#bloodDiseaseTemp_emailAddress").val()){
		$("#emailAddress").css({"background-color":"red","color":"white"});
	}else{
		$("#emailAddressNew").val($("#sampleBloodDiseaseTemp_emailAddress").val());
	}
	
	if($("#sampleBloodDiseaseTemp_address").val()!=$("#bloodDiseaseTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleBloodDiseaseTemp_address").val());
	}
	
	if($("#sampleBloodDiseaseTemp_gestationalAge").val()!=$("#bloodDiseaseTemp_gestationalAge").val()){
		$("#gestationalAge").css({"background-color":"red","color":"white"});
	}else{
		$("#gestationalAgeNew").val($("#sampleBloodDiseaseTemp_gestationalAge").val());
	}
	
	if($("#sampleBloodDiseaseTemp_reason").val()!=$("#bloodDiseaseTemp_reason").val()){
		$("#reason").css({"background-color":"red","color":"white"});
	}else{
		$("#reasonNew").val($("#sampleBloodDiseaseTemp_reason").val());
	}
	
	if($("#sampleBloodDiseaseTemp_reason2").val()!=$("#bloodDiseaseTemp_reason2").val()){
		$("#reason2").css({"background-color":"red","color":"white"});
	}else{
		$("#reason2New").val($("#sampleBloodDiseaseTemp_reason2").val());
	}
	
	if($("#sampleBloodDiseaseTemp_tumorHistory").val()!=$("#bloodDiseaseTemp_tumorHistory").val()){
		$("#tumorHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#tumorHistoryNew").val($("#sampleBloodDiseaseTemp_tumorHistory").val());
	}
	
	if($("#sampleBloodDiseaseTemp_firstTransfusionDate").val()!=$("#bloodDiseaseTemp_firstTransfusionDate").val()){
		$("#firstTransfusionDate").css({"background-color":"red","color":"white"});
	}else{
		$("#firstTransfusionDateNew").val($("#sampleBloodDiseaseTemp_firstTransfusionDate").val());
	}
	
	if($("#sampleBloodDiseaseTemp_medicine").val()!=$("#bloodDiseaseTemp_medicine").val()){
		$("#medicine").css({"background-color":"red","color":"white"});
	}else{
		$("#medicineNew").val($("#sampleBloodDiseaseTemp_medicine").val());
	}
	
	if($("#sampleBloodDiseaseTemp_medicineType").val()!=$("#bloodDiseaseTemp_medicineType").val()){
		$("#bloodDiseaseTempNew_medicineType").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodDiseaseTempNew_medicineType").val($("#sampleBloodDiseaseTemp_medicineType").val());
	}
	
	if($("#sampleBloodDiseaseTemp_radioactiveRays").val()!=$("#bloodDiseaseTemp_radioactiveRays").val()){
		$("#radioactiveRays").css({"background-color":"red","color":"white"});
	}else{
		$("#radioactiveRaysNew").val($("#sampleBloodDiseaseTemp_radioactiveRays").val());
	}
	
	if($("#sampleBloodDiseaseTemp_cadmium").val()!=$("#bloodDiseaseTemp_cadmium").val()){
		$("#cadmium").css({"background-color":"red","color":"white"});
	}else{
		$("#cadmiumNew").val($("#sampleBloodDiseaseTemp_cadmium").val());
	}
	
	if($("#sampleBloodDiseaseTemp_trisome21Value").val()!=$("#bloodDiseaseTemp_trisome21Value").val()){
		$("#trisome21Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome21ValueNew").val($("#sampleBloodDiseaseTemp_trisome21Value").val());
	}
	
	if($("#sampleBloodDiseaseTemp_trisome18Value").val()!=$("#bloodDiseaseTemp_trisome18Value").val()){
		$("#trisome18Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome18ValueNew").val($("#sampleBloodDiseaseTemp_trisome18Value").val());
	}
	
	if($("#sampleBloodDiseaseTemp_trisome13Value").val()!=$("#bloodDiseaseTemp_trisome13Value").val()){
		$("#trisome13Value").css({"background-color":"red","color":"white"});
	}else{
		$("#trisome13ValueNew").val($("#sampleBloodDiseaseTemp_trisome13Value").val());
	}
	
	if($("#sampleBloodDiseaseTemp_confirmAgeTwo").val()!=$("#bloodDiseaseTemp_confirmAgeTwo").val()){
		$("#confirmAgeTwo").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeTwoNew").val($("#sampleBloodDiseaseTemp_confirmAgeTwo").val());
	}
	
	if($("#sampleBloodDiseaseTemp_confirmAgeThree").val()!=$("#bloodDiseaseTemp_confirmAgeThree").val()){
		$("#confirmAgeThree").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeThreeNew").val($("#sampleBloodDiseaseTemp_confirmAgeThree").val());
	}
	
	if($("#sampleBloodDiseaseTemp_confirmAge").val()!=$("#bloodDiseaseTemp_confirmAge").val()){
		$("#confirmAge").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeNew").val($("#sampleBloodDiseaseTemp_confirmAge").val());
	}
	
	if($("#sampleBloodDiseaseTemp_stemCellsCure").val()!=$("#bloodDiseaseTemp_stemCellsCure").val()){
		$("#stemCellsCure").css({"background-color":"red","color":"white"});
	}else{
		$("#stemCellsCureNew").val($("#sampleBloodDiseaseTemp_stemCellsCure").val());
	}
	
	if($("#sampleBloodDiseaseTemp_badMotherhood").val()!=$("#bloodDiseaseTemp_badMotherhood").val()){
		$("#badMotherhood").css({"background-color":"red","color":"white"});
	}else{
		$("#badMotherhoodNew").val($("#sampleBloodDiseaseTemp_badMotherhood").val());
	}
	
	if($("#sampleBloodDiseaseTemp_diagnosis").val()!=$("#bloodDiseaseTemp_diagnosis").val()){
		$("#diagnosis").css({"background-color":"red","color":"white"});
	}else{
		$("#diagnosisNew").val($("#sampleBloodDiseaseTemp_diagnosis").val());
	}
	
	if($("#sampleBloodDiseaseTemp_isInsure").val()!=$("#bloodDiseaseTemp_isInsure").val()){
		$("#bloodDiseaseTempNew_isInsure").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodDiseaseTempNew_isInsure").val($("#sampleBloodDiseaseTemp_isInsure").val());
	}
	
	if($("#sampleBloodDiseaseTemp_privilegeType").val()!=$("#bloodDiseaseTemp_privilegeType").val()){
		$("#bloodDiseaseTempNew_privilegeType").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodDiseaseTempNew_privilegeType").val($("#sampleBloodDiseaseTemp_privilegeType").val());
	}
	
	if($("#sampleBloodDiseaseTemp_linkman").val()!=$("#bloodDiseaseTemp_linkman").val()){
		$("#linkman").css({"background-color":"red","color":"white"});
	}else{
		$("#linkmanNew").val($("#sampleBloodDiseaseTemp_linkman").val());
	}
	
	if($("#sampleBloodDiseaseTemp_isInvoice").val()!=$("#bloodDiseaseTemp_isInvoice").val()){
		$("#bloodDiseaseTempNew_isInvoice").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodDiseaseTempNew_isInvoice").val($("#sampleBloodDiseaseTemp_isInvoice").val());
	}
	
	if($("#sampleBloodDiseaseTemp_money").val()!=$("#bloodDiseaseTemp_money").val()){
		$("#money").css({"background-color":"red","color":"white"});
	}else{
		$("#moneyNew").val($("#sampleBloodDiseaseTemp_money").val());
	}
	
	if($("#sampleBloodDiseaseTemp_bloodHistory").val()!=$("#bloodDiseaseTemp_bloodHistory").val()){
		$("#bloodHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#bloodHistoryNew").val($("#sampleBloodDiseaseTemp_bloodHistory").val());
	}
	
	if($("#sampleBloodDiseaseTemp_paymentUnit").val()!=$("#bloodDiseaseTemp_paymentUnit").val()){
		$("#paymentUnit").css({"background-color":"red","color":"white"});
	}else{
		$("#paymentUnitNew").val($("#sampleBloodDiseaseTemp_paymentUnit").val());
	}
	
	if($("#sampleBloodDiseaseTemp_reportMan_id").val()!=$("#bloodDiseaseTemp_reportMan_id").val()){
		$("#reportMan").css({"background-color":"red","color":"white"});
	}else{
		$("#reportManIdNew").val($("#sampleBloodDiseaseTemp_reportMan_id").val());
		$("#reportManNew").val($("#sampleBloodDiseaseTemp_reportMan_name").val());
	}
	
	if($("#sampleBloodDiseaseTemp_auditMan_id").val()!=$("#bloodDiseaseTemp_auditMan_id").val()){
		$("#auditMan").css({"background-color":"red","color":"white"});
	}else{
		$("#auditManIdNew").val($("#sampleBloodDiseaseTemp_auditMan_id").val());
		$("#auditManNew").val($("#sampleBloodDiseaseTemp_auditMan_name").val());
	}
	
	if($("#sampleBloodDiseaseTemp_note").val()!=""){
		$("#noteNew").val($("#sampleBloodDiseaseTemp_note").val());
	}
	if($("#bloodDiseaseTemp_note").val()!=""){
		$("#noteNew").val($("#bloodDiseaseTemp_note").val());
	}
	if($("#sampleBloodDiseaseTemp_note").val()!="" && $("#bloodDiseaseTemp_note").val()!=""){
		$("#noteNew").val($("#sampleBloodDiseaseTemp_note").val());
	}
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}

load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");

$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {	

//	var reg =$("#sampleBloodDiseaseTemp_address").val();
//	if(reg==""){
//		message("家庭住址不能为空！");
//		return;
//	}

	var nextStepFlow =$("#bloodDiseaseTempNew_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.pleaseSelectNextFlow);
		return;
	}

	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleBloodDiseaseTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleBloodDiseaseTemp_id").val(),
					title : $("#sampleBloodDiseaseTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleBloodDiseaseTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action?saveType="+$("#saveType").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInput/copySampleInput.action?id=' + $("#sampleBloodDiseaseTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleBloodDiseaseTemp_id").val()){
		commonChangeState("formId=" + $("#sampleBloodDiseaseTemp_id").val() + "&tableId=SampleBloodDiseaseTemp");
	}	
});


function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.plasma.bloodDiseaseInfoInput,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleTypeIdNew").value = id;
		 document.getElementById("sampleTypeNew").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:600,height:700,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("voucherTypeIdNew").value = id;
			 document.getElementById("voucherTypeNew").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleBloodDiseaseTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleBloodDiseaseTemp_voucherCode").val())){
				var id = $("#sampleBloodDiseaseTemp_voucherCode").val();
				ajax("post", "/sample/sampleInput/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleBloodDiseaseTemp_phoneNum").val())){
				//return;
				var id = $("#sampleBloodDiseaseTemp_phoneNum").val();
				ajax("post", "/sample/sampleInputTemp/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

		//家庭住址验证	 
		function checkAddress(){
			var address = $("#sampleBloodDiseaseTemp_address").val();
			if(address==""){
				message(biolims.sample.addressIsEmpty);
		}
}
		//B超异常提醒验证
		function change(){
			var reg = $('#sampleBloodDiseaseTemp_embryoType option:selected').val();
			if(reg =="2"){
				$("#sampleBloodDiseaseTemp_messages").css("display","");
			}
		}
		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 600,
						height : 700,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("productIdNew").value = id;
							document.getElementById("productNameNew").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}
		
//检测项目的Id给到	
function a(){
	var productName = $("#productName").val();
	var id1 = $("#sampleBloodDiseaseTemp_productId").val();
	var name1= $("#sampleBloodDiseaseTemp_productName").val();
	var id2= $("#bloodDiseaseTemp_productId").val();
	var name2 = $("#bloodDiseaseTemp_productName").val();
	if(productName==name1){
		$("#productIdNew").val(id1);
	}else if(productName==name2){
		$("#productIdNew").val(id2);
	}else if(productName==""){
		$("#productIdNew").val("");
	}
}

//样本类型
function b(){
	var sampleType = $("#sampleType").val();
	var id1 = $("#sampleBloodDiseaseTemp_sampleType_id").val();
	var name1 = $("#sampleBloodDiseaseTemp_sampleType_name").val();
	var id2 = $("#bloodDiseaseTemp_sampleType_id").val();
	var name2 = $("#bloodDiseaseTemp_sampleType_name").val();
	if(sampleType==name1){
		$("#sampleTypeIdNew").val(id1);
	}else if(sampleType==name2){
		$("#sampleTypeIdNew").val(id2);
	}else if(sampleType==""){
		$("#sampleTypeIdNew").val("");
	}
}
//证件号码
function c(){
	var voucherType = $("#voucherType").val();
	var id1 =$("#sampleBloodDiseaseTemp_voucherType_id").val();
	var name1 = $("#sampleBloodDiseaseTemp_voucherType_name").val();
	var id2 = $("#bloodDiseaseTemp_voucherType_id").val();
	var name2 =$("#bloodDiseaseTemp_voucherType_name").val();
	if(voucherType==name1){
		$("#voucherTypeIdNew").val(id1);
	}else if(voucherType==name2){
		$("#voucherTypeIdNew").val(id2);
	}else if(voucherType==""){
		$("#voucherTypeIdNew").val("");
	}
}		