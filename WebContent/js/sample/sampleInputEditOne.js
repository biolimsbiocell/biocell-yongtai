﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	//验证有无图片如果没有就进行上传图片
	if($("#upload_imga_id").val()==''){
		$(":input").addClass(".text input readonlytrue");
		$("[type='text']").focus(function ()
		{
			message(biolims.sample.pleaseUploadImage);
			return; 
		}) ;
	}
	
	
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInput/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/sample/sampleInput/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {
	//sampleInfo 的图片Id
	var sampleInfoImg = $("#upload_imga_id10").val();
	//sampleInputTemp 的图片Id
	var sampleInputTempimg = $("#upload_imga_id").val();
	if($("#upload_imga_name11").val()!="" && $("#upload_imga_id10").val()!="" && sampleInfoImg == sampleInputTempimg){
//		//检测项目验证
		var productName = $("#sampleInputTemp_productName").val();
		if (productName == "") {
			message(biolims.sample.productNameIsEmpty);
			return;
		}
//		//验证家庭住址
//		var reg = $("#sampleInputTemp_address").val();
//		if (reg == "") {
//			message("家庭住址不能为空！");
//			return;
//		}
////		孕周的验证
//		var sendDate = $("#sampleInputTemp_sendDate").val();
//		var endMenstruationDate =$("#sampleInputTemp_endMenstruationDate").val();
//		var date1 = sendDate.toString().split("-");
//		if(date1!="" && endMenstruationDate!=""){
//			var date2 =  date1[0]+date1[1]+date1[2].substring(0,2);//精确到时分秒的日期转换成年月日
//			var r=date2.replace(/^(\d{4})(\d{2})(\d{2})$/, "$1-$2-$3");//把字符串转换成日期yyyy-MM-dd
//			console.log(r);
//			
//			var date = DateDiff(r,endMenstruationDate);
//			var chu =parseInt(date/7);
//		//	var mo = date%7;
//		//	var zhi = "";
//		//	if(mo==0){
//		//		zhi = chu+"w";
//		//	}else{
//		//		zhi = chu+"w"+"+"+mo;
//		//	}
//			var gestationalAge =$("#sampleInputTemp_gestationalAge").val();
//			var gestationalAgeReg = /^$|^[0-2][0-9][w]\+[1-9]$|^[0-2][0-9][w]$/;
//			if(gestationalAgeReg.test(gestationalAge)){
//				var numAge = gestationalAge.substring(0,2);
//				var sumAge =gestationalAge.substring(4);
//				if((chu-numAge)>7 || (chu-numAge)<-7){
//					if(confirm("注意:孕周录入值与计算值偏差过大，超过一周。是否录入？")){
////						save();
////						return;
//					}else{
//						$("#sampleInputTemp_gestationalAge").val("");
//						document.getElementById("sampleInputTemp_gestationalAge").focus(); 
//						return;
//					}
//				}
//				if(numAge>27){
//					if(confirm("此孕周超过送检孕周要求,是否录入？")){
////						save();
////						return;
//					}else {
//						$("#sampleInputTemp_gestationalAge").val("");
//						document.getElementById("sampleInputTemp_gestationalAge").focus(); 
//						return;
//					}
//				}else if(sumAge>7 && numAge<=11 && sumAge!=""){
//					if(confirm("此孕周超过送检孕周要求,是否录入？")){
////						save();
////						return;
//					}else{
//						$("#sampleInputTemp_gestationalAge").val("");
//						document.getElementById("sampleInputTemp_gestationalAge").focus(); 
//						return;
//					}
//				}
//			}else{
//				message("请输入正确的孕周！");
//				return;
//			}
//		}
//		//体重验证
//		var weight =$("#sampleInputTemp_weight").val();
//		if(weight>100){
//			if(confirm("根据体重系统检测出该孕妇为无创DNA检查慎用人群,是否录入?")){
//				
//			}else {
//				$("#sampleInputTemp_weight").val("");
//				document.getElementById("sampleInputTemp_weight").focus(); 
//				return;
//			}
//		}
		
	}else {
		if($("#upload_imga_id").val()==""){
			message(biolims.sample.pleaseUploadImage2Save);
			return;
		}
	}
	
	
	
	
	
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#sampleTemplate", {
		userId : userId,
		userName : userName,
		formId : $("#sampleTemplate_id").val(),
		title : $("#sampleTemplate_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#sampleTemplate_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	if (checkSubmit() == true) {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInput/save.action?imgId="+$("#upload_imga_id").val()+"&saveType="+$("#saveType").val();
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/sample/sampleInput/copySampleInput.action?id='
			+ $("#sampleInputTemp_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#sampleInputTemp_id").val()) {
				commonChangeState("formId=" + $("#sampleInputTemp_id").val()
						+ "&tableId=SampleInputTemp");
			}
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInputTemp_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : biolims.sample.infoInput,
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

// var item = menu.add({
// text: '复制'
// });
// item.on('click', editCopy);
function sampleKind() {
	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
	var sampleKind = new Ext.Window(
			{
				id : 'sampleKind',
				modal : true,
				title : biolims.sample.selectType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleKind.close();
					}
				} ]
			});
	sampleKind.show();
}
function setyblx(id, name) {
	document.getElementById("sampleInputTemp_sampleType_id").value = id;
	document.getElementById("sampleInputTemp_sampleType_name").value = name;

	var win = Ext.getCmp('sampleKind');
	if (win) {
		win.close();
	}
}

// 收据类型
function receiptTypeFun() {
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
	var receiptTypeFun = new Ext.Window(
			{
				id : 'receiptTypeFun',
				modal : true,
				title : biolims.sample.selectReceiptType,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=sjlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						receiptTypeFun.close();
					}
				} ]
			});
	receiptTypeFun.show();
}
function setsjlx(id, name) {
	document.getElementById("sampleInputTemp_receiptType").value = id;
	document.getElementById("sampleInputTemp_receiptType_name").value = name;
	var win = Ext.getCmp('receiptTypeFun');
	if (win) {
		win.close();
	}
}

// 手机号码验证
function checkPhone() {
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	if (reg.test($("#sampleInputTemp_phoneNum").val())) {
		// return;
		var id = $("#sampleInputTemp_phoneNum").val();
		ajax("post", "/sample/sampleInput/findPhone.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message(biolims.sample.phoneRepeat);
				}
			}
		}, null);
	} else {
		message(biolims.sample.pleaseInputPhone);
	}
}
function change() {
	var selected = $('#sampleInputTemp_embryoType option:selected').val();
	if (selected == "2") {
		$("#sampleInputTemp_message").css("display", "");
	}
}

function checkChange() {
	var selected = $('#sampleInputTemp_coupleChromosome option:selected').val();
	if (selected == "2") {
		$("#sampleInputTemp_remind").css("display", "");
	}
}



/*
 * 上传图片
 */
function upLoadImg1(){
	var isUpload = true;
	var id=$("#template_testType").val();
	var fId=$("#upload_imga_id11").val();
	var fName=$("#upload_imga_name").val();
	
	load("/system/template/template/toSampeUpload.action", { // 是否修改
		fileId:fId,
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			fName=data.fileName;
			fId=data.fileId;
//				$("#template_fileInfo").val(fId);
//				$("#template_fileInfo_name").val(fName);
			 document.getElementById('upload_imga_id').value=fId;
			 document.getElementById('upload_imga_name11').value=fName;
		});
	});
}


// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setProductFun(id, name) {
	var productName = "";
	ajax(
			"post",
			"/com/biolims/system/product/findProductToSample.action",
			{
				code : id,
			},
			function(data) {

				if (data.success) {
					$.each(data.data, function(i, obj) {
						productName += obj.name + ",";
					});
					document.getElementById("sampleInputTemp_productId").value = id;
					document.getElementById("sampleInputTemp_productName").value = productName;
				}
			}, null);
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}
//计算天数差的函数，通用  
function  DateDiff(sDate1,  sDate2){    //sDate1和sDate2是yyyy-MM-dd格式  
    var  aDate,  oDate1,  oDate2,  iDays ;
    aDate  =  sDate1.split("-");
    oDate1 = new Date(aDate[0] , aDate[1] ,aDate[2]); //转换为12-18-2006格式 
    aDate = sDate2.split("-") ;
    oDate2 = new Date(aDate[0] , aDate[1] , aDate[2]);
    iDays  =  parseInt(Math.abs(oDate1  -  oDate2)/1000/60/60/24);//把相差的毫秒数转换为天数  

    return  iDays;
}