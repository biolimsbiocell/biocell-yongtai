﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	if($("#sampleGeneTemp_name").val() != ""){
		$("#geneTempNew_name").val($("#sampleGeneTemp_name").val());
	}
	if($("#geneTemp_name").val() != ""){
		$("#geneTempNew_name").val($("#geneTemp_name").val());
	}
	
	if($("#sampleGeneTemp_productName").val()!=$("#geneTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleGeneTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleGeneTemp_productId").val());
		$("#productNameNew").val($("#sampleGeneTemp_productName").val());
	}
	
	if($("#sampleGeneTemp_sampleType_id").val()!=$("#geneTemp_sampleType_id").val()){
		$("#sampleType").css({"background-color":"red","color":"white"});
	}else{
		$("#sampleTypeIdNew").val($("#sampleGeneTemp_sampleType_id").val());
		$("#sampleTypeNew").val($("#sampleGeneTemp_sampleType_name").val());
	}
	
	if($("#sampleGeneTemp_voucherType_id").val()!=$("#geneTemp_voucherType_id").val()){
		$("#voucherType").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherTypeIdNew").val($("#sampleGeneTemp_voucherType_id").val());
		$("#voucherTypeNew").val($("#sampleGeneTemp_voucherType_name").val());
	}
	
	if($("#sampleGeneTemp_acceptDate").val()!=$("#geneTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleGeneTemp_acceptDate").val());
	}
	
	if($("#sampleGeneTemp_area").val()!=$("#geneTemp_area").val()){
		$("#area").css({"background-color":"red","color":"white"});
	}else{
		$("#areaNew").val($("#sampleGeneTemp_area").val());
	}
	
	if($("#sampleGeneTemp_hospital").val()!=$("#geneTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleGeneTemp_hospital").val());
	}
	
	if($("#sampleGeneTemp_sendDate").val()!=$("#geneTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleGeneTemp_sendDate").val());
	}
	
	if($("#sampleGeneTemp_reportDate").val()!=$("#geneTemp_reportDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleGeneTemp_reportDate").val());
	}
	
	if($("#sampleGeneTemp_confirmAgeOne").val()!=$("#geneTemp_confirmAgeOne").val()){
		$("#confirmAgeOne").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeOneNew").val($("#sampleGeneTemp_confirmAgeOne").val());
	}
	
	if($("#sampleGeneTemp_doctor").val()!=$("#geneTemp_doctor").val()){
		$("#doctor").css({"background-color":"red","color":"white"});
	}else{
		$("#doctorNew").val($("#sampleGeneTemp_doctor").val());
	}
	
	if($("#sampleGeneTemp_patientName").val()!=$("#geneTemp_patientName").val()){
		$("#patientName").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameNew").val($("#sampleGeneTemp_patientName").val());
	}
	
	if($("#sampleGeneTemp_patientNameSpell").val()!=$("#geneTemp_patientNameSpell").val()){
		$("#patientNameSpell").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameSpellNew").val($("#sampleGeneTemp_patientNameSpell").val());
	}
	
	if($("#sampleGeneTemp_gender").val()!=$("#geneTemp_gender").val()){
		$("#geneTempNew_gender").css({"background-color":"red","color":"white"});
	}else{
		$("#geneTempNew_gender").val($("#sampleGeneTemp_gender").val());
	}
	
	if($("#sampleGeneTemp_age").val()!=$("#geneTemp_age").val()){
		$("#age").css({"background-color":"red","color":"white"});
	}else{
		$("#ageNew").val($("#sampleGeneTemp_age").val());
	}
	
	if($("#sampleGeneTemp_nativePlace").val()!=$("#geneTemp_nativePlace").val()){
		$("#nativePlace").css({"background-color":"red","color":"white"});
	}else{
		$("#nativePlaceNew").val($("#sampleGeneTemp_nativePlace").val());
	}
	
	if($("#sampleGeneTemp_nationality").val()!=$("#geneTemp_nationality").val()){
		$("#nationality").css({"background-color":"red","color":"white"});
	}else{
		$("#nationalityNew").val($("#sampleGeneTemp_nationality").val());
	}
	
	if($("#sampleGeneTemp_isInsure").val()!=$("#geneTemp_isInsure").val()){
		$("#geneTempNew_isInsure").css({"background-color":"red","color":"white"});
	}else{
		$("#geneTempNew_isInsure").val($("#sampleGeneTemp_isInsure").val());
	}
	
	if($("#sampleGeneTemp_voucherCode").val()!=$("#geneTemp_voucherCode").val()){
		$("#voucherCode").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherCodeNew").val($("#sampleGeneTemp_voucherCode").val());
	}
	
	if($("#sampleGeneTemp_phone").val()!=$("#geneTemp_phone").val()){
		$("#phone").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNew").val($("#sampleGeneTemp_phone").val());
	}
	
	if($("#sampleGeneTemp_emailAddress").val()!=$("#geneTemp_emailAddress").val()){
		$("#emailAddress").css({"background-color":"red","color":"white"});
	}else{
		$("#emailAddressNew").val($("#sampleGeneTemp_emailAddress").val());
	}
	
	if($("#sampleGeneTemp_address").val()!=$("#geneTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleGeneTemp_address").val());
	}
	
	if($("#sampleGeneTemp_bloodHistory").val()!=$("#geneTemp_bloodHistory").val()){
		$("#geneTempNew_bloodHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#geneTempNew_bloodHistory").val($("#sampleGeneTemp_bloodHistory").val());
	}
	
	if($("#sampleGeneTemp_tumorHistory").val()!=$("#geneTemp_tumorHistory").val()){
		$("#geneTempNew_tumorHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#geneTempNew_tumorHistory").val($("#sampleGeneTemp_tumorHistory").val());
	}
	
	if($("#sampleGeneTemp_cigarette").val()!=$("#geneTemp_cigarette").val()){
		$("#cigarette").css({"background-color":"red","color":"white"});
	}else{
		$("#cigaretteNew").val($("#sampleGeneTemp_cigarette").val());
	}
	
	if($("#sampleGeneTemp_wine").val()!=$("#geneTemp_wine").val()){
		$("#wine").css({"background-color":"red","color":"white"});
	}else{
		$("#wineNew").val($("#sampleGeneTemp_wine").val());
	}
	
	if($("#sampleGeneTemp_medicine").val()!=$("#geneTemp_medicine").val()){
		$("#medicine").css({"background-color":"red","color":"white"});
	}else{
		$("#medicineNew").val($("#sampleGeneTemp_medicine").val());
	}
	
	if($("#sampleGeneTemp_radioactiveRays").val()!=$("#geneTemp_radioactiveRays").val()){
		$("#radioactiveRays").css({"background-color":"red","color":"white"});
	}else{
		$("#radioactiveRaysNew").val($("#sampleGeneTemp_radioactiveRays").val());
	}
	
	if($("#sampleGeneTemp_pesticide").val()!=$("#geneTemp_pesticide").val()){
		$("#pesticide").css({"background-color":"red","color":"white"});
	}else{
		$("#pesticideNew").val($("#sampleGeneTemp_pesticide").val());
	}
	
	if($("#sampleGeneTemp_plumbane").val()!=$("#geneTemp_plumbane").val()){
		$("#plumbane").css({"background-color":"red","color":"white"});
	}else{
		$("#plumbaneNew").val($("#sampleGeneTemp_plumbane").val());
	}
	
	if($("#sampleGeneTemp_mercury").val()!=$("#geneTemp_mercury").val()){
		$("#mercury").css({"background-color":"red","color":"white"});
	}else{
		$("#mercuryNew").val($("#sampleGeneTemp_mercury").val());
	}
	
	if($("#sampleGeneTemp_cadmium").val()!=$("#geneTemp_cadmium").val()){
		$("#cadmium").css({"background-color":"red","color":"white"});
	}else{
		$("#cadmiumNew").val($("#sampleGeneTemp_cadmium").val());
	}
	
	if($("#sampleGeneTemp_medicalHistory").val()!=$("#geneTemp_medicalHistory").val()){
		$("#medicalHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#medicalHistoryNew").val($("#sampleGeneTemp_medicalHistory").val());
	}
	
	if($("#sampleGeneTemp_relationship").val()!=$("#geneTemp_relationship").val()){
		$("#relationship").css({"background-color":"red","color":"white"});
	}else{
		$("#relationshipNew").val($("#sampleGeneTemp_relationship").val());
	}
	
	if($("#sampleGeneTemp_isFee").val()!=$("#geneTemp_isFee").val()){
		$("#geneTempNew_isFee").css({"background-color":"red","color":"white"});
	}else{
		$("#geneTempNew_isFee").val($("#sampleGeneTemp_isFee").val());
	}
	
	if($("#sampleGeneTemp_privilegeType").val()!=$("#geneTemp_privilegeType").val()){
		$("#geneTempNew_privilegeType").css({"background-color":"red","color":"white"});
	}else{
		$("#geneTempNew_privilegeType").val($("#sampleGeneTemp_privilegeType").val());
	}
	
	if($("#sampleGeneTemp_isInvoice").val()!=$("#geneTemp_isInvoice").val()){
		$("#geneTempNew_isInvoice").css({"background-color":"red","color":"white"});
	}else{
		$("#geneTempNew_isInvoice").val($("#sampleGeneTemp_isInvoice").val());
	}
	
	if($("#sampleGeneTemp_money").val()!=$("#geneTemp_money").val()){
		$("#money").css({"background-color":"red","color":"white"});
	}else{
		$("#moneyNew").val($("#sampleGeneTemp_money").val());
	}
	
	if($("#sampleGeneTemp_SP").val()!=$("#geneTemp_SP").val()){
		$("#SP").css({"background-color":"red","color":"white"});
	}else{
		$("#SPNew").val($("#sampleGeneTemp_SP").val());
	}
	
	if($("#sampleGeneTemp_paymentUnit").val()!=$("#geneTemp_paymentUnit").val()){
		$("#paymentUnit").css({"background-color":"red","color":"white"});
	}else{
		$("#paymentUnitNew").val($("#sampleGeneTemp_paymentUnit").val());
	}
	
	if($("#sampleGeneTemp_reportMan_id").val()!=$("#geneTemp_reportMan_id").val()){
		$("#reportMan").css({"background-color":"red","color":"white"});
	}else{
		$("#reportManIdNew").val($("#sampleGeneTemp_reportMan_id").val());
		$("#reportManNew").val($("#sampleGeneTemp_reportMan_name").val());
	}
	
	if($("#sampleGeneTemp_auditMan_id").val()!=$("#geneTemp_auditMan_id").val()){
		$("#auditMan").css({"background-color":"red","color":"white"});
	}else{
		$("#auditManIdNew").val($("#sampleGeneTemp_auditMan_id").val());
		$("#auditManNew").val($("#sampleGeneTemp_auditMan_name").val());
	}
	
	if($("#sampleGeneTemp_linkman_name").val()!=$("#geneTemp_linkman_name").val()){
		$("#linkman").css({"background-color":"red","color":"white"});
	}else{
		$("#linkmanNew").val($("#sampleGeneTemp_linkman_name").val());
	}
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {	

	var nextStepFlow =$("#geneTempNew_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.selectNextFlow);
		return;
	}

	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleGeneTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleGeneTemp_id").val(),
					title : $("#sampleGeneTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleGeneTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});


function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action?saveType="+$("#saveType").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInputTemp/copySampleInput.action?id=' + $("#sampleGeneTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleGeneTemp_id").val()){
		commonChangeState("formId=" + $("#sampleGeneTemp_id").val() + "&tableId=SampleGeneTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.singe_geneAudit,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleTypeIdNew").value = id;
		 document.getElementById("sampleTypeNew").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:600,height:700,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("sampleGeneTemp_voucherType").value = id;
			 document.getElementById("sampleGeneTemp_voucherType_name").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleGeneTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleGeneTemp_voucherCode").val())){
				var id = $("#sampleGeneTemp_voucherCode").val();
				ajax("post", "/sample/sampleInputTemp/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleGeneTemp_phoneNum").val())){
				//return;
				var id = $("#sampleGeneTemp_phoneNum").val();
				ajax("post", "/sample/sampleInputTemp/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

		//家庭住址验证	 
		function checkAddress(){
			var address = $("#sampleGeneTemp_address").val();
			if(address==""){
				message(biolims.sample.addressIsEmpty);
		}
}
		//B超异常提醒验证
		function change(){
			var reg = $('#sampleGeneTemp_embryoType option:selected').val();
			if(reg =="2"){
				$("#sampleGeneTemp_messages").css("display","");
			}
		}

		
		
		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 600,
						height : 700,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("productIdNew").value = id;
							document.getElementById("productNameNew").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}
		
		
		
//检测项目的Id给到	
function a(){
	var productName = $("#productName").val();
	var id1 = $("#sampleGeneTemp_productId").val();
	var name1= $("#sampleGeneTemp_productName").val();
	var id2= $("#geneTemp_productId").val();
	var name2 = $("#geneTemp_productName").val();
	if(productName==name1){
		$("#productIdNew").val(id1);
	}else if(productName==name2){
		$("#productIdNew").val(id2);
	}else if(productName==""){
		$("#productIdNew").val("");
	}
}

//样本类型的Id给到
function b(){
	var sampleType = $("#sampleType").val();
	var id1 = $("#sampleGeneTemp_sampleType_id").val();
	var name1 = $("#sampleGeneTemp_sampleType_name").val();
	var id2 = $("#geneTemp_sampleType_id").val();
	var name2 = $("#geneTemp_sampleType_name").val();
	if(sampleType==name1){
		$("#sampleTypeIdNew").val(id1);
	}else if(sampleType==name2){
		$("#sampleTypeIdNew").val(id2);
	}else if(sampleType==""){
		$("#sampleTypeIdNew").val("");
	}
}
//证件号码的Id给到
function c(){
	var voucherType = $("#voucherType").val();
	var id1 =$("#sampleGeneTemp_voucherType_id").val();
	var name1 = $("#sampleGeneTemp_voucherType_name").val();
	var id2 = $("#geneTemp_voucherType_id").val();
	var name2 =$("#geneTemp_voucherType_name").val();
	if(voucherType==name1){
		$("#voucherTypeIdNew").val(id1);
	}else if(voucherType==name2){
		$("#voucherTypeIdNew").val(id2);
	}else if(voucherType==""){
		$("#voucherTypeIdNew").val("");
	}
}