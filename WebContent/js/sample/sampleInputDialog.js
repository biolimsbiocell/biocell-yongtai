var sampleInputDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
//	    fields.push({
//		name:'age-id',
//		type:"string"
//	});
	    fields.push({
		name:'age',
		type:"string"
	});
	    fields.push({
		name:'gender',
		type:"string"
	});
//	    fields.push({
//		name:'gender-name',
//		type:"string"
//	});
	    fields.push({
		name:'linkman-id',
		type:"string"
	});
	    fields.push({
		name:'linkman-name',
		type:"string"
	});
	    fields.push({
		name:'birthday',
		type:"string"
	});
	    fields.push({
		name:'phoneNum',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.sample.infoInputID,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'age',
		header:biolims.common.age,
		width:20*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'age-name',
//		header:'年龄',
//		width:20*10,
//		sortable:true
//		});
		cm.push({
		dataIndex:'gender',
		header:biolims.common.gender,
		width:20*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'gender-name',
//		header:'性别',
//		width:20*10,
//		sortable:true
//		});
		cm.push({
		dataIndex:'linkman-id',
		header:biolims.common.linkmanId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'linkman-name',
		header:biolims.common.linkmanName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'birthday',
		header:biolims.common.birthDay,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'phoneNum',
		header:biolims.common.phone,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleInput/showSampleInputListJson.action";
	var opts={};
	opts.title=biolims.sample.infoInput;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleInputFun(rec);
	};
	sampleInputDialogGrid=gridTable("show_dialog_sampleInput_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleInputDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
