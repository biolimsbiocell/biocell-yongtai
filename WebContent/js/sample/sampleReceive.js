$(function() {
	var options = table(true,"",
		"/sample/sampleReceive/showSampleReceiveTableJson.action", [
		   {
			"data": "id",
			"title": biolims.master.sampleBloodRecriveId,
		},{
			"data": "name",
			"title": "描述",
		},{
			"data": "ccoi",
			"title": "CCOI",
		},{
			"data": "barcode",
			"title": "产品批号",
		}, {
			"data": "acceptDate",
			"title": biolims.common.receiveDate,
			"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
		}, {
			"data": "stateName",
			"title": biolims.common.stateName,
		}], null)
	//myTable= renderRememberData($("#main"), options);
	myTable= renderData($("#main"), options);
	//恢复之前查询的状态
//	$('#main').on('init.dt', function() {
//		recoverSearchContent(myTable);
//	})
});
function add(){
		window.location=window.ctx+'/sample/sampleReceive/editSampleReceive.action?type='+$("#type").val();
	}
function edit(){
	var id="";
	var id = $(".selected").find("input").val();
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/sample/sampleReceive/editSampleReceive.action?id=' + id +'&type='+$("#type").val();
}
function view() {
	var id = "";
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/sampleReceive/viewSampleReceive.action?id=' + id +'&type='+$("#type").val();
}

//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.master.sampleBloodRecriveId,
			"type": "input",
			"searchName": "id"
		},{
			"txt": "CCOI",
			"type": "input",
			"searchName": "ccoi"
		},{
			"txt": "批次",
			"type": "input",
			"searchName": "barcode"
		},
		{
			"txt": biolims.receive.receiveDateStart,
			"type": "dataTime",
			"searchName": "acceptDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.receive.receiveDateEnd,
			"type": "dataTime",
			"searchName": "acceptDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"type": "table",
			"table": myTable
		}
	];
}
