var sampleCancerTempItemGrid;
$(function(){
	
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'drugDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'useDrugName',
		type:"string"
	});
	   fields.push({
		name:'effectOfProgress',
		type:"string"
	});
	   fields.push({
		name:'effectOfProgressSpeed',
		type:"string"
	});
	   fields.push({
		name:'geneticTestHistory',
		type:"string"
	});
	   
	   fields.push({
			name:'sampleDetectionName',
			type:"string"
		});
	   fields.push({
			name:'sampleExonRegion',
			type:"string"
		});
	   fields.push({
			name:'sampleDetectionResult',
			type:"string"
		});
	   
	    fields.push({
		name:'sampleCancerTemp-id',
		type:"string"
	});
	    fields.push({
		name:'sampleCancerTemp-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'drugDate',
		hidden : false,
		header:biolims.sample.drugDate,
		width:50*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'useDrugName',
		hidden : false,
		header:biolims.sample.useDrugName,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'effectOfProgress',
		hidden : false,
		header:biolims.sample.effectOfProgress,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var effectOfProgressSpeedstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.sample.rapidProgress ], [ '2', biolims.sample.localProgress ],[ '3', biolims.sample.slowProgress ],]
	});
	
	var effectOfProgressSpeedComboxFun = new Ext.form.ComboBox({
		store : effectOfProgressSpeedstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'effectOfProgressSpeed',
		header:biolims.sample.effectOfProgressSpeed,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(effectOfProgressSpeedComboxFun),
		editor: effectOfProgressSpeedComboxFun,
		sortable:true
	});
	
//把药效进展快慢修改为下拉框
//	cm.push({
//		dataIndex:'effectOfProgressSpeed',
//		hidden : false,
//		header:'药效进展快慢',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'geneticTestHistory',
		hidden : false,
		header:biolims.sample.geneticTestHistory,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleDetectionName',
		hidden : false,
		header:biolims.sample.sampleDetectionName,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleExonRegion',
		hidden : false,
		header:biolims.sample.sampleExonRegion,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleDetectionResult',
		hidden : false,
		header:biolims.sample.sampleOrderName,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	
	cm.push({
		dataIndex:'sampleCancerTemp-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'sampleCancerTemp-name',
//		hidden : false,
//		header:'相关主表',
//		width:20*10
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleCancerTemp/showSampleCancerTempItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.testOrder_drugInfo;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/sample/sampleCancerTemp/delSampleCancerTempItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				sampleCancerTempItemGrid.getStore().commitChanges();
				sampleCancerTempItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
			text : biolims.common.selectRelevantTable,
				handler : selectsampleCancerTempDialogFun
		});
	opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),biolims.common.batchUpload,null,{
				"Confirm":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = sampleCancerTempItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
							sampleCancerTempItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	sampleCancerTempItemGrid=gridEditTable("sampleCancerTempItemdiv",cols,loadParam,opts);
	$("#sampleCancerTempItemdiv").data("sampleCancerTempItemGrid", sampleCancerTempItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectsampleCancerTempFun(){
	var win = Ext.getCmp('selectsampleCancerTemp');
	if (win) {win.close();}
	var selectsampleCancerTemp= new Ext.Window({
	id:'selectsampleCancerTemp',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectsampleCancerTemp.close(); }  }]  }) });  
    selectsampleCancerTemp.show(); }
	function setsampleCancerTemp(rec){
		var gridGrid = $("#sampleCancerTempItemdiv").data("sampleCancerTempItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('sampleCancerTemp-id',rec.get('id'));
			obj.set('sampleCancerTemp-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectsampleCancerTemp')
		if(win){
			win.close();
		}
	}
	function selectsampleCancerTempDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/SampleCancerTempSelect.action?flag=sampleCancerTemp";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selsampleCancerTempVal(this);
				}
			}, true, option);
		}
	var selsampleCancerTempVal = function(win) {
		var operGrid = sampleCancerTempDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#sampleCancerTempItemdiv").data("sampleCancerTempItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('sampleCancerTemp-id',rec.get('id'));
				obj.set('sampleCancerTemp-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
	
