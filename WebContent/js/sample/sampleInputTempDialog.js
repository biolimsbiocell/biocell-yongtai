var sampleInputTempDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'hospital',
		type:"string"
	});
	    fields.push({
		name:'project-id',
		type:"string"
	});
	    fields.push({
		name:'project-name',
		type:"string"
	});
	    fields.push({
		name:'sampleUser',
		type:"string"
	});
	    fields.push({
		name:'sampleTime',
		type:"string"
	});
	    fields.push({
		name:'sendTime',
		type:"string"
	});
	    fields.push({
		name:'company-id',
		type:"string"
	});
	    fields.push({
		name:'company-name',
		type:"string"
	});
	    fields.push({
		name:'expressCode',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'expressCode',
		header:biolims.sample.expreceCode,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'company-id',
		header:biolims.sample.companyId,
		width:15*10,
		hidden :true,
		sortable:true
		});
		cm.push({
		dataIndex:'company-name',
		header:biolims.sample.companyName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'hospital',
		header:biolims.sample.hospital,
		width:10*10,
		hidden :true,
		sortable:true
	});
//		cm.push({
//		dataIndex:'project-id',
//		header:'项目ID',
//		width:15*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'project-name',
//		header:'项目',
//		width:15*10,
//		sortable:true
//		});
	cm.push({
		dataIndex:'sampleUser',
		header:biolims.sample.sampleUser,
		width:8*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleTime',
		header:biolims.sample.sampleTime,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sendTime',
		header:biolims.sample.sendOutTime,
		width:10*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:15*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:15*10,
		hidden:true,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:15*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:10*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:10*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleInputTemp/showSampleInputCheckListJson.action";
	var opts={};
	opts.title=biolims.sample.InfoAudit;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleInfoMainFun(rec);
	};
	sampleInputTempDialogGrid=gridTable("show_dialog_sampleInputTemp_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleInputTempDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
