var plasmaRepeatgrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
//	fields.push({
//		name : 'tempId',
//		type : "string"
//	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'patient',
		type : "string"
	});
	fields.push({
		name : 'idCard',
		type : "string"
	});
	fields.push({
		name : 'sequencingFun',
		type : "string"
	});
	fields.push({
		name : 'inspectDate',
		type : "string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
//	fields.push({
//		name : 'volume',
//		type : "string"
//	});
//	fields.push({
//		name : 'unit',
//		type : "string"
//	});
	fields.push({
		name : 'result',
		type : "string"
	});
	fields.push({
		name : 'nextFlow',
		type : "string"
	});
	fields.push({
		name : 'method',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
//	fields.push({
//		name : 'bloodSampleTask-id',
//		type : "string"
//	});
//	fields.push({
//		name : 'bloodSampleTask-name',
//		type : "string"
//	});

	fields.push({
		name : 'submit',
		type : "string"
	});

	fields.push({
		name : 'acceptDate',
		type : "string"
	});
//
//	fields.push({
//		name : 'reason',
//		type : "string"
//	});
//	fields.push({
//		name : 'concentration',
//		type : "string"
//	});
		fields.push({
		name:'classify',
		type:"string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.common.id,
		width : 40 * 6
	});
//	cm.push({
//		dataIndex : 'tempId',
//		hidden : true,
//		header : '临时表id',
//		width : 40 * 6
//	});
	cm.push({
		dataIndex : 'code',
		hidden : false,
		header : biolims.common.code,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'sampleCode',
		hidden : false,
		header : biolims.common.sampleCode,
		width : 30 * 6
	});

	cm.push({
		dataIndex : 'patient',
		hidden : true,
		header : biolims.common.patientName,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'idCard',
		hidden : true,
		header : biolims.common.idCard,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'phone',
		hidden : true,
		header : biolims.common.phone,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.productId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.productName,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sequencingFun',
		hidden : true,
		header : biolims.common.sequencingFun,
		width : 20 * 6
	});

	cm.push({
		dataIndex : 'inspectDate',
		hidden : true,
		header : biolims.common.inspectDate,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'reportDate',
		hidden : true,
		header : biolims.common.reportDate,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'orderId',
		hidden : true,
		header : biolims.common.orderId,
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'concentration',
//		hidden : false,
//		header : '浓度',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'volume',
//		hidden : false,
//		header : '体积',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'unit',
//		hidden : false,
//		header : '单位',
//		width : 20 * 6
//	});
//
	var storeisresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.disqualified ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeisresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'result',
		hidden : true,
		header : biolims.common.result,
		width : 20 * 6,
		editor : null,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.plasma.buildLib ], [ '1', biolims.plasma.repeatBlood ],[ '2', biolims.plasma.putInLib ],[ '3', biolims.plasma.feedBack2Group ],[ '4', biolims.plasma.end ],[ '5', '暂停' ]]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'nextFlow',
		hidden : true,
		header : biolims.common.nextFlow,
		width : 25 * 6,
		//editor : null,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
//	var storemethodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '文库构建' ], [ '1', '入库' ],[ '2', '终止' ],[ '3', '重抽血' ],[ '4', '暂停' ]]
//	});
//	var methodCob = new Ext.form.ComboBox({
//		store : storemethodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex : 'method',
		header : biolims.common.method,
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'submit',
		header : biolims.common.isExecute,
		width : 20 * 6,
		editor : submitCob,
		hidden:true,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});

//	cm.push({
//		dataIndex : 'reason',
//		hidden : false,
//		header : '原因',
//		width : 20 * 6
//	});

	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : biolims.common.state,
		width : 20 * 6
	});
	
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : biolims.common.note,
		width : 40 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

//	cm.push({
//		dataIndex : 'bloodSampleTask-id',
//		hidden : true,
//		header : '相关主表ID',
//		width : 15 * 10
//	});
//	cm.push({
//		dataIndex : 'bloodSampleTask-name',
//		hidden : true,
//		header : '相关主表',
//		width : 15 * 10
//	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/sample/repeat/showSampleRepeatPlasmaListJson.action";
	var opts = {};
	opts.title = biolims.plasma.repeatBlood;
	opts.height = document.body.clientHeight;
	opts.tbar = [];

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
//	opts.tbar.push({
//		text : "批量执行",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量执行", null, {
//				"确定" : function() {
//					var records = plasmaRepeatgrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						plasmaRepeatgrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						plasmaRepeatgrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : search
//	});
//	opts.tbar.push({
//		iconCls : 'save',
//		text : '保存',
//		handler : save
//	});
//	opts.tbar.push({
//		text : "批量处理意见",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_next_div"), "批量处理意见", null, {
//				"确定" : function() {
//					var records = plasmaRepeatgrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var nextFlow = $("#method").val();
//						plasmaRepeatgrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("method", nextFlow);
//						});
//						plasmaRepeatgrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	
	plasmaRepeatgrid = gridEditTable("show_repeatPlasma_div", cols, loadParam,
			opts);
	$("#show_repeatPlasma_div").data("plasmaRepeatgrid", plasmaRepeatgrid);
});

// 检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(plasmaRepeatgrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();
		}
	}, true, option);
}

// 保存
function save() {
	var itemJson = commonGetModifyRecords(plasmaRepeatgrid);
	if (itemJson.length > 0) {
		ajax("post", "/sample/repeat/savePlasmaRepeat.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				plasmaRepeatgrid.getStore().commitChanges();
				plasmaRepeatgrid.getStore().reload();
				message(biolims.common.saveSuccess);
			} else {
				message(biolims.common.saveFailed);
			}
		}, null);
	} else {
		message(biolims.common.noData2Save);
	}
}
