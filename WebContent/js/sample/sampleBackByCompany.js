var sampleBackByCompanyGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
	fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
   fields.push({
		name:'idCard',
		type:"string"
	});
   fields.push({
		name:'phone',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name:'acceptDate',
		type:"string"
	});
	fields.push({
		name:'reportDate',
		type:"string"
	});
	fields.push({
		name:'returnDate',
		type:"string"
	});
	fields.push({
		name:'method',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
		fields.push({
		name:'classify',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.sampleId,
		width:30*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : false,
		header:biolims.common.idCard,
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		hidden:false,
		width:30*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:30*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.acceptDate,
		width:30*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'returnDate',
		hidden : false,
		header:biolims.sample.returnDate,
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	var storePutCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.plasma.repeatBlood ], [ '1', biolims.pooling.refund ], [ '2', biolims.sample.openBoxUnqualified ] ]
	});
	var putCob = new Ext.form.ComboBox({
		store : storePutCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:biolims.common.method,
		width:20*6,
		//editor : putCob,
		renderer : Ext.util.Format.comboRenderer(putCob)
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleBackByCompany/showSampleBackByCompanyListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.sample.companyFeedback;
	opts.height=document.body.clientHeight;
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
//	opts.tbar.push({
//		text : '打印',
//		handler : exportexcel
//	});
//	opts.tbar.push({
//		text : "批量操作",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_way_div"), "批量操作", null, {
//				"确定" : function() {
//					var records = goodsMaterialsWayGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var isSend = $("#isSend").val();
//						goodsMaterialsWayGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isSend", isSend);
//						});
//						goodsMaterialsWayGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "批量日期",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_date_div"), "批量日期", null, {
//				"确定" : function() {
//					var records = goodsMaterialsWayGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var sendDate = $("#sendDate").val();
//						sendDate = sendDate.replace(/-/g,"/");
//						var date = new Date(sendDate);
//						goodsMaterialsWayGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("sendDate", date);
//						});
//						goodsMaterialsWayGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	sampleBackByCompanyGrid=gridEditTable("sampleBackByCompanydiv",cols,loadParam,opts);
	$("#sampleBackByCompanydiv").data("sampleBackByCompanyGrid", sampleBackByCompanyGrid);
});

//保存
function save(){	
	var itemJson = commonGetModifyRecords(sampleBackByCompanyGrid);
	if(itemJson.length>0){
		ajax("post", "/sample/sampleBackByCompany/saveSampleBackByCompany.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				sampleBackByCompanyGrid.getStore().commitChanges();
				sampleBackByCompanyGrid.getStore().reload();
				message(biolims.common.saveSuccess);
			} else {
				message(biolims.common.saveFailed);
			}
		}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
	
}
function exportexcel() {
	goodsMaterialsWayGrid.title = biolims.common.exportList;
	var vExportContent = sampleBackByCompanyGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function search() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				if (($("#startreturnDate").val() != undefined) && ($("#startreturnDate").val() != '')) {
					var startreturnDate = ">=##@@##" + $("#startreturnDate").val();
					$("#writeDate1").val(startreturnDate);
				}
				if (($("#endreturnDate").val() != undefined) && ($("#endreturnDate").val() != '')) {
					var endreturnDate = "<=##@@##" + $("#endreturnDate").val();

					$("#writeDate2").val(endreturnDate);

				}
				commonSearchAction(sampleBackByCompanyGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
}

