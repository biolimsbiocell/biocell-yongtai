var sampleInputTechnologyDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'sendDate',
		type:"string"
	});
	    fields.push({
		name:'contractNum',
		type:"string"
	});
	    fields.push({
		name:'projectName',
		type:"string"
	});
	    fields.push({
		name:'clientName',
		type:"string"
	});
	    fields.push({
		name:'clientUnit',
		type:"string"
	});
	    fields.push({
		name:'sampleState',
		type:"string"
	});
	    fields.push({
		name:'species',
		type:"string"
	});
	    fields.push({
		name:'organType',
		type:"string"
	});
	    fields.push({
		name:'sampleName',
		type:"string"
	});
	    fields.push({
		name:'sampleType-id',
		type:"string"
	});
	    fields.push({
		name:'sampleType-name',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'openBoxUser',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'isQualified',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sendDate',
		header:biolims.common.receiveDate,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'contractNum',
		header:biolims.common.contractId,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'projectName',
		header:biolims.common.projectName,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'clientName',
		header:biolims.sample.customerName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'clientUnit',
		header:biolims.sample.clientUnit,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleState',
		header:biolims.common.sampleState,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'species',
		header:biolims.wk.species,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'organType',
		header:biolims.sample.organType,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleName',
		header:biolims.common.sampleName,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'sampleType-id',
		header:biolims.common.sampleTypeId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'sampleType-name',
		header:biolims.common.sampleTypeName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.code,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'openBoxUser',
		header:biolims.common.openBoxUser,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'isQualified',
		header:biolims.common.isQualified,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleInputTechnology/showSampleInputTechnologyListJson.action";
	var opts={};
	opts.title=biolims.sample.scienceServiceInput;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleInputTechnologyFun(rec);
	};
	sampleInputTechnologyDialogGrid=gridTable("show_dialog_sampleInputTechnology_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleInputTechnologyDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
