var sampleItemDialogGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'condition',
		type:"string"
	});
	    fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
		name:'unusual-id',
		type:"string"
	});
	    fields.push({
			name:'unusual-name',
			type:"string"
		});
	    fields.push({
		name:'location',
		type:"string"
	});
//	    fields.push({
//		name:'expreceCode-name',
//		type:"string"
//	});
	    fields.push({
		name:'method',
		type:"string"
	});
	    fields.push({
		name:'advice',
		type:"string"
	});
	    fields.push({
		name:'isFull',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.sampleId,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.code,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'condition',
		header:biolims.common.sampleCondition,
		width:10*10,
		sortable:true
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
		cm.push({
		dataIndex:'isGood',
		header:biolims.common.isQualified,
		width:8*10,
		sortable:true,
		renderer : Ext.util.Format.comboRenderer(goodCob)
		});
		cm.push({
		dataIndex:'unusual-id',
		header:biolims.sample.unusualId,
		hidden:true,
		width:8*10,
		sortable:true
		});
		cm.push({
			dataIndex:'unusual-name',
			header:biolims.sample.unusualName,
			width:8*10,
			sortable:true
			});
		cm.push({
		dataIndex:'location',
		header:biolims.sample.location,
		width:10*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'expreceCode-name',
//		header:'快递单号',
//		width:20*10,
//		sortable:true
//		});
		cm.push({
			dataIndex : 'method',
			header : biolims.common.method,
			width : 80,
			renderer : function(value, metadata, record, rowIndex,
					colIndex, store) {
				if (value == "0") {
					return "<span style='color:red'>"+biolims.common.noPass+"</span>";
				} else if (value == "1") {
					return biolims.common.pass;
				} else {
					return biolims.common.pleaseChoose;
				}
			}
		});
		cm.push({
		dataIndex:'advice',
		header:biolims.common.method,
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'isFull',
		header:biolims.sample.isFull,
		width:8*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleReceive/showDialogSampleItemListJson.action";
	var opts={};
	opts.title=biolims.common.sampleDetail;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleReceiveIFun(rec);
	};
	sampleItemDialogGrid=gridTable("show_dialog_sampleItem_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleItemDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
