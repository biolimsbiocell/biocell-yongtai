var sampleCancerTempGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'orderNumber',
		type:"string"
	});
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'gender',
		type:"string"
	});
	    fields.push({
		name:'birthDate',
		type:"string"
	});
	    fields.push({
		name:'diagnosisDate',
		type:"string"
	});
	    
	    fields.push({
		name:'sampleStage',
		type:"string"
	});
	    fields.push({
		name:'inspectionDepartment-id',
		type:"string"
	});
	    fields.push({
		name:'inspectionDepartment-name',
		type:"string"
	});
	    fields.push({
		name:'crmProduct-id',
		type:"string"
	});
	    fields.push({
		name:'crmProduct-name',
		type:"string"
	});
	    fields.push({
		name:'samplingDate',
		type:"string"
	});
	    fields.push({
		name:'samplingLocation-id',
		type:"string"
	});
	    fields.push({
		name:'samplingLocation-name',
		type:"string"
	});
	    fields.push({
		name:'samplingNumber',
		type:"string"
	});
	    fields.push({
		name:'pathologyConfirmed',
		type:"string"
	});
	    fields.push({
		name:'bloodSampleDate',
		type:"string"
	});
	    fields.push({
		name:'plasmapheresisDate',
		type:"string"
	});
	    fields.push({
		name:'commissioner-id',
		type:"string"
	});
	    fields.push({
		name:'commissioner-name',
		type:"string"
	});
	    fields.push({
		name:'receivedDate',
		type:"string"
	});
	    fields.push({
		name:'sampleTypeId',
		type:"string"
	});
	    fields.push({
		name:'sampleTypeName',
		type:"string"
	});
	    fields.push({
		name:'sampleOrder-id',
		type:"string"
	});
	    fields.push({
		name:'sampleOrder-name',
		type:"string"
	});
	    fields.push({
			name:'medicalNumber',
			type:"string"
		});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'family',
		type:"string"
	});
	    fields.push({
		name:'familyPhone',
		type:"string"
	});
	    fields.push({
		name:'familySite',
		type:"string"
	});
	    fields.push({
		name:'medicalInstitutions',
		type:"string"
	});
	    fields.push({
		name:'medicalInstitutionsPhone',
		type:"string"
	});
	    fields.push({
		name:'medicalInstitutionsSite',
		type:"string"
	});
	    fields.push({
		name:'attendingDoctor',
		type:"string"
	});
	    fields.push({
		name:'attendingDoctorPhone',
		type:"string"
	});
	    fields.push({
		name:'attendingDoctorSite',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:' sampleInfoMain',
		type:"string"
	});
//	    fields.push({
//		name:'cancerType-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'cancerType-name',
//		type:"string"
//	});
//	    fields.push({
//		name:'cancerTypeSeedOne-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'cancerTypeSeedOne-name',
//		type:"string"
//	});
//	    fields.push({
//		name:'cancerTypeSeedTwo-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'cancerTypeSeedTwo-name',
//		type:"string"
//	});
    
	   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'orderNumber',
		header:biolims.common.orderCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:50*6,
		
		sortable:true
	});
	var genderstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.male ], [ '0', biolims.common.female]]
	});
	
	var genderComboxFun = new Ext.form.ComboBox({
		store : genderstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'gender',
		header:biolims.common.gender,
		width:10*6,
		
		renderer: Ext.util.Format.comboRenderer(genderComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'birthDate',
		header:biolims.common.birthDay,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'diagnosisDate',
		header:biolims.common.diagnosisDate,
		width:20*6,
		
		sortable:true
	});
	
	
	var sampleStagestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'I' ], [ '2', 'II' ],[ '3', 'III' ],[ '4', 'IV' ]]
	});
	
	var sampleStageComboxFun = new Ext.form.ComboBox({
		store : sampleStagestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'sampleStage',
		header:biolims.sample.sampleStage,
		width:10*6,
		
		renderer: Ext.util.Format.comboRenderer(sampleStageComboxFun),				
		sortable:true
	});
	
	
	
		cm.push({
		dataIndex:'inspectionDepartment-id',
		hidden:true,
		header:biolims.sample.inspectionDepartmentId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'inspectionDepartment-name',
		header:biolims.sample.inspectionDepartmentName,
		
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'crmProduct-id',
		hidden:true,
		header:biolims.common.productId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'crmProduct-name',
		header:biolims.common.productName,
		
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'samplingLocation-id',
		hidden:true,
		header:biolims.sample.samplingLocationId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'samplingLocation-name',
		header:biolims.sample.samplingLocationName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'samplingNumber',
		header:biolims.common.code,
		width:20*6,
		
		sortable:true
	});
	var pathologyConfirmedstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ]]
	});
	
	var pathologyConfirmedComboxFun = new Ext.form.ComboBox({
		store : pathologyConfirmedstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'pathologyConfirmed',
		header:biolims.sample.pathologyConfirmed,
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(pathologyConfirmedComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'bloodSampleDate',
		header:biolims.sample.bloodSampleDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'plasmapheresisDate',
		header:biolims.sample.plasmapheresisDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'commissioner-id',
		hidden:true,
		header:biolims.common.commissionerId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'commissioner-name',
		header:biolims.common.commissioner,
		
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'receivedDate',
		header:biolims.sample.receivedDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleTypeId',
		header:biolims.common.sampleTypeId,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleTypeName',
		header:biolims.common.sampleTypeName,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'sampleOrder-id',
		hidden:true,
		header:biolims.sample.sampleOrderId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'sampleOrder-name',
		header:biolims.sample.sampleOrderName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
			dataIndex:'medicalNumber',
			header:biolims.sample.medicalNumber,
			width:50*6,
			
			sortable:true
		});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.sample.sampleCode,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'family',
		header:biolims.sample.family,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'familyPhone',
		header:biolims.sample.familyPhone,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'familySite',
		header:biolims.sample.familySite,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'medicalInstitutions',
		header:biolims.sample.medicalInstitutions,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'medicalInstitutionsPhone',
		header:biolims.sample.medicalInstitutionsPhone,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'medicalInstitutionsSite',
		header:biolims.sample.medicalInstitutionsSite,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'attendingDoctor',
		header:biolims.sample.attendingDoctor,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'attendingDoctorPhone',
		header:biolims.sample.attendingDoctorPhone,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'attendingDoctorSite',
		header:biolims.sample.attendingDoctorSite,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:biolims.wk.approverId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.approver,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.wk.approverDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleInfoMain',
		header:biolims.sample.sampleInfoMain,
		width:20*6,
		hidden:true,
		sortable:true
	});
	//肿瘤类型
//	cm.push({
//		dataIndex:'cancerType-id',
//		hidden:true,
//		header:'肿瘤类型id',
//		width:20*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'cancerType-name',
//		header:'肿瘤类型',
//		width:20*10,
//		sortable:true
//		});

//	cm.push({
//		dataIndex:'cancerTypeSeedOne-id',
//		hidden:true,
//		header:'肿瘤子类一ID',
//		width:50*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'cancerTypeSeedOne-name',
//		header:'肿瘤子类一',
//		
//		width:50*10,
//		sortable:true
//		});
//	cm.push({
//		dataIndex:'cancerTypeSeedTwo-id',
//		hidden:true,
//		header:'肿瘤子类二ID',
//		width:50*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'cancerTypeSeedTwo-name',
//		header:'肿瘤子类二',
//		
//		width:50*10,
//		sortable:true
//		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleCancerTemp/showSampleCancerTempListJson.action";
	var opts={};
	opts.title=biolims.sample.testOrder;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sampleCancerTempGrid=gridTable("show_sampleCancerTemp_div",cols,loadParam,opts);
});

function add(){
		window.location=window.ctx+'/sample/sampleCancerTemp/editSampleCancerTemp.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/sample/sampleCancerTemp/editSampleCancerTemp.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/sampleCancerTemp/viewSampleCancerTemp.action?id=' + id;
}
function exportexcel() {
	sampleCancerTempGrid.title = biolims.common.exportList;
	var vExportContent = sampleCancerTempGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function getValue()
{
    var i=0;
    var values='';
    var checked = $("input:checked");//获取所有被选中的标签元素
    for(i=0;i<checked.length;i++)
        { //将所有被选中的标签元素的值保存成一个字符串，以逗号隔开
            if(i<checked.length-1)
            values+=checked[i].value+',';
            else
                values+=checked[i].value;
        }
      //alert(values);
        var myDate = new Date();
        
         f1.document.write(values);
            f1.document.execCommand("SaveAs",false,document.title+"_"+myDate.toLocaleDateString()+".txt");
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startbirthDate").val() != undefined) && ($("#startbirthDate").val() != '')) {
					var startbirthDatestr = ">=##@@##" + $("#startbirthDate").val();
					$("#birthDate1").val(startbirthDatestr);
				}
				if (($("#endbirthDate").val() != undefined) && ($("#endbirthDate").val() != '')) {
					var endbirthDatestr = "<=##@@##" + $("#endbirthDate").val();

					$("#birthDate2").val(endbirthDatestr);

				}
				
				if (($("#startdiagnosisDate").val() != undefined) && ($("#startdiagnosisDate").val() != '')) {
					var startdiagnosisDatestr = ">=##@@##" + $("#startdiagnosisDate").val();
					$("#diagnosisDate1").val(startdiagnosisDatestr);
				}
				if (($("#enddiagnosisDate").val() != undefined) && ($("#enddiagnosisDate").val() != '')) {
					var enddiagnosisDatestr = "<=##@@##" + $("#enddiagnosisDate").val();

					$("#diagnosisDate2").val(enddiagnosisDatestr);

				}
				
				if (($("#startsamplingDate").val() != undefined) && ($("#startsamplingDate").val() != '')) {
					var startsamplingDatestr = ">=##@@##" + $("#startsamplingDate").val();
					$("#samplingDate1").val(startsamplingDatestr);
				}
				if (($("#endsamplingDate").val() != undefined) && ($("#endsamplingDate").val() != '')) {
					var endsamplingDatestr = "<=##@@##" + $("#endsamplingDate").val();

					$("#samplingDate2").val(endsamplingDatestr);

				}
				
				if (($("#startbloodSampleDate").val() != undefined) && ($("#startbloodSampleDate").val() != '')) {
					var startbloodSampleDatestr = ">=##@@##" + $("#startbloodSampleDate").val();
					$("#bloodSampleDate1").val(startbloodSampleDatestr);
				}
				if (($("#endbloodSampleDate").val() != undefined) && ($("#endbloodSampleDate").val() != '')) {
					var endbloodSampleDatestr = "<=##@@##" + $("#endbloodSampleDate").val();

					$("#bloodSampleDate2").val(endbloodSampleDatestr);

				}
				
				if (($("#startplasmapheresisDate").val() != undefined) && ($("#startplasmapheresisDate").val() != '')) {
					var startplasmapheresisDatestr = ">=##@@##" + $("#startplasmapheresisDate").val();
					$("#plasmapheresisDate1").val(startplasmapheresisDatestr);
				}
				if (($("#endplasmapheresisDate").val() != undefined) && ($("#endplasmapheresisDate").val() != '')) {
					var endplasmapheresisDatestr = "<=##@@##" + $("#endplasmapheresisDate").val();

					$("#plasmapheresisDate2").val(endplasmapheresisDatestr);

				}
				
				if (($("#startreceivedDate").val() != undefined) && ($("#startreceivedDate").val() != '')) {
					var startreceivedDatestr = ">=##@@##" + $("#startreceivedDate").val();
					$("#receivedDate1").val(startreceivedDatestr);
				}
				if (($("#endreceivedDate").val() != undefined) && ($("#endreceivedDate").val() != '')) {
					var endreceivedDatestr = "<=##@@##" + $("#endreceivedDate").val();

					$("#receivedDate2").val(endreceivedDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				
				
				commonSearchAction(sampleCancerTempGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
