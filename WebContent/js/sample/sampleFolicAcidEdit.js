﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#upload_imga_id10").val()==''){
		$(":input").addClass(".text input readonlytrue");
		$("[type='text']").focus(function () 
		{
			message(biolims.sample.pleaseUploadImage);
			return; 
		}) ;
	}
});
function add() {
	window.location = window.ctx + "/sample/sampleInput/editSampleInput.action";
}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInput/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {	
	//sampleInfo 的图片Id
	var sampleInfoImg = $("#upload_imga_id10").val();
	//sampleInputTemp 的图片Id
	var sampleBreastCancerTempImg = $("#upload_imga_id").val();
	if($("#upload_imga_name11").val()!="" && sampleInfoImg!="" && sampleInfoImg == sampleBreastCancerTempImg){
		//检测项目验证
		var productName = $("#sampleFolicAcidTemp_product_name").val();
		if (productName == "") {
			message(biolims.sample.productNameIsEmpty);
			return;
		}
		
	}else {
		if($("#upload_imga_id").val()==""){
			message(biolims.sample.pleaseUploadImage2Save);
			return;
		}
	}

	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleFolicAcidTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleFolicAcidTemp_id").val(),
					title : $("#sampleInput_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleFolicAcidTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});


/*
 * 上传图片
 */
function upLoadImg1(){
	var isUpload = true;
	var fId=$("#upload_imga_id11").val();
	var fName=$("#upload_imga_name").val();
	
	load("/system/template/template/toSampeUpload.action", { // 是否修改
		fileId:fId,
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			fName=data.fileName;
			fId=data.fileId;
			 document.getElementById('upload_imga_id').value=fId;
			 document.getElementById('upload_imga_name11').value=fName;
		});
	});
}


function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInput/save.action?saveType="+$("#saveType").val()+"&imgId="+$("#upload_imga_id").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInput/copySampleInput.action?id=' + $("#sampleFolicAcidTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleInput_id").val()){
		commonChangeState("formId=" + $("#sampleFolicAcidTemp_id").val() + "&tableId=SampleFolicAcidTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleFolicAcidTemp_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.infoInput,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleFolicAcidTemp_sampleType_id").value = id;
		 document.getElementById("sampleFolicAcidTemp_sampleType_name").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:600,height:700,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("sampleFolicAcidTemp_voucherType").value = id;
			 document.getElementById("sampleFolicAcidTemp_voucherType_name").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleFolicAcidTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleFolicAcidTemp_voucherCode").val())){
				var id = $("#sampleFolicAcidTemp_voucherCode").val();
				ajax("post", "/sample/sampleInput/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleFolicAcidTemp_phoneNum").val())){
				//return;
				var id = $("#sampleFolicAcidTemp_phoneNum").val();
				ajax("post", "/sample/sampleInput/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

		//家庭住址验证	 
		function checkAddress(){
			var address = $("#sampleFolicAcidTemp_address").val();
			if(address==""){
				message(biolims.sample.addressIsEmpty);
		}
}
		//B超异常提醒验证
		function change(){
			var reg = $('#sampleFolicAcidTemp_embryoType option:selected').val();
			if(reg =="2"){
				$("#sampleFolicAcidTemp_messages").css("display","");
			}
		}
		
		
		
		
		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 600,
						height : 700,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("sampleFolicAcidTemp_product_id").value = id;
							document.getElementById("sampleFolicAcidTemp_product_name").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}
