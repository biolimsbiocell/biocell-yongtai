$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "样本类型ID",
	});
	colOpts.push({
		"data": "name",
		"title": "样本类型",
	});
	colOpts.push({
		"data": "code",
		"title": "样本标记",
	});
	var tbarOpts = [];
	var addSampleTypeOptions = table(true, null,
			'/sample/dicSampleType/showDialogDicSampleTypeTableJson.action',colOpts , tbarOpts)
		var sampleTypeTable = renderData($("#addDicSampleType"), addSampleTypeOptions);
	$("#addDicSampleType").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addDicSampleType_wrapper .dt-buttons").empty();
		$('#addDicSampleType_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addDicSampleType tbody tr");
	sampleTypeTable.ajax.reload();
	sampleTypeTable.on('draw', function() {
		trs = $("#addDicSampleType tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

