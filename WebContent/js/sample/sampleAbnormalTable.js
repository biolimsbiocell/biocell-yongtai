var sampleAbnormalTab,oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "patientName",
		"title": biolims.common.patientName,
		"createdCell": function(td) {
			$(td).attr("saveName", "patientName");
		}
	});
	colOpts.push({
		"data": "dicSampleType-id",
		"title": biolims.common.sampleTypeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	});
	colOpts.push({
		"data": "dicSampleType-name",
		"title": biolims.common.sampleType,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-name");
		}
	});
	colOpts.push({
		"data": "orderId",
		"title": biolims.common.orderId,
		"createdCell": function(td) {
			$(td).attr("saveName", "orderId");
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.master.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "nextFlowId",
		"visible":false,
		"title": biolims.common.nextFlowId ,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
		"data": "nextFlow",
		"visible":false,
		"title": biolims.common.nextFlow,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlow");
		}
	})
	colOpts.push({
		"data": "method",
		"title": biolims.common.result,
//		"className": "select",
		"name": biolims.common.qualified+"|"+biolims.common.disqualified+"|"+"备用空管",
		"createdCell": function(td) {
			$(td).attr("saveName", "method");
			$(td).attr("selectOpt", biolims.common.qualified+"|"+biolims.common.disqualified+"|"+"备用空管");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.qualified;
			}
			if(data =="3"){
				return "备用空管"
			}
			if(data == "0") {
				return biolims.common.disqualified;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"visible":false,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text:biolims.sample.checkOrder,
		action: function() {
			viewBloodSplit();
		}
	});
//	tbarOpts.push({
//		text: biolims.common.nextFlow,
//		action: function() {
//			nextFlow();
//		}
//	});
	var sampleAbnormalOps = table(true, null, "/sample/sampleAbnormal/showSampleAbnormalTableJson.action", colOpts, tbarOpts);
	sampleAbnormalTab = renderData($("#sampleAbnormaldiv"), sampleAbnormalOps);
	//dwb 日志 2018-05-12 10:11:19
	sampleAbnormalTab.on('draw', function() {
		oldChangeLog = sampleAbnormalTab.ajax.json();
	});
});
//下一步流向
function nextFlow() {
	var rows = $("#sampleAbnormaldiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var productId = "";
	var sampleType="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		sampleType = $(k).find("td[savename='dicSampleType-id']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=SampleReceive&productId="
					+ productIds+"&sampleType="+sampleType,''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}
// 保存
function saveItem() {
	var data = saveItemjson($("#sampleAbnormaldiv"));
	$.ajax({
		type: 'post',
		url: '/sample/sampleAbnormal/saveSampleAbnormal.action',
		data: {
			itemDataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed);
			};
		}
	})
}

//dwb 日志 2018-05-11 13:53:56
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += biolims.common.sampleCode+'"' + v.sampleCode + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if(k == "method") {
				var method = $(tds[j]).text();
				if(method == biolims.common.disqualified) {
					json[k] = "0";
				} else if(method == biolims.common.qualified) {
					json[k] = "1";
				}else if(method == "备用空管"){
					json[k] = "3";
				}
				continue;
			}
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//查看任务单
function viewBloodSplit(){
	var id = $(".selected").find("td[savename='orderId']").text();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/sample/sampleReceive/editSampleReceive.action?id=" + id;
}
function executeAbnormal(){
	var rows = $("#sampleAbnormaldiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.selectYouWant);
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find(".icheck").val());
	});
	var data = saveItemjson($("#sampleAbnormaldiv"));
	var changeLog = "样本中心-异常管理：";
	changeLog = getChangeLog(data, $("#sampleAbnormaldiv"), changeLog);
	var changeLogs="";
	if(changeLog !="样本中心-异常管理："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/sample/sampleAbnormal/executeAbnormal.action',
		data: {
			ids:ids,
			dataJson: data,
			changeLog:changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
function save(){
	var data = saveItemjson($("#sampleAbnormaldiv"));
	var changeLog = "样本中心-异常管理：";
	changeLog = getChangeLog(data, $("#sampleAbnormaldiv"), changeLog);
	var changeLogs="";
	if(changeLog !="样本中心-异常管理："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/sample/sampleAbnormal/save.action',
		data: {
			dataJson: data,
			changeLog:changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			};
		}
	})
}
function feedback(){
	var rows = $("#sampleAbnormaldiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.selectYouWant);
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find(".icheck").val());
	});
	var data = saveItemjson($("#sampleAbnormaldiv"));
	$.ajax({
		type: 'post',
		url: '/sample/sampleAbnormal/feedbackAbnormal.action',
		data: {
			ids:ids,
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.Executesuccessfully);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.Onfailure)
			};
		}
	})
}