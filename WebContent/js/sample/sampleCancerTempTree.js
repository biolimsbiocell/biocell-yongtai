function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/sample/sampleCancerTemp/editSampleCancerTemp.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/sampleCancerTemp/viewSampleCancerTemp.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : biolims.sample.orderNum,
		dataIndex : 'orderNumber',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.id,
		dataIndex : 'id',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.sname,  //姓名
		dataIndex : 'name',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.gender,
		dataIndex : 'gender',
		width:10*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.birthDay,
		dataIndex : 'birthDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.diagnosisDate,
		dataIndex : 'diagnosisDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.tumorTypeAndStage,
		dataIndex : 'tumorTypeAndStage',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header :biolims.sample.inspectionDepartmentName,
		dataIndex : 'inspectionDepartment-name',
		width:50*6,
		hidden:false
	}, 
	    {
		header : biolims.common.productName,
		dataIndex : 'crmProduct-name',
		width:50*6,
		hidden:false
	}, 
	   
	{
		header : biolims.sample.samplingDate,
		dataIndex : 'samplingDate',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.sample.samplingLocationName,
		dataIndex : 'samplingLocation-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.common.code,
		dataIndex : 'samplingNumber',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.pathologyConfirmed,
		dataIndex : 'pathologyConfirmed',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.bloodSampleDate,
		dataIndex : 'bloodSampleDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.plasmapheresisDate,
		dataIndex : 'plasmapheresisDate',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.common.commissioner,
		dataIndex : 'commissioner-name',
		width:50*6,
		hidden:false
	}, 
	   
	{
		header : biolims.sample.receivedDate,
		dataIndex : 'receivedDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.sampleTypeId,
		dataIndex : 'sampleTypeId',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.sampleTypeName,
		dataIndex : 'sampleTypeName',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : biolims.sample.sampleOrderName,
		dataIndex : 'sampleOrder-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.sample.sampleCode,
		dataIndex : 'sampleCode',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.family,
		dataIndex : 'family',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.familyPhone,
		dataIndex : 'familyPhone',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.familySite,
		dataIndex : 'familySite',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.medicalInstitutions,
		dataIndex : 'medicalInstitutions',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.medicalInstitutionsPhone,
		dataIndex : 'medicalInstitutionsPhone',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.medicalInstitutionsSite,
		dataIndex : 'medicalInstitutionsSite',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.attendingDoctor,
		dataIndex : 'attendingDoctor',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.attendingDoctorPhone,
		dataIndex : 'attendingDoctorPhone',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.sample.attendingDoctorSite,
		dataIndex : 'attendingDoctorSite',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.note,
		dataIndex : 'note',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : biolims.sample.createUserName,
		dataIndex : 'createUser-name',
		width:50*6,
		hidden:false
	}, 
	   
	{
		header : biolims.sample.createDate,
		dataIndex : 'createDate',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.wk.approver,
		dataIndex : 'confirmUser-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.wk.approverDate,
		dataIndex : 'confirmDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.state,
		dataIndex : 'state',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.stateName,
		dataIndex : 'stateName',
		width:20*6,
		hidden:true
	}, 
	
			
			
			
			{
				header : biolims.common.upId,
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#sampleCancerTempTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
