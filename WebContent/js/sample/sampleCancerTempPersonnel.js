var sampleCancerTempPersonnelGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'checkOutTheAge',
		type:"string"
	});
	    fields.push({
		name:'sampleCancerTemp-id',
		type:"string"
	});
	    fields.push({
		name:'sampleCancerTemp-name',
		type:"string"
	});
	    fields.push({
		name:'tumorCategory-id',
		type:"string"
	});
	    fields.push({
		name:'tumorCategory-name',
		type:"string"
	});
	   fields.push({
		name:'familyRelation',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'checkOutTheAge',
		hidden : false,
		header:biolims.sample.checkOutTheAge,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'sampleCancerTemp-id',
//		hidden : true,
//		header:'相关主表ID',
//		width:20*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'sampleCancerTemp-name',
//		hidden : false,
//		header:'相关主表',
//		width:20*10
//	});
	cm.push({
		dataIndex:'tumorCategory-id',
		hidden : true,
		header:biolims.sample.dicTypeId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tumorCategory-name',
		hidden : false,
		header:biolims.sample.dicTypeName,
		width:20*10
	});
	cm.push({
		dataIndex:'familyRelation',
		hidden : false,
		header:biolims.sample.familyRelation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleCancerTemp/showSampleCancerTempPersonnelListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.testOrder_familyHistory;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/sample/sampleCancerTemp/delSampleCancerTempPersonnel.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
			text : biolims.common.selectRelevantTable,
				handler : selectsampleCancerTempDialogFun
		});
	opts.tbar.push({
			text : biolims.sample.selectTumorCategory,
				handler : selecttumorCategoryFun
		});
	opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),biolims.common.batchUpload,null,{
				"Confirm":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = sampleCancerTempPersonnelGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
							sampleCancerTempPersonnelGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	sampleCancerTempPersonnelGrid=gridEditTable("sampleCancerTempPersonneldiv",cols,loadParam,opts);
	$("#sampleCancerTempPersonneldiv").data("sampleCancerTempPersonnelGrid", sampleCancerTempPersonnelGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectsampleCancerTempFun(){
	var win = Ext.getCmp('selectsampleCancerTemp');
	if (win) {win.close();}
	var selectsampleCancerTemp= new Ext.Window({
	id:'selectsampleCancerTemp',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectsampleCancerTemp.close(); }  }]  }) });  
    selectsampleCancerTemp.show(); }
	function setsampleCancerTemp(rec){
		var gridGrid = $("#sampleCancerTempPersonneldiv").data("sampleCancerTempPersonnelGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('sampleCancerTemp-id',rec.get('id'));
			obj.set('sampleCancerTemp-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectsampleCancerTemp')
		if(win){
			win.close();
		}
	}
	function selectsampleCancerTempDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/SampleCancerTempSelect.action?flag=sampleCancerTemp";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selsampleCancerTempVal(this);
				}
			}, true, option);
		}
	var selsampleCancerTempVal = function(win) {
		var operGrid = sampleCancerTempDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#sampleCancerTempPersonneldiv").data("sampleCancerTempPersonnelGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('sampleCancerTemp-id',rec.get('id'));
				obj.set('sampleCancerTemp-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
function selecttumorCategoryFun(){
	var win = Ext.getCmp('selecttumorCategory');
	if (win) {win.close();}
	var selecttumorCategory= new Ext.Window({
	id:'selecttumorCategory',modal:true,title:biolims.sample.selectTumorCategory,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=tumorCategory' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selecttumorCategory.close(); }  }]  }) ;  
    selecttumorCategory.show(); }
	function settumorCategory(rec){
		var gridGrid = $("#sampleCancerTempPersonneldiv").data("sampleCancerTempPersonnelGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('tumorCategory-id',rec.get('id'));
			obj.set('tumorCategory-name',rec.get('name'));
		});
		var win = Ext.getCmp('selecttumorCategory')
		if(win){
			win.close();
		}
	}
	function selecttumorCategoryDialogFun(){
			var title = '';
			var url = '';
			title = biolims.sample.selectTumorCategory;
			url = ctx + "/DicTypeSelect.action?flag=tumorCategory";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						seltumorCategoryVal(this);
				}
			}, true, option);
		}
	var seltumorCategoryVal = function(win) {
		var operGrid = dicTypeDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#sampleCancerTempPersonneldiv").data("sampleCancerTempPersonnelGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('tumorCategory-id',rec.get('id'));
				obj.set('tumorCategory-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
