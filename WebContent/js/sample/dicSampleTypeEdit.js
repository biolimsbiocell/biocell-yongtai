$(function() {
	
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	var mainFileInput=fileInput ('1','dicSampleType',$("#dicSampleType_id").val());
	fieldCustomFun();
	//渲染图标
	var picCode=$("#dicSampleType_picCode").val();
	if(picCode){
		$("."+picCode).parent("li").addClass("chosed");
	}
	//选择图标
	$(".site-doc-icon li").click(function () {
		$(".site-doc-icon .chosed").removeClass("chosed");
		$(this).addClass("chosed");
		var code=$(this).children(".code").text();
		$("#dicSampleType_picCode").val(code);
	});
	
})	
function change(id) {
	$("#" + id).css({
		"background-color" : "white",
		"color" : "black"
	});
}
function add() {
	window.location = window.ctx + "/sample/dicSampleType/editDicSampleType.action";
}

function list() {
	window.location = window.ctx + '/sample/dicSampleType/showDicSampleTypeList.action';
}
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
function tjsp() {
			top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {icon: 3, title:biolims.common.prompt,btn:biolims.common.selected}, function(index){
				top.layer.open({
					  title: biolims.common.submit,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toStartView.action?formName=DicSampleType",
					  yes: function(index, layer) {
						 var datas={
									userId : userId,
									userName : userName,
									formId : $("#dicSampleType_id").val(),
									title : $("#dicSampleType_name").val(),
									formName : 'DicSampleType'
								}
							ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
								if (data.success) {
									top.layer.msg(biolims.common.submitSuccess);
									if (typeof callback == 'function') {
										callback(data);
									}
									dialogWin.dialog("close");
								} else {
									top.layer.msg(biolims.common.submitFail);
								}
							}, null);
							top.layer.close(index);
						},
						cancel: function(index, layer) {
							top.layer.close(index)
						}
				
				});     
				  top.layer.close(index);
				});
}
function sp(){
	
		var taskId = $("#bpmTaskId").val();
		var formId = $("#dicSampleType_id").val();
		
					top.layer.open({
						  title: biolims.common.handle,
						  type:2,
						  anim: 2,
						  area: ['800px','500px']
						  ,btn: biolims.common.selected,
						  content: window.ctx+"/workflow/processinstance/toCompleteTaskView.action?taskId="+taskId+"&formId="+formId,
						  yes: function(index, layer) {
							  var operVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
							  var opinionVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();
							  var opinion =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinion").val();
								if(!operVal){
									top.layer.msg(biolims.common.pleaseSelectOper);
									return false;
								}
								if (operVal == "2") {
									_trunTodoTask(taskId, callback, dialogWin);
								} else {
									var paramData = {};
									paramData.oper = operVal;
									paramData.info = opinion;
					
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									}
									ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
										if (data.success) {
											top.layer.msg(biolims.common.submitSuccess);
											if (typeof callback == 'function') {
											}
										} else {
											top.layer.msg(biolims.common.submitFail);
										}
									}, null);
								}
								window.open(window.ctx+"/main/toPortal.action",'_parent');
							},
							cancel: function(index, layer) {
								top.layer.close(index)
							}
					
					});     
}

function ck(){
			top.layer.open({
					  title: biolims.common.checkFlowChart,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toTraceProcessInstanceView.action?formId=" + $("#dicSampleType_id").val(),
					  yes: function(index, layer) {
							top.layer.close(index)
					   },
						cancel: function(index, layer) {
							top.layer.close(index)
						}
				});     
}

/*function save() {
if(checkSubmit()==true){
		form1.action = window.ctx
				+ "/sample/dicSampleType/save.action?loadNumber=1";
		form1.submit();
	}
}*/

function save() {
if(checkSubmit()==true){

		//自定义字段
		//拼自定义字段（实验记录）
		var inputs = $("#fieldItemDiv input");
		var options = $("#fieldItemDiv option");
		var contentData = {};
		var checkboxArr = [];
		//必填验证
		var requiredField=requiredFilter();
			if(!requiredField){
				return false;
			}
		$("#fieldItemDiv .checkboxs").each(function(i, v) {
			$(v).find("input").each(function(ii, inp) {
				var k = inp.name;
				if(inp.checked == true) {
					checkboxArr.push(inp.value);
					contentData[k] = checkboxArr;
				}
			});
		});
		inputs.each(function(i, inp) {
			var k = inp.name;
			if(inp.type != "checkbox") {
				contentData[k] = inp.value;
			}
		});
		options.each(function(i, opt) {
			if(opt.selected == true) {
				var k = opt.getAttribute("name");
				contentData[k] = opt.value;
			}
		});
		document.getElementById("fieldContent").value = JSON.stringify(contentData);
		
		var changeLog = "样本类型：";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		var changeLogs="";
		if(changeLog !="样本类型："){
			changeLogs=changeLog;
		}
		top.layer.load(4, {shade:0.3}); 
		var jsonStr = JSON.stringify($("#form1").serializeObject());  
		$.ajax({
			url: ctx + '/sample/dicSampleType/save.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLogs,
				bpmTaskId : $("#bpmTaskId").val()
			},
			success: function(data) {
				//关闭加载层
				//top.layer.close(index);
				if(data.success) {
					top.layer.closeAll();
					var url = "/sample/dicSampleType/editDicSampleType.action?id="+data.id;
					if(data.bpmTaskId){
						url = url +"&bpmTaskId="+data.bpmTaskId;
					}
					window.location.href=url;
				}else{
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveFailed);
				   }
				}
		});
	}
}		

$.fn.serializeObject = function() {
	   var o = {};  
	    var a = this.serializeArray();  
	    $.each(a, function() {  
	        if (o[this.name]) {  
	            if (!o[this.name].push) {  
	                o[this.name] = [ o[this.name] ];  
	            }  
	            o[this.name].push(this.value || '');  
	        } else {  
	            o[this.name] = this.value || '';  
	        }  
	    });  
	    return o;  
};



function editCopy() {
	window.location = window.ctx + '/sample/dicSampleType/copyDicSampleType.action?id=' + $("#dicSampleType_id").val();
}
function changeState() {
	var paraStr="formId=" + $("#dicSampleType_id").val()
	+ "&tableId=DicSampleType";
	top.layer.open({
		  title: biolims.common.changeState,
		  type:2,
		  anim: 2,
		  area: ['400px','400px']
		  ,btn: biolims.common.selected,
		  content: window.ctx
			+ "/applicationTypeAction/applicationTypeActionLook.action?" + paraStr
			+ "&flag=changeState'",
		  yes: function(index, layer) {
			  top.layer.confirm(biolims.common.toSubmit, {icon: 3, title:biolims.common.prompt,btn:biolims.common.selected}, function(index){
				 ajax("post","/applicationTypeAction/exeFun.action",{
					 applicationTypeActionId:$('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					 formId:$("#dicSampleType_id").val()
				 },function(response){
					 var respText = response.message;
					 if (respText == '') {
							window.location.reload();
						} else {
							top.layer.msg(respText);
						}
				 },null)
				 top.layer.close(index);
			 })
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}
	
	});   
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
/*	fs.push($("#dicSampleType_id").val());
	nsc.push("样本编码"+biolims.common.describeEmpty);*/
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			top.layer.msg(mess);
			return false;
		}
		return true;
}
function fileUp(){
	if(!$("#dicSampleType_id").val()){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
function fileView(){
	top.layer.open({
		title:biolims.common.attachment,
		type:2,
		skin: 'layui-top.layer-lan',
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		content:window.ctx+"/operfile/initFileList.action?flag=1&modelType=dicSampleType&id="+$("#dicSampleType_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
function settextreadonly() {
    jQuery(":text, textarea").each(function() {
	var _vId = jQuery(this).attr('id');
	jQuery(this).css("background-color","#B4BAB5").attr("readonly", "readOnly");
	if (_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}
function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}


function showtype(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/dic/type/dicTypeSelectTable.action?flag=yblx",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#dicSampleType_type_id").val(id)
		$("#dicSampleType_type_name").val(name)
		},
	})
}
//下一步流向
function nextflow(){
	top.layer.open({
		title: biolims.common.selectNextFlow,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/nextFlow/nextFlow/selectAllNextFlow.action", ''
		],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(name);
			rows.find("td[savename='nextFlowId']").text(id);
			top.layer.close(index)
		},
	})
}

function showstate(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/dic/state/dicStateSelectTable.action?flag=commonState",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#dicSampleType_state").val(id)
		$("#dicSampleType_stateName").val(name)
		},
	})
}
//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

		//查找自定义列表的订单模块的此检测项目的相关内容
		$.ajax({
			type: "post",
			url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
			data: {
				moduleValue: "DicSampleType"
			},
			async: false,
			success: function(data) {
				var objValue = JSON.parse(data);
				if(objValue.success) {
					$.each(objValue.data, function(i, n) {
						var inputs = '';
						var disabled = n.readOnly ? ' ' : "disabled";
						var defaultValue = n.defaultValue ? n.defaultValue : ' ';
						if(n.fieldType == "checkbox") {
							var checkboxs = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
						} else if(n.fieldType == "radio") {
							var options = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
						} else if(n.fieldType == "date") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + ' required=' + n.isRequired + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}

						$("#fieldItemDiv").append(inputs);
					});

				} else {
					top.layer.msg(biolims.customList.updateFailed);
				}
			}
		});
	

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
						}
					}
				});
			};
		}
	}
	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}
