﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	if($("#sampleFolicAcidTemp_name").val() != ""){
		$("#folicAcidTempNew_name").val($("#sampleFolicAcidTemp_name").val());
	}
	if($("#folicAcidTemp_name").val() != ""){
		$("#folicAcidTempNew_name").val($("#folicAcidTemp_name").val());
	}
	
	if($("#sampleFolicAcidTemp_productName").val()!=$("#folicAcidTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleFolicAcidTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleFolicAcidTemp_productId").val());
		$("#productNameNew").val($("#sampleFolicAcidTemp_productName").val());
	}
	
	if($("#sampleFolicAcidTemp_sampleType_id").val()!=$("#folicAcidTemp_sampleType_id").val()){
		$("#sampleType").css({"background-color":"red","color":"white"});
	}else{
		$("#sampleTypeIdNew").val($("#sampleFolicAcidTemp_sampleType_id").val());
		$("#sampleTypeNew").val($("#sampleFolicAcidTemp_sampleType_name").val());
	}
	
	if($("#sampleFolicAcidTemp_voucherType_id").val()!=$("#folicAcidTemp_voucherType_id").val()){
		$("#voucherType").css({"background-color":"red","color":"white"});
	}else{
		$("#voucherTypeIdNew").val($("#sampleFolicAcidTemp_voucherType_id").val());
		$("#voucherTypeNew").val($("#sampleFolicAcidTemp_voucherType_name").val());
	}
	
	if($("#sampleFolicAcidTemp_acceptDate").val()!=$("#folicAcidTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleFolicAcidTemp_acceptDate").val());
	}
	
	if($("#sampleFolicAcidTemp_area").val()!=$("#folicAcidTemp_area").val()){
		$("#area").css({"background-color":"red","color":"white"});
	}else{
		$("#areaNew").val($("#sampleFolicAcidTemp_area").val());
	}
	
	if($("#sampleFolicAcidTemp_hospital").val()!=$("#folicAcidTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleFolicAcidTemp_hospital").val());
	}
	
	if($("#sampleFolicAcidTemp_sendDate").val()!=$("#folicAcidTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleFolicAcidTemp_sendDate").val());
	}
	
	if($("#sampleFolicAcidTemp_reportDate").val()!=$("#folicAcidTemp_sendDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleFolicAcidTemp_reportDate").val());
	}
	
	if($("#sampleFolicAcidTemp_patientName").val()!=$("#folicAcidTemp_patientName").val()){
		$("#patientName").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameNew").val($("#sampleFolicAcidTemp_patientName").val());
	}
	
	if($("#sampleFolicAcidTemp_patientName").val()!=$("#folicAcidTemp_phoneNum").val()){
		$("#phoneNum").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNumNew").val($("#sampleFolicAcidTemp_phoneNum").val());
	}
	
	if($("#sampleFolicAcidTemp_address").val()!=$("#folicAcidTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleFolicAcidTemp_address").val());
	}
	
	if($("#sampleFolicAcidTemp_otherTumorHistory").val()!=$("#folicAcidTemp_otherTumorHistory").val()){
		$("#otherTumorHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#otherTumorHistoryNew").val($("#sampleFolicAcidTemp_otherTumorHistory").val());
	}
	
	if($("#sampleFolicAcidTemp_pregnancyTime").val()!=$("#folicAcidTemp_pregnancyTime").val()){
		$("#pregnancyTime").css({"background-color":"red","color":"white"});
	}else{
		$("#pregnancyTimeNew").val($("#sampleFolicAcidTemp_pregnancyTime").val());
	}
	
	if($("#sampleFolicAcidTemp_parturitionTime").val()!=$("#folicAcidTemp_parturitionTime").val()){
		$("#parturitionTime").css({"background-color":"red","color":"white"});
	}else{
		$("#parturitionTimeNew").val($("#sampleFolicAcidTemp_parturitionTime").val());
	}
	
	if($("#sampleFolicAcidTemp_confirmAgeOne").val()!=$("#folicAcidTemp_confirmAgeOne").val()){
		$("#confirmAgeOne").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeOneNew").val($("#sampleFolicAcidTemp_confirmAgeOne").val());
	}
	
	if($("#sampleFolicAcidTemp_badMotherhood").val()!=$("#folicAcidTemp_badMotherhood").val()){
		$("#badMotherhood").css({"background-color":"red","color":"white"});
	}else{
		$("#badMotherhoodNew").val($("#sampleFolicAcidTemp_badMotherhood").val());
	}
	
	if($("#sampleFolicAcidTemp_isPay").val()!=$("#folicAcidTemp_isPay").val()){
		$("#folicAcidTempNew_isPay").css({"background-color":"red","color":"white"});
	}else{
		$("#folicAcidTempNew_isPay").val($("#sampleFolicAcidTemp_isPay").val());
	}
	
	if($("#sampleFolicAcidTemp_isInvoice").val()!=$("#folicAcidTemp_isInvoice").val()){
		$("#folicAcidTempNew_isInvoice").css({"background-color":"red","color":"white"});
	}else{
		$("#folicAcidTempNew_isInvoice").val($("#sampleFolicAcidTemp_isInvoice").val());
	}
	
	if($("#sampleFolicAcidTemp_money").val()!=$("#folicAcidTemp_money").val()){
		$("#money").css({"background-color":"red","color":"white"});
	}else{
		$("#moneyNew").val($("#sampleFolicAcidTemp_money").val());
	}
	
	if($("#sampleFolicAcidTemp_receipt").val()!=$("#folicAcidTemp_receipt").val()){
		$("#receipt").css({"background-color":"red","color":"white"});
	}else{
		$("#receiptNew").val($("#sampleFolicAcidTemp_receipt").val());
	}
	
	if($("#sampleFolicAcidTemp_paymentUnit").val()!=$("#folicAcidTemp_paymentUnit").val()){
		$("#paymentUnit").css({"background-color":"red","color":"white"});
	}else{
		$("#paymentUnitNew").val($("#sampleFolicAcidTemp_paymentUnit").val());
	}
	
	if($("#sampleFolicAcidTemp_reportMan_id").val()!=$("#folicAcidTemp_reportMan_id").val()){
		$("#reportMan").css({"background-color":"red","color":"white"});
	}else{
		$("#reportManIdNew").val($("#sampleFolicAcidTemp_reportMan_id").val());
		$("#reportManNew").val($("#sampleFolicAcidTemp_reportMan_name").val());
	}
	
	if($("#sampleFolicAcidTemp_auditMan_id").val()!=$("#folicAcidTemp_auditMan_id").val()){
		$("#auditMan").css({"background-color":"red","color":"white"});
	}else{
		$("#auditManIdNew").val($("#sampleFolicAcidTemp_auditMan_id").val());
		$("#auditManNew").val($("#sampleFolicAcidTemp_auditMan_name").val());
	}
	
	if($("#sampleFolicAcidTemp_reason").val()!=""){
		$("#reasonNew").val($("#sampleFolicAcidTemp_reason").val());
	}
	if($("#folicAcidTemp_reason").val()!=""){
		$("#reasonNew").val($("#folicAcidTemp_reason").val());
	}
	if($("#sampleFolicAcidTemp_reason").val()!="" && $("#folicAcidTemp_reason").val()!=""){
		$("#reasonNew").val($("#sampleFolicAcidTemp_reason").val());
	}
	
});
//function add() {
//	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
//}
load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {	

//	var reg =$("#sampleFolicAcidTemp_address").val();
//	if(reg==""){
//		message("家庭住址不能为空！");
//		return;
//	}
	
	var nextStepFlow =$("#folicAcidTempNew_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.selectNextFlows);
		return;
	}
	

	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleFolicAcidTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleFolicAcidTemp_id").val(),
					title : $("#sampleInput_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleFolicAcidTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});



function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action?saveType="+$("#saveType").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInputTemp/copySampleInput.action?id=' + $("#sampleFolicAcidTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleInput_id").val()){
		commonChangeState("formId=" + $("#sampleFolicAcidTemp_id").val() + "&tableId=SampleFolicAcidTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.folateAudit,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleTypeIdNew").value = id;
		 document.getElementById("sampleTypeNew").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:600,height:700,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("sampleFolicAcidTemp_voucherType").value = id;
			 document.getElementById("sampleFolicAcidTemp_voucherType_name").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleFolicAcidTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleFolicAcidTemp_voucherCode").val())){
				var id = $("#sampleFolicAcidTemp_voucherCode").val();
				ajax("post", "/sample/sampleInputTemp/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleFolicAcidTemp_phoneNum").val())){
				//return;
				var id = $("#sampleFolicAcidTemp_phoneNum").val();
				ajax("post", "/sample/sampleInputTemp/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

		//家庭住址验证	 
		function checkAddress(){
			var address = $("#sampleFolicAcidTemp_address").val();
			if(address==""){
				message(biolims.sample.addressIsEmpty);
		}
}
		//B超异常提醒验证
		function change(){
			var reg = $('#sampleFolicAcidTemp_embryoType option:selected').val();
			if(reg =="2"){
				$("#sampleFolicAcidTemp_messages").css("display","");
			}
		}
		
		
		
		
		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 600,
						height : 700,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("productIdNew").value = id;
							document.getElementById("productNameNew").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}
