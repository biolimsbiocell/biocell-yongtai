var sampleInputGrid;
$(function(){
	var cols={};
    var fields=[];
	cols.sm = true;
    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
    fields.push({
    	name:'upLoadAccessory-fileName',
    	type:'string'
    });
    fields.push({
    	name:'upLoadAccessory-id',
    	type:'string'
    });
	fields.push({
		name:'productId',
		type:"string"
	});
    fields.push({
			name:'productName',
			type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    
		    
    fields.push({
		name:'upImgTime',
		type:"date",
		dateFormat:"Y-m-d"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
	
		width:20*6,
		
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'upImgTime',
		header:biolims.sample.upImgTime,
		width:25*6,
		sortable:true,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.testProject,
		width:20*6,
		hidden:false,
		sortable:true
	});
	
	
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowState,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowStateName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	
	cm.push({
		dataIndex:'upLoadAccessory-fileName',
		hidden : false,
		header:biolims.sample.upLoadAccessoryName,
		width:20*6,
	});
	cm.push({
		dataIndex:'upLoadAccessory-id',
		hidden : true,
		header:biolims.sample.upLoadAccessoryId,
		width:20*6,
	});
	
	

	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleInput/showSampleInputListJson1.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.sample.infoInput;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
//	opts.tbar.push({
//		text : '填加明细',
//		handler : null
//	});
	//批量上传图片的按钮
	opts.tbar.push({
		text:biolims.common.batchUploadPictures,
		width : 80,
			handler : function(rowIndex, colIndex) {
			var win = Ext.getCmp('doc');
			if (win) {
				win.close();
			}
			var doc = new Ext.Window(
					{
						id : 'doc',
						modal : true,
						title : biolims.common.attachment,
						layout : 'fit',
						width : 900,
						height : 500,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=sampleInput&flag=doc' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
								buttons : [ {
									text : biolims.common.close,
									handler : function() {
										doc.close();
									}
								} ]
					});
			doc.show();
		}
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : function(){
			var ob = sampleInputGrid.getStore().recordType;
			sampleInputGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("state","3");
			p.set("stateName",biolims.common.create);
			sampleInputGrid.getStore().add(p);
			sampleInputGrid.startEditing(0,0);
		}
	});
	opts.tbar.push({
		text : biolims.common.save,
		handler : save
	});
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		text : biolims.sample.upLoadOrderPicture,
		handler : function() {
			var isUpload = true;
			var record = sampleInputGrid.getSelectOneRecord();
			load("/system/template/template/toSampeUpload.action", { // 是否修改
				fileId : record.get("upLoadAccessory-id"),
				isUpload : isUpload
			}, null, function() {
				$("#upload_file_div").data("callback", function(data) {
					record.set("upLoadAccessory-fileName", data.fileName);
					record.set("upLoadAccessory-id", data.fileId);
				});
			});
		}
	});
	
	
	sampleInputGrid=gridEditTable("show_sampleInput_div",cols,loadParam,opts);
	$("#show_sampleInput_div").data("sampleInputGrid", sampleInputGrid);
});

//点击保存按钮利用ajax保存到SampleInfo表里（没有选中框的）
function save(){	

//	var selectRecord = sampleInputGrid.store;
//	var inItemGrid = $("#show_sampleInput_div").data("sampleInputGrid");
//	for(var i =0; i<selectRecord.getCount(); i++){
//		var codes = selectRecord.getAt(i).get("code");
//		ajax("post","/sample/sampleInput/isCodeRepeart.action",{
//			code:codes
//		},function(data){
//			if(data.success){
//				if(data.data){
//					message("输入的样本编号重复！");
//				}
//			}
//		},null);
//	}
	var itemJson = commonGetModifyRecords(sampleInputGrid);

	if(itemJson.length>0){
//		message("不能同时保存多条");
//		return;
//	}else if(selectRecord.length>0){
//		$.each(selectRecord, function(i, obj) {
			ajax("post", "/sample/sampleInput/saveToSampleInfo.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#show_sampleInput_div").data("sampleInputGrid").getStore().commitChanges();
					$("#show_sampleInput_div").data("sampleInputGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
}
//填加明细
//function addData(){
//	window.location=window.ctx+'/operfile/initFileList.action';
//}

//新建
function add(){
		window.location=window.ctx+'/sample/sampleInput/editSampleInput.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	var r = sampleInputGrid.getSelectOneRecord();
	window.location=window.ctx+'/sample/sampleInput/editSampleInput.action?imgId=' + r.get("upLoadAccessory-id")+'&number='+r.get("id");
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/sampleInput/viewSampleInput.action?id=' + id;
}
function exportexcel() {
	sampleInputGrid.title = biolims.common.exportList;
	var vExportContent = sampleInputGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startbirthday").val() != undefined) && ($("#startbirthday").val() != '')) {
					var startbirthdaystr = ">=##@@##" + $("#startbirthday").val();
					$("#birthday1").val(startbirthdaystr);
				}
				if (($("#endbirthday").val() != undefined) && ($("#endbirthday").val() != '')) {
					var endbirthdaystr = "<=##@@##" + $("#endbirthday").val();

					$("#birthday2").val(endbirthdaystr);

				}
				
				
				commonSearchAction(sampleInputGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
