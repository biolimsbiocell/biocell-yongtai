﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	if($("#sampleBreastCancerTemp_name").val() != ""){
		$("#breastCancerTempNew_name").val($("#sampleBreastCancerTemp_name").val());
	}
	if($("#breastCancerTemp_name").val() != ""){
		$("#breastCancerTempNew_name").val($("#breastCancerTemp_name").val());
	}
	
	if($("#sampleBreastCancerTemp_productName").val()!=$("#breastCancerTemp_productName").val()){
		$("#productName").css({"background-color":"red","color":"white"});
		$("#productIdNew").val($("#sampleBreastCancerTemp_productId").val());
	}else{
		$("#productIdNew").val($("#sampleBreastCancerTemp_productId").val());
		$("#productNameNew").val($("#sampleBreastCancerTemp_productName").val());
	}
	
	if($("#sampleBreastCancerTemp_acceptDate").val()!=$("#breastCancerTemp_acceptDate").val()){
		$("#acceptDate").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptDateNew").val($("#sampleBreastCancerTemp_acceptDate").val());
	}
	
	if($("#sampleBreastCancerTemp_area").val()!=$("#breastCancerTemp_area").val()){
		$("#area").css({"background-color":"red","color":"white"});
	}else{
		$("#areaNew").val($("#sampleBreastCancerTemp_area").val());
	}
	
	if($("#sampleBreastCancerTemp_hospital").val()!=$("#breastCancerTemp_hospital").val()){
		$("#hospital").css({"background-color":"red","color":"white"});
	}else{
		$("#hospitalNew").val($("#sampleBreastCancerTemp_hospital").val());
	}
	
	if($("#sampleBreastCancerTemp_serialNum").val()!=$("#breastCancerTemp_serialNum").val()){
		$("#serialNum").css({"background-color":"red","color":"white"});
	}else{
		$("#serialNumNew").val($("#sampleBreastCancerTemp_serialNum").val());
	}
	
	if($("#sampleBreastCancerTemp_relationNum").val()!=$("#breastCancerTemp_relationNum").val()){
		$("#relationNum").css({"background-color":"red","color":"white"});
	}else{
		$("#relationNumNew").val($("#sampleBreastCancerTemp_relationNum").val());
	}
	
	if($("#sampleBreastCancerTemp_relationship1").val()!=$("#breastCancerTemp_relationship1").val()){
		$("#breastCancerTempNew_relationship1").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_relationship1").val($("#sampleBreastCancerTemp_relationship1").val());
	}
	
	if($("#sampleBreastCancerTemp_sendDate").val()!=$("#breastCancerTemp_sendDate").val()){
		$("#sendDate").css({"background-color":"red","color":"white"});
	}else{
		$("#sendDateNew").val($("#sampleBreastCancerTemp_sendDate").val());
	}
	
	if($("#sampleBreastCancerTemp_reportDate").val()!=$("#breastCancerTemp_reportDate").val()){
		$("#reportDate").css({"background-color":"red","color":"white"});
	}else{
		$("#reportDateNew").val($("#sampleBreastCancerTemp_reportDate").val());
	}
	
	if($("#sampleBreastCancerTemp_patientName").val()!=$("#breastCancerTemp_patientName").val()){
		$("#patientName").css({"background-color":"red","color":"white"});
	}else{
		$("#patientNameNew").val($("#sampleBreastCancerTemp_patientName").val());
	}
	
	if($("#sampleBreastCancerTemp_gender").val()!=$("#breastCancerTemp_gender").val()){
		$("#breastCancerTempNew_gender").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_gender").val($("#sampleBreastCancerTemp_gender").val());
	}
	
	if($("#sampleBreastCancerTemp_maritalStatus").val()!=$("#breastCancerTemp_maritalStatus").val()){
		$("#breastCancerTempNew_maritalStatus").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_maritalStatus").val($("#sampleBreastCancerTemp_maritalStatus").val());
	}
	
	if($("#sampleBreastCancerTemp_age").val()!=$("#breastCancerTemp_age").val()){
		$("#age").css({"background-color":"red","color":"white"});
	}else{
		$("#ageNew").val($("#sampleBreastCancerTemp_age").val());
	}
	
	if($("#sampleBreastCancerTemp_nativePlace").val()!=$("#breastCancerTemp_nativePlace").val()){
		$("#nativePlace").css({"background-color":"red","color":"white"});
	}else{
		$("#nativePlaceNew").val($("#sampleBreastCancerTemp_nativePlace").val());
	}
	
	if($("#sampleBreastCancerTemp_nationality").val()!=$("#breastCancerTemp_nationality").val()){
		$("#nationality").css({"background-color":"red","color":"white"});
	}else{
		$("#nationalityNew").val($("#sampleBreastCancerTemp_nationality").val());
	}
	
	if($("#sampleBreastCancerTemp_heights").val()!=$("#breastCancerTemp_heights").val()){
		$("#heights").css({"background-color":"red","color":"white"});
	}else{
		$("#heightsNew").val($("#sampleBreastCancerTemp_heights").val());
	}
	
	if($("#sampleBreastCancerTemp_weight").val()!=$("#breastCancerTemp_weight").val()){
		$("#weight").css({"background-color":"red","color":"white"});
	}else{
		$("#weightNew").val($("#sampleBreastCancerTemp_weight").val());
	}
	
	if($("#sampleBreastCancerTemp_isInsure").val()!=$("#breastCancerTemp_isInsure").val()){
		$("#breastCancerTempNew_isInsure").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_isInsure").val($("#sampleBreastCancerTemp_isInsure").val());
	}
	
	if($("#sampleBreastCancerTemp_phone").val()!=$("#breastCancerTemp_phone").val()){
		$("#phone").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNew").val($("#sampleBreastCancerTemp_phone").val());
	}
	
	if($("#sampleBreastCancerTemp_emailAddress").val()!=$("#breastCancerTemp_emailAddress").val()){
		$("#emailAddress").css({"background-color":"red","color":"white"});
	}else{
		$("#emailAddressNew").val($("#sampleBreastCancerTemp_emailAddress").val());
	}
	
	if($("#sampleBreastCancerTemp_address").val()!=$("#breastCancerTemp_address").val()){
		$("#address").css({"background-color":"red","color":"white"});
	}else{
		$("#addressNew").val($("#sampleBreastCancerTemp_address").val());
	}
	
	if($("#sampleBreastCancerTemp_choiceType").val()!=$("#breastCancerTemp_choiceType").val()){
		$("#breastCancerTempNew_choiceType").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_choiceType").val($("#sampleBreastCancerTemp_choiceType").val());
	}
	
	if($("#sampleBreastCancerTemp_tumorHistory").val()!=$("#breastCancerTemp_tumorHistory").val()){
		$("#breastCancerTempNew_tumorHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_tumorHistory").val($("#sampleBreastCancerTemp_tumorHistory").val());
	}
	
	if($("#sampleBreastCancerTemp_ovaryTumorType").val()!=$("#breastCancerTemp_ovaryTumorType").val()){
		$("#breastCancerTempNew_ovaryTumorType").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_ovaryTumorType").val($("#sampleBreastCancerTemp_ovaryTumorType").val());
	}
	
	if($("#sampleBreastCancerTemp_confirmAge").val()!=$("#breastCancerTemp_confirmAge").val()){
		$("#confirmAge").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeNew").val($("#sampleBreastCancerTemp_confirmAge").val());
	}
	
	if($("#sampleBreastCancerTemp_otherTumorHistory").val()!=$("#breastCancerTemp_otherTumorHistory").val()){
		$("#breastCancerTempNew_otherTumorHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_otherTumorHistory").val($("#sampleBreastCancerTemp_otherTumorHistory").val());
	}
	
	if($("#sampleBreastCancerTemp_tumorType").val()!=$("#breastCancerTemp_tumorType").val()){
		$("#breastCancerTempNew_tumorType").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_tumorType").val($("#sampleBreastCancerTemp_tumorType").val());
	}
	
	if($("#sampleBreastCancerTemp_confirmAgeOne").val()!=$("#breastCancerTemp_confirmAgeOne").val()){
		$("#confirmAgeOne").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeOneNew").val($("#sampleBreastCancerTemp_confirmAgeOne").val());
	}
	
	if($("#sampleBreastCancerTemp_isInvoice").val()!=$("#breastCancerTemp_isInvoice").val()){
		$("#breastCancerTempNew_isInvoice").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_isInvoice").val($("#sampleBreastCancerTemp_isInvoice").val());
	}
	
	if($("#sampleBreastCancerTemp_tumorTypeOne").val()!=$("#breastCancerTemp_tumorTypeOne").val()){
		$("#breastCancerTempNew_tumorTypeOne").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_tumorTypeOne").val($("#sampleBreastCancerTemp_tumorTypeOne").val());
	}
	
	if($("#sampleBreastCancerTemp_relationship2").val()!=$("#breastCancerTemp_relationship2").val()){
		$("#breastCancerTempNew_relationship2").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_relationship2").val($("#sampleBreastCancerTemp_relationship2").val());
	}
	
	if($("#sampleBreastCancerTemp_confirmAgeTwo").val()!=$("#breastCancerTemp_confirmAgeTwo").val()){
		$("#confirmAgeTwo").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeTwoNew").val($("#sampleBreastCancerTemp_confirmAgeTwo").val());
	}
	if($("#sampleBreastCancerTemp_confirmAgeThree").val()!=$("#breastCancerTemp_confirmAgeThree").val()){
		$("#confirmAgeThree").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeThreeNew").val($("#sampleBreastCancerTemp_confirmAgeThree").val());
	}
	
	if($("#sampleBreastCancerTemp_confirmAge").val()!=$("#breastCancerTemp_confirmAge").val()){
		$("#confirmAge").css({"background-color":"red","color":"white"});
	}else{
		$("#confirmAgeNew").val($("#sampleBreastCancerTemp_confirmAge").val());
	}
	
	if($("#sampleBreastCancerTemp_menstrualCycle").val()!=$("#breastCancerTemp_menstrualCycle").val()){
		$("#menstrualCycle").css({"background-color":"red","color":"white"});
	}else{
		$("#menstrualCycleNew").val($("#sampleBreastCancerTemp_menstrualCycle").val());
	}
	
	if($("#sampleBreastCancerTemp_firstGestationAge").val()!=$("#breastCancerTemp_firstGestationAge").val()){
		$("#firstGestationAge").css({"background-color":"red","color":"white"});
	}else{
		$("#firstGestationAgeNew").val($("#sampleBreastCancerTemp_firstGestationAge").val());
	}
	
	if($("#sampleBreastCancerTemp_pregnancyTime").val()!=$("#breastCancerTemp_pregnancyTime").val()){
		$("#pregnancyTime").css({"background-color":"red","color":"white"});
	}else{
		$("#pregnancyTimeNew").val($("#sampleBreastCancerTemp_pregnancyTime").val());
	}
	
	if($("#sampleBreastCancerTemp_parturitionTime").val()!=$("#breastCancerTemp_parturitionTime").val()){
		$("#parturitionTime").css({"background-color":"red","color":"white"});
	}else{
		$("#parturitionTimeNew").val($("#sampleBreastCancerTemp_parturitionTime").val());
	}
	
	if($("#sampleBreastCancerTemp_otherBreastHistory").val()!=$("#breastCancerTemp_otherBreastHistory").val()){
		$("#otherBreastHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#otherBreastHistoryNew").val($("#sampleBreastCancerTemp_otherBreastHistory").val());
	}
	
	if($("#sampleBreastCancerTemp_otherBreastHistory").val()!=$("#breastCancerTemp_otherBreastHistory").val()){
		$("#breastCancerTempNew_otherBreastHistory").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_otherBreastHistory").val($("#sampleBreastCancerTemp_otherBreastHistory").val());
	}
	
	if($("#sampleBreastCancerTemp_gestationalAge").val()!=$("#breastCancerTemp_gestationalAge").val()){
		$("#gestationalAge").css({"background-color":"red","color":"white"});
	}else{
		$("#gestationalAgeNew").val($("#sampleBreastCancerTemp_gestationalAge").val());
	}
	
	if($("#sampleBreastCancerTemp_gestationIVF").val()!=$("#breastCancerTemp_gestationIVF").val()){
		$("#breastCancerTempNew_gestationIVF").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_gestationIVF").val($("#sampleBreastCancerTemp_gestationIVF").val());
	}
	
	if($("#sampleBreastCancerTemp_menopauseAge").val()!=$("#breastCancerTemp_menopauseAge").val()){
		$("#menopauseAge").css({"background-color":"red","color":"white"});
	}else{
		$("#menopauseAgeNew").val($("#sampleBreastCancerTemp_menopauseAge").val());
	}
	
	if($("#sampleBreastCancerTemp_trisome21Value").val()!=$("#breastCancerTemp_trisome21Value").val()){
		$("#breastCancerTempNew_trisome21Value").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_trisome21Value").val($("#sampleBreastCancerTemp_trisome21Value").val());
	}
	
	if($("#sampleBreastCancerTemp_acceptNum").val()!=$("#breastCancerTemp_acceptNum").val()){
		$("#acceptNum").css({"background-color":"red","color":"white"});
	}else{
		$("#acceptNumNew").val($("#sampleBreastCancerTemp_acceptNum").val());
	}
	
	if($("#sampleBreastCancerTemp_serialNum").val()!=$("#breastCancerTemp_serialNum").val()){
		$("#serialNum").css({"background-color":"red","color":"white"});
	}else{
		$("#serialNumNew").val($("#sampleBreastCancerTemp_serialNum").val());
	}
	
	if($("#sampleBreastCancerTemp_cigarette").val()!=$("#breastCancerTemp_cigarette").val()){
		$("#cigarette").css({"background-color":"red","color":"white"});
	}else{
		$("#cigaretteNew").val($("#sampleBreastCancerTemp_cigarette").val());
	}
	
	if($("#sampleBreastCancerTemp_wine").val()!=$("#breastCancerTemp_wine").val()){
		$("#wine").css({"background-color":"red","color":"white"});
	}else{
		$("#wineNew").val($("#sampleBreastCancerTemp_wine").val());
	}
	
	if($("#sampleBreastCancerTemp_medicine").val()!=$("#breastCancerTemp_medicine").val()){
		$("#medicine").css({"background-color":"red","color":"white"});
	}else{
		$("#medicineNew").val($("#sampleBreastCancerTemp_medicine").val());
	}
	
	if($("#sampleBreastCancerTemp_medicineType").val()!=$("#breastCancerTemp_medicineType").val()){
		$("#medicineType").css({"background-color":"red","color":"white"});
	}else{
		$("#medicineTypeNew").val($("#sampleBreastCancerTemp_medicineType").val());
	}
	
	if($("#sampleBreastCancerTemp_radioactiveRays").val()!=$("#breastCancerTemp_radioactiveRays").val()){
		$("#radioactiveRays").css({"background-color":"red","color":"white"});
	}else{
		$("#radioactiveRaysNew").val($("#sampleBreastCancerTemp_radioactiveRays").val());
	}
	
	if($("#sampleBreastCancerTemp_pesticide").val()!=$("#breastCancerTemp_pesticide").val()){
		$("#pesticide").css({"background-color":"red","color":"white"});
	}else{
		$("#pesticideveRaysNew").val($("#sampleBreastCancerTemp_pesticide").val());
	}
	
	if($("#sampleBreastCancerTemp_plumbane").val()!=$("#breastCancerTemp_plumbane").val()){
		$("#plumbane").css({"background-color":"red","color":"white"});
	}else{
		$("#plumbaneNew").val($("#sampleBreastCancerTemp_plumbane").val());
	}
	
	if($("#sampleBreastCancerTemp_mercury").val()!=$("#breastCancerTemp_mercury").val()){
		$("#mercury").css({"background-color":"red","color":"white"});
	}else{
		$("#mercuryNew").val($("#sampleBreastCancerTemp_mercury").val());
	}
	
	if($("#sampleBreastCancerTemp_cadmium").val()!=$("#breastCancerTemp_cadmium").val()){
		$("#cadmium").css({"background-color":"red","color":"white"});
	}else{
		$("#cadmiumNew").val($("#sampleBreastCancerTemp_cadmium").val());
	}
	
	if($("#sampleBreastCancerTemp_otherJunk").val()!=$("#breastCancerTemp_otherJunk").val()){
		$("#otherJunk").css({"background-color":"red","color":"white"});
	}else{
		$("#otherJunkNew").val($("#sampleBreastCancerTemp_otherJunk").val());
	}
	
	if($("#sampleBreastCancerTemp_breastExa").val()!=$("#breastCancerTemp_breastExa").val()){
		$("#breastExa").css({"background-color":"red","color":"white"});
	}else{
		$("#breastExaNew").val($("#sampleBreastCancerTemp_breastExa").val());
	}
	
	if($("#sampleBreastCancerTemp_pathologyType").val()!=$("#breastCancerTemp_pathologyType").val()){
		$("#pathologyType").css({"background-color":"red","color":"white"});
	}else{
		$("#pathologyTypeNew").val($("#sampleBreastCancerTemp_pathologyType").val());
	}
	
	if($("#sampleBreastCancerTemp_t").val()!=$("#breastCancerTemp_t").val()){
		$("#t").css({"background-color":"red","color":"white"});
	}else{
		$("#tNew").val($("#sampleBreastCancerTemp_t").val());
	}
	
	if($("#sampleBreastCancerTemp_n").val()!=$("#breastCancerTemp_n").val()){
		$("#n").css({"background-color":"red","color":"white"});
	}else{
		$("#nNew").val($("#sampleBreastCancerTemp_n").val());
	}
	
	if($("#sampleBreastCancerTemp_m").val()!=$("#breastCancerTemp_m").val()){
		$("#m").css({"background-color":"red","color":"white"});
	}else{
		$("#mNew").val($("#sampleBreastCancerTemp_m").val());
	}
	
	if($("#sampleBreastCancerTemp_staging").val()!=$("#breastCancerTemp_staging").val()){
		$("#staging").css({"background-color":"red","color":"white"});
	}else{
		$("#stagingNew").val($("#sampleBreastCancerTemp_staging").val());
	}
	
	if($("#sampleBreastCancerTemp_er").val()!=$("#breastCancerTemp_er").val()){
		$("#er").css({"background-color":"red","color":"white"});
	}else{
		$("#erNew").val($("#sampleBreastCancerTemp_er").val());
	}
	
	if($("#sampleBreastCancerTemp_pr").val()!=$("#breastCancerTemp_pr").val()){
		$("#pr").css({"background-color":"red","color":"white"});
	}else{
		$("#prNew").val($("#sampleBreastCancerTemp_pr").val());
	}
	
	if($("#sampleBreastCancerTemp_Her2").val()!=$("#breastCancerTemp_Her2").val()){
		$("#Her2").css({"background-color":"red","color":"white"});
	}else{
		$("#Her2New").val($("#sampleBreastCancerTemp_Her2").val());
	}
	
	if($("#sampleBreastCancerTemp_fish").val()!=$("#breastCancerTemp_fish").val()){
		$("#fish").css({"background-color":"red","color":"white"});
	}else{
		$("#fishNew").val($("#sampleBreastCancerTemp_fish").val());
	}
	
	if($("#sampleBreastCancerTemp_ki").val()!=$("#breastCancerTemp_ki").val()){
		$("#ki").css({"background-color":"red","color":"white"});
	}else{
		$("#kiNew").val($("#sampleBreastCancerTemp_ki").val());
	}
	
	if($("#sampleBreastCancerTemp_p53").val()!=$("#breastCancerTemp_p53").val()){
		$("#p53").css({"background-color":"red","color":"white"});
	}else{
		$("#p53New").val($("#sampleBreastCancerTemp_p53").val());
	}
	
	if($("#sampleBreastCancerTemp_antigen1").val()!=$("#breastCancerTemp_antigen1").val()){
		$("#antigen1").css({"background-color":"red","color":"white"});
	}else{
		$("#antigen1New").val($("#sampleBreastCancerTemp_antigen1").val());
	}
	
	if($("#sampleBreastCancerTemp_antigen2").val()!=$("#breastCancerTemp_antigen2").val()){
		$("#antigen2").css({"background-color":"red","color":"white"});
	}else{
		$("#antigen2New").val($("#sampleBreastCancerTemp_antigen2").val());
	}
	
	if($("#sampleBreastCancerTemp_antigen3").val()!=$("#breastCancerTemp_antigen3").val()){
		$("#antigen3").css({"background-color":"red","color":"white"});
	}else{
		$("#antigen3New").val($("#sampleBreastCancerTemp_antigen3").val());
	}
	
	if($("#sampleBreastCancerTemp_roentgenFindings1").val()!=$("#breastCancerTemp_roentgenFindings1").val()){
		$("#roentgenFindings1").css({"background-color":"red","color":"white"});
	}else{
		$("#roentgenFindings1New").val($("#sampleBreastCancerTemp_roentgenFindings1").val());
	}
	
	if($("#sampleBreastCancerTemp_rate1").val()!=$("#breastCancerTemp_rate1").val()){
		$("#rate1").css({"background-color":"red","color":"white"});
	}else{
		$("#rate1New").val($("#sampleBreastCancerTemp_rate1").val());
	}
	
	if($("#sampleBreastCancerTemp_roentgenFindings2").val()!=$("#breastCancerTemp_roentgenFindings2").val()){
		$("#roentgenFindings2").css({"background-color":"red","color":"white"});
	}else{
		$("#roentgenFindings2New").val($("#sampleBreastCancerTemp_roentgenFindings2").val());
	}
	
	if($("#sampleBreastCancerTemp_rate2").val()!=$("#breastCancerTemp_rate2").val()){
		$("#rate2").css({"background-color":"red","color":"white"});
	}else{
		$("#rate2New").val($("#sampleBreastCancerTemp_rate2").val());
	}
	
	if($("#sampleBreastCancerTemp_roentgenFindings3").val()!=$("#breastCancerTemp_roentgenFindings3").val()){
		$("#roentgenFindings3").css({"background-color":"red","color":"white"});
	}else{
		$("#roentgenFindings3New").val($("#sampleBreastCancerTemp_roentgenFindings3").val());
	}
	
	if($("#sampleBreastCancerTemp_rate3").val()!=$("#breastCancerTemp_rate3").val()){
		$("#rate3").css({"background-color":"red","color":"white"});
	}else{
		$("#rate3New").val($("#sampleBreastCancerTemp_rate3").val());
	}
	
	if($("#sampleBreastCancerTemp_signed").val()!=$("#breastCancerTemp_signed").val()){
		$("#signed").css({"background-color":"red","color":"white"});
	}else{
		$("#signedNew").val($("#sampleBreastCancerTemp_signed").val());
	}
	
	if($("#sampleBreastCancerTemp_phoneNum2").val()!=$("#breastCancerTemp_phoneNum2").val()){
		$("#phoneNum2").css({"background-color":"red","color":"white"});
	}else{
		$("#phoneNum2New").val($("#sampleBreastCancerTemp_phoneNum2").val());
	}
	
	if($("#sampleBreastCancerTemp_isFee").val()!=$("#breastCancerTemp_isFee").val()){
		$("#breastCancerTempNew_isFee").css({"background-color":"red","color":"white"});
	}else{
		$("#breastCancerTempNew_isFee").val($("#sampleBreastCancerTemp_isFee").val());
	}
	
	if($("#sampleBreastCancerTemp_reportMan_name").val()!=$("#breastCancerTemp_reportMan_name").val()){
		$("#reportMan").css({"background-color":"red","color":"white"});
		$("#reportManIdNew").val($("#sampleBreastCancerTemp_reportMan_id").val());
	}else{
		$("#reportManIdNew").val($("#sampleBreastCancerTemp_reportMan_id").val());
		$("#reportManNew").val($("#sampleBreastCancerTemp_reportMan_name").val());
	}
	
	if($("#sampleBreastCancerTemp_auditMan_name").val()!=$("#breastCancerTemp_auditMan_name").val()){
		$("#auditMan").css({"background-color":"red","color":"white"});
		$("#auditManIdNew").val($("#sampleBreastCancerTemp_auditMan_id").val());
	}else{
		$("#auditManIdNew").val($("#sampleBreastCancerTemp_auditMan_id").val());
		$("#auditManNew").val($("#sampleBreastCancerTemp_auditMann_name").val());
	}
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInputTemp/editSampleInput.action";
}

load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInputTemp/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {	

	var nextStepFlow =$("#breastCancerTempNew_nextStepFlow").val();
	if(nextStepFlow==""){
		message(biolims.common.pleaseSelectNextFlow);
		return;
	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleBreastCancerTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleBreastCancerTemp_id").val(),
					title : $("#sampleBreastCancerTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleBreastCancerTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInputTemp/save.action?saveType="+$("#saveType").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInputTemp/copySampleInput.action?id=' + $("#sampleBreastCancerTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleBreastCancerTemp_id").val()){
		commonChangeState("formId=" + $("#sampleBreastCancerTemp_id").val() + "&tableId=SampleBreastCancerTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleInfo_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.breastCancerAudit,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 800,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleBreastCancerTemp_sampleType_id").value = id;
		 document.getElementById("sampleBreastCancerTemp_sampleType_name").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:500,height:500,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("sampleBreastCancerTemp_voucherType").value = id;
			 document.getElementById("sampleBreastCancerTemp_voucherType_name").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleBreastCancerTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleBreastCancerTemp_voucherCode").val())){
				var id = $("#sampleBreastCancerTemp_voucherCode").val();
				ajax("post", "/sample/sampleInputTemp/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleBreastCancerTemp_phoneNum").val())){
				//return;
				var id = $("#sampleBreastCancerTemp_phoneNum").val();
				ajax("post", "/sample/sampleInputTemp/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

	

	
		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 500,
						height : 500,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("productIdNew").value = id;
							document.getElementById("productNameNew").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}
		
		
		
//检测项目的Id给到	
function a(){
	var productName = $("#productName").val();
	var id1 = $("#sampleBreastCancerTemp_productId").val();
	var name1= $("#sampleBreastCancerTemp_productName").val();
	var id2= $("#breastCancerTemp_productId").val();
	var name2 = $("#breastCancerTemp_productName").val();
	if(productName==name1){
		$("#productIdNew").val(id1);
	}else if(productName==name2){
		$("#productIdNew").val(id2);
	}else if(productName==""){
		$("#productIdNew").val("");
	}
}

//样本类型
function b(){
	var sampleType = $("#sampleType").val();
	var id1 = $("#sampleBreastCancerTemp_sampleType_id").val();
	var name1 = $("#sampleBreastCancerTemp_sampleType_name").val();
	var id2 = $("#breastCancerTemp_sampleType_id").val();
	var name2 = $("#breastCancerTemp_sampleType_name").val();
	if(sampleType==name1){
		$("#sampleTypeIdNew").val(id1);
	}else if(sampleType==name2){
		$("#sampleTypeIdNew").val(id2);
	}else if(sampleType==""){
		$("#sampleTypeIdNew").val("");
	}
}
//证件号码
function c(){
	var voucherType = $("#voucherType").val();
	var id1 =$("#sampleBreastCancerTemp_voucherType_id").val();
	var name1 = $("#sampleBreastCancerTemp_voucherType_name").val();
	var id2 = $("#breastCancerTemp_voucherType_id").val();
	var name2 =$("#breastCancerTemp_voucherType_name").val();
	if(voucherType==name1){
		$("#voucherTypeIdNew").val(id1);
	}else if(voucherType==name2){
		$("#voucherTypeIdNew").val(id2);
	}else if(voucherType==""){
		$("#voucherTypeIdNew").val("");
	}
}