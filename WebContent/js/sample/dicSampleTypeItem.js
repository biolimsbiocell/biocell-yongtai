﻿var dicSampleTypeItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'dicSampleType-id',
			type:"string"
		});
	   fields.push({
			name:'dicSampleType-name',
			type:"string"
		});
	   fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
		    fields.push({
				name:'dnextId',
				type:"string"
			});
		   fields.push({
				name:'dnextName',
				type:"string"
			});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:150
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:50*10
	});
	cm.push({
		dataIndex:'dnextId',
		hidden : true,
		header:biolims.sample.dnextId,
		width:150
	});
	cm.push({
		dataIndex:'dnextName',
		hidden : false,
		header:biolims.sample.dnextName,
		width:20*6
	});
	
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/dicSampleType/showDicSampleTypeItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.openBoxStep;
	opts.height =  document.body.clientHeight-160;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/sample/dicSampleType/delDicSampleTypeItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dicSampleTypeItemGrid.getStore().commitChanges();
				dicSampleTypeItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	opts.tbar.push({
			text : biolims.common.selectProduct,
			handler :ProductFun
	});
	opts.tbar.push({
		text : biolims.sample.selectNextStep,
		handler : selectdnext
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	dicSampleTypeItemGrid=gridEditTable("dicSampleTypeItemdiv",cols,loadParam,opts);
	$("#dicSampleTypeItemdiv").data("dicSampleTypeItemGrid", dicSampleTypeItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function ProductFun() {
	var win = Ext.getCmp('productFun');
	if (win) {
		win.close();
	}
	var productFun = new Ext.Window(
			{
				id : 'productFun',
				modal : true,
				title : biolims.common.selectProduct,
				layout : 'fit',
				width : 600,
				height : 600,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						productFun.close();
					}
				} ]
			});
	productFun.show();
}

function setProductFun(id, name) {
	var selectRecord = dicSampleTypeItemGrid.getSelectionModel().getSelections();
	for(var i=0;i<selectRecord.length;i++){
		selectRecord[i].set("productId",id);
		selectRecord[i].set("productName",name);
		
	}
//	var productName = "";
//	ajax(
//			"post",
//			"/com/biolims/system/product/findProductToSample.action",
//			{
//				code : id,
//			},
//			function(data) {
//
//				if (data.success) {
//					$.each(data.data, function(i, obj) {
//						productName += obj.name + ",";
//					});
//				}
//			}, null);
	var win = Ext.getCmp('productFun');
	if (win) {
		win.close();
	}
}
	
	
	//选择默认下一步流向
	function selectdnext() {
		var title = '';
		var url = '';
		title = biolims.sample.selectNextStep;
		url = "/system/nextFlow/nextFlow/showDialogWorkOrderNextFlowList.action";
		var option = {};
		option.width = 650;
		option.height = 560;
		loadDialogPage(null, title, url, {
			"Confirm" : function() {
				selVal2(this);
				}
			}, true, option);
	}
	var selVal2 = function(win) {
		var operGrid = $("#show_dialog_nextflow_grid_div").data("showNextFlowGrid");
		var selRecords = operGrid.getSelectionModel().getSelections();
		if(selRecords.length==1){
			var id="";
			var name="";
			$.each(selRecords, function(i, obj) {
				id = obj.get("id");
				name = obj.get("name");
			});
			
			var selectRecord = dicSampleTypeItemGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						
						obj.set('dnextId',id);
						obj.set('dnextName',name);
						
					});
					$(win).dialog("close");
				} else {
					message(biolims.common.selectYouWant);
					return;
				}
		}else{
			message(biolims.common.selectRecord);
		}
	
	};
