var showAbnormalTypeGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'parent',
		type:"String"
	});
	    fields.push({
		name:'method',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'name',
		header:biolims.sample.unusualName,
		width:50*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'parent',
		header:biolims.master.parentName,
		hidden : true,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'method',
		header:biolims.common.operate,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	/*var model=$("#model").val();
	var sequencingPlatform = $("#sequencingPlatform").val();
	var productId=$("#productId").val();
	var sampleTypeId=$("#sampleTypeId").val();*/
	var loadParam={};
	
	loadParam.url=ctx+"/sample/sampleAbnormal/showAbnormalTypeAllJson.action";
	
	var opts={};
	opts.title=biolims.common.unusualName;
	
	opts.height=400;
	opts.width=450;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	var n=$("#n").val();
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setNextFlowFun(rec);
		if(n==2){
			setNextFlow2();
		}else if(n==3){
			setNextFlow3();
		}else if(n==4){
			setNextFlow4();
		}else{
			setNextFlow();
		}
		
	};
	showAbnormalTypeGrid=gridTable("show_abnormalType_div1",cols,loadParam,opts);
	$("#show_abnormalType_div1").data("showAbnormalTypeGrid", showAbnormalTypeGrid);
	
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "biolims.common.search", null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(shownextFlowDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
