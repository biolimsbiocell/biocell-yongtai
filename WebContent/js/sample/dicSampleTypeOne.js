$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.sampleId,
	});
	colOpts.push({
		"data": "name",
		"title": "样本名称",
	});
	colOpts.push({
		"data": "code",
		"title": biolims.sample.code,
	});
	var tbarOpts = [];
	var addSampleTypeOptions = table(false, null,
			'/sample/dicSampleType/showDialogDicSampleTypeTableJson.action',colOpts , tbarOpts)
		var sampleTypeTable = renderData($("#addDicSampleType"), addSampleTypeOptions);
	$("#addDicSampleType").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addDicSampleType_wrapper .dt-buttons").empty();
		$('#addDicSampleType_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addDicSampleType tbody tr");
	sampleTypeTable.ajax.reload();
	sampleTypeTable.on('draw', function() {
		trs = $("#addDicSampleType tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

