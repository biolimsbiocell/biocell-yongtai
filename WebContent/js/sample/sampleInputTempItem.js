﻿var sampleInputTempItemGrid;

$(function(){
	var cols={};
	cols.sm = true;
	var fields=[];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name:'handlingSuggestion',
		type:"string"
	});
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'patientNameSpell',
		type : "string"
	});
	fields.push({
		name : 'age',
		type : "string"
	});
	fields.push({
		name : 'acceptDate',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'area',
		type : "string"
	});
	fields.push({
		name : 'hospital',
		type : "string"
	});
	fields.push({
		name : 'sendDate',
		type : "string"
	});
	fields.push({
		name : 'doctor',
		type : "string"
	});
	fields.push({
		name : 'inHosNum',
		type : "string"
	});
	fields.push({
		name : 'sampleType-id',
		type : "string"
	});
	fields.push({
		name : 'sampleType-name',
		type : "string"
	});
	fields.push({
		name : 'weight',
		type : "string"
	});
	fields.push({
		name : 'gestationalAge',
		type : "string"
	});
	fields.push({
		name : 'voucherType-id',
		type : "string"
	});

	fields.push({
		name : 'voucherType-name',
		type : "string"
	});
	fields.push({
		name : 'voucherCode',
		type : "string"
	});
	fields.push({
		name : 'phoneNum',
		type : "string"
	});
	fields.push({
		name : 'address',
		type : "string"
	});
	fields.push({
		name : 'endMenstruationDate',
		type : "string"
	});
	fields.push({
		name : 'gestationIVF',
		type : "string"
	});
	// 如果报错String改成Integer
	fields.push({
		name : 'pregnancyTime',
		type : "string"
	});
	fields.push({
		name : 'parturitionTime',
		type : "string"
	});
	fields.push({
		name : 'badMotherhood',
		type : "string"
	});
	fields.push({
		name : 'organGrafting',
		type : "string"
	});
	fields.push({
		name : 'outTransfusion',
		type : "string"
	});
	fields.push({
		name : 'firstTransfusionDate',
		type : "string"
	});
	fields.push({
		name : 'stemCellsCure',
		type : "string"
	});
	fields.push({
		name : 'immuneCure',
		type : "string"
	});
	fields.push({
		name : 'endImmuneCureDate',
		type : "string"
	});
	fields.push({
		name : 'embryoType',
		type : "string"
	});
	fields.push({
		name : 'NT',
		type : "string"
	});
	fields.push({
		name : 'reason',
		type : "string"
	});
	fields.push({
		name : 'testPattern',
		type : "string"
	});
	fields.push({
		name:'nextStepFlow',
		type:"string"
	});
	fields.push({
		name:'confirmCarry',
		type:"string"
	});
	// 如果报错就string改成double
	fields.push({
		name : 'trisome21Value',
		type : "string"
	});
	fields.push({
		name : 'trisome18Value',
		type : "string"
	});
	fields.push({
		name : 'trisome13Value',
		type : "string"
	});
	fields.push({
		name : 'coupleChromosome',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	fields.push({
		name : 'reason2',
		type : "string"
	});
	fields.push({
		name : 'diagnosis',
		type : "string"
	});
	fields.push({
		name : 'medicalHistory',
		type : "string"
	});
	fields.push({
		name : 'isInsure',
		type : "string"
	});
	fields.push({
		name : 'isFee',
		type : "string"
	});
	fields.push({
		name : 'privilegeType',
		type : "string"
	});
	fields.push({
		name : 'linkman-id',
		type : "string"
	});
	fields.push({
		name : 'linkman-name',
		type : "string"
	});
	fields.push({
		name : 'isInvoice',
		type : "string"
	});
	fields.push({
		name : 'paymentUnit',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'money',
		type : "string"
	});
	fields.push({
		name : 'receiptType-id',
		type : "string"
	});
	fields.push({
		name : 'receiptType-name',
		type : "string"
	});
	fields.push({
		name : 'gender',
		type : "string"
	});
	fields.push({
		name : 'birthday',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'reference21Range',
		type : "string"
	});
	fields.push({
		name : 'suppleAgreement2',
		type : "string"
	});
	fields.push({
		name : 'sampleInfo-id',
		type : "string"
	});
	fields.push({
		name : 'sampleInfo-name',
		type : "string"
	});
	fields.push({
		name : 'reportMan',
		type : "string"
	});
	fields.push({
		name : 'auditMan',
		type : "string"
	});
	fields.push({
		name : 'reference18Range',
		type : "string"
	});
	fields.push({
		name : 'reference13Range',
		type : "string"
	});
	fields.push({
		name : 'sid',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});

	cm.push({
		dataIndex : 'code',
		header : biolims.common.code,
		width : 20 * 6,

		sortable : true,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});

	cm.push({
		dataIndex : 'name',
		header : biolims.common.name,
		width : 50 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex : 'patientName',
		header : '孕妇姓名',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'patientNameSpell',
		header : '姓名拼音',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'age',
		header : '年龄',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'acceptDate',
		header : biolims.common.acceptDate,
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'reportDate',
		header : biolims.common.reportDate,
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'area',
		header : '地区',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'hospital',
		header : '送检医院',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sendDate',
		header : '送检日期',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'doctor',
		header : '送检医生',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'inHosNum',
		header : '住院号',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleType-id',
		header : '样本类型ID',
		width : 20 * 6,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleType-Name',
		header : biolims.common.sampleType,
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'weight',
		header : '体重',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'gestationalAge',
		header : '孕周',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'voucherType-id',
		header : '证件类型ID',
		width : 20 * 6,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'voucherType-Name',
		header : '证件类型',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'voucherCode',
		header : '证件号码',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'phoneNum',
		header : biolims.common.phone,
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'address',
		header : '家庭住址',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'endMenstruationDate',
		header : '末次月经',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'gestationIVF',
		header : 'IVF妊娠',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'pregnancyTime',
		header : '孕几次',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'parturitionTime',
		header : '产几次',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'badMotherhood',
		header : '不良孕产史',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'organGrafting',
		header : '器官移植',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'outTransfusion',
		header : '外源输血',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'firstTransfusionDate',
		header : '最后一次外源输血时间',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'stemCellsCure',
		header : '干细胞治疗',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'immuneCure',
		header : '免疫治疗',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'endImmuneCureDate',
		header : '最后一次免疫治疗时间',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'embryoType',
		header : '单/双/多胎',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'NT',
		header : 'NT值',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'reason',
		header : '异常结果描述',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'testPattern',
		header : '筛查模式',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'trisome21Value',
		header : '21-三体比值',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'trisome18Value',
		header : '18-三体比值',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'trisome13Value',
		header : '13-三体比值',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'coupleChromosome',
		header : '夫妻双方染色体',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleNum',
		header : biolims.common.code,
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'reason2',
		header : '异常结果描述2',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'diagnosis',
		header : '临床诊断',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'medicalHistory',
		header : '简要病史',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'isInsure',
		header : '是否购买保险',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'isFee',
		header : '是否收费',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'privilegeType',
		header : '优惠类型',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'linkman-id',
		header : '推荐人ID',
		width : 20 * 6,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'linkman-name',
		header : '推荐人',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'isInvoice',
		header : '是否需要发票',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'paymentUnit',
		header : '开票单位',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'createUser-id',
		header : '录入人ID',
		width : 50 * 6,
		hidden: true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : '录入人',
		width : 50 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'money',
		header : '金额',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'receiptType-id',
		header : '收据类型ID',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'receiptType-name',
		header : '收据类型',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	
	var storeisGender = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '男' ], [ '1', '女' ] ]
	});
	var selectGender = new Ext.form.ComboBox({
		store : storeisGender,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'gender',
		hidden:false,
		header : '性别',
		width : 20 * 6,
//		sortable : true,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
		editor : selectGender,
		renderer : Ext.util.Format.comboRenderer(selectGender)
	});
	cm.push({
		dataIndex : 'birthday',
		header : '出生日期',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		header : '状态id',
		width : 20 * 6,
		hidden : true,
		sortable : true,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex : 'stateName',
		header : biolims.common.workFlowState,
		width : 20 * 6,
		sortable : true,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex : 'phone',
		header : '联系方式',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'reference21Range',
		header : '21-三体（参考范围）',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'suppleAgreement2',
		header : '补充协议',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleInfo-id',
		header : '相关主表',
		width : 20 * 6,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sampleInfo-name',
		header : '相关主表',
		width : 20 * 6,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'reportMan',
		header : '报告者',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'auditMan',
		header : '审核者',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'reference18Range',
		header : '18-三体（参考范围）',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'reference13Range',
		header : '13-三体（参考范围）',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'sid',
		header : '取样打包明细Id',
		width : 20 * 6,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	cm.push({
		dataIndex:'handlingSuggestion',
		hidden : false,
		header:biolims.common.method,
		width:50*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storenextStepFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.pass ], [ '1', '反馈到项目管理' ],['2','不通过'] ]
	});
	var nextStepFlowCob = new Ext.form.ComboBox({
		store : storenextStepFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextStepFlow',
		hidden : false,
		header:biolims.common.nextFlow,
		width:10*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
		editor : nextStepFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextStepFlowCob)
	});
	var storeconfirmCarryCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes ] ]
	});
	var confirmCarryCob = new Ext.form.ComboBox({
		store : storeconfirmCarryCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'confirmCarry',
		hidden : false,
		header:biolims.common.confirm2Execute,
		width:10*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
		editor : confirmCarryCob,
		renderer : Ext.util.Format.comboRenderer(confirmCarryCob)
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleInputTemp/showSampleInputTempItemListJson.action?code="+ $("#code").val();
	var opts={};
	opts.title="信息录入对比数据";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/sample/sampleInputTemp/delSampleInputTempItem.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				sampleInputTempItemGrid.getStore().commitChanges();
//				sampleInputTempItemGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	
	
	
	

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = sampleInputTempItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
                			sampleInputTempItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	
	
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	
	sampleInputTempItemGrid=gridEditTable("sampleInputTempItemdiv",cols,loadParam,opts);
	$("#sampleInputTempItemdiv").data("sampleInputTempItemGrid", sampleInputTempItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

