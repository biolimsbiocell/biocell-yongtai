var dicSampleTypeTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.dicSampleType.id,
	});
	    fields.push({
			"data":"name",
			"title":biolims.dicSampleType.name,
		});
	    fields.push({
		"data":"stateName",
		"title":biolims.dicSampleType.stateName,
	});
	
	    fields.push({
		"data":"nextFlowId",
		"title":biolims.dicSampleType.nextFlowId,
	});
	
	    fields.push({
		"data":"nextFlow",
		"title":biolims.dicSampleType.nextFlow,
	});
	
	    fields.push({
		"data":"type-id",
		"title":biolims.common.typeID
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.dicSampleType.type
	});
	
	
	    fields.push({
		"data":"note",
		"title":biolims.dicSampleType.note,
	});
	
	    fields.push({
		"data":"code",
		"title":biolims.dicSampleType.code,
	});
	
	    fields.push({
		"data":"orderNumber",
		"title":biolims.dicSampleType.ordeRnumber,
	});
	
	    fields.push({
		"data":"first",
		"title":biolims.dicSampleType.first,
	});
	
	    fields.push({
		"data":"picCode",
		"title":biolims.dicSampleType.picCode,
	});
	
	    fields.push({
		"data":"picCodeName",
		"title":biolims.dicSampleType.picCodeName,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.dicSampleType.state+"ID"
	});
	    fields.push({
		"data":"stateName",
		"title":biolims.dicSampleType.state
	});
	
	var options = table(true, "","/sample/dicSampleType/showDicSampleTypeTableJson.action",
	 fields, null)
	dicSampleTypeTable = renderData($("#addDicSampleTypeTable"), options);
	$('#addDicSampleTypeTable').on('init.dt', function() {
		recoverSearchContent(dicSampleTypeTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.dicSampleType.id
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.dicSampleType.stateName
		});
	   fields.push({
		    "searchName":"nextFlowId",
			"type":"input",
			"txt":biolims.dicSampleType.nextFlowId
		});
	   fields.push({
		    "searchName":"nextFlow",
			"type":"input",
			"txt":biolims.dicSampleType.nextFlow
		});
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.common.typeID
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.dicSampleType.type
	});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.dicSampleType.name
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.dicSampleType.note
		});
	   fields.push({
		    "searchName":"code",
			"type":"input",
			"txt":biolims.dicSampleType.code
		});
	   fields.push({
		    "searchName":"orderNumber",
			"type":"input",
			"txt":biolims.dicSampleType.ordeRnumber
		});
	   fields.push({
		    "searchName":"first",
			"type":"input",
			"txt":biolims.dicSampleType.first
		});
	   fields.push({
		    "searchName":"picCode",
			"type":"input",
			"txt":biolims.dicSampleType.picCode
		});
	   fields.push({
		    "searchName":"picCodeName",
			"type":"input",
			"txt":biolims.dicSampleType.picCodeName
		});
	fields.push({
	    "type":"input",
		"searchName":"state.id",
		"txt":biolims.dicSampleType.state+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"state.name",
		"txt":biolims.dicSampleType.state
	});
	
	fields.push({
		"type":"table",
		"table":dicSampleTypeTable
	});
	return fields;
}
