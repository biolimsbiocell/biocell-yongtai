var sampleInputTechnologyGrid;
$(function(){
	var cols={};
	var fields=[];
    fields.push({
	name:'id',
	type:"string"
});
    fields.push({
	name:'code',
	type:"string"
});
    fields.push({
	name:'name',
	type:"string"
});
    fields.push({
	name:'patientName',
	type:"string"
});
    fields.push({
	name:'idCard',
	type:"string"
});
 fields.push({
	name:'billNumber',
	type:"string"
});
    fields.push({
	name:'sampleType-id',
	type:"string"
});
    fields.push({
		name:'sampleType-name',
		type:"string"
	});
    fields.push({
	name:'hospital',
	type:"string"
});
    fields.push({
	name:'price',
	type:"string"
});
fields.push({
	name:'upLoadAccessory-fileName',
	type:'string'
});
fields.push({
	name:'upLoadAccessory-id',
	type:'string'
});
//录入人
fields.push({
	name:'createUser1',
	type:"string"
});
    fields.push({
	name:'type-id',
	type:"string"
});
    fields.push({
		name:'type-name',
		type:"string"
	});
    fields.push({
	name:'productId',
	type:"string"
});
    fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
	name:'state',
	type:"string"
});
    fields.push({
	name:'stateName',
	type:"string"
});
fields.push({
	name:'upImgTime',
	type:"date",
	dateFormat:"Y-m-d"
});
	
	
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		hidden:true,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:30*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'upImgTime',
		header:biolims.sample.upImgTime,
		width:25*6,
		sortable:true,
		hidden:true,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'billNumber',
		header:biolims.sample.billNumber,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.idCard,
		hidden : true,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser1',
		hidden : false,
		header:biolims.sample.createUser1,
		width:20*6
	});
	
	cm.push({
		dataIndex:'type-id',
		header:biolims.sample.payTypeId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'type-name',
		header:biolims.sample.payTypeName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header: biolims.common.productId,
		width:20*6,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'productName',
		header: biolims.common.testProject,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowState,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleInputTechnology/showSampleInputTechnologyListJson.action";
	var opts={};
	opts.title=biolims.sample.scienceServiceInput;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sampleInputTechnologyGrid=gridTable("show_sampleInputTechnology_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/sample/sampleInputTechnology/editSampleInputTechnology.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	
	var myRecord=sampleInputTechnologyGrid.getSelectRecord();
	var code = myRecord[0].get('code');
	
	window.location=window.ctx+'/sample/sampleInputTechnology/editSampleInputTechnology.action?id=' + id+'&code='+code;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/sampleInputTechnology/viewSampleInputTechnology.action?id=' + id;
}
function exportexcel() {
	sampleInputTechnologyGrid.title = biolims.common.exportList;
	var vExportContent = sampleInputTechnologyGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(sampleInputTechnologyGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
