$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : biolims.dicSampleType.id,
		field : "id"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.stateName,
		field : "stateName"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.nextFlowId,
		field : "nextFlowId"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.nextFlow,
		field : "nextFlow"
	});
	
	  fields.push({
		title : biolims.dicSampleType.type,
		field : "type.name"
	});
	   
	fields.push({
		title : biolims.dicSampleType.name,
		field : "name"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.note,
		field : "note"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.code,
		field : "code"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.ordeRnumber,
		field : "orderNumber"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.first,
		field : "first"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.picCode,
		field : "picCode"
	});
	
	   
	fields.push({
		title : biolims.dicSampleType.picCodeName,
		field : "picCodeName"
	});
	
	  fields.push({
		title : biolims.dicSampleType.state,
		field : "stateName"
	});
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/sample/dicSampleType/showDicSampleTypeListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/sample/dicSampleType/editDicSampleType.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/sample/dicSampleType/editDicSampleType.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/sample/dicSampleType/viewDicSampleType.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
	    "searchName":"id",
		"type":"input",
		"txt":biolims.dicSampleType.id
	});
	fields.push({
	    "searchName":"stateName",
		"type":"input",
		"txt":biolims.dicSampleType.stateName
	});
	fields.push({
	    "searchName":"nextFlowId",
		"type":"input",
		"txt":biolims.dicSampleType.nextFlowId
	});
	fields.push({
	    "searchName":"nextFlow",
		"type":"input",
		"txt":biolims.dicSampleType.nextFlow
	});
	fields.push({
		"type":"input",
		"searchName":"type.id",
		"txt":biolims.common.typeID
	});
	fields.push({
		"type":"input",
		"searchName":"type.name",
		"txt":biolims.dicSampleType.type
	});
	fields.push({
	    "searchName":"name",
		"type":"input",
		"txt":biolims.dicSampleType.name
	});
	fields.push({
	    "searchName":"note",
		"type":"input",
		"txt":biolims.dicSampleType.note
	});
	fields.push({
	    "searchName":"code",
		"type":"input",
		"txt":biolims.dicSampleType.code
	});
	fields.push({
	    "searchName":"ordeRnumber",
		"type":"input",
		"txt":biolims.dicSampleType.ordeRnumber
	});
	fields.push({
	    "searchName":"first",
		"type":"input",
		"txt":biolims.dicSampleType.first
	});
	fields.push({
	    "searchName":"picCode",
		"type":"input",
		"txt":biolims.dicSampleType.picCode
	});
	fields.push({
	    "searchName":"picCodeName",
		"type":"input",
		"txt":biolims.dicSampleType.picCodeName
	});
	fields.push({
		"type":"input",
		"searchName":"state",
		"txt":biolims.dicSampleType.state+"ID"
	});
	fields.push({
		"type":"input",
		"searchName":"stateName",
		"txt":biolims.dicSampleType.state
	});
	
	fields.push({
		"type":"table",
		"table":dicSampleTypeTable
	});

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

