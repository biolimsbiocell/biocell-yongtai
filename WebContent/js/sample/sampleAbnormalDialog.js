var sampleAbnormalDialogGrid;
$(function(){
	var cols={};
    var fields=[];
    fields.push({
    	name:'id',
    	type:"string"
    });
        fields.push({
    	name:'name',
    	type:"string"
    });
        fields.push({
    	name:'handleDate',
    	type:"string"
    });
        fields.push({
    	name:'advice',
    	type:"string"
    });
        fields.push({
    	name:'note',
    	type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.pooling.abnormalSampleId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'handleDate',
		header:biolims.common.treatDate,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'advice',
		header:biolims.common.method,
		
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*10,
		sortable:true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/sampleAbnormal/showSampleAbnormalListJson.action";
	var opts={};
	opts.title=biolims.pooling.abnormalSamples;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSampleAbnormalFun(rec);
	};
	sampleAbnormalDialogGrid=gridTable("show_dialog_sampleAbnormal_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleAbnormalDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
