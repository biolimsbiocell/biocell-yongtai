var dicSampleTypeTable;
var olddicSampleTypeChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.dicSampleType.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.dicSampleType.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"nextFlowId",
		"title": biolims.dicSampleType.nextFlowId,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
	    },
		"visible": false,	
		"className": "edit"
	});
	   colOpts.push({
		"data":"nextFlow",
		"title": biolims.dicSampleType.nextFlow,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlow");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "type-id",
		"title": biolims.common.typeID,
		"createdCell": function(td) {
			$(td).attr("saveName", "type-id");
		}
	});
	colOpts.push( {
		"data": "type-name",
		"title": biolims.dicSampleType.type,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "type-name");
			$(td).attr("type-id", rowData['type-id']);
		}
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.dicSampleType.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.dicSampleType.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"code",
		"title": biolims.dicSampleType.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"ordeNumber",
		"title": biolims.dicSampleType.ordeRnumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "ordeNumber");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"first",
		"title": biolims.dicSampleType.first,
		"createdCell": function(td) {
			$(td).attr("saveName", "first");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"picCode",
		"title": biolims.dicSampleType.picCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "picCode");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"picCodeName",
		"title": biolims.dicSampleType.picCodeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "picCodeName");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "state",
		"title": biolims.dicSampleType.state+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
		}
	});
	colOpts.push( {
		"data": "stateName",
		"title": biolims.dicSampleType.state,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "stateName");
			$(td).attr("state", rowData['state']);
		}
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#dicSampleTypeTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#dicSampleTypeTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#dicSampleTypeTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#dicSampleType_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/sample/dicSampleType/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 dicSampleTypeTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveDicSampleType($("#dicSampleTypeTable"));
		}
	});
	}
	
	var dicSampleTypeOptions = 
	table(true, "","/sample/dicSampleType/showDicSampleTypeTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	dicSampleTypeTable = renderData($("#dicSampleTypeTable"), dicSampleTypeOptions);
	dicSampleTypeTable.on('draw', function() {
		olddicSampleTypeChangeLog = dicSampleTypeTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveDicSampleType(ele) {
	var data = saveDicSampleTypejson(ele);
	var ele=$("#dicSampleTypeTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/sample/dicSampleType/saveDicSampleTypeTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveDicSampleTypejson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "type-name") {
				json["type-id"] = $(tds[j]).attr("type-id");
				continue;
			}
			
			if(k == "state-name") {
				json["state-id"] = $(tds[j]).attr("state-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		olddicSampleTypeChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
