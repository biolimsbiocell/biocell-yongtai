﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/sample/sampleAbnormal/editSampleAbnormal.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleAbnormal/showSampleAbnormalList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleAbnormal", {
					userId : userId,
					userName : userName,
					formId : $("#sampleAbnormal_id").val(),
					title : $("#sampleAbnormal_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleAbnormal_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleAbnormal/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleAbnormal/copySampleAbnormal.action?id=' + $("#sampleAbnormal_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#sampleAbnormal_id").val() + "&tableId=sampleAbnormal");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.pooling.abnormalSamples,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);