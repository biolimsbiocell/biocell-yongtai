﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	//判断如果没有上传图片不能录入信息
	if($("#upload_imga_id10").val()==''){
		$(":input").addClass(".text input readonlytrue");
		$("[type='text']").focus(function () 
		{
			message(biolims.sample.pleaseUploadImage);
			return; 
		}) ;
	}
	
	
});
function add() {
	window.location = window.ctx + "/sample/sampleInput/editSampleInput.action";
}

load("/experiment/dna/experimentDnaGet/showDnaSampleFromReceiveList.action", { }, "#sampleInputItemImg");
$("#markup").css("width","75%");

$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/sample/sampleInput/showSampleInputList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	checkAddress();
	save();
}
$("#toolbarbutton_save").click(function() {	

	//sampleInfo 的图片Id
	var sampleInfoImg = $("#upload_imga_id10").val();
	//sampleInputTemp 的图片Id
	var sampleBreastCancerTempImg = $("#upload_imga_id").val();
	if($("#upload_imga_name11").val()!="" && sampleInfoImg!="" && sampleInfoImg == sampleBreastCancerTempImg){
		//写验证
		//检测项目验证
		var productName = $("#sampleBreastCancerTemp_product_name").val();
		if (productName == "") {
			message(biolims.sample.productNameIsEmpty);
			return;
		}
		//验证证件类型不能为空	
		var re = $("#sampleBreastCancerTemp_voucherType_name").val();
		if (re == "") {
			message(biolims.sample.voucherTypeIsEmpty);
			return;
		}
		//证件号码验证 ps:输入正确的证件号码后自动计算出年龄，性别
		//身份证号的正则
		var idCodeReg = /^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
		var voucherCode =$("#sampleBreastCancerTemp_voucherCode").val();
		if(voucherCode!=""){
			if(idCodeReg.test(voucherCode)){//自动获取年龄
				var year = voucherCode.substring(6,10);
				var currentTime =new Date();
				var birthday =voucherCode.substring(6,14);
				var b=birthday.replace(/^(\d{4})(\d{2})(\d{2})$/, "$1-$2-$3");//把字符串转换成日期yyyy-MM-dd
//				$("#sampleInputTemp_birthday").val(b);//出身日期
				var ages = currentTime.getFullYear().toString() -year;
				if(ages>=16 && ages<=50){
					$("#sampleBreastCancerTemp_age").val(ages);
				}else{
					message(biolims.sample.pleaseInputRightId);
					return;
				}
				
				var genderNum = voucherCode.substring(16,17);//自动获取性别
				if(genderNum=='1' || genderNum=='3' || genderNum=='5'|| genderNum=='7'|| genderNum=='9'){
					$("#sampleInputTemp_gender").val(1);
				}else{
					$("#sampleInputTemp_gender").val(0);
				}
				
			}else{
				message(biolims.sample.pleaseInputRightId);
				return;
			}
		}else {
			message(biolims.sample.voucherIsEmpty);
			return;
		}
		
		//体重验证
		var weight =$("#sampleBreastCancerTemp_weight").val();
		if(weight>100.0){
			if(confirm(biolims.sample.Non_invasivePrenatalTesting)){
				
			}else {
				$("#sampleBreastCancerTemp_weight").val("");
				document.getElementById("sampleBreastCancerTemp_weight").focus(); 
				return;
			}
		}
		
		//最后一次放化疗时间验证
		var outTransFusion = $("#sampleBreastCancerTemp_outTransfusion").val();
		if(outTransFusion=="1"){
			if($("#sampleBreastCancerTemp_endImmuneCureDate").val()=="" || $("#sampleBreastCancerTemp_endImmuneCureDate").val()==null){
				message(biolims.sample.pleaseInputForeignBlood);
				return;
			}
		}
		//产次 ，孕次对比验证
		var parturitionTime =$("#sampleBreastCancerTemp_parturitionTime").val();//产几次
		var pregnancyTime =$("#sampleBreastCancerTemp_pregnancyTime").val();	 //孕几次
		if(parturitionTime>pregnancyTime){
			message(biolims.sample.InputError);
			return;
		}
		
	}else {
		if($("#upload_imga_id").val()==""){
			message(biolims.sample.pleaseUploadImage2Save);
			return;
		}
	}

	save();
});	

$(function(){
	var immuneCureVar = $("#sampleBreastCancerTemp_outTransfusion").val();
	//最后一次放化疗时间设置失效
	if(immuneCureVar=='0'){
		$("#last input").css({"background-color": "#DEDEDE"});
		document.getElementById("sampleBreastCancerTemp_endImmuneCureDate").disabled =true;
	}
});

function change(){
	var immuneCureVar = $("#sampleBreastCancerTemp_outTransfusion").val();
	if(immuneCureVar=='0'){
		$("#last input").css("background-color", "#DEDEDE");
		$("#last input").val('');
		document.getElementById("sampleBreastCancerTemp_endImmuneCureDate").disabled =true;
	}else if(immuneCureVar=='1'){
		$("#last input").css("background-color", "white");
		document.getElementById("sampleBreastCancerTemp_endImmuneCureDate").disabled =false;
	}
}


$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleBreastCancerTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleBreastCancerTemp_id").val(),
					title : $("#sampleBreastCancerTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleBreastCancerTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});


function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/sample/sampleInput/saveToSampleBreastCaner.action?imgId="+$("#upload_imga_id").val();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/sample/sampleInput/copySampleInput.action?id=' + $("#sampleBreastCancerTemp_id").val();
}
$("#toolbarbutton_status").click(function(){
	if ($("#sampleBreastCancerTemp_id").val()){
		commonChangeState("formId=" + $("#sampleBreastCancerTemp_id").val() + "&tableId=SampleBreastCancerTemp");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleBreastCancerTemp_code").val());
	nsc.push(biolims.sample.InfoInputCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.infoInput,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

//	var item = menu.add({
//				    	text: '复制'
//						});
//	item.on('click', editCopy);
	function sampleKind() {
		var win = Ext.getCmp('sampleKind');
		if (win) {
			win.close();
		}
		var sampleKind = new Ext.Window(
				{
					id : 'sampleKind',
					modal : true,
					title : biolims.sample.selectType,
					layout : 'fit',
					width : 600,
					height : 700,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
										+ window.ctx
										+ "/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							sampleKind.close();
						}
					} ]
				});
		sampleKind.show();
	}
	function setyblx(id,name){
		 document.getElementById("sampleBreastCancerTemp_sampleType_id").value = id;
		 document.getElementById("sampleBreastCancerTemp_sampleType_name").value = name;

		var win = Ext.getCmp('sampleKind');
		if(win){win.close();}
		}
	
	

//		  证件类型
		  function voucherTypeFun(){
			 var win = Ext.getCmp('voucherTypeFun');
			 if (win) {win.close();}
			 var voucherTypeFun= new Ext.Window({
			 id:'voucherTypeFun',modal:true,title:biolims.sample.selectCertificateType,layout:'fit',width:600,height:700,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=zjlx' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: biolims.common.close,
			  handler: function(){
				  voucherTypeFun.close(); }  }]  });     voucherTypeFun.show(); }
			  function setzjlx(id,name){
			  document.getElementById("sampleBreastCancerTemp_voucherType").value = id;
			 document.getElementById("sampleBreastCancerTemp_voucherType_name").value = name;
			 var win = Ext.getCmp('voucherTypeFun');
			 if(win){win.close();}
		}
	
	function checkType(){
		var re=$("#sampleBreastCancerTemp_voucherType_name").val();
		if(re==""){
			message(biolims.sample.voucherTypeIsEmpty);
		}
	}
	//证件号码验证
		function checkFun(){
			var reg=/^[1-9]{1}[0-9]{14}$|^[1-9]{1}[0-9]{16}([0-9]|[xX])$/;
			if(reg.test($("#sampleBreastCancerTemp_voucherCode").val())){
				var id = $("#sampleBreastCancerTemp_voucherCode").val();
				ajax("post", "/sample/sampleInput/findIdentity.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.IdRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputRightId);
			}
		}
	//手机号码验证
		function checkPhone(){
			var reg=/^((\+?86)|(\(\+86\)))?1\d{10}$/;
			if(reg.test($("#sampleBreastCancerTemp_phoneNum").val())){
				//return;
				var id = $("#sampleBreastCancerTemp_phoneNum").val();
				ajax("post", "/sample/sampleInput/findPhone.action", {
					id : id
				}, function(data) {
					if (data.success) {
						if (data.data) {
							message(biolims.sample.phoneRepeat);
						}
					} 
				}, null);
			}else{
				message(biolims.sample.pleaseInputPhone);
			}
		}

		// 选择检查项目
		function voucherProductFun() {
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
			var voucherProductFun = new Ext.Window(
					{
						id : 'voucherProductFun',
						modal : true,
						title : biolims.QPCR.selectProject,
						layout : 'fit',
						width : 600,
						height : 700,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								voucherProductFun.close();
							}
						} ]
					});
			voucherProductFun.show();
		}
		function setProductFun(id, name) {
			var productName = "";
			ajax(
					"post",
					"/com/biolims/system/product/findProductToSample.action",
					{
						code : id,
					},
					function(data) {

						if (data.success) {
							$.each(data.data, function(i, obj) {
								productName += obj.name + ",";
							});
							document.getElementById("sampleBreastCancerTemp_product_id").value = id;
							document.getElementById("sampleBreastCancerTemp_product_name").value = productName;
						}
					}, null);
			var win = Ext.getCmp('voucherProductFun');
			if (win) {
				win.close();
			}
		}
/*
 * 上传图片
 */
function upLoadImg1(){
	var isUpload = true;
	var id=$("#template_testType").val();
	var fId=$("#upload_imga_id11").val();
	var fName=$("#upload_imga_name").val();
	
	load("/system/template/template/toSampeUpload.action", { // 是否修改
		fileId:fId,
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			fName=data.fileName;
			fId=data.fileId;
//						$("#template_fileInfo").val(fId);
//						$("#template_fileInfo_name").val(fName);
			 document.getElementById('upload_imga_id').value=fId;
			 document.getElementById('upload_imga_name11').value=fName;
		});
	});
}
	