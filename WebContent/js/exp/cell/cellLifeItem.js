var cellLifeItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'cellId',
		type:"string"
	});
	   fields.push({
		name:'generation',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'gender',
		type:"string"
	});
	    fields.push({
		name:'dealType-id',
		type:"string"
	});
	    fields.push({
		name:'dealType-name',
		type:"string"
	});
	   fields.push({
		name:'reformDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'cellLife-id',
		type:"string"
	});
	    fields.push({
		name:'cellLife-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : false,
		header:'序号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cellId',
		hidden : false,
		header:'克隆号',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generation',
		hidden : false,
		header:'代次',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'入库数量',
		width:10*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
			var gendercob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'gender',
		hidden : false,
		header:'性别',
		width:10*6,
		
		renderer: Ext.util.Format.comboRenderer(gendercob),editor: gendercob
	});
	cm.push({
		dataIndex:'dealType-id',
		hidden : true,
		header:'处理方式ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dealType-name',
		hidden : false,
		header:'处理方式',
		width:15*10
	});
	cm.push({
		dataIndex:'reformDate',
		hidden : false,
		header:'改造日期',
		width:15*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cellLife-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cellLife-name',
		hidden : false,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/exp/cell/showCellLifeItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="细胞活性条件";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/exp/cell/delCellLifeItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				cellLifeItemGrid.getStore().commitChanges();
				cellLifeItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择处理方式',
			handler : selectdealTypeFun
		});
    
	opts.tbar.push({
			text : '选择相关主表',
			handler : selectcellLifeFun
		});
	
	
	
	
	
	
	
	

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = cellLifeItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							cellLifeItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	cellLifeItemGrid=gridEditTable("cellLifeItemdiv",cols,loadParam,opts);
	$("#cellLifeItemdiv").data("cellLifeItemGrid", cellLifeItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectdealTypeFun(){
	var win = Ext.getCmp('selectdealType');
	if (win) {win.close();}
	var selectdealType= new Ext.Window({
	id:'selectdealType',modal:true,title:'选择处理方式',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=dealType' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectdealType.close(); }  }]  });     selectdealType.show(); }
	function setdealType(id,name){
		var gridGrid = $("#cellLifeItemdiv").data("cellLifeItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('dealType-id',id);
			obj.set('dealType-name',name);
		});
		var win = Ext.getCmp('selectdealType')
		if(win){
			win.close();
		}
	}
	
function selectcellLifeFun(){
	var win = Ext.getCmp('selectcellLife');
	if (win) {win.close();}
	var selectcellLife= new Ext.Window({
	id:'selectcellLife',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CellLifeSelect.action?flag=cellLife' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectcellLife.close(); }  }]  });     selectcellLife.show(); }
	function setcellLife(id,name){
		var gridGrid = $("#cellLifeItemdiv").data("cellLifeItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('cellLife-id',id);
			obj.set('cellLife-name',name);
		});
		var win = Ext.getCmp('selectcellLife')
		if(win){
			win.close();
		}
	}
	
