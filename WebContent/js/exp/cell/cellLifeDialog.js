var cellLifeDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'followUser-id',
		type:"string"
	});
	    fields.push({
		name:'followUser-name',
		type:"string"
	});
	    fields.push({
		name:'followDate',
		type:"string"
	});
	    fields.push({
		name:'followResult',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'orderChance',
		type:"string"
	});
	    fields.push({
		name:'followExplain',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'名称',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'followUser-id',
		header:'跟踪人员ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'followUser-name',
		header:'跟踪人员',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'followDate',
		header:'跟踪日期',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'followResult',
		header:'跟踪结果',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:'创建人ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'orderChance',
		header:'成单可能性',
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'followExplain',
		header:'结果说明',
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/exp/cell/showCellLifeListJson.action";
	var opts={};
	opts.title="细胞活性";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setCellLifeFun(rec);
	};
	cellLifeDialogGrid=gridTable("show_dialog_cellLife_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(cellLifeDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
