var cellLifeGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'followUser-id',
		type:"string"
	});
	    fields.push({
		name:'followUser-name',
		type:"string"
	});
	    fields.push({
		name:'followDate',
		type:"string"
	});
	    fields.push({
		name:'followResult',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'orderChance',
		type:"string"
	});
	    fields.push({
		name:'followExplain',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'名称',
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'followUser-id',
		hidden:true,
		header:'跟踪人员ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'followUser-name',
		header:'跟踪人员',
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'followDate',
		header:'跟踪日期',
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'followResult',
		header:'跟踪结果',
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'orderChance',
		header:'成单可能性',
		width:10*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'followExplain',
		header:'结果说明',
		width:50*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/exp/cell/showCellLifeListJson.action";
	var opts={};
	opts.title="细胞活性";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	cellLifeGrid=gridTable("show_cellLife_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/exp/cell/editCellLife.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/exp/cell/editCellLife.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/exp/cell/viewCellLife.action?id=' + id;
}
function exportexcel() {
	cellLifeGrid.title = '导出列表';
	var vExportContent = cellLifeGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startfollowDate").val() != undefined) && ($("#startfollowDate").val() != '')) {
					var startfollowDatestr = ">=##@@##" + $("#startfollowDate").val();
					$("#followDate1").val(startfollowDatestr);
				}
				if (($("#endfollowDate").val() != undefined) && ($("#endfollowDate").val() != '')) {
					var endfollowDatestr = "<=##@@##" + $("#endfollowDate").val();

					$("#followDate2").val(endfollowDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(cellLifeGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
