$(function() {
	var cols = {};
	// cols.sm = true;
	var fields = [];

	fields.push({
		name : 'taskId',
		type : "string"
	});
	fields.push({
		name : 'formId',
		type : "string"
	});
	fields.push({
		name : 'formName',
		type : "string"
	});
	fields.push({
		name : 'title',
		type : "string"
	});
	fields.push({
		name : 'taskName',
		type : "string"
	});
	fields.push({
		name : 'applUserName',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		xtype : 'actioncolumn',
		width : 30,
		items : [ {
			icon : window.ctx + '/javascript/lib/ext-3.4.0/examples/shared/icons/fam/application_go.png',
			tooltip : '处理',
			handler : function(grid, rowIndex, colIndex) {
				
				if(window.parent.document.getElementById("alwaysopen").value=='false'){
        			window.parent.closewest();
        		}
				
				var rec = grid.getStore().getAt(rowIndex);
				var url = "/workflow/processinstance/getFormUrl.action";
				ajax("post", url, {
					formName : rec.get("formName")
				},
						function(data) {
							if (data.success) {
								window.location = window.ctx + data.url + rec.get("formId") + "&bpmTaskId="
										+ rec.get("taskId");
							}
						});
			}
		} ]
	});
	cm.push({
		dataIndex : 'taskId',
		header : '任务ID',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'formId',
		header : '表单ID',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'formName',
		header : '表单名称',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'title',
		header : '标题',
		width : 250
	});
	cm.push({
		dataIndex : 'taskName',
		header : '当前任务',
		width : 150
	});
	cm.push({
		dataIndex : 'applUserName',
		header : '发起人',
		width : 120
	});
	cm.push({
		dataIndex : 'startDate',
		header : '任务创建时间',
		width : 150
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/processinstance/showToDoTaskListJson.action?userId=" + window.userId
			+ "&groupIds=" + window.groupIds;
	var opts = {};
	opts.height = 400;
	loadParam.limit = 1000;
	var grid1 = gridTable("show_process_todo_div", cols, loadParam, opts);
	$("#show_process_todo_div").data("grid1", grid1);
	


});
function titleContentSelect() {
	var grid1 = $("#show_process_todo_div").data("grid1");
	  var rec = grid1.getSelectionModel().getSelected();
		if(window.parent.document.getElementById("alwaysopen").value=='false'){
			window.parent.closewest();
		}
		
	
		var url = "/workflow/processinstance/getFormUrl.action";
		ajax("post", url, {
			formName : rec.get("formName")
		},
				function(data) {
					if (data.success) {
						window.location = window.ctx + data.url + rec.get("formId") + "&bpmTaskId="
								+ rec.get("taskId");
					}
				});
}
function titleContentSearch(){
	var title = $("#titleContent").val();
	var grid1 = $("#show_process_todo_div").data("grid1");
 	grid1.store.reload();
	var filter = function(record, id){
		   if (record.get("title").indexOf(title)>-1){
		      	return true;
		      }
		   else
		      return false;
		};
		var onStoreLoad = function(store, records, options){
		   store.filterBy(filter);
		};
		grid1.store.on("load", onStoreLoad);
	
}