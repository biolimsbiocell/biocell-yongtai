$(function() {
	$("button").button();
	var cols = {};
	// cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});

	fields.push({
		name : 'processInstanceId',
		type : "string"
	});
	fields.push({
		name : 'applUserId',
		type : "string"
	});

	fields.push({
		name : 'applUserName',
		type : "string"
	});
	fields.push({
		name : 'formName',
		type : "string"
	});
	fields.push({
		name : 'tableName',
		type : "string"
	});
	fields.push({
		name : 'tablePath',
		type : "string"
	});
	fields.push({
		name : 'formTitle',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'businessKey',
		type : "string"
	});
	fields.push({
		name : 'currentTaskName',
		type : "string"
	});
	fields.push({
		name : 'currentTaskAssignee',
		type : "string"
	});
	fields.push({
		name : 'version',
		type : "string"
	});
	fields.push({
		name : 'isEnd',
		type : "string"
	});

	fields.push({
		name : 'isSubProcess',
		type : "string"
	});
	fields.push({
		name : 'parentProcessId',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'processInstanceId',
		header : biolims.common.processInstanceID,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'applUserId',
		header : biolims.common.startUser + "ID",
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'applUserName',
		header : biolims.common.startUser,
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'formName',
		header : biolims.master.relatedTableId,
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'tableName',
		header : biolims.master.formName,
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'tablePath',
		header : biolims.master.classPath,
		width : 120,
		hidden: true,
		sortable : false
	});
	cm.push({
		dataIndex : 'formTitle',
		header : biolims.common.formPrompt,
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'startDate',
		header : biolims.common.startTime,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : biolims.common.endTime,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'businessKey',
		header : biolims.common.businessID,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'currentTaskName',
		header : biolims.common.currentNode,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'currentTaskAssignee',
		header : biolims.common.processingPerson,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'version',
		header : biolims.common.edition,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'isEnd',
		header : biolims.common.stateName,
		width : 100,
		sortable : true
	});
	cm.push({
		dataIndex : 'isSubProcess',
		header : biolims.common.IsSubProcess,
		width : 100,
		sortable : false
	});
	cm.push({
		dataIndex : 'parentProcessId',
		header : biolims.common.mainstreamProcessID,
		width : 100,
		sortable : false
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/processinstance/showProcessInstanceJson.action";
	var opts = {};
	opts.title = biolims.common.processInstanceMonitoring;
	opts.height = document.documentElement.clientHeight - 30;
	opts.tbar = [];

	var grid;

	opts.tbar.push({
		text : biolims.common.trackingProcess,
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
				openDialog(window.ctx + url + "?instanceId=" + selRocord[0].get("processInstanceId"));
			} else {
				message(biolims.common.pleaseSelectProcessInstance);
			}
		}
	});

	opts.tbar.push({
		text : biolims.common.forcedTermination,
		handler : function() {
			confirmMsg(biolims.common.sureTermination, null, function() {
				var selRocord = grid.getSelectRecord();
				if (selRocord && selRocord.length > 0) {
					var url = "/workflow/processinstance/stopProcessInstance.action";

					ajax("post", url, {
						instanceId : selRocord[0].get("processInstanceId"),
						formName : selRocord[0].get("formName"),
						businessKey : selRocord[0].get("businessKey")
					}, function(data) {
						if (data.success) {
							message(biolims.common.terminateTheSuccess);
							grid.getStore().reload();
						} else {
							message(biolims.common.saveFailed);
						}
					}, null);
				} else {
					message(biolims.common.pleaseSelectProcessInstance);
				}
			});
		}
	},{
		text : biolims.common.seeForm,
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				var tablePath = selRocord[0].get("tablePath");
				openDialogExt(window.ctx+tablePath);  
			} else {
				message(biolims.common.pleaseSelectProcessInstance);
			}
		}
	});

	grid = gridTable("show_process_instance_div", cols, loadParam, opts);
	$("#search_btn").click(function() {
		var paramData = {
			sendUser : $("#send_user").val(),
			formId : $("#form_id").val(),
			formName : $("#form_name").val(),
			limit : grid.toolbars[1].pageSize
		};
		grid.getStore().load({
			params : paramData
		});
	});

});