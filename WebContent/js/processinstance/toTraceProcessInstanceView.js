$(function() {
	/*
	 * 只显示流程图，不显示各节点详细信息的实现方法 var url='${ctx
	 * }/workflow/process/${executionId}/trace/${processInstanceId}';
	 * $.post(url,function(data){ var dt=eval(data); var viv="<img alt='跟踪工作流'
	 * src='${ctx }/workflow/view/${processInstanceId}'
	 * style='position:absolute; left:0px; top:0px;'><div style='position:
	 * absolute;border: 2px solid red;left: "+(dt.x-1)+"px;top:
	 * "+(dt.y-1)+"px;width: "+(dt.width-2)+"px;height: "+(dt.height-2)+"px;'
	 * class='ui-corner-all-12'></div>"; $('body').append(viv); });
	 */

	var instanceId = $("#instance_id").val();
	// -显示流程图,并输出各节点的信息-
	// 流程节点详细信息

	ajax("post", "/workflow/processinstance/traceProcessInstance.action", {
		instanceId : instanceId
	}, function(infos) {

		var positionHtml = "";

		// 生成图片
		var varsArray = new Array();
		$.each(infos, function(i, v) {
			var $positionDiv = $('<div/>', {
				'class' : 'activiyAttr'
			}).css({
				position : 'absolute',
				left : (v.x - 1),
				top : (v.y - 1),
				width : (v.width - 2),
				height : (v.height - 2)
			});

			if (v.isCurrentActiviti) {
				$positionDiv.addClass('ui-corner-all-12').css({
					border : '2px solid red'
				});
			} else if (v.type == 'userTask' || v.type == 'serviceTask' || v.type == 'timerintermediatecatchevent1') {
				$positionDiv.addClass('ui-corner-all-12').css({
					border : '2px solid black'
				});
			}

			positionHtml += $positionDiv.outerHTML();
			varsArray[varsArray.length] = v.vars;
		});

		// 追加到body
		$("body").append(positionHtml);

		// 设置每个节点的data
		$('.activiyAttr').each(function(i, v) {
			$(this).data('vars', varsArray[i]);
		});

		// 此处用于显示每个节点的信息，如果不需要可以删除
		$('.activiyAttr').qtip({
			content : function() {
				var vars = $(this).data('vars');
				var tipContent = "<table class='need-border'>";
				$.each(vars, function(varKey, varValue) {
					if (varValue) {
						tipContent += "<tr><td class='label'>" + varKey + "</td><td>" + varValue + "<td/></tr>";
					}
				});
				tipContent += "</table>";
				return tipContent;
			},
			position : {
				at : 'bottom left',
				adjust : {
					x : 3
				}
			}
		});
		// end qtip

	}, null);

});