var crmProductGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-id',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'kind-id',
		type:"string"
	});
	    fields.push({
		name:'kind-name',
		type:"string"
	});
	    fields.push({
		name:'dutyDept-id',
		type:"string"
	});
	    fields.push({
		name:'dutyDept-name',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'upProductId-id',
		type:"string"
	});
	    fields.push({
		name:'upProductId-name',
		type:"string"
	});
	    fields.push({
		name:'referPrice',
		type:"string"
	});
	    fields.push({
		name:'standardPrice',
		type:"string"
	});
	    fields.push({
		name:'unit-id',
		type:"string"
	});
	    fields.push({
		name:'unit-name',
		type:"string"
	});
	    fields.push({
		name:'cost',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'kind-id',
		hidden:true,
		header:'类别ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'kind-name',
		header:'类别',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'type-id',
		hidden:true,
		header:'分类ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'type-name',
		header:'分类',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'dutyUser-id',
		hidden:true,
		header:'负责人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'dutyUser-name',
		header:'负责人',
		width:10*10,
		sortable:true
		});
		/*cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:10*10,
		sortable:true
		});*/
	cm.push({
		dataIndex:'name',
		header:'名称',
		width:20*10,
		sortable:true
	});
		
		/*cm.push({
		dataIndex:'dutyDept-id',
		hidden:true,
		header:'负责部门ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'dutyDept-name',
		header:'负责部门',
		width:10*10,
		sortable:true
		});*/
		cm.push({
		dataIndex:'state-id',
		hidden:true,
		header:'状态ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'state-name',
		header:'状态',
		width:15*10,
		sortable:true
		});
		/*
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:12*10,
		sortable:true
	});*/
		/*cm.push({
		dataIndex:'upProductId-id',
		hidden:true,
		header:'上级产品ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'upProductId-name',
		header:'上级产品',
		width:15*10,
		sortable:true
		});*/
	/*cm.push({
		dataIndex:'referPrice',
		header:'参考价格',
		width:10*10,
		sortable:true
	});*/

	/*cm.push({
		dataIndex:'cost',
		header:'单位成本',
		width:10*10,
		sortable:true
	});*/
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/service/product/showCrmProductListJson.action";
	var opts={};
	opts.title="产品主数据";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	crmProductGrid=gridTable("show_crmProduct_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/service/product/editCrmProduct.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/crm/service/product/editCrmProduct.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/crm/service/product/viewCrmProduct.action?id=' + id;
}
function exportexcel() {
	crmProductGrid.title = '导出列表';
	var vExportContent = crmProductGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(crmProductGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});