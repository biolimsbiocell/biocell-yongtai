$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var array=document.getElementById('crmProduct_detectionIn').value.split(",");
	 document.getElementById('crmProduct_detectionIn_text').value=array[0];
	 $("input[name='crmProduct.detectionIn.radio']").each(function() {
			if (array[0].indexOf($(this).val()) >= 0) {
				$(this).attr("checked", true);
			}
		})
			var array=document.getElementById('crmProduct_reportBack').value.split(",");
	 document.getElementById('crmProduct_reportBack_text').value=array[0];
	 $("input[name='crmProduct.reportBack.radio']").each(function() {
			if (array[0].indexOf($(this).val()) >= 0) {
				$(this).attr("checked", true);
			}
		})
			var array=document.getElementById('crmProduct_science').value.split(",");
	 document.getElementById('crmProduct_science_text').value=array[0];
	 $("input[name='crmProduct.science.radio']").each(function() {
			if (array[0].indexOf($(this).val()) >= 0) {
				$(this).attr("checked", true);
			}
		})
			var array=document.getElementById('crmProduct_overseasReturn').value.split(",");
	 document.getElementById('crmProduct_overseasReturn_text').value=array[0];
	 $("input[name='crmProduct.overseasReturn.radio']").each(function() {
			if (array[0].indexOf($(this).val()) >= 0) {
				$(this).attr("checked", true);
			}
		})
		load("/crm/service/product/showCrmProductEmailList.action", {
			id : $("#crmProduct_id").val()
		}, "#crmProductEmailpage");
		load("/crm/service/product/showCrmPatientEmailList.action", {
			id : $("#crmProduct_id").val()
		}, "#crmPatientEmailpage");
})	
function add() {
	window.location = window.ctx + "/crm/service/product/editCrmProduct.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/service/product/showCrmProductList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	
	save();
});	
function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	var crmPatientEmailDivData = $("#crmPatientEmaildiv").data(
	"crmPatientEmailGrid");
document.getElementById('crmPatientEmailJson').value = commonGetModifyRecords(crmPatientEmailDivData);
var crmProductEmailDivData = $("#crmProductEmaildiv").data(
"crmProductEmailGrid");
document.getElementById('crmProductEmailJson').value = commonGetModifyRecords(crmProductEmailDivData);
	var obj = document.getElementsByName("crmProduct.detectionIn.radio");
    for(var i=0; i<obj.length; i ++){
        if(obj[i].checked){
//        	if(document.getElementById('crmProduct_detectionIn_text').value != ""){
        	    document.getElementById('crmProduct_detectionIn').value =obj[i].value;
//        	}
//        	else { 
//        		document.getElementById('crmProduct_detectionIn').value="";
//        	}
        }
    }
    var obj = document.getElementsByName("crmProduct.reportBack.radio");
    for(var i=0; i<obj.length; i ++){
        if(obj[i].checked){
//        	if(document.getElementById('crmProduct_reportBack_text').value != ""){
        	    document.getElementById('crmProduct_reportBack').value =obj[i].value;
//        	}
//        	else { 
//        		document.getElementById('crmProduct_detectionIn').value="";
//        	}
        }
    }
    var obj = document.getElementsByName("crmProduct.science.radio");
    for(var i=0; i<obj.length; i ++){
        if(obj[i].checked){
//           	if(document.getElementById('crmProduct_science_text').value != ""){
        	    document.getElementById('crmProduct_science').value =obj[i].value;
//           	}
//        	else { 
//        		document.getElementById('crmProduct_science').value="";
//        	}
        }
    }
    var obj = document.getElementsByName("crmProduct.overseasReturn.radio");
    for(var i=0; i<obj.length; i ++){
        if(obj[i].checked){
//           	if(document.getElementById('crmProduct_overseasReturn_text').value != ""){
        	    document.getElementById('crmProduct_overseasReturn').value =obj[i].value;
//           	}
//        	else { 
//        		document.getElementById('crmProduct_overseasReturn').value="";
//        	}
        }
    }
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/service/product/save.action";
		$("#toolbarbutton_save").hide();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/crm/service/product/copyCrmProduct.action?id=' + $("#crmProduct_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#crmProduct_id").val() + "&tableId=crmProduct");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmProduct_id").val());
	nsc.push("编码不能为空！");
	fs.push($("#crmProduct_name").val());
	nsc.push("name不能为空！");
	/*fs.push($("#crmProduct_state").val());
	nsc.push("状态不能为空！");
	fs.push($("#crmProduct_createDate").val());
	nsc.push("创建日期不能为空！");*/
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'产品主数据',
	    	   contentEl:'markup'
	       } ]
	   });
	
	
	new Ext.form.NumberField({
		decimalPrecision : 2,
		selectOnFocus : true,
		applyTo : 'crmProduct_standardPrice'
	});
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
if (handlemethod == "modify") {
	var t = "#crmProduct_id";
	settextreadonlyById(t);
}
});
function uploadFile() {
	var isUpload = true;
	
	load("/operfile/toCommonUpload.action", { // 是否修改
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			
			$("#crmProduct_path").val(data.fileId);
			$("#crmProduct_path_name").val(data.fileName);
			message("上传成功，请保存 ！");
		});
		
	});
}
function downFile() {
var id = $("#crmProduct_path").val();	
window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
}