var crmProductEmailGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'emailUser-id',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'id',
		width:10*10,
		hidden : true
	});
	var tlstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/crm/service/product/showTableJson.action?table=User',
			method : 'POST'
		})
	});
	tlstore.load();
	var tlcob = new Ext.form.ComboBox({
		store : tlstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
		cm.push({
		dataIndex:'emailUser-id',
		header:'邮件接收人',
		width:15*10,
		renderer : Ext.util.Format.comboRenderer(tlcob),
		editor : tlcob
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/service/product/showCrmProductEmailListJson.action?id="+ $("#crmProduct_id").val()+"&type=product";
	var opts={};
	opts.title="";
	opts.height =  document.body.clientHeight-180;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientTumorInformation.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				scpProToGrid.getStore().commitChanges();
//				scpProToGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text : '删除选中',
		handler : function() {
			
			var selectRecord = crmProductEmailGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmProductEmail",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmProductEmailGrid.getStore().commitChanges();
						crmProductEmailGrid.getStore().reload();
						message("删除成功！");
					} else {
						message("删除失败！");
					}
				}, null);
			} else {
				message("请选择！");
			}
		}
	});

	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	crmProductEmailGrid=gridEditTable("crmProductEmaildiv",cols,loadParam,opts);
	$("#crmProductEmaildiv").data("crmProductEmailGrid", crmProductEmailGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})