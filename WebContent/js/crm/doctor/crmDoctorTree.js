$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : biolims.crmDoctor.id,
		field : "id"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.mobile,
		field : "mobile"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.weiChatId,
		field : "weiChatId"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.isMonthFee,
		field : "isMonthFee"
	});
	
	  fields.push({
		title : biolims.crmDoctor.typeId,
		field : "typeId.name"
	});
	   
	fields.push({
		title : biolims.crmDoctor.state,
		field : "state"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.name,
		field : "name"
	});
	
	  fields.push({
		title : biolims.crmDoctor.crmCustomer,
		field : "crmCustomer.name"
	});
	  fields.push({
		title : biolims.crmDoctor.ks,
		field : "ks.name"
	});
	   
	fields.push({
		title : biolims.crmDoctor.clinicCharacteristics,
		field : "clinicCharacteristics"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.goodAtDisease,
		field : "goodAtDisease"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.doctorIntroduction,
		field : "doctorIntroduction"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.mail,
		field : "mail"
	});
	
	   
	fields.push({
		title : biolims.crmDoctor.payTreasure,
		field : "payTreasure"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/crm/doctor/crmDoctor/showCrmDoctorListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/crm/doctor/editCrmDoctor.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/doctor/editCrmDoctor.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/doctor/viewCrmDoctor.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : biolims.crmDoctor.id,
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.mobile,
		"searchName" : 'mobile',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.weiChatId,
		"searchName" : 'weiChatId',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.isMonthFee,
		"searchName" : 'isMonthFee',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.crmDoctor.typeId,
		"searchName" : 'typeId.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.crmDoctor.state,
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.name,
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.crmDoctor.crmCustomer,
		"searchName" : 'crmCustomer.name',
		"type" : "input"
	});
	   fields.push({
		header : biolims.crmDoctor.ks,
		"searchName" : 'ks.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.crmDoctor.clinicCharacteristics,
		"searchName" : 'clinicCharacteristics',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.goodAtDisease,
		"searchName" : 'goodAtDisease',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.doctorIntroduction,
		"searchName" : 'doctorIntroduction',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.mail,
		"searchName" : 'mail',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmDoctor.payTreasure,
		"searchName" : 'payTreasure',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

