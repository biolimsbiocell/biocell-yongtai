var crmDoctorItemTable;
var oldcrmDoctorItemChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#crmDoctor_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.crmDoctorItem.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
	    "visible": false,
	});
	colOpts.push( {
		"data": "crmDoctor-id",
		"title": biolims.crmDoctorItem.crmDoctorId,
		"createdCell": function(td) {
			$(td).attr("saveName", "crmDoctor-id");
		},
		"visible": false,
	});
	colOpts.push( {
		"data": "crmDoctor-name",
		"title": biolims.crmDoctorItem.crmDoctorId,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "crmDoctorId-name");
			$(td).attr("crmDoctor-id", rowData['crmDoctor-id']);
		},
		"visible": false,
	});
	   colOpts.push({
		"data":"productId",
		"title": biolims.crmDoctorItem.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
	    },
		"visible": false,	
	});
	   colOpts.push({
		"data":"productName",
		"title": biolims.crmDoctorItem.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
	    },
	   
	});
	   colOpts.push({
		"data":"money",
		"title": biolims.crmDoctorItem.money,
		"createdCell": function(td) {
			$(td).attr("saveName", "money");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"discount",
		"title": biolims.crmDoctorItem.discount,
		"createdCell": function(td) {
			$(td).attr("saveName", "discount");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"price",
		"title": biolims.crmDoctorItem.price,
		"createdCell": function(td) {
			$(td).attr("saveName", "price");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"way",
		"title": biolims.crmDoctorItem.way,
		"createdCell": function(td) {
			$(td).attr("saveName", "way");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.crmDoctorItem.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#crmDoctorItemTable"));
	}
	});
/*	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#crmDoctorItemTable"))
		}
	});*/
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#crmDoctorItemTable"));
		}
	});
	/*tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#crmDoctorItem_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/crm/doctor/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 crmDoctorItemTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});*/
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#crmDoctorItemTable"),
				"/crm/doctor/delCrmDoctorItem.action");
		}
	});
//	tbarOpts.push({
//		text: biolims.common.save,
//		action: function() {
//			saveCrmDoctorItem($("#crmDoctorItemTable"));
//		}
//	});
	}
	tbarOpts.push({
		text: '<i class="fa fa-yelp"></i>'+biolims.master.product,
		action: voucherProductFun
	});
	var crmDoctorItemOptions = table(true,
		id,
		'/crm/doctor/showCrmDoctorItemTableJson.action', colOpts, tbarOpts);
	crmDoctorItemTable = renderData($("#crmDoctorItemTable"), crmDoctorItemOptions);
	crmDoctorItemTable.on('draw', function() {
		oldcrmDoctorItemChangeLog = crmDoctorItemTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveCrmDoctorItem(ele) {
	var data = saveCrmDoctorItemjson(ele);
	var ele=$("#crmDoctorItemTable");
	var changeLog = biolims.common.templateFieldsItem+"：";
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog != biolims.common.templateFieldsItem+"："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/crm/doctor/saveCrmDoctorItemTable.action',
		data: {
			id: $("#crmDoctor_id").val(),
			dataJson: data,
			changeLog: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	});
}
// 获得保存时的json数据
function saveCrmDoctorItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "crmDoctorId-name") {
				json["crmDoctorId-id"] = $(tds[j]).attr("crmDoctorId-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//function getChangeLog(data, ele, changeLog) {
//	var saveJson = JSON.parse(data);
//	saveJson.forEach(function(v, i) {
//		var id = v.id;
//		if (!id) {
//			changeLog += '新增记录:';
//			for ( var k in v) {
//				var title = ele.find("th[savename=" + k + "]").text();
//				changeLog += '"' + title + '"为"' + v[k] + '";';
//			}
//			return true;
//		}
//		changeLog += biolims.common.uumberIs+':"' + v.code + '":';
//		oldcrmDoctorItemChangeLog.data.forEach(function(vv, ii) {
//			if(vv.id == id) {
//				for(var k in v) {
//					if(v[k] != vv[k]) {
//						var title = ele.find("th[savename=" + k + "]").text();
//						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
//					}
//				}
//				return false;
//			}
//		});
//	});
//	return changeLog;
//}
//选择检查产品
function voucherProductFun() {
	var rows = $("#crmDoctorItemTable .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer.open({
		title: biolims.crm.selectProduct,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/com/biolims/system/product/showProductSelTree.action",
		yes: function(index, layer) {
			var name = [],
				id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			rows.addClass("editagain");
			rows.find("td[savename='productName']").attr("productId", id).text(name.join(","));
			rows.find("td[savename='productId']").text(id.join(","));
			top.layer.close(index);
		},
		
	});
}