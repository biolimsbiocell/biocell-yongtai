$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "编码",
	});
	colOpts.push({
		"data" : "name",
		"title" : "名称",
	});
	colOpts.push({
		"data" : "mobile",
		"title" : "联系方式",
	});
	var tbarOpts = [];
	var addCrmPatientOptions = table(false, null,
			'/crm/doctor/showDialogCrmPatientTableJson.action',
			colOpts, tbarOpts)
	var CrmPatientTable = renderData($("#addcrmPatientTable"), addCrmPatientOptions);
	$("#addcrmPatientTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addcrmPatientTable_wrapper .dt-buttons").empty();
				$('#addcrmPatientTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addcrmPatientTable tbody tr");
			CrmPatientTable.ajax.reload();
			CrmPatientTable.on('draw', function() {
				trs = $("#addcrmPatientTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})

