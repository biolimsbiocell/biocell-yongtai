var crmDoctorTable;
var oldcrmDoctorChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.crmDoctor.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"mobile",
		"title": biolims.crmDoctor.mobile,
		"createdCell": function(td) {
			$(td).attr("saveName", "mobile");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"weiChatId",
		"title": biolims.crmDoctor.weiChatId,
		"createdCell": function(td) {
			$(td).attr("saveName", "weiChatId");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"isMonthFee",
		"title": biolims.crmDoctor.isMonthFee,
		"createdCell": function(td) {
			$(td).attr("saveName", "isMonthFee");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "typeId-id",
		"title": biolims.crmDoctor.typeId+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "typeId-id");
		}
	});
	colOpts.push( {
		"data": "typeId-name",
		"title": biolims.crmDoctor.typeId,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "typeId-name");
			$(td).attr("typeId-id", rowData['typeId-id']);
		}
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.crmDoctor.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.crmDoctor.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "crmCustomer-id",
		"title": biolims.common.ofHospital+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "crmCustomer-id");
		}
	});
	colOpts.push( {
		"data": "crmCustomer-name",
		"title": biolims.crmDoctor.crmCustomer,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "crmCustomer-name");
			$(td).attr("crmCustomer-id", rowData['crmCustomer-id']);
		}
	});
	colOpts.push( {
		"data": "ks-id",
		"title": biolims.crmDoctor.ks+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "ks-id");
		}
	});
	colOpts.push( {
		"data": "ks-name",
		"title": biolims.crmDoctor.ks,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "ks-name");
			$(td).attr("ks-id", rowData['ks-id']);
		}
	});
	   colOpts.push({
		"data":"clinicCharacteristics",
		"title": biolims.crmDoctor.clinicCharacteristics,
		"createdCell": function(td) {
			$(td).attr("saveName", "clinicCharacteristics");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"goodAtDisease",
		"title": biolims.crmDoctor.goodAtDisease,
		"createdCell": function(td) {
			$(td).attr("saveName", "goodAtDisease");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"doctorIntroduction",
		"title": biolims.crmDoctor.doctorIntroduction,
		"createdCell": function(td) {
			$(td).attr("saveName", "doctorIntroduction");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"mail",
		"title": biolims.crmDoctor.mail,
		"createdCell": function(td) {
			$(td).attr("saveName", "mail");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"payTreasure",
		"title": biolims.crmDoctor.payTreasure,
		"createdCell": function(td) {
			$(td).attr("saveName", "payTreasure");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#crmDoctorTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#crmDoctorTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#crmDoctorTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#crmDoctor_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/crm/doctor/crmDoctor/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 crmDoctorTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveCrmDoctor($("#crmDoctorTable"));
		}
	});
	}
	
	var crmDoctorOptions = 
	table(true, "","/crm/doctor/crmDoctor/showCrmDoctorTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	crmDoctorTable = renderData($("#crmDoctorTable"), crmDoctorOptions);
	crmDoctorTable.on('draw', function() {
		oldcrmDoctorChangeLog = crmDoctorTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveCrmDoctor(ele) {
	var data = saveCrmDoctorjson(ele);
	var ele=$("#crmDoctorTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/crm/doctor/crmDoctor/saveCrmDoctorTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveCrmDoctorjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "typeId-name") {
				json["typeId-id"] = $(tds[j]).attr("typeId-id");
				continue;
			}
			
			if(k == "crmCustomer-name") {
				json["crmCustomer-id"] = $(tds[j]).attr("crmCustomer-id");
				continue;
			}
			
			if(k == "ks-name") {
				json["ks-id"] = $(tds[j]).attr("ks-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldcrmDoctorChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
