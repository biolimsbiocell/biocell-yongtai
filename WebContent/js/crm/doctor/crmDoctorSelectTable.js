var crmDoctorTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.crmDoctor.id,
	});
	
	    fields.push({
		"data":"mobile",
		"title":biolims.crmDoctor.mobile,
	});
	
	    fields.push({
		"data":"weiChatId",
		"title":biolims.crmDoctor.weiChatId,
	});
	
	    fields.push({
		"data":"isMonthFee",
		"title":biolims.crmDoctor.isMonthFee,
	});
	
	    fields.push({
		"data":"typeId-id",
		"title":biolims.crmDoctor.typeId+"ID"
	});
	    fields.push({
		"data":"typeId-name",
		"title":biolims.crmDoctor.typeId
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.crmDoctor.state,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.crmDoctor.name,
	});
	
	    fields.push({
		"data":"crmCustomer-id",
		"title":biolims.crmDoctor.crmCustomer+"ID"
	});
	    fields.push({
		"data":"crmCustomer-name",
		"title":biolims.crmDoctor.crmCustomer
	});
	
	    fields.push({
		"data":"ks-id",
		"title":biolims.crmDoctor.ks
	});
	    fields.push({
		"data":"ks-name",
		"title":biolims.crmDoctor.ks
	});
	
	    fields.push({
		"data":"clinicCharacteristics",
		"title":biolims.crmDoctor.clinicCharacteristics,
	});
	
	    fields.push({
		"data":"goodAtDisease",
		"title":biolims.crmDoctor.goodAtDisease,
	});
	
	    fields.push({
		"data":"doctorIntroduction",
		"title":biolims.crmDoctor.doctorIntroduction,
	});
	
	    fields.push({
		"data":"mail",
		"title":biolims.crmDoctor.mail,
	});
	
	    fields.push({
		"data":"payTreasure",
		"title":biolims.crmDoctor.payTreasure,
	});
	
	var options = table(true, "","/crm/doctor/showCrmDoctorTableJson.action",
	 fields, null)
	crmDoctorTable = renderData($("#addCrmDoctorTable"), options);
	$('#addCrmDoctorTable').on('init.dt', function() {
		recoverSearchContent(crmDoctorTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.crmDoctor.id
		});
	   fields.push({
		    "searchName":"mobile",
			"type":"input",
			"txt":biolims.crmDoctor.mobile
		});
	   fields.push({
		    "searchName":"weiChatId",
			"type":"input",
			"txt":biolims.crmDoctor.weiChatId
		});
	   fields.push({
		    "searchName":"isMonthFee",
			"type":"input",
			"txt":biolims.crmDoctor.isMonthFee
		});
	fields.push({
	    "type":"input",
		"searchName":"typeId.id",
		"txt":biolims.crmDoctor.typeId+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"typeId.name",
		"txt":biolims.crmDoctor.typeId
	});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.crmDoctor.state
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.crmDoctor.name
		});
	fields.push({
	    "type":"input",
		"searchName":"crmCustomer.id",
		"txt":biolims.crmDoctor.crmCustomer+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"crmCustomer.name",
		"txt":biolims.crmDoctor.crmCustomer
	});
	fields.push({
	    "type":"input",
		"searchName":"ks.id",
		"txt":biolims.crmDoctor.ks+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"ks.name",
		"txt":biolims.crmDoctor.ks
	});
	   fields.push({
		    "searchName":"clinicCharacteristics",
			"type":"input",
			"txt":biolims.crmDoctor.clinicCharacteristics
		});
	   fields.push({
		    "searchName":"goodAtDisease",
			"type":"input",
			"txt":biolims.crmDoctor.goodAtDisease
		});
	   fields.push({
		    "searchName":"doctorIntroduction",
			"type":"input",
			"txt":biolims.crmDoctor.doctorIntroduction
		});
	   fields.push({
		    "searchName":"mail",
			"type":"input",
			"txt":biolims.crmDoctor.mail
		});
	   fields.push({
		    "searchName":"payTreasure",
			"type":"input",
			"txt":biolims.crmDoctor.payTreasure
		});
	
	fields.push({
		"type":"table",
		"table":crmDoctorTable
	});
	return fields;
}
