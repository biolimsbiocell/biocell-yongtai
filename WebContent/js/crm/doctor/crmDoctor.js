var crmDoctorTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.crmDoctor.id,
	});
	
	    fields.push({
		"data":"mobile",
		"title":biolims.crmDoctor.mobile,
	});
	
	    fields.push({
		"data":"weichatId",
		"title":biolims.crmDoctor.weiChatId,
	});
	
	    fields.push({
		"data":"isMonthFee",
		"title":biolims.crmDoctor.isMonthFee,
	});
	
	    fields.push({
		"data":"type-id",
		"title":biolims.crmDoctor.typeId+"ID"
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.crmDoctor.typeId
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.crmDoctor.state,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.crmDoctor.name,
	});
	
	    fields.push({
		"data":"crmCustomer-id",
		"title":biolims.crmDoctor.typeId+"ID"
	});
	    fields.push({
		"data":"crmCustomer-name",
		"title":biolims.crmDoctor.crmCustomer
	});
	
	    fields.push({
		"data":"ks-id",
		"title":biolims.crmDoctor.ks+"ID"
	});
	    fields.push({
		"data":"ks-name",
		"title":biolims.crmDoctor.ks
	});
	
	    fields.push({
		"data":"clinicCharacteristics",
		"title":biolims.crmDoctor.clinicCharacteristics,
	});
	
	    fields.push({
		"data":"goodAtDisease",
		"title":biolims.crmDoctor.goodAtDisease,
	});
	
	    fields.push({
		"data":"doctorIntroduction",
		"title":biolims.crmDoctor.doctorIntroduction,
	});
	
	    fields.push({
		"data":"mail",
		"title":biolims.crmDoctor.mail,
	});
	
	    fields.push({
		"data":"payTreasure",
		"title":biolims.crmDoctor.payTreasure,
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "CrmDoctor"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/crm/doctor/showCrmDoctorTableJson.action",
	 fields, null)
	crmDoctorTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(crmDoctorTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/crm/doctor/editCrmDoctor.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/doctor/editCrmDoctor.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/doctor/viewCrmDoctor.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.crmDoctor.id
		});
	   fields.push({
		    "searchName":"mobile",
			"type":"input",
			"txt":biolims.crmDoctor.mobile
		});
	   fields.push({
		    "searchName":"weichatId",
			"type":"input",
			"txt":biolims.crmDoctor.weiChatId
		});
	   fields.push({
		    "searchName":"isMonthFee",
			"type":"input",
			"txt":biolims.crmDoctor.isMonthFee
		});
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.crmDoctor.typeId+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.crmDoctor.typeId
	});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.crmDoctor.state
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.crmDoctor.name
		});
/*	fields.push({
	    "type":"input",
		"searchName":"crmCustomer.id",
		"txt":biolims.crmDoctor.crmCustomer+"ID"
	});*/
	fields.push({
	    "type":"input",
		"searchName":"crmCustomer.name",
		"txt":biolims.crmDoctor.crmCustomer
	});
	fields.push({
	    "type":"input",
		"searchName":"ks.id",
		"txt":biolims.crmDoctor.ks+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"ks.name",
		"txt":biolims.crmDoctor.ks
	});
	   fields.push({
		    "searchName":"clinicCharacteristics",
			"type":"input",
			"txt":biolims.crmDoctor.clinicCharacteristics
		});
	   fields.push({
		    "searchName":"goodAtDisease",
			"type":"input",
			"txt":biolims.crmDoctor.goodAtDisease
		});
	   fields.push({
		    "searchName":"doctorIntroduction",
			"type":"input",
			"txt":biolims.crmDoctor.doctorIntroduction
		});
	   fields.push({
		    "searchName":"mail",
			"type":"input",
			"txt":biolims.crmDoctor.mail
		});
	   fields.push({
		    "searchName":"payTreasure",
			"type":"input",
			"txt":biolims.crmDoctor.payTreasure
		});
	
	fields.push({
		"type":"table",
		"table":crmDoctorTable
	});
	return fields;
}
