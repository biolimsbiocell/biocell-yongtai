var crmPatientDialogGrid;/** 医生js*/
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});

	    fields.push({
		name:'name',
		type:"string"
	});

	    fields.push({
			name:'ks-id',
			type:"string"
		});
		    fields.push({
				name:'ks-name',
				type:"string"
			});
	    fields.push({
		name:'crmCustomer-id',
		type:"string"
	});
	    fields.push({
			name:'crmCustomer-name',
			type:"string"
		});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
			name:'mobile',
			type:"string"
		});
	    
	    
	    
		fields.push({
	    name:'isMonthFee',
	    type:"string"
	});

	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header: biolims.common.id,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:150,
		sortable:true
	});
	cm.push({
		dataIndex:'ks-id',
		header:biolims.crm.ksId,
		width:290,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'mobile',
		header:biolims.common.phone,
		width:150,
		sortable:true
	});
	cm.push({
		dataIndex:'ks-name',
		header:biolims.crm.ksName,
		width:290,
		sortable:true
	});
	cm.push({
		dataIndex:'crmCustomer-id',
		header:biolims.master.hospitalId,
		width:290,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'crmCustomer-name',
		header:biolims.master.hospitalName,
		width:290,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:10*10,
		renderer: function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "0") {
				return biolims.master.invalid;
			} else if (value == "1") {
				return biolims.master.valid;
			}
		}
	});
	cm.push({
		dataIndex:'isMonthFee',
		header:biolims.crm.isMonthFee,
		width:10*10,
		renderer: function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "0") {
				return biolims.common.no;
			} else if (value == "1") {
				return biolims.common.yes;
			}
		}
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/doctor/showDialogCrmPatientListJson.action?crmCustomerId="+ $("#crmCustomerId").val()+"";
	var opts={};
	opts.title=biolims.crm.DoctorInfo;
	opts.height=document.body.clientHeight*0.9;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setDicProbeFun(rec);
	};
	crmPatientDialogGrid=gridTable("show_dialog_crmPatient_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 400;
		option.height = 300;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)": function() {
				commonSearchAction(crmPatientDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
function nd(){
	window.open(window.ctx + '/crm/doctor/editCrmPatient.action','','');
}

