var crmPatientGrid;/**   医生主页面*/
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
			name:'crmCustomer-id',
			type:"string"
		});
	    fields.push({
			name:'crmCustomer-name',
			type:"string"
		});
	    fields.push({
			name:'name',
			type:"string"
		});
	    fields.push({
			name:'state',
			type:"string"
		});
	    fields.push({
			name:'isMonthFee',
			type:"string"
		});
	    fields.push({
			name:'ks-id',
			type:"string"
		});
	    fields.push({
			name:'ks-name',
			type:"string"
		});
	    fields.push({
			name:'jobTitle',
			type:"string"
		});
	    fields.push({
			name:'mobile',
			type:"string"
		});
	    fields.push({
			name:'payTreasure',
			type:"string"
		});
	    fields.push({
			name:'mail',
			type:"string"
		});
	    fields.push({
			name:'doctorIntroduction',
			type:"string"
		});
	    fields.push({
			name:'goodAtDisease',
			type:"string"
		});
	    fields.push({
			name:'clinicCharacteristics',
			type:"string"
		});
	    fields.push({
			name:'type-id',
			type:"string"
		});
	    fields.push({
			name:'type-name',
			type:"string"
		});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:150,
		sortable:true
	});
	cm.push({
		dataIndex:'crmCustomer-id',
		header:biolims.master.hospitalId,
		width:10*10,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'crmCustomer-name',
		header:biolims.master.hospitalName,
		width:10*20,
		sortable:true
	});
	cm.push({
		dataIndex:'ks-id',
		header:biolims.crm.ksId,
		width:10*10,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'ks-name',
		header:biolims.crm.ksName,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'clinicCharacteristics',
		header:biolims.crm.clinicCharacteristics,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'goodAtDisease',
		header:biolims.crm.goodAtDisease,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'doctorIntroduction',
		header:biolims.crm.doctorIntroduction,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'mail',
		header:biolims.crm.email,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'payTreasure',
		header:biolims.crm.payTreasure,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'mobile',
		header:biolims.common.phone,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'weichatId',
		header:biolims.master.weichatId,
		width:10*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'ks-name',
//		header:'科室',
//		width:10*10,
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'jobTitle',
//		header:'职称',
//		width:10*10,
//		sortable:true
//	});
//
//	cm.push({
//		dataIndex:'name',
//		header:'姓名',
//		width:150,
//		sortable:true
//	});

	cm.push({
		dataIndex:'isMonthFee',
		header:biolims.crm.isMonthFee,
		width:20*3,
		renderer: function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "0") {
				return biolims.common.no;
			} else if (value == "1") {
				return biolims.common.yes;
			}
		}
	});

	cm.push({
	dataIndex:'type-id',
	header:biolims.common.customerCategoryID,
	width:20*6,
	hidden:true,
	sortable:true
});
	cm.push({
		dataIndex:'type-name',
		header:biolims.common.customerCategory,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:150,
		renderer: function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "1") {
				return biolims.master.valid;
			} else if (value == "0") {
				return biolims.master.invalid;
			}
		}
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/doctor/showCrmPatientListJson.action";
	loadParam.limit = 1000;
	var opts={};
	opts.title=biolims.crm.DoctorInfo;
	opts.height=document.body.clientHeight*0.8;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	crmPatientGrid=gridTable("show_crmPatient_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/doctor/editCrmPatient.action';//进入链接为不知何用
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;//editCrmPatient
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/crm/doctor/editCrmPatient.action?id=' + id;//进入链接为不知何用
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/crm/doctor/viewCrmPatient.action?id=' + id;//进入链接为不知何用
}
	function exportexcel() {
		crmPatientGrid.title = biolims.crm.DoctorList;
		var vExportContent = crmPatientGrid.getExcelXml();
		var x = document.getElementById('gridhtm');
		x.value = vExportContent;
		document.excelfrm.submit();
	}
$(function() {
	$("#opensearch").click(function() {
		var option = {
				
		};
		option.width = 542;option.height = 417;
		loadDialogPage($("#jstj"),
				biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(crmPatientGrid);
				$(this).dialog("close");
				},"清空(Empty)": function() {
					form_reset();
					}
				}, true, option);
		});
	});
