$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx
			+ "/crm/doctor/editCrmPatient.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx
			+ '/crm/doctor/showCrmPatientList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
function save() {
	if (checkSubmit() == true) {
		var crmDoctorItemDivData = $("#crmDoctorItemdiv").data("crmDoctorItemGrid");
		document.getElementById('crmDoctorItemJson').value = commonGetModifyRecords(crmDoctorItemDivData);
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/doctor/save.action";
		$("#toolbarbutton_save").hide();
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				       msg : biolims.common.beingProcessed,
				       removeMask : true// 完成后移除
				 });
			loadMarsk.show();	
			}
}
function editCopy() {
	window.location = window.ctx
			+ '/crm/doctor/copyCrmPatient.action?id='
			+ $("#crmPatient_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#crmPatient_id").val()
			+ "&tableId=crmPatient");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmDoctor_id").val());
	nsc.push(biolims.common.IdEmpty);
//	fs.push($("#crmDoctor_type").val());
//	nsc.push("客户类别不能为空");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}

function showCrmDoctorFun() {
	var win = Ext.getCmp('showCrmDoctorFun');
	if (win) {
		win.close();
	}
	var showCrmDoctorFun = new Ext.Window(
			{
				id : 'showCrmDoctorFun',
				modal : true,
				title : "",
				layout : 'fit',
				width : document.body.clientWidth / 2,
				height : document.body.clientHeight / 1.5,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=khlx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showCrmDoctorFun.close();
					}
				} ]
			});
	showCrmDoctorFun.show();
}
function setkhlx(id,name){
	document.getElementById("crmDoctor_type").value = id;
	document.getElementById("crmDoctor_type_name").value = name;
//	if($("#crmDoctor_type").val() == 'docrmcustomer' || $("#crmDoctor_type").val() == 'incrmcustomer'){
//		document.getElementById("qyxx").style.display="none";
//	}else{
//		document.getElementById("qyxx").style.display="inline";
//	}
	var win = Ext.getCmp('showCrmDoctorFun');
	if(win){win.close();}
	}



$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : biolims.crm.DoctorInfo,
				contentEl : 'markup'
			}]
		});
	});
	load("/crm/doctor/showCrmDoctorItemList.action", {
		id : $("#crmDoctor_id").val()
	}, "#crmDoctorItempage");
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	} else if ('modify' == handlemethod) {
		settextreadonlyJquery($("#crmDoctor_id"));
	}
});
