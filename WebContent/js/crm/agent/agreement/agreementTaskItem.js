var agreementTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'agreementTask-id',
		type:"string"
	});
	    fields.push({
		name:'agreementTask-name',
		type:"string"
	});
	   fields.push({
		name:'advance-id',
		type:"string"
	});
	   fields.push({
			name:'advance-name',
			type:"string"
		});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
			name:'productName',
			type:"string"
		});
	   fields.push({
		name:'money',
		type:"string"
	});
	   fields.push({
		name:'discount',
		type:"string"
	});
	   fields.push({
		name:'price',
		type:"string"
	});
	   fields.push({
		name:'startDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'endDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'accessory',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'agreementTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'agreementTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'advance-id',
		hidden : false,
		header:biolims.common.primaryId,
		width:15*10
	});
	var testAdvance =new Ext.form.TextField({
        allowBlank: false
	});
	testAdvance.on('focus', function() {
		loadTestAdvance();
	});
	cm.push({
		dataIndex:'advance-name',
		header:biolims.common.primary,
		width:15*10,
		editor : testAdvance
	});
	
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.master.productId,
		width:20*6
	});
	var testProduct =new Ext.form.TextField({
        allowBlank: false
	});
	testProduct.on('focus', function() {
		ProductFun();
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.master.product,
		width:15*10,
		editor : testProduct
	});
	
	cm.push({
		dataIndex:'money',
		hidden : false,
		header:biolims.crm.money,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'discount',
		hidden : false,
		header:biolims.crm.discount,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'price',
		hidden : false,
		header:biolims.crm.productFee,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'startDate',
		hidden : false,
		header:biolims.common.startTime,
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'endDate',
		hidden : false,
		header:biolims.common.endTime,
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		xtype : 'actioncolumn',
		width : 80,
		hidden : false,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"&modelType=agreementTaskItem' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		}]
	});
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/agreement/agreementTask/showAgreementTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.crm.protocolDetails;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/agent/agreement/agreementTask/delAgreementTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				agreementTaskItemGrid.getStore().commitChanges();
				agreementTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.primary,
		handler : loadTestAdvance
	});
	opts.tbar.push({
		text : biolims.master.product,
		handler : ProductFun
	});
//	opts.tbar.push({
//		text : '查看附件',
//		handler : loadPic
//	});
	
	agreementTaskItemGrid=gridEditTable("agreementTaskItemdiv",cols,loadParam,opts);
	$("#agreementTaskItemdiv").data("agreementTaskItemGrid", agreementTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})


	function ProductFun() {
		var win = Ext.getCmp('productFun');
		if (win) {
			win.close();
		}
		var productFun = new Ext.Window(
				{
					id : 'productFun',
					modal : true,
					title : biolims.crm.selectProduct,
					layout : 'fit',
					width : 600,
					height : 600,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							productFun.close();
						}
					} ]
				});
		productFun.show();
	}

	function setProductFun(id,name) {
		var gridGrid = $("#agreementTaskItemdiv").data("agreementTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('productId',id);
			obj.set('productName',name);
		});
		var win = Ext.getCmp('productFun');
		if (win) {
			win.close();
		}
		
	}

	function loadPic(){
		var record = agreementTaskItemGrid.getSelectionModel().getSelections();
		if(record.length==1){
			var model="agreementTaskItem";
			window.open(window.ctx+"/crm/agent/agreement/agreementTask/loadPic.action?id="+record[0].get("id")+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
			
		}else{
			message(biolims.common.selectRecord);
		}

	}
 
	var loadAdvance;	
	//查询代理商
	function loadTestAdvance(){
		var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadAdvance=loadDialogPage(null, biolims.common.primary, "/crm/agent/primary/primaryTask/primaryTaskSelect.action", {
			"Confirm": function() {
				var operGrid = primaryTaskDialogGrid;
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = agreementTaskItemGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						$.each(records, function(a, b) {
							b.set("advance-id", obj.get("id"));
							b.set("advance-name", obj.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}
	function setPrimaryTaskFun(){
		var operGrid = $("#show_dialog_primaryTask_div").data("primaryTaskDialogGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		var records = agreementTaskItemGrid.getSelectRecord();
		if (selectRecord.length > 0) {
			$.each(selectRecord, function(i, obj) {
				$.each(records, function(a, b) {
					b.set("advance-id", obj.get("id"));
					b.set("advance-name", obj.get("name"));
				});
			});
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		loadAdvance.dialog("close");

	}

