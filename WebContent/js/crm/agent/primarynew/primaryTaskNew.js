var primaryTaskNewTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.common.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.common.designation,
	});
	
	    fields.push({
		"data":"address",
		"title":biolims.common.addr,
	});
	
	    fields.push({
		"data":"principal",
		"title":biolims.master.personName,
	});
	
	    fields.push({
		"data":"phone",
		"title":biolims.common.phone,
	});
	
	    fields.push({
		"data":"marketer",
		"title":biolims.crm.marketerName,
	});
	
	    fields.push({
		"data":"money",
		"title":biolims.crm.surplusMoney,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":"创建人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.sample.createUserName
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.sample.createDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	    fields.push({
		"data":"state-name",
		"title":biolims.common.stateName,
	});
	    fields.push({
		"data":"note",
		"title":biolims.common.note,
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "PrimaryTaskNew"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});

	var options = table(true, "","/crm/agent/primarynew/primaryTaskNew/showPrimaryTaskNewTableJson.action",
	 fields, null)
	primaryTaskNewTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(primaryTaskNewTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/crm/agent/primarynew/primaryTaskNew/editPrimaryTaskNew.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/agent/primarynew/primaryTaskNew/editPrimaryTaskNew.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/agent/primarynew/primaryTaskNew/viewPrimaryTaskNew.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":"编码"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":"名称"
		});
	   fields.push({
		    "searchName":"address",
			"type":"input",
			"txt":"地址"
		});
	   fields.push({
		    "searchName":"principal",
			"type":"input",
			"txt":"负责人"
		});
	   fields.push({
		    "searchName":"phone",
			"type":"input",
			"txt":"联系电话"
		});
	   fields.push({
		    "searchName":"marketer",
			"type":"input",
			"txt":"管理销售"
		});
	   fields.push({
		    "searchName":"money",
			"type":"input",
			"txt":"余额"
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":"创建人"
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":"创建日期"
		});
	fields.push({
			"txt": "创建日期(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state-name",
			"type":"input",
			"txt":"状态名称"
		});
	fields.push({
		"type":"table",
		"table":primaryTaskNewTable
	});
	return fields;
}
