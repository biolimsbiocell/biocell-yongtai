$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : "编号",
		field : "id"
	});
	
	   
	fields.push({
		title : "名称",
		field : "name"
	});
	
	   
	fields.push({
		title : "地址",
		field : "address"
	});
	
	   
	fields.push({
		title : "负责人",
		field : "principal"
	});
	
	   
	fields.push({
		title : "电话",
		field : "phone"
	});
	
	   
	fields.push({
		title : "管理销售",
		field : "marketer"
	});
	
	   
	fields.push({
		title : "预付余额",
		field : "money"
	});
	
	  fields.push({
		title : "创建人",
		field : "createUser.name"
	});
	   
	fields.push({
		title : "创建日期",
		field : "createDate"
	});
	
	   
	fields.push({
		title : "计数",
		field : "sum"
	});
	
	   
	fields.push({
		title : "状态",
		field : "state"
	});
	
	   
	fields.push({
		title : "状态名称",
		field : "stateName"
	});
	
	   
	fields.push({
		title : "备注",
		field : "note"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/crm/agent/primarynew/primaryTaskNew/showPrimaryTaskNewListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/crm/agent/primarynew/primaryTaskNew/editPrimaryTaskNew.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/agent/primarynew/primaryTaskNew/editPrimaryTaskNew.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/agent/primarynew/primaryTaskNew/viewPrimaryTaskNew.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : "编号",
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "名称",
		"searchName" : 'name',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "地址",
		"searchName" : 'address',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "负责人",
		"searchName" : 'principal',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "电话",
		"searchName" : 'phone',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "管理销售",
		"searchName" : 'marketer',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "预付余额",
		"searchName" : 'money',
		"type" : "input"
	});
	
	   fields.push({
		header : '创建人',
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "创建日期",
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "计数",
		"searchName" : 'sum',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态",
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态名称",
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "备注",
		"searchName" : 'note',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

