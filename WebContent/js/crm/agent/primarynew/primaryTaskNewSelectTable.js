var primaryTaskNewTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.primaryTask.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.primaryTask.name,
	});
	
	    fields.push({
		"data":"address",
		"title":biolims.primaryTask.address,
	});
	
	    fields.push({
		"data":"principal",
		"title":biolims.primaryTask.principal,
	});
	
	    fields.push({
		"data":"phone",
		"title":biolims.primaryTask.phone,
	});
	
	    fields.push({
		"data":"marketer",
		"title":biolims.primaryTask.marketer,
	});
	
	    fields.push({
		"data":"money",
		"title":biolims.primaryTask.money,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":"创建人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.primaryTask.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.primaryTask.createDate,
	});
	
	    fields.push({
		"data":"sum",
		"title":biolims.primaryTask.sum,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.primaryTask.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.primaryTask.stateName,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.primaryTask.note,
	});
	
	var options = table(true, "","/crm/agent/primarynew/primaryTaskNew/showPrimaryTaskNewTableJson.action",
	 fields, null)
	primaryTaskNewTable = renderData($("#addPrimaryTaskNewTable"), options);
	$('#addPrimaryTaskNewTable').on('init.dt', function() {
		recoverSearchContent(primaryTaskNewTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.primaryTask.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.primaryTask.name
		});
	   fields.push({
		    "searchName":"address",
			"type":"input",
			"txt":biolims.primaryTask.address
		});
	   fields.push({
		    "searchName":"principal",
			"type":"input",
			"txt":biolims.primaryTask.principal
		});
	   fields.push({
		    "searchName":"phone",
			"type":"input",
			"txt":biolims.primaryTask.phone
		});
	   fields.push({
		    "searchName":"marketer",
			"type":"input",
			"txt":biolims.primaryTask.marketer
		});
	   fields.push({
		    "searchName":"money",
			"type":"input",
			"txt":biolims.primaryTask.money
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.primaryTask.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.primaryTask.createDate
		});
	fields.push({
			"txt": "创建日期(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建日期(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"sum",
			"type":"input",
			"txt":biolims.primaryTask.sum
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.primaryTask.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.primaryTask.stateName
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.primaryTask.note
		});
	
	fields.push({
		"type":"table",
		"table":primaryTaskNewTable
	});
	return fields;
}
