var primaryTaskNewTable;
var oldprimaryTaskNewChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.primaryTask.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.primaryTask.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"address",
		"title": biolims.primaryTask.address,
		"createdCell": function(td) {
			$(td).attr("saveName", "address");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"principal",
		"title": biolims.primaryTask.principal,
		"createdCell": function(td) {
			$(td).attr("saveName", "principal");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"phone",
		"title": biolims.primaryTask.phone,
		"createdCell": function(td) {
			$(td).attr("saveName", "phone");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"marketer",
		"title": biolims.primaryTask.marketer,
		"createdCell": function(td) {
			$(td).attr("saveName", "marketer");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"money",
		"title": biolims.primaryTask.money,
		"createdCell": function(td) {
			$(td).attr("saveName", "money");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "创建人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.primaryTask.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.primaryTask.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"sum",
		"title": biolims.primaryTask.sum,
		"createdCell": function(td) {
			$(td).attr("saveName", "sum");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.primaryTask.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.primaryTask.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.primaryTask.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"visible": false,	
		"className": "textarea"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#primaryTaskNewTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#primaryTaskNewTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#primaryTaskNewTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#primaryTaskNew_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/crm/agent/primarynew/primaryTaskNew/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 primaryTaskNewTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			savePrimaryTaskNew($("#primaryTaskNewTable"));
		}
	});
	}
	
	var primaryTaskNewOptions = 
	table(true, "","/crm/agent/primarynew/primaryTaskNew/showPrimaryTaskNewTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	primaryTaskNewTable = renderData($("#primaryTaskNewTable"), primaryTaskNewOptions);
	primaryTaskNewTable.on('draw', function() {
		oldprimaryTaskNewChangeLog = primaryTaskNewTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function savePrimaryTaskNew(ele) {
	var data = savePrimaryTaskNewjson(ele);
	var ele=$("#primaryTaskNewTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/crm/agent/primarynew/primaryTaskNew/savePrimaryTaskNewTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function savePrimaryTaskNewjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldprimaryTaskNewChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
