$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/crm/agent/primary/primaryTask/editPrimaryTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/agent/primary/primaryTask/showPrimaryTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("PrimaryTask", {
					userId : userId,
					userName : userName,
					formId : $("#primaryTask_id").val(),
					title : $("#primaryTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#primaryTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
	    var primaryAdvanceTaskItemDivData = $("#primaryAdvanceTaskItemdiv").data("primaryAdvanceTaskItemGrid");
		document.getElementById('primaryAdvanceTaskItemJson').value = commonGetModifyRecords(primaryAdvanceTaskItemDivData);
	    var primaryAgreementTaskItemDivData = $("#primaryAgreementTaskItemdiv").data("primaryAgreementTaskItemGrid");
		document.getElementById('primaryAgreementTaskItemJson').value = commonGetModifyRecords(primaryAgreementTaskItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/crm/agent/primary/primaryTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/crm/agent/primary/primaryTask/copyPrimaryTask.action?id=' + $("#primaryTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#primaryTask_id").val() + "&tableId=primaryTask");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#primaryTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.crm.agentInfo,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/crm/agent/primary/primaryTask/showPrimaryAdvanceTaskItemList.action", {
				id : $("#primaryTask_id").val()
			}, "#primaryAdvanceTaskItempage");
load("/crm/agent/primary/primaryTask/showPrimaryAgreementTaskItemList.action", {
				id : $("#primaryTask_id").val()
			}, "#primaryAgreementTaskItempage");
load("/crm/agent/primary/primaryTask/showPrimaryOrderTaskItemList.action", {
				id : $("#primaryTask_id").val()  
			}, "#primaryOrderTaskItempage"); 
load("/crm/agent/primary/primaryTask/showPrimarySampleTaskItemList.action", {
	id : $("#primary_id").val()  
			}, "#primarySampleTaskItempage"); 

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
	//评级
	function rate() {
		var win = Ext.getCmp('showrateList');
		if (win) {
			win.close();
		}
		var showrateList = new Ext.Window(
				{
					id : 'showrateList',
					modal : true,
					title : biolims.crm.selectRate,
					layout : 'fit',
					width : 780,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/dic/type/dicTypeSelect.action?flag=rate' frameborder='0' width='780' height='500' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							showrateList.close();
						}
					} ]
				});
		showrateList.show();
	}
	function setrate(id,name) {
		$("#primaryTask_rate").val(id);
		$("#primaryTask_rate_name").val(name);
		var win = Ext.getCmp('showrateList');
		if (win) {
			win.close();
		}
	}