var primarySampleTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name : 'code',
		type : "string"
	});	   
    fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
    fields.push({
		name:'productName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	
	cm.push({
		dataIndex : 'code',
		hidden : false,
		header : biolims.common.code,
		width : 20 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'patientName',
		hidden : false,
		header :biolims.common.patientName,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'sampleNum',
		hidden : false,
		header : biolims.common.sampleNum,
		width : 15 * 6
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.projectName,
		width : 25 * 6,
		sortable:true
	});	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/primary/primaryTask/showPrimarySampleTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.crm.sampleDetails;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/agent/primary/primaryTask/delSampleOrderTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				primaryOrederTaskItemGrid.getStore().commitChanges();
				primaryOrederTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//		text : biolims.common.editableColAppear,
//		handler : null
//	});
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		handler : null
//	});
//	opts.tbar.push({
//		text : biolims.common.uncheck,
//		handler : null
//	});
	primaryAdvanceTaskItemGrid=gridEditTable("primarySampleTaskItemdiv",cols,loadParam,opts);
	$("#primarySampleTaskItemdiv").data("primarySampleTaskItemGrid", primaryAdvanceTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectprimaryTaskFun(){
	var win = Ext.getCmp('selectprimaryTask');
	if (win) {win.close();}
	var selectprimaryTask= new Ext.Window({
	id:'selectprimaryTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectprimaryTask.close(); }  }]  }) });  
    selectprimaryTask.show(); }
	function setprimaryTask(rec){
		var gridGrid = $("#primarySampleTaskItemdiv").data("primarySampleTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('primary-id',rec.get('id'));
			obj.set('primary-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectprimaryTask')
		if(win){
			win.close(); 
		}
	}
