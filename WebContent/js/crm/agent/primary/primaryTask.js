var primaryTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'address',
		type:"string"
	});
	    fields.push({
		name:'principal',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'marketer-id',
		type:"string"
	});
	    fields.push({
			name:'marketer-name',
			type:"string"
		});
	    fields.push({
		name:'money',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
			name:'createUser-name',
			type:"string"
		});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'rate-id',
		type:"string"
	});
	    fields.push({
			name:'rate-name',
			type:"string"
		});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
			name:'state-name',
			type:"string"
		});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:25*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'address',
		header:biolims.common.addr,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'principal',
		header:biolims.master.personName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'marketer-id',
		header:biolims.crm.marketerId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'marketer-name',
		header:biolims.crm.marketerName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'money',
		header:biolims.crm.surplusMoney,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		hidden:true,
		width:20*10,
		sortable:true
		});
		cm.push({
			dataIndex:'createUser-name',
			header:biolims.sample.createUserName,
			
			width:20*10,
			sortable:true
			});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'rate-id',
		header:biolims.crm.rateId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'rate-name',
		header:biolims.crm.rate,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'state-id',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'state-name',
		header:biolims.common.stateName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/primary/primaryTask/showPrimaryTaskListJson.action";
	var opts={};
	opts.title=biolims.crm.agentInfo;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	primaryTaskGrid=gridTable("show_primaryTask_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/agent/primary/primaryTask/editPrimaryTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/crm/agent/primary/primaryTask/editPrimaryTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/crm/agent/primary/primaryTask/viewPrimaryTask.action?id=' + id;
}
function exportexcel() {
	primaryTaskGrid.title = biolims.common.exportList;
	var vExportContent = primaryTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(primaryTaskGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
