function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/crm/agent/primary/primaryTask/editPrimaryTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/crm/agent/primary/primaryTask/viewPrimaryTask.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : biolims.common.id,
		dataIndex : 'id',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.designation,
		dataIndex : 'name',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.addr,
		dataIndex : 'address',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.master.personName,
		dataIndex : 'principal',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.phone,
		dataIndex : 'phone',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.crm.marketerName,
		dataIndex : 'marketer',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.crm.surplusMoney,
		dataIndex : 'money',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.sample.createUserName,
		dataIndex : 'createUser-name',
		width:20*6,
		hidden:false
	}, 
	   
	{
		header : biolims.sample.createDate,
		dataIndex : 'createDate',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.crm.sum,
		dataIndex : 'sum',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.state,
		dataIndex : 'state',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.stateName,
		dataIndex : 'stateName',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.note,
		dataIndex : 'note',
		width:50*6,
		hidden:true
	}, 
	
			
			
			
			{
				header : biolims.common.upId,
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#primaryTaskTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
