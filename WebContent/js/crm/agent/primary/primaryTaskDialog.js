var primaryTaskDialogGrid;
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'address',
		type:"string"
	});
	    fields.push({
		name:'principal',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'marketer',
		type:"string"
	});
	    fields.push({
		name:'money',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'address',
		header:biolims.common.addr,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'principal',
		header:biolims.master.personName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'marketer',
		header:biolims.crm.marketerName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'money',
		header:biolims.crm.surplusMoney,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state-id',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state-name',
		header:biolims.common.stateName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/primary/primaryTask/showDialogPrimaryTaskListJson.action";
	var opts={};
	opts.title=biolims.crm.agentInfo;
	opts.height=document.body.clientHeight-150;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setPrimaryTaskFun(rec);
		setPrimaryTaskFun();
	};
	primaryTaskDialogGrid=gridTable("show_dialog_primaryTask_div",cols,loadParam,opts);
	$("#show_dialog_primaryTask_div").data("primaryTaskDialogGrid", primaryTaskDialogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(primaryTaskDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
