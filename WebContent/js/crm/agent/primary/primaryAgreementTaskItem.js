var primaryAgreementTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'primaryTask-id',
		type:"string"
	});
	    fields.push({
		name:'primaryTask-name',
		type:"string"
	});
	   fields.push({
		name:'startDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'endDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
			name:'productName',
			type:"string"
		});
	   fields.push({
		name:'money',
		type:"string"
	});
	   fields.push({
		name:'discount',
		type:"string"
	});
	   fields.push({
		name:'price',
		type:"string"
	});
	   fields.push({
		name:'deduct',
		type:"string"
	});
	   fields.push({
		name:'isvalid',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'primaryTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
	});
	cm.push({
		dataIndex:'primaryTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'startDate',
		hidden : false,
		header:biolims.crm.startDate,
		width:20*6,
		sortable : true,
		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'endDate',
		hidden : false,
		header:biolims.crm.endDate,
		width:20*6,
		sortable : true,
		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:biolims.master.productId,
		width:20*6,
		hidden:true,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.master.product,
		width:20*6,
		sortable : true,
	});
	cm.push({
		dataIndex:'money',
		hidden : false,
		header:biolims.crm.money,
		width:20*6,
		sortable : true,
	});
	cm.push({
		dataIndex:'discount',
		hidden : false,
		header:biolims.crm.discount,
		width:20*6,
		sortable : true,
	});
	cm.push({
		dataIndex:'price',
		hidden : false,
		header:biolims.crm.productFee,
		width:20*6,
		sortable : true,
	});
	cm.push({
		dataIndex:'deduct',
		hidden : false,
		header:biolims.crm.deduct,
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.master.valid], [ '0', biolims.master.invalid ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	
	
	cm.push({
		dataIndex:'isvalid',
		hidden : false,
		header:biolims.analysis.validState,
		width:20*6,
		sortable : true,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/primary/primaryTask/showPrimaryAgreementTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.crm.protocolInformationDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/agent/primary/primaryTask/delPrimaryAgreementTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				primaryAgreementTaskItemGrid.getStore().commitChanges();
				primaryAgreementTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.batchIsValid,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_isvalid_div"), biolims.common.batchIsValid, null, {
				"Confirm": function() {
					var records = primaryAgreementTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isvalid = $("#isvalid").val();
						primaryAgreementTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isvalid", isvalid);
						});
						primaryAgreementTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	primaryAgreementTaskItemGrid=gridEditTable("primaryAgreementTaskItemdiv",cols,loadParam,opts);
	$("#primaryAgreementTaskItemdiv").data("primaryAgreementTaskItemGrid", primaryAgreementTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectprimaryTaskFun(){
	var win = Ext.getCmp('selectprimaryTask');
	if (win) {win.close();}
	var selectprimaryTask= new Ext.Window({
	id:'selectprimaryTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectprimaryTask.close(); }  }]  }) });  
    selectprimaryTask.show(); }
	function setprimaryTask(rec){
		var gridGrid = $("#primaryAgreementTaskItemdiv").data("primaryAgreementTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('primaryTask-id',rec.get('id'));
			obj.set('primaryTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectprimaryTask')
		if(win){
			win.close();
		}
	}
