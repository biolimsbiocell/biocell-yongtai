var primaryAdvanceTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'primaryTask-id',
		type:"string"
	});
	    fields.push({
		name:'primaryTask-name',
		type:"string"
	});
	   fields.push({
		name:'advanceMoney',
		type:"string"
	});
	   fields.push({
		name:'advanceDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'actualMoney',
		type:"string"
	});
	   fields.push({
		name:'surplusMoney',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'primaryTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
	});
	cm.push({
		dataIndex:'primaryTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'advanceMoney',
		hidden : false,
		header:biolims.crm.advanceMoney,
		width:20*6,
		sortable : true,
	});
	cm.push({
		dataIndex:'advanceDate',
		hidden : false,
		header:biolims.crm.advanceDate,
		width:20*6,
		sortable : true,
		
		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'actualMoney',
		hidden : true,
		header:biolims.crm.actualMoney,
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'surplusMoney',
		hidden : true,
		header:biolims.crm.surplusMoney,
		sortable : true,
		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
	});   
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/primary/primaryTask/showPrimaryAdvanceTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.crm.prepaidInfoDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/agent/primary/primaryTask/delPrimaryAdvanceTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				primaryAdvanceTaskItemGrid.getStore().commitChanges();
				primaryAdvanceTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	primaryAdvanceTaskItemGrid=gridEditTable("primaryAdvanceTaskItemdiv",cols,loadParam,opts);
	$("#primaryAdvanceTaskItemdiv").data("primaryAdvanceTaskItemGrid", primaryAdvanceTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectprimaryTaskFun(){
	var win = Ext.getCmp('selectprimaryTask');
	if (win) {win.close();}
	var selectprimaryTask= new Ext.Window({
	id:'selectprimaryTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectprimaryTask.close(); }  }]  }) });  
    selectprimaryTask.show(); }
	function setprimaryTask(rec){
		var gridGrid = $("#primaryAdvanceTaskItemdiv").data("primaryAdvanceTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('primaryTask-id',rec.get('id'));
			obj.set('primaryTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectprimaryTask')
		if(win){
			win.close();
		} 
		
	}
