var primaryOrderTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'receivedDate',
		type:"string"
	});
	  
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.orderCode,
		width:20*6,	
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:20*6,
		
		sortable:true
	});	
	cm.push({
		dataIndex:'receivedDate',
		header:biolims.common.receiveDate,
		width:20*6,
		
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/primary/primaryTask/showPrimaryOrderTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.goods.orderDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/agent/primary/primaryTask/delPrimaryOrderTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				primaryOrederTaskItemGrid.getStore().commitChanges();
				primaryOrederTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//		text : biolims.common.editableColAppear,
//		handler : null
//	});
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		handler : null
//	});
//	opts.tbar.push({
//		text : biolims.common.uncheck,
//		handler : null
//	});
	primaryAdvanceTaskItemGrid=gridEditTable("primaryOrederTaskItemdiv",cols,loadParam,opts);
	$("#primaryOrederTaskItemdiv").data("primaryOrederTaskItemGrid", primaryAdvanceTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectprimaryTaskFun(){
	var win = Ext.getCmp('selectprimaryTask');
	if (win) {win.close();}
	var selectprimaryTask= new Ext.Window({
	id:'selectprimaryTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectprimaryTask.close(); }  }]  }) });  
    selectprimaryTask.show(); }
	function setprimaryTask(rec){
		var gridGrid = $("#primaryOrederTaskItemdiv").data("primaryOrederTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('primary-id',rec.get('id'));
			obj.set('primary-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectprimaryTask')
		if(win){
			win.close(); 
		}
	}
