$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/crm/agent/advance/advanceTask/editAdvanceTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/agent/advance/advanceTask/showAdvanceTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("AdvanceTask", {
					userId : userId,
					userName : userName,
					formId : $("#advanceTask_id").val(),
					title : $("#advanceTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#advanceTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
	    var advanceTaskItemDivData = $("#advanceTaskItemdiv").data("advanceTaskItemGrid");
		document.getElementById('advanceTaskItemJson').value = commonGetModifyRecords(advanceTaskItemDivData);
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/agent/advance/advanceTask/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
					msg : biolims.common.beingProcessed,
					removeMask : true// 完成后移除
				});
		loadMarsk.show();	
		}
	}		
function editCopy() {
	window.location = window.ctx + '/crm/agent/advance/advanceTask/copyAdvanceTask.action?id=' + $("#advanceTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#advanceTask_id").val() + "&tableId=advanceTask");
//}
$("#toolbarbutton_status").click(function(){
	var selRecord = advanceTaskItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		if(selRecord.getAt(j).get("advance-id")==""){
			message(biolims.crm.pleaseAgent);
			return;
		}
	}
	if(advanceTaskItemGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	commonChangeState("formId=" + $("#advanceTask_id").val() + "&tableId=AdvanceTask");
	

});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#advanceTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.crm.agentPrepayApplication,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/crm/agent/advance/advanceTask/showAdvanceTaskItemList.action", {
				id : $("#advanceTask_id").val()
			}, "#advanceTaskItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);