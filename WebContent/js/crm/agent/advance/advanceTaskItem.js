var advanceTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	    fields.push({
		name:'advanceTask-id',
		type:"string"
	});
	    fields.push({
		name:'advanceTask-name',
		type:"string"
	});
	   fields.push({
		name:'advance-id',
		type:"string"
	});
	   fields.push({
			name:'advance-name',
			type:"string"
		});
	   fields.push({
		name:'money',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:50*6,
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'advanceTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,   //编号
		width:20*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'advanceTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	cm.push({
		dataIndex:'advance-id',
		hidden : false,
		header:biolims.common.primaryId,
		width:15*10
	});
	var testAdvance =new Ext.form.TextField({
        allowBlank: false
	});
	testAdvance.on('focus', function() {
		loadTestAdvance();
	});
	cm.push({
		dataIndex:'advance-name',
		header:biolims.common.primary,
		width:15*10,
		editor : testAdvance
	});
	
	cm.push({
		dataIndex:'money',                        //申请金额
		hidden : false,
		header:biolims.crm.applyAmount,
		width:30*6,
		
		editor : new Ext.form.NumberField({
			allowBlank : true,
//			decimalPrecision:2
		})
	});
	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width: 30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/agent/advance/advanceTask/showAdvanceTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.crm.prepayApplicationDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/agent/advance/advanceTask/delAdvanceTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				advanceTaskItemGrid.getStore().commitChanges();
				advanceTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.primary,
		handler : loadTestAdvance
	});
	advanceTaskItemGrid=gridEditTable("advanceTaskItemdiv",cols,loadParam,opts);
	$("#advanceTaskItemdiv").data("advanceTaskItemGrid", advanceTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectadvanceTaskFun(){
	var win = Ext.getCmp('selectadvanceTask');
	if (win) {win.close();}
	var selectadvanceTask= new Ext.Window({
	id:'selectadvanceTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectadvanceTask.close(); }  }]  }) });  
    selectadvanceTask.show(); }
	function setadvanceTask(rec){
		var gridGrid = $("#advanceTaskItemdiv").data("advanceTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('advanceTask-id',rec.get('id'));
			obj.set('advanceTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectadvanceTask')
		if(win){
			win.close();
		}
	}
	var loadAdvance;	
	//查询代理商
	function loadTestAdvance(){
		var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadAdvance=loadDialogPage(null, biolims.common.primary, "/crm/agent/primary/primaryTask/primaryTaskSelect.action", {
			"Confirm" : function() {
				var operGrid = primaryTaskDialogGrid;
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = advanceTaskItemGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						$.each(records, function(a, b) {
							b.set("advance-id", obj.get("id"));
							b.set("advance-name", obj.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}
	function setPrimaryTaskFun(){
		var operGrid = $("#show_dialog_primaryTask_div").data("primaryTaskDialogGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		var records = advanceTaskItemGrid.getSelectRecord();
		if (selectRecord.length > 0) {
			$.each(selectRecord, function(i, obj) {
				$.each(records, function(a, b) {
					b.set("advance-id", obj.get("id"));
					b.set("advance-name", obj.get("name"));
				});
			});
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		loadAdvance.dialog("close");

	}

