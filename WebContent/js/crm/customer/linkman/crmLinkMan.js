var crmLinkManGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'isZhuYaoLinkMan',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'sex-id',
		type:"string"
	});
	    fields.push({
		name:'sex-name',
		type:"string"
	});
	    fields.push({
		name:'trailId-id',
		type:"string"
	});
	    fields.push({
		name:'trailId-name',
		type:"string"
	});
	    fields.push({
		name:'mobile',
		type:"string"
	});
	    fields.push({
		name:'email',
		type:"string"
	});
	    fields.push({
		name:'customerId-id',
		type:"string"
	});
	    fields.push({
		name:'customerId-name',
		type:"string"
	});
	    fields.push({
		name:'telephone',
		type:"string"
	});
	    fields.push({
		name:'deptName',
		type:"string"
	});
	    fields.push({
		name:'marketActId-id',
		type:"string"
	});
	    fields.push({
		name:'marketActId-name',
		type:"string"
	});
	    fields.push({
		name:'fax',
		type:"string"
	});
	    fields.push({
		name:'job',
		type:"string"
	});
	    fields.push({
		name:'isTel',
		type:"string"
	});
	    fields.push({
		name:'isFax',
		type:"string"
	});
	    fields.push({
		name:'isMail',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'isPrivate',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-id',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-name',
		type:"string"
	});
	    fields.push({
		name:'dutyDept-id',
		type:"string"
	});
	    fields.push({
		name:'dutyDept-name',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'isZhuYaoLinkMan',
		header:'主要联系人',
		width:15*10,
		renderer : function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "1") {
				return "<span>"+biolims.common.yes+"</span>";
			} else if (value == "0") {
				return biolims.common.no;
			}
		}
		});
	cm.push({
		dataIndex:'sex-id',
		hidden:true,
		header:'性别ID',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'sex-name',
		header:biolims.common.gender,
		width:15*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'trailId-id',
//		hidden:true,
//		header:'线索来源ID',
//		width:15*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'trailId-name',
//		header:'线索来源',
//		width:15*10,
//		sortable:true
//		});
	cm.push({
		dataIndex:'mobile',
		header:'手机',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'email',
		header:'email',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'customerId-id',
		hidden:true,
		header:'客户来源ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'customerId-name',
		header:'客户来源',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'telephone',
		header:'电话',
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'deptName',
		header:'所属部门',
		width:10*10,
		sortable:true
	});
//		cm.push({
//		dataIndex:'marketActId-id',
//		hidden:true,
//		header:'市场活动来源ID',
//		width:15*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'marketActId-name',
//		header:'市场活动来源',
//		width:15*10,
//		sortable:true
//		});
	cm.push({
		dataIndex:'fax',
		header:'传真',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'job',
		header:'职务',
		width:15*10,
		sortable:true
	});
//	var isTelstore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '是' ], [ '0', '否' ]]
//	});
	
//	var isTelComboxFun = new Ext.form.ComboBox({
//		store : isTelstore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		emptyText : '',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'isTel',
//		header:'请勿致电',
//		width:15*10,
//		renderer: Ext.util.Format.comboRenderer(isTelComboxFun),				
//		sortable:true
//	});
//	var isFaxstore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '是' ], [ '0', '否' ]]
//	});
	
//	var isFaxComboxFun = new Ext.form.ComboBox({
//		store : isFaxstore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		emptyText : '',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'isFax',
//		header:'请勿传真',
//		width:15*10,
//		renderer: Ext.util.Format.comboRenderer(isFaxComboxFun),				
//		sortable:true
//	});
//	var isMailstore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '是' ], [ '0', '否' ]]
//	});
//	
//	var isMailComboxFun = new Ext.form.ComboBox({
//		store : isMailstore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		emptyText : '',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'isMail',
//		header:'请勿邮件',
//		width:15*10,
//		renderer: Ext.util.Format.comboRenderer(isMailComboxFun),				
//		sortable:true
//	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:12*10,
		sortable:true
	});
//	var isPrivatestore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '是' ], [ '0', '否' ]]
//	});
//	
//	var isPrivateComboxFun = new Ext.form.ComboBox({
//		store : isPrivatestore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		emptyText : '',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'isPrivate',
//		header:'是否私有',
//		width:15*10,
//		renderer: Ext.util.Format.comboRenderer(isPrivateComboxFun),				
//		sortable:true
//	});
		cm.push({
		dataIndex:'dutyUser-id',
		hidden:true,
		header:biolims.master.personId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'dutyUser-name',
		header:biolims.master.personName,
		width:10*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'dutyDept-id',
//		hidden:true,
//		header:'负责部门ID',
//		width:10*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'dutyDept-name',
//		header:'负责部门',
//		width:10*10,
//		sortable:true
//		});
		cm.push({
		dataIndex:'state-id',
		hidden:true,
		header:'状态ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'state-name',
		header:biolims.common.stateName,
		width:15*10,
		sortable:true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/linkman/showCrmLinkManListJson.action";
	var opts={};
	opts.title=biolims.common.linkmanName;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	crmLinkManGrid=gridTable("show_crmLinkMan_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/customer/linkman/editCrmLinkMan.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/crm/customer/linkman/editCrmLinkMan.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/crm/customer/linkman/viewCrmLinkMan.action?id=' + id;
}
function exportexcel() {
	crmLinkManGrid.title = biolims.common.exportList;
	var vExportContent = crmLinkManGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 300;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(crmLinkManGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);

	});

});