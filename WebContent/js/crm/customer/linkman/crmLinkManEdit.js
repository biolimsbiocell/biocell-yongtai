$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/crm/customer/linkman/editCrmLinkMan.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/customer/linkman/showCrmLinkManList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var crmLinkManItemDivData = $("#crmLinkManItemdiv").data("crmLinkManItemGrid");
		document.getElementById('crmLinkManItemJson').value = commonGetModifyRecords(crmLinkManItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/customer/linkman/save.action";
		$("#toolbarbutton_save").hide();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/crm/customer/linkman/copyCrmLinkMan.action?id=' + $("#crmLinkMan_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#crmLinkMan_id").val() + "&tableId=crmLinkMan");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmLinkMan_id").val());
	nsc.push("编码不能为空！");
	fs.push($("#crmLinkMan_createUser").val());
	nsc.push("创建人不能为空！");
	fs.push($("#crmLinkMan_createDate").val());
	nsc.push("创建日期不能为空！");
	fs.push($("#crmLinkMan_customerId").val());
	nsc.push("所属客户不能为空！");
	fs.push($("#crmLinkMan_state").val());
	nsc.push("状态不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-10,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'联系人',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/crm/customer/linkman/showCrmLinkManItemList.action", {
				id : $("#crmLinkMan_id").val()
			}, "#crmLinkManItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
function showCustomer() {
	var win = Ext.getCmp('showCustomer');
	if (win) {
		win.close();
	}
	var showCustomer = new Ext.Window(
			{
				id : 'showCustomer',
				modal : true,
				title : biolims.user.selectCustomers,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						showCustomer.close();
					}
				} ]
			});
	showCustomer.show();
}
function setCrmCustomerFun(rec){
	document.getElementById('crmLinkMan_customerId').value=rec.get('id');
	document.getElementById('crmLinkMan_customerId_name').value=rec.get('name');
	document.getElementById('crmLinkMan_dutyUser').value=rec.get('dutyManId-id');
	document.getElementById('crmLinkMan_dutyUser_name').value=rec.get('dutyManId-name');
	records = crmLinkManItemGrid.getAllRecord();
	$.each(records, function(i, obj) {
		obj.set("dutyUser-id", rec.get('dutyManId-id'));
		obj.set("dutyUser-name", rec.get('dutyManId-name'));
	});
	crmLinkManItemGrid.startEditing(0, 0);
	var win = Ext.getCmp('showCustomer')
	if(win){win.close();}
	}
