var crmCustomerDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'linkManId-id',
		type:"string"
	});
	    fields.push({
		name:'linkManId-name',
		type:"string"
	});
	    fields.push({
		name:'dutyManId-id',
		type:"string"
	});
	    fields.push({
		name:'dutyManId-name',
		type:"string"
	});
	    fields.push({
		name:'dutyDeptId-id',
		type:"string"
	});
	    fields.push({
		name:'dutyDeptId-name',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'upCustomerId-id',
		type:"string"
	});
	    fields.push({
		name:'upCustomerId-name',
		type:"string"
	});
	    fields.push({
		name:'trailId-id',
		type:"string"
	});
	    fields.push({
		name:'trailId-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'capital',
		type:"string"
	});
	    fields.push({
			name:'street',
			type:"string"
		});
	    fields.push({
		name:'currencyTypeId-id',
		type:"string"
	});
	    fields.push({
		name:'currencyTypeId-name',
		type:"string"
	});
	    fields.push({
		name:'orgCode',
		type:"string"
	});
	    fields.push({
		name:'industry',
		type:"string"
	});
	    fields.push({
		name:'kind-id',
		type:"string"
	});
	    fields.push({
		name:'kind-name',
		type:"string"
	});
	    fields.push({
		name:'aptitude1',
		type:"string"
	});
	    fields.push({
		name:'aptitude2',
		type:"string"
	});
	    fields.push({
		name:'aptitude3-id',
		type:"string"
	});
	    fields.push({
		name:'aptitude3-name',
		type:"string"
	});
	    fields.push({
		name:'bank',
		type:"string"
	});
	    fields.push({
		name:'account',
		type:"string"
	});
	    fields.push({
		name:'bankCode',
		type:"string"
	});
	    fields.push({
			name:'ks',
			type:"string"
		});
	    fields.push({
			name:'postcode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.sample.customerId,
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:200,
		sortable:true
	});
	cm.push({
		dataIndex:'street',
		header:biolims.common.addr,
		width:200,
		sortable:true
	});
	cm.push({
		dataIndex:'ks',
		header:biolims.common.phone,
		width:200,
		sortable:true
	});
	cm.push({
		dataIndex:'postcode',
		header:biolims.sample.customerPostcode,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'dutyManId-name',
		header:biolims.master.personName,
		width:100,
		sortable:true
		});
		cm.push({
		dataIndex:'dutyDeptId-name',
		header:biolims.crm.dutyDeptId,
		width:150,
		sortable:true
		});
	
		cm.push({
			dataIndex:'type-name',
			header:biolims.storage.studyType,
			width:15*10,
			sortable:true
			});
	
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'state-name',
		header:biolims.common.stateName,
		width:15*10,
		sortable:true
		});
	
		
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:12*10,
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/customer/showCrmCustomerListJson.action";
	var opts={};
	opts.title=biolims.crm.customerInfo;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){

		$('#selectId').val(id);
		setPValue(rec);
	};
	crmCustomerDialogGrid=gridTable("show_dialog_crmCustomer_div",cols,loadParam,opts);
})
function showCustomerFun() {
	var win = Ext.getCmp('showCustomerWin');
	if (win) {
		win.close();
	}
	var showCustomerWin = new Ext.Window(
			{
				id : 'showCustomerWin',
				modal : true,
				title : biolims.storage.selectClassification,
				layout : 'fit',
				width : document.body.clientWidth / 2,
				height : document.body.clientHeight / 1.5,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=cclx' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showCustomerWin.close();
					}
				} ]
			});
	
	showCustomerWin.show();
}
function setcclx(id,name){
	 document.getElementById("crmCustomer_type").value = id;
	document.getElementById("crmCustomer_type_name").value = name;
	var win = Ext.getCmp('showCustomerWin')
	if(win){win.close();}
	}
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(crmCustomerDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}

function nd(){
	window.open(window.ctx + '/crm/customer/customer/editCrmCustomer.action','','');
}