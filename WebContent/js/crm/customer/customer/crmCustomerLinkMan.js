var crmCustomerLinkManGrid;
$(function(){
	var cols={};
//	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'email',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'telephone',
		type:"string"
	});
	    fields.push({
		name:'sex',
		type:"string"
    });
	    fields.push({
		name:'mobile',
		type:"string"
	});
	    fields.push({
		name:'customerId-id',
		type:"string"
	});
	    fields.push({
		name:'customerId-name',
		type:"string"
	});
	    fields.push({
		name:'deptName',
		type:"string"
	});
	    fields.push({
		name:'fax',
		type:"string"
	});
	    fields.push({
		name:'job',
		type:"string"
	});
	    fields.push({
		name:'isTel',
		type:"string"
	});
	    fields.push({
		name:'isFax',
		type:"string"
	});
	    fields.push({
		name:'isMail',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'isPrivate',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-id',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-name',
		type:"string"
	});
	    fields.push({
		name:'dutyDept-id',
		type:"string"
	});
	    fields.push({
		name:'dutyDept-name',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'isZhuYaoLinkMan',
		type:"string"
	});
	    
	    /*fields.push({
		name:'linkMan_Post',
		type:"string"
	});*/
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.linkmanId,
		width:15*10
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.linkmanName,
		width:15*10
	});
	cm.push({
		dataIndex:'isZhuYaoLinkMan',
		hidden : false,
		header:biolims.crm.mainLinkman,
		width:15*10,
		renderer : function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "1") {
				return "<span>"+biolims.common.yes+"</span>";
			} else if (value == "0") {
				return biolims.common.no;
			}
		}
	});
	cm.push({
		dataIndex:'telephone',
		hidden : false,
		header:biolims.common.linkMan_telNumber,
		width:15*10
	});
	/*cm.push({
		dataIndex:'linkMan_Post',
		hidden : false,
		header:'联系人职务',
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'email',
		hidden : false,
		header:biolims.common.linkMan_mail,
		width:10*10
	});
	cm.push({
		dataIndex:'sex',
		header:biolims.common.gender,
		width:10*10
	});
	cm.push({
		dataIndex:'mobile',
		hidden : false,
		header:biolims.common.phone,
		width:10*10
	});
	cm.push({
		dataIndex:'customerId-id',
		hidden : true,
		header:biolims.sample.customerId,
		width:10*10
	});
	cm.push({
		dataIndex:'customerId-name',
		hidden : false,
		header:biolims.sample.customerName,
		width:10*10
	});
	cm.push({
		dataIndex:'deptName',
		hidden : false,
		header:biolims.equipment.departmentName,
		width:10*10
	});
	cm.push({
		dataIndex:'fax',
		hidden : false,
		header:biolims.purchase.fax,
		width:10*10
	});
	cm.push({
		dataIndex:'job',
		hidden : false,
		header:biolims.crm.job,
		width:10*10
	});
	/*cm.push({
		dataIndex:'isTel',
		hidden : false,
		header:'请勿致电',
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isFax',
		hidden : false,
		header:'请勿传真',
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isMail',
		hidden : false,
		header:'请勿邮件',
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'createUser-id',
		hidden : true,
		header:biolims.sample.createUserId,
		width:10*10
	});
	cm.push({
		dataIndex:'createUser-name',
		hidden : false,
		header:biolims.sample.createUserName,
		width:10*10
	});
	cm.push({
		dataIndex:'createDate',
		hidden : false,
		header:biolims.common.linkMan_mail,
		width:10*10
	});
	/*cm.push({
		dataIndex:'isPrivate',
		hidden : false,
		header:'是否私有',
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'dutyUser-id',
		hidden : true,
		header:biolims.master.personId,
		width:10*10
	});
	cm.push({
		dataIndex:'dutyUser-name',
		hidden : false,
		header:biolims.master.personName,
		width:10*10
	});
	cm.push({
		dataIndex:'dutyDept-id',
		hidden : true,
		header:biolims.crm.dutyDeptId,
		width:10*10
	});
	cm.push({
		dataIndex:'dutyDept-name',
		hidden : false,
		header:biolims.crm.dutyDeptName,
		width:10*10
	});
	cm.push({
		dataIndex:'state-id',
		hidden : true,
		header:'状态编号',
		width:10*10
	});
	cm.push({
		dataIndex:'state-name',
		hidden : false,
		header:biolims.common.stateName,
		width:10*10
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/customer/showCrmCustomerLinkManListJson.action?id="+ $("#crmCustomer_id").val();
	var opts={};
	opts.title=biolims.common.linkmanName;
	opts.height =  document.body.clientHeight-330;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/customer/customer/delCrmCustomerLinkMan.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	/*opts.tbar.push({
			text : '选择所属客户',
			handler : selectcrmCustomerFun
		});
    
	opts.tbar.push({
			text : '选择联系人编号',
			handler : selectlinkManIdFun
		});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});*/
	crmCustomerLinkManGrid=gridEditTable("crmCustomerLinkMandiv",cols,loadParam,opts);
	$("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid", crmCustomerLinkManGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmCustomerFun(){
	var win = Ext.getCmp('selectcrmCustomer');
	if (win) {win.close();}
	var selectcrmCustomer= new Ext.Window({
	id:'selectcrmCustomer',modal:true,title:biolims.master.selectCustomer,layout:'fit',width:900,height:700,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmCustomerSelect.action?flag=crmCustomer' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmCustomer.close(); }  }]  });     selectcrmCustomer.show(); }
	function setcrmCustomer(id,name){
		var gridGrid = $("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmCustomer-id',id);
			obj.set('crmCustomer-name',name);
		});
		var win = Ext.getCmp('selectcrmCustomer')
		if(win){
			win.close();
		}
	}
	
function selectlinkManIdFun(){
	var win = Ext.getCmp('selectlinkManId');
	if (win) {win.close();}
	var selectlinkManId= new Ext.Window({
	id:'selectlinkManId',modal:true,title:biolims.master.selectLinkmanId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmLinkManSelect.action?flag=linkManId' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectlinkManId.close(); }  }]  });     selectlinkManId.show(); }
	function setlinkManId(id,name){
		var gridGrid = $("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('linkManId-id',id);
			obj.set('linkManId-name',name);
		});
		var win = Ext.getCmp('selectlinkManId')
		if(win){
			win.close();
		}
	}
	
