var CrmCustomerTable;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.user.eduName1,
	});
	colOpts.push({
		"data" : "dutyManId-name",
		"title" : biolims.master.personName,
	});
	colOpts.push({
		"data" : "street",
		"title" : "地址",
	});
	var tbarOpts = [];
	tbarOpts.push({
		text:"搜索",
		action: search,
	});
	var addCrmCustomerOptions = table(false, null,
			'/crm/customer/customer/showDialogCrmCustomerTableJson.action',
			colOpts, tbarOpts)
			CrmCustomerTable = renderData($("#addcrmCustomerTable"), addCrmCustomerOptions);
	$("#addcrmCustomerTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				//$("#addcrmCustomerTable_wrapper .dt-buttons").empty();
				$('#addcrmCustomerTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addcrmCustomerTable tbody tr");
			CrmCustomerTable.ajax.reload();
			CrmCustomerTable.on('draw', function() {
				trs = $("#addcrmCustomerTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})
function searchOptions() {
	return [{
			"txt": "编码",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "名称",
			"type": "input",
			"searchName": "name"
		},
		{
			"txt": "负责人",
			"type": "input",
			"searchName": "dutyManId-name"
		},
		{
			"type": "table",
			"table": CrmCustomerTable
		}
	];
}
