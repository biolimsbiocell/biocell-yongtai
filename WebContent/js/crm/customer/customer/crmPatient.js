var crmPatientTable;
var oldcrmPatientChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#cC1_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.crmPatient.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"lastName",
		"title": biolims.crmPatient.lastName,
		"createdCell": function(td) {
			$(td).attr("saveName", "lastName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"firstName",
		"title": biolims.crmPatient.firstName,
		"createdCell": function(td) {
			$(td).attr("saveName", "firstName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.crmPatient.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"gender",
		"title": biolims.crmPatient.gender,
		"createdCell": function(td) {
			$(td).attr("saveName", "gender");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"placeOfBirth",
		"title": biolims.crmPatient.placeOfBirth,
		"createdCell": function(td) {
			$(td).attr("saveName", "placeOfBirth");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"dateOfBirth",
		"title": biolims.crmPatient.dateOfBirth,
		"createdCell": function(td) {
			$(td).attr("saveName", "dateOfBirth");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"age",
		"title": biolims.crmPatient.age,
		"createdCell": function(td) {
			$(td).attr("saveName", "age");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"adultTeenager",
		"title": biolims.crmPatient.adultTeenager,
		"createdCell": function(td) {
			$(td).attr("saveName", "adultTeenager");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"maritalStatus",
		"title": biolims.crmPatient.maritalStatus,
		"createdCell": function(td) {
			$(td).attr("saveName", "maritalStatus");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"jc",
		"title": biolims.crmPatient.jc,
		"createdCell": function(td) {
			$(td).attr("saveName", "jc");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"occupation",
		"title": biolims.crmPatient.occupation,
		"createdCell": function(td) {
			$(td).attr("saveName", "occupation");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"consentReceived",
		"title": biolims.crmPatient.consentReceived,
		"createdCell": function(td) {
			$(td).attr("saveName", "consentReceived");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "patientStatus-id",
		"title": "病人状态ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "patientStatus-id");
		}
	});
	colOpts.push( {
		"data": "patientStatus-name",
		"title": biolims.crmPatient.patientStatus,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "patientStatus-name");
			$(td).attr("patientStatus-id", rowData['patientStatusr-id']);
		}
	});
	   colOpts.push({
		"data":"address",
		"title": biolims.crmPatient.address,
		"createdCell": function(td) {
			$(td).attr("saveName", "address");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"telphoneNumber1",
		"title": biolims.crmPatient.telphoneNumber1,
		"createdCell": function(td) {
			$(td).attr("saveName", "telphoneNumber1");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"telphoneNumber2",
		"title": biolims.crmPatient.telphoneNumber2,
		"createdCell": function(td) {
			$(td).attr("saveName", "telphoneNumber2");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"telphoneNumber3",
		"title": biolims.crmPatient.telphoneNumber3,
		"createdCell": function(td) {
			$(td).attr("saveName", "telphoneNumber3");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "创建人姓名ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.crmPatient.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUserr-id']);
		}
	});
	colOpts.push( {
		"data": "crmCustomerId-id",
		"title": "客户单位编码ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "crmCustomerId-id");
		}
	});
	colOpts.push( {
		"data": "crmCustomerId-name",
		"title": biolims.crmPatient.crmCustomerId,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "crmCustomerId-name");
			$(td).attr("crmCustomerId-id", rowData['crmCustomerIdr-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.crmPatient.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#crmPatientTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#crmPatientTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#crmPatientTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#crmPatient_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/crm/customer/customer/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 crmPatientTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveCrmPatient($("#crmPatientTable"));
		}
	});
	}
	
	var crmPatientOptions = table(true,
		id,
		'/crm/customer/customer/showCrmPatientTableJson.action', colOpts, tbarOpts)
	crmPatientTable = renderData($("#crmPatientTable"), crmPatientOptions);
	crmPatientTable.on('draw', function() {
		oldcrmPatientChangeLog = crmPatientTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveCrmPatient(ele) {
	var data = saveCrmPatientjson(ele);
	var ele=$("#crmPatientTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/crm/customer/customer/saveCrmPatientTable.action',
		data: {
			id: $("#cC1_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveCrmPatientjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "patientStatus-name") {
				json["patientStatus-id"] = $(tds[j]).attr("patientStatus-id");
				continue;
			}
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			
			if(k == "crmCustomerId-name") {
				json["crmCustomerId-id"] = $(tds[j]).attr("crmCustomerId-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldcrmPatientChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
