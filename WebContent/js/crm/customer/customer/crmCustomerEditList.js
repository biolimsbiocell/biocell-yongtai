var crmCustomerTable;
var oldcrmCustomerChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	colOpts.push( {
		"data": "createUser-id",
		"title": biolims.sample.createUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.crmCustomer.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.crmCustomer.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"ks",
		"title": biolims.tStorageReagentBuySerial.supplierLinkTel,
		"createdCell": function(td) {
			$(td).attr("saveName", "ks");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "district-id",
		"title": biolims.crm.districtId,
		"createdCell": function(td) {
			$(td).attr("saveName", "district-id");
		}
	});
	colOpts.push( {
		"data": "district-name",
		"title": biolims.crmCustomer.district,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "district-name");
			$(td).attr("district-id", rowData['district-id']);
		}
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.crmCustomer.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "type-id",
		"title": biolims.purchase.typeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "type-id");
		}
	});
	colOpts.push( {
		"data": "type-name",
		"title": biolims.crmCustomer.type,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "type-name");
			$(td).attr("type-id", rowData['type-id']);
		}
	});
	   colOpts.push({
		"data":"street",
		"title": biolims.crmCustomer.street,
		"createdCell": function(td) {
			$(td).attr("saveName", "street");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"postcode",
		"title": biolims.crmCustomer.postcode,
		"createdCell": function(td) {
			$(td).attr("saveName", "postcode");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"email",
		"title": biolims.crmCustomer.email,
		"createdCell": function(td) {
			$(td).attr("saveName", "email");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"yjly",
		"title": biolims.crmCustomer.yjly,
		"createdCell": function(td) {
			$(td).attr("saveName", "yjly");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "dutyManId-id",
		"title": biolims.crmCustomer.dutyManId+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "dutyManId-id");
		}
	});
	colOpts.push( {
		"data": "dutyManId-name",
		"title": biolims.crmCustomer.dutyManId,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dutyManId-name");
			$(td).attr("dutyManId-id", rowData['dutyManId-id']);
		}
	});
	colOpts.push( {
		"data": "state-id",
		"title": biolims.crmCustomer.state+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "state-id");
		}
	});
	colOpts.push( {
		"data": "state-name",
		"title": biolims.crmCustomer.state,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "state-name");
			$(td).attr("state-id", rowData['state-id']);
		}
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#crmCustomerTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#crmCustomerTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#crmCustomerTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#crmCustomer_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/crm/customer/customer/crmCustomer/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 crmCustomerTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveCrmCustomer($("#crmCustomerTable"));
		}
	});
	}
	
	var crmCustomerOptions = 
	table(true, "","/crm/customer/customer/crmCustomer/showCrmCustomerTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	crmCustomerTable = renderData($("#crmCustomerTable"), crmCustomerOptions);
	crmCustomerTable.on('draw', function() {
		oldcrmCustomerChangeLog = crmCustomerTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveCrmCustomer(ele) {
	var data = saveCrmCustomerjson(ele);
	var ele=$("#crmCustomerTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/crm/customer/customer/crmCustomer/saveCrmCustomerTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveCrmCustomerjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			
			if(k == "district-name") {
				json["district-id"] = $(tds[j]).attr("district-id");
				continue;
			}
			
			if(k == "type-name") {
				json["type-id"] = $(tds[j]).attr("type-id");
				continue;
			}
			
			if(k == "dutyManId-name") {
				json["dutyManId-id"] = $(tds[j]).attr("dutyManId-id");
				continue;
			}
			
			if(k == "state-name") {
				json["state-id"] = $(tds[j]).attr("state-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldcrmCustomerChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
