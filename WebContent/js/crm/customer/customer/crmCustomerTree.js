$(document).ready(function() {


var fields = [];
	  fields.push({
		title : biolims.sample.createUserId,
		field : "createUser.name"
	});
	   
	fields.push({
		title : biolims.crmCustomer.createDate,
		field : "createDate"
	});
	
	   
	fields.push({
		title : biolims.tStorageReagentBuySerial.supplierLinkTel,
		field : "ks"
	});
	
	  fields.push({
		title : biolims.crmCustomer.district,
		field : "district.name"
	});
	   
	fields.push({
		title : biolims.crmCustomer.name,
		field : "name"
	});
	
	  fields.push({
		title : biolims.crmCustomer.type,
		field : "type.name"
	});
	   
	fields.push({
		title : biolims.crmCustomer.street,
		field : "street"
	});
	
	   
	fields.push({
		title : biolims.crmCustomer.postcode,
		field : "postcode"
	});
	
	   
	fields.push({
		title : biolims.crmCustomer.email,
		field : "email"
	});
	
	   
	fields.push({
		title : biolims.crmCustomer.yjly,
		field : "yjly"
	});
	
	  fields.push({
		title : biolims.crmCustomer.dutyManId,
		field : "dutyManId.name"
	});
	  fields.push({
		title : biolims.crmCustomer.state,
		field : "state.name"
	});
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/crm/customer/customer/crmCustomer/showCrmCustomerListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/crm/customer/customer/crmCustomer/editCrmCustomer.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/customer/customer/crmCustomer/editCrmCustomer.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/customer/customer/crmCustomer/viewCrmCustomer.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   fields.push({
		header : biolims.sample.createUserId,
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.crmCustomer.createDate,
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tStorageReagentBuySerial.supplierLinkTel,
		"searchName" : 'ks',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.crmCustomer.district,
		"searchName" : 'district.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.crmCustomer.name,
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.crmCustomer.type,
		"searchName" : 'type.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.crmCustomer.street,
		"searchName" : 'street',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmCustomer.street,
		"searchName" : 'postcode',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmCustomer.email,
		"searchName" : 'email',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.crmCustomer.yjly,
		"searchName" : 'yjly',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.crmCustomer.dutyManId,
		"searchName" : 'dutyManId.name',
		"type" : "input"
	});
	   fields.push({
		header : biolims.crmCustomer.state,
		"searchName" : 'state.name',
		"type" : "input"
	});

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

