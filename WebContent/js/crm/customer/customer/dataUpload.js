 function getPath(obj) {
  if (obj) {
  if (window.navigator.userAgent.indexOf("MSIE") >= 1) {
  obj.select(); return document.selection.createRange().text;
  }
  else if (window.navigator.userAgent.indexOf("Firefox") >= 1) {
  if (obj.files) {
  return obj.files.item(0).getAsDataURL();
  }
  return obj.value;
  }
  return obj.value;
  }
  }  

$(function() {
	$("#dataUpload_file_div").dialog({
		modal : true,
		title : biolims.common.uploadResult,
		closeText : "",
		width : 380,
		height : 400,
		resizable : false,
		buttons : {
			"Confirm" : function() {
			  //以下即为完整客户端路径
			  var filepath=getPath(document.getElementById("file-upload"));
				$.post("/crm/customer/customer/ajaxDateUpload.action",{"filepath":filepath});
//				var callback = $("#dataUpload_file_div").data("callback");
//				if ((typeof callback) != "function") {
//					return;
//				}
//				var data = {};
//				data.fileId = $("#file_id").val();
//				data.fileName = $("#file_name").html();
//				callback(data);
//				$(this).dialog("close");
			},
			"Cancel" : function() {
				$(this).dialog("close");
			}
		},
		close : function(event, ui) {
			$(this).dialog("destroy");
			$(this).remove();
		}
	});

//	$("#file-upload").fileupload({
//		forceIframeTransport : true,
//		autoUpload : true,
//		type : 'POST',
//		dataType : "json",
//		url : ctx + "/operfile/ajaxUpload.action?modelType=" + $("#module_type").val(),
//		always : function(e, data) {
//			if (data != "error") {
//				var result = data.result;
//				$("#file_id").val(result.fileId);
//				$("#file_name").html(result.fileName);
//				$("#upload_time").html(result.uploadDate);
//			} else {
//				// TODO 文件上传失败提醒
//			}
//		}
//	});
});
