var contractGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
			name:'note',
			type:"string"
		});
	    fields.push({
		name:'contractType-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
		name:'startDate',
		type:"string"
	});
	   fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	   fields.push({
		name:'endDate',
		type:"string"
	});
	    fields.push({
		name:'currencyType-name',
		type:"string"
	});
	    fields.push({
		name:'crmLinkMan-name',
		type:"string"
	});
	    fields.push({
			name:'emailNote',
			type:"string"
		});
		 fields.push({
				name:'headLine',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.contractId,
		width:15*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.name,
		width:35*10
	});
	cm.push({
		dataIndex:'contractType-name',
		hidden : false,
		header:biolims.crm.contractType,
		width:35*10
	});
	cm.push({
		dataIndex:'dutyUser-id',
		hidden : true,
		header:biolims.master.dutyUserId,
		width:10*10
	});
	cm.push({
		dataIndex:'dutyUser-name',
		hidden : false,
		header:biolims.master.dutyUserName,
		width:10*10
	});
	cm.push({
		dataIndex:'createDate',
		hidden : false,
		header:biolims.master.createDate,
		width:12*10
	});
	cm.push({
		dataIndex:'fee',
		hidden : false,
		header:biolims.crm.fee,
		width:10*10
	});
	cm.push({
		dataIndex:'assign',
		hidden : false,
		header:biolims.master.assign,
		width:25*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/customer/showContractListJson.action?id="+ $("#crmCustomer_id").val();
	var opts={};
	opts.title=biolims.crm.contractInfo;
	opts.height =  document.body.clientHeight-190;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/linkman/delCrmLinkManItem.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				scpProToGrid.getStore().commitChanges();
//				scpProToGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
    
//	opts.tbar.push({
//			text : '选择负责人',
//			handler : selectdutyUserFun
//		});
//	opts.tbar.push({
//		text : '填加明细',
//		iconCls : 'add',
//		handler : function() {
//			var ob = crmLinkManItemGrid.getStore().recordType;
//			var p = new ob({});
//			p.isNew = true;
//			p.set("dutyUser-id", $("#crmLinkMan_dutyUser").val());
//			p.set("dutyUser-name", $("#crmLinkMan_dutyUser_name").val());
//			crmLinkManItemGrid.stopEditing();
//			crmLinkManItemGrid.getStore().insert(0, p);
//			crmLinkManItemGrid.startEditing(0, 0);
//		}
//	});
//	opts.tbar.push({
//			text : '选择所属',
//			handler : selectcrmLinkManFun
//		});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	contractGrid=gridEditTable("contractListdiv",cols,loadParam,opts);
	$("#contractListdiv").data("contractGrid", contractGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectdutyUserFun(){
	var win = Ext.getCmp('selectdutyUser');
	if (win) {win.close();}
	var selectdutyUser= new Ext.Window({
	id:'selectdutyUser',modal:true,title:biolims.master.selectHead,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=dutyUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectdutyUser.close(); }  }]  });     selectdutyUser.show(); }
	function setdutyUser(id,name){
//		var gridGrid = $("#crmLinkManItemdiv").data("crmLinkManItemGrid");
//		alert(id);
//		alert(name);
		var records = crmLinkManItemGrid.getSelectRecord();
		if(!records.length){
			records = crmLinkManItemGrid.getAllRecord();
		}
		
//		alert(selRecords);
		$.each(records, function(i, obj) {
			obj.set('dutyUser-id',id);
			obj.set('dutyUser-name',name);
		});
		var win = Ext.getCmp('selectdutyUser')
		if(win){
			win.close();
		}
	}
	
//function selectcrmLinkManFun(){
//	var win = Ext.getCmp('selectcrmLinkMan');
//	if (win) {win.close();}
//	var selectcrmLinkMan= new Ext.Window({
//	id:'selectcrmLinkMan',modal:true,title:'选择所属',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmLinkManSelect.action?flag=crmLinkMan' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectcrmLinkMan.close(); }  }]  });     selectcrmLinkMan.show(); }
//	function setcrmLinkMan(id,name){
//		var gridGrid = $("#crmLinkManItemdiv").data("crmLinkManItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('crmLinkMan-id',id);
//			obj.set('crmLinkMan-name',name);
//		});
//		var win = Ext.getCmp('selectcrmLinkMan')
//		if(win){
//			win.close();
//		}
//	}
	
