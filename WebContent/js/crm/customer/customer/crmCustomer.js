var crmCustomerTable;
$(function() {
	var fields=[];
	fields.push({
		"data":"id",
		"title":biolims.common.customId
	});
	    fields.push({
		"data":"createUser-id",
		"title":biolims.sample.createUserId
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.crmCustomer.createUser
	});
	    fields.push({
	    	"data":"customerType",
	    	"title":"客户类型",
	    	"render": function(data, type, full, meta) {
				if(data == "0") {
					return "渠道用户";
				}
				if(data == "1") {
					return "直接用户";
				}
				else {
					return '';
				}
			}
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.crmCustomer.createDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"ks",
		"title":biolims.tStorageReagentBuySerial.supplierLinkTel,
	});
	
	    fields.push({
		"data":"district-id",
		"title":biolims.crm.districtId
	});
	    fields.push({
		"data":"district-name",
		"title":biolims.crmCustomer.district
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.crmCustomer.name,
	});
	
	    fields.push({
		"data":"type-id",
		"title":biolims.purchase.typeId
	});
	    fields.push({
		"data":"type-name",
		"title":biolims.crmCustomer.type
	});
	
	    fields.push({
		"data":"street",
		"title":biolims.crmCustomer.street,
	});
	
	    fields.push({
		"data":"postcode",
		"title":biolims.crmCustomer.postcode,
	});
	
	    fields.push({
		"data":"email",
		"title":biolims.crmCustomer.email,
	});
	
	    fields.push({
		"data":"yjly",
		"title":biolims.crmCustomer.yjly,
	});
	
	    fields.push({
		"data":"dutyManId-id",
		"title":biolims.master.personId
	});
	    fields.push({
		"data":"dutyManId-name",
		"title":biolims.crmCustomer.dutyManId
	});
	
	    fields.push({
		"data":"state-id",
		"title":"状态ID"
	});
	    fields.push({
		"data":"state-name",
		"title":biolims.crmCustomer.state
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "CrmCustomer"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.master.personId);
				}
			}
		});

	var options = table(true, "","/crm/customer/customer/showCrmCustomerTableJson.action",
	 fields, null)
	crmCustomerTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(crmCustomerTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/crm/customer/customer/editCrmCustomer.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/customer/customer/editCrmCustomer.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/customer/customer/viewCrmCustomer.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":biolims.sample.createUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.crmCustomer.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.crmCustomer.createDate
		});
	fields.push({
			"txt": biolims.crmCustomer.createDate+"(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.crmCustomer.createDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"ks",
			"type":"input",
			"txt":biolims.tStorageReagentBuySerial.supplierLinkTel
		});
	fields.push({
	    "type":"input",
		"searchName":"district.id",
		"txt":biolims.crm.districtId
	});
	fields.push({
	    "type":"input",
		"searchName":"district.name",
		"txt":biolims.crmCustomer.district
	});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.crmCustomer.name
		});
	fields.push({
	    "type":"input",
		"searchName":"type.id",
		"txt":biolims.purchase.typeId
	});
	fields.push({
	    "type":"input",
		"searchName":"type.name",
		"txt":biolims.crmCustomer.type
	});
	   fields.push({
		    "searchName":"street",
			"type":"input",
			"txt":biolims.crmCustomer.street
		});
	   fields.push({
		    "searchName":"postcode",
			"type":"input",
			"txt":biolims.crmCustomer.postcode
		});
	   fields.push({
		    "searchName":"email",
			"type":"input",
			"txt":biolims.crmCustomer.email
		});
	   fields.push({
		    "searchName":"yjly",
			"type":"input",
			"txt":biolims.crmCustomer.yjly
		});
	fields.push({
	    "type":"input",
		"searchName":"dutyManId.id",
		"txt":biolims.crmCustomer.dutyManId+"ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"dutyManId.name",
		"txt":biolims.crmCustomer.dutyManId
	});
	fields.push({
	    "type":"input",
		"searchName":"state.id",
		"txt":"状态ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"state.name",
		"txt":biolims.crmCustomer.state
	});
	fields.push({
		"type":"select",
		"options": biolims.common.pleaseChoose+"|"+"渠道用户"+"|"+"直接用户",
		"changeOpt": "''|0|1",
		"searchName":"customerType",
		"txt":"客户类型"
	});
	
	fields.push({
		"type":"table",
		"table":crmCustomerTable
	});
	return fields;
}
