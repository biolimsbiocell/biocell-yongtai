var projectTable;
var oldprojectChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#crmCustomer_id").text();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.tProject.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.tProject.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "type-id",
		"title": biolims.tProject.type+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "type-id");
		}
	});
	colOpts.push( {
		"data": "type-name",
		"title": biolims.tProject.type,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "type-name");
			$(td).attr("type-id", rowData['typer-id']);
		}
	});
	colOpts.push( {
		"data": "crmDoctor-id",
		"title": biolims.master.hospitalId,
		"createdCell": function(td) {
			$(td).attr("saveName", "crmDoctor-id");
		}
	});
	colOpts.push( {
		"data": "crmDoctor-name",
		"title": biolims.tProject.crmDoctor,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "crmDoctor-name");
			$(td).attr("crmDoctor-id", rowData['crmDoctorr-id']);
		}
	});
	colOpts.push( {
		"data": "inspectionDepartment-id",
		"title": biolims.crm.ksId,
		"createdCell": function(td) {
			$(td).attr("saveName", "inspectionDepartment-id");
		}
	});
	colOpts.push( {
		"data": "inspectionDepartment-name",
		"title": biolims.tProject.inspectionDepartment,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "inspectionDepartment-name");
			$(td).attr("inspectionDepartment-id", rowData['inspectionDepartmentr-id']);
		}
	});
	colOpts.push( {
		"data": "state-id",
		"title": biolims.tProject.state+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "state-id");
		}
	});
	colOpts.push( {
		"data": "state-name",
		"title": biolims.tProject.state,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "state-name");
			$(td).attr("state-id", rowData['stater-id']);
		}
	});
	colOpts.push( {
		"data": "dutyUser-id",
		"title": biolims.tProject.dutyUser+"ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "dutyUser-id");
		}
	});
	colOpts.push( {
		"data": "dutyUser-name",
		"title": biolims.tProject.dutyUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dutyUser-name");
			$(td).attr("dutyUser-id", rowData['dutyUserr-id']);
		}
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.tProject.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
/*	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#projectTable"))
	}
	});*/
/*	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#projectTable"))
		}
	});*/
/*	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#projectTable"))
		}
	});*/
/*	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#project_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/crm/customer/customer/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 projectTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});*/
	
/*	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveProject($("#projectTable"));
		}
	});*/
	}
	var projectOptions = table(true,
		id,
		'/crm/customer/customer/showProjectTableJson.action', colOpts, tbarOpts)
	projectTable = renderData($("#projectTable"), projectOptions);
	projectTable.on('draw', function() {
		oldprojectChangeLog = projectTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveProject(ele) {
	var data = saveProjectjson(ele);
	var ele=$("#projectTable");
	var changeLog = "医院信息明细：";
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="医院信息明细："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/crm/customer/customer/saveProjectTable.action',
		data: {
			id: $("#cC1_id").val(),
			dataJson: data,
			changeLog: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveProjectjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {

			// 判断并转换为数字
			var k = $(tds[j]).attr("savename");
			if(k == "type-name") {
				json["type-id"] = $(tds[j]).attr("type-id");
				continue;
			}
			
			if(k == "crmDoctor-name") {
				json["crmDoctor-id"] = $(tds[j]).attr("crmDoctor-id");
				continue;
			}
			
			if(k == "inspectionDepartment-name") {
				json["inspectionDepartment-id"] = $(tds[j]).attr("inspectionDepartment-id");
				continue;
			}
			
			if(k == "state-name") {
				json["state-id"] = $(tds[j]).attr("state-id");
				continue;
			}
			
			if(k == "dutyUser-name") {
				json["dutyUser-id"] = $(tds[j]).attr("dutyUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldprojectChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
