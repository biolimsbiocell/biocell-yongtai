$(function() {
	var searchModel = '<div class="input-group"><input type="text" class="form-control" placeholder="Search..." id="searchvalue" onkeydown="entersearch()"><span class="input-group-btn"> <button class="btn btn-info" type="button" onclick="searchModel()">按名称搜索</button></span> </div>';
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "科室ID",
		"visible" :false,
	});
	colOpts.push({
		"data" : "ksNum",
		"title" : "科室编号",
	});
	colOpts.push({
		"data" : "ksName",
		"title" : "科室名称",
	});
	colOpts.push({
		"data" : "ksPhone",
		"title" : "科室电话",
	});
	var tbarOpts = [];
	var addcrmCustomerKsItem = table(false, null,

			'/crm/customer/customer/findKsByHpId.action?id=' + $("#hpId").val(),
			colOpts, tbarOpts)
	crmCustomerKsItem = renderData($("#addcrmCustomerKsItem"), addcrmCustomerKsItem);
	$("#addcrmCustomerKsItem").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addDicTypeTable_wrapper .dt-buttons").html(searchModel);
				//$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addcrmCustomerKsItem tbody tr");
			crmCustomerKsItem.ajax.reload(); 
			crmCustomerKsItem.on('draw', function() {
				trs = $("#addcrmCustomerKsItem tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})
//回车搜索
function entersearch(){  
    var event = window.event || arguments.callee.caller.arguments[0];  
    if (event.keyCode == 13)  
    {  
        searchModel();
    }  
} 
//搜索方法
function searchModel() {
	DicTypeTable.settings()[0].ajax.data = {
		query: JSON.stringify({
			name:"%"+$("#searchvalue").val()+"%"
		})
	};
	DicTypeTable.ajax.reload();
}
