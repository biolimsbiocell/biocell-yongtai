var crmPatientManPageGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'lastName',
		type:"string"
	});
	    fields.push({
		name:'firstName',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'gender',
		type:"string"
	});
	    fields.push({
		name:'dateOfBirth',
		type:"string"
	});
	    fields.push({
		name:'age',
		type:"string"
	});
	    fields.push({
		name:'placeOfBirth',
		type:"string"
	});
	    fields.push({
		name:'race',
		type:"string"
	});
	    fields.push({
		name:'adultTeenager',
		type:"string"
	});
	    fields.push({
		name:'maritalStatus',
		type:"string"
	});
	    fields.push({
		name:'occupation',
		type:"string"
	});
	    fields.push({
		name:'consentReceived',
		type:"string"
	});
	    fields.push({
		name:'patientStatus-id',
		type:"string"
	});
	    fields.push({
		name:'patientStatus-name',
		type:"string"
	});
	    fields.push({
		name:'address',
		type:"string"
	});
	    fields.push({
		name:'jc',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber1',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber2',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber3',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient1',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient2',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient3',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber1',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber2',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber3',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'crmCustomerId-id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomerId-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.master.patientId,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'lastName',
		header:biolims.crm.lastName,
		width:50,
		sortable:true
	});
	cm.push({
		dataIndex:'firstName',
		header:biolims.crm.firstName,
		width:50,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:150,
		sortable:true
	});
	var genderstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.male ], [ '0', biolims.common.female ]]
	});
	
	var genderComboxFun = new Ext.form.ComboBox({
		store : genderstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'gender',
		header:biolims.common.gender,
		width:50,
		renderer: Ext.util.Format.comboRenderer(genderComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'dateOfBirth',
		header:biolims.common.birthDay,
		width:12*10,
		sortable:true
	});
	cm.push({
		dataIndex:'age',
		header:biolims.common.age,
		width:50,
		sortable:true
	});
	cm.push({
		dataIndex:'placeOfBirth',
		header:biolims.crm.placeOfBirth,
		width:15*10,
		sortable:true
	});

	
	cm.push({
		dataIndex:'adultTeenager',
		header:biolims.common.adultChild,
		width:10*10
	});
	var maritalStatusstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.married ], [ '0', biolims.common.unmarried ]]
	});
	
	var maritalStatusComboxFun = new Ext.form.ComboBox({
		store : maritalStatusstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'maritalStatus',
		header:biolims.common.marriage,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(maritalStatusComboxFun),				
		sortable:true
	});


	var jcstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.patient ], [ '0', biolims.common.healthy ],[ '2', biolims.common.overseasMedical ]]
	});
	
	var jcComboxFun = new Ext.form.ComboBox({
		store : jcstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'jc',
		header:biolims.crm.jc,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(jcComboxFun),				
		sortable:true
	});
	
	
	cm.push({
		dataIndex:'occupation',
		header:biolims.common.occupation,
		width:15*10,
		sortable:true
	});
	var consentReceivedstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no]]
	});
	
	var consentReceivedComboxFun = new Ext.form.ComboBox({
		store : consentReceivedstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'consentReceived',
		header:biolims.master.consentReceived,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(consentReceivedComboxFun),				
		sortable:true
	});
		cm.push({
		dataIndex:'patientStatus-id',
		hidden:true,
		header:biolims.master.patientStatusId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'patientStatus-name',
		header:biolims.master.patientStatusId,
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'address',
		header:biolims.common.address,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'telphoneNumber1',
		header:biolims.common.phone,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'telphoneNumber2',
		header:biolims.common.phone,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'telphoneNumber3',
		header:biolims.common.phone,
		width:10*10,
		sortable:true
	});
	
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'crmCustomerId-id',
		hidden:true,
		header:biolims.sample.customerIdId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'crmCustomerId-name',
		header:biolims.sample.customerName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.startDate,
		width:12*10,
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/customer/showCrmPantienManpageListJson.action?id="+ $("#crmCustomer_id").val();
	var opts={};
	opts.title='1111';
	opts.height =  document.body.clientHeight-190;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/linkman/delCrmLinkManItem.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientManPageGrid.getStore().commitChanges();
//				crmPatientManPageGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
    
//	opts.tbar.push({
//			text : '选择负责人',
//			handler : selectdutyUserFun
//		});
//	opts.tbar.push({
//		text : '填加明细',
//		iconCls : 'add',
//		handler : function() {
//			var ob = crmLinkManItemGrid.getStore().recordType;
//			var p = new ob({});
//			p.isNew = true;
//			p.set("dutyUser-id", $("#crmLinkMan_dutyUser").val());
//			p.set("dutyUser-name", $("#crmLinkMan_dutyUser_name").val());
//			crmLinkManItemGrid.stopEditing();
//			crmLinkManItemGrid.getStore().insert(0, p);
//			crmLinkManItemGrid.startEditing(0, 0);
//		}
//	});
//	opts.tbar.push({
//			text : '选择所属',
//			handler : selectcrmLinkManFun
//		});
	/*opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});*/
	
	crmPatientManPageGrid=gridEditTable("crmPantienManItmdiv",cols,loadParam,opts);
	
	$("#crmPantienManItmdiv").data("crmPatientManPageGrid", crmPatientManPageGrid);
	$(".x-panel-bbar,.x-toolbar").css("width","100%");
	$(".x-panel-tbar").css("width","100%");
})

/*function selectdutyUserFun(){
	var win = Ext.getCmp('selectdutyUser');
	if (win) {win.close();}
	var selectdutyUser= new Ext.Window({
	id:'selectdutyUser',modal:true,title:'选择负责人',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=dutyUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectdutyUser.close(); }  }]  });     selectdutyUser.show(); }
	function setdutyUser(id,name){
//		var gridGrid = $("#crmLinkManItemdiv").data("crmLinkManItemGrid");
//		alert(id);
//		alert(name);
		var records = crmLinkManItemGrid.getSelectRecord();
		if(!records.length){
			records = crmLinkManItemGrid.getAllRecord();
		}
		
//		alert(selRecords);
		$.each(records, function(i, obj) {
			obj.set('dutyUser-id',id);
			obj.set('dutyUser-name',name);
		});
		var win = Ext.getCmp('selectdutyUser')
		if(win){
			win.close();
		}
	}
	
//function selectcrmLinkManFun(){
//	var win = Ext.getCmp('selectcrmLinkMan');
//	if (win) {win.close();}
//	var selectcrmLinkMan= new Ext.Window({
//	id:'selectcrmLinkMan',modal:true,title:'选择所属',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmLinkManSelect.action?flag=crmLinkMan' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectcrmLinkMan.close(); }  }]  });     selectcrmLinkMan.show(); }
//	function setcrmLinkMan(id,name){
//		var gridGrid = $("#crmLinkManItemdiv").data("crmLinkManItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('crmLinkMan-id',id);
//			obj.set('crmLinkMan-name',name);
//		});
//		var win = Ext.getCmp('selectcrmLinkMan')
//		if(win){
//			win.close();
//		}
//	}
	*/
