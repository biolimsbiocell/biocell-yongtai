var crmCustomerContractGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomer-id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomer-name',
		type:"string"
	});
	    fields.push({
		name:'contractId-id',
		type:"string"
	});
	    fields.push({
		name:'contractId-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmCustomer-id',
		hidden : true,
		header:biolims.sample.customerIdId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmCustomer-name',
		hidden : false,
		header:biolims.sample.customerId,
		width:15*10
	});
	cm.push({
		dataIndex:'contractId-id',
		hidden : true,
		header:biolims.common.contractCodeId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contractId-name',
		hidden : false,
		header:biolims.common.contractId,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/customer/showCrmCustomerContractListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.crm.contractDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/customer/customer/delCrmCustomerContract.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	opts.tbar.push({
			text : biolims.crm.selectCustomerId,
			handler : selectcrmCustomerFun
		});
    
	opts.tbar.push({
			text : biolims.crm.selectContractId,
			handler : selectcontractIdFun
		});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmCustomerContractGrid=gridEditTable("crmCustomerContractdiv",cols,loadParam,opts);
	$("#crmCustomerContractdiv").data("crmCustomerContractGrid", crmCustomerContractGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmCustomerFun(){
	var win = Ext.getCmp('selectcrmCustomer');
	if (win) {win.close();}
	var selectcrmCustomer= new Ext.Window({
	id:'selectcrmCustomer',modal:true,title:biolims.crm.selectCustomerId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmCustomerSelect.action?flag=crmCustomer' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmCustomer.close(); }  }]  });     selectcrmCustomer.show(); }
	function setcrmCustomer(id,name){
		var gridGrid = $("#crmCustomerContractdiv").data("crmCustomerContractGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmCustomer-id',id);
			obj.set('crmCustomer-name',name);
		});
		var win = Ext.getCmp('selectcrmCustomer')
		if(win){
			win.close();
		}
	}
	
function selectcontractIdFun(){
	var win = Ext.getCmp('selectcontractId');
	if (win) {win.close();}
	var selectcontractId= new Ext.Window({
	id:'selectcontractId',modal:true,title:biolims.crm.selectContractId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/ContractSelect.action?flag=contractId' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcontractId.close(); }  }]  });     selectcontractId.show(); }
	function setcontractId(id,name){
		var gridGrid = $("#crmCustomerContractdiv").data("crmCustomerContractGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('contractId-id',id);
			obj.set('contractId-name',name);
		});
		var win = Ext.getCmp('selectcontractId')
		if(win){
			win.close();
		}
	}
	
