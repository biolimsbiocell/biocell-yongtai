var crmCustomerIteamTable;
var oldcrmCustomerIteamTable;
$(function() {
	// 加载子表
	var id = document.getElementById('crmCustomer_id').innerHTML
	var tbarOpts = [];
	var colOpts = [];
//	colOpts.push({
//		"data":"id",
//		"title": "ID",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "ksID");
//	    },
//		"visible": false,	
//		
//	});
	
	colOpts.push({
		"data":"ksNum",
		"title": "科室编号",
//		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "ksNum");
	    },
	});
	colOpts.push({
		"data":"ksName",
		"title": "科室名称",
//		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "ksName");
	    },
	});
	colOpts.push({
		"data":"ksPhone",
		"title": "科室电话",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "ksPhone");
	    },
	});
	var handlemethod = $("#handlemethod").val();
//按钮
	if(handlemethod != "view") {
		tbarOpts.push({
			text: "添加明细",
			action: function() {
				addItem($("#crmCustomerIteam"))
			}
		});
		tbarOpts.push({
			text : "选择科室",
			className : "btn btn-sm btn-success choseRoom hiddle",
			action : function() {
				showinspectionDepartment($("#crmCustomerIteam"));
			}
		});
		tbarOpts
		.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked(
						$("#crmCustomerIteam"),
						"/crm/customer/customer/delKs.action",
						"删除文档：", id,crmCustomerIteamTable);
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#crmCustomerIteam"))
			}
		});
		
	}
	
	var crmCustomerIteams = table(true,
		id,
		'/crm/customer/customer/showCrmCustomerIteamTableJson.action', colOpts, tbarOpts)
	crmCustomerIteamTable = renderData($("#crmCustomerIteam"), crmCustomerIteams);
	crmCustomerIteamTable.on('draw', function() {
		oldcrmCustomerIteamTable = crmCustomerIteamTable.ajax.json();
	});
});	

//选择科室
function showinspectionDepartment(ele) {
		var rows = $("#crmCustomerIteam .selected");
		var length = rows.length;
		if (!length) {
			top.layer.msg(biolims.common.pleaseSelectData);
			return false;
		}
		top.layer.open({
			title : "选择科室",
			type : 2,
			offset : [ '10%', '10%' ],
			area : [ document.body.clientWidth - 300,
					document.body.clientHeight - 100 ],
			btn : biolims.common.selected,
			content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=ks", ''],
			yes: function(index, layer) {
			     var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
					0).text();
				top.layer.close(index)
			    rows.addClass("editagain");
//				rows.find("td[savename='ksNum']").attr(
//								"sampletype-id", id).text(type);
				rows.find("td[savename='ksNum']").text(id);
				rows.find("td[savename='ksName']").text(name);

				top.layer.close(index)
				

			}
		})
	}
	
//json
function savejson(ele) {

		var trs = ele.find("tbody").children(".editagain");
		var data = [];
		var flag = true;

		var trss = ele.find("tbody tr");
		trss.each(function(i, val) {
			var json = {};
			var tds = $(val).children("td");
			json["id"] = $(tds[0]).find("input").val();
			for (var j = 1; j < tds.length; j++) {
				var k = $(tds[j]).attr("savename");
				json[k] = $(tds[j]).text();
				}
			data.push(json);
			})
			return JSON.stringify(data);
		}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldcrmCustomerIteamTable.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}


