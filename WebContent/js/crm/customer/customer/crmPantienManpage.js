var crmPatientManPageGrid;
$(function(){
	//alert("this is test");
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'lastName',
		type:"string"
	});
	    fields.push({
		name:'firstName',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'gender',
		type:"string"
	});
	    fields.push({
		name:'dateOfBirth',
		type:"string"
	});
	    fields.push({
		name:'age',
		type:"string"
	});
	    fields.push({
		name:'placeOfBirth',
		type:"string"
	});
	    fields.push({
		name:'race',
		type:"string"
	});
	    fields.push({
		name:'adultTeenager',
		type:"string"
	});
	    fields.push({
		name:'maritalStatus',
		type:"string"
	});
	    fields.push({
		name:'occupation',
		type:"string"
	});
	    fields.push({
		name:'consentReceived',
		type:"string"
	});
	    fields.push({
		name:'patientStatus-id',
		type:"string"
	});
	    fields.push({
		name:'patientStatus-name',
		type:"string"
	});
	    fields.push({
		name:'address',
		type:"string"
	});
	    fields.push({
		name:'jc',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber1',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber2',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber3',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient1',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient2',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient3',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber1',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber2',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber3',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'crmCustomerId-id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomerId-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.master.patientId,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'lastName',
		header:biolims.crm.lastName,
		width:50,
		sortable:true
	});
	cm.push({
		dataIndex:'firstName',
		header:biolims.crm.firstName,
		width:50,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:150,
		sortable:true
	});
	var genderstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.male ], [ '0', biolims.common.female ]]
	});
	
	var genderComboxFun = new Ext.form.ComboBox({
		store : genderstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'gender',
		header:biolims.common.gender,
		width:50,
		renderer: Ext.util.Format.comboRenderer(genderComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'dateOfBirth',
		header:biolims.common.birthDay,
		width:12*10,
		sortable:true
	});
	cm.push({
		dataIndex:'age',
		header:biolims.common.age,
		width:50,
		sortable:true
	});
	cm.push({
		dataIndex:'placeOfBirth',
		header:biolims.crm.placeOfBirth,
		width:15*10,
		sortable:true
	});

	var adultTeenagerstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.crm.adult ], [ '0', biolims.crm.child ]]
	});
	
	var adultTeenagerComboxFun = new Ext.form.ComboBox({
		store : adultTeenagerstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'adultTeenager',
		header:biolims.common.adultChild,
		width:10*10,
		renderer: Ext.util.Format.comboRenderer(adultTeenagerComboxFun),				
		sortable:true
	});
	var maritalStatusstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.married ], [ '0', biolims.common.unmarried]]
	});
	
	var maritalStatusComboxFun = new Ext.form.ComboBox({
		store : maritalStatusstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'maritalStatus',
		header:biolims.common.marriage,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(maritalStatusComboxFun),				
		sortable:true
	});


	var jcstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.patient ], [ '0', biolims.common.healthy ],[ '2', biolims.common.overseasMedical ]]
	});
	
	var jcComboxFun = new Ext.form.ComboBox({
		store : jcstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'jc',
		header:biolims.crm.jc,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(jcComboxFun),				
		sortable:true
	});
	
	
	cm.push({
		dataIndex:'occupation',
		header:biolims.common.occupation,
		width:15*10,
		sortable:true
	});
	var consentReceivedstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ]]
	});
	
	var consentReceivedComboxFun = new Ext.form.ComboBox({
		store : consentReceivedstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'consentReceived',
		header:biolims.master.consentReceived,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(consentReceivedComboxFun),				
		sortable:true
	});
		cm.push({
		dataIndex:'patientStatus-id',
		hidden:true,
		header:biolims.master.patientStatusId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'patientStatus-name',
		header:biolims.master.patientStatusName,
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'address',
		header:biolims.common.address,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'telphoneNumber1',
		header:biolims.common.phone,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'telphoneNumber2',
		header:biolims.common.phone,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'telphoneNumber3',
		header:biolims.common.phone,
		width:10*10,
		sortable:true
	});
	
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'crmCustomerId-id',
		hidden:true,
		header:biolims.sample.customerIdId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'crmCustomerId-name',
		header:biolims.sample.customerName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.startDate,
		width:12*10,
		sortable:true
	});
	
	
	cols.cm=cm;
	var loadParam={};
	//alert("this is test");
	loadParam.url=ctx+"/crm/customer/customer/showCrmPantienManpageListJson.action?id="+ $("#crmCustomer_id").val();
	var opts={};
	opts.title=biolims.crm.testingIndividual;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
      /* opts.delSelect = function(ids) {
		//ajax("post", "/crm/customer/customer/delCrmCustomerLinkMan.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};*/
    
	/*opts.tbar.push({
			text : '选择所属客户',
			handler : selectcrmCustomerFun
		});
    
	opts.tbar.push({
			text : '选择联系人编号',
			handler : selectlinkManIdFun
		});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});*/
	crmPatientManPageGrid=gridEditTable("crmPantienManpagediv",cols,loadParam,opts);
	$("#crmPantienManpagediv").data("crmPatientManPageGrid", crmPatientManPageGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

/*function selectcrmCustomerFun(){
	var win = Ext.getCmp('selectcrmCustomer');
	if (win) {win.close();}
	var selectcrmCustomer= new Ext.Window({
	id:'selectcrmCustomer',modal:true,title:'选择所属客户',layout:'fit',width:900,height:700,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmCustomerSelect.action?flag=crmCustomer' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectcrmCustomer.close(); }  }]  });     selectcrmCustomer.show(); }
	function setcrmCustomer(id,name){
		var gridGrid = $("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmCustomer-id',id);
			obj.set('crmCustomer-name',name);
		});
		var win = Ext.getCmp('selectcrmCustomer')
		if(win){
			win.close();
		}
	}
	
function selectlinkManIdFun(){
	var win = Ext.getCmp('selectlinkManId');
	if (win) {win.close();}
	var selectlinkManId= new Ext.Window({
	id:'selectlinkManId',modal:true,title:'选择联系人编号',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmLinkManSelect.action?flag=linkManId' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectlinkManId.close(); }  }]  });     selectlinkManId.show(); }
	function setlinkManId(id,name){
		var gridGrid = $("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('linkManId-id',id);
			obj.set('linkManId-name',name);
		});
		var win = Ext.getCmp('selectlinkManId')
		if(win){
			win.close();
		}
	}
	*/
