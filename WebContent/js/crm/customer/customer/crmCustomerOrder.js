var crmCustomerOrderGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomer-id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomer-name',
		type:"string"
	});
	    fields.push({
		name:'crmOrderId-id',
		type:"string"
	});
	    fields.push({
		name:'crmOrderId-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmCustomer-id',
		hidden : true,
		header:biolims.sample.customerIdId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmCustomer-name',
		hidden : false,
		header:biolims.sample.customerId,
		width:15*10
	});
	cm.push({
		dataIndex:'crmOrderId-id',
		hidden : true,
		header:biolims.common.orderCodeId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmOrderId-name',
		hidden : false,
		header:biolims.common.orderCode,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/customer/showCrmCustomerOrderListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.crm.customerOrder;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/customer/customer/delCrmCustomerOrder.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	opts.tbar.push({
			text : biolims.crm.selectCustomerId,
			handler : selectcrmCustomerFun
		});
    
	opts.tbar.push({
			text : biolims.crm.selectOrderId,
			handler : selectcrmOrderIdFun
		});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmCustomerOrderGrid=gridEditTable("crmCustomerOrderdiv",cols,loadParam,opts);
	$("#crmCustomerOrderdiv").data("crmCustomerOrderGrid", crmCustomerOrderGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmCustomerFun(){
	var win = Ext.getCmp('selectcrmCustomer');
	if (win) {win.close();}
	var selectcrmCustomer= new Ext.Window({
	id:'selectcrmCustomer',modal:true,title:biolims.crm.selectCustomerId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmCustomerSelect.action?flag=crmCustomer' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmCustomer.close(); }  }]  });     selectcrmCustomer.show(); }
	function setcrmCustomer(id,name){
		var gridGrid = $("#crmCustomerOrderdiv").data("crmCustomerOrderGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmCustomer-id',id);
			obj.set('crmCustomer-name',name);
		});
		var win = Ext.getCmp('selectcrmCustomer')
		if(win){
			win.close();
		}
	}
	
function selectcrmOrderIdFun(){
	var win = Ext.getCmp('selectcrmOrderId');
	if (win) {win.close();}
	var selectcrmOrderId= new Ext.Window({
	id:'selectcrmOrderId',modal:true,title:biolims.crm.selectOrderId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmOrderSelect.action?flag=crmOrderId' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmOrderId.close(); }  }]  });     selectcrmOrderId.show(); }
	function setcrmOrderId(id,name){
		var gridGrid = $("#crmCustomerOrderdiv").data("crmCustomerOrderGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmOrderId-id',id);
			obj.set('crmOrderId-name',name);
		});
		var win = Ext.getCmp('selectcrmOrderId')
		if(win){
			win.close();
		}
	}
	
