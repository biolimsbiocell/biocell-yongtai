var crmLinkManItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'createDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
			name:'link',
			type:"string"
		});
	    fields.push({
		name:'dutyUser-id',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-name',
		type:"string"
	});
	   fields.push({
		name:'content',
		type:"string"
	});
	   fields.push({
		name:'fee',
		type:"string"
	});
	   fields.push({
		name:'assign',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'crmLinkMan-id',
		type:"string"
	});
	    fields.push({
		name:'crmLinkMan-name',
		type:"string"
	});
	    fields.push({
			name:'emailNote',
			type:"string"
		});
		 fields.push({
				name:'headLine',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'crmLinkMan-name',
		header:biolims.common.sname,
		width:15*10
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var linkstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=gtfs',
			method : 'POST'
		})
	});
	linkstore.load();
	var linkcob = new Ext.form.ComboBox({
		store : linkstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});

	cm.push({
		dataIndex:'link',
		header:biolims.master.link,
		renderer : Ext.util.Format.comboRenderer(linkcob)
	});
	cm.push({
		dataIndex:'headLine',
		hidden : false,
		header:biolims.master.headLine,
		width:35*10
	});
	cm.push({
		dataIndex:'content',
		hidden : false,
		header:biolims.master.content,
		width:35*10
	});

	cm.push({
		xtype : 'actioncolumn',
		width : 120,
		header : biolims.master.checkEmailContent,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.master.emailContent,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('headLine')){
					if(rec.get('id')&&rec.get('id')!='NEW'){
						
						var url = window.ctx+"/crm/customer/linkman/toEmailNote.action?&id="+ rec.get('id');
						
						window.open(url,biolims.master.checkEmailContent,'height=400,width=600,top=100,left=100,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no,status=no');
						
					}else{
						message(biolims.common.pleaseSaveRecord);
					}
				}
			}
		} ]
	});
	cm.push({
		dataIndex:'dutyUser-id',
		hidden : true,
		header:biolims.master.dutyUserId,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'emailNote',
//		hidden : true,
//		header:'邮件内容',
//		width:20*20,
//		editor : new Ext.form.TextArea({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'dutyUser-name',
		hidden : false,
		header:biolims.master.dutyUserName,
		width:10*10
	});
	cm.push({
		dataIndex:'createDate',
		hidden : false,
		header:biolims.master.createDate,
		width:12*10,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'fee',
		hidden : false,
		header:biolims.crm.fee,
		width:10*10,
		hidden:true
	});
	cm.push({
		dataIndex:'assign',
		hidden : false,
		header:biolims.master.assign,
		width:25*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*10
	});

//	cm.push({
//		dataIndex:'crmLinkMan-id',
//		hidden : true,
//		header:'所属ID',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'crmLinkMan-name',
//		hidden : false,
//		header:'所属',
//		width:15*10
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/customer/showCrmLinkManListJson.action?id="+ $("#crmCustomer_id").val();
	var opts={};
	opts.title=biolims.master.communicateInformation;
	opts.height =  document.body.clientHeight-190;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/customer/linkman/delCrmLinkManItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				crmLinkManItemGrid.getStore().commitChanges();
				crmLinkManItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择负责人',
//			handler : selectdutyUserFun
//		});
//	opts.tbar.push({
//		text : '填加明细',
//		iconCls : 'add',
//		handler : function() {
//			var ob = crmLinkManItemGrid.getStore().recordType;
//			var p = new ob({});
//			p.isNew = true;
//			p.set("dutyUser-id", $("#crmLinkMan_dutyUser").val());
//			p.set("dutyUser-name", $("#crmLinkMan_dutyUser_name").val());
//			crmLinkManItemGrid.stopEditing();
//			crmLinkManItemGrid.getStore().insert(0, p);
//			crmLinkManItemGrid.startEditing(0, 0);
//		}
//	});
//	opts.tbar.push({
//			text : '选择所属',
//			handler : selectcrmLinkManFun
//		});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmLinkManItemGrid=gridEditTable("crmLinkManItemdiv",cols,loadParam,opts);
	$("#crmLinkManItemdiv").data("crmLinkManItemGrid", crmLinkManItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectdutyUserFun(){
	var win = Ext.getCmp('selectdutyUser');
	if (win) {win.close();}
	var selectdutyUser= new Ext.Window({
	id:'selectdutyUser',modal:true,title:biolims.master.selectHead,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=dutyUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectdutyUser.close(); }  }]  });     selectdutyUser.show(); }
	function setdutyUser(id,name){
//		var gridGrid = $("#crmLinkManItemdiv").data("crmLinkManItemGrid");
//		alert(id);
//		alert(name);
		var records = crmLinkManItemGrid.getSelectRecord();
		if(!records.length){
			records = crmLinkManItemGrid.getAllRecord();
		}
		
//		alert(selRecords);
		$.each(records, function(i, obj) {
			obj.set('dutyUser-id',id);
			obj.set('dutyUser-name',name);
		});
		var win = Ext.getCmp('selectdutyUser')
		if(win){
			win.close();
		}
	}
	
//function selectcrmLinkManFun(){
//	var win = Ext.getCmp('selectcrmLinkMan');
//	if (win) {win.close();}
//	var selectcrmLinkMan= new Ext.Window({
//	id:'selectcrmLinkMan',modal:true,title:'选择所属',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmLinkManSelect.action?flag=crmLinkMan' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectcrmLinkMan.close(); }  }]  });     selectcrmLinkMan.show(); }
//	function setcrmLinkMan(id,name){
//		var gridGrid = $("#crmLinkManItemdiv").data("crmLinkManItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('crmLinkMan-id',id);
//			obj.set('crmLinkMan-name',name);
//		});
//		var win = Ext.getCmp('selectcrmLinkMan')
//		if(win){
//			win.close();
//		}
//	}
	
