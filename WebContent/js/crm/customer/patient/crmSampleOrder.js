var crmSampleOrderGrid;
var crmSampleOrderChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#crmPatient_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": true
	});
	
	colOpts.push({
		"data": "productName",
		"title": "产品名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	});
	colOpts.push({
		"data": "barcode",
		"title": "产品批号",
		"createdCell": function(td) {
			$(td).attr("saveName", "barcode");
		}
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data": "abbreviation",
		"title": "姓名缩写",
		"createdCell": function(td) {
			$(td).attr("saveName", "abbreviation");
		}
	});
	colOpts.push({
		"data": "filtrateCode",
		"title": "筛选号",
		"createdCell": function(td) {
			$(td).attr("saveName", "filtrateCode");
		}
	});
	colOpts.push({
		"data": "randomCode",
		"title": "随机号",
		"createdCell": function(td) {
			$(td).attr("saveName", "randomCode");
		}
	});
	colOpts.push({
		"data": "crmCustomer-name",
		"title": "医院",
		"createdCell": function(td) {
			$(td).attr("saveName", "crmCustomer-name");
		}
	});
	colOpts.push({
		"data": "inspectionDepartment-name",
		"title": "科室",
		"createdCell": function(td) {
			$(td).attr("saveName", "inspectionDepartment-name");
		}
	});
	colOpts.push({
		"data": "hepatitisHbv",
		"title": "乙肝",
		"name": "阳性+|阴性-",
		"visible": true,
//		"className": "select",
		"createdCell": function(td) {
			$(td).attr("saveName", "hepatitisHbv");
			$(td).attr("selectOpt", "阳性+|阴性-");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "阳性+";
			}
			if(data == "0") {
				return "阴性-";
			} else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "hepatitisHcv",
		"title": "丙肝",
		"name": "阳性+|阴性-",
		"visible": true,
//		"className": "select",
		"createdCell": function(td) {
			$(td).attr("saveName", "hepatitisHcv");
			$(td).attr("selectOpt", "阳性+|阴性-");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "阳性+";
			}
			if(data == "0") {
				return "阴性-";
			} else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "lymphoidCellSeries",
		"title": "淋巴值",
		"createdCell": function(td) {
			$(td).attr("saveName", "lymphoidCellSeries");
		}
	});
	colOpts.push({
		"data": "heparinTube",
		"title": "采血管数",
		"createdCell": function(td) {
			$(td).attr("saveName", "heparinTube");
		}
	});
	colOpts.push({
		"data": "round",
		"title": "采血轮次",
		"createdCell": function(td) {
			$(td).attr("saveName", "round");
		}
	});
	colOpts.push({
		"data": "drawBloodTime",
		"title": "采血日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "drawBloodTime");
		}
	});
	
	
	
//	colOpts.push({
//		"data": "gender",
//		"title": biolims.common.gender,
//		"name": biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
//		"visible": true,
//		"className": "select",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "gender");
//			$(td).attr("selectOpt", biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown);
//		},
//		"render": function(data, type, full, meta) {
//			if(data == "1") {
//				return biolims.common.male;
//			}
//			if(data == "0") {
//				return biolims.common.female;
//			}
//			if(data == "3") {
//				return biolims.common.unknown;
//			} else {
//				return '';
//			}
//		}
//	});
//	colOpts.push({
//		"data": "age",
//		"title": biolims.common.age,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "age");
//		},
//		"visible": true
////		,
////		"className": "date"
//	});
//	colOpts.push({
//		"data": "nation",
//		"title": biolims.common.race,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "nation");
//		},
//		"visible": true
////		,
////		"className": "date"
//	});
//	colOpts.push({
//		"data": "email",
//		"title": biolims.crm.email,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "email");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "zipCode",
//		"title": biolims.crm.postcode,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "zipCode");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "diagnosisDate",
//		"title": biolims.common.diagnosisDate,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "diagnosisDate");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "commissioner-id",
//		"title": biolims.common.commissionerId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "commissioner-id");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "commissioner-name",
//		"title": biolims.common.commissioner,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "commissioner-name");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "nativePlace",
//		"title": biolims.crm.placeOfBirth,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "nativePlace");
//		},
//		"visible": true
//	});
////	colOpts.push({
////		"data": "zipCode",
////		"title": "邮编",
////		"createdCell": function(td) {
////			$(td).attr("saveName", "zipCode");
////		},
////		"visible": true
////	});
//	colOpts.push({
//		"data": "medicalNumber",
//		"title": biolims.sample.medicalNumber,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "medicalNumber");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "familyCode",
//		"title": biolims.common.familyCode,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "familyCode");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "family",
//		"title": biolims.sample.family,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "family");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "familyPhone",
//		"title": biolims.sample.familyPhone,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "familyPhone");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "familySite",
//		"title": biolims.sample.familySite,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "familySite");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "crmCustomer-id",
//		"title": biolims.sample.medicalInstitutions+"id",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "crmCustomer-id");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "crmCustomer-name",
//		"title": biolims.sample.medicalInstitutions,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "crmCustomer-name");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "createUser-id",
//		"title": biolims.sample.createUserId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "createUser-id");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "createUser-name",
//		"title": biolims.sample.createUserName,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "createUser-name");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "stateName",
//		"title": biolims.common.state,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "stateName");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "barcode",
//		"title": biolims.common.barCode,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "barcode");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "subjectID",
//		"title": "受试者编号",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "subjectID");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "familyHistorysummary",
//		"title": "家族史描述",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "familyHistorysummary");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "prenatal",
//		"title": "是否处于孕期",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "prenatal");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "hospitalPatientID",
//		"title": "住院号",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "hospitalPatientID");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "weights",
//		"title": "体重",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "weights");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "gestationalWeeks",
//		"title": "孕周",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "gestationalWeeks");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "birthDate",
//		"title": "出生日期",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "birthDate");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "createDate",
//		"title": "创建时间",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "createDate");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "idCard",
//		"title": "身份证号",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "idCard");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "productName",
//		"title": "产品名称",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "productName");
//		},
//		"visible": false
//	});
	
	
//	colOpts.push({
//		"data": "sampleStage",
//		"title": biolims.sample.sampleStage,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "sampleStage");
//			$(td).attr("selectOpt", "|I|II|III|IV");
//		},
//		"visible": true,
//		"className": "select",
//		"name": "|I|II|III|IV",
//		"render": function(data, type, full, meta) {
//			if(data == "I") {
//				return '1';
//			}else if(data == "I") {
//				return '2';
//			}else if(data == "II") {
//				return '3';
//			}else if(data == "IV") {
//				return '4';
//			}else {
//				return '';
//			}
//		}
//	});
//	colOpts.push({
//		"data": "inspectionDepartment-id",
//		"title": biolims.sample.inspectionDepartmentId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "inspectionDepartment-id");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "inspectionDepartment-name",
//		"title": biolims.sample.inspectionDepartmentName,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "inspectionDepartment-name");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "productId",
//		"title": biolims.common.productId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "productId");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "productName",
//		"title": biolims.common.productName,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "productName");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "samplingDate",
//		"title": biolims.sample.samplingDate,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "samplingDate");
//		},
//		"visible": true
//	});
//	
//	colOpts.push({
//		"data": "samplingLocation-id",
//		"title": biolims.sample.samplingLocationId,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "samplingLocation-id");
//		},
//		"visible": false
//	});
//	colOpts.push({
//		"data": "samplingLocation-name",
//		"title": biolims.sample.samplingLocationName,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "samplingLocation-name");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "samplingNumber",
//		"title": biolims.common.code,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "samplingNumber");
//		},
//		"visible": true
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
//	colOpts.push({
//		"data": "recationSum",
//		"title": biolims.tStorageReagentBuySerial.recationSum,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "recationSum");
//		},
//		"visible": true,
//		"className": "edit"
//	});
	
	var handlemethod = $("#handlemethod").val();
//	if(handlemethod != "view") {
//		tbarOpts.push({
//			text: biolims.common.fillDetail,
//			action: function() {
//				addItem($("#crmSampleOrderGrid"))
//			}
//		});
//		tbarOpts.push({
//			text: biolims.common.addwindow,
//			action: function() {
//				addItemLayer($("#crmSampleOrderGrid"))
//			}
//		});
//		tbarOpts.push({
//			text: biolims.common.Editplay,
//			action: function() {
//				editItemLayer($("#crmSampleOrderGrid"))
//			}
//		});
//		tbarOpts.push({
//			text: biolims.common.save,
//			action: function() {
//				saveStorageReagentBuySerial($("#crmSampleOrderGrid"));
//			}
//		});
//	}

	var crmSampleOrderOptions = table(true,
		id,
		'/crm/customer/patient/showCrmOrderNewListJson.action', colOpts, tbarOpts)
	crmSampleOrderGrid = renderData($("#crmSampleOrderGrid"), crmSampleOrderOptions);
	crmSampleOrderGrid.on('draw', function() {
		crmSampleOrderChangeLog = crmSampleOrderGrid.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});

// 保存
function saveStorageReagentBuySerial(ele) {
	var data = saveStorageReagentBuySerialjson(ele);
	var ele = $("#crmSampleOrderGrid");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);

	$.ajax({
		type: 'post',
		url: '/storage/saveStorageReagentBuySerialTable.action',
		data: {
			id: $("#storage_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveStorageReagentBuySerialjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断男女并转换为数字
//			if(k == "gender") {
//				var gender = $(tds[j]).text();
//				if(gender == biolims.common.male) {
//					json[k] = "1";
//				} else if(gender == biolims.common.female) {
//					json[k] = "0";
//				} else if(gender == biolims.common.unknown) {
//					json[k] = "3";
//				} else {
//					json[k] = "";
//				}
//				continue;
//			}
			if(k == "storage-name") {
				json["storage-id"] = $(tds[j]).attr("storage-id");
				continue;
			}

			if(k == "costCenter-name") {
				json["costCenter-id"] = $(tds[j]).attr("costCenter-id");
				continue;
			}

			if(k == "position-name") {
				json["position-id"] = $(tds[j]).attr("position-id");
				continue;
			}

			if(k == "storageInItem-name") {
				json["storageInItem-id"] = $(tds[j]).attr("storageInItem-id");
				continue;
			}

			if(k == "regionType-name") {
				json["regionType-id"] = $(tds[j]).attr("regionType-id");
				continue;
			}

			if(k == "linkStorageItem-name") {
				json["linkStorageItem-id"] = $(tds[j]).attr("linkStorageItem-id");
				continue;
			}

			if(k == "rankType-name") {
				json["rankType-id"] = $(tds[j]).attr("rankType-id");
				continue;
			}

			if(k == "currencyType-name") {
				json["currencyType-id"] = $(tds[j]).attr("currencyType-id");
				continue;
			}

			if(k == "timeUnit-name") {
				json["timeUnit-id"] = $(tds[j]).attr("timeUnit-id");
				continue;
			}

			if(k == "unit-name") {
				json["unit-id"] = $(tds[j]).attr("unit-id");
				continue;
			}

			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		crmSampleOrderChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}