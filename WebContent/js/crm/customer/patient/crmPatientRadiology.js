var crmPatientRadiologyGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'examinationItem',
		type:"string"
	});
	   fields.push({
		name:'dateOfExamination',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'hospital',
		type:"string"
	});
	   fields.push({
		name:'examination',
		type:"string"
	});
	   fields.push({
		name:'hospitalPatient',
		type:"string"
	});
	   fields.push({
		name:'keyResultsDescription',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-id',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.master.radiologyDepartmentCheckId,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
		var examinationItemstore = new Ext.data.JsonStore({
					root : 'results',
					remoteSort : true,
					fields : [ {
						name : 'id'
					}, {
						name : 'name'
					} ],
					proxy : new Ext.data.HttpProxy({
						url : window.ctx + '/dic/type/dicTypeCobJson.action?type=fskjcjcxm',
						method : 'POST'
						})
					});

					examinationItemstore.load();
					var examinationItemcob = new Ext.form.ComboBox({
						store : examinationItemstore,
						displayField : 'name',
						valueField : 'id',
						typeAhead : true,
						mode : 'remote',
						forceSelection : true,
						triggerAction : 'all',
						selectOnFocus : true
					});
	cm.push({
		dataIndex:'examinationItem',
		hidden : false,
		header:biolims.master.examinationItem,
		width:15*10,
		
		renderer: Ext.util.Format.comboRenderer(examinationItemcob),editor: examinationItemcob
	});
	cm.push({
		dataIndex:'dateOfExamination',
		hidden : false,
		header:biolims.master.dateOfExamination,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'hospital',
		hidden : false,
		header:biolims.master.hospitalName,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'examination',
		hidden : false,
		header:biolims.master.examination,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hospitalPatient',
		hidden : false,
		header:biolims.master.hospitalPatient,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'keyResultsDescription',
		hidden : false,
		header:biolims.master.keyResultsDescription,
		width:25*10,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-id',
		hidden : true,
		header:biolims.master.patientId,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-name',
		hidden : true,
		header:biolims.common.patientName,
		width:10*10
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 80,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')&&rec.get('id')!='NEW'){
					
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=crmPatientRadiology&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		} ]
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/showCrmPatientRadiologyListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.radiologyDepartmentCheckId;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientRadiology.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientRadiologyGrid.getStore().commitChanges();
//				crmPatientRadiologyGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmPatientRadiologyGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientRadiology",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmPatientRadiologyGrid.getStore().commitChanges();
						crmPatientRadiologyGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientRadiologyGrid=gridEditTable("crmPatientRadiologydiv",cols,loadParam,opts);
	$("#crmPatientRadiologydiv").data("crmPatientRadiologyGrid", crmPatientRadiologyGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmPatientFun(){
	var win = Ext.getCmp('selectcrmPatient');
	if (win) {win.close();}
	var selectcrmPatient= new Ext.Window({
	id:'selectcrmPatient',modal:true,title:biolims.master.selectPatientId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmPatientSelect.action?flag=crmPatient' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmPatient.close(); }  }]  });     selectcrmPatient.show(); }
	function setcrmPatient(id,name){
		var gridGrid = $("#crmPatientRadiologydiv").data("crmPatientRadiologyGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmPatient-id',id);
			obj.set('crmPatient-name',name);
		});
		var win = Ext.getCmp('selectcrmPatient')
		if(win){
			win.close();
		}
	}
	
