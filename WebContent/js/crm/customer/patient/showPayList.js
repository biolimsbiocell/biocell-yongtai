var orderNum;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'isFee',
		type : "string"
	});
	fields.push({
		name : 'fee',
		type : "string"
	});
	fields.push({
		name : 'consumptionDate',
		type : "date",
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'feeWay',
		type : "string"
	});
	fields.push({
		name : 'monthFee',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'bankNum',
		type : "string"
	});
	fields.push({
		name : 'receipNum',
		type : "string"
	});
	fields.push({
		name : 'posNum',
		type : "string"
	});
	fields.push({
		name : 'backFee',
		type : "string"
	});
	fields.push({
		name : 'backFeeDate',
		type : "date",
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'invoiceCode',
		type : "string"
	});
	fields.push({
		name : 'invoiceDate',
		type : "date",
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'invoiceNum',
		type : "string"
	});
	fields.push({
		name : 'alipayNum',
		type : "string"
	});
	fields.push({
		name : 'realFee',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'financeUser-id',
		type : "string"
	});
	fields.push({
		name : 'financeUser-name',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-id',
		type : "string"
	});

	fields.push({
		name : 'sampleOrder-name',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-productName',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-commissioner-name',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-inspectionDepartment-name',
		type : "string"
	});

	fields.push({
		name : 'sampleOrder-medicalInstitutions',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-attendingDoctor',
		type : "string"
	});

	fields.push({
		name : 'sampleOrder-MedicalNumber',
		type : "string"
	});
	fields.push({
		name : 'sampleId',
		type : "string"
	});
	fields.push({
		name : 'source',
		type : "string"
	});
	
	cols.fields = fields;
	var cm = [];
	var gridIsFee = new Ext.form.ComboBox({
		transform : "grid_is_fee",
		width : 50,
		triggerAction : 'all',
		lazyRender : true
	});
	cm.push({
		dataIndex : 'isFee',
		header : biolims.master.isFee,
		width : 100,
		editor : gridIsFee,
		renderer : function(value, metadata, record, rowIndex, colIndex, store) {
			/*if (value == "0") {
				return "未收费";
			} else if (value == "4") {
				return "已付费未提交";
			} else */
			if(value=="0"){
				return biolims.master.nowPayPaid;
			}else if (value == "1") {
				return biolims.master.agentPaid;
			}else if (value == "2") {
				return biolims.common.cancel;
			}else if (value == "3") {
				return biolims.pooling.refund;
			}
			else {
				return biolims.common.pleaseChoose;
			}
			/*else if (value == "6") {
				return "延迟收费";
			} else if (value == "5") {
				return "月结";
			}   */
		}
	});
	cm.push({
		dataIndex : 'fee',
		header : biolims.master.fee,
		width : 150,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'consumptionDate',
		header : biolims.master.paymentTime,
		width : 100,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			maxValue : new Date(),
			format : 'Y-m-d'
		})
	});
	var gridfeeWay = new Ext.form.ComboBox({
		transform : "grid_fee_way",
		width : 50,
		triggerAction : 'all',
		lazyRender : true
	});
	cm.push({
		dataIndex : 'feeWay',
		header : biolims.sample.payTypeName,
		width : 100,
		editor : gridfeeWay,
		renderer : function(value, metadata, record, rowIndex, colIndex, store) {
			if (value == "0") {
				return biolims.master.suchIntermediaryBank;
			} else if (value == "1") {
				return biolims.master.posBankTransfer;
			} else if (value == "2") {
				return biolims.master.cash;
			} else if (value == "3") {
				return biolims.master.alipay;
			}
		}
	});
	cm.push({
		dataIndex : 'sampleId',
		header : biolims.common.sampleId,
		width : 150,
		hidden : true,
	});
	cm.push({
		dataIndex : 'source',
		header : biolims.master.source,
		width : 150,
		hidden : true,
	});
	
	cm.push({
		dataIndex : 'monthFee',
		header : biolims.master.MonthFee,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});

	cm.push({
		dataIndex : 'note',
		header : biolims.master.chargeNote,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'bankNum',
		header : biolims.master.bankCardNumber,
		width : 150,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'receipNum',
		header : biolims.master.receipNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'posNum',
		header : biolims.master.posNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'backFee',
		header : biolims.master.backFee,
		width : 150,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'backFeeDate',
		header : biolims.master.backFeeDate,
		width : 150,
		hidden : true,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			maxValue : new Date(),
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'invoiceCode',
		header : biolims.sample.billNumber,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'invoiceNum',
		header : biolims.sample.invoiceNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'invoiceDate',
		header : biolims.sample.invoiceDate,
		width : 150,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			// maxValue : new Date(),
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'alipayNum',
		header : biolims.master.alipayNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'realFee',
		header : biolims.master.realFee,
		width : 150,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 150,
		hidden: true,
//		editor : new Ext.form.TextField({
//			allowBlank : true,
//			allowNegative : false
//		})
	});
	cm.push({
		dataIndex : 'financeUser-id',
		header : biolims.master.financeUserId,
		width : 150,
		hidden : true
	});
	cm.push({
		dataIndex : 'financeUser-name',
		header : biolims.master.financeUserName,
		width : 150,
		hidden : true
	});


	var orderNum = new Ext.form.TextField({
		allowBlank : true
	});
	orderNum.on('focus',function(){
		getOrderNum();
	});
	cm.push({
		dataIndex : 'sampleOrder-id',
		header : biolims.sample.orderNum,
		width : 10 * 15,
		editor : orderNum
	});
	cm.push({
		dataIndex : 'sampleOrder-name',
		header : biolims.master.inspectionPeopleName,
		width : 10 * 15,

	});
	cm.push({
		dataIndex : 'sampleOrder-productName',
		header : biolims.common.productName,
		width : 20 * 15
	});
	cm.push({
		dataIndex : 'sampleOrder-commissioner-name',
		header : biolims.master.commissioner,
		width : 150
	});
	// cm.push({
	// dataIndex : 'sampleOrder-inspectionDepartment-name',
	// header : '科室',
	// width : 150
	// });
	// cm.push({
	// dataIndex : 'sampleOrder-medicalInstitutions',
	// header : '送检医院',
	// width : 150
	// });
	// cm.push({
	// dataIndex : 'sampleOrder-attendingDoctor',
	// header : '送检医师',
	// width : 150
	// });
	// cm.push({
	// dataIndex:'sampleOrder-MedicalNumber',
	// header:'电子病历号',
	// width:10*15,
	// hidden:true
	// });

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 100,
		hidden : true
	});

	//
	// cm.push({
	// dataIndex : 'crmPatient-qyFee',
	// header : '取样费',
	// width : 150,
	// editor : new Ext.form.NumberField({
	// allowBlank : true,
	// allowNegative : false
	// })
	// });

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/crm/customer/pay/showPayListJson.action";
	loadParam.limit = 10000;
	var opts = {};
	opts.title = biolims.master.charge4Confirmation;
	opts.height = document.body.clientHeight - 60;

	opts.tbar = [];
	
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	

	opts.tbar.push({
		text : biolims.sample.payTypeName,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_sampleInfo_div"), biolims.sample.payTypeName, null, {
				"Confirm" : function() {
					var inItemGrid = $("#show_detail_grid_div").data("customerInfoGrid");
					var records = inItemGrid.getSelectRecord();
					if (!records.length) {
						records = inItemGrid.getAllRecord();
					}
					if (records && records.length > 0) {
						var isGood = $("#isFee").val();
						inItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isFee", isGood);
						});
						inItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.master.realFee,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_fee_div"), biolims.master.realFee, null, {
				"Confirm" : function() {
					var inItemGrid = $("#show_detail_grid_div").data("customerInfoGrid");
					var records = inItemGrid.getSelectRecord();
					if (!records.length) {
						records = inItemGrid.getAllRecord();
					}
					if (records && records.length > 0) {
						var feeReal = $("#feeReal").val();
						inItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("realFee", feeReal);
						});
						inItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.master.paymentTime,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_consumption_div"), biolims.master.paymentTime, null, {
				"Confirm" : function() {
					var inItemGrid = $("#show_detail_grid_div").data("customerInfoGrid");
					var records = inItemGrid.getSelectRecord();
					if (!records.length) {
						records = inItemGrid.getAllRecord();
					}
					if (records && records.length > 0) {
						var consumptionDate = $("#skrq").val();
						var arr2 = consumptionDate.split("-");
						var date2 = new Date(arr2[0], parseInt(arr2[1]) - 1, arr2[2]);
						inItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("consumptionDate", date2);
						});
						inItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.sample.billNumber,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_invoice_div"), biolims.sample.billNumber, null, {
				"收款日期(Invoice No.)" : function() {
					var inItemGrid = $("#show_detail_grid_div").data("customerInfoGrid");
					var records = inItemGrid.getSelectRecord();
					if (!records.length) {
						records = inItemGrid.getAllRecord();
					}
					if (records && records.length > 0) {
						var invoiceNumber = $("#invoiceNumber").val();
						inItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("invoiceCode", invoiceNumber);
						});
						inItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.sample.invoiceNum,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_invoiceName_div"), biolims.sample.invoiceNum, null, {
				"发票编号(Invoice No.)" : function() {
					var inItemGrid = $("#show_detail_grid_div").data("customerInfoGrid");
					var records = inItemGrid.getSelectRecord();
					if (!records.length) {
						records = inItemGrid.getAllRecord();
					}
					if (records && records.length > 0) {
						var invoiceName = $("#invoiceName").val();
						inItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("invoiceNum", invoiceName);
						});
						inItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.sample.invoiceDate,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_fprq_div"), biolims.sample.invoiceDate, null, {
				"发票编号(Invoice No.)" : function() {
					var inItemGrid = $("#show_detail_grid_div").data("customerInfoGrid");
					var records = inItemGrid.getSelectRecord();
					if (!records.length) {
						records = inItemGrid.getAllRecord();
					}
					if (records && records.length > 0) {
						var invoiceDate = $("#kprq").val();
						var arr1 = invoiceDate.split("-");
						var date1 = new Date(arr1[0], parseInt(arr1[1]) - 1, arr1[2]);
						inItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("invoiceDate", date1);
						});
						inItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.crm.bindingOrder,
		handler : function() {
			
			getOrderNum();
		}
	});
	opts.tbar.push({
		text : "<font color='red'>" + biolims.common.save + "</font>",
		handler : function() {
			var inItemGrid = $("#show_detail_grid_div").data("customerInfoGrid");
			var itemJson = commonGetModifyRecords(inItemGrid);
			var grid=inItemGrid.store;
			for(var i=0;i<grid.getCount();i++){
				if(grid.getAt(i).get("isFee")=="0" && grid.getAt(i).get("fee")==""){
					message(biolims.crm.chargeAmountEmpty);
					return;
				}
			}
			if (itemJson == '') {
				message(biolims.crm.pleaseFillCharge);
			} else {
				ajax("post", "/crm/customer/pay/savePayInfo.action", {
					itemDataJson : itemJson
				}, function(data) {
					if (data.success) {
						$("#show_detail_grid_div").data("customerInfoGrid").getStore().commitChanges();
						$("#show_detail_grid_div").data("customerInfoGrid").getStore().reload();
						message(biolims.common.saveSuccess);
					} else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}

		}
	});
	opts.tbar.push({
		text : biolims.report.exportFile,
		handler : function() {
			exportexcel1();
		}
	});
	opts.tbar.push({
		text : biolims.crm.submitOrders,
		handler : function() {
			submitSample();
		}
	});

	var customerInfoGrid = gridEditTable("show_detail_grid_div", cols, loadParam, opts);

	$("#show_detail_grid_div").data("customerInfoGrid", customerInfoGrid);

	function exportexcel1() {
		customerInfoGrid.title = biolims.crm.chargeConfirmationDetailList;
		var vExportContent = customerInfoGrid.getExcelXml();
		var x = document.getElementById('gridhtm1');
		x.value = vExportContent;
		document.excelfrm1.submit();
	}
	/*orderNum = gridEditTable("show_detail_grid_div", cols, loadParam, opts);
	$("#show_detail_grid_div").data("orderNum", orderNum);*/
});
window.searchMainType = function(domId) {
	Ext.QuickTips.init();

	$("#" + domId).css("width", "80px");
}
window.searchFeeType = function(domId) {
	Ext.QuickTips.init();

	$("#" + domId).css("width", "80px");
}
function selectCustomer() {
	var grid = $("#show_detail_grid_div").data("customerInfoGrid");

	if (($("#sampleDate").val() != undefined) && ($("#sampleDate").val() != '')) {
		var startSendDatestr = ">=##@@##" + $("#sampleDate").val();

		$("#sampleDate1").val(startSendDatestr);
	}
	if (($("#endAcceptDate").val() != undefined) && ($("#endAcceptDate").val() != '')) {
		var endSendDatestr = "<=##@@##" + $("#endAcceptDate").val();

		$("#sampleDate2").val(endSendDatestr);

	}
	commonSearchAction(grid);

}
$(function() {
	searchMainType("mainType");
	searchFeeType("feeType");
});

function sampleReportFun() {
	location.href = ctx + "/sample/blood/customer/showCustomerInfnList.action?state_sam=166";

}
//订单号
function getOrderNum(){
	var win = Ext.getCmp('getOrderNum');
	if (win) {
		win.close();
	}
	var getUser = new Ext.Window(
			{
				id : 'getOrderNum',
				modal : true,
				title : biolims.master.selectOrder,
				layout : 'fit',
				width : document.body.clientWidth / 1.8,
				height : document.body.clientHeight / 1.2,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/pay/showChargeSampleList.action?flag=OrderNum' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						getUser.close();
					}
				} ]
			});
	getUser.show();
}
function setOrderNum(rec){
	records = $("#show_detail_grid_div").data("customerInfoGrid").getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("sampleOrder-id",rec.get("id"));
	});
	var win = Ext.getCmp('getOrderNum');
	if(win){win.close();}
}


//提交样本
function submitSample(){
	
	var customerInfoGrid = $("#show_detail_grid_div").data("customerInfoGrid");
	var record = customerInfoGrid.getSelectionModel().getSelections();
	if(record.length>0){
	if(customerInfoGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	
	for ( var i = 0; i < record.length; i++) {
		if(record[i].get("isFee")==""){
			message(biolims.crm.chargeIsEmpty);
			return;
		}
	}
	
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		ajax("post", "/crm/customer/pay/submitSample.action", {
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				customerInfoGrid.getStore().commitChanges();
				customerInfoGrid.getStore().reload();
				if(data.str==""){
					message(biolims.common.submitSuccess);
				}else{
					message(data.str);
				}
				
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);	
	}else{
		message(biolims.common.pleaseSelect);
	}
}