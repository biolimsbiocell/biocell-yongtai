var crmCustomerLinkManGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'linkMan_mail',
		type:"string"
	});
	    fields.push({
		name:'linkMan_name',
		type:"string"
	});
	    fields.push({
		name:'linkMan_telNumber',
		type:"string"
	});
	    fields.push({
		name:'linkMan_post',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.linkmanId,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'linkMan_name',
		hidden : false,
		header:biolims.common.linkmanName,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'linkMan_telNumber',
		hidden : false,
		header:biolims.common.linkMan_telNumber,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'linkMan_post',
		hidden : false,
		header:biolims.common.linkMan_post,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'linkMan_mail',
		hidden : false,
		header:biolims.common.linkMan_mail,
		hidden:true,
		width:10*10
	});
	cols.cm=cm;
	var loadParam={};
	//showCrmOrderListJson  
	loadParam.url=ctx+"/crm/customer/patient/showCrmPatientLinkManListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.linkmanName;
	opts.height =  220;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/customer/delCrmCustomerLinkMan.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				scpProToGrid.getStore().commitChanges();
//				scpProToGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmCustomerLinkManGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientLinkMan",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmCustomerLinkManGrid.getStore().commitChanges();
						crmCustomerLinkManGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmCustomerLinkManGrid=gridEditTable("crmCustomerLinkMandiv",cols,loadParam,opts);
	$("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid", crmCustomerLinkManGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmCustomerFun(){
	var win = Ext.getCmp('selectcrmCustomer');
	if (win) {win.close();}
	var selectcrmCustomer= new Ext.Window({
	id:'selectcrmCustomer',modal:true,title:biolims.master.selectCustomer,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmCustomerSelect.action?flag=crmCustomer' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmCustomer.close(); }  }]  });     selectcrmCustomer.show(); }
	function setcrmCustomer(id,name){
		var gridGrid = $("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmCustomer-id',id);
			obj.set('crmCustomer-name',name);
		});
		var win = Ext.getCmp('selectcrmCustomer')
		if(win){
			win.close();
		}
	}
	
function selectlinkManIdFun(){
	var win = Ext.getCmp('selectlinkManId');
	if (win) {win.close();}
	var selectlinkManId= new Ext.Window({
	id:'selectlinkManId',modal:true,title:biolims.master.selectLinkmanId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmLinkManSelect.action?flag=linkManId' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectlinkManId.close(); }  }]  });     selectlinkManId.show(); }
	function setlinkManId(id,name){
		var gridGrid = $("#crmCustomerLinkMandiv").data("crmCustomerLinkManGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('linkManId-id',id);
			obj.set('linkManId-name',name);
		});
		var win = Ext.getCmp('selectlinkManId')
		if(win){
			win.close();
		}
	}
	
