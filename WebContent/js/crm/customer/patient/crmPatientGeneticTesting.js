var crmPatientGeneticTestingGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'dateOfTest',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'hospitalOrCompany',
		type:"string"
	});
	   fields.push({
		name:'test',
		type:"string"
	});
	   fields.push({
		name:'biomarker',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'treatment',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-id',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-name',
		type:"string"
	});
	    fields.push({
			name:'locus',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.master.geneticTestingId,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
		var methodstore = new Ext.data.JsonStore({
					root : 'results',
					remoteSort : true,
					fields : [ {
						name : 'id'
					}, {
						name : 'name'
					} ],
					proxy : new Ext.data.HttpProxy({
						url : window.ctx + '/dic/type/dicTypeCobJson.action?type=jyjcjcxm',
						method : 'POST'
						})
					});

					methodstore.load();
					var methodcob = new Ext.form.ComboBox({
						store : methodstore,
						displayField : 'name',
						valueField : 'id',
						typeAhead : true,
						mode : 'remote',
						forceSelection : true,
						triggerAction : 'all',
						selectOnFocus : true
					});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:biolims.common.sequencingFun,
		width:15*10,
		
		renderer: Ext.util.Format.comboRenderer(methodcob),editor: methodcob
	});
	cm.push({
		dataIndex:'dateOfTest',
		hidden : false,
		header:biolims.common.dateOfTest,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'hospitalOrCompany',
		hidden : false,
		header:biolims.master.hospitalOrCompany,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'test',
		hidden : false,
		header:biolims.master.test,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
		var biomarkerstore = new Ext.data.JsonStore({
					root : 'results',
					remoteSort : true,
					fields : [ {
						name : 'id'
					}, {
						name : 'name'
					} ],
					proxy : new Ext.data.HttpProxy({
						url : window.ctx + '/dic/type/dicTypeCobJson.action?type=swbzw',
						method : 'POST'
						})
					});

					biomarkerstore.load();
					var biomarkercob = new Ext.form.ComboBox({
						store : biomarkerstore,
						displayField : 'name',
						valueField : 'id',
						typeAhead : true,
						mode : 'remote',
						forceSelection : true,
						triggerAction : 'all',
						selectOnFocus : true
					});
	cm.push({
		dataIndex:'biomarker',
		hidden : false,
		header:biolims.master.biomarker,
		width:15*10,
		
		renderer: Ext.util.Format.comboRenderer(biomarkercob),editor: biomarkercob
	});
	cm.push({
		dataIndex:'subjectGene',
		hidden : false,
		header:biolims.master.subjectGene,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'locus',
		hidden : false,
		header:biolims.sanger.chromosomalLocation,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'treatment',
		hidden : false,
		header:biolims.master.treatment,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-id',
		hidden : true,
		header:biolims.master.patientId,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-name',
		hidden : true,
		header:biolims.common.patientName,
		width:10*10
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:25*10,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 100,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')&&rec.get('id')!='NEW'){
					
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=crmPatientGenetic&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		} ]
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/showCrmPatientGeneticTestingListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.molecularPathologicalExamination;
	opts.height =  document.body.clientHeight-400;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientGeneticTesting.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientGeneticTestingGrid.getStore().commitChanges();
//				crmPatientGeneticTestingGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmPatientGeneticTestingGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientGeneticTesting",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmPatientGeneticTestingGrid.getStore().commitChanges();
						crmPatientGeneticTestingGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientGeneticTestingGrid=gridEditTable("crmPatientGeneticTestingdiv",cols,loadParam,opts);
	$("#crmPatientGeneticTestingdiv").data("crmPatientGeneticTestingGrid", crmPatientGeneticTestingGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmPatientFun(){
	var win = Ext.getCmp('selectcrmPatient');
	if (win) {win.close();}
	var selectcrmPatient= new Ext.Window({
	id:'selectcrmPatient',modal:true,title:biolims.master.selectPatientId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmPatientSelect.action?flag=crmPatient' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmPatient.close(); }  }]  });     selectcrmPatient.show(); }
	function setcrmPatient(id,name){
		var gridGrid = $("#crmPatientGeneticTestingdiv").data("crmPatientGeneticTestingGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmPatient-id',id);
			obj.set('crmPatient-name',name);
		});
		var win = Ext.getCmp('selectcrmPatient')
		if(win){
			win.close();
		}
	}
	
