/* 
 * 文件描述: 单位组的弹框
 * 
 */	
var addSampleOrder;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.orderCode,
	});
	colOpts.push({
		"data": "barcode",
		"title": "批次号",
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "gender",
		"title": biolims.common.gender,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "age",
		"title": biolims.common.age,
	});
	colOpts.push({
		"data": "productName",
		"title": "产品名称",
	});
	colOpts.push({
		"data": "ccoi",
		"title": "CCOI",
	});
	colOpts.push({
		"data": "round",
		"title": "采血轮次",
	});
	colOpts.push({
		"data": "filtrateCode",
		"title": "筛选号",
	});
	colOpts.push({
		"data": "randomCode",
		"title": "随机号",
	});
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.state,
	});
	var tbarOpts = [];

	tbarOpts.push({
		text:"搜索",
		action: search,
	});
	
	var options = table(false, null,
			'/crm/customer/patient/showSampleOrderDialogListJson.action', colOpts, tbarOpts)
	addSampleOrder = renderData($("#addSampleOrder"), options);
	
	$("#addSampleOrder").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			//$("#addSampleOrder_wrapper .dt-buttons").empty();
			$('#addSampleOrder_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addSampleOrder tbody tr");
			
			addSampleOrder.ajax.reload();
			addSampleOrder.on('draw', function() {
				trs = $("#addSampleOrder tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "姓名",
			"type": "input",
			"searchName": "name"
		},
		{
			"txt": "批次号",
			"type": "input",
			"searchName": "barcode"
		},
		{
			"txt": "CCOI",
			"type": "input",
			"searchName": "ccoi"
//		},
//		{
//			"txt": biolims.common.gender,
//			"type": "select",
//			"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
//			"changeOpt": "''|1|0|3",
//			"searchName": "gender"
		},
		{
			"txt": "筛选号",
			"type": "input",
			"searchName": "filtrateCode"
		},
		{
			"txt": "随机号",
			"type": "input",
			"searchName": "randomCode"
		},
		{
			"type": "table",
			"table": addSampleOrder
		}
	];
}