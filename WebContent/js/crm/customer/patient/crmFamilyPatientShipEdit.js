$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/crm/customer/patient/crmFamilyPatientShip/editCrmFamilyPatientShip.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/customer/patient/crmFamilyPatientShip/showCrmFamilyPatientShipList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#crmFamilyPatientShip", {
					userId : userId,
					userName : userName,
					formId : $("#crmFamilyPatientShip_id").val(),
					title : $("#crmFamilyPatientShip_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#crmFamilyPatientShip_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/crm/customer/patient/crmFamilyPatientShip/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/crm/customer/patient/crmFamilyPatientShip/copyCrmFamilyPatientShip.action?id=' + $("#crmFamilyPatientShip_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#crmFamilyPatientShip_id").val() + "&tableId=crmFamilyPatientShip");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmFamilyPatientShip_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.familyRelationTable,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);