var crmPatientTreatGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'scheme',
		type : "string"
	});
	fields.push({
		name : 'begin',
		type : "date",
		dateFormat:'Y-m-d'
	});
	fields.push({
		name : 'end',
		type : "date",
		dateFormat:'Y-m-d'
	});
	fields.push({
		name : 'treat-id',
		type : "string"
	});
	fields.push({
		name : 'crmPatient-id',
		type : "string"
	});
	fields.push({
		name : 'crmPatientDiagnosis-id',
		type : "string"
	});
	fields.push({
		name : 'crmPatientDiagnosis-diagnosis',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : 'id',
		width : 15 * 10,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var treatstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=zl',
			method : 'POST'
		})
	});
	treatstore.load();
	var treatcob = new Ext.form.ComboBox({
		store : treatstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'treat-id',
		header : biolims.master.treatId,
		width : 10 * 10,
		renderer : Ext.util.Format.comboRenderer(treatcob),
		editor : treatcob
	});
	cm.push({
		dataIndex:'begin',
		hidden : false,
		header:biolims.common.startTime,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'end',
		hidden : false,
		header:biolims.common.endTime,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'scheme',
		hidden : false,
		header:biolims.master.scheme,
		width:25*10,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmPatient-id',
		hidden : true,
		header : biolims.master.patientId,
		width : 10 * 10
	});
	cm.push({
		dataIndex : 'crmPatientDiagnosis-id',
		hidden : true,
		header : biolims.master.crmPatientDiagnosisId,
		width : 10 * 10
	});
	cm.push({
		dataIndex : 'crmPatientDiagnosis-diagnosis',
		header : biolims.master.crmPatientDiagnosisNote,
		width : 10 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/customer/patient/showCrmPatientTreatListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.master.treatmentOptions;
	opts.height = document.body.clientHeight - 300;
	opts.tbar = [];
//	opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientDiagnosis.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				scpProToGrid.getStore().commitChanges();
//				scpProToGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		iconCls : 'add',
//		handler : function() {
//				var records = crmPatientDiagnosisGrid.getSelectRecord()
//				if (records && records.length > 0) {
//					$.each(records, function(i, obj) {
//						var ob = crmPatientTreatGrid.getStore().recordType;
//						var p = new ob({});
//						p.isNew = true;
//						p.set("crmPatientDiagnosis-id", obj.get("id"));
//						p.set("crmPatientDiagnosis-diagnosis", obj.get("diagnosis"));
//						crmPatientTreatGrid.stopEditing();
//						crmPatientTreatGrid.getStore().insert(0, p);
//						crmPatientTreatGrid.startEditing(0, 0);
//					});
//				}
//				else{
//					var ob = crmPatientTreatGrid.getStore().recordType;
//					var p = new ob({});
//					p.isNew = true;
//					crmPatientTreatGrid.stopEditing();
//					crmPatientTreatGrid.getStore().insert(0, p);
//					crmPatientTreatGrid.startEditing(0, 0);
//				}
//			}
//	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmPatientTreatGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientTreat",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmPatientTreatGrid.getStore().commitChanges();
						crmPatientTreatGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientTreatGrid = gridEditTable("crmPatientTreatdiv", cols,
			loadParam, opts);
	$("#crmPatientTreatdiv").data("crmPatientTreatGrid",
			crmPatientTreatGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmPatientFun() {
	var win = Ext.getCmp('selectcrmPatient');
	if (win) {
		win.close();
	}
	var selectcrmPatient = new Ext.Window(
			{
				id : 'selectcrmPatient',
				modal : true,
				title : biolims.master.selectPatientId,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/CrmPatientSelect.action?flag=crmPatient' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						selectcrmPatient.close();
					}
				} ]
			});
	selectcrmPatient.show();
}
function setcrmPatient(id, name) {
	var gridGrid = $("#crmPatientTreatdiv").data("crmPatientTreatGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('crmPatient-id', id);
		obj.set('crmPatient-name', name);
	});
	var win = Ext.getCmp('selectcrmPatient')
	if (win) {
		win.close();
	}
}
