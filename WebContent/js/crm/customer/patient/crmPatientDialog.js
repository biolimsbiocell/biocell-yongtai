$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.sample.medicalNumber,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "gender",
		"title": biolims.common.gender,
		"name": biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			}
			if(data == "") {
				return '';
			}
		},
	});
	colOpts.push({
		"data": "dateOfBirth",
		"title": biolims.common.birthDay,
	});
	colOpts.push({
		"data": "age",
		"title": biolims.common.age,
	});
	colOpts.push({
		"data": "adultTeenager",
		"title": biolims.crmPatient.adultTeenager,
	});
	var tbarOpts = [];
	var options = table(false, null,
		'/crm/customer/patient/showCrmPatientDialogListJson.action', colOpts, null)
	var addCrmPatient = renderData($("#addCrmPatient"), options);
	$("#addCrmPatient").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addCrmPatient_wrapper .dt-buttons").empty();
			$('#addCrmPatient_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addCrmPatient tbody tr");
			addCrmPatient.ajax.reload();
			addCrmPatient.on('draw', function() {
				trs = $("#addCrmPatient tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});
})