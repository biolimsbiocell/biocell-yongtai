var crmPatientHistoryGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'familyRelation',
		type:"string"
	});
	    fields.push({
		name:'tumorCategory-id',
		type:"string"
	});
	    fields.push({
		name:'tumorCategory-name',
		type:"string"
	});
	   fields.push({
		name:'checkOutTheAge',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-id',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'familyRelation',
		hidden : false,
		header:biolims.sample.familyRelation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tumorCategory-id',
		hidden : true,
		header:biolims.sample.dicTypeId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tumorCategory-name',
		hidden : false,
		header:biolims.sample.dicTypeName,
		width:20*10
	});
	cm.push({
		dataIndex:'checkOutTheAge',
		hidden : false,
		header:biolims.sample.checkOutTheAge,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-name',
		hidden : false,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/crmPatient/showCrmPatientHistoryListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.familyHistory;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/customer/patient/crmPatient/delCrmPatientHistory.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
			text : biolims.sample.selectTumorCategory,
				handler : selecttumorCategoryFun
		});
	opts.tbar.push({
			text : biolims.common.selectRelevantTable,
				handler : selectcrmPatientDialogFun
		});
	opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),biolims.common.batchUpload,null,{
				"Confirm":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = crmPatientHistoryGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
							crmPatientHistoryGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientHistoryGrid=gridEditTable("crmPatientHistorydiv",cols,loadParam,opts);
	$("#crmPatientHistorydiv").data("crmPatientHistoryGrid", crmPatientHistoryGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selecttumorCategoryFun(){
	var win = Ext.getCmp('selecttumorCategory');
	if (win) {win.close();}
	var selecttumorCategory= new Ext.Window({
	id:'selecttumorCategory',modal:true,title:biolims.sample.selectTumorCategory,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=tumorCategory' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selecttumorCategory.close(); }  }]  }) ;  
    selecttumorCategory.show(); }
	function settumorCategory(rec){
		var gridGrid = $("#crmPatientHistorydiv").data("crmPatientHistoryGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('tumorCategory-id',rec.get('id'));
			obj.set('tumorCategory-name',rec.get('name'));
		});
		var win = Ext.getCmp('selecttumorCategory')
		if(win){
			win.close();
		}
	}
	function selecttumorCategoryDialogFun(){
			var title = '';
			var url = '';
			title = biolims.sample.selectTumorCategory;
			url = ctx + "/DicTypeSelect.action?flag=tumorCategory";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						seltumorCategoryVal(this);
				}
			}, true, option);
		}
	var seltumorCategoryVal = function(win) {
		var operGrid = dicTypeDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#crmPatientHistorydiv").data("crmPatientHistoryGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('tumorCategory-id',rec.get('id'));
				obj.set('tumorCategory-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
function selectcrmPatientFun(){
	var win = Ext.getCmp('selectcrmPatient');
	if (win) {win.close();}
	var selectcrmPatient= new Ext.Window({
	id:'selectcrmPatient',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmPatient.close(); }  }]  }) });  
    selectcrmPatient.show(); }
	function setcrmPatient(rec){
		var gridGrid = $("#crmPatientHistorydiv").data("crmPatientHistoryGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmPatient-id',rec.get('id'));
			obj.set('crmPatient-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectcrmPatient')
		if(win){
			win.close();
		}
	}
	function selectcrmPatientDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/CrmPatientSelect.action?flag=crmPatient";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selcrmPatientVal(this);
				}
			}, true, option);
		}
	var selcrmPatientVal = function(win) {
		var operGrid = crmPatientDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#crmPatientHistorydiv").data("crmPatientHistoryGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('crmPatient-id',rec.get('id'));
				obj.set('crmPatient-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
