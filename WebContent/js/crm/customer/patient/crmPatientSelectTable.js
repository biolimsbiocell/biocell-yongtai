var CrmPatientTable;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.sample.medicalNumber,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "id");
			$(td).attr("sfz", rowData['sfz']);
			$(td).attr("placeOfBirth", rowData['placeOfBirth']);
			$(td).attr("tumourType-id", rowData['tumourType-id']);
			$(td).attr("tumourType-name", rowData['tumourType-name']);
			$(td).attr("customer-id", rowData['customer-id']);
			$(td).attr("ks-id", rowData['ks-id']);
			$(td).attr("customerDoctor-id", rowData['customerDoctor-id']);
			$(td).attr("ccoi", rowData['ccoi']);
			$(td).attr("customer-crmPhone", rowData['customer-crmPhone']);
			$(td).attr("customer-street", rowData['customer-street']);
			$(td).attr("customerDoctor-mobile", rowData['customerDoctor-mobile']);
			$(td).attr("abbreviation", rowData['abbreviation']);
			$(td).attr("crmPhone", rowData['crmPhone']);
			
			$(td).attr("round", rowData['round']);
			$(td).attr("randomCode", rowData['randomCode']);
			$(td).attr("hepatitisHbv", rowData['hepatitisHbv']);
			$(td).attr("hepatitisHcv", rowData['hepatitisHcv']);
			$(td).attr("HivVirus", rowData['HivVirus']);
			$(td).attr("syphilis", rowData['syphilis']);
			$(td).attr("whiteBloodCellNum", rowData['whiteBloodCellNum']);
			$(td).attr("percentageOfLymphocytes", rowData['percentageOfLymphocytes']);
			$(td).attr("lymphoidCellSeries", rowData['lymphoidCellSeries']);
			$(td).attr("counterDrawBlood", rowData['counterDrawBlood']);
			$(td).attr("heparinTube", rowData['heparinTube']);
			$(td).attr("tumorStaging-id", rowData['tumorStaging-id']);
			$(td).attr("tumorStaging-name", rowData['tumorStaging-name']);
			$(td).attr("zhongliuNote", rowData['zhongliuNote']);
			$(td).attr("tumorStagingNote", rowData['tumorStagingNote']);
			$(td).attr("family", rowData['family']);
			$(td).attr("familyPhone", rowData['familyPhone']);
			$(td).attr("familySite", rowData['familySite']);
			$(td).attr("email", rowData['email']);
			$(td).attr("zipCode", rowData['zipCode']);
			$(td).attr("medicationPlan", rowData['medicationPlan']);
			
			$(td).attr("productId", rowData['productId']);
			$(td).attr("productName", rowData['productName']);
			
			$(td).attr("hospitalPatientID", rowData['hospitalPatientID']);
			

	



			
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.crmPatient.name,
	});
	colOpts.push({
		"data" : "gender",
		"title" : biolims.crmPatient.gender,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			}
			if(data == "") {
				return '';
			}
		}
	});
	colOpts.push({
		"data" : "dateOfBirth",
		"title" : biolims.crmPatient.dateOfBirth,
	});
	colOpts.push({
		"data" : "age",
		"title" : "建档年龄",
	});
	colOpts.push({
		"data" : "race",
		"title" : biolims.common.race,
	});
	colOpts.push({
		"data" : "filtrateCode",
		"title" : "筛选号",
	});
	colOpts.push({
		"data" : "familyId-id",
		"title" : biolims.sample.personShipCode,
	});
	colOpts.push({
		"data" : "customer-name",
		"title" : "医院名称",
	});
	colOpts.push({
		"data" : "ks-name",
		"title" : "科室名称",
	});
	colOpts.push({
		"data" : "customerDoctor-name",
		"title" : "主治医生",
	});
	colOpts.push({
		"data" : "infectionScreening",
		"title" : "感染筛查",
	});
	colOpts.push({
		"data" : "tumourType-name",
		"title" : "肿瘤类型",
	});
	var tbarOpts = [];
	var addCrmPatientOptions = table(false, null,
			'/crm/customer/patient/crmPatientSelectTableJson.action?name='+encodeURIComponent(encodeURIComponent($(
			"#code").val())),
			colOpts, tbarOpts)
	CrmPatientTable = renderData($("#addPatientTable"), addCrmPatientOptions);
	$("#addPatientTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addPatientTable_wrapper .dt-buttons").empty();
				$('#addPatientTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addPatientTable tbody tr");
			CrmPatientTable.ajax.reload();
			CrmPatientTable.on('draw', function() {
				trs = $("#addPatientTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})
function query(){
	var filtrateCode = $.trim($("#filtrateCode").val());
	var searchItemLayerValue = {};
	var k1 = $("#filtrateCode").attr("searchname");
	if(filtrateCode != null && filtrateCode != "") {
		searchItemLayerValue[k1] = "%" + filtrateCode + "%";
	} else {
		searchItemLayerValue[k1] = filtrateCode;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	sessionStorage.setItem("searchContent", query);
	CrmPatientTable.settings()[0].ajax.data = param;
	CrmPatientTable.ajax.reload();
}