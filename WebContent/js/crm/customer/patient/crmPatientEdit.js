function addLoadEvent(func) {
	var oldonload = window.onload;
//	if (typeof window.onload != 'function') {
//		window.onload = func;
//	} else {
//		window.onload = function() {
//			oldonload();
//			func();
//		};
//	}
	if($("#open").val()=="Y"){
		$("#btn_save").hide();
		$(document).attr('title','电子病历详细信息')
	}
	if($("#crmPatient_infectiousDiseases").val()=="1"){
		$("#infectiousDiseasestext").show();
	}else{
		$("#infectiousDiseasestext").hide();
	}
	
	if($("#crmPatient_historyOfSmoking").val()=="1"){
		$("#historyOfSmoking").show();
	}else{
		$("#historyOfSmoking").hide();
	}
	
	if($("#crmPatient_environmentalExposures").val()=="1"){
		$("#environmentalExposures").show();
	}else{
		$("#environmentalExposures").hide();
	}
	
	if($("#crmPatient_ysj").val()=="1"){
		$("#ysj").show();
	}else{
		$("#ysj").hide();
	}
	
	if($("#crmPatient_otherDiseases").val()=="1"){
		$("#otherDiseases").show();
	}else{
		$("#otherDiseases").hide();
	}
	$(".step").click(
			function() {
				if($(this).hasClass("done")) {
					var index = $(".wizard_steps .step").index($(this));
					$(".wizard_steps .selected").removeClass("selected")
						.addClass("done");
					$(this).removeClass("done").addClass("selected");
					$(".HideShowPanel").eq(index).slideDown().siblings(
						'.HideShowPanel').slideUp();
					//table刷新和操作隐藏
					console.log(index);
					if(index == 0) {
						$(".box-tools").hide();
					} else {
						$(".box-tools").hide();
					}
					$('body,html').animate({
						scrollTop: 0
					}, 500, function() {
						$(".HideShowPanel").each(function() {
							if($(this).css("display") == "block") {
								var table = $(this).find(".dataTables_scrollBody").children("table").attr("id");
								$("#" + table).DataTable().ajax.reload();
							}
						});
					});

				} else if($(this).hasClass("disabled")) {
					top.layer.msg("请先完成上一步");
				}
			});
	layui.use('form', function() {
		var form = layui.form;
		// 控制性别选择   婚姻情况
		form.on('checkbox', function(data) {
			$(data.othis[0]).siblings(".layui-form-checkbox").removeClass(
				"layui-form-checked");
			
			if(data.elem.checked) {
				if($(data.elem).attr("note")=="1"){
					$("#crmPatient_gender").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="2"){
					$("#crmPatient_maritalStatus").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="3"){
					$("#crmPatient_consentReceived").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="4"){
					if(data.elem.value=="1"){
						$("#infectiousDiseasestext").show();
					}else{
						$("#infectiousDiseasestext").hide();
						$("#crmPatient_infectiousDiseasestext").val("");
					}
					$("#crmPatient_infectiousDiseases").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="5"){
					if(data.elem.value=="1"){
						$("#historyOfSmoking").show();
					}else{
						$("#historyOfSmoking").hide();
						$("#crmPatient_historyOfSmokingtext").val("");
					}
					$("#crmPatient_historyOfSmoking").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="6"){
					if(data.elem.value=="1"){
						$("#environmentalExposures").show();
					}else{
						$("#environmentalExposures").hide();
						$("#crmPatient_environmentalExposurestext").val("");
					}
					$("#crmPatient_environmentalExposures").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="7"){
					if(data.elem.value=="1"){
						$("#ysj").show();
					}else{
						$("#ysj").hide();
						$("#crmPatient_ysjtext").val("");
					}
					$("#crmPatient_ysj").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="8"){
					if(data.elem.value=="1"){
						$("#otherDiseases").show();
					}else{
						$("#otherDiseases").hide();
						$("#crmPatient_otherDiseasestext").val("");
					}
					$("#crmPatient_otherDiseases").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="9"){
					$("#crmPatient_PFSStatus").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="10"){
					$("#crmPatient_statusatFollowup").val(data.elem.value);
				}
				if($(data.elem).attr("note")=="11"){
					$("#crmPatient_state").val(data.elem.value);
				}
			}
		});
	});
	$("#crmPatient_dateOfBirth").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
	fieldCustomFun();
	
	$("#crmPatient_createDate").datepicker({
		language: "zh-TW",
		autoclose: false, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	if(handlemethod == "modify") {
		$("#crmPatient_id").prop("readonly", "readonly");
	}

}
var mainFileInput;
function fileUp() {
	if(!$("#crmPatient_id").val()){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('1', 'crmPatient', $("#crmPatient_id").val());
	mainFileInput.off("fileuploaded");
}
function fileUp2() {
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('2', 'crmPatient', $("#crmPatient_id").val());
	mainFileInput.off("fileuploaded");
}
function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=crmPatient&id=" + $("#crmPatient_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index);
		}
	});
}
function fileView2() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=2&modelType=crmPatient&id=" + $("#crmPatient_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index);
		}
	});
}
function buildSelect(ids, sel) {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "add") {
		if (ids == null || ids == ' ') {
			ids = 0;
		}
	}
	if (sel == null || ids == ' ') {
		sel = '#selA';
		jQuery("#selA").empty();
		jQuery("#selB").empty();
		jQuery("#selC").empty();
		jQuery("#selA").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
		jQuery("#selB").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
		jQuery("#selC").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
	}
	// alert(sel =='#selB');
	if (sel == '#selB') {
		document.getElementById("crmPatient_selA").value = ids;
		jQuery("#selB").empty();
		jQuery("#selC").empty();
		jQuery("#selB").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
		jQuery("#selC").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
	}
	if (sel == '#selC') {
		document.getElementById("crmPatient_selB").value = ids;
		jQuery("#selC").empty();
		jQuery("#selC").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
	}
	if (sel == 'sel') {
		document.getElementById("crmPatient_selC").value = ids;
	} else {
		$.post("/crm/customer/customer/citySelect.action", {
			"data" : ids
		}, function(data) {
			var array = data.message.split(",");
			for ( var i = 1; i < array.length; i++) {
				jQuery(sel).append('<option value="' + array[i] + '">' + array[i + 1] + '</option>');
				i = i + 1;
			}
		}, "json");
	}
	// if( $("#handlemethod").val() != "add"&&ids==0){
	//		
	// var value = $("#crmCustomer_selA").val();
	// alert(value);
	// $('#selA option[value="'+value+'"] ').attr('selected',true);
	// $("#selA option[value='"+value+"'] ").attr("selected",true);
	// var ops = document.getElementById("selA").options;
	// for(var i=0;i<ops.length; i++){
	// var tempValue = ops[i].value;
	// alert(ops.length);
	// if(tempValue == value)
	// { alert(value);
	// ops[i].selected = true;
	// }
	// }
	// }
}
function buildSelectnoadd() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "add") {
		jQuery("#selA").empty();
		jQuery("#selB").empty();
		jQuery("#selC").empty();
		jQuery("#selA").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
		jQuery("#selB").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
		jQuery("#selC").append("<option value='0'>--------"+ biolims.common.pleaseChoose+ "--------</option>");
		var valueA = $("#crmPatient_selA").val();
		var valueB = $("#crmPatient_selB").val();
		var valueC = $("#crmPatient_selC").val();
		$.post("/crm/customer/customer/citySelect.action", {
			"data" : 0
		}, function(data) {
			var array = data.message.split(",");
			for ( var i = 1; i < array.length; i++) {
				if (array[i] == valueA) {
					jQuery("#selA").append('<option value="' + array[i] + '" selected>' + array[i + 1] + '</option>');
				} else {
					jQuery("#selA").append('<option value="' + array[i] + '">' + array[i + 1] + '</option>');
				}
				i = i + 1;
			}
		}, "json");
		$.post("/crm/customer/customer/citySelect.action", {
			"data" : valueA
		}, function(data) {
			var array = data.message.split(",");
			for ( var i = 1; i < array.length; i++) {
				if (array[i] == valueB) {
					jQuery("#selB").append('<option value="' + array[i] + '" selected>' + array[i + 1] + '</option>');
				} else {
					jQuery("#selB").append('<option value="' + array[i] + '">' + array[i + 1] + '</option>');
				}
				i = i + 1;
			}
		}, "json");
		$.post("/crm/customer/customer/citySelect.action", {
			"data" : valueB
		}, function(data) {
			var array = data.message.split(",");
			for ( var i = 1; i < array.length; i++) {
				if (array[i] == valueC) {
					jQuery("#selC").append('<option value="' + array[i] + '" selected>' + array[i + 1] + '</option>');
				} else {
					jQuery("#selC").append('<option value="' + array[i] + '">' + array[i + 1] + '</option>');
				}
				i = i + 1;
			}
		}, "json");
		// if( $("#handlemethod").val() != "add"&&ids==0){
		//			
		// var value = $("#crmCustomer_selA").val();
		// alert(value);
		// $('#selA option[value="'+value+'"] ').attr('selected',true);
		// $("#selA option[value='"+value+"'] ").attr("selected",true);
		// var ops = document.getElementById("selA").options;
		// for(var i=0;i<ops.length; i++){
		// var tempValue = ops[i].value;
		// alert(ops.length);
		// if(tempValue == value)
		// { alert(value);
		// ops[i].selected = true;
		// }
		// }
		// }
	}
}
addLoadEvent(buildSelect);
//addLoadEvent(buildSelectnoadd);
$(function() {
	if ($("#isSaleManage").val() == "true" || $("#filingUser").val() == $("#crmPatient_createUser").val()) {
		for ( var i = 1; i <= 30; i++) {
			document.getElementById("xing" + i).style.display = "inline";
		}
	}
});
//$(function() {
//	$("#tabs").tabs({
//		select : function(event, ui) {
//		}
//	});
//});
//$(function() {
//	// jiazaiOrder();
//	$("#tabss").tabs({
//		select : function(event, ui) {
//		}
//	});
//});

function jiazaiOrder() {
	var id = $("#crmPatient_id").val();
	if ($("#first").val() == null || $("#first").val() == "") {
		
		window.location = window.ctx + '/crm/customer/patient/copyCrmPatient.action?id=' + $("#crmPatient_id").val();
	}
	if ($("#first2").val() == null || $("#first2").val() == "") {
		
	}
}

function add() {
	window.location = window.ctx + "/crm/customer/patient/editCrmPatient.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/crm/customer/patient/showCrmPatientList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {

	save();
});
function save() {
	//自定义字段
	//拼自定义字段儿（实验记录）
	var inputs = $("#fieldItemDiv input");
	var options = $("#fieldItemDiv option");
	var contentData = {};
	var checkboxArr = [];
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
		
		// 校验筛选号和随机号
		
		if($("#crmPatient_filtrateCode").val().length != 5){
			top.layer.msg("筛选号长度为5位！");

			return false;
		}
		// 随机号
		if($("#crmPatient_randomCode").val()!= '' && $("#crmPatient_randomCode").val().length != 4){
			top.layer.msg("随机号长度为4位！");

			return false;
		}
		// 校验肿瘤类型
		if($("#crmPatient_hepatitisHbv").val()==""||$("#crmPatient_hepatitisHbv").val()==null){
			top.layer.msg("请选择乙肝HBV！");
			return false;
		}
		if($("#crmPatient_hepatitisHcv").val()==""||$("#crmPatient_hepatitisHcv").val()==null){
			top.layer.msg("请选择丙肝HCV！");
			return false;
		}
		if($("#crmPatient_HivVirus").val()==""||$("#crmPatient_HivVirus").val()==null){
			top.layer.msg("请选择艾滋HIV！");

			return false;
		}
		if($("#crmPatientr_syphilis").val()==""||$("#crmPatient_syphilis").val()==null){
			top.layer.msg("请选择梅毒TPHA！");
			return false;
		}
		var a=/[^a-za-z0-9u4e00-u9fa5@.]/g;
		if(a.test($("#crmPatient_percentageOfLymphocytes").val())){
			top.layer.msg("淋巴细胞百分比不能输入符号！");
			return false;
		}
		if($("#crmPatient_customer_name").val()==""||$("#crmPatient_customer_name").val()==null){
			top.layer.msg("请选择医疗机构！");
			return false;
		}
		if($("#crmPatient_ks_name").val()==""||$("#crmPatient_ks_name").val()==null){
			top.layer.msg("请选择科室！");
			return false;
		}	
		
	$("#fieldItemDiv .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	document.getElementById("fieldContent").value = JSON.stringify(contentData);
	
	var changeLog = biolims.master.EMR+"-";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	document.getElementById("changeLog").value = changeLog;
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "modify"&&checkSubmit() == true) {
			document.getElementById('crmLinkManItemJson').value = savecrmLinkManItemjson($("#crmLinkManItemTable"));
			console.log($("#crmLinkManItemJson").val());
			top.layer.load(4, {shade:0.3}); 
			$("#form1").attr("action", "/crm/customer/patient/save.action");
			$("#form1").submit();
			top.layer.closeAll();
			top.layer.msg("保存成功！");
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#crmPatient_id").val(),
				obj: 'CrmPatient'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					top.layer.load(4, {shade:0.3}); 
					$("#form1").attr("action", "/crm/customer/patient/save.action");
					$("#form1").submit();
					top.layer.closeAll();
				}
			}
		});
	}
}
function editCopy() {
	window.location = window.ctx + '/crm/customer/patient/copyCrmPatient.action?id=' + $("#crmPatient_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#crmPatient_id").val() + "&tableId=crmPatient");
}
function checkSubmit() {
	if($("#crmPatient_id").val() == null || $("#crmPatient_id").val() == "") {
		top.layer.msg(biolims.master.patientIdIsEmpty);
		return false;
	};
	return true;
//	var mess = "";
//	var fs = new Array();
//	var nsc = new Array();
//	fs.push($("#crmPatient_id").val());
//	nsc.push(biolims.master.patientIdIsEmpty);
//
//	// if(!$("#crmPatient_sfz").val()){
//	//	
//	// fs.push($("#crmPatient_dateOfBirth").val());
//	// nsc.push("出生日期不能为空！");
//	// }
//	//	
//	mess = commonFieldsNotNullVerify(fs, nsc);
//	if (mess != "") {
//		message(mess);
//		return false;
//	}
//	return true;
}
$(function() {
//	Ext.onReady(function() {
//		var tabs = new Ext.TabPanel({
//			id : 'tabs11',
//			renderTo : 'maintab',
//
//			autoWidth : true,
//			activeTab : 0,
//			margins : '0 0 0 0',
//			items : [ {
//				title : biolims.master.EMR,
//				contentEl : 'markup'
//			} ]
//		});
//	});
//	load("/crm/customer/patient/showCrmLinkManItemList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmLinkManItempage");
//	// load("/crm/customer/patient/showCrmConsumerMarketList.action", {
//	// id : $("#crmPatient_id").val()
//	// }, "#crmConsumerMarketpage");
//	// load("/crm/customer/patient/showCrmPatientTumorInformationList.action", {
//	// id : $("#crmPatient_id").val()
//	// }, "#crmPatientTumorInformationpage");
//	load("/crm/customer/patient/showCrmPatientDiagnosisList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientDiagnosispage1");
//	load("/crm/customer/patient/showCrmPatientSurgeriesList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientDiagnosispage2");
//	load("/crm/customer/patient/showCrmPatientRadiologyList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientRadiologypage");
//	load("/crm/customer/patient/showCrmPatientLaboratoryList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientLaboratorypage");
//	load("/crm/customer/patient/showCrmPatientPathologyList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientPathologypage");
//	load("/crm/customer/patient/showCrmPatientLinkManList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientLinkManpage");
//	load("/crm/customer/patient/showCrmPatientGeneticTestingList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientGeneticTestingpage");
//	load("/crm/customer/patient/showCrmPatientRestsList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientRestspage");
//	load("/crm/customer/patient/showCrmPatientTreatList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientTreatpage");
//	load("/crm/customer/patient/showCrmPatientTreatList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientTreatpage");
//	load("/crm/customer/patient/showCrmPatientPersonnelList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientPersonnelpage");
//	load("/crm/customer/patient/showCrmPatientItemList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmPatientitempage");
//	// 订单和样本
//	load("/crm/customer/patient/showCrmOrderList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#sampleOrderpage");
//	//临床样本信息
//	load("/crm/customer/patient/showCrmSampleInfoList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#sampleInputpage");;
//	//科研样本信息
////	load("/crm/customer/patient/showCrmScientificSampleInfoList.action", {
////		id : $("#crmPatient_id").val()
////	}, "#scientificSampleInputpage");
//	//确认订单
//	load("/crm/customer/patient/showCustoremerpageList.action", {
//		id : $("#crmPatient_id").val()
//	}, "#crmCustoremerpage");
//	

	var handlemethod = $("#handlemethod").val();
//	if (handlemethod == "view") {
//		settextreadonlyByAll();
//	} else if ('modify' == handlemethod) {
//		settextreadonlyJquery($("#crmPatient_id"));
//	}
	var myDate = new Date();
	// alert(myDate.getYear());
	// alert(myDate.getYear()-$("#crmPatient_dateOfBirth").val().substr(0, 4));
//	if ($("#crmPatient_patientStatus").val() == 'brzt1') {
//		document.getElementById("dq").style.display = "inline";
//		document.getElementById("dq1").style.display = "inline";
//	}
//	if ($("#crmPatient_patientStatus").val() == 'brzt2') {
//		document.getElementById("sw").style.display = "inline";
//		document.getElementById("sw1").style.display = "inline";
//	}
//	if ($("#crmPatient_dateOfBirth")) {
//		document.getElementById('dqAge').value = myDate.getFullYear() - $("#crmPatient_dateOfBirth").val().substr(0, 4);
//	}
});
function sfzVerify() {
	$.post("/crm/customer/patient/sfzVerify.action", {
		"data" : $("#crmPatient_sfz").val()
	}, function(data) {
		if (data.message == false) {
			alert(biolims.master.dataExisting);
		}
	}, "json");
}
function showCustomer() {
	var win = Ext.getCmp('showCustomer');
	if (win) {
		win.close();
	}
	var showCustomer = new Ext.Window(
			{
				id : 'showCustomer',
				modal : true,
				title : biolims.master.selectHospital,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerFun1' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showCustomer.close();
					}
				} ]
			});
	showCustomer.show();
}
function setCrmCustomerFun1(rec) {
	// records = inItemGrid.getSelectRecord();
	// $.each(records, function(i, obj) {
	// obj.set("customer-id", rec.get('id'));
	// obj.set("customer-name", rec.get('name'));
	// });
	document.getElementById("sbr_customer_id").value = rec.get('id');
	document.getElementById("sbr_customer_name").value = rec.get('name');
	var win = Ext.getCmp('showCustomer');
	if (win) {
		win.close();
	}
}

function showkindFun() {
	var win = Ext.getCmp('showkindFun');
	if (win) {
		win.close();
	}
	var showkindFun = new Ext.Window(
			{
				id : 'showkindFun',
				modal : true,
				title : biolims.master.selectDepartment,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=ks' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showkindFun.close();
					}
				} ]
			});
	showkindFun.show();
}

function setks(id, name) {
	// records = inItemGrid.getSelectRecord();
	// $.each(records, function(i, obj) {
	// obj.set("ks-id", id);
	// obj.set("ks-name", name);
	// });
	document.getElementById("sbr_ks_id").value = id;
	document.getElementById("sbr_ks_name").value = name;
	var win = Ext.getCmp('showkindFun');
	if (win) {
		win.close();
	}
}

function showCustomerDoctor() {
	var win = Ext.getCmp('showCustomerDoctor');
	if (win) {
		win.close();
	}
	var showCustomerDoctor = new Ext.Window(
			{
				id : 'showCustomerDoctor',
				modal : true,
				title : biolims.master.selectDoctor,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerDoctorFun2' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showCustomerDoctor.close();
					}
				} ]
			});
	showCustomerDoctor.show();
}
function setCrmCustomerDoctorFun2(rec) {
	// records = inItemGrid.getSelectRecord();
	// $.each(records, function(i, obj) {
	// obj.set("customerDoctor-id", rec.get('id'));
	// obj.set("customerDoctor-name", rec.get('name'));
	// });
	document.getElementById("sbr_customerDoctor_id").value = rec.get('id');
	document.getElementById("sbr_customerDoctor_name").value = rec.get('name');
	var win = Ext.getCmp('showCustomerDoctor')
	if (win) {
		win.close();
	}
}





//查询类型=================================================================================
/**
 * 选择肿瘤类型
 */

function WeiChatCancerTypeFun() {
	var win = Ext.getCmp('WeiChatCancerTypeFun');
	if (win) {
		win.close();
	}
	var WeiChatCancerTypeFun = new Ext.Window(
			{
				id : 'WeiChatCancerTypeFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sample/sampleCancerTemp/weiChatCancerTypeSelect.action?flag=WeiChatCancerTypeFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						WeiChatCancerTypeFun.close();
					}
				} ]
			});
	WeiChatCancerTypeFun.show();
}
/*
 * ajax 异步
 * */
function setWeiChatCancerTypeFun(rec) {

document.getElementById("crmPatient_cancerType_Id").value = rec.get("id");
document.getElementById("crmPatient_cancerType_cancerTypeName").value = rec.get("cancerTypeName");
	var win = Ext.getCmp('WeiChatCancerTypeFun');
	if (win) {
		win.close();
	}
}

/**
 * 选择肿瘤类型1
 */

function WeiChatCancerTypeOneFun() {
	if($("#crmPatient_cancerType_Id").val()==""||$("#crmPatient_cancerType_Id").val()==null){
		message(biolims.sample.pleaseSelectTumorType);
		Ext.getCmp('WeiChatCancerTypeOneFun').close();
	}
	var win = Ext.getCmp('WeiChatCancerTypeOneFun');
	if (win) {
		win.close();
	}
	var WeiChatCancerTypeOneFun = new Ext.Window(
			{
				id : 'WeiChatCancerTypeOneFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/sampleCancerTemp/weiChatCancerTypeSelectOne.action?code="+$("#crmPatient_cancerType_Id").val()+"&flag=WeiChatCancerTypeOneFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						WeiChatCancerTypeOneFun.close();
					}
				} ]
			});
	WeiChatCancerTypeOneFun.show();
}
/*
 * 
 * */
function setWeiChatCancerTypeOneFun(rec) {

document.getElementById("crmPatient_cancerTypeSeedOne_Id").value = rec.get("id");
document.getElementById("crmPatient_cancerTypeSeedOne_cancerTypeName").value = rec.get("cancerTypeName");
	var win = Ext.getCmp('WeiChatCancerTypeOneFun');
	if (win) {
		win.close();
	}
}


/**
 * 选择肿瘤类型2
 */

function WeiChatCancerTypeTwoFun() {
	if($("#crmPatient_cancerType_Id").val()==""||$("#crmPatient_cancerType_Id").val()==null){
		message(biolims.sample.pleaseSelectTumorType);
		Ext.getCmp('WeiChatCancerTypeTwoFun').close();
	}
	var win = Ext.getCmp('WeiChatCancerTypeTwoFun');
	if (win) {
		win.close();
	}
	var WeiChatCancerTypeTwoFun = new Ext.Window(
			{
				id : 'WeiChatCancerTypeTwoFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/sampleCancerTemp/weiChatCancerTypeSelectTwo.action?code="+$("#crmPatient_cancerType_Id").val()+"&flag=WeiChatCancerTypeTwoFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						WeiChatCancerTypeTwoFun.close();
					}
				} ]
			});
	WeiChatCancerTypeTwoFun.show();
}
/*
 * 
 * */
function setWeiChatCancerTypeTwoFun(rec) {

document.getElementById("crmPatient_cancerTypeSeedTwo_Id").value = rec.get("id");
document.getElementById("crmPatient_cancerTypeSeedTwo_cancerTypeName").value = rec.get("cancerTypeName");
	var win = Ext.getCmp('WeiChatCancerTypeTwoFun');
	if (win) {
		win.close();
	}
}

//医疗机构
function showHos() {
	top.layer.open({
		title: biolims.common.selectedDoctor,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/customer/customer/crmCustomerSelectTable.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(
				0).text();
			var street = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(
					3).text();
			debugger
			top.layer.close(index)
			    
			$("#crmPatient_customer_id").val(id);
			$("#crmPatient_customer_name").val(name);
			$("#crmPatient_customer_street").val(street);
			
		},
	})
}



function showinspectionDepartment() {
	top.layer.open({
		title: biolims.master.selectDepartment,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=ks", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#crmPatient_ks_id").val(id)
			$("#crmPatient_ks_name").val(name)
		},
	})
}

//送检医院与主治医生添加关联性
function showDoctors() {

	var sampleOrder_crmCustomer_id = $("#crmPatient_customer_id").val();
	if(sampleOrder_crmCustomer_id == null || sampleOrder_crmCustomer_id == "") {
		top.layer.msg(biolims.common.selectedDoctor);
		return
	}

	top.layer.open({
		title: biolims.master.selectDoctor,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/crm/doctor/showDialogCrmPatientTable.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(
				0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(1).text();
			var mobile = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(2).text();
			top.layer.close(index)
			$("#crmPatient_customerDoctor_id").val(id)
			$("#crmPatient_customerDoctor_name").val(name)
			$("#crmPatient_customerDoctor_mobile").val(mobile)
		},
	})

}

/* 获取肿瘤分期 */
function showTimesTable() {
	top.layer.open({
		title: "分期",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=zlfqd", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#crmPatient_cancerInstalment_id").val(id);
			$("#crmPatient_cancerInstalment_name").val(name);
		},
	})
}

//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

	//查找自定义列表的订单模块的此检测项目的相关内容
	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		data: {
			moduleValue: "CrmPatient"
		},
		async: false,
		success: function(data) {
			var objValue = JSON.parse(data);
			if(objValue.success) {
				$.each(objValue.data, function(i, n) {
					var inputs = '';
					var disabled = n.readOnly ? ' ' : "disabled";
					var defaultValue = n.defaultValue ? n.defaultValue : ' ';
					if(n.fieldType == "checkbox") {
						var checkboxs = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:10px"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
					} else if(n.fieldType == "radio") {
						var options = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:10px"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control" lay-ignore>' + options + '</select></div></div>';
					} else if(n.fieldType == "date") {
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:10px"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
					} else {
						if(n.isRequired!="false"){
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '<img class="requiredimage" src="/images/required.gif" /></span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}else{
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}
					}

					$("#fieldItemDiv").append(inputs);
				});

			} else {
				top.layer.msg(biolims.customList.updateFailed);
			}
		}
	});

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
						inp.setAttribute("changelog", inp.value);
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
							val.setAttribute("changelog", val.value);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}


//展开关闭相关病史
function patientShowAndHide (that) {
	if(that.getAttribute("state")=="1"){
		$(".changeView").slideDown();
		that.setAttribute("state","0")
		that.setAttribute("class","glyphicon glyphicon-minus")
	}else{
		$(".changeView").slideUp();
		that.setAttribute("state","1")
        that.setAttribute("class","glyphicon glyphicon-plus")
	}
}
//展开医院信息
function patientShowAndHide2 (that) {
	if(that.getAttribute("state")=="1"){
		$(".changeView2").slideDown();
		that.setAttribute("state","0")
		that.setAttribute("class","glyphicon glyphicon-minus")
	}else{
		$(".changeView2").slideUp();
		that.setAttribute("state","1")
		that.setAttribute("class","glyphicon glyphicon-plus")
	}
}
function showcancerTypeTable(){
	top.layer.open({
		title: "选择肿瘤类型",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=cancerType", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#crmPatient_tumourType_id").val(id);
			$("#crmPatient_tumourType_name").val(name);
		},
	})
}

/**
 * 身份证验证
 * 
 * @param v
 * @returns
 */
function checkidcardno(v) {
	var reg =/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
	if(!v.match(reg)) {
		top.layer.msg(("您输入的身份证号码有误,请重新输入!"));
	}
}
/**
 * 手机号验证
 * 
 * @param v
 */
function checktelephone(v) {
	var a = /^[1][3,4,5,7,8][0-9]{9}$/;
	if(v.length != 11 || !v.match(a)) {
		top.layer.msg(("不是完整的11位手机号或者正确的手机号"));
	}
}
/**
 * 邮箱验证
 * 
 * @param v
 */
function checkemail(v) {
	var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	if(!v.match(myreg)) {
		top.layer.msg(("email格式不正确，请重新输入！"));
	}
}
/**
 * 限制数字
 * @param v
 * @returns
 */
function lunci(v) {
var myreg = /^[1-9]\d*$/;
if(!v.match(myreg)) {
	top.layer.msg(("采血轮次只能输入整数！"));
}
}
function ag(v) {
	var myreg = /^\d+(\.\d+)?$/;
	if(!v.match(myreg)) {
		top.layer.msg(("年龄只能输入数字！"));
	}
}

function filtrateCode(v) {
	var myreg = /^\d+(\.\d+)?$/;
	if(!v.match(myreg)) {
		top.layer.msg(("筛选号只能输入数字！"));
	}
}

function randomCode(v) {
	var myreg = /^\d+(\.\d+)?$/;
	if(!v.match(myreg)) {
		top.layer.msg(("随机号只能输入数字！"));
	}
}
function whiteBloodCellNum(v) {
	var myreg = /^\d+(\.\d+)?$/;
	if(!v.match(myreg)) {
		top.layer.msg(("白细胞计数只能输入数字！"));
	}
}
function percentageOfLymphocytes(v) {
	var myreg = /^\d+(\.\d+)?$/;
	if(!v.match(myreg)) {
		top.layer.msg(("淋巴细胞百分比只能输入数字！"));
	}
}
//选择检查产品
function voucherProductFun() {

	top.layer.open({
		title: biolims.crm.selectProduct,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/com/biolims/system/product/showProductSelTree.action",
		yes: function(index, layer) {
			var name = [],
				id = [];
				price = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(1).text());
				id.push($(v).children("td").eq(0).text());
				if($(v).children("td").eq(2).text()!=""){
					price.push($(v).children("td").eq(2).text());
				}
			});
			

			
			$("#crmPatient_productId").val(id.join(","));
			$("#crmPatient_productName").val(name.join(","));
			var sum = 0;
			for(var i=0;i<price.length;i++){
				var p = Number(price[i]);
				sum += p;
			}
			$("#sampleOrder_fee").val(sum);
			top.layer.close(index)
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

