var crmLinkManItemGrid;
var crmLinkManItemChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#crmPatient_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title":biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false,
		"className": "edit"
	});
	colOpts.push({
		"data": "sampleOrder-id",
		"title": "订单号",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-id");
		},
	});
	colOpts.push({
		"data": "sampleOrder-barcode",
		"title": "批次号",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-barcode");
		},
	});
	colOpts.push({
		"data": "link",
		"title": biolims.master.link,
		"createdCell": function(td) {
			$(td).attr("saveName", "link");
		},
		"className": "edit"
	});
	colOpts.push({
		"data": "headLine",
		"title": biolims.master.headLine,
		"createdCell": function(td) {
			$(td).attr("saveName", "headLine");
		},
		"visible": true,
		"className": "edit"
	});
	colOpts.push({
		"data": "content",
		"title": biolims.master.content,
		"createdCell": function(td) {
			$(td).attr("saveName", "content");
		},
		"visible": true,
		"className": "edit"
	});
//	colOpts.push({
//		"data": "dutyUser-name",
//		"title": biolims.master.dutyUserName,
//		"createdCell": function(td, data, rowData) {
//			$(td).attr("saveName", "dutyUser-name");
//			$(td).attr("dutyUser-id", rowData['dutyUser-id']);
//		}
//	});
	colOpts.push({
		"data": "createDate",
		"title": biolims.master.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
		},
		"visible": true,
		"className": "date"
	});
	colOpts.push({
		"data": "fee",
		"title": biolims.master.fee,
		"createdCell": function(td) {
			$(td).attr("saveName", "fee");
		},
		"visible": true,
		"className": "edit"
	});
	colOpts.push({
		"data": "assign",
		"title": biolims.master.assign,
		"createdCell": function(td) {
			$(td).attr("saveName", "assign");
		},
		"visible": true,
		"className": "edit"
	});
	
//	colOpts.push({
//		"data": "qcState",
//		"title": biolims.tStorageReagentBuySerial.qcState,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "qcState");
//			$(td).attr("selectOpt", "|NotQC|QCPass|QC");
//		},
//		"visible": true,
//		"className": "select",
//		"name": "|NotQC|QCPass|QC",
//		"render": function(data, type, full, meta) {
//			if(data == "NotQC") {
//				return '0';
//			}
//			if(data == "QCPass") {
//				return '1';
//			}
//			if(data == "QC") {
//				return '2';
//			} else {
//				return '';
//			}
//		}
//	});
	colOpts.push({
		"data": "ourPeople",
		"title": biolims.master.ourPeople,
		"createdCell": function(td) {
			$(td).attr("saveName", "ourPeople");
		},
		"visible": true,
		"className": "edit"
	});
	colOpts.push({
		"data": "customerPeople",
		"title": biolims.master.customerPeople,
		"createdCell": function(td) {
			$(td).attr("saveName", "customerPeople");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		},
		"visible": true,
		"className": "edit"
	});
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#crmLinkManItemTable"));
			}
		});
		tbarOpts.push({
			text: biolims.common.addwindow,
			action: function() {
				addItemLayer($("#crmLinkManItemTable"));
			}
		});
		tbarOpts.push({
			text: biolims.common.Editplay,
			action: function() {
				editItemLayer($("#crmLinkManItemTable"));
			}
		});
		tbarOpts.push({
			text: "选择订单",
			action: function() {
				selectSampleOrder($("#crmLinkManItemTable"));
			}
		});
		tbarOpts.push({
			text: biolims.common.save,
			action: function() {
				saveCrmLinkManItem($("#crmLinkManItemTable"));
			}
		});
	}

	var crmLinkManItemOptions = table(true,
		id,
		'/crm/customer/patient/showCrmLinkManItemNewListJson.action', colOpts, tbarOpts);
	crmLinkManItemGrid = renderData($("#crmLinkManItemTable"), crmLinkManItemOptions);
	crmLinkManItemGrid.on('draw', function() {
		crmLinkManItemChangeLog = crmLinkManItemGrid.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});

//获得保存时的json数据
function savecrmLinkManItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
//			// 判断男女并转换为数字
//			if(k == "gender") {
//				var gender = $(tds[j]).text();
//				if(gender == "男") {
//					json[k] = "0";
//				} else if(gender == "女") {
//					json[k] = "1";
//				} else if(gender == "未知") {
//					json[k] = "2";
//				} else {
//					json[k] = "";
//				}
//				continue;
//			}
//			// 添加样本类型ID
//			if(k == "sampletype-name") {
//				json["sampleType-id"] = $(tds[j]).attr("sampletype-id");
//				continue;
//			}
//			// 添加接收人ID
//			if(k == "receiveUser-name") {
//				json["receiveUser-id"] = $(tds[j]).attr("receiveUser-id");
//				continue;
//			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

// 保存
function saveCrmLinkManItem(ele) {
	var data = savecrmLinkManItemjson(ele);
	var ele = $("#crmLinkManItemTable");
	var changeLog = biolims.master.communicateInformation+"：";
	changeLog = getChangeLogCrmLinkManItem(data, ele, changeLog);

	$.ajax({
		type: 'post',
		url: '/crm/customer/patient/saveCrmLinkManItemTable.action',
		data: {
			id: $("#crmPatient_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
//// 获得保存时的json数据
//function saveStorageReagentBuySerialjson(ele) {
//	var trs = ele.find("tbody").children(".editagain");
//	var data = [];
//	trs.each(function(i, val) {
//		var json = {};
//		var tds = $(val).children("td");
//		json["id"] = $(tds[0]).find("input").val();
//		for(var j = 1; j < tds.length; j++) {
//			var k = $(tds[j]).attr("savename");
//			// 判断并转换为数字
//
//			if(k == "storage-name") {
//				json["storage-id"] = $(tds[j]).attr("storage-id");
//				continue;
//			}
//
//			if(k == "costCenter-name") {
//				json["costCenter-id"] = $(tds[j]).attr("costCenter-id");
//				continue;
//			}
//
//			if(k == "position-name") {
//				json["position-id"] = $(tds[j]).attr("position-id");
//				continue;
//			}
//
//			if(k == "storageInItem-name") {
//				json["storageInItem-id"] = $(tds[j]).attr("storageInItem-id");
//				continue;
//			}
//
//			if(k == "regionType-name") {
//				json["regionType-id"] = $(tds[j]).attr("regionType-id");
//				continue;
//			}
//
//			if(k == "linkStorageItem-name") {
//				json["linkStorageItem-id"] = $(tds[j]).attr("linkStorageItem-id");
//				continue;
//			}
//
//			if(k == "rankType-name") {
//				json["rankType-id"] = $(tds[j]).attr("rankType-id");
//				continue;
//			}
//
//			if(k == "currencyType-name") {
//				json["currencyType-id"] = $(tds[j]).attr("currencyType-id");
//				continue;
//			}
//
//			if(k == "timeUnit-name") {
//				json["timeUnit-id"] = $(tds[j]).attr("timeUnit-id");
//				continue;
//			}
//
//			if(k == "unit-name") {
//				json["unit-id"] = $(tds[j]).attr("unit-id");
//				continue;
//			}
//
//			json[k] = $(tds[j]).text();
//		}
//		data.push(json);
//	});
//	return JSON.stringify(data);
//}

function getChangeLogCrmLinkManItem(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		crmLinkManItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function selectSampleOrder(){
	var rows=$("#crmLinkManItemTable .selected");
	var length = rows.length;
	if(!length){
		top.layer.msg("请选择数据！");
		return false;
	}
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/crm/customer/patient/showSampleOrderDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
			.eq(0).text();
			var batch = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
			.eq(1).text();
			rows.addClass("editagain");
			rows.find("td[saveName=sampleOrder-id]").text(id);
			rows.find("td[saveName=sampleOrder-barcode]").text(batch);
			top.layer.closeAll();
		},
	})
}