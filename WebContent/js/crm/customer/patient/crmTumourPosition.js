var id = new Ext.form.TextField({
	allowBlank : true
});
var cob;
id.on('change', function(field, newValue, oldValue) {
	
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : window.ctx + '/common/hasId.action',
		method : 'POST',
		params : {
			id : newValue,
			obj : 'DicType'
		},
		success : function(response) {
			var respText = Ext.util.JSON.decode(response.responseText);
			myMask.hide();
			if (respText.message == '') {
				return true;
			} else {
				message(respText.message);
			
				field.setValue(oldValue);
			
			}

		},
		failure : function(response) {
		}
	});
	
	
	return true;
	
	
	

});
var sysCode = new Ext.form.TextField({
	allowBlank : true
});
var code = new Ext.form.TextField({
	allowBlank : true
});
var dicName = new Ext.form.TextField({
	allowBlank : true
});
var orderNumber = new Ext.form.NumberField({
	allowBlank : true,
	allowNegative : false
});

var store = new Ext.data.JsonStore({
	root : 'results',
	remoteSort : true,
	fields : [ {
		name : 'id'
	}, {
		name : 'name'
	} ],
	proxy : new Ext.data.HttpProxy({
		url : window.ctx + '/tumour/position/showDicMainTypeListJson.action',
		method : 'POST'
	})
});

store.load();
var cob = new Ext.form.ComboBox({

	store : store,
	displayField : 'name',
	valueField : 'id',
	typeAhead : true,

	mode : 'remote',
	forceSelection : true,
	triggerAction : 'all',

	selectOnFocus : true
});
var note = new Ext.form.TextArea({
	allowBlank : true,
	maxLength : 200,
	maxLengthText : biolims.master.isNotGreater200
});
var storeStateCob = new Ext.data.ArrayStore({
	fields : [ 'id', 'name' ],
	data : [ [ '0',biolims.master.invalid ], [ '1', biolims.master.valid ] ]
});

var stateCob = new Ext.form.ComboBox({
	store : storeStateCob,
	displayField : 'name',
	valueField : 'id',
	mode : 'local'
});


window.searchMainType = function(domId) {
	Ext.QuickTips.init();

	var store = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/tumour/position/showDicMainTypeListJson.action',
			method : 'POST'
		})
	});


	store.load();
	cob = new Ext.form.ComboBox({

		store : store,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,

		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',

		selectOnFocus : true,
		applyTo : domId
	});
	cob.on('select', function(combo) {
		var grid = gridGrid;
	
		grid.store.reload();
		
		var filter1 = function(record, id){
			   if (record.get("superior")==combo.getValue()){
			      	return true;
			      }
			   else
			      return false;
			};
			var onStoreLoad1 = function(store, records, options){
			   store.filterBy(filter1);
			};
			grid.store.on("load", onStoreLoad1);
		
	});

	$("#" + domId).css("width", "200px");
}

$(function() {
	searchMainType("mainType");
});

