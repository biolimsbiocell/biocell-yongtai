var crmConsumerMarketGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'crmProduct-id',
		type:"string"
	});
	    fields.push({
		name:'consumptionTime',
		type : "date",
		dateFormat:'Y-m-d'
	});
	    
	    fields.push({
			name:'contractTime',
			type : "date",
			dateFormat:'Y-m-d'
		});
	    fields.push({
			name:'paymentTime',
			type : "date",
			dateFormat:'Y-m-d'
		});
	    fields.push({
			name:'skillReport',
			type : "date",
			dateFormat:'Y-m-d'
		});
	    fields.push({
			name:'clinicReport',
			type : "date",
			dateFormat:'Y-m-d'
		});
	    fields.push({
			name:'fSkillReport',
			type : "date",
			dateFormat:'Y-m-d'
		});
	    fields.push({
			name:'fReport',
			type : "date",
			dateFormat:'Y-m-d'
		});
	    fields.push({
		name:'crmPatient-id',
		type:"string"
	});
	    fields.push({
			name:'isFee',
			type:"string"
		});
	    fields.push({
			name:'fee',
			type:"string"
		});
	    fields.push({
			name:'code',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'id',
		width:10*10,
		hidden : true
	});
	var crmProductstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/crm/service/product/showTableJson.action?table=CrmProduct',
			method : 'POST'
		})
	});
	crmProductstore.load();
	var crmProductcob = new Ext.form.ComboBox({
		store : crmProductstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
		cm.push({
		dataIndex:'crmProduct-id',
		header:biolims.master.product,
		width:15*10,
		renderer : Ext.util.Format.comboRenderer(crmProductcob),
		editor : crmProductcob,
		hidden : true
		});
		cm.push({
			dataIndex:'code',
			hidden : true,
			header:biolims.master.receiveOrder,
			width:10*10
		});
	cm.push({
		dataIndex:'consumptionTime',
		header:biolims.master.consumptionTime,
		width:12*10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex:'contractTime',
		header:biolims.master.contractTime,
		width:12*10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex:'paymentTime',
		header:biolims.master.paymentTime,
		width:12*10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex:'isFee',
		hidden : true,
		header:biolims.master.isFee,
		width:10*10
	});
	cm.push({
		dataIndex:'fee',
		hidden : true,
		header:biolims.master.fee,
		width:10*10
	});
	cm.push({
		dataIndex:'skillReport',
		header:biolims.master.skillReport,
		width:12*10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex:'clinicReport',
		header:biolims.master.clinicReport,
		width:12*10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex:'fSkillReport',
		header:biolims.master.fSkillReport,
		width:12*12,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex:'fReport',
		header:biolims.master.fReport,
		width:12*10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
		cm.push({
			dataIndex:'crmPatient-id',
			hidden : true,
			header:biolims.master.patientId,
			width:10*10
		});
		cm.push({
			xtype : 'actioncolumn',
			width : 80,
			header : biolims.common.uploadAttachment,
			items : [ {
				icon : window.ctx + '/images/img_attach.gif',
				tooltip : biolims.common.attachment,
				handler : function(grid, rowIndex, colIndex) {
					var rec = grid.getStore().getAt(rowIndex);
					if(rec.get('id')&&rec.get('id')!='NEW'){
						
						var win = Ext.getCmp('doc');
						if (win) {win.close();}
						var doc= new Ext.Window({
						id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
						plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
						collapsible: true,maximizable: true,
						items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
						html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=crmConsumerMarket&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
						buttons: [
						{ text: biolims.common.close,
						 handler: function(){
						 doc.close(); }  }]  }); 
						 doc.show();
					
					}else{
						message(biolims.common.pleaseSaveRecord);
					}
				}
			} ]
		});
		
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/showCrmConsumerMarketListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.individualPinFeeRecordsManagement;
	opts.height =  220;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmConsumerMarketGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmConsumerMarket",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmConsumerMarketGrid.getStore().commitChanges();
						crmConsumerMarketGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmConsumerMarketGrid=gridEditTable("crmConsumerMarketdiv",cols,loadParam,opts);
	$("#crmConsumerMarketdiv").data("crmConsumerMarketGrid", crmConsumerMarketGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})