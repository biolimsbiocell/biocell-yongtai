var crmFamilyPatientShipGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    
	    fields.push({
			name:'state',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.sample.familyName,
		width:50*6,
		
		sortable:true
	});
	
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:50*6,
		
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/crmFamilyPatientShip/showCrmFamilyPatientShipListJson.action";
	var opts={};
	opts.title=biolims.sample.familyRelationTable;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	crmFamilyPatientShipGrid=gridTable("show_crmFamilyPatientShip_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/customer/patient/crmFamilyPatientShip/editCrmFamilyPatientShip.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/crm/customer/patient/crmFamilyPatientShip/editCrmFamilyPatientShip.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/crm/customer/patient/crmFamilyPatientShip/viewCrmFamilyPatientShip.action?id=' + id;
}
function exportexcel() {
	crmFamilyPatientShipGrid.title = biolims.common.exportList;
	var vExportContent = crmFamilyPatientShipGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(crmFamilyPatientShipGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
