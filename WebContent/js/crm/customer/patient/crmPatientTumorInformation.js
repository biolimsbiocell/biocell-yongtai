var crmPatientTumorInformationGrid;
$(function(){
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',               
		type : "string"
	});
	fields.push({
		name : 'customer-id',               
		type : "string"
	});
	fields.push({
		name : 'patientPathology-id',               
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'kind-id',
		type : "string"
	});

	fields.push({
		name : 'sampleKind-id',
		type : "string"
	});
    fields.push({
		name:'tumorLocation-id',
		type:"string"
	});
    fields.push({
		name:'site-id',
		type:"string"
	});
	fields.push({
		name : 'sequencingName',
		type : "string"
	});
	fields.push({
		name : 'inspectDate',
		type : "date",
		dateFormat:'Y-m-d'
	});
	fields.push({
		name : 'reportDate',
		type : "date",
		dateFormat:'Y-m-d'
	});
	fields.push({
		name : 'patientId',
		type : "string"
	});
	
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'num',
		type : "string"
	});
	fields.push({
		name : 'originalNum',
		type : "string"
	});
	fields.push({
		name : 'unit-id',
		type : "string"
	});
	fields.push({
		name : 'sampleCity-id',
		type : "string"
	});
	fields.push({
		name : 'sampleCity-name',
		type : "string"
	});
	fields.push({
		name : 'isGood',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'barCode',
		type : "string"
	});
	fields.push({
		name : 'location',
		type : "string"
	});

	fields.push({
		name : 'method',
		type : "string"
	});
	fields.push({
		name : 'isPass',
		type : "string"
	});
	fields.push({
		name : 'sampleBloodRecrive-id',
		type : "string"
	});
	fields.push({
		name : 'isContract',
		type : "string"
	});
	fields.push({
		name : 'species',
		type : "string"
	});
	fields.push({
		name : 'solution',
		type : "string"
	});
	fields.push({
		name : 'concentration',
		type : "string"
	});
	fields.push({
		name : 'deliverySampleNumber',
		type : "string"
	});
	fields.push({
		name : 'fileNum',
		type : "string"
	});
	/*
	 * fields.push({ name : 'position-id', type : "string" });
	 */
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'customer-id',
		header : 'ID',
		width : 100,
		hidden : true
	});
	var patientPathologystore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'pathologicalDiagnosis'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + "/crm/customer/patient/showCrmPatientPathologyJson.action?id="+ $("#id_parent_hidden").val(),
			method : 'POST'
		})
	});
	patientPathologystore.load();
	var patientPathologycob = new Ext.form.ComboBox({
		store : patientPathologystore,
		displayField : 'pathologicalDiagnosis',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'patientPathology-id',
		header : biolims.master.patientPathologyId,
		width : 120,
		renderer : Ext.util.Format.comboRenderer(patientPathologycob),
		editor : patientPathologycob
	});
	cm.push({
		dataIndex : 'code',
		header : biolims.common.code,
		width : 100
	});
	var kindstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=bblx',
			method : 'POST'
		})
	});
	kindstore.load();
	var kindcob = new Ext.form.ComboBox({
		store : kindstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'kind-id',
		hidden : false,
		header : biolims.master.kindId,
		width : 100,
		renderer : Ext.util.Format.comboRenderer(kindcob)
	});
	var sampleKindstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, 
		{
			name : 'name'
		},
		{
			name : 'code'
		}
		],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx
					+ '/sample/blood/dicSampleTypeSelect.action?type=1,2,3',
			method : 'POST'
		})
	});
	sampleKindstore.load();
	sampleKindstore.on('load',function(store,record,opts){});
	var sampleKindcob = new Ext.form.ComboBox({
		store : sampleKindstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true,
		listeners: {  //为Combo添加select事件
			focus: function(combo, record, index) {   // 该事件会返回选中的项对应在 store中的 record值. index参数是排列号.
				
				sampleKindstore.load();
		  }
		  }
	});
	cm.push({
		dataIndex : 'sampleKind-id',
		header : biolims.master.sampleKindId,
		width : 100,
		renderer : Ext.util.Format.comboRenderer(sampleKindcob)
	});
	var tlstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/tumour/position/showDicMainTypeListJson.action',
			method : 'POST'
		})
	});
	tlstore.load();
	var tlcob = new Ext.form.ComboBox({
		store : tlstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
		cm.push({
		dataIndex:'tumorLocation-id',
		header:biolims.master.tumorLocationId,
		width:15*10,
		renderer : Ext.util.Format.comboRenderer(tlcob)
		});
////		cm.push({
////		dataIndex:'tumorLocation-name',
////		header:'肿瘤所在器官',
////		width:15*10,
////		sortable:true
////		});
		var sitestore = new Ext.data.JsonStore({
			root : 'results',
			remoteSort : true,
			fields : [ {
				name : 'id'
			}, 
			{
				name : 'name'
			},
			{
				name : 'superior'
			}],
			proxy : new Ext.data.HttpProxy({
				url : window.ctx + '/tumour/position/showDicMainTypeList.action',
				method : 'POST'
			})
		});
		sitestore.load();
		setTimeout(function() {
		sitestore.on('load',function(store,record,opts){
			var records = crmPatientTumorInformationGrid.getSelectRecord();	
			if(records.length>0){
				store.filterBy(function(record){
					return record.get("superior")==records[0].get("tumorLocation-id")
				});
			}
		});
		}, 1200);
		var sitecob = new Ext.form.ComboBox({
			store : sitestore,
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'remote',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true,
			listeners: {  //为Combo添加select事件
				focus: function(combo, record, index) {   // 该事件会返回选中的项对应在 store中的 record值. index参数是排列号.
					
					sitestore.load();
			  }
			  }
		});
		cm.push({
		dataIndex:'site-id',
		header:biolims.master.siteId,
		width:15*10,
		renderer : Ext.util.Format.comboRenderer(sitecob)
		});
//	var sequencingstore = new Ext.data.JsonStore({
//		root : 'results',
//		remoteSort : true,
//		fields : [ {
//			name : 'id'
//		}, {
//			name : 'name'
//		} ],
//		proxy : new Ext.data.HttpProxy({
//			url : window.ctx + '/crm/service/product/showCrmProductJXListJson.action?fl=jccp',
//			method : 'POST'
//		})
//	});
//	sequencingstore.load();
//	var sequencingcob = new Ext.form.ComboBox({
//		store : sequencingstore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'remote',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
	cm
			.push({
				dataIndex : 'sequencingName',
				header : biolims.common.productName,
				width : 110
			});
	cm.push({
		dataIndex : 'inspectDate',
		header : biolims.master.inspectDate,
		width : 100,
		renderer : formatDate
	});
	cm.push({
		dataIndex : 'reportDate',
		header : biolims.common.reportDate,
		width : 100,
		renderer : formatDate
	});
//	cm.push({
//		dataIndex : 'patientId',
//		header : '病人编号',
//		width : 100,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex : 'barCode',
		header : biolims.common.barCode,
		hidden:true,
		width : 100
	});
	cm.push({
		dataIndex : 'patientName',
		header : biolims.common.patientName,
		width : 100
	});
//	cm.push({
//		dataIndex : 'num',
//		header : '库存数量',
//		width : 100,
//		hidden:true
//	});
	cm.push({
		dataIndex : 'originalNum',
		header : biolims.master.originalNum,
		width : 100
	});
	var unitstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx
					+ '/dic/unit/dicUnitTypeCobJson.action?type=weight',
			method : 'POST'
		})
	});
	unitstore.load();
	var unitcob = new Ext.form.ComboBox({
		store : unitstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'unit-id',
		header : biolims.common.unit,
		width : 100,
		renderer : Ext.util.Format.comboRenderer(unitcob)
	});
	cm.push({
		dataIndex : 'deliverySampleNumber',
		header : biolims.master.deliverySampleNumber,
		width : 150
	});
//	var contract = new Ext.form.ComboBox({
//		transform : "grid_contract",
//		width : 50,
//		triggerAction : 'all',
//		lazyRender : true
//	});
//	cm.push({
//		dataIndex : 'isContract',
//		header : '是否合同完整',
//		width : 150,
//		editor : contract,
//		renderer : function(value, metadata, record, rowIndex,
//				colIndex, store) {
//			if (value == "0") {
//				return "<span style='color:red'>否</span>";
//			} else if (value == "1") {
//				return "是";
//			} else {
//				return "请选择";
//			}
//		}
//	});
//	var gridMethod = new Ext.form.ComboBox({
//		transform : "grid_method",
//		width : 50,
//		triggerAction : 'all',
//		lazyRender : true
//	});
//	cm.push({
//		dataIndex : 'method',
//		header : '是否签署知情同意书',
//		width : 150,
//		
//		editor : gridMethod,
//		renderer : function(value, metadata, record, rowIndex,
//				colIndex, store) {
//			if (value == "0") {
//				return "否";
//			} else if (value == "1") {
//				return "是";
//			} else {
//				return "请选择";
//			}
//		}
//	});
//	var gridIsPass = new Ext.form.ComboBox({
//		transform : "grid_is_pass",
//		width : 50,
//		triggerAction : 'all',
//		lazyRender : true
//	});
//	cm
//			.push({
//				dataIndex : 'isPass',
//				header : '是否有病历',
//				width : 100,
//				
//				editor : gridIsPass,
//				renderer : function(value, metadata, record, rowIndex,
//						colIndex, store) {
//					if (value == "0") {
//						return "否";
//					} else if (value == "1") {
//						return "是";
//					} else {
//						return "请选择";
//					}
//				}
//			});
//	cm.push({
//		dataIndex : 'solution',
//		header : '溶液',
//		width : 150,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
////	cm.push({
////		dataIndex : 'species',
////		header : '物种来源',
////		width : 150,
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
////	});
//
//	cm.push({
//		dataIndex : 'concentration',
//		header : '浓度',
//		width : 150,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//
//	cm.push({
//		dataIndex : 'sampleCity-id',
//		header : '城市id',
//		width : 100,
//		hidden : true
//	});
//	cm.push({
//		dataIndex : 'sampleCity-name',
//		header : '城市',
//		width : 100,
//		hidden :true
//	});
//	var gridIsGood = new Ext.form.ComboBox({
//		transform : "grid_is_good",
//		width : 50,
//		triggerAction : 'all',
//		lazyRender : true
//	});
//	cm
//			.push({
//				dataIndex : 'isGood',
//				header : '是否合格',
//				width : 100,
//				editor : gridIsGood,
//				renderer : function(value, metadata, record, rowIndex,
//						colIndex, store) {
//					if (value == "0") {
//						return "<span style='color:red'>否</span>";
//					} else if (value == "1") {
//						return "是";
//					} else {
//						return "请选择";
//					}
//				}
//			});
//
	cm.push({
		dataIndex : 'location',
		header : biolims.common.storageLocalName,
		width : 100
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 100
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 150,
		hidden : true
	});
//
	cm.push({
		dataIndex : 'stateName',
		header : biolims.common.stateName,
		width : 150
	});
	cm.push({
		dataIndex : 'sampleBloodRecrive-id',
		header : biolims.master.sampleBloodRecriveId,
		width : 150,
		hidden : true
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 80,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')&&rec.get('id')!='NEW'){
					
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=sampleBloodInfo&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		} ]
	});
	cols.cm=cm;
	var loadParam={};
	var pId = "NEW";
	if($("#id_parent_hidden").val()){
		pId = $("#id_parent_hidden").val();
	}
	loadParam.url=ctx+"/sample/blood/showSampleDetailListJson.action?patientId="+ pId;
	var opts={};
	opts.title=biolims.sample.sampleInfo;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientTumorInformation.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientTumorInformationGrid.getStore().commitChanges();
//				crmPatientTumorInformationGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientTumorInformationGrid=gridEditTable("crmPatientTumorInformationdiv",cols,loadParam,opts);
	$("#crmPatientTumorInformationdiv").data("crmPatientTumorInformationGrid", crmPatientTumorInformationGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

