var crmPatientDialogTwoGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	   
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'gender',
		type:"string"
	});
	    fields.push({
		name:'dateOfBirth',
		type:"string"
	});
	    fields.push({
		name:'age',
		type:"string"
	});
	   
	    fields.push({
		name:'crmCustomerId-id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomerId-name',
		type:"string"
	});
	   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.master.patientId,
		width:10*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:120,
		sortable:true
	});
	cm.push({
		dataIndex:'gender',
		header:biolims.common.sname,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'dateOfBirth',
		header:biolims.common.birthDay,
		width:12*10,
		sortable:true
	});
	cm.push({
		dataIndex:'age',
		header:biolims.common.age,
		width:15*10,
		sortable:true
	});	
		cm.push({
		dataIndex:'crmCustomerId-id',
		header:biolims.sample.customerIdId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'crmCustomerId-name',
		header:biolims.sample.attendingDoctor,
		width:15*10,
		sortable:true
		});
	
		
		
	cols.cm=cm;
	var loadParam={};
	//alert(encodeURIComponent($("#code").val()));
	loadParam.url=ctx+"/crm/customer/patient/crmPatientSelectTwoJson.action?id="+encodeURIComponent(encodeURIComponent($("#code").val()));
	var opts={};
	opts.tbar = [];
	opts.tbar.push({
		text :biolims.master.lookEMR,
		handler : function(){
			
			window.open(ctx+'/crm/customer/patient/viewCrmPatient.action?id='+$('#selectId').val(),'');
		}
	});
	opts.title=biolims.master.patientMasterData;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setCrmPatientTwoFun(rec);
	};
	crmPatientDialogTwoGrid=gridTable("show_dialog_crmPatientTwo_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(crmPatientDialogTwoGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
