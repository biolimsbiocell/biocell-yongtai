var crmPatientGrid;
$(function() {
	var fields = [];
	fields.push({
		"data": "id",
		"title": biolims.sample.medicalNumber,
		"visible": true,
	});

	fields.push({
		"data": "name",
		"title": biolims.common.sname,
		"visible": true,
	});

	fields.push({
		"data": "gender",
		"title": biolims.common.gender,
		"visible": true,
		"name": biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			}
			if(data == "") {
				return '';
			}
		},
	});

	fields.push({
		"data": "dateOfBirth",
		"title": biolims.common.birthDay,
		"visible": true,
	});

	fields.push({
		"data": "age",
		"title": biolims.common.age,
		"visible": true,
	});

	fields.push({
		"data": "maritalStatus",
		"title": biolims.common.marriage,
		"visible": true,
		"name": biolims.common.married+"|"+biolims.common.unmarried,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.married;
			}
			if(data == "0") {
				return biolims.common.unmarried;
			}
			if(data == "") {
				return '';
			}
		},
	});


	fields.push({
		"data": "weichatId",
		"title": biolims.master.weichatId,
		"visible": true,
	});


//	fields.push({
//		"data": "isEnd",
//		"title": biolims.master.isEnd,
//		"visible": true,
//	});

	fields.push({
		"data": "createUser-id",
		"title": biolims.sample.createUserId,
		"visible": false,
	});

	fields.push({
		"data": "createUser-name",
		"title": biolims.sample.createUserName,
		"visible": true,
	});

	fields.push({
		"data": "createDate",
		"title": biolims.sample.createDate,
		"visible": true,
	});
	

	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		async: false,
		data: {
			moduleValue: "CrmPatient"
		},
		success: function(data) {
			var objData = JSON.parse(data);
			if(objData.success) {
				$.each(objData.data, function(i, n) {
					var str = {
						"data": n.fieldName,
						"title": n.label
					};
					fields.push(str);
				});

			} else {
				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
			}
		}
	});
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.uploadCSV,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
			csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						fileId: data.response.fileId
					},
					url: ctx + "/crm/customer/patient/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							if(data.prompt!=""){
								alert(data.prompt)
							}
							crmPatientGrid.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
					}
				});
			});

		}
	})
	tbarOpts.push({
			text: biolims.common.downloadCsvTemplet,
			action: function() {
				downCsv()
			}
		});
	var options = table(true, "", ctx + '/crm/customer/patient/showCrmPatientNewListJson.action',
		fields, tbarOpts);
	crmPatientGrid = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(crmPatientGrid);
	});
});
function downCsv(){
	window.location.href=ctx+"/js/crm/customer/patient/patient.csv";
}
// 新建
function add() {
	window.location = window.ctx +
		"/crm/customer/patient/editCrmPatient.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/customer/patient/editCrmPatient.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/customer/patient/viewCrmPatient.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
	var fields = [];
	fields.push({
		"searchName": "id",
		"type": "input",
		"txt": biolims.sample.medicalNumber
	});
	fields.push({
		"searchName": "name",
		"type": "input",
		"txt": biolims.common.sname
	});
	fields.push({
		"type": "select",
		"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
		"changeOpt": "''|1|0|3",
		"searchName" : "gender",
		"txt":biolims.common.gender
	});
	fields.push({
		"type": "dataTime",
		"searchName": "dateOfBirth##@@##",
		"mark": "c##@@##",
		"txt": biolims.common.birthDay
	});
	fields.push({
		"searchName": "age",
		"type": "input",
		"txt": biolims.common.age
	});
/*	fields.push({
		"type": "input",
		"searchName": "adultTeenager",
		"txt": biolims.common.adultChild
	});*/
	fields.push({
		"type": "select",
		"searchName": "maritalStatus",
		"options": biolims.common.pleaseChoose+"|"+biolims.common.married+"|"+biolims.common.unmarried,
		"changeOpt": "''|1|0",
		"txt": biolims.common.marriage
	});
/*	fields.push({
		"type": "input",
		"searchName": "consentReceived",
		"options": biolims.common.pleaseChoose+"|"+biolims.common.yes+"|"+biolims.common.yes,
		"changeOpt": "''|1|0",
		"txt": biolims.master.consentReceived
	});*/
	fields.push({
		"type": "input",
		"searchName": "weichatId",
		"txt": biolims.master.weichatId
	});
/*	fields.push({
		"searchName": "isEnd",
		"type": "input",
		"txt": biolims.master.isEnd
	});*/
	fields.push({
		"searchName": "createUser.name",
		"type": "input",
		"txt": biolims.sample.createUserName
	});
	fields.push({
		"type": "dataTime",
		"searchName": "createDate##@@##1",
		"mark": "s##@@##",
		"txt": biolims.sample.createDate+"(开始)"
	});
	fields.push({
		"type": "dataTime",
		"searchName": "createDate##@@##2",
		"mark": "e##@@##",
		"txt": biolims.sample.createDate+"(结束)"
	});
	fields.push({
		"type": "table",
		"table": crmPatientGrid
	});
	return fields;
}
