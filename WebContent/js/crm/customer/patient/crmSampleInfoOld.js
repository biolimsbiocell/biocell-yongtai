var crmSampleMainGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	
	fields.push({
		name : 'orderNum',
		type : "string"
	});
	fields.push({
		name : 'patientId',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
//	//添加订单
//	fields.push({
//		name : 'sampleOrder-id',
//		type : "string"
//	});
//	fields.push({
//		name : 'sampleOrder-name',
//		type : "string"
//	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.common.id,
		width : 40 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'code',
		hidden : false,
		header : biolims.common.code,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'sampleType-id',
		hidden : true,
		header : biolims.common.sampleTypeId,
		width : 40 * 6
	});
	cm.push({
		dataIndex : 'sampleType-name',
		hidden : false,
		header : biolims.common.sampleType,
		width : 40 * 6
	}); 
	cm.push({
		dataIndex : 'name',
		hidden : true,
		header : biolims.common.sampleName,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'patientName',
		hidden : false,
		header : biolims.common.patientName,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'productId',
		hidden : false,
		header : biolims.common.projectId,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.projectName,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'state',
		hidden : false,
		header : biolims.common.state,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'stateName',
		hidden : false,
		header : biolims.common.stateName,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'orderNum',
		hidden : false,
		header : biolims.sample.orderNum,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'patientId',
		hidden : false,
		header : biolims.sample.patientId,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : biolims.common.note,
		width : 50 * 6
	});	

	
	
	
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url=ctx+"/crm/customer/patient/showCrmSampleInfoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.master.clinicalSamplesInfo;
	opts.height = document.body.clientHeight-370;
	//怎么得到ID
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	crmSampleMainGrid = gridEditTable("crmSampleMaindiv", cols, loadParam, opts);
	// //
	// crmCustomerLinkManGrid=gridEditTable("crmCustomerLinkMandiv",cols,loadParam,opts);
	// $("#crmSampleOrderdiv").data("crmSampleOrderGrid", crmSampleOrderGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");

});


