var crmSampleOrderGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'gender',
		type : "string"
	});
	fields.push({
		name : 'birthDate',
		type : "string"
	});
	fields.push({
		name : 'diagnosisDate',
		type : "string"
	});
	fields.push({
		name : 'dicType-id',
		type : "string"
	});
	fields.push({
		name : 'dicType-name',
		type : "string"
	});
	fields.push({
		name : 'sampleStage',
		type : "string"
	});
	fields.push({
		name : 'inspectionDepartment-id',
		type : "string"
	});
	fields.push({
		name : 'inspectionDepartment-name',
		type : "string"
	});
	fields.push({
		name : 'crmProduct-id',
		type : "string"
	});
	fields.push({
		name : 'crmProduct-name',
		type : "string"
	});
	fields.push({
		name : 'samplingDate',
		type : "string"
	});
	fields.push({
		name : 'samplingLocation-id',
		type : "string"
	});
	fields.push({
		name : 'samplingLocation-name',
		type : "string"
	});
	fields.push({
		name : 'samplingNumber',
		type : "string"
	});
	fields.push({
		name : 'pathologyConfirmed',
		type : "string"
	});
	fields.push({
		name : 'bloodSampleDate',
		type : "string"
	});
	fields.push({
		name : 'plasmapheresisDate',
		type : "string"
	});
	fields.push({
		name : 'commissioner-id',
		type : "string"
	});
	fields.push({
		name : 'commissioner-name',
		type : "string"
	});
	fields.push({
		name : 'receivedDate',
		type : "string"
	});
	fields.push({
		name : 'sampleTypeId',
		type : "string"
	});
	fields.push({
		name : 'sampleTypeName',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'medicalNumber',
		type : "string"
	});
	fields.push({
		name : 'family',
		type : "string"
	});
	fields.push({
		name : 'familyPhone',
		type : "string"
	});
	fields.push({
		name : 'familySite',
		type : "string"
	});
	fields.push({
		name : 'medicalInstitutions',
		type : "string"
	});
	fields.push({
		name : 'medicalInstitutionsPhone',
		type : "string"
	});
	fields.push({
		name : 'medicalInstitutionsSite',
		type : "string"
	});
	fields.push({
		name : 'attendingDoctor',
		type : "string"
	});
	fields.push({
		name : 'attendingDoctorPhone',
		type : "string"
	});
	fields.push({
		name : 'attendingDoctorSite',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-id',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-name',
		type : "string"
	});
	fields.push({
		name : 'confirmDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'sampleFlag',
		type : "string"
	});
	fields.push({
		name : 'successFlag',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.sname,
		width : 50 * 6,

		sortable : true
	});
	var genderstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.male ], [ '0', biolims.common.female ] ]
	});

	var genderComboxFun = new Ext.form.ComboBox({
		store : genderstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'gender',
		header : biolims.common.gender,
		width : 10 * 6,

		renderer : Ext.util.Format.comboRenderer(genderComboxFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'birthDate',
		header : biolims.common.birthDay,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'diagnosisDate',
		header : biolims.common.diagnosisDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'dicType-id',
		hidden : true,
		header : biolims.sample.dicTypeId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'dicType-name',
		header : biolims.sample.dicTypeName,
		width : 20 * 10,
		sortable : true
	});
	var sampleStagestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'I' ], [ '2', 'II' ], [ '3', 'III' ], [ '4', 'IV' ] ]
	});

	var sampleStageComboxFun = new Ext.form.ComboBox({
		store : sampleStagestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'sampleStage',
		header : biolims.sample.sampleStage,
		width : 10 * 6,

		renderer : Ext.util.Format.comboRenderer(sampleStageComboxFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'inspectionDepartment-id',
		hidden : true,
		header : biolims.sample.inspectionDepartmentId,
		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'inspectionDepartment-name',
		header : biolims.sample.inspectionDepartmentName,

		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmProduct-id',
		hidden : true,
		header : biolims.common.productId,
		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmProduct-name',
		header : biolims.common.productName,

		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'samplingDate',
		header : biolims.sample.samplingDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'samplingLocation-id',
		hidden : true,
		header : biolims.sample.samplingLocationId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'samplingLocation-name',
		header : biolims.sample.samplingLocationName,

		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'samplingNumber',
		header : biolims.common.code,
		width : 20 * 6,

		sortable : true
	});
	var pathologyConfirmedstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes], [ '0', biolims.common.no ] ]
	});

	var pathologyConfirmedComboxFun = new Ext.form.ComboBox({
		store : pathologyConfirmedstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'pathologyConfirmed',
		header : biolims.sample.pathologyConfirmed,
		width : 20 * 6,

		renderer : Ext.util.Format.comboRenderer(pathologyConfirmedComboxFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'bloodSampleDate',
		header : biolims.sample.bloodSampleDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'plasmapheresisDate',
		header : biolims.sample.plasmapheresisDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'commissioner-id',
		hidden : true,
		header : biolims.common.commissionerId,
		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'commissioner-name',
		header : biolims.common.commissioner,

		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'receivedDate',
		header : biolims.sample.receivedDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'sampleTypeId',
		header : biolims.common.sampleTypeId,
		width : 50 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sampleTypeName',
		header : biolims.common.sampleType,
		width : 50 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'sampleCode',
		header : biolims.sample.sampleCode,
		width : 50 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'medicalNumber',
		header : biolims.sample.medicalNumber,
		width : 50 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'family',
		header : biolims.sample.family,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'familyPhone',
		header : biolims.sample.familyPhone,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'familySite',
		header : biolims.sample.familySite,
		width : 50 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'medicalInstitutions',
		header : biolims.sample.medicalInstitutions,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'medicalInstitutionsPhone',
		header : biolims.sample.medicalInstitutionsPhone,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'medicalInstitutionsSite',
		header : biolims.sample.medicalInstitutionsSite,
		width : 50 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'attendingDoctor',
		header : biolims.sample.attendingDoctor,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'attendingDoctorPhone',
		header : biolims.sample.attendingDoctorPhone,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'attendingDoctorSite',
		header : biolims.sample.attendingDoctorSite,
		width : 50 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 50 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : biolims.sample.createUserId,
		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : biolims.sample.createUserName,

		width : 50 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.sample.createDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-id',
		hidden : true,
		header : biolims.wk.approverId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-name',
		header : biolims.wk.approver,

		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmDate',
		header : biolims.wk.approverDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : biolims.common.stateName,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sampleFlag',
		header : biolims.sample.sampleFlag,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'successFlag',
		header : biolims.sample.successFlag,
		width : 20 * 6,

		sortable : true
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/crm/customer/patient/showCrmOrderListJson.action?id=" + $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.sample.orderList;
	opts.height = document.body.clientHeight - 370;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	crmSampleOrderGrid = gridEditTable("crmSampleOrderdiv", cols, loadParam, opts);
	// //
	// crmCustomerLinkManGrid=gridEditTable("crmCustomerLinkMandiv",cols,loadParam,opts);
	// $("#crmSampleOrderdiv").data("crmSampleOrderGrid", crmSampleOrderGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
