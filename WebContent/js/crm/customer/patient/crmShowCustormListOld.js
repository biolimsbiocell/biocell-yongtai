var crmCustormerGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'isFee',
		type : "string"
	});
	fields.push({
		name : 'fee',
		type : "string"
	});
	fields.push({
		name : 'consumptionDate',
		type : "date",
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'feeWay',
		type : "string"
	});
	fields.push({
		name : 'monthFee',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'bankNum',
		type : "string"
	});
	fields.push({
		name : 'receipNum',
		type : "string"
	});
	fields.push({
		name : 'posNum',
		type : "string"
	});
	fields.push({
		name : 'backFee',
		type : "string"
	});
	fields.push({
		name : 'backFeeDate',
		type : "date",
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'invoiceCode',
		type : "string"
	});
	fields.push({
		name : 'invoiceDate',
		type : "date",
		dateFormat : 'Y-m-d'
	});
	fields.push({
		name : 'invoiceNum',
		type : "string"
	});
	fields.push({
		name : 'alipayNum',
		type : "string"
	});
	fields.push({
		name : 'realFee',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'financeUser-id',
		type : "string"
	});
	fields.push({
		name : 'financeUser-name',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-id',
		type : "string"
	});

	fields.push({
		name : 'sampleOrder-name',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-productName',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-commissioner-name',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-inspectionDepartment-name',
		type : "string"
	});

	fields.push({
		name : 'sampleOrder-medicalInstitutions',
		type : "string"
	});
	fields.push({
		name : 'sampleOrder-attendingDoctor',
		type : "string"
	});

	fields.push({
		name : 'sampleOrder-MedicalNumber',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'isFee',
		header : biolims.master.isFee,
		width : 100,
		renderer : function(value, metadata, record, rowIndex, colIndex, store) {
			if (value == "0") {
				return biolims.master.notPaid;
			} else if (value == "4") {
				return biolims.master.hasPaidNotSubmit;
			} else if (value == "1") {
				return biolims.master.hasPaid;
			} else if (value == "6") {
				return biolims.master.delayCharge;
			} else if (value == "5") {
				return biolims.master.MonthFee;
			} else if (value == "2") {
				return biolims.common.cancel;
			} else if (value == "3") {
				return biolims.pooling.refund;
			} else {
				return biolims.common.pleaseChoose;
			}
		}
	});
	cm.push({
		dataIndex : 'fee',
		header : biolims.master.fee,
		width : 150,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'consumptionDate',
		header : biolims.master.paymentTime,
		width : 100,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			maxValue : new Date(),
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'feeWay',
		header : biolims.sample.payTypeName,
		width : 100,
		renderer : function(value, metadata, record, rowIndex, colIndex, store) {
			if (value == "0") {
				return biolims.master.suchIntermediaryBank;
			} else if (value == "1") {
				return biolims.master.posBankTransfer;
			} else if (value == "2") {
				return biolims.master.cash;
			} else if (value == "3") {
				return biolims.master.alipay;
			}
		}
	});

	cm.push({
		dataIndex : 'monthFee',
		header : biolims.master.MonthFee,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});

	cm.push({
		dataIndex : 'note',
		header : biolims.master.chargeNote,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'bankNum',
		header : biolims.master.bankCardNumber,
		width : 150,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'receipNum',
		header : biolims.master.receipNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'posNum',
		header : biolims.master.posNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'backFee',
		header : biolims.master.backFee,
		width : 150,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'backFeeDate',
		header : biolims.master.backFeeDate,
		width : 150,
		hidden : true,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			maxValue : new Date(),
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'invoiceCode',
		header : biolims.sample.billNumber,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'invoiceNum',
		header : biolims.sample.invoiceNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'invoiceDate',
		header : biolims.sample.invoiceDate,
		width : 150,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			// maxValue : new Date(),
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'alipayNum',
		header : biolims.master.alipayNum,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'realFee',
		header : biolims.master.realFee,
		width : 150,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true,
			allowNegative : false
		})
	});
	cm.push({
		dataIndex : 'financeUser-id',
		header : biolims.master.financeUserId,
		width : 150,
		hidden : true
	});
	cm.push({
		dataIndex : 'financeUser-name',
		header : biolims.master.financeUserName,
		width : 150,
		hidden : true
	});

	cm.push({
		dataIndex : 'sampleOrder-id',
		header : 'id',
		width : 10 * 15,
		hidden : true
	});
	cm.push({
		dataIndex : 'sampleOrder-name',
		header : biolims.master.inspectionPeopleName,
		width : 10 * 15,

	});
	cm.push({
		dataIndex : 'sampleOrder-productName',
		header : biolims.common.productName,
		width : 20 * 15
	});
	cm.push({
		dataIndex : 'sampleOrder-commissioner-name',
		header : biolims.master.commissioner,
		width : 150
	});
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 100,
		hidden : true
	});
	
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/crm/customer/patient/showCrmCustormerListJson.action?id=" + $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.master.charge4Confirmation;
	opts.height = document.body.clientHeight - 370;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	crmCustormerGrid = gridEditTable("crmCustormerdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
