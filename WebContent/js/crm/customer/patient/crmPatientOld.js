var crmPatientGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'lastName',
		type:"string"
	});
	    fields.push({
		name:'firstName',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'gender',
		type:"string"
	});
	    fields.push({
		name:'dateOfBirth',
		type:"string"
	});
	    fields.push({
		name:'age',
		type:"string"
	});
	    fields.push({
		name:'placeOfBirth',
		type:"string"
	});
	    fields.push({
		name:'race',
		type:"string"
	});
	    fields.push({
		name:'adultTeenager',
		type:"string"
	});
	    fields.push({
		name:'maritalStatus',
		type:"string"
	});
	    fields.push({
		name:'occupation',
		type:"string"
	});
	    fields.push({
		name:'consentReceived',
		type:"string"
	});
	    fields.push({
		name:'patientStatus-id',
		type:"string"
	});
	    fields.push({
		name:'patientStatus-name',
		type:"string"
	});
	    fields.push({
		name:'address',
		type:"string"
	});
	    fields.push({
		name:'jc',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber1',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber2',
		type:"string"
	});
	    fields.push({
		name:'telphoneNumber3',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient1',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient2',
		type:"string"
	});
	    fields.push({
		name:'additionalRecipient3',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber1',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber2',
		type:"string"
	});
	    fields.push({
		name:'additionalTelphoneNumber3',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'crmCustomerId-id',
		type:"string"
	});
	    fields.push({
		name:'crmCustomerId-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
			name:'isEnd',
			type:"string"
		});
	    fields.push({
			name:'weichatId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.sample.medicalNumber,
		width:10*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'lastName',
//		header:'姓',
//		width:50,
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'firstName',
//		header:'名',
//		width:50,
//		sortable:true
//	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:150,
		sortable:true
	});
	var genderstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.male], [ '0', biolims.common.female]]
	});
	
	var genderComboxFun = new Ext.form.ComboBox({
		store : genderstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'gender',
		header:biolims.common.gender,
		width:50,
		renderer: Ext.util.Format.comboRenderer(genderComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'dateOfBirth',
		header:biolims.common.birthDay,
		width:12*10,
		sortable:true
	});
	cm.push({
		dataIndex:'age',
		header:biolims.common.age,
		width:50,
		sortable:true
	});
//	cm.push({
//		dataIndex:'placeOfBirth',
//		header:'籍贯',
//		width:15*10,
//		sortable:true
//	});

	/*var adultTeenagerstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '成年' ], [ '0', '儿童' ]]
	});
	*/
	/*var adultTeenagerComboxFun = new Ext.form.ComboBox({
		store : adultTeenagerstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
cm.push({
		dataIndex:'adultTeenager',
		header:biolims.common.adultChild,
		width:10*10,
		//renderer: Ext.util.Format.comboRenderer(adultTeenagerComboxFun),				
		sortable:true
	});
	var maritalStatusstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.married], [ '0', biolims.common.unmarried]]
	});
	
	var maritalStatusComboxFun = new Ext.form.ComboBox({
		store : maritalStatusstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'maritalStatus',
		header:biolims.common.marriage,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(maritalStatusComboxFun),				
		sortable:true
	});


	var jcstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.patient], [ '0', biolims.common.healthy],[ '2', biolims.common.overseasMedical ]]
	});
	
	var jcComboxFun = new Ext.form.ComboBox({
		store : jcstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	/*cm.push({
		dataIndex:'jc',
		header:'检测个体类型',
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(jcComboxFun),				
		sortable:true
	});*/
	
	
//	cm.push({
//		dataIndex:'occupation',
//		header:'职业',
//		width:15*10,
//		sortable:true
//	});
	var consentReceivedstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ]]
	});
	
	var consentReceivedComboxFun = new Ext.form.ComboBox({
		store : consentReceivedstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'consentReceived',
		header:biolims.master.consentReceived,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(consentReceivedComboxFun),				
		sortable:true
	});
		cm.push({
		dataIndex:'patientStatus-id',
		hidden:true,
		header:biolims.master.patientStatusId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'patientStatus-name',
		header:biolims.master.patientStatusName,
		width:10*10,
		sortable:true
		});
		cm.push({
			dataIndex:'weichatId',
			header:biolims.master.weichatId,
			width:12*10,
			sortable:true
		});
//	cm.push({
//		dataIndex:'address',
//		header:'家庭住址',
//		width:15*10,
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'telphoneNumber2',
//		header:'主联系电话',
//		width:10*10,
//		sortable:true
//	}); 
//	cm.push({
//		dataIndex:'telphoneNumber1',
//		header:'其他联系电话',
//		width:10*10,
//		sortable:true
//	});
	
//	cm.push({
//		dataIndex:'telphoneNumber3',
//		header:'联系电话3',
//		width:10*10,
//		sortable:true,
//		hidden:true
//	});
		cm.push({
			dataIndex:'isEnd',
			header:biolims.master.isEnd,
			width:12*10,
			sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:10*10,
		sortable:true
		});
//		cm.push({
//		dataIndex:'crmCustomerId-id',
//		hidden:true,
//		header:'客户idID',
//		width:15*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'crmCustomerId-name',
//		header:'客户名称',
//		width:15*10,
//		sortable:true
//		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:12*10,
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/showCrmPatientListJson.action";
	var opts={};
	opts.title=biolims.master.testingIndividual;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	crmPatientGrid=gridTable("show_crmPatient_div",cols,loadParam,opts);
});
function patientButton(){
	$.post("/crm/customer/patient/patientButton.action",{"data":$("#patientNum").val()}, 
			function (data){
		message(data.message);
		}, "json");
	}
function add(){
		window.location=window.ctx+'/crm/customer/patient/editCrmPatient.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/crm/customer/patient/editCrmPatient.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/crm/customer/patient/viewCrmPatient.action?id=' + id;
}
function exportexcel() {
	crmPatientGrid.title = biolims.master.testingIndividualList;
	var vExportContent = crmPatientGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(crmPatientGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});