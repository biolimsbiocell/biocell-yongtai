var crmPatientSurgeriesGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'previousProceduresSurgeies',
		type:"string"
	});
	   fields.push({
		name:'nameOfProcedureOrSurgery',
		type:"string"
	});
	   fields.push({
		name:'dateOfProcedureAndSurgery',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'hospital',
		type:"string"
	});
	   fields.push({
		name:'primarySurgeon',
		type:"string"
	});
	   fields.push({
		name:'resectionMargins',
		type:"string"
	});
	   fields.push({
		name:'priorCancerHistory',
		type:"string"
	});
	   fields.push({
		name:'familyHistoryOfCancer',
		type:"string"
	});
	   fields.push({
		name:'infectiousDiseases',
		type:"string"
	});
	   fields.push({
		name:'historyOfSmoking',
		type:"string"
	});
	   fields.push({
		name:'environmentalExposures',
		type:"string"
	});
	   fields.push({
		name:'otherDiseases',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-id',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-name',
		type:"string"
	});
		fields.push({
			name : 'crmPatientDiagnosis-id',
			type : "string"
		});
		fields.push({
			name : 'crmPatientDiagnosis-diagnosis',
			type : "string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.master.surgeryId,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
			var previousProceduresSurgeiescob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : biolims.master.surgery
				}, {
					id : '0',
					name : biolims.common.operate
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'previousProceduresSurgeies',
		hidden : false,
		header:biolims.master.previousProceduresSurgeies,
		width:15*10,
		
		renderer: Ext.util.Format.comboRenderer(previousProceduresSurgeiescob),editor: previousProceduresSurgeiescob
	});
	cm.push({
		dataIndex:'nameOfProcedureOrSurgery',
		hidden : false,
		header:biolims.master.nameOfProcedureOrSurgery,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dateOfProcedureAndSurgery',
		hidden : false,
		header:biolims.master.dateOfProcedureAndSurgery,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'hospital',
		hidden : false,
		header:biolims.master.hospitalName,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'primarySurgeon',
		hidden : false,
		header:biolims.sample.attendingDoctor,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'resectionMargins',
		hidden : false,
		header:biolims.master.resectionMargins,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'priorCancerHistory',
//		hidden : false,
//		header:'自身癌症史',
//		width:15*10,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'familyHistoryOfCancer',
//		hidden : false,
//		header:'家族癌症史 ',
//		width:15*10,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'infectiousDiseases',
//		hidden : false,
//		header:'传染病',
//		width:15*10,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'historyOfSmoking',
//		hidden : false,
//		header:'吸烟史',
//		width:15*10,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'environmentalExposures',
//		hidden : false,
//		header:'环境风险/毒物 ',
//		width:15*10,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'otherDiseases',
		hidden : false,
		header:biolims.master.otherDiseases,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-id',
		hidden : true,
		header:biolims.master.patientId,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-name',
		hidden : true,
		header:biolims.common.patientName,
		width:10*10
	});
	cm.push({
		dataIndex : 'crmPatientDiagnosis-id',
		hidden : true,
		header : biolims.master.crmPatientDiagnosisId,
		width : 10 * 10
	});
	cm.push({
		dataIndex : 'crmPatientDiagnosis-diagnosis',
		header : biolims.master.crmPatientDiagnosisNote,
		width : 10 * 10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/showCrmPatientSurgeriesListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.surgeryDetail;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientSurgeries.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientSurgeriesGrid.getStore().commitChanges();
//				crmPatientSurgeriesGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		iconCls : 'add',
//		handler : function() {
//				var records = crmPatientDiagnosisGrid.getSelectRecord()
////				if (records && records.length > 0) {
////					$.each(records, function(i, obj) {
////						var ob = crmPatientSurgeriesGrid.getStore().recordType;
////						var p = new ob({});
////						p.isNew = true;
////						p.set("crmPatientDiagnosis-id", obj.get("id"));
////						p.set("crmPatientDiagnosis-diagnosis", obj.get("diagnosis"));
////						crmPatientSurgeriesGrid.stopEditing();
////						crmPatientSurgeriesGrid.getStore().insert(0, p);
////						crmPatientSurgeriesGrid.startEditing(0, 0);
////					});
////				}
////				else{
//					var ob = crmPatientSurgeriesGrid.getStore().recordType;
//					var p = new ob({});
//					p.isNew = true;
//					crmPatientSurgeriesGrid.stopEditing();
//					crmPatientSurgeriesGrid.getStore().insert(0, p);
//					crmPatientSurgeriesGrid.startEditing(0, 0);
////				}
//			}
//	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmPatientSurgeriesGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientSurgeries",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmPatientSurgeriesGrid.getStore().commitChanges();
						crmPatientSurgeriesGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientSurgeriesGrid=gridEditTable("crmPatientSurgeriesdiv",cols,loadParam,opts);
	$("#crmPatientSurgeriesdiv").data("crmPatientSurgeriesGrid", crmPatientSurgeriesGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmPatientFun(){
	var win = Ext.getCmp('selectcrmPatient');
	if (win) {win.close();}
	var selectcrmPatient= new Ext.Window({
	id:'selectcrmPatient',modal:true,title:biolims.master.selectPatientId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmPatientSelect.action?flag=crmPatient' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmPatient.close(); }  }]  });     selectcrmPatient.show(); }
	function setcrmPatient(id,name){
		var gridGrid = $("#crmPatientSurgeriesdiv").data("crmPatientSurgeriesGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmPatient-id',id);
			obj.set('crmPatient-name',name);
		});
		var win = Ext.getCmp('selectcrmPatient')
		if(win){
			win.close();
		}
	}
	
