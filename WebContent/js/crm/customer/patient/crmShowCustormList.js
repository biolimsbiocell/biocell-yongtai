var crmCustormerGrid;
var crmCustormerChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#crmPatient_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "isFee",
		"title": biolims.master.isFee,
		"createdCell": function(td) {
			$(td).attr("saveName", "isFee");
			$(td).attr("selectOpt", "|"+biolims.master.notPaid+"|"+biolims.master.hasPaidNotSubmit
					+"|"+biolims.master.hasPaid+"|"+biolims.master.delayCharge
					+"|"+biolims.master.MonthFee+"|"+biolims.common.cancel);
		},
		"visible": true,
		"className": "select",
		"name": "|"+biolims.master.notPaid+"|"+biolims.master.hasPaidNotSubmit
		+"|"+biolims.master.hasPaid+"|"+biolims.master.delayCharge
		+"|"+biolims.master.MonthFee+"|"+biolims.common.cancel,
		"render": function(data, type, full, meta) {
			if (data == "0") {
				return biolims.master.notPaid;
			} else if (data == "4") {
				return biolims.master.hasPaidNotSubmit;
			} else if (data == "1") {
				return biolims.master.hasPaid;
			} else if (data == "6") {
				return biolims.master.delayCharge;
			} else if (data == "5") {
				return biolims.master.MonthFee;
			} else if (data == "2") {
				return biolims.common.cancel;
			} else if (data == "3") {
				return biolims.pooling.refund;
			} else {
				return biolims.common.pleaseChoose;
			}
		}
	});
	colOpts.push({
		"data": "fee",
		"title": biolims.master.fee,
		"createdCell": function(td) {
			$(td).attr("saveName", "fee");
		},
		"visible": true,
		"className": "edit"
	});
	colOpts.push({
		"data": "consumptionDate",
		"title": biolims.master.paymentTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "consumptionDate");
		},
		"className": "date"
	});
	colOpts.push({
		"data": "feeWay",
		"title": biolims.sample.payTypeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "feeWay");
			$(td).attr("selectOpt", "|"+biolims.master.suchIntermediaryBank
					+"|"+biolims.master.posBankTransfer
					+"|"+biolims.master.cash+"|"+biolims.master.alipay);
		},
		"visible": true,
		"className": "select",
		"name": "|"+biolims.master.suchIntermediaryBank
		+"|"+biolims.master.posBankTransfer
		+"|"+biolims.master.cash+"|"+biolims.master.alipay,
		"render": function(data, type, full, meta) {
			if (data == "0") {
				return biolims.master.suchIntermediaryBank;
			} else if (data == "1") {
				return biolims.master.posBankTransfer;
			} else if (data == "2") {
				return biolims.master.cash;
			} else if (data == "3") {
				return biolims.master.alipay;
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data": "monthFee",
		"title": biolims.master.MonthFee,
		"createdCell": function(td) {
			$(td).attr("saveName", "monthFee");
		},
		"visible": true,
		"className": "edit"
	});
	colOpts.push({
		"data": "note",
		"title": biolims.master.chargeNote,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		},
		"visible": true,
		"className": "edit"
	});
	colOpts.push({
		"data": "bankNum",
		"title": biolims.master.bankCardNumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "bankNum");
		},
		"visible": false,
		"className": "edit"
	});
	colOpts.push({
		"data": "receipNum",
		"title": biolims.master.receipNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "receipNum");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "posNum",
		"title": biolims.master.posNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "posNum");
		},
		"visible": true,
		"className": "edit"
	});
	colOpts.push({
		"data": "backFee",
		"title": biolims.master.backFee,
		"createdCell": function(td) {
			$(td).attr("saveName", "backFee");
		},
		"visible": false,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "backFeeDate",
		"title": biolims.master.backFeeDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "backFeeDate");
		},
		"visible": true,
		"className": "date"
	});
	
	colOpts.push({
		"data": "invoiceCode",
		"title": biolims.sample.billNumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "invoiceCode");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "invoiceNum",
		"title": biolims.sample.invoiceNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "invoiceNum");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "invoiceDate",
		"title": biolims.sample.invoiceDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "invoiceDate");
		},
		"visible": true,
		"className": "date"
	});
	
	colOpts.push({
		"data": "alipayNum",
		"title": biolims.master.alipayNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "alipayNum");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "realFee",
		"title": biolims.master.realFee,
		"createdCell": function(td) {
			$(td).attr("saveName", "realFee");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "state",
		"title": biolims.common.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "financeUser-id",
		"title": biolims.master.financeUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "financeUser-id");
		},
		"visible": false,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "financeUser-name",
		"title": biolims.master.financeUserName,
		"createdCell": function(td) {
			$(td).attr("saveName", "financeUser-name");
		},
		"visible": true,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "sampleOrder-id",
		"title": "订单id",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-id");
		},
		"visible": false,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "sampleOrder-name",
		"title": biolims.master.inspectionPeopleName,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-name");
		},
		"visible": false,
		"className": "edit"
	});
	
	colOpts.push({
		"data": "sampleOrder-productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-productName");
		},
		"visible": true
	});
	
	colOpts.push({
		"data": "sampleOrder-commissioner-name",
		"title": biolims.master.commissioner,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-commissioner-name");
		},
		"visible": true
	});
	
	colOpts.push({
		"data": "id",
		"title": "ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false
	});
	
	

	var crmCustormerOptions = table(true,
		id,
		'/crm/customer/patient/showCrmCustormerNewListJson.action', colOpts, tbarOpts)
	crmCustormerGrid = renderData($("#crmCustormerGrid"), crmCustormerOptions);
	crmCustormerGrid.on('draw', function() {
		crmCustormerChangeLog = crmCustormerGrid.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});

// 保存
function saveStorageReagentBuySerial(ele) {
	var data = saveStorageReagentBuySerialjson(ele);
	var ele = $("#crmCustormerGrid");
	var changeLog = biolims.master.communicateInformation+"：";
	changeLog = getChangeLog(data, ele, changeLog);

	$.ajax({
		type: 'post',
		url: '/storage/saveStorageReagentBuySerialTable.action',
		data: {
			id: $("#storage_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveStorageReagentBuySerialjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字

			if(k == "storage-name") {
				json["storage-id"] = $(tds[j]).attr("storage-id");
				continue;
			}

			if(k == "costCenter-name") {
				json["costCenter-id"] = $(tds[j]).attr("costCenter-id");
				continue;
			}

			if(k == "position-name") {
				json["position-id"] = $(tds[j]).attr("position-id");
				continue;
			}

			if(k == "storageInItem-name") {
				json["storageInItem-id"] = $(tds[j]).attr("storageInItem-id");
				continue;
			}

			if(k == "regionType-name") {
				json["regionType-id"] = $(tds[j]).attr("regionType-id");
				continue;
			}

			if(k == "linkStorageItem-name") {
				json["linkStorageItem-id"] = $(tds[j]).attr("linkStorageItem-id");
				continue;
			}

			if(k == "rankType-name") {
				json["rankType-id"] = $(tds[j]).attr("rankType-id");
				continue;
			}

			if(k == "currencyType-name") {
				json["currencyType-id"] = $(tds[j]).attr("currencyType-id");
				continue;
			}

			if(k == "timeUnit-name") {
				json["timeUnit-id"] = $(tds[j]).attr("timeUnit-id");
				continue;
			}

			if(k == "unit-name") {
				json["unit-id"] = $(tds[j]).attr("unit-id");
				continue;
			}

			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		crmCustormerChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}