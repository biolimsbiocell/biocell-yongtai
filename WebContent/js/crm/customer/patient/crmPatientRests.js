var crmPatientRestsGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'diagnosis',
		type : "string"
	});
	fields.push({
		name : 'dateOfDiagnosis',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'hospitalName',
		type : "string"
	});
	fields.push({
		name : 'primaryPhysician',
		type : "string"
	});
	fields.push({
		name : 'patientAge',
		type : "string"
	});
	fields.push({
		name : 'diagnosisResult',
		type : "string"
	});
	fields.push({
		name : 'stageOfDisease',
		type : "string"
	});
	fields.push({
		name : 'recurrencesOrMetastases',
		type : "string"
	});
	fields.push({
		name : 'diagnosisDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'locationOfTumor',
		type : "string"
	});
	fields.push({
		name : 'treatment',
		type : "string"
	});
	fields.push({
		name : 'radiotherapy',
		type : "string"
	});
	fields.push({
		name : 'chemotherapy',
		type : "string"
	});
	fields.push({
		name : 'targetedTherapy',
		type : "string"
	});
	fields.push({
		name : 'crmPatient-name',
		type : "string"
	});
	fields.push({
		name : 'tnm',
		type : "string"
	});
	fields.push({
		name : 'crmPatient-id',
		type : "string"
	});
	fields.push({
		name : 'result',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.master.diagnosisId,
		width : 15 * 10,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'dateOfDiagnosis',
		hidden : false,
		header : biolims.master.dateOfDiagnosis,
		width : 12 * 10,

		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'diagnosis',
		hidden : false,
		header : biolims.master.diagnosis,
		width : 15 * 10,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'hospitalName',
		hidden : false,
		header : biolims.master.hospitalName,
		width : 20 * 10,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'primaryPhysician',
		hidden : false,
		header : biolims.sample.attendingDoctor,
		width : 15 * 10,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'patientAge',
		hidden : false,
		header : biolims.master.patientAge,
		width : 15 * 10,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex : 'tnm',
//		hidden : false,
//		header : 'TNM分期',
//		width : 15 * 10,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex : 'diagnosisResult',
		hidden : false,
		header : biolims.master.diagnosisResult,
		width : 15 * 10,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'result',
		hidden : false,
		header : biolims.common.result,
		width : 15 * 10,

		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex : 'stageOfDisease',
//		hidden : false,
//		header : '疾病等级',
//		width : 15 * 10,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	var recurrencesOrMetastasescob = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '是'
//			}, {
//				id : '0',
//				name : '否'
//			} ]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex : 'recurrencesOrMetastases',
//		hidden : false,
//		header : '复发或转移',
//		width : 15 * 10,
//
//		renderer : Ext.util.Format.comboRenderer(recurrencesOrMetastasescob),
//		editor : recurrencesOrMetastasescob
//	});
//	cm.push({
//		dataIndex : 'diagnosisDate',
//		hidden : false,
//		header : '时间',
//		width : 12 * 10,
//
//		renderer : formatDate,
//		editor : new Ext.form.DateField({
//			format : 'Y-m-d'
//		})
//	});
//	cm.push({
//		dataIndex : 'locationOfTumor',
//		hidden : false,
//		header : '肿瘤部位',
//		width : 15 * 10,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	var treatmentcob = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '是'
//			}, {
//				id : '0',
//				name : '否'
//			} ]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex : 'treatment',
//		hidden : false,
//		header : '是否治疗',
//		width : 15 * 10,
//
//		renderer : Ext.util.Format.comboRenderer(treatmentcob),
//		editor : treatmentcob
//	});
//	cm.push({
//		dataIndex : 'radiotherapy',
//		hidden : false,
//		header : '放疗',
//		width : 15 * 10,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'chemotherapy',
//		hidden : false,
//		header : '化疗',
//		width : 15 * 10,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'targetedTherapy',
//		hidden : false,
//		header : '靶向治疗',
//		width : 15 * 10,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	var previousProceduresSurgeiescob = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '是'
//			}, {
//				id : '0',
//				name : '否'
//			} ]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
	cm.push({
		dataIndex : 'crmPatient-name',
		hidden : true,
		header : biolims.common.patientName,
		width : 10 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmPatient-id',
		hidden : true,
		header : biolims.master.patientId,
		width : 10 * 10
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 80,
		header :biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')&&rec.get('id')!='NEW'){
					
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=crmPatientRests&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		} ]
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/customer/patient/showCrmPatientRestsListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.master.cancerDiagnosis;
	opts.height = document.body.clientHeight - 300;
	opts.tbar = [];
//	opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientDiagnosis.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientRestsGrid.getStore().commitChanges();
//				crmPatientRestsGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmPatientRestsGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientDiagnosis",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmPatientRestsGrid.getStore().commitChanges();
						crmPatientRestsGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientRestsGrid = gridEditTable("crmPatientRestsdiv", cols,
			loadParam, opts);
	$("#crmPatientRestsdiv").data("crmPatientRestsGrid",
			crmPatientRestsGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmPatientFun() {
	var win = Ext.getCmp('selectcrmPatient');
	if (win) {
		win.close();
	}
	var selectcrmPatient = new Ext.Window(
			{
				id : 'selectcrmPatient',
				modal : true,
				title : biolims.master.selectPatientId,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/CrmPatientSelect.action?flag=crmPatient' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						selectcrmPatient.close();
					}
				} ]
			});
	selectcrmPatient.show();
}
function setcrmPatient(id, name) {
	var gridGrid = $("#crmPatientRestsdiv").data("crmPatientRestsGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('crmPatient-id', id);
		obj.set('crmPatient-name', name);
	});
	var win = Ext.getCmp('selectcrmPatient')
	if (win) {
		win.close();
	}
}
