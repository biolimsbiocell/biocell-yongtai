var crmPatientPathologyGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'tumorLocation-id',
			type:"string"
		});
	    fields.push({
			name:'site-id',
			type:"string"
		});
	    fields.push({
			name:'pathologicalDiagnosis',
			type:"string"
		});
	    fields.push({
			name:'grade',
			type:"string"
		});
	    fields.push({
			name:'differentiation',
			type:"string"
		});
	    fields.push({
			name:'degreeOfDifferentiation-id',
			type:"string"
		});
	    fields.push({
			name:'biopsy',
			type:"string"
		});
	   fields.push({
		name:'examinationItem',
		type:"string"
	});
	    fields.push({
			name:'dateOfBiopsy',
			type : "date",
			dateFormat:'Y-m-d'
		});
	    fields.push({
			name:'result',
			type:"string"
		});
	    fields.push({
			name:'primaryVsSecondary',
			type:"string"
		});
	    
	    
	    
	   fields.push({
		name:'dateOfExamination',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'hospital',
		type:"string"
	});
	   fields.push({
		name:'examination',
		type:"string"
	});
	   fields.push({
		name:'hospitalPatient',
		type:"string"
	});
	   fields.push({
		name:'keyResultsDescription',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-id',
		type:"string"
	});
	    fields.push({
		name:'crmPatient-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.master.pathologicalExaminationId,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
		var examinationItemstore = new Ext.data.JsonStore({
					root : 'results',
					remoteSort : true,
					fields : [ {
						name : 'id'
					}, {
						name : 'name'
					} ],
					proxy : new Ext.data.HttpProxy({
						url : window.ctx + '/dic/type/dicTypeCobJson.action?type=bljcjcxm',
						method : 'POST'
						})
					});

					examinationItemstore.load();
					var examinationItemcob = new Ext.form.ComboBox({
						store : examinationItemstore,
						displayField : 'name',
						valueField : 'id',
						typeAhead : true,
						mode : 'remote',
						forceSelection : true,
						triggerAction : 'all',
						selectOnFocus : true
					});
	cm.push({
		dataIndex:'examinationItem',
		hidden : false,
		header:biolims.master.examinationItem,
		width:15*10,
		
		renderer: Ext.util.Format.comboRenderer(examinationItemcob),editor: examinationItemcob
	});
	cm.push({
		dataIndex:'dateOfExamination',
		hidden : false,
		header:biolims.master.dateOfExamination,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'keyResultsDescription',
		hidden : false,
		header:biolims.master.keyResultsDescription,
		width:25*10,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tumorLocationString',
		hidden : false,
		header:"肿瘤所在器官",
		width:25*10,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'siteString',
		hidden : false,
		header:"肿瘤位置",
		width:25*10,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	
//	var tlstore = new Ext.data.JsonStore({
//		root : 'results',
//		remoteSort : true,
//		fields : [ {
//			name : 'id'
//		}, {
//			name : 'name'
//		} ],
//		proxy : new Ext.data.HttpProxy({
//			url : window.ctx + '/tumour/position/showDicMainTypeListJson.action',
//			method : 'POST'
//		})
//	});
//	tlstore.load();
//	var tlcob = new Ext.form.ComboBox({
//		store : tlstore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'remote',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//		cm.push({
//		dataIndex:'tumorLocation-id',
//		header:biolims.master.tumorLocationId,
//		width:10*10,
//		renderer : Ext.util.Format.comboRenderer(tlcob),
//		editor : tlcob
//		});
////		cm.push({
////		dataIndex:'tumorLocation-name',
////		header:'肿瘤所在器官',
////		width:15*10,
////		sortable:true
////		});
//		var sitestore = new Ext.data.JsonStore({
//			root : 'results',
//			remoteSort : true,
//			fields : [ {
//				name : 'id'
//			}, 
//			{
//				name : 'name'
//			},
//			{
//				name : 'superior'
//			}],
//			proxy : new Ext.data.HttpProxy({
//				url : window.ctx + '/tumour/position/showDicMainTypeList.action',
//				method : 'POST'
//			})
//		});
//		sitestore.load();
//		sitestore.on('load',function(store,record,opts){
//			var records = crmPatientTumorInformationGrid.getSelectRecord();	
//			if(records.length>0){
//				store.filterBy(function(record){
//					return record.get("superior")==records[0].get("tumorLocation-id")
//				});
//			}
//		});
//		var sitecob = new Ext.form.ComboBox({
//			store : sitestore,
//			displayField : 'name',
//			valueField : 'id',
//			typeAhead : true,
//			mode : 'remote',
//			forceSelection : true,
//			triggerAction : 'all',
//			selectOnFocus : true,
//			listeners: {  //为Combo添加select事件
//				focus: function(combo, record, index) {   // 该事件会返回选中的项对应在 store中的 record值. index参数是排列号.
//					
//					sitestore.load();
//			  }
//			  }
//		});
//		cm.push({
//		dataIndex:'site-id',
//		header:biolims.master.siteId,
//		width:10*10,
//		renderer : Ext.util.Format.comboRenderer(sitecob),
//		editor : sitecob
//		});
//		cm.push({
//		dataIndex:'site-name',
//		header:'肿瘤位置',
//		width:15*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'pathologicalDiagnosis',
//		header:'病理诊断',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//		});
		cm.push({
		dataIndex:'pathologicalDiagnosis',
		header:biolims.master.pathologicalDiagnosis,
		width:35*10,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
		});
//		var gradestore = new Ext.data.JsonStore({
//			root : 'results',
//			remoteSort : true,
//			fields : [ {
//				name : 'id'
//			}, {
//				name : 'name'
//			} ],
//			proxy : new Ext.data.HttpProxy({
//				url : window.ctx + '/dic/type/dicTypeCobJson.action?type=showgradeFun',
//				method : 'POST'
//			})
//		});
//		gradestore.load();
//		var gradecob = new Ext.form.ComboBox({
//			store : gradestore,
//			displayField : 'name',
//			valueField : 'id',
//			typeAhead : true,
//			mode : 'remote',
//			forceSelection : true,
//			triggerAction : 'all',
//			selectOnFocus : true
//		});
		cm.push({
		dataIndex:'grade',
		header:biolims.common.grade,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
		});
//		cm.push({
//		dataIndex:'grade-name',
//		header:'等级',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//		});
	var differentiationstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ]]
	});
	
	var differentiationComboxFun = new Ext.form.ComboBox({
		store : differentiationstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'differentiation',
		header:biolims.master.differentiation,
		width:15*5,
		renderer: Ext.util.Format.comboRenderer(differentiationComboxFun),				
		editor:differentiationComboxFun
	});
	var degreestore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=fhcd',
			method : 'POST'
		})
	});
	degreestore.load();
	var degreecob = new Ext.form.ComboBox({
		store : degreestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
		cm.push({
		dataIndex:'degreeOfDifferentiation-id',
		header:biolims.master.degreeOfDifferentiationId,
		width:15*5,
		renderer : Ext.util.Format.comboRenderer(degreecob),
		editor : degreecob
		});
//		cm.push({
//		dataIndex:'degreeOfDifferentiation-name',
//		header:'分化程度',
//		width:15*10,
//		sortable:true
//		});
	var biopsystore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ]]
	});
	
	var biopsyComboxFun = new Ext.form.ComboBox({
		store : biopsystore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'biopsy',
		header:biolims.master.biopsy,
		width:15*8,
		renderer: Ext.util.Format.comboRenderer(biopsyComboxFun),
		editor:biopsyComboxFun
	});
	cm.push({
		dataIndex:'dateOfBiopsy',
		header:biolims.master.dateOfBiopsy,
		width:12*10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex:'result',
		header:biolims.common.result,
		width:25*10,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	var primaryVsSecondarystore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.master.primary ], [ '0', biolims.master.metastasis]]
	});
	
	var primaryVsSecondaryComboxFun = new Ext.form.ComboBox({
		store : primaryVsSecondarystore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'primaryVsSecondary',
		header:biolims.master.primaryVsSecondary,
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(primaryVsSecondaryComboxFun),				
		editor:primaryVsSecondaryComboxFun
	});
	cm.push({
		dataIndex:'hospital',
		hidden : false,
		header:biolims.master.hospitalName,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'examination',
		hidden : false,
		header:biolims.master.examination,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hospitalPatient',
		hidden : false,
		header:biolims.master.hospitalPatient,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-id',
		hidden : true,
		header:biolims.master.patientId,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmPatient-name',
		hidden : true,
		header:biolims.common.patientName,
		width:10*10
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 80,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')&&rec.get('id')!='NEW'){
					
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=crmPatientPathology&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		} ]
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/customer/patient/showCrmPatientPathologyListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={}; 
	opts.title=biolims.master.pathologicalExamination;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientPathology.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientPathologyGrid.getStore().commitChanges();
//				crmPatientPathologyGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = crmPatientPathologyGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientPathology",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						crmPatientPathologyGrid.getStore().commitChanges();
						crmPatientPathologyGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	crmPatientPathologyGrid=gridEditTable("crmPatientPathologydiv",cols,loadParam,opts);
	$("#crmPatientPathologydiv").data("crmPatientPathologyGrid", crmPatientPathologyGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmPatientFun(){
	var win = Ext.getCmp('selectcrmPatient');
	if (win) {win.close();}
	var selectcrmPatient= new Ext.Window({
	id:'selectcrmPatient',modal:true,title:biolims.master.selectPatientId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmPatientSelect.action?flag=crmPatient' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcrmPatient.close(); }  }]  });     selectcrmPatient.show(); }
	function setcrmPatient(id,name){
		var gridGrid = $("#crmPatientPathologydiv").data("crmPatientPathologyGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmPatient-id',id);
			obj.set('crmPatient-name',name);
		});
		var win = Ext.getCmp('selectcrmPatient')
		if(win){
			win.close();
		}
	}
	
