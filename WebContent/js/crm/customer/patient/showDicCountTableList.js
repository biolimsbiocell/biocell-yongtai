var cGrid;
$(function(){
	var cols={};
	var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'path',
		type:"string"
	});
	fields.push({
		name:'type-name',
		type:"string"
	});
	fields.push({
		name:'subType-name',
		type:"string"
	});
	fields.push({
		name:'state-name',
		type:"string"
	});
	fields.push({
		name:'note',
	    type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:100,
		sortable:true,
		hidden:true
	
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:400,
		sortable:true
	});
	cm.push({
		dataIndex:'type-name',
		header:biolims.storage.studyType,
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'subType-name',
		header:biolims.common.type,
		width:200,
		sortable:true
	});
	cm.push({
		dataIndex:'path',
		header:biolims.common.path,
		width:100,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.sample.note,
		width:100,
		sortable:true
	});
	cm.push({
		dataIndex:'state-name',
		header:biolims.common.state,
		width:100,
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/sample/dicCountTable/showDicCountTableListJson.action?queryMethod=" + $("#queryMethod").val();
	var opts={};
	opts.height=document.body.clientHeight-30;
	opts.title=biolims.master.calculationMethod4DictionaryTables;
	opts.rowselect=function(id){
		$("#id").val(id);
	};
	opts.rowdblclick=function(id){
		$('#id').val(id);
		edit();
	};
	cGrid=gridTable("sc_d_grid_div",cols,loadParam,opts);
})
function add(){
	
	var url = "";
	if($("#sub_type_id").val()){
		
		url = "&subTypeId="+$("#sub_type_id").val();
	}
		window.location=window.ctx+"/sample/dicCountTable/toEditDicCountTable.action?reqMethodType=add"+url;
	}
function edit(){
	var id="";
	id=document.getElementById("id").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/sample/dicCountTable/toEditDicCountTable.action?reqMethodType=modify&id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/sample/dicCountTable/toEditDicCountTable.action?reqMethodType=view&id=' + id;
}
function exportexcel() {
	cGrid.title = biolims.common.exportList;
	var vExportContent = cGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(cGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);

	});

});