$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/crm/kb/editKnowledgeBase.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/kb/showKnowledgeBaseList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	
	save();
});	
function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/kb/save.action";
		$("#toolbarbutton_save").hide();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/crm/kb/copyKnowledgeBase.action?id=' + $("#knowledgeBase_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#knowledgeBase_id").val() + "&tableId=knowledgeBase");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'知识库',
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
function uploadFile1() {
	var isUpload = true;
	
	load("/operfile/toCommonUpload.action", { // 是否修改
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			
			$("#knowledgeBase_path").val(data.fileId);
			$("#knowledgeBase_path_name").val(data.fileName);
			
			
		});
	});
}
function downFile1() {
var id = $("#knowledgeBase_path").val();	
window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
}