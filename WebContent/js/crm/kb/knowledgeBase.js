var knowledgeBaseGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'original',
		type:"string"
	});
	    fields.push({
		name:'title',
		type:"string"
	});
	    fields.push({
		name:'daodu',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'antistop',
		type:"string"
	});
	    fields.push({
		name:'correlation',
		type:"string"
	});
	    fields.push({
		name:'noopen',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'type-id',
		hidden:true,
		header:'选择类别ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'type-name',
		header:'选择类别',
		width:15*10,
		sortable:true
		});
	var originalstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ]]
	});
	
	var originalComboxFun = new Ext.form.ComboBox({
		store : originalstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	/*cm.push({
		dataIndex:'original',
		header:'是否原创',
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(originalComboxFun),				
		sortable:true
	});*/
	cm.push({
		dataIndex:'title',
		header:'标题',
		width:15*10,
		sortable:true
	});
	/*cm.push({
		dataIndex:'daodu',
		header:'导读',
		width:15*10,
		sortable:true
	});*/
	cm.push({
		dataIndex:'note',
		header:'内容',
		width:50*10,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'antistop',
		header:'关键词',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'correlation',
		header:'相关知识',
		width:15*10,
		sortable:true
	});
	var noopenstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ]]
	});
	
	var noopenComboxFun = new Ext.form.ComboBox({
		store : noopenstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'noopen',
		header:'不公开',
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(noopenComboxFun),				
		sortable:true
	});
		cm.push({
		dataIndex:'state-id',
		hidden:true,
		header:'状态ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'state-name',
		header:'状态',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:12*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/kb/showKnowledgeBaseListJson.action";
	var opts={};
	opts.title="知识库";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	knowledgeBaseGrid=gridTable("show_knowledgeBase_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/kb/editKnowledgeBase.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/crm/kb/editKnowledgeBase.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/crm/kb/viewKnowledgeBase.action?id=' + id;
}
function exportexcel() {
	knowledgeBaseGrid.title = '导出列表';
	var vExportContent = knowledgeBaseGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(knowledgeBaseGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
