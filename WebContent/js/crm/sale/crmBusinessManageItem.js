var crmBusinessManageItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'progress',
		type:"string"
	});
	   fields.push({
		name:'type',
		type:"string"
	});
	   fields.push({
		name:'days',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'saleType-id',
		type:"string"
	});
	    fields.push({
		name:'saleType-name',
		type:"string"
	});
	    fields.push({
		name:'productType-id',
		type:"string"
	});
	    fields.push({
		name:'productType-name',
		type:"string"
	});
	    fields.push({
		name:'technologyType-id',
		type:"string"
	});
	    fields.push({
		name:'technologyType-name',
		type:"string"
	});
	   fields.push({
		name:'chengdanProbability',
		type:"string"
	});
	    fields.push({
		name:'followupWork',
		type:"string"
	});
	    fields.push({
	    	name:'crmBusinessManage-id',
	    	type:"string"
	    });
	    fields.push({
	    	name:'crmBusinessManage-name',
	    	type:"string"
	    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'序号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'跟踪日期',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'progress',
		hidden : false,
		header:'跟踪方式',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'type',
		hidden : false,
		header:'跟踪内容',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'days',
		hidden : false,
		header:'项目来源',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'saleType-id',
		hidden : true,
		header:'业务类型ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'saleType-name',
		hidden : false,
		header:'业务类型',
		width:15*10
	});
	cm.push({
		dataIndex:'productType-id',
		hidden : true,
		header:'产品类型ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productType-name',
		hidden : false,
		header:'产品类型',
		width:15*10
	});
	cm.push({
		dataIndex:'technologyType-id',
		hidden : true,
		header:'技术类型ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'technologyType-name',
		hidden : false,
		header:'技术类型',
		width:15*10
	});
	cm.push({
		dataIndex:'chengdanProbability',
		hidden : false,
		header:'成单可能性',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'followupWork',
		hidden : false,
		header:'后续工作',
		width:15*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:50*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmBusinessManage-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmBusinessManage-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/sale/crmBusinessManage/showCrmBusinessManageItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="商机管理明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/sale/crmBusinessManage/delCrmBusinessManageItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '选择业务类型',
		handler : selectYWType
	});
	opts.tbar.push({
		text : '选择产品类型',
		handler : selectCPType
	});
	opts.tbar.push({
		text : '选择技术类型',
		handler : selectJSType
	});
	opts.tbar.push({
		text : '选择后续工作',
		handler : selectFlowUpWork
	});
	

	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = crmBusinessManageItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
                			crmBusinessManageItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	crmBusinessManageItemGrid=gridEditTable("crmBusinessManageItemdiv",cols,loadParam,opts);
	$("#crmBusinessManageItemdiv").data("crmBusinessManageItemGrid", crmBusinessManageItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
//选择技术类型
function selectJSType(){
	var win = Ext.getCmp('selectdeal');
	if (win) {win.close();}
	var selectdeal= new Ext.Window({
	id:'selectdeal',modal:true,title:'选择技术类型',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=technologicalType' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectdeal.close(); }  }]  });     selectdeal.show(); }
	function settechnologicalType(id,name){
		var gridGrid =crmBusinessManageItemGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('technologyType-id',id);
			obj.set('technologyType-name',name);
		});
		var win = Ext.getCmp('selectdeal')
		if(win){
			win.close();
		}
	}
//选择产品类型showTypeList
function selectCPType(){
	var win = Ext.getCmp('selectdeal');
	if (win) {win.close();}
	var selectdeal= new Ext.Window({
	id:'selectdeal',modal:true,title:'选择产品类型',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=showTypeList' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectdeal.close(); }  }]  });     selectdeal.show(); }
	function setshowTypeList(id,name){
		var gridGrid =crmBusinessManageItemGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('productType-id',id);
			obj.set('productType-name',name);
		});
		var win = Ext.getCmp('selectdeal')
		if(win){
			win.close();
		}
	}
//选择业务类型crmSaleChance
function selectYWType(){
	var win = Ext.getCmp('selectdeal');
	if (win) {win.close();}
	var selectdeal= new Ext.Window({
	id:'selectdeal',modal:true,title:'选择业务类型',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=crmSaleChance' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectdeal.close(); }  }]  });     selectdeal.show(); }
	function setcrmSaleChance(id,name){
		var gridGrid =crmBusinessManageItemGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('saleType-id',id);
			obj.set('saleType-name',name);
		});
		var win = Ext.getCmp('selectdeal')
		if(win){
			win.close();
		}
	}
//选择后续工作
function selectFlowUpWork(){
	var title = '';
	var url = '';
	title = "选择后续实验模块";
	url = "/exp/project/project/showDialogApplicationTypeTableList.action";
	var option = {};
	option.width = 500;
	option.height = 500;
	loadDialogPage(null, title, url, {
		"确定" : function() {
			selVal(this);
			}
		}, true, option);
}
var selVal = function(win) {
var operGrid = $("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid");
var selectRecord = operGrid.getSelectionModel().getSelections();
var str = "";
var strName = "";
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			str = str + obj.get("id")+",";
			strName = strName + obj.get("name")+",";
		});
		alert(str);
		alert(strName);
		var gridGrid =crmBusinessManageItemGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('followupWork',strName);
		});
		$(win).dialog("close");
	} else {
		message("请选择您要选择的数据");
		return;
	}
};
/*function selectnextStepNameTable() {
	var title = '';
	var url = '';
	title = "选择后续实验模块";
	url = "/exp/project/project/showDialogApplicationTypeTableList.action";
	var option = {};
	option.width = 500;
	option.height = 500;
	loadDialogPage(null, title, url, {
		"确定" : function() {
			selVal(this);
			}
		}, true, option);
}
var selVal = function(win) {
var operGrid = $("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid");
var selectRecord = operGrid.getSelectionModel().getSelections();
var str = "";
var strName = "";
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			str = str + obj.get("id")+",";
			strName = strName + obj.get("name")+",";
		});
		document.getElementById("injectBlackCut_nextStepName_id").value =str;
		document.getElementById("injectBlackCut_nextStepName").value =strName;
		$(win).dialog("close");
	} else {
		message("请选择您要选择的数据");
		return;
	}
};*/