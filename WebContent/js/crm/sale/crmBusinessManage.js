var crmBusinessManageGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'postType-id',
		type:"string"
	});
	    fields.push({
		name:'postType-name',
		type:"string"
	});
	    fields.push({
		name:'linkmanNname-id',
		type:"string"
	});
	    fields.push({
		name:'linkmanNname-name',
		type:"string"
	});
	    fields.push({
		name:'linkmanMobile',
		type:"string"
	});
	    fields.push({
		name:'linkmanPhone',
		type:"string"
	});
	    fields.push({
		name:'businessManage-id',
		type:"string"
	});
	    fields.push({
		name:'businessManage-name',
		type:"string"
	});
	    fields.push({
		name:'email',
		type:"string"
	});
	    fields.push({
		name:'unitName',
		type:"string"
	});
	    fields.push({
		name:'unitAddress',
		type:"string"
	});
	    fields.push({
		name:'belongConsignor-id',
		type:"string"
	});
	    fields.push({
		name:'belongConsignor-name',
		type:"string"
	});
	    fields.push({
		name:'resDirection',
		type:"string"
	});
	    fields.push({
		name:'dept',
		type:"string"
	});
	    fields.push({
		name:'industryType-id',
		type:"string"
	});
	    fields.push({
		name:'industryType-name',
		type:"string"
	});
	    fields.push({
		name:'country',
		type:"string"
	});
	    fields.push({
		name:'privince',
		type:"string"
	});
	    fields.push({
		name:'city',
		type:"string"
	});
	    fields.push({
		name:'content1',
		type:"string"
	});
	    fields.push({
		name:'content2',
		type:"string"
	});
	    fields.push({
		name:'content3',
		type:"string"
	});
	    fields.push({
		name:'content4',
		type:"string"
	});
	    fields.push({
		name:'content5',
		type:"string"
	});
	    fields.push({
		name:'content6',
		type:"string"
	});
	    fields.push({
		name:'content7',
		type:"string"
	});
	    fields.push({
		name:'content8',
		type:"string"
	});
	    fields.push({
		name:'content9',
		type:"string"
	});
	    fields.push({
		name:'content10',
		type:"string"
	});
	    fields.push({
		name:'content11',
		type:"string"
	});
	    fields.push({
		name:'content12',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:15*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'商机来源',
		width:20*6,
		sortable:true
	});
		cm.push({
		dataIndex:'type-id',
		hidden:true,
		header:'客户经理ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'type-name',
		header:'客户经理',
		width:10*6,
		sortable:true
		});
		cm.push({
		dataIndex:'state-id',
		hidden:true,
		header:'状态ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'state-name',
		header:'状态',
		width:6*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:6*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:15*6,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:30*6,
		sortable:true
	});
		cm.push({
		dataIndex:'postType-id',
		hidden:true,
		header:'岗位类型ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'postType-name',
		header:'岗位类型',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'linkmanNname-id',
		hidden:true,
		header:'联系人姓名ID',
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'linkmanNname-name',
		header:'联系人',
		width:15*6,
		sortable:true
		});
	cm.push({
		dataIndex:'linkmanMobile',
		header:'联系人手机',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'linkmanPhone',
		header:'固定电话',
		width:20*6,
		sortable:true
	});
		cm.push({
		dataIndex:'businessManage-id',
		hidden:true,
		header:'相关主表ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'businessManage-name',
		header:'相关主表',
		hidden:true,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'email',
		header:'email',
		width:15*8,
		sortable:true
	});
	cm.push({
		dataIndex:'unitName',
		header:'单位名称',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'unitAddress',
		header:'单位地址',
		width:30*6,
		sortable:true
	});
		cm.push({
		dataIndex:'belongConsignor-id',
		hidden:true,
		header:'所属委托人ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'belongConsignor-name',
		header:'所属委托人',
		width:15*6,
		sortable:true
		});
	cm.push({
		dataIndex:'resDirection',
		header:'研究方向',
		width:15*6,
		sortable:true
	});
	cm.push({
		dataIndex:'dept',
		header:'部门/学科',
		width:15*6,
		sortable:true
	});
		cm.push({
		dataIndex:'industryType-id',
		hidden:true,
		header:'行业类型ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'industryType-name',
		header:'行业类型',
		width:15*6,
		sortable:true
		});
	cm.push({
		dataIndex:'country',
		header:'国家',
		width:15*6,
		sortable:true
	});
	cm.push({
		dataIndex:'privince',
		header:'省',
		width:10*6,
		sortable:true
	});
	cm.push({
		dataIndex:'city',
		header:'市',
		width:10*6,
		sortable:true
	});
	cm.push({
		dataIndex:'content1',
		header:'content1',
		width:50*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content2',
		header:'content2',
		width:50*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content3',
		header:'content3',
		width:50*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content4',
		header:'content4',
		width:50*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content5',
		header:'content5',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content6',
		header:'content6',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content7',
		header:'content7',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content8',
		header:'content8',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content9',
		header:'content9',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content10',
		header:'content10',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content11',
		header:'content11',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content12',
		header:'content12',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/sale/crmBusinessManage/showCrmBusinessManageListJson.action";
	var opts={};
	opts.title="商机管理";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	crmBusinessManageGrid=gridTable("show_crmBusinessManage_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/sale/crmBusinessManage/editCrmBusinessManage.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/crm/sale/crmBusinessManage/editCrmBusinessManage.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/crm/sale/crmBusinessManage/viewCrmBusinessManage.action?id=' + id;
}
function exportexcel() {
	crmBusinessManageGrid.title = '导出列表';
	var vExportContent = crmBusinessManageGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startcontent5").val() != undefined) && ($("#startcontent5").val() != '')) {
					var startcontent5str = ">=##@@##" + $("#startcontent5").val();
					$("#content51").val(startcontent5str);
				}
				if (($("#endcontent5").val() != undefined) && ($("#endcontent5").val() != '')) {
					var endcontent5str = "<=##@@##" + $("#endcontent5").val();

					$("#content52").val(endcontent5str);

				}
				
				if (($("#startcontent6").val() != undefined) && ($("#startcontent6").val() != '')) {
					var startcontent6str = ">=##@@##" + $("#startcontent6").val();
					$("#content61").val(startcontent6str);
				}
				if (($("#endcontent6").val() != undefined) && ($("#endcontent6").val() != '')) {
					var endcontent6str = "<=##@@##" + $("#endcontent6").val();

					$("#content62").val(endcontent6str);

				}
				
				if (($("#startcontent7").val() != undefined) && ($("#startcontent7").val() != '')) {
					var startcontent7str = ">=##@@##" + $("#startcontent7").val();
					$("#content71").val(startcontent7str);
				}
				if (($("#endcontent7").val() != undefined) && ($("#endcontent7").val() != '')) {
					var endcontent7str = "<=##@@##" + $("#endcontent7").val();

					$("#content72").val(endcontent7str);

				}
				
				if (($("#startcontent8").val() != undefined) && ($("#startcontent8").val() != '')) {
					var startcontent8str = ">=##@@##" + $("#startcontent8").val();
					$("#content81").val(startcontent8str);
				}
				if (($("#endcontent8").val() != undefined) && ($("#endcontent8").val() != '')) {
					var endcontent8str = "<=##@@##" + $("#endcontent8").val();

					$("#content82").val(endcontent8str);

				}
				
				
				commonSearchAction(crmBusinessManageGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
