﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state=$("#crmSaleOrder_state").val();
	if(state!=1){
		var item = menu.add({
			text : '生成详细设计'
		});
		item.on('click', editProjectDetail);
	
		var item = menu.add({
			text : '生成项目管理'
		});
		item.on('click', editProject);
	}
});
function add() {
	window.location = window.ctx
			+ "/crm/sale/crmSaleOrder/editCrmSaleOrder.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/crm/sale/crmSaleOrder/showCrmSaleOrderList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#crmSaleOrder", {
		userId : userId,
		userName : userName,
		formId : $("#crmSaleOrder_id").val(),
		title : $("#crmSaleOrder_name").val()
	}, function() {
		window.location.reload();
	});
});
$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#crmSaleOrder_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});
function save() {
	if (checkSubmit() == true) {
		var crmSaleOrderLinkmanDivData = crmSaleOrderLinkmanGrid;
		document.getElementById('crmSaleOrderLinkmanJson').value = commonGetModifyRecords(crmSaleOrderLinkmanDivData);
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/sale/crmSaleOrder/save.action";
		form1.submit();
	}
}
function editCopy() {
	window.location = window.ctx
			+ '/crm/sale/crmSaleOrder/copyCrmSaleOrder.action?id='
			+ $("#crmSaleOrder_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#crmSaleOrder_id").val())
				commonChangeState("formId=" + $("#crmSaleOrder_id").val()
						+ "&tableId=CrmSaleOrder");
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmSaleOrder_id").val());
	nsc.push("编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : '订单管理',
				contentEl : 'markup'
			} ]
		});
	});
	load("/crm/sale/crmSaleOrder/showCrmSaleOrderLinkmanList.action", {
		id : $("#crmSaleOrder_id").val(),
		wid : $("#crmSaleOrder_crmCustomer").val()
	}, "#crmSaleOrderLinkmanpage");
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

var item = menu.add({
	text : '复制'
});
item.on('click', editCopy);
//var item = menu.add({
//	text : '生成合同'
//});
//item.on('click', editContract);
// var item = menu.add({
// text: '生成子订单'
// });
// item.on('click', editChildrenOrder);
// 生成合同事件
function editContract() {
	// window.location = window.ctx +
	// '/crm/sale/crmSaleOrder/editContractList.action?id='+$("#crmSaleOrder_id").val();
	var id = $("#crmSaleOrder_id").val();
	if (id) {
		ajax("post", "/crm/sale/crmSaleOrder/editContractList.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("生成合同成功。");
				} else {
					message("生成合同失败，请重试！");
				}
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
	} else {
		message("请先添加订单管理信息并保存。");
	}
}
// 生成详细设计事件
function editProjectDetail() {
	// window.location = window.ctx +
	// '/crm/sale/crmSaleOrder/editProjectDetail.action?id='+$("#crmSaleOrder_id").val();
	var id = $("#crmSaleOrder_id").val();
	var projectType=$("#crmSaleOrder_workType").val();
	var jsType=$("#crmSaleOrder_technologyType").val();
	var cpType=$("#crmSaleOrder_type").val();
	var zs=$("#crmSaleOrder_genus").val();
	var jydb=$("#crmSaleOrder_geneTargetingType").val();
	var zcp=$("#crmSaleOrder_product").val();
	var zcpsl=$("#crmSaleOrder_productNo").val();
	if (id) {
		if(projectType != "" && jsType!="" && cpType!="" && zs!="" && jydb!="" && zcp != "" && zcpsl !=""){
		ajax("post", "/crm/sale/crmSaleOrder/editProjectDetail.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("生成详细设计成功。");
				} else {
					message("生成详细设计失败，请重试！");
				}
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		}else{ 
			message("项目类型,技术类型,基因打靶类型,种属,产品类型,终产品,终产品数量不能为空！"); 
			}
		
	} else {
		message("请先添加订单管理信息并保存。");
	}
}
//选择初步设计
function ProjectStartFun(){
	var win = Ext.getCmp('ProjectStartFun');
	if (win) {win.close();}
	var CrmSaleChanceFun= new Ext.Window({
	id:'ProjectStartFun',modal:true,title:'选择初步设计',layout:'fit',width:600,height:580,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ window.ctx+ "/exp/project/projectStart/projectStartSelect.action?flag=ProjectStartFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 CrmSaleChanceFun.close(); }  }]  });     CrmSaleChanceFun.show(); }
function setProjectStartFun(rec){
	document.getElementById('crmSaleOrder_projectStart').value=rec.get('id');
	document.getElementById('crmSaleOrder_projectStart_name').value=rec.get('name');
	document.getElementById('crmSaleOrder_name').value=rec.get('name');
	var win = Ext.getCmp('ProjectStartFun')
	if(win){win.close();}
	}

// 生成项目管理事件
function editProject() {
	//window.location = window.ctx + '/crm/sale/crmSaleOrder/editProject.action?id='+ $("#crmSaleOrder_id").val();
	var id = $("#crmSaleOrder_id").val();
	var id = $("#crmSaleOrder_id").val();
	var projectType=$("#crmSaleOrder_workType").val();
	var jsType=$("#crmSaleOrder_technologyType").val();
	var cpType=$("#crmSaleOrder_type").val();
	var zs=$("#crmSaleOrder_genus").val();
	var jydb=$("#crmSaleOrder_geneTargetingType").val();
	var zcp=$("#crmSaleOrder_product").val();
	var zcpsl=$("#crmSaleOrder_productNo").val();
	if (id) {
		if(projectType != "" && jsType!="" && cpType!="" && zs!="" && jydb!="" && zcp != "" && zcpsl !=""){
		ajax("post", "/crm/sale/crmSaleOrder/editProject.action", {
			id : id
		}, function(data) {
			if (data.success) {
				if (data.data) {
					message("生成项目管理成功。");
					window.location.reload();
				} else {
					message("生成项目管理失败，请重试！");
				}
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		}else{
			message("项目类型,技术类型,基因打靶类型,种属,产品类型,终产品,终产品数量不能为空！"); 
		}
	} else {
		message("请先添加订单管理信息并保存。");
	}
}

function CrmCustomerFun() {
	var win = Ext.getCmp('CrmCustomerFun');
	if (win) {
		win.close();
	}
	var CrmCustomerFun = new Ext.Window(
			{
				id : 'CrmCustomerFun',
				modal : true,
				title : '选择客户',
				layout : 'fit',
				width : 600,
				height : 580,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+ window.ctx+ "/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						CrmCustomerFun.close();
					}
				} ]
			});
	CrmCustomerFun.show();
}
function setCrmCustomerFun(rec){
	 var cId=$("#crmSaleOrder_crmCustomer").val();
	 if(cId==null || cId==""){
		 	document.getElementById('crmSaleOrder_crmCustomer').value = rec.get('id');
			document.getElementById('crmSaleOrder_crmCustomer_name').value = rec.get('name');
			var win = Ext.getCmp('CrmCustomerFun')
			if (win) {
				win.close();
			}
}
}
/*var id = "";
var name = "";
function setCrmCustomerFun(rec) {
	 var cId=$("#crmSaleOrder_crmCustomer").val();
	 if(cId==null || cId==""){
		 	document.getElementById('crmSaleOrder_crmCustomer').value = rec.get('id');
			document.getElementById('crmSaleOrder_crmCustomer_name').value = rec.get('name');
			var win = Ext.getCmp('CrmCustomerFun')
			if (win) {
				win.close();
			}
			
			
			
			id = rec.get('id');
			name = rec.get('name');
			ajax("post", "/crm/sale/crmSaleOrder/showLinkManList.action", {
				code : id,
			}, function(data) {
				if (data.success) {

					var ob = crmSaleOrderLinkmanGrid.getStore().recordType;
					crmSaleOrderLinkmanGrid.stopEditing();

					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;

						p.set("id", obj.id);
						p.set("name", obj.name);
						p.set("email", obj.email);
						p.set("mobile", obj.mobile);
						p.set("telephone", obj.telephone);
						p.set("trans", obj.trans);

						crmSaleOrderLinkmanGrid.getStore().add(p);
					});

					crmSaleOrderLinkmanGrid.startEditing(0, 0);
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null);
	 }else{
			if(rec.get('id')==cId){
 				var win = Ext.getCmp('CrmCustomerFun')
 				if(win){win.close();}
 			 }else{
 				crmSaleOrderLinkmanGrid.store.removeAll();
 				document.getElementById('crmSaleOrder_crmCustomer').value = rec.get('id');
 				document.getElementById('crmSaleOrder_crmCustomer_name').value = rec.get('name');
 				var win = Ext.getCmp('CrmCustomerFun')
 				if (win) {
 					win.close();
 				}
 				id = rec.get('id');
 				name = rec.get('name');
 				ajax("post", "/crm/sale/crmSaleOrder/showLinkManList.action", {
 					code : id,
 				}, function(data) {
 					if (data.success) {

 						var ob = crmSaleOrderLinkmanGrid.getStore().recordType;
 						crmSaleOrderLinkmanGrid.stopEditing();

 						$.each(data.data, function(i, obj) {
 							var p = new ob({});
 							p.isNew = true;

 							p.set("id", obj.id);
 							p.set("name", obj.name);
 							p.set("email", obj.email);
 							p.set("mobile", obj.mobile);
 							p.set("telephone", obj.telephone);
 							p.set("trans", obj.trans);

 							crmSaleOrderLinkmanGrid.getStore().add(p);
 						});

 						crmSaleOrderLinkmanGrid.startEditing(0, 0);
 					} else {
 						message("获取明细数据时发生错误！");
 					}
 				}, null);
 			 }
	 }
	
}*/


function CrmSaleChanceFun(){
	var win = Ext.getCmp('CrmSaleChanceFun');
	if (win) {win.close();}
	var CrmSaleChanceFun= new Ext.Window({
	id:'CrmSaleChanceFun',modal:true,title:'选择商机',layout:'fit',width:600,height:580,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ window.ctx+ "/crm/sale/crmSaleChance/crmSaleChanceSelect.action?flag=CrmSaleChanceFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 CrmSaleChanceFun.close(); }  }]  });     CrmSaleChanceFun.show(); }
function setCrmSaleChanceFun(rec){
	//alert(rec.get('evalustionUser-name'));
	//alert(rec.get('evalustionUser-id'));
	document.getElementById('crmSaleOrder_crmSaleChance').value=rec.get('id');
	document.getElementById('crmSaleOrder_crmSaleChance_name').value=rec.get('name');
	document.getElementById('crmSaleOrder_boss').value=rec.get('evalustionUser-id');
	document.getElementById('crmSaleOrder_boss_name').value=rec.get('evalustionUser-name');
	var win = Ext.getCmp('CrmSaleChanceFun')
	if(win){win.close();}
	}
	 
 function CrmContractFun(){
	 var win = Ext.getCmp('CrmContractFun');
	 if (win) {win.close();}
	 var CrmContractFun= new Ext.Window({
	 id:'CrmContractFun',modal:true,title:'选择销售合同',layout:'fit',width:600,height:580,closeAction:'close',
	 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	 collapsible: true,maximizable: true,
	 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	 html:"<iframe scrolling='no' name='maincontentframe' src='"+ window.ctx+ "/crm/contract/crmContract/crmContractSelect.action?flag=CrmContractFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	 buttons: [
	 { text: '关闭',
	  handler: function(){
	  CrmContractFun.close(); }  }]  });     CrmContractFun.show(); }

function setCrmContractFun(rec){
	
	 //alert(rec.get('product-name'));
	 //alert(rec.get('product-id'));
	 document.getElementById('crmSaleOrder_crmContract').value=rec.get('id');
	 document.getElementById('crmSaleOrder_crmContract_name').value=rec.get('name');
	 document.getElementById('crmSaleOrder_product_name').value=rec.get('product-name');
	 document.getElementById('crmSaleOrder_product').value=rec.get('product-id');
	 
	 var win = Ext.getCmp('CrmContractFun')
	 if(win){win.close();}
	 }
//选择见栓人
	function selectManager(){
		var win = Ext.getCmp('selecttransUser');
		if (win) {win.close();}
		var selecttransUser= new Ext.Window({
		id:'selecttransUser',modal:true,title:'选择客户经理',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/showUserListAllSelect.action?funType=managerUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
			 selecttransUser.close(); }  }]  });     selecttransUser.show(); }
	function addUserGrid(record,funType){
		var n="";
		var nId="";
		for ( var ij = 0; ij < record.length; ij++) {
		    n=n+record[ij].get('name') +",";
		    nId=nId+record[ij].get('id') +",";
		} 
		if(funType=='managerUser'){
			document.getElementById('crmSaleOrder_manager').value = n;
		}
		
		var win = Ext.getCmp('selecttransUser')
		if(win){
			win.close();
		}
	}
	 //查看初步设计
	 function lookProjectStart(){
		 var crmID = $("#crmSaleOrder_projectStart").val();
		 if(crmID){
			 var win = Ext.getCmp('project');
			 if (win) {win.close();}
			 var project= new Ext.Window({
			 id:'project',modal:true,title:'查看初步设计',layout:'fit',width:1300,height:600,closeAction:'close',
			 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 collapsible: true,maximizable: true,
			 items: new Ext.BoxComponent({id:'maincontent', region: 'center',
			 html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/exp/project/projectStart/editProjectStart.action?id="+crmID+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			 buttons: [
			 { text: '关闭',
			  handler: function(){
			 	project.close(); }  }]  });     project.show(); 
		 }else{
			 message("请先选择初步设计！");
		 }
	}
	 //查看合同
	 function lookCrmSaleOrder(){
		 var crmID = $("#crmSaleOrder_crmContract").val();
		 if(crmID){
		 var win = Ext.getCmp('project');
		 if (win) {win.close();}
		 var project= new Ext.Window({
		 id:'project',modal:true,title:'查看合同',layout:'fit',width:1300,height:600,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		 html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/crm/contract/crmContract/viewCrmContract.action?id="+crmID+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: '关闭',
		  handler: function(){
		 	project.close(); }  }]  });     project.show(); 
		 }else{
			 message("请先选择合同！");
		 }
	}
	 var item = menu.add({
	    	text: '取消'
			});
		item.on('click', cancel);
		function cancel() {
			ajax("post", "/sample/task/cancel.action", {
				modelType : "CrmSaleOrder",
				id : $("#crmSaleOrder_id").val()
			}, function(data) {
				location.reload();
			}, null);
		}