var crmSaleOrderGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'geneId',
		type : "string"
	});
	fields.push({
		name : 'orderStateName',
		type : "string"
	});
	fields.push({
		name : 'geneName',
		type : "string"
	});
	fields.push({
		name : 'crmCustomer-crmCustomer-department-name',
		type : "string"
	});
	fields.push({
		name : 'productNo',
		type : "string"
	});
	fields.push({
		name : 'checkup-id',
		type : "string"
	});
	fields.push({
		name : 'checkup-name',
		type : "string"
	});
	fields.push({
		name : 'orderSource-id',
		type : "string"
	});
	fields.push({
		name : 'orderSource-name',
		type : "string"
	});
	fields.push({
		name : 'neoResult',
		type : "string"
	});
	fields.push({
		name : 'orderSource-id',
		type : "string"
	});
	fields.push({
		name : 'orderSource-name',
		type : "string"
	});
	// fields.push({
	// name:'crmMarcketActivity-id',
	// type:"string"
	// });
	// fields.push({
	// name:'crmMarcketActivity-name',
	// type:"string"
	// });
	fields.push({
		name : 'crmCustomer-id',
		type : "string"
	});
	fields.push({
		name : 'crmCustomer-name',
		type : "string"
	});
	fields.push({
		name : 'workType-id',
		type : "string"
	});
	fields.push({
		name : 'workType-name',
		type : "string"
	});
	fields.push({
		name : 'product-id',
		type : "string"
	});
	fields.push({
		name : 'product-name',
		type : "string"
	});
	fields.push({
		name : 'genus-id',
		type : "string"
	});
	fields.push({
		name : 'genus-name',
		type : "string"
	});
	fields.push({
		name : 'strain-id',
		type : "string"
	});
	fields.push({
		name : 'strain-name',
		type : "string"
	});
	fields.push({
		name : 'technologyType-id',
		type : "string"
	});
	fields.push({
		name : 'technologyType-name',
		type : "string"
	});
	fields.push({
		name : 'projectStart-id',
		type : "string"
	});
	fields.push({
		name : 'projectStart-name',
		type : "string"
	});
	fields.push({
		name : 'customerRequirements',
		type : "string"
	});
	// fields.push({
	// name:'boss-id',
	// type:"string"
	// });
	// fields.push({
	// name:'boss-name',
	// type:"string"
	// });
	// fields.push({
	// name:'money',
	// type:"string"
	// });
	fields.push({
		name : 'type-id',
		type : "string"
	});
	fields.push({
		name : 'type-name',
		type : "string"
	});
	fields.push({
		name : 'geneTargetingType-id',
		type : "string"
	});
	fields.push({
		name : 'geneTargetingType-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : 'string'
	});
	// fields.push({
	// name:'crmSaleChance-id',
	// type:"string"
	// });
	// fields.push({
	// name:'crmSaleChance-name',
	// type:"string"
	// });
	fields.push({
		name : 'manager-id',
		type : "string"
	});
	fields.push({
		name : 'manager-name',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'parent-id',
		type : "string"
	});
	fields.push({
		name : 'parent-name',
		type : "string"
	});
	fields.push({
		name : 'crmContract-identifiers',
		type : "string"
	});
	fields.push({
		name : 'crmContract-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-name',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-id',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-name',
		type : "string"
	});
	fields.push({
		name : 'confirmDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'projectAsk',
		type : "string"
	});
	fields.push({
		name : 'client',
		type : "string"
	});
	// 基因ID
	fields.push({
		name : 'geneId',
		type : "string"
	});
	// 基因别名
	fields.push({
		name : 'geneByName',
		type : "string"
	});
	// 终产品
	fields.push({
		name : 'product-id',
		type : "string"
	});
	fields.push({
		name : 'product-name',
		type : "string"
	});
	fields.push({
		name : 'productNo',
		type : "string"
	});
	// 初步设计
	fields.push({
		name : 'projectStart-id',
		type : "string"
	});
	fields.push({
		name : 'projectStart-name',
		type : "string"
	});
	// 合同实际开始时间
	fields.push({
		name : 'startDate',
		type : "string"
	});
	// 合同实际结束时间
	fields.push({
		name : 'endDate',
		type : "string"
	});

	fields.push({
		name : 'content2',
		type : "string"
	});
	fields.push({
		name : 'content3',
		type : "string"
	});
	fields.push({
		name : 'content4',
		type : "string"
	});
	fields.push({
		name : 'content5',
		type : "string"
	});
	fields.push({
		name : 'content6',
		type : "string"
	});
	fields.push({
		name : 'content7',
		type : "string"
	});
	fields.push({
		name : 'content8',
		type : "string"
	});
	fields.push({
		name : 'content9',
		type : "string"
	});
	fields.push({
		name : 'content10',
		type : "string"
	});
	fields.push({
		name : 'content11',
		type : "string"
	});
	fields.push({
		name : 'genProjectDate',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'orderStateName',
		hidden : false,
		header : '后续工作',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'id',
		header : '编码',
		width : 10 * 9,
		sortable : true
	});

	cm.push({
		dataIndex : 'name',
		header : '订单名称',
		width : 30 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'content2',
		header : '项目名称',
		width : 30 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'workType-id',
		hidden : true,
		header : '项目类型ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'workType-name',
		header : '项目类型',
		width : 15 * 10,
		sortable : true
	});
		/*cm.push({
		dataIndex : 'crmContract-id',
		hidden : false,
		header : '合同编号',
		width : 15 * 10,
		sortable : true
	});*/
	cm.push({
		dataIndex : 'crmContract-identifiers',
		hidden : false,
		header : '合同编号',
		width : 15 * 10,
		sortable : true
	});
		cm.push({
		dataIndex : 'crmContract-name',
		header : '合同名称',
		width : 40 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'manager-id',
		hidden : true,
		header : '客户经理ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'manager-name',
		header : '客户经理',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneName',
		header : '基因正式名称',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderSource-id',
		hidden : true,
		header : '订单来源ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderSource-name',
		header : '订单来源',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'genProjectDate',
		header : '下单日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneId',
		header : '基因ID',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneByName',
		header : '基因别名',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'technologyType-id',
		hidden : true,
		header : '技术类型ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'technologyType-name',
		hidden : false,
		header : '技术类型',
		width : 10 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneTargetingType-id',
		hidden : true,
		header : '基因打靶类型ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneTargetingType-name',
		hidden : false,
		header : '基因打靶类型',
		width : 8 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-id',
		hidden : true,
		header : '产品类型ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-name',
		hidden : false,
		header : '产品类型',
		width : 8 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'genus-id',
		hidden : true,
		header : '种属ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'genus-name',
		hidden : false,
		header : '种属',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'strain-id',
		hidden : true,
		header : '品系ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'strain-name',
		hidden : false,
		header : '品系',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'product-id',
		hidden : true,
		header : '终产品ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'product-name',
		hidden : false,
		header : '终产品',
		width : 25 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'productNo',
		hidden : false,
		header : '终产品数量',
		width : 20 * 5,
		sortable : true
	});
	cm.push({
		dataIndex : 'checkup-id',
		header : '质控要求ID',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'checkup-name',
		hidden : false,
		header : '质控要求',
		width : 20 * 6,
		sortable : true
	});
	var isRecordstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});

	var isRecordComboxFun = new Ext.form.ComboBox({
		store : isRecordstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'neoResult',
		hidden : false,
		header : '是否去Neo',
		width : 20 * 5,
		renderer : Ext.util.Format.comboRenderer(isRecordComboxFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'customerRequirements',
		hidden : false,
		header : '客户要求',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'startDate',
		header : '实际合同开始时间',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : '实际合同结束时间',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmCustomer-id',
		hidden : true,
		header : '委托人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmCustomer-name',
		hidden : false,
		header : '委托人',
		width : 10 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'client',
		hidden : false,
		header : '委托单位',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectStart-id',
		hidden : true,
		header : '初步设计ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectStart-name',
		header : '初步设计',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : '创建人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : '创建人',
		width : 10 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : '创建日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-id',
		hidden : true,
		header : '审核人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-name',
		header : '审核人',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmDate',
		header : '审核日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : '工作流状态',
		width : 40 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '工作流状态',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : '备注',
		width : 10 * 10,
		sortable : true
	});
	/*
	 * var isCheckUpstore = new Ext.data.ArrayStore({ fields : [ 'id', 'name' ],
	 * data : [ [ '1', '需要' ], [ '0', '不需要' ] ] });
	 * 
	 * var isCheckUpstoreFun = new Ext.form.ComboBox({ store : isCheckUpstore,
	 * displayField : 'name', valueField : 'id', typeAhead : true, mode :
	 * 'local', forceSelection : true, triggerAction : 'all', emptyText : '',
	 * selectOnFocus : true });
	 */
	// 父级订单
	cm.push({
		dataIndex : 'parent-id',
		header : '父级订单ID',
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'parent-name',
		header : '父级订单',
		width : 10 * 10,
		sortable : false
	});
	// cm.push({
	// dataIndex:'crmMarcketActivity-id',
	// hidden:true,
	// header:'市场推广活动',
	// width:15*10,
	// sortable:true
	// });
	// cm.push({
	// dataIndex:'crmMarcketActivity-name',
	// header:'市场推广活动',
	// width:15*10,
	// sortable:true
	// });
	// cm.push({
	// dataIndex:'money',
	// header:'金额',
	// width:10*6,
	// sortable:true
	// });

	/*
	 * cm.push({ dataIndex:'crmSaleChance-id', hidden:true, header:'商机ID',
	 * width:15*10, sortable:true }); cm.push({ dataIndex:'crmSaleChance-name',
	 * header:'商机', width:15*10, sortable:true });
	 */
	// cm.push({
	// dataIndex:'boss-id',
	// hidden:true,
	// header:'项目负责人ID',
	// width:15*10,
	// sortable:true
	// });
	// cm.push({
	// dataIndex:'boss-name',
	// header:'项目负责人',
	// width:15*5,
	// sortable:true
	// });
	
	/*
	 * cm.push({ dataIndex:'projectAsk', hidden:true, header:'项目要求', width:50*6,
	 * 
	 * sortable:true });
	 */
	cm.push({
		dataIndex : 'content1',
		header : 'content1',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content2',
		header : 'content2',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content3',
		header : 'content3',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content4',
		header : 'content4',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content5',
		header : 'content5',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content6',
		header : 'content6',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content7',
		header : 'content7',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content8',
		header : 'content8',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content9',
		header : 'content9',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content10',
		header : 'content10',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content11',
		header : 'content11',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/sale/crmSaleOrder/showCrmSaleOrderListJson.action";
	var opts = {};
	opts.title = "订单管理";
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	crmSaleOrderGrid = gridTable("show_crmSaleOrder_div", cols, loadParam, opts);
})
function add() {
	window.location = window.ctx
			+ '/crm/sale/crmSaleOrder/editCrmSaleOrder.action';
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/crm/sale/crmSaleOrder/editCrmSaleOrder.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/crm/sale/crmSaleOrder/viewCrmSaleOrder.action?id=' + id;
}
function exportexcel() {
	crmSaleOrderGrid.title = '导出列表';
	var vExportContent = crmSaleOrderGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function CrmMarcketActivityFun() {
	var win = Ext.getCmp('CrmMarcketActivityFun');
	if (win) {
		win.close();
	}
	var CrmMarcketActivityFun = new Ext.Window(
			{
				id : 'CrmMarcketActivityFun',
				modal : true,
				title : '选择市场推广活动',
				layout : 'fit',
				width : 600,
				height : 580,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='/crm/marcket/crmMarcketActivity/crmMarcketActivitySelect.action?flag=CrmMarcketActivityFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						CrmMarcketActivityFun.close();
					}
				} ]
			});
	CrmMarcketActivityFun.show();
}

function setCrmMarcketActivityFun(rec) {
	document.getElementById('crmSaleOrder_crmMarcketActivity').value = rec
			.get('id');
	document.getElementById('crmSaleOrder_crmMarcketActivity_name').value = rec
			.get('name');
	var win = Ext.getCmp('CrmMarcketActivityFun')
	if (win) {
		win.close();
	}
}
$(function() {
	$("#opensearch").click(
			function() {
				var option = {};
				option.width = 742;
				option.height = 417;
				loadDialogPage($("#jstj"), "搜索", null, {
					"开始检索" : function() {
						//创建日期搜索
						if (($("#startcreateDate").val() != undefined)
								&& ($("#startcreateDate").val() != '')) {
							var startcreateDatestr = ">=##@@##"
									+ $("#startcreateDate").val();
							$("#createDate1").val(startcreateDatestr);
						}
						if (($("#endcreateDate").val() != undefined)
								&& ($("#endcreateDate").val() != '')) {
							var endcreateDatestr = "<=##@@##"
									+ $("#endcreateDate").val();
							$("#createDate2").val(endcreateDatestr);
						}
						//审核日期搜索
						if (($("#startconfirmDate").val() != undefined)
								&& ($("#startconfirmDate").val() != '')) {
							var startconfirmDatestr = ">=##@@##"
									+ $("#startconfirmDate").val();
							$("#confirmDate1").val(startconfirmDatestr);
						}
						if (($("#endconfirmDate").val() != undefined)
								&& ($("#endconfirmDate").val() != '')) {
							var endconfirmDatestr = "<=##@@##"
									+ $("#endconfirmDate").val();

							$("#confirmDate2").val(endconfirmDatestr);

						}
						// 下单日期搜索
						if (($("#startgenProjectDate").val() != undefined)
								&& ($("#startgenProjectDate").val() != '')) {
							var startgenProjectDatestr = ">=##@@##"
									+ $("#startgenProjectDate").val();
							$("#genProjectDate1").val(startgenProjectDatestr);
						}

						if (($("#endgenProjectDate").val() != undefined)
								&& ($("#endgenProjectDate").val() != '')) {
							var endgenProjectDatestr = "<=##@@##"
									+ $("#endgenProjectDate").val();

							$("#genProjectDate2").val(endgenProjectDatestr);

						}
						//实际合同开始日期搜索
						if (($("#startstartDate").val() != undefined)
								&& ($("#startstartDate").val() != '')) {
							var startstartDatestr = ">=##@@##"
									+ $("#startstartDate").val();
							$("#startDate1").val(startstartDatestr);
						}
						
						if (($("#endstartDate").val() != undefined)
								&& ($("#endstartDate").val() != '')) {
							var endstartDatestr = "<=##@@##"
									+ $("#endstartDate").val();

							$("#startDate2").val(endstartDatestr);
						}
						//实际合同结束日期搜索
						if (($("#startendDate").val() != undefined)
								&& ($("#startendDate").val() != '')) {
							var startendDatestr = ">=##@@##"
									+ $("#startendDate").val();
							$("#endDate1").val(startendDatestr);
						}
						
						if (($("#endendDate").val() != undefined)
								&& ($("#endendDate").val() != '')) {
							var endendDatestr = "<=##@@##"
									+ $("#endendDate").val();

							$("#endDate2").val(endendDatestr);
						}
						if (($("#startcontent9").val() != undefined)
								&& ($("#startcontent9").val() != '')) {
							var startcontent9str = ">=##@@##"
									+ $("#startcontent9").val();
							$("#content91").val(startcontent9str);
						}
						if (($("#endcontent9").val() != undefined)
								&& ($("#endcontent9").val() != '')) {
							var endcontent9str = "<=##@@##"
									+ $("#endcontent9").val();

							$("#content92").val(endcontent9str);

						}

						if (($("#startcontent10").val() != undefined)
								&& ($("#startcontent10").val() != '')) {
							var startcontent10str = ">=##@@##"
									+ $("#startcontent10").val();
							$("#content101").val(startcontent10str);
						}
						if (($("#endcontent10").val() != undefined)
								&& ($("#endcontent10").val() != '')) {
							var endcontent10str = "<=##@@##"
									+ $("#endcontent10").val();

							$("#content102").val(endcontent10str);

						}

						if (($("#startcontent11").val() != undefined)
								&& ($("#startcontent11").val() != '')) {
							var startcontent11str = ">=##@@##"
									+ $("#startcontent11").val();
							$("#content111").val(startcontent11str);
						}
						if (($("#endcontent11").val() != undefined)
								&& ($("#endcontent11").val() != '')) {
							var endcontent11str = "<=##@@##"
									+ $("#endcontent11").val();

							$("#content112").val(endcontent11str);

						}

						if (($("#startcontent12").val() != undefined)
								&& ($("#startcontent12").val() != '')) {
							var startcontent12str = ">=##@@##"
									+ $("#startcontent12").val();
							$("#content121").val(startcontent12str);
						}
						if (($("#endcontent12").val() != undefined)
								&& ($("#endcontent12").val() != '')) {
							var endcontent12str = "<=##@@##"
									+ $("#endcontent12").val();

							$("#content122").val(endcontent12str);

						}

						commonSearchAction(crmSaleOrderGrid);
						$(this).dialog("close");

					},
					"清空" : function() {
						form_reset();

					}
				}, true, option);
			});
});
Ext.onReady(function() {
	var item = menu.add({
		text : '列表模式'
	});
	item.on('click', lbms);

});
function lbms() {
	var url = ctx + "/crm/sale/crmSaleOrder/showCrmSaleOrderList.action";
	location.href = url;
}
Ext.onReady(function() {
	var item1 = menu.add({
		text : '树状模式'
	});
	item1.on('click', szms);
});
function szms() {
	var url = ctx + "/crm/sale/crmSaleOrder/showCrmSaleOrderTree.action";
	location.href = url;
}
