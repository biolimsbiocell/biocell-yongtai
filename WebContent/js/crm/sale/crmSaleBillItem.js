﻿
var crmSaleBillItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'billHead',
		type:"string"
	});
	   fields.push({
		name:'billDet',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'money',
		type:"string"
	});
	   fields.push({
		name:'billCode',
		type:"string"
	});
	   fields.push({
		name:'billDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'crmSaleBill-id',
		type:"string"
	});
	    fields.push({
		name:'crmSaleBill-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'billHead',
		hidden : false,
		header:'发票抬头',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'billDet',
		hidden : false,
		header:'明细',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'期数',
		width:10*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'money',
		hidden : false,
		header:'金额',
		width:10*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'billCode',
		hidden : false,
		header:'发票号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'billDate',
		hidden : false,
		header:'开票日期',
		width:15*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmSaleBill-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmSaleBill-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/sale/crmSaleBill/showCrmSaleBillItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="发票开票信息";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/sale/crmSaleBill/delCrmSaleBillItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	 if($("#crmSaleBill_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
		 } else{
			/*opts.tbar.push({
					text : '选择相关主表',
					handler : selectcrmSaleBillFun
				});*/
			
			
			
			
			opts.tbar.push({
				text : '显示可编辑列',
				handler : null
			});
			opts.tbar.push({
				text : '取消选中',
				handler : null
			});
			
			
			
			opts.tbar.push({
				text : "批量上传（CSV文件）",
				handler : function() {
					var options = {};
					options.width = 350;
					options.height = 200;
					loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
						"确定":function(){
							goInExcelcsv();
							$(this).dialog("close");
						}
					},true,options);
				}
			});
		 }

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = crmSaleBillItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							crmSaleBillItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	crmSaleBillItemGrid=gridEditTable("crmSaleBillItemdiv",cols,loadParam,opts);
	$("#crmSaleBillItemdiv").data("crmSaleBillItemGrid", crmSaleBillItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmSaleBillFun(){
	var win = Ext.getCmp('selectcrmSaleBill');
	if (win) {win.close();}
	var selectcrmSaleBill= new Ext.Window({
	id:'selectcrmSaleBill',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmSaleBillSelect.action?flag=crmSaleBill' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectcrmSaleBill.close(); }  }]  });     selectcrmSaleBill.show(); }
	function setcrmSaleBill(id,name){
		var gridGrid = $("#crmSaleBillItemdiv").data("crmSaleBillItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmSaleBill-id',id);
			obj.set('crmSaleBill-name',name);
		});
		var win = Ext.getCmp('selectcrmSaleBill')
		if(win){
			win.close();
		}
	}
	
