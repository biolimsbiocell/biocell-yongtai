$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/crm/sale/crmBusinessManage/editCrmBusinessManage.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/sale/crmBusinessManage/showCrmBusinessManageList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#crmBusinessManage", {
					userId : userId,
					userName : userName,
					formId : $("#crmBusinessManage_id").val(),
					title : $("#crmBusinessManage_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#crmBusinessManage_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});

//选择项目联系人
function selectProjectLinkMan(){
	var win = Ext.getCmp('selectProjectLinkManFun');
	if (win) {win.close();}
	var selectProjectLinkManFun= new Ext.Window({
	id:'selectProjectLinkManFun',modal:true,title:'选择项目联系人',layout:'fit',width:600,height:580,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ window.ctx+ "/crm/customer/linkman/crmLinkManSelect.action?flag=selectProjectLinkManFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectProjectLinkManFun.close(); }  }]  });     selectProjectLinkManFun.show(); }
function setCrmLinkManFun(rec){
	document.getElementById('crmBusinessManage_linkmanNname').value=rec.get('id');
	document.getElementById('crmBusinessManage_linkmanNname_name').value=rec.get('name');
	document.getElementById('crmBusinessManage_email').value=rec.get('email');
	var win = Ext.getCmp('selectProjectLinkManFun')
	if(win){win.close();}
	}

//新建联系人
function addLinkMan(){
	var win = Ext.getCmp('addLinkMan');
	if (win) {win.close();}
	var addLinkMan= new Ext.Window({
	id:'addLinkMan',modal:true,title:'添加联系人',layout:'fit',width:1000,height:600,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/crm/customer/linkman/editCrmLinkMan.action' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 addLinkMan.close(); }  }]  });     addLinkMan.show(); }




function save() {
if(checkSubmit()==true){
	    var crmBusinessManageItemDivData = $("#crmBusinessManageItemdiv").data("crmBusinessManageItemGrid");
		document.getElementById('crmBusinessManageItemJson').value = commonGetModifyRecords(crmBusinessManageItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/sale/crmBusinessManage/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/crm/sale/crmBusinessManage/copyCrmBusinessManage.action?id=' + $("#crmBusinessManage_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#crmBusinessManage_id").val() + "&tableId=crmBusinessManage");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmBusinessManage_id").val());
	nsc.push("编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'商机管理',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/crm/sale/crmBusinessManage/showCrmBusinessManageItemList.action", {
				id : $("#crmBusinessManage_id").val()
			}, "#crmBusinessManageItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);