function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/crm/sale/crmBusinessManage/editCrmBusinessManage.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/crm/sale/crmBusinessManage/viewCrmBusinessManage.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : '编码',
		dataIndex : 'id',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '商机来源',
		dataIndex : 'name',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : '客户经理',
		dataIndex : 'type-name',
		width:15*6,
		hidden:false
	}, 
	    {
		header : '状态',
		dataIndex : 'state-name',
		width:15*6,
		hidden:false
	}, 
	    {
		header : '创建人',
		dataIndex : 'createUser-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : '创建日期',
		dataIndex : 'createDate',
		width:15*6,
		hidden:false
	}, 
	
	   
	{
		header : '备注',
		dataIndex : 'note',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : '岗位类型',
		dataIndex : 'postType-name',
		width:15*6,
		hidden:false
	}, 
	    {
		header : '联系人姓名',
		dataIndex : 'linkmanNname-name',
		width:50*6,
		hidden:false
	}, 
	   
	{
		header : '联系人手机',
		dataIndex : 'linkmanMobile',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '固定电话',
		dataIndex : 'linkmanPhone',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : '相关主表',
		dataIndex : 'businessManage-name',
		width:15*6,
		hidden:true
	}, 
	   
	{
		header : 'email',
		dataIndex : 'email',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '单位名称',
		dataIndex : 'unitName',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '单位地址',
		dataIndex : 'unitAddress',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : '所属委托人',
		dataIndex : 'belongConsignor-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : '研究方向',
		dataIndex : 'resDirection',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '部门/学科',
		dataIndex : 'dept',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : '行业类型',
		dataIndex : 'industryType-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : '国家',
		dataIndex : 'country',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '省',
		dataIndex : 'privince',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '市',
		dataIndex : 'city',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : 'content1',
		dataIndex : 'content1',
		width:50*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content2',
		dataIndex : 'content2',
		width:50*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content3',
		dataIndex : 'content3',
		width:50*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content4',
		dataIndex : 'content4',
		width:50*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content5',
		dataIndex : 'content5',
		width:15*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content6',
		dataIndex : 'content6',
		width:15*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content7',
		dataIndex : 'content7',
		width:15*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content8',
		dataIndex : 'content8',
		width:15*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content9',
		dataIndex : 'content9',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content10',
		dataIndex : 'content10',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content11',
		dataIndex : 'content11',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content12',
		dataIndex : 'content12',
		width:20*6,
		hidden:true
	}, 
	
			
			
			
			{
				header : '上级编码',
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#crmBusinessManageTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
