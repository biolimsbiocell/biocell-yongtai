﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/crm/sale/crmSaleBill/editCrmSaleBill.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/sale/crmSaleBill/showCrmSaleBillList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#crmSaleBill", {
					userId : userId,
					userName : userName,
					formId : $("#crmSaleBill_id").val(),
					title : $("#crmSaleBill_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#crmSaleBill_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});






function save() {
if(checkSubmit()==true){
	    var crmSaleBillItemDivData = $("#crmSaleBillItemdiv").data("crmSaleBillItemGrid");
		document.getElementById('crmSaleBillItemJson').value = commonGetModifyRecords(crmSaleBillItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/sale/crmSaleBill/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/crm/sale/crmSaleBill/copyCrmSaleBill.action?id=' + $("#crmSaleBill_id").val();
}
$("#toolbarbutton_status").click(function() {
	if ($("#crmSaleBill_id").val())
		commonChangeState("formId=" + $("#crmSaleBill_id").val() + "&tableId=CrmSaleBill");
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmSaleBill_id").val());
	nsc.push("编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'发票管理',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/crm/sale/crmSaleBill/showCrmSaleBillItemList.action", {
				id : $("#crmSaleBill_id").val()
			}, "#crmSaleBillItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
function CrmContractFun(){
		var win = Ext.getCmp('CrmContractFun');
		if (win) {win.close();}
		var CrmContractFun= new Ext.Window({
		id:'CrmContractFun',modal:true,title:'选择所属合同',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='/crm/contract/crmContract/crmContractSelect.action?flag=CrmContractFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 CrmContractFun.close(); }  }]  });     CrmContractFun.show(); 
		 }

function setCrmContractFun(rec){
		document.getElementById('crmSaleBill_crmContract').value=rec.get('id');
		document.getElementById('crmSaleBill_crmContract_name').value=rec.get('name');
		var win = Ext.getCmp('CrmContractFun')
		if(win){win.close();}
		//客户经理
		var id=rec.get('id');
		var name=rec.get('name');
		var manager=rec.get('manager-name');
		document.getElementById('crmSaleBill_crmContract_manager_name').value = manager;
		}
