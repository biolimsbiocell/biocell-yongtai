﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	//如果状态为21（已下达）就改变商机里的所有为只读和灰色
	var crmSaleChance_state = $("#crmSaleChance_state").val();
	if(crmSaleChance_state!="" && crmSaleChance_state!=null && crmSaleChance_state == "21"){
		var inputs=$("#frame-table1 tr td input");
	    for(var i=0;i<inputs.length;i++){
	    	inputs[i].setAttribute("readOnly",true);
	    	inputs.addClass(".text input readonlytrue");
	    }
	    var textareas =$("#textareas textarea").val();
	    $("#crmSaleChance_content").addClass(".text input readonlytrue");
	    //$("#crmSaleChance_content").setAttribute("readOnly",true);
	}
	
});
function add() {
	window.location = window.ctx + "/crm/sale/crmSaleChance/editCrmSaleChance.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/crm/sale/crmSaleChance/showCrmSaleChanceList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var id=$("#crmSaleChance_id").val();
	if (!id) {
		message("编码不能为空！");
		return;
	}
//	var project = $("#crmSaleChance_project").val();
//	if (!project) {
//		message("项目不能为空！");
//		return;
//	}
	
	save();
});

$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("CrmSaleChance",{
					userId : userId,
					userName : userName,
					formId : $("#crmSaleChance_id").val(),
					title : $("#crmSaleChance_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#crmSaleChance_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});

function save() {
if(checkSubmit()==true){
		var crmSaleChanceLinkmanDivData = crmSaleChanceLinkmanGrid;
		document.getElementById('crmSaleChanceLinkmanJson').value = commonGetModifyRecords(crmSaleChanceLinkmanDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/sale/crmSaleChance/save.action";
		form1.submit();
		}
}		
function editCopy() {
	window.location = window.ctx + '/crm/sale/crmSaleChance/copyCrmSaleChance.action?id=' + $("#crmSaleChance_id").val();
}
$("#toolbarbutton_status").click(function() {
	var manager = $("#crmSaleChance_manager").val();
	if (!manager) {
		message("客户经理不能为空！");
		return;
	}
	
	var geneName=$("#crmSaleChance_geneName").val();
	if (!geneName) {
		message("基因名称未填！");
		return;
	}
	var changeUser = $("#crmSaleChance_changeUser").val();
	if (!changeUser) {
		message("项目评估人不能为空！");
		return;
	}
	if ($("#crmSaleChance_id").val())
		commonChangeState("formId=" + $("#crmSaleChance_id").val() + "&tableId=CrmSaleChance");
});
//选择项目联系人
function selectProjectLinkMan(){
	var win = Ext.getCmp('selectProjectLinkManFun');
	if (win) {win.close();}
	var selectProjectLinkManFun= new Ext.Window({
	id:'selectProjectLinkManFun',modal:true,title:'选择项目联系人',layout:'fit',width:600,height:580,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ window.ctx+ "/crm/customer/linkman/crmLinkManSelect.action?flag=selectProjectLinkManFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectProjectLinkManFun.close(); }  }]  });     selectProjectLinkManFun.show(); }
function setCrmLinkManFun(rec){
	document.getElementById('crmSaleChance_linkMan').value=rec.get('id');
	document.getElementById('crmSaleChance_linkMan_name').value=rec.get('name');
	document.getElementById('crmSaleChance_linkMan_mobile').value=rec.get('mobile');
	document.getElementById('crmSaleChance_linkMan_telephone').value=rec.get('telephone');
	document.getElementById('crmSaleChance_linkMan_area').value=rec.get('area');
	document.getElementById('crmSaleChance_linkMan_selA').value=rec.get('selA');
	document.getElementById('crmSaleChance_linkMan_selB').value=rec.get('selB');
	document.getElementById('crmSaleChance_linkMan_email').value=rec.get('email');
	document.getElementById('crmSaleChance_linkMan_customerId_department_name').value=rec.get('customerId-department-name');
	var win = Ext.getCmp('selectProjectLinkManFun');
	if(win){win.close();}
	}
//项目评估申请选择项目负责人
function aa(){
	var win = Ext.getCmp('selecttransUser');
	if (win) {win.close();}
	var selecttransUser= new Ext.Window({
		id:'selecttransUser',modal:true,title:'选择项目负责人',layout:'fit',width:800,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
			html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/showUserListAllSelect.action' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			          { text: '关闭',
			        	  handler: function(){
			        		  selecttransUser.close(); }  }]  });     selecttransUser.show(); }
		function addUserGrid(record,funType){
			var n="";
			var nId="";
			for(var i=0;i< record.length;i++){
				 n=n+record[i].get('name') +",";
				 nId=nId+record[i].get('id') +",";
			}
			document.getElementById('crmSaleChance_changeUser').value = n;
			document.getElementById('crmSaleChance_changeUserId').value = nId;
			var win = Ext.getCmp('selecttransUser');
			if(win){
				win.close();
			}
		}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'项目评估申请',
	    	   contentEl:'markup'
	       } ]
	   });
	load("/crm/sale/crmSaleChance/showCrmSaleChanceLinkmanList.action", {
		id : $("#crmSaleChance_id").val(),
	}, "#crmSaleChanceLinkmanpage");
	var crmSaleChance_state = $("#crmSaleChance_stateName").val();
	if($("#crmSaleChance_state").val()!='1'){
		var item = menu.add({
			text: '生成项目可行性评估'
		});
		item.on('click', function(){
			if($("#crmSaleChance_manager").val()!=null && $("#crmSaleChance_manager").val()!=""){
			Ext.MessageBox.confirm("标题", "是否确认信息内容", function (btn) {
				 if(btn=='yes'){
						var id = $("#crmSaleChance_id").val();
						if(id){
							ajax("post", "/exp/project/projectFeasibility/createProjectPage.action", {
								id : id
								}, function(data) {
									if (data.success) {
										if(data.data){
											message("生成【项目评估页面】成功。");
											window.location.reload();
										}
									} else {
										message("生成项目评估页面信息失败，请重试！");
									}
								}, null);
						}
				 }else{
					 return;
				 }
			});
			}else{
				message("【客户经理】不能为空！");
			}
		});
		var item = menu.add({
			text: '生成初步设计页面',
			id:'projectStart'
		});
		item.on('click', function () {
			var nId=$("#crmSaleChance_changeUserId").val();
			if($("#crmSaleChance_manager").val()!=null && $("#crmSaleChance_manager").val()!=""){
				if($("#crmSaleChance_geneName").val()!=null && $("#crmSaleChance_geneName").val()!=""){
		    Ext.MessageBox.confirm("标题", "是否确认信息内容", function (btn) {
			       if(btn=='yes'){
			    	   var id = $("#crmSaleChance_id").val();
			    		if(id){
			    			ajax("post", "/exp/project/projectStart/createProjectStartPage.action", {
			    				id : id,
			    				nId:nId
			    				}, function(data) {
			    					if (data.success) {
			    						if(data.data){
			    							message("生成【初步设计页面】成功。");
			    							window.location.reload();
			    						}
			    					} else {
			    						message("生成初步设计页面信息失败，请重试！");
			    					}
			    				}, null);
			    		}
			       }else{
			    	   return;
			       }
			    });
				}else{
		    	message("【基因名称】不能为空！");
		    }
			}else{
				message("【客户经理】不能为空！");
			}
			});
		}
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

function lookForProjectMessage(){
	var str = "";
	var tableId = $("#crmSaleChance_id").val();
	var projectId = $("#crmSaleChance_project").val();
//	alert(tableId);
//	if(tableId){
//		var win = Ext.getCmp('project');
//	if (win) {win.close();}
//	var project= new Ext.Window({
//	id:'project',modal:true, title:'查看项目管理', layout:'fit', width:1200, height:600, closeAction:'close',
//	plain:true, bodyStyle:'padding:5px;', buttonAlign:'center',
//	collapsible:true, maximizable:true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/exp/project/project/viewProject.action?id=" + tableId + "' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	handler: function(){
//		 project.close(); }  }
////		 { text: '体外受精获取精子信息',
////					 handler: function(){
////						 transferTable();
////						 project.close(); }  }
//		 ]  });     
//		 project.show(); 
//	}else{
//		 message("请您生成出库单！");
//	}
	if (projectId != "") {
	ajax("post", "/crm/sale/crmSaleChance/getProjectMessage.action", {
		projectId : projectId
		}, function(data) {
			if (data.success) {
				if(data.data){
//					alert(data.data[0].id);
//					alert("g:"+data.data[0].geneTargetingType);
					setDocumentGetElementByIdValue4Obj(data.data[0].geneTargetingType, "crmSaleChance_dbMethod");
					setDocumentGetElementByIdValue4Obj(data.data[0].technologyType, "crmSaleChance_technologyType");
					setDocumentGetElementByIdValue4Obj(data.data[0].genus, "crmSaleChance_genus");
					setDocumentGetElementByIdValue4Obj(data.data[0].strain, "crmSaleChance_strain");
					setDocumentGetElementByIdValue4Obj(data.data[0].orderSource, "crmSaleChance_chanceSource");
					if (data.data[0].projectStart != null) {
						document.getElementById("crmSaleChance_geneName").value =data.data[0].projectStart.geneName;
					} else {
						document.getElementById("crmSaleChance_geneName").value = "";
					}
//					alert(data.data[0].geneTargetingType.id);
//					alert(data.data[0].technologyType.id);
//					alert(data.data[0].genus.id);
//					alert(data.data[0].strain.id);
//					alert(data.data[0].orderSource.id);
//					alert(data.data[0].projectStart.geneName);
				}
			} else {
				message("获取信息失败，请重试！");
			}
		}, null);
	} else {
		message("没有项目编号！");
		return;
	}
//	form1.action = window.ctx + "/crm/sale/crmSaleChance/getProjectMessageAndSet.action?id=" + $("#crmSaleChance_id").val() + "&projectId=" + $("#crmSaleChance_project").val();
//	form1.submit();
}

function setDocumentGetElementByIdValue4Obj(dataElement, target){
	var target_name = target + "_name";
	if (dataElement != null) {
		document.getElementById(target).value = dataElement.id;
		document.getElementById(target_name).value = dataElement.name;
	} else {
		document.getElementById(target).value = "";
		document.getElementById(target_name).value = "";
	}
}

	/*var item = menu.add({
		text: '复制'
	});
	item.on('click', editCopy);*/
	
	/*var item = menu.add({
		text: '复制'
	});
	item.on('click', editCopy);
	//生成订单事件
	function editOrder(){
		window.location = window.ctx + '/crm/sale/crmSaleChance/editOrderList.action?id='+$("#crmSaleChance_id").val();
	}*/

function createchangeStateName(){
	if($("#crmSaleChance_id").val()!="NEW"&&$("#crmSaleChance_stateName").val()!="新建"){
		$("#crmSaleChance_changeStateName").val("初步设计");
		save();
	}else{
		message("工作流状态未完成");
	}
}