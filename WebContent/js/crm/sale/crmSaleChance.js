var crmSaleChanceGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
//	fields.push({
//		name : 'linkMan-customerId-department-name',
//		type : "string"
//	});
	fields.push({
		name : 'changeStateName',
		type : "string"
	});
	fields.push({
		name : 'genus-id',
		type : "string"
	});
	fields.push({
		name : 'genus-name',
		type : "string"
	});
	fields.push({
		name : 'strain-id',
		type : "string"
	});
	fields.push({
		name : 'strain-name',
		type : "string"
	});
	fields.push({
		name : 'chanceSource-id',
		type : "string"
	});
	fields.push({
		name : 'chanceSource-name',
		type : "string"
	});
	fields.push({
		name : 'type-id',
		type : "string"
	});
	fields.push({
		name : 'type-name',
		type : "string"
	});
	fields.push({
		name : 'area-id',
		type : "string"
	});
	fields.push({
		name : 'area-name',
		type : "string"
	});
//	fields.push({
//		name : 'email',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'manager-id',
		type : "string"
	});
	fields.push({
		name : 'manager-name',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'content',
		type : "string"
	});
	fields.push({
		name : 'evalustionType-id',
		type : "string"
	});
	fields.push({
		name : 'evalustionType-name',
		type : "string"
	});
	fields.push({
		name : 'bussinessType-id',
		type : "string"
	});
	fields.push({
		name : 'bussinessType-name',
		type : "string"
	});
	fields.push({
		name : 'productType-id',
		type : "string"
	});
	fields.push({
		name : 'productType-name',
		type : "string"
	});
	fields.push({
		name : 'technologyType-id',
		type : "string"
	});
	fields.push({
		name : 'technologyType-name',
		type : "string"
	});
	
	fields.push({
		name : 'preFinishDate',
		type : "string"
	});
	fields.push({
		name : 'geneName',
		type : "string"
	});
	fields.push({
		name : 'geneId',
		type : "string"
	});
	fields.push({
		name : 'evalustionUser-id',
		type : "string"
	});
	fields.push({
		name : 'evalustionUser-name',
		type : "string"
	});
	fields.push({
		name : 'successChance',
		type : "string"
	});
	fields.push({
		name : 'followUser-id',
		type : "string"
	});
	fields.push({
		name : 'followUser-name',
		type : "string"
	});
	fields.push({
		name : 'dbMethod-id',
		type : "string"
	});
	fields.push({
		name : 'dbMethod-name',
		type : "string"
	});
	fields.push({
		name : 'followDate',
		type : "string"
	});
	fields.push({
		name : 'followResult-id',
		type : "string"
	});
	fields.push({
		name : 'followResult-name',
		type : "string"
	});
	fields.push({
		name : 'followExplain',
		type : "string"
	});
	fields.push({
		name : 'orderChance',
		type : "string"
	});
	//项目评估负责人
	fields.push({
		name : 'changeUser-id',
		type : "string"
	});
	fields.push({
		name : 'changeUser-name',
		type : "string"
	});
	fields.push({
		name : 'linkMan-id',
		type : "string"
	});
	//联系人
	fields.push({
		name : 'linkMan-name',
		type : "string"
	});
	fields.push({
		name : 'linkMan-email',
		type : "string"
	});
	fields.push({
		name : 'linkMan-mobile',
		type : "string"
	});
	fields.push({
		name : 'content1',
		type : "string"
	});
	fields.push({
		name : 'content2',
		type : "string"
	});
	fields.push({
		name : 'content3',
		type : "string"
	});
	fields.push({
		name : 'content4',
		type : "string"
	});
	fields.push({
		name : 'content5',
		type : "string"
	});
	fields.push({
		name : 'content6',
		type : "string"
	});
	fields.push({
		name : 'content7',
		type : "string"
	});
	fields.push({
		name : 'content8',
		type : "string"
	});
	fields.push({
		name : 'content9',
		type : "string"
	});
	fields.push({
		name : 'content10',
		type : "string"
	});
	fields.push({
		name : 'content11',
		type : "string"
	});
	fields.push({
		name : 'content12',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'changeStateName',
		header : '是否生成后续工作',
		width : 15 * 8,
		sortable : true
	});
	cm.push({
		dataIndex : 'id',
		header : '编码',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : '创建人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : '创建人',
		width : 10 * 6,
		sortable : true
	});
	/*
	 * cm.push({ dataIndex:'evalustionUser-id', header:'项目评估负责人ID', hidden:true,
	 * width:15*10, sortable:true }); cm.push({ dataIndex:'evalustionUser-name',
	 * header:'评估负责人', width:10*7, sortable:true });
	 */
	cm.push({
		dataIndex : 'manager-id',
		header : '客户经理ID',
		hidden : true,
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'manager-name',
		header : '客户经理',
		width : 10 * 7,
		sortable : true
	});
//	cm.push({
//		dataIndex : 'linkMan-customerId-department-name',
//		header : '联系人单位',
//		width : 30 * 4,
//		sortable : true
//	});
//
//	cm.push({
//		dataIndex : 'email',
//		header : '邮件',
//		width : 20 * 6,
//		sortable : true
//	});
//	cm.push({
//		dataIndex : 'phone',
//		header : '电话',
//		width : 15 * 6,
//		sortable : true
//	});

//	cm.push({
//		dataIndex : 'email',
//		header : '邮件',
//		hidden : true,
//		width : 20 * 6,
//		sortable : true
//	});
//	cm.push({
//		dataIndex : 'phone',
//		header : '电话',
//		hidden : true,
//		width : 15 * 6,
//		sortable : true
//	});
	cm.push({
		dataIndex : 'geneName',
		header : '基因名称',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneId',
		hidden : false,
		header : '基因ID(NCBI)',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'genus-id',
		hidden : true,
		header : '种属ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'genus-name',
		header : '种属',
		width : 10 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'strain-id',
		hidden : true,
		header : '品系ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'strain-name',
		header : '品系',
		width : 10 * 6,
		sortable : true
	});

	cm.push({
		dataIndex : 'dbMethod-id',
		header : '打靶方案ID',
		hidden : true,
		width : 20 * 5,
		sortable : true
	});
	cm.push({
		dataIndex : 'dbMethod-name',
		header : '打靶方案',
		width : 20 * 5,
		sortable : true
	});
	/*
	 * cm.push({ dataIndex:'manager-id', hidden:true, header:'客户经理ID',
	 * width:15*10, sortable:true }); cm.push({ dataIndex:'manager-name',
	 * header:'客户经理', width:15*10, sortable:true });
	 */
	/*
	 * cm.push({ dataIndex:'name', header:'描述', width:20*6, sortable:true });
	 */
	/*
	 * cm.push({ dataIndex:'area-id', hidden:true, header:'地区ID', width:15*10,
	 * sortable:true }); cm.push({ dataIndex:'area-name', header:'地区',
	 * width:10*6, sortable:true });
	 */
	cm.push({
		dataIndex : 'createDate',
		header : '创建日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : '期望结束日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'chanceSource-id',
		hidden : true,
		header : '来源ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'chanceSource-name',
		header : '来源',
		width : 10 * 4,
		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : '工作流状态ID',
		width : 40 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '工作流状态',
		width : 12 * 6,
		sortable : true
	});
	/*
	 * cm.push({ dataIndex:'content', header:'商机内容', width:50*6,
	 * 
	 * sortable:true }); cm.push({ dataIndex:'followUser-id', header:'跟踪人员ID',
	 * hidden:true, width:15*10, sortable:true }); cm.push({
	 * dataIndex:'followUser-name', header:'跟踪人员', width:15*10, sortable:true
	 * }); cm.push({ dataIndex:'followDate', header:'跟踪日期', width:15*10,
	 * sortable:true }); cm.push({ dataIndex:'followResult-id', header:'跟踪结果ID',
	 * hidden:true, width:15*10, sortable:true }); cm.push({
	 * dataIndex:'followResult-name', header:'跟踪结果', width:15*10, sortable:true
	 * }); cm.push({ dataIndex:'followExplain', header:'结果说明', width:50*10,
	 * sortable:true });
	 */
	/*
	 * cm.push({ dataIndex:'orderChance', header:'成单可能性', width:10*10,
	 * sortable:true });
	 */
	/*
	 * cm.push({ dataIndex:'evalustionType-id', header:'评估类型ID', hidden:true,
	 * width:15*10, sortable:true }); cm.push({ dataIndex:'evalustionType-name',
	 * header:'评估类型', width:15*10, sortable:true });
	 */
	cm.push({
		dataIndex : 'bussinessType-id',
		header : '项目类型ID',
		hidden : true,
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'bussinessType-name',
		header : '项目类型',
		width : 15 * 7,
		sortable : true
	});
	cm.push({
		dataIndex : 'productType-id',
		header : '产品类型ID',
		hidden : true,
		width : 15 * 8,
		sortable : true
	});
	cm.push({
		dataIndex : 'productType-name',
		header : '产品类型',
		width : 15 * 7,
		sortable : true
	});

	/*
	 * cm.push({ dataIndex:'resType-id', header:'物种ID', hidden:true,
	 * width:20*10, sortable:true }); cm.push({ dataIndex:'resType-name',
	 * header:'物种', width:20*10, sortable:true });
	 */

	cm.push({
		dataIndex : 'technologyType-id',
		header : '技术类型ID',
		hidden : true,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'technologyType-name',
		header : '技术类型',
		width : 20 * 10,
		sortable : true
	});
	//项目评估负责人
	cm.push({
		dataIndex : 'changeUser-id',
		hidden:true,
		header : '项目评估负责人ID',
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'changeUser-name',
		header : '项目评估负责人',
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'linkMan-id',
		hidden : true,
		header : '联系人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'linkMan-name',
		hidden : false,
		header : '联系人',
		width : 15 * 4,
		sortable : true
	});
	cm.push({
		dataIndex : 'linkMan-email',
		hidden : false,
		header : '邮件',
		width : 15 * 8,
		sortable : true
	});
	cm.push({
		dataIndex : 'linkMan-mobile',
		hidden : false,
		header : '电话',
		width : 15 * 4,
		sortable : true
	});
	/*
	 * cm.push({ dataIndex:'preFinishDate', header:'预计评估完成时间', width:15*10,
	 * sortable:true });
	 */
	/*
	 * cm.push({ dataIndex:'successChance', header:'成单可能性', width:10*10,
	 * sortable:true });
	 */

	cm.push({
		dataIndex : 'content1',
		header : '客户要求',
		width : 20 * 6,
		hidden : false,
		sortable : true
	});
	cm.push({
		dataIndex : 'content2',
		header : 'content2',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content3',
		header : 'content3',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content4',
		header : 'content4',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content5',
		header : 'content5',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content6',
		header : 'content6',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content7',
		header : 'content7',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content8',
		header : 'content8',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content9',
		header : 'content9',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content10',
		header : 'content10',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content11',
		header : 'content11',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content12',
		header : 'content12',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/sale/crmSaleChance/showCrmSaleChanceListJson.action";
	var opts = {};
	opts.title = "项目评估申请";
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	crmSaleChanceGrid = gridTable("show_crmSaleChance_div", cols, loadParam,
			opts);
})
function add() {
	window.location = window.ctx
			+ '/crm/sale/crmSaleChance/editCrmSaleChance.action';
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/crm/sale/crmSaleChance/editCrmSaleChance.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/crm/sale/crmSaleChance/viewCrmSaleChance.action?id=' + id;
}

function exportexcel() {
	crmSaleChanceGrid.title = '导出列表';
	var vExportContent = crmSaleChanceGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(
			function() {
				var option = {};
				option.width = 642;
				option.height = 417;
				loadDialogPage($("#jstj"), "搜索", null, {
					"开始检索" : function() {

						if (($("#startcreateDate").val() != undefined)
								&& ($("#startcreateDate").val() != '')) {
							var startcreateDatestr = ">=##@@##"
									+ $("#startcreateDate").val();
							$("#createDate1").val(startcreateDatestr);
						}
						if (($("#endcreateDate").val() != undefined)
								&& ($("#endcreateDate").val() != '')) {
							var endcreateDatestr = "<=##@@##"
									+ $("#endcreateDate").val();

							$("#createDate2").val(endcreateDatestr);

						}
						
						if (($("#startendDate").val() != undefined)
								&& ($("#startendDate").val() != '')) {
							var startendDatestr = ">=##@@##"
									+ $("#startendDate").val();
							$("#endDate1").val(startendDatestr);
						}
						if (($("#endendDate").val() != undefined)
								&& ($("#endendDate").val() != '')) {
							var endendDatestr = "<=##@@##"
									+ $("#endendDate").val();

							$("#endDate2").val(endendDatestr);

						}
						
						if (($("#startcontent9").val() != undefined)
								&& ($("#startcontent9").val() != '')) {
							var startcontent9str = ">=##@@##"
									+ $("#startcontent9").val();
							$("#content91").val(startcontent9str);
						}
						if (($("#endcontent9").val() != undefined)
								&& ($("#endcontent9").val() != '')) {
							var endcontent9str = "<=##@@##"
									+ $("#endcontent9").val();

							$("#content92").val(endcontent9str);

						}

						if (($("#startcontent10").val() != undefined)
								&& ($("#startcontent10").val() != '')) {
							var startcontent10str = ">=##@@##"
									+ $("#startcontent10").val();
							$("#content101").val(startcontent10str);
						}
						if (($("#endcontent10").val() != undefined)
								&& ($("#endcontent10").val() != '')) {
							var endcontent10str = "<=##@@##"
									+ $("#endcontent10").val();

							$("#content102").val(endcontent10str);

						}

						if (($("#startcontent11").val() != undefined)
								&& ($("#startcontent11").val() != '')) {
							var startcontent11str = ">=##@@##"
									+ $("#startcontent11").val();
							$("#content111").val(startcontent11str);
						}
						if (($("#endcontent11").val() != undefined)
								&& ($("#endcontent11").val() != '')) {
							var endcontent11str = "<=##@@##"
									+ $("#endcontent11").val();

							$("#content112").val(endcontent11str);

						}

						if (($("#startcontent12").val() != undefined)
								&& ($("#startcontent12").val() != '')) {
							var startcontent12str = ">=##@@##"
									+ $("#startcontent12").val();
							$("#content121").val(startcontent12str);
						}
						if (($("#endcontent12").val() != undefined)
								&& ($("#endcontent12").val() != '')) {
							var endcontent12str = "<=##@@##"
									+ $("#endcontent12").val();

							$("#content122").val(endcontent12str);

						}

						commonSearchAction(crmSaleChanceGrid);
						$(this).dialog("close");

					},
					"清空" : function() {
						form_reset();

					}
				}, true, option);
			});
});
