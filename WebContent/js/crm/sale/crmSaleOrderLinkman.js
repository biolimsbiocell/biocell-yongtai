﻿var crmSaleOrderLinkmanGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	// fields.push({
	// name:'id',
	// type:"string"
	// });
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'email',
		type : "string"
	});
	fields.push({
		name : 'ktEmail',
		type : "string"
	});
	fields.push({
		name : 'deptName',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'tel',
		type : "string"
	});
	fields.push({
		name : 'trans',
		type : "string"
	});
	/*
	 * fields.push({ name : 'station', type : "string" });
	 */
	fields.push({
		name : 'crmSaleOrder-id',
		type : "string"
	});
	fields.push({
		name : 'crmSaleOrder-name',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	// cm.push({
	// dataIndex:'id',
	// hidden : true,
	// header:'编码',
	// width:40*6,
	//		
	// editor : new Ext.form.TextField({
	// allowBlank : true
	// })
	// });
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编号',
		width : 30 * 6
	//		
	// editor : new Ext.form.TextField({
	// allowBlank : true
	// })
	});
	cm.push({
		dataIndex : 'name',
		hidden : false,
		header : '姓名',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'email',
		hidden : false,
		header : 'Email',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'ktEmail',
		hidden : false,
		header : '接收课题回报Email',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'phone',
		hidden : false,
		header : '手机',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'tel',
		hidden : false,
		header : '固定电话',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'deptName',
		hidden : false,
		header : '单位名称',
		width : 30 * 8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'trans',
		hidden : false,
		header : '传真',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*
	 * cm.push({ dataIndex : 'station', hidden : false, header : '岗位', width :
	 * 30 * 6, editor : new Ext.form.TextField({ allowBlank : true }) });
	 */
	cm.push({
		dataIndex : 'crmSaleOrder-id',
		hidden : true,
		header : '相关主表ID',
		width : 15 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmSaleOrder-name',
		hidden : true,
		header : '相关主表',
		width : 15 * 10
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/sale/crmSaleOrder/showCrmSaleOrderLinkmanListJson.action?id="
			+ $("#id_parent_hidden").val() + "&wid="
			+ $("#crmSaleOrder_id").val();
	var opts = {};
	opts.title = "订单管理联系人";
	opts.height = document.body.clientHeight - 100;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/crm/sale/crmSaleOrder/delCrmSaleOrderLinkman.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	//    
	// opts.tbar.push({
	// text : '选择相关主表',
	// handler : selectcrmSaleOrderFun
	// });
	//	
	//	
	// opts.tbar.push({
	// text : "批量上传（CSV文件）",
	// handler : function() {
	// var options = {};
	// options.width = 350;
	// options.height = 200;
	// loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
	// "确定":function(){
	// goInExcelcsv();
	// $(this).dialog("close");
	// }
	// },true,options);
	// }
	// });

	function goInExcelcsv() {
		var file = document.getElementById("file-uploadcsv").files[0];
		var n = 0;
		var ob = crmSaleOrderLinkmanGrid.getStore().recordType;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
				if (n > 0) {
					if (this[0]) {
						var p = new ob({});
						p.isNew = true;
						var o;
						o = 0 - 1;
						p.set("po.fieldName", this[o]);

						o = 1 - 1;
						p.set("po.fieldName", this[o]);

						o = 2 - 1;
						p.set("po.fieldName", this[o]);

						o = 3 - 1;
						p.set("po.fieldName", this[o]);

						o = 4 - 1;
						p.set("po.fieldName", this[o]);

						o = 5 - 1;
						p.set("po.fieldName", this[o]);

						o = 6 - 1;
						p.set("po.fieldName", this[o]);

						o = 7 - 1;
						p.set("po.fieldName", this[o]);

						crmSaleOrderLinkmanGrid.getStore().insert(0, p);
					}
				}
				n = n + 1;

			});
		}
	}

	if ($("#crmSaleOrder_state").val() == 1) {
		// alert("工作流状态已完成，页面不可编辑。");
	} else {
		/*
		 * opts.tbar.push({ text : '填加明细', handler : null });
		 */
		opts.tbar.push({
			text : '显示可编辑列',
			handler : null
		});
		opts.tbar.push({
			text : '取消选中',
			handler : null
		});
		opts.tbar.push({
			text : '选择订单管理联系人',
			handler : selectOrderMangerUser
		});
	}
	crmSaleOrderLinkmanGrid = gridEditTable("crmSaleOrderLinkmandiv", cols,
			loadParam, opts);
	$("#crmSaleOrderLinkmandiv").data("crmSaleOrderLinkmanGrid",
			crmSaleOrderLinkmanGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmSaleOrderFun() {
	var win = Ext.getCmp('selectcrmSaleOrder');
	if (win) {
		win.close();
	}
	var selectcrmSaleOrder = new Ext.Window(
			{
				id : 'selectcrmSaleOrder',
				modal : true,
				title : '选择相关主表',
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/CrmSaleOrderSelect.action?flag=crmSaleOrder' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						selectcrmSaleOrder.close();
					}
				} ]
			});
	selectcrmSaleOrder.show();
}
// 选择订单管理联系人
function selectOrderMangerUser() {
	var title = '';
	var url = '';
	title = "选择订单管理联系人";
	url = "/crm/customer/linkman/crmLinkManSelect.action";
	var options = {};
	options.width = 800;
	options.height = 500;
	loadDialogPage(null, title, url, {
		"确定" : function() {
			selVal(this);
			options.close();
		}
	}, true, options);
}
var selVal = function(win) {
	var operGrid = crmLinkManDialogGrid;
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var crmSaleOrderGrid=crmSaleOrderLinkmanGrid;
	var ob = crmSaleOrderGrid.getStore().recordType;
	crmSaleOrderGrid.stopEditing();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			var p=new ob({});
			p.isNew =true;
			p.set("name",obj.get("name"));
			p.set("email",obj.get("email"));
			p.set("ktEmail",obj.get("ktemail"));
			p.set("phone",obj.get("mobile"));
			p.set("tel",obj.get("telephone"));
			p.set("trans",obj.get("trans"));
			p.set("trans",obj.get("trans"));
			p.set("deptName",obj.get("customerId-department-name"));
			crmSaleOrderGrid.getStore().add(p);
		});
		crmSaleOrderGrid.startEditing(0, 0);
	} else {
		message("请选择您要选择的数据");
		return;
	}
};
