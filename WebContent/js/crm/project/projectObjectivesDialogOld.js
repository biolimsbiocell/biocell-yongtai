var projectObjectivesDialogGrid;
$(function(){
	var cols={};
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'tProject-id',
		type:"string"
	});
	    fields.push({
		name:'tProject-name',
		type:"string"
	});
	    fields.push({
			name:'orderCode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.master.name,
		width:20*6
	});
	cm.push({
		dataIndex:'orderCode',
		hidden : false,
		header:"订单编号",
		width:20*6,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/project/showProjectObjectivesListJson.action?id="+ $("#id").val();
	var opts={};
	opts.title="选择订单";
	opts.width = document.body.clientWidth - 400;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		setDicProject();		
	};
	
	//projectObjectivesDialogGrid=gridTable("show_dialog_project_div",cols,loadParam,opts);
	projectObjectivesDialogGrid=gridTable("show_dialog_projectObjectives_div",cols,loadParam,opts);
	$("#show_dialog_projectObjectives_div").data("projectObjectivesDialogGrid", projectObjectivesDialogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
