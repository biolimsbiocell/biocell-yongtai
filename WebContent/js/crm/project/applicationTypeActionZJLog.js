var applicationTypeActionLogZJGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'taskid',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({// 进读
		name : 'advance',
		type : "string"
	});
	fields.push({
		name : 'accomplispNumber',// 完成数量
		type : "string"
	});
	fields.push({
		name : 'unaccomplispNumber',// 未完成数量
		type : "string"
	});
	fields.push({
		name : 'abnormalNumber',// 异常数量
		type : "string"
	});
	fields.push({
		name : 'reportId',// 报告主键
		type : "string"
	});
	fields.push({
		name : 'reportName',// 报告名称
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'taskid',
		header :biolims.common.taskId_id,
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.taskId,
		width : 30 * 10
	});
	cm.push({
		dataIndex : 'advance',
		header : biolims.sample.taskFlag,
		width : 10 * 10
	});
	cm.push({
		dataIndex : 'accomplispNumber',
		header :biolims.common.sampleCompletionNum,
		width : 10 * 10

	});
	cm.push({
		dataIndex : 'unaccomplispNumber',
		header : biolims.common.sampleNotCompletionNum,
		width : 10 * 10
	});
	cm.push({
		dataIndex : 'abnormalNumber',
		header : biolims.common.abnormalQuantity,
		width : 6 * 10
	});
	cm.push({
		dataIndex : 'reportId',
		header : biolims.common.reportPrimaryKey,
//		hidden : true,
		width : 15 * 10
	});
	cm.push({
				xtype : 'actioncolumn',
				width : 80,
				header : biolims.common.attachment,
				items : [ {
					icon : window.ctx + '/images/img_attach.gif',
					tooltip :biolims.common.attachment,
					handler : function(grid, rowIndex, colIndex) {
						var rec = grid.getStore().getAt(rowIndex);
						// if(rec.get('id')&&rec.get('id')!='NEW'){

						var win = Ext.getCmp('doc');
						if (win) {
							win.close();
						}
						var doc = new Ext.Window(
								{
									id : 'doc',
									modal : true,
									title : biolims.common.attachment,
									layout : 'fit',
									width : 900,
									height : 500,
									closeAction : 'close',
									plain : true,
									bodyStyle : 'padding:5px;',
									buttonAlign : 'center',
									collapsible : true,
									maximizable : true,
									items : new Ext.BoxComponent(
											{
												id : 'maincontent',
												region : 'center',
												html : "<iframe scrolling='no' name='maincontentframe' src='"
														+ window.ctx
														+ "/operfile/initFileZjList.action?modelType=experimentDnaGet&flag=doc&reportId="
														+ rec.get('reportId')
														+ "' frameborder='0' width='100%' height='100%' ></iframe>"
											}),
									buttons : [ {
										text : '关闭(close)',
										handler : function() {
											doc.close();
										}
									} ]
								});
						doc.show();

						// }
					}
				} ]
			});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/project/showApplicationTypeActionLogListZJJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.master.processRecords;
	opts.height = document.body.clientHeight - 250;
	opts.tbar = [];
	// opts.delSelect = function(ids) {
	// ajax("post", "/crm/project/delApplicationTypeActionLog.action", {
	// ids : ids
	// }, function(data) {
	// if (data.success) {
	// scpProToGrid.getStore().commitChanges();
	// scpProToGrid.getStore().reload();
	// message(biolims.common.deleteSuccess);
	// } else {
	// message(biolims.common.deleteFailed);
	// }
	// }, null);
	// };
	// opts.tbar.push({
	// text : biolims.master.selectOper,
	// handler : selectoperUserFun
	// });
	// opts.tbar.push({
	// text : biolims.common.editableColAppear,
	// handler : null
	// });
	// opts.tbar.push({
	// text : biolims.common.uncheck,
	// handler : null
	// });
	applicationTypeActionLogZJGrid = gridEditTable(
			"applicationTypeActionLogZJdiv", cols, loadParam, opts);
	// $("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid",
	// applicationTypeActionLogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectoperUserFun() {
	var win = Ext.getCmp('selectoperUser');
	if (win) {
		win.close();
	}
	var selectoperUser = new Ext.Window(
			{
				id : 'selectoperUser',
				modal : true,
				title : biolims.master.selectOper,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/core/user/userSelect.action?flag=operUser' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						selectoperUser.close();
					}
				} ]
			});
	selectoperUser.show();
}
function setoperUser(id, name) {
	var gridGrid = $("#applicationTypeActionLogdiv").data(
			"applicationTypeActionLogGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('operUser-id', id);
		obj.set('operUser-name', name);
	});
	var win = Ext.getCmp('selectoperUser')
	if (win) {
		win.close();
	}
}
