$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.projectId,
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.common.projectName,
	});
	colOpts.push({
		"data" : "mainProject-id",
		"title" : biolims.sample.customerId,
		"visible": false
	});
	colOpts.push({
		"data" : "mainProject-name",
		"title" : biolims.sample.customerName,
	});
	colOpts.push({
		"data" : "crmDoctor-id",
		"title" : biolims.sample.crmDoctorId,
		"visible": false
	});
	colOpts.push({
		"data" : "crmDoctor-name",
		"title" : biolims.sample.crmDoctorName,
	});
	colOpts.push({
		"data" : "crmlinkMan-id",
		"title" : biolims.crm.crmlinkManNameId,
		"visible": false
	});
	colOpts.push({
		"data" : "crmlinkMan-name",
		"title" : biolims.crm.crmlinkManName,
	});
	var tbarOpts = [];
	var addCrmPatientOptions = table(false, null,
			'/crm/project/showProjectListNewJson.action',
			colOpts, tbarOpts)
	var CrmPatientTable = renderData($("#addcrmProjectTable"), addCrmPatientOptions);
	$("#addcrmProjectTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addcrmProjectTable_wrapper .dt-buttons").empty();
				$('#addcrmProjectTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addcrmProjectTable tbody tr");
			CrmPatientTable.ajax.reload();
			CrmPatientTable.on('draw', function() {
				trs = $("#addcrmProjectTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})

