var applicationTypeActionLogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'applicationTypeTableId',
		type:"string"
	});
	   fields.push({
		name:'contentId',
		type:"string"
	});
	   fields.push({
		name:'oldActionName',
		type:"string"
	});
	   fields.push({
		name:'actionName',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'nextNote',
			type:"string"
		});
	    fields.push({
		name:'operUser-id',
		type:"string"
	});
	    fields.push({
		name:'operUser-name',
		type:"string"
	});
	   fields.push({
		name:'operDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
			name:'startDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'endDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : false,
		header:biolims.common.id,
		width:20*10,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'applicationTypeTableId',
		hidden : false,
		header:biolims.master.relatedModule,
		width:15*10,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contentId',
		hidden : false,
		header:biolims.master.contentId,
		width:15*10,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'oldActionName',
		hidden : false,
		header:biolims.master.oldActionName,
		width:50,
		hidden:true
	});
	cm.push({
		dataIndex:'actionName',
		hidden : false,
		header:biolims.common.state,
		width:150
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.master.note,
		width:30*10,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextNote',
		hidden : false,
		header:biolims.master.nextNote,
		width:30*10,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'operUser-id',
		hidden : true,
		header:biolims.master.operUserId,
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'operUser-name',
		hidden : false,
		header:biolims.master.operUserName,
		width:10*10
	});
	cm.push({
		dataIndex:'operDate',
		hidden : false,
		header:biolims.master.operDate,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'startDate',
		hidden : false,
		header:biolims.common.startTime,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'endDate',
		hidden : false,
		header:biolims.common.endTime,
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/project/showApplicationTypeActionLogListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.processRecords;
	opts.height =  document.body.clientHeight-250;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/project/delApplicationTypeActionLog.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
			text : biolims.master.selectOper,
			handler : selectoperUserFun
		});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	applicationTypeActionLogGrid=gridEditTable("applicationTypeActionLogdiv",cols,loadParam,opts);
	$("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid", applicationTypeActionLogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectoperUserFun(){
	var win = Ext.getCmp('selectoperUser');
	if (win) {win.close();}
	var selectoperUser= new Ext.Window({
	id:'selectoperUser',modal:true,title:biolims.master.selectOper,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=operUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectoperUser.close(); }  }]  });     selectoperUser.show(); }
	function setoperUser(id,name){
		var gridGrid = $("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('operUser-id',id);
			obj.set('operUser-name',name);
		});
		var win = Ext.getCmp('selectoperUser')
		if(win){
			win.close();
		}
	}
	
