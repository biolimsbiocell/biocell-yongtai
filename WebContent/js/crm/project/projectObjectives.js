var projectObjectivesGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'tProject-id',
		type:"string"
	});
	    fields.push({
		name:'tProject-name',
		type:"string"
	});
	    fields.push({
			name:'returnedMoney',
			type:"string"
		});
		    fields.push({
			name:'returnedPerson-id',
			type:"string"
		});
		    fields.push({
			name:'returnedPerson-name',
			type:"string"
		});
		    fields.push({
				name:'returnedScale',
				type:"string"
			});
		    fields.push({
				name:'returnedDate',
				type:"date",
				dateFormat:"Y-m-d"
			});
		    fields.push({
				name:'orderCode',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.master.name,
		width:50*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderCode',
		hidden : false,
		header:"订单编号",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'returnedMoney',
		hidden : true,
		header:'回款金额',
		width:20*10,
		
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'returnedDate',
		hidden : true,
		header:'回款日期',
		width:20*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'returnedPerson-id',
		hidden : true,
		header:'回款销售id',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var returnedPersonCob =new Ext.form.TextField({
        allowBlank: false
	});
	returnedPersonCob.on('focus', function() {
		selectoperUserFun();
	});
	cm.push({
		dataIndex:'returnedPerson-name',
		hidden : true,
		header:'回款销售',
		width:20*10,
//		editor : returnedPersonCob
	});
	cm.push({
		dataIndex:'returnedScale',
		hidden : true,
		header:'回款比例',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/project/showProjectObjectivesListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="订单信息";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/project/delProjectObjectives.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				projectObjectivesGrid.getStore().commitChanges();
				projectObjectivesGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//			text : biolims.common.selectRelevantTable,
//				handler : selecttProjectDialogFun
//		});
//	opts.tbar.push({
//		text : biolims.common.batchUploadCSV,
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),biolims.common.batchUpload,null,{
//				"Confirm":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = projectObjectivesGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
							projectObjectivesGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
//	opts.tbar.push({
//		text : '选择销售',
//		handler : selectoperUserFun
//	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	projectObjectivesGrid=gridEditTable("projectObjectivesdiv",cols,loadParam,opts);
	$("#projectObjectivesdiv").data("projectObjectivesGrid", projectObjectivesGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selecttProjectFun(){
	var win = Ext.getCmp('selecttProject');
	if (win) {win.close();}
	var selecttProject= new Ext.Window({
	id:'selecttProject',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selecttProject.close(); }  }]  }) });  
    selecttProject.show(); }
	function settProject(rec){
		var gridGrid = $("#projectObjectivesdiv").data("projectObjectivesGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('tProject-id',rec.get('id'));
			obj.set('tProject-name',rec.get('name'));
		});
		var win = Ext.getCmp('selecttProject')
		if(win){
			win.close();
		}
	}
	function selecttProjectDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/ProjectSelect.action?flag=tProject";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						seltProjectVal(this);
				}
			}, true, option);
		}
	var seltProjectVal = function(win) {
		var operGrid = projectDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#projectObjectivesdiv").data("projectObjectivesGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('tProject-id',rec.get('id'));
				obj.set('tProject-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};


	function selectoperUserFun(){
		var win = Ext.getCmp('selectoperUser');
		if (win) {win.close();}
		var selectoperUser= new Ext.Window({
		id:'selectoperUser',modal:true,title:'选择销售',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=returnedPerson' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
			 selectoperUser.close(); }  }]  });     selectoperUser.show(); }
		function setreturnedPerson(id,name){
			var gridGrid = projectObjectivesGrid;
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('returnedPerson-id',id);
				obj.set('returnedPerson-name',name);
			});
			var win = Ext.getCmp('selectoperUser')
			if(win){
				win.close();
			}
		}
		
