var crmCustomerTable;
$(function() {
	var fields=[];
	fields.push({
		"data":"id",
		"title":biolims.common.projectId,
	});
	fields.push({
		"data":'name',
		"title":biolims.common.projectName,
	});
	fields.push({
		"data":'mainProject-id',
		"title":biolims.sample.customerId,
	});
	
	fields.push({
		"data":'mainProject-name',
		"title":biolims.sample.customerName,
	});
	
	fields.push({
		"data":'crmDoctor-id',
		"title":biolims.sample.crmDoctorId,
	});
	
	fields.push({
		"data":'crmDoctor-name',
		"title":biolims.sample.crmDoctorName,
	});
	    fields.push({
		"data":'type-name',
		"title":biolims.master.projectType,
	});
	
	    fields.push({
		"data":'dutyUser-name',
		"title":biolims.master.personName,
	});
	
	    fields.push({
		"data":'note',
		"title":biolims.master.projectNote,
	});
	    fields.push({
		"data":'state-name',
		"title":biolims.master.note,
	});
	
	    fields.push({
		"data":'planStartDate',
		"title":biolims.common.startTime,
	});
	
	    fields.push({
		"data":'factStartDate',
		"title":biolims.master.expectedDeliveryDate,
	});
	
	    fields.push({
		"data":'planEndDate',
		"title":biolims.master.actualDeliveryDate,
	});
	
	    fields.push({
		"data":'sampleCodeNum',
		"title":biolims.common.associatedSampleNumber,
	});
	
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "Project"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						fields.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/crm/project/showProjectListJson.action",
	 fields, null);
	crmCustomerTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(crmCustomerTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
	"/crm/project/toEditProject.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
	"/crm/project/toEditProject.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/crm/project/toViewProject.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];

	fields.push({
	    "type":"input",
		"searchName":"id",
		"txt":biolims.common.projectId,
	});
	fields.push({
	    "type":"input",
		"searchName":"name",
		"txt":biolims.common.projectName,
	});
	   fields.push({
		    "searchName":"mainProject.name",
			"type":"input",
			"txt":biolims.sample.customerName,
		});
//	fields.push({
//			"txt": "创建日期(Start)",
//			"type": "dataTime",
//			"searchName": "createDate##@@##1",
//			"mark": "s##@@##",
//		},
//		{
//			"txt": "创建日期(End)",
//			"type": "dataTime",
//			"mark": "e##@@##",
//			"searchName": "createDate##@@##2"
//		});
//	   fields.push({
//		    "searchName":"ks",
//			"type":"input",
//			"txt":biolims.crmCustomer.ks
//		});
//	fields.push({
//	    "type":"input",
//		"searchName":"district.id",
//		"txt":"所属片区ID"
//	});
//	fields.push({
//	    "type":"input",
//		"searchName":"district.name",
//		"txt":biolims.crmCustomer.district
//	});
//	   fields.push({
//		    "searchName":"name",
//			"type":"input",
//			"txt":biolims.crmCustomer.name
//		});
//	fields.push({
//	    "type":"input",
//		"searchName":"type.id",
//		"txt":"分类ID"
//	});
//	fields.push({
//	    "type":"input",
//		"searchName":"type.name",
//		"txt":biolims.crmCustomer.type
//	});
//	   fields.push({
//		    "searchName":"street",
//			"type":"input",
//			"txt":biolims.crmCustomer.street
//		});
//	   fields.push({
//		    "searchName":"postcode",
//			"type":"input",
//			"txt":biolims.crmCustomer.postcode
//		});
//	   fields.push({
//		    "searchName":"email",
//			"type":"input",
//			"txt":biolims.crmCustomer.email
//		});
//	   fields.push({
//		    "searchName":"yjly",
//			"type":"input",
//			"txt":biolims.crmCustomer.yjly
//		});
//	fields.push({
//	    "type":"input",
//		"searchName":"dutyManId.id",
//		"txt":"负责人ID"
//	});
//	fields.push({
//	    "type":"input",
//		"searchName":"dutyManId.name",
//		"txt":biolims.crmCustomer.dutyManId
//	});
//	fields.push({
//	    "type":"input",
//		"searchName":"state.id",
//		"txt":"状态ID"
//	});
//	fields.push({
//	    "type":"input",
//		"searchName":"state.name",
//		"txt":biolims.crmCustomer.state
//	});
	
	fields.push({
		"type":"table",
		"table":crmCustomerTable
	});
	return fields;
}
