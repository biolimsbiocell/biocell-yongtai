$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})
function list() {
	window.location = window.ctx + '/crm/project/showProjectList.action';
}

function add() {
	window.location = window.ctx + "/crm/project/toEditProject.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	// window.location = window.ctx +
	// '/project/project/toEditProject.action?id=' + id;
	window.open('', window.ctx + '/crm/project/toEditProject.action?id=' + id);
}

function newSave() {

	if (checkSubmit() == false) {
		return false;

	}
	var myMask = new Ext.LoadMask(Ext.getBody(), {
		msg : biolims.common.pleaseWait
	});
	myMask.show();
	Ext.Ajax.request({
		url : window.ctx + '/common/hasId.action',
		method : 'POST',
		params : {
			id : $("#project_id").val(),
			obj : 'Project'
		},
		success : function(response) {
			var respText = Ext.util.JSON.decode(response.responseText);
			myMask.hide();
			if (respText.message == '') {
				save();
			} else {
				message(respText.message);

			}

		},
		failure : function(response) {
		}
	});
}

function save() {
	if (checkSubmit() == true) {
		var myMask = new Ext.LoadMask(Ext.getBody(), {
			msg : biolims.common.pleaseWait
		});
		myMask.show();
//		 var projectObjectivesDivData = $("#projectObjectivesdiv").data("projectObjectivesGrid");
//			document.getElementById('projectObjectivesJson').value = commonGetModifyRecords(projectObjectivesDivData);
//		 var applicationTypeActionLogDivData = $("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid");
//			document.getElementById('applicationTypeActionLogJson').value = commonGetModifyRecords(applicationTypeActionLogDivData);
//		    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		
		form1.action = window.ctx + "/crm/project/save.action";
		form1.submit();
		// Ext.MessageBox.hide();
		// Ext.MessageBox.alert('Status', 'Changes saved successfully.', '');
	} else {
		return false;
	}

}

function checkSubmit() {

	var mess = "";
	var fs = [ "project_id", "project_name","project_mainProject_id","project.dutyUser.id" ];
	var nsc = [ biolims.master.projectIdIsEmpty, biolims.master.projectNameIsEmpty,biolims.master.customerIsEmpty,biolims.master.projectLeaderIsEmpty];

	mess = commonFieldsNotNullVerifyById(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;

}
Ext.onReady(function() {
	Ext.QuickTips.init();
	
	new Ext.form.NumberField({
		decimalPrecision : 2,
		selectOnFocus : true,
		applyTo : 'project.yjsjd'
	});
});

function viewProject() {
	if (trim(document.getElementById('project_mainProject_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/crm/project/toViewProject.action?id='
				+ document.getElementById('project_mainProject_id').value);

	}
}
function viewProjectBudget() {
	if (trim(document.getElementById('project_budget_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/project/budget/toViewBudget.action?id='
				+ document.getElementById('project_budget_id').value);

	}
}
function viewContract() {
	if (trim(document.getElementById('project_contract_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/contract/collectionContract/toViewContract.action?id='
				+ document.getElementById('project_contract_id').value);

	}
}
function viewControl() {
	if (trim(document.getElementById('project_contract_id').value) == '') {
		message(biolims.common.selectRecord);
	} else {
		openDialog(window.ctx + '/contract/collectionContract/toViewContract.action?id='
				+ document.getElementById('project_contract_id').value);

	}
}
$(function() {
	load("/crm/project/showApplicationTypeActionLogZJList.action", {
		id : $("#project_id").val()
	}, "#applicationTypeActionLogZJpage");//质检任务单
	load("/crm/project/showProjectObjectivesJKList.action", {
		id : $("#project_id").val()
	}, "#projectObjectivesJKpage");//建库任务单
	load("/crm/project/showProjectObjectivesSJList.action", {
		id : $("#project_id").val()
	}, "#projectObjectivesSJpage");//上机任务单
	

//load("/crm/project/showApplicationTypeActionLogList.action", {
//	id : $("#project_id").val()
//}, "#applicationTypeActionLogpage");
//load("/crm/project/showProjectObjectivesList.action", {
//	id : $("#project_id").val()
//}, "#projectObjectivespage");




});