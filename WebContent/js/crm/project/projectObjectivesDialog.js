$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "id",
		"visible": false
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.master.name,
		"visible": false
	});
	colOpts.push({
		"data" : "orderCode",
		"title" : "订单编号",
		"visible": false
	});
	var tbarOpts = [];
	var addCrmPatientOptions = table(false, null,
			'/crm/project/showProjectListNewJson.action',
			colOpts, tbarOpts)
	var CrmPatientTable = renderData($("#addcrmProjectOrderTable"), addCrmPatientOptions);
	$("#addcrmProjectOrderTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addcrmProjectOrderTable_wrapper .dt-buttons").empty();
				$('#addcrmProjectOrderTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addcrmProjectOrderTable tbody tr");
			CrmPatientTable.ajax.reload();
			CrmPatientTable.on('draw', function() {
				trs = $("#addcrmProjectOrderTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})

