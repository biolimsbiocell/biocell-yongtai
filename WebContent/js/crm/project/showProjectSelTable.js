$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "项目编号",
	});
	colOpts.push({
		"data": "name",
		"title": "项目名称",
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/crm/project/showProjectJson.action',colOpts , tbarOpts)
		var projectSelTable = renderData($("#addProject"), options);
	$("#addProject").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addProject_wrapper .dt-buttons").empty();
		$('#addProject_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addProject tbody tr");
	projectSelTable.ajax.reload();
	projectSelTable.on('draw', function() {
		trs = $("#addProject tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

