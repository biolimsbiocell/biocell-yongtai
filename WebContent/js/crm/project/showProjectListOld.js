var gridGrid;
Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
			Ext.grid.RowNumberer = Ext.extend(Ext.grid.RowNumberer, {
				width : 30,
				renderer : function(value, cellmeta, record, rowIndex,
						columnIndex, store) {
					return store.lastOptions.params.start + rowIndex + 1;
				}
			});
			var store = new Ext.data.JsonStore({
				root : 'results',
				totalProperty : 'total',
				remoteSort : true,
				fields : [ {
					name : 'id',
					type : 'string'
				}, {
					name : 'name',
					type : 'string'
				}, {
					name : 'mainProject-id',
					type : 'string'
				}, {
					name : 'mainProject-name',
					type : 'string'
				}, {
					name : 'crmDoctor-id',
					type : 'string'
				}, {
					name : 'crmDoctor-name',
					type : 'string'
				}, {
					name : 'type-name',
					type : 'string'
				}, {
					name : 'dutyUser-name',
					type : 'string'
				}, {
					name : 'note',
					type : 'string'
				}, {
					name : 'state-name',
					type : 'string'
				}, {
					name : 'planStartDate',
					type : 'string'
				}, {
					name : 'factStartDate',
					type : 'string'
				}, {
					name : 'planEndDate',
					type : 'string'
				},
				{
					name : 'sampleCodeNum',
					type : 'string'
				}],
				proxy : new Ext.data.HttpProxy({url: ctx+'/crm/project/showProjectListJson.action',method: 'POST'})
});
gridGrid = new Ext.grid.GridPanel({
						autoWidth : true,
						id : gridGrid,
						titleCollapse : true,
						autoScroll : true,
						height : document.body.clientHeight - 35,
						iconCls : 'icon-grid',
						title : biolims.master.projectManagement,
						collapsible : true,
						store : store,
						columnLines : true,
						trackMouseOver : true,
						loadMask : true,
						colModel : new Ext.ux.grid.LockingColumnModel([
								new Ext.grid.RowNumberer(), {
									dataIndex : 'id',
									header : biolims.common.projectId,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'name',
									header : biolims.common.projectName,
									width : 200,
									sortable : true
								}, {
									dataIndex : 'mainProject-id',
									header : biolims.sample.customerId,
									width : 120,
									sortable : false
								}, {
									dataIndex : 'mainProject-name',
									header : biolims.sample.customerName,
									width : 200,
									sortable : true
								}, {
									dataIndex : 'crmDoctor-id',
									header : biolims.sample.crmDoctorId,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'crmDoctor-name',
									header : biolims.sample.crmDoctorName,
									width : 150,
									sortable : true
								}, {
									dataIndex : 'type-name',
									header : biolims.master.projectType,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'dutyUser-name',
									header : biolims.master.personName,
									width : 75,
									sortable : true
								}, {
									dataIndex : 'note',
									header : biolims.master.projectNote,
									width : 200,
									sortable : true
								}, {
									dataIndex : 'state-name',
									header : biolims.master.note,
									width : 100,
									sortable : true
								}, {
									dataIndex : 'planStartDate',
									header : biolims.common.startTime,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'factStartDate',
									header : biolims.master.expectedDeliveryDate,
									width : 120,
									sortable : true
								}, {
									dataIndex : 'planEndDate',
									header : biolims.master.actualDeliveryDate,
									width : 120,
									sortable : true
								} ,{
									dataIndex : 'sampleCodeNum',
									header : biolims.common.associatedSampleNumber,
									width : 120,
									sortable : true
								} 
								
								
								]),
						stripeRows : true,
						view : new Ext.ux.grid.LockingGridView(),
						viewConfig : {
							forceFit : true,
							enableRowBody : false,
showPreview:false
},
bbar: new Ext.PagingToolbar({
									id : 'bbarId',
									pageSize : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
											: 1),
									store : store,
									displayInfo : true,
									displayMsg : biolims.common.displayMsg,
									beforePageText : biolims.common.page,
									afterPageText : biolims.common.afterPageText,
									emptyMsg : biolims.common.noData,
									plugins : new Ext.ui.plugins.ComboPageSize(
											{
												addToItem : false,
												prefixText : biolims.common.show,
												postfixText : biolims.common.entris
											})
								})
					});
			gridGrid.render('grid');
			gridGrid.getSelectionModel().on('rowselect',
					function(sm, rowIdx, r) {
						document.getElementById("id").value = r.data.id;
					});
			gridGrid.on('rowdblclick', function() {
				var record = gridGrid.getSelectionModel().getSelections();
				document.getElementById("id").value = record[0].data.id;
				edit();
			});
			store.on('beforeload', function() {
				store.baseParams = {
					data : document.getElementById('extJsonDataString').value
				};
			});
			store
					.load({
						params : {
							start : 0,
							limit : parseInt((document.body.clientHeight - 35) > 0 ? (document.body.clientHeight - 35) / 24
									: 1)
						}
					});
		});
function exportexcel() {
	gridGrid.title = biolims.common.exportList;
	var vExportContent = gridGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function normalsearch(gridName) {
	var limit = parseInt((parent.document.body.clientHeight - 250) > 0 ? (parent.document.body.clientHeight - 250) / 25
			: 1);
	if (trim(document.getElementById("limitNum").value) != '') {
		limit = document.getElementById("limitNum").value;

	}

	var fields = [ 'id', 'name' ];
	var fieldsValue = [ document.getElementById('id').value,
			document.getElementById('name').value ];
	searchAllGrid(gridName, fields, fieldsValue, 0, limit);

}

function list() {
	window.location = window.ctx + '/crm/project/showProjectList.action';
}

function add() {
	window.location = window.ctx + "/crm/project/toEditProject.action";
}

function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/crm/project/toEditProject.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	// window.location = window.ctx +
	// '/project/project/toViewProject.action?id=' + id;
	openDialog(window.ctx + '/crm/project/toViewProject.action?id=' + id);
}

$(function() {
	searchworkflow("stateName", "Project", null);
});

Ext.onReady(function() {
	Ext.QuickTips.init();
	new Ext.form.DateField({

		applyTo : 'queryStartDate'
	});
	$("#queryStartDate").css("width", "80px");
});
Ext.onReady(function() {
	Ext.QuickTips.init();
	new Ext.form.DateField({

		applyTo : 'queryEndDate'
	});
	$("#queryEndDate").css("width", "80px");
});

var genderstore = new Ext.data.ArrayStore({
	fields : [ 'id', 'name' ],
	data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
});

var genderComboxFun = new Ext.form.ComboBox({
	store : genderstore,
	displayField : 'name',
	valueField : 'id',
	typeAhead : true,
	mode : 'local',
	forceSelection : true,
	triggerAction : 'all',
	emptyText : '',
	selectOnFocus : true
});

$(function() {

	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 542;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, false, option);
	});

});