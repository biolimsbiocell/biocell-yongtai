function addLoadEvent() {
	var oldonload = window.onload;
	
	$("#project_planStartDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
	$("#project_factStartDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
	$("#project_planEndDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	$("#btn_submit").hide();
	
	fieldCustomFun();
	
//	$("#crmPatient_createDate").datepicker({
//		language: "zh-TW",
//		autoclose: false, //选中之后自动隐藏日期选择框
//		format: "yyyy-mm-dd" //日期格式，详见 
//	});
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	if(handlemethod == "modify") {
		$("#project_id").prop("readonly", "readonly");
	}

}
var mainFileInput;
function fileUp() {
	if(!$("#project_id").val()){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('1', 'project', $("#project_id").val());
	mainFileInput.off("fileuploaded");
}
function fileUp2() {
	$("#uploadFile").modal("show");
	mainFileInput = fileInput('2', 'project', $("#project_id").val());
	mainFileInput.off("fileuploaded");
}
function fileView() {
	top.layer.open({
		title: "附件",
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=project&id=" + $("#project_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index);
		}
	});
}
function fileView2() {
	top.layer.open({
		title: "附件",
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=2&modelType=project&id=" + $("#project_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index);
		}
	});
}
addLoadEvent();
$(function() {
//	if ($("#isSaleManage").val() == "true" || $("#filingUser").val() == $("#crmPatient_createUser").val()) {
//		for ( var i = 1; i <= 30; i++) {
//			document.getElementById("xing" + i).style.display = "inline";
//		}
//	}
});

function jiazaiOrder() {
	var id = $("#crmPatient_id").val();
	if ($("#first").val() == null || $("#first").val() == "") {
		
		window.location = window.ctx + '/crm/customer/patient/copyCrmPatient.action?id=' + $("#crmPatient_id").val();
	}
	if ($("#first2").val() == null || $("#first2").val() == "") {
		
	}
}

function add() {
	window.location = window.ctx + "/crm/project/toEditProject.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/crm/project/showProjectList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {

	save();
});
function save() {
	//自定义字段
	//拼自定义字段儿（实验记录）
	var inputs = $("#fieldItemDiv input");
	var options = $("#fieldItemDiv option");
	var contentData = {};
	var checkboxArr = [];
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	$("#fieldItemDiv .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	document.getElementById("fieldContent").value = JSON.stringify(contentData);
	
	var changeLog = "项目管理:";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	document.getElementById("changeLog").value = changeLog;
	/*if(checkSubmit() == true) {
//		document.getElementById('crmLinkManItemJson').value = savecrmLinkManItemjson($("#crmLinkManItemTable"));
//		console.log($("#crmLinkManItemJson").val());
		form1.action = window.ctx +
			"/crm/project/save.action";
		form1.submit();
	}*/
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "modify"&&checkSubmit() == true) {
	//		document.getElementById('crmLinkManItemJson').value = savecrmLinkManItemjson($("#crmLinkManItemTable"));
	//		console.log($("#crmLinkManItemJson").val());
		top.layer.load(4, {shade:0.3}); 
			$("#form1").attr("action", "/crm/project/save.action");
			$("#form1").submit();
			top.layer.closeAll();
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#project_id").val(),
				obj: 'Project'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					top.layer.load(4, {shade:0.3}); 
					$("#form1").attr("action", "/crm/project/save.action");
					$("#form1").submit();
					top.layer.closeAll();
				}
			}
		});
	}
}
function editCopy() {
	window.location = window.ctx + '/crm/customer/patient/copyCrmPatient.action?id=' + $("#crmPatient_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#crmPatient_id").val() + "&tableId=crmPatient");
}
function checkSubmit() {
//	if($("#crmPatient_id").val() == null || $("#crmPatient_id").val() == "") {
//		top.layer.msg(biolims.master.patientIdIsEmpty);
//		return false;
//	};
	return true;
}
$(function() {
	var myDate = new Date();
});
function sfzVerify() {
	$.post("/crm/customer/patient/sfzVerify.action", {
		"data" : $("#crmPatient_sfz").val()
	}, function(data) {
		if (data.message == false) {
			alert(biolims.master.dataExisting);
		}
	}, "json");
}
function showCustomer() {
	var win = Ext.getCmp('showCustomer');
	if (win) {
		win.close();
	}
	var showCustomer = new Ext.Window(
			{
				id : 'showCustomer',
				modal : true,
				title : biolims.master.selectHospital,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerFun1' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showCustomer.close();
					}
				} ]
			});
	showCustomer.show();
}
function setCrmCustomerFun1(rec) {
	document.getElementById("sbr_customer_id").value = rec.get('id');
	document.getElementById("sbr_customer_name").value = rec.get('name');
	var win = Ext.getCmp('showCustomer');
	if (win) {
		win.close();
	}
}

function showkindFun() {
	var win = Ext.getCmp('showkindFun');
	if (win) {
		win.close();
	}
	var showkindFun = new Ext.Window(
			{
				id : 'showkindFun',
				modal : true,
				title : biolims.master.selectDepartment,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=ks' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showkindFun.close();
					}
				} ]
			});
	showkindFun.show();
}

function setks(id, name) {
	document.getElementById("sbr_ks_id").value = id;
	document.getElementById("sbr_ks_name").value = name;
	var win = Ext.getCmp('showkindFun');
	if (win) {
		win.close();
	}
}

function showCustomerDoctor() {
	var win = Ext.getCmp('showCustomerDoctor');
	if (win) {
		win.close();
	}
	var showCustomerDoctor = new Ext.Window(
			{
				id : 'showCustomerDoctor',
				modal : true,
				title : biolims.master.selectDoctor,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerDoctorFun2' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						showCustomerDoctor.close();
					}
				} ]
			});
	showCustomerDoctor.show();
}
function setCrmCustomerDoctorFun2(rec) {
	// records = inItemGrid.getSelectRecord();
	// $.each(records, function(i, obj) {
	// obj.set("customerDoctor-id", rec.get('id'));
	// obj.set("customerDoctor-name", rec.get('name'));
	// });
	document.getElementById("sbr_customerDoctor_id").value = rec.get('id');
	document.getElementById("sbr_customerDoctor_name").value = rec.get('name');
	var win = Ext.getCmp('showCustomerDoctor')
	if (win) {
		win.close();
	}
}





//查询类型=================================================================================
/**
 * 选择肿瘤类型
 */

function WeiChatCancerTypeFun() {
	var win = Ext.getCmp('WeiChatCancerTypeFun');
	if (win) {
		win.close();
	}
	var WeiChatCancerTypeFun = new Ext.Window(
			{
				id : 'WeiChatCancerTypeFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sample/sampleCancerTemp/weiChatCancerTypeSelect.action?flag=WeiChatCancerTypeFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						WeiChatCancerTypeFun.close();
					}
				} ]
			});
	WeiChatCancerTypeFun.show();
}
/*
 * ajax 异步
 * */
function setWeiChatCancerTypeFun(rec) {

document.getElementById("crmPatient_cancerType_Id").value = rec.get("id");
document.getElementById("crmPatient_cancerType_cancerTypeName").value = rec.get("cancerTypeName");
	var win = Ext.getCmp('WeiChatCancerTypeFun');
	if (win) {
		win.close();
	}
}

/**
 * 选择肿瘤类型1
 */

function WeiChatCancerTypeOneFun() {
	if($("#crmPatient_cancerType_Id").val()==""||$("#crmPatient_cancerType_Id").val()==null){
		message(biolims.sample.pleaseSelectTumorType);
		Ext.getCmp('WeiChatCancerTypeOneFun').close();
	}
	var win = Ext.getCmp('WeiChatCancerTypeOneFun');
	if (win) {
		win.close();
	}
	var WeiChatCancerTypeOneFun = new Ext.Window(
			{
				id : 'WeiChatCancerTypeOneFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/sampleCancerTemp/weiChatCancerTypeSelectOne.action?code="+$("#crmPatient_cancerType_Id").val()+"&flag=WeiChatCancerTypeOneFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						WeiChatCancerTypeOneFun.close();
					}
				} ]
			});
	WeiChatCancerTypeOneFun.show();
}
/*
 * 
 * */
function setWeiChatCancerTypeOneFun(rec) {

document.getElementById("crmPatient_cancerTypeSeedOne_Id").value = rec.get("id");
document.getElementById("crmPatient_cancerTypeSeedOne_cancerTypeName").value = rec.get("cancerTypeName");
	var win = Ext.getCmp('WeiChatCancerTypeOneFun');
	if (win) {
		win.close();
	}
}


/**
 * 选择肿瘤类型2
 */

function WeiChatCancerTypeTwoFun() {
	if($("#crmPatient_cancerType_Id").val()==""||$("#crmPatient_cancerType_Id").val()==null){
		message(biolims.sample.pleaseSelectTumorType);
		Ext.getCmp('WeiChatCancerTypeTwoFun').close();
	}
	var win = Ext.getCmp('WeiChatCancerTypeTwoFun');
	if (win) {
		win.close();
	}
	var WeiChatCancerTypeTwoFun = new Ext.Window(
			{
				id : 'WeiChatCancerTypeTwoFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 700,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/sample/sampleCancerTemp/weiChatCancerTypeSelectTwo.action?code="+$("#crmPatient_cancerType_Id").val()+"&flag=WeiChatCancerTypeTwoFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						WeiChatCancerTypeTwoFun.close();
					}
				} ]
			});
	WeiChatCancerTypeTwoFun.show();
}
/*
 * 
 * */
function setWeiChatCancerTypeTwoFun(rec) {

document.getElementById("crmPatient_cancerTypeSeedTwo_Id").value = rec.get("id");
document.getElementById("crmPatient_cancerTypeSeedTwo_cancerTypeName").value = rec.get("cancerTypeName");
	var win = Ext.getCmp('WeiChatCancerTypeTwoFun');
	if (win) {
		win.close();
	}
}

//医疗机构
function showHos() {
	top.layer.open({
		title: biolims.user.selectionUnit,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/customer/customer/crmCustomerSelectTable.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#project_mainProject_id").val(id);
			$("#project_mainProject_name").val(name);
		},
	})

}
function showinspectionDepartment() {
	top.layer.open({
		title: "选择科室",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=ks", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#crmPatient_ks_id").val(id)
			$("#crmPatient_ks_name").val(name)
		},
	})
}

//送检医院与主治医生添加关联性
function showDoctors() {

	top.layer.open({
		title: biolims.user.selectCustomers,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/doctor/showDialogCrmPatientTable.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(
				0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(1).text();
			var mobile = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(2).text();
			top.layer.close(index)
			$("#project_crmDoctor_id").val(id)
			$("#project_crmDoctor_name").val(name)
		},
	})

}

//选择项目负责人
function showconfirmUser() {
	top.layer.open({
		title: biolims.user.selectionProjectLeader,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=XXGL", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#project_dutyUser_id").val(id);
			$("#project_dutyUser_name").val(name);
		},
	})
}

//选择项目销售
function showXS() {
	top.layer.open({
		title: biolims.user.selectProjectSales,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=XXGL", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#project_person_id").val(id);
			$("#project_person_name").val(name);
		},
	})
}

//选择项目类型
function showXMLX() {
	top.layer.open({
		title: biolims.user.selectProjectType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=xmlx", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#project_type_id").val(id)
			$("#project_type_name").val(name)
		},
	})
}
//选择状态
function showZT() {
	top.layer.open({
		title: biolims.user.selectedState,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/state/dicStateSelectTable.action?flag=project", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicStateTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#project_state_id").val(id);
			$("#project_state_name").val(name);
		},
	})
}

//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

	//查找自定义列表的订单模块的此检测项目的相关内容
	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		data: {
			moduleValue: "Project"
		},
		async: false,
		success: function(data) {
			var objValue = JSON.parse(data);
			if(objValue.success) {
				$.each(objValue.data, function(i, n) {
					var inputs = '';
					var disabled = n.readOnly ? ' ' : "disabled";
					var defaultValue = n.defaultValue ? n.defaultValue : ' ';
					if(n.fieldType == "checkbox") {
						var checkboxs = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
					} else if(n.fieldType == "radio") {
						var options = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
					} else if(n.fieldType == "date") {
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
					} else {
						if(n.isRequired!="false"){
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '<img class="requiredimage" src="/images/required.gif" /></span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}else{
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}
					}

					$("#fieldItemDiv").append(inputs);
				});

			} else {
				top.layer.msg(biolims.customList.updateFailed);
			}
		}
	});

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
						inp.setAttribute("changelog", inp.value);
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
							inp.setAttribute("changelog", inp.value);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}