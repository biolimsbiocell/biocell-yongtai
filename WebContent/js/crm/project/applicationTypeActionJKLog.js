var applicationTypeActionLogJKGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'taskid',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({//进读
		name:'advance',
		type:"string"
	});
	   fields.push({
		name:'accomplispNumber',//完成数量
		type:"string"
	});
	   fields.push({
		name:'unaccomplispNumber',//未完成数量
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'taskid',
		header:biolims.common.orderId,
		width:20*10
	});
	cm.push({
		dataIndex:'name',
		header:biolims.wk.sequenceBillName,
		width:20*10
	});
	cm.push({
		dataIndex:'advance',
		header:biolims.sample.taskFlag
	});
	cm.push({
		dataIndex:'accomplispNumber',
		header:biolims.common.sampleCompletionNum
	});
	cm.push({
		dataIndex:'unaccomplispNumber',
		header:biolims.common.sampleNotCompletionNum,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/project/showApplicationTypeActionLogListJKJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.master.processRecords;
	opts.height =  document.body.clientHeight-250;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/crm/project/delApplicationTypeActionLog.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				scpProToGrid.getStore().commitChanges();
//				scpProToGrid.getStore().reload();
//				message(biolims.common.deleteSuccess);
//			} else {
//				message(biolims.common.deleteFailed);
//			}
//		}, null);
//	};
//	opts.tbar.push({
//			text : biolims.master.selectOper,
//			handler : selectoperUserFun
//		});
//	opts.tbar.push({
//		text : biolims.common.editableColAppear,
//		handler : null
//	});
//	opts.tbar.push({
//		text : biolims.common.uncheck,
//		handler : null
//	});
	applicationTypeActionLogJKGrid=gridEditTable("applicationTypeActionLogJKdiv",cols,loadParam,opts);
//	$("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid", applicationTypeActionLogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectoperUserFun(){
	var win = Ext.getCmp('selectoperUser');
	if (win) {win.close();}
	var selectoperUser= new Ext.Window({
	id:'selectoperUser',modal:true,title:biolims.master.selectOper,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=operUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectoperUser.close(); }  }]  });     selectoperUser.show(); }
	function setoperUser(id,name){
		var gridGrid = $("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('operUser-id',id);
			obj.set('operUser-name',name);
		});
		var win = Ext.getCmp('selectoperUser')
		if(win){
			win.close();
		}
	}
	
