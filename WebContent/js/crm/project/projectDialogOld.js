var projectDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
			name:'code',
			type:"string"
		});
	    fields.push({
	    	name : 'mainProject-id',
			type : 'string'
	});
	    fields.push({
	    	name : 'mainProject-name',
			type : 'string'
	});   
	    fields.push({
	    	name : 'crmDoctor-id',
			type : 'string'
	});
	    fields.push({
	    	name : 'crmDoctor-name',
			type : 'string'
	});
	    
	    fields.push({
	    	name : 'type-name',
			type : 'string'
		});
		    fields.push({
		    	name : 'dutyUser-name',
				type : 'string'
		});
	    fields.push({
	    	name : 'note',
			type : 'string'
	});
	    fields.push({
	    	name : 'state-name',
			type : 'string'
		});
	    
	    
	    
	    fields.push({
	    	name : 'planStartDate',
			type : 'string'
		});
	    fields.push({
	    	name : 'factStartDate',
			type : 'string'
		});
	    fields.push({
	    	name : 'planEndDate',
			type : 'string'
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex : 'id',
		header : "项目编号",
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header :"项目名称",
		width : 200,
		sortable : true
	});
//	cm.push({
//		dataIndex : 'code',
//		header : "样本编号",
//		width : 200,
//		sortable : true
//	});
	cm.push({
		dataIndex : 'mainProject-id',
		header : "客户编号",
		width : 120,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'mainProject-name',
		header :"客户名称",
		width : 200,
		hidden : false,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmDoctor-id',
		header : "医生编号",
		width : 120,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmDoctor-name',
		header : "医生名称",
		width : 150,
		sortable : true
	});
//	cm.push({
//		dataIndex : 'type-name',
//		header : biolims.master.projectType,
//		width : 120,
//		sortable : true
//	});
//	cm.push({
//		dataIndex : 'dutyUser-name',
//		header : biolims.master.personName,
//		width : 75,
//		sortable : true
//	});
	cm.push({
		dataIndex : 'note',
		header : biolims.master.projectNote,
		width : 200,
		sortable : true
	});
	cm.push({
		dataIndex : 'state-name',
		header : biolims.master.note,
		width : 100,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'planStartDate',
		header : biolims.common.startTime,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'factStartDate',
		header : biolims.master.expectedDeliveryDate,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'planEndDate',
		header : biolims.master.actualDeliveryDate,
		width : 120,
		sortable : true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/project/selProjectListJson.action";
	var opts={};
	opts.title="选择项目";
	opts.width = document.body.clientWidth - 850;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		setDicProject();		
	};
	
	//projectDialogGrid=gridTable("show_dialog_project_div",cols,loadParam,opts);
	projectDialogGrid=gridTable("show_dialog_project_div",cols,loadParam,opts);
	$("#show_dialog_project_div").data("projectDialogGrid", projectDialogGrid);

});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(projectDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
