﻿var crmContrctCollectGrid;
$(function() {
	var cols = {};
	cols.sm = true;

	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'money',
		type : "string"
	});
	fields.push({
		name : 'billDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'billMoney',
		type : "string"
	});
	fields.push({
		name : 'billId',
		type : "string"
	});
	fields.push({
		name : 'num',
		type : "string"
	});
	fields.push({
		name : 'crmContrctPay-projectNode-id',
		type : "string"
	});
	fields.push({
		name : 'crmContrctPay-projectNode-progress',
		type : "string"
	});
	fields.push({
		name : 'remind',
		type : "string"
	});
	fields.push({
		name : 'brokerage',
		type : "string"
	});
	// fields.push({
	// name:'scaling',
	// type:"string"
	// });
	fields.push({
		name : 'receivedDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'planReceivedDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'request',
		type : "string"
	});
	fields.push({
		name : 'crmContrctPay-id',
		type : "string"
	});
	fields.push({
		name : 'crmContrctPay-name',
		type : "string"
	});
	fields.push({
		name : 'crmContract-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-name',
		type : "string"
	});
	fields.push({
		name : 'collectNum',
		type : "string"
	});
	fields.push({
		name : 'crmContract-periodsNum',
		type : "string"
	});
	fields.push({
		name : 'crmContract-manager-name',
		type : "string"
	});
	fields.push({
		name : 'crmContract-manager-id',
		type : "string"
	});
	fields.push({
		name : 'note2',
		type : "string"
	});
	fields.push({
		name : 'billState',
		type : "string"
	});
	fields.push({
		name : 'collectType',
		type : "string"
	});
	fields.push({
		name : 'collectDept',
		type : "string"
	});
	// *********
	fields.push({
		name : 'billDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'billMoney',
		type : "string"
	});
	fields.push({
		name : 'billCode',
		type : "string"
	});
	fields.push({
		name : 'details',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	cols.fields = fields;

	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编码',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'collectNum',
		header : '序号',
		hidden : true,
		width : 10 * 6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : '',
		hidden : false,
		header : '合同',
		width : 10 * 6,
		sortable : true,
		renderer : function(data, metadata, record, rowIndex, columnIndex,
				store) {
			// if(data=="0"||data=="开始"){
			return String.format('<a href=\"javascript:continueFun(\''
					+ rowIndex + '\')\">查看</a>');
			// }else{
			// return String.format('已完成','Mallity');
			// }
		}
	});
	cm.push({
		dataIndex : 'receivedDate',
		hidden : false,
		header : '回款日期',
		width : 15 * 6,
		sortable : true,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	var selectCollectType = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '银行转账'
			}, {
				id : '1',
				name : '支票'
			}, {
				id : '2',
				name : '现金'
			}, {
				id : '总计',
				name : '总计'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'collectType',
		hidden : false,
		header : '回款类型',
		width : 10 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectCollectType),
		editor : selectCollectType
	});
	cm.push({
		dataIndex : 'collectDept',
		hidden : false,
		header : '回款单位',
		whidth : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'money',
		hidden : false,
		header : '回款金额（元）',
		width : 15 * 10,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals : true
		// decimalPrecision:2
		})
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : '备注',
		whidth : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var selectBillState = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '未开'
			}, {
				id : '1',
				name : '已开'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'billState',
		hidden : false,
		header : '发票状态',
		width : 8 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectBillState),
		editor : selectBillState
	});
	/*
	 * cm.push({ dataIndex:'scaling', hidden : false, header:'收款比例（%）',
	 * width:20*6, editor : new Ext.form.NumberField({ allowDecimals:true
	 * //decimalPrecision:2 }) });
	 */
	/*
	 * cm.push({ dataIndex:'projectNode', hidden : false, header:'项目节点',
	 * width:25*6,
	 * 
	 * renderer: formatDate, editor: new Ext.form.DateField({format: 'Y-m-d'})
	 * });
	 */
	/*
	 * var remind = new Ext.form.ComboBox({ store : new Ext.data.JsonStore({
	 * fields : [ 'id', 'name' ], data : [ { id : '0', name : '开票申请' },{ id :
	 * '1', name : '催款' } ] }), displayField : 'name', valueField : 'id',
	 * typeAhead : true, mode : 'local', forceSelection : true, triggerAction :
	 * 'all', selectOnFocus : true }); cm.push({ dataIndex:'remind', hidden :
	 * false, header:'到达节点提醒', width:15*10, renderer:
	 * Ext.util.Format.comboRenderer(remind),editor: remind });
	 */

	/*
	 * cm.push({ dataIndex:'crmContrctPay-id', hidden : true, header:'对应回款计划ID',
	 * width:15*10, editor : new Ext.form.TextField({ allowBlank : true }) });
	 * cm.push({ dataIndex:'crmContrctPay-name', hidden : false,
	 * header:'对应回款计划', width:15*10 });
	 */
	/*
	 * cm.push({ dataIndex:'crmContrctPay-projectNode-id', hidden : true,
	 * header:'项目节点ID', width:15*10, editor : new Ext.form.TextField({
	 * allowBlank : true }) }); cm.push({
	 * dataIndex:'crmContrctPay-projectNode-progress', hidden : false,
	 * header:'项目节点', width:15*10 }); cm.push({ dataIndex:'billDate', hidden :
	 * false, header:'开票日期', width:25*6, renderer: formatDate, editor: new
	 * Ext.form.DateField({format: 'Y-m-d'}) }); cm.push({
	 * dataIndex:'billMoney', hidden : false, header:'开票金额（元）', width:20*6,
	 * editor : new Ext.form.NumberField({ allowDecimals:true,
	 * decimalPrecision:2 }) });
	 */
	cm.push({
		dataIndex : 'billId',
		hidden : false,
		header : '发票号',
		width : 40 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'brokerage',
		hidden : false,
		header : '经手人',
		width : 10 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*
	 * cm.push({ dataIndex:'num', hidden : false, header:'申请开票数量', width:20*6,
	 * 
	 * editor : new Ext.form.TextField({ allowBlank : true }) });
	 */
	/*
	 * cm.push({ dataIndex:'request', hidden : false, header:'开票要求', width:30*6,
	 * editor : new Ext.form.TextField({ allowBlank : true }) });
	 */

	/*
	 * cm.push({ dataIndex:'planReceivedDate', hidden : false, header:'计划回款日期',
	 * width:15*6,
	 * 
	 * renderer: formatDate, editor: new Ext.form.DateField({format: 'Y-m-d'})
	 * });
	 */
	cm.push({
		dataIndex : 'crmContract-name',
		hidden : false,
		header : '合同名称',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmContract-periodsNum',
		hidden : false,
		header : '合同期数',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmContract-manager-id',
		hidden : true,
		header : '客户经理ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmContract-manager-name',
		hidden : true,
		header : '客户经理',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note2',
		hidden : true,
		header : '备注',
		width : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-id',
		hidden : true,
		header : '相关主表ID',
		width : 15 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-name',
		hidden : true,
		header : '相关主表',
		width : 15 * 10
	});
	// ******
	cm.push({
		dataIndex : 'billDate',
		hidden : true,
		header : '票据开具日期',
		whidth : 20 * 10,
		sortable : true,
		renderer : formatDate,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'billMoney',
		hidden : true,
		header : '票据申请金额',
		whidth : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex : 'billCode',
		hidden : true,
		header : '票据抬头',
		whidth : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'details',
		hidden : true,
		header : '票据明细',
		whidth : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : '状态',
		whidth : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/contract/crmContract/showCrmContrctCollectListJson.action";
	var opts = {};
	opts.title = "回款管理";
	opts.height = document.body.clientHeight;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/crm/contract/crmContract/delCrmContrctCollect.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};

	if ($("#crmContract_state").val() == 1) {
		// alert("工作流状态已完成，页面不可编辑。");
	} else {
		/*
		 * opts.tbar.push({ text : '选择相关主表', handler : selectcrmContractFun });
		 */
		opts.tbar.push({
			text : '填加明细',
			iconCls : 'add',
			handler : function() {
				var ob = crmContrctCollectGrid.getStore().recordType;
				var p = new ob({});
				p.isNew = true;
				crmContrctCollectGrid.stopEditing();
				p.set("state", "0");
				crmContrctCollectGrid.getStore().insert(0, p);
				crmContrctCollectGrid.startEditing(0, 0);
			}
		});
		opts.tbar.push({
			text : '显示可编辑列',
			handler : null
		});
		opts.tbar.push({
			text : '取消选中',
			handler : null
		});
		opts.tbar.push({
			text : '选择发票',
			handler : selectcrmContrctBill
		});
		opts.tbar.push({
			text : "搜索",
			handler : function() {
				// if(checkSubmitState()==false){
				// return;
				// }
				var options = {};
				options.width = 600;
				options.height = 300;
				loadDialogPage($("#collect_data_div"), "搜索", null, {
					"确定" : function() {

						if (($("#startreceivedDate").val() != undefined)
								&& ($("#startreceivedDate").val() != '')) {
							var startbillDatestr = ">=##@@##"
									+ $("#startreceivedDate").val();
							$("#receivedDate1").val(startbillDatestr);
						}
						if (($("#endreceivedDate").val() != undefined)
								&& ($("#endreceivedDate").val() != '')) {
							var endbillDatestr = "<=##@@##"
									+ $("#endreceivedDate").val();
							$("#receivedDate2").val(endbillDatestr);
						}

						commonSearchAction(crmContrctCollectGrid);
						// var records =
						// esBreedDisposeItemGrid.getSelectRecord();
						// if (records && records.length > 0) {
						// var action = $("#action").val();
						// // birthDate = birthDate.replace(/-/g,"/");
						// // var date = new Date(birthDate);
						// esBreedDisposeItemGrid.stopEditing();
						// $.each(records, function(i, obj) {
						// obj.set("action", action);
						// });
						// esBreedDisposeItemGrid.startEditing(0, 0);
						// }else{
						// message("请选择数据！");
						// }
						$(this).dialog("close");
					},
					"清空" : function() {
						form_reset();

					}
				}, true, options);
			}
		});
		opts.tbar.push({
			text : '保存',
			handler : save
		});

		opts.tbar.push({
			text : '计算回款总和',
			handler : countPaymentsData
		});
		// opts.tbar.push({
		// text : '检查计算回款总和',
		// handler : CheckCountPaymentsData
		// });
		opts.tbar.push({
			text : "批量上传（CSV文件）",
			handler : function() {
				var options = {};
				options.width = 350;
				options.height = 200;
				loadDialogPage($("#bat_uploadcsv_div"), "批量上传", null, {
					"确定" : function() {
						goInExcelcsv();
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
		opts.tbar.push({
			text : '导出',
			handler : exportexcel
		});

	}

	function goInExcelcsv() {
		var file = document.getElementById("file-uploadcsv").files[0];
		var n = 0;
		var ob = crmContrctCollectGrid.getStore().recordType;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
				if (n > 0) {
					if (this[0]) {
						var p = new ob({});
						p.isNew = true;
						var o;
						o = 0 - 1;
						p.set("po.fieldName", this[o]);

						o = 1 - 1;
						p.set("po.fieldName", this[o]);

						o = 2 - 1;
						p.set("po.fieldName", this[o]);

						o = 3 - 1;
						p.set("po.fieldName", this[o]);

						o = 4 - 1;
						p.set("po.fieldName", this[o]);

						o = 5 - 1;
						p.set("po.fieldName", this[o]);

						o = 6 - 1;
						p.set("po.fieldName", this[o]);

						o = 7 - 1;
						p.set("po.fieldName", this[o]);

						o = 8 - 1;
						p.set("po.fieldName", this[o]);

						crmContrctCollectGrid.getStore().insert(0, p);
					}
				}
				n = n + 1;

			});
		};
	}

	crmContrctCollectGrid = gridEditTable("crmContrctCollectdiv", cols,
			loadParam, opts);
	$("#crmContrctCollectdiv").data("crmContrctCollectGrid",
			crmContrctCollectGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function exportexcel() {
	crmContrctCollectGrid.title = '导出列表';
	var vExportContent = crmContrctCollectGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

function selectcrmContractFun() {
	var win = Ext.getCmp('selectcrmContract');
	if (win) {
		win.close();
	}
	var selectcrmContract = new Ext.Window(
			{
				id : 'selectcrmContract',
				modal : true,
				title : '选择相关主表',
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/CrmContractSelect.action?flag=crmContract' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						selectcrmContract.close();
					}
				} ]
			});
	selectcrmContract.show();
}

function setcrmContract(id, name) {
	var gridGrid = $("#crmContrctCollectdiv").data("crmContrctCollectGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('crmContract-id', id);
		obj.set('crmContract-name', name);
	});
	var win = Ext.getCmp('selectcrmContract');
	if (win) {
		win.close();
	}
}
// + $("#crmContract_id").val() +

// 选择发票
function selectcrmContrctBill() {
	var gridGrid = crmContrctCollectGrid;
	var selRecords = gridGrid.getSelectionModel().getSelections();
	if (selRecords.length > 0) {
		var win = Ext.getCmp('selectcrmContrctPay');
		if (win) {
			win.close();
		}
		var selectcrmContrctPay = new Ext.Window(
				{
					id : 'selectcrmContrctPay',
					modal : true,
					title : '选择发票',
					layout : 'fit',
					width : 1200,
					height : 600,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"
										+ ctx
										+ "/crm/contract/crmContract/showCrmContrctBillDialogList.action"
										+ "' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : '关闭',
						handler : function() {
							selectcrmContrctPay.close();
						}
					} ]
				});
		selectcrmContrctPay.show();
	} else {
		message("请先勾选数据记录。");
		return;
	}
}
// function setCrmContractPayFun(rec){
// var gridGrid = crmContrctCollectGrid;
// var selRecords = gridGrid.getSelectionModel().getSelections();
// $.each(selRecords, function(i, obj) {
// obj.set('crmContrctPay-id', rec.get("id"));
// obj.set('crmContrctPay-name', rec.get("name"));
// obj.set('crmContrctPay-projectNode-id', rec.get("projectNode-id"));
// obj.set('crmContrctPay-projectNode-progress', rec.get("projectNode-name"));
// });
// var win = Ext.getCmp('selectcrmContrctPay')
// if(win){
// win.close();
// }
// }
function setCrmContractBillFun(rec) {
	var gridGrid = crmContrctCollectGrid;
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('billId', rec.get("billId"));// 发票号
		obj.set('billCode', rec.get("billCode"));// 票据抬头
		obj.set('details', rec.get("details"));// 票据明细
		obj.set('billMoney', rec.get("billMoney"));// 申请金额
		obj.set('billDate', rec.get("billDate"));
		obj.set('crmContract-identifiers', rec.get("crmContract-identifiers"));
		obj.set('crmContract-type.name', rec.get("crmContract-type.name"));
		obj.set('crmContract-name', rec.get("crmContract-name"));
		obj.set('crmContract-periodsNum', rec.get("crmContract-periodsNum"));

		obj.set('crmContract-writeDate', rec.get("crmContract-writeDate"));
		obj.set('crmContract-crmCustomer', rec.get("crmContract-crmCustomer"));
		obj.set('crmContract-manager-name', rec.get("crmContract-manager.name"));
		obj.set('note2', rec.get("id"));
		obj.set('crmContract-id',rec.get("crmContract-id"));
		// obj.set('crmContrctPay-name', rec.get("name"));
		// obj.set('crmContrctPay-projectNode-id', rec.get("projectNode-id"));
		// obj.set('crmContrctPay-projectNode-progress',
		// rec.get("projectNode-name"));
	});
	var win = Ext.getCmp('selectcrmContrctPay');
	if (win) {
		win.close();
	}
}

// 保存回款明细信息
function save() {
	CheckCountPaymentsData();// 检查是否存在总计项
	var crmContrctCollectItemDivData = crmContrctCollectGrid;
	document.getElementById('crmContrctCollectItemJson').value = commonGetModifyRecords(crmContrctCollectItemDivData);
	var JsonStr = $("#crmContrctCollectItemJson").val();

	ajax("post",
			"/crm/contract/crmContract/saveContrctCollectItemOutside.action", {
				crmContrctCollectItemJson : JsonStr
			}, function(data) {
				if (data.success) {
					crmContrctCollectGrid.getStore().commitChanges();
					crmContrctCollectGrid.getStore().reload();
					message("保存成功。");
				} else {
					message("保存失败，请重试！");
				}
			}, null);
	var selRecords=crmContrctCollectItemDivData.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		var ids = obj.get("note2");
		var crmContrctCollect =obj.get("id");
		if(ids!=null&&ids.length>0){
			if(ids.indexOf(",")>0){
				ajax("post",
						"/crm/contract/crmContract/saveContrctCollectItemCut.action", {
					ids: ids,crmContrctCollect:crmContrctCollect
						}, function(data) {
							if (data.success) {
								
								message("分割成功。");
							} else {
								message("分割失败，请重试！");
							}
						}, null);
			}else{
				ajax("post",
						"/crm/contract/crmContract/saveContrctCollectItemOne.action", {
					ids: ids,crmContrctCollect:crmContrctCollect
						}, function(data) {
							if (data.success) {
								
								message("分割成功。");
							} else {
								message("分割失败，请重试！");
							}
						}, null);
			}
			
			
		}else{
			
		}
		
	});
//	form1.action = window.ctx
//			+ "/crm/contract/crmContract/showCrmContractCollectItemList.action";
	// document.getElementById('crmContrctCollectItemJson').value =
	// commonGetModifyRecords(crmContrctCollectItemDivData);
	// form1.action = window.ctx +
	// "/crm/contract/crmContract/saveContrctCollectItem.action";
//	form1.submit();
}

// 计算当前页面所有回款金额总和
function countPaymentsData() {
	CheckCountPaymentsData();
	var gridGrid = crmContrctCollectGrid; // 导入目的的子表
	// var cellId = $("#southernBamhiTest_id").val();
	var sum = 0;
	var selRecords = gridGrid.getSelectionModel().getSelections();
	if (selRecords && selRecords.length > 0) {
		for ( var i = 0; i < selRecords.length; i++) {
			// alert(selRecords[i].get("money"));
			sum = Number(sum) + Number(selRecords[i].get("money"));
		}
		var ob = crmContrctCollectGrid.store.recordType;
		crmContrctCollectGrid.stopEditing();
		var p = new ob({});
		p.isNew = true;
		p.set("collectDept", "");
		p.set("receivedDate", new Date());
		p.set("collectType", "总计");
		p.set("money", sum);
		crmContrctCollectGrid.getStore().add(p);
	} else {
		message("请先选中数据！");
	}
	// $.each(selRecords, function(i, obj) {
	// // obj.get[i]("money");
	// alert(obj.get[i]("money"));
	// // sum = sum +
	// // obj.set('crmContract-name',name);
	// });
	// var temp = crmContrctCollectGrid.store; // 源数据的子表
	// if (!temp || temp.getCount() == 0) {
	// message("请先添加底物浓度信息。");
	// return;
	// } else if (temp.getCount() > 0) {
	// var temid = temp.getAt(0).get("id");
	// if (!temid || temid == "") {
	// message("请先保存底物浓度信息。");
	// return;
	// }
	// }
	// if (cellId && (!oldGrid || oldGrid.getCount() == 0)) {
	// var ob = southernBamhiResultGrid.getStore().recordType;
	// southernBamhiResultGrid.stopEditing();
	// for ( var i = 0; i < temp.getCount(); i++) {
	// var p = new ob({});
	// p.isNew = true;
	// p.set("name", temp.getAt(i).get("name"));
	// southernBamhiResultGrid.getStore().add(p);
	// }
	// southernBamhiResultGrid.startEditing(0, 0);
	// } else {
	// message("检测结果中已有记录，请删除现有记录后再重新导入数据。");
	// }
}

function CheckCountPaymentsData() {
	// var flag = true;
	var temp = crmContrctCollectGrid.getStore();
	for ( var i = 0; i < temp.getCount(); i++) {
		if (temp.getAt(i).get("collectType") == "总计") {
			// alert(temp.getAt(i).get("collectType"));
			temp.remove(temp.getAt(i));
			// flag = false;
		}
		// southernBamhiResultGrid.getStore().add(p);
	}
	// alert(flag);
}

function continueFun(k) {
//	 alert(k);
	var n = parseInt(k);
	var gridGrid = crmContrctCollectGrid.store;
	var id = gridGrid.getAt(n).get("crmContract-id");
	if (id) {
		var win = Ext.getCmp('crmContractView');
		if (win) {
			win.close();
		}
		var crmContractView = new Ext.Window(
				{
					id : 'crmContractView',
					modal : true,
					title : '查看合同详细信息',
					layout : 'fit',
					width : 1300,
					height : 600,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"
										+ ctx
										+ "/crm/contract/crmContract/viewCrmContract.action?id="
										+ id
										+ "' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : '关闭',
						handler : function() {
							crmContractView.close();
							// window.location = window.ctx
							// +
							// "/ani/inject/injectBlackCutTail/showInjectBlackCutTailAllItemList.action";
						}
					} ]
				});
		crmContractView.show();
	} else {
		message("请先选择发票后查看！");
	}
}

function setCrmContract(grids){
	var billId = "";
	var ids = "";
	var selRecords = grids.getSelectionModel().getSelections();
	if(selRecords.length>1){
		alert("分割路线");
		$.each(selRecords, function(i, obj) {
			billId+=obj.get("billId")+",";
			ids+=obj.get("id")+",";
		});
		billId = billId.substring(0, billId.length-1);
		ids = ids.substring(0, ids.length-1);
		var selRecords1=crmContrctCollectGrid.getSelectionModel().getSelections();
		
		$.each(selRecords1, function(i, obj) {
			
			obj.set('billId',billId);
			obj.set('note2',ids);
			
			obj.set('note','一个回款对多张发票');
		});
//		ajax("post",
//				"/crm/contract/crmContract/saveContrctCollectItemCut.action", {
//					ids : billId
//				}, function(data) {
//					if (data.success) {
//						message("分割成功。");
//					} else {
//						message("分割失败，请重试！");
//					}
//				}, null);
	}else{
		$.each(selRecords, function(i, obj) {
			setCrmContractBillFun(obj);
			
		});
		
	}
	
	var win = Ext.getCmp('selectcrmContrctPay');
	if(win){
		win.close();
	}
	
}
