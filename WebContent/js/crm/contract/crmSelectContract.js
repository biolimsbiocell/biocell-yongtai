var crmContractGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'project-id',
		type : "string"
	});
	fields.push({
		name : 'project-name',
		type : "string"
	});
	fields.push({
		name : 'conState-id',
		type : "string"
	});
	fields.push({
		name : 'conState-name',
		type : "string"
	});
	fields.push({
		name : 'conMoneyState-id',
		type : 'string'
	}) ;
	fields.push({
		name : 'conMoneyState-name',
		type : 'string'
	});
	fields.push({
		name : 'projectType-id',
		type : "string"
	});
	fields.push({
		name : 'projectType-name',
		type : "string"
	});
	fields.push({
		name : 'customerProperty',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
//	fields.push({
//	name : 'customerRequirements',
//	type : "string"
//	});
	fields.push({
		name : 'crmCustomer-id',
		type : "string"
	});
	fields.push({
		name : 'crmCustomer-name',
		type : "string"
	});
	fields.push({
		name : 'crmCustomerDept-id',
		type : "string"
	});
	fields.push({
		name : 'crmCustomerDept-name',
		type : "string"
	});
	fields.push({
		name : 'writeDate',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'type-id',
		type : "string"
	});
	fields.push({
		name : 'type-name',
		type : "string"
	});
//	fields.push({
//	name : 'product-id',
//	type : "string"
//	});
//	fields.push({
//	name : 'product-name',
//	type : "string"
//	});
	fields.push({
		name : 'organization',
		type : "string"
	});
	fields.push({
		name : 'money',
		type : "string"
	});
	fields.push({
		name : 'crmSaleOrder-id',
		type : "string"
	});
	fields.push({
		name : 'crmSaleOrder-name',
		type : "string"
	});
	fields.push({
		name : 'parent-id',
		type : "string"
	});
	fields.push({
		name : 'parent-name',
		type : "string"
	});
	fields.push({
		name:'currency',
		type:"string"
	});
	fields.push({
		name : 'periodsNum',
		type : "string"
	});
	fields.push({
		name : 'crmContractPay-id',
		type : "string"
	});
	fields.push({
		name : 'crmContractPay-name',
		type : "string"
	});
	fields.push({
		name : 'manager-id',
		type : "string"
	});
	fields.push({
		name : 'manager-name',
		type : "string"
	});
	fields.push({
		name : 'isRecord',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-id',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-name',
		type : "string"
	});
	fields.push({
		name : 'confirmDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'content1',
		type : "string"
	});
	fields.push({
		name : 'content2',
		type : "string"
	});
	fields.push({
		name : 'content3',
		type : "string"
	});
	fields.push({
		name : 'content4',
		type : "string"
	});
	fields.push({
		name : 'content5',
		type : "string"
	});
	fields.push({
		name : 'content6',
		type : "string"
	});
	fields.push({
		name : 'content7',
		type : "string"
	});
	fields.push({
		name : 'content8',
		type : "string"
	});
	fields.push({
		name : 'content9',
		type : "string"
	});
	fields.push({
		name : 'content10',
		type : "string"
	});
	fields.push({
		name : 'content11',
		type : "string"
	});
	fields.push({
		name : 'content12',
		type : "string"
	});
	fields.push({
		name : 'orderNum',
		type : "string"
	});
	fields.push({
		name : 'client',
		type : "string"
	});
	
	
	fields.push({
		name : 'finalProduct-id',
		type : "string"
	});
	fields.push({
		name : 'finalProduct-name',
		type : "string"
	});
	fields.push({
		name : 'productNo',
		type : "string"
	});
	fields.push({
		name : 'finalProduct-dicSampleType-id',
		type : "string"
	});
	fields.push({
		name : 'finalProduct-dicSampleType-name',
		type : "string"
	});
	fields.push({
		name : 'finalProduct-pronoun',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '编码',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'writeDate',
		header : '签订日期',
		width : 15 * 5,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectType-id',
		hidden : true,
		header : '课题类型ID',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectType-name',
		header : '课题类型',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'manager-id',
		hidden : true,
		header : '客户经理ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'manager-name',
		header : '客户经理',
		width : 10*6,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : '合同名称',
		width : 20 * 6,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'finalProduct-id',
		header : '终产品id',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'finalProduct-name',
		header : '终产品',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'productNo',
		header : '终产品数量',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'finalProduct-dicSampleType-id',
		header : '终产品样本类型id',
		width : 20 * 6,
		hidden: true,
		sortable : true
	});
	cm.push({
		dataIndex : 'finalProduct-dicSampleType-name',
		header : '终产品样本类型',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'finalProduct-pronoun',
		header : '终产品代次',
		width : 20 * 6,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'money',
		header : '合同总金额（元）',
		width : 20 * 4,
		sortable : true
	});
	cm.push({
		dataIndex:'currency',
		hidden : false,
		header:'币种',
		width:10*6,

		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
	cm.push({
		dataIndex : 'crmCustomer-id',
		hidden : true,
		header : '委托人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmCustomer-name',
		header : '委托人',
		width : 10*6,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmCustomerDept-name',
		header : '委托人单位',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'client',
		header : '委托单位',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});

	cm.push({
		dataIndex : 'crmCustomerDept-id',
		hidden : true,
		header : '委托单位ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'customerProperty',
		header : '客户性质',
		width : 10* 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'project-id',
		header : '项目ID',
		hidden : true,
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'project-name',
		header : '项目名称',
		width : 15 * 8,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'startDate',
		header : '合同开始日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : '合同结束日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-id',
		hidden : true,
		header : '合同类型ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-name',
		header : '合同类型',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'conState-id',
		hidden : true,
		header : '合同实施状态ID',
		width :6* 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'conState-name',
		header : '合同实施状态',
		width : 10* 10,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'conMoneyState-id',
		hidden : true,
		header : '合同款使用状态ID',
		width : 6*10,
		sortable : true
	});
	cm.push({
		dataIndex : 'conMoneyState-name',
		header : '合同款使用状态',
		width : 10*10 ,
		hidden : true,
		sortable : true
	}) ;
	cm.push({
		dataIndex:'periodsNum',
		hidden : false,
		header:'收款期数',
		width:10*6,

		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});


//	cm.push({
//	dataIndex : 'product-id',
//	hidden : true,
//	header : '终产品ID',
//	width : 15 * 10,
//	sortable : true
//	});
//	cm.push({
//	dataIndex : 'product-name',
//	header : '终产品',

//	width : 15 * 6,
//	sortable : true
//	});
//	cm.push({
//	dataIndex : 'customerRequirements',
//	header : '客户要求',
//	width : 15 * 10,
//	sortable : true
//	});

//	var isRecordstore = new Ext.data.ArrayStore({
//	fields : [ 'id', 'name' ],
//	data : [ [ '1', '北京百奥赛图基因生物技术有限公司' ], [ '0', '百奥赛图江苏基因生物技术有限公司' ] ]
//	});

//	var isRecordComboxFun = new Ext.form.ComboBox({
//	store : isRecordstore,
//	displayField : 'name',
//	valueField : 'id',
//	typeAhead : true,
//	mode : 'local',
//	forceSelection : true,
//	triggerAction : 'all',
//	emptyText : '',
//	selectOnFocus : true
//	});
	cm.push({
		dataIndex : 'organization',
		header : '受托方（乙方）',
		width : 20 * 6,
		//renderer : Ext.util.Format.comboRenderer(isRecordComboxFun),
		sortable : true
	});

	cm.push({
		dataIndex : 'crmSaleOrder-id',
		hidden : true,
		header : '订单ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmSaleOrder-name',
		header : '订单',
		hidden : true,
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'parent-id',
		hidden : true,
		header : '父级合同ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'parent-name',
		header : '父级合同',

		width : 15 * 10,
		sortable : true
	});
	// cm.push({
	// dataIndex:'crmContractPay-id',
	// hidden:true,
	// header:'付款计划ID',
	// width:15*10,
	// sortable:true
	// });
	// cm.push({
	// dataIndex:'crmContractPay-name',
	// header:'付款计划',
	//		
	// width:15*10,
	// sortable:true
	// });

	var isRecordstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});

	var isRecordComboxFun = new Ext.form.ComboBox({
		store : isRecordstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'isRecord',
		header : '是否备案',
		width : 10 * 6,
		hidden : true,
		renderer : Ext.util.Format.comboRenderer(isRecordComboxFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : '创建人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : '创建人',

		width : 10* 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : '创建日期',
		width : 15 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-id',
		hidden : true,
		header : '审核人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-name',
		header : '审核人',
		width : 15 *6,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmDate',
		header : '审核日期',
		width : 15 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : '工作流状态',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '工作流状态',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderNum',
		header : '订单数量',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content1',
		header : 'content1',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content2',
		header : 'content2',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content3',
		header : 'content3',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content4',
		header : 'content4',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content5',
		header : 'content5',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content6',
		header : 'content6',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content7',
		header : 'content7',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content8',
		header : 'content8',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content9',
		header : 'content9',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content10',
		header : 'content10',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content11',
		header : 'content11',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content12',
		header : 'content12',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
	+ "/crm/contract/crmContract/showCrmContractListJson.action";
	var opts = {};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setCrmContractFun(rec);
	};

	crmContractGrid = gridTable("show_crmContract_div", cols, loadParam, opts);
});


