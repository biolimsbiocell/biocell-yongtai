﻿
var crmContrctCollectGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'money',
		type:"string"
	});
	   fields.push({
		name:'billDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'billMoney',
		type:"string"
	});
	   fields.push({
		name:'billId',
		type:"string"
	});
	   fields.push({
			name:'num',
			type:"string"
		});
	   fields.push({
			name:'crmContrctPay-projectNode-id',
			type:"string"
		});
	   fields.push({
		   name:'crmContrctPay-projectNode-progress',
		   type:"string"
	   });
	   fields.push({
			name:'remind',
			type:"string"
		});
//	   fields.push({
//			name:'scaling',
//			type:"string"
//		});
	   fields.push({
		name:'receivedDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'planReceivedDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'request',
			type:"string"
		});
	   fields.push({
			name:'crmContrctPay-id',
			type:"string"
		});
		    fields.push({
			name:'crmContrctPay-name',
			type:"string"
		});
	    fields.push({
		name:'crmContract-id',
		type:"string"
	});
	    fields.push({
		name:'crmContract-name',
		type:"string"
	});
	    fields.push({
			name:'collectNum',
			type:"string"
	});
	    fields.push({
			name:'crmContract-periodsNum',
			type:"string"
	}); 
	    fields.push({
			name:'crmContract-manager-name',
			type:"string"
	}); 
	    fields.push({
			name:'crmContract-manager-id',
			type:"string"
	}); 
	    fields.push({
			name:'note2',
			type:"string"
	}); 
	    fields.push({
			name:'billState',
			type:"string"
	}); 
	    fields.push({
			name:'collectType',
			type:"string"
	}); 
	    fields.push({
			name:'collectDept',
			type:"string"
	}); 
	fields.push({
			name:'state',
			type:"string"
	}); 
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'collectNum',
		header:'序号',
		hidden : false,
		width:10*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'receivedDate',
		hidden : false,
		header:'回款日期',
		width:15*6,
		sortable : true,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	var selectCollectType = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '银行转账'
			},{
				id : '1',
				name : '支票'
			} ,{
				id :'2',
				name : '现金'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'collectType',
		hidden : false,
		header : '回款类型',
		width : 10*10,
		sortable : true,
		renderer: Ext.util.Format.comboRenderer(selectCollectType),editor: selectCollectType
	});
	cm.push({
		dataIndex : 'collectDept',
		hidden : false,
		header : '回款单位',
		whidth : 8*10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'money',
		hidden : false,
		header:'回款金额（元）',
		width:15*10,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : '备注',
		whidth : 20*10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var selectBillState = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '未开'
			},{
				id : '1',
				name : '已开'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'billState',
		hidden : false,
		header : '发票状态',
		width : 8*10,
		sortable : true,
		renderer: Ext.util.Format.comboRenderer(selectBillState),editor: selectBillState
	});
	/*cm.push({
		dataIndex:'scaling',
		hidden : false,
		header:'收款比例（%）',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});*/
	/*cm.push({
		dataIndex:'projectNode',
		hidden : false,
		header:'项目节点',
		width:25*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});*/
	/*var remind = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '开票申请'
			},{
				id : '1',
				name : '催款'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'remind',
		hidden : false,
		header:'到达节点提醒',
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(remind),editor: remind
	});*/
	
	/*cm.push({
		dataIndex:'crmContrctPay-id',
		hidden : true,
		header:'对应回款计划ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContrctPay-name',
		hidden : false,
		header:'对应回款计划',
		width:15*10
	});*/
	/*cm.push({
		dataIndex:'crmContrctPay-projectNode-id',
		hidden : true,
		header:'项目节点ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContrctPay-projectNode-progress',
		hidden : false,
		header:'项目节点',
		width:15*10
	});
	cm.push({
		dataIndex:'billDate',
		hidden : false,
		header:'开票日期',
		width:25*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'billMoney',
		hidden : false,
		header:'开票金额（元）',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});*/
	cm.push({
		dataIndex:'billId',
		hidden : false,
		header:'发票号',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'num',
		hidden : false,
		header:'申请开票数量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	/*cm.push({
		dataIndex:'request',
		hidden : false,
		header:'开票要求',
		width:30*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	
	/*cm.push({
		dataIndex:'planReceivedDate',
		hidden : false,
		header:'计划回款日期',
		width:15*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});*/
	cm.push({
		dataIndex:'crmContract-name',
		hidden : false,
		header:'合同名称',
		sortable : true,
		width:15*10
	});
	cm.push({
		dataIndex:'crmContract-periodsNum',
		hidden : false,
		header:'合同期数',
		sortable : true,
		width:15*10
	});
	cm.push({
		dataIndex:'crmContract-manager-id',
		hidden : true,
		header:'客户经理ID',
		width:15*10
	});
	cm.push({
		dataIndex:'crmContract-manager-name',
		hidden : false,
		header:'客户经理',
		sortable : true,
		width:15*10
	});
	cm.push({
		dataIndex:'note2',
		hidden : false,
		header:'备注',
		width:20*10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'状态',
		width:20*10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/contract/crmContract/showCrmContrctCollectListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="回款管理";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/contract/crmContract/delCrmContrctCollect.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
 
	 if($("#crmContract_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
	   } else{
				/*opts.tbar.push({
						text : '选择相关主表',
						handler : selectcrmContractFun
					});*/
			opts.tbar.push({
				text : '填加明细',
				iconCls : 'add',
				handler : function() {
					var ob = crmContrctCollectGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;
					crmContrctCollectGrid.stopEditing();
					p.set("state", "1");
					crmContrctCollectGrid.getStore().insert(0, p);
					crmContrctCollectGrid.startEditing(0, 0);
				}
			});
				opts.tbar.push({
					text : '显示可编辑列',
					handler : null
				});
				opts.tbar.push({
					text : '取消选中',
					handler : null
				});
								
				opts.tbar.push({
					text : '选择对应付款计划',
					handler : selectcrmContrctPay
				});
				
				opts.tbar.push({
					text : "批量上传（CSV文件）",
					handler : function() {
						var options = {};
						options.width = 350;
						options.height = 200;
						loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
							"确定":function(){
								goInExcelcsv();
								$(this).dialog("close");
							}
						},true,options);
					}
				});
				opts.tbar.push({
					text : '计算回款总额',
					handler : countPaymentsData
				});
	   }
	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = crmContrctCollectGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							crmContrctCollectGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	
	crmContrctCollectGrid=gridEditTable("crmContrctCollectdiv",cols,loadParam,opts);
	$("#crmContrctCollectdiv").data("crmContrctCollectGrid", crmContrctCollectGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});



function selectcrmContractFun(){
	var win = Ext.getCmp('selectcrmContract');
	if (win) {win.close();}
	var selectcrmContract= new Ext.Window({
	id:'selectcrmContract',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmContractSelect.action?flag=crmContract' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectcrmContract.close(); }  }]  });     selectcrmContract.show(); }
	function setcrmContract(id,name){
		var gridGrid = $("#crmContrctCollectdiv").data("crmContrctCollectGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmContract-id',id);
			obj.set('crmContract-name',name);
		});
		var win = Ext.getCmp('selectcrmContract')
		if(win){
			win.close();
		}
	}
	
	//选择对应付款计划
	function selectcrmContrctPay(){
		var win = Ext.getCmp('selectcrmContrctPay');
		if (win) {win.close();}
		var selectcrmContrctPay= new Ext.Window({
		id:'selectcrmContrctPay',modal:true,title:'选择付款计划',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/crm/contract/crmContract/showCrmContrctPayDialogList.action?id="+ $("#crmContract_id").val() +"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
			 selectcrmContrctPay.close(); }  }]  });     selectcrmContrctPay.show(); }
	function setCrmContractPayFun(rec){
		var gridGrid = crmContrctCollectGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmContrctPay-id', rec.get("id"));
			obj.set('crmContrctPay-name', rec.get("name"));
			obj.set('crmContrctPay-projectNode-id', rec.get("projectNode-id"));
			obj.set('crmContrctPay-projectNode-progress', rec.get("projectNode-name"));
		});
		var win = Ext.getCmp('selectcrmContrctPay')
		if(win){
			win.close();
		}
	}
	
	function countPaymentsData() {
		CheckCountPaymentsData();
		var gridGrid = crmContrctCollectGrid; // 导入目的的子表
		var sum = 0;
		var selRecords = gridGrid.getSelectionModel().getSelections();
		if (selRecords && selRecords.length > 0) {
			for ( var i = 0; i < selRecords.length; i++) {
				// alert(selRecords[i].get("money"));
				sum = Number(sum) + Number(selRecords[i].get("money"));
			}
			var ob = crmContrctCollectGrid.store.recordType;
			crmContrctCollectGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("collectNum", "总计");
			p.set("money", sum);
			crmContrctCollectGrid.getStore().add(p);
		} else {
			message("请先选中数据！");
		}
	}
	
	function CheckCountPaymentsData() {
		var temp = crmContrctCollectGrid.getStore();
		for ( var i = 0; i < temp.getCount(); i++) {
			if (temp.getAt(i).get("collectNum") == "总计") {
				temp.remove(temp.getAt(i));
			}
		}
	}