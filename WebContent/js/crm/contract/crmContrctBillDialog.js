﻿var crmContrctBillDialogGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'currency',
		type : "string"
	});
	fields.push({
		name : 'required',
		type : "string"
	});
	fields.push({
		name : 'billType',
		type : "string"
	});
	fields.push({
		name : 'taxPoint',
		type : "string"
	});
	fields.push({
		name : 'incomeCheck',
		type : "string"
	});
	fields.push({
		name : 'accountPeriod',
		type : "string"
	});
	fields.push({
		name : 'billDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	 fields.push({
		 name : 'applyBillDate',
		 type : "date",
		 dateFormat : "Y-m-d"
	 });
	 fields.push({
		 name : 'hopeBillDate',
		 type : "date",
		 dateFormat : "Y-m-d"
	 });
	fields.push({
		name : 'billMoney',
		type : "string"
	});
	fields.push({
		name:'receivedMoney',
		type:"string"
	});
	fields.push({
		name : 'billCode',
		type : "string"
	});
	fields.push({
		name : 'billId',
		type : "string"
	});
	fields.push({
		name : 'details',
		type : "string"
	});
	fields.push({
		name : 'num',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'note2',
		type : "string"
	});
	fields.push({
		name : 'crmContract-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-name',
		type : "string"
	});
	fields.push({
		name : 'crmContract-type-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-type-name',
		type : "string"
	});
	fields.push({
		name : 'crmContract-crmCustomer-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-crmCustomer-name',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : '',
		hidden : false,
		header : '查看合同',
		width : 10 * 6,
		sortable : true,
		renderer : function(data, metadata, record, rowIndex, columnIndex,
				store) {
			// if(data=="0"||data=="开始"){
			return String.format('<a href=\"javascript:continueFun(\''
					+ rowIndex + '\')\">查看</a>');
			// }else{
			// return String.format('已完成','Mallity');
			// }
		}
	});
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编码',
		width : 17 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		hidden : true,
		header : '描述',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-id',
		hidden : false,
		header : '合同编号',
		width : 10 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-name',
		hidden : false,
		header : '合同名称',
		sortable : true,
		width : 45 * 10
	});
	/*cm.push({
		dataIndex : 'crmContract-type-id',
		hidden : false,
		header : '合同类型ID',
		width : 10 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-type-name',
		hidden : false,
		header : '合同类型',
		width : 45 * 10
	});*/
	cm.push({
		dataIndex : 'billCode',
		hidden : false,
		header : '发票抬头',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'details',
		hidden : false,
		header : '明细',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'billId',
		hidden : false,
		header : '发票号',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'num',
		hidden : false,
		header : '期数',
		width : 10 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex : 'billMoney',
		hidden : false,
		header : '金额（元）',
		width : 15 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'receivedMoney',
		hidden : false,
		header:'已收金额（元）',
		width:20*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});

	cm.push({
		dataIndex : 'taxPoint',
		hidden : false,
		header : '税点(%)',
		width : 15 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var selectCurrency = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '人民币'
			}, {
				id : '1',
				name : '美元'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'currency',
		hidden : false,
		header : '币种',
		width : 6 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectCurrency),
		editor : selectCurrency
	});
	 cm.push({
		 dataIndex : 'applyBillDate',
		 hidden : false,
		 header : '申请开票日期',
		 width : 20 * 6,
		 sortable : true,
		 renderer : formatDate
	 });
	cm.push({
		dataIndex : 'hopeBillDate',
		hidden : false,
		header : '希望开票日期',
		width : 20 * 6,
		renderer : formatDate,
		sortable : true,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'required',
		hidden : false,
		header : '开票要求',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : '备注',
		width : 40 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var selectBillType = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '普通发票'
			}, {
				id : '1',
				name : '增值税发票'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'billType',
		hidden : false,
		header : '发票类型',
		width : 15 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectBillType),
		editor : selectBillType
	});

	var selectIncomeCheck = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '是'
			}, {
				id : '1',
				name : '否'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'incomeCheck',
		hidden : true,
		header : '技术性收入核定',
		width : 15 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectIncomeCheck),
		editor : selectIncomeCheck
	});
	cm.push({
		dataIndex : 'billDate',
		hidden : false,
		header : '开票日期',
		width : 25 * 6,
		sortable : true,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	
	cm.push({
		dataIndex : 'accountPeriod',
		hidden : false,
		header : '账期（天）',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-crmCustomer-id',
		hidden : true,
		header : '委托人id',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-crmCustomer-name',
		hidden : false,
		header : '委托人',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note2',
		hidden : false,
		header : '备注',
		width : 40 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/contract/crmContract/showDialogCrmContrctBillListJson.action";
	var opts={};
	opts.title="选择发票";
	opts.height =  document.body.clientHeight-10;
	
	opts.tbar = [];
	opts.tbar.push({
		text : "确定",
		handler : sureToChoose
	});
	
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setCrmContractBillFun(rec);
	};
 
	crmContrctBillDialogGrid=gridTable("show_dialog_crmContrctBill_div",cols,loadParam,opts);
//	crmContrctPayDialogGrid=gridTable("crmContrctBilldiv",cols,loadParam,opts);
})

function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				if (($("#startplanStartDate").val() != undefined) && ($("#startplanStartDate").val() != '')) {
					var startplanStartDatestr = ">=##@@##" + $("#startplanStartDate").val();
					$("#planStartDate1").val(startplanStartDatestr);
				}
				if (($("#endplanStartDate").val() != undefined) && ($("#endplanStartDate").val() != '')) {
					var endplanStartDatestr = "<=##@@##" + $("#endplanStartDate").val();

					$("#planStartDate2").val(endplanStartDatestr);

				}
				
				if (($("#startplanEndDate").val() != undefined) && ($("#startplanEndDate").val() != '')) {
					var startplanEndDatestr = ">=##@@##" + $("#startplanEndDate").val();
					$("#planEndDate1").val(startplanEndDatestr);
				}
				if (($("#endplanEndDate").val() != undefined) && ($("#endplanEndDate").val() != '')) {
					var endplanEndDatestr = "<=##@@##" + $("#endplanEndDate").val();

					$("#planEndDate2").val(endplanEndDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				commonSearchAction(crmContrctBillDialogGrid);
				$(this).dialog("close");
				
			},
			"清空" : function() {
				form_reset();
			}
		}, true, option);
		
	}
function continueFun(k) {
//	 alert(k);
	var n = parseInt(k);
	var gridGrid = crmContrctBillDialogGrid.store;
	var id = gridGrid.getAt(n).get("crmContract-id");
	if (id) {
		var win = Ext.getCmp('crmContractView');
		if (win) {
			win.close();
		}
		var crmContractView = new Ext.Window(
				{
					id : 'crmContractView',
					modal : true,
					title : '查看合同详细信息',
					layout : 'fit',
					width : 900,
					height : 400,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"
										+ ctx
										+ "/crm/contract/crmContract/viewCrmContract.action?id="
										+ id
										+ "' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : '关闭',
						handler : function() {
							crmContractView.close();
							// window.location = window.ctx
							// +
							// "/ani/inject/injectBlackCutTail/showInjectBlackCutTailAllItemList.action";
						}
					} ]
				});
		crmContractView.show();
	} else {
		message("请先选择发票后查看！");
	}
}

function sureToChoose(){
	window.parent.setCrmContract(crmContrctBillDialogGrid);
	alert("sureToChoose");
}
