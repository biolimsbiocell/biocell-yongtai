﻿var crmContractOrderGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	  fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	  /* fields.push({
		name:'planEndDate',
		type:"date",
		dateFormat:"Y-m-d"
	});*/
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'crmContract-id',
		type:"string"
	});
	    fields.push({
		name:'crmContract-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : false,
		header:'订单编号',
		sortable : true,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'订单名称',
		sortable : true,
		width:20*20
	});
	cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:15*10,
		sortable:true
		});
	
	cm.push({
		dataIndex:'createDate',
		hidden : false,
		header:'创建时间',
		width:40*6,
		renderer: formatDate,
		sortable : true,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	/*cm.push({
		dataIndex:'planEndDate',
		hidden : false,
		header:'计划完成时间',
		width:40*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});*/
	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/contract/crmContract/showCrmContractOrderListJson.action?id="+ $("#crmContract_id").val();
	var opts={};
	opts.title="合同订单管理";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/contract/crmContract/delCrmContractOrder.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	
//	 if($("#crmContract_state").val()==1){
////		   alert("工作流状态已完成，页面不可编辑。");
//	   } else{
//				/*opts.tbar.push({
//						text : '选择相关主表',
//						handler : selectcrmContractFun
//					});*/
//				
//				
//				
//				opts.tbar.push({
//					text : '显示可编辑列',
//					handler : null
//				});
//				opts.tbar.push({
//					text : '取消选中',
//					handler : null
//				});
//				
//				
//				
//				
//				opts.tbar.push({
//					text : "批量上传（CSV文件）",
//					handler : function() {
//						var options = {};
//						options.width = 350;
//						options.height = 200;
//						loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//							"确定":function(){
//								goInExcelcsv();
//								$(this).dialog("close");
//							}
//						},true,options);
//					}
//				});
//	   }
//	
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = crmContractProgressGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 6-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 7-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//							
//							crmContractProgressGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}
	
	crmContractOrderGrid=gridEditTable("crmContractOrderdiv",cols,loadParam,opts);
	$("#crmContractOrderdiv").data("crmContractOrderGrid", crmContractOrderGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

//function selectcrmContractFun(){
//	var win = Ext.getCmp('selectcrmContract');
//	if (win) {win.close();}
//	var selectcrmContract= new Ext.Window({
//	id:'selectcrmContract',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmContractSelect.action?flag=crmContract' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectcrmContract.close(); }  }]  });     selectcrmContract.show(); }
//	function setcrmContract(id,name){
//		var gridGrid = $("#crmContractProgressdiv").data("crmContractProgressGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('crmContract-id',id);
//			obj.set('crmContract-name',name);
//		});
//		var win = Ext.getCmp('selectcrmContract')
//		if(win){
//			win.close();
//		}
//	}
	
