﻿var crmContrctBillGrid;
$(function() {
//	document.getElementById('extJsonDataString').value = "{}";
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'currency',
		type : "string"
	});
	fields.push({
		name : 'required',
		type : "string"
	});
	fields.push({
		name : 'billType',
		type : "string"
	});
	fields.push({
		name : 'taxPoint',
		type : "string"
	});
	fields.push({
		name : 'incomeCheck',
		type : "string"
	});
	fields.push({
		name : 'accountPeriod',
		type : "string"
	});
	fields.push({
		name : 'billDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	 fields.push({
		 name : 'applyBillDate',
		 type : "date",
		 dateFormat : "Y-m-d"
	 });
	 fields.push({
		 name : 'hopeBillDate',
		 type : "date",
		 dateFormat : "Y-m-d"
	 });
	fields.push({
		name : 'billMoney',
		type : "string"
	});
	fields.push({
		name : 'receivedMoney',
		type : "string"
	});
	fields.push({
		name : 'billCode',
		type : "string"
	});
	fields.push({
		name : 'billId',
		type : "string"
	});
	fields.push({
		name : 'details',
		type : "string"
	});
	fields.push({
		name : 'num',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'note2',
		type : "string"
	});
	fields.push({
		name : 'crmContract-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-name',
		type : "string"
	});
	fields.push({
		name : 'crmContract-type-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-type-name',
		type : "string"
	});
	fields.push({
		name : 'isBill',
		type : "string"
	});
	fields.push({
		name : 'attention',
		type : "string"
	});
	//*******
	fields.push({
		name : 'billMoney',
		type : "string"
	});
	   fields.push({
			name:'receivedMoney',
			type:"string"
	});
	fields.push({
		name : '', //未到款金额
		type : "string"
	});
	fields.push({
		name : 'crmContract-manager-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-manager-name',
		type : "string"
	});
	fields.push({
		name : '', //技术类型
		type : "string"
	});
	fields.push({
		name : 'crmContract-writeDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'crmContract-crmCustomer-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-crmCustomer-name',
		type : "string"
	});
	fields.push({
		name : '',  //客户名称
		type : "string"
	});
	
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : '',
		hidden : false,
		header : '合同',
		width : 8 * 6,
		sortable : true,
		renderer : function(data, metadata, record, rowIndex, columnIndex,
				store) {
			// if(data=="0"||data=="开始"){
			return String.format('<a href=\"javascript:continueFun(\''
					+ rowIndex + '\')\">查看</a>');
			// }else{
			// return String.format('已完成','Mallity');
			// }
		}
	});
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编码',
		width : 20 * 6,
		sortable : true
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex : 'name',
		hidden : true,
		header : '描述',
		width : 20 * 6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex : 'crmContract-id',
		hidden : false,
		header : '合同编号',
		width : 10 * 10,
		sortable : true
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex : 'crmContract-name',
		hidden : false,
		header : '合同名称',
		sortable : true,
		width : 25 * 10
	});
	/*cm.push({
		dataIndex : 'crmContract-type-id',
		hidden : false,
		header : '合同类型ID',
		width : 10 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-type-name',
		hidden : false,
		header : '合同类型',
		width : 45 * 10
	});*/
	var selectIsBill = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '否'
			}, {
				id : '1',
				name : '是'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'isBill',
		hidden : true,
		header : '是否开票',
		width : 10 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectIsBill),
		editor : selectIsBill
	});
	cm.push({
		dataIndex : 'billCode',
		hidden : false,
		header : '发票抬头',
		width : 20 * 6,
		sortable : false,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'details',
		hidden : false,
		header : '明细',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : false
		})
	});
	cm.push({
		dataIndex : 'num',
		hidden : false,
		header : '期数',
		width : 8 * 5,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:false
		})
	});

	cm.push({
		dataIndex : 'billMoney',
		hidden : false,
		header : '金额（元）',
		width : 15 * 6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:false
		})
	});
	cm.push({
		dataIndex:'receivedMoney',
		hidden : false,
		header:'已收金额（元）',
		width:20*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
	cm.push({
		dataIndex : 'taxPoint',
		hidden : false,
		header : '税点(%)',
		width : 10 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var selectCurrency = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [  {
				id : '0',
				name : '人民币'
			},{
				id : '1',
				name : '美元'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'currency',
		hidden : false,
		header : '币种',
		width : 6 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectCurrency),
		editor : selectCurrency
	});
	 cm.push({
		 dataIndex : 'applyBillDate',
		 hidden : false,
		 header : '申请开票日期',
		 width : 15 * 6,
		 sortable : true,
		 renderer : formatDate
	 });
	cm.push({
		dataIndex : 'hopeBillDate',
		hidden : false,
		header : '希望开票日期',
		width : 15 * 6,
		renderer : formatDate,
		sortable : true,
		editor : new Ext.form.DateField({
			format : 'Y-m-d',
		})
	});
	cm.push({
		dataIndex : 'billDate',
		hidden : false,
		header : '开票日期',
		width : 15 * 6,
		sortable : true,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});
	cm.push({
		dataIndex : 'billId',
		hidden : false,
		header : '发票号',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var selectBillType = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '普通发票'
			}, {
				id : '1',
				name : '增值税发票'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'billType',
		hidden : false,
		header : '发票类型',
		width : 10 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectBillType),
		editor : selectBillType
	});
	cm.push({
		dataIndex : 'required',
		hidden : false,
		header : '开票要求',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'attention',
		hidden : false,
		header : '注意事项',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : '备注',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	var selectIncomeCheck = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '是'
			}, {
				id : '1',
				name : '否'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
//	cm.push({
//		dataIndex : 'incomeCheck',
//		hidden : false,
//		header : '技术性收入核定',
//		width : 12 * 8,
//		sortable : true,
//		renderer : Ext.util.Format.comboRenderer(selectIncomeCheck),
//		editor : selectIncomeCheck
//	});
	cm.push({
		dataIndex : 'accountPeriod',
		hidden : false,
		header : '账期（天）',
		width : 15 * 6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:false
		})
	});
	cm.push({
		dataIndex : 'note2',
		hidden : false,
		header : '备注2',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	//**********************
	cm.push({
		dataIndex : 'billMoney',
		hidden : false,
		header : '票据申请金额',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : '', 
		hidden : false,
		header : '未到款金额（元）',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-manager-id',
		hidden : true,
		header : '客户经理id',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-manager-name',
		hidden : false,
		header : '客户经理',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : '', 
		hidden : false,
		header : '技术类型',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-writeDate',
		hidden : false,
		header : '合同签订日期',
		width : 20 * 6,
		sortable : true,
		renderer : formatDate,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-crmCustomer-id',
		hidden : true,
		header : '委托人id',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-crmCustomer-name',
		hidden : false,
		header : '委托人',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({  
		dataIndex : '',
		hidden : false,
		header : '客户名称',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/contract/crmContractBill/showCrmContrctBillListJson.action";
	var opts = {};
	opts.title = "合同发票管理";
	opts.height = document.body.clientHeight;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/crm/contract/crmContractBill/delCrmContrctBill.action",
				{
					ids : ids
				}, function(data) {
					if (data.success) {
						scpProToGrid.getStore().commitChanges();
						scpProToGrid.getStore().reload();
						message("删除成功！");
					} else {
						message("删除失败！");
					}
				}, null);
	};

	if ($("#crmContract_state").val() == 1) {
		// alert("工作流状态已完成，页面不可编辑。");
	} else {
		/*
		 * opts.tbar.push({ text : '选择相关主表', handler : selectcrmContractFun });
		 */
		opts.tbar.push({
			text : '填加明细',
			iconCls : 'add',
			handler : function() {
				var ob = crmContrctBillGrid.getStore().recordType;
				var p = new ob({});
				p.isNew = true;
				crmContrctBillGrid.stopEditing();
				p.set("receivedMoney", "0");
				crmContrctBillGrid.getStore().insert(0, p);
				crmContrctBillGrid.startEditing(0, 0);
			}
		});
		opts.tbar.push({
			text : "搜索",
			handler : function() {
				var options = {};
				options.width = 600;
				options.height = 300;
				loadDialogPage($("#batccc_data_div"), "搜索", null, {
					"确定" : function() {
						//开票日期
						if (($("#startbillDate").val() != undefined)
								&& ($("#startbillDate").val() != '')) {
							var startbillDatestr = ">=##@@##"
									+ $("#startbillDate").val();
							$("#billDate1").val(startbillDatestr);
						}
						if (($("#endbillDate").val() != undefined)
								&& ($("#endbillDate").val() != '')) {
							var endbillDatestr = "<=##@@##"
									+ $("#endbillDate").val();
							$("#billDate2").val(endbillDatestr);
						}
						
						//申请开票日期
						if (($("#startapplyBillDate").val() != undefined)
								&& ($("#startapplyBillDate").val() != '')) {
							var startapplyBillDatestr = ">=##@@##"
									+ $("#startapplyBillDate").val();
							$("#applyBillDate1").val(startapplyBillDatestr);
						}
						if (($("#endapplyBillDate").val() != undefined)
								&& ($("#endapplyBillDate").val() != '')) {
							var endapplyBillDatestr = "<=##@@##"
									+ $("#endapplyBillDate").val();
							$("#applyBillDate2").val(endapplyBillDatestr);
						}
						
						commonSearchAction(crmContrctBillGrid);
//						alert($("#extJsonDataString").val());
//						document.getElementById('extJsonDataString').value = $("#extJsonDataString").val();
//						alert($("#extJsonDataString").val());
//						var records = esBreedDisposeItemGrid.getSelectRecord();
//						if (records && records.length > 0) {
//							var action = $("#action").val();
////							birthDate = birthDate.replace(/-/g,"/");
////							var date = new Date(birthDate);
//							esBreedDisposeItemGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								obj.set("action", action);
//							});
//							esBreedDisposeItemGrid.startEditing(0, 0);
//						}else{
//							message("请选择数据！");
//						}
						$(this).dialog("close");
					},"清空" : function() {
						form_reset();
					}
				}, true, options);
			}
		});
		opts.tbar.push({
			text : '显示可编辑列',
			handler : null
		});
		opts.tbar.push({
			text : '取消选中',
			handler : null
		});
		opts.tbar.push({
			text : '保存',
			handler : save
		});
//		opts.tbar.push({
//			text : "批量上传（CSV文件）",
//			handler : function() {
//				var options = {};
//				options.width = 350;
//				options.height = 200;
//				loadDialogPage($("#bat_uploadcsv_div"), "批量上传", null, {
//					"确定" : function() {
//						goInExcelcsv();
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}
//		});
		opts.tbar.push({
			text : '选择合同',
			handler : selectContract
		});
		opts.tbar.push({
			text : '导出',
			handler : exportexcel
		});
	}

	//crmContrctBillGrid=gridTable("show_crmContractBill_div", cols, loadParam, opts);
	
	function goInExcelcsv() {
		var file = document.getElementById("file-uploadcsv").files[0];
		var n = 0;
		var ob = crmContrctBillGrid.getStore().recordType;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
				if (n > 0) {
					if (this[0]) {
						var p = new ob({});
						p.isNew = true;
						var o;
						o = 0 - 1;
						p.set("po.fieldName", this[o]);

						o = 1 - 1;
						p.set("po.fieldName", this[o]);

						o = 2 - 1;
						p.set("po.fieldName", this[o]);

						o = 3 - 1;
						p.set("po.fieldName", this[o]);

						o = 4 - 1;
						p.set("po.fieldName", this[o]);

						o = 5 - 1;
						p.set("po.fieldName", this[o]);

						o = 6 - 1;
						p.set("po.fieldName", this[o]);

						o = 7 - 1;
						p.set("po.fieldName", this[o]);

						o = 8 - 1;
						p.set("po.fieldName", this[o]);

						crmContrctBillGrid.getStore().insert(0, p);
					}
				}
				n = n + 1;

			});
		};
	}

	crmContrctBillGrid = gridEditTable("crmContrctBilldiv", cols, loadParam,
			opts);
	$("#crmContrctBilldiv").data("crmContrctBillGrid", crmContrctBillGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function exportexcel() {
	crmContrctBillGrid.title = '导出列表';
	var vExportContent = crmContrctBillGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
			if(ui.index==1){
				load("/crm/contract/crmContractBill/showCrmContractNoBillItemList.action", null, "#crmContrctNoBilldiv");
			}
		}
	});
});
function selectcrmContractFun() {
	var win = Ext.getCmp('selectcrmContract');
	if (win) {
		win.close();
	}
	var selectcrmContract = new Ext.Window(
			{
				id : 'selectcrmContract',
				modal : true,
				title : '选择相关主表',
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/CrmContractSelect.action?flag=crmContract' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						selectcrmContract.close();
					}
				} ]
			});
	selectcrmContract.show();
}
function setcrmContract(id, name) {
	var gridGrid = $("#crmContrctBilldiv").data("crmContrctBillGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('crmContract-id', id);
		obj.set('crmContract-name', name);
	});
	var win = Ext.getCmp('selectcrmContract');
	if (win) {
		win.close();
	}
}
// 保存发票管理信息
function save() {
	var crmContrctBillItemDivData = crmContrctBillGrid;
	var records = crmContrctBillGrid.getAllRecord();
	var flag = true;
	$.each(records, function(i, obj) {
//		if(obj.get("billId")==null || obj.get("billId")==""){
//			flag = false;
//			message("发票号不能为空！");
//			return;
//		}
//		if(obj.get("isBill")==null || obj.get("isBill")==""){
//			flag = false;
//			message("请选择是否开票！");
//			return;
//		}
		if(obj.get("crmContract-id")==null || obj.get("crmContract-id")==""){
			flag = false;
			message("合同号不能为空！");
			return;
		}
	});
	if(flag){
		document.getElementById('crmContrctBillItemJson').value = commonGetModifyRecords(crmContrctBillItemDivData);
		var JsonStr =  $("#crmContrctBillItemJson").val();
//	form1.action = window.ctx
//			+ "/crm/contract/crmContract/saveContrctBillItem.action";
//	form1.submit();
		ajax("post", "/crm/contract/crmContract/saveContrctBillItemOutside.action", {
			crmContrctBillItemJson : JsonStr
		}, function(data) {
			if (data.success) {
				message("保存成功。");
				crmContrctBillGrid.getStore().commitChanges();
				crmContrctBillGrid.getStore().reload();
			} else {
				message("保存失败，请重试！");
			}
		}, null);
	}
}

// 选择合同
function selectContract() {
	var win = Ext.getCmp('selectContract');

	if (win) {
		win.close();
	}
	var selectContract = new Ext.Window(
			{
				id : 'selectContract',
				modal : true,
				title : '选择合同',
				layout : 'fit',
				width : 900,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/crm/contract/crmContract/selectCrmContract.action?flag=CrmContractFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						selectContract.close();
					}
				} ]
			});
	selectContract.show();
}

function setCrmContractFun(rec) {
	var gridGrid = $("#crmContrctBilldiv").data("crmContrctBillGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		
		obj.set('crmContract-id', rec.get('id'));
		obj.set('crmContract-name', rec.get('name'));
		obj.set('billCode',rec.get('client'));
		alert(rec.get('crmCustomer-id'));
		alert(rec.get('crmCustomer-name'));
		obj.set('crmContract-crmCustomer-id',rec.get('crmCustomer-id'));
		obj.set('crmContract-crmCustomer-name',rec.get('crmCustomer-name'));
	});
	var win = Ext.getCmp('selectContract');
	if (win) {
		win.close();
	}
}
// 查看合同
function continueFun(k) {
	var n = parseInt(k);
	var gridGrid = crmContrctBillGrid.store;
	var id = gridGrid.getAt(n).get("crmContract-id");
	if (id) {
		var win = Ext.getCmp('project');
		if (win) {
			win.close();
		}
		var project = new Ext.Window(
				{
					id : 'project',
					modal : true,
					title : '查看合同详细信息',
					layout : 'fit',
					width : 1300,
					height : 600,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"
										+ ctx
										+ "/crm/contract/crmContract/viewCrmContract.action?id="
										+ id
										+ "' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : '关闭',
						handler : function() {
							project.close();
						}
					} ]
				});
		project.show();
	} else {
		message("请先选择合同信息！");
	}
	
}