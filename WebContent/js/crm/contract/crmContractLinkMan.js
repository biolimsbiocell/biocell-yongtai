﻿var crmContractLinkManGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'email',
		type : "string"
	});
	fields.push({
		name : 'ktEmail',
		type : "string"
	});
	fields.push({
		name : 'deptName',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'tel',
		type : "string"
	});
	fields.push({
		name : 'trans',
		type : "string"
	});
	fields.push({
		name : 'crmContract-id',
		type : "string"
	});
	fields.push({
		name : 'crmContract-name',
		type : "string"
	});
	
	
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编号',
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'name',
		hidden : false,
		header : '姓名',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'email',
		hidden : false,
		header : 'Email',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'ktEmail',
		hidden : false,
		header : '接收课题回报Email',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'phone',
		hidden : false,
		header : '手机',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'tel',
		hidden : false,
		header : '固定电话',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'deptName',
		hidden : false,
		header : '单位名称',
		width : 30 * 8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'trans',
		hidden : false,
		header : '传真',
		width : 30 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-id',
		hidden : true,
		header : '相关主表ID',
		width : 15 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmContract-name',
		hidden : true,
		header : '相关主表',
		width : 15 * 10
	});
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/contract/crmContract/showCrmContractLinkManListJson.action?id="
			+ $("#id_parent_hidden").val() + "&wid="
			+ $("#crmCustomerid").val();
	var opts = {};
	opts.title = "合同管理联系人";
	opts.height = document.body.clientHeight - 100;
	opts.tbar = [];
	// opts.delSelect = function(ids) {
	// ajax("post",
	// "/crm/contract/crmContractLinkMan/delCrmContractLinkMan.action", {
	// ids : ids
	// }, function(data) {
	// if (data.success) {
	// scpProToGrid.getStore().commitChanges();
	// scpProToGrid.getStore().reload();
	// message("删除成功！");
	// } else {
	// message("删除失败！");
	// }
	// }, null);
	// };
	//    
	// opts.tbar.push({
	// text : '选择相关主表',
	// handler : selectcrmContractFun
	// });
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	//	
	// opts.tbar.push({
	// text : "批量上传（CSV文件）",
	// handler : function() {
	// var options = {};
	// options.width = 350;
	// options.height = 200;
	// loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
	// "确定":function(){
	// goInExcelcsv();
	// $(this).dialog("close");
	// }
	// },true,options);
	// }
	// });

	function goInExcelcsv() {
		var file = document.getElementById("file-uploadcsv").files[0];
		var n = 0;
		var ob = crmContractLinkManGrid.getStore().recordType;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
				if (n > 0) {
					if (this[0]) {
						var p = new ob({});
						p.isNew = true;
						var o;
						o = 0 - 1;
						p.set("po.fieldName", this[o]);

						o = 1 - 1;
						p.set("po.fieldName", this[o]);

						o = 2 - 1;
						p.set("po.fieldName", this[o]);

						o = 3 - 1;
						p.set("po.fieldName", this[o]);

						o = 4 - 1;
						p.set("po.fieldName", this[o]);

						o = 5 - 1;
						p.set("po.fieldName", this[o]);

						o = 6 - 1;
						p.set("po.fieldName", this[o]);

						o = 7 - 1;
						p.set("po.fieldName", this[o]);

						crmContractLinkManGrid.getStore().insert(0, p);
					}
				}
				n = n + 1;

			});
		}
	}
	// opts.tbar.push({
	// text : '显示可编辑列',
	// handler : null
	// });
	// opts.tbar.push({
	// text : '取消选中',
	// handler : null
	// });
	crmContractLinkManGrid = gridEditTable("crmContractLinkMandiv", cols,
			loadParam, opts);
	$("#crmContractLinkMandiv").data("crmContractLinkManGrid",
			crmContractLinkManGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmContractFun() {
	var win = Ext.getCmp('selectcrmContract');
	if (win) {
		win.close();
	}
	var selectcrmContract = new Ext.Window(
			{
				id : 'selectcrmContract',
				modal : true,
				title : '选择相关主表',
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/CrmContractSelect.action?flag=crmContract' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						selectcrmContract.close();
					}
				} ]
			});
	selectcrmContract.show();
}
function setcrmContract(id, name) {
	var gridGrid = $("#crmContractLinkMandiv").data("crmContractLinkManGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('crmContract-id', id);
		obj.set('crmContract-name', name);
	});
	var win = Ext.getCmp('selectcrmContract')
	if (win) {
		win.close();
	}
}
