﻿var state;
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});

	var item = menu.add({
		text : '复制'
	});
	item.on('click', editCopy);

	state = $("#crmContract_stateName").val();
	if (state != "开始") {
		var item = menu.add({
			text : '生成订单'
		});
		item.on('click', generateOrder);
	}
});
function add() {
	window.location = window.ctx
			+ "/crm/contract/crmContract/editCrmContract.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/crm/contract/crmContract/showCrmContractList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	if ($("#crmContract_confirmUser_name").val() == "") {
		message("审核人未填写");
		return;
	}
	submitWorkflow("CrmContract", {
		userId : userId,
		userName : userName,
		formId : $("#crmContract_id").val(),
		title : $("#crmContract_name").val()
	}, function() {
		window.location.reload();
	});
});

$("#toolbarbutton_sp")
		.click(
				function() {
					completeTask(
							$("#crmContract_id").val(),
							$(this).attr("taskId"),
							function() {
								document
										.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx
										+ '/dashboard/toDashboard.action';
							});
				});

function save() {
	//判断是否有合计的一行
	var crmContrctCollectGrid = $("#crmContrctCollectdiv").data("crmContrctCollectGrid");//回款管理
	CheckcrmContrctCollectGridData(crmContrctCollectGrid);
//	var crmContrctPayGrid = $("#crmContrctPaydiv").data("crmContrctPayGrid");//价款信息
//	CheckcrmContrctPayGridData(crmContrctPayGrid);
	var crmCBillGrid = $("#crmContrctBilldiv").data("crmContrctBillGrid");//发票管理
	CheckCountcountBillMoneyData(crmCBillGrid);
	
	if (checkSubmit() == true && nameDuplicateCheck() == true) {
//		var crmContrctPayDivData = $("#crmContrctPaydiv").data(
//				"crmContrctPayGrid");
//		document.getElementById('crmContrctPayJson').value = commonGetModifyRecords(crmContrctPayDivData);

		var crmContrctBillDivData = $("#crmContrctBilldiv").data(
				"crmContrctBillGrid");
		document.getElementById('crmContrctBillJson').value = commonGetModifyRecords(crmContrctBillDivData);

		// var crmContractOrderDivData =
		// $("#crmContractOrderdiv").data("crmContractOrderGrid");
		// document.getElementById('crmContractOrderJson').value =
		// commonGetModifyRecords(crmContractOrderDivData);

//		var crmContractLinkManDivData = $("#crmContractLinkMandiv").data(
//				"crmContractLinkManGrid");
//		document.getElementById('crmContractLinkManJson').value = commonGetModifyRecords(crmContractLinkManDivData);

		var crmContrctCollectDivData = $("#crmContrctCollectdiv").data(
				"crmContrctCollectGrid");
		document.getElementById('crmContrctCollectJson').value = commonGetModifyRecords(crmContrctCollectDivData);

		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/crm/contract/crmContract/save.action";
		form1.submit();
	}
}

function CheckcrmContrctCollectGridData(grid) {
	var temp = grid.getStore();
	for ( var i = 0; i < temp.getCount(); i++) {
		if (temp.getAt(i).get("collectNum") == "总计") {
			temp.remove(temp.getAt(i));
		}
	}
}
//function CheckcrmContrctPayGridData(grid){
//	var temp=grid.getStore();
//	for(var i=0;i<temp.getCount();i++){
//		if(temp.getAt(i).get("name")=="合计"){
//			temp.remove(temp.getAt(i));
//		}
//	}
//}
function CheckCountcountBillMoneyData(grid) {
	var temp = grid.getStore();
	for ( var i = 0; i < temp.getCount(); i++) {
		if (temp.getAt(i).get("name") == "总计") {
			temp.remove(temp.getAt(i));
		}
	}
}

function editCopy() {
	window.location = window.ctx
			+ '/crm/contract/crmContract/copyCrmContract.action?id='
			+ $("#crmContract_id").val();
}
$("#toolbarbutton_status").click(
		function() {
			if ($("#crmContract_id").val())
				commonChangeState("formId=" + $("#crmContract_id").val()
						+ "&tableId=CrmContract");
		});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#crmContract_id").val());
	nsc.push("编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : '合同管理',
				contentEl : 'markup'
			} ]
		});
	});
//	load("/crm/contract/crmContract/showCrmContrctPayList.action", {
//		id : $("#crmContract_id").val()
//	}, "#crmContrctPaypage");
//	load("/crm/contract/crmContract/showCrmContractOrderList.action", {
//		id : $("#crmContract_id").val()
//	}, "#crmContractOrderpage");
	load("/crm/contract/crmContract/showCrmContrctBillList.action", {
		id : $("#crmContract_id").val()
	}, "#crmContrctBillpage");
//	load("/crm/contract/crmContract/showCrmContractLinkManList.action", {
//		id : $("#crmContract_id").val(),
//		wid : $("#crmContract_crmCustomer").val()
//	}, "#crmContractLinkManpage");
	load("/crm/contract/crmContract/showCrmContrctCollectList.action", {
		id : $("#crmContract_id").val()
	}, "#crmContrctCollectpage");
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});
$(function() {
	new Ext.form.ComboBox({
		transform : "crmContract_isRecord",
		width : 80,
		hiddenId : "crmContract_isRecord",
		hiddenName : "crmContract.isRecord"
	});
});

// 合同生成订单
function generateOrder() {
	var orderId = $("#crmContract_crmSaleOrder").val();
	var mainId = $("#crmContract_id").val();
	var orderNum = $("#crmContract_orderNum").val();
	var createOrder = $("#crmContract_createOrder").val();
	var periodsNum = $("#crmContract_periodsNum").val();
	// if(orderId&&orderId!=""){
	// message("已经关联了订单，无法再次生成订单！");
	// }else{
	if (createOrder == "0") {
		message("【创建订单】为【否】,此合同不能创建订单！");
	} else {
		if (orderNum != null && orderNum != "") {
			if (periodsNum != null && periodsNum != "") {
				if (orderNum > 0) {
					if (periodsNum > 0) {
						ajax(
								"post",
								"/crm/contract/crmContract/generateOrder.action",
								{
									Id : mainId,
									orderNum : orderNum
								}, function(data) {
									if (data.success) {
										if (data.data) {
											message("生成【订单管理】数据成功");
											window.location.reload();
											// window.location = window.ctx +
											// "/crm/contract/crmContract/editCrmContract.action?id="+mainId;
										} else {
											message("生成订单数据失败，请重试！");
										}
									} else {
										message("获取明细数据时发生错误！");
									}
								}, null);
					} else {
						message("【收款期数】必须大于0！");
					}
				} else {
					message("【订单数量】必须大于0！");
				}
			} else {
				message("【收款期数】不能为空！");
			}
		} else {
			message("【订单数量】不能为空！");
		}
	}
}

// var item = menu.add({
// text: '生成发票'
// });
// item.on('click', editBill);
// //生成发票事件
// function editBill(){
// window.location = window.ctx +
// '/crm/contract/crmContract/editBillList.action?id='+$("#crmContract_id").val();
// }
function CrmCustomerFun() {
	var win = Ext.getCmp('CrmCustomerFun');
	if (win) {
		win.close();
	}
	var CrmCustomerFun = new Ext.Window(
			{
				id : 'CrmCustomerFun',
				modal : true,
				title : '选择委托人',
				layout : 'fit',
				width : 600,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/crm/customer/customer/crmCustomerSelect.action?flag=CrmCustomerFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						CrmCustomerFun.close();
					}
				} ]
			});
	CrmCustomerFun.show();
}
var id = "";
var name = "";
function setCrmCustomerFun(rec) {
	var cId = $("#crmContract_crmCustomer").val();
	if (cId == null || cId == "") {
		document.getElementById('crmContract_crmCustomer').value = rec
				.get('id');
		document.getElementById('crmContract_crmCustomer_name').value = rec
				.get('name');
//		document.getElementById('crmContract_crmCustomerDept_name').value = rec
//				.get('department-name');
//		document.getElementById('crmContract_crmCustomerDept').value = rec
//				.get('department-id');
//		document.getElementById('crmContract_client').value = rec
//				.get('department-name');
//		document.getElementById('crmContract_crmCustomer_selA').value = rec
//				.get('selA');
		var win = Ext.getCmp('CrmCustomerFun');
		if (win) {
			win.close();
		}
//		id = rec.get('id');
//		name = rec.get('name');
//		ajax("post", "/crm/contract/crmContract/showLinkManList.action", {
//			code : id,
//		}, function(data) {
//			if (data.success) {
//
//				var ob = crmContractLinkManGrid.getStore().recordType;
//				crmContractLinkManGrid.stopEditing();
//
//				$.each(data.data, function(i, obj) {
//					var p = new ob({});
//					p.isNew = true;
//
//					p.set("id", obj.id);
//					p.set("name", obj.name);
//					p.set("email", obj.email);
//					p.set("mobile", obj.mobile);
//					p.set("trans", obj.trans);
//					p.set("station", obj.station);
//					p.set("telephone", obj.telephone);
//
//					crmContractLinkManGrid.getStore().add(p);
//				});
//
//				crmContractLinkManGrid.startEditing(0, 0);
//			} else {
//				message("获取明细数据时发生错误！");
//			}
//		}, null);
	} else {
		if (rec.get('id') == cId) {
			var win = Ext.getCmp('CrmCustomerFun');
			if (win) {
				win.close();
			}
		} else {
//			crmContractLinkManGrid.store.removeAll();
			document.getElementById('crmContract_crmCustomer').value = rec
					.get('id');
			document.getElementById('crmContract_crmCustomer_name').value = rec
					.get('name');
//			document.getElementById('crmContract_crmCustomerDept_name').value = rec
//					.get('department-name');
//			document.getElementById('crmContract_client').value = rec
//					.get('department-name');
//			document.getElementById('crmContract_crmCustomerDept').value = rec
//					.get('department-id');
//			document.getElementById('crmContract_crmCustomer_selA').value = rec
//					.get('selA');
			var win = Ext.getCmp('CrmCustomerFun');
			if (win) {
				win.close();
			}
//			id = rec.get('id');
//			name = rec.get('name');
//			ajax("post", "/crm/contract/crmContract/showLinkManList.action", {
//				code : id,
//			}, function(data) {
//				if (data.success) {
//
//					var ob = crmContractLinkManGrid.getStore().recordType;
//					crmContractLinkManGrid.stopEditing();
//
//					$.each(data.data, function(i, obj) {
//						var p = new ob({});
//						p.isNew = true;
//
//						p.set("id", obj.id);
//						p.set("name", obj.name);
//						p.set("email", obj.email);
//						p.set("mobile", obj.mobile);
//						p.set("trans", obj.trans);
//						p.set("station", obj.station);
//						p.set("telephone", obj.telephone);
//
//						crmContractLinkManGrid.getStore().add(p);
//					});
//
//					crmContractLinkManGrid.startEditing(0, 0);
//				} else {
//					message("获取明细数据时发生错误！");
//				}
//			}, null);
		}
	}

}

function CrmCustomerDeptFun() {
	var win = Ext.getCmp('CrmCustomerDeptFun');
	if (win) {
		win.close();
	}
	var CrmCustomerDeptFun = new Ext.Window(
			{
				id : 'CrmCustomerDeptFun',
				modal : true,
				title : '选择委托单位',
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/customerdept/crmCustomerDept/crmCustomerDeptSelect.action?flag=CrmCustomerDeptFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						CrmCustomerDeptFun.close();
					}
				} ]
			});
	CrmCustomerDeptFun.show();
}
function setCrmCustomerDeptFun(rec) {
//	document.getElementById("crmContract_crmCustomerDept").value = rec
//			.get('id');
	document.getElementById("crmContract_client").value = rec
			.get('name');
	var win = Ext.getCmp('CrmCustomerDeptFun');
	if (win) {
		win.close();
	}
}

function legalPersonFun() {
	var win = Ext.getCmp('legalPersonFun');
	if (win) {
		win.close();
	}
	var legalPersonFun = new Ext.Window(
			{
				id : 'legalPersonFun',
				modal : true,
				title : '选择法人',
				layout : 'fit',
				width : 600,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/crm/customer/customer/crmCustomerSelect.action?flag=legalPersonFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						legalPersonFun.close();
					}
				} ]
			});
	legalPersonFun.show();
}

function setlegalPersonFun(rec) {
	document.getElementById("crmContract_legalPerson").value = rec
			.get('id');
	document.getElementById("crmContract_legalPerson_name").value = rec
			.get('name');
	var win = Ext.getCmp('legalPersonFun');
	if (win) {
		win.close();
	}
}
// 选择订单接收人
function ddjsUser() {
	var win = Ext.getCmp('selectmateUser');
	if (win) {
		win.close();
	}
	var selectmateUser = new Ext.Window(
			{
				id : 'selectmateUser',
				modal : true,
				title : '选择订单接收人',
				layout : 'fit',
				width : 800,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/core/user/showUserListAllSelect.action?funType=hlUser' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						selectmateUser.close();
					}
				} ]
			});
	selectmateUser.show();
}

function addUserGrid(record, funType) {
	var n = "";
	var nId = "";
	for ( var ij = 0; ij < record.length; ij++) {
		n = n + record[ij].get('name') + ",";
		nId = nId + record[ij].get('id') + ",";
	}
	if (funType == 'hlUser') {
		document.getElementById('crmContract_orderRecipient').value = n;
	}

	var win = Ext.getCmp('selectmateUser');
	if (win) {
		win.close();
	}
}

function nameDuplicateCheck() {
	var id = $("#crmContract_id").val();
	if (id == "NEW") {
		var crmContractName = $("#crmContract_name").val();
		var flag = false;
		// var projectNo = $("#injectEgeTalenBreed_projectNo").val();
		if (crmContractName != "") {
			ajax("post","/crm/contract/crmContract/checkNameDuplicate.action",{crmContractName:crmContractName},
					function(data){
				if(data.success){
					flag=true;
					//可用
				}else{
					flag=confirm("该合同名称已创建信息是否继续");
					//不可用
				}
			},null);
			if (flag == true) {
				return true;
			} else {
				return false;
			}
		} else {
			message("请先输入合同名称！");
			return false;
		}
	} else {
		return true;
	}
}

// submitWorkflow("CrmContract", {
// userId : userId,
// userName : userName,
// formId : $("#crmContract_id").val(),//表单ID,
// title : "",//提示标题
// data: JSON.stringify(data)//需要提交到工作流中的变量
// }, function() {//成功提交工作流的回调方法
// window.location.reload();
// });
var item = menu.add({
	text: '取消'
	});
item.on('click', cancel);
function cancel() {
	ajax("post", "/sample/task/cancel.action", {
		modelType : "CrmContract",
		id : $("#crmContract_id").val()
	}, function(data) {
		location.reload();
	}, null);
}

function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : '选择项目',
				layout : 'fit',
				width : 600,
				height : 600,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/finalProduct/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : '关闭',
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}	

function setProductFun(id, name) {
	if(id.indexOf(",") >= 0){
		message("只能选择一个终产品！");
	}else{
		var productName = "";
		ajax(
				"post",
				"/com/biolims/system/finalProduct/findProductToSample.action",
				{
					code : id
				},
				function(data) {

					if (data.success) {
						$.each(data.data, function(i, obj) {
							productName += obj.name + ",";
						});
						document.getElementById("crmContract_finalProduct").value = id;
						document.getElementById("crmContract_product").value = id;
						document.getElementById("crmContract_productName").value = productName;
//						if(id =="Z0009"){
//							$("#tr1").removeClass("aa");
//						}
					}
				}, null);
		var win = Ext.getCmp('voucherProductFun');
		if (win) {
			win.close();
		}
	}
	
}
