﻿//合同价款信息
var crmContrctPayGrid; 
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'name',
			type:"string"
		});
//	   fields.push({
//			name:'totalMoney',
//			type:"string"
//		});
	   fields.push({
		   name : 'periodsNum',
		   type : "string"
	   });
//	   fields.push({
//			name:'currency',
//			type:"string"
//		});
	   fields.push({
		name:'money',
		type:"string"
	});
	   fields.push({
		name:'billDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'billMoney',
		type:"string"
	});
	   fields.push({
		name:'billId',
		type:"string"
	});
	   fields.push({
			name:'num',
			type:"string"
		});
	   fields.push({
			name:'request',
			type:"string"
		});
	   fields.push({
			name:'projectNode-id',
			type:"string"
		});
	   fields.push({
		   name:'projectNode-progress',
		   type:"string"
	   });
	   fields.push({
			name:'remind',
			type:"string"
		});
	   fields.push({
			name:'scaling',
			type:"string"
		});
	   fields.push({
		name:'receivedDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'planReceivedDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'crmContract-id',
		type:"string"
	});
	    fields.push({
		name:'crmContract-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'periodsNum',
		hidden : false,
		header:'期数',
		width:10*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
	/*cm.push({
		dataIndex:'totalMoney',
		hidden : false,
		header:'合同总金额（元）',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});*/
	cm.push({
		dataIndex:'scaling',
		hidden : false,
		header:'收款比例（%）',
		width:20*6,
		sortable : true
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	/*var selectCurrency = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '人民币'
			},{
				id : '1',
				name : '美元'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'currency',
		hidden : false,
		header:'币种',
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(selectCurrency),editor: selectCurrency
	});*/
	cm.push({
		dataIndex:'money',
		hidden : false,
		header:'金额（元）',
		width:20*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
	
	cm.push({
		dataIndex:'projectNode-id',
		hidden : true,
		header:'项目节点ID',
		width:25*6
	});
	cm.push({
		dataIndex:'projectNode-progress',
		hidden : false,
		header:'项目节点',
		width:25*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var remind = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '开票申请'
			},{
				id : '1',
				name : '催款'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'remind',
		hidden : false,
		header:'到达节点提醒',
		width:15*10,
		sortable : true,
		renderer: Ext.util.Format.comboRenderer(remind),editor: remind
	});
	cm.push({
		dataIndex:'billDate',
		hidden : false,
		header:'开票日期',
		width:25*6,
		sortable : true,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	/*cm.push({
		dataIndex:'billMoney',
		hidden : false,
		header:'开票金额',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'billId',
		hidden : false,
		header:'发票号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'申请开票数量（张）',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'request',
		hidden : false,
		header:'开票要求',
		width:30*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
/*	cm.push({
		dataIndex:'receivedDate',
		hidden : false,
		header:'回款日期',
		width:15*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});*/
	cm.push({
		dataIndex:'planReceivedDate',
		hidden : false,
		header:'计划回款日期',
		width:15*6,
		sortable : true,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/contract/crmContract/showCrmContrctPayListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="合同付款计划";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/contract/crmContract/delCrmContrctPay.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	
	 if($("#crmContract_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
	   } else{
				/*opts.tbar.push({
						text : '选择相关主表',
						handler : selectcrmContractFun
					});*/
		   opts.tbar.push({
				text : '计算金额总和',
				handler : countBillMoney
			});
			   opts.tbar.push({
				   text : '计算收款比例',
				   handler : countScaling
			   });
				opts.tbar.push({
					text : '选择项目节点',
					handler : selcetProjectNodeFun
				});
				opts.tbar.push({
					text : '由收款期数生成行数',
					handler : generateRow
				});
				opts.tbar.push({
					text : '由申请开票数生成发票张数',
					handler : createPayNum
				});
				opts.tbar.push({
					text : '取消选中',
					handler : null
				});
				/*opts.tbar.push({
					text : "批量上传（CSV文件）",
					handler : function() {
						var options = {};
						options.width = 350;
						options.height = 200;
						loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
							"确定":function(){
								goInExcelcsv();
								$(this).dialog("close");
							}
						},true,options);
					}
				});*/
	   }
	 
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = crmContrctPayGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							crmContrctPayGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	
	crmContrctPayGrid=gridEditTable("crmContrctPaydiv",cols,loadParam,opts);
	$("#crmContrctPaydiv").data("crmContrctPayGrid", crmContrctPayGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectcrmContractFun(){
	var win = Ext.getCmp('selectcrmContract');
	if (win) {win.close();}
	var selectcrmContract= new Ext.Window({
	id:'selectcrmContract',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmContractSelect.action?flag=crmContract' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectcrmContract.close(); }  }]  });     selectcrmContract.show(); }
	function setcrmContract(id,name){
		var gridGrid = $("#crmContrctPaydiv").data("crmContrctPayGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmContract-id',id);
			obj.set('crmContract-name',name);
		});
		var win = Ext.getCmp('selectcrmContract')
		if(win){
			win.close();
		}
	}
	//计算收款比例
	function countScaling(){
		var grid = crmContrctPayGrid.store;
		for(var i=0;i<grid.getCount();i++){
			var A = grid.getAt(i).get("money");
			var C = $("#crmContract_money").val(); //主表中的合同总金额
			if(parseFloat(A)>=0 && parseFloat(C)>0 ){
				var V = A/C*100;
				grid.getAt(i).set("scaling",V.toFixed(2));
			}
		} 
	}
	//根据主表的收款期数生成相应的行数
	function generateRow(){
		var grid = crmContrctPayGrid.store;
		var num = parseInt($("#crmContract_periodsNum").val());	
		if(grid&&grid.getCount()>0){
			message("已经生成,请删除原有的数据!");
		}else{
			if(num && num!=''){
				var ob = crmContrctPayGrid.getStore().recordType;
				crmContrctPayGrid.stopEditing();
				for(var i=0;i<num;i++){
					var p = new ob({});
					p.isNew = true;
					p.set("id", "");
					p.set("periodsNum",i+1);
					crmContrctPayGrid.getStore().insert(i, p);
				}
				crmContrctPayGrid.startEditing(0, 0);
			}else{
				message("请先填写收款期数");
			}
		}	
	}
	//由申请发票张数生成发票管理
	function createPayNum(){
		var grid = crmContrctPayGrid.store;
		var fpgl=crmContrctBillGrid.getStore();
		if(fpgl.getCount()==0){
			for(var i=0;i<grid.getCount();i++){
				var count = grid.getAt(i).get("num");
				var ob = crmContrctBillGrid.getStore().recordType;
				if(count>0 && count !=""){
					for(var j=0;j<count;j++){
						var p =new ob({});
						p.isNew=true;
						var ps = grid.getAt(i).get("periodsNum");
						p.set("num",ps);
						p.set('currency',0);
						crmContrctBillGrid.getStore().insert(j, p);
						message("生成【合同发票管理】成功！");
					}
				} else {
					message("【申请发票张数】不正确！");
				}
				crmContrctBillGrid.startEditing(0, 0);
			}
		}else{
			message("先删除【合同发票管理】中的数据！");
		}
		}
	
	function selcetProjectNodeFun(){
			var win = Ext.getCmp('ProjectProgressFun');
			if (win) {win.close();}
			var ProjectProgressFun= new Ext.Window({
			id:'ProjectProgressFun',modal:true,title:'选择任务进度',layout:'fit',width:600,height:580,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='/exp/project/project/processSelect.action?projectId="+$("#crmContract_project").val()+"&flag=ProjectProgressFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 ProjectProgressFun.close(); }  }]  });     ProjectProgressFun.show(); }
			 function setProjectProgressFun(rec){
				 var gridGrid =crmContrctPayGrid;
					var selRecords = gridGrid.getSelectionModel().getSelections(); 
					$.each(selRecords, function(i, obj) {
						obj.set('projectNode-id',rec.get("id"));
						obj.set('projectNode-progress',rec.get("progress"));
					});
			var win = Ext.getCmp('ProjectProgressFun');
			if(win){win.close();}
		}
			 
		function countBillMoney(){
			CheckCountBillMoneyData();
			var gridGrid=crmContrctPayGrid;
			var sum=0;
			var selectRecord=gridGrid.getSelectionModel().getSelections();
			
			if(selectRecord && selectRecord.length>0){
				for(var i=0;i<selectRecord.length;i++){
					sum=Number(sum)+Number(selectRecord[i].get("money"));
				}
				var ob=crmContrctPayGrid.store.recordType;
				crmContrctPayGrid.stopEditing();
				var p=new ob({});
				p.isNew=true;
				p.set("name", "合计");
				p.set("money", sum);
				crmContrctPayGrid.getStore().add(p);
			}else{
				message("请先选中几条数据");
			}
		}
		
		function CheckCountBillMoneyData(){
			var temp=crmContrctPayGrid.getStore();
			for(var i=0;i<temp.getCount();i++){
				if(temp.getAt(i).get("name")=="合计"){
					temp.remove(temp.getAt(i));
				}
			}
		}

