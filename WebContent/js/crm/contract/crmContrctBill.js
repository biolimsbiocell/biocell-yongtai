﻿var crmContrctBillGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'currency',
		type:"string"
	});
	   fields.push({
		name:'isBill',
		type:"string"
	});
	   fields.push({
		name:'required',
		type:"string"
	});
	   fields.push({
		name:'billType',
		type:"string"
	});
	   fields.push({
		name:'incomeCheck',
		type:"string"
	});
	   fields.push({
		name:'accountPeriod',
		type:"string"
	});
	   fields.push({
		name:'billDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
	   name:'applyBillDate',
	   type:"date",
	   dateFormat:"Y-m-d"
   });
	   fields.push({
		name:'billMoney',
		type:"string"
	});
	   fields.push({
			name:'receivedMoney',
			type:"string"
	});
	   fields.push({
		name:'billCode',
		type:"string"
	});
	   fields.push({
		name:'billId',
		type:"string"
	});
	   fields.push({
		name:'details',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
			name:'attention',
			type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'note2',
			type:"string"
	});
	   fields.push({
		name:'crmContract-id',
		type:"string"
	});
	   fields.push({
		name:'crmContract-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:15*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var selectIsBill = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '否'
			}, {
				id : '1',
				name : '是'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'isBill',
		hidden : true,
		header : '是否开票',
		width : 10 * 10,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(selectIsBill),
		editor : selectIsBill
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'期数',
		width:10*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:false
		})
	});
	cm.push({
		dataIndex:'billCode',
		hidden : false,
		header:'发票抬头',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'details',
		hidden : false,
		header:'明细',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'billMoney',
		hidden : false,
		header:'金额（元）',
		width:20*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'receivedMoney',
		hidden : false,
		header:'已收金额（元）',
		width:20*6,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
	var selectCurrency = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '人民币'
			},{
				id : '1',
				name : '美元'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'currency',
		hidden : false,
		header:'币种',
		width:15*6,
		sortable : true,
		renderer: Ext.util.Format.comboRenderer(selectCurrency),editor: selectCurrency
	});
	cm.push({
		dataIndex:'required',
		hidden : false,
		header:'开票要求',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'attention',
		hidden : false,
		header : '注意事项',
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var selectBillType = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '普通发票'
			},{
				id : '1',
				name : '增值税发票'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'billType',
		hidden : false,
		header:'发票类型',
		width:15*6,
		sortable : true,
		renderer: Ext.util.Format.comboRenderer(selectBillType),editor: selectBillType
	});
	cm.push({
		dataIndex:'applyBillDate',
		hidden : false,
		header:'申请开票日期',
		width:15*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	var selectIncomeCheck = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '是'
			},{
				id : '1',
				name : '否'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
//	cm.push({
//		dataIndex:'incomeCheck',
//		hidden : false,
//		header:'技术性收入核定',
//		width:12*8,
//		renderer: Ext.util.Format.comboRenderer(selectIncomeCheck),editor: selectIncomeCheck
//	});
	cm.push({
		dataIndex:'billDate',
		hidden : false,
		header:'开票日期',
		width:15*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'billId',
		hidden : false,
		header:'发票号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'accountPeriod',
		hidden : false,
		header:'账期（天）',
		width:15*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'note2',
		hidden : false,
		header:'备注2',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/contract/crmContract/showCrmContrctBillListByHostTableJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="合同发票管理";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/contract/crmContract/delCrmContrctBill.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
 
	 if($("#crmContract_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
	   } else{
				/*opts.tbar.push({
						text : '选择相关主表',
						handler : selectcrmContractFun
					});*/
			opts.tbar.push({
				text : '填加明细',
				iconCls : 'add',
				handler : function() {
					var ob = crmContrctBillGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;
					crmContrctBillGrid.stopEditing();
					p.set("receivedMoney", "0");
					crmContrctBillGrid.getStore().insert(0, p);
					crmContrctBillGrid.startEditing(0, 0);
				}
			});
				opts.tbar.push({
					text : '显示可编辑列',
					handler : null
				});
				opts.tbar.push({
					text : '取消选中',
					handler : null
				});
				opts.tbar.push({
					text : '计算账期',
					handler : countAccountPeriod
				});
				opts.tbar.push({
					text : '计算开票总额',
					handler : countBillMoney
				});
				opts.tbar.push({
					text : "批量上传（CSV文件）",
					handler : function() {
						var options = {};
						options.width = 350;
						options.height = 200;
						loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
							"确定":function(){
								goInExcelcsv();
								$(this).dialog("close");
							}
						},true,options);
					}
				});
	   }
	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = crmContrctBillGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							crmContrctBillGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	
	
	crmContrctBillGrid=gridEditTable("crmContrctBilldiv",cols,loadParam,opts);
	$("#crmContrctBilldiv").data("crmContrctBillGrid", crmContrctBillGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectcrmContractFun(){
	var win = Ext.getCmp('selectcrmContract');
	if (win) {win.close();}
	var selectcrmContract= new Ext.Window({
	id:'selectcrmContract',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/CrmContractSelect.action?flag=crmContract' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectcrmContract.close(); }  }]  });     selectcrmContract.show(); }
	function setcrmContract(id,name){
		var gridGrid = $("#crmContrctBilldiv").data("crmContrctBillGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('crmContract-id',id);
			obj.set('crmContract-name',name);
		});
		var win = Ext.getCmp('selectcrmContract');
		if(win){
			win.close();
		}
	}
	//计算账期
	function countAccountPeriod(){
		var grid = crmContrctBillGrid.store;
		for(var i=0;i<grid.getCount();i++){
			var C = grid.getAt(i).get("billDate");
			var A = new Date();
			var V = (A-C)/(24 * 60 * 60 * 1000);
			grid.getAt(i).set("accountPeriod",parseInt(V.toFixed(2)));
		} 
	}
	
	function countBillMoney() {
		CheckCountcountBillMoneyData();
		var gridGrid = crmContrctBillGrid; // 导入目的的子表
		var sum = 0;
		var sum2=0;
		var selRecords = gridGrid.getSelectionModel().getSelections();
		if (selRecords && selRecords.length > 0) {
			for ( var i = 0; i < selRecords.length; i++) {
				// alert(selRecords[i].get("money"));
				sum = Number(sum) + Number(selRecords[i].get("billMoney"));
				sum2=Number(sum2) + Number(selRecords[i].get("receivedMoney"));
			}
			var ob = crmContrctBillGrid.store.recordType;
			crmContrctBillGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("name", "总计");
			p.set("billMoney", sum);
			p.set("receivedMoney", sum2);
			crmContrctBillGrid.getStore().add(p);
		} else {
			message("请先选中数据！");
		}
	}
	
	function CheckCountcountBillMoneyData() {
		var temp = crmContrctBillGrid.getStore();
		for ( var i = 0; i < temp.getCount(); i++) {
			if (temp.getAt(i).get("name") == "总计") {
				temp.remove(temp.getAt(i));
			}
		}
	}
	
