var crmContractGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'createOrder',
		type : "string"
	});
	fields.push({
		name : 'geneByName',
		type : "string"
	});
	fields.push({
		name : 'geneName',
		type : "string"
	});
	fields.push({
		name : 'geneId',
		type : "string"
	});
	fields.push({
		name : 'project-id',
		type : "string"
	});
	fields.push({
		name : 'project-name',
		type : "string"
	});
	fields.push({
		name : 'conState-id',
		type : "string"
	});
	fields.push({
		name : 'conState-name',
		type : "string"
	});
	fields.push({
		name : 'conMoneyState-id',
		type : 'string'
	});
	fields.push({
		name : 'conMoneyState-name',
		type : 'string'
	});
	fields.push({
		name : 'projectType-id',
		type : "string"
	});
	fields.push({
		name : 'projectType-name',
		type : "string"
	});
	fields.push({
		name : 'customerProperty',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'customerRequirements',
		type : "string"
	});
	fields.push({
		name : 'crmCustomer-id',
		type : "string"
	});
	fields.push({
		name : 'crmCustomer-name',
		type : "string"
	});
//	fields.push({
//		name : 'crmCustomerDept-id',
//		type : "string"
//	});
//	fields.push({
//		name : 'crmCustomerDept-name',
//		type : "string"
//	});
	fields.push({
		name : 'writeDate',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'factStartDate',
		type : "string"
	});
	fields.push({
		name : 'factEndDate',
		type : "string"
	});
	fields.push({
		name : 'type-id',
		type : "string"
	});
	fields.push({
		name : 'type-name',
		type : "string"
	});
	fields.push({
		name : 'productType-id',
		type : "string"
	});
	fields.push({
		name : 'productType-name',
		type : "string"
	});
	fields.push({
		name : 'workType-id',
		type : "string"
	});
	fields.push({
		name : 'workType-name',
		type : "string"
	});
	fields.push({
		name : 'genus-id',
		type : "string"
	});
	fields.push({
		name : 'genus-name',
		type : "string"
	});
	fields.push({
		name : 'strain-id',
		type : "string"
	});
	fields.push({
		name : 'strain-name',
		type : "string"
	});
	fields.push({
		name : 'geneTargetingType-id',
		type : "string"
	});
	fields.push({
		name : 'geneTargetingType-name',
		type : "string"
	});
	fields.push({
		name : 'technologyType-id',
		type : "string"
	});
	fields.push({
		name : 'technologyType-name',
		type : "string"
	});
	fields.push({
		name : 'productNo',
		type : "string"
	});
	fields.push({
		name : 'neoResult',
		type : "string"
	});
	fields.push({
		name : 'checkup-id',
		type : "string"
	});
	fields.push({
		name : 'checkup-name',
		type : "string"
	});
	fields.push({
		name : 'product',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'organization',
		type : "string"
	});
	fields.push({
		name : 'money',
		type : "string"
	});
	fields.push({
		name : 'crmSaleOrder-id',
		type : "string"
	});
	fields.push({
		name : 'crmSaleOrder-name',
		type : "string"
	});
	fields.push({
		name : 'parent-id',
		type : "string"
	});
	fields.push({
		name : 'parent-name',
		type : "string"
	});
	fields.push({
		name : 'currency',
		type : "string"
	});
	fields.push({
		name : 'periodsNum',
		type : "string"
	});
	fields.push({
		name : 'crmContractPay-id',
		type : "string"
	});
	fields.push({
		name : 'crmContractPay-name',
		type : "string"
	});
	fields.push({
		name : 'manager-id',
		type : "string"
	});
	fields.push({
		name : 'manager-name',
		type : "string"
	});
	fields.push({
		name : 'isRecord',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-id',
		type : "string"
	});
	fields.push({
		name : 'confirmUser-name',
		type : "string"
	});
	fields.push({
		name : 'confirmDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'conStateName',
		type : "string"
	});
	//合同来源
	fields.push({
		name : 'chanceSource-id',
		type : "string"
	});
	fields.push({
		name : 'chanceSource-name',
		type : "string"
	});
	fields.push({
		name : 'orderRecipient',
		type : "string"
	});
	fields.push({
		name : 'content1',
		type : "string"
	});
	fields.push({
		name : 'content2',
		type : "string"
	});
	fields.push({
		name : 'content3',
		type : "string"
	});
	fields.push({
		name : 'content4',
		type : "string"
	});
	fields.push({
		name : 'content5',
		type : "string"
	});
	fields.push({
		name : 'content6',
		type : "string"
	});
	fields.push({
		name : 'content7',
		type : "string"
	});
	fields.push({
		name : 'content8',
		type : "string"
	});
	fields.push({
		name : 'content9',
		type : "string"
	});
	fields.push({
		name : 'content10',
		type : "string"
	});
	fields.push({
		name : 'content11',
		type : "string"
	});
	fields.push({
		name : 'content12',
		type : "string"
	});
	fields.push({
		name : 'orderNum',
		type : "string"
	});
	fields.push({
		name : 'client',
		type : "string"
	});
	fields.push({
		name : 'identifiers',
		type : "string"
	});
	fields.push({
		name : 'kind-id',
		type : "string"
	});
	fields.push({
		name : 'kind-name',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'conStateName',
		header : '生成订单',
		width : 20 * 4,
		hidden : true,
		sortable : true
	});

	/*cm.push({
		dataIndex : 'conState',
		header : '合同签订状态',
		width : 20 * 4,
		hidden : false,
		sortable : true,
		renderer : function(value, cellmeta, record, rowIndex, columnIndex,
				store) {
			if (record.data['toDate'] && record.data['fromDate']) {
				return "已签订";
			}else{
				return "签订中";
			}
		}
	});*/

	cm.push({
		dataIndex : 'id',
		header : '编码',
		width : 18 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'writeDate',
		header : '签订日期',
		width : 15 * 6,
		sortable : true
	});
	
	
	/*cm.push({
		dataIndex : 'projectType-id',
		hidden : true,
		header : '项目类型ID',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'projectType-name',
		header : '项目类型',
		width : 20 * 6,
		sortable : true
	});*/
	cm.push({
		dataIndex : 'manager-id',
		hidden : true,
		header : '客户经理ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'manager-name',
		header : '客户经理',
		width : 15 * 6,
		sortable : true
	});
	
	//合同来源
	cm.push({
		dataIndex : 'chanceSource-id',
		hidden : true,
		header : '合同来源ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'chanceSource-name',
		header : '合同来源',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'name',
		header : '合同名称',
		width : 50 * 4,
		sortable : true
	});
	cm.push({
		dataIndex : 'identifiers',
		header : '合同编号',
		width : 50 * 4,
		sortable : true
	});
	cm.push({
		dataIndex : 'money',
		header : '合同总金额(元)',
		width : 20 * 5,
		sortable : true
	});
	cm.push({
		dataIndex : 'currency',
		hidden : false,
		header : '币种',
		width : 10 * 6,
		editor : new Ext.form.NumberField({
			allowDecimals : true
		// decimalPrecision:2
		})
	});
	cm.push({
		dataIndex : 'crmCustomer-id',
		hidden : true,
		header : '委托人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmCustomer-name',
		header : '委托人',
		width : 10 * 6,
		sortable : true
	});
//	cm.push({
//		dataIndex : 'crmCustomerDept-id',
//		hidden : true,
//		header : '委托单位ID',
//		width : 15 * 10,
//		sortable : true
//	});
//	cm.push({
//		dataIndex : 'crmCustomerDept-name',
//		header : '委托人单位',
//		width : 15 * 6,
//		sortable : true
//	});
	cm.push({
		dataIndex : 'client',
		header : '委托单位',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'startDate',
		header : '合同开始日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : '合同结束日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'factStartDate',
		header : '实际合同开始日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'factEndDate',
		header : '实际合同结束日期',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-id',
		hidden : true,
		header : '合同类型ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'type-name',
		header : '合同类型',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'kind-id',
		hidden : true,
		header : '合同种类ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'kind-name',
		header : '合同种类',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'conState-id',
		hidden : true,
		header : '合同实施状态ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'conState-name',
		header : '合同实施状态',
		width : 10 * 10,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'conMoneyState-id',
		hidden : true,
		header : '合同款使用状态ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'conMoneyState-name',
		header : '合同款使用状态',
		width : 10 * 10,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'periodsNum',
		hidden : false,
		header : '收款期数',
		width : 10 * 6,
		editor : new Ext.form.NumberField({
			allowDecimals : true
		})
	});
	cm.push({
		dataIndex : 'geneId',
		hidden : true,
		header : '基因ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneByName',
		hidden : true,
		header : '基因别名',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneName',
		hidden : true,
		header : '基因正式名称',
		width : 8 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'customerProperty',
		header : '客户性质',
		width : 10 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'workType-id',
		hidden : true,
		header : '项目类型ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'workType-name',
		header : '项目类型',
		width : 10 * 10,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'productType-id',
		hidden : true,
		header : '产品类型ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'productType-name',
		header : '产品类型',
		hidden : true,
		width : 10 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'genus-id',
		hidden : true,
		header : '种属ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'genus-name',
		header : '种属',
		width : 10 * 10,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'strain-id',
		hidden : true,
		header : '品系ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'strain-name',
		header : '品系',
		width : 10 * 10,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'technologyType-id',
		hidden : true,
		header : '技术类型ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'technologyType-name',
		header : '技术类型',
		width : 10 * 10,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneTargetingType-id',
		hidden : true,
		header : '基因打靶类型ID',
		width : 6 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'geneTargetingType-name',
		header : '基因打靶类型',
		hidden : true,
		width : 10 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'project-id',
		header : '项目ID',
		hidden : true,
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'project-name',
		header : '项目名称',
		width : 15 * 8,
		hidden : true,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'product',
		hidden : true,
		header : '终产品ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'productName',
		header : '终产品',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'productNo',
		header : '终产品数量',
		width : 10 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'customerRequirements',
		header : '客户要求',
		width : 15 * 10,
		sortable : true
	});

	// var isRecordstore = new Ext.data.ArrayStore({
	// fields : [ 'id', 'name' ],
	// data : [ [ '1', '北京百奥赛图基因生物技术有限公司' ], [ '0', '百奥赛图江苏基因生物技术有限公司' ] ]
	// });
	//
	// var isRecordComboxFun = new Ext.form.ComboBox({
	// store : isRecordstore,
	// displayField : 'name',
	// valueField : 'id',
	// typeAhead : true,
	// mode : 'local',
	// forceSelection : true,
	// triggerAction : 'all',
	// emptyText : '',
	// selectOnFocus : true
	// });
	cm.push({
		dataIndex : 'organization',
		header : '受托方（乙方）',
		width : 20 * 6,
		// renderer : Ext.util.Format.comboRenderer(isRecordComboxFun),
		sortable : true
	});

	cm.push({
		dataIndex : 'crmSaleOrder-id',
		hidden : true,
		header : '订单ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'crmSaleOrder-name',
		header : '订单',
		hidden : true,
		width : 15 * 10,
		sortable : true
	});
	var isCreatestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});

	var isCreateComboxFun = new Ext.form.ComboBox({
		store : isCreatestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'createOrder',
		header : '创建订单',
		width : 20 * 5,
		hidden : true,
		renderer : Ext.util.Format.comboRenderer(isCreateComboxFun),
		sortable : true
	});
	var isRecordstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	
	var isRecordComboxFun = new Ext.form.ComboBox({
		store : isRecordstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'neoResult',
		header : '是否去Neo',
		width : 20 * 5,
		hidden : true,
		renderer : Ext.util.Format.comboRenderer(isRecordComboxFun),
		sortable : true
	});
	/*var isCheckUpstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '需要' ], [ '0', '不需要' ] ]
	});

	var isCheckUpstoreFun = new Ext.form.ComboBox({
		store : isCheckUpstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
	cm.push({
		dataIndex : 'checkup-id',
		header : '质控要求ID',
		width : 20 * 6,
		hidden:true,
//		renderer : Ext.util.Format.comboRenderer(isCheckUpstoreFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'checkup-name',
		header : '质控要求',
		width : 20 * 6,
		hidden : true,
//		renderer : Ext.util.Format.comboRenderer(isCheckUpstoreFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'parent-id',
		hidden : true,
		header : '父级合同ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'parent-name',
		header : '父级合同',

		width : 15 * 10,
		sortable : true
	});
	// cm.push({
	// dataIndex:'crmContractPay-id',
	// hidden:true,
	// header:'付款计划ID',
	// width:15*10,
	// sortable:true
	// });
	// cm.push({
	// dataIndex:'crmContractPay-name',
	// header:'付款计划',
	//		
	// width:15*10,
	// sortable:true
	// });

	var isRecordstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});

	var isRecordComboxFun = new Ext.form.ComboBox({
		store : isRecordstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'isRecord',
		header : '是否备案',
		width : 10 * 6,
		renderer : Ext.util.Format.comboRenderer(isRecordComboxFun),
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : '创建人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : '创建人',

		width : 10 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : '创建日期',
		width : 15 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-id',
		hidden : true,
		header : '审核人ID',
		width : 15 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmUser-name',
		header : '审核人',
		width : 15 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'confirmDate',
		header : '审核日期',
		width : 15 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : '工作流状态',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '工作流状态',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderNum',
		header : '订单数量',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'orderRecipient',
		header : '订单接收人',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content1',
		header : 'content1',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content2',
		header : 'content2',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content3',
		header : 'content3',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content4',
		header : 'content4',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content5',
		header : 'content5',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content6',
		header : 'content6',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content7',
		header : 'content7',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content8',
		header : 'content8',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content9',
		header : 'content9',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content10',
		header : 'content10',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content11',
		header : 'content11',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'content12',
		header : 'content12',
		width : 15 * 6,
		hidden : true,
		sortable : true
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/crm/contract/crmContract/showCrmContractListJson.action";
	var opts = {};
	opts.title = "合同管理";
	opts.height = document.body.clientHeight - 34;

	opts.tbar = [];
//	opts.tbar.push({
//		text : "回款明细",
//		handler : selectReturnMoney
//	});
//
//	opts.tbar.push({
//		text : "发票管理",
//		handler : invoiceManagement
//	});
//	
//	opts.tbar.push({
//		text : "计算合同总金额",
//		handler : sumMoney
//	});

	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	crmContractGrid = gridTable("show_crmContract_div", cols, loadParam, opts);
})
function add() {
	window.location = window.ctx
			+ '/crm/contract/crmContract/editCrmContract.action';
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/crm/contract/crmContract/editCrmContract.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx
			+ '/crm/contract/crmContract/viewCrmContract.action?id=' + id;
}
function exportexcel() {
//	var rows=Ext.getCmp("Grid_ID").getSelectionModel().getSelections();
//	if(rows==null){
//		message("请先选择要导出的数据！");
//		return false;
//	}
//	rows.cm=cm;
//	crmContractGrid = gridTable("show_crmContract_div", rows, loadParam, opts);
	crmContractGrid.title = '导出列表';
	var vExportContent = crmContractGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(
			function() {
				var option = {};
				option.width = 800;
				option.height = 417;
				loadDialogPage($("#jstj"), "搜索", null, {
					"开始检索" : function() {
						//合同签订日期搜索
						if (($("#startwriteDate").val() != undefined)
								&& ($("#startwriteDate").val() != '')) {
							var startwriteDatestr = ">=##@@##"
									+ $("#startwriteDate").val();
							$("#writeDate1").val(startwriteDatestr);
						}
						if (($("#endwriteDate").val() != undefined)
								&& ($("#endwriteDate").val() != '')) {
							var endwriteDatestr = "<=##@@##"
									+ $("#endwriteDate").val();

							$("#writeDate2").val(endwriteDatestr);

						}

						if (($("#startstartDate").val() != undefined)
								&& ($("#startstartDate").val() != '')) {
							var startstartDatestr = ">=##@@##"
									+ $("#startstartDate").val();
							$("#startDate1").val(startstartDatestr);
						}
						if (($("#endstartDate").val() != undefined)
								&& ($("#endstartDate").val() != '')) {
							var endstartDatestr = "<=##@@##"
									+ $("#endstartDate").val();

							$("#startDate2").val(endstartDatestr);

						}

						if (($("#startendDate").val() != undefined)
								&& ($("#startendDate").val() != '')) {
							var startendDatestr = ">=##@@##"
									+ $("#startendDate").val();
							$("#endDate1").val(startendDatestr);
						}
						if (($("#endendDate").val() != undefined)
								&& ($("#endendDate").val() != '')) {
							var endendDatestr = "<=##@@##"
									+ $("#endendDate").val();

							$("#endDate2").val(endendDatestr);

						}

						if (($("#startcreateDate").val() != undefined)
								&& ($("#startcreateDate").val() != '')) {
							var startcreateDatestr = ">=##@@##"
									+ $("#startcreateDate").val();
							$("#createDate1").val(startcreateDatestr);
						}
						if (($("#endcreateDate").val() != undefined)
								&& ($("#endcreateDate").val() != '')) {
							var endcreateDatestr = "<=##@@##"
									+ $("#endcreateDate").val();

							$("#createDate2").val(endcreateDatestr);

						}

						if (($("#startconfirmDate").val() != undefined)
								&& ($("#startconfirmDate").val() != '')) {
							var startconfirmDatestr = ">=##@@##"
									+ $("#startconfirmDate").val();
							$("#confirmDate1").val(startconfirmDatestr);
						}
						if (($("#endconfirmDate").val() != undefined)
								&& ($("#endconfirmDate").val() != '')) {
							var endconfirmDatestr = "<=##@@##"
									+ $("#endconfirmDate").val();

							$("#confirmDate2").val(endconfirmDatestr);

						}
						
						if (($("#startfactStartDate").val() != undefined)
								&& ($("#startfactStartDate").val() != '')) {
							var startfactStartDatestr = ">=##@@##"
									+ $("#startfactStartDate").val();
							$("#factStartDate1").val(startfactStartDatestr);
						}
						if (($("#endfactStartDate").val() != undefined)
								&& ($("#endfactStartDate").val() != '')) {
							var endfactStartDatestr = "<=##@@##"
									+ $("#endfactStartDate").val();
							$("#factStartDate2").val(endfactStartDatestr);
						}
						
						if (($("#startfactEndDate").val() != undefined)
								&& ($("#startfactEndDate").val() != '')) {
							var startfactEndDatestr = ">=##@@##"
									+ $("#startfactEndDate").val();
							$("#factEndDate1").val(startfactEndDatestr);
						}
						if (($("#endfactEndDate").val() != undefined)
								&& ($("#endfactEndDate").val() != '')) {
							var endfactEndDatestr = "<=##@@##"
									+ $("#endfactEndDate").val();
							$("#factEndDate2").val(endfactEndDatestr);
						}
						
						if (($("#startcontent9").val() != undefined)
								&& ($("#startcontent9").val() != '')) {
							var startcontent9str = ">=##@@##"
									+ $("#startcontent9").val();
							$("#content91").val(startcontent9str);
						}
						if (($("#endcontent9").val() != undefined)
								&& ($("#endcontent9").val() != '')) {
							var endcontent9str = "<=##@@##"
									+ $("#endcontent9").val();

							$("#content92").val(endcontent9str);

						}

						if (($("#startcontent10").val() != undefined)
								&& ($("#startcontent10").val() != '')) {
							var startcontent10str = ">=##@@##"
									+ $("#startcontent10").val();
							$("#content101").val(startcontent10str);
						}
						if (($("#endcontent10").val() != undefined)
								&& ($("#endcontent10").val() != '')) {
							var endcontent10str = "<=##@@##"
									+ $("#endcontent10").val();

							$("#content102").val(endcontent10str);

						}

						if (($("#startcontent11").val() != undefined)
								&& ($("#startcontent11").val() != '')) {
							var startcontent11str = ">=##@@##"
									+ $("#startcontent11").val();
							$("#content111").val(startcontent11str);
						}
						if (($("#endcontent11").val() != undefined)
								&& ($("#endcontent11").val() != '')) {
							var endcontent11str = "<=##@@##"
									+ $("#endcontent11").val();

							$("#content112").val(endcontent11str);

						}

						if (($("#startcontent12").val() != undefined)
								&& ($("#startcontent12").val() != '')) {
							var startcontent12str = ">=##@@##"
									+ $("#startcontent12").val();
							$("#content121").val(startcontent12str);
						}
						if (($("#endcontent12").val() != undefined)
								&& ($("#endcontent12").val() != '')) {
							var endcontent12str = "<=##@@##"
									+ $("#endcontent12").val();

							$("#content122").val(endcontent12str);

						}

						commonSearchAction(crmContractGrid);
						$(this).dialog("close");

					},
					"清空" : function() {
						form_reset();

					}
				}, true, option);
			});
	
});
Ext.onReady(function() {
	var item = menu.add({
		text : '列表模式'
	});
	item.on('click', lbms);

});
function lbms() {
	var url = ctx + "/crm/contract/crmContract/showCrmContractList.action";
	location.href = url;
}
Ext.onReady(function() {
	var item1 = menu.add({
		text : '树状模式'
	});
	item1.on('click', szms);
});
function szms() {
	var url = ctx + "/crm/contract/crmContract/showCrmContractTree.action";
	location.href = url;
}

// 回款明细
function selectReturnMoney() {
	window.location = window.ctx
			+ "/crm/contract/crmContract/showCrmContractCollectItemList.action";
}

// 发票管理
function invoiceManagement() {
	window.location = window.ctx
			+ "/crm/contract/crmContract/showCrmContractBillItemList.action";
}

//计算合同总金额
function sumMoney(){
	CheckCountPaymentsData();
	var gridGrid = crmContractGrid; // 导入目的的子表
	var sum = 0;
	var selRecords = gridGrid.getSelectionModel().getSelections();
	if (selRecords && selRecords.length > 0) {
		for ( var i = 0; i < selRecords.length; i++) {
			// alert(selRecords[i].get("money"));
			sum = Number(sum) + Number(selRecords[i].get("money"));
		}
		var ob = crmContractGrid.store.recordType;
		crmContractGrid.stopEditing();
		var p = new ob({});
		p.isNew = true;
		//p.set("collectDept", "");
		//p.set("receivedDate", new Date());
		p.set("conStateName", "总计（元）");
		p.set("money", sum);
		crmContractGrid.getStore().add(p);
	}else {
		message("请选中几条数据");
	}
}

function CheckCountPaymentsData() {
	var temp = crmContractGrid.getStore();
	for ( var i = 0; i < temp.getCount(); i++) {
		if (temp.getAt(i).get("conStateName") == "总计（元）") {
			temp.remove(temp.getAt(i));
		}
	}
}



