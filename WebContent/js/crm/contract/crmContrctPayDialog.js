﻿
var crmContrctPayDialogGrid;
$(function(){
	var cols={};
	cols.sm = false;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'name',
			type:"string"
		});
	   fields.push({
		name:'money',
		type:"string"
	});
	   fields.push({
		name:'billDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'billMoney',
		type:"string"
	});
	   fields.push({
		name:'billId',
		type:"string"
	});
	   fields.push({
			name:'num',
			type:"string"
		});
	   fields.push({
			name:'projectNode',
			type:"date"
		});
	   fields.push({
			name:'remind',
			type:"string"
		});
//	   fields.push({
//			name:'scaling',
//			type:"string"
//		});
//	   fields.push({
//			name:'totalMoney',
//			type:"string"
//		});
//	   filelds.push({
//		   name : 'periodsNum',
//		   type : "string"
//	   });
//	   fields.push({
//			name:'currency',
//			type:"string"
//		});
	   fields.push({
		name:'receivedDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'planReceivedDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'crmContract-id',
		type:"string"
	});
	    fields.push({
		name:'crmContract-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6
	});
//	cm.push({
//		dataIndex:'periodsNum',
//		hidden : false,
//		header:'收款期数',
//		width:10*6,
//		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true
//			//decimalPrecision:2
//		})
//	});
	cm.push({
		dataIndex:'money',
		hidden : false,
		header:'每期金额（元）',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true
			//decimalPrecision:2
		})
	});
//	cm.push({
//		dataIndex:'totalMoney',
//		hidden : false,
//		header:'合同总金额（元）',
//		width:20*6,
//		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true
//			//decimalPrecision:2
//		})
//	});
//	var selectCurrency = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '0',
//				name : '人民币'
//			},{
//				id : '1',
//				name : '美元'
//			} ]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'currency',
//		hidden : false,
//		header:'币种',
//		width:15*10,
//		renderer: Ext.util.Format.comboRenderer(selectCurrency),editor: selectCurrency
//	});
//	cm.push({
//		dataIndex:'scaling',
//		hidden : false,
//		header:'收款比例',
//		width:20*6,
//		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true
//			//decimalPrecision:2
//		})
//	});
	cm.push({
		dataIndex:'projectNode',
		hidden : false,
		header:'项目节点',
		width:25*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	/*var remind = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : '开票申请'
			},{
				id : '1',
				name : '催款'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'remind',
		hidden : false,
		header:'到达节点提醒',
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(remind),editor: remind
	});
	cm.push({
		dataIndex:'billDate',
		hidden : false,
		header:'开票日期',
		width:25*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'billMoney',
		hidden : false,
		header:'开票金额',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'billId',
		hidden : false,
		header:'发票号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'申请开票数量（张）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'request',
		hidden : false,
		header:'开票要求',
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'receivedDate',
		hidden : false,
		header:'回款日期',
		width:15*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});*/
	cm.push({
		dataIndex:'planReceivedDate',
		hidden : false,
		header:'计划回款日期',
		width:15*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'crmContract-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/contract/crmContract/showCrmContrctPayListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="选择合同付款计划";
	opts.height =  document.body.clientHeight;
	
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setCrmContractBillFun(rec);
	};
 
	
	crmContrctPayDialogGrid=gridTable("crmContrctPayDialogdiv",cols,loadParam,opts);
})
