var applicationTypeActionLogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'applicationTypeTableId',
		type:"string"
	});
	   fields.push({
		name:'contentId',
		type:"string"
	});
	   fields.push({
		name:'oldActionName',
		type:"string"
	});
	   fields.push({
		name:'actionName',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'nextNote',
			type:"string"
		});
	    fields.push({
		name:'operUser-id',
		type:"string"
	});
	    fields.push({
		name:'operUser-name',
		type:"string"
	});
	   fields.push({
		name:'operDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
			name:'startDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'endDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : false,
		header:'编码',
		width:20*10,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'applicationTypeTableId',
		hidden : false,
		header:'关联模块',
		width:15*10,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contentId',
		hidden : false,
		header:'内容id',
		width:15*10,
		hidden:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'oldActionName',
		hidden : false,
		header:'原状态',
		width:50,
		hidden:true
	});
	cm.push({
		dataIndex:'actionName',
		hidden : false,
		header:'状态',
		width:150
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'当前进展',
		width:30*10,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextNote',
		hidden : false,
		header:'下一步计划',
		width:30*10,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'operUser-id',
		hidden : true,
		header:'操作人ID',
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'operUser-name',
		hidden : false,
		header:'操作人',
		width:10*10
	});
	cm.push({
		dataIndex:'operDate',
		hidden : false,
		header:'操作日期',
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'startDate',
		hidden : false,
		header:'开始日期',
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'endDate',
		hidden : false,
		header:'结束日期',
		width:12*10,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/overseas/showApplicationTypeActionLogListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="过程记录";
	opts.height =  document.body.clientHeight-250;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/crm/overseas/delApplicationTypeActionLog.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
			text : '选择操作人',
			handler : selectoperUserFun
		});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	applicationTypeActionLogGrid=gridEditTable("applicationTypeActionLogdiv",cols,loadParam,opts);
	$("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid", applicationTypeActionLogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectoperUserFun(){
	var win = Ext.getCmp('selectoperUser');
	if (win) {win.close();}
	var selectoperUser= new Ext.Window({
	id:'selectoperUser',modal:true,title:'选择操作人',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=operUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectoperUser.close(); }  }]  });     selectoperUser.show(); }
	function setoperUser(id,name){
		var gridGrid = $("#applicationTypeActionLogdiv").data("applicationTypeActionLogGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('operUser-id',id);
			obj.set('operUser-name',name);
		});
		var win = Ext.getCmp('selectoperUser')
		if(win){
			win.close();
		}
	}
	
