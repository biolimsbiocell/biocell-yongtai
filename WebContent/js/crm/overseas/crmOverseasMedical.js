var crmOverseasMedicalGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'patient-id',
		type:"string"
	});
	    fields.push({
		name:'patient-name',
		type:"string"
	});
	    fields.push({
		name:'supplier-id',
		type:"string"
	});
	    fields.push({
		name:'supplier-name',
		type:"string"
	});
	    fields.push({
		name:'customer-id',
		type:"string"
	});
	    fields.push({
		name:'customer-name',
		type:"string"
	});
	    fields.push({
		name:'passportNo',
		type:"string"
	});
	    fields.push({
		name:'remark',
		type:"string"
	});
	    fields.push({
		name:'effect',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'描述',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'patient-id',
		hidden:true,
		header:'病人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'patient-name',
		header:'病人',
		width:10*10,
		sortable:true
		});
		/*cm.push({
		dataIndex:'supplier-id',
		hidden:true,
		header:'海外医疗中心ID',
		width:15*10,
		sortable:true
		});*/
		cm.push({
		dataIndex:'supplier-name',
		header:'海外医疗中心',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'customer-id',
		hidden:true,
		header:'渠道推荐ID',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'effect',
		header:'就医效果',
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'创建人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:12*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'stateName',
		header:'状态名称',
		width:150,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/crm/overseas/showCrmOverseasMedicalListJson.action";
	var opts={};
	opts.title="海外就医";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	crmOverseasMedicalGrid=gridTable("show_crmOverseasMedical_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/crm/overseas/editCrmOverseasMedical.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/crm/overseas/editCrmOverseasMedical.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/crm/overseas/viewCrmOverseasMedical.action?id=' + id;
}
function exportexcel() {
	crmOverseasMedicalGrid.title = '导出列表';
	var vExportContent = crmOverseasMedicalGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				
				if (($("#startCreateDate").val() != undefined) && ($("#startCreateDate").val() != '')) {
					var startReceiveDatestr = ">=##@@##" + $("#startCreateDate").val();

					$("#createDate1").val(startReceiveDatestr);
				}
				if (($("#endCreateDate").val() != undefined) && ($("#endCreateDate").val() != '')) {
					var endReceiveDatestr = "<=##@@##" + $("#endCreateDate").val();

					$("#createDate2").val(endReceiveDatestr);

				}
				commonSearchAction(crmOverseasMedicalGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});