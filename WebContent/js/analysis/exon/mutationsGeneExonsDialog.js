var mutationsGeneExonsDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'mutationGenes',
		type:"string"
	});
	    fields.push({
		name:'transcript',
		type:"string"
	});
	    fields.push({
		name:'totleNumberExon',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'mutationGenes',
		header:'突变基因',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'transcript',
		header:'转录本',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'totleNumberExon',
		header:'总外显子数',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:'时间',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:'创建人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态描述',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/exon/mutationsGeneExons/showMutationsGeneExonsListJson.action";
	var opts={};
	opts.title="突变基因转录本及总外显子表";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setMutationsGeneExonsFun(rec);
	};
	mutationsGeneExonsDialogGrid=gridTable("show_dialog_mutationsGeneExons_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(mutationsGeneExonsDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
