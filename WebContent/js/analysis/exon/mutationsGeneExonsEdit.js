$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/analysis/exon/editMutationsGeneExons.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/analysis/exon/mutationsGeneExons/showMutationsGeneExonsList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#mutationsGeneExons", {
					userId : userId,
					userName : userName,
					formId : $("#mutationsGeneExons_id").val(),
					title : $("#mutationsGeneExons_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#mutationsGeneExons_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/analysis/exon/mutationsGeneExons/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/analysis/exon/mutationsGeneExons/copyMutationsGeneExons.action?id=' + $("#mutationsGeneExons_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#mutationsGeneExons_id").val() + "&tableId=mutationsGeneExons");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#mutationsGeneExons_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'突变基因转录本及总外显子表',
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);