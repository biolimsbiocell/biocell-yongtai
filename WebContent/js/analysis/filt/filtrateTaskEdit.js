﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/analysis/filt/filtrateTask/editFiltrateTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/analysis/filt/filtrateTask/showFiltrateTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#filtrateTask", {
					userId : userId,
					userName : userName,
					formId : $("#filtrateTask_id").val(),
					title : $("#filtrateTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#filtrateTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var filtrateTaskItemDivData = $("#filtrateTaskItemdiv").data("filtrateTaskItemGrid");
		document.getElementById('filtrateTaskItemJson').value = commonGetModifyRecords(filtrateTaskItemDivData);
	    var sampleFiltrateInfoDivData = $("#sampleFiltrateInfodiv").data("sampleFiltrateInfoGrid");
		document.getElementById('sampleFiltrateInfoJson').value = commonGetModifyRecords(sampleFiltrateInfoDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/analysis/filt/filtrateTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/analysis/filt/filtrateTask/copyFiltrateTask.action?id=' + $("#filtrateTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#filtrateTask_id").val() + "&tableId=filtrateTask");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#filtrateTask_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'过滤',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/analysis/filt/filtrateTask/showFiltrateTaskItemList.action", {
				id : $("#filtrateTask_id").val()
			}, "#filtrateTaskItempage");
load("/analysis/filt/filtrateTask/showSampleFiltrateInfoList.action", {
				id : $("#filtrateTask_id").val()
			}, "#sampleFiltrateInfopage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);