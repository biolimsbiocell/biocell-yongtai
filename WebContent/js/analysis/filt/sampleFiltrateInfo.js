﻿
var sampleFiltrateInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'desDataNum',
		type:"string"
	});
	   fields.push({
		name:'filtDataNum',
		type:"string"
	});
	   fields.push({
		name:'normDataNum',
		type:"string"
	});
	   fields.push({
		name:'sequenceType',
		type:"string"
	});
	   fields.push({
		name:'realSequenceType',
		type:"string"
	});
	   fields.push({
		name:'isSequenceType',
		type:"string"
	});
	   fields.push({
		name:'isDataNum',
		type:"string"
	});
	   fields.push({
		name:'addDataNum',
		type:"string"
	});
	   fields.push({
		name:'abnomal',
		type:"string"
	});
	   fields.push({
		name:'idGood',
		type:"string"
	});
	   fields.push({
		name:'nextflow',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'filtrateTask-id',
		type:"string"
	});
	    fields.push({
		name:'filtrateTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'过滤结果id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:'文库编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'desDataNum',
		hidden : false,
		header:'下机数据量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtDataNum',
		hidden : false,
		header:'过滤数据量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'normDataNum',
		hidden : false,
		header:'要求数据量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequenceType',
		hidden : false,
		header:'要求测序类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'realSequenceType',
		hidden : false,
		header:'实际测序类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
			var isSequenceTypecob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'isSequenceType',
		hidden : false,
		header:'测序类型是否一致',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(isSequenceTypecob),editor: isSequenceTypecob
	});
			var isDataNumcob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'isDataNum',
		hidden : false,
		header:'是否满足数据量要求',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(isDataNumcob),editor: isDataNumcob
	});
	cm.push({
		dataIndex:'addDataNum',
		hidden : false,
		header:'需要加测数据量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'abnomal',
		hidden : false,
		header:'异常值',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
			var idGoodcob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'idGood',
		hidden : false,
		header:'是否合格',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(idGoodcob),editor: idGoodcob
	});
			var nextflowcob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'nextflow',
		hidden : false,
		header:'下一步流向',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(nextflowcob),editor: nextflowcob
	});
			var submitcob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(submitcob),editor: submitcob
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtrateTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtrateTask-name',
		hidden : false,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/filt/filtrateTask/showSampleFiltrateInfoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="过滤结果";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/filt/filtrateTask/delSampleFiltrateInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择关联主表',
			handler : selectfiltrateTaskFun
		});
	
	
	
	
	
	
	
	
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = sampleFiltrateInfoGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 12-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 13-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 14-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 15-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 16-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 17-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							sampleFiltrateInfoGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	sampleFiltrateInfoGrid=gridEditTable("sampleFiltrateInfodiv",cols,loadParam,opts);
	$("#sampleFiltrateInfodiv").data("sampleFiltrateInfoGrid", sampleFiltrateInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectfiltrateTaskFun(){
	var win = Ext.getCmp('selectfiltrateTask');
	if (win) {win.close();}
	var selectfiltrateTask= new Ext.Window({
	id:'selectfiltrateTask',modal:true,title:'选择关联主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/FiltrateTaskSelect.action?flag=filtrateTask' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectfiltrateTask.close(); }  }]  });     selectfiltrateTask.show(); }
	function setfiltrateTask(id,name){
		var gridGrid = $("#sampleFiltrateInfodiv").data("sampleFiltrateInfoGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('filtrateTask-id',id);
			obj.set('filtrateTask-name',name);
		});
		var win = Ext.getCmp('selectfiltrateTask')
		if(win){
			win.close();
		}
	}
	
