﻿
var filtrateTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'smapleCode',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'rRNAMappingRate',
			type:"string"
		});
	   fields.push({
			name:'rRNAMappingNumber',
			type:"string"
		});
	   fields.push({
			name:'without3pAdaptorReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'without3pAdaptorReadsRate',
			type:"string"
		});
	   fields.push({
			name:'withoutInsertReadsRate',
			type:"string"
		});
	   fields.push({
			name:'withoutInsertReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'polyATReadsRate',
			type:"string"
		});
	   fields.push({
			name:'polyATReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'exLengthrReadsRate',
			type:"string"
		});
	   fields.push({
			name:'exLengthrReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'rawReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'rawBasesNumber',
			type:"string"
		});
	   fields.push({
			name:'cleanReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'cleanBasesNumber',
			type:"string"
		});
	   fields.push({
			name:'cleanReadsRate',
			type:"string"
		});
	   fields.push({
			name:'rawReadsLength',
			type:"string"
		});
	   fields.push({
			name:'cleanReadsLength',
			type:"string"
		});
	   fields.push({
			name:'lowqualityReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'lowqualityReadsRate',
			type:"string"
		});
	   fields.push({
			name:'nsReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'nsReadsRate',
			type:"string"
		});
	   fields.push({
			name:'adapterPollutedReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'adapterPollutedReadsRate',
			type:"string"
		});
	   fields.push({
			name:'polyAReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'polyAReadsRate',
			type:"string"
		});
	   fields.push({
			name:'polyTReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'polyTReadsRate',
			type:"string"
		});
	   fields.push({
			name:'highATReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'highATReadsRate',
			type:"string"
		});
	   fields.push({
			name:'discardedPrimerPollutedReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'discardedPrimerPollutedReadsRate',
			type:"string"
		});
	   fields.push({
			name:'trimedPrimerPollutedReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'trimedPrimerPollutedReadsRate',
			type:"string"
		});
	   fields.push({
			name:'cleanQ30BasesRate',
			type:"string"
		});
	   fields.push({
			name:'rawQ30BasesRate',
			type:"string"
		});
	   fields.push({
			name:'cleanQ20BasesRate',
			type:"string"
		});
	   fields.push({
			name:'rawQ20BasesRate',
			type:"string"
		});
	   fields.push({
			name:'totalReadsNumber',
			type:"string"
		});
	   fields.push({
			name:'totalCleanBasesNumber',
			type:"string"
		});
	   fields.push({
			name:'q30',
			type:"string"
		});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'filtrateTask-id',
		type:"string"
	});
	    fields.push({
		name:'filtrateTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	var flag1=false;//adaptorpollutedreadsrate\nsreadsrate
	var flag2=true;//rrnamappingrate
	var flag3=true;//trimedprimerpollutedreadsrate
	var flag4=true;//polyareadsrate\polytreadsrate\highatreadsrate\discaredprimerpollutedreadsrate
	var flag5=true;//without3padaptorreadsrate\withoutinsertreadsrate\polyatreadsrate\ex-lengthreadsrate
	var project = $("#filtrateTask_projectId").val();
	if(project!=null&&project!='0'&&project=='7'){
		flag1=true;
		flag5=false;
	}
	if(project!=null&&project!='0'&&(project=='3'||project=='4')){
		flag2=false;
	}
	if(project!=null&&project!='0'&&(project=='5'||project=='6')){
		flag3=false;
	}
	if(project!=null&&project!='0'&&project=='6'){
		flag4=false;
	}
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'过滤明细id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'smapleCode',
		hidden : false,
		header:'样本编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:'文库编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rawReadsNumber',
		hidden : false,
		header:'Raw Reads Number',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rawBasesNumber',
		hidden : false,
		header:'Raw Bases Number',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cleanReadsNumber',
		hidden : false,
		header:'Clean Reads Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cleanBasesNumber',
		hidden : false,
		header:'Clean Bases Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cleanReadsRate',
		hidden : false,
		header:'Clean Reads Rate',
		width:20*7,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rawReadsLength',
		hidden : false,
		header:'Raw Reads Length',
		width:20*7,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cleanReadsLength',
		hidden : false,
		header:'Clean Reads Length',
		width:20*7,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'lowqualityReadsNumber',
		hidden : true,
		header:'Low-quality Reads Number',
		width:20*9,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'lowqualityReadsRate',
		hidden : true,
		header:'Low-quality Reads Rate',
		width:20*9,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nsReadsNumber',
		hidden : true,
		header:'Ns Reads Number',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nsReadsRate',
		hidden : flag1,
		header:'Ns Reads Rate',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'adapterPollutedReadsNumber',
		hidden : true,
		header:'Adapter Polluted Reads Number',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'adapterPollutedReadsRate',
		hidden : flag1,
		header:'Adapter Polluted Reads Rate',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'polyAReadsNumber',
		hidden : true,
		header:'PolyA Reads Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'polyAReadsRate',
		hidden : flag4,
		header:'PolyA Reads Rate',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'polyTReadsNumber',
		hidden : true,
		header:'PolyT Reads Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'polyTReadsRate',
		hidden : flag4,
		header:'PolyT Reads Rate',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'highATReadsNumber',
		hidden : true,
		header:'HighAT Reads Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'highATReadsRate',
		hidden : flag4,
		header:'HighAT Reads Rate',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'discardedPrimerPollutedReadsNumber',
		hidden : true,
		header:'Discarded Primer Polluted Reads Number',
		width:20*12,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'discardedPrimerPollutedReadsRate',
		hidden : flag4,
		header:'Discarded Primer Polluted  ReadsRate',
		width:20*12,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'trimedPrimerPollutedReadsNumber',
		hidden : true,
		header:'Trimed Primer Polluted Reads Number',
		width:20*12,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'trimedPrimerPollutedReadsRate',
		hidden : flag3,
		header:'Trimed Primer Polluted Reads Rate',
		width:20*12,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cleanQ20BasesRate',
		hidden : true,
		header:'Clean Q20 Bases Rate',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cleanQ30BasesRate',
		hidden : true,
		header:'Clean Q30 Bases Rate',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rawQ20BasesRate',
		hidden : true,
		header:'Raw Q20 Bases Rate',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rawQ30BasesRate',
		hidden : true,
		header:'Raw Q30 Bases Rate',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rRNAMappingRate',
		hidden : flag2,
		header:'rRNA Mapping Rate',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rRNAMappingNumber',
		hidden : true,
		header:'rRNA Mapping Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'without3pAdaptorReadsRate',
		hidden : flag5,
		header:'Without 3p Adaptor Reads Rate',
		width:20*12,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'without3pAdaptorReadsNumber',
		hidden : true,
		header:'Without 3p Adaptor Reads Number',
		width:20*12,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'withoutInsertReadsRate',
		hidden : flag5,
		header:'Without Insert Reads Rate',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'withoutInsertReadsNumber',
		hidden : true,
		header:'Without Insert Reads Number',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'polyATReadsRate',
		hidden : flag5,
		header:'Poly A/T Reads Rate',
		width:20*9,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'polyATReadsNumber',
		hidden : true,
		header:'PolyAT Reads Number',
		width:20*9,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'exLengthrReadsRate',
		hidden : flag5,
		header:'Ex-Lengthr Reads Rate',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'exLengthrReadsNumber',
		hidden : true,
		header:'Ex-Lengthr Reads Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'totalReadsNumber',
		hidden : true,
		header:'Total Reads Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'totalCleanBasesNumber',
		hidden : true,
		header:'Total Clean Bases Number',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'q30',
		hidden : true,
		header:'Q30',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtrateTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtrateTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/filt/filtrateTask/showFiltrateTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="过滤明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/filt/filtrateTask/delFiltrateTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				filtrateTaskItemGrid.getStore().commitChanges();
				filtrateTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = filtrateTaskItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							filtrateTaskItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '读取Csv',
		handler : getCsv
	});
	function getCsv(){
		//alert($("#deSequencingTask_id").val());
		ajax("post", "/analysis/filt/filtrateTask/getCsv.action", {
			taskId : $("#filtrateTask_id").val()
		},function(data){
			message("读取成功！");
		},null);
	}
	filtrateTaskItemGrid=gridEditTable("filtrateTaskItemdiv",cols,loadParam,opts);
	$("#filtrateTaskItemdiv").data("filtrateTaskItemGrid", filtrateTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectfiltrateTaskFun(){
	var win = Ext.getCmp('selectfiltrateTask');
	if (win) {win.close();}
	var selectfiltrateTask= new Ext.Window({
	id:'selectfiltrateTask',modal:true,title:'选择关联主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/FiltrateTaskSelect.action?flag=filtrateTask' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectfiltrateTask.close(); }  }]  });     selectfiltrateTask.show(); }
	function setfiltrateTask(id,name){
		var gridGrid = $("#filtrateTaskItemdiv").data("filtrateTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('filtrateTask-id',id);
			obj.set('filtrateTask-name',name);
		});
		var win = Ext.getCmp('selectfiltrateTask')
		if(win){
			win.close();
		}
	}
	
