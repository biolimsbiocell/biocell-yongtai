var filtrateTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
			name:'projectIsGood',
			type:"string"
		});
	    fields.push({
			name:'dataIsGood',
			type:"string"
		});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'statename',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*6,
		
		sortable:true
	});
	var storeprojectIsGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ]]
	});
	var projectIsGoodCob = new Ext.form.ComboBox({
		store : storeprojectIsGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'projectIsGood',
		header:'项目整体状态',
		width:50*6,
		sortable:true,
		editor:projectIsGoodCob,
		renderer : Ext.util.Format.comboRenderer(projectIsGoodCob)
	});
	var storedataIsGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ]]
	});
	var dataIsGoodCob = new Ext.form.ComboBox({
		store : storedataIsGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'dataIsGood',
		header:'数据量是否足够',
		width:50*6,
		sortable:true,
		editor:dataIsGoodCob,
		renderer : Ext.util.Format.comboRenderer(dataIsGoodCob)
	});
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'statename',
		header:'工作流状态',
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/filt/filtrateTask/showFiltrateTaskListJson.action";
	var opts={};
	opts.title="过滤";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	filtrateTaskGrid=gridTable("show_filtrateTask_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/analysis/filt/filtrateTask/editFiltrateTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/analysis/filt/filtrateTask/editFiltrateTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/analysis/filt/filtrateTask/viewFiltrateTask.action?id=' + id;
}
function exportexcel() {
	filtrateTaskGrid.title = '导出列表';
	var vExportContent = filtrateTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				
				commonSearchAction(filtrateTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
