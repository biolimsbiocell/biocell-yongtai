var analysisTaskAbnormalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'poolingCode',
		type : "string"
	});
	fields.push({
		name : 'resultOne',
		type : "string"
	});
	fields.push({
		name : 'resultTwo',
		type : "string"
	});
	fields.push({
		name : 'cnv',
		type : "string"
	});
	// =====13=====
	fields.push({
		name : 'tOne13',
		type : "string"
	});
	fields.push({
		name : 'tTwo13',
		type : "string"
	});
	fields.push({
		name : 'a13',
		type : "string"
	});
	fields.push({
		name : 'l13',
		type : "string"
	});
	fields.push({
		name : 'risk13',
		type : "string"
	});
	fields.push({
		name : 'fetal13',
		type : "string"
	});
	fields.push({
		name : 'tFour13',
		type : "string"
	});
	fields.push({
		name : 'sampleNum13',
		type : "string"
	});
	fields.push({
		name : 'tFive13',
		type : "string"
	});
	// ====
	// =====18=====
	fields.push({
		name : 'tOne18',
		type : "string"
	});
	fields.push({
		name : 'tTwo18',
		type : "string"
	});
	fields.push({
		name : 'a18',
		type : "string"
	});
	fields.push({
		name : 'l18',
		type : "string"
	});
	fields.push({
		name : 'risk18',
		type : "string"
	});
	fields.push({
		name : 'fetal18',
		type : "string"
	});
	fields.push({
		name : 'tFour18',
		type : "string"
	});
	fields.push({
		name : 'sampleNum18',
		type : "string"
	});
	fields.push({
		name : 'tFive18',
		type : "string"
	});
	// ====

	// =====21=====
	fields.push({
		name : 'tOne21',
		type : "string"
	});
	fields.push({
		name : 'tTwo21',
		type : "string"
	});
	fields.push({
		name : 'a21',
		type : "string"
	});
	fields.push({
		name : 'l21',
		type : "string"
	});
	fields.push({
		name : 'risk21',
		type : "string"
	});
	fields.push({
		name : 'fetal21',
		type : "string"
	});
	fields.push({
		name : 'tFour21',
		type : "string"
	});
	fields.push({
		name : 'sampleNum21',
		type : "string"
	});
	fields.push({
		name : 'tFive21',
		type : "string"
	});
	// ====
	// =====X=====
	fields.push({
		name : 'tOneX',
		type : "string"
	});
	fields.push({
		name : 'tTwoX',
		type : "string"
	});
	fields.push({
		name : 'lX',
		type : "string"
	});
	fields.push({
		name : 'riskX',
		type : "string"
	});
	fields.push({
		name : 'fetalX',
		type : "string"
	});
	fields.push({
		name : 'tFourX',
		type : "string"
	});
	fields.push({
		name : 'sampleNumX',
		type : "string"
	});
	fields.push({
		name : 'tFiveX',
		type : "string"
	});
	// ====
	// =====Y=====
	fields.push({
		name : 'tOneY',
		type : "string"
	});
	fields.push({
		name : 'tTwoY',
		type : "string"
	});
	fields.push({
		name : 'lY',
		type : "string"
	});
	fields.push({
		name : 'riskY',
		type : "string"
	});
	fields.push({
		name : 'fetalY',
		type : "string"
	});
	fields.push({
		name : 'tFourY',
		type : "string"
	});
	fields.push({
		name : 'sampleNumY',
		type : "string"
	});
	fields.push({
		name : 'tFiveY',
		type : "string"
	});
	// ====
	fields.push({
		name : 'fetalAdd',
		type : "string"
	});
	fields.push({
		name : 'r13',
		type : "string"
	});
	fields.push({
		name : 'r18',
		type : "string"
	});
	fields.push({
		name : 'r21',
		type : "string"
	});
	fields.push({
		name : 'rx',
		type : "string"
	});
	fields.push({
		name : 'ry',
		type : "string"
	});
	fields.push({
		name : 'seqInfo',
		type : "string"
	});
	fields.push({
		name : 'fetalExceptionSample',
		type : "string"
	});
	fields.push({
		name : 'otherRstExceptionResult',
		type : "string"
	});
	fields.push({
		name : 'age',
		type : "string"
	});
	fields.push({
		name : 'gesWeeks',
		type : "string"
	});
	fields.push({
		name : 'babyNum',
		type : "string"
	});
	fields.push({
		name : 'tsResult',
		type : "string"
	});
	fields.push({
		name : 'weight',
		type : "string"
	});
	fields.push({
		name : 'riskValue21',
		type : "string"
	});
	fields.push({
		name : 'riskValue18',
		type : "string"
	});
	fields.push({
		name : 'protest',
		type : "string"
	});
	fields.push({
		name : 'rawReadNum',
		type : "string"
	});
	
	fields.push({
		name : 'AlignRate',
		type : "string"
	});
	
	fields.push({
		name : 'duplicationRate',
		type : "string"
	});
//	fields.push({
//		name : 'gcContent',
//		type : "string"
//	});
	fields.push({
		name : 'realURNum',
		type : "string"
	});
	fields.push({
		name : 'chr13',
		type : "string"
	});
	fields.push({
		name : 'chr18',
		type : "string"
	});
	fields.push({
		name : 'chr21',
		type : "string"
	});
	fields.push({
		name : 'suggestResult',
		type : "string"
	});
	fields.push({
		name : 'crstResult',
		type : "string"
	});
	fields.push({
		name : 'xrstResult',
		type : "string"
	});
//	fields.push({
//		name : 'readsMb',
//		type : "string"
//	});
	fields.push({
		name : 'alignRate',
		type : "string"
	});
	fields.push({
		name : 'gcContent',
		type : "string"
	});
	fields.push({
		name : 'q30Ratio',
		type : "string"
	});
	fields.push({
		name : 'result',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'isReport',
		type : "string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});

	    fields.push({
	    name:'method',
	    type:"string"
	});
	    fields.push({
			name : 'note',
			type : "string"
		});
	    fields.push({
		name:'isExecute',
		type:"string"
	});
	    fields.push({
			name:'nextflow',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'文库编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:10*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'样本编号',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		header:'pooling号',
		width:20*6,
		sortable:true
	});
	// ==13==
	cm.push({
		dataIndex : 'tOne13',
		hidden : false,
		header : '13号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo13',
		hidden : false,
		header : '13号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a13',
		hidden : false,
		header : '13号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l13',
		hidden : false,
		header : '13号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk13',
		hidden : false,
		header : '13号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal13',
		hidden : false,
		header : '13号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour13',
		hidden : false,
		header : '13号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum13',
		hidden : false,
		header : '13号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive13',
		hidden : false,
		header : '13号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==18==
	cm.push({
		dataIndex : 'tOne18',
		hidden : false,
		header : '18号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo18',
		hidden : false,
		header : '18号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a18',
		hidden : false,
		header : '18号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l18',
		hidden : false,
		header : '18号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk18',
		hidden : false,
		header : '18号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal18',
		hidden : false,
		header : '18号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour18',
		hidden : false,
		header : '18号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum18',
		hidden : false,
		header : '18号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive18',
		hidden : false,
		header : '18号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==21==
	cm.push({
		dataIndex : 'tOne21',
		hidden : false,
		header : '21号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo21',
		hidden : false,
		header : '21号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a21',
		hidden : false,
		header : '21号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l21',
		hidden : false,
		header : '21号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk21',
		hidden : false,
		header : '21号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal21',
		hidden : false,
		header : '21号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour21',
		hidden : false,
		header : '21号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum21',
		hidden : false,
		header : '21号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive21',
		hidden : false,
		header : '21号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==X==
	cm.push({
		dataIndex : 'tOneX',
		hidden : false,
		header : 'X号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwoX',
		hidden : false,
		header : 'X号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'lX',
		hidden : false,
		header : 'X号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'riskX',
		hidden : false,
		header : 'X号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalX',
		hidden : false,
		header : 'X号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFourX',
		hidden : false,
		header : 'X号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNumX',
		hidden : false,
		header : 'X号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFiveX',
		hidden : false,
		header : 'X号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==Y==
	cm.push({
		dataIndex : 'tOneY',
		hidden : false,
		header : 'Y号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwoY',
		hidden : false,
		header : 'Y号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'lY',
		hidden : false,
		header : 'Y号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'riskY',
		hidden : false,
		header : 'Y号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalY',
		hidden : false,
		header : 'Y号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFourY',
		hidden : false,
		header : 'Y号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNumY',
		hidden : false,
		header : 'Y号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFiveY',
		hidden : false,
		header : 'Y号染色体T5',
		width : 20 * 6
	});
	// =====
	cm.push({
		dataIndex : 'fetalAdd',
		hidden : false,
		header : '加权胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r13',
		hidden : false,
		header : '13号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r18',
		hidden : false,
		header : '18号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r21',
		hidden : false,
		header : '21号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'rx',
		hidden : false,
		header : 'X号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'ry',
		hidden : false,
		header : 'Y号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'seqInfo',
		hidden : false,
		header : '质控情况',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalExceptionSample',
		hidden : false,
		header : '胎儿浓度异常样本',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'otherRstExceptionResult',
		hidden : false,
		header : '其他染色体异常结果',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'age',
		hidden : false,
		header : '年龄',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'gender',
//		hidden : false,
//		header : '性别',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex : 'gender',
		hidden : false,
		header : '性别',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'gesWeeks',
		hidden : false,
		header : '孕周',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'babyNum',
		hidden : false,
		header : '胎儿数量',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tsResult',
		hidden : false,
		header : '唐筛结果',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'weight',
		hidden : false,
		header : '体重',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'riskValue21',
		hidden : false,
		header : '21风险值',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'riskValue18',
		hidden : false,
		header : '18风险值',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'protest',
		hidden : false,
		header : '补充协议',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'rawReadNum',
		hidden : false,
		header : 'Raw Read Num',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'AlignRate',
		hidden : false,
		header : 'Align Rate',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'duplicationRate',
		hidden : false,
		header : 'Duplication Rate',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'realURNum',
		hidden : false,
		header : 'Real UR Num',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'gcContent',
		hidden : false,
		header : 'GC Content',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr13',
		hidden : false,
		header : 'Chr13',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr18',
		hidden : false,
		header : 'Chr18',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr21',
		hidden : false,
		header : 'Chr21',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'suggestResult',
		hidden : false,
		header : '结果建议',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crstResult',
		hidden : false,
		header : '常染色体判断结果',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'xrstResult',
		hidden : false,
		header : '性染色体判断结果',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'gcContent',
		hidden : false,
		header : 'gc_content',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'alignRate',
		hidden : false,
		header : 'alignRate',
		width : 20 * 6
	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '合格' ], [ '1', '不合格' ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		header:'是否合格',
		hidden : true,
		width:20*6,
		sortable:true,
//		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	
	var storeadviceCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [  [ '0', 'T13高风险' ], [ '1', 'T18高风险' ] ,[ '2', 'T21高风险' ],[ '3', '低风险'],['4','重抽血'],['5','退费'],['6','重建库'],['7','重上机']]
	});
	var adviceCob = new Ext.form.ComboBox({
		store : storeadviceCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理方式',
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(adviceCob),
//		editor:adviceCob
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', 'T13高风险' ], [ '1', 'T18高风险' ] ,[ '2', 'T21高风险' ],[ '3', '低风险'],['4','重抽血'],['5','退费'],['6','重建库'],['7','重上机']]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextflow',
		hidden : false,
		header:'处理意见',
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	
	var storeisExecuteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ]]
	});
	var isExecuteCob = new Ext.form.ComboBox({
		store : storeisExecuteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:'是否执行',
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(isExecuteCob),
		editor:isExecuteCob
	});
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
//	cm.push({
//		dataIndex:'mainID',
//		header:'相关主表ID',
//		width:20*6,
//		hidden:true,
//		sortable:true
//	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/analy/analysisTaskAbnormal/showAnalysisTaskAbnormalListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title="异常样本";
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	opts.tbar.push({
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	analysisTaskAbnormalGrid=gridEditTable("show_analysisTaskAbnormal_div",cols,loadParam,opts);
	$("#show_analysisTaskAbnormal_div").data("analysisTaskAbnormalGrid", analysisTaskAbnormalGrid);
});


//保存
function save(){	
	var selectRecord = analysisTaskAbnormalGrid.getSelectionModel();
	var inItemGrid = $("#show_analysisTaskAbnormal_div").data("analysisTaskAbnormalGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(itemJson==""){
		message("没有需要保存的数据！");
		return;
	}
	//获取是否执行
	var isExecute=analysisTaskAbnormalGrid.getSelectionModel().getSelected().get("isExecute"); 
	//获取处理意见
	var advice=analysisTaskAbnormalGrid.getSelectionModel().getSelected().get("advice"); 
	if(advice=="" && isExecute=="1"){
		message("项目管理未给出意见，不能执行！");
		return;
	}
//	else if(advice=="8" && isExecute=="1"){
//		message("意见不明确，请等待给出明确意见再执行！");
//		return;
//	}
	
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			
			ajax("post", "/analysis/analy/analysisTaskAbnormal/saveAnalysisTaskAbnormal.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#show_analysisTaskAbnormal_div").data("analysisTaskAbnormalGrid").getStore().commitChanges();
					$("#show_analysisTaskAbnormal_div").data("analysisTaskAbnormalGrid").getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
		});
	}else{
		message("没有需要保存的数据！");
	}
}

//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
		
			
			commonSearchAction(analysisTaskAbnormalGrid);
			$(this).dialog("close");

		},
		"清空" : function() {
			form_reset();

		}
	}, true, option);
}