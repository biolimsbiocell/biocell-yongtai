﻿var analysisManagerGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
//	   fields.push({
//		name:'id',
//		type:"string"
//	});
//	   fields.push({
//		name:'name',
//		type:"string"
//	});
//	   fields.push({
//		name:'sampleCode',
//		type:"string"
//	});
//	   fields.push({
//		name:'nextFlow',
//		type:"string"
//	});
//	   fields.push({
//		name:'cnv',
//		type:"string"
//	});
//	   fields.push({
//		name:'result1',
//		type:"string"
//	});
//	   fields.push({
//		name:'result2',
//		type:"string"
//	});
////	   fields.push({
////		name:'readingResult',
////		type:"string"
////	});
//	   fields.push({
//		name:'upload',
//		type:"string"
//	});
//	   fields.push({
//		name:'result',
//		type:"string"
//	});
//	   fields.push({
//		name:'method',
//		type:"string"
//	});
//	   fields.push({
//		name : 'isReport',
//		type : "string"
//	});
//    fields.push({
//		name:'interpretationTask-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'interpretationTask-name',
//		type:"string"
//	});
//	 // 上传图片
//	fields.push({
//		name : 'upLoadAccessory-fileName',
//		type : "string"
//	});
//	fields.push({
//		name : 'upLoadAccessory-id',
//		type : "string"
//	});
//	//
//	fields.push({
//		name : 'code',
//		type : "string"
//	});
//	fields.push({
//		name : 'reads_mb',
//		type : "string"
//	});
//	fields.push({
//		name : 'gc_antent',
//		type : "string"
//	});
//	fields.push({
//		name : 'q30_ratio',
//		type : "string"
//	});
//	fields.push({
//		name : 'align_ratio',
//		type : "string"
//	});
//	fields.push({
//		name : 'ur_ratio',
//		type : "string"
//	});
    fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'poolingCode',
		type : "string"
	});
	fields.push({
		name : 'resultOne',
		type : "string"
	});
	fields.push({
		name : 'resultTwo',
		type : "string"
	});
	fields.push({
		name : 'cnv',
		type : "string"
	});
	// =====13=====
	fields.push({
		name : 'tOne13',
		type : "string"
	});
	fields.push({
		name : 'tTwo13',
		type : "string"
	});
	fields.push({
		name : 'a13',
		type : "string"
	});
	fields.push({
		name : 'l13',
		type : "string"
	});
	fields.push({
		name : 'risk13',
		type : "string"
	});
	fields.push({
		name : 'fetal13',
		type : "string"
	});
	fields.push({
		name : 'tFour13',
		type : "string"
	});
	fields.push({
		name : 'sampleNum13',
		type : "string"
	});
	fields.push({
		name : 'tFive13',
		type : "string"
	});
	// ====
	// =====18=====
	fields.push({
		name : 'tOne18',
		type : "string"
	});
	fields.push({
		name : 'tTwo18',
		type : "string"
	});
	fields.push({
		name : 'a18',
		type : "string"
	});
	fields.push({
		name : 'l18',
		type : "string"
	});
	fields.push({
		name : 'risk18',
		type : "string"
	});
	fields.push({
		name : 'fetal18',
		type : "string"
	});
	fields.push({
		name : 'tFour18',
		type : "string"
	});
	fields.push({
		name : 'sampleNum18',
		type : "string"
	});
	fields.push({
		name : 'tFive18',
		type : "string"
	});
	// ====

	// =====21=====
	fields.push({
		name : 'tOne21',
		type : "string"
	});
	fields.push({
		name : 'tTwo21',
		type : "string"
	});
	fields.push({
		name : 'a21',
		type : "string"
	});
	fields.push({
		name : 'l21',
		type : "string"
	});
	fields.push({
		name : 'risk21',
		type : "string"
	});
	fields.push({
		name : 'fetal21',
		type : "string"
	});
	fields.push({
		name : 'tFour21',
		type : "string"
	});
	fields.push({
		name : 'sampleNum21',
		type : "string"
	});
	fields.push({
		name : 'tFive21',
		type : "string"
	});
	// ====
	// =====X=====
	fields.push({
		name : 'tOneX',
		type : "string"
	});
	fields.push({
		name : 'tTwoX',
		type : "string"
	});
	fields.push({
		name : 'lX',
		type : "string"
	});
	fields.push({
		name : 'riskX',
		type : "string"
	});
	fields.push({
		name : 'fetalX',
		type : "string"
	});
	fields.push({
		name : 'tFourX',
		type : "string"
	});
	fields.push({
		name : 'sampleNumX',
		type : "string"
	});
	fields.push({
		name : 'tFiveX',
		type : "string"
	});
	// ====
	// =====Y=====
	fields.push({
		name : 'tOneY',
		type : "string"
	});
	fields.push({
		name : 'tTwoY',
		type : "string"
	});
	fields.push({
		name : 'lY',
		type : "string"
	});
	fields.push({
		name : 'riskY',
		type : "string"
	});
	fields.push({
		name : 'fetalY',
		type : "string"
	});
	fields.push({
		name : 'tFourY',
		type : "string"
	});
	fields.push({
		name : 'sampleNumY',
		type : "string"
	});
	fields.push({
		name : 'tFiveY',
		type : "string"
	});
	// ====
	fields.push({
		name : 'fetalAdd',
		type : "string"
	});
	fields.push({
		name : 'r13',
		type : "string"
	});
	fields.push({
		name : 'r18',
		type : "string"
	});
	fields.push({
		name : 'r21',
		type : "string"
	});
	fields.push({
		name : 'rx',
		type : "string"
	});
	fields.push({
		name : 'ry',
		type : "string"
	});
	fields.push({
		name : 'seqInfo',
		type : "string"
	});
	fields.push({
		name : 'fetalExceptionSample',
		type : "string"
	});
	fields.push({
		name : 'otherRstExceptionResult',
		type : "string"
	});
	fields.push({
		name : 'age',
		type : "string"
	});
	fields.push({
		name : 'gesWeeks',
		type : "string"
	});
	fields.push({
		name : 'babyNum',
		type : "string"
	});
	fields.push({
		name : 'tsResult',
		type : "string"
	});
	fields.push({
		name : 'weight',
		type : "string"
	});
	fields.push({
		name : 'riskValue21',
		type : "string"
	});
	fields.push({
		name : 'riskValue18',
		type : "string"
	});
	fields.push({
		name : 'protest',
		type : "string"
	});
	fields.push({
		name : 'rawReadNum',
		type : "string"
	});
	
	fields.push({
		name : 'AlignRate',
		type : "string"
	});
	
	fields.push({
		name : 'duplicationRate',
		type : "string"
	});
//	fields.push({
//		name : 'gcContent',
//		type : "string"
//	});
	fields.push({
		name : 'realURNum',
		type : "string"
	});
	fields.push({
		name : 'chr13',
		type : "string"
	});
	fields.push({
		name : 'chr18',
		type : "string"
	});
	fields.push({
		name : 'chr21',
		type : "string"
	});
	fields.push({
		name : 'suggestResult',
		type : "string"
	});
	fields.push({
		name : 'crstResult',
		type : "string"
	});
	fields.push({
		name : 'xrstResult',
		type : "string"
	});
//	fields.push({
//		name : 'readsMb',
//		type : "string"
//	});
	fields.push({
		name : 'alignRate',
		type : "string"
	});
	fields.push({
		name : 'gcContent',
		type : "string"
	});
	fields.push({
		name : 'q30Ratio',
		type : "string"
	});
//	fields.push({
//		name : 'alignRatio',
//		type : "string"
//	});
//	fields.push({
//		name : 'urRatio',
//		type : "string"
//	});
	fields.push({
		name : 'result',
		type : "string"
	});
//	fields.push({
//		name : 'alignRatio',
//		type : "string"
//	});
//	fields.push({
//		name : 'urRatio',
//		type : "string"
//	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'isReport',
		type : "string"
	});
//	fields.push({
//		name : 'sampleType',
//		type : "string"
//	});
//	fields.push({
//		name : 'gender',
//		type : "string"
//	});
//	fields.push({
//		name : 'sampleRcvTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportExpDate',
//		type : "string"
//	});
//	fields.push({
//		name : 'computerTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'machineTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'rcvCNVTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'interpretationTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'rcvPotoTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportToCustomServiceTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'ExceptionSample',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportState',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportSendTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'clinicalInfo',
//		type : "string"
//	});
	fields.push({
		name : 'method',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'submit',
		type : "string"
	});
//	fields.push({
//		name : 'analysisTask-id',
//		type : "string"
//	});
//	fields.push({
//		name : 'analysisTask-name',
//		type : "string"
//	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
    
	cols.fields=fields;
	var cm=[];
//	cm.push({
//		dataIndex:'id',
//		hidden : true,
//		header:'编号',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'name',
//		hidden : true,
//		header:'描述',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'sampleCode',
//		hidden : false,
//		header:'样本编号',
//		width:30*6
////		editor : new Ext.form.TextField({allowBlank : true})
//	});
//	cm.push({
//		dataIndex:'code',
//		hidden : false,
//		header:'pooling号',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'cnv',
//		hidden : false,
//		header:'cnv',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'result1',
//		hidden : false,
//		header:'result1',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'result2',
//		hidden : false,
//		header:'result2',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'reads_mb',
//		hidden : false,
//		header:'reads_mb',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'gc_antent',
//		hidden : false,
//		header:'gc_antent',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'q30_ratio',
//		hidden : false,
//		header:'q30_ratio',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'align_ratio',
//		hidden : false,
//		header:'align_ratio',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'ur_ratio',
//		hidden : false,
//		header:'ur_ratio',
//		width:30*6
//	});
////	cm.push({
////		dataIndex:'readingResult',
////		hidden : false,
////		header:'解读结果',
////		width:30*6
////	});
//	 //鼠标聚焦触发事件
//	 var code = new Ext.form.TextField({
//	 allowBlank : true
//	 });
//	 code.on('focus', function() {
//		 var isUpload = true;
//			var record = analysisManagerGrid.getSelectOneRecord();
//			load("/system/template/template/toSampeUpload.action", {
//				fileId : record.get("upLoadAccessory-id"),
//				isUpload : isUpload
//			}, null, function() {
//				$("#upload_file_div").data("callback", function(data) {
//					record.set("upLoadAccessory-fileName", data.fileName);
//					record.set("upLoadAccessory-id", data.fileId);
//				});
//			});
//	 });
//		cm.push({
//			dataIndex : 'upLoadAccessory-fileName',
//			header : '上传图片',
//			width : 150,
//			hidden : false,
//			editor : code
//		});
//		cm.push({
//			dataIndex : 'upLoadAccessory-id',
//			header : '图片Id',
//			width : 150,
//			hidden : true
//		});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'结果',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	var isGood = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '0',
//				name : 'T13高风险'
//				},{
//				id : '1',
//				name : 'T18高风险'
//				},{
//				id : '2',
//				name : 'T21高风险'
//			},{
//				id : '3',
//				name : '低风险'
//			},{
//				id : '4',
//				name : '重抽血'
//			},{
//				id : '5',
//				name : '退费'
//			},{
//				id : '6',
//				name : '重建库'
//			},{
//				id : '7',
//				name : '重上机'
//			}
//			]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	
//	
//	var storenextFlowCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '合格' ], [ '1', '建库组反馈' ],[ '2', '反馈项目管理' ]]
//	});
//	var nextFlowCob = new Ext.form.ComboBox({
//		store : storenextFlowCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(nextFlowCob),
//		editor: nextFlowCob
//	});
//	
//	cm.push({
//		dataIndex:'method',
//		hidden : false,
//		header:'处理方式',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(isGood),
//		editor: isGood
//	});
//	var isRepost = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '是'
//			},{
//				id : '0',
//				name : '否'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex : 'isReport',
//		hidden : false,
//		header : '是否出报告',
//		width : 20 * 6,
//		renderer: Ext.util.Format.comboRenderer(isRepost),editor: isRepost
//	});
//	cm.push({
//		dataIndex:'interpretationTask-id',
//		hidden : true,
//		header:'关联主表ID',
//		width:20*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'interpretationTask-name',
//		hidden : true,
//		header:'关联主表',
//		width:20*10
//	});
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编号',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'poolingCode',
		hidden : true,
		header : 'pooling号',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleCode',
		hidden : false,
		header : '样本编号',
		width : 20 * 6
	});

	// ==13==
	cm.push({
		dataIndex : 'tOne13',
		hidden : false,
		header : '13号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo13',
		hidden : false,
		header : '13号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a13',
		hidden : false,
		header : '13号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l13',
		hidden : false,
		header : '13号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk13',
		hidden : false,
		header : '13号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal13',
		hidden : false,
		header : '13号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour13',
		hidden : false,
		header : '13号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum13',
		hidden : false,
		header : '13号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive13',
		hidden : false,
		header : '13号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==18==
	cm.push({
		dataIndex : 'tOne18',
		hidden : false,
		header : '18号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo18',
		hidden : false,
		header : '18号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a18',
		hidden : false,
		header : '18号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l18',
		hidden : false,
		header : '18号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk18',
		hidden : false,
		header : '18号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal18',
		hidden : false,
		header : '18号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour18',
		hidden : false,
		header : '18号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum18',
		hidden : false,
		header : '18号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive18',
		hidden : false,
		header : '18号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==21==
	cm.push({
		dataIndex : 'tOne21',
		hidden : false,
		header : '21号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo21',
		hidden : false,
		header : '21号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a21',
		hidden : false,
		header : '21号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l21',
		hidden : false,
		header : '21号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk21',
		hidden : false,
		header : '21号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal21',
		hidden : false,
		header : '21号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour21',
		hidden : false,
		header : '21号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum21',
		hidden : false,
		header : '21号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive21',
		hidden : false,
		header : '21号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==X==
	cm.push({
		dataIndex : 'tOneX',
		hidden : false,
		header : 'X号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwoX',
		hidden : false,
		header : 'X号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'lX',
		hidden : false,
		header : 'X号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'riskX',
		hidden : false,
		header : 'X号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalX',
		hidden : false,
		header : 'X号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFourX',
		hidden : false,
		header : 'X号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNumX',
		hidden : false,
		header : 'X号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFiveX',
		hidden : false,
		header : 'X号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==Y==
	cm.push({
		dataIndex : 'tOneY',
		hidden : false,
		header : 'Y号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwoY',
		hidden : false,
		header : 'Y号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'lY',
		hidden : false,
		header : 'Y号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'riskY',
		hidden : false,
		header : 'Y号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalY',
		hidden : false,
		header : 'Y号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFourY',
		hidden : false,
		header : 'Y号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNumY',
		hidden : false,
		header : 'Y号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFiveY',
		hidden : false,
		header : 'Y号染色体T5',
		width : 20 * 6
	});
	// =====
	cm.push({
		dataIndex : 'fetalAdd',
		hidden : false,
		header : '加权胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r13',
		hidden : false,
		header : '13号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r18',
		hidden : false,
		header : '18号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r21',
		hidden : false,
		header : '21号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'rx',
		hidden : false,
		header : 'X号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'ry',
		hidden : false,
		header : 'Y号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'seqInfo',
		hidden : false,
		header : '质控情况',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalExceptionSample',
		hidden : false,
		header : '胎儿浓度异常样本',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'otherRstExceptionResult',
		hidden : false,
		header : '其他染色体异常结果',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'age',
		hidden : false,
		header : '年龄',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'gender',
//		hidden : false,
//		header : '性别',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex : 'gender',
		hidden : false,
		header : '性别',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'gesWeeks',
		hidden : false,
		header : '孕周',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'babyNum',
		hidden : false,
		header : '胎儿数量',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tsResult',
		hidden : false,
		header : '唐筛结果',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'weight',
		hidden : false,
		header : '体重',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'riskValue21',
		hidden : false,
		header : '21风险值',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'riskValue18',
		hidden : false,
		header : '18风险值',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'protest',
		hidden : false,
		header : '补充协议',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'rawReadNum',
		hidden : false,
		header : 'Raw Read Num',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'AlignRate',
		hidden : false,
		header : 'Align Rate',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'duplicationRate',
		hidden : false,
		header : 'Duplication Rate',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'realURNum',
		hidden : false,
		header : 'Real UR Num',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'gcContent',
		hidden : false,
		header : 'GC Content',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr13',
		hidden : false,
		header : 'Chr13',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr18',
		hidden : false,
		header : 'Chr18',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr21',
		hidden : false,
		header : 'Chr21',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'suggestResult',
		hidden : false,
		header : '结果建议',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crstResult',
		hidden : false,
		header : '常染色体判断结果',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'xrstResult',
		hidden : false,
		header : '性染色体判断结果',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'readsMb',
//		hidden : false,
//		header : 'reads_mb',
//		width : 20 * 6
//	});
	cm.push({
		dataIndex : 'gcContent',
		hidden : false,
		header : 'gc_content',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'readsMb',
//		hidden : false,
//		header : 'reads_mb',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'gcContent',
//		hidden : false,
//		header : 'gc_content',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'readsMb',
//		hidden : false,
//		header : 'reads_mb',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'gcContent',
//		hidden : false,
//		header : 'gc_content',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'q30Ratio',
//		hidden : false,
//		header : 'q30_ratio',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'alignRatio',
//		hidden : false,
//		header : 'align_ratio',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'urRatio',
//		hidden : false,
//		header : 'ur_ratio',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'alignRatio',
//		hidden : false,
//		header : 'align_ratio',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'urRatio',
//		hidden : false,
//		header : 'ur_ratio',
//		width : 20 * 6
//	});
	cm.push({
		dataIndex : 'alignRate',
		hidden : false,
		header : 'alignRate',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'urRatio',
//		hidden : false,
//		header : 'ur_ratio',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'resultOne',
//		hidden : false,
//		header : 'result1',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'resultTwo',
//		hidden : false,
//		header : 'result2',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'cnv',
//		hidden : false,
//		header : 'cnv',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'sampleType',
//		hidden : false,
//		header : '样本类型',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'sampleRcvTime',
//		hidden : false,
//		header : '样本接收日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportExpDate',
//		hidden : false,
//		header : '报告截止日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'computerTime',
//		hidden : false,
//		header : '上机时间',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'machineTime',
//		hidden : false,
//		header : '预下机时间',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'rcvCNVTime',
//		hidden : false,
//		header : '收到CNV日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'interpretationTime',
//		hidden : false,
//		header : '解读完成日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'rcvPotoTime',
//		hidden : false,
//		header : '收到图片日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportToCustomServiceTime',
//		hidden : false,
//		header : '报告发客服日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'ExceptionSample',
//		hidden : false,
//		header : '异常样本',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportState',
//		hidden : false,
//		header : '报告状态',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportSendTime',
//		hidden : false,
//		header : '报告发出日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'clinicalInfo',
//		hidden : false,
//		header : '临床信息(补充)',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'result',
		hidden : true,
		header : '结果',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(isGood),editor: isGood
	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : 'T13高风险'
				},{
				id : '1',
				name : 'T18高风险'
				},{
				id : '2',
				name : 'T21高风险'
			},{
				id : '3',
				name : '低风险'
			},{
				id : '4',
				name : '重抽血'
			},{
				id : '5',
				name : '退费'
			},{
				id : '6',
				name : '重建库'
			},{
				id : '7',
				name : '重上机'
			},{
				id : '8',
				name : '待反馈'
			}
			]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'method',
		hidden : false,
		header : '处理方式',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(isGood),
		editor: isGood
	});
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '是'
			},{
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	
	var isRepost = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '是'
			},{
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'isReport',
		hidden : false,
		header : '是否出报告',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(isRepost),editor: isRepost
	});
	
	
	cm.push({
		dataIndex : 'submit',
		hidden : true,
		header : '是否提交',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(submit),editor: submit
	});
	
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : '备注',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex : 'analysisTask-id',
//		hidden : true,
//		header : '关联主表ID',
//		width : 20 * 10
//	});
//	cm.push({
//		dataIndex : 'analysisTask-name',
//		hidden : true,
//		header : '关联主表',
//		width : 20 * 10
//	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : '状态ID',
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'stateName',
		hidden : true,
		header : '状态',
		width : 20 * 10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/analy/analysisManager/showAnalysisManagerListJson.action";
	var opts={};
	opts.title="信息分析审核";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];

	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	analysisManagerGrid=gridEditTable("analysisManagerdiv",cols,loadParam,opts);
	$("#analysisManagerdiv").data("analysisManagerGrid", analysisManagerGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//保存
function save(){
	var selectRecord = analysisManagerGrid.getSelectionModel();
	//var inItemGrid = $("#analysisManagerdiv").data("analysisManagerGrid");
	var selRecord = analysisManagerGrid.store;
	var itemJson = commonGetModifyRecords(analysisManagerGrid);
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			var pCode=obj.get("isReport");
			if(pCode == ""){
				isRepeat = true;
				message("请选择是否出报告！");
				return;
			}
			if(!isRepeat){
				if(itemJson.length>0){
					ajax("post", "/analysis/analy/analysisManager/saveAnalysisManager.action", {
						itemDataJson : itemJson
					}, function(data) {
						if (data.success) {
							$("#analysisManagerdiv").data("analysisManagerGrid").getStore().commitChanges();
							$("#analysisManagerdiv").data("analysisManagerGrid").getStore().reload();
							message("保存成功！");
						} else {
							message("保存失败！");
						}
					}, null);			
			}else{
				message("没有需要保存的数据！");
			}
		}
		});
	}
	
	
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
			commonSearchAction(analysisManagerGrid);
			$(this).dialog("close");
		},
		"清空" : function() {
			form_reset();
		}
	}, true, option);
}	
