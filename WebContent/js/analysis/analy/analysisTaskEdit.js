﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
//	if($("#analysisTask_stateName").val()!="完成"){
//		load("/analysis/analy/analysisTask/showAnalysisInfoTaskList.action", {}, "#analysisTaskEditPage");
//		$("#markup").css("width","0%");
//	}
});	
function add() {
	window.location = window.ctx + "/analysis/analy/analysisTask/editAnalysisTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/analysis/analy/analysisTask/showAnalysisTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var flag=true;
	for(var i=0;i<analysisInfoGrid.store.getCount();i++){
		if(analysisInfoGrid.store.getAt(i).get("submit")=="1" && analysisInfoGrid.store.getAt(i).get("result") !=null){
			flag=true;
		}else{
			flag=false;
			break;
		}
	}
	if(flag){
		save();
	}
//	else{
//		message("请选择是否提交！");
//		return;
//	}
//	var selectRecord=analysisItemGrid.store;
//	var selRecord=analysisInfoGrid.store;
//	if (selectRecord.getCount()> 0) {
//		$.each(selectRecord.getSelections(), function(i, obj) {
//		for(var j=0;j<selectRecord.getCount();j++){
//			for(var i=0;i<selRecord.getCount();i++){
//				var d1 = selectRecord.getAt(j).get("sampleCode");
//				var d2 = selRecord.getAt(i).get("sampleCode");
//				if(d1 != d2){
////					isRepeat = true;
//					message("样本编号不一致！");
//					return;					
//				}
//			}
//		}
//	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#analysisTask", {
					userId : userId,
					userName : userName,
					formId : $("#analysisTask_id").val(),
					title : $("#analysisTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#analysisTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function DeSequencingTaskFun(){
	var DeSequencingTaskFun=null;
	var win = Ext.getCmp('DeSequencingTaskFun');
	if (win) {win.close();}
	DeSequencingTaskFun= new Ext.Window({
	id:'DeSequencingTaskFun',modal:true,title:'选择下机质控号',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/analysis/desequencing/deSequencingTask/deSequencingTaskSelect.action?flag=DeSequencingTaskFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 DeSequencingTaskFun.close(); }  }]  });     DeSequencingTaskFun.show(); }

function setDeSequencingTaskFun(rec){
	document.getElementById('analysisTask_desequencingTask').value=rec.get('id');
	document.getElementById('analysisTask_desequencingTask_name').value=rec.get('name');
	var win = Ext.getCmp('DeSequencingTaskFun');
	var id=rec.get("id");
	ajax("post", "/analysis/analy/analysisTask/getdeSequencingTaskList.action", {
		code : id,
		}, function(data) {
			if (data.success) {	
				var ob = analysisItemGrid.getStore().recordType;
				analysisItemGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("name", obj.sName);
					p.set("sampleCode", obj.sampleCode);
					analysisItemGrid.getStore().add(p);							
				});
				
				analysisItemGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
	if(win){win.close();}
	}



function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
//	    var analysisItemDivData = $("#analysisItemdiv").data("analysisItemGrid");
//		document.getElementById('analysisItemJson').value = commonGetModifyRecords(analysisItemDivData);
	    var analysisInfoDivData = $("#analysisInfodiv").data("analysisInfoGrid");
		document.getElementById('analysisInfoJson').value = commonGetModifyRecords(analysisInfoDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/analysis/analy/analysisTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/analysis/analy/analysisTask/copyAnalysisTask.action?id=' + $("#analysisTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#analysisTask_id").val() + "&tableId=analysisTask");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#analysisTask_id").val()){
		commonChangeState("formId=" + $("#analysisTask_id").val() + "&tableId=AnalysisTask");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'信息分析',
	    	   contentEl:'markup'
	       } ]
	   });
});
//load("/analysis/analy/analysisTask/showAnalysisItemList.action", {
//				id : $("#analysisTask_id").val()
//			}, "#analysisItempage");
load("/analysis/analy/analysisTask/showAnalysisInfoList.action", {
				id : $("#analysisTask_id").val()
			}, "#analysisInfopage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);