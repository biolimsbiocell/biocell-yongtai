var analysisTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
	    	name:'flowCell',
	    	type:"string"
	    });
	    fields.push({
	    	name:'outPut',
	    	type:"string"
	    });
	    fields.push({
	    	name:'cluster',
	    	type:"string"
	    });
	    fields.push({
	    	name:'pf',
	    	type:"string"
	    });
	    fields.push({
	    	name:'reasonType',
	    	type:"string"
	    });
	    fields.push({
	    	name:'detailed',
	    	type:"string"
	    });
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
	    	name:'downComputerTime',
	    	type:"string"
	    });
	    fields.push({
		name:'jobOrder',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'flowCell',
		header:'Flow Cell号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'outPut',
		header:'产量',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'cluster',
		header:'Cluster',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'pf',
		header:'PF',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'downComputerTime',
		header:'预下机时间',
		width:20*6,
		hidden:false,
		sortable:true
	});
	
//	cm.push({
//		dataIndex:'reasonType',
//		header:'原因分类',
//		width:20*6,
//		hidden:false,
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'detailed',
//		header:'详细情况',
//		width:20*6,
//		hidden:false,
//		sortable:true
//	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'下达人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'下达人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'下达时间',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'jobOrder',
		header:'任务单',
		hidden:true,
		width:20*6,
		sortable:true
	});
		/*cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:'分析员ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:'分析员',
		width:20*10,
		sortable:true
		});*/
	cm.push({
		dataIndex:'acceptDate',
		header:'分析时间',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'工作流id',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/analy/analysisTask/showAnalysisTaskListJson.action";
	var opts={};
	opts.title="信息分析";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	analysisTaskGrid=gridTable("show_analysisTask_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/analysis/analy/analysisTask/editAnalysisTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/analysis/analy/analysisTask/editAnalysisTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/analysis/analy/analysisTask/viewAnalysisTask.action?id=' + id;
}
function exportexcel() {
	analysisTaskGrid.title = '导出列表';
	var vExportContent = analysisTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startacceptDate").val() != undefined) && ($("#startacceptDate").val() != '')) {
					var startacceptDatestr = ">=##@@##" + $("#startacceptDate").val();
					$("#acceptDate1").val(startacceptDatestr);
				}
				if (($("#endacceptDate").val() != undefined) && ($("#endacceptDate").val() != '')) {
					var endacceptDatestr = "<=##@@##" + $("#endacceptDate").val();

					$("#acceptDate2").val(endacceptDatestr);

				}
				
				
				commonSearchAction(analysisTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
