﻿var analysisItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   /*fields.push({
		   name:'analyzer-id',
		   type:"string"
	   });
	   fields.push({
		   name:'analyzer-name',
		   type:"string"
	   });*/
	   fields.push({
		name:'status',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'analysisTask-id',
		type:"string"
	});
	    fields.push({
		name:'analysisTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:'pooling号',
		width:55*6
		/*editor : new Ext.form.TextField({
			allowBlank : false
		})*/
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:'文库号',
		width:55*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本号',
		width:55*6
	});
	/*cm.push({
		dataIndex:'analyzer-id',
		hidden : true,
		header:'分析员',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'analyzer-name',
		hidden : false,
		header:'分析员',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'status',
		hidden : true,
		header:'状态',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:45*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'analysisTask-id',
		hidden : true,
		header:'关联主表ID',
		width:20*10
	});
	cm.push({
		dataIndex:'analysisTask-name',
		hidden : true,
		header:'关联主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/analy/analysisTask/showAnalysisItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="样本明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/analy/analysisTask/delAnalysisItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				analysisItemGrid.getStore().commitChanges();
				analysisItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
			text : '选择关联主表',
			handler : selectanalysisTaskFun
		});
	/*opts.tbar.push({
		text : "生成结果",
		handler :function(){
			generatResult();
		} 
	});*/
	/*opts.tbar.push({
		text : "添加分析员",
		handler :addAnalyzer
	});*/
	/*function generatResult(){
		var id=document.getElementById('analysisTask_id').value;
		ajax("post", "/analysis/analy/analysisTask/getAnalysisTaskList.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = analysisInfoGrid.getStore().recordType;
					analysisInfoGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("name", obj.sName);
						p.set("sampleCode", obj.SISampleCode);
						p.set("age", obj.SIGender);
						p.set("sampleType", obj.SIsampleType);
						p.set("note", obj.SINote);
						p.set("reportDate", obj.SIReportDate);
						analysisInfoGrid.getStore().add(p);							
					});
					analysisInfoGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null);
	 if(win){win.close();}
	}*/
	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = analysisItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							analysisItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	analysisItemGrid=gridEditTable("analysisItemdiv",cols,loadParam,opts);
	$("#analysisItemdiv").data("analysisItemGrid", analysisItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//=======================

function DeSequencingTask(){
		 	var id=document.getElementById('deSequencingTask_id').value;
			ajax("post", "/analysis/desequencing/deSequencingItem/getDeSequencingList.action", {
				code : id,
				}, function(data) {
					if (data.success) {	
						var ob = deSequencingInfoGrid.getStore().recordType;
						deSequencingInfoGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("name", obj.sName);
							p.set("code", obj.code);
							deSequencingInfoGrid.getStore().add(p);							
						});
						
						deSequencingInfoGrid.startEditing(0, 0);		
					} else {
						message("获取明细数据时发生错误！");
					}
				}, null);
		 if(win){win.close();}
	}
//===================

function selectanalysisTaskFun(){
	var selectanalysisTask=null;
	var win = Ext.getCmp('selectanalysisTask');
	if (win) {win.close();}
	selectanalysisTask= new Ext.Window({
	id:'selectanalysisTask',modal:true,title:'选择关联主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/AnalysisTaskSelect.action?flag=analysisTask' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectanalysisTask.close(); }  }]  });     selectanalysisTask.show(); }
	function setanalysisTask(id,name){
		var gridGrid = $("#analysisItemdiv").data("analysisItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('analysisTask-id',id);
			obj.set('analysisTask-name',name);
		});
		var win = Ext.getCmp('selectanalysisTask');
		if(win){
			win.close();
		}
	}
	/*function addAnalyzer() {
		var win = Ext.getCmp('addAnalyzer');
		if (win) {
			win.close();
		}
		var addAnalyzer= new Ext.Window(
				{
					id : 'addAnalyzer',
					modal : true,
					title : '选择分析员',
					layout : 'fit',
					width : 500,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src="
										+ window.ctx
										+ "'/core/user/userSelect.action?flag=addAnalyzer' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text: biolims.common.close,
						handler : function() {
							addAnalyzer.close();
						}
					} ]
				});
		addAnalyzer.show();
	}*/
	function setaddAnalyzer(id,name) {
		var selRecords = analysisItemGrid.getSelectionModel().getSelections();
		$.each(selRecords, function(i, obj) {
			obj.set('analyzer-id', id);
			obj.set('analyzer-name', name);
		});
		var win = Ext.getCmp('addAnalyzer');
		if (win) {
			win.close();
		}
	}
