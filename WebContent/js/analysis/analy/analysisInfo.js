﻿var analysisInfoGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'poolingCode',
		type : "string"
	});
	fields.push({
		name : 'resultOne',
		type : "string"
	});
	fields.push({
		name : 'resultTwo',
		type : "string"
	});
	fields.push({
		name : 'cnv',
		type : "string"
	});
	// =====13=====
	fields.push({
		name : 'tOne13',
		type : "string"
	});
	fields.push({
		name : 'tTwo13',
		type : "string"
	});
	fields.push({
		name : 'a13',
		type : "string"
	});
	fields.push({
		name : 'l13',
		type : "string"
	});
	fields.push({
		name : 'risk13',
		type : "string"
	});
	fields.push({
		name : 'fetal13',
		type : "string"
	});
	fields.push({
		name : 'tFour13',
		type : "string"
	});
	fields.push({
		name : 'sampleNum13',
		type : "string"
	});
	fields.push({
		name : 'tFive13',
		type : "string"
	});
	// ====
	// =====18=====
	fields.push({
		name : 'tOne18',
		type : "string"
	});
	fields.push({
		name : 'tTwo18',
		type : "string"
	});
	fields.push({
		name : 'a18',
		type : "string"
	});
	fields.push({
		name : 'l18',
		type : "string"
	});
	fields.push({
		name : 'risk18',
		type : "string"
	});
	fields.push({
		name : 'fetal18',
		type : "string"
	});
	fields.push({
		name : 'tFour18',
		type : "string"
	});
	fields.push({
		name : 'sampleNum18',
		type : "string"
	});
	fields.push({
		name : 'tFive18',
		type : "string"
	});
	// ====

	// =====21=====
	fields.push({
		name : 'tOne21',
		type : "string"
	});
	fields.push({
		name : 'tTwo21',
		type : "string"
	});
	fields.push({
		name : 'a21',
		type : "string"
	});
	fields.push({
		name : 'l21',
		type : "string"
	});
	fields.push({
		name : 'risk21',
		type : "string"
	});
	fields.push({
		name : 'fetal21',
		type : "string"
	});
	fields.push({
		name : 'tFour21',
		type : "string"
	});
	fields.push({
		name : 'sampleNum21',
		type : "string"
	});
	fields.push({
		name : 'tFive21',
		type : "string"
	});
	// ====
	// =====X=====
	fields.push({
		name : 'tOneX',
		type : "string"
	});
	fields.push({
		name : 'tTwoX',
		type : "string"
	});
	fields.push({
		name : 'lX',
		type : "string"
	});
	fields.push({
		name : 'riskX',
		type : "string"
	});
	fields.push({
		name : 'fetalX',
		type : "string"
	});
	fields.push({
		name : 'tFourX',
		type : "string"
	});
	fields.push({
		name : 'sampleNumX',
		type : "string"
	});
	fields.push({
		name : 'tFiveX',
		type : "string"
	});
	// ====
	// =====Y=====
	fields.push({
		name : 'tOneY',
		type : "string"
	});
	fields.push({
		name : 'tTwoY',
		type : "string"
	});
	fields.push({
		name : 'lY',
		type : "string"
	});
	fields.push({
		name : 'riskY',
		type : "string"
	});
	fields.push({
		name : 'fetalY',
		type : "string"
	});
	fields.push({
		name : 'tFourY',
		type : "string"
	});
	fields.push({
		name : 'sampleNumY',
		type : "string"
	});
	fields.push({
		name : 'tFiveY',
		type : "string"
	});
	// ====
	fields.push({
		name : 'fetalAdd',
		type : "string"
	});
	fields.push({
		name : 'r13',
		type : "string"
	});
	fields.push({
		name : 'r18',
		type : "string"
	});
	fields.push({
		name : 'r21',
		type : "string"
	});
	fields.push({
		name : 'rx',
		type : "string"
	});
	fields.push({
		name : 'ry',
		type : "string"
	});
	fields.push({
		name : 'seqInfo',
		type : "string"
	});
	fields.push({
		name : 'fetalExceptionSample',
		type : "string"
	});
	fields.push({
		name : 'otherRstExceptionResult',
		type : "string"
	});
	fields.push({
		name : 'age',
		type : "string"
	});
	fields.push({
		name : 'gesWeeks',
		type : "string"
	});
	fields.push({
		name : 'babyNum',
		type : "string"
	});
	fields.push({
		name : 'tsResult',
		type : "string"
	});
	fields.push({
		name : 'weight',
		type : "string"
	});
	fields.push({
		name : 'riskValue21',
		type : "string"
	});
	fields.push({
		name : 'riskValue18',
		type : "string"
	});
	fields.push({
		name : 'protest',
		type : "string"
	});
	fields.push({
		name : 'rawReadNum',
		type : "string"
	});
	
	fields.push({
		name : 'AlignRate',
		type : "string"
	});
	
	fields.push({
		name : 'duplicationRate',
		type : "string"
	});
//	fields.push({
//		name : 'gcContent',
//		type : "string"
//	});
	fields.push({
		name : 'realURNum',
		type : "string"
	});
	fields.push({
		name : 'chr13',
		type : "string"
	});
	fields.push({
		name : 'chr18',
		type : "string"
	});
	fields.push({
		name : 'chr21',
		type : "string"
	});
	fields.push({
		name : 'suggestResult',
		type : "string"
	});
	fields.push({
		name : 'crstResult',
		type : "string"
	});
	fields.push({
		name : 'xrstResult',
		type : "string"
	});
//	fields.push({
//		name : 'readsMb',
//		type : "string"
//	});
	fields.push({
		name : 'alignRate',
		type : "string"
	});
	fields.push({
		name : 'gcContent',
		type : "string"
	});
	fields.push({
		name : 'q30Ratio',
		type : "string"
	});
//	fields.push({
//		name : 'alignRatio',
//		type : "string"
//	});
//	fields.push({
//		name : 'urRatio',
//		type : "string"
//	});
	fields.push({
		name : 'result',
		type : "string"
	});
//	fields.push({
//		name : 'alignRatio',
//		type : "string"
//	});
//	fields.push({
//		name : 'urRatio',
//		type : "string"
//	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'isReport',
		type : "string"
	});
//	fields.push({
//		name : 'sampleType',
//		type : "string"
//	});
//	fields.push({
//		name : 'gender',
//		type : "string"
//	});
//	fields.push({
//		name : 'sampleRcvTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportExpDate',
//		type : "string"
//	});
//	fields.push({
//		name : 'computerTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'machineTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'rcvCNVTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'interpretationTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'rcvPotoTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportToCustomServiceTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'ExceptionSample',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportState',
//		type : "string"
//	});
//	fields.push({
//		name : 'reportSendTime',
//		type : "string"
//	});
//	fields.push({
//		name : 'clinicalInfo',
//		type : "string"
//	});
	fields.push({
		name : 'method',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'submit',
		type : "string"
	});
	fields.push({
		name : 'analysisTask-id',
		type : "string"
	});
	fields.push({
		name : 'analysisTask-name',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	
	
	fields.push({
		name : 'contractId',
		type : "string"
	});
	fields.push({
		name : 'projectId',
		type : "string"
	});
	fields.push({
		name : 'orderType',
		type : "string"
	});
	fields.push({
		name : 'techTaskId',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编号',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'poolingCode',
		hidden : true,
		header : 'pooling号',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleCode',
		hidden : false,
		header : '样本编号',
		width : 20 * 6
	});

	// ==13==
	cm.push({
		dataIndex : 'tOne13',
		hidden : false,
		header : '13号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo13',
		hidden : false,
		header : '13号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a13',
		hidden : false,
		header : '13号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l13',
		hidden : false,
		header : '13号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk13',
		hidden : false,
		header : '13号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal13',
		hidden : false,
		header : '13号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour13',
		hidden : false,
		header : '13号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum13',
		hidden : false,
		header : '13号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive13',
		hidden : false,
		header : '13号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==18==
	cm.push({
		dataIndex : 'tOne18',
		hidden : false,
		header : '18号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo18',
		hidden : false,
		header : '18号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a18',
		hidden : false,
		header : '18号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l18',
		hidden : false,
		header : '18号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk18',
		hidden : false,
		header : '18号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal18',
		hidden : false,
		header : '18号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour18',
		hidden : false,
		header : '18号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum18',
		hidden : false,
		header : '18号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive18',
		hidden : false,
		header : '18号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==21==
	cm.push({
		dataIndex : 'tOne21',
		hidden : false,
		header : '21号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwo21',
		hidden : false,
		header : '21号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'a21',
		hidden : false,
		header : '21号染色体A值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'l21',
		hidden : false,
		header : '21号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'risk21',
		hidden : false,
		header : '21号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetal21',
		hidden : false,
		header : '21号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFour21',
		hidden : false,
		header : '21号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum21',
		hidden : false,
		header : '21号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFive21',
		hidden : false,
		header : '21号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==X==
	cm.push({
		dataIndex : 'tOneX',
		hidden : false,
		header : 'X号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwoX',
		hidden : false,
		header : 'X号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'lX',
		hidden : false,
		header : 'X号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'riskX',
		hidden : false,
		header : 'X号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalX',
		hidden : false,
		header : 'X号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFourX',
		hidden : false,
		header : 'X号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNumX',
		hidden : false,
		header : 'X号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFiveX',
		hidden : false,
		header : 'X号染色体T5',
		width : 20 * 6
	});
	// =====
	// ==Y==
	cm.push({
		dataIndex : 'tOneY',
		hidden : false,
		header : 'Y号染色体T1值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tTwoY',
		hidden : false,
		header : 'Y号染色体T2值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'lY',
		hidden : false,
		header : 'Y号染色体L值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'riskY',
		hidden : false,
		header : 'Y号风险系数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalY',
		hidden : false,
		header : 'Y号胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFourY',
		hidden : false,
		header : 'Y号染色体T4值',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNumY',
		hidden : false,
		header : 'Y号参考样本个数',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tFiveY',
		hidden : false,
		header : 'Y号染色体T5',
		width : 20 * 6
	});
	// =====
	cm.push({
		dataIndex : 'fetalAdd',
		hidden : false,
		header : '加权胎儿浓度',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r13',
		hidden : false,
		header : '13号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r18',
		hidden : false,
		header : '18号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'r21',
		hidden : false,
		header : '21号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'rx',
		hidden : false,
		header : 'X号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'ry',
		hidden : false,
		header : 'Y号染色体',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'seqInfo',
		hidden : false,
		header : '质控情况',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'fetalExceptionSample',
		hidden : false,
		header : '胎儿浓度异常样本',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'otherRstExceptionResult',
		hidden : false,
		header : '其他染色体异常结果',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'age',
		hidden : false,
		header : '年龄',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'gender',
//		hidden : false,
//		header : '性别',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex : 'gender',
		hidden : false,
		header : '性别',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'gesWeeks',
		hidden : false,
		header : '孕周',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'babyNum',
		hidden : false,
		header : '胎儿数量',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tsResult',
		hidden : false,
		header : '唐筛结果',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'weight',
		hidden : false,
		header : '体重',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'riskValue21',
		hidden : false,
		header : '21风险值',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'riskValue18',
		hidden : false,
		header : '18风险值',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'protest',
		hidden : false,
		header : '补充协议',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'rawReadNum',
		hidden : false,
		header : 'Raw Read Num',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'AlignRate',
		hidden : false,
		header : 'Align Rate',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'duplicationRate',
		hidden : false,
		header : 'Duplication Rate',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'realURNum',
		hidden : false,
		header : 'Real UR Num',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'gcContent',
		hidden : false,
		header : 'GC Content',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr13',
		hidden : false,
		header : 'Chr13',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr18',
		hidden : false,
		header : 'Chr18',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'chr21',
		hidden : false,
		header : 'Chr21',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'suggestResult',
		hidden : false,
		header : '结果建议',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crstResult',
		hidden : false,
		header : '常染色体判断结果',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'xrstResult',
		hidden : false,
		header : '性染色体判断结果',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'readsMb',
//		hidden : false,
//		header : 'reads_mb',
//		width : 20 * 6
//	});
	cm.push({
		dataIndex : 'gcContent',
		hidden : false,
		header : 'gc_content',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'readsMb',
//		hidden : false,
//		header : 'reads_mb',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'gcContent',
//		hidden : false,
//		header : 'gc_content',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'readsMb',
//		hidden : false,
//		header : 'reads_mb',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'gcContent',
//		hidden : false,
//		header : 'gc_content',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'q30Ratio',
//		hidden : false,
//		header : 'q30_ratio',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'alignRatio',
//		hidden : false,
//		header : 'align_ratio',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'urRatio',
//		hidden : false,
//		header : 'ur_ratio',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'alignRatio',
//		hidden : false,
//		header : 'align_ratio',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'urRatio',
//		hidden : false,
//		header : 'ur_ratio',
//		width : 20 * 6
//	});
	cm.push({
		dataIndex : 'alignRate',
		hidden : false,
		header : 'alignRate',
		width : 20 * 6
	});
//	cm.push({
//		dataIndex : 'urRatio',
//		hidden : false,
//		header : 'ur_ratio',
//		width : 20 * 6
//	});
//	cm.push({
//		dataIndex : 'resultOne',
//		hidden : false,
//		header : 'result1',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'resultTwo',
//		hidden : false,
//		header : 'result2',
//		width : 20 * 6,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'cnv',
//		hidden : false,
//		header : 'cnv',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'sampleType',
//		hidden : false,
//		header : '样本类型',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'sampleRcvTime',
//		hidden : false,
//		header : '样本接收日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportExpDate',
//		hidden : false,
//		header : '报告截止日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'computerTime',
//		hidden : false,
//		header : '上机时间',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'machineTime',
//		hidden : false,
//		header : '预下机时间',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'rcvCNVTime',
//		hidden : false,
//		header : '收到CNV日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'interpretationTime',
//		hidden : false,
//		header : '解读完成日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'rcvPotoTime',
//		hidden : false,
//		header : '收到图片日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportToCustomServiceTime',
//		hidden : false,
//		header : '报告发客服日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'ExceptionSample',
//		hidden : false,
//		header : '异常样本',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportState',
//		hidden : false,
//		header : '报告状态',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'reportSendTime',
//		hidden : false,
//		header : '报告发出日期',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex : 'clinicalInfo',
//		hidden : false,
//		header : '临床信息(补充)',
//		width : 20 * 6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'result',
		hidden : false,
		header : '结果',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(isGood),editor: isGood
	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : 'T13高风险'
				},{
				id : '1',
				name : 'T18高风险'
				},{
				id : '2',
				name : 'T21高风险'
			},{
				id : '3',
				name : '低风险'
			},{
				id : '4',
				name : '重抽血'
			},{
				id : '5',
				name : '退费'
			},{
				id : '6',
				name : '重建库'
			},{
				id : '7',
				name : '重上机'
			},{
				id : '8',
				name : '待反馈'
			}
			]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'method',
		hidden : false,
		header : '处理方式',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(isGood),
		editor: isGood
	});
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '是'
			},{
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	
	var isRepost = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '是'
			},{
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'isReport',
		hidden : true,
		header : '是否出报告',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(isRepost),editor: isRepost
	});
	
	
	cm.push({
		dataIndex : 'submit',
		hidden : false,
		header : '是否提交',
		width : 20 * 6,
		renderer: Ext.util.Format.comboRenderer(submit),editor: submit
	});
	
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : '备注',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'analysisTask-id',
		hidden : true,
		header : '关联主表ID',
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'analysisTask-name',
		hidden : false,
		header : '关联主表',
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : '状态ID',
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'stateName',
		hidden : true,
		header : '状态',
		width : 20 * 10
	});
	
	cm.push({
		dataIndex : 'contractId',
		hidden : false,
		header : '合同ID',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'projectId',
		hidden : false,
		header : '项目ID',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'orderType',
		hidden : false,
		header : '任务单类型',
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'techTaskId',
		hidden : false,
		header : '科技服务任务单',
		width : 20 * 6
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/analysis/analy/analysisTask/showAnalysisInfoListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = "结果明细";
	opts.height = document.body.clientHeight - 100;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/analysis/analy/analysisTask/delAnalysisInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '选择关联主表',
		handler : selectanalysisTaskFun
	});
	//生成Excel
//	opts.tbar.push({
//		text : '生成Excel',
//		handler : createExcel
//	});
	
	opts.tbar.push({
		text : '读取CSV',
		handler : getCsv
	});
//	function createExcel(){
//		ajax("post", "/analysis/analy/analysisTask/createCsv.action", {
//			taskId : $("#analysisTask_id").val()
//		},function(data){
//			if(data.success){
//				if(data.fileId != ""){
//					message("生成成功！");
//				}else{
//					message("没有要生成的数据！");
//				}
//			}else{
//				message("另一个程序正在使用此文件，进程无法访问。");
//			}
//		},null);
//	}
	
	function getCsv(){
		ajax("post", "/analysis/analy/analysisTask/getCsv.action", {
			taskId : $("#analysisTask_id").val()
		},function(data){
//			message("读取成功！");
		},null);
	}
	function goInExcelcsv() {
		var file = document.getElementById("file-upload1csv").files[0];
		var n = 0;
		var ob = analysisInfoGrid.getStore().recordType;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
				if (n > 0) {
					if (this[0]) {
						var p = new ob({});
						p.isNew = true;
//						var sampleCodes=analysisItemGrid.store;
//            		for ( var i = 0; i < sampleCodes.getCount(); i++) {
//            			if(sampleCodes.getAt(i).get("sampleCode")!=this[0]){
//            				n=n+1;
//            			}else{
            				p.set("sampleCode", this[0]);
    						p.set("tOne13", this[1]);
    						p.set("tTwo13", this[2]);
    						p.set("a13", this[3]);
    						p.set("l13", this[4]);
    						p.set("risk13", this[5]);
    						p.set("fetal13", this[6]);
    						p.set("tFour13", this[7]);
    						p.set("sampleNum13", this[8]);
    						p.set("tFive13", this[9]);

    						p.set("tOne18", this[10]);
    						p.set("tTwo18", this[11]);
    						p.set("a18", this[12]);
    						p.set("l18", this[13]);
    						p.set("risk18", this[14]);
    						p.set("fetal18", this[15]);
    						p.set("tFour18", this[16]);
    						p.set("sampleNum18", this[17]);
    						p.set("tFive18", this[18]);

    						p.set("a21", this[21]);
    						p.set("l21", this[22]);
    						p.set("risk21", this[23]);
    						p.set("fetal21", this[24]);
    						p.set("tFour21", this[25]);
    						p.set("sampleNum21", this[26]);
    						p.set("tFive21", this[27]);

    						p.set("tOneX", this[28]);
    						p.set("tTwoX", this[29]);
    						p.set("lX", this[30]);
    						p.set("riskX", this[31]);
    						p.set("fetalX", this[32]);
    						p.set("tFourX", this[33]);
    						p.set("sampleNumX", this[34]);
    						p.set("tFiveX", this[35]);

    						p.set("tOneY", this[36]);
    						p.set("tTwoY", this[37]);
    						p.set("lY", this[38]);
    						p.set("riskY", this[39]);
    						p.set("fetalY", this[40]);
    						p.set("tFourY", this[41]);
    						p.set("sampleNumY", this[42]);
    						p.set("tFiveY", this[43]);

    						p.set("fetalAdd", this[44]);
    						p.set("gender", this[45]);
    						p.set("r13", this[46]);
    						p.set("r18", this[47]);
    						p.set("r21", this[48]);
    						p.set("rx", this[49]);
    						p.set("ry", this[50]);
    						p.set("seqInfo", this[51]);
    						p.set("fetalExceptionSample", this[52]);
    						p.set("otherRstExceptionResult", this[53]);
    						p.set("age", this[54]);
    						p.set("gesWeeks", this[55]);
    						p.set("babyNum", this[56]);
    						p.set("tsResult", this[57]);
    						p.set("weight", this[58]);
    						p.set("riskValue21", this[59]);
    						p.set("riskValue18", this[60]);
    						p.set("protest", this[61]);
    						p.set("rawReadNum", this[62]);
    						p.set("duplicationRate", this[63]);
    						p.set("realURNum", this[64]);
    						p.set("gcCount", this[65]);
    						p.set("chr13", this[66]);
    						p.set("chr18", this[67]);
    						p.set("chr21", this[68]);
    						p.set("suggestResult", this[69]);
    						p.set("crstResult", this[70]);
    						p.set("xrstResult", this[71]);
    						p.set("method", this[72]);
    						analysisInfoGrid.getStore().insert(0, p);
//            			}
//            			}
					}
				}
				n = n + 1;
			});
		};
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	analysisInfoGrid = gridEditTable("analysisInfodiv", cols, loadParam, opts);
	$("#analysisInfodiv").data("analysisInfoGrid", analysisInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectanalysisTaskFun() {
	var selectanalysisTask = null;
	var win = Ext.getCmp('selectanalysisTask');
	if (win) {
		win.close();
	}
	selectanalysisTask = new Ext.Window(
			{
				id : 'selectanalysisTask',
				modal : true,
				title : '选择关联主表',
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ ctx
									+ "/AnalysisTaskSelect.action?flag=analysisTask' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						selectanalysisTask.close();
					}
				} ]
			});
	selectanalysisTask.show();
}
function setanalysisTask(id, name) {
	var gridGrid = $("#analysisInfodiv").data("analysisInfoGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('analysisTask-id', id);
		obj.set('analysisTask-name', name);
	});
	var win = Ext.getCmp('selectanalysisTask');
	if (win) {
		win.close();
	}
}
