var analysisInfoTaskGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
    	name:'id',
    	type:"string"
    });
    fields.push({
    	name:'sampleCode',
    	type:"string"
    });
    fields.push({
    	name:'checkProject',
    	type:"string"
    });
    fields.push({
    	name:'wkCode',
    	type:"string"
    });
    fields.push({
    	name:'endTime',
    	type:"string"
    });
    fields.push({
    	name:'state',
    	type:"string"
    });
    cols.fields=fields;
    var cm=[];
    cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
    cm.push({
    	dataIndex:'sampleCode',
    	hidden : false,
    	header:'样本编号',
    	width:50*6
    });
    cm.push({
    	dataIndex:'checkProject',
    	hidden : false,
    	header:'检测项目',
    	width:50*6
    });
    cm.push({
    	dataIndex:'wkCode',
    	hidden : false,
    	header:'文库号',
    	width:50*6
    });
    cm.push({
    	dataIndex:'endTime',
    	hidden : false,
    	header:'结束时间',
    	width:50*6
    });
    cm.push({
    	dataIndex:'state',
    	hidden : true,
    	header:'状态',
    	width:20*6
    });
    cols.cm=cm;
    var loadParam={};
	loadParam.url=ctx+"/analysis/analy/analysisTask/showAnalysisInfoTaskListJson.action";
	var opts={};
	opts.title="信息分析";
	opts.height=document.body.clientHeight-34;
	
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	analysisInfoTaskGrid=gridEditTable("show_analysisInfoTask_div",cols,loadParam,opts);
	$("#show_analysisInfoTask_div").data("analysisInfoTaskGrid", analysisInfoTaskGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//添加实验样本
function addItem(){
	var selectRecord=analysisInfoTaskGrid.getSelectionModel();
	var selRecord=analysisItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("sampleCode");
				if(oldv == obj.get("sampleCode")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;					
				}
			}
			if(!isRepeat){
			var ob = analysisItemGrid.getStore().recordType;
			analysisItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("sampleCode", obj.get("sampleCode"));
			p.set("wkCode",obj.get("wkCode"));
			analysisItemGrid.getStore().add(p);
			analysisItemGrid.startEditing(0, 0);	
		}
	});
		$("#markup").css("width","100%");
		$("#analysisTaskEditPage").css("width","0%");
	}else{
		message("请选择样本");
	}
}