var dataTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleId',
		type:"string"
	});
	   fields.push({
		name:'crmPatientId',
		type:"string"
	});
	   fields.push({
		name:'form',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	    fields.push({
			name:'MicroRNA',
			type:"string"
		});
	    fields.push({
			name:'type',
			type:"string"
		});
	    
	    fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-name',
			type:"string"
		});
	   
	    fields.push({
			name:'state',
			type:"string"
		});
	    //新增字段
	    fields.push({
			name:'poolingId',
			type:"string"
		});
	    fields.push({
			name:'sampleTypeId',
			type:"string"
		});
	    fields.push({
			name:'sampleTypeName',
			type:"string"
		});   
	    fields.push({
			name:'cancerType',
			type:"string"
		});
	  
	    fields.push({
			name:'cancerTypeSeedOne',
			type:"string"
		});
	    
	    fields.push({
			name:'cancerTypeSeedTwo',
			type:"string"
		});
	    fields.push({
			name:'productId',
			type:"string"
		});
	    fields.push({
			name:'productName',
			type:"string"
		});
	    fields.push({
			name:'sampleCode',
			type:"string"
		});
	    
	    
	    
	cols.fields=fields;
	
//	var storeStateCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '线粒体', '线粒体' ], [ 'MicroRNA', 'MicroRNA' ] ]
//	});
	var cm=[];
	
	/*var stateCob = new Ext.form.ComboBox({
		store : storeStateCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});*/
	
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:15*10
	});
	
	cm.push({
		hidden:false,
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		sortable:true,
		width:15*10
	});
	cm.push({
		hidden:false,
		dataIndex:'sampleId',
		header:biolims.common.sampleId,
		sortable:true,
		width:15*10
	});
	cm.push({
		hidden:true,
		dataIndex:'poolingId',
		header:biolims.pooling.code,
		width:15*10
	});
	cm.push({
		hidden:false,
		dataIndex:'crmPatientId',
		header:biolims.sample.medicalNumber,
		width:15*10
	});
	
	cm.push({
		dataIndex:'form',
		hidden : true,
		header:biolims.common.type,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:biolims.analysis.dataTaskId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : true,
		header:biolims.analysis.dataTaskName,
		width:15*10

	});
	cm.push({
		dataIndex:'MicroRNA',
		hidden : true,
		header:'MicroRNA',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'type',
		hidden : true,
		header:biolims.analysis.type,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleTypeId',
		hidden : true,
		header:biolims.common.sampleTypeId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleTypeName',
		hidden : false,
		header:biolims.common.sampleTypeName,
		width:15*10

	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:15*10

	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*10

	});
	/*var sampleStagestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'I' ], [ '2', 'II' ],[ '3', 'III' ],[ '4', 'IV' ]]
	});
	
	var sampleStageComboxFun = new Ext.form.ComboBox({
		store : sampleStagestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
	/*cm.push({
		dataIndex:'sampleStage',
		header:'分期',
		width:10*6,
		
		renderer: Ext.util.Format.comboRenderer(sampleStageComboxFun),				
		sortable:true
	});
	*/
	cm.push({
		dataIndex:'cancerType',
		hidden : false,
		header:biolims.sample.dicTypeName,
		width:15*10

	});
	
	cm.push({
		dataIndex:'cancerTypeSeedOne',
		hidden : false,
		header:biolims.sample.cancerTypeSeedOne,
		width:15*10

	});
	cm.push({
		dataIndex:'cancerTypeSeedTwo',
		hidden : false,
		header:biolims.sample.cancerTypeSeedTwo,
		width:15*10

	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/data/dataTask/showDataTaskTempListJson.action?id="+ $("#dataTask_id").val()+"&fcId="+$("#dataTask_fcnumber").val();
	loadParam.limit = 1000;
	var opts={};
	opts.title=biolims.analysis.sample2Analysed;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : function() {
			var selectRecord=dataTaskTempGrid.getSelectionModel();
			var selRecord=dataTaskItemGrid.store;
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					var isRepeat = false;
					for(var j=0;j<selRecord.getCount();j++){
						var oldv = selRecord.getAt(j).get("sampleId");
						if(oldv == obj.get("sampleId")){
							isRepeat = true;
							message(biolims.common.haveDuplicate);
							return;					
						}
					}
					if(!isRepeat){
					var ob = dataTaskItemGrid.getStore().recordType;
					dataTaskItemGrid.stopEditing();
					var p = new ob({});
					p.isNew = true;
					//p.set("id","NEW");
//					p.set("blood-id",obj.get("id"));
//					p.set("blood-code",obj.get("code"));
					p.set("sampleId",obj.get("sampleId"));
					p.set("poolingId",obj.get("poolingId"));
					p.set("crmPatientId",obj.get("crmPatientId"));
					p.set("sampleCode",obj.get("sampleCode"));
					p.set("sampleTypeId",obj.get("sampleTypeId"));
					p.set("sampleTypeName",obj.get("sampleTypeName"));
					
					p.set("cancerType",obj.get("cancerType"));
					p.set("cancerTypeSeedOne",obj.get("cancerTypeSeedOne"));
					p.set("cancerTypeSeedTwo",obj.get("cancerTypeSeedTwo"));
					p.set("tempId",obj.get("id"));
					p.set("productId",obj.get("productId"));
					p.set("productName",obj.get("productName"));
					p.set("sampleCode",obj.get("sampleCode"));
					dataTaskItemGrid.getStore().add(p);
					dataTaskItemGrid.startEditing(0, 0);
				}
			});
			}else{
				message(biolims.common.pleaseChooseSamples);
			}
		}
	});
//       opts.delSelect = function(ids) {
//		ajax("post", "/analysis/data/dataTask/delDataTaskTemp.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				dataTaskTempGrid.getStore().commitChanges();
//				dataTaskTempGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
	 
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
	   dataTaskTempGrid=gridEditTable("dataTaskTempdiv",cols,loadParam,opts);
	$("#dataTaskTempdiv").data("dataTaskTempGrid", dataTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

