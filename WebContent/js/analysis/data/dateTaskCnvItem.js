var dateTaskCnvItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'mutantGenes',
		type:"string"
	});
	   fields.push({
		name:'otherName',
		type:"string"
	});
	   fields.push({
		name:'copyNumberVaration',
		type:"string"
	});
	   fields.push({
		name:'copyVarationRatio',
		type:"string"
	});
	   fields.push({
		name:'copyNumberVarationExon',
		type:"string"
	});
	   fields.push({
		name:'mutationClass',
		type:"string"
	});
	   /*fields.push({
		name:'mutationType',
		type:"string"
	});*/
	   fields.push({
		name:'mutationFiction',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	   fields.push({
		name:'createDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
			name:'sampleCode',
			type:"string"
	});
	   fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	}); 
	    fields.push({
		name:'validState',
		type:"string"
	}); 
	    fields.push({
		name:'accordance',
		type:"string"
	});
    fields.push({
		name:'relatedMutation',
		type:"string"
	}); 
	    fields.push({
		name:'relatedChemotherapy',
		type:"string"
	});  
	    fields.push({
			name:'mutationPosition',
			type:"string"
		}); 
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'acceptUser-id',
		hidden : true,
		header:biolims.common.testUserId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'acceptUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:100
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var accordanceStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.analysis.inconsistency ], [ '1', biolims.analysis.consistency ],]
	});
	
	var accordanceComboxFun = new Ext.form.ComboBox({
		store : accordanceStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'accordance',
		hidden : false,
		header:biolims.analysis.accordance,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(accordanceComboxFun),
		editor: accordanceComboxFun,
		sortable:true
	});
	var validStateStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.master.invalid], [ '1', biolims.master.valid ],]
	});
	
	var validStateComboxFun = new Ext.form.ComboBox({
		store : validStateStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'validState',
		hidden : false,
		header:biolims.analysis.validState,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(validStateComboxFun),
		editor: validStateComboxFun,
		sortable:true
	});
	//得到焦点时发生的事件
	mutantGenesType3 =new Ext.form.TextField({
        allowBlank: false
	});
	mutantGenesType3.on('focus', function() {
		showmutantGenesType3();
	});
	cm.push({
		dataIndex:'mutantGenes',
		header:biolims.analysis.mutantGenes,
		width:15*10,
		sortable:true,
		editor : mutantGenesType3
	});
//	cm.push({
//		dataIndex:'mutantGenes',
//		hidden : false,
//		header:'突变基因',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'otherName',
		hidden : false,
		header:biolims.analysis.otherName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutationPosition',
		hidden : false,
		header:biolims.master.chromosomeSite,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var copyNumberVarationStore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '基因扩增' ], [ '2', '拷贝数缺失' ],]
//	});
	
	/*var copyNumberVarationComboxFun = new Ext.form.ComboBox({
		store : copyNumberVarationStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
	/*cm.push({
		dataIndex:'copyNumberVaration',
		hidden : false,
		header:'拷贝数变异',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(copyNumberVarationComboxFun),
		editor: copyNumberVarationComboxFun,
		sortable:true
	});*/
	var treatstorekbby = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=cnvkbsby',
			method : 'POST'
		})
	});
	treatstorekbby.load();
	var treatcobkbby = new Ext.form.ComboBox({
		store : treatstorekbby,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'copyNumberVaration',
		header : biolims.analysis.copyNumberVaration,
		width : 10 * 15,
		renderer : Ext.util.Format.comboRenderer(treatcobkbby),
		editor : treatcobkbby
	});
//	cm.push({
//		dataIndex:'copyNumberVaration',
//		hidden : false,
//		header:'拷贝数变异',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'copyNumberVarationExon',
		hidden : false,
		header:biolims.analysis.copyNumberVarationExon,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'copyVarationRatio',
		hidden : false,
		header:biolims.analysis.copyVarationRatio,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*var mutationClasssStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '种系突变' ], [ '2', '多态性突变' ],[ '3', '肿瘤突变' ],]
	});
	
	var mutationClassComboxFun = new Ext.form.ComboBox({
		store : mutationClasssStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
	
	var treatstorefl = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=cnvtbfl',
			method : 'POST'
		})
	});
	treatstorefl.load();
	var treatcobfl = new Ext.form.ComboBox({
		store : treatstorefl,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'mutationClass',
		header : biolims.analysis.mutationClass,
		width : 10 * 10,
		renderer : Ext.util.Format.comboRenderer(treatcobfl),
		editor : treatcobfl
	});
	/*cm.push({
		dataIndex:'mutationClass',
		hidden : false,
		header:'突变分类',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationClassComboxFun),
		editor: mutationClassComboxFun,
		sortable:true
	});*/
	/*cm.push({
		dataIndex:'relatedMutation',
		hidden : false,
		header:'靶向相关突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationClassComboxFun),
		editor: mutationClassComboxFun,
		sortable:true
	});*/
	var treatstorecnvbfj = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=cnvbtbfj',
			method : 'POST'
		})
	});
	treatstorecnvbfj.load();
	var treatcobcnvbfj = new Ext.form.ComboBox({
		store : treatstorecnvbfj,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'relatedMutation',
		header : biolims.analysis.relatedMutation,
		width : 10 * 13,
		renderer : Ext.util.Format.comboRenderer(treatcobcnvbfj),
		editor : treatcobcnvbfj
	});
	
	/*cm.push({
		dataIndex:'relatedChemotherapy',
		hidden : false,
		header:'化疗相关突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationClassComboxFun),
		editor: mutationClassComboxFun,
		sortable:true
	});*/
	var treatstorecnvhfj = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=cnvstbfj',
			method : 'POST'
		})
	});
	treatstorecnvhfj.load();
	var treatcobcnvhfj = new Ext.form.ComboBox({
		store : treatstorecnvhfj,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'relatedChemotherapy',
		header : biolims.analysis.relatedChemotherapy,
		width : 10 * 15,
		renderer : Ext.util.Format.comboRenderer(treatcobcnvhfj),
		editor : treatcobcnvhfj
	});
//	cm.push({
//		dataIndex:'mutationClass',
//		hidden : false,
//		header:'突变分类',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	/*var mutationTypeStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [
		        [ '1', '基因融合' ], [ '2', '基因拷贝数变异' ],[ '3', '同义突变' ],[ '4', '错义突变' ],
		        [ '5', '插入非移码突变' ],[ '6', '插入移码突变' ],[ '7', '缺失非移码突变' ],[ '8', '缺失移码突变' ],
		        [ '9', '无义突变' ],[ '10', '剪切突变' ],[ '11', '其他突变' ],
		       ]
	});*/
	
	/*var mutationTypeComboxFun = new Ext.form.ComboBox({
		store : mutationTypeStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
	/*cm.push({
		dataIndex:'mutationType',
		hidden : false,
		header:'突变类型',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationTypeComboxFun),
		editor: mutationTypeComboxFun,
		sortable:true
	});*/
//	cm.push({
//		dataIndex:'mutationType',
//		hidden : false,
//		header:'突变类型',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	/*var mutationFictionStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'level1' ], [ '2', 'level2' ],[ '3', 'level3' ],[ '4', 'level4' ],]
	});
	
	var mutationFictionComboxFun = new Ext.form.ComboBox({
		store : mutationFictionStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'mutationFiction',
		hidden : false,
		header:'突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});*/
//	cm.push({
//		dataIndex:'mutationFiction',
//		hidden : false,
//		header:'突变分级',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/data/dataTask/showDateTaskCnvItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.analysis.copyNumberVaration;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/data/dataTask/delDateTaskCnvItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dateTaskCnvItemGrid.getStore().commitChanges();
				dateTaskCnvItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	/*opts.tbar.push({
			text : '选择相关主表',
				handler : selectdataTaskDialogFun
		});
	opts.tbar.push({
			text : '选择下达人',
				handler : selectcreateUserFun
		});*/
//	opts.tbar.push({
//		text : "上传csv文件",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsvCnv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
	
	var stateName = $("#dataTask_stateName").val();
	if(stateName == biolims.common.uploaded){
		opts.tbar.push({
			text : biolims.analysis.experimentalResultsComparison,
			handler : function() {
				cnVcontrast();
			}
		});
	}
	function goInExcelcsv(){
		var file = document.getElementById("file-cnvuploadcsv").files[0];  
		var n = 0;
		var ob = dateTaskCnvItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
	            	if(n>0){
	            		if(this[0]){
	            			var p = new ob({});
	            			p.isNew = true;		
	            			p.set("sampleCode",this[0]);
	            			p.set("mutantGenes",this[1]);
	            			p.set("otherName",this[2]);
	            			p.set("mutationPosition",this[3]);
	            			p.set("copyNumberVaration",this[4]);
	            			p.set("copyNumberVarationExon",this[5]);
	            			p.set("copyVarationRatio",this[6]);
	            			p.set("mutationClass",this[7]);
	            			p.set("relatedMutation",this[8]);
	            			p.set("relatedChemotherapy",this[9]);
	            			p.set("dataTask-id",$("#dataTask_id").val());
	            			p.set("state","0");
	            			p.set("stateName",biolims.common.create);
	            			p.set("acceptUser-id",window.userId);
	            			p.set("acceptUser-name",window.userName);
							dateTaskCnvItemGrid.getStore().insert(0, p);
	            		}
	            	}
	                 n = n +1;
	            	
	            });
		};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	dateTaskCnvItemGrid=gridEditTable("dateTaskCnvItemdiv",cols,loadParam,opts);
	$("#dateTaskCnvItemdiv").data("dateTaskCnvItemGrid", dateTaskCnvItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectdataTaskFun(){
	var win = Ext.getCmp('selectdataTask');
	if (win) {win.close();}
	var selectdataTask= new Ext.Window({
	id:'selectdataTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectdataTask.close(); }  }]  }) });  
    selectdataTask.show(); }
	function setdataTask(rec){
		var gridGrid = $("#dateTaskCnvItemdiv").data("dateTaskCnvItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('dataTask-id',rec.get('id'));
			obj.set('dataTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectdataTask');
		if(win){
			win.close();
		}
	}
	function selectdataTaskDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/DataTaskSelect.action?flag=dataTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						seldataTaskVal(this);
				}
			}, true, option);
		}
	var seldataTaskVal = function(win) {
		var operGrid = dataTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dateTaskCnvItemdiv").data("dateTaskCnvItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('dataTask-id',rec.get('id'));
				obj.set('dataTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
function selectcreateUserFun(){
	var win = Ext.getCmp('selectcreateUser');
	if (win) {win.close();}
	var selectcreateUser= new Ext.Window({
	id:'selectcreateUser',modal:true,title:biolims.common.selectGivePeople,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=createUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcreateUser.close(); }  }]  }) ;  
    selectcreateUser.show(); }
	function setcreateUser(rec){
		var gridGrid = $("#dateTaskCnvItemdiv").data("dateTaskCnvItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('createUser-id',rec.get('id'));
			obj.set('createUser-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectcreateUser');
		if(win){
			win.close();
		}
	}
	function selectcreateUserDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectGivePeople;
			url = ctx + "/UserSelect.action?flag=createUser";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selcreateUserVal(this);
				}
			}, true, option);
		}
	var selcreateUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dateTaskCnvItemdiv").data("dateTaskCnvItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('createUser-id',rec.get('id'));
				obj.set('createUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
//	//查询知识库
//	function showmutantGenesType3(){
//		var options = {};
//		options.width = document.body.clientWidth-800;
//		options.height = document.body.clientHeight-40;
//		loadDialogPage(null, "突变基因表", "/kb/knowledgeBaseSelect.action", {
//			"确定" : function() {
//				var operGrid = $("#show_dialog_knowledgeBase_div").data("knowledgeBaseDialogGrid");
//				var selectRecord = operGrid.getSelectionModel().getSelections();
//				
//				var records = dateTaskCnvItemGrid.getSelectRecord();
//				if (selectRecord.length > 0) {
//					$.each(selectRecord, function(i, obj) {
//						$.each(records, function(a, b) {
//							b.set("mutantGenes", obj.get("mutationGenes"));
//
//						});
//					});
//				}else{
//					message("请选择您要选择的数据");
//					return;
//				}
//				$(this).dialog("close");
//			}
//		}, true, options);
//	}
	//查询突变基因对应的信息
	function showmutantGenesType3() {
		var win = Ext.getCmp('showjybcList');
		if (win) {
			win.close();
		}
		var showjybcList = new Ext.Window(
				{
					id : 'showjybcList',
					modal : true,
					title : biolims.analysis.mutantGenes,
					layout : 'fit',
					width : 780,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/dic/type/dicTypeSelect.action?flag=jyrstw' frameborder='0' width='780' height='500' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							showjybcList.close();
						}
					} ]
				});
		showjybcList.show();
	}
	function setjyrstw(id,name) {
		var selectRecord = dateTaskCnvItemGrid.getSelectionModel().getSelections();
		for(var i=0;i<selectRecord.length;i++){
			ajax("post", "/analysis/data/dataTask/selDicType.action", {
				name : name
			}, function(data) {
				if(data.success){
					selectRecord[i].set("mutantGenes",name);
					if(data.d!=null){
						selectRecord[i].set("otherName",data.d.note);
					}
					if(data.d3!=null){
						selectRecord[i].set("mutationPosition",data.d3.note);
					}
				}
			}, null);
		}
		var win = Ext.getCmp('showjybcList');
		if (win) {
			win.close();
		}
	}
	function cnVcontrast(){
		 var csVresult = dateTaskCnvItemGrid.store;
		 var n=csVresult.getCount()/2;
		 for(var i=0;i<n;i++){
			 for(var j=n;j<csVresult.getCount();j++){
				 if(csVresult.getAt(i).get("sampleCode")==csVresult.getAt(j).get("sampleCode")&& csVresult.getAt(i).get("mutantGenes") == csVresult.getAt(j).get("mutantGenes")){
					 var m = 0;
					//拷贝数变异
					 if(csVresult.getAt(i).get("copyNumberVaration") != csVresult.getAt(j).get("copyNumberVaration")){
						 m = 1;
					 }
					 //拷贝数变异总外显子
					 if(csVresult.getAt(i).get("copyNumberVarationExon") != csVresult.getAt(j).get("copyNumberVarationExon")){
						 m = 1;
					 }
					 //拷贝数变异倍数
					 if(csVresult.getAt(i).get("copyVarationRatio") != csVresult.getAt(j).get("copyVarationRatio")){
						 m = 1;
					 }
					 //突变分类
					 if(csVresult.getAt(i).get("mutationClass") != csVresult.getAt(j).get("mutationClass")){
						 m = 1;
					 }
					 if(m ==1){
						 csVresult.getAt(i).set("accordance","0");
						 csVresult.getAt(j).set("accordance","0");
					 }else{
						 csVresult.getAt(i).set("accordance","1");
						 csVresult.getAt(j).set("accordance","1");
						 csVresult.getAt(j).set("validState","1");
						 csVresult.getAt(i).set("validState","");
					 }
				 }
			 }
		 }
	}

	
//var	taskgridCnv = gridEditTable("dateTaskCnvItemdiv", cols, loadParam, opts,operParams);
	setTimeout(function(){
	   //从工作流中待办事项中进入	
            if($("#bpmTaskId").val()&&$("#bpmTaskId").val()!='null'){
			
                        //判断是否筛选完，如果没有，就需要按分析人显示
			if($("#dataTask_stateName").val()!='已上传'){
				sxCnv();	
			}
		}
	},100);
function sxCnv() {
	dateTaskCnvItemGrid.store.reload();
	var filter3 = function(record, id){
		var flag = true;
		   if (record.get("acceptUser-id")==window.userId){
		      	 flag = true;
		      }
		   else{
			     flag =  false; 
		   }
		   return flag;
		};
		var onStoreLoad3 = function(store, records, options){
		   store.filterBy(filter3);
		};
		dateTaskCnvItemGrid.store.on("load", onStoreLoad3);
}