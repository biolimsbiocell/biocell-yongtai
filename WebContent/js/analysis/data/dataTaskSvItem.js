var dataTaskSvItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'mutantGenes',
		type:"string"
	});
	   fields.push({
		name:'otherName',
		type:"string"
	});
	   fields.push({
		name:'partnerGenes',
		type:"string"
	});
	   fields.push({
		name:'fusionProduct',
		type:"string"
	});
	   fields.push({
		name:'mutantGeneScript',
		type:"string"
	});
	   fields.push({
		name:'partnerScript',
		type:"string"
	});
	   fields.push({
		name:'mutantGenePoint',
		type:"string"
	});
	   fields.push({
		name:'partnerPoint',
		type:"string"
	});
	   /*fields.push({
		name:'mutationStopPosition',
		type:"string"
	});*/
	  /* fields.push({
		name:'mutationSource',
		type:"string"
	});*/
	   fields.push({
		name:'mutationClass',
		type:"string"
	});
	   /*fields.push({
		name:'mutationType',
		type:"string"
	});*/
	   /*fields.push({
		name:'mutationStatus',
		type:"string"
	});*/
	   fields.push({
		name:'mutatinAbundance',
		type:"string"
	});
	   fields.push({
		name:'mutationFiction',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	   fields.push({
		name:'createDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
			name:'sampleCode',
			type:"string"
	});
	   fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	}); 
	    fields.push({
		name:'validState',
		type:"string"
	}); 
	    fields.push({
		name:'accordance',
		type:"string"
	});
	    
    fields.push({
		name:'MutantGeneLc',
		type:"string"
	}); 
	    fields.push({
		name:'partnerLc',
		type:"string"
	});
    fields.push({
		name:'relatedMutation',
		type:"string"
	}); 
    fields.push({
		name:'relatedChemotherapy',
		type:"string"
	});
    fields.push({
		name:'partnerGenesName',
		type:"string"
	});
    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'acceptUser-id',
		hidden : true,
		header:biolims.common.testUserId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'acceptUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:100
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var accordanceStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.analysis.different], [ '1', biolims.analysis.same ],]
	});
	
	var accordanceComboxFun = new Ext.form.ComboBox({
		store : accordanceStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'accordance',
		hidden : false,
		header:biolims.analysis.accordance,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(accordanceComboxFun),
		editor: accordanceComboxFun,
		sortable:true
	});
	var validStateStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.master.invalid ], [ '1', biolims.master.valid ],]
	});
	
	var validStateComboxFun = new Ext.form.ComboBox({
		store : validStateStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'validState',
		hidden : false,
		header:biolims.analysis.validState,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(validStateComboxFun),
		editor: validStateComboxFun,
		sortable:true
	});
	//得到焦点时发生的事件
	mutantGenesType1 =new Ext.form.TextField({
        allowBlank: false
	});
	mutantGenesType1.on('focus', function() {
		showmutantGenesType1();
	});
	cm.push({
		dataIndex:'mutantGenes',
		header:biolims.analysis.mutantGenes,
		width:15*10,
		sortable:true,
		editor : mutantGenesType1
	});
	cm.push({
		dataIndex:'otherName',
		hidden : false,
		header:biolims.analysis.otherName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'partnerGenes',
		hidden : false,
		header:biolims.analysis.partnerGenes,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'partnerGenesName',
		hidden : false,
		header:biolims.analysis.partnerGenesName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fusionProduct',
		hidden : false,
		header:biolims.analysis.fusionProduct,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutantGeneScript',
		hidden : false,
		header:biolims.analysis.mutantGeneScript,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'partnerScript',
		hidden : false,
		header:biolims.analysis.partnerScript,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'MutantGeneLc',
		hidden : false,
		header:biolims.analysis.MutantGeneLc,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'partnerLc',
		hidden : false,
		header:biolims.analysis.partnerLc,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutantGenePoint',
		hidden : false,
		header:biolims.analysis.mutantGenePoint,
		width:20*7,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'partnerPoint',
		hidden : false,
		header:biolims.analysis.partnerPoint,
		width:20*7,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'mutationStopPosition',
		hidden : false,
		header:'突变终止位置',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutationSource',
		hidden : false,
		header:'突变来源',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	var mutationClasssStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.analysis.germlineMutation ], [ '2', biolims.analysis.polymorphismMutation ],[ '3', biolims.analysis.tumorMutation ],]
	});
	
	var mutationClassComboxFun = new Ext.form.ComboBox({
		store : mutationClasssStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	/*cm.push({
		dataIndex:'mutationClass',
		hidden : false,
		header:'突变分类',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationClassComboxFun),
		editor: mutationClassComboxFun,
		sortable:true
	});*/
	var treatstoresvfl = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=svtbfl',
			method : 'POST'
		})
	});
	treatstoresvfl.load();
	var treatcobsvfl = new Ext.form.ComboBox({
		store : treatstoresvfl,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'mutationClass',
		header : biolims.analysis.mutationClass,
		width : 10 * 10,
		renderer : Ext.util.Format.comboRenderer(treatcobsvfl),
		editor : treatcobsvfl
	});
	/*var mutationTypeStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [
		        [ '1', '基因融合' ], [ '2', '基因拷贝数变异' ],[ '3', '同义突变' ],[ '4', '错义突变' ],
		        [ '5', '插入非移码突变' ],[ '6', '插入移码突变' ],[ '7', '缺失非移码突变' ],[ '8', '缺失移码突变' ],
		        [ '9', '无义突变' ],[ '10', '剪切突变' ],[ '11', '其他突变' ],
		       ]
	});
	var mutationTypeComboxFun = new Ext.form.ComboBox({
		store : mutationTypeStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
	/*cm.push({
		dataIndex:'mutationType',
		hidden : false,
		header:'突变类型',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationTypeComboxFun),
		editor: mutationTypeComboxFun,
		sortable:true
	});*/
//	cm.push({
//		dataIndex:'mutationType',
//		hidden : false,
//		header:'突变类型',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	/*var mutationStatusStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '纯合多态性' ], [ '2', '杂合多态性' ],]
	});*/
	
	/*var mutationStatusComboxFun = new Ext.form.ComboBox({
		store : mutationStatusStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});*/
	/*cm.push({
		dataIndex:'mutationStatus',
		hidden : false,
		header:'突变状态',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationStatusComboxFun),
		editor: mutationStatusComboxFun,
		sortable:true
	});*/
//	cm.push({
//		dataIndex:'mutationStatus',
//		hidden : false,
//		header:'突变状态',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'mutatinAbundance',
		hidden : false,
		header:biolims.analysis.mutatinAbundance,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var mutationFictionStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'level1' ], [ '2', 'level2' ],[ '3', 'level3' ],[ '4', 'level4' ],]
	});
	
	var mutationFictionComboxFun = new Ext.form.ComboBox({
		store : mutationFictionStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	/*cm.push({
		dataIndex:'mutationFiction',
		hidden : false,
		header:'突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});*/
	/*cm.push({
		dataIndex:'relatedMutation',
		hidden : false,
		header:'靶向相关突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});*/
	var treatstoresvbfj = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=svbtbfj',
			method : 'POST'
		})
	});
	treatstoresvbfj.load();
	var treatcobsvbfj = new Ext.form.ComboBox({
		store : treatstoresvbfj,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'relatedMutation',
		header : biolims.analysis.relatedMutation,
		width : 10 * 13,
		renderer : Ext.util.Format.comboRenderer(treatcobsvbfj),
		editor : treatcobsvbfj
	});
	
	/*cm.push({
		dataIndex:'relatedChemotherapy',
		hidden : false,
		header:'化疗相关突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});*/
	var treatstoresvhfj = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=svstbfj',
			method : 'POST'
		})
	});
	treatstoresvhfj.load();
	var treatcobsvhfj = new Ext.form.ComboBox({
		store : treatstoresvhfj,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'relatedChemotherapy',
		header : biolims.analysis.relatedChemotherapy,
		width : 10 * 15,
		renderer : Ext.util.Format.comboRenderer(treatcobsvhfj),
		editor : treatcobsvhfj
	});
	
//	cm.push({
//		dataIndex:'mutationFiction',
//		hidden : false,
//		header:'突变分级',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/data/dataTask/showDataTaskSvItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.analysis.fusionGeneMutationsTable;
	opts.height =  document.body.clientHeight-280;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/data/dataTask/delDataTaskSvItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dataTaskSvItemGrid.getStore().commitChanges();
				dataTaskSvItemGrids.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//		text : "上传csv文件",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_svuploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv1();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
	
	var stateName = $("#dataTask_stateName").val();
	if(stateName == biolims.common.uploaded){
		opts.tbar.push({
			text : biolims.analysis.experimentalResultsComparison,
			handler : function() {
				sVcontrast();
			}
		});
	}
	function goInExcelcsv1(){
		var file = document.getElementById("file-svuploadcsv").files[0];  
		var n = 0;
		var ob = dataTaskSvItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312'); 
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
	            	if(n>0){
	            		if(this[0]){
	            			var p = new ob({});
	            			p.isNew = true;		
	            			p.set("sampleCode",this[0]);
	            			p.set("mutantGenes",this[1]);
	            			p.set("otherName",this[2]);
	            			p.set("partnerGenes",this[3]);
	            			p.set("partnerGenesName",this[4]);
	            			p.set("fusionProduct",this[5]);
	            			p.set("mutantGeneScript",this[6]);
	            			p.set("partnerScript",this[7]);
	            			p.set("MutantGeneLc",this[8]);
	            			p.set("partnerLc",this[9]);
	            			p.set("mutantGenePoint",this[10]);
	            			p.set("partnerPoint",this[11]);
	            			p.set("mutationClass",this[12]);
	            			p.set("mutatinAbundance",this[13]);
	            			p.set("relatedMutation",this[14]);
	            			p.set("relatedChemotherapy",this[15]);
	            			p.set("dataTask-id",$("#dataTask_id").val());
	            			p.set("state","0");
	            			p.set("stateName","新建");
	            			p.set("acceptUser-id",window.userId);
	            			p.set("acceptUser-name",window.userName);
							dataTaskSvItemGrid.getStore().insert(0, p);
	            		}
	            	}
	                 n = n +1;
	            	
	            });
		};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	dataTaskSvItemGrid=gridEditTable("dataTaskSvItemdiv",cols,loadParam,opts);
	$("#dataTaskSvItemdiv").data("dataTaskSvItemGrid", dataTaskSvItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectdataTaskFun(){
	var win = Ext.getCmp('selectdataTask');
	if (win) {win.close();}
	var selectdataTask= new Ext.Window({
	id:'selectdataTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectdataTask.close(); }  }]  }) });  
    selectdataTask.show(); }
	function setdataTask(rec){
		var gridGrid = $("#dataTaskSvItemdiv").data("dataTaskSvItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('dataTask-id',rec.get('id'));
			obj.set('dataTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectdataTask')
		if(win){
			win.close();
		}
	}
	function selectdataTaskDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/DataTaskSelect.action?flag=dataTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						seldataTaskVal(this);
				}
			}, true, option);
		}
	var seldataTaskVal = function(win) {
		var operGrid = dataTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dataTaskSvItemdiv").data("dataTaskSvItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('dataTask-id',rec.get('id'));
				obj.set('dataTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
function selectcreateUserFun(){
	var win = Ext.getCmp('selectcreateUser');
	if (win) {win.close();}
	var selectcreateUser= new Ext.Window({
	id:'selectcreateUser',modal:true,title:biolims.common.selectGivePeople,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=createUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcreateUser.close(); }  }]  }) ;  
    selectcreateUser.show(); }
	function setcreateUser(rec){
		var gridGrid = $("#dataTaskSvItemdiv").data("dataTaskSvItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('createUser-id',rec.get('id'));
			obj.set('createUser-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectcreateUser')
		if(win){
			win.close();
		}
	}
	function selectcreateUserDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectGivePeople;
			url = ctx + "/UserSelect.action?flag=createUser";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selcreateUserVal(this);
				}
			}, true, option);
		}
	var selcreateUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dataTaskSvItemdiv").data("dataTaskSvItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('createUser-id',rec.get('id'));
				obj.set('createUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
	//查询知识库
//	function showmutantGenesType1(){
//		var options = {};
//		options.width = document.body.clientWidth-800;
//		options.height = document.body.clientHeight-40;
//		loadDialogPage(null, "突变基因表", "/kb/knowledgeBaseSelect.action", {
//			"确定" : function() {
//				var operGrid = $("#show_dialog_knowledgeBase_div").data("knowledgeBaseDialogGrid");
//				var selectRecord = operGrid.getSelectionModel().getSelections();
//				
//				var records = dataTaskSvItemGrid.getSelectRecord();
//				if (selectRecord.length > 0) {
//					$.each(selectRecord, function(i, obj) {
//						$.each(records, function(a, b) {
//							b.set("mutantGenes", obj.get("mutationGenes"));
//							var name = obj.get("mutationGenes");
//							ajax("post", "/analysis/data/dataTask/setSvnItem.action", {
//								code : name
//								}, function(data) {
//									//alert(data.data);
//									if (data.success) {
//										
//										$.each(data.data, function(i, obj) {
//											
//											b.set("transcript",obj.transcript);
//											b.set("exon1",obj.totleNumberExon);
//											b.set("exon2",obj.totleNumberExon);
//											
//										});
//										
//									} else {
//										message("获取明细数据时发生错误！");
//									}
//								}, null); 
//							//ajax结束
//						});
//					});
//				}else{
//					message("请选择您要选择的数据");
//					return;
//				}
//				$(this).dialog("close");
//			}
//		}, true, options);
//	}
	//查询突变基因对应的信息
	function showmutantGenesType1() {
		var win = Ext.getCmp('showjybcList');
		if (win) {
			win.close();
		}
		var showjybcList = new Ext.Window(
				{
					id : 'showjybcList',
					modal : true,
					title : biolims.analysis.mutantGenes,
					layout : 'fit',
					width : 780,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/dic/type/dicTypeSelect.action?flag=cyzlb' frameborder='0' width='780' height='500' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							showjybcList.close();
						}
					} ]
				});
		showjybcList.show();
	}
	
	function setcyzlb(id,name) {
		
		var selectRecord = dataTaskSvItemGrid.getSelectionModel().getSelections();
		for(var i=0;i<selectRecord.length;i++){
			ajax("post", "/analysis/data/dataTask/selDicType.action", {
				name : name
			}, function(data) {
				if(data.success){
					selectRecord[i].set("mutantGenes",name);
					if(data.d!=null){
						selectRecord[i].set("otherName",data.d.note);
					}
					if(data.d2!=null){
						selectRecord[i].set("mutantGeneScript",data.d2.note);
					}
				}
			}, null);
		}
		var win = Ext.getCmp('showjybcList');
		if (win) {
			win.close();
		}
	}	
function sVcontrast(){
	 var sVresult = dataTaskSvItemGrid.store;
	 var n=sVresult.getCount()/2;
	 for(var i=0;i<n;i++){
		 for(var j=n;j<sVresult.getCount();j++){
			 if(sVresult.getAt(i).get("sampleCode")==sVresult.getAt(j).get("sampleCode")&& sVresult.getAt(i).get("mutantGenes") == sVresult.getAt(j).get("mutantGenes")){
				 var m = 0;
				 //伙伴基因
				 if(sVresult.getAt(i).get("partnerGenes") != sVresult.getAt(j).get("partnerGenes")){
					 m = 1;
				 }
				 //伙伴基因其他名称
				 if(sVresult.getAt(i).get("partnerGenesName") != sVresult.getAt(j).get("partnerGenesName")){
					 m = 1;
				 }
				 //融合产物 
				 if(sVresult.getAt(i).get("fusionProduct") != sVresult.getAt(j).get("fusionProduct")){
					 m = 1;
				 }
				 //突变基因位置
				 if(sVresult.getAt(i).get("MutantGeneLc") != sVresult.getAt(j).get("MutantGeneLc")){
					 m = 1;
				 }
				 //伙伴基因位置
				 if(sVresult.getAt(i).get("partnerLc") != sVresult.getAt(j).get("partnerLc")){
					 m = 1;
				 }
				 //突变基因染色体断裂位点
				 if(sVresult.getAt(i).get("mutantGenePoint") != sVresult.getAt(j).get("mutantGenePoint")){
					 m = 1;
				 }
				 //伙伴基因染色体断裂位点
				 if(sVresult.getAt(i).get("partnerPoint") != sVresult.getAt(j).get("partnerPoint")){
					 m = 1;
				 }
				 //突变分类
				 if(sVresult.getAt(i).get("mutationClass") != sVresult.getAt(j).get("mutationClass")){
					 m = 1;
				 }
				 //突变丰度
				 if(sVresult.getAt(i).get("mutatinAbundance") != sVresult.getAt(j).get("mutatinAbundance")){
					 m = 1;
				 }
				 
				 if(m ==1){
					 sVresult.getAt(i).set("accordance","0");
					 sVresult.getAt(j).set("accordance","0");
				 }else{
					 sVresult.getAt(i).set("accordance","1");
					 sVresult.getAt(j).set("accordance","1");
					 sVresult.getAt(j).set("validState","1");
					 sVresult.getAt(i).set("validState","");
				 }
			 }
		 }
	 }
}

//var taskgridSv = gridEditTable("dataTaskSvItemdiv", cols, loadParam, opts,operParams);
setTimeout(function(){
   //从工作流中待办事项中进入	
        if($("#bpmTaskId").val()&&$("#bpmTaskId").val()!='null'){
		
                    //判断是否筛选完，如果没有，就需要按分析人显示
		if($("#dataTask_stateName").val()!='已上传'){
			sxSv();	
		}
	}
},100);
function sxSv() {
	dataTaskSvItemGrid.store.reload();
var filter1 = function(record, id){
	var flag = true;
	   if (record.get("acceptUser-id")==window.userId){
	      	 flag = true;
	      }
	   else{
		     flag =  false; 
	   }
	   return flag;
	};
	var onStoreLoad1 = function(store, records, options){
	   store.filterBy(filter1);
	};
	dataTaskSvItemGrid.store.on("load", onStoreLoad1);
}
