var dataTaskSvnItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'mutantGenes',
		type:"string"
	});
	   fields.push({
		name:'otherName',
		type:"string"
	});
	   fields.push({
		name:'transcript',
		type:"string"
	});
	   fields.push({
		name:'mutantGeneLc',
		type:"string"
	});
	   fields.push({
		name:'aminoAcidMutation',
		type:"string"
	});
	   fields.push({
		name:'baseMutation',
		type:"string"
	});
	   fields.push({
		name:'mutationStartPosition',
		type:"string"
	});
	   fields.push({
		name:'mutationStopPosition',
		type:"string"
	});
	   /*fields.push({
		name:'mutationSource',
		type:"string"
	});*/
	   fields.push({
		name:'mutationClass',
		type:"string"
	});
	   fields.push({
		name:'mutationType',
		type:"string"
	});
	   fields.push({
		name:'mutationStatus',
		type:"string"
	});
	   fields.push({
		name:'mutatinAbundance',
		type:"string"
	});
	   fields.push({
		name:'mutationFiction',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	   fields.push({
		name:'createDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	}); 
    fields.push({
		name:'validState',
		type:"string"
	});
    fields.push({
		name:'accordance',
		type:"string"
	});
    fields.push({
		name:'referenceBase',
		type:"string"
	});
    fields.push({
		name:'mutantBase',
		type:"string"
	});
    fields.push({
		name:'relatedMutation',
		type:"string"
	});
    fields.push({
		name:'relatedChemotherapy',
		type:"string"
	});
    //新加字段
    fields.push({
		name:'familyCode',//家族编号
		type:"string"
	});
    fields.push({
		name:'familyCode',//家族编号
		type:"string"
	});
    fields.push({
		name:'variationSource',//变异来源
		type:"string"
	});
    fields.push({
		name:'testMethod',//检测方法
		type:"string"
	});
    fields.push({
		name:'acmgGgene',//ACMG重要基因
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//实验员
	var sampleUserFun = new Ext.form.TextField({
		allowBlank : true
	});
	sampleUserFun.on('focus',function(){
		sampleUserFunSelFun();
	});
	cm.push({
		dataIndex : 'acceptUser-id',
		header : biolims.common.testUserId,
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'acceptUser-name',
		header : biolims.common.testUserName,
		width : 100
		//editor : sampleUserFun
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var accordanceStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.analysis.inconsistency ], [ '1', biolims.analysis.consistency],]
	});
	
	var accordanceComboxFun = new Ext.form.ComboBox({
		store : accordanceStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'accordance',
		hidden : false,
		header:biolims.analysis.accordance,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(accordanceComboxFun),
		editor: accordanceComboxFun,
		sortable:true
	});
	var validStateStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.master.invalid ], [ '1', biolims.master.valid ],]
	});
	
	var validStateComboxFun = new Ext.form.ComboBox({
		store : validStateStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'validState',
		hidden : false,
		header:biolims.analysis.validState,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(validStateComboxFun),
		editor: validStateComboxFun,
		sortable:true
	});
	//得到焦点时发生的事件
	mutantGenesType2 =new Ext.form.TextField({
        allowBlank: false
	});
	mutantGenesType2.on('focus', function() {
		showmutantGenesType2();
	});
	cm.push({
		dataIndex:'mutantGenes',
		header:biolims.analysis.mutantGenes,
		width:15*10,
		sortable:true,
		editor : mutantGenesType2
	});
//	cm.push({
//		dataIndex:'mutantGenes',
//		hidden : false,
//		header:'突变基因',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'otherName',
		hidden : false,
		header:biolims.analysis.otherName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transcript',
		hidden : false,
		header:biolims.analysis.transcript,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutantGeneLc',
		hidden : false,
		header:biolims.analysis.MutantGeneLc,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'aminoAcidMutation',
		hidden : false,
		header:biolims.analysis.aminoAcidMutation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'baseMutation',
		hidden : false,
		header:biolims.analysis.baseMutation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'referenceBase',
		hidden : false,
		header:biolims.analysis.referenceBase,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutantBase',
		hidden : false,
		header:biolims.analysis.mutantBase,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutationStartPosition',
		hidden : false,
		header:biolims.analysis.mutationStartPosition,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutationStopPosition',
		hidden : false,
		header:biolims.analysis.mutationStopPosition,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'mutationSource',
		hidden : false,
		header:'突变来源',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
//	var mutationClasssStore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '种系突变' ], [ '2', '多态性突变' ],[ '3', '肿瘤突变' ],]
//	});
//	
//	var mutationClassComboxFun = new Ext.form.ComboBox({
//		store : mutationClasssStore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		emptyText : '',
//		selectOnFocus : true
//	});
	/*cm.push({
		dataIndex:'mutationClass',
		hidden : false,
		header:'突变分类',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationClassComboxFun),
		editor: mutationClassComboxFun,
		sortable:true
	});*/
	var treatstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=snvtbfl',
			method : 'POST'
		})
	});
	treatstore.load();
	var treatcob = new Ext.form.ComboBox({
		store : treatstore,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'mutationClass',
		header : biolims.analysis.mutationClass,
		width : 10 * 10,
		renderer : Ext.util.Format.comboRenderer(treatcob),
		editor : treatcob
	});
//	cm.push({
//		dataIndex:'mutationClass',
//		hidden : false,
//		header:'突变分类',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	var mutationTypeStore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [
//		        [ '1', '基因融合' ], [ '2', '基因拷贝数变异' ],[ '3', '同义突变' ],[ '4', '错义突变' ],
//		        [ '5', '插入非移码突变' ],[ '6', '插入移码突变' ],[ '7', '缺失非移码突变' ],[ '8', '缺失移码突变' ],
//		        [ '9', '无义突变' ],[ '10', '剪切突变' ],[ '11', '其他突变' ],
//		       ]
//	});
//	
//	var mutationTypeComboxFun = new Ext.form.ComboBox({
//		store : mutationTypeStore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		emptyText : '',
//		selectOnFocus : true
//	});
	/*cm.push({
		dataIndex:'mutationType',
		hidden : false,
		header:'突变类型',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationTypeComboxFun),
		editor: mutationTypeComboxFun,
		sortable:true
	});*/
	var treatstorelx = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=snvtblx',
			method : 'POST'
		})
	});
	treatstorelx.load();
	var treatcoblx = new Ext.form.ComboBox({
		store : treatstorelx,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'mutationType',
		header : biolims.analysis.mutationType,
		width : 10 * 18,
		renderer : Ext.util.Format.comboRenderer(treatcoblx),
		editor : treatcoblx
	});
//	cm.push({
//		dataIndex:'mutationType',
//		hidden : false,
//		header:'突变类型',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var mutationStatusStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.analysis.homozygousPolymorphism ], [ '2', biolims.analysis.heterozygousPolymorphism ],]
	});
	
	var mutationStatusComboxFun = new Ext.form.ComboBox({
		store : mutationStatusStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	/*cm.push({
		dataIndex:'mutationStatus',
		hidden : false,
		header:'突变状态',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationStatusComboxFun),
		editor: mutationStatusComboxFun,
		sortable:true
	});*/
	var treatstorezt = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=snvtbzt',
			method : 'POST'
		})
	});
	treatstorezt.load();
	var treatcobzt = new Ext.form.ComboBox({
		store : treatstorezt,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'mutationStatus',
		header : biolims.analysis.mutationStatus,
		width : 10 * 10,
		renderer : Ext.util.Format.comboRenderer(treatcobzt),
		editor : treatcobzt
	});
//	cm.push({
//		dataIndex:'mutationStatus',
//		hidden : false,
//		header:'突变状态',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'mutatinAbundance',
		hidden : false,
		header:biolims.analysis.mutatinAbundance,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var mutationFictionStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'level1' ], [ '2', 'level2' ],[ '3', 'level3' ],[ '4', 'level4' ],]
	});
	
	var mutationFictionComboxFun = new Ext.form.ComboBox({
		store : mutationFictionStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	/*cm.push({
		dataIndex:'mutationFiction',
		hidden : false,
		header:'突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});*/
	/*cm.push({
		dataIndex:'relatedMutation',
		hidden : false,
		header:'靶向相关突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});*/
	var treatstorebfj = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=snvbtbfj',
			method : 'POST'
		})
	});
	treatstorebfj.load();
	var treatcobbfj = new Ext.form.ComboBox({
		store : treatstorebfj,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'relatedMutation',
		header : biolims.analysis.relatedMutation,
		width : 10 * 13,
		renderer : Ext.util.Format.comboRenderer(treatcobbfj),
		editor : treatcobbfj
	});
	
	/*cm.push({
		dataIndex:'relatedChemotherapy',
		hidden : false,
		header:'化疗相关突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});*/
	var treatstorehfj = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'name'
		}, {
			name : 'note'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx + '/dic/type/dicTypeCobJson.action?type=snvstbfj',
			method : 'POST'
		})
	});
	treatstorehfj.load();
	var treatcobhfj = new Ext.form.ComboBox({
		store : treatstorehfj,
		displayField : 'note',
		valueField : 'name',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'relatedChemotherapy',
		header : biolims.analysis.relatedChemotherapy,
		width : 10 * 15,
		renderer : Ext.util.Format.comboRenderer(treatcobhfj),
		editor : treatcobhfj
	});
//	cm.push({
//		dataIndex:'mutationFiction',
//		hidden : false,
//		header:'突变分级',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	//新增字段
	cm.push({
		dataIndex:'familyCode',//家族编号
		hidden : false,
		header:biolims.common.familyCode,
		width:20*10
	});
	
	cm.push({
		dataIndex:'variationSource',//变异来源
		hidden : false,
		header:biolims.common.sourceVariation,
		width:20*10
	});
	
	cm.push({
		dataIndex:'testMethod',//检测方法
		hidden : false,
		header:biolims.common.sequencingFun,
		width:20*10
	});
	var acmgGene = new Ext.data.ArrayStore({
		fields : [ 'value', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ],]
	});
	
	var acmgGeneFun = new Ext.form.ComboBox({
		store : acmgGene,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'acmgGene',//ACMG重要基因
		hidden : false,
		header:biolims.common.ACMGimportantGenes,
		width:20*10,
		editor:acmgGeneFun
	});
	
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/data/dataTask/showDataTaskSvnItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.analysis.baseMutationsTab;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/data/dataTask/delDataTaskSvnItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dataTaskSvnItemGrid.getStore().commitChanges();
				dataTaskSvnItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	/*opts.tbar.push({
			text : '选择相关主表',
				handler : selectdataTaskDialogFun
		});
	opts.tbar.push({
			text : '选择下达人',
				handler : selectcreateUserFun
		});*/
	opts.tbar.push({
		text : biolims.common.uploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_svnuploadcsv_div"),biolims.common.batchUpload,null,{
				"Confirm":function(){
					goInExcelcsvSvn();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	var stateName = $("#dataTask_stateName").val();
	if(stateName == biolims.common.uploaded){
		opts.tbar.push({
			text : biolims.analysis.experimentalResultsComparison,
			handler : function() {
				contrast();
			}
		});
	}
	function goInExcelcsvSvn(){
		var file = document.getElementById("file-svnuploadcsv").files[0];  
		var n = 0;
		var ob = dataTaskSvnItemGrid.getStore().recordType;
		var sv = dataTaskSvItemGrid.getStore().recordType;
		var cnv = dateTaskCnvItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
//                			alert(this[1]=="SNV");
                			var type="";
                			//判断
                			if(this[1]=="SNV"){
                				type="SNV";
                			}
                			if(this[1]=="SV"){
                				type="SV";
                			}
                			if(this[1]=="CNV"){
                				type="CNV";
                			}
                			if(this[1]=="SNP"){
                				if(this[2]=="GSTM1"||this[2]=="GSTT1"||(this[2]=="CYP2D6"&&this[3]=="")){
                					type="CNV";
                				}else{
                					type="SNV";
                				}
                			}
                			if(this[1]=="Mutant"||this[1]=="Germline"){
                				type="SNV";
                			}
                			//SNV
                			if(type=="SNV"){
                				var p = new ob({});
                    			p.isNew = true;	
                    			p.set("sampleCode",this[0]);
                    			p.set("mutantGenes",this[2]);
                    			ajax("post", "/analysis/data/dataTask/selDicType.action", {
    	            				name : this[2]
    	            			}, function(data) {
    	            				if(data.success){
    	            					if(data.d==null){
        	            				
            	                			p.set("otherName","");
        	            				}else{
//    	            						p.set("mutantGenes",data.d.name);
            	                			p.set("otherName",data.d.note);
        	            				}
    	            					if(data.d2==null){
    	            						p.set("transcript","");
    	            					}else{
    	            						p.set("transcript",data.d2.note);
    	            					}
    	            				}
    	            				
    	            			}, null);
                    			var mutantGeneLc=this[3].split(":");
                    			p.set("mutantGeneLc",mutantGeneLc[2]);
                    			var aminoAcidMutation=this[4].split("(");
                    			p.set("aminoAcidMutation",aminoAcidMutation[0]);
                    			if(aminoAcidMutation[1]==null){
                    				p.set("baseMutation","");
                    			}else{
                    				p.set("baseMutation",aminoAcidMutation[1].substring(0,aminoAcidMutation[1].length-1));
                    			}
                    			p.set("referenceBase",this[7]);
                    			p.set("mutantBase",this[8]);
                    			p.set("mutationStartPosition",this[5]);
                    			p.set("mutationStopPosition",this[6]);
                    			p.set("mutationClass",this[1]);
                    			p.set("mutationType",this[10]);
                    			p.set("mutationStatus",this[9]);
                    			p.set("mutatinAbundance",this[12]);
                    			p.set("acceptUser-id",window.userId);
                    			p.set("acceptUser-name",window.userName);
                    			dataTaskSvnItemGrid.getStore().add(p);
                			}
                			//SV
                			if(type=="SV"){
                    			var p2 = new sv({});
                    			p2.isNew = true;	
                    			p2.set("sampleCode",this[0]);
                    			p2.set("mutantGenes",this[2]);
                    			ajax("post", "/analysis/data/dataTask/selDicType.action", {
    	            				name : this[2]
    	            			}, function(data) {
    	            				if(data.success){
    	            					if(data.d==null){
            	                			p2.set("otherName","");
        	            				}else{
            	                			p2.set("otherName",data.d.note);
        	            				}
    	            					if(data.d2==null){
    	            						p2.set("mutantGeneScript","");
    	            					}else{
    	            						p2.set("mutantGeneScript",data.d2.note);
    	            					}
    	            				}
    	            			}, null);
                    			p2.set("partnerGenes",this[3]+"/"+this[4]);
                    			p2.set("fusionProduct",this[3]+"/"+this[4]);
                    			p2.set("MutantGeneLc",this[3]+"/"+this[4]);
                    			p2.set("partnerLc",this[3]+"/"+this[4]);
                    			p2.set("mutantGenePoint",this[5]);
                    			p2.set("partnerPoint",this[6]);
                    			p2.set("mutationClass",this[1]);
                    			p2.set("mutatinAbundance",this[12]);
                    			p2.set("acceptUser-id",window.userId);
                    			p2.set("acceptUser-name",window.userName);
                    			dataTaskSvItemGrid.getStore().add(p2);
                			}
                			//CNV
                			if(type=="CNV"){
                				var p3 = new cnv({});
                    			p3.isNew = true;	
                    			p3.set("sampleCode",this[0]);
                    			p3.set("mutantGenes",this[2]);
                    			ajax("post", "/analysis/data/dataTask/selDicType.action", {
    	            				name : this[2]
    	            			}, function(data) {
    	            				if(data.success){
    	            					if(data.d==null){
            	                			p3.set("otherName","");
        	            				}else{
            	                			p3.set("otherName",data.d.note);
        	            				}
    	            					if(data.d3==null){
    	            						p3.set("mutationPosition","");
    	            					}else{
    	            						p3.set("mutationPosition",data.d3.note);
    	            					}
    	            				}
    	            				
    	            			}, null);
                    			if(this[2]=="GSTM1"||this[2]=="GSTT1"||(this[2]=="CYP2D6"&&this[3]=="")){
                    				p3.set("copyNumberVaration",biolims.analysis.dualCopyNumberMissing);
                    			}else{
                    				if(this[13]>1){
                    					p3.set("copyNumberVaration",biolims.analysis.geneAmplification);
                    				}else if(this[13]<1){
                    					p3.set("copyNumberVaration",biolims.analysis.singleCopyNumberMissing);
                    				}else{
                    					p3.set("copyNumberVaration","");
                    				}
                    			}
                    			p3.set("copyVarationRatio",this[13]);
                    			p3.set("mutationClass",this[1]);
                    			p3.set("acceptUser-id",window.userId);
                    			p3.set("acceptUser-name",window.userName);
                    			dateTaskCnvItemGrid.getStore().add(p3);
                			}
//                			p.set("relatedMutation",this[15]);
//                			p.set("relatedChemotherapy",this[16]);
//                			p.set("dataTask-id",$("#dataTask_id").val());
//                			p.set("state","0");
//                			p.set("stateName","新建");
//                			p.set("acceptUser-id",window.userId);
//                			p.set("acceptUser-name",window.userName);
//                			//p.set("validState","0");
//							dataTaskSvnItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});

	
	dataTaskSvnItemGrid=gridEditTable("dataTaskSvnItemdiv",cols,loadParam,opts);
	$("#dataTaskSvnItemdiv").data("dataTaskSvnItemGrid", dataTaskSvnItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectdataTaskFun(){
	var win = Ext.getCmp('selectdataTask');
	if (win) {win.close();}
	var selectdataTask= new Ext.Window({
	id:'selectdataTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectdataTask.close(); }  }]  }) });  
    selectdataTask.show(); }
	function setdataTask(rec){
		var gridGrid = $("#dataTaskSvnItemdiv").data("dataTaskSvnItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('dataTask-id',rec.get('id'));
			obj.set('dataTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectdataTask')
		if(win){
			win.close();
		}
	}
	function selectdataTaskDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/DataTaskSelect.action?flag=dataTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						seldataTaskVal(this);
				}
			}, true, option);
		}
	var seldataTaskVal = function(win) {
		var operGrid = dataTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dataTaskSvnItemdiv").data("dataTaskSvnItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('dataTask-id',rec.get('id'));
				obj.set('dataTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
function selectcreateUserFun(){
	var win = Ext.getCmp('selectcreateUser');
	if (win) {win.close();}
	var selectcreateUser= new Ext.Window({
	id:'selectcreateUser',modal:true,title:biolims.common.selectGivePeople,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=createUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcreateUser.close(); }  }]  }) ;  
    selectcreateUser.show(); }
	function setcreateUser(rec){
		var gridGrid = $("#dataTaskSvnItemdiv").data("dataTaskSvnItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('createUser-id',rec.get('id'));
			obj.set('createUser-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectcreateUser')
		if(win){
			win.close();
		}
	}
	function selectcreateUserDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectGivePeople;
			url = ctx + "/UserSelect.action?flag=createUser";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selcreateUserVal(this);
				}
			}, true, option);
		}
	var selcreateUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dataTaskSvnItemdiv").data("dataTaskSvnItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('createUser-id',rec.get('id'));
				obj.set('createUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
	
	
	//查询知识库
//		function showmutantGenesType2(){
//			var options = {};
//			options.width = document.body.clientWidth-800;
//			options.height = document.body.clientHeight-40;
//			loadDialogPage(null, "突变基因表", "/kb/knowledgeBaseSelect.action", {
//				"确定" : function() {
//					var operGrid = $("#show_dialog_knowledgeBase_div").data("knowledgeBaseDialogGrid");
//					var selectRecord = operGrid.getSelectionModel().getSelections();
//					var records = dataTaskSvnItemGrid.getSelectRecord();
//					if (selectRecord.length > 0) {
//						$.each(selectRecord, function(i, obj) {
//							$.each(records, function(a, b) {
//								
//								b.set("mutantGenes", obj.get("mutationGenes"));
//								//ajax连载数据
//								//ajaxSetSvnItem(obj.get("mutationGenes"));
//								//ajax更替数据,begin
//								var name = obj.get("mutationGenes");
//								ajax("post", "/analysis/data/dataTask/setSvnItem.action", {
//									code : name
//									}, function(data) {
//										//alert(data.data);
//										if (data.success) {
//											
//											$.each(data.data, function(i, obj) {
//												
//												b.set("transcript",obj.transcript);
//												b.set("exon",obj.totleNumberExon);
//												
//											});
//											
//										} else {
//											message("获取明细数据时发生错误！");
//										}
//									}, null); 
//								//ajax结束
//							});
//						});
//					}else{
//						message("请选择您要选择的数据");
//						return;
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
		//查询突变基因对应的信息
		function showmutantGenesType2() {
			var win = Ext.getCmp('showjybcList');
			if (win) {
				win.close();
			}
			var showjybcList = new Ext.Window(
					{
						id : 'showjybcList',
						modal : true,
						title : biolims.analysis.mutantGenes,
						layout : 'fit',
						width : 780,
						height : 500,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/dic/type/dicTypeSelect.action?flag=jybc' frameborder='0' width='780' height='500' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								showjybcList.close();
							}
						} ]
					});
			showjybcList.show();
		}
		function setjybc(id,name) {
			var selectRecord = dataTaskSvnItemGrid.getSelectionModel().getSelections();
			for(var i=0;i<selectRecord.length;i++){
				ajax("post", "/analysis/data/dataTask/selDicType.action", {
					name : name
				}, function(data) {
					if(data.success){
						selectRecord[i].set("mutantGenes",name);
						if(data.d!=null){
							selectRecord[i].set("otherName",data.d.note);
						}
						if(data.d2!=null){
							selectRecord[i].set("transcript",data.d2.note);
						}
					}
				}, null);
			}
			var win = Ext.getCmp('showjybcList');
			if (win) {
				win.close();
			}
		}
		/**
		 * ajax方法
		 * @param name
		 */
function contrast(){
	 var result = dataTaskSvnItemGrid.store;
	 var n=result.getCount()/2;
	 for(var i=0;i<n;i++){
		 for(var j=n;j<result.getCount();j++){
			 if(result.getAt(i).get("sampleCode")==result.getAt(j).get("sampleCode")&& result.getAt(i).get("mutantGenes") == result.getAt(j).get("mutantGenes")){
//				 alert(result.getAt(i).get("sampleCode")+'---'+result.getAt(j).get("sampleCode"));
				 var m = 0;
				 //突变基因位置
				 if(result.getAt(i).get("mutantGeneLc") != result.getAt(j).get("mutantGeneLc")){
				 	 m = 1;
				 }
				 //氨基酸突变
				 if(result.getAt(i).get("aminoAcidMutation") != result.getAt(j).get("aminoAcidMutation")){
					 m = 1;
				 }
				 //碱基突变
				 if(result.getAt(i).get("baseMutation") != result.getAt(j).get("baseMutation")){
					 m = 1;
				 }
				 //参考碱基
				 if(result.getAt(i).get("referenceBase") != result.getAt(j).get("referenceBase")){
					 m = 1;
				 }
				//突变碱基
				 if(result.getAt(i).get("mutantBase") != result.getAt(j).get("mutantBase")){
					 m = 1;
				 }
				 //染色体起始位置
				 if(result.getAt(i).get("mutationStartPosition") != result.getAt(j).get("mutationStartPosition")){
					 m = 1;
				 }
				 //染色体终止位置
				 if(result.getAt(i).get("mutationStopPosition") != result.getAt(j).get("mutationStopPosition")){
					 m = 1;
				 }
				 //突变分类
				 if(result.getAt(i).get("mutationClass") != result.getAt(j).get("mutationClass")){
					 m = 1;
				 }
				 //突变类型
				 if(result.getAt(i).get("mutationType") != result.getAt(j).get("mutationType")){
					 m = 1;
				 }
			 	 //纯杂合状态
				 if(result.getAt(i).get("mutationStatus") != result.getAt(j).get("mutationStatus")){
					 m = 1;
				 }
				 //突变丰度
				 if(result.getAt(i).get("mutatinAbundance") != result.getAt(j).get("mutatinAbundance")){
					 m = 1;
				 }

				 if(m ==1){
					 result.getAt(i).set("accordance","0");
					 result.getAt(j).set("accordance","0");
				 }else{
					 result.getAt(i).set("accordance","1");
					 result.getAt(j).set("accordance","1");
					 result.getAt(j).set("validState","1");
					 result.getAt(i).set("validState","");
				 }
			 }
			
		 }
	 }
}		


		
//function sampleUserFunSelFun(){
//	var win = Ext.getCmp('sampleUserFunSelFun');
//	if (win) {
//		win.close();
//	}
//	var records = dataTaskSvnItemGrid.getSelectRecord();	
//	var sampleUserFunSelFun = new Ext.Window(
//			{
//				id : 'sampleUserFunSelFun',
//				modal : true,
//				title : '选择实验员',
//				layout : 'fit',
//				width : document.body.clientWidth / 1.5,
//				height : document.body.clientHeight / 1.1,
//				closeAction : 'close',
//				plain : true,
//				bodyStyle : 'padding:5px;',
//				buttonAlign : 'center',
//				collapsible : true,
//				maximizable : true,
//				items : new Ext.BoxComponent(
//						{
//							id : 'maincontent',
//							region : 'center',
//							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
//									+ window.ctx
//									+ "/core/user/userSelect.action?flag=CrmCustomerFun33' frameborder='0' width='100%' height='100%' ></iframe>"
//						}),
//				buttons : [ {
//					text: biolims.common.close,
//					handler : function() {
//						sampleUserFunSelFun.close();
//					}
//				} ]
//			});
//	sampleUserFunSelFun.show();
//}
//function setCrmCustomerFun33(id,name){
//	alert("d");
//	records = dataTaskSvnItemGrid.getSelectRecord();
//	$.each(records, function(i, obj) {
//		obj.set("acceptUser-id", id);
//		obj.set("acceptUser-name",name);
//	});
//	var win = Ext.getCmp('sampleUserFunSelFun');
//	if(win){win.close();}
//}
//dataTaskSvnItemGrid = gridEditTable("dataTaskSvnItemdiv", cols, loadParam, opts,operParams);
setTimeout(function(){
   //从工作流中待办事项中进入	
        if($("#bpmTaskId").val()&&$("#bpmTaskId").val()!='null'){
		
                    //判断是否筛选完，如果没有，就需要按分析人显示
		if($("#dataTask_stateName").val()!=biolims.common.uploaded){
			sxSvn();	
		}
	}
},10);
function sxSvn() {
	dataTaskSvnItemGrid.store.reload();
var filter2 = function(record, id){
	var flag = true;
	   if (record.get("acceptUser-id")==window.userId){
	      	 flag = true;
	      }
	   else{
		     flag =  false; 
	   }
	   return flag;
	};
	var onStoreLoad2 = function(store, records, options){
	   store.filterBy(filter2);
	};
	dataTaskSvnItemGrid.store.on("load", onStoreLoad2);
}

