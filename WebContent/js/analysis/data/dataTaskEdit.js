$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state = $("#dataTask_state").val();
	var fcId =  $("#dataTask_fcnumber").val();
	if(state=="3"){
		load("/analysis/data/dataTask/showDataTaskTempList.action?fcId="+fcId, null, "#dataTaskTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#dataTaskTempPage").remove();
		$("#showFc").css("display","none");
	}
});	
$(function() {
	$("#tabss").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/analysis/data/dataTask/editDataTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/analysis/data/dataTask/showDataTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("DataTask", {
					userId : userId,
					userName : userName,
					formId : $("#dataTask_id").val(),
					title : $("#dataTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#dataTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});
/**
 * 改变状态，审核
 */
$("#toolbarbutton_status").click(function(){
	var flag=false;
	var grid1 = dataTaskItemGrid.store;
	//判断是否有不合格的数据
	for(var i=0; i<grid1.getCount(); i++){
		var user = grid1.getAt(i).get("acceptUser-name");
		var user2 = grid1.getAt(i).get("acceptUser2-name");
		var state = grid1.getAt(i).get("states");
		var method = grid1.getAt(i).get("method");
		//判断是否通过
		if(user==""||user2==""){
			message(biolims.analysis.unqualifiedDetail);
			flag=false;
			break;
		}else{
			flag=true;
		}
	}
	if(flag){
		commonChangeState("formId=" + $("#dataTask_id").val() + "&tableId=DataTask");
	}
	
});





function save() {

if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText:biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	
	    var dataTaskSvnItemDivData = $("#dataTaskSvnItemdiv").data("dataTaskSvnItemGrid");
		document.getElementById('dataTaskSvnItemJson').value = commonGetModifyRecords(dataTaskSvnItemDivData);
	    var dataTaskSvItemDivData = $("#dataTaskSvItemdiv").data("dataTaskSvItemGrid");
		document.getElementById('dataTaskSvItemJson').value = commonGetModifyRecords(dataTaskSvItemDivData);
	    var dateTaskCnvItemDivData = $("#dateTaskCnvItemdiv").data("dateTaskCnvItemGrid");
		document.getElementById('dateTaskCnvItemJson').value = commonGetModifyRecords(dateTaskCnvItemDivData);
		var dataTaskItemdivData = $("#dataTaskItemdiv").data("dataTaskItemGrid");
		document.getElementById('dateTaskItemJson').value = commonGetModifyRecords(dataTaskItemdivData);
		
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/analysis/data/dataTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/analysis/data/dataTask/copyDataTask.action?id=' + $("#dataTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#dataTask_id").val() + "&tableId=dataTask");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#dataTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.analysis.mutationScreeningResultTable,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/analysis/data/dataTask/showDataTaskSvnItemList.action", {
				id : $("#dataTask_id").val()
			}, "#dataTaskSvnItempage");
load("/analysis/data/dataTask/showDataTaskSvItemList.action", {
				id : $("#dataTask_id").val()
			}, "#dataTaskSvItempage");
load("/analysis/data/dataTask/showDateTaskCnvItemList.action", {
				id : $("#dataTask_id").val()
			}, "#dateTaskCnvItempage");

//数据分析明细
load("/analysis/data/dataTask/showDataTaskItemList.action", {
			id : $("#dataTask_id").val()
			}, "#dataTaskItempage");
//病人信息
//alert($("#dataTask_fcnumber").val());
//load("/analysis/data/dataTask/showSampleinfoList.action", {
//				id : $("#dataTask_id").val()
//			}, "#personelpage");

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
function showfcFun(){
	var win = Ext.getCmp('showfcFun');
	if (win) {win.close();}
	var showfcFun= new Ext.Window({
	id:'showfcFun',modal:true,title:biolims.sequencing.selectFCCode,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/analysis/desequencing/deSequencingTask/showDeSequencingTaskByState.action?flag=ShowfcFun3' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 showfcFun.close(); }  }]  });
	showfcFun.show(); 
}


function setShowfcFun3(rec){
	$("#dataTask_fcnumber").val(rec.get("flowCode")) ;
	ajax("post", "/analysis/data/dataTask/selectOrAddByFC.action", {
		fcId : rec.get("flowCode")
	}, function(data) {
		console.log(data);
		for(var i=0;i<data.data.length;i++){
			var ob = dataTaskTempGrid.getStore().recordType;
			var p = new ob({});
			var obj = data.data[i];
			console.log(obj);
			p.isNew = true;
			p.set("id",obj.id);
			p.set("sampleCode",obj.sampleCode);
			p.set("sampleId",obj.sampleId);
			p.set("crmPatientId",obj.crmPatientId);
			p.set("sampleTypeId",obj.sampleTypeId);
			p.set("sampleTypeName",obj.sampleTypeName);
			p.set("cancerType",obj.cancerType);
			p.set("cancerTypeSeedOne",obj.cancerTypeSeedOne);
			p.set("cancerTypeSeedTwo",obj.cancerTypeSeedTwo);
			p.set("productId",obj.productId);
			p.set("productName",obj.productName);
		

			dataTaskTempGrid.getStore().add(p);
		}
	}, null);
	
	var win = Ext.getCmp('showfcFun');
	if(win){win.close();
	}
}



////选择FC号
//	function showfcFun(){
//		var options = {};
//		options.width = document.body.clientWidth-800;
//		options.height = document.body.clientHeight-40;
//		loadDialogPage(null, "选择FC号", "/analysis/desequencing/deSequencingTask/showDeSequencingTaskByState.action", {
//			"确定" : function() {
//				var operGrid = $("#show_deSequencingTaskByState_div").data("deSequencingTaskByStateGrid");
//				var selectRecord = operGrid.getSelectionModel().getSelections();
//				//var records = dataTaskSvnItemGrid.getSelectRecord();
//				if (selectRecord.length > 0) {
//					$.each(selectRecord, function(i, obj) {
//						alert(obj.get("flowCode"));
//						//设置数据
//						 $("#dataTask_fcnumber").val(obj.get("flowCode")) ;
//						
//					});
//				}else{
//					message("请选择您要选择的数据");
//					return;
//				}
//				$(this).dialog("close");
//			}
//		}, true, options);
//	}




