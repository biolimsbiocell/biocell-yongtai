var mutationItemGrid;

$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleId',
		type:"string"
	});
	   fields.push({
		name:'crmPatientId',
		type:"string"
	});
	   fields.push({
			name:'name',
			type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'xjPot',
		type:"string"
	});
	    fields.push({
		name:'tbFrequence',
		type:"string"
	});
	   fields.push({
		name:'genePot',
		type:"string"
	});
	   fields.push({
		name:'hgmdPubmedid',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'form',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	    fields.push({
			name:'MicroRNA',
			type:"string"
		});
	    fields.push({
			name:'type',
			type:"string"
		});
	    
	    fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-name',
			type:"string"
		});
	   
	    fields.push({
			name:'acceptUser-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
		});
	    fields.push({
			name:'acceptUser2-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser2-name',
			type:"string"
		});
	    fields.push({
			name:'reason',
			type:"string"
		});
	    fields.push({
			name:'method',
			type:"string"
		});
	    fields.push({
			name:'states',
			type:"string"
		});
	cols.fields=fields;
	

	var cm=[];

	
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:15*10
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'名称',
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var sampleUserFun = new Ext.form.TextField({
		allowBlank : true
	});
	sampleUserFun.on('focus',function(){
		sampleUserFunSelFun();
	});
	cm.push({
		dataIndex : 'acceptUser-id',
		header : '实验员1',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'acceptUser-name',
		header : '实验员1',
		width : 100,
		//editor : sampleUserFun
	});
	
	//实验员2
	var sampleUserFun2 = new Ext.form.TextField({
		allowBlank : true
	});
	sampleUserFun2.on('focus',function(){
		sampleUserFunSelFun2();
	});
	cm.push({
		dataIndex : 'acceptUser2-id',
		header : '实验员2',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'acceptUser2-name',
		header : '实验员2',
		width : 100,
		//editor : sampleUserFun2
	});
	
	cm.push({
		hidden:false,
		dataIndex:'sampleId',
		header:'样本编号',
		width:15*10
	});
	cm.push({
		hidden:false,
		dataIndex:'crmPatientId',
		header:'电子病历编号',
		width:15*10
	});
	
	cm.push({
		hidden:true,
		dataIndex:'name',
		header:'名称',
		width:15*10
	});
	//审核状态
	var stateStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '审核不通过' ], [ '2', '审核通过' ],]
	});
	
	var statesComboxFun = new Ext.form.ComboBox({
		store : stateStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'states',
		hidden : false,
		header:'审核状态',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(statesComboxFun),
		editor: statesComboxFun,
		sortable:true
	});
	

//	var method = new Ext.form.ComboBox({
//		transform : "grid-method",
//		width : 100,
//		triggerAction : 'all',
//		lazyRender : true
//	});
	
	cm.push({
		dataIndex : 'method',
		header : '处理方式',
		width : 80,
		hidden:false,
		//editor : method,
		renderer : function(value, metadata, record, rowIndex, colIndex, store) {
			if (value == "0") {
				return "<span style='color:red'>不通过</span>";
			} else if (value == "1") {
				return "通过";
			} else {
				return "请选择";
			}
		}
	});
	
	
	cm.push({
		dataIndex : 'reason',
		header : '失败原因',
		width : 140,
		hidden:false,
	
	});
	
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:'突变位置',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xjPot',
		hidden : true,
		header:'碱基改变',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tbFrequence',
		hidden : true,
		header:'突变频率',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'genePot',
		hidden : true,
		header:'基因改变',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hgmdPubmedid',
		hidden : true,
		header:'参考文献ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'注释',
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'form',
		hidden : true,
		header:'类型',
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:'二代任务ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : true,
		header:'二代任务',
		width:15*10

	});
	cm.push({
		dataIndex:'MicroRNA',
		hidden : true,
		header:'MicroRNA',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'type',
		hidden : true,
		header:'纯合/杂合',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/data/mutation/showmutationAuditItemListJson.action";
	loadParam.limit = 1000;
	var opts={};
	opts.title="突变审核";
	opts.height =  document.body.clientHeight-115;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/data/mutation/delDataTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				mutationItemGrid.getStore().commitChanges();
				mutationItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	
		opts.tbar.push({
			text : '显示可编辑列',
			handler : null
		});
		opts.tbar.push({
			text : '取消选中',
			handler : null
		});
		opts.tbar.push({
			text : '查询明细',
			handler : function edit(){
				//alert("执行查询操作");
				var id="";
				var operGrid = mutationItemGrid;
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var num = 0;
				$.each(selectRecord, function(i, obj) {
					num++;
					id=obj.get("dataTask-id");
					//alert(id);
				});
				//alert(num);
				if (id==""||id==undefined){
					message("请选择一条记录!");
					return false;
				}
				if (num>1){
					message("只能选择一条记录!");
					return false;
				}
				
				window.location=window.ctx+'/analysis/data/mutation/showItemListById.action?id=' + id;
			}
		});
		
		//编辑
		
		opts.tbar.push({
			text : '保存结果',
			handler : function() {
				
					
					var itemJson = commonGetModifyRecords(mutationItemGrid);
					var snvItemJson = commonGetModifyRecords(mutationSvnItemGrid);
					var svItemJson = commonGetModifyRecords(mutaionSvItemGrid);
					var cnvItemJson = commonGetModifyRecords(mutationCnvItemGrid);
					
					//if(selectRecord.getSelections().length>0){
						//$.each(selectRecord.getSelections(), function(i, obj) {
					if(itemJson.length>0||snvItemJson.length>0||svItemJson.length>0||cnvItemJson.length>0){
							ajax("post", "/analysis/data/mutation/saveMutationItem.action", {
								mutationItemJson : itemJson,
								mutationSvnItemJson :snvItemJson,
								mutationSvItemJson :svItemJson,
								mutationCnvItemJson :cnvItemJson
							}, function(data) {
								if (data.success) {
									
									mutationItemGrid.getStore().commitChanges();
									mutationItemGrid.getStore().reload();
									message("保存成功！");
								} else {
									message("保存失败！");
								}
							}, null);			
						//});
					}else{
						message("没有需要保存的数据！");
					}
				
			
			}
		});
		opts.tbar.push({
			text : "批量结果",
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bar_rna"), "批量结果", null, {
					"确定" : function() {
						var records = mutationItemGrid.getSelectRecord();
						if (!records.length) {
							records = mutationItemGrid.getAllRecord();
						}
						if (records && records.length > 0) {
							var method = $("#grid-method").val();
							mutationItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("method", method);
							});
							mutationItemGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});

	   
	   mutationItemGrid=gridEditTable("mutationItemdiv",cols,loadParam,opts);
	$("#mutationItemdiv").data("mutationItemGrid", mutationItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	
	$(function() {
		$("#tabs").tabs({
			select : function(event, ui) {
			}
		});
	});	

	load("/analysis/data/mutation/mutationSvnItemList.action", {
		id : $("#sgtid").val()
	}, "#mutationSvnItempage");
	
	load("/analysis/data/mutation/showmutationSvItemList.action", {
		id : $("#sgtid").val()
	}, "#mutationSvItempage");
	load("/analysis/data/mutation/showMutationCnvItemList.action", {
		id : $("#sgtid").val()
	}, "#mutationCnvItempage");
});










