var dataTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleId',
		type:"string"
	});
	   fields.push({
		name:'crmPatientId',
		type:"string"
	});
	   fields.push({
			name:'name',
			type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'xjPot',
		type:"string"
	});
	    fields.push({
		name:'tbFrequence',
		type:"string"
	});
	   fields.push({
		name:'genePot',
		type:"string"
	});
	   fields.push({
		name:'hgmdPubmedid',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'form',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	    fields.push({
			name:'MicroRNA',
			type:"string"
		});
	    fields.push({
			name:'type',
			type:"string"
		});
	    
	    fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-name',
			type:"string"
		});
	   
	    fields.push({
			name:'acceptUser-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
		});
	    fields.push({
			name:'acceptUser2-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser2-name',
			type:"string"
		});
	    fields.push({
			name:'reason',
			type:"string"
		});
	    fields.push({
			name:'method',
			type:"string"
		});
	    fields.push({
			name:'states',
			type:"string"
		});
	    //新增字段
	    fields.push({
			name:'poolingId',
			type:"string"
		});
	    /*fields.push({
			name:'wkTaskId',
			type:"string"
		});*/
	    fields.push({
			name:'sampleTypeId',
			type:"string"
		});
	    fields.push({
			name:'sampleTypeName',
			type:"string"
		});   
	    /*fields.push({
			name:'sampleStage',
			type:"string"
		});*/
	   
	    fields.push({
			name:'cancerType',
			type:"string"
		});
	  
	    fields.push({
			name:'cancerTypeSeedOne',
			type:"string"
		});
	    
	    fields.push({
			name:'cancerTypeSeedTwo',
			type:"string"
		});
	    fields.push({
			name:'tempId',
			type:"string"
		});
	    fields.push({
			name:'productId',
			type:"string"
		});
	    fields.push({
			name:'productName',
			type:"string"
		});
	    fields.push({
			name:'sampleCode',
			type:"string"
		});
	    
	    
	cols.fields=fields;
	
	var storeStateCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ biolims.analysis.mitochondria, biolims.analysis.mitochondria ], [ 'MicroRNA', 'MicroRNA' ] ]
	});
	var cm=[];
	
	/**纯合/杂合*/
	var stateCob = new Ext.form.ComboBox({
		store : storeStateCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:15*10
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.designation,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var sampleUserFun = new Ext.form.TextField({
		allowBlank : true
	});
	sampleUserFun.on('focus',function(){
		sampleUserFunSelFun();
	});
	cm.push({
		dataIndex : 'acceptUser-id',
		header : biolims.common.testUser1Id,
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'acceptUser-name',
		header : biolims.common.testUser1Name,
		width : 100,
		editor : sampleUserFun
	});
	
	//实验员2
	var sampleUserFun2 = new Ext.form.TextField({
		allowBlank : true
	});
	sampleUserFun2.on('focus',function(){
		sampleUserFunSelFun2();
	});
	cm.push({
		dataIndex : 'acceptUser2-id',
		header : biolims.common.testUser2Id,
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'acceptUser2-name',
		header : biolims.common.testUser2Name,
		width : 100,
		editor : sampleUserFun2
	});
	cm.push({
		hidden:false,
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:15*10
	});
	cm.push({
		hidden:false,
		dataIndex:'sampleId',
		header:biolims.common.sampleId,
		width:15*10
	});
	cm.push({
		hidden:true,
		dataIndex:'poolingId',
		header:biolims.pooling.code,
		width:15*10
	});
	cm.push({
		hidden:true,
		dataIndex:'productId',
		header:biolims.common.productId,
		width:15*10
	});
	cm.push({
		hidden:false,
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*10
	});
	/*cm.push({
		hidden:false,
		dataIndex:'wkTaskId',
		header:'文库编号',
		width:15*10
	});*/
	cm.push({
		hidden:false,
		dataIndex:'crmPatientId',
		header:biolims.sample.medicalNumber,
		width:15*10
	});
	
	cm.push({
		hidden:true,
		dataIndex:'name',
		header:biolims.common.designation,
		width:15*10
	});
	cm.push({
		hidden:true,
		dataIndex:'tempId',
		header:biolims.common.tempId,
		width:15*10
	});
	//审核状态
	var stateStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.analysis.auditNotThrough ], [ '2', biolims.analysis.approved ],]
	});
	
	var statesComboxFun = new Ext.form.ComboBox({
		store : stateStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'states',
		hidden : true,
		header:biolims.analysis.states,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(statesComboxFun),
		editor: statesComboxFun,
		sortable:true
	});
	

	var method = new Ext.form.ComboBox({
		transform : "grid-method",
		width : 100,
		triggerAction : 'all',
		lazyRender : true
	});
	
	cm.push({
		dataIndex : 'method',
		header : biolims.common.method,
		width : 80,
		hidden:false,
		editor : method,
		renderer : function(value, metadata, record, rowIndex, colIndex, store) {
			if (value == "0") {
				return "<span style='color:red'>"+biolims.common.noPass+"</span>";
			} else if (value == "1") {
				return biolims.common.pass;
			} else {
				return biolims.common.pleaseChoose;
			}
		}
	});
	
	
	cm.push({
		dataIndex : 'reason',
		header : biolims.common.reason,
		width : 140,
		hidden:false,
		editor : new Ext.form.TextField({
			allowBlank : true,
			validator : function(val) {
				if (val.length > 100) {
					message(biolims.master.isNotGreater100);
					return false;
				} else {
					return true;
				}

			}
		})
	});
	
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:biolims.analysis.location,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xjPot',
		hidden : true,
		header:biolims.analysis.xjPot,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tbFrequence',
		hidden : true,
		header:biolims.analysis.tbFrequence,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'genePot',
		hidden : true,
		header:biolims.analysis.genePot,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hgmdPubmedid',
		hidden : true,
		header:biolims.analysis.hgmdPubmedid,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.sample.note,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'form',
		hidden : true,
		header:biolims.common.type,
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:biolims.analysis.dataTaskId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : true,
		header:biolims.analysis.dataTaskName,
		width:15*10

	});
	cm.push({
		dataIndex:'MicroRNA',
		hidden : true,
		header:'MicroRNA',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'type',
		hidden : true,
		header:biolims.analysis.type,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleTypeId',
		hidden : true,
		header:biolims.common.sampleTypeId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleTypeName',
		hidden : false,
		header:biolims.common.sampleTypeName,
		width:15*10

	});
	var sampleStagestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'I' ], [ '2', 'II' ],[ '3', 'III' ],[ '4', 'IV' ]]
	});
	
	var sampleStageComboxFun = new Ext.form.ComboBox({
		store : sampleStagestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	/*cm.push({
		dataIndex:'sampleStage',
		header:'分期',
		width:10*6,
		
		renderer: Ext.util.Format.comboRenderer(sampleStageComboxFun),				
		sortable:true
	});*/
	
	cm.push({
		dataIndex:'cancerType',
		hidden : false,
		header:biolims.sample.dicTypeName,
		width:15*10

	});
	
	cm.push({
		dataIndex:'cancerTypeSeedOne',
		hidden : false,
		header:biolims.sample.cancerTypeSeedOne,
		width:15*10

	});
	cm.push({
		dataIndex:'cancerTypeSeedTwo',
		hidden : false,
		header:biolims.sample.cancerTypeSeedTwo,
		width:15*10

	});
	
	
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/data/dataTask/showDataTaskItemListJson.action?id="+ $("#dataTask_id").val()+"&fcId="+$("#dataTask_fcnumber").val();
	loadParam.limit = 1000;
	var opts={};
	opts.title=biolims.analysis.technicalAnalysisTask;
	opts.height =  document.body.clientHeight-65;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/data/dataTask/delDataTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dataTaskItemGrid.getStore().commitChanges();
				dataTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	 
	   if($("#dataTask_id").val() != undefined){
		  
		   opts.tbar.push({
				text : biolims.common.chooseTester1,
				handler : function() {
					sampleUserFunSelFun();
				}
			});
		   opts.tbar.push({
				text : biolims.common.chooseTester2,
				handler : function() {
					sampleUserFunSelFun2();
				}
			});
		
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.batchResult,
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bar_rna"), biolims.common.batchResult, null, {
					"Confirm" : function() {
						var records = dataTaskItemGrid.getSelectRecord();
						if (!records.length) {
							records = dataTaskItemGrid.getAllRecord();
						}
						if (records && records.length > 0) {
							var method = $("#grid-method").val();
							dataTaskItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("method", method);
							});
							dataTaskItemGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
//		opts.tbar.push({
//			text : "批量上传(csv文件)",
//			handler : function() {
//				var options = {};
//				options.width = 350;
//				options.height = 200;
//				loadDialogPage($("#bat_upload_divRna6"), "批量上传", null, {
//					"确定" : function() {
//						goInExcelRna();
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}
//		});
//		opts.tbar.push({
//			text : "模板下载",
//			handler : function() {
//			window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\xsl\\dataTaskItem.xlsx','','');
//			}
//		});
	   }
	   dataTaskItemGrid=gridEditTable("dataTaskItemdiv",cols,loadParam,opts);
	$("#dataTaskItemdiv").data("dataTaskItemGrid", dataTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	function getPath(obj) {
		  if (obj) {
		  if (window.navigator.userAgent.indexOf("MSIE") >= 1) {
		  obj.select(); return document.selection.createRange().text;
		  }
		  else if (window.navigator.userAgent.indexOf("Firefox") >= 1) {
		  if (obj.files) {
		  return obj.files.item(0).getAsDataURL();
		  }
		  return obj.value;
		  }
		  return obj.value;
		  }
		  } 
	
	var idTmr = "";
	function goInExcelRna(){
		var file = document.getElementById("file-uploadRna6").files[0];  
		var reader = new FileReader();  
		//将文件以文本形式读入页面  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
		var n = 0;	
    	var ob = dataTaskItemGrid.getStore().recordType;
    	var csv_data = $.simple_csv(this.result);
        $(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;	
                			p.set("sampleId",this[0]);
							p.set("location",this[1]);
							p.set("xjPot",this[2]);
							p.set("tbFrequence",this[3]);
							p.set("genePot",this[4]);
							p.set("MicroRNA",this[5]);
							p.set("type",this[6]);
							dataTaskItemGrid.getStore().insert(0, p);
                		}
                	}
                    n = n +1;
                });
    		}  
	     }
		function Cleanup() { 
		window.clearInterval(idTmr); 
		CollectGarbage(); 
		}
		
		/*setTimeout(function(){
			if($("#bpmTaskId").val()&&$("#bpmTaskId").val()!='null'){
				
				if($("#sampleGen2Task_stateName").val()!='已实验'){
					sx();	
				}
				if(window.userName!=$("#sampleCancerTask_createUser_name").val()){
						
				}
			}
		},100);*/
	
});
/*function sx() {
	dataTaskItemGrid.store.reload();
	var filterseq = function(record, id){
		var flag = true;
		   if (record.get("acceptUser-id")==window.userId){
		      	 flag = true;
		      }
		   else{
		     flag =  false; 
		   }
		   return flag;
		};
		var onStoreLoadSeq = function(store, records, options){
		   store.filterBy(filterseq);
		};
		dataTaskItemGrid.store.on("load", onStoreLoadSeq);
}*/
function sampleUserFunSelFun(){
	var win = Ext.getCmp('sampleUserFunSelFun');
	if (win) {
		win.close();
	}
	var records = dataTaskItemGrid.getSelectRecord();	
	var sampleUserFunSelFun = new Ext.Window(
			{
				id : 'sampleUserFunSelFun',
				modal : true,
				title : biolims.common.chooseTester,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/core/user/userSelect.action?flag=CrmCustomerFun3' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleUserFunSelFun.close();
					}
				} ]
			});
	sampleUserFunSelFun.show();
}

function setCrmCustomerFun3(id,name){
	records = dataTaskItemGrid.getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("acceptUser-id", id);
		obj.set("acceptUser-name",name);
	});
	var win = Ext.getCmp('sampleUserFunSelFun');
	if(win){win.close();}
}
/**
 * 实验员2
 */
function sampleUserFunSelFun2(){
	var win = Ext.getCmp('sampleUserFunSelFun');
	if (win) {
		win.close();
	}
	var records = dataTaskItemGrid.getSelectRecord();	
	var sampleUserFunSelFun = new Ext.Window(
			{
				id : 'sampleUserFunSelFun',
				modal : true,
				title : biolims.common.chooseTester,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/core/user/userSelect.action?flag=CrmCustomerFun32' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						sampleUserFunSelFun.close();
					}
				} ]
			});
	sampleUserFunSelFun.show();
}
function setCrmCustomerFun32(id,name){
	records = dataTaskItemGrid.getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("acceptUser2-id", id);
		obj.set("acceptUser2-name",name);
	});
	var win = Ext.getCmp('sampleUserFunSelFun');
	if(win){win.close();}
}

