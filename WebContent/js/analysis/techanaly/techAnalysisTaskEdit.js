﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/analysis/techanaly/techAnalysisTask/editTechAnalysisTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/analysis/techanaly/techAnalysisTask/showTechAnalysisTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#techAnalysisTask", {
					userId : userId,
					userName : userName,
					formId : $("#techAnalysisTask_id").val(),
					title : $("#techAnalysisTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#techAnalysisTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var sampleTechAnalysisInfoDivData = $("#sampleTechAnalysisInfodiv").data("sampleTechAnalysisInfoGrid");
		document.getElementById('sampleTechAnalysisInfoJson').value = commonGetModifyRecords(sampleTechAnalysisInfoDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/analysis/techanaly/techAnalysisTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/analysis/techanaly/techAnalysisTask/copyTechAnalysisTask.action?id=' + $("#techAnalysisTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#techAnalysisTask_id").val() + "&tableId=techAnalysisTask");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#techAnalysisTask_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'科技服务信息分析',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/analysis/techanaly/techAnalysisTask/showSampleTechAnalysisInfoList.action", {
				id : $("#techAnalysisTask_id").val()
			}, "#sampleTechAnalysisInfopage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);