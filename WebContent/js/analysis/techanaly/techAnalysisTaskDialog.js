var techAnalysisTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'headUser',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
	    fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'techTaskId',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser',
		header:'创建人',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'headUser',
		header:'负责人',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:'项目id',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'contractId',
		header:'合同号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'techTaskId',
		header:'任务单id',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态id',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/techanaly/techAnalysisTask/showTechAnalysisTaskListJson.action";
	var opts={};
	opts.title="科技服务信息分析";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setTechAnalysisTaskFun(rec);
	};
	techAnalysisTaskDialogGrid=gridTable("show_dialog_techAnalysisTask_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(techAnalysisTaskDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
