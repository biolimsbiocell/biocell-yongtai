﻿
var sampleTechAnalysisInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'compareRates',
		type:"string"
	});
	   fields.push({
		name:'multiMapRates',
		type:"string"
	});
	   fields.push({
		name:'geneDifferNum',
		type:"string"
	});
	   fields.push({
		name:'coverage',
		type:"string"
	});
	   fields.push({
		name:'snpNum',
		type:"string"
	});
	   fields.push({
		name:'miRnaPrefer',
		type:"string"
	});
	   fields.push({
		name:'contigNum',
		type:"string"
	});
	   fields.push({
		name:'disHomo',
		type:"string"
	});
	   fields.push({
		name:'sampleCluster',
		type:"string"
	});
	   fields.push({
		name:'convertRates',
		type:"string"
	});
	   fields.push({
		name:'fragmentSize',
		type:"string"
	});
	   fields.push({
		name:'peaksNum',
		type:"string"
	});
	   fields.push({
		name:'motifResult',
		type:"string"
	});
	   fields.push({
		name:'geneRegionNum',
		type:"string"
	});
	   fields.push({
		name:'uniqueMappedRatio',
		type:"string"
	});
	   fields.push({
		name:'vaildPairdEndReads',
		type:"string"
	});
	   fields.push({
		name:'danglingEndPairdReads',
		type:"string"
	});
	   fields.push({
		name:'selfCirclePairdReads',
		type:"string"
	});
	   fields.push({
		name:'eriNum',
		type:"string"
	});
	   fields.push({
		name:'eriDis',
		type:"string"
	});
	   fields.push({
		name:'transCisRate',
		type:"string"
	});
	   fields.push({
		name:'catchSpecific',
		type:"string"
	});
	   fields.push({
		name:'snpNumLevel',
		type:"string"
	});
	   fields.push({
		name:'contigN50',
		type:"string"
	});
	   fields.push({
		name:'hostCompareRates',
		type:"string"
	});
	   fields.push({
		name:'kimerDepth',
		type:"string"
	});
	   fields.push({
		name:'singalIp',
		type:"string"
	});
	   fields.push({
		name:'gcDepth',
		type:"string"
	});
	   fields.push({
		name:'geneNum',
		type:"string"
	});
	   fields.push({
		name:'sample+spilice',
		type:"string"
	});
	   fields.push({
		name:'aug16sV3',
		type:"string"
	});
	   fields.push({
		name:'aug16sV4',
		type:"string"
	});
	   fields.push({
		name:'standardValue',
		type:"string"
	});
	   fields.push({
		name:'realValue',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	    fields.push({
		name:'techAnalysisTask-id',
		type:"string"
	});
	    fields.push({
		name:'techAnalysisTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'分析明细id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'文库号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'compareRates',
		hidden : false,
		header:'比对率',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'multiMapRates',
		hidden : false,
		header:'multi map率',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'geneDifferNum',
		hidden : false,
		header:'差异基因数目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'coverage',
		hidden : false,
		header:'覆盖度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpNum',
		hidden : false,
		header:'snp数目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'miRnaPrefer',
		hidden : false,
		header:'mirna碱基偏好性',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contigNum',
		hidden : false,
		header:'contig数量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'disHomo',
		hidden : false,
		header:'均一性分布',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCluster',
		hidden : false,
		header:'样品聚类效果',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'convertRates',
		hidden : false,
		header:'转化率',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fragmentSize',
		hidden : false,
		header:'插入片段长度分值',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'peaksNum',
		hidden : false,
		header:'peaks数目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'motifResult',
		hidden : false,
		header:'motif预测结果不为空',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'geneRegionNum',
		hidden : false,
		header:'富集区域相关基因数目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'uniqueMappedRatio',
		hidden : false,
		header:'unique mapped ratio',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'vaildPairdEndReads',
		hidden : false,
		header:'valid paired-end reads',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'danglingEndPairdReads',
		hidden : false,
		header:'dangling end paired-end reads',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'selfCirclePairdReads',
		hidden : false,
		header:'self circle paired-end reads',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
			var eriNumcob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'eriNum',
		hidden : false,
		header:'理论酶切片段数量是否正确',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(eriNumcob),editor: eriNumcob
	});
			var eriDiscob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'eriDis',
		hidden : false,
		header:'理论酶切片段分布描述是否正确',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(eriDiscob),editor: eriDiscob
	});
			var transCisRatecob = new Ext.form.ComboBox({
			store : new Ext.data.JsonStore({
				fields : [ 'id', 'name' ],
				data : [ {
					id : '1',
					name : '是'
				}, {
					id : '0',
					name : '否'
				} ]
			}),
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			selectOnFocus : true
		});
	cm.push({
		dataIndex:'transCisRate',
		hidden : false,
		header:'同一物种不同样本顺反比例是否一致',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(transCisRatecob),editor: transCisRatecob
	});
	cm.push({
		dataIndex:'catchSpecific',
		hidden : false,
		header:'捕获特异性',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpNumLevel',
		hidden : false,
		header:'snp数量级=~基因组大小*0.001',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contigN50',
		hidden : false,
		header:'contig n50',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hostCompareRates',
		hidden : false,
		header:'宿主比对率',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'kimerDepth',
		hidden : false,
		header:'kmer depth',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'singalIp',
		hidden : false,
		header:'signalip类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'gcDepth',
		hidden : false,
		header:'gc-depth图聚集分布深度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'geneNum',
		hidden : false,
		header:'基因数目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sample+spilice',
		hidden : false,
		header:'各样品拼接比例',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'aug16sV3',
		hidden : false,
		header:'扩增16s v3-v4区域的箱线图范围',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'aug16sV4',
		hidden : false,
		header:'扩增16s v3区域的箱线图范围',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'standardValue',
		hidden : false,
		header:'标准值',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'realValue',
		hidden : false,
		header:'实际值',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理方式',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techAnalysisTask-id',
		hidden : true,
		header:'相关主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techAnalysisTask-name',
		hidden : false,
		header:'相关主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/techanaly/techAnalysisTask/showSampleTechAnalysisInfoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="科技服务信息分析结果";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/techanaly/techAnalysisTask/delSampleTechAnalysisInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择相关主表',
			handler : selecttechAnalysisTaskFun
		});
	
	
	
	
	
	
	
	
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = sampleTechAnalysisInfoGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 12-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 13-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 14-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 15-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 16-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 17-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 18-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 19-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 20-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 21-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 22-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 23-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 24-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 25-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 26-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 27-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 28-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 29-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 30-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 31-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 32-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 33-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 34-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 35-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 36-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 37-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 38-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 39-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 40-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							sampleTechAnalysisInfoGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : '读取Csv',
		handler : getCsv
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	function getCsv(){
		//alert($("#deSequencingTask_id").val());
		ajax("post", "/analysis/techanaly/techAnalysisTask/getCsv.action", {
			taskId : $("#techAnalysisTask_id").val()
		},function(data){
			message("读取成功！");
		},null);
	}
	sampleTechAnalysisInfoGrid=gridEditTable("sampleTechAnalysisInfodiv",cols,loadParam,opts);
	$("#sampleTechAnalysisInfodiv").data("sampleTechAnalysisInfoGrid", sampleTechAnalysisInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selecttechAnalysisTaskFun(){
	var win = Ext.getCmp('selecttechAnalysisTask');
	if (win) {win.close();}
	var selecttechAnalysisTask= new Ext.Window({
	id:'selecttechAnalysisTask',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/StringSelect.action?flag=techAnalysisTask' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selecttechAnalysisTask.close(); }  }]  });     selecttechAnalysisTask.show(); }
	function settechAnalysisTask(id,name){
		var gridGrid = $("#sampleTechAnalysisInfodiv").data("sampleTechAnalysisInfoGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('techAnalysisTask-id',id);
			obj.set('techAnalysisTask-name',name);
		});
		var win = Ext.getCmp('selecttechAnalysisTask')
		if(win){
			win.close();
		}
	}
	
