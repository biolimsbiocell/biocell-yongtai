﻿/* 
 * 文件名称 :deSequencingTaskEdit.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/01/30
 * 文件描述: 下机质控的相关页面函数
 * 
 */
var oldChangeLog;
$(function() {
		if($("#deSequencingTask_id").text()=="NEW"){
			$("#deSequencingTaskModal").modal("show");
			choseClinicalOrScientific();
		}else{
		}
		var handlemethod = $("#handlemethod").val();
		if(handlemethod == "modify") {
		}
	renderDatatables();
	//批量结果的按钮变为下拉框
	btnChangeDropdown($('#main'), $(".resultsBatchBtn"), ["合格", "不合格"], "result");
	//// 上传附件
	fileInput('1', 'deSequencingTask', $("#deSequencingTask_id").text());
});
//选择读取或上传
function choseClinicalOrScientific() {
	$(".modal-header .btn-lg").click(function() {
		var index = $(this).index();
		$(this).siblings(".btn-lg").removeClass("chosedType");
		$(this).addClass("chosedType");
	});
	//上传、新建的不同流向
	$("#modal-body .col-xs-12").click(function() {
		var index = $(this).index();
		//读取
		if(index == 0) {
			$("#deSequencingTaskModal").modal("hide");
			readCsv();
		}
		//上传
		if(index == 1) {
			$("#deSequencingTaskModal").modal("hide");
//			$("#uploadCsv").modal("show");
			uploadCsv();
		}
	});
}
//渲染的datatables

function renderDatatables() {
	var colOpts = [];
	colOpts.push({
		"data": 'id',
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": 'name',
		"title": biolims.common.name,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
		}
	})
	colOpts.push({
		"data": 'orderCode',
		"title": biolims.common.orderCode, //订单编号  科研隐藏
		"createdCell": function(td) {
			$(td).attr("saveName", "orderCode");
		}
	})
	colOpts.push({
		"data": "fastq",
		"title": "Fastq",
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "fastq");
		}
	})
	colOpts.push({
		"data": "md5",
		"title": "MD5",
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "md5");
		}
	})
	colOpts.push({
		"data": "readCount",
		"title": "Read Count",
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "readCount");
		}
	})
	colOpts.push({
		"data": "gcContent",
		"title": biolims.analysis.gcContent,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "gcContent");
		}
	})
	colOpts.push({
		"data": "nContent",
		"title": biolims.analysis.nContent,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "nContent");
		}
	})
	colOpts.push({
		"data": "qTwentyPCT",
		"title": biolims.analysis.qTwentyPCT,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "qTwentyPCT");
		}
	})
	colOpts.push({
		"data": "qThirtyPCT",
		"title": biolims.analysis.qThirtyPCT,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "qThirtyPCT");
		}
	})
	colOpts.push({
		"data": "ratioOfLane",
		"title": biolims.analysis.ratioOfLane,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "ratioOfLane");
		}
	})
	colOpts.push({
		"data": "raw_reads",
		"title": biolims.analysis.raw_reads,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "raw_reads");
		}
	})
	colOpts.push({
		"data": "cleanReads",
		"title": biolims.analysis.cleanReads,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "cleanReads");
		}
	})
	colOpts.push({
		"data": "ratioOfReads",
		"title": biolims.analysis.ratioOfReads,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "ratioOfReads");
		}
	})
	colOpts.push({
		"data": "raw_size",
		"title": biolims.analysis.raw_size,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "raw_size");
		}
	})
	colOpts.push({
		"data": "cleanBases",
		"title": biolims.analysis.cleanBases,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "cleanBases");
		}
	})
	colOpts.push({
		"data": "ratioOfBases",
		"title": biolims.analysis.ratioOfBases,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "ratioOfBases");
		}
	})
	colOpts.push({
		"data": "hq_reads",
		"title": biolims.master.hq_reads,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "hq_reads");
		}
	})
	colOpts.push({
		"data": "medianInsert",
		"title": biolims.analysis.medianInsert,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "medianInsert");
		}
	})
	colOpts.push({
		"data": "result",
		"title": biolims.common.result,
		"className": "select",
		"name": biolims.common.qualified+"|"+biolims.common.disqualified,
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", biolims.common.qualified+"|"+biolims.common.disqualified);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.qualified;
			}
			if(data == "0") {
				return biolims.common.disqualified;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "nextFlowId",
		"title": biolims.common.nextFlowId,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
		"data": "nextFlow",
		"title": biolims.common.nextFlow,
		"width": "170px",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "nextFlow");
			$(td).attr("nextFlowId", rowData['nextFlowId']);
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"width": "170px",
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
			//			$(td).css({
			//				'max-width': '170px',
			//				'overflow': 'hidden',
			//				'text-overflow': 'ellipsis'
			//			});
		}
	})

	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#main"));
		}
	});
	tbarOpts.push({
		text: biolims.common.addwindow ,
		action: function() {
			addItemLayer($("#main"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#main"))
		}
	});
	tbarOpts.push({
		text:biolims.common.copyRow,
		action: function() {
			copyRow($("#main"))
		}
	});
	tbarOpts.push({
		text: biolims.common.uploadCSV,
		action: function() {
//			$("#uploadCsv").modal("show");
			uploadCsv();
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#main"),
				"/analysis/desequencing/deSequencingTask/delDeSequencingItem.action");
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i> '+biolims.common.batchResult,
		className: 'btn btn-sm btn-success resultsBatchBtn',
	});
	tbarOpts.push({
		text: '<i class="fa fa-paypal"></i> '+biolims.dicSampleType.nextFlow,
		action: function() {
			nextFlow()
		}
	});
	var options = table(true, $("#deSequencingTask_id").text(), "/analysis/desequencing/deSequencingTask/showDeSequencingItemListJson.action", colOpts, tbarOpts);

	myTable = renderRememberData($("#main"), options);
	myTable.on('draw', function() {
		oldChangeLog = myTable.ajax.json();
	});
}

//上传
function uploadCsv() {
	$("#uploadCsv").modal("show");
	$(".fileinput-remove").click();
	var csvFileInput = fileInputCsv("");
		csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
		$.ajax({
			type: "post",
			data: {
				id: $("#deSequencingTask_id").text(),
				fileId: data.response.fileId
			},
			url: ctx + "/analysis/desequencing/deSequencingTask/uploadCsvFile.action",
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message=="success") {
//					myTable.ajax.reload();
					$("deSequencingTask_id").text(data.mainId);
					window.location = window.ctx + "/analysis/desequencing/deSequencingTask/editDeSequencingTask.action?id="+data.mainId;
				} else if(data.message=="noData"){
					top.layer.msg(biolims.common.uploadFailed+",请添加有内容的CSV文件");
				}else if(data.message=="addFcCode"){
					top.layer.msg(biolims.common.uploadFailed+",请添加以FC编号为CSV文件名称得正确的CSV文件");
				}else if(data.message=="noRequire"){
					top.layer.msg(biolims.common.uploadFailed+",CSV文件中没有找到正确的样本编号");
				}else if(data.success==false){
					top.layer.msg(biolims.common.uploadFailed);
				}

			}
		});
	});
	/*var csvFileInput = fileInputCsv("");
	csvFileInput.on("fileuploaded", function(event, data, previewId, index) {
		$.ajax({
			type: "post",
			data: {
				id: $("#deSequencingTask_id").text(),
				fileId: data.response.fileId
			},
			url: ctx + "/analysis/desequencing/deSequencingTask/uploadCsvFile.action",
			success: function(data) {
				var data = JSON.parse(data)
				if(data.success) {
					$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/analysis/desequencing/deSequencingTask/editDeSequencingTask.action?id="+data.id;
					myTable.ajax.reload();
				} else {
					top.layer.msg(biolims.common.uploadFailed)
				}

			}
		});
	});*/
}
//下一步流向
function nextFlow() {
	var rows = $("#main .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//判断先选产品
	var productIds = "";
	var sampleType="";
	top.layer.open({
		title:biolims.common.selectNextFlow ,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=deSequencingTask&", ''
		],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='nextFlow']").attr("nextFlowId", id).text(name);
			rows.find("td[savename='nextFlowId']").text(id);
			top.layer.close(index)
		},
	})
}
//保存
function save() {
	//日志功能 dwb 2018-05-11 19:11:45
	var changeLog = "下机质控:";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	var changeLogs = "";
	if(changeLog != "下机质控:"){
		changeLogs = changeLog;
	}
	var tabledata = saveItemjson($("#main"));
	var ele = $("#main");
	var changeLogItem = "下机质控明细：";
	var data = saveItemjson(ele);
	changeLogItem = getChangeLog(tabledata, ele, changeLogItem);
	var changeLogItems = "";
	if(changeLogItem != "下机质控明细："){
		changeLogItems = changeLogItem;
	}
	
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	var data = {
		main: JSON.stringify(jsonn),
		itemJson: tabledata
	};
	top.layer.load(4, {shade:0.3}); 
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	$.ajax({
		type: "post",
		url: ctx + "/analysis/desequencing/deSequencingTask/save.action?changeLog="+changeLogs+"&changeLogIteml="+changeLogItems+"&ids="+$("#deSequencingTask_id").text(),
		data: data,
		async:false,
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				$("#deSequencingTask_id").text(data.id);
				$("#deSequencingTask_id").val(data.id);
				myTable.settings()[0].ajax.data = {
					"id": data.id
				};
				myTable.ajax.reload();
			}
		}
	});
}
//日志 dwb 2018-05-11 19:22:20
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += biolims.common.code+'"' + v.sampleCode + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if(k=="result"){
				var result = $(tds[j]).text();
				if(result == biolims.common.qualified) {
					json[k] = "1";
				} else if(result == biolims.common.disqualified) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			if(k == "productName") {
				json["productId"] = $(tds[j]).attr("productId");
				continue;
			}
			if(k == "nextFlow") {
				console.log($(tds[j]).attr("nextFlowId"));
				json["nextFlowId"] = $(tds[j]).attr("nextFlowId");
				continue;
			}
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function add() {
	window.location = window.ctx + "/sample/deSequencingTask/editdeSequencingTask.action?type=" + $("#type2").val();
}

function list() {
	window.location = window.ctx + '/sample/deSequencingTask/showdeSequencingTaskTable.action?type=' + $("#type2").val();
}
//提交审批
function tjsp() {
	top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	}, function(index1) {
		top.layer.open({
			title: biolims.sample.pleaseSelectReviewer,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
			yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
				$("#confirmUserName").text(name);
				$("#confirmUserId").val(id);
				save();
				top.layer.close(index);
			},
			end:function(){
				splc();
			}
		})
		top.layer.close(index1);
	});
}
//审批流程图
function splc() {
	top.layer.open({
		title: biolims.common.approvalProcess ,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toStartView.action?formName=deSequencingTask",
		yes: function(index2, layer) {
			var datas = {
				userId: $("#confirmUserId").val(),
				userName: $("#confirmUserName").text(),
				formId: $("#sampleReveice_id").text(),
				title: $("#deSequencingTask_name").val(),
			}
			ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
				if(data.success) {
					top.layer.msg(biolims.common.submitSuccess);
					top.layer.close(index);
					if(typeof callback == 'function') {
						callback(data);
					}
				} else {
					top.layer.msg(biolims.common.submitFail);
				}
			}, null);
		}

	});
}

function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#sampleReveice_id").val();

	top.layer.open({
		title: biolims.common.approvalProcess ,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toCompleteTaskView.action?taskId=" + taskId + "&formId=" + formId,
		yes: function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();

			if(!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if(operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data: JSON.stringify(paramData),
					formId: formId,
					taskId: taskId,
					userId: window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						window.open(window.ctx+"/main/toPortal.action",'_parent');
						top.layer.closeAll();
						if(typeof callback == 'function') {}
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
			}
		}

	});
}
//上传附件
function fileUp() {
	if($("#deSequencingTask_id").text()=="NEW"){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
//查看附件
function fileView() {
	top.layer.open({
		title: "附件",
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=deSequencingTask&id=" + $("#deSequencingTask_id").text(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
//改变状态
function changeState() {
    var	id=$("#deSequencingTask_id").text()
	var paraStr = "formId=" + id +
		"&tableId=deSequencingTask";
	console.log(paraStr)
	top.layer.open({
		title: biolims.common.approvalProcess ,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(indexx) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		
	});
}
function readCsv(){
	$.ajax({
		type: "post",
		url: ctx + "/analysis/desequencing/deSequencingTask/readCsvFile.action",
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				var readData = data.data;
				// <br/>总文件数："+readData.sumNum+"&nbsp&nbsp完成文件数："+readData.nowNum
				var qtmess = "<br/>"+biolims.sequencing.sumnumbe+readData.sumNum
				+"&nbsp&nbsp"+biolims.sequencing.CompletedNumber+readData.nowNum;
				if(readData.flag == "success"){
					$("#maincontentframe", window.parent.document)[0].src 
					= window.ctx+"/analysis/desequencing/deSequencingTask/showDeSequencingTaskList.action";
				}else if(readData.flag == "noFile"){
					top.layer.msg(biolims.sequencing.readNoting);
				}else if(readData.flag == "dataMismatch"){
					//文件中数据与上机明细不匹配！
					top.layer.msg(readData.fileName+biolims.sequencing.fileNotMatch+qtmess);
				}else if(readData.flag == "noData"){
					//文件中没有数据
					top.layer.msg(readData.fileName+biolims.sequencing.fileNotDate+qtmess);
				}else if(readData.flag == "addFcCode"){
					//FC号在上机测序明细中没有数据
					top.layer.msg(readData.fileName+biolims.sequencing.FCNoData+qtmess);
				}else if(readData.flag == "moveError"){
					//在移动文件中发生错误 
					top.layer.msg(readData.fileName+biolims.sequencing.FileMoveError+qtmess);
				}
				myTable.ajax.reload();
			} else {
				top.layer.msg(biolims.common.uploadFailed);
			};

		}
	});
}

function add(){
	window.location=window.ctx+'/analysis/desequencing/deSequencingTask/editDeSequencingTask.action';
}
function list(){
	window.location=window.ctx+'/analysis/desequencing/deSequencingTask/showDeSequencingTaskList.action';
}
function downCsv(){
	window.location.href=ctx+"/js/analysis/desequencing/uploadFileRoot/deSequencingTaskItem.csv";
}