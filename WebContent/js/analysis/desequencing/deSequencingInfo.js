﻿
var deSequencingInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		   name:'sampleCode',
		   type:"string"
	   });
	   fields.push({
		   name:'outPut',
		   type:"string"
	   });
	   fields.push({
		   name:'clusters',
		   type:"string"
	   });
	   fields.push({
		   name:'pf',
		   type:"string"
	   });
//	   fields.push({
//		   name:'reasonType',
//		   type:"string"
//	   });
	   fields.push({
		   name:'indexLibrary',
		   type:"string"
	   });
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'assess',
		type:"string"
	});
	   fields.push({
		name:'backDate',
		type:"string"
	});
	   fields.push({
		name:'errorRate',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'advice',
		type:"string"
	});
	   fields.push({
		name:'lane',
		type:"string"
	});
	   fields.push({
		name:'project',
		type:"string"
	});
	   fields.push({
		name:'need',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'base',
		type:"string"
	});
	   fields.push({
		name:'reads',
		type:"string"
	});
	   fields.push({
		name:'pct',
		type:"string"
	});
	   fields.push({
		name:'adapter',
		type:"string"
	});
	   fields.push({
		name:'gc',
		type:"string"
	});
	   fields.push({
		   name:'sampleCount',
		   type:"string"
	   });
	   fields.push({
		name:'q20',
		type:"string"
	});
	   fields.push({
		name:'q30',
		type:"string"
	});
	   fields.push({
		name:'index',
		type:"string"
	});
	   fields.push({
		name:'ind',
		type:"string"
	});
	   fields.push({
		name:'dup',
		type:"string"
	});
	   fields.push({
		name:'ployAT',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'desequencingTask-id',
		type:"string"
	});
	    fields.push({
		name:'desequencingTask-name',
		type:"string"
	});
	    
	    fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	 /**===========================================================>
	 * 
	 * 添加下机质控结果明细2
	 */
	   fields.push({
			name:'lane2',
			type:"string"
		});
	   fields.push({
			name:'kit',
			type:"string"
		});
	   fields.push({
			name:'raw_reads',
			type:"string"
		});
	   fields.push({
			name:'raw_len',
			type:"string"
		});
	   fields.push({
			name:'raw_GC',
			type:"string"
		});
	   
	   fields.push({
			name:'raw_size',
			type:"string"
		});
	   fields.push({
			name:'hq_reads',
			type:"string"
		});
	   fields.push({
			name:'hq_len',
			type:"string"
		});
	   fields.push({
			name:'hq_GC',
			type:"string"
		});
	   fields.push({
			name:'hq_size',
			type:"string"
		});
	   
	   fields.push({
			name:'hq_reads_pct',
			type:"string"
		});
	   fields.push({
			name:'hq_data_pct',
			type:"string"
		});
	   fields.push({
			name:'demultiplexing_Reads',
			type:"string"
		});
	   fields.push({
			name:'demultiplexing_PCT',
			type:"string"
		});
	   fields.push({
			name:'qTwentyPCT',
			type:"string"
		});
	   fields.push({
			name:'qThirtyPCT',
			type:"string"
		});
	   fields.push({
			name:'lane',
			type:"string"
		});
	   fields.push({
			name:'length',
			type:"string"
		});
	   fields.push({
			name:'gcContent',
			type:"string"
		});
	   fields.push({
			name:'nContent',
			type:"string"
		});
	   fields.push({
			name:'pf',
			type:"string"
		});
	   fields.push({
			name:'cleanReads',
			type:"string"
		});
	   fields.push({
			name:'ratioOfReads',
			type:"string"
		});
	   fields.push({
			name:'cleanBases',
			type:"string"
		});
	   fields.push({
			name:'ratioOfBases',
			type:"string"
		});
	   fields.push({
			name:'dexReads',
			type:"string"
		});
	   fields.push({
			name:'ratioOfDex',
			type:"string"
		});
	   
	 
	   
	   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.analysis.code,
		width:20*6
	});
//	cm.push({
//		dataIndex:'sampleCode',
//		hidden : true,
//		header:'样本号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'lane',
//		hidden : false,
//		header:'lane',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'outPut',
//		hidden : true,
//		header:'产量',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'clusters',
//		hidden : false,
//		header:'Clusters',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'pf',
//		hidden : false,
//		header:'PF',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'sampleCount',
//		hidden : true,
//		header:'样品量',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'project',
//		hidden : false,
//		header:'project',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'indexLibrary',
//		hidden : false,
//		header:'文库号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'need',
//		hidden : false,
//		header:'need',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'base',
//		hidden : false,
//		header:'base',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'reads',
//		hidden : false,
//		header:'reads',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'pct',
//		hidden : false,
//		header:'pct',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'adapter',
//		hidden : false,
//		header:'adapter',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'gc',
//		hidden : false,
//		header:'gc',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'q20',
//		hidden : false,
//		header:'q20',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'q30',
//		hidden : false,
//		header:'q30',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'poolingCode',
//		hidden : false,
//		header:'pooling号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'index',
//		hidden : false,
//		header:'index',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'ind',
//		hidden : false,
//		header:'ind',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'dup',
//		hidden : false,
//		header:'dup',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'ployAT',
//		hidden : false,
//		header:'ployAT',
//		width:20*6
//	});
//	
//	cm.push({
//		dataIndex:'assess',
//		hidden : true,
//		header:'测试评估',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'backDate',
//		hidden : true,
//		header:'反馈时间',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'errorRate',
//		hidden : true,
//		header:'错误率',
//		width:20*6
//	});
//	var result = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '合格'
//			},{
//				id : '0',
//				name : '不合格'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'结果',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(result),editor: result
//	});
//	cm.push({
//		dataIndex:'advice',
//		hidden : true,
//		header:'处理意见',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'method',
//		hidden : true,
//		header:'处理方式',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'note',
//		hidden : false,
//		header:'备注',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'desequencingTask-id',
//		hidden : true,
//		header:'关联主表ID',
//		width:20*10
//	});
//	cm.push({
//		dataIndex:'desequencingTask-name',
//		hidden : true,
//		header:'关联主表',
//		width:20*10
//	});
//	
//	cm.push({
//		dataIndex:'contractId',
//		hidden : false,
//		header:'合同ID',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'projectId',
//		hidden : false,
//		header:'项目ID',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'orderType',
//		hidden : false,
//		header:'任务单类型',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'techTaskId',
//		hidden : true,
//		header:'科技服务任务单',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'state',
//		hidden : true,
//		header:'状态',
//		width:20*6
//	});
	/**===========================================================>
	 * 
	 * 添加下机质控结果明细2
	 */
//	cm.push({
//		dataIndex:'lane2',
//		hidden : true,
//		header:'泳道',
//		width:20*6
//	});
	cm.push({
		dataIndex:'kit',
		hidden : false,
		header:biolims.sequencing.kit,
		width:20*6
	});
	cm.push({
		dataIndex:'lane',
		hidden : false,
		header:biolims.pooling.lane2,
		width:20*6
	});
	cm.push({
		dataIndex:'pf',
		hidden : false,
		header:biolims.sequencing.PF,
		width:20*6
	});
	cm.push({
		dataIndex:'length',
		hidden : false,
		header:biolims.analysis.length,
		width:20*6
	});
	cm.push({
		dataIndex:'gcContent',
		hidden : false,
		header:biolims.analysis.gcContent,
		width:20*6
	});
	cm.push({
		dataIndex:'nContent',
		hidden : false,
		header:biolims.analysis.nContent,
		width:20*6
	});
	
	cm.push({
		dataIndex:'qTwentyPCT',
		hidden : false,
		header:biolims.analysis.qTwentyPCT,
		width:20*6
	});
	cm.push({
		dataIndex:'qThirtyPCT',
		hidden : false,
		header:biolims.analysis.qThirtyPCT,
		width:20*6
	});
	
	cm.push({
		dataIndex:'raw_reads',
		hidden : false,
		header:biolims.analysis.raw_reads,
		width:20*6
	});
	cm.push({
		dataIndex:'cleanReads',
		hidden : false,
		header:biolims.analysis.cleanReads,
		width:20*6
	});
	cm.push({
		dataIndex:'ratioOfReads',
		hidden : false,
		header:biolims.analysis.ratioOfReads,
		width:20*6
	});
//	cm.push({
//		dataIndex:'raw_len',
//		hidden : false,
//		header:'原始序列长度(bp)',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'raw_GC',
//		hidden : false,
//		header:'原始数据GC含量',
//		width:20*6
//	});
	cm.push({
		dataIndex:'raw_size',
		hidden : false,
		header:biolims.analysis.raw_size,
		width:20*6
	});
	cm.push({
		dataIndex:'cleanBases',
		hidden : false,
		header:biolims.analysis.cleanBases,
		width:20*6
	});
	cm.push({
		dataIndex:'ratioOfBases',
		hidden : false,
		header:biolims.analysis.ratioOfBases,
		width:20*6
	});
	cm.push({
		dataIndex:'dexReads',
		hidden : false,
		header:biolims.analysis.dexReads,
		width:20*6
	});
	cm.push({
		dataIndex:'ratioOfDex',
		hidden : false,
		header:biolims.analysis.ratioOfDex,
		width:20*6
	});
	
//	cm.push({
//		dataIndex:'hq_reads',
//		hidden : false,
//		header:'高质量序列数据',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'hq_len',
//		hidden : false,
//		header:'高质量序列长度',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'hq_GC',
//		hidden : false,
//		header:'高质量数据GC含量',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'hq_size',
//		hidden : false,
//		header:'高质量数据量(MB)',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'hq_reads_pct',
//		hidden : false,
//		header:'高质量序列百分比',
//		width:20*6
//	});
//	
//	cm.push({
//		dataIndex:'hq_data_pct',
//		hidden : false,
//		header:'高质量数据百分比比',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'demultiplexing_Reads',
//		hidden : false,
//		header:'解码序列数目',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'demultiplexing_PCT',
//		hidden : false,
//		header:'解码比例',
//		width:20*6
//	});
//	
	
	 cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/desequencing/deSequencingTask/showDeSequencingInfoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.analysis.resultDetails;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/desequencing/deSequencingTask/delDeSequencingInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择关联主表',
//			handler : selectdesequencingTaskFun
//		});
//	
	function goInExcelcsv(){
		var file = document.getElementById("file-upload1csv").files[0];  
		var n = 0;
		var num=0;
		var ob = deSequencingInfoGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			p.set("lane",this[0]);
//                			p.set("",this[1]);
                			p.set("sampleCode",this[2]);
                			p.set("need",this[3]);
                			p.set("base",this[4]);
                			p.set("reads",this[5]);
                			p.set("pct",this[6]);
                			p.set("adapter",this[7]);
                			p.set("gc",this[8]);
                			p.set("q20",this[9]);
                			p.set("q30",this[10]);
                			p.set("poolingCode",this[11]);
                			p.set("index",this[12]);
                			p.set("ind",this[12]);
                			p.set("dup",this[12]);
                			p.set("ployAT",this[12]);
                			p.set("outPut",this[16]);
							deSequencingInfoGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                });
    	};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	//生成Excel
//	opts.tbar.push({
//		text : '生成Excel',
//		handler : createExcel
//	});
	
	opts.tbar.push({
		text : biolims.analysis.readCSV,
		handler : getCsv1
	});
//	function createExcel(){
//		ajax("post", "/experiment/sequencing/createExcel.action", {
//			taskId : $("#sequencing_id").val()
//		},function(data){
//			message("生成成功！");
//		},null);
//	}
	
	function getCsv1(){
//		alert($("#deSequencingTask_id").val());
		ajax("post", "/analysis/desequencing/deSequencingTask/getCsv.action", {
			taskId : $("#deSequencingTask_id").val()
		},function(data){
			/*message("读取成功！");*/
			window.open(window.location,'_self');;
		},null);
	}
	deSequencingInfoGrid=gridEditTable("deSequencingInfodiv",cols,loadParam,opts);
	$("#deSequencingInfodiv").data("deSequencingInfoGrid", deSequencingInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectdesequencingTaskFun(){
	var selectdesequencingTask=null;
	var win = Ext.getCmp('selectdesequencingTask');
	if (win) {win.close();}
	selectdesequencingTask= new Ext.Window({
	id:'selectdesequencingTask',modal:true,title:biolims.common.relatedMainTableId,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/DeSequencingTaskSelect.action?flag=desequencingTask' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectdesequencingTask.close(); }  }]  });     selectdesequencingTask.show(); }
	function setdesequencingTask(id,name){
		var gridGrid = $("#deSequencingInfodiv").data("deSequencingInfoGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('desequencingTask-id',id);
			obj.set('desequencingTask-name',name);
		});
		var win = Ext.getCmp('selectdesequencingTask');
		if(win){
			win.close();
		}
	}
	
	
	function getCsv(){
		ajax("post", "/analysis/desequencing/deSequencingTask/getCsv.action", {
//			taskId : $("#sequencing_id").val()
		},function(data){
//			message("读取成功！");
		},null);
	}
