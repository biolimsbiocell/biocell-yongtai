var deSequencingUpLoadGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'fileName',//文件名称
		type : "string"
	});
	fields.push({
		name : 'versionNo',//版本号
		type : "String"
	});
	
	//文件上传
	fields.push({
		name : 'attach-fileName',
		type : "string"
	});
	fields.push({
		name : 'attach-id',
		type : "string"
	});
	
	
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '文件编号',
		width : 15 * 10
	});
	
	cm.push({
		dataIndex : 'fileName',
		hidden : false,
		header : '文件名称',
		width : 15 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	cm.push({
		dataIndex : 'uploadTime',
		hidden : false,
		header : '上传时间',
		width : 15 * 10,
		renderer : formatDate,
		editor : new Ext.form.DateField({
			format : 'Y-m-d',
			value: new Date()
		})
	});
	
	
	cm.push({
		dataIndex : 'versionNo',
		hidden : false,
		header : '版本号',
		width : 15 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'attach-fileName',
		header : biolims.common.attachment,
		width : 200,
		hidden : false
	});
	
	cm.push({
		dataIndex : 'attach-id',
		header : biolims.report.reportFileId,
		width : 200,
		hidden : true
	});
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/analysis/desequencing/deSequencingTask/showdeSequencingUpLoadListJson.action";
	var opts = {};
	opts.title = "上传文件";
	opts.height = document.body.clientHeight + 60;
	opts.tbar = [];
//	opts.delSelect = function(ids) {
//		ajax("post", "/crm/customer/patient/delCrmPatientDiagnosis.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				crmPatientRestsGrid.getStore().commitChanges();
//				crmPatientRestsGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
/*	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : function() {
			
			var selectRecord = fileInfoUpGrid.getSelectRecord();
			if (selectRecord && selectRecord.length > 0) {
				var vals = "";
				$.each(selectRecord, function(i, obj) {
					vals = vals + "'" + obj.get("id") + "',";
				});
				vals = vals.substring(0, vals.length - 1);
				ajax("post", "/crm/customer/patient/delModelId.action", {
					objName : "CrmPatientDiagnosis",
					objProperty : "id",
					vals : vals
				}, function(data) {
					if (data.success) {
						fileInfoUpGrid.getStore().commitChanges();
						fileInfoUpGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
			} else {
				message(biolims.common.pleaseChoose);
			}
		}
	});*/

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	
	opts.tbar.push({
		text : biolims.report.uploadReportTemplate,
		handler : function() {
			var isUpload = true;
			var record = deSequencingUpLoadGrid.getSelectOneRecord();
			load("/system/template/template/toSampeUpload.action", { // 是否修改
				fileId : record.get("attach-id"),
				isUpload : isUpload
			}, null, function() {
				$("#upload_file_div").data("callback", function(data) {
					record.set("attach-fileName", data.fileName);
					record.set("attach-id", data.fileId);
				});
			});
		}
	});
	
	opts.tbar.push({
		text : biolims.common.save,
		handler : function() {
			var result = commonGetModifyRecords(deSequencingUpLoadGrid);
			if (result.length > 0) {
				ajax("post", "/analysis/desequencing/deSequencingTask/saveUpLoadList.action", {
					itemDataJson : result
				}, function(data) {
					if (data.success) {
						message(biolims.common.saveSuccess);
						deSequencingUpLoadGrid.getStore().commitChanges();
						deSequencingUpLoadGrid.getStore().reload();
					}else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}
		}
	});
	
	opts.tbar.push({
		text : '下载附件',
		handler : function() {
			var record = deSequencingUpLoadGrid.getSelectOneRecord();
			downFile(record.get("attach-id"));
		}
	});
	deSequencingUpLoadGrid = gridEditTable("deSequencingUpLoaddiv", cols,
			loadParam, opts);
	$("#deSequencingUpLoaddiv").data("deSequencingUpLoadGrid",
			deSequencingUpLoadGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


function downFile(id){
	window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
}
