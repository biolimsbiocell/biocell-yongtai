var deSequencingTab;
$(function() {
	var options = table(true, "",
			"/analysis/desequencing/deSequencingTask/showDeSequencingTaskListJson.action", [ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "name",
				"title" : biolims.common.name,
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "scopeName",
				"title" : biolims.purchase.costCenter,
			}, {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	deSequencingTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(deSequencingTab);
	})
});
function add(){
		window.location=window.ctx+'/analysis/desequencing/deSequencingTask/editDeSequencingTask.action'+"?sampleStyle="+$("#sampleStyle").val();
	}
function edit(){
	var id="";
	id=$(".selected").find("input").val();
	if (id==""||id==undefined){
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/analysis/desequencing/deSequencingTask/editDeSequencingTask.action?id=' + id+"&sampleStyle="+$("#sampleStyle").val();
}
function view() {
	var id = "";
	id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/analysis/desequencing/deSequencingTask/viewDeSequencingTask.action?id=' + id+"&sampleStyle="+$("#sampleStyle").val();
}
//弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"searchName" : "confirmDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "confirmDate##@@##2"
	}, {
		"type" : "table",
		"table" : deSequencingTab
	} ];
}