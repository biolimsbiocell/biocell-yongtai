var deSequencingItemDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'flowCode',
		type:"string"
	});
	   fields.push({
			name:'sampleAmount',
			type:"string"
		});
	   fields.push({
			name:'outPut',
			type:"string"
		});
	   fields.push({
			name:'cluster',
			type:"string"
		});
	   fields.push({
			name:'pf',
			type:"string"
		});
	   fields.push({
			name:'q30',
			type:"string"
		});
	   fields.push({
			name:'result',
			type:"string"
		});
	   fields.push({
			name:'reason',
			type:"string"
		});
	   fields.push({
		   name:'laneCode',
		   type:"string"
	   });
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'libraryCode',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'desequencingTask-id',
		type:"string"
	});
	    fields.push({
		name:'desequencingTask-name',
		type:"string"
	});
	    
	    
    fields.push({
		name:'machine',
		type:"string"
	});
	   fields.push({
		name:'tiles',
		type:"string"
	});
	   fields.push({
		name:'alignRate',
		type:"string"
	});
	   fields.push({
		name:'errorRate',
		type:"string"
	});
	   fields.push({
		name:'pfCluster',
		type:"string"
	});
	   
   fields.push({
		name:'createDate',
		type:"string"
	});
	   fields.push({
		name:'gc',
		type:"string"
	});
	   fields.push({
		name:'q20',
		type:"string"
	});
	   fields.push({
		name:'phasing',
		type:"string"
	});
	   fields.push({
		name:'prePhasing',
		type:"string"
	});
	   
   fields.push({
		name:'desResult',
		type:"string"
	});
	   fields.push({
		name:'production',
		type:"string"
	});
	   fields.push({
		name:'length',
		type:"string"
	});
	/**
	 * =====================================》
	 * 下机质控 明细
	 * 
	 */
	   fields.push({
			name:'sampleID',
			type:"string"
		});
	   fields.push({
			name:'kit',
			type:"string"
		});
	   fields.push({
			name:'raw_reads',
			type:"string"
		});
	   fields.push({
			name:'raw_len',
			type:"string"
		});
	   fields.push({
			name:'raw_size',
			type:"string"
		});
	   fields.push({
			name:'hq_reads',
			type:"string"
		});
	   fields.push({
			name:'hq_len',
			type:"string"
		});
	   fields.push({
			name:'hq_size',
			type:"string"
		});
	   fields.push({
			name:'hq_reads_pct',
			type:"string"
		});
	   fields.push({
			name:'per_lane_percent_raw_reads',
			type:"string"
		});
	   fields.push({
			name:'per_lane_percent_HQ_reads',
			type:"string"
		});
	   fields.push({
			name:'medianInsert',
			type:"string"
		});
	   fields.push({
			name:'duplicate',
			type:"string"
		});
	   fields.push({
			name:'onTarget_raw',
			type:"string"
		});
	   fields.push({
			name:'onTarget_add',
			type:"string"
		});
	   fields.push({
			name:'oriDepth',
			type:"string"
		});
	   fields.push({
			name:'dedupDepth',
			type:"string"
		});
	   fields.push({
			name:'hqDataPct',
			type:"string"
		});
	   fields.push({
			name:'mappingPct',
			type:"string"
		});
	   fields.push({
			name:'qTwentyPCT',
			type:"string"
		});
	   fields.push({
			name:'qThirtyPCT',
			type:"string"
		});
	   fields.push({
			name:'oneXcoverage',
			type:"string"
		});
	   fields.push({
			name:'tenXcoverage',
			type:"string"
		});
	   fields.push({
			name:'twentyXcoverage',
			type:"string"
		});
	   fields.push({
			name:'fiftyXcoverage',
			type:"string"
		});
	   fields.push({
			name:'twentyPctMeancoverage',
			type:"string"
		});
	   fields.push({
			name:'submit',
			type:"string"
		});
	   fields.push({
			name:'nextFlow',
			type:"string"
		});
	   
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'stat',
			type:"string"
		});
	   fields.push({
			name:'lane',
			type:"string"
		});
	   fields.push({
			name:'length',
			type:"string"
		});
	   fields.push({
			name:'gcContent',
			type:"string"
		});
	   fields.push({
			name:'nContent',
			type:"string"
		});
	   fields.push({
			name:'ratioOfLane',
			type:"string"
		});
	   fields.push({
			name:'cleanReads',
			type:"string"
		});
	   fields.push({
			name:'ratioOfReads',
			type:"string"
		});
	   fields.push({
			name:'cleanBases',
			type:"string"
		});
	   fields.push({
			name:'ratioOfBases',
			type:"string"
		});
	   fields.push({
			name:'meanDepthDedup',
			type:"string"
		});
	   fields.push({
			name:'oneCoverageDedup',
			type:"string"
		});
	   fields.push({
			name:'tenCoverageDedup',
			type:"string"
		});
	   
	   fields.push({
			name:'twentyCoverageDedup',
			type:"string"
		});
	   
	   fields.push({
			name:'fiftyCoverageDedup',
			type:"string"
		});
	   
	   fields.push({
			name:'twentyMeanCoverageDedup',
			type:"string"
		});
	   fields.push({
			name:'runId',
			type:"string"
		});
		fields.push({
			name:'productName',
			type:"string"
		});
		fields.push({
			name:'projectId',
			type:"string"
		});
		fields.push({
			name:'orderCode',
			type:"string"
		});

	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'flowCode',
//		hidden : false,
//		header:'flow cell号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'laneCode',
//		hidden : false,
//		header:'lane号',
//		width:20*6
//	});
//	//鼠标聚焦时触发事件 
//	var code =new Ext.form.TextField({
//            allowBlank: false
//    });
//	code.on('focus', function() {
//		var selectRecord = deSequencingItemGrid.getSelectionModel();
//		if (selectRecord.getSelections().length > 0) {
//			$.each(selectRecord.getSelections(), function(i, obj) {
//				var code=obj.get("poolingCode");
//				loadMaterials(code);
//			});
//		}
//	});
//	cm.push({
//		dataIndex:'poolingCode',
//		hidden : false,
//		header:'pooling号',
//		width:20*6,
//		editor : code
//	});
//	cm.push({
//		dataIndex:'sampleAmount',
//		hidden : false,
//		header:'样本量',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'outPut',
//		hidden : true,
//		header:'产量',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'cluster',
//		hidden : false,
//		header:'cluster',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'pf',
//		hidden : false,
//		header:'PF',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'q30',
//		hidden : false,
//		header:'Q30',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'结果',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'reason',
//		hidden : false,
//		header:'原因分类',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'note',
//		hidden : false,
//		header:'备注',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'desequencingTask-id',
//		hidden : true,
//		header:'关联主表ID',
//		width:20*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'desequencingTask-name',
//		hidden : true,
//		header:'关联主表',
//		width:20*10
//	});
//	
//	
//	cm.push({
//		dataIndex:'machine',
//		hidden : false,
//		header:'机器号',
//		width:20*6,
//		editor : code
//	});
//	cm.push({
//		dataIndex:'tiles',
//		hidden : false,
//		header:'Tiles',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'alignRate',
//		hidden : true,
//		header:'Align_rate(%)',
//		width:20*6
//	});
//	
//	cm.push({
//		dataIndex:'errorRate',
//		hidden : false,
//		header:'Error_rate(%)',
//		width:20*6,
//		editor : code
//	});
//	cm.push({
//		dataIndex:'pfCluster',
//		hidden : false,
//		header:'PF_Cluster',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'createDate',
//		hidden : true,
//		header:'Date',
//		width:20*6
//	});
//	
//	cm.push({
//		dataIndex:'gc',
//		hidden : false,
//		header:'GC(%)',
//		width:20*6,
//		editor : code
//	});
//	cm.push({
//		dataIndex:'q20',
//		hidden : false,
//		header:'Q20(%)',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'phasing',
//		hidden : true,
//		header:'Phasing(%)',
//		width:20*6
//	});
//	
//	cm.push({
//		dataIndex:'prePhasing',
//		hidden : false,
//		header:'PrePhasing(%)',
//		width:20*6,
//		editor : code
//	});
//	cm.push({
//		dataIndex:'desResult',
//		hidden : false,
//		header:'Result',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'production',
//		hidden : true,
//		header:'Production(G)',
//		width:20*6
//	});
//	
//	cm.push({
//		dataIndex:'length',
//		hidden : true,
//		header:'Length',
//		width:20*6
//	});
	/**
	 * =====================================》
	 * 下机质控 明细
	 * 
	 */
	cm.push({
		dataIndex:'orderCode',
		hidden : false,
		header:"订单编号",
		width:20*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'projectId',
		hidden : false,
		header:"项目编号",
		width:20*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sampleID',
		hidden : false,
		header:"文库编号",
		width:20*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:"检测项目",
		width:20*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'runId',
		hidden : false,
		header:"RUN_ID",
		width:20*6
//		,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:biolims.common.sampleType,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stat',
		hidden : true,
		header:biolims.common.stat,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'kit',
		hidden : true,
		header:biolims.sequencing.kit,
		width:20*6
	});
	cm.push({
		dataIndex:'lane',
		hidden : true,
		header:biolims.pooling.lane2,
		width:20*6
	});
	cm.push({
		dataIndex:'length',
		hidden : true,
		header:biolims.analysis.length,
		width:20*6
	});
	cm.push({
		dataIndex:'gcContent',
		hidden : false,
		header:biolims.analysis.gcContent,
		width:20*6
	});
	cm.push({
		dataIndex:'nContent',
		hidden : false,
		header:biolims.analysis.nContent,
		width:20*6
	});
	
	cm.push({
		dataIndex:'qTwentyPCT',
		hidden : false,
		header:biolims.analysis.qTwentyPCT,
		width:20*6
	});
	cm.push({
		dataIndex:'qThirtyPCT',
		hidden : false,
		header:biolims.analysis.qThirtyPCT,
		width:20*6
	});
	cm.push({
		dataIndex:'ratioOfLane',
		hidden : false,
		header:biolims.analysis.ratioOfLane,
		width:20*6
	});
	cm.push({
		dataIndex:'raw_reads',
		hidden : false,
		header:biolims.analysis.raw_reads,
		width:20*6
	});
	cm.push({
		dataIndex:'cleanReads',
		hidden : false,
		header:biolims.analysis.cleanReads,
		width:20*6
	});
	cm.push({
		dataIndex:'ratioOfReads',
		hidden : false,
		header:biolims.analysis.ratioOfReads,
		width:20*6
	});
//	cm.push({
//		dataIndex:'raw_len',
//		hidden : true,
//		header:'原始序列长度(bp)',
//		width:20*6
//	});
	cm.push({
		dataIndex:'raw_size',
		hidden : false,
		header:biolims.analysis.raw_size,
		width:20*6
	});
	cm.push({
		dataIndex:'cleanBases',
		hidden : false,
		header:biolims.analysis.cleanBases,
		width:20*6
	});
	cm.push({
		dataIndex:'ratioOfBases',
		hidden : false,
		header:biolims.analysis.ratioOfBases,
		width:20*6
	});
	cm.push({
		dataIndex:'hq_reads',
		hidden : false,
		header:biolims.master.hq_reads,
		width:20*6
	});
//	cm.push({
//		dataIndex:'hq_len',
//		hidden : true,
//		header:'高质量序列长度',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'hq_size',
//		hidden : true,
//		header:'高质量数据量(MB)',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'hq_reads_pct',
//		hidden : true,
//		header:'高质量序列百分比',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'per_lane_percent_raw_reads',
//		hidden : true,
//		header:'原始序列通量比',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'per_lane_percent_HQ_reads',
//		hidden : true,
//		header:'高质量序列通量比',
//		width:20*6
//	});
	cm.push({
		dataIndex:'medianInsert',
		hidden : false,
		header:biolims.analysis.medianInsert,
		width:20*6
	});
	cm.push({
		dataIndex:'duplicate',
		hidden : false,
		header:biolims.analysis.duplicate,
		width:20*6
	});
	cm.push({
		dataIndex:'onTarget_raw',
		hidden : false,
		header:biolims.analysis.onTarget_raw,
		width:20*6
	});
	cm.push({
		dataIndex:'onTarget_add',
		hidden : false,
		header:biolims.analysis.onTarget_add,
		width:20*6
	});
	cm.push({
		dataIndex:'mappingPct',
		hidden : false,
		header:biolims.analysis.mappingPct,
		width:20*6
	});
	cm.push({
		dataIndex:'oriDepth',
		hidden : false,
		header:biolims.analysis.oriDepth,
		width:20*6
	});
//	cm.push({
//		dataIndex:'dedupDepth',
//		hidden : true,
//		header:'去重复测序深度',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'hqDataPct',
//		hidden : true,
//		header:'高质量数据百分比',
//		width:20*6
//	});
	
	

	
	cm.push({
		dataIndex:'oneXcoverage',
		hidden : false,
		header:biolims.analysis.oneXcoverage,
		width:20*6
	});
	cm.push({
		dataIndex:'tenXcoverage',
		hidden : false,
		header:biolims.analysis.tenXcoverage,
		width:20*6
	});
	cm.push({
		dataIndex:'twentyXcoverage',
		hidden : false,
		header:biolims.analysis.twentyXcoverage,
		width:20*6
	});
	cm.push({
		dataIndex:'fiftyXcoverage',
		hidden : false,
		header:biolims.analysis.fiftyXcoverage,
		width:20*6
	});
	cm.push({
		dataIndex:'twentyPctMeancoverage',
		hidden : false,
		header:biolims.analysis.twentyPctMeancoverage,
		width:20*6
	});
	cm.push({
		dataIndex:'meanDepthDedup',
		hidden : false,
		header:biolims.analysis.meanDepthDedup,
		width:20*6
	});
	cm.push({
		dataIndex:'oneCoverageDedup',
		hidden : false,
		header:biolims.analysis.oneCoverageDedup,
		width:20*6
	});
	cm.push({
		dataIndex:'tenCoverageDedup',
		hidden : false,
		header:biolims.analysis.tenCoverageDedup,
		width:20*6
	});
	cm.push({
		dataIndex:'twentyCoverageDedup',
		hidden : false,
		header:biolims.analysis.twentyCoverageDedup,
		width:20*6
	});
	cm.push({
		dataIndex:'fiftyCoverageDedup',
		hidden : false,
		header:biolims.analysis.fiftyCoverageDedup,
		width:20*6
	});
	cm.push({
		dataIndex:'twentyMeanCoverageDedup',
		hidden : false,
		header:biolims.analysis.twentyMeanCoverageDedup,
		width:20*6
	});
	var result = new Ext.form.ComboBox({
	store : new Ext.data.JsonStore({
		fields : [ 'id', 'name' ],
		data : [ {
			id : '1',
			name : biolims.common.qualified
		},{
			id : '0',
			name : biolims.common.disqualified
		}]
	}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '待生成报告订单' ],[ '1', '下机质控异常' ]]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:biolims.common.nextFlow,
		width:20*6,
		editor : nextFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	
//	var storePutCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no] ]
//	});
//	var putCob = new Ext.form.ComboBox({
//		store : storePutCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'submit',
//		hidden : false,
//		header:biolims.common.toSubmit,
//		width:20*6,
//		editor : putCob,
//		renderer : Ext.util.Format.comboRenderer(putCob)
//	});
	
	 cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/desequencing/deSequencingItem/showDialogDeSequencingItemListJson.action?projectId="+$("#projectId").val()+"";
	var opts={};
	opts.title=biolims.common.dataDetail;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setDeSequencingItemFun(rec);
	};
	deSequencingItemDialogGrid=gridTable("show_dialog_deSequencingItem_div",cols,loadParam,opts);
	$("#show_dialog_deSequencingItem_div").data("deSequencingItemDialogGrid", deSequencingItemDialogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	
	
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(deSequencingItemDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
