﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});

function add() {
	window.location = window.ctx + "/analysis/desequencing/deSequencingItem/editDeSequencingItem.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/analysis/desequencing/deSequencingItem/showDeSequencingItemList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#deSequencingItem", {
					userId : userId,
					userName : userName,
					formId : $("#deSequencingItem_id").val(),
					title : $("#deSequencingItem_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#deSequencingItem_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/analysis/desequencing/deSequencingItem/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/analysis/desequencing/deSequencingItem/copyDeSequencingItem.action?id=' + $("#deSequencingItem_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#deSequencingItem_id").val() + "&tableId=deSequencingItem");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	 tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.dataDetail,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);