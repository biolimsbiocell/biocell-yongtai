var assignmentTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'fcnumber',
		type:"string"
	});
	    fields.push({
		name:'knumber',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDate,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.common.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'fcnumber',
		header:biolims.sequencing.fcCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'knumber',
		hidden:true,
		header:biolims.analysis.knumber,
		width:20*6,
		
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/assignment/assignmentTask/showAssignmentTaskListJson.action";
	var opts={};
	opts.title=biolims.analysis.dataAnalysisResultTable;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	assignmentTaskGrid=gridTable("show_assignmentTask_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/analysis/assignment/assignmentTask/editAssignmentTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/analysis/assignment/assignmentTask/editAssignmentTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/analysis/assignment/assignmentTask/viewAssignmentTask.action?id=' + id;
}
function exportexcel() {
	assignmentTaskGrid.title = biolims.common.exportList;
	var vExportContent = assignmentTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(assignmentTaskGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
