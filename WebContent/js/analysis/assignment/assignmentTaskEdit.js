$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state = $("#assignmentTask_state").val();
	var fcId =  $("#assignmentTask_fcnumber").val();
	if(state=="3"){
		load("/analysis/assignment/assignmentTask/showAssignmentTaskTempList.action?fcId="+fcId, null, "#assignmentTaskTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#assignmentTaskTempPage").remove();
		$("#showFc").css("display","none");
	}
});	
$(function() {
	$("#tabss").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/analysis/assignment/assignmentTask/editAssignmentTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/analysis/assignment/assignmentTask/showAssignmentTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
	var str = $("#assignmentTaskItem_acceptUser_name");
	//alert(str);
	submitWorkflow("AssignmentTask", {
		userId : userId,
		userName : userName,
		formId : $("#assignmentTask_id").val(),
		title : $("#assignmentTask_name").val()
	}, function() {
		window.location.reload();
	});	
		
});

$("#toolbarbutton_sp").click(function() {
		completeTask($("#assignmentTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});
/**
 * 改变状态，审核
 */
$("#toolbarbutton_status").click(function(){
	var flag=false;
	var grid1 = assignmentTaskItemGrid.store;
	//判断是否有不合格的数据
	for(var i=0; i<grid1.getCount(); i++){
		var user = grid1.getAt(i).get("acceptUser-name");
		var state = grid1.getAt(i).get("states");
		var method = grid1.getAt(i).get("method");
		//判断是否通过
		if(user==""){
			message(biolims.analysis.unqualifiedDetail);
			flag=false;
			break;
		}else{
			flag=true;
		}
	}
	if(flag){
		commonChangeState("formId=" + $("#assignmentTask_id").val() + "&tableId=AssignmentTask");
	}
	
});





function save() {

if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText:biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	
	var assignmentTaskItemdivData = $("#assignmentTaskItemdiv").data("assignmentTaskItemGrid");
	document.getElementById('assignmentTaskItemJson').value = commonGetModifyRecords(assignmentTaskItemdivData);
	
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/analysis/assignment/assignmentTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}

function editCopy() {
	window.location = window.ctx + '/analysis/assignment/assignmentTask/copyAssignmentTask.action?id=' + $("#assignmentTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#assignmentTask_id").val() + "&tableId=assignmentTask");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#assignmentTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'生信任务分配结果表',//biolims.analysis.mutationScreeningResultTable
	    	   contentEl:'markup'
	       } ]
	   });
});


//数据分析明细
load("/analysis/assignment/assignmentTask/showAssignmentTaskItemList.action", {
			id : $("#assignmentTask_id").val()
			}, "#assignmentTaskItempage");


var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
function showfcFun(){
	var win = Ext.getCmp('showfcFun');
	if (win) {win.close();}
	var showfcFun= new Ext.Window({
	id:'showfcFun',modal:true,title:biolims.sequencing.selectFCCode,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/analysis/assignment/assignmentTask/showDeSequencingTaskByState.action?flag=ShowfcFun3' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 showfcFun.close(); }  }]  });
	showfcFun.show(); 
}


function setShowfcFun3(id,rec){
	$("#assignmentTask_fcnumber").val(rec.get("fcCode")) ;
	ajax("post", "/analysis/assignment/assignmentTask/showSequencingResultListJson.action", {
		fcId : id
	}, function(data) {
		console.log(data);
		for(var i=0;i<data.data.length;i++){
			var ob = assignmentTaskTempGrid.getStore().recordType;
			var p = new ob({});
			var obj = data.data[i];
			console.log(obj);
			p.isNew = true;
			p.set("id",obj.id);
			p.set("sampleCode",obj.code);
			p.set("sampleId",obj.sampleId);
			p.set("crmPatientId",obj.crmPatientId);
			p.set("sampleTypeId",obj.sampleType.id);
			p.set("sampleTypeName",obj.sampleType.name);
			p.set("productId",obj.productId);
			p.set("productName",obj.productName);
			assignmentTaskTempGrid.getStore().add(p);
		}
	}, null);
	
	var win = Ext.getCmp('showfcFun');
	if(win){win.close();
	}
}





