var deSequencingTaskByStateGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	    fields.push({
		name:'fcCode',
		type:"string"
	});
	    fields.push({
		name:'refCode',
		type:"string"
	});
	    fields.push({
		name:'spec',
		type:"string"
	});
	    fields.push({
		name:'refName',
		type:"string"
	});
	    fields.push({
		name:'reagent',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'name',
//		header:'描述',
//		width:20*10,
//		sortable:true
//	});
//		cm.push({
//		dataIndex:'createUser-id',
//		header:'下达人ID',
//		width:20*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'createUser-name',
//		header:'下达人',
//		width:20*10,
//		sortable:true
//		});
//	cm.push({
//		dataIndex:'createDate',
//		header:'下达时间',
//		width:20*10,
//		sortable:true
//	});
//		cm.push({
//		dataIndex:'acceptUser-id',
//		header:'操作员ID',
//		width:20*10,
//		sortable:true
//		});
//		cm.push({
//		dataIndex:'acceptUser-name',
//		header:'操作员',
//		width:20*10,
//		sortable:true
//		});
//	cm.push({
//		dataIndex:'acceptDate',
//		header:'操作时间',
//		width:20*10,
//		sortable:true
//	});
	cm.push({
		dataIndex:'fcCode',
		header:biolims.analysis.flowCode,
		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'state',
//		header:'工作流id',
//		width:20*10,
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'stateName',
//		header:'工作流状态',
//		width:20*10,
//		sortable:true
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/assignment/assignmentTask/showDeSequencingTaskByStateJson.action";
	var opts={};
	opts.title=biolims.sequencing.selectFCCode;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setShowfcFun3(id,rec);
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	deSequencingTaskByStateGrid=gridTable("show_deSequencingTaskByState_div",cols,loadParam,opts);
});
