var dateTaskCnvItemCheckedGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'mutantGenes',
		type:"string"
	});
	   fields.push({
		name:'otherName',
		type:"string"
	});
	   fields.push({
		name:'copyNumberVaration',
		type:"string"
	});
	   fields.push({
		name:'copyVarationRatio',
		type:"string"
	});
	   fields.push({
		name:'mutationSource',
		type:"string"
	});
	   fields.push({
		name:'mutationClass',
		type:"string"
	});
	   fields.push({
		name:'mutationType',
		type:"string"
	});
	   fields.push({
		name:'mutationFiction',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	   fields.push({
		name:'createDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'样本编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutantGenes',
		hidden : false,
		header:'突变基因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'otherName',
		hidden : false,
		header:'其他名称',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var copyNumberVarationStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '基因扩增' ], [ '2', '拷贝数缺失' ],]
	});
	
	var copyNumberVarationComboxFun = new Ext.form.ComboBox({
		store : copyNumberVarationStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'copyNumberVaration',
		hidden : false,
		header:'拷贝数变异',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(copyNumberVarationComboxFun),
		editor: copyNumberVarationComboxFun,
		sortable:true
	});
//	cm.push({
//		dataIndex:'copyNumberVaration',
//		hidden : false,
//		header:'拷贝数变异',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'copyVarationRatio',
		hidden : false,
		header:'拷贝数变异倍数',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mutationSource',
		hidden : false,
		header:'突变来源',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var mutationClasssStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '种系突变' ], [ '2', '多态性突变' ],[ '3', '肿瘤突变' ],]
	});
	
	var mutationClassComboxFun = new Ext.form.ComboBox({
		store : mutationClasssStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'mutationClass',
		hidden : false,
		header:'突变分类',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationClassComboxFun),
		editor: mutationClassComboxFun,
		sortable:true
	});
//	cm.push({
//		dataIndex:'mutationClass',
//		hidden : false,
//		header:'突变分类',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var mutationTypeStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [
		        [ '1', '基因融合' ], [ '2', '基因拷贝数变异' ],[ '3', '同义突变' ],[ '4', '错义突变' ],
		        [ '5', '插入非移码突变' ],[ '6', '插入移码突变' ],[ '7', '缺失非移码突变' ],[ '8', '缺失移码突变' ],
		        [ '9', '无义突变' ],[ '10', '剪切突变' ],[ '11', '其他突变' ],
		       ]
	});
	
	var mutationTypeComboxFun = new Ext.form.ComboBox({
		store : mutationTypeStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'mutationType',
		hidden : false,
		header:'突变类型',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationTypeComboxFun),
		editor: mutationTypeComboxFun,
		sortable:true
	});
//	cm.push({
//		dataIndex:'mutationType',
//		hidden : false,
//		header:'突变类型',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var mutationFictionStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'level1' ], [ '2', 'level2' ],[ '3', 'level3' ],[ '4', 'level4' ],]
	});
	
	var mutationFictionComboxFun = new Ext.form.ComboBox({
		store : mutationFictionStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'mutationFiction',
		hidden : false,
		header:'突变分级',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(mutationFictionComboxFun),
		editor: mutationFictionComboxFun,
		sortable:true
	});
//	cm.push({
//		dataIndex:'mutationFiction',
//		hidden : false,
//		header:'突变分级',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : false,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'createDate',
		hidden : false,
		header:'下达时间',
		width:50*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'createUser-id',
		hidden : true,
		header:'下达人ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'createUser-name',
		hidden : false,
		header:'下达人',
		width:20*10
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'工作流状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/check/dataTask/showDateTaskCnvItemCheckedListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="肿瘤突变表";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/check/dataTask/delDateTaskCnvItemChecked.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
			text : '选择相关主表',
				handler : selectdataTaskDialogFun
		});
	opts.tbar.push({
			text : '选择下达人',
				handler : selectcreateUserFun
		});
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = dateTaskCnvItemCheckedGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			o= 12-1;
                			p.set("po.fieldName",this[o]);
                			o= 13-1;
                			p.set("po.fieldName",this[o]);
                			o= 14-1;
                			p.set("po.fieldName",this[o]);
							dateTaskCnvItemCheckedGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	dateTaskCnvItemCheckedGrid=gridEditTable("dateTaskCnvItemCheckeddiv",cols,loadParam,opts);
	$("#dateTaskCnvItemCheckeddiv").data("dateTaskCnvItemCheckedGrid", dateTaskCnvItemCheckedGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectdataTaskFun(){
	var win = Ext.getCmp('selectdataTask');
	if (win) {win.close();}
	var selectdataTask= new Ext.Window({
	id:'selectdataTask',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectdataTask.close(); }  }]  }) });  
    selectdataTask.show(); }
	function setdataTask(rec){
		var gridGrid = $("#dateTaskCnvItemCheckeddiv").data("dateTaskCnvItemCheckedGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('dataTask-id',rec.get('id'));
			obj.set('dataTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectdataTask')
		if(win){
			win.close();
		}
	}
	function selectdataTaskDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/DataTaskSelect.action?flag=dataTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						seldataTaskVal(this);
				}
			}, true, option);
		}
	var seldataTaskVal = function(win) {
		var operGrid = dataTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dateTaskCnvItemCheckeddiv").data("dateTaskCnvItemCheckedGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('dataTask-id',rec.get('id'));
				obj.set('dataTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
function selectcreateUserFun(){
	var win = Ext.getCmp('selectcreateUser');
	if (win) {win.close();}
	var selectcreateUser= new Ext.Window({
	id:'selectcreateUser',modal:true,title:'选择下达人',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=createUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectcreateUser.close(); }  }]  }) ;  
    selectcreateUser.show(); }
	function setcreateUser(rec){
		var gridGrid = $("#dateTaskCnvItemCheckeddiv").data("dateTaskCnvItemCheckedGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('createUser-id',rec.get('id'));
			obj.set('createUser-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectcreateUser')
		if(win){
			win.close();
		}
	}
	function selectcreateUserDialogFun(){
			var title = '';
			var url = '';
			title = "选择下达人";
			url = ctx + "/UserSelect.action?flag=createUser";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selcreateUserVal(this);
				}
			}, true, option);
		}
	var selcreateUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#dateTaskCnvItemCheckeddiv").data("dateTaskCnvItemCheckedGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('createUser-id',rec.get('id'));
				obj.set('createUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
