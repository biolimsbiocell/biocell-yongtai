var dataTaskcheckItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleId',
		type:"string"
	});
	   fields.push({
		name:'crmPatientId',
		type:"string"
	});
	   fields.push({
			name:'name',
			type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'xjPot',
		type:"string"
	});
	    fields.push({
		name:'tbFrequence',
		type:"string"
	});
	   fields.push({
		name:'genePot',
		type:"string"
	});
	   fields.push({
		name:'hgmdPubmedid',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'form',
		type:"string"
	});
	    fields.push({
		name:'dataTask-id',
		type:"string"
	});
	    fields.push({
		name:'dataTask-name',
		type:"string"
	});
	    fields.push({
			name:'MicroRNA',
			type:"string"
		});
	    fields.push({
			name:'type',
			type:"string"
		});
	    
	    fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-name',
			type:"string"
		});
	   
	    fields.push({
			name:'acceptUser-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser-name',
			type:"string"
		});
	    fields.push({
			name:'acceptUser2-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUser2-name',
			type:"string"
		});
	    fields.push({
			name:'reason',
			type:"string"
		});
	    fields.push({
			name:'method',
			type:"string"
		});
	    fields.push({
			name:'states',
			type:"string"
		});
	cols.fields=fields;
	
	var storeStateCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '线粒体', '线粒体' ], [ 'MicroRNA', 'MicroRNA' ] ]
	});
	var cm=[];
	
	/**纯合/杂合*/
	var stateCob = new Ext.form.ComboBox({
		store : storeStateCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:15*10
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'名称',
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var sampleUserFun = new Ext.form.TextField({
		allowBlank : true
	});
	sampleUserFun.on('focus',function(){
		sampleUserFunSelFun();
	});
	cm.push({
		dataIndex : 'acceptUser-id',
		header : '实验员1',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'acceptUser-name',
		header : '实验员1',
		width : 100,
		editor : sampleUserFun
	});
	
	//实验员2
	var sampleUserFun2 = new Ext.form.TextField({
		allowBlank : true
	});
	sampleUserFun2.on('focus',function(){
		sampleUserFunSelFun2();
	});
	cm.push({
		dataIndex : 'acceptUser2-id',
		header : '实验员2',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'acceptUser2-name',
		header : '实验员2',
		width : 100,
		editor : sampleUserFun2
	});
	
	cm.push({
		hidden:false,
		dataIndex:'sampleId',
		header:'样本编号',
		width:15*10
	});
	cm.push({
		hidden:false,
		dataIndex:'crmPatientId',
		header:'电子病历编号',
		width:15*10
	});
	
	cm.push({
		hidden:true,
		dataIndex:'name',
		header:'名称',
		width:15*10
	});
	//审核状态
	var stateStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '审核不通过' ], [ '2', '审核通过' ],]
	});
	
	var statesComboxFun = new Ext.form.ComboBox({
		store : stateStore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'states',
		hidden : false,
		header:'审核状态',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(statesComboxFun),
		editor: statesComboxFun,
		sortable:true
	});
	

	var method = new Ext.form.ComboBox({
		transform : "grid-method",
		width : 100,
		triggerAction : 'all',
		lazyRender : true
	});
	
	cm.push({
		dataIndex : 'method',
		header : '处理方式',
		width : 80,
		hidden:false,
		editor : method,
		renderer : function(value, metadata, record, rowIndex, colIndex, store) {
			if (value == "0") {
				return "<span style='color:red'>不通过</span>";
			} else if (value == "1") {
				return "通过";
			} else {
				return "请选择";
			}
		}
	});
	
	
	cm.push({
		dataIndex : 'reason',
		header : '失败原因',
		width : 140,
		hidden:false,
		editor : new Ext.form.TextField({
			allowBlank : true,
			validator : function(val) {
				if (val.length > 100) {
					message("不能大于100个字符");
					return false;
				} else {
					return true;
				}

			}
		})
	});
	
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:'突变位置',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xjPot',
		hidden : true,
		header:'碱基改变',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tbFrequence',
		hidden : true,
		header:'突变频率',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'genePot',
		hidden : true,
		header:'基因改变',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hgmdPubmedid',
		hidden : true,
		header:'参考文献ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'注释',
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'form',
		hidden : true,
		header:'类型',
		width:15*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-id',
		hidden : true,
		header:'二代任务ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTask-name',
		hidden : true,
		header:'二代任务',
		width:15*10

	});
	cm.push({
		dataIndex:'MicroRNA',
		hidden : true,
		header:'MicroRNA',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'type',
		hidden : true,
		header:'纯合/杂合',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/check/dataTask/showDataTaskCheckItemListJson.action?id="+ $("#dataTask_id").val()+"&fcId="+$("#dataTask_fcnumber").val();
	loadParam.limit = 1000;
	var opts={};
	opts.title="技术分析任务明细";
	opts.height =  document.body.clientHeight-115;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/data/dataTask/delDataTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dataTaskcheckItemGrid.getStore().commitChanges();
				dataTaskcheckItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	 
	   if($("#dataTask_id").val() != undefined){
		  
		   opts.tbar.push({
				text : "选择实验员",
				handler : function() {
					sampleUserFunSelFun();
					sampleUserFunSelFun2();
				}
			});
		
		opts.tbar.push({
			text : '显示可编辑列',
			handler : null
		});
		opts.tbar.push({
			text : '取消选中',
			handler : null
		});
		opts.tbar.push({
			text : "批量结果",
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bar_rna"), "批量结果", null, {
					"确定" : function() {
						var records = dataTaskcheckItemGrid.getSelectRecord();
						if (!records.length) {
							records = dataTaskcheckItemGrid.getAllRecord();
						}
						if (records && records.length > 0) {
							var method = $("#grid-method").val();
							dataTaskcheckItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("method", method);
							});
							dataTaskcheckItemGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
//		opts.tbar.push({
//			text : "批量上传(csv文件)",
//			handler : function() {
//				var options = {};
//				options.width = 350;
//				options.height = 200;
//				loadDialogPage($("#bat_upload_divRna6"), "批量上传", null, {
//					"确定" : function() {
//						goInExcelRna();
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}
//		});
//		opts.tbar.push({
//			text : "模板下载",
//			handler : function() {
//			window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\xsl\\dataTaskItem.xlsx','','');
//			}
//		});
	   }
	   dataTaskcheckItemGrid=gridEditTable("dataTaskCheckItemdiv",cols,loadParam,opts);
	$("#dataTaskCheckItemdiv").data("dataTaskcheckItemGrid", dataTaskcheckItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	function getPath(obj) {
		  if (obj) {
		  if (window.navigator.userAgent.indexOf("MSIE") >= 1) {
		  obj.select(); return document.selection.createRange().text;
		  }
		  else if (window.navigator.userAgent.indexOf("Firefox") >= 1) {
		  if (obj.files) {
		  return obj.files.item(0).getAsDataURL();
		  }
		  return obj.value;
		  }
		  return obj.value;
		  }
		  } 
	
	var idTmr = "";
	function goInExcelRna(){
		var file = document.getElementById("file-uploadRna6").files[0];  
		var reader = new FileReader();  
		//将文件以文本形式读入页面  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
		var n = 0;	
    	var ob = dataTaskcheckItemGrid.getStore().recordType;
    	var csv_data = $.simple_csv(this.result);
        $(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;	
                			p.set("sampleId",this[0]);
							p.set("location",this[1]);
							p.set("xjPot",this[2]);
							p.set("tbFrequence",this[3]);
							p.set("genePot",this[4]);
							p.set("MicroRNA",this[5]);
							p.set("type",this[6]);
							dataTaskcheckItemGrid.getStore().insert(0, p);
                		}
                	}
                    n = n +1;
                });
    		}  
	     }
		function Cleanup() { 
		window.clearInterval(idTmr); 
		CollectGarbage(); 
		}
		
		setTimeout(function(){
			if($("#bpmTaskId").val()&&$("#bpmTaskId").val()!='null'){
				
				if($("#sampleGen2Task_stateName").val()!='已实验'){
					sx();	
				}
				if(window.userName!=$("#sampleCancerTask_createUser_name").val()){
						
				}
			}
		},100);
	
});
function sx() {
	dataTaskcheckItemGrid.store.reload();
	var filterseq = function(record, id){
		var flag = true;
		   if (record.get("acceptUser-id")==window.userId){
		      	 flag = true;
		      }
		   else{
			     flag =  false; 
		   }
		   return flag;
		};
		var onStoreLoadSeq = function(store, records, options){
		   store.filterBy(filterseq);
		};
		dataTaskcheckItemGrid.store.on("load", onStoreLoadSeq);
}
function sampleUserFunSelFun(){
	var win = Ext.getCmp('sampleUserFunSelFun');
	if (win) {
		win.close();
	}
	var records = dataTaskcheckItemGrid.getSelectRecord();	
	var sampleUserFunSelFun = new Ext.Window(
			{
				id : 'sampleUserFunSelFun',
				modal : true,
				title : '选择实验员',
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/core/user/userSelect.action?flag=CrmCustomerFun3' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						sampleUserFunSelFun.close();
					}
				} ]
			});
	sampleUserFunSelFun.show();
}

function setCrmCustomerFun3(id,name){
	records = dataTaskcheckItemGrid.getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("acceptUser-id", id);
		obj.set("acceptUser-name",name);
	});
	var win = Ext.getCmp('sampleUserFunSelFun');
	if(win){win.close();}
}
/**
 * 实验员2
 */
function sampleUserFunSelFun2(){
	var win = Ext.getCmp('sampleUserFunSelFun');
	if (win) {
		win.close();
	}
	var records = dataTaskcheckItemGrid.getSelectRecord();	
	var sampleUserFunSelFun = new Ext.Window(
			{
				id : 'sampleUserFunSelFun',
				modal : true,
				title : '选择实验员',
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/core/user/userSelect.action?flag=CrmCustomerFun32' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						sampleUserFunSelFun.close();
					}
				} ]
			});
	sampleUserFunSelFun.show();
}
function setCrmCustomerFun32(id,name){
	records = dataTaskcheckItemGrid.getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("acceptUser2-id", id);
		obj.set("acceptUser2-name",name);
	});
	var win = Ext.getCmp('sampleUserFunSelFun');
	if(win){win.close();}
}

