var dataTransferDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'project-name',
		type:"string"
	});
	    fields.push({
			name:'orderId',
			type:"string"
		});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'userGroup-name',
		type:"string"
	});
	    fields.push({
		name:'tester-name',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'project-name',
		header:'项目名称',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'template-name',
		header:'实验模板',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'userGroup-name',
		header:'实验组',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'tester-name',
		header:'实验员',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态名称',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/dataTransfer/showDataTransferListJson.action";
	var opts={};
	opts.title="数据传输";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	dataTransferDialogGrid=gridTable("show_dialog_dataTransfer_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(dataTransferDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
