$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + '/analysis/dataTransfer/editDataTransfer.action';
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + "/analysis/dataTransfer/showDataTransferList.action";
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("DataTransfer", {
					userId : userId,
					userName : userName,
					formId : $("#dataTransfer_id").val(),
					title : $("#dataTransfer_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#dataTransfer_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){
	    var dataTransferItemDivData = $("#dataTransferItemdiv").data("dataTransferItemGrid");
		document.getElementById('dataTransferItemJson').value = commonGetModifyRecords(dataTransferItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/analysis/dataTransfer/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/analysis/dataTransfer/copyDataTransfer.action?id=' + $("#dataTransfer_id").val();
}
$("#toolbarbutton_status").click(function(){
	commonChangeState("formId=" + $("#dataTransfer_id").val() + "&tableId=dataTransfer");

});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#dataTransfer_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'数据传输',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/analysis/dataTransfer/showDataTransferItemList.action", {
				id : $("#dataTransfer_id").val()
			}, "#dataTransferItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	
	//科研任务单
	var loadTechService;
	function selectOrderId(){
		 	var options = {};
		 	options.width = document.body.clientWidth - 470;
		 	options.height = document.body.clientHeight - 80;
		 	loadTechService=loadDialogPage(null, biolims.common.selectExtract, "/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogList.action", {
				"Confirm" : function() {
					var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						$("#dataTransfer_orderId").val(selectRecord[0].get("id"));
						$("#dataTransfer_userGroup").val(selectRecord[0].get("acceptUser-id"));
						$("#dataTransfer_userGroup_name").val(selectRecord[0].get("acceptUser-name"));
						ajax("post", "/analysis/dataTransfer/setDataTransferItem.action", { 
							id : selectRecord[0].get("id"),
							}, function(data) {
								if (data.success) {	
									var ob = dataTransferItemGrid.getStore().recordType;
									dataTransferItemGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										if($("#dataTransfer_project").val()==null||("")==$("#dataTransfer_project").val()){
										$("#dataTransfer_project").val(obj.project.id);
										$("#dataTransfer_project_name").val(obj.project.name);}
										var p = new ob({});
										p.isNew = true;
										p.set("sampleCode", obj.code);
										p.set("inwardCode",obj.inwardCode);
										p.set("sampleName",obj.name);
										dataTransferItemGrid.getStore().add(p);							
									});
									
									dataTransferItemGrid.startEditing(0, 0);		
								} 
							}, null); 
						
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
	}
	