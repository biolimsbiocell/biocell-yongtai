var dataTransferItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleName',
		type:"string"
	});
	   fields.push({
		name:'inwardCode',
		type:"string"
	});
	   fields.push({
			name:'flowCell',
			type:"string"
		});
		   fields.push({
			name:'laneCode',
			type:"string"
		});
		   fields.push({
			name:'transferWay',
			type:"string"
		});
	   fields.push({
		name:'ftp',
		type:"string"
	});
	   fields.push({
		name:'dataTransfer-id',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'sampleName',
		hidden : false,
		header:'原始样本名称',
		width:20*6,
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'inwardCode',
		hidden : true,
		header:'内部项目号',
		width:20*6,
	});
	
	cm.push({
		dataIndex:'flowCell',
		hidden : false,
		header:'FC号',
		width:20*6,
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'laneCode',
		hidden : false,
		header:'lane编号',
		width:20*6,
	});
	
	cm.push({
		dataIndex:'transferWay',
		hidden : false,
		header:'传输方式',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ftp',
		hidden : false,
		header:'FTP账户',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataTransfer-id',
		hidden : true,
		header:'相关主表',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/analysis/dataTransfer/showDataTransferItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="数据明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/analysis/dataTransfer/delDataTransferItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});	
	dataTransferItemGrid=gridEditTable("dataTransferItemdiv",cols,loadParam,opts);
	$("#dataTransferItemdiv").data("dataTransferItemGrid", dataTransferItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});