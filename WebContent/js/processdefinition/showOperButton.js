$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'operTitle',
		type : "string"
	});
	fields.push({
		name : 'operValue',
		type : "string"
	});
	fields.push({
		name : 'orderNumber',
		type : "string"
	});

	cols.fields = fields;

	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 120,
		hidden : true
	});
	cm.push({
		dataIndex : 'operTitle',
		header : biolims.common.designation,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'operValue',
		header : biolims.common.value,
		width : 100,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'orderNumber',
		header : biolims.user.sortingNumber,
		width : 100,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/processdefinition/showOperButtonJson.action?activitiId=" + $("#activiti_id").val()
			+ "&definitionId=" + $("#definition_id").val();
	var opts = {};
	// opts.title = "流程定义配置人员";
	opts.height = 350;
	opts.width = 450;
	opts.id = "operButton";
	opts.tbar = [];

	opts.delSelect = function(ids) {
		ajax("post", "/workflow/processdefinition/deleteOperButton.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);

	};
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});

	opts.tbar.push({
		text : biolims.common.save,
		handler : function() {
			var datas = [];
			var modifyRecord = operButtonGrid.getModifyRecord();
			if (modifyRecord) {
				var activitiId = $("#activiti_id").val();
				var definitionId = $("#definition_id").val();
				$.each(modifyRecord, function(i, obj) {
					var data = {};
					data.id = obj.get("id");
					data.operTitle = obj.get("operTitle");
					data.operValue = obj.get("operValue");
					data.activitiId = activitiId;
					data.definitionId = definitionId;
					data.orderNumber = obj.get("orderNumber");
					datas.push(data);
				});
			}

			if (datas.length > 0) {
				ajax("post", "/workflow/processdefinition/saveOperButton.action", {
					data : JSON.stringify(datas),
				}, function(data) {
					if (data.success) {
						message(biolims.common.saveSuccess);
						operButtonGrid.getStore().reload();
					} else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}
		}
	});

	var operButtonGrid = gridEditTable("button_set_div", cols, loadParam, opts);
});