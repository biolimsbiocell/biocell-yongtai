$(function() {
	$("#workflow_tabs").tabs({
		select : function(event, ui) {
			setIframeSrc(ui.index);
		}
	});
	load("/workflow/processdefinition/processDefinitionActivitis.action", {
		definitionId : $("#definition_id").val()
	}, "#workflow_tabs_0");

	function setIframeSrc(index) {
		var url = "";
		if (index == 0) {
			url = "/workflow/processdefinition/processDefinitionActivitis.action";
		} else if (index == 1) {
			url = "/workflow/processdefinition/showBindFormList.action";
		}
		for ( var i = 0; i < 2; i++) {
			$("#workflow_tabs_" + i).html("");
		}
		load(url, {
			definitionId : $("#definition_id").val()
		}, "#workflow_tabs_" + index);
	}
});