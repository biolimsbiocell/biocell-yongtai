$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'formName',
		type : "string"
	});
	fields.push({
		name : 'processDefinitionKey',
		type : "string"
	});

	cols.fields = fields;

	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 120
	});
	cm.push({
		dataIndex : 'formName',
		header : biolims.master.formName,
		width : 200,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'processDefinitionKey',
		header : biolims.common.processKEY,
		width : 200,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/processdefinition/showBindFormListJson.action";
	var opts = {};
	// opts.title = "流程定义配置人员";
	opts.height = document.documentElement.clientHeight - 40;
	opts.id = "bindFormGrid";
	opts.tbar = [];

	opts.delSelect = function(ids) {
		ajax("post", "/workflow/processdefinition/deleteBindForm.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);

	};
	
	opts.tbar.push({
		text : biolims.common.fillDetail,
		iconCls : 'add',
		handler : function() {
			var ob = bindFormGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			p.set("processDefinitionKey",$("#key_id").val());
			var store = bindFormGrid.getStore();
			bindFormGrid.stopEditing();
			store.insert(0, p);
			bindFormGrid.startEditing(0, 0);

		}
	});
	
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});

	opts.tbar.push({
		text : biolims.common.save,
		handler : function() {
			var datas = [];
			var modifyRecord = bindFormGrid.getModifyRecord();
			if (modifyRecord) {
				$.each(modifyRecord, function(i, obj) {
					var data = {};
					data.id = obj.get("id");
					data.formName = obj.get("formName");
					data.processDefinitionKey = obj.get("processDefinitionKey");

					datas.push(data);
				});
			}

			if (datas.length > 0) {
				ajax("post", "/workflow/processdefinition/saveBindForm.action", {
					data : JSON.stringify(datas),
				}, function(data) {
					if (data.success) {
						message(biolims.common.saveSuccess);
						bindFormGrid.getStore().reload();
					} else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.selectFormType,
		handler : function() {
		var win = Ext.getCmp('searchFormFun');
		if (win) {win.close();}
		var searchFormFun= new Ext.Window({
		id:'searchFormFun',modal:true,title:biolims.common.selectFormType,layout:'fit',width:document.body.clientWidth/2,height:400,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/applicationTypeTable/applicationTypeTableSelect.action?flag=searchFormFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 searchFormFun.close(); }  }]  });     searchFormFun.show(); }
		
	});
	
	opts.tbar.push({
		text : biolims.common.returnTheListOfDefinitions,
		handler : function() {
			window.location = window.ctx + "/workflow/processdefinition/showProcessDefinitionList.action";

		}
	});

	var bindFormGrid = gridEditTable("form_set_div", cols, loadParam, opts);
	$("#form_set_div").data("formGrid", bindFormGrid);
	
});
function setsearchFormFun(id,name){
		
	var formGrid = $('#form_set_div').data('formGrid');
	var selRecords = formGrid.getSelectionModel().getSelections(); 
	var selRecord=selRecords[0];
	selRecord.set('formName',id);
	
	
	var win = Ext.getCmp('searchFormFun')
	if(win){win.close();}
	}