$(function() {
	$("button").button();

	var cols = {};
	// cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'processDefinitionId',
		type : "string"
	});
	fields.push({
		name : 'deploymentId',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'key',
		type : "string"
	});
	fields.push({
		name : 'version',
		type : "string"
	});
	fields.push({
		name : 'resourceName',
		type : "string"
	});
	fields.push({
		name : 'diagramResourceName',
		type : "string"
	});
	fields.push({
		name : 'deploymentTime',
		type : "string"
	});
	fields.push({
		name : 'isMailRemind',
		type : "string"
	});
	fields.push({
		name : 'wpAdminId',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 120,
		hidden : true
	});
	cm.push({
		dataIndex : 'processDefinitionId',
		header : biolims.common.processDefinitionID,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'deploymentId',
		header :biolims.common.processDeploymentID,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.formTypeName,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'key',
		header : biolims.common.processKEY,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'version',
		header : biolims.common.edition,
		width : 120,
		sortable : true
	});
	cm.push({
		dataIndex : 'resourceName',
		header : 'XML',
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'diagramResourceName',
		header : biolims.common.picture,
		width : 130,
		sortable : true
	});
	cm.push({
		dataIndex : 'deploymentTime',
		header : biolims.common.deploymentTime,
		width : 150,
		sortable : true
	});
	cm.push({
		dataIndex : 'isMailRemind',
		header : biolims.common.mailReminding,
		width : 100
	});
	cm.push({
		dataIndex : 'wpAdminId',
		header : biolims.common.processManager,
		width : 100
	});
	
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/processdefinition/showProcessDefinitionListJson.action";
	var opts = {};
	opts.title = biolims.common.processDefinitionManagement;
	opts.height = document.documentElement.clientHeight - 30;
	opts.tbar = [];

	var grid;
	opts.tbar.push({
		text : biolims.common.deploymentProcessDefinition,
		handler : function() {
			load("/operfile/toCommonUpload.action", {
				fileId : '',
				isUpload : true
			}, null, function() {
				$("#upload_file_div").data("callback", function(data) {
					var fileId = data.fileId;
					ajax("post", "/workflow/processdefinition/deploy.action", {
						fileId : fileId
					}, function(data) {
						if (data.success) {
							message(data.message);
							grid.getStore().reload();
						} else {
							message(data.message);
						}
					}, null);
				});
			});
		}
	});
	opts.tbar.push({
		text : biolims.common.checkXML,
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				var url = "/workflow/processdefinition/loadResourceByDeployment.action";
				openDialog(window.ctx + url + "?deploymentId=" + selRocord[0].get("deploymentId") + "&resourceName="
						+ selRocord[0].get("resourceName"));
			} else {
				message(biolims.common.pleaseSelectAProcessDefinition);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.checkFlowChart,
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				var url = "/workflow/processdefinition/loadResourceByDeployment.action";
				openDialog(window.ctx + url + "?deploymentId=" + selRocord[0].get("deploymentId") + "&resourceName="
						+ selRocord[0].get("diagramResourceName"));
			} else {
				message(biolims.common.pleaseSelectAProcessDefinition);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.configurationProcessDefinition,
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				var url = "/workflow/processdefinition/processDefinitionConfigView.action";
				window.location = window.ctx + url + "?definitionId=" + selRocord[0].get("processDefinitionId")+"&keyId=" + selRocord[0].get("key");
			} else {
				message(biolims.common.pleaseSelectAProcessDefinition);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.mailReminding,
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				confirmMsg(biolims.common.sureDefinition+"( " + selRocord[0].get("processDefinitionId") + " )"+biolims.common.sureDefinition2, null,
						function() {
							ajax("post", "/workflow/processdefinition/setMailRemind.action", {
								id : selRocord[0].get("id")
							}, function(data) {
								if (data.success) {
									message(biolims.common.setSuccess);
									grid.getStore().reload();
								} else {
									message(biolims.common.setFailure);
								}
							}, null);
						});
			} else {
				message(biolims.common.pleaseSelectAProcessDefinition);
			}
		}
	});

	grid = gridTable("show_process_definition_div", cols, loadParam, opts);
	$("#search_btn").click(function() {
		var paramData = {
			name : $("#name").val(),
			key : $("#key").val(),
			limit : grid.toolbars[1].pageSize
		};
		grid.getStore().load({
			params : paramData
		});
	});
});