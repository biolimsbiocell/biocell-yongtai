$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'type',
		type : "string"
	});
	fields.push({
		name : 'userIds',
		type : "string"
	});
	fields.push({
		name : 'userNames',
		type : "string"
	});
	fields.push({
		name : 'formNames',
		type : "string"
	});
	fields.push({
		name : 'groupIds',
		type : "string"
	});
	fields.push({
		name : 'groupNames',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 120
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.designation,
		width : 200
	});
	cm.push({
		dataIndex : 'type',
		header : biolims.common.type,
		width : 150
	});
	cm.push({
		dataIndex : 'userIds',
		header : biolims.common.userIds,
		width : 100,
		hidden : true,
		editor :userNames 
	});

	var userNames = new Ext.form.TextField({
		allowBlank : true
	});
	userNames.on('focus', function() {
		var record = grid.getSelectRecord()[0];
		var type = record.get("type");
		if (type != 'userTask') {
			message(biolims.common.setTiShi);
			return;
		}
		showUserSelectList();

	});

	cm.push({
		dataIndex : 'userNames',
		header : biolims.user.id,
		width : 150,
		editor : userNames
	});
	cm.push({
		dataIndex : 'groupIds',
		header : biolims.common.userGroup+' ID',
		width : 100,
		hidden : true,
		editor : groupName
	});

	var groupName = new Ext.form.TextField({
		allowBlank : true
	});
	groupName.on('focus', function() {
		var record = grid.getSelectRecord()[0];
		var type = record.get("type");
		if (type != 'userTask') {
			message(biolims.common.setTiShi);
			return;
		}
		showUserGroup();
	});
	cm.push({
		dataIndex : 'groupNames',
		header : biolims.common.userGroup,
		width : 150,
		editor : groupName
	});
	
	var formNames = new Ext.form.TextField({
		allowBlank : true
	});
	
	cm.push({
		dataIndex : 'formNames',
		header : biolims.master.relatedTableId,
		width : 150,
		editor : formNames
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/processdefinition/processDefinitionActivitisJson.action?definitionId="
			+ $("#definition_id").val();
	var opts = {};
	// opts.title = "流程定义配置人员";
	opts.id = "configUserGrid";
	opts.height = document.documentElement.clientHeight - 40;
	opts.tbar = [];

	opts.tbar.push({
		text : biolims.common.save,
		handler : function() {
			var param = {};
			var datas = [];
			var modifyRecord = grid.getAllRecord();
			if (modifyRecord) {
				$.each(modifyRecord, function(i, obj) {
					if (obj.get("userIds") || obj.get("groupIds")|| obj.get("formNames")) {
						var data = {};
						data.id = obj.get("id");
						data.userIds = obj.get("userIds");
						data.userNames = obj.get("userNames");
						data.groupIds = obj.get("groupIds");
						data.groupNames = obj.get("groupNames");
						data.formNames =  obj.get("formNames");
						datas.push(data);
					}
				});
			}

			if (datas.length > 0) {
				param.data = datas;
				param.definitionId = $("#definition_id").val();
				ajax("post", "/workflow/processdefinition/saveDefinitionActivitisConfigUser.action", {
					data : JSON.stringify(param),
				}, function(data) {
					if (data.success) {
						message(biolims.common.saveSuccess);
						grid.getStore().reload();
					} else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.setUpTaskRules,
		handler : function() {
			var records = grid.getSelectRecord();
			if (!records || records.length <= 0) {
				message(biolims.common.pleaseSelectNodeData);
				return;
			}

			var options = {};
			options.width = 289;
			options.height = 234;
			options.data = {};
			options.data.definitionId = $("#definition_id").val();
			options.data.activitiId = records[0].get("id");

			loadDialogPageNo(null, biolims.common.setUpTaskRules, "/workflow/processdefinition/showVoteRule.action", {
				"Confirm" : function() {
					var datas = {};
					datas.oneVote = $("input[name=oneVote]:checked").val();
					datas.voteMethod = $("input[name=voteMethod]:checked").val();
					datas.voteType = $("input[name=voteType]:checked").val();
					datas.voteNum = $("#voteNum").val();
					datas.oneVoteUser = $("#oneVoteUser").val();
					datas.id = $("#voteRuleId").val();
					datas.definitionId = $("#definitionId").val();
					datas.activitiId = $("#activitiId").val();
					ajax("post", "/workflow/processdefinition/saveVoteRule.action", datas, function(data) {
						if (data.success) {
							message(biolims.common.saveSuccess);
							grid.getStore().reload();
						} else {
							message(biolims.common.saveFailed);
						}
					}, null);
					$(this).dialog("close");
				}
			}, true, options);

		}
	});

	opts.tbar.push({
		text : biolims.common.setTheOperationButton,
		handler : function() {
			var records = grid.getSelectRecord();
			if (!records || records.length <= 0) {
				message(biolims.common.pleaseSelectNodeData);
				return;
			}

			var options = {};
			options.width = 480;
			options.height = 400;
			options.data = {};
			options.data.activitiId = records[0].get("id");

			loadDialogPageNo(null,biolims.common.setTheOperationButton, "/workflow/processdefinition/showOperButton.action", null, true, options);
		}
	});
	function showUserSelectList(){
			var win = Ext.getCmp('showUserSelectListFun');
			if (win) {
				win.close();
			}
			var showUserSelectListFun = new Ext.Window(
					{
						id : 'showUserSelectListFun',
						modal : true,
						title : biolims.master.selectOper,
						layout : 'fit',
						width : document.body.clientWidth / 1.3,
						height : document.body.clientHeight / 1.5,
						closeAction : 'close',
						plain : true,
						bodyStyle : 'padding:5px;',
						buttonAlign : 'center',
						collapsible : true,
						maximizable : true,
						items : new Ext.BoxComponent(
								{
									id : 'maincontent',
									region : 'center',
									html : "<iframe scrolling='no' name='maincontentframe' src='"
											+ ctx
											+ "/core/user/showUserListAllSelect.action' frameborder='0' width='100%' height='100%' ></iframe>"
								}),
						buttons : [ {
							text : biolims.common.close,
							handler : function() {
								showUserSelectListFun.close();
							}
						} ]
					});
			showUserSelectListFun.show();
		}


	
	function showUserGroup(){
			var win = Ext.getCmp('showUserGroupFun');
			if (win) {win.close();}
			var showUserGroupFun= new Ext.Window({
			id:'showUserGroupFun',modal:true,title:biolims.common.selectionWorkingGroup,layout:'fit',width:document.body.clientWidth/2,height:document.body.clientHeight/1.5,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/userGroup/userGroupSelect.action?flag=showUserGroupFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: biolims.common.close,
			 handler: function(){
			 showUserGroupFun.close(); }  }]  });     showUserGroupFun.show(); 
		}
	opts.tbar.push({
		text : biolims.common.returnTheListOfDefinitions,
		handler : function() {
			window.location = window.ctx + "/workflow/processdefinition/showProcessDefinitionList.action";

		}
	});

	var grid = gridEditTable("user_set_div", cols, loadParam, opts, {
		isShowDefaultTbar : false
	});
	$("#user_set_div").data("userGrid", grid);
});
function addUserGrid(record) {
	var expPersToGrid = $("#user_set_div").data("userGrid");
	var selRecords = expPersToGrid.getSelectionModel().getSelections(); 
	var selRecord=selRecords[0];
	
	
    var userIds = "";
    var userNames = "";
	$.each(record, function(i, obj) {
		userIds += obj.get("id")+",";
	    userNames += obj.get("name")+",";
	});
	userIds = userIds.substring(0,userIds.length-1);
	userNames = userNames.substring(0,userNames.length-1);
	selRecord.set("userIds",userIds);
	selRecord.set("userNames",userNames);
	var win = Ext.getCmp('showUserSelectListFun')
	if(win){win.close();}
}
function setshowUserGroupFun(rec){
	var expPersToGrid = $('#user_set_div').data('userGrid');
	var selRecords = expPersToGrid.getSelectionModel().getSelections(); 
	var selRecord=selRecords[0];
	selRecord.set('groupIds',rec.get('id'));
	selRecord.set('groupNames',rec.get('name'));
	
	var win = Ext.getCmp('showUserGroupFun')
	if(win){win.close();}
	}