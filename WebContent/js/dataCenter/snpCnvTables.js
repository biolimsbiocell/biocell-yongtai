/* 
* 文件名称 :
* 创建者 : 郭恒开
* 创建日期: 2018/06/05
* 文件描述: snp/cnv和拷贝数分析的详细页面
* 
*/

$(function () {
	var myChart1 = echarts.init(document.getElementById("snp"));
	renderSnp(myChart1);
	var myChart2 = echarts.init(document.getElementById("cnv"));
	renderCnv(myChart2);
	var myChart3 = echarts.init(document.getElementById("geneReload"));
	rendergeneReload(myChart3);
	var myChart4 = echarts.init(document.getElementById("muta"));
	renderMuta(myChart4);
})
// snp
function renderSnp(myChart1) {
	myChart1.setOption({
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['pie', 'funnel'],
					option: {
						funnel: {
							x: '25%',
							width: '50%',
							funnelAlign: 'left',
							max: 1548
						}
					}
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		calculable: true,
		series: [{
			name: 'SNP',
			type: 'pie',
			radius: '55%',
			center: ['50%', '60%'],
			data: [{
					value: 50,
					name: '错义'
				},
				{
					value: 40,
					name: '无义'
				},
				{
					value: 36,
					name: '移位'
				},
				{
					value: 30,
					name: '插入'
				},
				{
					value: 29,
					name: '删除'
				},
				{
					value: 24,
					name: '缺失'
				},
			]
		}]
	})
}
// cnv
function renderCnv(myChart1) {
	myChart1.setOption({
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['pie', 'funnel'],
					option: {
						funnel: {
							x: '25%',
							width: '50%',
							funnelAlign: 'left',
							max: 1548
						}
					}
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		calculable: true,
		series: [{
			name: 'CNV',
			type: 'pie',
			radius: '55%',
			center: ['50%', '60%'],
			data: [{
					value: 23,
					name: '基因扩增'
				},
				{
					value: 64,
					name: '单拷贝数缺失'
				},
				{
					value: 88,
					name: '多拷贝数缺失'
				},
			]
		}]
	})
}
// 基因重排
function rendergeneReload(myChart1) {
	myChart1.setOption({
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['pie', 'funnel'],
					option: {
						funnel: {
							x: '25%',
							width: '50%',
							funnelAlign: 'left',
							max: 1548
						}
					}
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		calculable: true,
		series: [{
			name: '突变效应',
			type: 'pie',
			radius: '55%',
			center: ['50%', '60%'],
			data: [{
					value: 55,
					name: '基因扩增'
				},
				{
					value: 40,
					name: '单拷贝缺失'
				},
				{
					value: 10,
					name: '多拷贝缺失'
				},
			]
		}]
	})
}
// 突变效应
function renderMuta(myChart1) {
	myChart1.setOption({
		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['pie', 'funnel'],
					option: {
						funnel: {
							x: '25%',
							width: '50%',
							funnelAlign: 'left',
							max: 1548
						}
					}
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		calculable: true,
		series: [{
			name: '突变效应',
			type: 'pie',
			radius: '55%',
			center: ['50%', '60%'],
			data: [{
					value: 32,
					name: '激活突变'
				},
				{
					value: 80,
					name: '抑制突变'
				},
				{
					value: 3,
					name: '未知'
				},
				{
					value: 11,
					name: '皆可'
				},
			]
		}]
	})
}