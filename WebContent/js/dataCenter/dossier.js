/* 
 * 文件名称 :dossier.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/06/01
 * 文件描述: 病历夹
 * 
 */

$(function() {
	var id = $("#id").val();
	if($("#handlemethod").val() == "modify") {
		$("#id").prop("readonly", "readonly");
	}
	// 点击上一步下一步切换
	stepViewChange();
	//layui 的 form 模块是必须的
	layui.use(['form'], function() {
		var form = layui.form;
		var right=[
     {
         title: "张笠家族", value: "jd1", data: [
         { title: "张笠之父", data: [
         { title: "ue201874订单", data: [
         	 { title: "血液样本", data: [
         	 	 { title: "样本接收", data: [] }
        , { title: "样本处理", data: [] }
        , { title: "核酸提取", data: [] }
        , { title: "文库混合", data: [] }
         	 ] }
        , { title: "脑髓样本", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "血浆样本", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "白细胞样本", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "uu2814订单", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "wq7888订单", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "ppew3订单", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        , { title: "张笠之母", value: "jd1.2",data: [
         { title: "订单1", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单2", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单3", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单4", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        , { title: "张笠之子", value: "jd1.3",data: [
         { title: "订单1", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单2", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单3", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单4", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        , { title: "张笠之姑", value: "jd1.4", data: [
         { title: "订单1", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单2", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单3", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单4", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        ]
    }
  , 
  
     {
         title: "胡成学家族", value: "jd1", data: [
         { title: "病人1", data: [
         { title: "订单1", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单2", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单3", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单4", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        , { title: "病人2", value: "jd1.2",data: [
         { title: "订单1", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单2", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单3", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单4", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        , { title: "病人3", value: "jd1.3",data: [
         { title: "订单1", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单2", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单3", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单4", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        , { title: "病人4", value: "jd1.4", data: [
         { title: "订单1", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单2", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单3", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
        , { title: "订单4", data: [
         	 { title: "样本1", data: [
         	 	 { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }
         	 ] }
        , { title: "样本2", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本3", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
        , { title: "样本4", data: [ { title: "实验1", data: [] }
        , { title: "实验2", data: [] }
        , { title: "实验3", data: [] }
        , { title: "实验4", data: [] }] }
         ] }
         
         ] }
        ]
    }
  , 

]
		//获取权限配置的数据
//		$.ajax({
//			type: "post",
//			url: ctx + "/core/role/getRights.action",
//			data: {
//				roleId: id
//			},
//			success: function(data) {
//				var right = JSON.parse(data);
				xtree1 = new layuiXtree({
					elem: 'xtree1',
					form: form,
					data: right,
					isopen: false, //加载完毕后的展开状态，默认值：true
					ckall: false, //启用全选功能，默认值：false	
					ckallback: function() {}, //全选框状态改变后执行的回调函数
					icon: { //三种图标样式，更改几个都可以，用的是layui的图标
						//open: "&#xe7a0;", //节点打开的图标
						open: "&#xe674;", //节点打开的图标
						//close: "&#xe622;", //节点关闭的图标
						close: "&#xe64c;", //节点关闭的图标
						end: "&#xe621;" //末尾节点的图标
					},
					color: { //三种图标颜色，独立配色，更改几个都可以
						open: "#EE9A00", //节点图标打开的颜色
						close: "#EEC591", //节点图标关闭的颜色
						end: "#828282" //末级节点图标的颜色
					},
					click: function(data) { //节点选中状态改变事件监听，全选框有自己的监听事件
						$(".strong").removeClass("strong")
						data.othis.addClass("strong");
						$("#xtree2 .table").hide();
						var elem = data.elem;
						var title=elem.title;
						if(title.indexOf("家系")!=-1){
							$("#xtree2 .table").eq(0).show();
						}
						if(title.indexOf("病人")!=-1){
							$("#xtree2 .table").eq(1).show();
						}
						if(title.indexOf("订单")!=-1){
							$("#xtree2 .table").eq(2).show();
						}
						if(title.indexOf("样本")!=-1){
							$("#xtree2 .table").eq(3).show();
						}
						if(title.indexOf("实验")!=-1){
							$("#xtree2 .table").eq(4).show();
						}
						
					}
				});
				$("#xtree1").children(".animal").remove();
			//}
		//});
	});
})
