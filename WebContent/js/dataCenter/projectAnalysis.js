/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/23
 * 文件描述: 订单儿搜索
 * 
 */
$(function() {
	columnar();
	$("input[type='checkbox']").iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
	$(".snpAdnCnv").click(function () {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/analysis/data/dataTask/snpCnvTables.action";
	});
});

window.myChart = echarts.init(document.getElementById("sampleBar"));
// 柱状图
function columnar() {
	myChart.clear();
	myChart.setOption({
		title: {
			text: '柱状图'
		},
		toolbox: {
			saveAsImage: {
				show: true
			},
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		tooltip: {
			trigger: 'axis'
		},
		xAxis: {
			// type : 'category',
			axisLabel: { // X轴文字没有间距
				interval: 0
			},
			data: ['TP53', 'PIK3CA', 'KMT2D', 'FAT4', 'ARID1A', 'PTEN', 'MKT2C', 'APC', 'KRAS', 'BRAF']
		},
		yAxis: {
			type: 'value',
			axisLabel: {
				formatter: '{value}%'
			}
		},
		series: [{
			name: '总数',
			type: 'bar',
			itemStyle: {
				normal: {
					color: function(params) {
						// build a color map as your need.
						var colorList = [
							'#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B', '#FE8463', '#C1232B', '#B5C334', '#FCCE10', '#C1232B', '#B5C334', '#FCCE10'
						];
						return colorList[params.dataIndex]
					},
					label: {
						show: true,
						position: 'top',
						formatter: '{c}'
					}
				}
			},
			data: [55, 40, 36, 30, 29, 24, 18, 15, 15, 10]
			//      data.sort(function(a,b) {
			//      	return a-b;
			//      })
		}]
	});

}
// 折线
function lineChart() {
	myChart.clear();
	myChart.setOption({

		title: {
			text: '折线图'
		},
		tooltip: {},
		toolbox: {
			show: true,
			feature: {
				// mark: { show: true },//目前还不知道有啥用
				dataView: {
					show: true,
					readOnly: true
				}, //数据视图
				// magicType: { show: true, type: ['line', 'bar'] },//折线与柱状的切换
				restore: {
					show: true
				}, //重置
				saveAsImage: {
					show: true
				} //保存为图片
			}
		},
		legend: {
			data: ['数量']
		},
		xAxis: {
			axisLabel: { // X轴文字没有间距
				interval: 0
			},
			data: ['TP53', 'PIK3CA', 'KMT2D', 'FAT4', 'ARID1A', 'PTEN', 'MKT2C', 'APC', 'KRAS', 'BRAF'],
			axisLine: {
				lineStyle: {
					type: 'solid',
					color: '#003333', //左边线的颜色
					width: '2' //坐标线的宽度
				}
			}
		},
		yAxis: {
			axisLine: {
				lineStyle: {
					type: 'solid',
					color: '#003333',
					width: '2'
				}
			}
		},
		series: [{
			name: '数量',
			type: 'line',
			data: [55, 40, 36, 30, 29, 24, 18, 15, 15, 10],
			itemStyle: {
				normal: {
					color: '#6699FF',
					lineStyle: {
						color: '#CC6600'
					}
				}
			},
		}]
	});

}
// 饼状图
function Pancake() {
	myChart.clear();
	myChart.setOption({

		tooltip: {
			trigger: 'item',
			formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		legend: {
			orient: 'vertical',
			x: 'left',
			data: ['TP53', 'PIK3CA', 'KMT2D', 'FAT4', 'ARID1A', 'PTEN', 'MKT2C', 'APC', 'KRAS', 'BRAF']
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				magicType: {
					show: true,
					type: ['pie', 'funnel'],
					option: {
						funnel: {
							x: '25%',
							width: '50%',
							funnelAlign: 'left',
							max: 1548
						}
					}
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		calculable: true,
		series: [{
			name: '访问来源',
			type: 'pie',
			color: ['#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B', '#FE8463', '#B5C334', '#FCCE10', '#E87C25', '#27727B', '#FE8463'],
			radius: '55%',
			center: ['50%', '60%'],
			data: [{
					value: 50,
					name: 'TP53'
				},
				{
					value: 40,
					name: 'PIK3CA'
				},
				{
					value: 36,
					name: 'KMT2D'
				},
				{
					value: 30,
					name: 'FAT4'
				},
				{
					value: 29,
					name: 'ARID1A'
				},
				{
					value: 24,
					name: 'PTEN'
				},
				{
					value: 18,
					name: 'MKT2C'
				},
				{
					value: 15,
					name: 'APC'
				},
				{
					value: 15,
					name: 'KRAS'
				},
				{
					value: 10,
					name: 'BRAF'
				}
			]
		}]
	})
}