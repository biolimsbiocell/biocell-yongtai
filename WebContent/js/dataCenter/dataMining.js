/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/23
 * 文件描述: 订单儿搜索
 * 
 */
$(function() {
	columnar();
	$("input[type='checkbox']").iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
});
$("[aria-controls=tubian]").click(function() {
	window.myChart = echarts.init(document.getElementById("colu")); // 柱状
	window.myChart2 = echarts.init(document.getElementById("line")); // 折线
	columnarLine();
})

// 柱状图
function columnar() {
window.myChart = echarts.init(document.getElementById("columnar")); // 柱状
	window.myChart2 = echarts.init(document.getElementById("lineChart")); // 折线
//	myChart.clear();
//	myChart2.clear();
	myChart.setOption({
		title: {
			text: '柱状图'
		},
		toolbox: {
			saveAsImage: {
				show: true
			},
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		tooltip: {
			trigger: 'axis'
		},
		xAxis: {
			axisLabel: { // X轴文字没有间距
				interval: 0,
				interval:0,//横轴信息全部显示  
                rotate:-30,//-30度角倾斜显示  
			},
			// type : 'category',
			data: ['TP53', 'PIK3CA', 'KMT2D', 'FAT4', 'ARID1A', 'PTEN', 'MKT2C', 'APC', 'KRAS', 'BRAF']
		},
		yAxis: {
			type: 'value',
			axisLabel: {
				formatter: '{value}%'
			}
		},
		series: [{
			name: '总数',
			type: 'bar',
			itemStyle: {
				normal: {
					color: function(params) {
						// build a color map as your need.
						var colorList = [
							'#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B', '#FE8463', '#C1232B', '#B5C334', '#FCCE10', '#C1232B', '#B5C334', '#FCCE10'
						];
						return colorList[params.dataIndex]
					},
					label: {
						show: true,
						position: 'top',
						formatter: '{c}'
					}
				}
			},
			data: [55, 40, 36, 30, 29, 24, 18, 15, 15, 10]
			//      data.sort(function(a,b) {
			//      	return a-b;
			//      })
		}]
	});
	myChart2.setOption({
		title: {
			text: '折线图'
		},
		tooltip: {},
		toolbox: {
			show: true,
			feature: {
				// mark: { show: true },//目前还不知道有啥用
				dataView: {
					show: true,
					readOnly: true
				}, //数据视图
				// magicType: { show: true, type: ['line', 'bar'] },//折线与柱状的切换
				restore: {
					show: true
				}, //重置
				saveAsImage: {
					show: true
				} //保存为图片
			}
		},
		legend: {
			data: ['数量']
		},
		xAxis: {
			axisLabel: { // X轴文字没有间距
				interval: 0,
				interval:0,//横轴信息全部显示  
                rotate:-30,//-30度角倾斜显示  
			},
			data: ['TP53', 'PIK3CA', 'KMT2D', 'FAT4', 'ARID1A', 'PTEN', 'MKT2C', 'APC', 'KRAS', 'BRAF'],
			axisLine: {
				lineStyle: {
					type: 'solid',
					color: '#003333', //左边线的颜色
					width: '2' //坐标线的宽度
				}
			}
		},
		yAxis: {
			axisLine: {
				lineStyle: {
					type: 'solid',
					color: '#003333',
					width: '2'
				}
			}
		},
		series: [{
			name: '数量',
			type: 'line',
			data: [55, 40, 36, 30, 29, 24, 18, 15, 15, 10],
			itemStyle: {
				normal: {
					color: '#6699FF',
					lineStyle: {
						color: '#CC6600'
					}
				}
			}
		}]
	});
}
function columnarLine() {
window.myChart = echarts.init(document.getElementById("colu")); // 柱状
	window.myChart2 = echarts.init(document.getElementById("line")); // 折线
//	myChart.clear();
//	myChart2.clear();
	myChart.setOption({
		title: {
			text: '柱状图'
		},
		toolbox: {
			saveAsImage: {
				show: true
			},
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		tooltip: {
			trigger: 'axis'
		},
		xAxis: {
			axisLabel: { // X轴文字没有间距
				interval: 0,
				interval:0,//横轴信息全部显示  
                rotate:-30,//-30度角倾斜显示  
			},
			// type : 'category',
			data: ['TP53', 'PIK3CA', 'KMT2D', 'FAT4', 'ARID1A', 'PTEN', 'MKT2C', 'APC', 'KRAS', 'BRAF']
		},
		yAxis: {
			type: 'value',
			axisLabel: {
				formatter: '{value}%'
			}
		},
		series: [{
			name: '总数',
			type: 'bar',
			itemStyle: {
				normal: {
					color: function(params) {
						// build a color map as your need.
						var colorList = [
							'#C1232B', '#B5C334', '#FCCE10', '#E87C25', '#27727B', '#FE8463', '#C1232B', '#B5C334', '#FCCE10', '#C1232B', '#B5C334', '#FCCE10'
						];
						return colorList[params.dataIndex]
					},
					label: {
						show: true,
						position: 'top',
						formatter: '{c}'
					}
				}
			},
			data: [55, 40, 36, 30, 29, 24, 18, 15, 15, 10]
			//      data.sort(function(a,b) {
			//      	return a-b;
			//      })
		}]
	});
	myChart2.setOption({
		title: {
			text: '折线图'
		},
		tooltip: {},
		toolbox: {
			show: true,
			feature: {
				// mark: { show: true },//目前还不知道有啥用
				dataView: {
					show: true,
					readOnly: true
				}, //数据视图
				// magicType: { show: true, type: ['line', 'bar'] },//折线与柱状的切换
				restore: {
					show: true
				}, //重置
				saveAsImage: {
					show: true
				} //保存为图片
			}
		},
		legend: {
			data: ['数量']
		},
		xAxis: {
			axisLabel: { // X轴文字没有间距
				interval: 0,
				interval:0,//横轴信息全部显示  
                rotate:-30,//-30度角倾斜显示  
			},
			data: ['TP53', 'PIK3CA', 'KMT2D', 'FAT4', 'ARID1A', 'PTEN', 'MKT2C', 'APC', 'KRAS', 'BRAF'],
			axisLine: {
				lineStyle: {
					type: 'solid',
					color: '#003333', //左边线的颜色
					width: '2' //坐标线的宽度
				}
			}
		},
		yAxis: {
			axisLine: {
				lineStyle: {
					type: 'solid',
					color: '#003333',
					width: '2'
				}
			}
		},
		series: [{
			name: '数量',
			type: 'line',
			data: [55, 40, 36, 30, 29, 24, 18, 15, 15, 10],
			itemStyle: {
				normal: {
					color: '#6699FF',
					lineStyle: {
						color: '#CC6600'
					}
				}
			}
		}]
	});
}