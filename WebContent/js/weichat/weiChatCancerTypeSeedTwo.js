var weiChatCancerTypeSeedTwoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'cancerTypeName',
		type:"string"
	});
	    fields.push({
		name:'weiChatCancerType-id',
		type:"string"
	});
	    fields.push({
		name:'weiChatCancerType-cancerTypeName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cancerTypeName',
		hidden : false,
		header:biolims.sample.cancerTypeName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'weiChatCancerType-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'weiChatCancerType-cancerTypeName',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/weichat/weiChatCancerType/showWeiChatCancerTypeSeedTwoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.tumorChildTableTwo;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/weichat/weiChatCancerType/delWeiChatCancerTypeSeedTwo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//			text : '选择相关主表',
//				handler : selectcancerTypeIdDialogFun
//		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = weiChatCancerTypeSeedTwoGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
							weiChatCancerTypeSeedTwoGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	weiChatCancerTypeSeedTwoGrid=gridEditTable("weiChatCancerTypeSeedTwodiv",cols,loadParam,opts);
	$("#weiChatCancerTypeSeedTwodiv").data("weiChatCancerTypeSeedTwoGrid", weiChatCancerTypeSeedTwoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectcancerTypeIdFun(){
	var win = Ext.getCmp('selectcancerTypeId');
	if (win) {win.close();}
	var selectcancerTypeId= new Ext.Window({
	id:'selectcancerTypeId',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectcancerTypeId.close(); }  }]  }) });  
    selectcancerTypeId.show(); }
	function setcancerTypeId(rec){
		var gridGrid = $("#weiChatCancerTypeSeedTwodiv").data("weiChatCancerTypeSeedTwoGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('cancerTypeId-id',rec.get('id'));
			obj.set('cancerTypeId-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectcancerTypeId')
		if(win){
			win.close();
		}
	}
	function selectcancerTypeIdDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/WeiChatCancerTypeSelect.action?flag=cancerTypeId";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selcancerTypeIdVal(this);
				}
			}, true, option);
		}
	var selcancerTypeIdVal = function(win) {
		var operGrid = weiChatCancerTypeDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#weiChatCancerTypeSeedTwodiv").data("weiChatCancerTypeSeedTwoGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('cancerTypeId-id',rec.get('id'));
				obj.set('cancerTypeId-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
