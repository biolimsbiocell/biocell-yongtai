var weiChatCancerTypeDialogOneGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'cancerTypeName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'cancerTypeName',
		header:biolims.common.designation,
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	//alert($("#code").val());
	loadParam.url=ctx+"/sample/sampleCancerTemp/weiChatCancerTypeSelectOneJson.action?id="+$("#code").val();
	var opts={};
	opts.title=biolims.sample.tumorChildTableOne;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setWeiChatCancerTypeOneFun(rec);
	};
	weiChatCancerTypeDialogOneGrid=gridTable("show_dialog_weiChatCancerTypeOne_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(weiChatCancerTypeDialogOneGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
