var weiChatCancerTypeGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'cancerTypeName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'cancerTypeName',
		header:biolims.common.designation,
		width:50*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/weichat/weiChatCancerType/showWeiChatCancerTypeListJson.action";
	var opts={};
	opts.title=biolims.sample.dicTypeName;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	weiChatCancerTypeGrid=gridTable("show_weiChatCancerType_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/weichat/weiChatCancerType/editWeiChatCancerType.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/weichat/weiChatCancerType/editWeiChatCancerType.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/weichat/weiChatCancerType/viewWeiChatCancerType.action?id=' + id;
}
function exportexcel() {
	weiChatCancerTypeGrid.title = biolims.common.exportList;
	var vExportContent = weiChatCancerTypeGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(weiChatCancerTypeGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
