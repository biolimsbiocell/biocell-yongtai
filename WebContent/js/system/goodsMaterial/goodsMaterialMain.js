var goodsMaterialMainGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'product-id',
		type:"string"
	});
	    fields.push({
		name:'product-name',
		type:"string"
	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'product-id',
		hidden:true,
		header:biolims.wk.productId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'product-name',
		header:biolims.wk.productName,
		
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'num',
		header:biolims.common.count,
		width:20*6,
		
		sortable:true
	});
	var statestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.master.valid ], [ '0', biolims.master.invalid]]
	});
	
	var stateComboxFun = new Ext.form.ComboBox({
		store : statestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(stateComboxFun),				
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/goodsMaterial/goodsMaterialMain/showGoodsMaterialMainListJson.action";
	var opts={};
	opts.title=biolims.master.materialInfo;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	goodsMaterialMainGrid=gridTable("show_goodsMaterialMain_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/system/goodsMaterial/goodsMaterialMain/editGoodsMaterialMain.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/goodsMaterial/goodsMaterialMain/editGoodsMaterialMain.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/goodsMaterial/goodsMaterialMain/viewGoodsMaterialMain.action?id=' + id;
}
function exportexcel() {
	goodsMaterialMainGrid.title = biolims.common.exportList;
	var vExportContent = goodsMaterialMainGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(goodsMaterialMainGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
