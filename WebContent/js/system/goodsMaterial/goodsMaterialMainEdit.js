﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/system/goodsMaterial/goodsMaterialMain/editGoodsMaterialMain.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/system/goodsMaterial/goodsMaterialMain/showGoodsMaterialMainList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#goodsMaterialMain", {
					userId : userId,
					userName : userName,
					formId : $("#goodsMaterialMain_id").val(),
					title : $("#goodsMaterialMain_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#goodsMaterialMain_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/system/goodsMaterial/goodsMaterialMain/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/system/goodsMaterial/goodsMaterialMain/copyGoodsMaterialMain.action?id=' + $("#goodsMaterialMain_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#goodsMaterialMain_id").val() + "&tableId=goodsMaterialMain");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#goodsMaterialMain_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#goodsMaterialMain_product").val());
	nsc.push(biolims.wk.productIdIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.master.materialInfo,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text : biolims.common.copy
						});
	item.on('click', editCopy);
	function ProductFun() {
		var win = Ext.getCmp('productFun');
		if (win) {
			win.close();
		}
		var productFun = new Ext.Window(
				{
					id : 'productFun',
					modal : true,
					title : biolims.pooling.selectBusinessType,
					layout : 'fit',
					width : 500,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text : biolims.common.close,
						handler : function() {
							productFun.close();
						}
					} ]
				});
		productFun.show();
	}
	function setProductFun(id, name) {
		var productName = "";
		ajax(
				"post",
				"/com/biolims/system/product/findProductToSample.action",
				{
					code : id,
				},
				function(data) {

					if (data.success) {
						$.each(data.data, function(i, obj) {
							productName += obj.name + ",";
						});
						document.getElementById("goodsMaterialMain_product").value = id;
						document.getElementById("goodsMaterialMain_product_name").value = productName;
					}
				}, null);
		var win = Ext.getCmp('productFun');
		if (win) {
			win.close();
		}
	}