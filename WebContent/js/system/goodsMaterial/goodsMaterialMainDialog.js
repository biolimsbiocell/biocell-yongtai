var goodsMaterialMainDialogGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'product-id',
		type:"string"
	});
	    fields.push({
		name:'product-name',
		type:"string"
	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'product-id',
		header:biolims.wk.productId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'product-name',
		header:biolims.wk.productName,
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'num',
		header:biolims.common.count,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/goodsMaterial/goodsMaterialMain/showGoodsMaterialMainListJson.action";
	var opts={};
	opts.title=biolims.master.materialInfo;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setGoodsMaterialMainFun(rec);
	};
	goodsMaterialMainDialogGrid=gridTable("show_dialog_goodsMaterialMain_div",cols,loadParam,opts);
	$("#show_dialog_goodsMaterialMain_div").data("goodsMaterialMainDialogGrid",goodsMaterialMainDialogGrid);
	
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(goodsMaterialMainDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
