/* 
* 文件名称 :cosTemplateDialogTable.js
* 创建者 : 郭恒开
* 创建日期: 2018/01/25
* 文件描述:选择实验页面设备的datatables
* 
*/
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.nextFlowId,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.nextFlow,
	});
	var tbarOpts = [];
	var cosOptions = table(false, null,
			'/system/nextFlow/nextFlow/shownextFlowDialogTableJson.action?model='+$("#model").val()+"&productId="+$("#productId").val()+"&sampleType="+$("#sampleTypeId").val(),colOpts , tbarOpts)
		var nextFlowTable = renderData($("#addNextFlow"), cosOptions);
	$("#addNextFlow").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addNextFlow_wrapper .dt-buttons").empty();
		$('#addNextFlow_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addNextFlow tbody tr");
	nextFlowTable.ajax.reload();
	nextFlowTable.on('draw', function() {
		trs = $("#addNextFlow tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

