$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/system/nextFlow/nextFlow/editNextFlow.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/system/nextFlow/nextFlow/showNextFlowList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#nextFlow", {
					userId : userId,
					userName : userName,
					formId : $("#nextFlow_id").val(),
					title : $("#nextFlow_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#nextFlow_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/system/nextFlow/nextFlow/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/system/nextFlow/nextFlow/copyNextFlow.action?id=' + $("#nextFlow_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#nextFlow_id").val() + "&tableId=nextFlow");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#nextFlow_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.nextFlow,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
	
	function selectnextStepNameTable() {
		var title = '';
		var url = '';
		title = biolims.common.selectRelevantTable;
		url = "/com/biolims/system/work/workOrder/showDialogApplicationTypeTableList.action";
		var option = {};
		option.width = 600;
		option.height = 600;
		loadDialogPage(null, title, url, {
			"Confirm" : function() {
				selVal(this);
				}
			}, true, option);
		
	}
	var selVal = function(win) {
	var operGrid = $("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid");
	var selRecords = operGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		document.getElementById('nextFlow_applicationTypeTable').value=obj.get("id");
		document.getElementById('nextFlow_applicationTypeTable_name').value=obj.get("name");
		$(win).dialog("close");
	});
	
	};
	function selectmainTable() {
		var title = '';
		var url = '';
		title = biolims.common.selectRelevantTable;
		url = "/com/biolims/system/work/workOrder/showDialogApplicationTypeTableList.action";
		var option = {};
		option.width = 600;
		option.height = 600;
		loadDialogPage(null, title, url, {
			"Confirm" : function() {
				selmainVal(this);
				}
			}, true, option);
		
	}
	var selmainVal = function(win) {
	var operGrid = $("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid");
	var selRecords = operGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		document.getElementById('nextFlow_mainTable').value=obj.get("id");
		document.getElementById('nextFlow_mainTable_name').value=obj.get("name");
		$(win).dialog("close");
	});
	
	};