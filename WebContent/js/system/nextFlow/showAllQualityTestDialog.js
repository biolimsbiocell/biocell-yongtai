$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编号",
	});
	colOpts.push({
		"data": "name",
		"title": "名称",
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/system/nextFlow/nextFlow/showAllQualityTestDialogListJson.action',colOpts , null)
		var qualityTestNext = renderData($("#qualityTestNext"), options);
	$("#qualityTestNext").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#qualityTestNext_wrapper .dt-buttons").empty();
		$('#qualityTestNext_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#qualityTestNext tbody tr");
	qualityTestNext.ajax.reload();
	qualityTestNext.on('draw', function() {
		trs = $("#qualityTestNext tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

