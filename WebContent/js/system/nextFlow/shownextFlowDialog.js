var shownextFlowDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'applicationTypeTable-id',
		type:"string"
	});
	    fields.push({
		name:'applicationTypeTable-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.nextFlow,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'applicationTypeTable-id',
		header:biolims.master.relatedTableId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'applicationTypeTable-name',
		header:biolims.master.relatedTableName,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		hidden : true,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		hidden:true,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态描述',
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*10,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var model=$("#model").val();
	var sequencingPlatform = $("#sequencingPlatform").val();
	var productId=$("#productId").val();
	var sampleTypeId=$("#sampleTypeId").val();
	
	var loadParam={};
	if(model!="null"&&productId!="null"&&sampleTypeId=="null"){
		loadParam.url=ctx+"/system/nextFlow/nextFlow/shownextFlowDialogJson.action?model="+model+"&productId="+productId;
	}else if(model=="null"&&productId=="null"&&sampleTypeId!="null"){
		loadParam.url=ctx+"/system/nextFlow/nextFlow/shownextFlowDialogBysampleTypeIdJson.action?sampleTypeId="+sampleTypeId;
	}else if(model=="null"&&productId=="null"&&sampleTypeId=="null"){
		loadParam.url=ctx+"/system/nextFlow/nextFlow/shownextFlowDialogAllJson.action";
	}else if(model!="null"&&productId=="null"&&sampleTypeId=="null"){
		loadParam.url=ctx+"/system/nextFlow/nextFlow/showPoolingTaskNextFlowJson.action?model="+model+"&sequencingPlatform="+sequencingPlatform;
	}else{
		loadParam.url=ctx+"/system/nextFlow/nextFlow/shownextFlowDialogAllJson.action";
		
	}
	
	var opts={};
	opts.title=biolims.common.nextFlow;
	
	opts.height=400;
	opts.width=450;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	var n=$("#n").val();
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setNextFlowFun(rec);
		if(n==2){
			setNextFlow2();
		}else if(n==3){
			setNextFlow3();
		}else if(n==4){
			setNextFlow4();
		}else{
			setNextFlow();
		}
		
	};
	shownextFlowDialogGrid=gridTable("show_dialog_nextFlow_div1",cols,loadParam,opts);
	$("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid", shownextFlowDialogGrid);
	
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "biolims.common.search", null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(shownextFlowDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
