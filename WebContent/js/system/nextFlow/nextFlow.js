var nextFlowGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'applicationTypeTable-id',
		type:"string"
	});
	    fields.push({
		name:'applicationTypeTable-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
			name:'sort',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.nextFlow,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'applicationTypeTable-id',
		hidden:true,
		header:biolims.master.relatedTableId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'applicationTypeTable-name',
		header:biolims.master.relatedTableName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
	var storeRp = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', "无效" ], [ '1', "有效"] ]
	});
	var rpCob = new Ext.form.ComboBox({
		store : storeRp,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:false,
		sortable:true,
		renderer : Ext.util.Format.comboRenderer(rpCob)
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sort',
		header:biolims.common.orderNumber,
		width:10*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/nextFlow/nextFlow/showNextFlowListJson.action";
	var opts={};
	opts.title=biolims.common.nextFlow;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	nextFlowGrid=gridTable("show_nextFlow_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/system/nextFlow/nextFlow/editNextFlow.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/nextFlow/nextFlow/editNextFlow.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/nextFlow/nextFlow/viewNextFlow.action?id=' + id;
}
function exportexcel() {
	nextFlowGrid.title = biolims.common.exportList;
	var vExportContent = nextFlowGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(nextFlowGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
