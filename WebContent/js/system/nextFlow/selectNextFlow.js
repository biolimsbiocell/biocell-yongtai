$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.nextFlowId,
	});
	colOpts.push({
		"data": "mainTable-id",
		"title": biolims.common.experimentModuleId,
	});
	colOpts.push({
		"data": "mainTable-name",
		"title": biolims.common.experimentModule,
	});
	colOpts.push({
		"data": "mainTable-enName",
		"title": biolims.common.experimentModule,
		"visible":false
	});
	colOpts.push({
		"data": "itemResultTable",
		"title": biolims.common.itemResultTable,
	});
	
	colOpts.push({
		"data": "name",
		"title": biolims.common.nextFlow,
	});
	
	colOpts.push({
		"data": "mainTable-moduleType",
		"title": "类型",
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/system/nextFlow/nextFlow/selectNextFlowJson.action',colOpts , tbarOpts)
		var nextFlowTable = renderData($("#addNextFlow"), options);
	$("#addNextFlow").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addNextFlow_wrapper .dt-buttons").empty();
		$('#addNextFlow_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addNextFlow tbody tr");
	nextFlowTable.ajax.reload();
	nextFlowTable.on('draw', function() {
		trs = $("#addNextFlow tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

