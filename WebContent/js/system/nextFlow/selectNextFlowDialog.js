$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "样本类型ID",
	});
	colOpts.push({
		"data": "name",
		"title": "样本类型",
	});
	colOpts.push({
		"data": "applicationTypeTable-id",
		"title": "表单ID",
	});
	colOpts.push({
		"data": "applicationTypeTable-name",
		"title": "表单名称",
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/system/nextFlow/nextFlow/selectNextFlowJson.action',colOpts , tbarOpts)
		var nextFlowTable = renderData($("#addNextFlow"), options);
	$("#addNextFlow").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addNextFlow_wrapper .dt-buttons").empty();
		$('#addNextFlow_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addNextFlow tbody tr");
	nextFlowTable.ajax.reload();
	nextFlowTable.on('draw', function() {
		trs = $("#addNextFlow tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

