var saveLocationDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'upId-id',
		type:"string"
	});
	    fields.push({
		name:'upId-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'upId-id',
		header:biolims.common.upIdId,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'upId-name',
		header:biolims.common.upId,
		width:20*10,
		sortable:true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/location/saveLocation/showSaveLocationListJson.action";
	var opts={};
	opts.title=biolims.master.LocationInfo;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSaveLocationFun(rec);
	};
	saveLocationDialogGrid=gridTable("show_dialog_saveLocation_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(saveLocationDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
