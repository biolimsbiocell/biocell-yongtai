var saveLocationGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'upId-id',
		type:"string"
	});
	    fields.push({
		name:'upId-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'upId-id',
		hidden:true,
		header:biolims.common.upIdId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'upId-name',
		header:biolims.common.upId,
		
		width:20*10,
		sortable:true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/location/saveLocation/showSaveLocationListJson.action";
	var opts={};
	opts.title=biolims.master.LocationInfo;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	saveLocationGrid=gridTable("show_saveLocation_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/system/location/saveLocation/editSaveLocation.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/location/saveLocation/editSaveLocation.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/location/saveLocation/viewSaveLocation.action?id=' + id;
}
function exportexcel() {
	saveLocationGrid.title = biolims.common.exportList;
	var vExportContent = saveLocationGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(saveLocationGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
