/*var qualityProductGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
			name:'experType-id',
			type:"string"
		});
		    fields.push({
			name:'experType-name',
			type:"string"
		});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'expectValue',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
			dataIndex:'testType-id',
			hidden:true,
			header:biolims.master.testTypeName,
			width:20*10,
			sortable:true
			});
			cm.push({
			dataIndex:'testType-name',
			header:biolims.master.testTypeName,
			
			width:20*10,
			sortable:true
			});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'expectValue',
		header:biolims.common.expectValue,
		width:20*6,
		
		sortable:true
	});
	var state = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.master.valid
			}, {
				id : '0',
				name : biolims.master.invalid
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(state),
//		editor: state
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/quality/qualityProduct/showQualityProductListJson.action";
	var opts={};
	opts.title=biolims.common.qualityProduct;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	qualityProductGrid=gridTable("show_qualityProduct_div",cols,loadParam,opts);
	$("#show_qualityProduct_div").data("qualityProductGrid", qualityProductGrid);
});
function add(){
		window.location=window.ctx+'/system/quality/qualityProduct/editQualityProduct.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/quality/qualityProduct/editQualityProduct.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/quality/qualityProduct/viewQualityProduct.action?id=' + id;
}
function exportexcel() {
	qualityProductGrid.title = biolims.common.exportList;
	var vExportContent = qualityProductGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(qualityProductGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
*/
var showQualityProductList;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": biolims.common.id,
		}, {
			"data": "name",
			"title": biolims.common.name,
		}, {
			"data": "createUser-id",
			"visible": false,
			"title": biolims.sample.createUserId,
		}, {
			"data": "createUser-name",
			"title": biolims.sample.createUserName,
		}, {
			"data": "experType-id",
			"visible": false,
			"title": biolims.master.testTypeId,
		}, {
			"data": "experType-name",
			"title": biolims.master.testTypeName,
		}, {
			"data": "createDate",
			"title": biolims.sample.createDate,
		}, {
			"data": "expectValue",
			"title": biolims.common.expectValue,
		}, {
			"data": "state",
			"title": biolims.common.state,
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return biolims.master.valid;
				}
				else if(data == "0") {
					return biolims.master.invalid;
				}else {
					return "";
				}
			}
		}];
	
		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "QualityProduct"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						};
//						colData.push(str);
					});
					
				}else{
					top.layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});
		
		
	var options = table(true, "",
			"/system/quality/qualityProduct/showQualityProductListJson.action",colData , null);
	sampleOrderTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleOrderTable);
	});
});


/*function changeState(that) {
var state = $(that).val();
if (state == "0e") {
	$("#supplier_state_id").val("不可用");
} else {
	$("#supplier_state_id").val("可用");
}
}*/
// 新建
function add() {
	window.location = window.ctx +
	'/system/quality/qualityProduct/editQualityProduct.action';
	
	
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
	'/system/quality/qualityProduct/editQualityProduct.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/quality/qualityProduct/viewQualityProduct.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "编码",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "创建人",
			"type": "input",
			"searchName": "createUser.name"
		},/*
		{

			"txt": "性别",
			"type": "select",
			"options": "请选择|男|女|未知",
			"changeOpt": "''|0|1|3",
			"searchName": "gender"
		},
		{
			"txt": "审核人",
			"type": "top.layer",
			"searchName": "confirmUser",
			"action": "confirmUser()"
		},*/
		{
			"txt": "创建时间(开始)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "创建时间(截止)",
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},/*
		{
			"txt": "审核时间(开始)",
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "审核时间(截止)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		},*/
		{
			"txt": "状态",
			"type": "select",
			"searchName": "state",
			"options":"请选择"+"|"+biolims.master.valid+"|"+biolims.master.invalid,
			"changeOpt":"''|1|0"
		},
		{
			"type": "table",
			"table": sampleOrderTable
		}
	];
}

function confirmUser() {
	top.layer.msg("这个是选的");
}
