var selectQualityGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'expectValue',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'num',
		header:biolims.common.count,
		width:10*6,
		sortable:true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'expectValue',
		header:biolims.common.expectValue,
		width:20*6,
		
		sortable:true
	});
	var state = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : biolims.master.invalid
			}, {
				id : '1',
				name : biolims.master.valid
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(state),
//		editor: state
	});
	/*cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:false,
		sortable:true
	});*/
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		hidden : true,
		sortable:true
	});
	/*cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/quality/qualityProduct/showSelectQualityListJson.action";
	var opts={};
	opts.title="质控品";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	selectQualityGrid=gridTable("selectQualitydiv",cols,loadParam,opts);*/
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/quality/qualityProduct/showSelectQualityListJson.action";
	var opts={};
	opts.title=biolims.common.qualityProduct;
	opts.height =  document.body.clientHeight-270;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	selectQualityGrid=gridEditTable("selectQualitydiv",cols,loadParam,opts);
})