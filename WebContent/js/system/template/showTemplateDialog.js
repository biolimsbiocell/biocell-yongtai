$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编码",
	});
	colOpts.push({
		"data": "name",
		"title": "名称",
	});
	colOpts.push({
		"data": "testType-id",
		"title": "类型",
		"visible":false
	});
	colOpts.push({
		"data": "testType-name",
		"title": "类型",
	});
	colOpts.push({
		"data": "versionNum",
		"title": "版本号",
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/system/template/template/showTemplateDialogListJson.action?type='+$("#type").val(), colOpts, null)
	var addTemplate = renderData($("#addTemplate"), options);
	$("#addTemplate").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addTemplate_wrapper .dt-buttons").empty();
			$('#addTemplate_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addTemplate tbody tr");
			addTemplate.ajax.reload();
			addTemplate.on('draw', function() {
				trs = $("#addTemplate tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})