var templateGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	fields.push({
		name : 'codeMain-id',
		type : "string"
	});
	fields.push({
		name : 'codeMain-name',
		type : "string"
	});
	fields.push({
		name : 'qcNum',
		type : "string"
	});
	fields.push({
		name : 'testType-id',
		type : "string"
	});
	fields.push({
		name : 'testType-name',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'storageContainer-id',
		type : "string"
	});
	fields.push({
		name : 'storageContainer-name',
		type : "string"
	});
	fields.push({
		name : 'fileInfo-id',
		type : "string"
	});
	fields.push({
		name : 'fileInfo-fileName',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'duringDays',
		type : "string"
	});
	fields.push({
		name : 'remindDays',
		type : "string"
	});
	fields.push({
		name : 'dicSampleType-id',
		type : "string"
	});
	fields.push({
		name : 'dicSampleType-name',
		type : "string"
	});
	fields.push({
		name : 'productNum',
		type : "string"
	});
	fields.push({
		name : 'acceptUser-id',
		type : "string"
	});
	fields.push({
		name : 'acceptUser-name',
		type : "string"
	});
	fields.push({
		name : 'templateFields',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 30 * 6,
		// hidden:true,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.designation,
		width : 30 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'sampleNum',
		header : biolims.common.sampleConsume,
		width : 10 * 6,
		hidden : false
	});
	cm.push({
		dataIndex : 'codeMain-id',
		header : biolims.master.codeMainId,
		width : 30 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'codeMain-name',
		header : biolims.master.codeMain,
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'qcNum',
		header : biolims.master.QCKind,
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'testType-id',
		hidden : true,
		header : biolims.master.testTypeName,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'testType-name',
		header : biolims.master.testTypeName,

		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		hidden : true,
		header : biolims.sample.createUserId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : biolims.sample.createUserName,

		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.sample.createDate,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'storageContainer-id',
		hidden : true,
		header : biolims.storage.containerId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'storageContainer-name',
		header : biolims.storage.container,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'fileInfo-id',
		hidden : true,
		header : biolims.common.templateId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'fileInfo-fileName',
		header : biolims.common.templateName,

		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'duringDays',
		header : biolims.master.duringDays,
		width : 10 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'remindDays',
		header : biolims.master.remindDays,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'dicSampleType-id',
		hidden : true,
		header : biolims.common.dicSampleTypeId,
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'dicSampleType-name',
		header : biolims.common.dicSampleTypeName,
		width : 10 * 10,
		sortable : true,
		hidden : false
	});
	cm.push({
		dataIndex : 'productNum',
		hidden : false,
		header : biolims.common.productNum,
		width : 20 * 5
	});
	cm.push({
		dataIndex : 'acceptUser-id',
		hidden : true,
		header : biolims.common.acceptUserId,
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'acceptUser-name',
		header : biolims.common.acceptUserName,
		width : 20 * 6,
		sortable : true,
		hidden : false
	});
	var storestateCob = new Ext.data.ArrayStore(
			{
				fields : [ 'id', 'name' ],
				data : [ [ '1', biolims.master.valid ],
						[ '0', biolims.master.invalid ] ]
			});
	var stateCob = new Ext.form.ComboBox({
		store : storestateCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'state',
		hidden : false,
		header : biolims.common.state,
		width : 20 * 6,
		editor : stateCob,
		renderer : Ext.util.Format.comboRenderer(stateCob)
	});
	// cm.push({
	// dataIndex:'stateName',
	// header:'工作流状态',
	// width:20*6,
	//		
	// sortable:true
	// });
	cm.push({
		dataIndex : 'templateFields',
		hidden : true,
		header : biolims.common.templateFields,
		sort : true,
		width : 20 * 6,
		
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/system/template/template/showTemplateListJson.action";
	var opts = {};
	opts.title = biolims.master.testTemplate;
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	templateGrid = gridTable("show_template_div", cols, loadParam, opts);
});
function add() {
	window.location = window.ctx
			+ '/system/template/template/editTemplate.action';
}
function edit() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/system/template/template/editTemplate.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/system/template/template/viewTemplate.action?id=' + id;
}
function exportexcel() {
	templateGrid.title = biolims.common.exportList;
	var vExportContent = templateGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(
			function() {
				var option = {};
				option.width = 542;
				option.height = 417;
				loadDialogPage($("#jstj"), biolims.common.search, null, {
					"开始检索(Start retrieve)" : function() {

						if (($("#startcreateDate").val() != undefined)
								&& ($("#startcreateDate").val() != '')) {
							var startcreateDatestr = ">=##@@##"
									+ $("#startcreateDate").val();
							$("#createDate1").val(startcreateDatestr);
						}
						if (($("#endcreateDate").val() != undefined)
								&& ($("#endcreateDate").val() != '')) {
							var endcreateDatestr = "<=##@@##"
									+ $("#endcreateDate").val();

							$("#createDate2").val(endcreateDatestr);

						}

						commonSearchAction(templateGrid);
						$(this).dialog("close");

					},
					"清空(Empty)" : function() {
						form_reset();

					}
				}, true, option);
			});
});
