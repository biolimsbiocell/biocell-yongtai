﻿
/* 
 * 文件名称 :templateEditStep1.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/02/08
 * 文件描述: 新建/编辑 模板第一步
 * 
 */
$(function() {
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "add"){
		$("#history_version_btn").show();
	} else {
		$("#history_version_btn").hide();
	}
	if(handlemethod == "view") {
		settextreadonly();
	}
	if(handlemethod == "modify") {
		$("#template_id").prop("readonly", "readonly");
	}
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'Template', $("#template_id").text());
})

//选择历史版本SOP
function choseHistoryVersionSop(){
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/template/template/showTemplateDialogList.action?type=0",""],
		yes: function(index, layero) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents()
				.find("#addTemplate .chosed").children("td").eq(0).text();
			$("#chose_template_id").val(id);
			$.ajax({
				type : 'post',
				url : ctx + '/system/template/template/getHistoryTemplate.action',
				data : {
					id : id
				},
				success : function (data) {
					var template = JSON.parse(data);
					if (template != null && template != "" && template != undefined){
						if (template.testType != null){
							$("#template_testType").val(template.testType.id);
							$("#template_testType_name").val(template.testType.name);
							$("#template_testType_sysCode").val(template.testType.sysCode);
						}
						if (template.storageContainer != null){
							$("#template_storageContainer").val(template.storageContainer.id);
							$("#template_storageContainer_name").val(template.storageContainer.name);
						}
						$("#template_qcNum").val(template.qcNum);
						if (template.codeMain != null){
							$("#template_codeMain").val(template.codeMain.id);
							$("#template_codeMain_name").val(template.codeMain.name);
						}
						$("#template_stepsNum").val(template.stepsNum);
						$("#template_sampleNum").val(template.sampleNum);
						$("#template_duringDays").val(template.duringDays);
						$("#template_remindDays").val(template.remindDays);
						$("#template_dicSampleTypeId").val(template.dicSampleTypeId);
						$("#template_dicSampleTypeName").val(template.dicSampleTypeName);
						$("#template_productNum1").val(template.productNum1);
						if (template.acceptUser != null){
							$("#template_acceptUser").val(template.acceptUser.id);
							$("#template_acceptUser_name").val(template.acceptUser.name);
						}
						if (template.dicSampleType != null){
							$("#template_dicSampleType").val(template.dicSampleType.id);
							$("#template_dicSampleType_name").val(template.dicSampleType.name);
						}
						$("#template_productNum").val(template.productNum);
						$("#template_templateFields").val(template.templateFields);
						$("#template_templateFieldsCode").val(template.templateFieldsCode);
						$("#template_templateFieldsItem").val(template.templateFieldsItem);
						$("#template_templateFieldsItemCode").val(template.templateFieldsItemCode);
						$("#template_state").val(template.state);
						$("#template_isBlend").val(template.isBlend);
						$("#template_isSeparate").val(template.isSeparate);
						
						$("#template_documentNum").val(template.documentNum);
						$("#template_documentName").val(template.documentName);
						$("#template_versionNum").val(template.versionNum);
					}
				}
			})
			top.layer.close(index);
		},
	});
}

//下一步
function nextTemplateStep() {
	var id = $("#template_id").text();
	if(id == "NEW") {
		top.layer.msg(biolims.common.pleaseSaveRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/template/template/templateEditStepTwoOld.action?id=" + id;
}
//实验类型
function sylxCheck() {
	top.layer.open({
		title: biolims.master.selectTestType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/nextFlow/nextFlow/selectNextFlow.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(2).text();
			var sys_code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(5).text();
			top.layer.close(index);
			$("#template_testType").val(id);
			$("#template_testType_name").val(name);
			$("#template_testType_sysCode").val(sys_code);
			$("#template_moduleType").val(type);
		},
	})
}
//容器类型
function containerCheck() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/container/showContainerTable.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#template_storageContainer").val(id)
			$("#template_storageContainer_name").val(name)
		},
	})
}
//选择条码模板
function selCode() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/syscode/codeMain/codeMainTabSelect.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCodeMain .chosed").children("td").eq(
				0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCodeMain .chosed").children("td").eq(1).text();
			top.layer.close(index)
			$("#template_codeMain").val(id)
			$("#template_codeMain_name").val(name)
		},
	})
}

//选择产物类型
function loadTestDicSampleType() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/sample/dicSampleType/selectDicSampleTypeOne.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td").eq(
				0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td").eq(1).text();
			top.layer.close(index)
			$("#template_dicSampleType").val(id)
			$("#template_dicSampleType_name").val(name)
		},
	})
}
//选择多个产物类型
function loadTestDicSampleType1() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/sample/dicSampleType/showDialogDicSampleTypeTable.action", ''],
		yes: function(index, layer) {
			var rows = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleTypeTable .selected");
			var sampleTypeId = "";
			var sampleTypeName = "";
			$.each(rows, function(j, k) {
				if(j < rows.length - 1) {
					sampleTypeId += ($(k).children("td").eq(
						1).text()) + ",";
					sampleTypeName += ($(k).children("td").eq(
						2).text()) + ",";
				} else {
					sampleTypeId += ($(k).children("td").eq(
						1).text());
					sampleTypeName += ($(k).children("td").eq(
						2).text());
				}
			});
			top.layer.close(index)
			$("#template_dicSampleTypeId").val(sampleTypeId)
			$("#template_dicSampleTypeName").val(sampleTypeName)
		},
	})
}
//选择实验结果
function showTemplateFields(that) {
	var sysCode = $("#template_testType_sysCode").val();
	if(!sysCode) {
		top.layer.msg(biolims.master.pleaseSelectTestType);
		return false;
	}
	if($(that).attr("chose") == "result") { //选择实验结果
		var taName = sysCode.split(",")[1];
		var txt = $("#template_templateFieldsItem");
		var hide = $("#template_templateFieldsItemCode");
	} else { //选择实验明细
		var taName = sysCode.split(",")[0];
		var txt = $("#template_templateFields");
		var hide = $("#template_templateFieldsCode");
	}
	var title = $(that).parents(".input-group").children(".input-group-addon").text();
	top.layer.open({
		title: title,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/template/template/chooseTemplateItem.action?taName=" + taName, ''],
		yes: function(index, layer) {
			var rows = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#templateItemChoose .selected");
			var columnComment = "";
			var columnName = "";
			$.each(rows, function(j, k) {
				if(j < rows.length - 1) {
					columnComment += ($(k).children("td").eq(
						3).text()) + ",";
					columnName += ($(k).children("td").eq(
						2).text()) + ",";
				} else {
					columnComment += ($(k).children("td").eq(
						3).text());
					columnName += ($(k).children("td").eq(
						2).text());
				}
			});
			txt.val(columnComment);
			hide.val(columnName);
			top.layer.close(index);
		},
	})

}
//选择实验组
function showacceptUser() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/userGroup/userGroupSelTable.action"],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#template_acceptUser").val(id);
			$("#template_acceptUser_name").val(name);
		},
	})
}
//清空多个产物类型
function dgmx1() {
	$("#template_dicSampleTypeName").val("");
	$("#template_dicSampleTypeId").val("");
}
// 清空单个个产物类型
function dgmx() {
	$("#template_dicSampleType_name").val("");
	$("#template_dicSampleType").val("");
}
//清空实验明细
function qkmx() {
	$("#template_templateFields").val("");
	$("#template_templateFieldsCode").val("");
}
// 清空实验结果
function qkjg() {
	$("#template_templateFieldsItem").val("");
	$("#template_templateFieldsItemCode").val("");
}
//上传sop文档
function fileUp() {
	if($("#template_id").text() == "NEW") {
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
//查看附件
function fileView() {
//	top.layer.open({
//		title: biolims.common.attachment,
//		type: 2,
//		skin: 'layui-top.layer-lan',
//		area: ["650px", "400px"],
//		content: window.ctx + "/operfile/initFileTable.action?flag=1&modelType=Template&id=" + $("#template_id").text(),
//		cancel: function(index, layer) {
//			top.layer.close(index)
//		}
//	})
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=Template&id=" + $("#template_id").text(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
//保存
function save() {
	//必填验证
	var requiredField = requiredFilter();
	if(!requiredField) {
		return false;
	}
	var handlemethod = $("#handlemethod").val();

	var changeLog = "检测模板 -";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	if(changeLog != "检测模板 -") {
		document.getElementById("changeLog").value = changeLog;
	}

	var choseId = $("#chose_template_id").val();
	if(handlemethod == "modify") {
		top.layer.load(4, {
			shade: 0.3
		});
		if (choseId != null && choseId != null && choseId!=""){
			$("#form1").attr("action", "/system/template/template/saveByHistory.action?choseId="+choseId);
		} else {
			$("#form1").attr("action", "/system/template/template/save.action");
		}
		$("#form1").submit();
		top.layer.closeAll();
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#template_id").text(),
				obj: 'Template'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					top.layer.load(4, {
						shade: 0.3
					});
					if (choseId != null && choseId != null){
						$("#form1").attr("action", "/system/template/template/saveByHistory.action?choseId="+choseId);
					} else {
						$("#form1").attr("action", "/system/template/template/save.action");
					}
					$("#form1").submit();
					top.layer.closeAll();
				}
			}
		});
	}
}

function list() {
//	window.location = window.ctx +
//		'/system/template/template/showTemplateTable.action';
	window.location = window.ctx +'/system/template/template/showTemplateTableOld.action?type=0';
}

function add() {
	window.location = window.ctx +
		"/system/template/template/editTemplateOld.action";
}