var templateTable;
$(function() {
	var cols=[]
	cols.push({
		"data": "id",
		"title": biolims.common.id,
	})
	cols.push({
		"data": "name",
		"title": biolims.user.eduName1,
	})
	cols.push({
		"data": "testType-name",
		"title": biolims.master.testTypeName,
	})
	cols.push({
		"data": "createUser-name",
		"title": biolims.tInstrument.createUser,
	})
	cols.push({
		"data": "createDate",
		"title": biolims.crmCustomer.createDate,
	})
	var options = table(true, "",
		"/system/template/template/showTemplateTableJson.action?type="+$("#type").val(), cols, null)
	templateTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(templateTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/system/template/template/editTemplateOld.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/template/template/editTemplateOld.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/template/template/viewTemplate.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.tInstrument.createUser,
			"type": "input",
			"searchName": "createUser"
		},
		
		{
			"txt": biolims.sampleOut.acceptUserName ,
			"type": "top.layer",
			"searchName": "confirmUser",
			"action": "confirmUser()"
		},
		{
			"txt": biolims.tStorage.createDate+"(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.tStorage.createDate+"(End)",
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		}/*,
		{
			"txt":biolims.common.confirmDateStart,
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.common.confirmDateEnd,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		},
		{
			"txt": biolims.common.state ,
			"type": "input",
			"searchName": "stateName",
		}*/
	,{
		"type":"table",
		"table":templateTable
	}];
}
function confirmUser() {
	top.layer.msg(biolims.common.pleaseChoose );
}
//恢复之前查询的状态
function recoverSearchContent() {
	var searchContent = sessionStorage.getItem("searchContent");
	if(!searchContent) {
		return false;
	}
	var flag = false;
	for(var k in searchContent) {
		if(searchContent[k]) {
			flag = true;
		}
	}
	if(flag) {
		var param = {
			query: searchContent
		};

		myTable.settings()[0].ajax.data = param;
		setTimeout(function  () {
			myTable.ajax.reload();
		}, 500);

	}
}
