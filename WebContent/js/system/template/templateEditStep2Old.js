﻿/* 
 * 文件名称 :templateEditStep2.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/02/08
 * 文件描述: 新建/编辑 模板第二步
 * 
 */
var id;
$(function() {
	id = $("#template_id").text().trim();
	//生成步骤
	var stepsNum = $("#stepsNum").val();
	renderSteps(stepsNum);
	//生成富文本编辑器
	myEditor = CKEDITOR.replace('editor1');
	//默认渲染第一个步骤数据
	renderTemplateData(id, "1");
})
//生成步骤
function renderSteps(stepsNum) {
	var stepsLi = "<li><a class='selected step'><span class='step_no'>1</span></a></li>";
	for(var i = 0; i < stepsNum; i++) {
		if(i > 0) {
			stepsLi += "<li><a class='disabled step'><span class='step_no'>" + (i + 1) + "</span></a></li>";
		}
	}
	$(".wizard_steps").append(stepsLi);
	//为每一个步骤注册点击事件
	$(".wizard_steps .step").click(function() {
		$(".wizard_steps .step").removeClass("selected").addClass("disabled");
		$(this).removeClass("disabled").addClass("selected");
		var orderNum = $(this).text();
		//更新明细、原辅料、设备
		renderTemplateData(id, orderNum);
		//更新自定义字段儿
		contentItemTable.ajax.url('/system/template/template/showContentTableJson.action?itemId=' + orderNum).load();
	});
}
//选择实验员
function showdutyManId(){
	top.layer.open({
	title:biolims.common.pleaseChoose,
	type:2,
	area:[document.body.clientWidth-300,document.body.clientHeight-100],
	btn: biolims.common.selected,
	content:[window.ctx+"/core/user/selectUserTable.action?groupId=admin",''],
	yes: function(index, layer) {
	var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
	var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
	top.layer.close(index);
	$("#test_User_Name").val(name);
	},
})
}

//渲染模板数据
function renderTemplateData(id, orderNum) {
	$.ajax({
		type: "post",
		data: {
			id: id,
			orderNum: orderNum
		},
		url: ctx + "/system/template/template/templateEditStepTwoJson.action",
		success: function(data) {
			var data = JSON.parse(data);
			var list = data.itemList;
			var reagentList = data.reagentList;
			var cosTypeList = data.cosTypeList;
			var zhijianList=data.zhijianList;
			//默认渲染第一个步骤
			//名称
			if(list.length) {
				$(".wizard_steps .selected").attr("stepid", list[0].id);
				$("#template_name").val(list[0].name);
				$("#template_code").val(list[0].code);
				$("#test_User_Name").val(list[0].testUserList);
				$("#estimated_time").val(list[0].estimatedTime);
				//编辑器代码
				myEditor.setData(list[0].note);
			} else {
				$(".wizard_steps .selected").attr("stepid", "")
				$("#template_name").val("");
				$("#template_code").val("");
				$("#test_User_Name").val("");
				$("#estimated_time").val("");
				//编辑器代码
				myEditor.setData("");
			}

			//质检
			var zhijianFlag = true;
			$("#zhijianBody .zhijianli").remove();
			if(!zhijianList.length){
				$("#zjType").text("");
				$("#zjType").attr("");
			}else{
				zhijianList.forEach(function(v, i) {
				if(v.itemId == orderNum) {
					zhijianFlag = false;
						$("#zjType").text(v.typeName);
						$("#zjType").attr("zjId",v.typeId);
					$("#reagentBody .nozhijian").slideUp();
					renderZhijian(v);
				}
			});
			}
			
			if(zhijianFlag) {
				$("#reagentBody .noReag").slideDown();
			}
			//原辅料
			var reagentFlag = true;
			$("#reagentBody .reagli").remove();
			reagentList.forEach(function(v, i) {
				if(v.itemId == orderNum) {
					reagentFlag = false;
					$("#reagentBody .noReag").slideUp();
					renderReagent(v);
				}
			});
			if(reagentFlag) {
				$("#reagentBody .noReag").slideDown();
			}
			//设备
			var cosFlag = true;
			$("#cosBody .cosli").remove();
			cosTypeList.forEach(function(v, i) {
				if(v.itemId == orderNum) {
					$("#cosBody .noCos").slideUp();
					cosFlag = false;
					renderCos(v);
				}
			});
			if(cosFlag) {
				$("#cosBody .noCos").slideDown();
			}
		}
	});
}
//渲染质检
function renderZhijian(zhijian) {
	var zhijianLis = "";
	var id = zhijian.id ? zhijian.id : "";
	var code = zhijian.code ? zhijian.code : "";
	var name = zhijian.name ? zhijian.name : "";
	var nextId = zhijian.nextId ? zhijian.nextId : "";
	var typeId = zhijian.typeId ? zhijian.typeId : "";
	var typeName = zhijian.typeName ? zhijian.typeName : "";
	zhijianLis += '<li class="zhijianli zhijianOld" id=' + id + '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span zhijianid=\'' + code + '\' class="zhijianName">' + name + '</span></small><i class="fa fa-trash pull-right"></i></li>'
	
	$("#zhijianBody").append(zhijianLis);
	//原辅料和设备删除操作
	reagentAndCosRemove();
}
//渲染设备
function renderReagent(reagent) {
	var reagentLis = "";
	var id = reagent.id ? reagent.id : "";
	var code = reagent.code ? reagent.code : "";
	var name = reagent.name ? reagent.name : "";
	var num = reagent.num ? reagent.num : " ";
	reagentLis += '<li class="reagli reagOld" id=' + id + '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">'+biolims.common.reagentName+':<span reagentid=' + code + ' class="reaName">' + name + '</span></small>&nbsp;&nbsp;单个用量：&nbsp;<input type="number" value=\''+num+'\'  placeholder="请输入" style="width:70px;" /><i class="fa fa-trash pull-right"></i></li>'
	$("#reagentBody").append(reagentLis);
	//原辅料和设备删除操作
	reagentAndCosRemove();
}
//渲染原辅料
function renderCos(cos) {
	var cosLis = "";
	var id = cos.id ? cos.id : "";
	var typeId = cos.type.id ? cos.type.id : "";
	var typeName = cos.type.name ? cos.type.name : "";
	
	cosLis += '<li class="cosli cosOld" id=' + id + '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">'+biolims.common.instrumentType+':<span cosid=' + typeId + ' class="cosName">' + typeName + '</span></small><i class="fa fa-trash pull-right"></i></li>'
	$("#cosBody").append(cosLis);
	//原辅料和设备删除操作
	reagentAndCosRemove();
}

//为步骤明细的编辑器设置动画
function animateEdit(that) {
	if($(that).hasClass("xxx")) {
		$(that).removeClass("xxx");
		$("#stepContentModer").animate({
			"height": "10%"
		}, 800, "swing", function() {
			$(this).animate({
				"width": "0%",
				"height": "0%",
			}, 600, "linear");
		});
	} else {
		$(that).addClass("xxx");
		$("#stepContentModer").animate({
			"width": "100%",
			"height": "10%"
		}, 500, "swing", function() {
			$(this).animate({
				"height": "90%"
			}, 600, "linear");
		});
	}
}

//选择质检类型
function choseType(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=zjType", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			$("#zjType").text(name);
			$("#zjType").attr("zjId",id);
			top.layer.close(index);
			
		},
	})
}

//选择检测项
function chosezhijian() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area:  ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/detecyion/sampleDeteyion/showSampledetecyion.action",''],
		yes: function(index, layer) {
			var reali='';
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .selected").each(function(i,v){
				reali += '<li class="zhijianli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span zhijianid=\''+$(v).children("td").eq(1).text()+'\' class="zhijianName">'+$(v).children("td").eq(2).text()+'</span></small><i class="fa fa-trash pull-right"></i></li>';
			})
				$("#zhijianBody").append(reali);
				$("#zhijianBody .nozhijian").slideUp();
				reagentAndCosRemove();
			top.layer.close(index);
			choseType();
		},
	})
	
}
//选择原辅料
function choseReagent() {
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/getStorage.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorage .chosed").children("td").eq(1).text();
			var code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorage .chosed").children("td").eq(0).text();
			var reali = '<li class="reagli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">原辅料名称:<span reagentid=' + code + ' class="reaName">' + name + '</span></small>&nbsp;&nbsp;单个用量：&nbsp;<input type="number" placeholder="请输入" style="width:70px;" /><i class="fa fa-trash pull-right"></i></li>';
			$("#reagentBody .noReag").slideUp();
			$("#reagentBody").append(reali);
			top.layer.close(index);
			reagentAndCosRemove();
		},
	})
}
//选择设备
function choseCos() {
	top.layer.open({
		title: biolims.common.selCosType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=device", ''],
		yes: function(index, layer) {
			var typeName = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var typeId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			var cosli = '<li class="cosli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">设备类型:<span cosid=' + typeId + ' class="cosName">' + typeName + '</span></small><i class="fa fa-trash pull-right"></i></li>'
			$("#cosBody .noCos").slideUp();
			$("#cosBody").append(cosli);
			top.layer.close(index);
			reagentAndCosRemove();
		},
	})
}
//质检和原辅料和设备删除操作
function reagentAndCosRemove() {
	$(".fa-trash").unbind("click").click(function() {
		var that = this;
		top.layer.confirm(biolims.common.confirm2Del,{icon: 3, title:biolims.common.prompt,
			btn:biolims.common.selected
		 }, function(index) {
			var li = $(that).parent("li");
			if(li.hasClass("zhijianOld")) {
				var id = li.attr("id").split(",");
				$.ajax({
					type: "post",
					url: ctx + "/system/template/template/delZhijianItem.action",
					data: {
						ids: id,
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							li.remove();
							top.layer.msg(biolims.common.deleteSuccess);
							if(!$("#zhijianBody .zhijianli").length) {
								$("#zhijianBody .nozhijian").slideDown();
							}
						}

					}
				});
			}else if(li.hasClass("reagOld")) {
				var id = li.attr("id").split(",");
				$.ajax({
					type: "post",
					url: ctx + "/system/template/template/delReagentItem.action",
					data: {
						ids: id,
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							li.remove();
							top.layer.msg(biolims.common.deleteSuccess);
							if(!$("#reagentBody .reagli").length) {
								$("#reagentBody .noReag").slideDown();
							}
						}

					}
				});
			} else if(li.hasClass("cosOld")) {
				var id = li.attr("id").split(",");
				$.ajax({
					type: "post",
					url: ctx + "/system/template/template/delCosItem.action",
					data: {
						ids: id,
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							li.remove();
							top.layer.msg(biolims.common.deleteSuccess);
							if(!$("#cosBody .cosli").length) {
								$("#cosBody .noCos").slideDown();
							}
						}
					}
				});
			} else {
				var noitem = $(that).parents(".todo-list").children("li");
				li.remove();
				if(noitem.length == "2") {
					noitem.slideDown();
				}
			}
			top.layer.close(index);
		});

	});
}
//模板步骤的删除
function removeStepItem() {
	if($(".wizard_steps").children("li").length=="1"){
		top.layer.msg(biolims.common.stepsNo);
		return false;
	}
	top.layer.confirm(biolims.common.confirm2Del,  {icon: 3, title:biolims.common.prompt,
		btn:biolims.common.selected
	 },function(index) {
		$.ajax({
			type: "post",
			data: {
				id: id,
				itemId: $(".wizard_steps .selected").attr("stepid"),
			},
			url:ctx+"/system/template/template/delTemplateItem.action",
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					window.location.reload();
				}
			}
		});
		top.layer.close(index);
	});

}
//保存数据
function saveStepItem() {
	//拼自定义字段儿
	var contentData = saveTemTablejson($("#contentItem"));
	//拼质检的数据
	var zhijianli = $("#zhijianBody .zhijianli");
	var zhijian = [];
	zhijianli.each(function(i, val) {
		var zhijianItem = {};
		zhijianItem.typeId = $("#zjType").attr("zjId");
		zhijianItem.typeName = $("#zjType").text();
		zhijianItem.id = val.id;
		zhijianItem.name = $(val).find(".zhijianName").text();
		zhijianItem.code = $(val).find(".zhijianName").attr('zhijianid');
		zhijian.push(zhijianItem);
	});
	//拼原辅料的数据
	var reagentli = $("#reagentBody .reagli");
	var reagent = [];
	reagentli.each(function(i, val) {
		var reagentItem = {};
		reagentItem.id = val.id;
		reagentItem.name = $(val).find(".reaName").text();
		reagentItem.code = $(val).find(".reaName").attr('reagentid');
		reagentItem.num = $(val).children("input").val();
		reagent.push(reagentItem);
	});
	//拼设备的数据
	var cosli = $("#cosBody .cosli");
	var cos = [];
	cosli.each(function(i, val) {
		var cosItem = {};
		cosItem.id = val.id;
		cosItem.typeName = $(val).find(".cosName").text();
		cosItem.typeId = $(val).find(".cosName").attr('cosid');
		cos.push(cosItem);
	});

	template = {
		id: $(".wizard_steps .selected").attr("stepid"),
		code: $("#template_code").val(),
		name: $("#template_name").val(),
		note: myEditor.getData(),
		content: contentData,
		testUserList:$("#test_User_Name").val(),
		estimatedTime:$("#estimated_time").val()
	};
	template = JSON.stringify(template);
	reagent = JSON.stringify(reagent);
	cos = JSON.stringify(cos);
	zhijian = JSON.stringify(zhijian);
	$.ajax({
		type: "post",
		url: ctx + "/system/template/template/saveItem.action",
		data: {
			id: id,
			orderNum: $(".wizard_steps .selected .step_no").text(),
			itemJson: template,
			reagentJson: reagent,
			cosJson: cos,
			zjianJson:zhijian
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				$(".wizard_steps .selected").click();
			}
		}
	});
}
//上一步
function preTemplateStep() {
	var id = $("#template_id").text();
	window.location = window.ctx +
		"/system/template/template/editTemplateOld.action?id=" + id;
}