var instrumentDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-id',
		type:"string"
	});
	    fields.push({
		name:'dutyUser-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
    fields.push({
		name:'spec',
		type:"string"
	});
    fields.push({
		name:'searchCode',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'outCode',
		type:"string"
	});
    fields.push({
		name:'accuracy',
		type:"string"
	});
    fields.push({
		name:'purchaseDate',
		type:"string"
	});
    fields.push({
		name:'surveyScope',
		type:"string"
	});
    fields.push({
		name:'checkItem',
		type:"string"
	});
    fields.push({
		name:'type-id',
		type:"string"
	});
    fields.push({
		name:'type-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.instrumentNo,
		width:12*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.instrumentName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'type-id',
		header:biolims.common.instrumentSortId,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'type-name',
		header:biolims.common.instrumentSortName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'spec',
		header:biolims.common.instrumentGroup,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'searchCode',
		header:biolims.common.searchCode,
		width:15*10,
		sortable:true,
		hidden : true
	});
	cm.push({
		dataIndex:'state-id',
		header:biolims.common.state,
		width:15*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'state-name',
		header:biolims.common.stateName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:15*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'dutyUser-id',
		header:biolims.master.personId,
		width:15*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dutyUser-name',
		header:biolims.master.personName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.startDate,
		width:15*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+'/equipment/main/showDialogInstrumentListJson.action?p_type='+$("#p_type").val();
	var opts={};
	opts.tbar =[];
	opts.title=biolims.common.instrument;
	opts.height=document.body.clientHeight-120;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		setPValue(rec);
	};
	opts.tbar.push({
		text : biolims.common.retrieve,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#jstj"), biolims.common.retrieve, null, {
				"Confirm": function() {
					commonSearchAction(instrumentDialogGrid);
					$(this).dialog("close");
				}
			}, true, options);
			
		}
	});
	instrumentDialogGrid=gridTable("show_dialog_instrument_div",cols,loadParam,opts);
	$("#show_dialog_instrument_div").data("instrumentDialogGrid", instrumentDialogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});