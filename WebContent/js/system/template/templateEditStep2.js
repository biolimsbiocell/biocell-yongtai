﻿/* 
 * 文件名称 :templateEditStep2.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/11
 * 文件描述: 新建/编辑 模板第二步
 * 
 */
var id;
//var defaultCaozuo='<li class="time-label" id="startCaozuo"><span class="bg-red">严格执行有关文件的操作规范 </span></li><li class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm btn-box-tool" onclick="choseReagent(this)"><i class="fa fa-search-plus"></i>添加原辅料</button><button class="btn btn-sm btn-box-tool" onclick="choseCos(this)"><i class="fa fa-search-plus"></i>添加设备类型</button><button class="btn btn-sm btn-box-tool" onclick="moreCaozuo (this)"><i class="fa fa-level-down"></i>操作新增</button></span><h3 class="timeline-header"><a href="####">生产-1</a></h3><div class="timeline-body"><div class="input-group"><span class="input-group-addon">操作简述</span> <input type="text" class="form-control input-sm"></div></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div></div></div></li><li><i class="fa fa-balance-scale bg-yellow"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm  btn-warning addTemplateItem" id="pageThreeaddItem">自定义结果字段</button></span><h3 class="timeline-header"><a href="####">生产-End</a></h3><div class="timeline-body">生产结果</div><div class="timeline-footer box-body selection_widget" id="pagethree"><ul class="list" style="padding-left: 0px;"></ul></div></div></li><li> <i class="fa fa-clock-o bg-gray"></i></li>';
var defaultCaozuo='<li class="time-label" id="startCaozuo"><span class="bg-red">严格执行有关文件的操作规范 </span></li><li class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm btn-box-tool" onclick="choseReagent(this)"><i class="fa fa-search-plus"></i>添加原辅料</button><button class="btn btn-sm btn-box-tool" onclick="choseCos(this)"><i class="fa fa-search-plus"></i>添加设备类型</button><button class="btn btn-sm btn-box-tool" onclick="chosezhijian(this)"><i class="fa fa-search-plus"></i>添加质检项</button><button style="background-color:red;color:white" class="btn btn-sm btn-box-tool" onclick="addSeTiem(this)"><i class="fa fa-times" aria-hidden="true"><input type="hidden" class="shide" value="1"></i>删除开始/结束时间</button><button class="btn btn-sm btn-box-tool" onclick="moreCaozuo (this)"><i class="fa fa-level-down"></i>操作新增</button></span><h3 class="timeline-header"><a href="####">生产-1</a></h3><div class="timeline-body"><div class="input-group"><span class="input-group-addon">操作简述</span><input type="text" class="form-control input-sm" /></div></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="zhijianBody"></ul></div></div></div></li><li> <i class="fa fa-clock-o bg-gray"></i></li>';
$(function() {
	id = $("#template_id").text().trim();
	//生成步骤
	var stepsNum = $("#stepsNum").val();
	renderSteps(stepsNum);
	//生成富文本编辑器
	myEditor = CKEDITOR.replace('editor1');
	//默认渲染第一个步骤数据
	renderTemplateData(id, "1", "1");
	//箭头步骤点击
	arrowClick()
	
	
	
})
function addSeTiem(that){
	if($(that).text()=="添加开始/结束时间"){
		$(that).text("删除开始/结束时间")
		$(that).css({'background-color':'red'});
		$(that).css({'color':'white'});
		$(that).prepend('<i class="fa fa-times" aria-hidden="true"></i>')
		$(that).prepend('<input type="hidden" class="shide" value="1">')
		
		
	}else if($(that).text()=="删除开始/结束时间"){
	$(that).text("添加开始/结束时间")
	     $(that).css("background-color","");
	     $(that).css("color","");
	   
		$(that).prepend('<i class="fa fa-check" aria-hidden="true"></i>')
		$(that).prepend('<input type="hidden" class="shide" value="0">')
		
		
	}
	
}
//箭头步骤点击
function arrowClick() {
	$(".bwizard-steps>li").click(function() {
		var stepId = $(this).attr("note");
		var orderid = $(".wizard_steps .selected").attr("stepid");
		var orderNum= $(".wizard_steps .selected .step_no").text();
		if(stepId == "2") { //渲染质检
			var action = "/system/template/templeNew/templatezhijianListPage.action";
			var data = {
				id: id,
				orderNum: orderNum,
				stepId: stepId
			};
		} else if(stepId == "3") { //渲染BL操作
			var action = "/system/template/templeNew/templateProducingCellList.action";
			var data = {
				id: id,
				orderNum:orderNum,
				stepId: stepId,
				orderid: orderid
			};
		} else if(stepId == "4") { //渲染完工清场
			var action = "/system/template/templeNew/findTempleFinishedListPage.action";
			var data = {
				id: id,
				orderNum: orderNum,
				stepId: stepId,
				orderid: orderid
			};
		}
		if(stepId != "1") {
			$.ajax({
				type: "post",
				url: action,
				data: data,
				success: function(res) {
					//console.log(res);
					if(stepId == "2") {
						renderZhijian(JSON.parse(res).zhijianList)
					}else if(stepId =='3'){
						renderCaozuo(JSON.parse(res).producCell);
					}else if(stepId =='4'){
						//操作指令
						if($("#fourCZZL_wrapper").length){
							pageFourtable.ajax.url('/system/template/templeNew/templateFinishedTableJson.action?orderNum=' + orderNum + '&orderid=' + orderid + '&stepId=1').load();
						}else{
							pageFourtable=operationInstruction(id, "fourCZZL", '/system/template/templeNew/templateFinishedTableJson.action?orderNum=' + orderNum + '&orderid=' + orderid + '&stepId='+stepId);
						}
						//渲染自定义字段
						renderProductionRecord("pageFour",JSON.parse(res).content);
					}
				}
			});
		}
	});
}
//生成步骤
function renderSteps(stepsNum) {
	var stepsLi = "<li><a class='selected step'><span class='step_no'>1</span></a></li>";
	for(var i = 0; i < stepsNum; i++) {
		if(i > 0) {
			stepsLi += "<li><a class='disabled step'><span class='step_no'>" + (i + 1) + "</span></a></li>";
		}
	}
	$(".wizard_steps").append(stepsLi);
	//为每一个步骤注册点击事件
	$(".wizard_steps .step").click(function() {
		$(".wizard_steps .step").removeClass("selected").addClass("disabled");
		$(this).removeClass("disabled").addClass("selected");
		var orderNum = $(this).text();
		var orderid = $(".wizard_steps .selected").attr("stepid");
		$(".bwizard-steps>li").eq(0).children("a").click();
		//更新步骤信息和自定义字段
		renderTemplateData(id, orderNum, "1");
		//更新操作指令
		pageOnetable.ajax.url('/system/template/templeNew/templateNstructionTableJson.action?orderNum=' + orderNum + '&orderid=' + orderid + '&stepId=1').load();
		//更新操作指令
		pageOnetableGqzb.ajax.url('/system/template/templeNew/templateBeforeReagentTableJson.action?orderNum=' + orderNum + '&orderid=' + orderid + '&stepId=1').load();
	});
}
////更新步骤信息和自定义字段
function renderTemplateData(id, orderNum, stepId) {
	$.ajax({
		type: "post",
		data: {
			id: id,
			orderNum: orderNum,
			stepId: stepId
		},
		url: ctx + "/system/template/templeNew/templateEditStepTwoTableJson.action",
		success: function(data) {
			var data = JSON.parse(data);
			var list = data.itemList;
			var content = data.content;
			//默认渲染第一个步骤
			//名称
			if(list.length) {
				$(".wizard_steps .selected").attr("stepid", list[0].id);
				$("#template_name").val(list[0].name);
				$("#template_code").val(list[0].code);
				$("#estimated_time").val(list[0].estimatedTime);
				$("#print_label_coding").val(list[0].printLabelCoding);
				$("#blend").val(list[0].blend);
				
				$("#reinfusionPlan").val(list[0].reinfusionPlan);
				$("#harvest").val(list[0].harvest);
				$("#incubator").val(list[0].incubator);
				$("#transportPlan").val(list[0].transportPlan);
				$("#bottleOrBag").val(list[0].bottleOrBag);
				$("#cellObservation").val(list[0].cellObservation);
				//编辑器代码
				myEditor.setData(list[0].note);
				//自定义字段渲染
				renderProductionRecord("pageone",content);
			} else {
				$(".wizard_steps .selected").attr("stepid", "")
				$("#template_name").val("");
				$("#template_code").val("");
				$("#estimated_time").val("");
				$("#print_label_coding").val("");
				$("#blend").val("");
				$("#bottleOrBag").val("");
				$("#reinfusionPlan").val("");
				$("#harvest").val("");
				$("#incubator").val("");
				$("#transportPlan").val("");
				$("#bottleOrBag").val("");
				$("#cellObservation").val("");
				//编辑器代码
				myEditor.setData("");
				renderProductionRecord("pageone",content);
			}
		}
	});
}
//渲染质检
function renderZhijian(zhijianList) {
	$("#zhijianBody .zhijianli").remove();
	if(!zhijianList.length) {
		$("#zjType").text("");
		$("#zjType").attr("");
		$("#zhijianBody .nozhijian").slideDown();
	} else {
		$("#zhijianBody .nozhijian").slideUp();
		var zhijianLis = "";
		zhijianList.forEach(function(v, i) {
			$("#zjType").text(v.typeName);
			$("#zjType").attr("zjId", v.typeId);
			$("#reagentBody .nozhijian").slideUp();
			var id = v.id ? v.id : "";
			var code = v.code ? v.code : "";
			var name = v.name ? v.name : "";
			zhijianLis += '<li class="zhijianli zhijianOld" id=' + id + '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span zhijianid="' + code + '" class="zhijianName">' + name + '</span></small><i class="fa fa-trash pull-right"></i></li>';
		});
		$("#zhijianBody").append(zhijianLis);
	}
	//原辅料和设备he质检删除操作
	reagentAndCosRemove();
}
//渲染BL操作
function renderCaozuo (caozuo) {
	var caozuo=JSON.parse(caozuo);
	var max=caozuo.length;
	if(max){
		$(".timeline").html('<li class="time-label" id="startCaozuo"><span class="bg-red">严格执行有关文件的操作规范 </span></li>');
		caozuo.forEach(function (v,i) {
			if(i<max-1){
//				var li=$('<li id="'+v.id+'" class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm btn-box-tool" onclick="choseReagent(this)"><i class="fa fa-search-plus"></i>添加原辅料</button><button class="btn btn-sm btn-box-tool addcos" onclick="choseCos(this)"><i class="fa fa-search-plus"></i>添加设备类型</button><button style="background-color:red;color:white" class="btn btn-sm btn-box-tool" onclick="addSeTiem(this)"><i class="fa fa-times" aria-hidden="true"><input type="hidden" class="shide" value='+v.hideTime+'></i>删除开始/结束时间</button></span><h3 class="timeline-header"><a href="####">'+v.title+' </a></h3><div class="timeline-body"><div class="input-group"><span class="input-group-addon">操作简述</span> <input type="text" class="form-control input-sm" value="'+v.name+'"></div></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div><div class="col-xs-12 selection_widget" id="pagethree'+v.id+'"><ul class="list" style="padding-left: 0px;"></ul></div></div></div></li>');
				if(v.hideTime=="1"){
					var li=$('<li id="'+v.id+'" class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm btn-box-tool" onclick="choseReagent(this)"><i class="fa fa-search-plus"></i>添加原辅料</button><button class="btn btn-sm btn-box-tool addcos" onclick="choseCos(this)"><i class="fa fa-search-plus"></i>添加设备类型</button><button class="btn btn-sm btn-box-tool" onclick="chosezhijian(this)"><i class="fa fa-search-plus"></i>添加质检项</button><button style="background-color:red;color:white" class="btn btn-sm btn-box-tool" onclick="addSeTiem(this)"><i class="fa fa-times" aria-hidden="true"><input type="hidden" class="shide" value='+v.hideTime+'></i>删除开始/结束时间</button></span><h3 class="timeline-header"><a href="####">'+v.title+' </a></h3><div class="timeline-body"><div class="input-group"><span class="input-group-addon">操作简述</span> <input type="text" class="form-control input-sm" value="'+v.name+'"></div></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="zhijianBody"></ul></div><div class="col-xs-12 selection_widget" id="pagethree'+v.id+'"><ul class="list" style="padding-left: 0px;"></ul></div></div></div></li>');
	
				}else if(v.hideTime="0"){
					var li=$('<li id="'+v.id+'" class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm btn-box-tool" onclick="choseReagent(this)"><i class="fa fa-search-plus"></i>添加原辅料</button><button class="btn btn-sm btn-box-tool addcos" onclick="choseCos(this)"><i class="fa fa-search-plus"></i>添加设备类型</button><button class="btn btn-sm btn-box-tool" onclick="chosezhijian(this)"><i class="fa fa-search-plus"></i>添加质检项</button><button  class="btn btn-sm btn-box-tool" onclick="addSeTiem(this)"><i class="fa fa-check" aria-hidden="true"><input type="hidden" class="shide" value='+v.hideTime+'></i>添加开始/结束时间</button></span><h3 class="timeline-header"><a href="####">'+v.title+' </a></h3><div class="timeline-body"><div class="input-group"><span class="input-group-addon">操作简述</span> <input type="text" class="form-control input-sm" value="'+v.name+'"></div></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="zhijianBody"></ul></div><div class="col-xs-12 selection_widget" id="pagethree'+v.id+'"><ul class="list" style="padding-left: 0px;"></ul></div></div></div></li>');

				}
				if(i==caozuo.length-2){
					li.find(".addcos").after('<button class="btn btn-sm btn-box-tool" onclick="moreCaozuo (this)"><i class="fa fa-level-down"></i>操作新增</button>');
				}
				li.find(".addcos").after('<button class="btn btn-sm btn-box-tool" onclick="remoreCaozuo(this)"><i class="fa fa-level-down"></i>操作删除</button>');
				li.find(".addcos").after('<button class="btn btn-sm btn-box-tool btn-warning addTemplateItem" id="pageThreeaddItem'+v.id+'"><i class="fa fa-level-down"></i>自定义结果字段</button>');
				var reagent=v.reagent;
				var cos=v.cos;
				var zhijianList=v.zhijianList;
				reagent.forEach(function(vv,ii) {
					renderReagent(li,vv)
				})
				cos.forEach(function(vvv,iii) {
					renderCos(li,vvv)
				})
				zhijianList.forEach(function(vvv,iii) {
					renderZhiJianNew(li,vvv)
				})
				$(".timeline").append(li);
				addTemplateItemBysc('pageThreeaddItem'+v.id);
				renderProductionRecord("pagethree"+v.id,v.centent);
			}
		});
//		$(".timeline").append('<li><i class="fa fa-balance-scale bg-yellow"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm  btn-warning addTemplateItem" id="pageThreeaddItem">自定义结果字段</button></span><h3 class="timeline-header"><a href="####">生产-End</a></h3><div class="timeline-body">生产结果</div><div class="timeline-footer box-body selection_widget" id="pagethree"><ul class="list" style="padding-left: 0px;"></ul></div></div></li><li> <i class="fa fa-clock-o bg-gray"></i></li>');
//		renderProductionRecord("pagethree",caozuo[caozuo.length-1].centent);
	}else{
		$(".timeline").html(defaultCaozuo);
	}	
	addTemplateItemBysc('pageThreeaddItem');
	reagentAndCosRemove();
}

//渲染设备
function renderReagent(li,reagent) {
	var reagentLis = "";
	var id = reagent.id ? reagent.id : "";
	var code = reagent.code ? reagent.code : "";
	var name = reagent.name ? reagent.name : "";
	reagentLis += '<li class="reagli reagOld" id=' + id + '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">' + biolims.common.reagentName + ':<span reagentid=' + code + ' class="reaName">' + name + '</span></small><i class="fa fa-trash pull-right"></i><div class="input-group"><span class="input-group-addon">原辅料用量</span> <input  style="width:65%" type="text" class="form-control input-sm sum" value='+ reagent.amount+'><input readonly="readonly" style="width:25%" type="text" class="form-control input-sm unit" value='+reagent.unit+'></div></li>'
	li.find("#reagentBody").append(reagentLis);
}
//渲染质检
function renderZhiJianNew(li,zhijian) {
	var reali = "";
	var id = zhijian.id ? zhijian.id : "";
	var code = zhijian.code ? zhijian.code : "";
	var name = zhijian.name ? zhijian.name : "";
	var sum = zhijian.sampleNum ? zhijian.sampleNum : "";
	var unit = zhijian.sampleNumUnit ? zhijian.sampleNumUnit : "";
	var sname = zhijian.sampleType ? zhijian.sampleType : "";
	var snameId = zhijian.sampleTypeId ? zhijian.sampleTypeId : "";
	var type = zhijian.typeId ? zhijian.typeId : "";
	var typeName = zhijian.typeName ? zhijian.typeName : "";
//	reali += '<li class="zhijianli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">检测项:<span zhijianid=\'' + code + '\' class="zhijianName">' + name + '</span></small><i class="fa fa-trash pull-right"></i></li>';
	reali += '<li class="zhijianli zhijianOld" id=' + id +' ><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">检测项:<span zhijianid=\'' + code + '\' class="zhijianName">' + name + '</span></small><i class="fa fa-trash pull-right"></i><div class="input-group"><span class="input-group-addon">样本量</span> <input style="width:90%" type="text" class="form-control input-sm sum" value='+sum+'></div><div class="input-group"><span class="input-group-addon">单位</span> <input style="width:90%" type="text" class="form-control input-sm unit" value='+unit+'></div><div class="input-group"><span class="input-group-addon">样本名称<img class="requiredimages" src="/images/required.gif"></span> <input style="width:73%" type="text" readonly=readonly class="form-control input-sm sname" value='+sname+'><input style="width:73%" type="hidden" readonly=readonly class="form-control input-sm snameId" value='+snameId+'><input style="width:73%" type="hidden" readonly=readonly class="form-control input-sm type" value='+type+'><input style="width:73%" type="hidden" readonly=readonly class="form-control input-sm typeName" value='+typeName+'><button class="btn btn-info" type="button" onclick="xuanze(this)"><i class="glyphicon glyphicon-search"></i></button></div></li>';
	li.find("#zhijianBody").append(reali);
}
//渲染原辅料
function renderCos(li,cos) {
	var cosLis = "";
	var id = cos.id ? cos.id : "";
	var typeId = cos.typeId ? cos.typeId : "";
	var typeName = cos.typeName ? cos.typeName : "";
	cosLis += '<li class="cosli cosOld" id=' + id + '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">' + biolims.common.instrumentType + ':<span cosid=' + typeId + ' class="cosName">' + typeName + '</span></small><i class="fa fa-trash pull-right"></i></li>'
	li.find("#cosBody").append(cosLis);
}

//为步骤明细的编辑器设置动画
function animateEdit(that) {
	if($(that).hasClass("xxx")) {
		$(that).removeClass("xxx");
		$("#stepContentModer").animate({
			"height": "10%"
		}, 800, "swing", function() {
			$(this).animate({
				"width": "0%",
				"height": "0%",
			}, 600, "linear");
		});
	} else {
		$(that).addClass("xxx");
		$("#stepContentModer").animate({
			"width": "100%",
			"height": "10%"
		}, 500, "swing", function() {
			$(this).animate({
				"height": "90%"
			}, 600, "linear");
		});
	}
}

//选择质检类型
function choseType(that) {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		closeBtn: 0,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=zjType", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			if(id==""||id==null){
				top.layer.msg("请选择一条数据");
			}else{
				$(that).find('.typeName').val(name)
				$(that).find('.type').val(id)
				$("#zjType").text(name);
				$("#zjType").attr("zjId",id);
				top.layer.close(index);
			}
		},
	})
}


//选择样本质检类型
function xuanze(that) {
	top.layer
			.open({
				title : "选择样本名称",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/sample/dicSampleType/selectDicSampleTypeOne.action",
						'' ],
				yes : function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find(
					"#addDicSampleType .chosed").children("td").eq(0)
					.text();
					var type = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(1)
							.text();
					
					$(that).prev().prev().prev().prev().val(type);
					$(that).prev().prev().prev().val(id);
					top.layer.close(index)
				},
			})
}

//选择检测项
function chosezhijian(that) {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/detecyion/sampleDeteyion/showSampledetecyion.action", ''],
		yes: function(index, layer) {
			
//			alert($(v).children("td").eq(1).text())
//			alert($(v).children("td").eq(2).text())
			var reali = '';
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .selected").each(function(i, v) {
				reali += '<li class="zhijianli zhijianliNew"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">检测项:<span zhijianid=\'' + $(v).children("td").eq(1).text() + '\' class="zhijianName">' + $(v).children("td").eq(2).text() + '</span></small><i class="fa fa-trash pull-right"></i><div class="input-group"><span class="input-group-addon">样本量</span> <input style="width:90%" type="text" class="form-control input-sm sum"></div><div class="input-group"><span class="input-group-addon">单位</span> <input style="width:90%" type="text" class="form-control input-sm unit"></div><div class="input-group"><span class="input-group-addon">样本名称<img class="requiredimages" src="/images/required.gif"></span> <input style="width:73%"  readonly=readonly type="text" class="form-control input-sm sname"><input style="width:73%" type="hidden" readonly=readonly class="form-control input-sm snameId"><input style="width:73%" type="hidden" readonly=readonly class="form-control input-sm type"><input style="width:73%" type="hidden" readonly=readonly class="form-control input-sm typeName"><button class="btn btn-info" type="button" onclick="xuanze(this)"><i class="glyphicon glyphicon-search"></i></button></div></li>';
				

			})
			if(reali==''||reali==null){
				
			}else{
				
				$(that).parents(".timeline-item").find("#zhijianBody").append(reali);
			    $("#zhijianBody").append(reali);
				$("#zhijianBody .nozhijian").slideUp();
				choseType($(that).parents(".timeline-item").find("#zhijianBody"));
			}
			top.layer.close(index);
			
			reagentAndCosRemove();
		},
	})

}
//选择原辅料
function choseReagent(that) {
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/getStorage.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorage .chosed").children("td").eq(1).text();
			var code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorage .chosed").children("td").eq(0).text();
			var unit = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorage .chosed").children("td").eq(0).attr("unit");
			var reali = '<li class="reagli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">原辅料名称:<span reagentid=' + code + ' class="reaName">' + name + '</span></small><i class="fa fa-trash pull-right"></i><div class="input-group"><span class="input-group-addon">原辅料用量</span> <input style="width:65%" type="text" class="form-control input-sm sum"><input readonly="readonly" style="width:25%" type="text" class="form-control input-sm unit" value='+unit+'></div></li>';
			$(that).parents(".timeline-item").find("#reagentBody").append(reali);
			top.layer.close(index);
			reagentAndCosRemove();
		},
	})
}
//选择设备
function choseCos(that) {
	top.layer.open({
		title: biolims.common.selCosType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=device", ''],
		yes: function(index, layer) {
			var typeName = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var typeId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			var cosli = '<li class="cosli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">设备类型:<span cosid=' + typeId + ' class="cosName">' + typeName + '</span></small><i class="fa fa-trash pull-right"></i></li>'
			$(that).parents(".timeline-item").find("#cosBody").append(cosli);
			top.layer.close(index);
			reagentAndCosRemove();
		},
	})
}
//质检和原辅料和设备删除操作
function reagentAndCosRemove() {
	$(".todo-list .fa-trash").unbind("click").click(function() {
		var that = this;
		top.layer.confirm(biolims.common.confirm2Del, {
			icon: 3,
			title: biolims.common.prompt,
			btn: biolims.common.selected
		}, function(index) {
			var li = $(that).parent("li");
			if(li.hasClass("zhijianOld")) {
				var id = li.attr("id");
				$.ajax({
					type: "post",
					url: ctx + "/system/template/templeNew/deltemplatezhijian.action",
					data: {
						ids: id,
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							li.remove();
							top.layer.msg(biolims.common.deleteSuccess);
							if(!$("#zhijianBody .zhijianli").length) {
								$("#zhijianBody .nozhijian").slideDown();
							}
						}
					}
				});
			} else if(li.hasClass("reagOld")) {
				var id = li.attr("id");
				$.ajax({
					type: "post",
					url: ctx + "/system/template/templeNew/deltemProductiongReanger.action",
					data: {
						ids: id,
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							li.remove();
							top.layer.msg(biolims.common.deleteSuccess);
							if(!$("#reagentBody .reagli").length) {
								$("#reagentBody .noReag").slideDown();
							}
						}

					}
				});
			} else if(li.hasClass("cosOld")) {
				var id = li.attr("id");
				$.ajax({
					type: "post",
					url: ctx + "/system/template/templeNew/deltemProductiongCos.action",
					data: {
						ids: id,
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							li.remove();
							top.layer.msg(biolims.common.deleteSuccess);
							if(!$("#cosBody .cosli").length) {
								$("#cosBody .noCos").slideDown();
							}
						}
					}
				});
			} else {
				var noitem = $(that).parents(".todo-list").children("li");
				li.remove();
				if(noitem.length == "2") {
					noitem.slideDown();
				}
			}
			top.layer.close(index);
		});

	});
}
//模板步骤的删除
function removeStepItem() {
	if($(".wizard_steps").children("li").length == "1") {
		top.layer.msg(biolims.common.stepsNo);
		return false;
	}
	top.layer.confirm(biolims.common.confirm2Del, {
		icon: 3,
		title: biolims.common.prompt,
		btn: biolims.common.selected
	}, function(index) {
		$.ajax({
			type: "post",
			data: {
				id: id,
				itemId: $(".wizard_steps .selected").attr("stepid"),
			},
			url: ctx + "/system/template/template/delTemplateItem.action",
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					window.location.reload();
				}
			}
		});
		top.layer.close(index);
	});

}

//操作新增
function moreCaozuo(that) {
	var num = $(that).parents(".timeline-item").children(".timeline-header").text().split("-")[1];
	var newNum = parseInt(num) + 1;
	var newCaozuo = '<li class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><button class="btn btn-sm btn-box-tool" onclick="choseReagent(this)"><i class="fa fa-search-plus"></i>添加原辅料</button><button class="btn btn-sm btn-box-tool" onclick="choseCos(this)"><i class="fa fa-search-plus"></i>添加设备类型</button><button class="btn btn-sm btn-box-tool" onclick="chosezhijian(this)"><i class="fa fa-search-plus"></i>添加质检项</button><button style="background-color:red;color:white" class="btn btn-sm btn-box-tool" onclick="addSeTiem(this)"><i class="fa fa-times" aria-hidden="true"><input type="hidden" class="shide" value="1"></i>删除开始/结束时间</button><button class="btn btn-sm btn-box-tool" onclick="remoreCaozuo(this)"><i class="fa fa-level-down"></i>操作删除</button><button class="btn btn-sm btn-box-tool" onclick="moreCaozuo (this)"><i class="fa fa-level-down"></i>操作新增</button></span><h3 class="timeline-header"><a href="####">生产-' + newNum + '</a></h3><div class="timeline-body"><div class="input-group"><span class="input-group-addon">操作简述</span> <input type="text" class="form-control input-sm"></div></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="zhijianBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div></div></div></li>';
	$(that).parents("li").after(newCaozuo);
	$(that).remove();
}

//操作删除
function remoreCaozuo(that) {
	var li = $(that).parents("li");
	if(li.hasClass("caozuoItem")) {
		var id = li.attr("id");
		if(id!=undefined
				&&id!=""){
			$.ajax({
				type: "post",
				data: {
					id: id
				},
				url: ctx + "/system/template/templeNew/delTempleProducingCell.action",
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.msg("删除成功！");
						$(that).parents("li").remove();
					}else{
						top.layer.msg("删除失败，请重试！");
					}
				}
			});
		}else{
			$(that).parents("li").remove();
		}
	};
	
	
}
//乐观锁
var lgs = true;
//保存数据 

function saveStepItem() {
	var pan =true;
	if(lgs){
		var requiredField = templateRequiredFilter();		
		if (!requiredField) {	
			return false;
		}	
		lgs = false;
		var action = "";
		var saveData = "";
			
		var stepId = $(".bwizard-steps>.active").attr("note");
		//拼生产记录自定义字段儿


	
		if(pan){
		if(stepId == "1") { //工前准备
			var contentData = saveProductionRecordJson($("#pageone")); //自定义字段
			var template = {
				id: $(".wizard_steps .selected").attr("stepid"),
				stepId: stepId,
				code: $("#template_code").val(), //步骤编号
				name: $("#template_name").val(), //名称
				estimatedTime: $("#estimated_time").val(), //预计用天
				printLabelCoding: $("#print_label_coding").val(), //打印标签编码
				blend: $("#blend").val(),
				reinfusionPlan: $("#reinfusionPlan").val(),
				harvest: $("#harvest").val(),
				incubator: $("#incubator").val(),
				bottleOrBag:$("#bottleOrBag").val(),
				cellObservation:$("#cellObservation").val(),
//				transportPlan: $("#transportPlan").val(),
				note: myEditor.getData(),
				content: contentData,
				operationInstruction: saveOperationInstruction($("#oneCZZL")),
			};
			saveData = JSON.stringify(template);
			//console.log(saveData);
			action = "/system/template/templeNew/saveTempleItem.action";
		} else if(stepId == "2") { //拼质检的数据
			var zhijianli = $("#zhijianBody .zhijianli");
			var zhijian = [];
			zhijianli.each(function(i, val) {
				var zhijianItem = {};
				zhijianItem.typeId = $("#zjType").attr("zjId");
				zhijianItem.typeName = $("#zjType").text();
				zhijianItem.id = val.id;
				zhijianItem.name = $(val).find(".zhijianName").text();
				zhijianItem.code = $(val).find(".zhijianName").attr('zhijianid');
				zhijian.push(zhijianItem);
			});
			var template = {
				id: $(".wizard_steps .selected").attr("stepid"),
				stepId: stepId,
				zhijian: zhijian
			}
			saveData = JSON.stringify(template);
			action = "/system/template/templeNew/saveQualityTesting.action";
		} else if(stepId == "3") { //生产操作
			var flag=false;
			$("#thirdTab").find("input.sname").each(function(){
				if($(this).val()==""){
					flag=true;
				}
			});
			if(flag){
				top.layer.msg("样本名称不能为空！");
				lgs = true;
				return false;
			}
			var arr=[];
			$(".caozuoItem").each(function (i,v) {
				var obj={};
				obj.id = v.id;
				obj.title=$(v).find(".timeline-header a").text();
				obj.text=$(v).find(".input-sm").val();
				obj.hideTime=$(v).find(".shide").val();
				//拼原辅料的数据
				var reagentli = $(v).find(".reagli");
				var reagent = [];
				reagentli.each(function(i, val) {
					var reagentItem = {};
					console.log($(val))
					reagentItem.id = val.id;
					reagentItem.name = $(val).find(".reaName").text();
					reagentItem.code = $(val).find(".reaName").attr('reagentid');
					reagentItem.amount  = $(val).find(".sum").val();
					reagentItem.unit  = $(val).find(".unit").val();
					reagent.push(reagentItem);
				});
				obj.reagent=reagent;
				//拼设备的数据
				var cosli =$(v).find(".cosli");
				var cos = [];
				cosli.each(function(i, val) {
					var cosItem = {};
					cosItem.id = val.id;
					cosItem.typeName = $(val).find(".cosName").text();
					cosItem.typeId = $(val).find(".cosName").attr('cosid');
					cos.push(cosItem);
				});
				obj.cos=cos;
				//拼质检数据
				var zhijianli =$(v).find(".zhijianli")
				var zhijian = [];
				zhijianli.each(function(i, val) {
					var zhijianItem = {};
					zhijianItem.typeId = $("#zjType").attr("zjId");
					zhijianItem.typeName = $("#zjType").text();
					zhijianItem.id = val.id;
					zhijianItem.name = $(val).find(".zhijianName").text();
					zhijianItem.code = $(val).find(".zhijianName").attr('zhijianid');
					
					zhijianItem.sum = $(val).find(".sum").val();
					zhijianItem.unit = $(val).find(".unit").val();
					zhijianItem.sname = $(val).find(".sname").val();
					zhijianItem.snameId = $(val).find(".snameId").val();
					zhijianItem.type = $(val).find(".type").val();
					zhijianItem.typeName = $(val).find(".typeName").val();
					zhijian.push(zhijianItem);
				});
				obj.zhijian=zhijian;
				//
				debugger
				console.log(zhijian)
				obj.customTest = saveProductionRecordJson($("#pagethree"+v.id)); //自定义字段
				arr.push(obj)
			});
			var result = saveProductionRecordJson($("#pagethree")); //自定义字段
			var template = {
				id: $(".wizard_steps .selected .step_no").text(),
				stepId: stepId,
				result: result,
				content: arr,
			};
			saveData = JSON.stringify(template);
			//console.log(saveData);
			action = "/system/template/templeNew/saveTempleManufacture.action";
		} else if(stepId == "4") {
			var contentData = saveProductionRecordJson($("#pageFour")); //自定义字段
			var template = {
				id: $(".wizard_steps .selected").attr("stepid"),
				stepId: stepId,
				content: contentData,
				operationInstruction:saveOperationInstruction($("#fourCZZL")),
			};
			saveData = JSON.stringify(template);
			//console.log(saveData);
			action = "/system/template/templeNew/saveTemplefinished.action";
		}
		$.ajax({
			type: "post",
			url: ctx + action,
			data: {
				id: $("#template_id").text().trim(),
				orderNum: $(".wizard_steps .selected .step_no").text(),
				itemJson: saveData,
			},
			success: function(data) {
				//console.log(data);
				var data = JSON.parse(data);
				if(data.success) {
					lgs = true;
					top.layer.msg(biolims.common.saveSuccess);
					if(stepId=="1"){
						$(".wizard_steps .selected").click();
					}else{
						$(".bwizard-steps .active").click();
					}
				}else{
					lgs = true;
					top.layer.msg("保存失败，请重试！");
				}
			}
		});
		}
	}
	
}

//必填验证
function templateRequiredFilter() {
	var flag=true;
	$(".requiredimage").each(function(i, v) {
		var ipt = $(v).parent(".input-group-addon").siblings("input[type='text']");
		var ipt1 = $(v).parent(".input-group-addon").siblings("select");

		if(ipt1.length) {
			if(!ipt1.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt1.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}

		
		if(ipt.length) {
			if(!ipt.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
	});
	return flag;
}
//上一步
function preTemplateStep() {
	var id = $("#template_id").text();
	window.location = window.ctx +
		"/system/template/template/editTemplate.action?id=" + id;
}


//备注信息
function layerNote(that) {
	if($(that).hasClass("btn-warning")) {
		var txt = $(that).parents("tr").attr("note");
	} else {
		var txt = '';
	}
	top.layer.open({
		title: "备注",
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCounts" class="form-control" style="width: 99%;height: 99%;">' +
			txt + '</textarea>',
		yes: function(index, layero) {
			var content = $("#checkCounts", parent.document).val();
			if(content) {
				$(that).addClass("btn-warning").parents("tr").attr(
					"note", content);
				$(that).addClass("btn-warning").parents("tr").addClass("editagain");
				$(that).addClass("btn-warning").parents("tr").find("td[savename='name']").attr(
					"operationNote", content);
			} else {
				$(that).removeClass("btn-warning").parents("tr").attr(
					"note", "");
				$(that).addClass("btn-warning").parents("tr").addClass("editagain");
				$(that).addClass("btn-warning").parents("tr").find("td[savename='name']").attr(
					"operationNote", content);
			}
			top.layer.close(index);
		},
	});
}