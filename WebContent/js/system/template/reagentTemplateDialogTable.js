/* 
* 文件名称 :reagentTemplateDialogTable.js
* 创建者 : 郭恒开
* 创建日期: 2018/01/25
* 文件描述:选择实验页面原辅料的datatables
* 
*/
var reagentTable;
var addReagentOptions;
var cellPassageId=$("#cellPassageId").val();

$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "storage-id",
		"title": biolims.common.reagentNo,
	});
	colOpts.push({
		"data": "storage-name",
		"title": biolims.common.reagentName,
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.batchCoding,
	});
	colOpts.push({
		"data": "storageOut-outDate",
		"title": "出库时间",
	});
	colOpts.push({
		"data": "expireDate",
		"title": biolims.common.expirationDate,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "expireDate");
			$(td).attr("useid", rowData['id']);
		},
	});
	colOpts.push({
		"data": "num",
		"title": "数量",
	});
	colOpts.push({
		"data": "unitGroupNew-mark2-name",
		"title": "单位",
	});
//	colOpts.push({
//		"data": "note",
//		"title": biolims.common.snCode,
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("useid", rowData['id']);
//		},
//	});
//	colOpts.push({
//		"data" : "usedFinish",
//		"title" : "是否用完",
////		"className" : "select",
//		"name" : "是|否|",
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "usedFinish");
//			$(td).attr("useid", rowData['id']);
//		},
//		"render" : function(data, type, full, meta) {
//			if (data == "0") {
//				return "否";
//			}
//			if (data == "1") {
//				return "是";
//			} else {
//				return '';
//			}
//		}
//	});
//	colOpts.push({
//		"data": "batch",
//		"title": "批次号",
//	});
	var tbarOpts = [];
	tbarOpts.push({
		text : '确认用完试剂',
		action : function() {
			queren();
		}
	});
	tbarOpts.push({
		text : '打印',
		action : function() {
			dayin();
		}
	});
	
	var state=$("#state").val();
	var pici=$("#pici").val();
	var reagentId=$("#reagentId").val();
	if(state=="state"){
		addReagentOptions= table(false,$("#reagentId").val(),
				'/storage/getStrogeReagentJsonByPici.action?batch='+$("#batch").val()+'&pici='+pici+'&reagentId='+reagentId,colOpts , tbarOpts)
	}else{
		addReagentOptions= table(false,$("#reagentId").val(),
				'/storage/getStrogeReagentJson.action?batch='+$("#batch").val()+'&reagentId='+reagentId,colOpts , tbarOpts)
	}
	
	reagentTable = renderData($("#addReagent"), addReagentOptions);
	$("#addReagent").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
//		$("#addReagent_wrapper .dt-buttons").empty();
//		$('#addReagent_wrapper').css({
//			"padding": "0 16px"
//		});
	var trs = $("#addReagent tbody tr");
	reagentTable.ajax.reload();
	reagentTable.on('draw', function() {
		trs = $("#addReagent tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

function dayin(){
	var storageId;
	var rows = $("#addReagent .chosed");
	var length = rows.length;
	if (!length) {
		top.layer.msg("请选择数据！");
		return false;
	}
	rows.each(function(i,j,k){
		storageId=$(this).find("td[savename='expireDate']").attr('useid');
		
	})

	var url = '__report=productSj.rptdesign&id=' + cellPassageId+'&storageItemId=' + storageId;	
	commonPrint(url);
	
}

function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '', '');
}
function queren() {
	var rows = $("#addReagent .chosed");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("td[savename='expireDate']").attr("useid"));
	});
	$.ajax({
		type : "post",
		url : ctx + "/storage/queren.action",
		data : {
			ids:ids
		},
		async : false,
		success : function(data) {
			var data = JSON.parse(data);
			if (data.success) {
				top.layer.msg("确认成功");
				reagentTable.ajax.reload();
			}
		}
	});
		
}

