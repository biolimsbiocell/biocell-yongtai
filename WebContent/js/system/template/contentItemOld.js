/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#template_id").text().trim();

	var tbarOpts = [];

	var colOpts = [];

	colOpts.push({
		"data": "label",
		"title": biolims.common.displayName,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "label");
		}
	});
	colOpts.push({
		"data": "fieldName",
		"title": biolims.common.fieldName ,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "fieldName");
		}
	});
	colOpts.push({
		"data": "type",
		"title": biolims.common.dataType,
		"className": "select",
		"name": biolims.common.textBox+"|"+biolims.common.digitalBox+"|"+biolims.common.dateTextBox+"|"+biolims.common.radiobutton+"|"+biolims.common.checkBox,
		"createdCell": function(td) {
			$(td).attr("saveName", "type");
			$(td).attr("selectOpt", biolims.common.textBox+"|"+biolims.common.digitalBox+"|"+biolims.common.dateTextBox+"|"+biolims.common.radiobutton+"|"+biolims.common.checkBox);
		},
		"render": function(data, type, full, meta) {
			if(data == "text") {
				return biolims.common.textBox;
			}
			if(data == "number") {
				return biolims.common.digitalBox;
			}
			if(data == "date") {
				return biolims.common.dateTextBox;
			}
			if(data == "radio") {
				return biolims.common.radiobutton;
			}
			if(data == "checkbox") {
				return biolims.common.checkBox;
			}
		}
	});
	colOpts.push({
		"data": "required",
		"title": biolims.common.required,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "required");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "true") {
				return biolims.common.yes;
			}
			if(data == "false") {
				return biolims.common.no;
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data": "defaultValue",
		"title": biolims.common.defaultValue,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "defaultValue");
		}
	});
	colOpts.push({
		"data": "readOnly",
		"title": biolims.common.readOnly,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "readOnly");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "true") {
				return biolims.common.yes;
			}
			if(data == "false") {
				return biolims.common.no;
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data": "qualityChecking",
		"title":'是否质检',
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "qualityChecking");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "true") {
				return biolims.common.yes;
			}
			if(data == "false") {
				return biolims.common.no;
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data": "singleOption",
		"title": biolims.common.optionName,
		"width": "180px",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "singleOption");
			if(data != null) {
				if(data.length) {
					var name = '';
					var value = '';
					data.forEach(function(v, i) {
						if(i > 0) {
							val += "/" + v.itemValue;
						} else {
							val = v.itemValue;
						}
					});
					$(td).attr("singleOption-id", val);
				} else {
					$(td).attr("singleOption-id", "");
				}
			}

		},
		"render": function(data, type, full, meta) {
			if(data != undefined) {
				console.log("1212")
				if(data.length) {
					var str = '';
					data.forEach(function(v, i) {
						if(i > 0) {
							str += '/' + v.itemName;
						} else {
							str += v.itemName;
						}
					});
					return str;
				} else {
					return "";
				}
			}

		}
	});
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
		tbarOpts.push({
			text: biolims.common.fieldAddition,
			className: 'btn btn-sm btn-success addTemplateItem',
		});
		tbarOpts.push({
			text: biolims.common.delSelected,
			className: 'btn btn-sm btn-success',
			action: function() {
				removeChosedItem($("#contentItem"))
			}
		});
		tbarOpts.push({
			text: biolims.common.editwindow,
			action: function() {
				editItemLayer($("#contentItem"))
			}
		});
	}
	var contentItemOptions = table(true, id, '/system/template/template/showContentTableJson.action?itemId=1', colOpts, tbarOpts);
	contentItemTable = renderData($("#contentItem"), contentItemOptions);
	//自定义字段儿添加
	addTemplateItem();
});
//自定义字段儿添加
function addTemplateItem() {
	var options = [biolims.common.textBox, biolims.common.digitalBox, biolims.common.dateTextBox, biolims.common.radiobutton, biolims.common.checkBox];
	var lis = '';
	options.forEach(function(v, i) {
		lis += '<li val=' + i + '><a href="###">' + v + '</a></li>';
	});
	$("#contentItem").on('init.dt', function() {
		$(".addTemplateItem").addClass("dropdown-toggle").attr("data-toggle", "dropdown").append('<span class="caret"></span>').wrap("<div class='btn-group'></div>").after('<ul class="dropdown-menu">' + lis + '</ul>');
		$(".addTemplateItem").next("ul").children("li").click(function() {
			var item = $(this).attr("val");
			var name = $(this).text();
			top.layer.open({
				title: biolims.common.addCustomFields,
				type: 1,
				area: [document.body.clientWidth - 300, "400px"],
				btn: biolims.common.selected,
				content: $("#layerItem").html(),
				success: function() {
					$(".layui-layer-content .datatype",parent.document).val(name);
					if(item == "0" || item == "1" || item == "2") { //文本框//数字框//日期框
						$(".layui-layer-content .input-group",parent.document).css("margin-top", "30px");
						$(".layui-layer-content #choseCheekBox",parent.document).remove();
					}
					if(item == "3") { //单选框
						$(".layui-layer-content #choseCheekBox",parent.document).show();
					}
					if(item == "4") { //多选框
						$(".layui-layer-content #choseCheekBox",parent.document).show();
						$(".layui-layer-content .addItemTembtn",parent.document).show().addClass("xxx");
					}
					if($(".addItemTembtn",parent.document).hasClass("xxx")) {
						$(".addItemTembtn",parent.document).click(function() {
							var data = $("#choseCheekBox .choseCheekBox",parent.document).eq(0).clone();
							data.find("input").val("").attr("placeholder", "");
							$(this).before(data);
						});
					}
				},
				yes: function(index, layer) {
					//清除没有内容选项
					$("#contentItem").find(".dataTables_empty").parent("tr").remove();
					//获取添加的数据
					var inputs = $(".form-control",parent.document);
					var addItemLayerValue = [];
					var itemValue = [];
					var itemName = [];
					inputs.each(function(i, val) {
						if(i>4){
							if(i < 14) {
								addItemLayerValue.push($(val).val());
							} else {
								if(i % 2) {
									itemName.push($(val).val());
								} else {
									itemValue.push($(val).val());
								}
							}}

					});
					var itemValueStr = itemValue.join("/");
					var itemNameStr = itemName.join("/");
					//生成tr
					var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
					tr.height(32);
					var ths = $("#contentItem").find("th");
					for(var i = 1; i < ths.length; i++) {
						var edit = $(ths[i]).attr("key");
						var saveName = $(ths[i]).attr("saveName");
						var width = $(ths[i]).width();
						if(edit == "select") {
							var selectOpt = $(ths[i]).attr("selectopt");
							tr.append("<td class=" + edit + " saveName=" + saveName + " selectopt=" + selectOpt + ">" + addItemLayerValue[i - 1] + "</td>");
						} else if(saveName == "singleOption") {
							tr.append("<td class=" + edit + " saveName=" + saveName + " singleOption-id='" + itemValueStr + "' style='overflow:hidden;text-overflow:ellipsis;max-width: " + width + "px'>" + itemNameStr + "</td>");
						} else {
							tr.append("<td class=" + edit + " saveName=" + saveName + " style='overflow:hidden;text-overflow:ellipsis;max-width: " + width + "px'>" + addItemLayerValue[i - 1] + "</td>");
						}
					}
					$("#contentItem").find("tbody").prepend(tr);
					checkall($("#contentItem"));
					top.layer.close(index);
				},
				cancel: function(index, layer) {
					top.layer.close(index)
				}
			});
		});
	});
}

// 获得保存时的json数据
function saveTemTablejson(ele) {
	ele.find("tbody").find(".dataTables_empty").parent("tr").remove();
	var trs = ele.find("tbody").children("tr");
	var data = [];
	trs.each(function(i, val) {
		console.log("xxxxx");
		var json = {};
		var tds = $(val).children("td");
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断选择框并转换为数据库字段
			if(k == "type") {
				var type = $(tds[j]).text();
				if(type == biolims.common.textBox) {
					json[k] = "text";
				} else if(type == biolims.common.digitalBox) {
					json[k] = "number";
				} else if(type == biolims.common.dateTextBox) {
					json[k] = "date";
				} else if(type == biolims.common.radiobutton) {
					json[k] = "radio";
				} else if(type == biolims.common.checkBox) {
					json[k] = "checkbox";
				}
				continue;
			}
			if(k == "readOnly") {
				var readOnly = $(tds[j]).text();
				if(readOnly == biolims.common.yes) {
					json[k] = "true";
				} else if(readOnly == biolims.common.no) {
					json[k] = "false";
				}
				continue;
			}
			if(k == "qualityChecking") {
				var qualityChecking = $(tds[j]).text();
				if(qualityChecking == biolims.common.yes) {
					json[k] = "true";
				} else if(qualityChecking == biolims.common.no) {
					json[k] = "false";
				}
				continue;
			}
			if(k == "required") {
				var required = $(tds[j]).text();
				if(required == biolims.common.yes) {
					json[k] = "true";
				} else if(required == biolims.common.no) {
					json[k] = "false";
				}
				continue;
			}
			if(k == "singleOption") {
				console.log("1111111")
				var itemValue = $(tds[j]).attr("singleoption-id").split("/");
				var itemName = $(tds[j]).text().split("/");
				var singleOption = [];
				itemValue.forEach(function(vvv, iii) {
					singleOption.push({
						itemValue: vvv,
						itemName: itemName[iii]
					})
				});
				json.singleOption = singleOption;
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return data;
}
//删除选中
function removeChosedItem(ele) {
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.confirm2Del + length + biolims.common.barRecord, {icon: 3, title:biolims.common.prompt,
		btn:biolims.common.selected
	 }, function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			$(val).remove();
		});
		top.layer.msg(biolims.common.deleteSuccess);
	});

}