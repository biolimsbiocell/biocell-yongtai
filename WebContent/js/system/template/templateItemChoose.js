﻿$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "tableName",
		"title": biolims.common.tableName,
	});
	colOpts.push({
		"data": "columnName",
		"title": biolims.common.columnName,
	});
	colOpts.push({
		"data": "columnComment",
		"title": biolims.common.columnDescription,
	});
	colOpts.push({
		"data": "fieldType",
		"title": biolims.common.fieldType,
	});
	colOpts.push({
		"data": "dataLength",
		"title": biolims.common.databaseLength,
	});
	colOpts.push({
		"data": "modeltype",
		"title": biolims.common.entityName,
	});
	colOpts.push({
		"data": "controltype",
		"title": biolims.common.controltype,
	});
	colOpts.push({
		"data": "isdisplay",
		"title": biolims.common.doesItShow,
	});
	colOpts.push({
		"data": "isreadonly",
		"title": biolims.common.readOnly,
	});
	colOpts.push({
		"data": "isrequire",
		"title": biolims.common.isrequire,
	});
	colOpts.push({
		"data": "colsort",
		"title": biolims.common.sort,
	});
	colOpts.push({
		"data": "modeltypeclass",
		"title": biolims.common.functionName,
	});
	var tbarOpts = [];
	var addUserOptions = table(true, null,
		'/system/template/template/showChooseTemplateItemList.action?taName='+$("#taName").val(),
		colOpts, tbarOpts)
	var UserTable = renderData($("#templateItemChoose"), addUserOptions);
	$("#templateItemChoose").on('init.dt',function(e, settings) {
			// 清除操作按钮
			$("#templateItemChoose_wrapper .dt-buttons").empty();
			$('#templateItemChoose_wrapper').css({
				"padding": "0 16px"
			});
			UserTable.ajax.reload();
		});
})

