﻿
var cosItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
//	   fields.push({
//		name:'code',
//		type:"string"
//	});
//	   fields.push({
//		name:'name',
//		type:"string"
//	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
			name : 'type-id',
			type : "string"
		});
		fields.push({
			name : 'type-name',
			type : "string"
		});
    fields.push({
		name:'itemId',
		type:"string"
	});
    //
    fields.push({
		name:'temperature',
		type:"string"
	});
	   fields.push({
			name:'speed',
			type:"string"
		});
	   fields.push({
			name:'time',
			type:"string"
		});
	   fields.push({
			name:'note',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.instrumentId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var instrumentType = new Ext.form.TextField({
		allowBlank : false
	});
	instrumentType.on('focus', function() {
		setInstrumentType();
	});
	cm.push({
		dataIndex : 'type-id',
		hidden : true,
		header :biolims.common.instrumentTypeId,
		width : 20 * 6,
	});
	cm.push({
		dataIndex : 'type-name',
		hidden : false,
		header :biolims.common.instrumentType,
		width : 20 * 6,
		editor : instrumentType
	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.no
			},{
				id : '1',
				name : biolims.common.yes
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isDetecting,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(isGood),
		editor: isGood
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'template-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.itemId,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/template/template/showCosItemListJson.action?id="+ $("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title=biolims.common.instrumentDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/system/template/template/delCosItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = cosItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							cosItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : function (){
			//获取选择的数据
			var selectRcords=templateItemGrid.getSelectionModel().getSelections();
			//获取全部数据
			var allRcords=templateItemGrid.store;
			//选中的数量
			var length1=selectRcords.length;
			//全部数据量
			var length2=allRcords.getCount();
			//var selRecord = cosItemGrid.store;
			if(length2>0){
				if(length1==1){
					var code="";
					$.each(selectRcords, function(i, obj) {
						code=obj.get("code");
					});
					if(code!=null){
						var ob = cosItemGrid.getStore().recordType;
						var p = new ob({});
						p.isNew = true;
						p.set("itemId", code);
						cosItemGrid.stopEditing();
						cosItemGrid.getStore().insert(0, p);
						cosItemGrid.startEditing(0, 0);
					}else{
						message(biolims.common.addTemplateDetail);
						return;
					}				
				}else if(length1>1){
					message(biolims.common.onlyChooseOne);
					return;
				}else{
					message(biolims.common.pleaseSelectData);
					return;
				}
			}else{
				message(biolims.common.theDataIsEmpty);
				return;
			}
		}
	});
	cosItemGrid=gridEditTable("cosItemdiv",cols,loadParam,opts);
	$("#cosItemdiv").data("cosItemGrid", cosItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//function selecttemplateFun(){
//	var win = Ext.getCmp('selecttemplate');
//	if (win) {win.close();}
//	var selecttemplate= new Ext.Window({
//	id:'selecttemplate',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TemplateSelect.action?flag=template' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: biolims.common.close,
//	 handler: function(){
//		 selecttemplate.close(); }  }]  });     selecttemplate.show(); }
//	function settemplate(id,name){
//		var gridGrid = $("#cosItemdiv").data("cosItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('template-id',id);
//			obj.set('template-name',name);
//		});
//		var win = Ext.getCmp('selecttemplate');
//		if(win){
//			win.close();
//		}
//	}
//选择设备	
//function selectinstrumentFun(){
//		var win = Ext.getCmp('selectinstrument');
//		if (win) {win.close();}
//		var selectinstrument= new Ext.Window({
//		id:'selectinstrument',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
//		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//		collapsible: true,maximizable: true,
//		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/equipment/main/instrumentSelect.action?flag=InstrumentFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
//		buttons: [{
//			text : "确定",
//			handler :setInstrumentFun()
//		},
//		{ text: biolims.common.close,
//		 handler: function(){
//			 selectinstrument.close(); }  }]  });     selectinstrument.show(); }
//function setInstrumentFun(){
//	var operGrid = $("#show_dialog_instrument_div").data("instrumentDialogGrid");
//	 var selectRecord = operGrid.getSelectionModel().getSelections(); 
//		if(selRecords.length>0){
//			var b=true;
//			for(var a=0;a<gridGrid.store.getCount();a++){
//				if(obj.id==gridGrid.store.getAt(a).get("instrument-id")){
//					b=false;
//				}
//			}
//			if(b){
//				 var ob = gridGrid.getStore().recordType;
//				var p = new ob({});
//				p.isNew = true;
//				p.set("instrument-id",obj.id);
//				p.set("instrument-name",obj.get("name"));
//				gridGrid.getStore().add(p);		
//				gridGrid.startEditing(0, 0);
//		}else{
//			message(biolims.common.selectRecord);
//		}
//		var win = Ext.getCmp('selectinstrument');
//		if(win){
//			win.close();
//		}
//}
function setinstrument(id, name) {
	var record = cosItemGrid.getSelectionModel().getSelected();
	record.set('type-id', id);
	record.set('type-name', name);

	var win = Ext.getCmp('instrumentType');
	if (win) {
		win.close();
	}
}
function setInstrumentType(){
	var win = Ext.getCmp('instrumentType');
	if (win) {
		win.close();
	}
	var instrumentType = new Ext.Window(
			{
				id : 'instrumentType',
				modal : true,
				title : "选择设备类型",
				layout : 'fit',
				width : document.body.clientWidth / 2,
				height : document.body.clientHeight / 1.5,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?setinstrumentType=true&flag=instrument' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						instrumentType.close();
					}
				} ]
			});
	instrumentType.show();
}	
