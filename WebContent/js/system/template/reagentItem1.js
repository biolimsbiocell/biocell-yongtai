﻿var reagentItem1Grid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
			name:'itemId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.reagentId,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.reagentNo,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.reagentName,
		width:45*6
	});
	//鼠标单击触发事件 
//	var batchs =new Ext.form.TextField({
//            allowBlank: false
//    });
//	batchs.on('focus', function() {
//		var selectRecord = reagentItemGrid.getSelectionModel();
//		if (selectRecord.getSelections().length > 0) {
//			$.each(selectRecord.getSelections(), function(i, obj) {
//				var code=obj.get("code");
//				loadStorageReagentBuy(code);
//			});
//		}
//	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6
//		editor:batchs
	});
//	cm.push({
//		dataIndex:'batch',
//		hidden : false,
//		header:'批次',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.singleDose,
		width:20*6
	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.no
			},{
				id : '1',
				name : biolims.common.yes
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isDetecting,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(isGood)
//		editor: isGood
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10
	});
	cm.push({
		dataIndex:'template-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.itemId,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	var t = "11";
	var code=$("#code").val();
	var tid = $("#tid").val();
	loadParam.url=ctx+"/system/template/template/showReagentItemListJson.action?code="+code+"&t="+t+"&tid="+tid;
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight-270;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/system/template/template/delReagentItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				reagentItem1Grid.getStore().commitChanges();
				reagentItem1Grid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	reagentItem1Grid=gridEditTable("reagentItemdiv1",cols,loadParam,opts);
	$("#reagentItemdiv1").data("reagentItem1Grid", reagentItem1Grid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:biolims.common.chosePurchasingReagents,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}
function setStorageReagentBuy(rec){
	
	var gridGrid = $("#reagentItemdiv").data("reagentItemGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}
//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, biolims.common.theInventoryMasterData, url, {
		"Confirm" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					reagentItemGrid.stopEditing();
					var ob = reagentItemGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("itemId",code);
				
					reagentItemGrid.getStore().add(p);	
				});
				reagentItemGrid.startEditing(0, 0);
				
				options.close();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	}, true, options);
}
function selecttemplateFun(){
	var win = Ext.getCmp('selecttemplate');
	if (win) {win.close();}
	var selecttemplate= new Ext.Window({
	id:'selecttemplate',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:400,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TemplateSelect.action?flag=template' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selecttemplate.close(); }  }]  });     selecttemplate.show(); }
	function settemplate(id,name){
		var gridGrid = $("#reagentItemdiv").data("reagentItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('template-id',id);
			obj.set('template-name',name);
		});
		var win = Ext.getCmp('selecttemplate')
		if(win){
			win.close();
		}
	}
	
