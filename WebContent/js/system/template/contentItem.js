/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
var contentItemTable;
var pageOnetableGqzb;
var oldChangeLog;
$(function() {
	// 加载子表
	var id = $("#template_id").text().trim();
	var orderNum = $(".wizard_steps .selected .step_no").text();
	var orderid = $(".wizard_steps .selected").attr("stepid");
	var stepId = $(".bwizard-steps>.active").attr("note");
	//第一页自定义字段儿添加
	addTemplateItem('pageOneaddItem');
	addTemplateItem('pageFouraddItem');
	//操作指令
	pageOnetable=operationInstruction(id, "oneCZZL", '/system/template/templeNew/templateNstructionTableJson.action?orderNum=1&orderid=' + orderid + '&stepId=1')

	//工前准备试剂
	pageOnetableGqzb=operationInstructionGqzb(id, "onegqzb", '/system/template/templeNew/templateBeforeReagentTableJson.action?orderNum=1&orderid=' + orderid + '&stepId=1')

});


//渲染自定义字段儿
function renderProductionRecord(contenid,content) {
	var data = JSON.parse(content);
	var lis = '';
	data.forEach(function(v, i) {
		lis = lis + '<li fieldName=' + v.fieldName + ' type=' + v.type + ' requiredd=' + v.required + ' defaultValue="' + v.defaultValue + '"   readOnlyy=' + v.readOnly + '><a><span>' + v.label + '</span> <i class="fa  fa-pencil-square-o" title="编辑" onclick="editTemplateItem(this)"></i><i class="fa fa-trash" title="删除" onclick="removeTemplateItem(this)"></i></a></li>';
	})
	$("#"+contenid).find("ul").html(lis);
}
$("#pageOneaddItem").click(function(){
	$("#fieldDisplay2").addClass('gu');
	});
//自定义字段儿添加
function addTemplateItem(contenId) {

	var options = [biolims.common.textBox, biolims.common.digitalBox, biolims.common.dateTextBox];
	var lis = '';
	options.forEach(function(v, i) {
		lis += '<li val=' + i + '><a href="###">' + v + '</a></li>';
	});
	$("#"+contenId).addClass("dropdown-toggle").attr("data-toggle", "dropdown").append('<span class="caret"></span>').wrap("<div class='btn-group pull-right'></div>").after('<ul class="dropdown-menu">' + lis + '</ul>');
	$("#"+contenId).next("ul").children("li").click(function() {
		var item = $(this).attr("val");
		var name = $(this).text();
		var that=this;
		top.layer.open({
			title: biolims.common.addCustomFields,
			type: 1,
			area: [document.body.clientWidth - 300, "400px"],
			btn: biolims.common.selected,
			content: $("#layerItem").html(),
			success: function() {

				$(".layui-layer-content .datatype", parent.document).val(name);
				if(item == "0" || item == "1" || item == "2") { //文本框//数字框//日期框
					$(".layui-layer-content .input-group", parent.document).css("margin-top", "30px");
					$(".layui-layer-content #choseCheekBox", parent.document).remove();
					
					$("#layerItem #fieldDisplay2").removeClass('gu');
				}
				if(item == "3") { //单选框
					$(".layui-layer-content #choseCheekBox", parent.document).show();
				}
				if(item == "4") { //多选框
					$(".layui-layer-content #choseCheekBox", parent.document).show();
					$(".layui-layer-content .addItemTembtn", parent.document).show().addClass("xxx");
				}
				if($(".addItemTembtn", parent.document).hasClass("xxx")) {
					$(".addItemTembtn", parent.document).click(function() {
						var data = $("#choseCheekBox .choseCheekBox", parent.document).eq(0).clone();
						data.find("input").val("").attr("placeholder", "");
						$(this).before(data);
					});
				}
			},
			yes: function(index, layer) {
				if($(".gu",parent.document).val()==""){
					top.layer.msg("字段编码不能为空！");
					return false;
				}
				//获取添加的数据
				var inputs = $(".form-control", parent.document);
				var addItemLayerValue = [];
				var itemValue = [];
				var itemName = [];
				inputs.each(function(i, val) {
					if(i > 4) {
						if(i < 11) {
							addItemLayerValue.push($(val).val());
						} else {
							if(i % 2) {
								itemName.push($(val).val());
							} else {
								itemValue.push($(val).val());
							}
						}
					}

				});
				//				var itemValueStr = itemValue.join("/");
				//				var itemNameStr = itemName.join("/");
				var type = addItemLayerValue[2];
				if(type == biolims.common.textBox) {
					var typee = "text";
				} else if(type == biolims.common.digitalBox) {
					var typee = "number";
				} else if(type == biolims.common.dateTextBox) {
					var typee = "date";
				} else if(type == biolims.common.radiobutton) {
					var typee = "radio";
				} else if(type == biolims.common.checkBox) {
					var typee = "checkbox";
				}
				var required = addItemLayerValue[3];
				if(required == biolims.common.yes) {
					var requiredd = "true";
				} else if(required == biolims.common.no) {
					var requiredd = "false";
				}
				var readOnly = addItemLayerValue[5];
				if(readOnly == biolims.common.yes) {
					var readOnlyy = "true";
				} else if(readOnly == biolims.common.no) {
					var readOnlyy = "false";
				}
				var li = '<li fieldName=' + addItemLayerValue[1] + ' type=' + typee + ' requiredd=' + requiredd + ' defaultValue="' + addItemLayerValue[4] + '"   readOnlyy=' + readOnlyy + '><a><span>' + addItemLayerValue[0] + '</span> <i class="fa  fa-pencil-square-o" title="编辑" onclick="editTemplateItem(this)"></i><i class="fa fa-trash" title="删除" onclick="removeTemplateItem(this)"></i></a></li>';
				if(contenId=="pageOneaddItem"){//生产操作
					$("#pageone ul").append(li);
				}else if (contenId=="pageThreeaddItem"){//完工清场
					$("#pagethree ul").append(li);
				}else if(contenId=="pageFouraddItem"){//工前准备
					$("#pageFour ul").append(li);
				}
				top.layer.close(index);
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}
		});
	});

}


//自定义字段儿添加
function addTemplateItemBysc(contenId) {
	var options = [biolims.common.textBox, biolims.common.digitalBox, biolims.common.dateTextBox];
	var lis = '';
	options.forEach(function(v, i) {
		lis += '<li val=' + i + '><a href="###">' + v + '</a></li>';
	});
	$("#"+contenId).addClass("dropdown-toggle").attr("data-toggle", "dropdown")
	.append('<span class="caret"></span>').wrap("<div class='btn-group pull-right'></div>").after('<ul class="dropdown-menu">' + lis + '</ul>');
	$("#"+contenId).next("ul").children("li").click(function() {
		var item = $(this).attr("val");
		var name = $(this).text();
		var that=this;
		top.layer.open({
			title: biolims.common.addCustomFields,
			type: 1,
			area: [document.body.clientWidth - 300, "400px"],
			btn: biolims.common.selected,
			content: $("#layerItem").html(),
			success: function() {
				$(".layui-layer-content .datatype", parent.document).val(name);
				if(item == "0" || item == "1" || item == "2") { //文本框//数字框//日期框
					$(".layui-layer-content .input-group", parent.document).css("margin-top", "30px");
					$(".layui-layer-content #choseCheekBox", parent.document).remove();
				}
				if(item == "3") { //单选框
					$(".layui-layer-content #choseCheekBox", parent.document).show();
				}
				if(item == "4") { //多选框
					$(".layui-layer-content #choseCheekBox", parent.document).show();
					$(".layui-layer-content .addItemTembtn", parent.document).show().addClass("xxx");
				}
				if($(".addItemTembtn", parent.document).hasClass("xxx")) {
					$(".addItemTembtn", parent.document).click(function() {
						var data = $("#choseCheekBox .choseCheekBox", parent.document).eq(0).clone();
						data.find("input").val("").attr("placeholder", "");
						$(this).before(data);
					});
				}
			},
			yes: function(index, layer) {
				//获取添加的数据
				var inputs = $(".form-control", parent.document);
				var addItemLayerValue = [];
				var itemValue = [];
				var itemName = [];
				inputs.each(function(i, val) {
					if(i > 4) {
						if(i < 11) {
							addItemLayerValue.push($(val).val());
						} else {
							if(i % 2) {
								itemName.push($(val).val());
							} else {
								itemValue.push($(val).val());
							}
						}
					}

				});
				//				var itemValueStr = itemValue.join("/");
				//				var itemNameStr = itemName.join("/");
				var type = addItemLayerValue[2];
				if(type == biolims.common.textBox) {
					var typee = "text";
				} else if(type == biolims.common.digitalBox) {
					var typee = "number";
				} else if(type == biolims.common.dateTextBox) {
					var typee = "date";
				} else if(type == biolims.common.radiobutton) {
					var typee = "radio";
				} else if(type == biolims.common.checkBox) {
					var typee = "checkbox";
				}
				var required = addItemLayerValue[3];
				if(required == biolims.common.yes) {
					var requiredd = "true";
				} else if(required == biolims.common.no) {
					var requiredd = "false";
				}
				var readOnly = addItemLayerValue[5];
				if(readOnly == biolims.common.yes) {
					var readOnlyy = "true";
				} else if(readOnly == biolims.common.no) {
					var readOnlyy = "false";
				}
				var li = '<li fieldName=' + addItemLayerValue[1] + ' type=' + typee + ' requiredd=' + requiredd + ' defaultValue="' + addItemLayerValue[4] + '"   readOnlyy=' + readOnlyy + '><a><span>' + addItemLayerValue[0] + '</span> <i class="fa  fa-pencil-square-o" title="编辑" onclick="editTemplateItem(this)"></i><i class="fa fa-trash" title="删除" onclick="removeTemplateItem(this)"></i></a></li>';
//				if(contenId=="pageOneaddItem"){//生产操作
					$("#"+contenId.replace("pageThreeaddItem","pagethree")+" ul").append(li);
					console.log($("#"+contenId.replace("pageThreeaddItem","pagethree")+""));
//				}
				top.layer.close(index);
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}
		});
	});

}


//自定義字段儿编辑
function editTemplateItem(that) {
	var target = $(that).parent("a").parent("li");
	var txt = target.find("span").text();
	var fieldName = target.attr("fieldName");
	var defaultValue = target.attr("defaultValue");
	var type = target.attr("type");
	var readOnlyy = target.attr("readOnlyy");
	var qualityChecking = target.attr("qualityChecking");
	var requiredd = target.attr("requiredd");
	top.layer.open({
		title: '编辑自定义字段',
		type: 1,
		area: [document.body.clientWidth - 300, "400px"],
		btn: biolims.common.selected,
		content: $("#layerItem").html(),
		success: function() {
			$(".layui-layer-content .input-group", parent.document).css("margin-top", "30px");
			$("#fieldDisplay1", parent.document).val(txt);
			$("#fieldDisplay2", parent.document).val(fieldName);
			if(type == "text") {
				var typee = biolims.common.textBox;
			} else if(type == "number") {
				var typee = biolims.common.digitalBox;
			} else if(type == "date") {
				var typee = biolims.common.dateTextBox;
			} else if(type == "radio") {
				var typee = biolims.common.radiobutton;
			} else if(type == "checkbox") {
				var typee = biolims.common.checkBox;
			}
			$("#fieldDisplay3", parent.document).val(typee);
			if(requiredd == "false") {
				$("#fieldDisplay4", parent.document).children("option:eq(0)").attr("selected", true);
			} else {
				$("#fieldDisplay4", parent.document).children("option:eq(1)").attr("selected", true);
			}
			$("#fieldDisplay5", parent.document).val(defaultValue);
			if(readOnlyy == "false") {
				$("#fieldDisplay6", parent.document).children("option:eq(0)").attr("selected", true);
			} else {
				$("#fieldDisplay6", parent.document).children("option:eq(1)").attr("selected", true);
			}
		},
		yes: function(index, layer) {
			//获取添加的数据
			var inputs = $(".form-control", parent.document);
			var addItemLayerValue = [];
			var itemValue = [];
			var itemName = [];
			inputs.each(function(i, val) {
				if(i > 4) {
					if(i < 11) {
						addItemLayerValue.push($(val).val());
					} else {
						if(i % 2) {
							itemName.push($(val).val());
						} else {
							itemValue.push($(val).val());
						}
					}
				}
			});
			var itemValueStr = itemValue.join("/");
			var itemNameStr = itemName.join("/");
			var type = addItemLayerValue[2];
			if(type == biolims.common.textBox) {
				var typee = "text";
			} else if(type == biolims.common.digitalBox) {
				var typee = "number";
			} else if(type == biolims.common.dateTextBox) {
				var typee = "date";
			} else if(type == biolims.common.radiobutton) {
				var typee = "radio";
			} else if(type == biolims.common.checkBox) {
				var typee = "checkbox";
			}
			var required = addItemLayerValue[3];
			if(required == biolims.common.yes) {
				var requiredd = "true";
			} else if(required == biolims.common.no) {
				var requiredd = "false";
			}
			var readOnly = addItemLayerValue[5];
			if(readOnly == biolims.common.yes) {
				var readOnlyy = "true";
			} else if(readOnly == biolims.common.no) {
				var readOnlyy = "false";
			}
			target.attr({
				"fieldName": addItemLayerValue[1],
				"defaultValue": addItemLayerValue[4],
				"type": typee,
				"readOnlyy": readOnlyy,
				"requiredd": requiredd,
			}).find("span").text(addItemLayerValue[0])
			top.layer.close(index);
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	});
}

//自定義字段儿删除
function removeTemplateItem(that) {
	top.layer.confirm(biolims.common.confirm2Del, {
		icon: 3,
		title: biolims.common.prompt,
		btn: biolims.common.selected
	}, function(index) {
		$(that).parent("a").parent("li").remove();
		var orderId = $(".wizard_steps .selected").attr("stepid");
		var stepId = $(".bwizard-steps>.active").attr("note");
		if(stepId=="1"){
			var action="/system/template/templeNew/delTempleRecord.action";
			var content=JSON.stringify(saveProductionRecordJson($("#pageone")));
		}else if(stepId=="3"){
			var action="/system/template/templeNew/deltemplateProductiongCentent.action";
			var content=JSON.stringify(saveProductionRecordJson($("#pagethree")));
		}else if(stepId=="4"){
			
		}
		if(orderId) {
			$.ajax({
				type: "post",
				url: action,
				data: {
					ids :$("#template_id").text().trim(),
					orderNum: $(".wizard_steps .selected .step_no").text(),
					orderId: orderId,
					stepId:stepId,
					content:content  //自定义字段
				},
				success: function(res) {
					var data = JSON.parse(res);
					if(data.success) {
						top.layer.msg(biolims.common.deleteSuccess);
					} else {
						top.layer.msg("删除有误，请联系管理员！")
					}
					top.layer.close(index);
				}
			});
		} else {
			top.layer.msg(biolims.common.deleteSuccess);
			top.layer.close(index);
		}
	});
}
// 获得保存时生产记录的自定义json数据
function saveProductionRecordJson(ele) {
	var lis = ele.find("li");
	var data = [];
	lis.each(function(i, v) {
		var json = {};
		// 判断选择框并转换为数据库字段
		json.label = $(v).find("span").text();
		json.fieldName = $(v).attr("fieldName");
		json.defaultValue = $(v).attr("defaultValue");
		json.type = $(v).attr("type");
		json.readOnly = $(v).attr("readOnlyy");
		json.qualityChecking = $(v).attr("qualityChecking");
		json.required = $(v).attr("requiredd");
		//		var type = $(v).attr("type");
		//		if(type == biolims.common.textBox) {
		//			json[k] = "text";
		//		} else if(type == biolims.common.digitalBox) {
		//			json[k] = "number";
		//		} else if(type == biolims.common.dateTextBox) {
		//			json[k] = "date";
		//		} else if(type == biolims.common.radiobutton) {
		//			json[k] = "radio";
		//		} else if(type == biolims.common.checkBox) {
		//			json[k] = "checkbox";
		//		}
		//		var readOnly = $(v).attr("readOnly");
		//		if(readOnly == biolims.common.yes) {
		//			json[k] = "true";
		//		} else if(readOnly == biolims.common.no) {
		//			json[k] = "false";
		//		}
		//		var qualityChecking = $(v).attr("qualityChecking"); //是否质检
		//		if(qualityChecking == biolims.common.yes) {
		//			json[k] = "true";
		//		} else if(qualityChecking == biolims.common.no) {
		//			json[k] = "false";
		//		}
		//		var required = $(v).attr("required");
		//		if(required == biolims.common.yes) {
		//			json[k] = "true";
		//		} else if(required == biolims.common.no) {
		//			json[k] = "false";
		//		}
		//		var singleoptionId = $(v).attr("singleoption-id").split("/");
		//		var singleoptionName = $(v).attr("singleoption-name").split("/");
		//		var singleOption = [];
		//		singleoptionId.forEach(function(vvv, iii) {
		//			singleOption.push({
		//				itemValue: vvv,
		//				itemName: singleoptionName[iii]
		//			})
		//		});
		//		json.singleOption = singleOption;
		data.push(json);
	});
	return data;
}

//操作指令
function operationInstruction(id, tableId, action) {
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "sort",
		"title": "序号",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "sort");
		}
	});
	colOpts.push({
		"data": "name",
		"title": "操作指令及工艺参数",
		"className": "edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "name");
			if(tableId=="oneCZZL"){
				$(td).attr("operationNote", rowData['operationNote']);
				$(td).parents("tr").attr("note",rowData['operationNote']);
			}
		}
	});
	colOpts.push({
		"data": "templeProduction",
		"title": "生产检查",
		"createdCell": function(td) {
			$(td).attr("saveName", "templeProduction");
		},
		"render": function(data) {
			return "<input type='checkbox' class='mySwitch' />"
		}
	});
	if(tableId=="oneCZZL"){
		colOpts.push({
			"data": "operationNote",
			"title": "操作记录",
			"createdCell": function(td) {
				$(td).attr("saveName", "operationNote");
			},
			"render": function(data, data, rowData) {
				if(tableId=="oneCZZL"){
					if(rowData['operationNote']==""){
						return "<button class='btn btn-xs' onclick='layerNote(this)'><i class='fa fa-pencil-square-o'></i>备注</button>"
					}else{
						return "<button class='btn btn-xs btn-warning' onclick='layerNote(this)'><i class='fa fa-pencil-square-o'></i>备注</button>"
					}
				}else{
					return "<button class='btn btn-xs' onclick='layerNote(this)'><i class='fa fa-pencil-square-o'></i>备注</button>"
				}
			}
		});
	}
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItemOne($("#" + tableId));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChosedItem($("#" + tableId));
		}
	});
	var contentItemOptions = table(true, id, action, colOpts, tbarOpts);
	contentItemTable = renderData($("#" + tableId), contentItemOptions);
	contentItemTable.on('draw', function() {
		$("#" + tableId).find(".mySwitch").each(function() {
			var state = $(this).is(':checked');
			$(this).bootstrapSwitch({
				onText: "是",
				offText: "否",
			}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
		})
	})
	return contentItemTable;
}

//工前准备试剂量
function operationInstructionGqzb(id, tableId, action) {
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "reagentName",
		"title": "试剂名称",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "reagentName");
			$(td).attr("reagentId", rowData['reagentId']);
//			$(td).attr("id", rowData['id']);
			$(td).attr("createDate", rowData['createDate']);
			$(td).attr("itemId", rowData['itemId']);
			$(td).attr("stepId", rowData['stepId']);
			$(td).attr("templateItem", rowData['templateItem']);
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	colOpts.push({
		"data": "reagentNum",
		"title": "准备量",
		"createdCell": function(td) {
			$(td).attr("saveName", "reagentNum");
		},
		"className": "edit",
	});
	colOpts.push({
		"data": "reagentNuit",
		"title": "单位",
		"createdCell": function(td) {
			$(td).attr("saveName", "reagentNuit");
		},
	});
//	if(){
		tbarOpts.push({
			text: "重新获取试剂表",
			action: function() {
				beforeReagentRefresh();
			}
		});
		tbarOpts.push({
			text: "保存",
			action: function() {
				savebeforeReagent(tableId);
			}
		});
//	}
//	tbarOpts.push({
//		text: biolims.common.delSelected,
//		action: function() {
//			removeChosedItem($("#" + tableId));
//		}
//	});
	var contentItemOptionsGqzb = table(true, id, action, colOpts, tbarOpts);
	pageOnetableGqzb = renderData($("#" + tableId), contentItemOptionsGqzb);
	//选择数据并提示
	pageOnetableGqzb.on('draw', function() {
		oldChangeLog = pageOnetableGqzb.ajax.json();
		
	});
	return pageOnetableGqzb;
}
//刷新工前准备试剂准备表
function beforeReagentRefresh(){
	$.ajax({
		type:"post",
		url: "/system/template/templeNew/beforeReagentRefresh.action",
		data:{
			orderNum:"1",
			stepId:"1",
			orderid:$(".wizard_steps .selected").attr("stepid"),
			id:$(".wizard_steps .selected").attr("stepid")//$("#template_id").text().trim(),
		},
		success:function (data) {
			var data=JSON.parse(data);
			if(data.success){
				top.layer.msg("刷新成功！");
				pageOnetableGqzb.ajax.url('/system/template/templeNew/templateBeforeReagentTableJson.action?orderNum=' + $(".wizard_steps .selected .step_no").text() + '&orderid=' + $(".wizard_steps .selected").attr("stepid") + '&stepId=1').load();
			}else{
				top.layer.msg("刷新失败，请联系管理员！");
			}
		}
	});
}
//保存工前准备试剂准备表
function savebeforeReagent(tableId){
	var beforeReagentdata = savebeforeReagentItem($("#"+tableId));
	var changeLogItem = "工前试剂准备：";
	changeLogItem = getChangeLog1(beforeReagentdata, $("#"+tableId), changeLogItem);
	var changeLogItemLast = "";
	if (changeLogItem != "工前试剂准备：") {
		changeLogItemLast = changeLogItem
	}
	$.ajax({
		type:"post",
		url: "/system/template/templeNew/saveBeforeReagent.action",
		data:{
			beforeReagentdata:beforeReagentdata,
			changeLogItem:changeLogItemLast,
			id:$("#template_id").text().trim(),
		},
		success:function (data) {
			var data=JSON.parse(data);
			if(data.success){
				top.layer.msg("保存成功！");
				pageOnetableGqzb.ajax.url('/system/template/templeNew/templateBeforeReagentTableJson.action?orderNum=' + $(".wizard_steps .selected .step_no").text() + '&orderid=' + $(".wizard_steps .selected").attr("stepid") + '&stepId=1').load();
			}else{
				top.layer.msg("保存失败，请联系管理员！");
			}
		}
	});
}
function savebeforeReagentItem(ele){
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");

			if (k == "reagentName") {
				json["reagentId"] = $(tds[j]).attr("reagentId");
				json["createDate"] = $(tds[j]).attr("createDate");
				json["itemId"] = $(tds[j]).attr("itemId");
				json["stepId"] = $(tds[j]).attr("stepId");
				json["templateItem"] = $(tds[j]).attr("templateItem");
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
			}
			
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog1(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

//操作指令添加明细
function addItemOne(ele) {
	$(".dataTables_scrollHead th").off();
	//清除没有内容选项
	ele.find(".dataTables_empty").parent("tr").remove();
	//添加明细
	var ths = ele.find("th");
	var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
	tr.height(32);
	for(var i = 1; i < ths.length; i++) {
		var edit = $(ths[i]).attr("key");
		var saveName = $(ths[i]).attr("saveName");
		if(saveName == "templeProduction") { //开关
			tr.append("<td class=" + edit + " saveName=" + saveName + "><input type='checkbox' class='mySwitch' /></td>");
		} else if(saveName == "operationNote") { //备注
			tr.append("<td class=" + edit + " saveName=" + saveName + "><button class='btn btn-xs'><i class='fa fa-pencil-square-o'></i>备注</button></td>");
		} else {
			tr.append("<td class=" + edit + " saveName=" + saveName + "></td>");
		}
	}
	ele.find("tbody").append(tr);
	ele.find(".mySwitch").each(function() {
		var state = $(this).is(':checked');
		$(this).bootstrapSwitch({
			onText: "是",
			offText: "否",
		}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
	})
	checkall(ele);
}

//操作指令删除选中 
function removeChosedItem(ele) {
	var stepId = $(".bwizard-steps>.active").attr("note");
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.confirm2Del + length + biolims.common.barRecord, {
		icon: 3,
		title: biolims.common.prompt,
		btn: biolims.common.selected
	}, function(index) {
		var arr=[];
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			arr.push(id);
		});
		var action="";
		if(stepId=="4"){//第四步
			action="/system/template/templeNew/delTempleItemNstructionsEnd.action";
		}else{
			action ="/system/template/templeNew/delTempleItemNstructions.action";
		}
		$.ajax({
			type:"post",
			url: action,
			data:{
				ids:JSON.stringify(arr)
			},
			success:function (data) {
				//console.log(data);
				var data=JSON.parse(data);
				if(data.success){
					rows.each(function(i, val) {
						$(val).remove();
					});
					top.layer.msg(biolims.common.deleteSuccess);
					//contentItemTable.ajax.reload();
				}else{
					top.layer.msg("删除有误，请联系管理员！");
					//contentItemTable.ajax.reload();
				}
				top.layer.close(index);
			}
		});
	});

}

//保存操作指令
function saveOperationInstruction(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		console.log($(val).attr("note"));
		if($(val).attr("note")){
			json["operationNote"] = $(val).attr("note");
		}else{
			json["operationNote"] = "";
		}
		console.log(json["operationNote"])
		for(var j = 1; j < tds.length; j++) {
			if(j < 3) {
				var k = $(tds[j]).attr("savename");
				
//				if(k == "name") {
//					json["operationNote"] = $(tds[j]).attr("operationNote");
//				}
				
				json[k] = $(tds[j]).text();
			}
		}
		data.push(json);
	});
	return data;
}