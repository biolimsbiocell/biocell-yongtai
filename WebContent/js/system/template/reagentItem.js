﻿var reagentItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'kitName',
			type:"string"
		});
   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
			name:'itemId',
			type:"string"
		});
	    fields.push({
			name:'sn',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.reagentId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.common.reagentNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.componentName,
		width:45*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'kitName',
		hidden : false,
		header:biolims.common.kitName,
		width:45*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//鼠标单击触发事件 
//	var batchs =new Ext.form.TextField({
//            allowBlank: false
//    });
//	batchs.on('focus', function() {
//		var selectRecord = reagentItemGrid.getSelectionModel();
//		if (selectRecord.getSelections().length > 0) {
//			$.each(selectRecord.getSelections(), function(i, obj) {
//				var code=obj.get("code");
//				loadStorageReagentBuy(code);
//			});
//		}
//	});
//	cm.push({
//		dataIndex:'batch',
//		hidden : false,
//		header:biolims.common.batch,
//		width:20*6,
//		editor:batchs
//	});
//	cm.push({
//		dataIndex:'batch',
//		hidden : false,
//		header:'批次',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.singleDose,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.no
			},{
				id : '1',
				name : biolims.common.yes
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isDetecting,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(isGood),editor: isGood
	});
	cm.push({
		dataIndex:'sn',
		hidden : true,
		header:'sn',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'template-name',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.itemId,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/template/template/showReagentItemListJson.action?id="+ $("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/system/template/template/delReagentItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				deSequencingItemGrid.getStore().commitChanges();
				deSequencingItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = reagentItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							reagentItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : function (){
			//获取选择的数据
			var selectRcords=templateItemGrid.getSelectionModel().getSelections();
			//获取全部数据
			var allRcords=templateItemGrid.store;
			//选中的数量
			var length1=selectRcords.length;
			//全部数据量
			var length2=allRcords.getCount();
			//var selRecord = reagentItemGrid.store;
			if(length2>0){
				if(length1==1){
					var code="";
					$.each(selectRcords, function(i, obj) {
						code=obj.get("code");
					});
					if(code!=null){
//						var ob = reagentItemGrid.getStore().recordType;
//						var p = new ob({});
//						p.isNew = true;
//						p.set("itemId", code);
//						reagentItemGrid.stopEditing();
//						reagentItemGrid.getStore().insert(0, p);
//						reagentItemGrid.startEditing(0, 0);
						showStorageList(code);
					}else{
						message(biolims.common.addTemplateDetail);
						return;
					}								
				}else if(length1>1){
					message(biolims.common.onlyChooseOne);
					return;
				}else{
					message(biolims.common.pleaseSelectData);
					return;
				}
			}else{
				message(biolims.common.theDataIsEmpty);
				return;
			}
		}
	});
	reagentItemGrid=gridEditTable("reagentItemdiv",cols,loadParam,opts);
	$("#reagentItemdiv").data("reagentItemGrid", reagentItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:biolims.common.chosePurchasingReagents,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}
function setStorageReagentBuy(rec){
	
	var gridGrid = $("#reagentItemdiv").data("reagentItemGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}
//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, biolims.common.theInventoryMasterData, url, {
		"Confirm" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					reagentItemGrid.stopEditing();
					var ob = reagentItemGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("kitName",obj.get("kit-name"));
					p.set("itemId",code);
				
					reagentItemGrid.getStore().add(p);	
				});
				reagentItemGrid.startEditing(0, 0);
				
				options.close();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	}, true, options);
}
function selecttemplateFun(){
	var win = Ext.getCmp('selecttemplate');
	if (win) {win.close();}
	var selecttemplate= new Ext.Window({
	id:'selecttemplate',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:400,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TemplateSelect.action?flag=template' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selecttemplate.close(); }  }]  });     selecttemplate.show(); }
	function settemplate(id,name){
		var gridGrid = $("#reagentItemdiv").data("reagentItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('template-id',id);
			obj.set('template-name',name);
		});
		var win = Ext.getCmp('selecttemplate')
		if(win){
			win.close();
		}
	}
	
