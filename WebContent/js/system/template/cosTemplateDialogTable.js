/* 
* 文件名称 :cosTemplateDialogTable.js
* 创建者 : 郭恒开
* 创建日期: 2018/01/25
* 文件描述:选择实验页面设备的datatables
* 
*/
var cosTable;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.tInstrumentState.instrumentCode,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.tInstrumentState.instrumentName,
	});
	colOpts.push({
		"data": "mainType-id",
		"title": biolims.common.instrumentTypeId,
	});
	colOpts.push({
		"data": "mainType-name",
		"title":biolims.common.instrumentType,
	});
	colOpts.push({
		"data": "state-name",
		"title": biolims.common.state,
	});
	colOpts.push({
		"data": "isFull",
		"title": "占用状态",
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "未占用";
			}
			if(data == "1") {
				return "占用";
			}
			else {
				return data;
			}
		}
	});
	colOpts.push({
		"data": "roomManagement-roomName",
		"title": "房间",
	});
	var tbarOpts = [];
	var cosOptions = table(false, $("#cosId").val(),
			'/equipment/main/selectCosJson.action',colOpts , tbarOpts)
	cosTable = renderData($("#addCos"), cosOptions);
//	if($("#roomNameId").val()!=null&&$("#roomNameId").val()!=""){
//		$("#roomName").val($("#roomNameId").val());
//	}
	query();
	$("#addCos").on( 'init.dt', function(e, settings) {
		// 清除操作按钮
		//$("#addCos_wrapper .dt-buttons").empty();
		$('#addCos_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addCos tbody tr");
	cosTable.ajax.reload();
	cosTable.on('draw', function() {
		trs = $("#addCos tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

function query(){
	var id = $("#id").val();
	var searchItemLayerValue = {};
	var k1 = $("#id").attr("searchname");
	if(id != null && id != "") {
		searchItemLayerValue[k1] = "%" + id + "%";
	} else {
		searchItemLayerValue[k1] = id;
	}
	var name = $("#name").val();
	var k2 = $("#name").attr("searchname");
	if(name != null && name != "") {
		searchItemLayerValue[k2] = "%" + name + "%";
	} else {
		searchItemLayerValue[k2] = name;
	}
	var roomName=$("#roomName").val();
	var k3 = $("#roomName").attr("searchname");
	if(roomName != null && roomName != "") {
		searchItemLayerValue[k3] = "%" + roomName + "%";
	} else {
		searchItemLayerValue[k3] = roomName;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query,
		"id":$("#cosId").val()
	};
	sessionStorage.setItem("searchContent", query);
	cosTable.settings()[0].ajax.data = param;
	cosTable.ajax.reload();
}
