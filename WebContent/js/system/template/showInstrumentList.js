function list() {
	window.location = window.ctx + '/system/template/instrument/showInstrumentList.action';
}
function lockList() {
	window.location = window.ctx + '/system/template/instrument/showInstrumentList.action?lock=true';
}
function add() {
	window.location = window.ctx + "/system/template/instrument/toEditInstrument.action";
}
function edit() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/template/instrument/toEditInstrument.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/template/instrument/toViewInstrument.action?id=' + id;
}

function editCopy() {
	var id = "";
	id = document.getElementById("id").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/template/instrument/toEditInstrument.action?copy=true&id=' + id;
}

function normalsearch(gridName) {
	var limit = parseInt((parent.document.body.clientHeight - 250) > 0 ? (parent.document.body.clientHeight - 250) / 25
			: 1);
	if (trim(document.getElementById("limitNum").value) != '') {
		limit = document.getElementById("limitNum").value;

	}
	var fields = [ 'id', 'note' ];
	var fieldsValue = [ document.getElementById('id').value, document.getElementById('note').value ];
	searchAllGrid(gridName, fields, fieldsValue, 0, limit);

}
function workflowSubmit() {
	Ext.onReady(function() {
		Ext.QuickTips.init();
		Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";

		var id = "";
		id = document.getElementById("id").value;
		if (id == "" || id == undefined) {
			message(biolims.common.selectRecord);
			return false;
		}

		var params = "formId : '" + id + "',applicationTypeTableId:'2'";
		params = '({' + params + '})';
		params = eval(params);

		var myMask = new Ext.LoadMask(Ext.getBody(), {
			msg : "Please wait..."
		});
		myMask.show();
		Ext.Ajax.request({
			url : window.ctx + '/workflowEngine/workflowSubmit.action',
			method : 'POST',
			params : params,
			success : function(response) {
				myMask.hide();
				// var respText = Ext.util.JSON.decode(response.responseText);

			},
			failure : function() {
			}

		});

	});
}

$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 450;
		option.height = 300;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(gridGrid);
				$(this).dialog("close");
			},
			"清空(Empty)" : function() {
				form_reset();
			}
		}, false, option);
	});

});