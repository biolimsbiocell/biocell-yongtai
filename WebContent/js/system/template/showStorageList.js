var showStorageListGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'spec',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'studyType-name',
		type:"string"
	});
	   fields.push({
		name:'position-name',
		type:"string"
	});
//	    fields.push({
//		name:'producer-name',
//		type:"string"
//	});
	    fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'unit-name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	   fields.push({
		name:'dutyUser-name',
		type:"string"
	});
	    fields.push({
		name:'useDesc',
		type:"string"
	});
	    fields.push({
		name:'searchCode',
		type:"string"
	});
	    fields.push({
		name:'source-name',
		type:"string"
	});
	   fields.push({
		name:'barCode',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'ifCall',
		type:"string"
	});
	    fields.push({
			name:'kit-id',
			type:"string"
		});
	    fields.push({
			name:'kit-name',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:"组分名称",
		width:45*6
	});
	cm.push({
		dataIndex:'kit-id',
		hidden : true,
		header:"原辅料盒",
		width:45*6
	});
	cm.push({
		dataIndex:'kit-name',
		hidden : false,
		header:"原辅料盒名称",
		width:45*6
	});
	cm.push({
		dataIndex:'spec',
		hidden : true,
		header:biolims.storage.spec,
		width:40*6
	});
	cm.push({
		dataIndex:'type-name',
		hidden : true,
		header:biolims.common.type,
		width:10*6,
	});
	cm.push({
		dataIndex:'studyType-name',
		hidden : true,
		header:biolims.storage.studyType,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'position-name',
		hidden : true,
		header:biolims.common.storageLocalName,
		width:15*10
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.sample.inventory,
		width:10*6,
	});
	cm.push({
		dataIndex:'unit-name',
		hidden : false,
		header:biolims.common.unit,
		width:10*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:40*6
	});
	cm.push({
		dataIndex:'state-name',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
	});
	cm.push({
		dataIndex:'dutyUser-name',
		hidden : true,
		header:biolims.master.personName,
		width:15*10,
	});
	cm.push({
		dataIndex:'useDesc',
		hidden : true,
		header:biolims.storage.useDesc,
		width:15*10
	});
	cm.push({
		dataIndex:'searchCode',
		hidden : true,
		header:"Component Cat No.",
		width:10*6,
	});
	cm.push({
		dataIndex:'source-name',
		hidden : true,
		header:biolims.sample.sourceName,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : true,
		header:"Catalog No.",
		width:40*6
	});
	var ifCall = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.no
			},{
				id : '1',
				name : biolims.common.yes
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'ifCall',
		hidden : true,
		header:biolims.storage.ifCall,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(ifCall),editor: ifCall
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/template/template/showStorageListJson.action";
	loadParam.limit=500;
	var opts={};
	opts.title=biolims.common.theInventoryMasterData;
	opts.height =  document.body.clientHeight*0.6;
	opts.tbar = [];

	showStorageListGrid=gridTable("showStorageListdiv",cols,loadParam,opts);
	$("#showStorageListdiv").data("showStorageListGrid", showStorageListGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
$(function() {
	$("#opensearch").click(function() {
//	function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(showStorageListGrid);
				$(this).dialog("close");
			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
//	}
	});
});