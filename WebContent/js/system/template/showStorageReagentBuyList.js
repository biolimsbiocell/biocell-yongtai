
var showStorageReagentBuyGrid;
$(function(){
	var cols={};
	//cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'serial',
		type:"string"
	});
	   fields.push({
		name:'spec',
		type:"string"
	});
	    fields.push({
		name:'rankType-id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
    	name:'productDate',
    	type:"string"
    });
	    fields.push({
    	name:'expireDate',
    	type:"string"
    });
	    fields.push({
		name:'remindDate',
		type:"string"
	});
	    fields.push({
		name:'inDate',
		type:"string"
	});
	    fields.push({
    	name:'outPrice',
    	type:"string"
    });
	    fields.push({
    	name:'storage-unit-name',
    	type:"string"
    });
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
			name:'useNum',
			type:"string"
		});
	    fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
    	name:'position-id',
    	type:"string"
    });
	    fields.push({
    	name:'position-name',
    	type:"string"
    });
	    fields.push({
    	name:'purchasePrice',
    	type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
	});
//	cm.push({
//		dataIndex:'serial',
//		hidden : true,
//		header:'批号',
//		width:30*6
//	});
	cm.push({
		dataIndex:'spec',
		hidden : true,
		header:biolims.storage.spec,
		width:30*6,
	});
	cm.push({
		dataIndex:'rankType-id',
		hidden:true,
		header:biolims.storage.rankType,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden:false,
		header:biolims.common.batch,
		width:20*6
	});
	cm.push({
		dataIndex:'productDate',
		hidden:true,
		header:biolims.storage.productDate,
		width:20*6
	});
	cm.push({
		dataIndex:'expireDate',
		hidden : false,
		header:biolims.storage.expireDate,
		width:20*6
	});
	cm.push({
		dataIndex:'remindDate',
		hidden : false,
		header:biolims.storage.remindDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'inDate',
		hidden : true,
		header:biolims.storage.inDate,
		width:15*10,
	});
	cm.push({
		dataIndex:'outPrice',
		hidden : true,
		header:biolims.storage.outPrice,
		width:15*10
	});
	cm.push({
		dataIndex:'storage-unit-name',
		hidden : false,
		header:biolims.common.unit,
		width:10*6
	});
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:biolims.storage.outPrice,
		width:20*6,
	});
	cm.push({
		dataIndex:'useNum',
		hidden : false,
		header:biolims.storage.useNum,
		width:20*6,
	});
	cm.push({
		dataIndex:'isGood',
		hidden : true,
		header:biolims.common.isQualified,
		width:15*10,
	});
	cm.push({
		dataIndex:'purchasePrice',
		hidden : true,
		header:biolims.storage.purchasePrice,
		width:15*10
	});
	cm.push({
		dataIndex:'position-id',
		hidden : true,
		header:biolims.common.storageId,
		width:15*10
	});
	cm.push({
		dataIndex:'position-name',
		hidden : true,
		header:biolims.common.storageLocalName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/template/template/showStorageReagentBuyListJson.action?codes="+ $("#codes").val();
	var opts={};
	opts.title=biolims.storage.purchaseAgent;
	opts.height =  document.body.clientHeight;
	opts.tbar = [];

	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setStorageReagentBuy(rec);
	};
	showStorageReagentBuyGrid=gridTable("showStorageReagentBuydiv",cols,loadParam,opts);
	$("#showStorageReagentBuyediv").data("showStorageReagentBuyGrid", showStorageReagentBuyGrid);
});

function disableEnter(event){
	var keyCode = event.keyCode?event.keyCode:event.which?event.which:event.charCode;
	if (keyCode ==13){
		var batch=$("#code").val();
		commonSearchAction(showStorageReagentBuyGrid);
	}
}
	
