var sampleQpcrResultGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
	   name:'orderId',
	   type:"string"
   });
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'fragmentSize',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'qpcr',
		type:"string"
	});
	   fields.push({
		name:'specific',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
	   name:'reason',
	   type:"string"
   });
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-id',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-name',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
		   fields.push({
			name:'i5',
			type:"string"
		});
		   fields.push({
			name:'i7',
			type:"string"
		});
		   fields.push({
				name:'sampleNum',
				type:"string"
			});
		   fields.push({
				name:'tempId',
				type:"string"
			});
		   
		   fields.push({
				name:'her2',
				type:"string"
			});
		   fields.push({
				name:'er',
				type:"string"
			}); 
		   fields.push({
				name:'pr',
				type:"string"
			});
		   fields.push({
				name:'rs',
				type:"string"
			}); 
		   fields.push({
				name:'wkConcentration',
				type:"string"
			});
		   fields.push({
				name:'wkVolume',
				type:"string"
			});
		   fields.push({
				name:'wkSumTotal',
				type:"string"
			});
		   fields.push({
				name:'loopNum',
				type:"string"
			});
		   fields.push({
				name:'pcrRatio',
				type:"string"
			});
		   fields.push({
				name:'rin',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
	});
	cm.push({
		dataIndex:'wkId',
		hidden : true,
		header:biolims.wk.wkCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6,
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'fragmentSize',
		hidden : true,
		header:biolims.pooling.pdLength,
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'qpcr',
		hidden : false,
		header:biolims.QPCR.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'her2',
		hidden : false,
		header:'HER2',
		width:15*6
	});
	cm.push({
		dataIndex:'er',
		hidden : false,
		header:'ER',
		width:15*6
	});
	cm.push({
		dataIndex:'pr',
		hidden : false,
		header:'PR',
		width:15*6
	});
	cm.push({
		dataIndex:'rs',
		hidden : false,
		header:'RS',
		width:15*6
	});
	cm.push({
		dataIndex:'specific',
		hidden : false,
		header:biolims.sample.specific,
		width:15*6
	});
	cm.push({
		dataIndex:'wkConcentration',
		header:biolims.wk.wkConcentration,
		width:20*6
	});
	cm.push({
		dataIndex:'wkVolume',
		header:biolims.wk.wkVolume,
		width:20*6
	});
	cm.push({
		dataIndex:'wkSumTotal',
		header:biolims.wk.wkTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'loopNum',
		header:biolims.wk.loopNum,
		width:20*6
	});
	cm.push({
		dataIndex:'pcrRatio',
		header:biolims.wk.pcrRatio,
		width:20*6
	});
	cm.push({
		dataIndex:'rin',
		header:'RIN',
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:30*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6,
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:biolims.common.idCard,
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:biolims.common.phone,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:20*6,
	});
	var wkTypeCobStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.wk.Quality2100 ], [ '6', '2100 or Caliper' ], [ '1', biolims.wk.QualityQPCR ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCobStore,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:biolims.wk.wkType,
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6
	});
	cm.push({
		dataIndex:'qcQpcrTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'qcQpcrTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'volume',
		header:biolims.common.volume,
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6,
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSampleQPCRInfoListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.QPCR.testResult;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleQpcrResultGrid = gridEditTable("sampleQpcrResultdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
