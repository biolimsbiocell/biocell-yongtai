var sampleSequencingInfoGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'fcCode',
		type:"string"
	});
	   fields.push({
		name:'laneCode',
		type:"string"
	});
	   fields.push({
		name:'machineCode',
		type:"string"
	});
//	   fields.push({
//		name:'molarity',
//		type:"string"
//	});
	   fields.push({
		name:'quantity',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'computerDate',
		type:"string"
	});
	   fields.push({
		name:'pfPercent',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date"
	});
	    fields.push({
		name:'idCard',
		type:"string"
	});
	    fields.push({
		name:'sequenceFun',
		type:"string"
	});
	    fields.push({
		name:'inspectDate',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date"
	});
	    fields.push({
		name:'expectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'sequencing-id',
		type:"string"
	});
	    fields.push({
		name:'sequencing-name',
		type:"string"
	});
    fields.push({
		name:'dataDemand',
		type:"string"
	});
    	fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:biolims.pooling.code,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.sampleName,
		width:20*6
	});
	cm.push({
		dataIndex:'fcCode',
		hidden : false,
		header:biolims.sequencing.fcCode,
		width:20*6
	});
	cm.push({
		dataIndex:'laneCode',
		hidden : false,
		header:biolims.pooling.lane,
		width:20*6
	});
//	cm.push({
//		dataIndex:'machineCode',
//		hidden : false,
//		header:'机器号',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'molarity',
//		hidden : false,
//		header:'摩尔浓度',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'quantity',
		hidden : true,
		header:biolims.sequencing.quantity,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:biolims.sequencing.density,
		width:20*6
	});
	cm.push({
		dataIndex:'computerDate',
		hidden : false,
		header:biolims.sequencing.sequencingDate,
		width:20*6
		
	});
	cm.push({
		dataIndex:'expectDate',
		hidden : false,
		header:biolims.sequencing.expectDate,
		width:20*6,
		sortable:true,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
		
	});
	cm.push({
		dataIndex:'pfPercent',
		hidden : true,
		header:'PF%',
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'method',
		hidden : true,
		header:biolims.common.method,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		header:biolims.common.sampleInfoReportDate,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.idCard,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.acceptDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		hidden:true,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'dataDemand',
		hidden : true,
		header:biolims.sequencing.dataRequest,
		width:20*10
	});
	
	cm.push({
		dataIndex:'contractId',
		header:biolims.common.contractId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:biolims.common.projectId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.common.orderType,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'techTaskId',
		header:biolims.common.taskId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSampleSequencingInfoListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.sequencing.sequencingResult;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleSequencingInfoGrid = gridEditTable("sampleSequencingInfodiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
