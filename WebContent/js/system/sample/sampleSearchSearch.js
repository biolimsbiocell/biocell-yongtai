$(function() {
	$(function() {
			$('input').attr('readonly','readonly')
//			$("#sampleInfo_codee").removeAttr("readonly");
			$("#sampleInfo_samplecodee").removeAttr("readonly");
			console.log(formatTime(null,'Y-M-D'))
			//console.log(sampleCodee)
			$("#sampleInfo_samplecodee").keyup(function(event){
				if(event.keyCode ==13){
					setTimeout(function() {
						$("#sampleInfo_codee").val("");
						$.ajax({
							type: "post",
							data: {
								batch: $("#sampleInfo_samplecodee").val()
							},
							url: ctx + "/sample/sampleSearch/findBatchNumByCellCode.action",
							success: function(data) {
								var dataa = JSON.parse(data) ;
								console.log(data)
								if(dataa.success){
									var onedata = dataa.batch;
									if(onedata!=null && onedata!=""){
										$("#sampleInfo_codee").val(onedata);
										$(".ssearch").trigger("click");
									}else{
										$("#sampleInfo_codee").val("");
										top.layer.msg("该条码未查询到相关批次信息！");
									}
								}else{
									$("#sampleInfo_codee").val("");
									top.layer.msg("查询失败请联系管理员！");
								}
							}
						});
					},500);
				}
			});
			$("#sampleInfo_codee").keyup(function(event){
				if(event.keyCode ==13){
					setTimeout(function() {
						if($("#sampleInfo_codee").val()!=null || $("#sampleInfo_codee").val()!=""){
							$("#sampleInfo_samplecodee").val("");
							$(".ssearch").trigger("click");
						}else{
							top.layer.msg("请扫批次编号！");
						}
					},500);
				}
			});
			$('.ssearch').on('click',function(){
				$("#pch").val("");
				
				if(($("#sampleInfo_codee").val()==null||$("#sampleInfo_codee").val()=="")&&
						$("#sampleInfo_samplecodee").val()!=null&&$("#sampleInfo_samplecodee").val()!=""){
					$.ajax({
						type: "post",
						data: {
							batch: $("#sampleInfo_samplecodee").val()
						},
						async: false,
						url: ctx + "/sample/sampleSearch/findBatchNumByCellCode.action",
						success: function(data) {
							var dataa = JSON.parse(data) ;
							console.log(data)
							if(dataa.success){
								var onedata = dataa.batch;
								if(onedata!=null && onedata!=""){
									$("#sampleInfo_codee").val(onedata);
								}else{
									top.layer.msg("该条码未查询到相关批次信息！");
								}
							}else{
								top.layer.msg("查询失败请联系管理员！");
							}
						}
					});
				}
				
				
				var sampleCodee = $.trim($("#sampleInfo_codee").val());
				console.log(1)
				console.log(sampleCodee)
				if(sampleCodee){
				console.log(sampleCodee)
				$.ajax({
					type: "post",
					data: {
						batch: sampleCodee
					},
					url: ctx + "/sample/sampleSearch/sampleSearchInfo.action",
					success: function(data) {
						var dataa = JSON.parse(data) ;
						console.log(data)
						var onedata = dataa.sampleOrder
						if(onedata){
							$('.y').css('display','block')
							// 状态按钮
							//onedata.batchStateName='2'
							if(onedata.batchState=='0'){
								$('.remrmber').css('display','block')
								$('.remrmbertxt').text('订单录入完成')
								$('.remrmberimg img').attr('src','../../../../images/jlwc2.png')

							}else if(onedata.batchState=='1'){
								$('.remrmber').css('display','block')
								$('.remrmbertxt').text('样本已接收')
								$('.remrmberimg img').attr('src','../../../../images/ybjs.png')

							}else if(onedata.batchState=='2'){
								$('.remrmber').css('display','block')
								$('.remrmbertxt').text('正在生产')
								$('.remrmberimg img').attr('src','../../../../images/zzsc.png')

							}else if(onedata.batchState=='6'){
								$('.remrmber').css('display','block')
								$('.remrmbertxt').text('待放行')
								$('.remrmberimg img').attr('src','../../../../images/fx.png')

							}else if(onedata.batchState=='3'){
								$('.remrmber').css('display','block')
								$('.remrmbertxt').text('已放行')
								$('.remrmberimg img').attr('src','../../../../images/fx.png')

							}else if(onedata.batchState=='4'){
								$('.remrmber').css('display','block')
								$('.remrmbertxt').text('运输完成')
								$('.remrmberimg img').attr('src','../../../../images/yswc.png')

							}else if(onedata.batchState=='5'){
								$('.remrmber').css('display','block')
								$('.remrmbertxt').text('不合格')
								$('.remrmberimg img').attr('src','../../../../images/bhgRed.png')

							}else{
								$('.remrmber').css('display','none')
							}
							//$('.remrmber').css('display','block')
							$('#barcode').val(onedata.barcode)
							$('#round').val(onedata.round)
							$('#ccoi').val(onedata.ccoi)
							$('#productName').val(onedata.productName)
							$('#name').val(onedata.name)
							$('#whiteBloodCellNum').val(onedata.whiteBloodCellNum)
							$('#percentageOfLymphocytes').val(onedata.percentageOfLymphocytes)
							//时间格式化
							if(onedata.startBackTime){
								$('#startBackTime').val(formatTime(onedata.startBackTime,'Y-M-D'))
							}else{
								$('#startBackTime').val('')
							}
							if(onedata.feedBackTime){
								$('#startBackTime').val(formatTime(onedata.feedBackTime,'Y-M-D'))
							}else{
								$('#startBackTime').val('')
							}
							//$('#startBackTime').val(onedata.startBackTime)

							//$('#feedBackTime').val(onedata.feedBackTime)
							
							$('#crmCustomername').val(onedata.crmCustomer.name)
							$('#inspectionDepartment').val(onedata.inspectionDepartment.name)

							$('#sampleOrder').val(onedata.id)
							$('#crmPatient').val(onedata.medicalNumber)
						}else{
							$('.remrmber').css('display','none')
							$('.y').css('display','none')
							$('#barcode').val('')
							$('#round').val('')
							$('#ccoi').val('')
							$('#productName').val('')
							$('#name').val('')
							$('#whiteBloodCellNum').val('')
							$('#percentageOfLymphocytes').val('')
							$('#startBackTime').val('')
							$('#feedBackTime').val('')
							
							$('#crmCustomername').val('')
							$('#inspectionDepartment').val('')

							$('#sampleOrder').val('')
							$('#crmPatient').val('')
						}
						// 接受记录
						var towdata = dataa.sampleReceive
						if(towdata){
							$('#acceptUsername').val(towdata.acceptUser.name)
							
							 //$('#acceptDate').val(towdata.acceptDate)
							 if(towdata.acceptDate){
								$('#acceptDate').val(formatTime(towdata.acceptDate,'Y-M-D'))
							}else{
								$('#acceptDate').val('')
							}

							 $('#equimentNumber').val(towdata.equimentNumber)
							 $('#temperature').val(towdata.temperature)
							 if(towdata.expressCompany){
                                 $('#expressCompanyname').val(towdata.expressCompany.name)
                              }else{
                                 $('#expressCompanyname').val('')
                              }
							
							 $('#courier').val(towdata.courier)
							 $('#sampleReceive').val(towdata.id)
						}else{

							$('#acceptUsername').val('')
							 $('#acceptDate').val('')
							 $('#equimentNumber').val('')
							 $('#temperature').val('')
							 $('#expressCompanyname').val('')
							 $('#courier').val('')
							  $('#sampleReceive').val('')
						}
						
						 //var datar = JSON.parse('{"success":true,"list":[{"flg":0,"id":"4028800c6b411c55016b412b72a7000e","code":"CP00010016-01-190610","sampleCode":"CP00010016-01-190610","productId":"CP0001","productName":"扩增活化的淋巴细胞（EAL）","stageTime":null,"startDate":null,"endDate":"2019-06-10 19:31","tableTypeId":"CellPassage","stageName":"分袋（SP）","acceptUser":{"id":"admin","name":"admin","userPassword":"25d55ad283aa400af464c76d713c07ad","emailPassword":null,"state":{"id":"1z","name":"在职","state":"1","type":"user","note":null,"orderNum":1},"orderNumber":0,"email":"","fileId":null,"phone":"","sex":"","birthday":null,"inDate":null,"address":"","area":"北京","city":"","dicJob":null,"mobile":"","cardNo":"","edu":null,"department":null,"graduate":"","weichatId":null,"contactMethod":"","emergencyContactPerson":"","emergencyContactMethod":"","roles":null,"userGroups":null,"userJobs":null,"password":null,"oldPassword":null,"scopeId":"all","scopeName":"全部"},"taskId":"402880136b1b1563016b1b20f0c00009","taskMethod":"收获（H）","taskResult":"警戒","techTaskId":null,"note":null,"note2":null,"note3":null,"note4":null,"note5":null,"note6":null,"note7":null,"sampleOrder":null},{"flg":1,"id":"4028800c6b411c55016b4120aaa80009","code":"CP00010016-01-190610","sampleCode":"CP00010016-01-190610","productId":"CP0001","productName":"扩增活化的淋巴细胞（EAL）","stageTime":null,"startDate":null,"endDate":"2019-06-10 19:22","tableTypeId":"CellPassage","stageName":"装袋（BP）","acceptUser":{"id":"admin","name":"admin","userPassword":"25d55ad283aa400af464c76d713c07ad","emailPassword":null,"state":{"id":"1z","name":"在职","state":"1","type":"user","note":null,"orderNum":1},"orderNumber":0,"email":"","fileId":null,"phone":"","sex":"","birthday":null,"inDate":null,"address":"","area":"北京","city":"","dicJob":null,"mobile":"","cardNo":"","edu":null,"department":null,"graduate":"","weichatId":null,"contactMethod":"","emergencyContactPerson":"","emergencyContactMethod":"","roles":null,"userGroups":null,"userJobs":null,"password":null,"oldPassword":null,"scopeId":"all","scopeName":"全部"},"taskId":"402880136b127d87016b1280243a0000","taskMethod":"分袋（SP）","taskResult":"警戒","techTaskId":null,"note":null,"note2":null,"note3":null,"note4":null,"note5":null,"note6":null,"note7":null,"sampleOrder":null},{"flg":0,"id":"4028800c6b411c55016b411e89e30005","code":"CP00010016-01-190610","sampleCode":"CP00010016-01-190610","productId":"CP0001","productName":"扩增活化的淋巴细胞（EAL）","stageTime":null,"startDate":null,"endDate":"2019-06-10 19:20","tableTypeId":"CellPassage","stageName":"补液140（+140）","acceptUser":{"id":"admin","name":"admin","userPassword":"25d55ad283aa400af464c76d713c07ad","emailPassword":null,"state":{"id":"1z","name":"在职","state":"1","type":"user","note":null,"orderNum":1},"orderNumber":0,"email":"","fileId":null,"phone":"","sex":"","birthday":null,"inDate":null,"address":"","area":"北京","city":"","dicJob":null,"mobile":"","cardNo":"","edu":null,"department":null,"graduate":"","weichatId":null,"contactMethod":"","emergencyContactPerson":"","emergencyContactMethod":"","roles":null,"userGroups":null,"userJobs":null,"password":null,"oldPassword":null,"scopeId":"all","scopeName":"全部"},"taskId":"402880136b122d37016b1265ef0a002a","taskMethod":"装袋（BP）","taskResult":"警戒","techTaskId":null,"note":null,"note2":null,"note3":null,"note4":null,"note5":null,"note6":null,"note7":null,"sampleOrder":null},{"flg":0,"id":"4028800c6b41170b016b4118c36a0002","code":"CP00010016-01-190610","sampleCode":"CP00010016-01-190610","productId":"CP0001","productName":"扩增活化的淋巴细胞（EAL）","stageTime":null,"startDate":null,"endDate":"2019-06-10 19:14","tableTypeId":"CellPassage","stageName":"补液50（+50）","acceptUser":{"id":"admin","name":"admin","userPassword":"25d55ad283aa400af464c76d713c07ad","emailPassword":null,"state":{"id":"1z","name":"在职","state":"1","type":"user","note":null,"orderNum":1},"orderNumber":0,"email":"","fileId":null,"phone":"","sex":"","birthday":null,"inDate":null,"address":"","area":"北京","city":"","dicJob":null,"mobile":"","cardNo":"","edu":null,"department":null,"graduate":"","weichatId":null,"contactMethod":"","emergencyContactPerson":"","emergencyContactMethod":"","roles":null,"userGroups":null,"userJobs":null,"password":null,"oldPassword":null,"scopeId":"all","scopeName":"全部"},"taskId":"402880136b122d37016b12591543000b","taskMethod":"补液140（+140）","taskResult":"警戒","techTaskId":null,"note":null,"note2":null,"note3":null,"note4":null,"note5":null,"note6":null,"note7":null,"sampleOrder":null},{"flg":0,"id":"4028800c6b4110fa016b41142a030004","code":"CP00010016-01-190610","sampleCode":"CP00010016-01-190610","productId":"CP0001","productName":"扩增活化的淋巴细胞（EAL）","stageTime":null,"startDate":null,"endDate":"2019-06-10 19:09","tableTypeId":"CellPassage","stageName":"外周血单个核细胞分离（BL）","acceptUser":{"id":"admin","name":"admin","userPassword":"25d55ad283aa400af464c76d713c07ad","emailPassword":null,"state":{"id":"1z","name":"在职","state":"1","type":"user","note":null,"orderNum":1},"orderNumber":0,"email":"","fileId":null,"phone":"","sex":"","birthday":null,"inDate":null,"address":"","area":"北京","city":"","dicJob":null,"mobile":"","cardNo":"","edu":null,"department":null,"graduate":"","weichatId":null,"contactMethod":"","emergencyContactPerson":"","emergencyContactMethod":"","roles":null,"userGroups":null,"userJobs":null,"password":null,"oldPassword":null,"scopeId":"all","scopeName":"全部"},"taskId":"402880026b122298016b122a6b670001","taskMethod":"补液50（+50）","taskResult":"警戒","techTaskId":null,"note":null,"note2":null,"note3":null,"note4":null,"note5":null,"note6":null,"note7":null,"sampleOrder":null}]}') ;
						var scdata = dataa.scjl
						console.log('-----------------------------')
						console.log(scdata) //arr
						let objj = {
							"success":true,
							"list":scdata
						}
						console.log(objj)
						//console.log(data.list.length)
						// if(!data.list.length){
						// 	console.log(data.list.length)
						// 	//return false;
						// }
						// 这个是干嘛的？
						//$("#parentWrap").find("span").text(sampleCode);
						
						var result = template("templatee", objj);
						console.log(result)
						setInterval(function(){ $(".text-red").fadeOut(100).fadeIn(100); },1000);
						$("#parentWrap").after(result);
						if(!$('.scjl').hasClass('schight')){
							$('.scjl').addClass('schight')
						}
						if($(".viewSubTable").children(".glyphicon").hasClass('glyphicon-minus')){
							$(".viewSubTable").children(".glyphicon").removeClass('glyphicon-minus')
							$(".viewSubTable").children(".glyphicon").addClass('glyphicon-plus')
						}
						$(".viewSubTable").unbind('click').click(function () {
                            console.log('999eeee')
                            var fonCode=$(this).children(".glyphicon");
                            var corspanTr=$(this).parents("tr").siblings(".subTable");
                            if(fonCode.hasClass("glyphicon-plus")){//展开
                                fonCode.removeClass("glyphicon-plus").addClass("glyphicon-minus");
                                corspanTr.removeClass("hide");
                                $('.scjl').removeClass('schight')
                                var elemen=corspanTr.find("table");
                                if(!elemen.find("tr").length){
                                    var taskId=this.getAttribute("taskId");
                                    //renderSubTable (elemen,sampleCode,taskId);
                                }
                                
                            }else{//关闭
                                $('.scjl').addClass('schight')
                                fonCode.removeClass("glyphicon-minus").addClass("glyphicon-plus");
                            }
                        });
						//放行审核--------------------
						var fxdata = dataa.fxsh
						if(fxdata){
							var releaseAuditId = fxdata.releaseAuditId
							if(releaseAuditId){
								$('#tzbtn').attr('releaseAuditId',releaseAuditId)
								$('#shbtn').attr('releaseAuditId',releaseAuditId)
								$('#cpbgdy').attr('releaseAuditId',releaseAuditId)
								$('#cpbgde').attr('releaseAuditId',releaseAuditId)
								$('#zjcpjy').attr('releaseAuditId',releaseAuditId)
							}else{
								$('#tzbtn').attr('releaseAuditId','')
								$('#shbtn').attr('releaseAuditId','')
								$('#cpbgdy').attr('releaseAuditId',releaseAuditId)
								$('#cpbgde').attr('releaseAuditId',releaseAuditId)
								$('#zjcpjy').attr('releaseAuditId',releaseAuditId)
							}
						}else{
							$('#tzbtn').attr('releaseAuditId','')
							$('#shbtn').attr('releaseAuditId','')
							$('#cpbgdy').attr('releaseAuditId',releaseAuditId)
							$('#cpbgde').attr('releaseAuditId',releaseAuditId)
							$('#zjcpjy').attr('releaseAuditId',releaseAuditId)
						}
						
						
						// 运输安排------------------------
							var yundata = dataa.ysap
							console.log(yundata)
							if(yundata){
								$('#expresscrmCustomer').val(yundata.sampleOrder.crmCustomer.name)
								if(yundata.transportState==1){
									$('#transportState').val('已发送')
								}else{
									$('#transportState').val('未发送')
								}
								$('#transportOrderid').val(yundata.transportOrder.id)

								//$('#transportDate').val(yundata.transportOrder.transportDate)
								if(yundata.transportDate){
									$('#transportDate').val(formatTime(yundata.transportDate,'Y-M-D h:m'))
								}else{
									$('#transportDate').val('')
								}

								//$('#ytransportDate').val(yundata.transportOrder.transportDate2)
								if(yundata.transportDate2){
									$('#ytransportDate').val(formatTime(yundata.transportDate2,'Y-M-D h:m'))
								}else{
									$('#ytransportDate').val('')
								}

								//$('#signConfirmationDate').val(yundata.transportOrder.signConfirmationDate)
								if(yundata.confirmationDate){
									$('#signConfirmationDate').val(formatTime(yundata.confirmationDate,'Y-M-D h:m'))
								}else{
									$('#signConfirmationDate').val('')
								}
								

							}else{
								$('#transportState').val('')
								$('#transportOrderid').val('')
								$('#transportDate').val('')
								$('#ytransportDate').val('')
								$('#signConfirmationDate').val('')
								$('#expresscrmCustomer').val('')
							}

					// 生产偏差 渲染
					if(dataa.pc){
						dataa.pc.forEach(element => {
							if(element.fssj){
								element.fssj = formatTime(element.fssj,'Y-M-D')
							}else{
								element.fssj=''
							}
							
						});
					}
						
						var pcdata = dataa.pc
						console.log(pcdata)
						
						let pcobj = {
							"success":true,
							"list":pcdata
						}
						//pcdata=null
						if(pcdata){
							var resultt = template("templateee", pcobj)
							console.log(resultt)
							$('#my').html(resultt)
						}else{
							var resulttt = `<div class="row">
							<div class="col-xs-2">
									<div class="input-group">
										<span class="input-group-addon">无偏差</span>
										
										
									</div>
							</div></div>`
							$('#my').html(resulttt)
						}
						
					}
				});
			}
			})
			
			if($("#pch").val()!=""){
				$("#sampleInfo_codee").val($("#pch").val());
				$(".ssearch").trigger("click");
			}
			
			
		})
		function formatNumber (n) {
			n = n.toString()
			return n[1] ? n : '0' + n;
		}
		// 参数number为毫秒时间戳，format为需要转换成的日期格式
		function formatTime (number, format) {
			let time = new Date(number)
			let newArr = []
			let formatArr = ['Y', 'M', 'D', 'h', 'm', 's']
			newArr.push(time.getFullYear())
			newArr.push(formatNumber(time.getMonth() + 1))
			newArr.push(formatNumber(time.getDate()))
		
			newArr.push(formatNumber(time.getHours()))
			newArr.push(formatNumber(time.getMinutes()))
			newArr.push(formatNumber(time.getSeconds()))
		
			for (let i in newArr) {
				format = format.replace(formatArr[i], newArr[i])
			}
			return format;
		}
});
//弹出新窗口显示电子病历
function showCrmPatient(){
	var sDate =new Date()
	sDate =dateFtt("yyyy-MM-dd",sDate);
	window.open(window.ctx+"/crm/customer/patient/editCrmPatientNew.action?open=Y&id="+window.btoa($("#crmPatient").val())+"&barCod="+window.btoa($("#sampleInfo_codee").val())+"&st="+window.btoa(sDate));
}
//弹出新窗口显示申请单
function showSampleOrder(){
	window.open(window.ctx+"/system/sample/sampleOrder/editSampleOrder.action?open=Y&id="+$("#sampleOrder").val()+"&type=0&barCod="+$("#sampleInfo_codee").val());
}
//弹出新窗口显示样本接收
function showSampleReceive(){
	var sDate =new Date()
	sDate =dateFtt("yyyy-MM-dd",sDate);
	window.open(window.ctx+'/sample/sampleReceive/editSampleReceiveNew.action?id=' +window.btoa($("#sampleReceive").val()) +'&type=0&barCod='+$("#sampleInfo_codee").val()+"&st="+window.btoa(sDate));
}
//实验步骤批处理记录
function printTempalte(that) {
	var id = $(that).attr("cid");
	var orderNum = $(that).attr("ordernum");
	var mc = $(that).attr("mc");
	// 判断当前步骤是否是重复步骤
	$.ajax({
		type : "post",
		url : ctx
				+ "/experiment/cell/passage/cellPassage/getStepChongfu.action",
		data : {
			id : id,
			orderNum : orderNum,
			mc:mc,
			barCod:$("#sampleInfo_codee").val(),
		},
		success : function(data) {
			var data = JSON.parse(data);
			if (data.success) {
				if (data.chongfu) {
					var url = '__report=templateitemYC.rptdesign&id=' + id
							+ '&ordernum=' + orderNum;
					commonPrint(url);
				} else {
					var url = '__report=templateitem.rptdesign&id=' + id
							+ '&ordernum=' + orderNum;
					commonPrint(url);
				}
			} else {
				top.layer.msg("当前步骤数据存在问题，请联系管理员");
			}
		}
	});

}

function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '', '');
}

//打印成品检验报告单（一）
function showcpjyy(that){
	var barcode =$('#barcode').val();
	
	$.ajax({
		type : "post",
		data : {
			modelId:"yccpjybgdone",
			barCod:barcode,
			buttonName:$(that).text().trim(),
			
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirtv.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				var url = '__report='+data.reportN+'&batch=' + barcode;
				commonPrint(url);
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
	
//	if($(that).attr("releaseauditid")==null
//			||$(that).attr("releaseauditid")==""){
//		
//	}else{
//		var url = '__report=cpjybgdy.rptdesign&id=' + $(that).attr("releaseauditid");
//		commonPrint(url);
//	}
}
//成品检验报告单（二）
function showcpjye(that){
	var barcode =$('#barcode').val();
	$.ajax({
		type : "post",
		data : {
			modelId:"yccpjybgdtwo",
			barCod:barcode,
			buttonName:$(that).text().trim(),
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirtv.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				var url = '__report='+data.reportN+'&batch=' + barcode;
				commonPrint(url);
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
	
	
	
//	if($(that).attr("releaseauditid")==null
//			||$(that).attr("releaseauditid")==""){
//		
//	}else{
//		var url = '__report=cpjybgd2.rptdesign&id=' + $(that).attr("releaseauditid");
//		commonPrint(url);
//	}
}
//中间产品安全性检验报告单
function showzjcpjy(that){
	var barcode =$('#barcode').val();
	
	$.ajax({
		type : "post",
		data : {
			modelId:"yczjcpjybgd",
			barCod:barcode,
			buttonName:$(that).text().trim(),
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirtv.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				var url = '__report='+data.reportN+'&batch=' + barcode;
				commonPrint(url);
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
//	if($(that).attr("releaseauditid")==null
//			||$(that).attr("releaseauditid")==""){
//		
//	}else{
//		var url = '__report=cpaqxjcNew.rptdesign&id=' + $(that).attr("releaseauditid");
//		commonPrint(url);
//	}
}
//打印成品放行通知单
function showfxtz(that){
	var barcode =$('#barcode').val();
	$.ajax({
		type : "post",
		data : {
			modelId:"yccpfxtzd",
			barCod:barcode,
			buttonName:$(that).text().trim(),
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirtv.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				if($(that).attr("releaseauditid")==null
						||$(that).attr("releaseauditid")==""){
					
				}else{
				var url = '__report='+data.reportN+'&id=' + $(that).attr("releaseauditid");
				commonPrint(url);
				}
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
	
//	if($(that).attr("releaseauditid")==null
//			||$(that).attr("releaseauditid")==""){
//		
//	}else{
//		var url = '__report=cpfxtzd.rptdesign&id=' + $(that).attr("releaseauditid");
//		commonPrint(url);
//	}
}
//打印成品放行审核单
function showfxsh(that){
	var barcode =$('#barcode').val();
	$.ajax({
		type : "post",
		data : {
			modelId:"yccpfxshd",
			barCod:barcode,
			buttonName:$(that).text().trim(),
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirtv.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				if($(that).attr("releaseauditid")==null
						||$(that).attr("releaseauditid")==""){
					
				}else{
				var url = '__report='+data.reportN+'&id=' + $(that).attr("releaseauditid");
				commonPrint(url);
				}
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
	
//	if($(that).attr("releaseauditid")==null
//			||$(that).attr("releaseauditid")==""){
//		
//	}else{
//		var url = '__report=cpfxshd.rptdesign&id=' +  $(that).attr("releaseauditid");
//		commonPrint(url);
//	}
}
//跳转到偏差处理
function showpccl(that){
	if($(that).attr("pid")==null
			||$(that).attr("pid")==""){
		
	}else{
		window.open(window.ctx +
				"/deviation/deviationHandlingReport/editDeviationHandlingReport.action?id=" + $(that).attr("pid")+"&barCod="+$("#sampleInfo_codee").val(), '', '');
	}
}
//质检结果
function zhijianjieguo(that){
	top.layer.open({
		title: "查看质检结果",
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: [window.ctx +"/sample/sampleSearch/showQualityTestTable.action?code="+$(that).attr("cid")+"&ordernum="+$(that).attr("ordernum")+"&barCod="+$("#sampleInfo_codee").val(),''],
		yes: function(index, layer) {
			top.layer.close(index)	
		},
	});
}
function list() {
	window.location = window.ctx +
		'/sample/sampleSearchList/showSampleSearch.action';
}

function dateFtt(fmt,date) 
{ //author: meizz 
 var o = { 
 "M+" : date.getMonth()+1,     //月份 
 "d+" : date.getDate(),     //日 
 "h+" : date.getHours(),     //小时 
 "m+" : date.getMinutes(),     //分 
 "s+" : date.getSeconds(),     //秒 
 "q+" : Math.floor((date.getMonth()+3)/3), //季度 
 "S" : date.getMilliseconds()    //毫秒 
 }; 
 if(/(y+)/.test(fmt)) 
 fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length)); 
 for(var k in o) 
 if(new RegExp("("+ k +")").test(fmt)) 
 fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length))); 
 return fmt; 
}
