var sampleInItemInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'checked',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'classify',
			type:"string"
		});
	   fields.push({
			name:'upLocation',
			type:"string"
		});
	   
	   
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});	   
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
		name:'sampleIn-id',
		type:"string"
	});
	    fields.push({
		name:'sampleIn-name',
		type:"string"
	});
	    fields.push({
			name:'sampleTypeId',
			type:"string"
		});  
	   fields.push({
			name:'infoFrom',
			type:"string"
		});  
	   fields.push({
			name:'patientName',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.sample.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:30*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'checked',
		hidden : true,
		header:biolims.common.checked,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		header:biolims.sample.num,
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:biolims.common.location,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'upLocation',
		hidden : true,
		header:biolims.sample.upLocation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.sample.tempId,
		width:30*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleIn-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleIn-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cm.push({
		dataIndex:'sampleTypeId',
		hidden : true,
		header:biolims.common.sampleTypeId,
		width:20*6
	});
	cm.push({
		dataIndex:'infoFrom',
		hidden : true,
		header:biolims.sample.infoFrom,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/sample/sampleMain/showSampleInItemListJson.action?id=" + $("#sampleInfo_code").val();
	loadParam.limit=1000;
	var opts={};
	opts.title=biolims.sample.warehousingDetail;
	opts.height =  document.body.clientHeight-320;
	opts.tbar = [];
	sampleInItemInfoGrid=gridEditTable("sampleInItemInfoDiv",cols,loadParam,opts);
	$("#sampleInItemInfoDiv").data("sampleInItemInfoGrid", sampleInItemInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
	
