var qualityTestShowTab;
var qualityTestShowChangeLog;
$(function() {
	
	
	
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "code",
		"title" : "样本编号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "code");
			$(td).attr("qualityInfoId", rowData['qualityInfoId']);
		}
	})
	colOpts.push({
		"data" : "sampleType",
		"title" : "样本名称",
	})
	colOpts.push({
		"data" : "sampleNum",
		"title" : "检验量",
	})
	colOpts.push({
		"data" : "sampleNumUnit",
		"title" : "单位",
	})
	colOpts.push({
		"data" : "batch",
		"title" : "产品批号 ",
	})
	colOpts.push({
		"data" : "sampleOrder-name",
		"title" : "姓名 ",
	})
	colOpts.push({
		"data" : "sampleOrder-filtrateCode",
		"title" : "筛选号 ",
	})
//	colOpts.push({
//		"data" : "orderCode",
//		"title" : "订单编号",
//	})
//	colOpts.push({
//		"data" : "productName",
//		"title" : "检测项目",
//	})
	colOpts.push({
		"data" : "cellType",
		"title" : "检测方式",
		// "className":"select",
		"name" : "|自主检测|第三方检测",
		"createdCell" : function(td) {
			$(td).attr("saveName", "cellType");
			$(td).attr("selectOpt", "|自主检测|第三方检测");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "自主检测";
			}
			if (data == "5") {
				return "第三方检测";
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : "sampleDeteyion-name",
		"title" : "检测项",
	});
//	colOpts.push({
//		"data" : "stepNum",
//		"title" : "步骤",
//	});
//	colOpts.push({
//		"data" : "experimentalStepsName",
//		"title" : "步骤名称",
//	});
	colOpts.push({
		"data" : "submit",
		"title" : "质检是否提交",
		// "className":"select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "submit");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			}
			if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : "qualitySubmitTime",
		"title" : "质检提交时间",
	});
	colOpts.push({
		"data" : "qualitySubmitUser",
		"title" : "质检提交人",
	})
	colOpts.push({
		"data" : "qualityReceiveTime",
		"title" : "质检接收时间",
	});
	colOpts.push({
		"data" : "qualityFinishTime",
		"title" : "质检完成时间",
	});
	colOpts.push({
		"data" : "qualified",
		"title" : biolims.common.isQualified,
		// "className": "select",
		"name" : biolims.common.qualified + "|" + biolims.common.disqualified
				+ "|" + "警戒",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "qualified");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified + "|" + "警戒");
			if (rowData['qualified'] == "1") {
				$(td).parent().addClass("qualified");
			}
			if (rowData['qualified'] == "3") {
				$(td).parent().addClass("qualified");// .css('color','red');
			}
		},
		"render" : function(data, type, full, meta) {
			if (data == "2") {
				return biolims.common.qualified;
			}
			if (data == "1") {
				return biolims.common.disqualified;
			}
			if (data == "3") {
				return "警戒";
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "qualityInfoId",
		"title" : "质检结果id",
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "qualityInfoId");
		},
	})
	colOpts.push({
		"data" : "note",
		"title" : "备注",
	})
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
	}
	var qualityTestShowTabOptions = table(true, null, '/sample/sampleSearch/showQualityTestTableJson.action?id='+$("#flag").val()+"&ordernum="+$("#ordernum1").val(),
			colOpts, tbarOpts)
	qualityTestShowTab = renderData($("#addDicTypeTable"), qualityTestShowTabOptions);
	qualityTestShowTab.on('draw', function() {
		qualityTestShowChangeLog = qualityTestShowTab.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});
//function chakan(that){
//	var itemid = $(that).parent("td").parent("tr").children("td").find(".icheck").eq(0).val();
//	var jiance = $(that).parent("td").parent("tr").children("td").eq(10).attr("sampleDeteyion-id");
//	top.layer.open({
//		title:"查看检测结果",
//		type:2,
//		area:[document.body.clientWidth-50,document.body.clientHeight-50],
//		btn: biolims.common.selected,
//		content:[window.ctx+"/experiment/quality/qualityTest/showQualityTestResultItemTable.action?itemid="+itemid+"&jiance="
//					+ jiance+"&ly=Z",''],
//		yes: function(index, layero) {
//			top.layer.close(index)
//		},
//	})
//}