var sampleOrderInfoGrid;
$(function(){
	var cols={};
	cols.sm = false;
    var fields=[];
    fields.push({
		name : 'id',
		type : "string"
	});
    fields.push({
		name:'primaryTask-id',
		type:"string"
	});
	    fields.push({
		name:'primaryTask-name',
		type:"string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'location',
		type : "string"
	});
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	
	fields.push({
		name : 'orderNum',
		type : "string"
	});
	fields.push({
		name : 'patientId',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
//	//添加订单
//	fields.push({
//		name : 'sampleOrder-id',
//		type : "string"
//	});
//	fields.push({
//		name : 'sampleOrder-name',
//		type : "string"
//	});
	fields.push({
		name:'dicType-id',
		type:"string"
	});
	    fields.push({
		name:'dicType-name',
		type:"string"
	});
    fields.push({
		name:'sampleStage',
		type:"string"
	});
    fields.push({
		name : 'project-id',
		type : "string"
	});
	fields.push({
		name : 'project-name',
		type : "string"
	});
	fields.push({
			name : 'personShip-id',
			type : "string"
		});
	fields.push({
		name : 'personShip-name',
		type : "string"
	});
	fields.push({
		name : 'idCard',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	//产前
	fields.push({
		name : 'receiveDate',
		type : "string"
	});
	fields.push({
		name : 'si-area',
		type : "string"
	});
	fields.push({
		name : 'receiveDate',
		type : "string"
	});
	fields.push({
		name : 'hospital',
		type : "string"
	});
	fields.push({
		name : 'inspectDate',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'si-doctor',
		type : "string"
	});
	fields.push({
		name : 'si-inHosNum',
		type : "string"
	});
	
	fields.push({
		name : 'sampleType-id',
		type : "string"
	});
	fields.push({
		name : 'sampleType-name',
		type : "string"
	});
	
	fields.push({
		name : 'age',
		type : "string"
	});
	fields.push({
		name : 'weight',
		type : "string"
	});
	fields.push({
		name : 'si-gestationalAge',
		type : "string"
	});
	fields.push({
		name : 'si-voucherType-id',
		type : "string"
	});
	fields.push({
		name : 'si-voucherType-name',
		type : "string"
	});
	fields.push({
		name : 'si-voucherCode',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'familyAddress',
		type : "string"
	});
	fields.push({
		name : 'si-endMenstruationDate',
		type : "date",
		dateFormat:"Y-m-d"
	});
	fields.push({
		name : 'si-gestationIVF',
		type : "string"
	});
	fields.push({
		name : 'si-pregnancyTime',
		type : "string"
	});
	fields.push({
		name : 'si-parturitionTime',
		type : "string"
	});
	fields.push({
		name : 'si-badMotherhood',
		type : "string"
	});
	fields.push({
		name : 'si-organGrafting',
		type : "string"
	});
	fields.push({
		name : 'si-outTransfusion',
		type : "string"
	});
	fields.push({
		name : 'si-firstTransfusionDate',
		type : "date",
		dateFormat:"Y-m-d"
	});
	fields.push({
		name : 'si-stemCellsCure',
		type : "string"
	});
	 fields.push({
		 name:'si-immuneCure',
		 type:"string"
	 });
	 fields.push({
		 name:'si-endImmuneCureDate',
		 type:"date",
		 dateFormat:"Y-m-d"
	 });
	 fields.push({
		 name:'si-embryoType',
		 type:"string"
	 });
	 fields.push({
		 name:'si-NT',
		 type:"string"
	 });
	 fields.push({
		 name:'si-reason',
		 type:"string"
	 });
	 fields.push({
		 name:'si-testPattern',
		 type:"string"
	 });
	 fields.push({
		 name:'si-trisome21Value',
		 type:"string"
	 });
	 fields.push({
		 name:'si-trisome18Value',
		 type:"string"
	 });
	 fields.push({
		 name:'si-coupleChromosome',
		 type:"string"
	 });
	 fields.push({
		 name:'si-reason2',
		 type:"string"
	 });
	 fields.push({
		 name:'si-diagnosis',
		 type:"string"
	 });
	 fields.push({
		 name:'si-medicalHistory',
		 type:"string"
	 });
	fields.push({
		name : 'isInsure',
		type : "string"
	});
	fields.push({
		name : 'isFee',
		type : "string"
	});
	fields.push({
		name : 'privilegeType',
		type : "string"
	});
	fields.push({
		name : 'si-linkman-id',
		type : "string"
	});
	fields.push({
		name : 'si-linkman-name',
		type : "string"
	});
	 fields.push({
		 name:'si-isInvoice',
		 type:"string"
	 });
	fields.push({
		name : 'si-paymentUnit',
		type : "string"
	});
	fields.push({
		name : 'createUser1',
		type : "string"
	});
	fields.push({
		name : 'createUser2',
		type : "string"
	});
	fields.push({
		name : 'si-suppleAgreement',
		type : "string"
	});
	fields.push({
		name : 'si-money',
		type : "string"
	});
	fields.push({
		name : 'si-receiptType-id',
		type : "string"
	});
	fields.push({
		name : 'si-receiptType-name',
		type : "string"
	});
	fields.push({
		name : 'upLoadAccessory-id',
		type : "string"
	});
	fields.push({
		name : 'upLoadAccessory-name',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	fields.push({
		name : 'sampleType2',
		type : "string"
	});
	fields.push({
		name : 'samplingDate',
		type : "string"
	});
	fields.push({
		name : 'unit',
		type : "string"
	});
	fields.push({
		name : 'sampleNote',
		type : "string"
	});
	fields.push({
		name : 'dicTypeName',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.common.id,
		width : 30 * 6,
		sortable:true
	});
	cm.push({
		dataIndex:'primaryTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
	});
	cm.push({
		dataIndex:'primaryTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	cm.push({
		dataIndex : 'code',
		hidden : false,
		header : biolims.common.code,
		width : 20 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'sampleType2',
		hidden : false,
		header : biolims.common.sampleType,
		width : 20 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'sampleType-id',
		hidden : true,
		header : biolims.common.sampleTypeId,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'sampleType-name',
		hidden : true,
		header : biolims.common.sampleTypeName,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'name',
		hidden : true,
		header : biolims.common.sampleName,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'patientName',
		hidden : false,
		header :biolims.common.patientName,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.projectId,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.projectName,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : biolims.common.state,
		width : 10 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'stateName',
		hidden : true,
		header : biolims.common.stateName,
		width : 20 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'note',
		hidden : true,
		header : biolims.common.note,
		width : 50 * 6,
		sortable:true
	});	
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'dicTypeName',
		header:biolims.sample.samplingType,
		width:20*10,
		sortable:true
		});
	var sampleStagestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'I' ], [ '2', 'II' ],[ '3', 'III' ],[ '4', 'IV' ]]
	});
	var sampleStageComboxFun = new Ext.form.ComboBox({
		store : sampleStagestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'sampleStage',
		header:biolims.sample.sampleStage,
		width:10*6,
		hidden : true,
		renderer: Ext.util.Format.comboRenderer(sampleStageComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'project-id',
		hidden:true,
		header:biolims.sample.projectId,
		width:20*10,
		sortable:true
		});
	cm.push({
	dataIndex:'project-name',
	header:biolims.sample.projectName,
	hidden : true,
	width:20*10,
	sortable:true
	});
	cm.push({
		dataIndex:'personShip-id',
		hidden:true,
		header:biolims.sample.personShipId,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'personShip-name',
		header:biolims.sample.personShipName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex : 'sampleNum',
		hidden : false,
		header : biolims.common.sampleNum,
		width : 15 * 6
	});
//	cm.push({
//		dataIndex : 'unit',
//		hidden : false,
//		header : '单位',
//		width : 15 * 6
//	});
	var unitstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx
					+ '/dic/unit/dicUnitTypeCobJson.action?type=weight',
			method : 'POST'
		})
	});
	unitstore.load();
	var unitcob = new Ext.form.ComboBox({
		store : unitstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'unit',
		header : biolims.common.unit,
		width : 100,
		renderer : Ext.util.Format.comboRenderer(unitcob)
//		editor : unitcob
	});
	cm.push({
		dataIndex : 'sampleNote',
		hidden : false,
		header : biolims.sample.sampleNote,
		width : 30 * 6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/sample/sampleOrder/showSampleOrderInfoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.sampleOrderInformation;
	opts.height =  document.body.clientHeight*0.5;
	opts.tbar = [];
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	sampleOrderInfoGrid=gridEditTable("sampleOrderInfodiv",cols,loadParam,opts);
	$("#sampleOrderInfodiv").data("sampleOrderInfoGrid", sampleOrderInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
	ajaxBysampleOrderItem();
});
