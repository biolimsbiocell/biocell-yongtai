var sampleMainGrid;
$(function() {
	$("#btn_edit").hide();
	$("#btn_add").hide();
	
	var fields = [];
	fields.push({
		"data": "batchStateName",
		"title": "批次状态",
	});
	fields.push({
		"data": "id",
		"title": "订单编号",
		"visible": true,
	});
	fields.push({
		"data": "barcode",
		"title": "产品批号",
		"visible": true,
		"createdCell" : function(td) {
			$(td).attr("saveName", "barcode");
		}
	});
	fields.push({
		"data": "name",
		"title": "姓名",
		"visible": true,
	});
	fields.push({
		"data": "round",
		"title": "采血轮次",
		"visible": true,
	});
	fields.push({
		"data": "filtrateCode",
		"title": "筛选号",
		"visible": true,
	});
	fields.push({
		"data": "randomCode",
		"title": "随机号",
		"visible": true,
	});
	fields.push({
		"data": "ccoi",
		"title": "CCOI",
		"visible": true,
	});
	fields.push({
		"data": "medicalNumber",
		"title": "电子病历编号",
		"visible": true,
	});
	fields.push({
		"data": "productName",
		"title": "产品名称",
		"visible": true,
	});
	fields.push({
		"data": "crmCustomer-name",
		"title": "医疗机构",
		"visible": true,
	});
	
	
	
	


	var options = table(true, "", ctx + '/sample/sampleSearchList/showSampleSearchJson.action',
		fields, null);
	sampleMainGrid = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		if($("#style").val()!=null
				&&$("#style").val()!=""){
			var searchItemLayerValue = {};
			searchItemLayerValue["style"] = $("#style").val();
			searchItemLayerValue["time"] = $("#time").val();
			var query = JSON.stringify(searchItemLayerValue);
			sessionStorage.setItem("searchContent", query);
		}
		recoverSearchContent(sampleMainGrid);
	})
});
//// 新建
//function add() {
//	window.location = window.ctx +
//		"/storage/toEditStorage.action?p_type=" + $("#p_type").val();
//}
// 编辑
//function edit() {
//	var id = $(".selected").find("input").val();
//	if(id == "" || id == undefined) {
//		top.layer.msg(biolims.common.selectRecord);
//		return false;
//	}
//	window.location = window.ctx +
//		"/system/sample/sampleMain/editSampleMain.action?id=" + id;
//}
// 查看
function view() {
	var id = $(".selected").find("td[savename='barcode']").text();
	if(id == "" || id == undefined) {
		top.layer.msg("请选择有批号的数据！");
		return false;
	}
	window.location = window.ctx +
		"/sample/sampleSearchList/showSampleSearchEdit.action?searchnum=" + id;
}
//弹框模糊查询参数
function searchOptions() {
	var fields = [];
	fields.push({
		"searchName": "barcode",
		"type": "input",
		"txt": "产品批号"
	});
	fields.push({
		"searchName": "name",
		"type": "input",
		"txt": "姓名"
	});
	fields.push({
		"searchName": "filtrateCode",
		"txt": "筛选号",
		"type": "input",
	});
	fields.push({
		"searchName": "randomCode",
		"txt": "随机号",
		"type": "input",
	});
	fields.push({
		"searchName": "ccoi",
		"txt": "CCOI",
		"type": "input",
	});	
	fields.push({
		"searchName": "crmCustomer-name",
		"txt": "医疗机构",
		"type": "input",
	});
	/*	fields.push({
		"searchName": "style",
		"txt": "类型",
		"options":"|细胞总数量|在培养数量|已完成细胞数量|问题细胞数量",
		"changeOpt":"|1|2|3|4",
		"type": "select",
	});
	fields.push({
		"searchName": "time",
		"txt": "年（格式yyyy）",
		"type": "input",
	});*/

	fields.push({
		"type": "table",
		"table": sampleMainGrid
	});
	return fields;
}