var sampleOrderTable;
$(function() {
	var colData = [{
			"data": "id",
			"title": "编码",
		}, {
			"data": "acceptDate",
			"title": biolims.wk.approverDate,
		}, {
			"data": "name",
			"title": biolims.common.sname,
		}, {
			"data": "note",
			"title": biolims.common.note,
		}, {
			"data": "createUser-id",
			"title": biolims.sample.createUserId,
		}, {
			"data": "createUser-name",
			"title": biolims.sample.createUserName,
		}, {
			"data": "stateName",
			"title": biolims.common.state,
		}, {
			"data": "createDate",
			"title": biolims.tStorage.createDate,
		}, {
			"data": "changeType",
			"title": biolims.common.typeName,
		}];
		
		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "SampleOrderChange"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});
		
		
	var options = table(true, "",
		"/system/sample/sampleOrderChange/showSampleOrderChangeTableJson.action",colData , null)
	sampleOrderTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleOrderTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/system/sample/sampleOrderChange/editsampleOrderChange.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/sample/sampleOrderChange/editsampleOrderChange.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/sample/sampleOrderChange/viewsampleOrderChange.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{

			"txt": biolims.common.gender,
			"type": "select",
			"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
			"changeOpt": "''|1|0|3",
			"searchName": "gender"
		},
		{
			"txt": biolims.wk.confirmUserName,
			"type": "top.layer",
			"searchName": "confirmUser",
			"action": "confirmUser()"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"txt": biolims.common.confirmDateStart,
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt":biolims.common.confirmDateEnd,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": sampleOrderTable
		}
	];
}
