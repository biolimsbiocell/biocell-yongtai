$(function() {
	var sampleOrder = $("#sampleOrder_id").val();
	$.ajax({
		type: "post",
		data: {
			sampleOrder: sampleOrder
		},
		url: ctx + "/system/sample/newSampleState/selectSampleStateOneForOrder.action",
		success: function(data) {
			var data = JSON.parse(data);
			if(!data.list.length){
				return false;
			}
			$("#parentWrap").find("span").text(sampleOrder);
			var result = template("template", data);
			$("#parentWrap").after(result);
			$(".viewSubTable").click(function () {
				var fonCode=$(this).children(".glyphicon");
				var corspanTr=$(this).parents("tr").siblings(".subTable");
				if(fonCode.hasClass("glyphicon-plus")){//展开
					fonCode.removeClass("glyphicon-plus").addClass("glyphicon-minus");
					corspanTr.removeClass("hide");
					var elemen=corspanTr.find("table");
					if(!elemen.find("tr").length){
						var taskId=this.getAttribute("taskId");
						renderSubTable (elemen,sampleOrder,taskId);
					}
				}else{//关闭
					corspanTr.addClass("hide");
					fonCode.removeClass("glyphicon-minus").addClass("glyphicon-plus");
				}
			});
		}
	});
})
function renderSubTable (elemen,sampleOrder,taskId) {
	var colData = [{
			"data": "id",
			"title": biolims.common.id,
			"visible": false,
		}, {
			"data": 'code',
			"title":  biolims.common.code,
		}, {
			"data": 'sampleCode',
			"title":  biolims.common.sampleCode,
		}, {
			"data": 'productName',
			"title": biolims.common.productName,
		}
//		, {
//			"data": 'stageTime',
//			"title": biolims.common.startTime,
//		}
		, {
			"data": 'startDate',
			"title": biolims.common.startTime,
		}, {
			"data": 'endDate',
			"title": biolims.common.endTime,
		}, {
			"data": 'stageName',
			"title":biolims.sample.stageName,
		}, {
			"data":'acceptUser-name',
			"title":  biolims.master.operUserName,
		}, {
			"data":'taskMethod',
			"title":biolims.sample.taskMethod,
		}, {
			"data": 'taskResult',
			"title":  biolims.sample.taskResult,
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return biolims.common.disqualified;
				}
				if(data == "1") {
					return biolims.common.qualified;
				}else {
					return '';
				}
			}
		}, {
			"data": 'taskId',
			"title": biolims.sample.taskId,
		}, {
			"data": 'note',
			"title": biolims.sample.note,
		}];
		var options = table(false,sampleOrder,
		"/system/sample/newSampleState/showSampleStateListJsonForOrder.action?taskId="+taskId,colData , null);
		console.log(elemen);
		renderData(elemen, options);
}