var SampleReceiveShowTab;
var SampleReceiveShowTabChangeLog;
$(function() {
	// 加载子表
	var colOpts = [];
	colOpts.push({
		"data" : 'id',
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "sampleCode",
		"title" : biolims.common.code,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data" : 'name',
		"visible" : false,
		"title" : biolims.common.packetNumber,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	})
	colOpts.push({
		"data" : 'orderCode',
		"width" : "100px",
		"title" : biolims.common.orderCode, // 订单编号 科研隐藏
		"createdCell" : function(td) {
			$(td).attr("saveName", "orderCode");
		}
	})
	colOpts.push({
		"data" : "barCode",
		"title" : biolims.common.barCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "barCode");
		}
	})
	colOpts.push({
		"data" : "sequenceFun",
		"title" : biolims.common.sequencingFun,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sequenceFun");
		}
	})
	colOpts.push({
		"data" : "productId",
		"title" : biolims.master.productId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data" : "productName",
		"title" : biolims.master.product,
		"width" : "100px",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "productName");
			$(td).attr("productId", rowData['productId']);
		}
	})
	colOpts.push({
		"data" : "gender",
		"title" : biolims.common.gender,
		"name" : biolims.common.male + "|" + biolims.common.female + "|"
				+ biolims.common.unknown,
		"createdCell" : function(td) {
			$(td).attr("saveName", "gender");
			$(td).attr(
					"selectOpt",
					biolims.common.male + "|" + biolims.common.female + "|"
							+ biolims.common.unknown);
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return biolims.common.female;
			}
			if (data == "1") {
				return biolims.common.male;
			}
			if (data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "patientName",
		"title" : biolims.common.patientName
				+ '<img src="/images/required.gif"/>',
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientName");
		}
	})
	colOpts.push({
		"data" : "dicSampleType-id",
		"title" : biolims.common.sampleTypeId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data" : "dicSampleType-name",
		"title" : biolims.common.sampleType
				+ '<img src="/images/required.gif"/>',
		"width" : "80px",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleType-name");
			$(td).attr("dicSampleType-id", rowData['dicSampleType-id']);
		}
	})
	colOpts.push({
		"data" : "samplingDate",
		"title" : biolims.sample.samplingDate,
		width : "200px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "samplingDate");
		}
	})
	colOpts.push({
		"data" : "dicTypeName",
		"title" : biolims.sample.samplingLocationName,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dicTypeName");
		}
	})
	colOpts.push({
		"data" : "analysisType",
		"title" : biolims.common.analysisType,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "analysisType");
		}
	})
	colOpts.push({
		"data" : "sampleNum",
		"title" : biolims.common.sampleNum
				+ '<img src="/images/required.gif"/>',
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleNum");
		}
	})
	colOpts.push({
		"data" : "unitGroup-id",
		"title" : biolims.common.unitGroupId,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unitGroup-id");
		}
	})
	colOpts.push({
		"data" : "unitGroup-name",
		"title" : biolims.common.unitGroup,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "unitGroup-name");
			$(td).attr("unitGroup-id", rowData['unitGroup-id']);
		}
	})
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "unit");
		}
	})
	colOpts.push({
		"data" : "unitNote",
		"title" : biolims.common.unitNote,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unitNote");
		}
	})
	colOpts.push({
		"data" : "idCard",
		"title" : biolims.common.outCode,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "idCard");
		}
	})
	colOpts.push({
		"data" : "EMRId",
		"title" : biolims.sample.medicalNumber,
		"createdCell" : function(td) {
			$(td).attr("saveName", "EMRId");
		}
	})
	colOpts.push({
		"data" : "labCode",
		"title" : biolims.common.actualExternalNumber,
		"createdCell" : function(td) {
			$(td).attr("saveName", "labCode");
		}
	})
	colOpts.push({
		"data" : "acceptDate",
		"title" : biolims.common.acceptDate,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "acceptDate");
		}
	})
	colOpts.push({
		"data" : "bloodColourTest",
		"title" : "血样颜色检验 ",
		"name" : "正常" + "|" + "鲜红" + "|"+"暗红",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodColourTest");
			$(td).attr("selectOpt",
					"正常" + "|" + "鲜红" + "|"+"暗红");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "正常";
			}
			if (data == "1") {
				return "鲜红";
			} else if(data == "2"){
				return '暗红';
			}else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "bloodColourNotes",
		"title" : "血样颜色检验备注 ",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodColourNotes");
		}
	})
	colOpts.push({
		"data" : "coagulationTest",
		"title" : "是否凝血 ",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "coagulationTest");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "coagulationNotes",
		"title" : "凝血检验备注 ",
		"createdCell" : function(td) {
			$(td).attr("saveName", "coagulationNotes");
		}
	})
	colOpts.push({
		"data" : "sealingBloodVessels",
		"title" : "采血管是否发送渗漏 ",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sealingBloodVessels");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "sealingBloodNotes",
		"title" : "采血管密封检验备注 ",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sealingBloodNotes");
		}
	})
	colOpts.push({
		"data" : "bloodVesselTime",
		"title" : "采血管是否在有效期内 ",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodVesselTime");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "bloodTime",
		"title" : "采血管有效期 ",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodTime");
		}
	})
	colOpts.push({
		"data" : "stainedWith",
		"title" : "是否用沾有1：100杀狍子剂的无尘纸擦拭采血管表面 ",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "stainedWith");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "other",
		"title" : "其他 ",
		"createdCell" : function(td) {
			$(td).attr("saveName", "other");
		}
	})
	colOpts.push({
		"data" : "otherInformationNotes",
		"title" : "其他情况说明 ",
		"createdCell" : function(td) {
			$(td).attr("saveName", "otherInformationNotes");
		}
	})
	colOpts.push({
		"data" : "isGood",
		"title" : biolims.common.isGood,
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "isGood");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return biolims.common.yes;
			}
			if (data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "method",
		"title" : biolims.common.result + '<img src="/images/required.gif"/>',
		"name" : biolims.common.qualified + "|" + biolims.common.disqualified,
		"createdCell" : function(td) {
			$(td).attr("saveName", "method");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified);
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return biolims.common.qualified;
			}
			if (data == "0") {
				return biolims.common.disqualified;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "nextFlowId",
		"title" : biolims.common.nextFlowId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
				"data" : "nextFlow",
				"title" : biolims.common.nextFlow
						+ '<img src="/images/required.gif"/>',
				"width" : "170px",
				"createdCell" : function(td, data, rowData) {
					$(td).attr("saveName", "nextFlow");
					$(td).attr("nextFlowId", rowData['nextFlowId']);
				}
			})
	colOpts.push({
		"data" : "unusual-id",
		"title" : biolims.sample.unusualName,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "unusual-id");
		}
	})
	colOpts.push({
		"data" : "location",
		"visible" : false,
		"title" : biolims.sample.expreceCode,
		"createdCell" : function(td) {
			$(td).attr("saveName", "location");
		}
	})
	colOpts.push({
		"data" : "advice",
		"visible" : false,
		"title" : biolims.common.method,
		"createdCell" : function(td) {
			$(td).attr("saveName", "advice");
		}
	})
	colOpts.push({
		"data" : "isFull",
		"visible" : false,
		"title" : biolims.sample.isFull,
		"createdCell" : function(td) {
			$(td).attr("saveName", "isFull");
		}
	})
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"width" : "170px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	})

	
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
	}
	var SampleReceiveShowTabOptions = table(true, $("#sampleInfo_orderNum").val(), '/system/sample/sampleMain/showSampleReceiveFromOrderNo.action',
			colOpts, tbarOpts)
	SampleReceiveShowTab = renderData($("#sampleReceiveShow"), SampleReceiveShowTabOptions);
	SampleReceiveShowTab.on('draw', function() {
		SampleReceiveShowTabChangeLog = SampleReceiveShowTab.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});