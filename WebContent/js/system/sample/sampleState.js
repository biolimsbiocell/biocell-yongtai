var sampleStateGrid;
var sampleStateChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#sampleInfo_code").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false,
		"className": "edit"
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		},
		"visible": true
	});
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		},
		"visible": true
	});
	colOpts.push({
		"data": "stageTime",
		"title": biolims.sample.stageTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "stageTime");
		},
		"visible": false
	});
	colOpts.push({
		"data": "startDate",
		"title": biolims.common.startTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "startDate");
		},
		"visible": true
	});
	
	colOpts.push({
		"data": "endDate",
		"title": biolims.common.endTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "endDate");
		},
		"visible": true
	});
	colOpts.push({
		"data": "tableTypeId",
		"title": biolims.sample.stageId,
		"createdCell": function(td) {
			$(td).attr("saveName", "tableTypeId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "stageName",
		"title": biolims.sample.stageName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stageName");
		},
		"visible": true
	});
	colOpts.push({
		"data": "acceptUser-id",
		"title": biolims.master.operUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptUser-id");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "acceptUser-name",
		"title": biolims.master.operUserName,
		"visible": true
	});
	
	colOpts.push({
		"data": "taskMethod",
		"title": biolims.sample.taskMethod,
		"createdCell": function(td) {
			$(td).attr("saveName", "taskMethod");
		},
		"visible": true
	});
	colOpts.push({
		"data": "taskResult",
		"title": biolims.sample.taskResult,
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}else {
				return data;
			}
		}
//		"createdCell": function(td) {
//			$(td).attr("saveName", "taskResult");
//			$(td).attr("selectOpt", "|不合格|合格");
//		},
//		"visible": true,
//		"className": "select",
//		"name": "|不合格|合格",
//		"render": function(data, type, full, meta) {
//			if(data == "不合格") {
//				return '0';
//			}
//			if(data == "合格") {
//				return '1';
//			}else {
//				return '';
//			}
//		}
	});
	colOpts.push({
		"data": "taskId",
		"title": biolims.sample.taskId,
		"createdCell": function(td) {
			$(td).attr("saveName", "taskId");
		},
		"visible": true
	});
	colOpts.push({
		"data": "note",
		"title": biolims.sample.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		},
		"visible": true
	});
	
	var handlemethod = $("#handlemethod").val();
//	if(handlemethod != "view") {
//		tbarOpts.push({
//			text: biolims.common.fillDetail,
//			action: function() {
//				addItem($("#sampleStateGrid"))
//			}
//		});
//		tbarOpts.push({
//			text: biolims.common.addwindow,
//			action: function() {
//				addItemLayer($("#sampleStateGrid"))
//			}
//		});
//		tbarOpts.push({
//			text: biolims.common.Editplay,
//			action: function() {
//				editItemLayer($("#sampleStateGrid"))
//			}
//		});
//		tbarOpts.push({
//			text: biolims.common.save,
//			action: function() {
//				saveStorageReagentBuySerial($("#sampleStateGrid"));
//			}
//		});
//	}

	var sampleStateOptions = table(true,
		id,
		'/system/sample/sampleMain/showSampleStateNewListJson.action', colOpts, tbarOpts);
	sampleStateGrid = renderData($("#sampleStateGrid"), sampleStateOptions);
	sampleStateGrid.on('draw', function() {
		sampleStateChangeLog = sampleStateGrid.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});

// 保存
//function saveStorageReagentBuySerial(ele) {
//	var data = saveStorageReagentBuySerialjson(ele);
//	var ele = $("#sampleStateGrid");
//	var changeLog = "样本状态：";
//	changeLog = getChangeLog(data, ele, changeLog);
//
//	$.ajax({
//		type: 'post',
//		url: '/storage/saveStorageReagentBuySerialTable.action',
//		data: {
//			id: $("#storage_id").val(),
//			dataJson: data,
//			changeLog: changeLog
//		},
//		success: function(data) {
//			var data = JSON.parse(data)
//			if(data.success) {
//				top.layer.msg(biolims.common.saveSuccess);
//				tableRefresh();
//			} else {
//				top.layer.msg(biolims.common.saveFailed)
//			};
//		}
//	})
//}
// 获得保存时的json数据
function saveStorageReagentBuySerialjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字

			if(k == "storage-name") {
				json["storage-id"] = $(tds[j]).attr("storage-id");
				continue;
			}

			if(k == "costCenter-name") {
				json["costCenter-id"] = $(tds[j]).attr("costCenter-id");
				continue;
			}

			if(k == "position-name") {
				json["position-id"] = $(tds[j]).attr("position-id");
				continue;
			}

			if(k == "storageInItem-name") {
				json["storageInItem-id"] = $(tds[j]).attr("storageInItem-id");
				continue;
			}

			if(k == "regionType-name") {
				json["regionType-id"] = $(tds[j]).attr("regionType-id");
				continue;
			}

			if(k == "linkStorageItem-name") {
				json["linkStorageItem-id"] = $(tds[j]).attr("linkStorageItem-id");
				continue;
			}

			if(k == "rankType-name") {
				json["rankType-id"] = $(tds[j]).attr("rankType-id");
				continue;
			}

			if(k == "currencyType-name") {
				json["currencyType-id"] = $(tds[j]).attr("currencyType-id");
				continue;
			}

			if(k == "timeUnit-name") {
				json["timeUnit-id"] = $(tds[j]).attr("timeUnit-id");
				continue;
			}

			if(k == "unit-name") {
				json["unit-id"] = $(tds[j]).attr("unit-id");
				continue;
			}

			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		sampleStateChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}