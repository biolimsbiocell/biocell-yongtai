var sampleWkInforRNAGrid;
$(function() {
	
	var cols = {};
	cols.sm = true;
	 var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
  });
 fields.push({
		name:'phone',
		type:"string"
	});
 fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
  fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   
	   fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
  fields.push({
		name:'orderType',
		type:"string"
	});
  fields.push({
		name:'jkTaskId',
		type:"string"
	});
  fields.push({
		name:'classify',
		type:"string"
	});
  fields.push({
		name:'sampleType',
		type:"string"
	});
  fields.push({
		name:'i5',
		type:"string"
	});
  fields.push({
		name:'i7',
		type:"string"
	});
  fields.push({
		name:'concentration',
		type:"string"
	});
  fields.push({
		name:'loopNum',
		type:"string"
	});
fields.push({
		name:'sumTotal',
		type:"string"
	});
fields.push({
		name:'pcrRatio',
		type:"string"
	});
fields.push({
		name:'expectNum',
		type:"string"
	});
fields.push({
		name:'sampleNum',
		type:"string"
	});
fields.push({
		name:'tempId',
		type:"string"
	});

fields.push({
		name:'rin',
		type:"string"
	});
fields.push({
		name:'nsConcentration',
		type:"string"
	});
fields.push({
		name:'nsVolume',
		type:"string"
	});
fields.push({
		name:'nsSumTotal',
		type:"string"
	});
fields.push({
		name:'qcConcentration',
		type:"string"
	});
fields.push({
		name:'qcVolume',
		type:"string"
	});
fields.push({
		name:'qcSumTotal',
		type:"string"
	});
fields.push({
		name:'qcYield',
		type:"string"
	});
fields.push({
		name:'temperature',
		type:"string"
	});
fields.push({
		name:'time',
		type:"string"
	});
fields.push({
		name:'wkConcentration',
		type:"string"
	});
fields.push({
		name:'wkVolume',
		type:"string"
	});
fields.push({
		name:'wkSumTotal',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : true,
		header:biolims.wk.wkCode,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'rin',
		hidden : false,
		header:'RIN',
		width:20*6
	});
	cm.push({
		dataIndex:'nsConcentration',
		header:biolims.wk.nsConcentration,
		width:20*6
	});
	cm.push({
		dataIndex:'nsVolume',
		hidden : false,
		header:biolims.wk.nsVolume,
		width:20*6
	});
	cm.push({
		dataIndex:'nsSumTotal',
		header:biolims.wk.nsSumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'qcConcentration',
		header:biolims.wk.rRNAqcConcentration,
		width:20*6
	});
	cm.push({
		dataIndex:'qcVolume',
		hidden : false,
		header:biolims.wk.rRNAqcVolume,
		width:20*6
	});
	cm.push({
		dataIndex:'qcSumTotal',
		header:biolims.wk.rRNASumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'qcYield',
		header:biolims.wk.rRNAqcYield,
		width:20*6
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.wk.temperature,
		width:20*6
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.wk.time,
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		header:biolims.wk.cDNAConcentration,
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.wk.cDNAVolume,
		width:20*6
	});
	cm.push({
		dataIndex:'sumTotal',
		header:biolims.wk.cDNASumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'loopNum',
		hidden : false,
		header:biolims.wk.loopNum,
		width:20*6
	});
	cm.push({
		dataIndex:'wkConcentration',
		header:biolims.wk.wkConcentration,
		width:20*6
	});
	cm.push({
		dataIndex:'wkVolume',
		hidden : false,
		header:biolims.wk.wkVolume,
		width:20*6
	});
	cm.push({
		dataIndex:'wkSumTotal',
		header:biolims.wk.wkTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'pcrRatio',
		hidden : false,
		header:biolims.wk.pcrRatio,
		width:20*6
	});
	cm.push({
		dataIndex:'expectNum',
		header:biolims.wk.expectNum,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:biolims.common.idCard,
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:biolims.common.phone,
		width:20*6
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		header:biolims.common.contractId,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:biolims.common.projectId,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.common.orderType,
		width:20*6,
		sortable:true,
		hidden : true
	});
	cm.push({
		dataIndex:'jkTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6
	});
	cm.push({
		dataIndex:'true',
		hidden : true,
		header:biolims.common.counts,
		width:20*6
	});
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6,
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSampleWkInforRNAListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.wk.rRNALibResult;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleWkInforRNAGrid = gridEditTable("sampleWkInforRNAdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
