var sampleMainGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'location',
		type : "string"
	});
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	
	fields.push({
		name : 'orderNum',
		type : "string"
	});
	fields.push({
		name : 'patientId',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
//	//添加订单
//	fields.push({
//		name : 'sampleOrder-id',
//		type : "string"
//	});
//	fields.push({
//		name : 'sampleOrder-name',
//		type : "string"
//	});
	fields.push({
		name:'dicType-id',
		type:"string"
	});
	    fields.push({
		name:'dicType-name',
		type:"string"
	});
    fields.push({
		name:'sampleStage',
		type:"string"
	});
    fields.push({
		name : 'project-id',
		type : "string"
	});
	fields.push({
		name : 'project-name',
		type : "string"
	});
	fields.push({
			name : 'personShip-id',
			type : "string"
		});
	fields.push({
		name : 'personShip-name',
		type : "string"
	});
	fields.push({
		name : 'idCard',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	//产前
	fields.push({
		name : 'receiveDate',
		type : "string"
	});
	fields.push({
		name : 'si-area',
		type : "string"
	});
	fields.push({
		name : 'receiveDate',
		type : "string"
	});
	fields.push({
		name : 'hospital',
		type : "string"
	});
	fields.push({
		name : 'inspectDate',
		type : "string"
	});
	
	fields.push({
		name : 'si-doctor',
		type : "string"
	});
	fields.push({
		name : 'si-inHosNum',
		type : "string"
	});
	
	fields.push({
		name : 'sampleType-id',
		type : "string"
	});
	fields.push({
		name : 'sampleType-name',
		type : "string"
	});
	
	fields.push({
		name : 'age',
		type : "string"
	});
	fields.push({
		name : 'weight',
		type : "string"
	});
	fields.push({
		name : 'si-gestationalAge',
		type : "string"
	});
	fields.push({
		name : 'si-voucherType-id',
		type : "string"
	});
	fields.push({
		name : 'si-voucherType-name',
		type : "string"
	});
	fields.push({
		name : 'si-voucherCode',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'familyAddress',
		type : "string"
	});
	fields.push({
		name : 'si-endMenstruationDate',
		type : "date",
		dateFormat:"Y-m-d"
	});
	fields.push({
		name : 'si-gestationIVF',
		type : "string"
	});
	fields.push({
		name : 'si-pregnancyTime',
		type : "string"
	});
	fields.push({
		name : 'si-parturitionTime',
		type : "string"
	});
	fields.push({
		name : 'si-badMotherhood',
		type : "string"
	});
	fields.push({
		name : 'si-organGrafting',
		type : "string"
	});
	fields.push({
		name : 'si-outTransfusion',
		type : "string"
	});
	fields.push({
		name : 'si-firstTransfusionDate',
		type : "date",
		dateFormat:"Y-m-d"
	});
	fields.push({
		name : 'si-stemCellsCure',
		type : "string"
	});
	 fields.push({
		 name:'si-immuneCure',
		 type:"string"
	 });
	 fields.push({
		 name:'si-endImmuneCureDate',
		 type:"date",
		 dateFormat:"Y-m-d"
	 });
	 fields.push({
		 name:'si-embryoType',
		 type:"string"
	 });
	 fields.push({
		 name:'si-NT',
		 type:"string"
	 });
	 fields.push({
		 name:'si-reason',
		 type:"string"
	 });
	 fields.push({
		 name:'si-testPattern',
		 type:"string"
	 });
	 fields.push({
		 name:'si-trisome21Value',
		 type:"string"
	 });
	 fields.push({
		 name:'si-trisome18Value',
		 type:"string"
	 });
	 fields.push({
		 name:'si-coupleChromosome',
		 type:"string"
	 });
	 fields.push({
		 name:'si-reason2',
		 type:"string"
	 });
	 fields.push({
		 name:'si-diagnosis',
		 type:"string"
	 });
	 fields.push({
		 name:'si-medicalHistory',
		 type:"string"
	 });
	fields.push({
		name : 'isInsure',
		type : "string"
	});
	fields.push({
		name : 'isFee',
		type : "string"
	});
	fields.push({
		name : 'privilegeType',
		type : "string"
	});
	fields.push({
		name : 'si-linkman-id',
		type : "string"
	});
	fields.push({
		name : 'si-linkman-name',
		type : "string"
	});
	 fields.push({
		 name:'si-isInvoice',
		 type:"string"
	 });
	fields.push({
		name : 'si-paymentUnit',
		type : "string"
	});
	fields.push({
		name : 'createUser1',
		type : "string"
	});
	fields.push({
		name : 'createUser2',
		type : "string"
	});
	fields.push({
		name : 'si-suppleAgreement',
		type : "string"
	});
	fields.push({
		name : 'si-money',
		type : "string"
	});
	fields.push({
		name : 'si-receiptType-id',
		type : "string"
	});
	fields.push({
		name : 'si-receiptType-name',
		type : "string"
	});
	fields.push({
		name : 'upLoadAccessory-id',
		type : "string"
	});
	fields.push({
		name : 'upLoadAccessory-name',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	fields.push({
		name : 'sampleType2',
		type : "string"
	});
	fields.push({
		name : 'samplingDate',
		type : "string"
	});
	fields.push({
		name : 'unit',
		type : "string"
	});
	fields.push({
		name : 'sampleNote',
		type : "string"
	});
	fields.push({
		name : 'dicTypeName',
		type : "string"
	});
	
	fields.push({
		name : 'crmCustomer-id',
		type : "string"
	});
	fields.push({
		name : 'crmCustomer-name',
		type : "string"
	});
	fields.push({
		name : 'crmDoctor-id',
		type : "string"
	});
	fields.push({
		name : 'crmDoctor-name',
		type : "string"
	});
	fields.push({
		name : 'species',
		type : "string"
	});
	fields.push({
		name : 'sellPerson-id',
		type : "string"
	});
	fields.push({
		name : 'sellPerson-name',
		type : "string"
	});
	fields.push({
		name : 'cardNumber',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'sendReportDate',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.common.id,
		width : 30 * 6,
		sortable:true
	});
	
	cm.push({
		dataIndex : 'code',
		hidden : false,
		header : biolims.common.code,
		width : 20 * 6,
		sortable:true
	});
	
	
	cm.push({
		dataIndex : 'sampleType2',
		hidden : true,
		header : biolims.common.sampleType,
		width : 20 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'sampleType-id',
		hidden : true,
		header : biolims.common.sampleTypeId,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'sampleType-name',
		hidden : false,
		header : biolims.common.sampleType,
		width : 20 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : biolims.common.state,
		width : 10 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'stateName',
		hidden : false,
		header : biolims.common.stateName,
		width : 20 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'orderNum',
		hidden : false,
		header : biolims.sample.orderNum,
		width : 30 * 6,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.outCode,//外部样本编号
		width:20*6,
		hidden : false
	});
	cm.push({
		dataIndex:'cardNumber',
		header:biolims.common.actualExternalNumber,//实际外部号
		width:20*6,
		hidden : false
	});
	cm.push({
		dataIndex : 'name',
		hidden : false,
		header : biolims.common.packetNumber,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'patientName',
		hidden : false,
		header :biolims.common.patientName,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.projectId,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header :biolims.common.productName,
		width : 25 * 6,
		sortable:true
	});
	cm.push({
		dataIndex:'project-id',
		hidden:true,
		header:biolims.sample.projectId,
		width:20*10,
		sortable:true
		});
	cm.push({
	dataIndex:'project-name',
	header:biolims.sample.projectName,
	hidden:false,
	width:20*6,
	sortable:true
	});
	cm.push({
		dataIndex:'crmCustomer-id',
		hidden:true,
		header:biolims.common.customId,
		width:20*10,
		sortable:true
		});
	cm.push({
	dataIndex:'crmCustomer-name',
	header:biolims.common.custom,
	hidden:false,
	width:20*10,
	sortable:true
	});
	
	cm.push({
		dataIndex:'crmDoctor-id',
		hidden:true,
		header:biolims.sample.crmDoctorId,
		width:20*10,
		sortable:true
		});
	cm.push({
	dataIndex:'crmDoctor-name',
	header:biolims.sample.crmDoctorName,
	hidden:false,
	width:20*6,
	sortable:true
	});
	cm.push({
		dataIndex:'sellPerson-id',
		hidden:true,
		header:biolims.common.salesId,
		width:20*10,
		sortable:true
		});
	cm.push({
	dataIndex:'sellPerson-name',
	header:biolims.common.sales,
	hidden:false,
	width:20*6,
	sortable:true
	});
	cm.push({
		dataIndex:'species',
		header:biolims.wk.species,
		hidden:false,
		width:20*10,
		sortable:true
		});
	
	cm.push({
		dataIndex : 'patientId',
		hidden : false,
		header : biolims.report.patientId,
		width : 30 * 6,
		sortable:true
	});
	cm.push({
		dataIndex : 'note',
		hidden : true,
		header : biolims.common.note,
		width : 50 * 6,
		sortable:true
	});	
	cm.push({
		dataIndex : 'location',
		hidden : false,
		header : biolims.common.location,
		width : 50 * 6,
		sortable:true
	});

	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'dicTypeName',
		header:biolims.sample.samplingType,
		width:20*10,
		sortable:true
		});
	var sampleStagestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'I' ], [ '2', 'II' ],[ '3', 'III' ],[ '4', 'IV' ]]
	});
	var sampleStageComboxFun = new Ext.form.ComboBox({
		store : sampleStagestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'sampleStage',
		header:biolims.sample.sampleStage,
		width:10*6,
		hidden:true,
		renderer: Ext.util.Format.comboRenderer(sampleStageComboxFun),				
		sortable:true
	});
	
	cm.push({
		dataIndex:'personShip-id',
		hidden:true,
		header:biolims.sample.personShipId,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'personShip-name',
		header:biolims.sample.personShipName,
		width:20*10,
		sortable:true
		});

	cm.push({
		dataIndex : 'sampleNum',
		hidden : false,
		header : biolims.common.sampleNum,
		width : 15 * 6
	});
//	cm.push({
//		dataIndex : 'unit',
//		hidden : false,
//		header : '单位',
//		width : 15 * 6
//	});
	var unitstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'name'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx
					+ '/dic/unit/dicUnitTypeCobJson.action?type=weight',
			method : 'POST'
		})
	});
	unitstore.load();
	var unitcob = new Ext.form.ComboBox({
		store : unitstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'unit',
		header : biolims.common.unit,
		width : 100,
		renderer : Ext.util.Format.comboRenderer(unitcob),
		editor : unitcob
	});
	cm.push({
		dataIndex : 'sampleNote',
		hidden : false,
		header : biolims.sample.sampleNote,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'sendReportDate',
		hidden : false,
		header : biolims.report.sendDate,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'reportDate',
		hidden : false,
		header :biolims.common.reportDate,
		width : 30 * 6
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx+ "/system/sample/sampleMain/showSampleMainListJson.action";
	var opts = {};
	
	opts.title = biolims.sample.sampleMasterData;
	opts.height = document.body.clientHeight-35;
	opts.tbar = [];
    opts.delSelect = function(ids) {
		ajax("post", "/crm/customer/linkman/delCrmLinkManItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				crmLinkManItemGrid.getStore().commitChanges();
				crmLinkManItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.sample.updatePatientId,
		handler : updatePatientId
		/*editor : writingPersonOne*/
	});
	opts.tbar.push({
		text :biolims.sample.updateProject,
		handler : updateProjectId
		/*editor : writingPersonOne*/
	});
	opts.tbar.push({
		text :biolims.sample.updateCustom,
		handler : updateCrm
		/*editor : writingPersonOne*/
	});
	
	opts.tbar.push({
		text : biolims.sample.updateDoctor,
		handler : updateDocter
		/*editor : writingPersonOne*/
	});
	
	
	
	
	//怎么得到ID
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){	
		console.log(arguments)
		$('#selectId').val(id);
		edit();
	};
	sampleMainGrid = gridTable("bat_sampleMain_div", cols, loadParam, opts);
//	opts.tbar = [];
//		function goInExcelcsv() {
//			var file = document.getElementById("file-uploadcsv").files[0];
//			var n = 0;
//			var ob = sampleMainGrid.getStore().recordType;
//			var reader = new FileReader();
//			reader.readAsText(file, 'GB2312');
//			reader.onload = function(f) {
//				var csv_data = $.simple_csv(this.result);
//				$(csv_data).each(function() {
//					if (n > 0) {
//						if (this[0]) {
//							var p = new ob({});
//							p.isNew = true;
//							var o;
//							o = 0 - 1;
//							p.set("po.fieldName", this[o]);
//
//							o = 1 - 1;
//							p.set("po.fieldName", this[o]);
//
//							o = 2 - 1;
//							p.set("po.fieldName", this[o]);
//
//							o = 3 - 1;
//							p.set("po.fieldName", this[o]);
//
//							o = 4 - 1;
//							p.set("po.fieldName", this[o]);
//
//							o = 5 - 1;
//							p.set("po.fieldName", this[o]);
//
//							o = 6 - 1;
//							p.set("po.fieldName", this[o]);
//
//							o = 7 - 1;
//							p.set("po.fieldName", this[o]);
//
//							sampleMainGrid.getStore().insert(0, p);
//						}
//					}
//					n = n + 1;
//				});
//			};
//		}
//		opts.tbar.push({
//			text : '显示可编辑列',
//			handler : null
//		});
//		opts.tbar.push({
//			text : '取消选中',
//			handler : null
//		});
//		opts.tbar.push({
//			text : '删除选中',
//			handler : null
//		});
//		opts.tbar.push({
//			text : '填加明细',
//			handler : null
//		});

//	sampleMainGrid = gridEditTable("bat_sampleMain_div", cols, loadParam, opts);
//	$("#bat_sampleMain_div").data("sampleMainGrid", sampleMainGrid);
//	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
//	$(".x-panel-tbar").css("width", "100%");
});
function add(){
	var id="";
	var selectRecord = sampleMainGrid.getSelectionModel().getSelections();
	console.log(selectRecord[0]);
	/*$.each(selectRecord, function(i, obj) {
		id=obj.get("id");
	});*/
}
function exportexcel() {
	sampleMainGrid.title = biolims.common.exportList;
	var vExportContent = sampleMainGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function selectSampleMain(){
	if (($("#sampleDate").val() != undefined) && ($("#sampleDate").val() != '')) {
		var startSendDatestr = ">=##@@##" + $("#sampleDate").val();

		$("#sampleDate1").val(startSendDatestr);
	}
	if (($("#endAcceptDate").val() != undefined) && ($("#endAcceptDate").val() != '')) {
		var endSendDatestr = "<=##@@##" + $("#endAcceptDate").val();

		$("#sampleDate2").val(endSendDatestr);

	}
	commonSearchAction(sampleMainGrid);
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 800;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sampleMainGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});

//编辑
function edit(){
	var id="";
	var operGrid = sampleMainGrid;
	var selectRecord = operGrid.getSelectionModel().getSelections();
	$.each(selectRecord, function(i, obj) {
		id=obj.get("id");
	});
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	
	window.location=window.ctx+'/system/sample/sampleMain/editSampleMain.action?id=' + id;
}
function view() {
	var id = "";
	var operGrid = sampleMainGrid;
	var selectRecord = operGrid.getSelectionModel().getSelections();
	$.each(selectRecord, function(i, obj) {
		id=obj.get("id");
	});
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/sample/sampleMain/viewSampleMain.action?id=' + id;//;
}
function updatePatientId(){
	var options = {};
	options.width = 400;
	options.height = 300;
	var selectRecord = sampleMainGrid.getSelectionModel().getSelections();
	if(selectRecord.length == 0){
		message(biolims.common.selectAtLeastRecord);
		return;
	}
	loadDialogPage($("#bat_patientId_div"), biolims.sample.fillPatientId, null, {
		"Confirm" : function() {
			var records = sampleMainGrid.getSelectRecord();
			if (records && records.length > 0) {
				var patientId = $("#patientId").val();
				for(var i=0;i<records.length;i++){
					ajax("post", "/system/sample/sampleMain/updatePatientId.action", {
						id : records[i].get("id"),
						patientId:patientId
					});
					
					//alert(patientId);
					//window.location=window.ctx+'/system/sample/sampleMain/updatePatientId.action?id=' + records[i].get("id") + "&patientId=" + patientId;
				}
				//window.location=window.ctx+'/system/sample/sampleMain/editSampleMain.action?id=' + id;
				//sampleMainGrid.stopEditing();
				/*$.each(records, function(i, obj) {
					obj.set("patientId", patientId);
				});*/
				//sampleMainGrid.startEditing(0, 0);
				//alert($("#patientId").val());
			}
			$(this).dialog("close");
			window.location.href=window.location.href;
		}
	}, true, options);
}
function updateProjectId(){
	var options = {};
	options.width = 400;
	options.height = 300;
	var selectRecord = sampleMainGrid.getSelectionModel().getSelections();
	if(selectRecord.length == 0){
		message(biolims.common.selectAtLeastRecord);
		return;
	}
	loadDialogPage($("#bat_project_div"), biolims.sample.fillPatientId, null, {
		"Confirm" : function() {
			var records = sampleMainGrid.getSelectRecord();
			if (records && records.length > 0) {
				var projectId = $("#sampleInfo_project").val();
				var projectName = $("#sampleInfo_project_name").val();
				for(var i=0;i<records.length;i++){
					ajax("post", "/system/sample/sampleMain/updateProjectId.action", {
						id : records[i].get("id"),
						projectId:projectId,
						projectName:projectName
					});
					
				}
			}
			$(this).dialog("close");
			window.location.href=window.location.href;
		}
	}, true, options);
}

function updateCrm(){
	var options = {};
	options.width = 400;
	options.height = 300;
	var selectRecord = sampleMainGrid.getSelectionModel().getSelections();
	if(selectRecord.length == 0){
		message(biolims.common.selectAtLeastRecord);
		return;
	}
	loadDialogPage($("#bat_crm_div"), biolims.sample.fillPatientId, null, {
		"Confirm" : function() {
			var records = sampleMainGrid.getSelectRecord();
			if (records && records.length > 0) {
				var crmId = $("#sampleInfo_crmCustomer_id").val();
				var crmName = $("#sampleInfo_crmCustomer_name").val();
				for(var i=0;i<records.length;i++){
					ajax("post", "/system/sample/sampleMain/updateCrm.action", {
						id : records[i].get("id"),
						crmId:crmId,
						crmName:crmName
					});
					
				}
			}
			$(this).dialog("close");
			window.location.href=window.location.href;
		}
	}, true, options);
}



function updateDocter(){
	var options = {};
	options.width = 400;
	options.height = 300;
	var selectRecord = sampleMainGrid.getSelectionModel().getSelections();
	if(selectRecord.length == 0){
		message(biolims.common.selectAtLeastRecord);
		return;
	}
	loadDialogPage($("#bat_docter_div"), biolims.sample.fillPatientId, null, {
		"Confirm" : function() {
			var records = sampleMainGrid.getSelectRecord();
			if (records && records.length > 0) {
				var docterId = $("#sampleInfo_crmDoctor_id").val();
				var docterName = $("#sampleInfo_crmDoctor_name").val();
				for(var i=0;i<records.length;i++){
					ajax("post", "/system/sample/sampleMain/updateDocter.action", {
						id : records[i].get("id"),
						docterId:docterId,
						docterName:docterName
					});
					
				}
			}
			$(this).dialog("close");
			window.location.href=window.location.href;
		}
	}, true, options);
}


