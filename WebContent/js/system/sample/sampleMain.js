var sampleMainGrid;
$(function() {
	var fields = [];
	fields.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
	});
	fields.push({
		"data": "sampleOrder-id",
		"title": "订单编号",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-barcode",
		"title": "产品批号",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-name",
		"title": "姓名",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-gender",
		"title": "性别",
//		"className": "select",
		"name": "|男|女|未知",
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return '男';
			}
			if(data == "0") {
				return '女';
			}
			if(data == "3") {
				return '未知';
			}
			if(data == "") {
				return '';
			}
		},
	});
	fields.push({
		"data": "sampleOrder-age",
		"title": "年龄",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-medicalNumber",
		"title": "电子病历编号",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-productName",
		"title": "产品名称",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-round",
		"title": "采血轮次",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-filtrateCode",
		"title": "筛选号",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-randomCode",
		"title": "随机号",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-ccoi",
		"title": "CCOI",
		"visible": true,
	});
	fields.push({
		"data": "sampleOrder-crmCustomer-name",
		"title": "医疗机构",
		"visible": true,
	});
//	fields.push({
//		"data": "id",
//		"title": biolims.common.id,
//		"visible": false,
//	});
//
//	fields.push({
//		"data": "code",
//		"title": biolims.common.code,
//		"visible": true,
//	});
//
//	fields.push({
//		"data": "sampleType2",
//		"title": biolims.common.sampleType,
//		"visible": false,
//	});
//
//	fields.push({
//		"data": "sampleType-id",
//		"title": biolims.common.sampleTypeId,
//		"visible": false,
//	});
//
//	fields.push({
//		"data": "sampleType-name",
//		"title": biolims.common.sampleType,
//		"visible": true,
//	});
//
//	fields.push({
//		"data": "state",
//		"title": biolims.common.state,
//		"visible": false,
//	});
//
//
//	fields.push({
//		"data": "stateName",
//		"title": biolims.common.stateName,
//		"visible": true,
//	});
//
//
//	fields.push({
//		"data": "orderNum",
//		"title": biolims.sample.orderNum,
//		"visible": true,
//	});
//
//
//	fields.push({
//		"data": "idCard",
//		"title": biolims.common.outCode,
//		"visible": true,
//	});
//
//	fields.push({
//		"data": "cardNumber",
//		"title": biolims.common.actualExternalNumber,
//		"visible": true,
//	});
//
//	fields.push({
//		"data": "name",
//		"title": biolims.common.packetNumber,
//		"visible": true,
//	});
//
//	fields.push({
//		"data": "patientName",
//		"title": biolims.common.patientName,
//		"visible": true,
//	});
//
//
//	fields.push({
//		"data": "productId",
//		"title": biolims.master.productId,
//		"visible": false,
//	});
//
//	fields.push({
//		"data": "productName",
//		"title": biolims.master.product,
//		"visible": true,
//	});
//
//	fields.push({
//		"data": "project-id",
//		"title": biolims.sample.projectId,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "project-name",
//		"title": biolims.sample.projectName,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "crmCustomer-id",
//		"title": biolims.common.customId,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "crmCustomer-name",
//		"title": biolims.common.custom,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "crmDoctor-id",
//		"title": biolims.sample.crmDoctorId,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "crmDoctor-name",
//		"title": biolims.sample.crmDoctorName,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "sellPerson-id",
//		"title": biolims.common.salesId,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "sellPerson-name",
//		"title": biolims.common.sales,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "species",
//		"title": biolims.wk.species,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "patientId",
//		"title": biolims.report.patientId,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "location",
//		"title": biolims.common.location,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "samplingDate",
//		"title": biolims.sample.samplingDate,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "dicTypeName",
//		"title": biolims.sample.samplingType,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "sampleStage",
//		"title": biolims.sample.sampleStage,
//		"visible": false,
//		"className": "select",
//		"name": "I|II|III|IV",
//		"render": function(data, type, full, meta) {
//			if(data == "1") {
//				return 'I';
//			}
//			if(data == "2") {
//				return 'II';
//			}
//			if(data == "3") {
//				return 'III';
//			}
//			if(data == "4") {
//				return 'IV';
//			}
//			if(data == "") {
//				return '';
//			}
//		},
//	});
//	
//	fields.push({
//		"data": "personShip-id",
//		"title": biolims.sample.personShipId,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "personShip-name",
//		"title": biolims.sample.personShipName,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "sampleNum",
//		"title": biolims.common.sampleNum,
//		"visible": false,
//	});
//	
//	fields.push({
//		"data": "unit",
//		"title": biolims.common.unit,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "sampleNote",
//		"title": biolims.sample.sampleNote,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "sendReportDate",
//		"title": biolims.report.sendDate,
//		"visible": true,
//	});
//	
//	fields.push({
//		"data": "reportDate",
//		"title": biolims.common.reportDate,
//		"visible": true,
//	});
//	

//	$.ajax({
//		type: "post",
//		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
//		async: false,
//		data: {
//			moduleValue: "Storage"
//		},
//		success: function(data) {
//			var objData = JSON.parse(data);
//			if(objData.success) {
//				$.each(objData.data, function(i, n) {
//					var str = {
//						"data": n.fieldName,
//						"title": n.label
//					}
//					colData.push(str);
//				});
//
//			} else {
//				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
//			}
//		}
//	});

	var options = table(true, "", ctx + '/system/sample/sampleMain/showSampleMainNewListJson.action',
		fields, null);
	sampleMainGrid = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleMainGrid);
	})
});
//// 新建
//function add() {
//	window.location = window.ctx +
//		"/storage/toEditStorage.action?p_type=" + $("#p_type").val();
//}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/sample/sampleMain/editSampleMain.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/sample/sampleMain/toViewSampleMain.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
	var fields = [];
	fields.push({
		"searchName": "sampleOrder-barcode",
		"type": "input",
		"txt": "产品批号"
	});
/*	fields.push({
		"searchName": "sampleType.name",
		"type": "input",
		"txt": biolims.common.sampleType
	});*/
	fields.push({
		"searchName": "sampleOrder-name",
		"type": "input",
		"txt": "姓名"
	});
	fields.push({
		"searchName": "sampleOrder-productName",
		"type": "input",
		"txt": biolims.common.productName+"名称"
	});
	fields.push({
		"searchName": "sampleOrder-medicalNumber",
		"type": "input",
		"txt": "电子病理编号"
	});
//	fields.push({
//		"searchName": "stateName",
//		"type": "input",
//		"txt": biolims.common.stateName
//	});
	fields.push({
		"searchName": "sampleOrder-id",
		"type": "input",
		"txt": "订单编号"
	});
	fields.push({
		"searchName": "sampleOrder.crmCustomer.name",
		"type": "input",
		"txt": "医疗机构"
	});
//	fields.push({
//		"searchName": "project.name",
//		"type": "input",
//		"txt": biolims.sample.projectName
//	});
//	fields.push({
//		"searchName": "crmCustomer.name",
//		"type": "input",
//		"txt": biolims.common.custom
//	});
//	fields.push({
//		"searchName": "crmDoctor.name",
//		"type": "input",
//		"txt": biolims.sample.crmDoctorName
//	});
//	fields.push({
//		"searchName": "patientId",
//		"type": "input",
//		"txt": biolims.report.patientId
//	});
//	fields.push({
//		"txt": "创建时间(Start)",
//		"type": "dataTime",
//		"searchName": "createDate##@@##1",
//		"mark": "s##@@##",
//	}, {
//		"txt": "创建时间(End)",
//		"type": "dataTime",
//		"mark": "e##@@##",
//		"searchName": "createDate##@@##2"
//	});
	

	fields.push({
		"type": "table",
		"table": sampleMainGrid
	});
	return fields;
}