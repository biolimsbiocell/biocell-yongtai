$(function() {
	selectById(); 
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	

function selectById(){
	$("#btn_sampleOrder").click(function(){
		if($("#sampleInfo_orderNum").val()==""){
			message(biolims.common.pleaseInputOrderNum);
		}else{
			openDialog(window.ctx + '/system/sample/sampleOrder/editSampleOrder.action?id=' + $("#sampleInfo_orderNum").val());
		}
		
	});
	$("#btn_patientId").click(function(){
		if($("#sampleInfo_patientId").val()==""){
			message(biolims.common.pleaseInputPatientId);
		}else{
			openDialog(window.ctx + '/crm/customer/patient/editCrmPatient.action?id=' + $("#sampleInfo_patientId").val());
		}
		
	});
}

function add() {
	message(biolims.common.sorryAboutThat);
	//window.location = window.ctx + "/sample/editSampleCancerTemp.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/system/sample/sampleMain/showSampleMainList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#sampleCancerTemp", {
					userId : userId,
					userName : userName,
					formId : $("#sampleCancerTemp_id").val(),
					title : $("#sampleCancerTemp_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#sampleCancerTemp_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	
	
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/system/sample/sampleMain/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		



function editCopy() {
	window.location = window.ctx + '/sample/sampleCancerTemp/copySampleCancerTemp.action?id=' + $("#sampleCancerTemp_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#sampleCancerTemp_id").val() + "&tableId=sampleCancerTemp");
}


function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
//	fs.push($("#sampleInfo_patientName").val());
//	nsc.push(biolims.common.patientNameIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}

$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});

	$("#tabs1").tabs({
		select : function(event, ui) {
		}
	});

	//样本状态
	load("/system/sample/sampleMain/showSampleStateList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleStatePage");
	//核酸提取结果
	load("/system/sample/sampleMain/showDnaSampleInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleDnaInfoPage");
	//超声破碎结果
	load("/system/sample/sampleMain/showUfSampleInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleUfTaskResultPage");
	//FFPE，血液文库结果
	load("/system/sample/sampleMain/showSampleWkInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleWkInfoPage");
	//CtDNA文库结果
	load("/system/sample/sampleMain/showSampleWkInfoCtDNAList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleWkInfoPageCtDNA");
	//mRNA文库结果
	load("/system/sample/sampleMain/showSampleWkInfomRNAList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleWkInfoPagemRNA");
	//rRNA文库结果
	load("/system/sample/sampleMain/showSampleWkInforRNAList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleWkInfoPagerRNA");
	
	//pooling领结果
	load("/system/sample/sampleMain/showSamplePoolingInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#samplePoolingInfoPage");
	//上机测序结果
	load("/system/sample/sampleMain/showSampleSequencingInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleSequenveIngInfoPage");
	//下机质控结果
	load("/system/sample/sampleMain/showSampleDesequencingInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleDesequencIngInfoPage");
	//PCR扩增结果
	load("/system/sample/sampleMain/showSamplePCRInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#samplePcrTaskResultPage");	
	//2100质控结果
	load("/system/sample/sampleMain/showSampleQc2100InfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleQc2100ResultPage");
	//QPCR质控结果
	load("/system/sample/sampleMain/showSampleQPCRInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleQpcrResultPage");
	//DNA纯化结果
	load("/system/sample/sampleMain/showSampleDnaInfoList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleDnaTaskResultPage");
	// 文库检测结果
	load("/system/sample/sampleMain/showSampleCheckWkList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleCheckServiceWkResultPage");
	// 核酸检测结果
	load("/system/sample/sampleMain/showSampleCheckServiceList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleCheckServiceResultPage");
	// cfDNA质量评估结果
	load("/system/sample/sampleMain/showSamplecfDnaList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleCFdnaResultPage");
	// 其他实验结果
	load("/system/sample/sampleMain/showSampleOtherList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleOtherResultPage");
	
	
	// 库存信息
	load("/system/sample/sampleMain/showSampleInItemList.action", {
		id : $("#sampleInfo_code").val()
	}, "#sampleInPage");
	
	
	
	// 过滤结果
//	load("/system/sample/sampleMain/showSamplefiltList.action", {
//		id : $("#sampleInfo_code").val()
//	}, "#sampleFiltResultPage");
	
});

$(function() {
	Ext.onReady(function(){
		var tabs=new Ext.TabPanel({
			   id:'tabs11',
		       renderTo:'maintab',
		       height:document.body.clientHeight-30,
		       autoWidth:true,
		       activeTab:0,
		       margins:'0 0 0 0',
		       items:[{
		    	   title:biolims.sample.sampleMasterData,
		    	   contentEl:'markup'
		       } ]
		   });

	});
	//加载明细款的主要load
	//load("/system/sample/sampleOrder/showSampleOrderPersonnelList.action", {
//					id : $("#text10").val()
//				}, "#sampleOrderPersonnelpage");
	//
	//load("/system/sample/sampleOrder/showSampleOrderItemList.action", {
//					id :  $("#text10").val()
//				}, "#sampleOrderItempage");

	var handlemethod = $("#handlemethod").val();
		if (handlemethod == "view") {
			settextreadonlyByAll();
		}
		if (handlemethod == "modify"){
			var t = "#sampleOrder_id";
			settextreadonlyById(t);	
			}
	});

function FamilyPatientShipFun(){
	var win = Ext.getCmp('FamilyPatientShipFun');
	if (win) {
		win.close();
	}
	var FamilyPatientShipFun = new Ext.Window(
			{
				id : 'FamilyPatientShipFun',
				modal : true,
				title : biolims.sample.selectGenealogyType,
				layout : 'fit',
				width : document.body.clientWidth / 1.5,
				height : document.body.clientHeight / 1.1,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' id='maincontentframe' name='maincontentframe' src='"
									+ window.ctx
									+ "/crm/customer/patient/crmFamilyPatientShip/crmFamilyPatientShipSelect.action?flag=CrmFamilyPatientShipFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						FamilyPatientShipFun.close();
					}
				} ]
			});
	FamilyPatientShipFun.show();
}
function setCrmFamilyPatientShipFun(rec){
	
	$("#sampleInfo_personShip_id").val(rec.get("id"));
	$("#sampleInfo_personShip_name").val(rec.get("name"));
	var win = Ext.getCmp('FamilyPatientShipFun');
	if(win){win.close();}
}