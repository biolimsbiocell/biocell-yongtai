var sampleOrderGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'gender',
		type:"string"
	});
	    fields.push({
		name:'birthDate',
		type:"string"
		
	});
	    fields.push({
		name:'diagnosisDate',
		type:"string"
	});
	    fields.push({
			name:'dicType-id',
			type:"string"
		});
		    fields.push({
			name:'dicType-name',
			type:"string"
		});
	    fields.push({
			name:'sampleStage',
			type:"string"
		});
	    fields.push({
		name:'inspectionDepartment-id',
		type:"string"
	});
	    fields.push({
		name:'inspectionDepartment-name',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'samplingDate',
		type:"string"
	});
	    fields.push({
		name:'samplingLocation-id',
		type:"string"
	});
	    fields.push({
		name:'samplingLocation-name',
		type:"string"
	});
	    fields.push({
		name:'samplingNumber',
		type:"string"
	});
	    fields.push({
		name:'pathologyConfirmed',
		type:"string"
	});
	    fields.push({
		name:'bloodSampleDate',
		type:"string"
	});
	    fields.push({
		name:'plasmapheresisDate',
		type:"string"
	});
	    fields.push({
		name:'commissioner-id',
		type:"string"
	});
	    fields.push({
		name:'commissioner-name',
		type:"string"
	});
	    fields.push({
		name:'receivedDate',
		type:"string"
	});
	    fields.push({
		name:'sampleTypeId',
		type:"string"
	});
	    fields.push({
		name:'sampleTypeName',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
			name:'medicalNumber',
			type:"string"
		});
	    fields.push({
			name:'familyCode',
			type:"string"
		});
	    fields.push({
		name:'family',
		type:"string"
	});
	    fields.push({
		name:'familyPhone',
		type:"string"
	});
	    fields.push({
		name:'familySite',
		type:"string"
	});
	    fields.push({
		name:'medicalInstitutions',
		type:"string"
	});
	    fields.push({
		name:'medicalInstitutionsPhone',
		type:"string"
	});
	    fields.push({
		name:'medicalInstitutionsSite',
		type:"string"
	});
	    fields.push({
		name:'attendingDoctor',
		type:"string"
	});
	    fields.push({
		name:'attendingDoctorPhone',
		type:"string"
	});
	    fields.push({
		name:'attendingDoctorSite',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'sampleFlag',
		type:"string"
	});
	    fields.push({
			name:'successFlag',
			type:"string"
		});
	    fields.push({
			name:'age',
			type:"string"
		});
	    fields.push({
			name:'orderType',
			type:"string"
		});
	    fields.push({
			name:'infoUserStr',
			type:"string"
		});
	    fields.push({
			name:'primary-id',
			type:"string"
		});
	    fields.push({
			name:'primary-name',
			type:"string"
		});
	    fields.push({
			name:'idcard',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.orderCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.sname,
		width:20*6,
		
		sortable:true
	});
	var genderstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.male ], [ '0', biolims.common.female ]]
	});
	
	var genderComboxFun = new Ext.form.ComboBox({
		store : genderstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'gender',
		header:biolims.common.gender,
		width:10*6,
		
		renderer: Ext.util.Format.comboRenderer(genderComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'age',
		header:biolims.common.age,
		width:10*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'birthDate',
		header:biolims.common.birthDay,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'diagnosisDate',
		header:biolims.common.diagnosisDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'receivedDate',
		header:biolims.common.receiveDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'dicType-id',
		hidden:true,
		header:biolims.sample.dicTypeId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'dicType-name',
		header:biolims.sample.dicTypeName,
		width:20*10,
		sortable:true
		});
	var sampleStagestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', 'I' ], [ '2', 'II' ],[ '3', 'III' ],[ '4', 'IV' ]]
	});
	
	var sampleStageComboxFun = new Ext.form.ComboBox({
		store : sampleStagestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'sampleStage',
		header:biolims.sample.sampleStage,
		width:10*6,
		
		renderer: Ext.util.Format.comboRenderer(sampleStageComboxFun),				
		sortable:true
	});
		cm.push({
		dataIndex:'inspectionDepartment-id',
		hidden:true,
		header:biolims.sample.inspectionDepartmentId,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'inspectionDepartment-name',
		header:biolims.sample.inspectionDepartmentName,
		
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'productId',
		hidden:true,
		header:biolims.common.productId,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'samplingDate',
		header:biolims.sample.samplingDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'samplingLocation-id',
		hidden:true,
		header:biolims.sample.samplingLocationId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'samplingLocation-name',
		header:biolims.sample.samplingLocationName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'samplingNumber',        
		header:biolims.common.code,
		width:20*6,
		
		sortable:true
	});
	var pathologyConfirmedstore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes], [ '0', biolims.common.no]]
	});
	
	var pathologyConfirmedComboxFun = new Ext.form.ComboBox({
		store : pathologyConfirmedstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'pathologyConfirmed',
		header:biolims.sample.pathologyConfirmed,
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(pathologyConfirmedComboxFun),				
		sortable:true
	});
	cm.push({
		dataIndex:'bloodSampleDate',
		header:biolims.sample.bloodSampleDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'plasmapheresisDate',
		header:biolims.sample.plasmapheresisDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'commissioner-id',
		hidden:true,
		header:biolims.common.commissionerId,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'commissioner-name',
		header:biolims.common.commissioner,
		
		width:20*6,
		sortable:true
		});
		cm.push({
			dataIndex:'primary-id',
			hidden:true,
			header:biolims.common.primaryId,
			width:20*6,
			sortable:true
			});
			cm.push({
			dataIndex:'primary-name',
			header:biolims.common.primary,
			
			width:20*6,
			sortable:true
			});
	
	cm.push({
		dataIndex:'sampleTypeId',
		header:biolims.common.sampleTypeId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleTypeName',
		header:biolims.common.sampleTypeName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.sample.sampleCode,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'medicalNumber',
		header:biolims.sample.medicalNumber,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'familyCode',
		header:'家族编号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'family',
		header:biolims.sample.family,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'familyPhone',
		header:biolims.sample.familyPhone,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'familySite',
		header:biolims.sample.familySite,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'medicalInstitutions',
		header:biolims.sample.medicalInstitutions,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'medicalInstitutionsPhone',
		header:biolims.sample.medicalInstitutionsPhone,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'medicalInstitutionsSite',
		header:biolims.sample.medicalInstitutionsSite,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'attendingDoctor',
		header:biolims.sample.attendingDoctor,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'attendingDoctorPhone',
		header:biolims.sample.attendingDoctorPhone,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'attendingDoctorSite',
		header:biolims.sample.attendingDoctorSite,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:biolims.wk.approverId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.approver,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.wk.approverDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleFlag',
		header:biolims.sample.sampleFlag,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'successFlag',
		header:biolims.sample.successFlag,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'infoUserStr',
		header:biolims.common.commissioner,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.sample.orderType,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'idcard',
		header:"身份证号",
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/sample/sampleUploadOrder/showSampleOrderListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.sample.orderList;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		text : "删除选中",
		handler : function() {
			var ids[];
			var store = sampleOrderGrid.getSelectionModel().getSelections();
			$.each(store,function(i,obj){
				ids[i] = obj;
			});
			ajax("post", "/system/sample/sampleUploadOrder/delSampleUploadInfo.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					sampleOrderGrid.getStore().commitChanges();
					sampleOrderGrid.getStore().reload();
					message(biolims.common.deleteSuccess);
				} else {
					message(biolims.common.deleteFailed);
				}
			}, null);
		}
	});
	opts.tbar.push({
	text : '批量上传订单(csv文件)',
	handler : function() {
		var options = {};
		options.width = 350;
		options.height = 200;
		loadDialogPage($("#bat_sampleOrderDD_div"),biolims.common.batchUpload,null,{
			"Confirm":function(){
				goInExcelcsvDD();
				$(this).dialog("close");
			}
		},true,options);
	}
});
	opts.tbar.push({
		text : '批量上传样本(csv文件)',
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_sampleOrderYB_div"),biolims.common.batchUpload,null,{
				"Confirm":function(){
					goInExcelcsvYB();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : "提交选中",
		handler : submitSample
	});
function goInExcelcsvDD(){
	var file = document.getElementById("file-uploadcsvDD-order").files[0];  
	var n = 0;
	var ob = sampleOrderGrid.getStore().recordType;
	var reader = new FileReader();  
	reader.readAsText(file,'GB2312');  
	reader.onload=function(f){  
		var csv_data = $.simple_csv(this.result);
		$(csv_data).each(function() {
			if(n>0){
        			var p = new ob({});
        			p.isNew = true;
        			
        			p.set("id",this[0]);//订单编号
        			p.set("name",this[1]);//姓名
        			if(this[2]==biolims.common.male){//性别
        				p.set("gender","1");
        			}else if(this[2]==biolims.common.female){
        				p.set("gender","0");	
        			}
        			p.set("age",this[3]);//年龄
        			if(this[4]){//出生日期
						var value15 = this[4];
						var a15   =   new Date(value15);	
						p.set("birthDate", a15);
					}
        			if(this[5]){//接收日期
						var value15 = this[5];
						var a15   =   new Date(value15);	
						p.set("receivedDate", a15);
					}
        			
        			
        			
        			//送检科室
        			ajax("post", "/sample/sampleOrder/GetDicTypeIdByName.action", {
        				name : this[6],
        				type : 'ks'
        			}, function(data) {
        				if (data.success) {
    						p.set("inspectionDepartment-id",data.dictid);
        				} else {
        					message("上传失败");
        					return
        				}
        			}, null);
        			
        			//检测项目
        			ajax("post", "/sample/sampleOrder/GetProductIdByName.action", {
        				itemDataJson : this[7]
        			}, function(data) {
        				if (data.success) {
        					p.set("productId",data.productid);
        				} else {
        					loadMarsk.hide();
        					p.set("productId",'');
        				}
        			}, null);
        			
        			
        			//销售代表
        			ajax("post", "/sample/sampleOrder/GetUserIdByName.action", {
        				name : this[8]
        			}, function(data) {
        				if (data.success) {
        						p.set("commissioner-id",data.id);
        				} else {
        					message("上传失败");
        					return
        				}
        			}, null);
        		
        			
        			p.set("inspectionDepartment-name",this[6]);//送检科室
        			p.set("productName",this[7]);//检测项目
        			p.set("commissioner-name",this[8]);//销售代表
        			p.set("attendingDoctor",this[9]);//主治医生
        			
        			//p.set("infoUserStr",this[6]);
        			
        			p.set("state","3");
        			p.set("stateName",biolims.common.create);
        			sampleOrderGrid.getStore().insert(0, p);
            	}
                 n = n +1;
            });
	};
}
	
	sampleOrderGrid=gridTable("show_sampleOrder_div",cols,loadParam,opts);
});

//保存
function save(){
	  
	var itemJson = commonGetModifyRecords(sampleOrderGrid);
	if(itemJson.length>0){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
			ajax("post", "/system/sample/sampleUploadOrder/saveUploadOrderUpload.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					loadMarsk.hide();
					var sampleOrderid ="";
					if(data.flag == 1){
						sampleOrderid=data.sampleOrderid;
						sampleOrderid = sampleOrderid.indexOf(",",sampleOrderid.length-1) == -1?sampleOrderid:sampleOrderid.substring(0, sampleOrderid.length-1);
						message(data.message+"\n   "+sampleOrderid);	
					}else{
						message(biolims.common.saveSuccess);
						sampleOrderGrid.getStore().commitChanges();
					}
					sampleOrderGrid.getStore().reload();
				} else {
					loadMarsk.hide();
					message(biolims.common.saveFailed);
				}
			}, null);			

	}else{
		message(biolims.common.noData2Save);
	}
}


function exportexcel() {
	sampleOrderGrid.title = biolims.common.exportList;
	var vExportContent = sampleOrderGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

function goInExcelcsvYB(){
	
}
//提交
function submitSample(){
	
}
