var samplePoolingGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	 var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
			name:'poolingCode',
			type:"string"
		});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'poolingTask-id',
		type:"string"
	});
	    fields.push({
		name:'poolingTask-name',
		type:"string"
	});
  fields.push({
		name:'wkIndex',
		type:"string"
	});
  
  fields.push({
		name:'mixAmount',
		type:"string"
	});
	   fields.push({
		name:'mixVolume',
		type:"string"
	});
	   fields.push({
		name:'qpcrConcentration',
		type:"string"
	});
 fields.push({
		name:'reason',
		type:"string"
	});
	   
 fields.push({
		name:'idCard',
		type:"string"
	});
  
  fields.push({
		name:'sequencingFun',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
		name:'acceptDate',
		type:"string"
	});
 fields.push({
		name:'reportDate',
		type:"string"
	});
 fields.push({
		name:'orderId',
		type:"string"
	});
 fields.push({
		name:'submit',
		type:"string"
	});
 fields.push({
		name:'patient',
		type:"string"
	});
 fields.push({
		name:'phone',
		type:"string"
	});
 
 
 fields.push({
		name:'sequencingReadLong',
		type:"string"
	});
fields.push({
		name:'sequencingType',
		type:"string"
	});
fields.push({
		name:'sequencingPlatform',
		type:"string"
	});
fields.push({
		name:'totalAmount',
		type:"string"
	});
fields.push({
		name:'totalVolume',
		type:"string"
	});

fields.push({
		name:'others',
		type:"string"
});
fields.push({
		name:'acceptUser-id',
		type:"string"
});
fields.push({
		name:'acceptUser-name',
		type:"string"
});



fields.push({
	name:'readsOne',
	type:"string"
});
fields.push({
	name:'readsTwo',
	type:"string"
});
fields.push({
	name:'index1',
	type:"string"
});
fields.push({
	name:'index2',
	type:"string"
});

fields.push({
	name:'contractId',
	type:"string"
});
fields.push({
	name:'projectId',
	type:"string"
});
fields.push({
	name:'orderType',
	type:"string"
});
fields.push({
	name:'techTaskId',
	type:"string"
});
fields.push({
	name:'classify',
	type:"string"
});

fields.push({
	name:'wkType',
	type:"string"
});
fields.push({
	name:'pdLength',
	type:"string"
});
fields.push({
	name:'concentration',
	type:"string"
});
fields.push({
	name:'lane',
	type:"string"
});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6,
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:biolims.pooling.poolingCode,
		width:20*6
	});
	cm.push({
		dataIndex:'lane',
		hidden : false,
		header:'Lane',
		width:20*6
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:biolims.wk.wkType,
		width:20*6
	});
	cm.push({
		dataIndex:'pdLength',
		hidden : false,
		header:biolims.pooling.pdLength,
		width:20*6
	});
	
	cm.push({
		dataIndex:'qpcrConcentration',
		header:biolims.pooling.qpcrConcentrtion,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.sampleName,
		width:20*6
	});
	cm.push({
		dataIndex:'wkIndex',
		header:biolims.wk.wkINDEX,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.pooling.sequencingCode,
		width:20*6
	});
	
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'mixAmount',
		hidden : true,
		header:biolims.pooling.mixAmount,
		width:20*6
	});
	cm.push({
		dataIndex:'mixVolume',
		hidden : true,
		header:biolims.pooling.mixVolume,
		width:20*6
	});
	
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'method',
		hidden : true,
		header:biolims.common.method,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'poolingTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'poolingTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:biolims.common.idCard,
		width:20*6
	});
	cm.push({
		dataIndex:'sequencingFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:20*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6
	});
	cm.push({
		dataIndex:'patient',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : true,
		header:biolims.common.productName,
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:biolims.common.phone,
		width:20*6
	});
	
//	var storereadLongCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '', '请选择' ], [ '0', '40+8' ],['1','50+8'],[ '2', '75+8' ],[ '3', '100+8' ],[ '4', '125+8' ],[ '5', '150+8' ],[ '6', '250+8' ],[ '7', '300+8' ],[ '8', '其他' ]]
//	});
//	var readLongCob = new Ext.form.ComboBox({
//		store : storereadLongCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'sequencingReadLong',
		hidden : true,
		header:biolims.pooling.sequencingReadLong,
		width:20*6
		//renderer : Ext.util.Format.comboRenderer(readLongCob)
	});
//	var storetypeCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '', '请选择' ], [ '0', 'PE' ],['1','SE'],[ '2', 'PE/SE' ]]
//	});
//	var typeCob = new Ext.form.ComboBox({
//		store : storetypeCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'sequencingType',
		hidden : true,
		header:biolims.pooling.sequencingType,
		width:20*6
		//renderer : Ext.util.Format.comboRenderer(typeCob)
	});
//	var storeplatformCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '', '请选择' ], [ '0', 'Nextseq 500' ],['1','Nextseq 550AR'],[ '2', 'Miseq' ],[ '3', 'Hiseq 2500 V4' ],[ '4', 'Hiseq 2500 Rapid' ],[ '5', '其他' ]]
//	});
//	var platformCob = new Ext.form.ComboBox({
//		store : storeplatformCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'sequencingPlatform',
		hidden : true,
		header:biolims.pooling.sequencingPlatform,
		width:20*6
		//renderer : Ext.util.Format.comboRenderer(platformCob)
	});
	cm.push({
		dataIndex:'totalAmount',
		hidden : true,
		header:biolims.sequencing.totalAmount,
		width:20*6
	});
	
	cm.push({
		dataIndex:'totalVolume',
		hidden : true,
		header:biolims.common.totalVolume,
		width:20*6
	});
	cm.push({
		dataIndex:'others',
		hidden : true,
		header:biolims.pooling.others,
		width:20*6
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSamplePoolingInfoListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.pooling.mixPoolingResults;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	samplePoolingGrid = gridEditTable("samplePoolingdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
