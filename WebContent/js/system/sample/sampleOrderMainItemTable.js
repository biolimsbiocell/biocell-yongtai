/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
var oldChangeLog;
var formm;
$(function() {
	layui.use('form', function() {
		formm = layui.form;
		// 控制性别选择
		formm.on('checkbox', function(data) {
			$(data.othis[0]).siblings(".layui-form-checkbox").removeClass(
					"layui-form-checked");
			if (data.elem.checked) {
				$("#sampleOrderGender").val(data.elem.value);
			}
		});
	});
	// 加载子表
	var id = $("#sampleOrder_id").val();

	var tbarOpts = [];

	var colOpts = [];

	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.common.sname,
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});

	colOpts.push({
		"data" : "gender",
		"title" : biolims.common.gender,
		"className" : "select",
		"createdCell" : function(td) {
			$(td).attr("saveName", "gender");
			$(td).attr(
					"selectOpt",
					biolims.common.male + "|" + biolims.common.female + "|"
							+ biolims.common.unknown);
		},
		"name" : biolims.common.male + "|" + biolims.common.female + "|"
				+ biolims.common.unknown,
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return biolims.common.female;
			}
			if (data == "1") {
				return biolims.common.male;
			}
			if (data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	});

	// colOpts.push({
	// "data" : "sampleCode",
	// "title" : biolims.common.outCode,
	// // "className" : "edit",
	// "createdCell" : function(td) {
	// $(td).attr("saveName", "sampleCode");
	// }
	// });

	colOpts.push({
		"data" : "slideCode",
		"title" : biolims.common.barCode + '<img src="/images/required.gif"/>',
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "slideCode");
		}
	});
	colOpts.push({
		"data" : "sampleType-id",
		"title" : "样本名称编号",
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "sampleType-id");
		},
	});

	colOpts.push({
		"data" : "sampleType-name",
		"title" : "样本名称"
				+ '<img src="/images/required.gif"/>',
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "sampleType-name");
			$(td).attr("sampleType-id", rowData['sampleType-id']);
		},
	});

	colOpts.push({
		"data" : "samplingDate",
		"title" : "采血时间",
		"className" : "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "samplingDate");
		}
	});
	
	// colOpts.push({
	// "data" : "receiveUser-id",
	// "title" : "质检接收人",
	// "createdCell" : function(td) {
	// $(td).attr("saveName", "receiveUser-id");
	// },
	// });

	// colOpts.push({
	// "data" : "releaseResults",
	// "title" : "放行结果",
	// "createdCell" : function(td, data, rowData) {
	// $(td).attr("saveName", "releaseRresults");
	// },
	// });
	colOpts.push({
		"data" : "releaseResults",
		"title" : "放行结果",
		
		"name" : biolims.common.qualified + "|" + biolims.common.disqualified,
		"createdCell" : function(td) {
			$(td).attr("saveName", "releaseResults");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified);
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return biolims.common.qualified;
			}
			if (data == "0") {
				return biolims.common.disqualified;
			}
			if(data=="2"){
				return "让步放行";
			}
			if(data=="3"){
				return "备用空管";
		    } else {
				return "";
			}
			
				

		}
	});
	colOpts.push({
		"data" : "receiveUser-name",
		"title" : "质检接收人名称",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "receiveUser-name");
			$(td).attr("receiveUser-id", rowData['receiveUser-id']);
		},
	});

	colOpts.push({
		"data" : "receiveDate",
		"title" : "质检接收日期",
		"createdCell" : function(td) {
			$(td).attr("saveName", "receiveDate");
		},
	/*
	 * "render": function(data) { if(data) { return format(data); } else {
	 * return ""; } }
	 */
	});

	// colOpts.push({
	// "data" : "stateName",
	// "title" : biolims.common.sampleState,
	// "createdCell" : function(td) {
	// $(td).attr("saveName", "stateName");
	// }
	// });

	var handlemethod = $("#handlemethod").val();

	if (handlemethod != "view") {

		tbarOpts.push({
			text : biolims.common.fillDetail,
			action : function() {
				addItem($("#sampleInfoTable"))

			}
		});

		tbarOpts.push({
			text : "复制",
			action : function() {
				copyItem($("#sampleInfoTable"))
			}
		});

		tbarOpts
				.push({
					text : biolims.common.delSelected,
					action : function() {
						removeChecked(
								$("#sampleInfoTable"),
								"/system/sample/sampleOrder/delSampleOrderItemTable.action",
								"订单明细删除样本：", id, myTable);
					}

				});

		// function panduan(){
		// $.ajax({
		// type : 'post',
		// url : '/system/sample/sampleOrder/delSampleOrderItemTable.action',
		// data : {
		// id : id,
		// },
		// success : function(data) {
		// var data = JSON.parse(data)
		// if (data.success) {
		// tableRefresh();
		// } else {
		// tableRefresh();
		// }
		// ;
		// }
		// })
		// }
//		tbarOpts.push({
//			text : biolims.common.addwindow,
//			action : function() {
//				addItemLayer($("#sampleInfoTable"))
//			}
//		});

		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#sampleInfoTable"))
			}
		});

		// tbarOpts.push({
		// text : "打印样本接收条码",
		// action : function() {
		// var lan = $("#lan").val();
		// if (lan == "en") {
		// downEnCsv()
		// } else {
		// downCsv()
		// }
		// }
		// });
		tbarOpts.push({
			text : biolims.common.batchUpload,
			action : function() {
				$("#uploadCsv").modal("show");
			}
		});
		tbarOpts.push({
			text : '<i class="fa fa-yelp"></i>' + "样本名称",
			action : addSampleType
		});
		tbarOpts.push({
			text : biolims.common.save,
			action : function() {
				saveItem($("#sampleInfoTable"));
			}
		});
		tbarOpts.push({
	        text: '<i class="fa  fa-ra"></i> ' + "打印标签",
	        action: Dy,
	    });

		tbarOpts.push({
			text: "打印样本接收条码",
			action: function() {
				downCsv()
			}
		});
	}else{
		tbarOpts.push({
	        text: '<i class="fa  fa-ra"></i> ' + "打印标签",
	        action: Dy,
	    });
		tbarOpts.push({
			text: "打印样本接收条码",
			action: function() {
				downCsv()
			}
		});
	}
	
	var sampleInfoOptions = table(true, id,
			'/system/sample/sampleOrder/showSampleOrderItemTableJson.action',
			colOpts, tbarOpts)
	myTable = renderData($("#sampleInfoTable"), sampleInfoOptions);
	// 日志 dwb 2018-05-12 11:45:52
	myTable.on('draw', function() {
		oldChangeLog = myTable.ajax.json();
	});
	// 上传csv
	var csvFileInput = fileInputCsv("");
	csvFileInput.on("fileuploaded", function(event, data, previewId, index) {
		$.ajax({
			type : "post",
			data : {
				id : $("#sampleOrder_id").val(),
				fileId : data.response.fileId
			},
			url : ctx + "/system/sample/sampleOrder/uploadCsvFile.action",
			success : function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					myTable.ajax.reload();
				} else {
					top.layer.msg(biolims.common.uploadFailed)
				}

			}
		});
	});
	// 上一步下一步
	stepViewChange();
});
function table(cheboxvisible, id, url, col, tbar) {
	var cols = new Array();
	cols.push({
		"data": "id",
		"width": "18px",
		"visible": cheboxvisible,
		"title": '<input type="checkbox" class="icheck"  id="checkall"/>',
		"orderable": false,
		"render": function(data, type, full, meta) {
			return '<input type="checkbox" class="icheck" value="' + data + '"/>';
		}
	})
	for(var j = 0; j < col.length; j++) {
		cols.push(col[j])
	}
		var tbars = new Array();
		if(tbar != null) {
			for(var i = 0; i < tbar.length; i++) {
				if(tbar[i].text == biolims.common.fillDetail) {
					tbars.push({
						text: '<i class="glyphicon glyphicon-plus"></i> ' + biolims.common.fillDetail,
						className: 'btn btn-sm  btn-success addItem',
						action: tbar[i].action
					});
				} else if(tbar[i].text == biolims.common.delSelected) {
					tbars.push({
						text: '<i class="glyphicon glyphicon-trash"></i> ' + biolims.common.delSelected,
						className: 'btn btn-sm btn-success',
						action: tbar[i].action
					});
				} else if(tbar[i].text == biolims.common.batchUpload) {
					tbars.push({
						text: '<i class="glyphicon glyphicon-share-alt"></i> ' + biolims.common.batchUpload,
						className: 'btn btn-sm  btn-success',
						action: tbar[i].action
					})
				} else if(tbar[i].text == biolims.common.save) {
					if(id != "NEW") {
						tbars.push({
							text:biolims.common.save,
							className: 'btn btn-sm btn-success subSave',
							action: tbar[i].action
						})
					}
				} else if(tbar[i].text == biolims.common.find) {
					if(id != "NEW") {
						tbars.push({
							text: '<i class="glyphicon glyphicon-search"></i>' + biolims.common.find,
							className: 'btn btn-sm btn-success',
							action: tbar[i].action
						})
					}
				} else {
					var className = tbar[i].className ? tbar[i].className : 'btn btn-sm btn-success';
					tbars.push({
						text: tbar[i].text,
						className: className,
						action: tbar[i].action
					})
				}
			}
		tbars.push({
			text: biolims.common.exportList,
			extend: 'excel',
			className: 'btn btn-sm btn-success hide'
		});
		tbars.push({
			text: biolims.common.copy,
			extend: 'copy',
			className: 'btn btn-sm btn-success hide',
			info: biolims.common.copySuccess,
		});
		tbars.push({
			text: biolims.common.print,
			extend: 'print',
			className: 'btn btn-sm  btn-success hide',
		});
		tbars.push({
			text: 'csv',
			extend: 'csv',
			className: 'btn btn-sm btn-success hide'
		});
//		tbars.push({
//			text: '<i class="glyphicon glyphicon-gift"></i>' + biolims.common.showHide,
//			extend: 'colvis',
//			columns: ':gt(0)',
//			className: 'btn btn-sm btn-success colvis'
//		});

	}

	var option = {
		// ajax的路径和数据
		ajax: {
			url: ctx + url,
			data: {
				id: id,
			}
		},
		// 每一列需要的数据key，对应上面thead里面的序列
		columns: cols,
		buttons: tbars

	}
	return option;
}


//下载csv
function downCsv(){
	//window.location.href=ctx+"/js/experiment/enmonitor/dust/cleanAreaDustItem.csv";
	var mainTableId=$("#sampleOrder_id").val();
	if(mainTableId!=""&&mainTableId!='NEW'){
		window.location.href=ctx+"/system/sample/sampleOrder/downloadCsvFile.action?id="+mainTableId;
	}else{
		top.layer.msg("请保存后再下载模板！");
	}
}
// 保存
function saveItem(ele) {

	var changeLog = biolims.order.itemEdit + "：";
	var data = saveItemjson(ele);
	if (!data) {
		return false;
	}
	var chongfu = true;
	// 条形码是否重复
	var a = ele.find('tr')
	a.each(function(i, val) {
				var json = {};
				var tds = $(val).children("td");
				json["id"] = $(tds[0]).find("input").val();
				if (json["id"]==""||json["id"]==null) {
					
					for (var j = 1; j < tds.length; j++) {
						var k = $(tds[j]).attr("savename");
						json[k] = $(tds[j]).text();
						if (k == "slideCode") {
							if (json[k]==""||json[k]==null) {

							} else {
								$.ajax({
											async : false,
											type : 'post',
											url : '/system/sample/sampleOrder/slideCode.action',
											data : {
												id : json[k],
											},
											success : function(data) {
												var data = JSON.parse(data)
												if (data.success) {
													if (data.result) {
														top.layer.msg("条形码重复");
														chongfu = false;
													} else {
													}
												} else {

												}
											}

										});
							}
						}
					}
					
				} else {

					for (var j = 1; j < tds.length; j++) {
						var k = $(tds[j]).attr("savename");
						json[k] = $(tds[j]).text();
						if (k == "slideCode") {
							if (json[k]==""||json[k]==null) {

							} else {
								$.ajax({
											async : false,
											type : 'post',
											url : '/system/sample/sampleOrder/slideCode1.action',
											data : {
												id : json[k],
												idold:json["id"]
											},
											success : function(data) {
												var data = JSON.parse(data)
												if (data.success) {
													if (data.result) {
														top.layer.msg("条形码重复");
														chongfu = false;
													} else {
													}
												} else {

												}
											}

										});
							}
						}
					}
				}

			});

	if (chongfu) {
		changeLog = getChangeLog(data, ele, changeLog);
		var changeLogs = "";
		if (changeLog != biolims.order.itemEdit + "：") {
			changeLogs = changeLog;
		}
		top.layer.load(4, {
			shade : 0.3
		});
		$.ajax({
			type : 'post',
			url : '/system/sample/sampleOrder/saveItem.action',
			data : {
				id : $("#sampleOrder_id").val(),

				dataJson : data,
				changeLog : changeLogs
			},
			success : function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveSuccess);
					
					if (data.sorry){
						top.layer.msg("有样本名称没有选择！");
					}
					
					tableRefresh();
				} else {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveFailed)
				}
				;
			}
		})
	} else {

	}

}
// 获得保存时的json数据
function saveItemjson(ele) {

	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag = true;

	var trss = ele.find("tbody tr");
	trss.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if (k == "sampleType-name") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请选择样本名称！");
					return false;
				}
			}
		}
	});

	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			// 放行结果
			if (k == "releaseResults") {
				var releaseResults = $(tds[j]).text();
				if (releaseResults == biolims.common.qualified) {
					json[k] = "1";
				} else if (releaseResults == biolims.common.disqualified) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断男女并转换为数字
			if (k == "gender") {
				var gender = $(tds[j]).text();
				if (gender == biolims.common.male) {
					json[k] = "1";
				} else if (gender == biolims.common.female) {
					json[k] = "0";
				} else if (gender == biolims.common.unknown) {
					json[k] = "3";
				} else {
					json[k] = "";
				}
				continue;
			}
//			// 验证必填字段
//			if (k == "name") {
//				if (!json[k]) {
//					console.log(json[k]);
//					flag = false;
//					top.layer.msg("请填写姓名！");
//					return false;
//				}
//			}
			if (k == "sampleType-name") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请选择样本名称！");
					return false;
				}
			}
			if (k == "slideCode") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请填写条形码！");
					return false;
				}
			}
			// 添加样本类型ID
			if (k == "sampletype-name") {
				json["sampleType-id"] = $(tds[j]).attr("sampletype-id");
				continue;
			}
			// 添加接收人ID
			if (k == "receiveUser-name") {
				json["receiveUser-id"] = $(tds[j]).attr("receiveUser-id");
				continue;
			}

		}
		data.push(json);
	});
	if (flag) {
		return JSON.stringify(data);
	} else {
		return false;
	}
}
// 复制行,只能复制一行
function copyItem(ele) {
	//查询一行数据
	var rowsall = ele.find("tr");
	var rows = ele.find(".selected");
	var length = rows.length;
	if (length == 1) {
		var row = rows.clone(true);
		var barcode = row.find("td").eq(3).text();
		//条形码截取并拼接
		var a = barcode.substring(0, barcode.length - 2);
		if (rowsall.length >= 10) {
			barcode = a + "" + rowsall.length;
		} else {
			barcode = a + "0" + rowsall.length;
		}
		//给第三个赋值(条形码)
		row.find("td").eq(3).text(barcode);
		//清空后面的值
		row.find("td").eq(4).text("");
		row.find("td").eq(5).text("");
		row.find("td").eq(6).text("");
		row.find("td").eq(7).text("");

		row.find(".icheck").iCheck('uncheck').val("");
		row.addClass("editagain");
		rows.before(row);
		checkall(ele);
		top.layer.msg("复制成功");
	} else if (!length || length < 1) {
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
	} else {
		top.layer.msg(biolims.common.onlyOne);
	}

}
// 变日期选择框
// function changeDateInput(ele) {
// 	$(ele).css("padding", "0px");
// 	var ipt = $('<input type="text" id="date" autofocus value=' + $(ele).text()
// 			+ '>');
// 	var width = $(ele).width();
// 	ipt.css({
// 		"width" : width + "px",
// 		"height" : "32px",
// 	});
// 	$(ele).html(ipt);
// 	ipt.datetimepicker({
// 		// ipt.datepicker({
// 		language : "zh-TW",
// 		autoclose : true, // 选中之后自动隐藏日期选择框
// 		format : "yyyy-mm-dd hh:ii" // 日期格式，详见
// 	// format: "yyyy-mm-dd" //日期格式，详见
// 	});
// 	$(ele).find("input").click();
// 	document.onkeydown = function(event) {
// 		var e = event || window.event || arguments.callee.caller.arguments[0];
// 		if (e.keyCode == 9) {
// 			var width = $("#edit").width();
// 			if (!$(".datepicker").length) {
// 				$("#date").parent("td").css({
// 					"padding" : "5px",
// 					"box-shadow" : "0px 0px 1px #17C697"
// 				});
// 				$("#date").parents("tr").addClass("editagain");
// 				$("#date").parent("td").html($("#date").val());
// 			}
// 			var next = $(ele).next("td");
// 			while (next.hasClass("undefined")) {
// 				next = next.next("td")
// 			}
// 			if (next.length) {
// 				next.click();
// 			} else {
// 				return false;
// 			}
// 		}

// 	};
// }
/*
 * 创建者 : 郭恒开 创建日期: 2018/03/09 文件描述: 修改日志 修改纪录：dwb 2018-05-10 12:28:05 保存增加日志
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
// 添加样本名称
function addSampleType() {
	var rows = $("#sampleInfoTable .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer
			.open({
				title : "选择样本名称",
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/sample/dicSampleType/selectDicSampleTypeOne.action",
						'' ],
				yes : function(index, layer) {
					var type = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(1)
							.text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(0)
							.text();
					rows.addClass("editagain");
					rows.find("td[savename='sampleType-name']").attr(
							"sampletype-id", id).text(type);
					rows.find("td[savename='sampleType-id']").text(id);

					top.layer.close(index)
				},
			})
}
// 点击上一步下一步切换
function stepViewChange() {
	$("#next").unbind("click").click(
			function() {
				$("#pre").attr("disabled", false);
				var index = $(".wizard_steps .step").index(
						$(".wizard_steps .selected"));
				if (index == $(".wizard_steps .step").length - 1) {
					$(this).attr("disabled", true);
					return false;
				}
				$(".HideShowPanel").eq(index + 1).slideDown().siblings(
						'.HideShowPanel').slideUp();
				$(".wizard_steps .step").eq(index).removeClass("selected")
						.addClass("done");
				var nextStep = $(".wizard_steps .step").eq(index + 1);
				if (nextStep.hasClass("disabled")) {
					nextStep.removeClass("disabled").addClass("selected");
				} else {
					nextStep.removeClass("done").addClass("selected");
				}
				//后更改点击下一步
				if ($("#sampleOrder_id").val() != ""
					&& $("#sampleOrder_id").val() != "NEW"
					&& $("#sampleOrder_id").val() != null) {
				var txm = "";
				$
						.ajax({
							type : "post",
							async : false,
							url : "/system/sample/sampleOrder/getSampleBarCode.action",
							data : {
								id : $("#sampleOrder_id")
										.val(),
							},
							success : function(dataBack) {
								var data = JSON
										.parse(dataBack);
								if (data.success) {
									txm = data.result;
								}
							}
						})
				$("#sampleBarCode").val(txm);
			}
				
				// table刷新和操作显示
				$(".box-tools").show();
				// 回到顶部
				$('body,html').animate({
					scrollTop : 0
				}, 500, function() {
					myTable.ajax.reload();
				});
			});
	$("#pre").click(
			function() {
				$("#next").attr("disabled", false);
				var index = $(".wizard_steps .step").index(
						$(".wizard_steps .selected"));
				if (index == 0) {
					$(this).attr("disabled", true);
					return false;
				}
				$(".HideShowPanel").eq(index - 1).slideDown().siblings(
						'.HideShowPanel').slideUp();
				$(".wizard_steps .step").eq(index).removeClass("selected")
						.addClass("done");
				$(".wizard_steps .step").eq(index - 1).removeClass("done")
						.addClass("selected");	
				// table刷新和操作隐藏
				$(".box-tools").hide();

				$('body,html').animate({
					scrollTop : 0
				}, 500);
			});
	$(".step")
			.click(
					function() {
						if ($(this).hasClass("done")) {
							var index = $(".wizard_steps .step").index($(this));
							$(".wizard_steps .selected")
									.removeClass("selected").addClass("done");
							$(this).removeClass("done").addClass("selected");
							$(".HideShowPanel").eq(index).slideDown().siblings(
									'.HideShowPanel').slideUp();
							// table刷新和操作隐藏
							// console.log(index);
							if (index == 0) {
								$(".box-tools").hide();
							} else {
								if ($("#sampleOrder_id").val() != ""
										&& $("#sampleOrder_id").val() != "NEW"
										&& $("#sampleOrder_id").val() != null) {
									var txm = "";
									$
											.ajax({
												type : "post",
												async : false,
												url : "/system/sample/sampleOrder/getSampleBarCode.action",
												data : {
													id : $("#sampleOrder_id")
															.val(),
												},
												success : function(dataBack) {
													var data = JSON
															.parse(dataBack);
													if (data.success) {
														txm = data.result;
													}
												}
											})
									$("#sampleBarCode").val(txm);
								}
								$(".box-tools").show();
								$("#sampleInfoTable").DataTable().ajax.reload();
							}

						} else if ($(this).hasClass("disabled")) {
							top.layer.msg(biolims.order.finishPre);
						}
					});
}
//function downCsv() {
//	window.location.href = ctx
//			+ "/js/sample/uploadFileRoot/sampleOrderItem.csv";
//}
function downEnCsv() {
	window.location.href = ctx
			+ "/js/sample/uploadFileRoot/sampleOrderItem_En.csv";
}

// 保存
var optimisticLock = true
function saveNumSampleOrder() {
	if(optimisticLock){
		optimisticLock = false;
		
		var myreg = /^[0-9]*$/;
		//采血管数子表
		if(!myreg.test($("#sampleNumberInput").val())){
			top.layer.msg("采血管数(明细)只能为数字!")
			optimisticLock = true;
			return false;
		}
		
		var barCode = "";
		var id = $("#sampleOrder_id").val();
		barCode = $("#sampleBarCode").val();
		if (barCode == "") {
			barCode = $("#sampleOrder_barcode").val();
			if (barCode == "") {
				optimisticLock = true;
				top.layer.msg("请输入条形码!");
				return;

			}
		}
		var number = $("#sampleNumberInput").val();
		if (id == "NEW") {
			optimisticLock = true;
			top.layer.msg("请先保存检测单!");
			return;
		}
		$.ajax({
			type : 'post',
			url : '/system/sample/sampleOrder/saveNumSampleOrderItem.action',
			data : {
				id : id,
				number : number,
				barCode : barCode
			},
			success : function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveSuccess);
					optimisticLock = true;
					tableRefresh();
				} else {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveFailed)
				}
				;
			}
		})
	}
	
	

}

function cx(v) {
	var myreg = /^\d+(\.\d+)?$/;
	if(!v.match(myreg)) {
		top.layer.msg(("采血管数只能输入数字！"));
	}
}

//打印
function Dy() {
	var ids = [];
	var sumNum = 1;
    var rows = $("#sampleInfoTable .selected");
    console.log(rows)
    var length = rows.length;
    var flag = true;
    console.log(length)
    if (!length) {
        top.layer.msg(biolims.common.pleaseSelect);
        return false;
    } else {
        rows.each(function (i, v) {
            var a = $(this).find("td[savename='slideCode']").text();
            if (a = "" || a == 0 || a == null) {
                flag = false;
                return false;
            }
        });
    }
    if (flag == false) {
        top.layer.msg("条形码没有值！");
        return false;
    }
    top.layer.open({
        type: 2,
        title: "选择打印机",
        area: top.screeProportion,
        anim:1,
        btn: biolims.common.selected,
        content: [window.ctx + "/system/newsyscode/codeMainNew/selCodeMainNew.action", ''],
        yes: function (index, layer) {
            var printCode = $('.layui-layer-iframe', parent.document).find("iframe").contents().
            								find("#selCodeMainNew .chosed").children("td").eq(0).text();
            rows.addClass("editagain");
            rows.each(function (i, v) {
            	var dyNums = [];
            	ids.push($(v).find("input[type=checkbox]").val());
                dyNums.push($(v).find("td[savename='slideCode']").text());
              
            });
            ajax("post", "/system/sample/sampleOrder/dyMakeCode.action", {
                ids: ids,
                printCode: printCode,
//                sumNum: sumNum,
//                dyNums: dyNums
            }, function () {
            }, null);
            top.layer.msg("选择成功");
            top.layer.close(index);
        },
    });
}
