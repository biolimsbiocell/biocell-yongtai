$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title":  biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
//		"visible": false
	});
	//原订单编号
	colOpts.push({
		"data": "oldId",
		"title":  biolims.sample.oldId,
		"createdCell": function(td) {
			$(td).attr("saveName", "oldId");
		},
	});
	//更新之后的订单编号
	colOpts.push({
		"data": "updateId",
		"title":  biolims.sample.updateId,
		"createdCell": function(td) {
			$(td).attr("saveName", "updateId");
		},
	});
	
	colOpts.push({
		"data": "sampleOrder-name",
		"title": biolims.common.sname,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-name");
		},
	});
	colOpts.push({
		"data": "sampleOrder-gender",
		"title": biolims.common.gender,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-gender");
		},
	});
	colOpts.push({
		"data": "sampleOrder-age",
		"title": biolims.common.age,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-age");
		},
	});
	colOpts.push({
		"data": "sampleOrder-nation",
		"title": biolims.common.race,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-nation");
		},
	});
	colOpts.push({
		"data": "sampleOrder-zipCode",
		"title": biolims.crm.postcode,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-zipCode");
		},
	});
	colOpts.push({
		"data": "sampleOrder-commissioner-id",
		"title": biolims.common.commissionerId,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-commissioner-id");
		},
	});
	colOpts.push({
		"data": "sampleOrder-commissioner-name",
		"title": biolims.common.sales,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-commissioner-name");
		},
	});
	colOpts.push({
		"data": "sampleOrder-nativePlace",
		"title": biolims.crm.placeOfBirth,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-nativePlace");
		},
	});
	colOpts.push({
		"data": "sampleOrder-medicalNumber",
		"title": biolims.sample.medicalNumber,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-medicalNumber");
		},
	});
	colOpts.push({
		"data": "sampleOrder-familyCode",
		"title": biolims.common.familyCode,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-familyCode");
		},
	});
	colOpts.push({
		"data": "sampleOrder-family",
		"title": biolims.sample.family,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-family");
		},
	});
	colOpts.push({
		"data": "sampleOrder-familyPhone",
		"title": biolims.sample.familyPhone,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-familyPhone");
		},
	});
	colOpts.push({
		"data": "sampleOrder-familySite",
		"title": biolims.sample.familySite,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-familySite");
		},
	});
	colOpts.push({
		"data": "sampleOrder-crmCustomer-id",
		"title": biolims.order.medicalInstitutionId,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-crmCustomer-id");
		},
	});
	colOpts.push({
		"data": "sampleOrder-crmCustomer-name",
		"title": biolims.sample.medicalInstitutions,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-crmCustomer-name");
		},
	});
	colOpts.push({
		"data": "sampleOrder-createUser-id",
		"title": biolims.sample.createUserId,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-createUser-id");
		},
	});
	colOpts.push({
		"data": "sampleOrder-createUser-name",
		"title": biolims.sample.createUserName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-createUser-name");
		},
	});
	colOpts.push({
		"data": "sampleOrder-stateName",
		"title": biolims.common.state,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-stateName");
		},
	});
	
	
	colOpts.push({
		"data": "sampleOrder-barcode",
		"title": biolims.common.barCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-barcode");
		},
		"visible": false
	});
	colOpts.push({
		"data": "sampleOrder-hospitalPatientID",
		"title": biolims.master.hospitalPatient,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-hospitalPatientID");
		},
	});
	colOpts.push({
		"data": "sampleOrder-birthDate",
		"title": biolims.common.birthDay,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-birthDate");
		},
	});
	colOpts.push({
		"data": "sampleOrder-createDate",
		"title": biolims.tStorage.createDate,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-createDate");
		},
	});
	
	colOpts.push({
		"data": "sampleOrder-idCard",
		"title":  biolims.common.idCard,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-idCard");
		},
	});
	
	colOpts.push({
		"data": "sampleOrder-productName",
		"title":  biolims.crmDoctorItem.productName,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-productName");
		},
	});
	
	colOpts.push({
		"data": "sampleOrder-note",
		"title":biolims.common.note,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-note");
		},
		"className":"edit"
	});
	var options = table(true, $("#sampleOrderChange_id").val(), "/system/sample/sampleOrderChange/showSampleOrderChangeItemListJson.action", colOpts,null);

	myTable = renderData($("#sampleOrderChangeItemdiv"), options);
	//选择数据并提示
	myTable.on('draw', function() {
		var index = 0;
		$("#sampleOrderChangeItemdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		
//		oldChangeLog = myTable.ajax.json();
	});
	/*myTable.on('draw', function() {
		sampleOrderChange = myTable.ajax.json();
	});*/
	//日期格式化
	$("#sampleOrderChange_birthDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	var id=$("#sampleOrderChange_id").val();
	if(id == "NEW") {
		$("#sampleOrderChangeModal").modal("show");
		choseWhich();
	}

	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	if(handlemethod == "modify") {
		$("#sampleOrderChange_id").prop("readonly", "readonly");
	}

	var productId = $("#sampleOrderChange_productId").val();
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'sampleOrderChange', $("#sampleOrderChange_id").val());

	//ai图片识别
	$("#aiBtn").click(function() {
		$("#AiPicture").click();
	});
	$("#aiBtn2").click(function() {
		$("#AiPicture2").click();
	});

});

/**
 * onchange（参数）
 * 
 * @param id
 */
function change(id) {
	$("#" + id).css({
		"background-color": "white",
		"color": "black"
	});
}

function add() {
	window.location = window.ctx +
		"/system/sample/sampleOrderChange/editsampleOrderChange.action";
}

function list() {
	window.location = window.ctx +
		'/system/sample/sampleOrderChange/showsampleOrderChangeTable.action';
}

//验证id
function validId() {
	$.ajax({
		type: "post",
		url: window.ctx + '/common/hasId.action',
		async: false,
		data: {
			id: $("#sampleOrderChange_id").val(),
			obj: 'SampleOrderChange'
		},
		success: function(data) {
			var obj = JSON.parse(data);
			if(obj.success) {
				if(obj.bool) {
					bool2 = true;
				} else {
					top.layer.msg(obj.msg);
				}
			} else {
				top.layer.msg(biolims.common.checkingFieldCodingFailure);
			}
		}
	});
}

//提交
function tjsp() {
	var requiredField = requiredFilter();
	if(!requiredField) {
		return false;
	}
	top.top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	}, function(index) {
		top.layer.open({
			title: biolims.common.approvalProcess,
			type: 2,
			anim: 2,
			area: ['800px', '500px'],
			btn: biolims.common.selected,
			content: window.ctx + "/workflow/processinstance/toStartView.action?formName=SampleOrder",
			yes: function(index, layer) {
				var datas = {
					userId: userId,
					userName: userName,
					formId: $("#sampleOrderChange_id").val(),
					title: $("#sampleOrderChange_name").val()
				}
				ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {
							callback(data);
						}
						dialogWin.dialog("close");
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
				top.layer.close(index);
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}

		});
		top.layer.close(index);
	});

}

function save() {
	//自定义字段
	//拼自定义字段儿（实验记录）
	var inputs = $("#fieldItemDiv input");
	var options = $("#fieldItemDiv option");
	var contentData = {};
	var checkboxArr = [];
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	var dataJson=JSON.stringify(jsonn);
	$("#fieldItemDiv .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
//	document.getElementById("fieldContent").value = JSON.stringify(contentData);
	var changeLog = "客服订单追加修改申请-";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});

	//子表日志
	var changeLogItem = biolims.order.itemEdit + "：";
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "modify") {
		top.layer.load(4, {
			shade: 0.3
		});
		$("#form1").attr("action", "/system/sample/sampleOrderChange/save.action");
		$("#form1").submit();
		top.layer.closeAll();
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#sampleOrderChange_id").val(),
				obj: 'SampleOrderChange'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					top.layer.load(4, {
						shade: 0.3
					});
					$("#form1").attr("action", "/system/sample/sampleOrderChange/save.action");
					$("#form1").submit();
					top.layer.closeAll();
				}
			}
		});
	}
}

//改变状态
function changeState() {
    var	id=$("#sampleOrderChange_id").val()
	var paraStr = "formId=" + id +
		"&tableId=SampleOrderChange";
	console.log(paraStr)
	top.layer.open({
		title:biolims.common.approvalProcess,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(indexx, layer) {
			top.layer.confirm(biolims.common.approve , {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		

	});
}


function makeSampleType() {
	var checks = "";
	$("[id='sck_checkedBoxTest']:checked").each(function() {
		checks += $(this).val() + ",";
	});
	document.getElementById("sampleOrder_sampleTypeId").value = checks;

}
/**
 * 复选框选中方法
 */
function viewSampleType() {

	var str1 = document.getElementById("sampleOrder_sampleTypeId").value;
	$("input[id='sck_checkedBoxTest']").each(function() {
		if(str1.indexOf($(this).val()) >= 0) {
			$(this).attr("checked", true);
		}
	});
}

function checkSubmit() {
	if($("#sampleOrder_id").val() == null || $("#sampleOrder_id").val() == "") {
		top.layer.msg(biolims.common.codeNotEmpty);
		return false;
	};
	return true;
}

//取消,修改,追加
function choseWhich() {
	//扫码、上传、新建的不同流向
	$("#modal-body .col-xs-12").click(function() {
		var index = $(this).index();
		//取消
		if(index == 0) {
			$("#sampleOrderChangeModal").modal("hide");
			showSampleOrderTable();
		}
		//上传
		if(index == 1) {
			$("#sampleOrderChangeModal").modal("hide");
			showSampleOrderTable1();
		}
		//新建
		if(index == 2) {
			$("#sampleOrderChangeModal").modal("hide");
			showSampleOrderTable2();
		}
	});
}

function showSampleOrderTable() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
			.eq(0).text();
			var sampleOrderChangId=$("#sampleOrderChange_id").val();
			$.ajax({
				type: 'post',
				url: '/system/sample/sampleOrder/addPurchaseApplyMakeUp.action',
				data: {
					id: id,
					sampleOrderChangId:sampleOrderChangId
				},
				success: function(data) {
					var data = JSON.parse(data)
					if(data.success) {
						window.location=ctx+"/system/sample/sampleOrderChange/editsampleOrderChange.action?id="+data.id;
					} else {
						top.layer.msg(biolims.common.addToDetailFailed);
					};
				}
			})
			top.layer.close(index);
		},
	})
}
function showSampleOrderTable1() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
			.eq(0).text();
			var sampleOrderChangId=$("#sampleOrderChange_id").val();
			var flag="EDIT";
			window.location=ctx+"/system/sample/sampleOrder/editSampleOrder.action?id="+id+"&flag="+flag;
			top.layer.close(index);
		},
	})
}
function showSampleOrderTable2() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
			.eq(0).text();
			var sampleOrderChangId=$("#sampleOrderChange_id").val();
			var flag="ADD";
			window.location=ctx+"/system/sample/sampleOrder/editSampleOrder.action?id="+id+"&flag="+flag;
			top.layer.close(index);
		},
	})
}
//审核人
function showAcceptUser(){
	top.layer.open({
		title: biolims.user.selectionAuditor,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
			$("#sampleOrderChange_acceptUser_name").val(name);
			$("#sampleOrderChange_acceptUser_id").val(id);
			top.layer.close(index);
		},
	})
}