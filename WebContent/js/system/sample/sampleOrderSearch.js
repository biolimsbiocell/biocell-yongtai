/* 
 * 文件名称 :
 * 创建者 : 郭恒开
 * 创建日期: 2018/04/23
 * 文件描述: 订单儿搜索
 * 
 */
var sampleOrderTable;
$(function() {
	$('#searchOption').searchExtend({
		searchOptions: [biolims.master.product , biolims.sample.medicalInstitutions, biolims.common.doctor, biolims.common.sales], //多选查询的标题
		searchOptionsUrl: '/system/sample/sampleOrder/searchOptions.action', //多选查询的url
		barChartId: "sampleBar", //柱图的div的ID
		pieChartId: "samplePie", //饼图的div的ID
		getChartUrl: "/system/sample/sampleOrder/sampleOrderChart.action", //柱图和饼图的url
		inputsearchOptions:inputsearchOptions()//不常用的输入框查询
	});
	//table表
	var colData = [{
		"data": "id",
		"title": biolims.common.orderCode,
	}, {
		"data": "name",
		"title": biolims.common.sname,
	}, {
		"data": "gender",
		"title": biolims.common.gender,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	}, {
		"data": "age",
		"title": biolims.common.age,
	}, {
		"data": "nation",
		"title": biolims.common.race,
	}, {
		"data": "zipCode",
		"title": biolims.crm.postcode,
	}, {
		"data": "diagnosisDate",
		"title": biolims.common.diagnosisDate,
	}, {
		"data": "commissioner-name",
		"title": biolims.common.sales,
	}, {
		"data": "nativePlace",
		"title": biolims.crm.placeOfBirth,
	}, {
		"data": "medicalNumber",
		"title": biolims.sample.medicalNumber,
	}, {
		"data": "familyCode",
		"title": biolims.common.familyCode,
	}, {
		"data": "family",
		"title": biolims.sample.family,
	}, {
		"data": "familyPhone",
		"title": biolims.sample.familyPhone,
	}, {
		"data": "familySite",
		"title": biolims.sample.familySite,
	}, {
		"data": "crmCustomer-name",
		"title": biolims.sample.medicalInstitutions,
	}, {
		"data": "createUser-name",
		"title": biolims.sample.createUserName,
	}, {
		"data": "stateName",
		"title": biolims.common.state,
	}, {
		"data": "barcode",
		"title": biolims.common.barCode,
	}, {
		"data": "familyHistorysummary",
		"title": biolims.order.familyHistorysummary,
	}, {
		"data": "prenatal",
		"title": biolims.order.prenatal,
	}, {
		"data": "hospitalPatientID",
		"title": biolims.master.hospitalPatient,
	}, {
		"data": "weights",
		"title": biolims.common.weight,
	}, {
		"data": "gestationalWeeks",
		"title": biolims.order.gestationalWeeks,
	}, {
		"data": "birthDate",
		"title": biolims.common.birthDay,
	}, {
		"data": "createDate",
		"title": biolims.tStorage.createDate,
		"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	}, {
		"data": "idCard",
		"title": biolims.common.idCard,
	}, {
		"data": "productName",
		"title": biolims.crmDoctorItem.productName,
	}];
	var options = table(true, "",
		"/system/sample/sampleOrder/sampleOrderSearchTable.action", colData, null)
	sampleOrderTable = renderRememberData($("#main"), options);
})


function inputsearchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{

			"txt": biolims.common.gender,
			"type": "select",
			"options": biolims.common.pleaseChoose + "|" + biolims.common.male + "|" + biolims.common.female + "|" + biolims.common.unknown,
			"changeOpt": "''|1|0|3",
			"searchName": "gender"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"txt": biolims.common.confirmDateStart,
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.common.confirmDateEnd,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "target",
			"targetDiv": $("#searchOther")
		}
	];
}