$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.orderCode,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
	})
	colOpts.push({
		"data": "gender",
		"title": biolims.common.gender,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "age",
		"title": biolims.common.age,
	});
	colOpts.push({
		"data": "medicalNumber",
		"title": biolims.sample.medicalNumber,
	});
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.state,
	});
	var tbarOpts = [];
	var options = table(false, null,
		'/system/sample/sampleOrder/showSampleOrderSelListJson.action?orderNum='+$("#orderNum").val(), colOpts, tbarOpts)
	var addSampleOrder = renderData($("#addSampleOrder"), options);
	$("#addSampleOrder").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addSampleOrder_wrapper .dt-buttons").empty();
			$('#addSampleOrder_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addSampleOrder tbody tr");
			addSampleOrder.ajax.reload();
			addSampleOrder.on('draw', function() {
				trs = $("#addSampleOrder tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})