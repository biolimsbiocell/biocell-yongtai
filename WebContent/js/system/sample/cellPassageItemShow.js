var cellPassageItemShowTab;
var cellPassageItemShowChangeLog;
$(function() {
	// 加载子表
	var colOpts = [];
	colOpts.push({
		"data" : 'id',
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : 'code',
		"title" : "样本编号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	});
	colOpts.push({
		"data" : 'productId',
		"title" : "检测产品编号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "productId");
		}
	});
	colOpts.push({
		"data" : 'productName',
		"title" : "检测产品",
		"createdCell" : function(td) {
			$(td).attr("saveName", "productName");
		}
	});
	colOpts.push({
		"data" : 'sampleType',
		"title" : "样本类型",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleType");
		}
	});
	colOpts.push({
		"data" : 'stepNum',
		"title" : "实验步骤号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "stepNum");
		}
	});
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
	}
	var cellPassageItemShowTabOptions = table(true, $("#sampleInfo_orderNum").val(), '/system/sample/sampleMain/showCellPassageItemFromOrderNo.action',
			colOpts, tbarOpts)
	cellPassageItemShowTab = renderData($("#cellPassageItemShow"), cellPassageItemShowTabOptions);
	cellPassageItemShowTab.on('draw', function() {
		cellPassageItemShowChangeLog = cellPassageItemShowTab.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});