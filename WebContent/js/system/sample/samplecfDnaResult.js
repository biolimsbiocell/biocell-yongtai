var samplecfdnaResultGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	 fields.push({
			name:'id',
			type:"string"
		});
		   fields.push({
			name:'name',
			type:"string"
		});
		   fields.push({
			name:'code',
			type:"string"
		});
		   fields.push({
			name:'sampleCode',
			type:"string"
		});
		   fields.push({
			name:'jkTaskId',
			type:"string"
		});
		   fields.push({
			name:'indexa',
			type:"string"
		});
		   fields.push({
			name:'volume',
			type:"string"
		});
		   fields.push({
			name:'unit',
			type:"string"
		});
		   fields.push({
			name:'result',
			type:"string"
		});
		   fields.push({
			name:'reason',
			type:"string"
		});
		   fields.push({
			name:'submit',
			type:"string"
		});
		   fields.push({
			name:'patientName',
			type:"string"
		});
		   fields.push({
			name:'productId',
			type:"string"
		});
		   fields.push({
			name:'productName',
			type:"string"
		});
		   fields.push({
			name:'inspectDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
		   fields.push({
			name:'acceptDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
		   fields.push({
			name:'idCard',
			type:"string"
		});
		   fields.push({
			name:'phone',
			type:"string"
		});
		   fields.push({
			name:'orderId',
			type:"string"
		});
		   fields.push({
			name:'sequenceFun',
			type:"string"
		});
		   fields.push({
			name:'reportDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
		   fields.push({
			name:'state',
			type:"string"
		});
		   fields.push({
			name:'method',
			type:"string"
		});
		   fields.push({
			name:'isExecute',
			type:"string"
		});
		   fields.push({
			name:'note',
			type:"string"
		});
		    fields.push({
			name:'cfdnaTask-id',
			type:"string"
		});
		    fields.push({
			name:'cfdnaTask-name',
			type:"string"
		});
		   fields.push({
			name:'rowCode',
			type:"string"
		});
		   fields.push({
			name:'colCode',
			type:"string"
		});
		   fields.push({
			name:'counts',
			type:"string"
		});
		   fields.push({
			name:'contractId',
			type:"string"
		});
		   fields.push({
			name:'projectId',
			type:"string"
		});
		   fields.push({
			name:'orderType',
			type:"string"
		});
		   fields.push({
			name:'classify',
			type:"string"
		});
		   fields.push({
			name:'dataType',
			type:"string"
		});
		   fields.push({
			name:'dataNum',
			type:"string"
		});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'jkTaskId',
		hidden : true,
		header:biolims.common.jkTaskId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'index',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'unit',
		hidden : false,
		header:biolims.common.unit,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var storeGoodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
//	});
//	var goodCob = new Ext.form.ComboBox({
//		store : storeGoodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'处理结果',
//		width:20*6,
//		editor : goodCob,
//		renderer : Ext.util.Format.comboRenderer(goodCob)
//	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.reason,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var storesubmitCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '是' ], [ '0', '否' ] ]
//	});
//	var submitCob = new Ext.form.ComboBox({
//		store : storesubmitCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'submit',
//		hidden : false,
//		header:'是否提交',
//		width:20*6,
//		editor : submitCob,
//		renderer : Ext.util.Format.comboRenderer(submitCob)
//	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:biolims.common.productId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : false,
		header:biolims.common.inspectDate,
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:biolims.common.idCard,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:biolims.common.phone,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : false,
		header:biolims.common.sequencingFun,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:biolims.common.method,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:biolims.common.confirm2Execute,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cfdnaTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cfdnaTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:biolims.common.counts,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataType',
		hidden : true,
		header:biolims.common.dataType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataNum',
		hidden : true,
		header:biolims.common.dataNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSamplecfDnaListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.master.cfDNAEvaluation;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	samplecfdnaResultGrid = gridEditTable("samplecfDnaResultdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
