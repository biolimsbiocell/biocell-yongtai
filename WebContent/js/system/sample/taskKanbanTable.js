var taskKanbanTable;
$(function() {
	var colData = [{
		"data": "id",
		"title": "操作",
		"width": "60px",
		"render": function(data) {
			return "<button id=" + data + " class='btn btn-info btn-xs plus' onclick='showKanBanStep(this)'>展开</button>";
		}
	},{
		"data": "techJkService-id",
		"title": "任务单号",
	}, {
		"data": "techJkService-confirmDate",
		"title": "下达时间",
	}, {
		"data": "id",
		"title": biolims.common.orderCode,
	}, {
		"data": "productName",
		"title": biolims.crmDoctorItem.productName,
	}, {
		"data": "newTask",
		"title": "最近任务单号",
	}];
	
	
var options = table(true, "",
	"/system/sample/sampleOrder/showSampleOrderTaskKanbanJson.action",colData , null)
taskKanbanTable = renderRememberData($("#main"), options);
//恢复之前查询的状态
$('#main').on('init.dt', function() {
	recoverSearchContent(taskKanbanTable);
})
});

function showKanBanStep(that){
	if(!$(that).parents("tr").next(".subTable").length) {
		$(that).parents("tr").after('<tr class="subTable" style="display:none"><td colspan="7"><table class="table table-bordered table-condensed" style="font-size: 12px;"></table></td></tr>');
	}
	var corspanTr = $(that).parents("tr").next(".subTable");
	if($(that).hasClass("plus")) { //展开
		$(that).removeClass("plus").addClass("minus");
		that.innerText = "关闭";
		corspanTr.slideDown();
		var elemen = corspanTr.find("table");
		if(!elemen.find("tr").length) {
			var id = that.id;
			renderSubTableSteps(elemen, id);
		}
	} else { //关闭
		corspanTr.slideUp();
		that.innerText = "展开";
		$(that).removeClass("minus").addClass("plus");
	}
}

function renderSubTableSteps(elemen, id) {
	var steps = '<br/><div id="wizard" class="form_wizard wizard_horizontal">' +
				'<ul class="wizard_steps">' +
				'<li><a onclick="showStep1(this)" mainid=\''+id+'\' class="done step"><span class="step_no">1</span><span class="step_descr">Step 1<br/><small>订单信息</small></span></a></li>' +
				'<li><a onclick="showStep2(this)" mainid=\''+id+'\' class="done step"><span class="step_no">2</span><span class="step_descr">Step 2<br/><small>样本信息</small></span></a></li>' +
				'<li><a onclick="showStep3(this)" mainid=\''+id+'\' class="done step"><span class="step_no">3</span><span class="step_descr">Step 3<br/><small>生产追踪</small></span></a></li>' +
				'</ul></div>';
	elemen.append(steps);
}

//点击1step
function showStep1(that){
	var id = $(that).attr("mainid");
	top.layer.open({
		title: "订单信息",
		type: 2,
		area: ["800px", "500px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/sample/sampleOrder/showStep1.action?id="+id, ''],
		yes: function(index, layer) {
			top.layer.closeAll();
		},
	})
}

//点击2step
function showStep2(that){
	var id = $(that).attr("mainid");
	top.layer.open({
		title: "订单明细",
		type: 2,
		area: ["800px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/sample/sampleOrder/showStep2.action?id="+id, ''],
		yes: function(index, layer) {
			top.layer.closeAll();
		},
	})
}

//点击3step
function showStep3(that){
	var id = $(that).attr("mainid");
	top.layer.open({
		title: "订单追踪信息",
		type: 2,
		area: ["800px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/sample/sampleOrder/showStep3.action?id="+id, ''],
		yes: function(index, layer) {
			top.layer.closeAll();
		},
	})
}

//查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/sample/sampleOrder/viewSampleOrderKanban.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{

			"txt": biolims.common.gender,
			"type": "select",
			"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
			"changeOpt": "''|1|0|3",
			"searchName": "gender"
		},
		{
			"txt": biolims.wk.confirmUserName,
			"type": "top.layer",
			"searchName": "confirmUser",
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"type": "table",
			"table": taskKanbanTable
		}
	];
}