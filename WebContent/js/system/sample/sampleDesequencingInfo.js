var sampleDesequencingInfoGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	 fields.push({
			name:'id',
			type:"string"
		});
		   fields.push({
			name:'name',
			type:"string"
		});
		   fields.push({
			name:'code',
			type:"string"
		});
		   fields.push({
			   name:'sampleCode',
			   type:"string"
		   });
		   fields.push({
			   name:'outPut',
			   type:"string"
		   });
		   fields.push({
			   name:'clusters',
			   type:"string"
		   });
		   fields.push({
			   name:'pf',
			   type:"string"
		   });
//		   fields.push({
//			   name:'reasonType',
//			   type:"string"
//		   });
		   fields.push({
			   name:'indexLibrary',
			   type:"string"
		   });
		   fields.push({
			name:'poolingCode',
			type:"string"
		});
		   fields.push({
			name:'wkCode',
			type:"string"
		});
		   fields.push({
			name:'assess',
			type:"string"
		});
		   fields.push({
			name:'backDate',
			type:"string"
		});
		   fields.push({
			name:'errorRate',
			type:"string"
		});
		   fields.push({
			name:'result',
			type:"string"
		});
		   fields.push({
			name:'advice',
			type:"string"
		});
		   fields.push({
			name:'lane',
			type:"string"
		});
		   fields.push({
			name:'project',
			type:"string"
		});
		   fields.push({
			name:'need',
			type:"string"
		});
		   fields.push({
			name:'method',
			type:"string"
		});
		   fields.push({
			name:'base',
			type:"string"
		});
		   fields.push({
			name:'reads',
			type:"string"
		});
		   fields.push({
			name:'pct',
			type:"string"
		});
		   fields.push({
			name:'adapter',
			type:"string"
		});
		   fields.push({
			name:'gc',
			type:"string"
		});
		   fields.push({
			   name:'sampleCount',
			   type:"string"
		   });
		   fields.push({
			name:'q20',
			type:"string"
		});
		   fields.push({
			name:'q30',
			type:"string"
		});
		   fields.push({
			name:'index',
			type:"string"
		});
		   fields.push({
			name:'ind',
			type:"string"
		});
		   fields.push({
			name:'dup',
			type:"string"
		});
		   fields.push({
			name:'ployAT',
			type:"string"
		});
		   fields.push({
			name:'note',
			type:"string"
		});
		    fields.push({
			name:'desequencingTask-id',
			type:"string"
		});
		    fields.push({
			name:'desequencingTask-name',
			type:"string"
		});
		    
		    fields.push({
			name:'contractId',
			type:"string"
		});
		   fields.push({
			name:'projectId',
			type:"string"
		});
		   fields.push({
			name:'orderType',
			type:"string"
		});
		   fields.push({
			name:'techTaskId',
			type:"string"
		});
		   fields.push({
			name:'state',
			type:"string"
		});
		 /**===========================================================>
		 * 
		 * 添加下机质控结果明细2
		 */
		   fields.push({
				name:'lane2',
				type:"string"
			});
		   fields.push({
				name:'kit',
				type:"string"
			});
		   fields.push({
				name:'raw_reads',
				type:"string"
			});
		   fields.push({
				name:'raw_len',
				type:"string"
			});
		   fields.push({
				name:'raw_GC',
				type:"string"
			});
		   
		   fields.push({
				name:'raw_size',
				type:"string"
			});
		   fields.push({
				name:'hq_reads',
				type:"string"
			});
		   fields.push({
				name:'hq_len',
				type:"string"
			});
		   fields.push({
				name:'hq_GC',
				type:"string"
			});
		   fields.push({
				name:'hq_size',
				type:"string"
			});
		   
		   fields.push({
				name:'hq_reads_pct',
				type:"string"
			});
		   fields.push({
				name:'hq_data_pct',
				type:"string"
			});
		   fields.push({
				name:'demultiplexing_Reads',
				type:"string"
			});
		   fields.push({
				name:'demultiplexing_PCT',
				type:"string"
			});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.sequencing.offlineQCId,
		width:20*6
	});
//	cm.push({
//		dataIndex:'sampleCode',
//		hidden : true,
//		header:'样本号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'lane',
//		hidden : false,
//		header:'lane',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'outPut',
//		hidden : true,
//		header:'产量',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'clusters',
//		hidden : false,
//		header:'Clusters',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'pf',
//		hidden : false,
//		header:'PF',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'sampleCount',
//		hidden : true,
//		header:'样品量',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'project',
//		hidden : false,
//		header:'project',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'indexLibrary',
//		hidden : false,
//		header:'文库号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'need',
//		hidden : false,
//		header:'need',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'base',
//		hidden : false,
//		header:'base',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'reads',
//		hidden : false,
//		header:'reads',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'pct',
//		hidden : false,
//		header:'pct',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'adapter',
//		hidden : false,
//		header:'adapter',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'gc',
//		hidden : false,
//		header:'gc',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'q20',
//		hidden : false,
//		header:'q20',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'q30',
//		hidden : false,
//		header:'q30',
//		width:20*6
//	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:biolims.pooling.code,
		width:20*6
	});
//	cm.push({
//		dataIndex:'index',
//		hidden : false,
//		header:'index',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'ind',
//		hidden : false,
//		header:'ind',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'dup',
//		hidden : false,
//		header:'dup',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'ployAT',
//		hidden : false,
//		header:'ployAT',
//		width:20*6
//	});
//	
//	cm.push({
//		dataIndex:'assess',
//		hidden : true,
//		header:'测试评估',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'backDate',
//		hidden : true,
//		header:'反馈时间',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'errorRate',
//		hidden : true,
//		header:'错误率',
//		width:20*6
//	});
//	var result = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '合格'
//			},{
//				id : '0',
//				name : '不合格'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'结果',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(result),editor: result
//	});
//	cm.push({
//		dataIndex:'advice',
//		hidden : true,
//		header:'处理意见',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'method',
//		hidden : true,
//		header:'处理方式',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'note',
//		hidden : false,
//		header:'备注',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'desequencingTask-id',
//		hidden : true,
//		header:'关联主表ID',
//		width:20*10
//	});
//	cm.push({
//		dataIndex:'desequencingTask-name',
//		hidden : true,
//		header:'关联主表',
//		width:20*10
//	});
//	
//	cm.push({
//		dataIndex:'contractId',
//		hidden : false,
//		header:'合同ID',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'projectId',
//		hidden : false,
//		header:'项目ID',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'orderType',
//		hidden : false,
//		header:'任务单类型',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'techTaskId',
//		hidden : true,
//		header:'科技服务任务单',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'state',
//		hidden : true,
//		header:'状态',
//		width:20*6
//	});
	/**===========================================================>
	 * 
	 * 添加下机质控结果明细2
	 */
	cm.push({
		dataIndex:'lane2',
		hidden : false,
		header:biolims.pooling.lane2,
		width:20*6
	});
	cm.push({
		dataIndex:'kit',
		hidden : false,
		header:biolims.sequencing.kit,
		width:20*6
	});
	cm.push({
		dataIndex:'raw_reads',
		hidden : false,
		header:biolims.master.raw_reads,
		width:20*6
	});
	cm.push({
		dataIndex:'raw_len',
		hidden : false,
		header:biolims.master.raw_len,
		width:20*6
	});
	cm.push({
		dataIndex:'raw_GC',
		hidden : false,
		header:biolims.master.raw_GC,
		width:20*6
	});
	cm.push({
		dataIndex:'raw_size',
		hidden : false,
		header:biolims.master.raw_size,
		width:20*6
	});
	cm.push({
		dataIndex:'hq_reads',
		hidden : false,
		header:biolims.master.hq_reads,
		width:20*6
	});
	cm.push({
		dataIndex:'hq_len',
		hidden : false,
		header:biolims.master.hq_len,
		width:20*6
	});
	cm.push({
		dataIndex:'hq_GC',
		hidden : false,
		header:biolims.master.hq_GC,
		width:20*6
	});
	cm.push({
		dataIndex:'hq_size',
		hidden : false,
		header:biolims.master.hq_size,
		width:20*6
	});
	cm.push({
		dataIndex:'hq_reads_pct',
		hidden : false,
		header:biolims.master.hq_reads_pct,
		width:20*6
	});
	
	cm.push({
		dataIndex:'hq_data_pct',
		hidden : false,
		header:biolims.master.hq_data_pct,
		width:20*6
	});
	cm.push({
		dataIndex:'demultiplexing_Reads',
		hidden : false,
		header:biolims.master.demultiplexing_Reads,
		width:20*6
	});
	cm.push({
		dataIndex:'demultiplexing_PCT',
		hidden : false,
		header:biolims.master.demultiplexing_PCT,
		width:20*6
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSampleDesequencingInfoListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.sequencing.offlineQCResult;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleDesequencingInfoGrid = gridEditTable("sampleDesequencingInfodiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
