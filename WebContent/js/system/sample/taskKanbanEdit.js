var taskKanbanEditTable;
$(function() {
	// 加载子表
	var id = $("#sampleOrder_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		},
		"visible": false,
		"className": "edit"
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		},
		"visible": true
	});
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		},
		"visible": true
	});
	colOpts.push({
		"data": "stageTime",
		"title": biolims.sample.stageTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "stageTime");
		},
		"visible": false
	});
	colOpts.push({
		"data": "startDate",
		"title": biolims.common.startTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "startDate");
		},
		"visible": true
	});
	
	colOpts.push({
		"data": "endDate",
		"title": biolims.common.endTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "endDate");
		},
		"visible": true
	});
	colOpts.push({
		"data": "tableTypeId",
		"title": biolims.sample.stageId,
		"createdCell": function(td) {
			$(td).attr("saveName", "tableTypeId");
		},
		"visible": false
	});
	colOpts.push({
		"data": "stageName",
		"title": biolims.sample.stageName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stageName");
		},
		"visible": true
	});
	colOpts.push({
		"data": "acceptUser-id",
		"title": biolims.master.operUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptUser-id");
		},
		"visible": false
	});
	
	colOpts.push({
		"data": "acceptUser-name",
		"title": biolims.master.operUserName,
		"visible": true
	});
	
	colOpts.push({
		"data": "taskMethod",
		"title": biolims.sample.taskMethod,
		"createdCell": function(td) {
			$(td).attr("saveName", "taskMethod");
		},
		"visible": true
	});
	colOpts.push({
		"data": "taskResult",
		"title": biolims.sample.taskResult,
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "taskId",
		"title": biolims.sample.taskId,
		"createdCell": function(td) {
			$(td).attr("saveName", "taskId");
		},
		"visible": true
	});
	colOpts.push({
		"data": "note",
		"title": biolims.sample.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		},
		"visible": true
	});
	

	var sampleStateOptions = table(true,
		id,'/system/sample/sampleMain/showSampleStateTaskKanbanJson.action', colOpts, null);
	taskKanbanEditTable = renderData($("#sampleStateGrid"), sampleStateOptions);
	taskKanbanEditTable.on('draw', function() {
		sampleStateChangeLog = taskKanbanEditTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
//	stepViewChange();
});
