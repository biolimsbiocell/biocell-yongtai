var DicTypeTable;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
	});
	colOpts.push({
		"data" : "name",
		"title" : "名称",
	});
	colOpts.push({
		"data" : "principal",
		"title" : "负责人",
	});
	colOpts.push({
		"data" : "phone",
		"title" : "联系电话",
	});
	colOpts.push({
		"data" : "createUser-name",
		"title" : "创建人",
	});
	colOpts.push({
		"data" : "createDate",
		"title" : "创建时间",
	});
	var tbarOpts = [];
	var addDicTypeOptions = table(false, null, '/system/sample/sampleOrder/selectAgreementTaskTableJson.action',
			colOpts, tbarOpts)//传值
			DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
	$("#addDicTypeTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮,弹窗搜索就隐藏下一行
				$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
			DicTypeTable.on('draw', function() {
				trs = $("#addDicTypeTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})