var sampleStateGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'stageTime',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'tableTypeId',
		type : "string"
	});
	fields.push({
		name : 'stageName',
		type : "string"
	});
	fields.push({
		name : 'acceptUser-id',
		type : "string"
	});
	fields.push({
		name : 'acceptUser-name',
		type : "string"
	});
	fields.push({
		name : 'taskId',
		type : "string"
	});
	fields.push({
		name : 'taskMethod',
		type : "string"
	});
	fields.push({
		name : 'taskResult',
		type : "string"
	});
	fields.push({
		name : 'techTaskId',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'note2',
		type : "string"
	});
	fields.push({
		name : 'note3',
		type : "string"
	});
	fields.push({
		name : 'note4',
		type : "string"
	});
	fields.push({
		name : 'note5',
		type : "string"
	});
	fields.push({
		name : 'note6',
		type : "string"
	});
	fields.push({
		name : 'note7',
		type : "string"
	});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 80,
		hidden:true,
		sortable : true
	});
	cm.push({
		dataIndex : 'code',
		header : biolims.common.code,
		width : 20 * 7,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'sampleCode',
		header : biolims.common.sampleCode,
		width : 120,

		sortable : true
	});
	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.productId,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.productName,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'stageTime',
		header : biolims.sample.stageTime,
		width : 20 * 10,
		sortable : true
	});
	
	cm.push({
		dataIndex : 'startDate',
		hidden : false,
		header : biolims.common.startTime,
		width : 13 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : biolims.common.endTime,

		width : 13 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'tableTypeId',
		hidden : true,
		header : biolims.sample.stageId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'stageName',
		header : biolims.sample.stageName,

		width : 10 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'acceptUser-id',
		header : biolims.master.operUserId,
		width : 20 * 6,
		hidden:true,
		sortable : true
	});
	cm.push({
		dataIndex : 'acceptUser-name',
		header : biolims.master.operUserName,
		width : 15 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'taskMethod',
		header : biolims.sample.taskMethod,

		width : 10 * 10,
		sortable : true
	});
	var storeisDepotCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', "不合格" ], [ '1', "合格" ] ]
	});
	var isDepotCob = new Ext.form.ComboBox({
		store : storeisDepotCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'taskResult',
		header : biolims.sample.taskResult,
		width : 20 * 6,
		
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(isDepotCob)
	});
	cm.push({
		dataIndex : 'taskId',
		hidden : false,
		header : biolims.sample.taskId,
		width : 15 * 10,
		sortable : true
	});
	
	

	cm.push({
		dataIndex : 'note',
		header : biolims.sample.note,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'note2',
		hidden : true,
		header : biolims.sample.note2,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note3',
		hidden : true,
		header : biolims.sample.note3,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note4',
		hidden : true,
		header : biolims.sample.note4,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note5',
		hidden : true,
		header : biolims.sample.note5,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note6',
		hidden : true,
		header : biolims.sample.note6,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note7',
		hidden : true,
		header : biolims.sample.note7,
		width : 20 * 10,
		sortable : true
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSampleStateListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.common.sampleState;
	opts.height = document.body.clientHeight - 200;
//	opts.rowselect = function(id) {
//		$("#selectId").val(id);
//	};
//	opts.rowdblclick = function(id) {
//		$('#selectId').val(id);
//		edit();
//	};
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.sample.checkOrder,
		handler : select
	});
	opts.isShowDefaultTbar = false;
	sampleStateGrid = gridEditTable("SampleStatediv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function select(){
	var selectRecord = sampleStateGrid.getSelectionModel().getSelections();
	var ids = [];	
	for ( var i = 0; i < selectRecord.length; i++) {
		ids.push(selectRecord[i].get("id"));
	}	
	if(selectRecord.length>0){
		ajax("post", "/system/sample/sampleMain/selSampleStateById.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				$.each(data.list,function(i,obj){
					var table=obj.tableTypeId;
					var id=obj.taskId;
					if(table=="SampleReceive"){//开箱检验
						window.open('/sample/sampleReceive/editSampleReceive.action?id='+id, '', '');
					}
					if(table=="PlasmaTask"){//样本处理
						window.open('/experiment/plasma/bloodSplit/editBloodSplit.action?id='+id, '', '');
					}
					if(table=="DnaTask"){//核酸提取
						window.open('/experiment/dna/experimentDnaGet/editExperimentDnaGet.action?id='+id, '', '');
					}
					if(table=="UfTask"){//超声破碎
						window.open('/experiment/uf/ufTask/editUfTask.action?id='+id, '', '');
					}
					if(table=="TechCheckServiceTask"){//质检实验
						window.open('/experiment/check/techCheckServiceTask/editTechCheckServiceTask.action?id='+id, '', '');
					}
					if(table=="WkTask"){//文库构建
						window.open('/experiment/wk/editWK.action?id='+id, '', '');
					}
					if(table=="PoolingTask"){//富集
						window.open('/experiment/pooling/editPooling.action?id='+id, '', '');
					}
					if(table=="WkBlendTask"){//文库混合
						window.open('/experiment/sj/wkblend/wkBlendTask/editWkBlendTask.action?id='+id, '', '');
					}
					if(table=="SangerTask"){//Sanger实验
						window.open('/experiment/sanger/sangerTask/editSangerTask.action?id='+id, '', '');
					}
				});
			} else {
				loadMarsk.hide();
				message(biolims.sample.checkFailed);
			}
		}, null);
	}else{
		message(biolims.common.pleaseSelect);
	}
	
}
