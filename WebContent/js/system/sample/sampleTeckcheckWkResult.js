var sampleTeckcheckWkResultGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'stageTime',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'tableTypeId',
		type : "string"
	});
	fields.push({
		name : 'stageName',
		type : "string"
	});
	fields.push({
		name : 'acceptUser',
		type : "string"
	});
	fields.push({
		name : 'taskId',
		type : "string"
	});
	fields.push({
		name : 'taskMethod',
		type : "string"
	});
	fields.push({
		name : 'taskResult',
		type : "string"
	});
	fields.push({
		name : 'techTaskId',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'note2',
		type : "string"
	});
	fields.push({
		name : 'note3',
		type : "string"
	});
	fields.push({
		name : 'note4',
		type : "string"
	});
	fields.push({
		name : 'note5',
		type : "string"
	});
	fields.push({
		name : 'note6',
		type : "string"
	});
	fields.push({
		name : 'note7',
		type : "string"
	});  	fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'pdSzie',
		type:"string"
	});
	   fields.push({
		name:'massCon',
		type:"string"
	});
	   fields.push({
		name:'molarCon',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	    fields.push({
		name:'pdResult',
		type:"string"
	});
	   fields.push({
		name:'molarResult',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'itemId',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
			name:'orderType',
			type:"string"
		});
		   fields.push({
			name:'orderId',
			type:"string"
		});
	    fields.push({
		name:'techCheckServiceTask-id',
		type:"string"
	});
	    fields.push({
		name:'techCheckServiceTask-name',
		type:"string"
	});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:biolims.wk.wkCode,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		width:30*6,
		header:biolims.common.orderId
	});
//	var storeGoodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', 'DNA' ], [ '1', '文库' ] ]
//	});
//	var orderType = new Ext.form.ComboBox({
//		store : storeGoodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'orderType',
//		header:'任务单类型',
//		width:20*6,
//		sortable:true,
//		renderer : Ext.util.Format.comboRenderer(orderType)
//	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.itemId
	});
	cm.push({
		dataIndex:'pdSzie',
		hidden : false,
		header:biolims.wk.pdSize,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massCon',
		hidden : false,
		header:biolims.wk.massCon,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'molarCon',
		hidden : false,
		header:biolims.wk.molarCon,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.wk.volume,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'pdResult',
		hidden : false,
		header:biolims.wk.pdResult,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'molarResult',
		hidden : false,
		header:biolims.wk.molarResult,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var result = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '1',
//				name : '合格'
//			}, {
//				id : '0',
//				name : '不合格'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'是否合格',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(result),
//		editor: result
//	});
//	var nextFlow = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '0',
//				name : '反馈到项目组'
//			}, {
//				id : '1',
//				name : '出报告'
//			}, {
//				id : '2',
//				name : '文库质控'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:30*6,
//		renderer: Ext.util.Format.comboRenderer(nextFlow),
//		editor: nextFlow
//	});
//	var submit = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '1',
//				name : '是'
//			}, {
//				id : '0',
//				name : '否'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'submit',
//		hidden : false,
//		header:'是否提交',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(submit),
//		editor: submit
//	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'techCheckServiceTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techCheckServiceTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSampleCheckWkListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.wk.wkDetectResult;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleTeckcheckWkResultGrid = gridEditTable("sampleTeckcheckWkResultdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
