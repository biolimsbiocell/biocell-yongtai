var sampleFiltResultGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	 fields.push({
			name:'id',
			type:"string"
		});
		   fields.push({
			name:'wkCode',
			type:"string"
		});
		   fields.push({
			name:'name',
			type:"string"
		});
		   fields.push({
			name:'sampleCode',
			type:"string"
		});
		   fields.push({
			name:'desDataNum',
			type:"string"
		});
		   fields.push({
			name:'filtDataNum',
			type:"string"
		});
		   fields.push({
			name:'normDataNum',
			type:"string"
		});
		   fields.push({
			name:'sequenceType',
			type:"string"
		});
		   fields.push({
			name:'realSequenceType',
			type:"string"
		});
		   fields.push({
			name:'isSequenceType',
			type:"string"
		});
		   fields.push({
			name:'isDataNum',
			type:"string"
		});
		   fields.push({
			name:'addDataNum',
			type:"string"
		});
		   fields.push({
			name:'abnomal',
			type:"string"
		});
		   fields.push({
			name:'idGood',
			type:"string"
		});
		   fields.push({
			name:'nextflow',
			type:"string"
		});
		   fields.push({
			name:'submit',
			type:"string"
		});
		   fields.push({
			name:'note',
			type:"string"
		});
		    fields.push({
			name:'filtrateTask-id',
			type:"string"
		});
		    fields.push({
			name:'filtrateTask-name',
			type:"string"
		});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.sample.filterId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:biolims.wk.wkCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'desDataNum',
		hidden : false,
		header:biolims.sample.desDataNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtDataNum',
		hidden : false,
		header:biolims.sample.filtDataNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'normDataNum',
		hidden : false,
		header:biolims.sample.normDataNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequenceType',
		hidden : false,
		header:biolims.sample.sequenceType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'realSequenceType',
		hidden : false,
		header:biolims.sample.realSequenceType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//			var isSequenceTypecob = new Ext.form.ComboBox({
//			store : new Ext.data.JsonStore({
//				fields : [ 'id', 'name' ],
//				data : [ {
//					id : '1',
//					name : '是'
//				}, {
//					id : '0',
//					name : '否'
//				} ]
//			}),
//			displayField : 'name',
//			valueField : 'id',
//			typeAhead : true,
//			mode : 'local',
//			forceSelection : true,
//			triggerAction : 'all',
//			selectOnFocus : true
//		});
//	cm.push({
//		dataIndex:'isSequenceType',
//		hidden : false,
//		header:'测序类型是否一致',
//		width:20*6,
//		
//		renderer: Ext.util.Format.comboRenderer(isSequenceTypecob),editor: isSequenceTypecob
//	});
//			var isDataNumcob = new Ext.form.ComboBox({
//			store : new Ext.data.JsonStore({
//				fields : [ 'id', 'name' ],
//				data : [ {
//					id : '1',
//					name : '是'
//				}, {
//					id : '0',
//					name : '否'
//				} ]
//			}),
//			displayField : 'name',
//			valueField : 'id',
//			typeAhead : true,
//			mode : 'local',
//			forceSelection : true,
//			triggerAction : 'all',
//			selectOnFocus : true
//		});
//	cm.push({
//		dataIndex:'isDataNum',
//		hidden : false,
//		header:'是否满足数据量要求',
//		width:20*6,
//		
//		renderer: Ext.util.Format.comboRenderer(isDataNumcob),editor: isDataNumcob
//	});
	cm.push({
		dataIndex:'addDataNum',
		hidden : false,
		header:biolims.sample.addDataNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'abnomal',
		hidden : false,
		header:biolims.sample.abnomal,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//			var idGoodcob = new Ext.form.ComboBox({
//			store : new Ext.data.JsonStore({
//				fields : [ 'id', 'name' ],
//				data : [ {
//					id : '1',
//					name : '是'
//				}, {
//					id : '0',
//					name : '否'
//				} ]
//			}),
//			displayField : 'name',
//			valueField : 'id',
//			typeAhead : true,
//			mode : 'local',
//			forceSelection : true,
//			triggerAction : 'all',
//			selectOnFocus : true
//		});
//	cm.push({
//		dataIndex:'idGood',
//		hidden : false,
//		header:'是否合格',
//		width:20*6,
//		
//		renderer: Ext.util.Format.comboRenderer(idGoodcob),editor: idGoodcob
//	});
//			var nextflowcob = new Ext.form.ComboBox({
//			store : new Ext.data.JsonStore({
//				fields : [ 'id', 'name' ],
//				data : [ {
//					id : '1',
//					name : '是'
//				}, {
//					id : '0',
//					name : '否'
//				} ]
//			}),
//			displayField : 'name',
//			valueField : 'id',
//			typeAhead : true,
//			mode : 'local',
//			forceSelection : true,
//			triggerAction : 'all',
//			selectOnFocus : true
//		});
//	cm.push({
//		dataIndex:'nextflow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
//		
//		renderer: Ext.util.Format.comboRenderer(nextflowcob),editor: nextflowcob
//	});
//			var submitcob = new Ext.form.ComboBox({
//			store : new Ext.data.JsonStore({
//				fields : [ 'id', 'name' ],
//				data : [ {
//					id : '1',
//					name : '是'
//				}, {
//					id : '0',
//					name : '否'
//				} ]
//			}),
//			displayField : 'name',
//			valueField : 'id',
//			typeAhead : true,
//			mode : 'local',
//			forceSelection : true,
//			triggerAction : 'all',
//			selectOnFocus : true
//		});
//	cm.push({
//		dataIndex:'submit',
//		hidden : false,
//		header:'是否提交',
//		width:20*6,
//		
//		renderer: Ext.util.Format.comboRenderer(submitcob),editor: submitcob
//	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtrateTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'filtrateTask-name',
		hidden : false,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSamplefiltListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.sample.filterResult;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleFiltResultGrid = gridEditTable("sampleFiltResultdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
