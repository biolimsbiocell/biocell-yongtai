var sampleOrderTable;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": biolims.common.orderNumberCodes,
		},{
			"data": "createDate",
			"title": biolims.tStorage.createDate,
		},{
			"data": "ccoi",
			"title": "CCOI",
		},{
			"data": "name",
			"title": biolims.common.sname,
		}, {
			"data": "gender",
			"title": biolims.common.gender,
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return biolims.common.male;
				}
				if(data == "0") {
					return biolims.common.female;
				}
				if(data == "3") {
					return biolims.common.unknown;
				} else {
					return '';
				}
			}
		},{
			"data": "drawBloodTime",
			"title": "采血日期",
		},{
			"data": "round",
			"title": "轮次",
		},{
			"data": "age",
			"title": biolims.common.age,
		},{
			"data": "nation",
			"title": biolims.common.race,
		}, {
			/*"data": "orderType",
			"title": "订单类型",
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return "用户订单";
				}
				if(data == "1") {
					return "客服订单";
				}
				else {
					return '';
				}
			}
		}, {*/
			"data": "zipCode",
			"title": biolims.crm.postcode,
		}, 
//		{
//			"data": "commissioner-id",
//			"title": biolims.common.commissionerId,
//		}, {
//			"data": "commissioner-name",
//			"title": biolims.common.sales,
//		},
		{
			"data": "nativePlace",
			"title": biolims.crm.placeOfBirth,
		}, {
			"data": "medicalNumber",
			"title": biolims.sample.medicalNumber,
		}, 
		{
			"data": "receiveState",
			"title": "接收状态",
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return "未接收";
				} else if (data =="1"){
					return "部分接收";
				}else if (data =="2"){
					return "全部接收";
				}else{
					return data;
				}
			},
		},
//		{
//			"data": "familyCode",
//			"title": biolims.common.familyCode,
//		},
		{
			"data": "family",
			"title": biolims.sample.family,
		}, {
			"data": "familyPhone",
			"title": biolims.sample.familyPhone,
		}, {
			"data": "familySite",
			"title": biolims.sample.familySite,
		}, {
			"data": "crmCustomer-id",
			"title": biolims.order.medicalInstitutionId,
		}, {
			"data": "crmCustomer-name",
			"title": biolims.sample.medicalInstitutions,
		}, {
			"data": "createUser-id",
			"title": biolims.sample.createUserId,
		}, {
			"data": "createUser-name",
			"title": biolims.sample.createUserName,
		}, {
			"data": "stateName",
			"title": biolims.common.state,
		},
//		{
//			"data": "barcode",
//			"title": biolims.common.barCode,
//		}, {
//			"data": "hospitalPatientID",
//			"title": biolims.master.hospitalPatient,
//		},
		{
			"data": "birthDate",
			"title": biolims.common.birthDay,
		}, {
			"data": "idCard",
			"title": biolims.common.idCard,
		}, {
			"data": "productName",
			"title": biolims.crmDoctorItem.productName,
			"createdCell" : function(td, data, rowData) {
				$(td).attr("productId", rowData['productId']);
		},
		}];
		
		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "SampleOrder"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});
	
	var type = $("#order_type").val();
		
	var options = table(true, "",
		"/system/sample/sampleOrder/showSampleOrderTableJson.action?type="+type,colData , null)
	sampleOrderTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleOrderTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/system/sample/sampleOrder/editSampleOrder.action?type='+$("#order_type").val();
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();

	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
//		'/system/sample/sampleOrder/editSampleOrder.action?id=' + window.btoa(id) + '&type=' + $("#order_type").val();
		'/system/sample/sampleOrder/editSampleOrder.action?id=' + id + '&type=' + $("#order_type").val();
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
//		'/system/sample/sampleOrder/viewSampleOrder.action?id=' + window.btoa(id) + '&type=' + $("#order_type").val();
		'/system/sample/sampleOrder/viewSampleOrder.action?id=' + id + '&type=' + $("#order_type").val();
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},{
			"txt": "姓名",
			"type": "input",
			"searchName": "name"
		},{

			"txt": biolims.common.gender,
			"type": "select",
			"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
			"changeOpt": "''|1|0|3",
			"searchName": "gender"
		},/*{
			"txt": "筛选号",
			"type": "input",
			"searchName": "filtrateCode"
		},*/{
			"txt": "年龄",
			"type": "input",
			"searchName": "age",
		},/*{
			"txt": "肿瘤类型",
			"type": "input",
			"searchName": "zhongliu"
		},*/{
			"txt": "医疗机构",
			"type": "input",
			"searchName": "crmCustomer-name"
		},/*{
			"txt": "主治医生",
			"type": "input",
			"searchName": "attendingDoctor"
		},{
			"txt": "代理商",
			"type": "input",
			"searchName": "agreementTask-name"
		},*/{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser-name"
		},
		{
			"txt": biolims.wk.confirmUserName,
			"type": "top.layer",
			"searchName": "confirmUser",
			"action": "confirmUser()"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		/*{
			"txt": biolims.common.confirmDateStart,
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt":biolims.common.confirmDateEnd,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		},*/
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": sampleOrderTable
		}
	];
}