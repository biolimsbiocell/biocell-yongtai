var sampleTeckServiceResultGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	 fields.push({
			name:'id',
			type:"string"
		});
		   fields.push({
			name:'sampleCode',
			type:"string"
		});
		   fields.push({
			name:'dnaCode',
			type:"string"
		});
		   fields.push({
			name:'od260',
			type:"string"
		});
		   fields.push({
			name:'od230',
			type:"string"
		});
		   fields.push({
			name:'rin',
			type:"string"
		});
		   fields.push({
			name:'s28',
			type:"string"
		});
		   fields.push({
			name:'volume',
			type:"string"
		});
		    fields.push({
			name:'concentration',
			type:"string"
		});
		   fields.push({
			name:'sumNum',
			type:"string"
		});
		   fields.push({
			name:'result',
			type:"string"
		});
		   fields.push({
			name:'nextFlow',
			type:"string"
		});
		   fields.push({
			name:'note',
			type:"string"
		});
		   fields.push({
			name:'state',
			type:"string"
		});
		   fields.push({
			name:'orderType',
			type:"string"
		});
		   fields.push({
			name:'orderId',
			type:"string"
		});
		   fields.push({
			name:'itemId',
			type:"string"
		});
		   fields.push({
			name:'submit',
			type:"string"
		});
		    fields.push({
			name:'techCheckServiceTask-id',
			type:"string"
		});
		    fields.push({
			name:'techCheckServiceTask-name',
			type:"string"
		});
		    fields.push({
			name:'classify',
			type:"string"
		});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dnaCode',
		hidden : false,
		header:biolims.dna.dnaCode,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		width:30*6,
		header:biolims.common.orderId
	});
//	var storeGoodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', 'DNA' ], [ '1', '文库' ] ]
//	});
//	var orderType = new Ext.form.ComboBox({
//		store : storeGoodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'orderType',
//		header:'任务单类型',
//		width:20*6,
//		sortable:true,
//		renderer : Ext.util.Format.comboRenderer(orderType)
//	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.itemId
	});
	cm.push({
		dataIndex:'od230',
		hidden : false,
		header:'od260/230',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od260',
		hidden : false,
		header:'od260/280',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rin',
		hidden : false,
		header:'RIN',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'s28',
		hidden : false,
		header:'28S/18S',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumNum',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var result = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '1',
//				name : '合格'
//			}, {
//				id : '0',
//				name : '不合格'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'结果判定',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(result),
//		editor: result
//	});
//	var nextFlow = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '0',
//				name : '反馈到项目组'
//			}, {
//				id : '1',
//				name : '出报告'
//			}, {
//				id : '2',
//				name : '继续'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:30*6,
//		renderer: Ext.util.Format.comboRenderer(nextFlow),
//		editor: nextFlow
//	});
//	var submit = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '1',
//				name : '是'
//			}, {
//				id : '0',
//				name : '否'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'submit',
//		hidden : false,
//		header:'是否提交',
//		width:20*6,
//		renderer: Ext.util.Format.comboRenderer(submit),
//		editor: submit
//	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'techCheckServiceTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techCheckServiceTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showSampleCheckServiceListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.dna.dnaDetectionResults;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleTeckServiceltGrid = gridEditTable("sampleTechServiceResultdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
