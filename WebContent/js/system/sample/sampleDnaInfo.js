﻿var sampleDnaGetResultGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
			name:'patientName',
			type:"string"
		});
	   fields.push({
			name:'sequenceFun',
			type:"string"
		});   
		   fields.push({
			name:'productName',
			type:"string"
		});
	   fields.push({
			name:'productId',
			type:"string"
		});
		   fields.push({
			name:'inspectDate',
			type:"string"
		});
		fields.push({
			name : 'orderId',
			type : "string"
		});
		fields.push({
			name : 'idCard',
			type : "string"
		});
		fields.push({
			name : 'phone',
			type : "string"
		});
		fields.push({
			name : 'reportDate',
			type : "string"
		});
			
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	    fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'orderType',
		type:"string"
	});
	    fields.push({
			name:'taskId',
			type:"string"
		});
   fields.push({
		name:'submit',
		type:"string"
	});
	    fields.push({
		name:'dnaTask-id',
		type:"string"
	});
	    fields.push({
		name:'dnaTask-name',
		type:"string"
	});

	   fields.push({
			name:'sumVolume',
			type:"string"
		});

	   fields.push({
			name:'result',
			type:"string"
		});
	   fields.push({
			name:'isToProject',
			type:"string"
		});
	   fields.push({
			name:'nextFlow',
			type:"string"
		});
	   fields.push({
			name:'advice',
			type:"string"
		});
	   fields.push({
			name:'od260',
			type:"string"
		});
	   fields.push({
			name:'od280',
			type:"string"
		});
	   fields.push({
			name:'rin',
			type:"string"
		});
	   fields.push({
			name:'contraction',
			type:"string"
		});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	   
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'dicSampleType-id',
			type:"string"
		});
	   fields.push({
			name:'dicSampleType-name',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'qbcontraction',
			type:"string"
		});
	

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6
	});
	
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:biolims.common.idCard,
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden:true,
		header:biolims.common.dicSampleTypeId,
		width:15*10,
		sortable:true
	});
//	var testDicSampleType2 =new Ext.form.TextField({
//        allowBlank: false
//	});
//	testDicSampleType2.on('focus', function() {
//		loadTestDicSampleType2();
//	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		sortable:true
		//editor : testDicSampleType2
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden:true,
		width:20*6,
		sortable:true
	});

	cm.push({
		dataIndex:'od260',
		hidden : false,
		header:'OD260/230',
		width:20*6
	});
	cm.push({
		dataIndex:'od280',
		hidden : false,
		header:'OD260/280',
		width:20*6
	});
	cm.push({
		dataIndex:'contraction',
		hidden : false,
		header:biolims.common.nanodropConcentration,
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.count,
		width:20*6
	});
	cm.push({
		dataIndex:'qbcontraction',
		hidden : false,
		header:biolims.common.qbConcentration,
		width:20*6
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6
	});
	cm.push({
		dataIndex:'rin',
		hidden : false,
		header:'RIN',
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:40*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'dnaTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dnaTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:biolims.common.classify,
		hidden:true,
		width:20*6
	});
	
	cols.cm = cm;
	var loadParam = {};
	
	loadParam.url = ctx + "/system/sample/sampleMain/showDnaSampleInfoListJson.action?id=" + $("#sampleInfo_code").val();
	
	var opts = {};
	opts.title = biolims.dna.dnaGetResult;
	opts.height = 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.isShowDefaultTbar = false;
	sampleDnaGetResultGrid = gridEditTable("sampleDnaGetResultdiv", cols, loadParam, opts);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
