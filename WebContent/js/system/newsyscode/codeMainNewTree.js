$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : "编号",
		field : "id"
	});
	
	   
	fields.push({
		title : "描述",
		field : "name"
	});
	
	   
	fields.push({
		title : "打印指令",
		field : "code"
	});
	
	   
	fields.push({
		title : "名字x轴",
		field : "namex"
	});
	
	   
	fields.push({
		title : "名字y轴",
		field : "namey"
	});
	
	   
	fields.push({
		title : "编码x轴",
		field : "codex"
	});
	
	   
	fields.push({
		title : "编码y轴",
		field : "codey"
	});
	
	   
	fields.push({
		title : "二维码x轴",
		field : "qrx"
	});
	
	   
	fields.push({
		title : "二维码y轴",
		field : "qry"
	});
	
	   
	fields.push({
		title : "ip地址",
		field : "ip"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/system/newsyscode/codeMainNew/showCodeMainNewListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/system/newsyscode/codeMainNew/editCodeMainNew.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/newsyscode/codeMainNew/editCodeMainNew.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/newsyscode/codeMainNew/viewCodeMainNew.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : "编号",
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "描述",
		"searchName" : 'name',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "打印指令",
		"searchName" : 'code',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "名字x轴",
		"searchName" : 'namex',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "名字y轴",
		"searchName" : 'namey',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "编码x轴",
		"searchName" : 'codex',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "编码y轴",
		"searchName" : 'codey',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "二维码x轴",
		"searchName" : 'qrx',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "二维码y轴",
		"searchName" : 'qry',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "ip地址",
		"searchName" : 'ip',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

