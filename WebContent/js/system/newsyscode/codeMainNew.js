var codeMainNewTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":"编码",
	});
	    fields.push({
		"data":"name",
		"title":"描述",
	});
	
	    fields.push({
		"data":"code",
		"title":"打印指令",
	});
	
	    fields.push({
		"data":"namex",
		"title":"名字x轴",
	});
	
	    fields.push({
		"data":"namey",
		"title":"名字y轴",
	});
	
	    fields.push({
		"data":"codex",
		"title":"编码x轴",
	});
	
	    fields.push({
		"data":"codey",
		"title":"编码y轴",
	});
	
	    fields.push({
		"data":"qrx",
		"title":"二维码x轴",
	});
	
	    fields.push({
		"data":"qry",
		"title":"二维码y轴",
	});
	
	    fields.push({
		"data":"ip",
		"title":"ip地址",
	});
	    fields.push({
			"data":"state",
			"title":"状态",
		});

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "CodeMainNew"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});

	var options = table(true, "","/system/newsyscode/codeMainNew/showCodeMainNewTableJson.action",
	 fields, null)
	codeMainNewTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(codeMainNewTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/system/newsyscode/codeMainNew/editCodeMainNew.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/newsyscode/codeMainNew/editCodeMainNew.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/newsyscode/codeMainNew/viewCodeMainNew.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":"编码"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":"描述"
		});
	   fields.push({
		    "searchName":"code",
			"type":"input",
			"txt":"打印指令"
		});
	   fields.push({
		    "searchName":"namex",
			"type":"input",
			"txt":"名字x轴"
		});
	   fields.push({
		    "searchName":"namey",
			"type":"input",
			"txt":"名字y轴"
		});
	   fields.push({
		    "searchName":"codex",
			"type":"input",
			"txt":"编码x轴"
		});
	   fields.push({
		    "searchName":"codey",
			"type":"input",
			"txt":"编码y轴"
		});
	   fields.push({
		    "searchName":"qrx",
			"type":"input",
			"txt":"二维码x轴"
		});
	   fields.push({
		    "searchName":"qry",
			"type":"input",
			"txt":"二维码x轴"
		});
	   fields.push({
		    "searchName":"ip",
			"type":"input",
			"txt":"ip地址"
		});
	
	fields.push({
		"type":"table",
		"table":codeMainNewTable
	});
	return fields;
}
