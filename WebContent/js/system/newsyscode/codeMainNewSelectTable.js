var codeMainNewTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.codeMainNew.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.codeMainNew.name,
	});
	
	    fields.push({
		"data":"code",
		"title":biolims.codeMainNew.code,
	});
	
	    fields.push({
		"data":"namex",
		"title":biolims.codeMainNew.namex,
	});
	
	    fields.push({
		"data":"namey",
		"title":biolims.codeMainNew.namey,
	});
	
	    fields.push({
		"data":"codex",
		"title":biolims.codeMainNew.codex,
	});
	
	    fields.push({
		"data":"codey",
		"title":biolims.codeMainNew.codey,
	});
	
	    fields.push({
		"data":"qrx",
		"title":biolims.codeMainNew.qrx,
	});
	
	    fields.push({
		"data":"qry",
		"title":biolims.codeMainNew.qry,
	});
	
	    fields.push({
		"data":"ip",
		"title":biolims.codeMainNew.ip,
	});
	
	var options = table(true, "","/system/newsyscode/codeMainNew/showCodeMainNewTableJson.action",
	 fields, null)
	codeMainNewTable = renderData($("#addCodeMainNewTable"), options);
	$('#addCodeMainNewTable').on('init.dt', function() {
		recoverSearchContent(codeMainNewTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.codeMainNew.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.codeMainNew.name
		});
	   fields.push({
		    "searchName":"code",
			"type":"input",
			"txt":biolims.codeMainNew.code
		});
	   fields.push({
		    "searchName":"namex",
			"type":"input",
			"txt":biolims.codeMainNew.namex
		});
	   fields.push({
		    "searchName":"namey",
			"type":"input",
			"txt":biolims.codeMainNew.namey
		});
	   fields.push({
		    "searchName":"codex",
			"type":"input",
			"txt":biolims.codeMainNew.codex
		});
	   fields.push({
		    "searchName":"codey",
			"type":"input",
			"txt":biolims.codeMainNew.codey
		});
	   fields.push({
		    "searchName":"qrx",
			"type":"input",
			"txt":biolims.codeMainNew.qrx
		});
	   fields.push({
		    "searchName":"qry",
			"type":"input",
			"txt":biolims.codeMainNew.qry
		});
	   fields.push({
		    "searchName":"ip",
			"type":"input",
			"txt":biolims.codeMainNew.ip
		});
	
	fields.push({
		"type":"table",
		"table":codeMainNewTable
	});
	return fields;
}
