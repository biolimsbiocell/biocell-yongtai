var codeMainNewTable;
var oldcodeMainNewChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.codeMainNew.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.codeMainNew.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
//	   colOpts.push({
//		"data":"code",
//		"title": biolims.codeMainNew.code,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "code");
//	    },
//		"className": "edit"
//	});
//	   colOpts.push({
//		"data":"namex",
//		"title": biolims.codeMainNew.namex,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "namex");
//	    },
//		"className": "edit"
//	});
//	   colOpts.push({
//		"data":"namey",
//		"title": biolims.codeMainNew.namey,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "namey");
//	    },
//		"className": "edit"
//	});
//	   colOpts.push({
//		"data":"codex",
//		"title": biolims.codeMainNew.codex,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "codex");
//	    },
//		"className": "edit"
//	});
//	   colOpts.push({
//		"data":"codey",
//		"title": biolims.codeMainNew.codey,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "codey");
//	    },
//		"className": "edit"
//	});
//	   colOpts.push({
//		"data":"qrx",
//		"title": biolims.codeMainNew.qrx,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "qrx");
//	    },
//		"className": "edit"
//	});
//	   colOpts.push({
//		"data":"qry",
//		"title": biolims.codeMainNew.qry,
//		"createdCell": function(td) {
//			$(td).attr("saveName", "qry");
//	    },
//		"className": "edit"
//	});
	   colOpts.push({
		"data":"ip",
		"title": biolims.codeMainNew.ip,
		"createdCell": function(td) {
			$(td).attr("saveName", "ip");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#codeMainNewTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#codeMainNewTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#codeMainNewTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#codeMainNew_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/system/newsyscode/codeMainNew/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 codeMainNewTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveCodeMainNew($("#codeMainNewTable"));
		}
	});
	}
	
	var codeMainNewOptions = 
	table(true, "","/system/newsyscode/codeMainNew/showCodeMainNewTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	codeMainNewTable = renderData($("#codeMainNewTable"), codeMainNewOptions);
	codeMainNewTable.on('draw', function() {
		oldcodeMainNewChangeLog = codeMainNewTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveCodeMainNew(ele) {
	var data = saveCodeMainNewjson(ele);
	var ele=$("#codeMainNewTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/system/newsyscode/codeMainNew/saveCodeMainNewTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveCodeMainNewjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldcodeMainNewChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
