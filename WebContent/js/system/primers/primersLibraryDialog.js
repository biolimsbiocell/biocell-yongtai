var primersLibraryDialogGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'receiveDate',
		type:"string"
	});
	    fields.push({
		name:'batch',
		type:"string"
	});
	    fields.push({
		name:'edNmole',
		type:"string"
	});
	    fields.push({
		name:'expiryDate',
		type:"string"
	});
	    fields.push({
		name:'referTotal',
		type:"string"
	});
	    fields.push({
		name:'hasDosage',
		type:"string"
	});
	    fields.push({
		name:'surplus',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.sanger.primerNumber,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiveDate',
		header:biolims.sanger.receiveDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'batch',
		header:biolims.common.batch,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'edNmole',
		header:biolims.sanger.edNmole,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'expiryDate',
		header:biolims.sanger.expiryDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'referTotal',
		header:biolims.sanger.referTotal,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'hasDosage',
		header:biolims.sanger.hasDosage,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'surplus',
		header:biolims.sanger.surplus,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.sanger.state,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.sanger.stateName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/primers/primersLibrary/showPrimersLibraryListJson.action";
	var opts={};
	opts.title=biolims.sanger.primerLib;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setPrimersLibraryFun(rec);
	};
	primersLibraryDialogGrid=gridTable("show_dialog_primersLibrary_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(primersLibraryDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
