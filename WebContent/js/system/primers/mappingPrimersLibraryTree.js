$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : biolims.common.id,
		field : "id"
	});
	
	   
	fields.push({
		title : biolims.common.name,
		field : "name"
	});
	
	  fields.push({
		title : biolims.sample.createUserName,
		field : "createUser.name"
	});
	   
	fields.push({
		title : biolims.tStorage.createDate,
		field : "createDate"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentFault.stateName,
		field : "state"
	});
	
	   
	fields.push({
		title : biolims.tInstrumentRepairPlan.stateName,
		field : "stateName"
	});
	
	   
	fields.push({
		title : biolims.mappingPrimersLibrary.prodectId,
		field : "prodectId"
	});
	
	   
	fields.push({
		title : biolims.mappingPrimersLibrary.prodectName,
		field : "prodectName"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/system/primers/mappingPrimersLibrary/showMappingPrimersLibraryListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/system/primers/mappingPrimersLibrary/editMappingPrimersLibrary.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/primers/mappingPrimersLibrary/editMappingPrimersLibrary.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/primers/mappingPrimersLibrary/viewMappingPrimersLibrary.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : biolims.common.id,
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.common.name,
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : biolims.sample.createUserName,
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : biolims.tStorage.createDate,
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentFault.stateName,
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.tInstrumentRepairPlan.stateName,
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.mappingPrimersLibrary.prodectId,
		"searchName" : 'prodectId',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.mappingPrimersLibrary.prodectName,
		"searchName" : 'prodectName',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

