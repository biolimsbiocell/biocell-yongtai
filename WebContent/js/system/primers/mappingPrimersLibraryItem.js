var mappingPrimersLibraryItemTable;
var oldmappingPrimersLibraryItemChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#mappingPrimersLibrary_id").text();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push( {
		"data": "mappingPrimersLibrary-id",
		"title": biolims.mappingPrimersLibraryItem.mappingPrimersLibrary,
		"createdCell": function(td) {
			$(td).attr("saveName", "mappingPrimersLibrary-id");
		},
	"visible":	false,
	});
	   colOpts.push({
		"data":"plName",
		"title": biolims.mappingPrimersLibraryItem.plName,
		"createdCell": function(td) {
			$(td).attr("saveName", "plName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"size",
		"title": biolims.mappingPrimersLibraryItem.size,
		"createdCell": function(td) {
			$(td).attr("saveName", "size");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"conclusion1",
		"title": biolims.mappingPrimersLibraryItem.conclusion1,
		"createdCell": function(td) {
			$(td).attr("saveName", "conclusion1");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"reference",
		"title": biolims.mappingPrimersLibraryItem.reference,
		"createdCell": function(td) {
			$(td).attr("saveName", "reference");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.mappingPrimersLibraryItem.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"chromosomalLocation",
		"title": biolims.mappingPrimersLibraryItem.chromosomalLocation,
		"createdCell": function(td) {
			$(td).attr("saveName", "chromosomalLocation");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"leftPrimer",
		"title": biolims.mappingPrimersLibraryItem.leftPrimer,
		"createdCell": function(td) {
			$(td).attr("saveName", "leftPrimer");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"rightPrimer",
		"title": biolims.mappingPrimersLibraryItem.rightPrimer,
		"createdCell": function(td) {
			$(td).attr("saveName", "rightPrimer");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"ampliconid",
		"title": biolims.mappingPrimersLibraryItem.ampliconid,
		"createdCell": function(td) {
			$(td).attr("saveName", "ampliconid");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.mappingPrimersLibraryItem.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"plId",
		"title": biolims.mappingPrimersLibraryItem.plId,
		"createdCell": function(td) {
			$(td).attr("saveName", "plId");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"id",
		"title": biolims.mappingPrimersLibraryItem.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#mappingPrimersLibraryItemTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#mappingPrimersLibraryItemTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#mappingPrimersLibraryItemTable"),
				"/system/primers/mappingPrimersLibrary/delMappingPrimersLibraryItem.action");
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#mappingPrimersLibraryItemTable"))
		}
	});
/*	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#mappingPrimersLibraryItem_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/system/primers/mappingPrimersLibrary/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 mappingPrimersLibraryItemTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});*/
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveMappingPrimersLibraryItem($("#mappingPrimersLibraryItemTable"));
		}
	});
	}
	
	var mappingPrimersLibraryItemOptions = table(true,
		id,
		'/system/primers/mappingPrimersLibrary/showMappingPrimersLibraryItemTableJson.action', colOpts, tbarOpts)
	mappingPrimersLibraryItemTable = renderData($("#mappingPrimersLibraryItemTable"), mappingPrimersLibraryItemOptions);
	mappingPrimersLibraryItemTable.on('draw', function() {
		oldmappingPrimersLibraryItemChangeLog = mappingPrimersLibraryItemTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveMappingPrimersLibraryItem(ele) {
	var data = saveMappingPrimersLibraryItemjson(ele);
	var ele=$("#mappingPrimersLibraryItemTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/system/primers/mappingPrimersLibrary/saveMappingPrimersLibraryItemTable.action',
		data: {
			id: $("#mappingPrimersLibrary_id").text(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveMappingPrimersLibraryItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "mappingPrimersLibrary-name") {
				json["mappingPrimersLibrary-id"] = $(tds[j]).attr("mappingPrimersLibrary-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldmappingPrimersLibraryItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
