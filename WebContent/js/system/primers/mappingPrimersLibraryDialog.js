var mappingPrimersLibraryDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'prodectId',
		type:"string"
	});
	    fields.push({
		name:'prodectName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'prodectId',
		header:biolims.common.productId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'prodectName',
		header:biolims.common.productName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/primers/mappingPrimersLibrary/showMappingPrimersLibraryListJson.action";
	var opts={};
	opts.title=biolims.master.productSiteLibrary;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setPrimersLibraryFun(rec);
	};
	mappingPrimersLibraryDialogGrid=gridTable("show_dialog_mappingPrimersLibrary_div",cols,loadParam,opts);
});
function setValue(){
	
	
	var record = mappingPrimersLibraryDialogGrid.getSelectionModel().getSelections();
	
	window.parent.setPrimersLibraryValue(record);
}
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(mappingPrimersLibraryDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
