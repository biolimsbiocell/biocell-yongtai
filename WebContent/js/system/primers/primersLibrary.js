var primersLibraryGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'receiveDate',
		type:"string"
	});
	    fields.push({
			name:'site',
			type:"string"
	});
	    fields.push({
		name:'batch',
		type:"string"
	});
	    fields.push({
		name:'edNmole',
		type:"string"
	});
	    fields.push({
		name:'expiryDate',
		type:"string"
	});
	    fields.push({
		name:'referTotal',
		type:"string"
	});
	    fields.push({
		name:'hasDosage',
		type:"string"
	});
	    fields.push({
		name:'surplus',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.sanger.primerNumber,
		width:40*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'receiveDate',
		header:biolims.sanger.receiveDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'site',
		header:biolims.sanger.chromosomalLocation,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'batch',
		header:biolims.common.batch,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'edNmole',
		header:biolims.sanger.edNmole,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'expiryDate',
		header:biolims.sanger.expiryDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'referTotal',
		header:biolims.sanger.referTotal,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'hasDosage',
		header:biolims.sanger.hasDosage,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'surplus',
		header:biolims.sanger.surplus,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.sanger.state,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.sanger.stateName,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/primers/primersLibrary/showPrimersLibraryListJson.action";
	var opts={};
	opts.title=biolims.sanger.primerLib;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	primersLibraryGrid=gridTable("show_primersLibrary_div",cols,loadParam,opts);
	$("#show_primersLibrary_div").data("primersLibraryGrid", primersLibraryGrid);
});
function add(){
		window.location=window.ctx+'/system/primers/primersLibrary/editPrimersLibrary.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/primers/primersLibrary/editPrimersLibrary.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/primers/primersLibrary/viewPrimersLibrary.action?id=' + id;
}
function exportexcel() {
	primersLibraryGrid.title = biolims.common.exportList;
	var vExportContent = primersLibraryGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startreceiveDate").val() != undefined) && ($("#startreceiveDate").val() != '')) {
					var startreceiveDatestr = ">=##@@##" + $("#startreceiveDate").val();
					$("#receiveDate1").val(startreceiveDatestr);
				}
				if (($("#endreceiveDate").val() != undefined) && ($("#endreceiveDate").val() != '')) {
					var endreceiveDatestr = "<=##@@##" + $("#endreceiveDate").val();

					$("#receiveDate2").val(endreceiveDatestr);

				}
				
				if (($("#startexpiryDate").val() != undefined) && ($("#startexpiryDate").val() != '')) {
					var startexpiryDatestr = ">=##@@##" + $("#startexpiryDate").val();
					$("#expiryDate1").val(startexpiryDatestr);
				}
				if (($("#endexpiryDate").val() != undefined) && ($("#endexpiryDate").val() != '')) {
					var endexpiryDatestr = "<=##@@##" + $("#endexpiryDate").val();

					$("#expiryDate2").val(endexpiryDatestr);

				}
				
				
				commonSearchAction(primersLibraryGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
