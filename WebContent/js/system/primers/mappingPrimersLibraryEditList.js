var mappingPrimersLibraryTable;
var oldmappingPrimersLibraryChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.mappingPrimersLibrary.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.mappingPrimersLibrary.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": biolims.sample.createUserId,
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.mappingPrimersLibrary.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.mappingPrimersLibrary.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.mappingPrimersLibrary.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.mappingPrimersLibrary.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"prodectId",
		"title": biolims.mappingPrimersLibrary.prodectId,
		"createdCell": function(td) {
			$(td).attr("saveName", "prodectId");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"prodectName",
		"title": biolims.mappingPrimersLibrary.prodectName,
		"createdCell": function(td) {
			$(td).attr("saveName", "prodectName");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#mappingPrimersLibraryTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#mappingPrimersLibraryTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#mappingPrimersLibraryTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#mappingPrimersLibrary_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/system/primers/mappingPrimersLibrary/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 mappingPrimersLibraryTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveMappingPrimersLibrary($("#mappingPrimersLibraryTable"));
		}
	});
	}
	
	var mappingPrimersLibraryOptions = 
	table(true, "","/system/primers/mappingPrimersLibrary/showMappingPrimersLibraryTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	mappingPrimersLibraryTable = renderData($("#mappingPrimersLibraryTable"), mappingPrimersLibraryOptions);
	mappingPrimersLibraryTable.on('draw', function() {
		oldmappingPrimersLibraryChangeLog = mappingPrimersLibraryTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveMappingPrimersLibrary(ele) {
	var data = saveMappingPrimersLibraryjson(ele);
	var ele=$("#mappingPrimersLibraryTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/system/primers/mappingPrimersLibrary/saveMappingPrimersLibraryTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveMappingPrimersLibraryjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldmappingPrimersLibraryChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
