var mappingPrimersLibraryTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.mappingPrimersLibrary.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.mappingPrimersLibrary.name,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":biolims.sample.createUserId
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.mappingPrimersLibrary.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.mappingPrimersLibrary.createDate,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.mappingPrimersLibrary.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.mappingPrimersLibrary.stateName,
	});
	
	    fields.push({
		"data":"prodectId",
		"title":biolims.mappingPrimersLibrary.prodectId,
	});
	
	    fields.push({
		"data":"prodectName",
		"title":biolims.mappingPrimersLibrary.prodectName,
	});
	
	var options = table(true, "","/system/primers/mappingPrimersLibrary/showMappingPrimersLibraryTableJson.action",
	 fields, null)
	mappingPrimersLibraryTable = renderData($("#addMappingPrimersLibraryTable"), options);
	$('#addMappingPrimersLibraryTable').on('init.dt', function() {
		recoverSearchContent(mappingPrimersLibraryTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.mappingPrimersLibrary.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.mappingPrimersLibrary.name
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":biolims.sample.createUserId
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.mappingPrimersLibrary.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.mappingPrimersLibrary.createDate
		});
	fields.push({
			"txt": biolims.mappingPrimersLibrary.createDate+"(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.mappingPrimersLibrary.createDate+"(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.mappingPrimersLibrary.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.mappingPrimersLibrary.stateName
		});
	   fields.push({
		    "searchName":"prodectId",
			"type":"input",
			"txt":biolims.mappingPrimersLibrary.prodectId
		});
	   fields.push({
		    "searchName":"prodectName",
			"type":"input",
			"txt":biolims.mappingPrimersLibrary.prodectName
		});
	
	fields.push({
		"type":"table",
		"table":mappingPrimersLibraryTable
	});
	return fields;
}
