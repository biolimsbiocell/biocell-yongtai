var environmentalMonitoringTable;
$(function() {
	var fields=[{
		"data":"id",
		"title": biolims.user.itemNo,
	}, {
		"data": "laboratoryNo",
		"title": "实验室编号",
	}, {
		"data": "detectionDate",
		"title": "检测时间",
	}, {
		"data": "monitoringType",
		"title": "检测类型",
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "尘埃粒子监测";
			}
			if(data == "1") {
				return "沉降菌监测";
			}
			if(data == "2") {
				return "纯化水监测";
			} else {
				return '';
			}
		}
	}, {
		"data": "indoorTemperature",
		"title": "室内温度",
	}, {
		"data": "indoorHumidity",
		"title": "室内湿度",
	}, {
		"data": "cleanzoneDifferrential",
		"title":"净化区之间压差",
	}, {
		"data": "cleanzoneOutdifferrential",
		"title": "净化区与室外压差",
	}, {
		"data": "cleanzoneTemperature",
		"title": "净化区温度",
	}, {
		"data": "cleanzoneHumidity",
		"title": "净化区湿度",
	}, {
		"data": "cleanzoneCleanliness",
		"title": "净化区洁净度",
	}, {
		"data": "cleanzonePositivePressure",
		"title": "净化区正压",
	}, {
		"data": "cleanzoneNoise",
		"title": "净化区噪声",
	}, {
		"data": "cleanzoneAiroutput",
		"title": "净化区送风量",
	}, {
		"data": "dustParticleNumber",
		"title": "尘埃粒子数",
	}, {
		"data": "samplingNumber",
		"title": "采样点数",
	}, {
		"data": "cultureDishNumber",
		"title": "培养皿数",
	}, {
		"data": "clumpCount",
		"title": "菌落数",
	}, {
		"data": "characters",
		"title": "性状",
	}, {
		"data": "phValue",
		"title": "酸碱度",
	}, {
		"data": "nitrate",
		"title": "硝酸盐",
	}, {
		"data": "nitrite",
		"title": "亚硝酸盐",
	}, {
		"data": "ammonia",
		"title": "氨",
	}, {
		"data": "conductivity",
		"title": "电导率",
	}, {
		"data": "toc",
		"title": "TOC",
	}, {
		"data": "simpleOxide",
		"title": "易氧化物",
	}, {
		"data": "nonvolatileMatter",
		"title": "不挥发物",
	}, {
		"data": "heavyMetal",
		"title": "重金属",
	}, {
		"data": "microbialLimit",
		"title": "微生物限度",
	}];
	
		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "EnvironmentalMonitoring"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});

	var options = table(true, "","/system/environmental/environmentalMonitoring/showEnvironmentalMonitoringTableJson.action",
	 fields, null)
	environmentalMonitoringTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(environmentalMonitoringTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/system/environmental/environmentalMonitoring/editEnvironmentalMonitoring.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/environmental/environmentalMonitoring/editEnvironmentalMonitoring.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/environmental/environmentalMonitoring/viewEnvironmentalMonitoring.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	
	fields.push({
		"txt":"实验室编号",
		"type":"input",
		"searchName":"laboratoryNo"
	});
	fields.push({
		"txt":"监测时间",
		"type":"dataTime",
		"searchName":"detectionDate##@@##",
		"mark": "s##@@##"
	});
	fields.push({
		"txt":"监测类型",
		"type":"select",
		"options":biolims.common.pleaseChoose+"|"+"尘埃粒子监测"+"|"+"沉降菌监测"+"|"+"纯化水监测",
		"changeOpt": "''|0|1|2",
		"searchName":"monitoringType"
	});
	fields.push({
		"type":"table",
		"table":environmentalMonitoringTable
	});
	return fields;
}
