var transBoxDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'boxSize',
		type:"string"
	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:30*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'boxSize',
		hidden:false,
		header:biolims.storage.spec,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'num',
		header : biolims.common.count,
		
		width:20*6,
		sortable:true
		});
	var state = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.master.valid
			}, {
				id : '0',
				name : biolims.master.invalid
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(state),
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/box/box/showDialogTransBoxListJson.action";
	var opts={};
	opts.title=biolims.storage.carryingCase;
	opts.height=document.body.clientHeight-200;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		//edit();
	};
	transBoxDialogGrid=gridTable("show_transBox_div",cols,loadParam,opts);
});