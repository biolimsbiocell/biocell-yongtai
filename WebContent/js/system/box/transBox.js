var transBoxGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'boxSize',
		type:"string"
	});
	    fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:30*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'boxSize',
		hidden:false,
		header:biolims.storage.spec,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'num',
		header : biolims.common.count,
		
		width:20*6,
		sortable:true
		});
	var state = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.master.valid
			}, {
				id : '0',
				name : biolims.master.invalid
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(state),
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/box/box/showTransBoxListJson.action";
	var opts={};
	opts.title=biolims.storage.carryingCase;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	transBoxGrid=gridTable("show_transBox_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/system/box/box/editTransBox.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/box/box/editTransBox.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/box/box/viewTransBox.action?id=' + id;
}
function exportexcel() {
	transBoxGrid.title = biolims.common.exportList;
	var vExportContent = transBoxGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {

				commonSearchAction(transBoxGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
