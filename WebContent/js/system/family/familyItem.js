var familyItemGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'patientNo',
		type : "string"
	});

	fields.push({
		name : 'crmPatient-id',
		type : "string"
	});
	fields.push({
		name : 'crmPatient-name',
		type : "string"
	});
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'birthDate',
		type : "date",
		dateFormat : "Y-m-d"
	});
	fields.push({
		name : 'age',
		type : "string"
	});
	fields.push({
		name : 'fathrtId',
		type : "string"
	});
	fields.push({
		name : 'matherId',
		type : "string"
	});
	fields.push({
		name : 'gender',
		type : "string"
	});
	fields.push({
		name : 'wife',
		type : "string"
	});
	fields.push({
		name : 'husband',
		type : "string"
	});
	fields.push({
		name : 'isDisease',
		type : "string"
	});
	fields.push({
		name : 'isDivorce',
		type : "string"
	});
	fields.push({
		name : 'isCustody',
		type : "string"
	});
	fields.push({
		name : 'haveChild',
		type : "string"
	});
	fields.push({
		name : 'isTwins',
		type : "string"
	});
	fields.push({
		name : 'groups',
		type : "string"
	});
	fields.push({
		name : 'isDead',
		type : "string"
	});
	fields.push({
		name : 'leftUp',
		type : "string"
	});
	fields.push({
		name : 'leftDown',
		type : "string"
	});
	fields.push({
		name : 'rightUp',
		type : "string"
	});
	fields.push({
		name : 'rightDown',
		type : "string"
	});
	fields.push({
		name : 'status',
		type : "string"
	});
	fields.push({
		name : 'patientId',
		type : "string"
	});
	fields.push({
		name : 'family-id',
		type : "string"
	});
	fields.push({
		name : 'family-name',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : '编号',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'patientNo',
		hidden : false,
		header : '编号',
		width : 6 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'patientName',
		hidden : false,
		header : '姓名',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmPatient-id',
		hidden : true,
		header : '编号',
		width : 10 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'crmPatient-name',
		hidden : true,
		header : '姓名',
		width : 10 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'age',
		hidden : false,
		header : '年龄',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'birthDate',
		hidden : false,
		header : '出生日期',
		width : 15 * 6,
		renderer : formatDate,
		sortable : true,
		editor : new Ext.form.DateField({
			format : 'Y-m-d'
		})
	});

	cm.push({
		dataIndex : 'fathrtId',
		hidden : false,
		header : '父亲',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'matherId',
		hidden : false,
		header : '母亲',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeisgenderCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '男' ], [ '0', '女' ], [ '-1', '未知' ] ]
	});
	var genderCob = new Ext.form.ComboBox({
		store : storeisgenderCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'gender',
		hidden : false,
		header : '性别 ',
		width : 10 * 6,
		editor : genderCob,
		renderer : Ext.util.Format.comboRenderer(genderCob)
	});
	cm.push({
		dataIndex : 'wife',
		hidden : false,
		header : '妻子',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'husband',
		hidden : false,
		header : '丈夫',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeisisDiseaseCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '是', '是' ], [ '否', '否' ] ]
	});
	var isDiseaseCob = new Ext.form.ComboBox({
		store : storeisisDiseaseCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	/*cm.push({
		dataIndex : 'groups',
		hidden : false,
		header : '生活圈',
		width : 10 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex : 'isDisease',
		hidden : false,
		header : '是否先症',
		width : 10 * 6,
		editor : isDiseaseCob,
		renderer : Ext.util.Format.comboRenderer(isDiseaseCob)
	});
	var storeisisDeadCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'S', '已去世' ], [ '', '未去世' ] ]
	});
	var isDeadCob = new Ext.form.ComboBox({
		store : storeisisDeadCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isDead',
		hidden : false,
		header : '是否去世',
		width : 10 * 6,
		editor : isDeadCob,
		renderer : Ext.util.Format.comboRenderer(isDeadCob)
	/*
	 * editor : new Ext.form.TextField({ allowBlank : true })
	 */
	});
	var storeisisDivorceCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '是', '是' ], [ '否', '否' ] ]
	});
	var isDivorceCob = new Ext.form.ComboBox({
		store : storeisisDivorceCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isDivorce',
		hidden : false,
		header : '是否离婚',
		width : 10 * 6,
		editor : isDivorceCob,
		renderer : Ext.util.Format.comboRenderer(isDivorceCob)
	});
	var storehaveChildCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '是', '是' ], [ '否', '否' ] ]
	});
	var haveChildCob = new Ext.form.ComboBox({
		store : storehaveChildCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'haveChild',
		hidden : false,
		header : '是否有子女',
		width : 15 * 6,
		editor : haveChildCob,
		renderer : Ext.util.Format.comboRenderer(haveChildCob)
	});
	var storeisTwinsCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '是', '是' ], [ '否', '否' ] ]
	});
	var isTwinsCob = new Ext.form.ComboBox({
		store : storeisTwinsCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isTwins',
		hidden : false,
		header : '是否双胞胎',
		width : 15 * 6,
		editor : isTwinsCob,
		renderer : Ext.util.Format.comboRenderer(isTwinsCob)
	});
	var storeisisCustodyCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '是', '是' ], [ '否', '否' ] ]
	});
	var isCustodyCob = new Ext.form.ComboBox({
		store : storeisisCustodyCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isCustody',
		hidden : false,
		header : '是否有监护权',
		width : 15 * 6,
		editor : isCustodyCob,
		renderer : Ext.util.Format.comboRenderer(isCustodyCob)
	});
	var storeisisBaseCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '是', '是' ], [ '', '否' ] ]
	});
	var isBaseCob = new Ext.form.ComboBox({
		store : storeisisBaseCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'leftUp',
		hidden : false,
		header : '癌症1',
		width : 15 * 6,
		editor : isBaseCob,
		renderer : Ext.util.Format.comboRenderer(isBaseCob)
	});
	cm.push({
		dataIndex : 'rightUp',
		hidden : false,
		header : '癌症2',
		width : 15 * 6,
		/*
		 * editor : new Ext.form.TextField({ allowBlank : true })
		 */
		editor : isBaseCob,
		renderer : Ext.util.Format.comboRenderer(isBaseCob)
	});
	cm.push({
		dataIndex : 'leftDown',
		hidden : false,
		header : '癌症3',
		width : 15 * 6,
		/*
		 * editor : new Ext.form.TextField({ allowBlank : true })
		 */
		editor : isBaseCob,
		renderer : Ext.util.Format.comboRenderer(isBaseCob)
	});
	cm.push({
		dataIndex : 'rightDown',
		hidden : false,
		header : '癌症4',
		width : 15 * 6,
		/*
		 * editor : new Ext.form.TextField({ allowBlank : true })
		 */
		editor : isBaseCob,
		renderer : Ext.util.Format.comboRenderer(isBaseCob)
	});
	cm.push({
		dataIndex : 'status',
		hidden : true,
		header : '状态',
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'patientId',
		hidden : true,
		header : '病案号',
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'family-id',
		hidden : true,
		header : '相关主表ID',
		width : 20 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'family-name',
		hidden : true,
		header : '相关主表',
		width : 20 * 10
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/system/family/family/showFamilyItemListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = "家系明细";
	opts.height = document.body.clientHeight - 100;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/system/family/family/delFamilyItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	/*
	 * opts.tbar.push({ text : '选择相关主表', handler : selectfamilyDialogFun });
	 */
	/*
	 * opts.tbar.push({ text : '选择电子病历', handler : selectcrmPatientDialogFun });
	 */
	opts.tbar.push({
		text : '生成家系图',
		handler : function() {
			window.open("/system/family/family/showFamily.action?id="
					+ $("#id_parent_hidden").val(), "", "", "");
			/*
			 * var win = Ext.getCmp('WeiChatCancerTypeFun'); if (win)
			 * {win.close();} var WeiChatCancerTypeFun= new Ext.Window({
			 * id:'WeiChatCancerTypeFun',modal:true,title:'选择先症',layout:'fit',width:500,height:500,closeAction:'close',
			 * plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			 * collapsible: true,maximizable: true, items: new
			 * Ext.BoxComponent({id:'maincontent', region: 'center', html:"<iframe
			 * scrolling='no' name='maincontentframe'
			 * src='/system/family/family/showFamily.action' frameborder='0'
			 * width='100%' height='100%' ></iframe>"}), buttons: [ { text:
			 * '关闭', handler: function(){ WeiChatCancerTypeFun.close(); } }] });
			 * WeiChatCancerTypeFun.show();
			 */
		}
	});
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"), "批量上传", null, {
				"确定" : function() {
					goInExcelcsv();
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	function goInExcelcsv() {
		var file = document.getElementById("file-uploadcsv").files[0];
		var n = 0;
		var ob = familyItemGrid.getStore().recordType;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
				if (n > 0) {
					if (this[0]) {
						var p = new ob({});
						p.isNew = true;
						var o;
						o = 0 - 1;
						p.set("po.fieldName", this[o]);
						o = 1 - 1;
						p.set("po.fieldName", this[o]);
						o = 2 - 1;
						p.set("po.fieldName", this[o]);
						o = 3 - 1;
						p.set("po.fieldName", this[o]);
						o = 4 - 1;
						p.set("po.fieldName", this[o]);
						o = 5 - 1;
						p.set("po.fieldName", this[o]);
						o = 6 - 1;
						p.set("po.fieldName", this[o]);
						o = 7 - 1;
						p.set("po.fieldName", this[o]);
						familyItemGrid.getStore().insert(0, p);
					}
				}
				n = n + 1;

			});
		}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	familyItemGrid = gridEditTable("familyItemdiv", cols, loadParam, opts);
	$("#familyItemdiv").data("familyItemGrid", familyItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectfamilyFun() {
	var win = Ext.getCmp('selectfamily');
	if (win) {
		win.close();
	}
	var selectfamily = new Ext.Window({
		id : 'selectfamily',
		modal : true,
		title : '选择相关主表',
		layout : 'fit',
		width : 500,
		height : 500,
		closeAction : 'close',
		plain : true,
		bodyStyle : 'padding:5px;',
		buttonAlign : 'center',
		collapsible : true,
		maximizable : true,
		items : new Ext.BoxComponent({
			id : 'maincontent',
			region : 'center',
			buttons : [ {
				text: biolims.common.close,
				handler : function() {
					selectfamily.close();
				}
			} ]
		})
	});
	selectfamily.show();
}
function setfamily(rec) {
	var gridGrid = $("#familyItemdiv").data("familyItemGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections();
	$.each(selRecords, function(i, obj) {
		obj.set('family-id', rec.get('id'));
		obj.set('family-name', rec.get('name'));
	});
	var win = Ext.getCmp('selectfamily')
	if (win) {
		win.close();
	}
}
function selectfamilyDialogFun() {
	var title = '';
	var url = '';
	title = "选择相关主表";
	url = ctx + "/FamilySelect.action?flag=family";
	var option = {};
	option.width = document.body.clientWidth - 30;
	option.height = document.body.clientHeight - 160;
	loadDialogPage(null, title, url, {
		"确定" : function() {
			selfamilyVal(this);
		}
	}, true, option);
}
var selfamilyVal = function(win) {
	var operGrid = familyDialogGrid;
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
		var gridGrid = $("#familyItemdiv").data("familyItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections();
		$.each(selRecords, function(i, obj) {
			obj.set('family-id', rec.get('id'));
			obj.set('family-name', rec.get('name'));
		});
		$(win).dialog("close");
		$(win).dialog("remove");
	} else {
		message("请选择您要选择的数据");
		return;
	}
};
function selectcrmPatientDialogFun() {
	var title = '';
	var url = '';
	title = "选择电子病历";
	url = ctx + "/crm/customer/patient/crmPatientSelect.action?flag=crmPatient";
	var option = {};
	option.width = document.body.clientWidth - 100;
	option.height = document.body.clientHeight - 160;
	loadDialogPage(null, title, url, {
		"确定" : function() {
			selcrmPatientVal(this);
			$(this).dialog("close");
		}
	}, true, option);
}
var selcrmPatientVal = function(win) {
	var operGrid = crmPatientDialogGrid;
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
		rec = selectRecord[0];

		// var gridGrid
		// =$("#show_dialog_crmPatient_div").data("crmPatientDialogGrid");
		var selRecords = familyItemGrid.getSelectionModel().getSelections();
		$.each(selRecords, function(i, obj) {
			obj.set('crmPatient-id', rec.get('id'));
			obj.set('crmPatient-name', rec.get('name'));
			obj.set('gender', rec.get('gender'));
			obj.set('age', rec.get('age'));
			obj.set('patientId', rec.get('familyId'));
		});
		$(win).dialog("close");
		$(win).dialog("remove");
	} else {
		message("请选择您要选择的数据");
		return;
	}
};
