$(function(){
	isShowDisease();
	$("#familyItem_birthDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
})
function isShowDisease(){
	$.ajax({
		type: "post",
		url: ctx + "/system/family/family/selectDiseaseValue.action",
		data: {
			id:$("#familyId").val()
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.flag) {
				$("#isDiseaseDiv").css("display","none");
				$("#relateManDiv").css("display","block");
				$("#familyTitle").text("关系关联人录入")
			var tr=[];
			var names=data.nameList;
			for(var i=0;i<names.length;i++){
				var name=names[i];
				tr.push('<option value="'+name+'"><fmt:message key="'+name+'" />'+name+'</option>');
			}
			$("#familyItem_relateMan").append(tr.join(""));
			}else{
				$("#isDiseaseDiv").css("display","block");
				$("#relateManDiv").css("display","none");
				$("#familyTitle").text("先症人信息录入")
				}
		}
	});
}
function haveBaby(){
	var child=$("#familyItem_haveChild").val();
	if(child=="1"){
		$("#familyItem_isCustody").val("1");
	}
}
function showPart(){
	var genderValue=$("#familyItem_gender").val();
	if(genderValue=="1"){
		$("#p1").css("display","block");
		$("#p2").css("display","block");
		$("#p3").css("display","block");
		$("#p4").css("display","block");
		$("#o1").css("display","none");
		$("#o2").css("display","none");
		$("#o3").css("display","none");
		$("#o4").css("display","none");
	}else if(genderValue=="0"){
		$("#o1").css("display","block");
		$("#o2").css("display","block");
		$("#o3").css("display","block");
		$("#o4").css("display","block");
		$("#p1").css("display","none");
		$("#p2").css("display","none");
		$("#p3").css("display","none");
		$("#p4").css("display","none");
	}else{
		$("#p1").css("display","block");
		$("#p2").css("display","block");
		$("#p3").css("display","block");
		$("#p4").css("display","block");
		$("#o1").css("display","block");
		$("#o2").css("display","block");
		$("#o3").css("display","block");
		$("#o4").css("display","block");
	}
}