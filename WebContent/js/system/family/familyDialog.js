var familyDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'cancer-id',
		type:"string"
	});
	    fields.push({
		name:'cancer-name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'hospital-id',
		type:"string"
	});
	    fields.push({
		name:'hospital-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'家系编号',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'cancer-id',
		header:'先症ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'cancer-name',
		header:'先症',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'hospital-id',
		header:'医院ID',
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'hospital-name',
		header:'医院',
		width:50*10,
		sortable:true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/family/family/showFamilyListJson.action";
	var opts={};
	opts.title="家系主数据";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setFamilyFun(rec);
	};
	familyDialogGrid=gridTable("show_dialog_family_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(familyDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
