/* 
 * 文件名称 :sampleReceiveEdit.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/01/30
 * 文件描述: 样本接收的相关页面函数
 * 
 */

var flg = true;
$(function() {
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#sampleReceive_state").text()=="Complete") {
		settextreadonly();
	}
	renderDatatables(flg);
	//批量结果的按钮变为下拉框
	btnChangeDropdown($('#main'), $(".resultsBatchBtn"), [biolims.common.qualified, biolims.common.disqualified], "method");
	//// 上传附件
	fileInput('1', 'sampleReceive', $("#sampleReveice_id").text());
});
//渲染的datatables
var oldChangeLog;
function renderDatatables(flg) {
	var colOpts = [];
	colOpts.push({
		"data": 'id',
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "patientNo",
		"title": biolims.user.itemNo,
		"createdCell": function(td) {
			$(td).attr("saveName", "patientNo");
		}
	})
	colOpts.push({
		"data": "patientName",
		"title": "姓名",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "patientName");
		}
	});
	colOpts.push({
		"data": 'age',
		"className": "edit",
		"title": "年龄", //订单编号  科研隐藏
		"createdCell": function(td) {
			$(td).attr("saveName", "age");
		}
	})
	colOpts.push({
		"data": 'birthDate',
		"className": "date",
		"title": "出生日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "birthDate");
		}
	})
	colOpts.push({
		"data": "fathrtId",
		"className": "edit",
		"title": "父亲",
		"createdCell": function(td) {
			$(td).attr("saveName", "fathrtId");
		}
	})
	colOpts.push({
		"data": "matherId",
		"title": "母亲",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "matherId");
		}
	})
	colOpts.push({
		"data": "gender",
		"title": biolims.common.gender,
		"className": "select",
		"name": biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
		"createdCell": function(td) {
			$(td).attr("saveName", "gender");
			$(td).attr("selectOpt", biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown);
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "-1") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "wife",
		"title": "妻子",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "wife");
		}
	})
	colOpts.push({
		"data": "husband",
		"title": "丈夫",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "husband");
		}
	})
	colOpts.push({
		"data": "patientId",
		"title": "丈夫",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "patientId");
		}
	})
	colOpts.push({
		"data": "crmPatient-id",
		"title": "丈夫",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "crmPatient-id");
		}
	})
	colOpts.push({
		"data": "crmPatient-name",
		"title": "丈夫",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "crmPatient-name");
		}
	})
	colOpts.push({
		"data": "family-id",
		"title": "丈夫",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "family-id");
		}
	})
	colOpts.push({
		"data": "family-name",
		"title": "丈夫",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "family-name");
		}
	})
	colOpts.push({
		"data": "isDisease",
		"title": "是否先症",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isDisease");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "isDead",
		"title": "是否死亡",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isDead");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "isDivorce",
		"title": "是否离婚",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isDivorce");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "haveChild",
		"title": "是否有子女",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "haveChild");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "isTwins",
		"title": "是否双胞胎",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isTwins");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "isCustody",
		"title": "是否有监护权",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isCustody");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "leftUp",
		"title": "疾病1",
		"width":"100px",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "leftUp");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "leftDown",
		"title": "疾病2",
		"width":"100px",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "leftDown");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "rightUp",
		"title": "疾病3",
		"width":"100px",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "rightUp");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "rightDown",
		"title": "疾病4",
		"width":"100px",
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "rightDown");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			if(data == "0") {
				return biolims.common.no;
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data": "fileNum",
		"title": "文件数量",
		"width":"100px",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "fileNum");
		}
	})

	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#main"));
		}
	});
	tbarOpts.push({
		text:biolims.common.addwindow,
		action: function() {
			addItemLayer($("#main"))
		}
	});
	tbarOpts.push({
		text:biolims.common.editwindow,
		action: function() {
			editItemLayer($("#main"))
		}
	});
	tbarOpts.push({
		text: biolims.common.copyRow,
		action: function() {
			copyItem($("#main"))
		}
	});
	tbarOpts.push({
		text: "选择电子病历",
		action: function() {
			selectEMR($("#main"))
		}
	});
	tbarOpts.push({
		text: "生成家系图",
		action: function() {
			createShipPic($("#main"))
		}
	});
	tbarOpts.push({
		text: "家系图引导",
		action: function() {
			shipPicGuide($("#main"))
		}
	});
	if($("#family_state").text()!="Complete"){
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#main"),
					"/system/family/family/delFamilyItem.action");
			}
		});
	}
	var options = table(true, $("#family_id").val(), "/system/family/family/showFamilyItemListJson.action", colOpts, tbarOpts);

	myTable = renderData($("#main"), options);
	myTable.on('draw', function() {
		oldChangeLog = myTable.ajax.json();
	});
}


//保存
function save() {
	var tabledata = saveItemjson($("#main"));
	if(!tabledata){
		return false;
	}
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	var data = {
		main: JSON.stringify(jsonn),
		itemJson: tabledata
	};
	var datas = saveItemjson($("#main"));
	var changeLogItem = "家系明细：";
	changeLogItem = getChangeLog(datas, $("#main"), changeLogItem);
	var changeLogItemLast ="";
	if(changeLogItem != "家系明细："){
		changeLogItemLast=changeLogItem
	}
	var changeLog = "家系主数据：";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: "post",
		url: ctx + "/system/family/family/save.action?changeLog="+changeLog+"&changeLogItem="+changeLogItemLast+"&ids="+$("#family_id").text(),
		data: data,
		async:false,
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				myTable.ajax.reload();
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx + "/system/family/family/editFamily.action?id="+data.id;
			}else{
				top.layer.closeAll();
				top.layer.msg("保存失败");
				}
		}
	});
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag=true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			// 判断男女并转换为数字
			if(k == "gender") {
				var gender = $(tds[j]).text();
				if(gender == biolims.common.male) {
					json[k] = "1";
				} else if(gender == biolims.common.female) {
					json[k] = "0";
				} else if(gender == biolims.common.unknown) {
					json[k] = "-1";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "isDisease") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "isDead") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "isDivorce") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "haveChild") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "isTwins") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "isCustody") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "leftUp") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "leftDown") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "rightUp") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			// 判断是否正常并转换为数字
			if(k == "rightDown") {
				var isGood = $(tds[j]).text();
				if(isGood == biolims.common.yes) {
					json[k] = "1";
				} else if(isGood == biolims.common.no) {
					json[k] = "0";
				} else {
					json[k] = "";
				}
				continue;
			}
			if(k=="age"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg("请填写年龄！");
					return false;
				}
			}
			if(k=="gender"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg("请填写性别！");
					return false;
				}
			}
		}
		data.push(json);
	});
	if(flag){
		return JSON.stringify(data);
	}else{
		return false;
	}
}
/*
 * 创建者 : dwb
 * 创建日期: 2018-05-10 17:13:12
 * 文件描述: 修改日志
 * 保存增加日志
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		 oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

function add() {
	window.location = window.ctx + "/system/family/family/editFamily.action";
}

function list() {
	window.location = window.ctx + '/system/family/family/showFamilyList.action';
}
//医疗机构
function showHos() {
	top.layer.open({
		title: biolims.common.selectedDoctor,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/customer/customer/crmCustomerSelectTable.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#family_hospital_id").val(id)
			$("#family_hospital_name").val(name)
		},
	})
}
//上传附件
function fileUp() {
	if($("#sampleReveice_id").text()=="NEW"){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
//查看附件
function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=sampleReceive&id=" + $("#sampleReveice_id").text(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

//复制行,只能复制一行
function copyItem(ele){
	var rows = ele.find(".selected");
	var length = rows.length;
	if(length==1){
		var row = rows.clone(true);
		row.find("td").eq(1).text("");
		row.addClass("editagain")
		row.find(".icheck").iCheck('uncheck').val("");
		rows.after(row);
		checkall(ele);
	}else if(!length||length<1){
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
	}else{
		top.layer.msg("只能选中一行复制");
	}
}

function downCsv(){
	var lan=$("#lan").val();
	if(lan=="en"){
		window.location.href=ctx+"/js/sample/uploadFileRoot/sampleReceiveItem_En.csv";
	}else{
		window.location.href=ctx+"/js/sample/uploadFileRoot/sampleReceiveItem.csv";
	}
}
//生成关系图
function createShipPic(){
	window.location.href=ctx+"/system/family/family/showFamily.action?id="
			+ $("#family_id").val();
}

//家系图引导
function shipPicGuide(){
	top.layer.open({
		title: "家属导向录入",
		type: 2,
		area: ["1150px", "420px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/family/family/familyGuide.action?id="+ $("#family_id").val(), ''],
		yes: function(index, layer) {
			var jsonnn = {};
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#formguide input").each(function(i, v) {
				var k = v.name;
				jsonnn[k] = v.value;
			});
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#formguide select").each(function(i, v) {
				var k = v.name;
				jsonnn[k] = v.value;
			});
			var itemJson=JSON.stringify(jsonnn);
			$.ajax({
				type: "post",
				url: ctx + "/system/family/family/insertFamilyItemTable.action",
				data: {
					id:$("#family_id").val(),
					itemJson:itemJson
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.closeAll();
						myTable.ajax.reload();
						top.layer.msg("添加成功");
					}else{
						top.layer.closeAll();
						}
				}
			});
			top.layer.close(index);
		},
	})
}
//电子病历
function selectEMR(){
	var rows = $("#main .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	if(length>1){
		top.layer.msg("只能选择一条数据");
		return false;
	}
	rows.each(function(i, val) {
		var checkBoxId = $(val).find("input[type=checkbox]").val();
	});
	top.layer.open({
		title: biolims.master.EMR,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/customer/patient/showCrmPatientDialogList.action", ''],
		yes: function(index, layer) {
			var crmPatientId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCrmPatient .chosed").children("td")
			.eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCrmPatient .chosed").children("td")
			.eq(1).text();
			var sex = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCrmPatient .chosed").children("td")
			.eq(2).text();
			var age = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCrmPatient .chosed").children("td")
			.eq(4).text();
			rows.addClass("editagain");
			rows.find("td[savename='patientName']").text(name);
			rows.find("td[savename='gender']").text(sex);
			rows.find("td[savename='age']").text(age);
			$("#family_patientNo").val(crmPatientId);
			top.layer.close(index);
		},
	})
}
