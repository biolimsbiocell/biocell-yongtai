var familyTable;
$(function() {
	
	var colData = [{
		//家系编号
			"data": "id",
			"title": biolims.user.itemNo,
		}, {
			"data": "cancer-id",
			"title": biolims.master.patientStatusId,
			"visible":false
		}, {
			"data": "cancer-cancerTypeName",
			"title": "qqq",
			"visible":false
		}, {
			"data": "crmPatient-cancerType-id",
			"title": biolims.master.hospitalId,
			"visible":false
		}, {
			"data": "crmPatient-cancerType-cancerTypeName",
			"title": "先症",
		}, {
			"data": "crmPatient-id",
			"title": biolims.sample.medicalNumber,
			"visible":false
		}, {
			"data": "crmPatient-name",
			"title": biolims.user.patientName,
			"visible":false
		}, {
			"data": "hospital-id",
			"title": biolims.common.code,
			"visible":false
		}, {
			"data": "hospital-name",
			"title": "医院",
		}, {
			"data": "patientNo",
			"title": "电子病历号",
		}, {
			"data": "patientName",
			"title": "姓名",
		}, {
			"data": "crmPatient-sampleCode",
			"title": "样本编号",
		}, {
			"data": "state",
			"title": biolims.common.note,
			"visible":false
		}, {
			"data": "note",
			"title": biolims.common.note,
		}];
		
		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "Family"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});
		
		
	var options = table(true, "",
		"/system/family/family/showFamilyListJson.action",colData , null)
	familyTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(familyTable);
	})
});
// 新建
function add() {
	window.location = window.ctx + "/system/family/family/editFamily.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/family/family/editFamily.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/family/family/viewFamily.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{

			"txt": biolims.common.gender,
			"type": "select",
			"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
			"changeOpt": "''|1|0|3",
			"searchName": "gender"
		},
		{
			"txt": biolims.wk.confirmUserName,
			"type": "top.layer",
			"searchName": "confirmUser",
			"action": "confirmUser()"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"txt": biolims.common.confirmDateStart,
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt":biolims.common.confirmDateEnd,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": sampleOrderTable
		}
	];
}