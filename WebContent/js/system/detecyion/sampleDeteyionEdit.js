﻿
$(function() {
	
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	var mainFileInput=fileInput ('1','sampleDeteyion',$("#sampleDeteyion_id").val());
	fieldCustomFun();
	
	okko();
	
	
	
})	
function change(id) {
	$("#" + id).css({
		"background-color" : "white",
		"color" : "black"
	});
}
function add() {
	window.location = window.ctx + "/system/detecyion/sampleDeteyion/editSampleDeteyion.action";
}

function list() {
	window.location = window.ctx + '/system/detecyion/sampleDeteyion/showSampleDeteyionList.action';
}

function tjsp() {
			layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {icon: 3, title:biolims.common.prompt}, function(index){
				layer.open({
					  title: biolims.common.submit,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toStartView.action?formName=SampleDeteyion",
					  yes: function(index, layero) {
						 var datas={
									userId : userId,
									userName : userName,
									formId : $("#sampleDeteyion_id").val(),
									title : $("#sampleDeteyion_name").val(),
									formName : 'SampleDeteyion'
								}
							ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
								if (data.success) {
									layer.msg(biolims.common.submitSuccess);
									if (typeof callback == 'function') {
										callback(data);
									}
									dialogWin.dialog("close");
								} else {
									layer.msg(biolims.common.submitFail);
								}
							}, null);
							layer.close(index);
						},
						cancel: function(index, layero) {
							layer.close(index)
						}
				
				});     
				  layer.close(index);
				});
}
function sp(){
	
		var taskId = $("#bpmTaskId").val();
		var formId = $("#sampleDeteyion_id").val();
		
					layer.open({
						  title: biolims.common.handle,
						  type:2,
						  anim: 2,
						  area: ['800px','500px']
						  ,btn: biolims.common.selected,
						  content: window.ctx+"/workflow/processinstance/toCompleteTaskView.action?taskId="+taskId+"&formId="+formId,
						  yes: function(index, layero) {
							  var operVal =  $(".layui-layer-iframe").find("iframe").contents().find("#oper").val();
							  var opinionVal =  $(".layui-layer-iframe").find("iframe").contents().find("#opinionVal").val();
							  var opinion =  $(".layui-layer-iframe").find("iframe").contents().find("#opinion").val();
								if(!operVal){
									layer.msg(biolims.common.pleaseSelectOper);
									return false;
								}
								if (operVal == "2") {
									_trunTodoTask(taskId, callback, dialogWin);
								} else {
									var paramData = {};
									paramData.oper = operVal;
									paramData.info = opinion;
					
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									}
									ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
										if (data.success) {
											layer.msg(biolims.common.submitSuccess);
											if (typeof callback == 'function') {
											}
										} else {
											layer.msg(biolims.common.submitFail);
										}
									}, null);
								}
								location.href =window.ctx+"/lims/pages/dashboard/dashboard.jsp";
							},
							cancel: function(index, layero) {
								layer.close(index)
							}
					
					});     
}

function ck(){
			layer.open({
					  title: biolims.common.checkFlowChart,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toTraceProcessInstanceView.action?formId=" + $("#sampleDeteyion_id").val(),
					  yes: function(index, layero) {
							layer.close(index)
					   },
						cancel: function(index, layero) {
							layer.close(index)
						}
				});     
}

/*function save() {
if(checkSubmit()==true){
		document.getElementById('sampleDeteyionItemJson').value = saveSampleDeteyionItemjson($("#sampleDeteyionItemTable"));
		console.log($("#sampleDeteyionItemJson").val());
		form1.action = window.ctx
				+ "/system/detecyion/sampleDeteyion/save.action?loadNumber=1";
		form1.submit();
	}
}*/

function save() {
if(checkSubmit()==true){
	
		var requiredField = requiredFilter();
		if (!requiredField) {
			flag = false;
			return false;
		}
		//自定义字段
		//拼自定义字段（实验记录）
		var inputs = $("#fieldItemDiv input");
		var options = $("#fieldItemDiv option");
		var contentData = {};
		var checkboxArr = [];
		$("#fieldItemDiv .checkboxs").each(function(i, v) {
			$(v).find("input").each(function(ii, inp) {
				var k = inp.name;
				if(inp.checked == true) {
					checkboxArr.push(inp.value);
					contentData[k] = checkboxArr;
				}
			});
		});
		inputs.each(function(i, inp) {
			var k = inp.name;
			if(inp.type != "checkbox") {
				contentData[k] = inp.value;
			}
		});
		options.each(function(i, opt) {
			if(opt.selected == true) {
				var k = opt.getAttribute("name");
				contentData[k] = opt.value;
			}
		});
		document.getElementById("fieldContent").value = JSON.stringify(contentData);
		
		var changeLog = "";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		var jsonStr = JSON.stringify($("#form1").serializeObject());  
		$.ajax({
			url: ctx + '/system/detecyion/sampleDeteyion/save.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				changeLog: changeLog,
				sampleDeteyionItemJson : saveSampleDeteyionItemjson($("#sampleDeteyionItemTable")),
				
				bpmTaskId : $("#bpmTaskId").val()
			},
			success: function(data) {
				//关闭加载层
				//layer.close(index);
				
				if(data.success) {
					var url = "/system/detecyion/sampleDeteyion/editSampleDeteyion.action?id="+data.id;
					
					if(data.bpmTaskId){
						url = url +"&bpmTaskId="+data.bpmTaskId;
					}
					
					window.location.href=url;
					
					
				}else{
					layer.msg(biolims.common.saveFailed);
				
				}
				}
			
		});
		
	}
}		

$.fn.serializeObject = function() {
	   var o = {};  
	    var a = this.serializeArray();  
	    $.each(a, function() {  
	        if (o[this.name]) {  
	            if (!o[this.name].push) {  
	                o[this.name] = [ o[this.name] ];  
	            }  
	            o[this.name].push(this.value || '');  
	        } else {  
	            o[this.name] = this.value || '';  
	        }  
	    });  
	    return o;  
};



function editCopy() {
	window.location = window.ctx + '/system/detecyion/sampleDeteyion/copySampleDeteyion.action?id=' + $("#sampleDeteyion_id").val();
}
function changeState() {
	var paraStr="formId=" + $("#sampleDeteyion_id").val()
	+ "&tableId=SampleDeteyion";
	layer.open({
		  title: biolims.common.changeState,
		  type:2,
		  anim: 2,
		  area: ['400px','400px']
		  ,btn: biolims.common.selected,
		  content: window.ctx
			+ "/applicationTypeAction/applicationTypeActionLook.action?" + paraStr
			+ "&flag=changeState'",
		  yes: function(index, layero) {
			  layer.confirm(biolims.common.toSubmit, {icon: 3, title:biolims.common.prompt}, function(index){
				 ajax("post","/applicationTypeAction/exeFun.action",{
					 applicationTypeActionId:sampleDeteyion,
					 formId:$("#sampleDeteyion_id").val()
				 },function(response){
					 var respText = response.message;
					 if (respText == '') {
							window.location.reload();
						} else {
							layer.msg(respText);
						}
				 },null)
				 layer.close(index);
			 })
			},
			cancel: function(index, layero) {
				layer.close(index)
			}
	
	});   
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sampleDeteyion_id").val());
	nsc.push("主表"+biolims.common.describeEmpty);
	fs.push($("#sampleDeteyion_name").val());
	nsc.push("描述"+biolims.common.describeEmpty);
	/*fs.push($("#sampleDeteyion_createUser").val());
	nsc.push("操作人"+biolims.common.describeEmpty);
	fs.push($("#sampleDeteyion_createDate").val());
	nsc.push("操作时间"+biolims.common.describeEmpty);
	fs.push($("#sampleDeteyion_state").val());
	nsc.push("状态"+biolims.common.describeEmpty);
	fs.push($("#sampleDeteyion_scopeId").val());
	nsc.push("成本中心id"+biolims.common.describeEmpty);
	fs.push($("#sampleDeteyion_scopeName").val());
	nsc.push("成本中心"+biolims.common.describeEmpty);*/
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			top.layer.msg(mess);
			return false;
		}
		return true;
}
function fileUp(){
	$("#uploadFile").modal("show");
}
function fileView(){
	layer.open({
		title:biolims.common.attachment,
		type:2,
		skin: 'layui-layer-lan',
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		content:window.ctx+"/operfile/initFileList.action?flag=1&modelType=sampleDeteyion&id="+$("#sampleDeteyion_id").val(),
		cancel: function(index, layero) {
			layer.close(index)
		}
	})
}
function settextreadonly() {
    jQuery(":text, textarea").each(function() {
	var _vId = jQuery(this).attr('id');
	jQuery(this).css("background-color","#B4BAB5").attr("readonly", "readOnly");
	if (_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}
function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}


//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

		//查找自定义列表的订单模块的此检测项目的相关内容
		$.ajax({
			type: "post",
			url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
			data: {
				moduleValue: "SampleDeteyion"
			},
			async: false,
			success: function(data) {
				var objValue = JSON.parse(data);
				if(objValue.success) {
					$.each(objValue.data, function(i, n) {
						var inputs = '';
						var disabled = n.readOnly ? ' ' : "disabled";
						var defaultValue = n.defaultValue ? n.defaultValue : ' ';
						if(n.fieldType == "checkbox") {
							var checkboxs = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
						} else if(n.fieldType == "radio") {
							var options = '';
							var singleOptionIdArry = n.singleOptionId.split("/");
							var singleOptionNameArry = n.singleOptionName.split("/");
							singleOptionIdArry.forEach(function(vv, jj) {
								options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
							});
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
						} else if(n.fieldType == "date") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + ' required=' + n.isRequired + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}

						$("#fieldItemDiv").append(inputs);
					});

				} else {
					layer.msg("自定义列表更新失败！");
				}
			}
		});
	

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}
function selectTemplate(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/template/template/showTemplateDialogList.action?type=0", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplate .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplate .chosed").children("td")
				.eq(0).text();
			$("#sampleDeteyion_template_id").val(id);
			$("#sampleDeteyion_template_name").val(name);
			top.layer.close(index);
		},
	})
}

function okko(){
	if($("#sampleDeteyion_yesOrNo2").val()=="0"){
		$("#okko").hide();
	}else{
		$("#okko").show();
	}
}