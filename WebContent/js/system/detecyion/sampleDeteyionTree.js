$(document).ready(function() {
var fields = [];
	fields.push({
		title : "主表",
		field : "id"
	});
	fields.push({
		title : "描述",
		field : "name"
	});
	fields.push({
		title : "操作人",
		field : "createUser"
	});
	fields.push({
		title : "操作时间",
		field : "createDate"
	});
	fields.push({
		title : "状态",
		field : "state"
	});
	
	   
	fields.push({
		title : "成本中心id",
		field : "scopeId"
	});
	
	   
	fields.push({
		title : "成本中心",
		field : "scopeName"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/system/detecyion/sampleDeteyion/showSampleDeteyionListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/system/detecyion/sampleDeteyion/editSampleDeteyion.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/detecyion/sampleDeteyion/editSampleDeteyion.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/detecyion/sampleDeteyion/viewSampleDeteyion.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : "主表",
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "描述",
		"searchName" : 'name',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "操作人",
		"searchName" : 'createUser',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "操作时间",
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态",
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "成本中心id",
		"searchName" : 'scopeId',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "成本中心",
		"searchName" : 'scopeName',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

