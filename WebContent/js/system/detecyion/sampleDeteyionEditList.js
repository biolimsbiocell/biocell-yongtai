var sampleDeteyionTable;
var oldsampleDeteyionChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.sampleDetecyion.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.sampleDetecyion.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"createUser",
		"title": biolims.sampleDetecyion.createUser,
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.sampleDetecyion.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.sampleDetecyion.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"scopeId",
		"title": biolims.sampleDetecyion.scopeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "scopeId");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"scopeName",
		"title": biolims.sampleDetecyion.scopeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "scopeName");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#sampleDeteyionTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#sampleDeteyionTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#sampleDeteyionTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#sampleDeteyion_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/system/detecyion/sampleDeteyion/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 sampleDeteyionTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveSampleDeteyion($("#sampleDeteyionTable"));
		}
	});
	}
	
	var sampleDeteyionOptions = 
	table(true, "","/system/detecyion/sampleDeteyion/showSampleDeteyionTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	sampleDeteyionTable = renderData($("#sampleDeteyionTable"), sampleDeteyionOptions);
	sampleDeteyionTable.on('draw', function() {
		oldsampleDeteyionChangeLog = sampleDeteyionTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveSampleDeteyion(ele) {
	var data = saveSampleDeteyionjson(ele);
	var ele=$("#sampleDeteyionTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/system/detecyion/sampleDeteyion/saveSampleDeteyionTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveSampleDeteyionjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldsampleDeteyionChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
