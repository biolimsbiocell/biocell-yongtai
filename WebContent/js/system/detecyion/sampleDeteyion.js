var sampleDeteyionTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		
		"title":'检测项编码',
	});
	
	    fields.push({
		"data":"name",
		"title":'描述',
	});
	


		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "SampleDeteyion"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});

	var options = table(true, "","/system/detecyion/sampleDeteyion/showSampleDeteyionTableJson.action",
	 fields, null)
	sampleDeteyionTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleDeteyionTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/system/detecyion/sampleDeteyion/editSampleDeteyion.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/detecyion/sampleDeteyion/editSampleDeteyion.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/detecyion/sampleDeteyion/viewSampleDeteyion.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":"编码"
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":"描述"
		});
//	   fields.push({
//		    "searchName":"createUser",
//			"type":"input",
//			"txt":biolims.sampleDetecyion.createUser
//		});
//	   fields.push({
//		    "searchName":"createDate",
//			"type":"input",
//			"txt":biolims.sampleDetecyion.createDate
//		});
//	fields.push({
//			"txt": "操作时间(Start)",
//			"type": "dataTime",
//			"searchName": "createDate##@@##1",
//			"mark": "s##@@##",
//		},
//		{
//			"txt": "操作时间(End)",
//			"type": "dataTime",
//			"mark": "e##@@##",
//			"searchName": "createDate##@@##2"
//		});
//	   fields.push({
//		    "searchName":"state",
//			"type":"input",
//			"txt":biolims.sampleDetecyion.state
//		});
//	   fields.push({
//		    "searchName":"scopeId",
//			"type":"input",
//			"txt":biolims.sampleDetecyion.scopeId
//		});
//	   fields.push({
//		    "searchName":"scopeName",
//			"type":"input",
//			"txt":biolims.sampleDetecyion.scopeName
//		});
	
	fields.push({
		"type":"table",
		"table":sampleDeteyionTable
	});
	return fields;
}
