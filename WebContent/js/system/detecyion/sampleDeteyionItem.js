var sampleDeteyionItemTable;
var oldsampleDeteyionItemChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#sampleDeteyion_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": 'ID',
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title":'名称',
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    }
	});
	colOpts.push({
		"data":"testingCriteria",
		"title": '检测标准',
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "testingCriteria");
	    }
	});
	   colOpts.push({
		"data":"note",
		"title": '质量标准',
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    }
	});
	   colOpts.push({
			"data":"nextFlow-id",
			"title": '流向ID',
			"createdCell": function(td) {
				$(td).attr("saveName", "nextFlow-id");
		    },
		    "visible":false
		});
	   colOpts.push({
		   "data":"nextFlow-name",
		   "title": '质检方式',
		   "createdCell": function(td,data,rowData) {
			   $(td).attr("saveName", "nextFlow-name");
			   $(td).attr("nextFlow-id", rowData["nextFlow-id"]);
		   }
	   });
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#sampleDeteyionItemTable"),
					"/system/detecyion/sampleDeteyion/delSampleDeteyionItem.action", "检测项：", id);
			}
		});
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#sampleDeteyionItemTable"))
	}
	});
	
	tbarOpts.push({
		text: '选择质检字段',
		action: function() {
			var rows = $("#sampleDeteyionItemTable .selected");
			var length = rows.length;
			if(!length) {
				top.layer.msg(biolims.common.pleaseSelectData);
				return false;
			}
			top.layer.open({
				title: '选择质检字段',
				type: 2,
				area: ["650px", "400px"],
				btn: biolims.common.selected,
				content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=Quality", ''],
				yes: function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
					var code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(3).text();
					top.layer.close(index);
			
					rows.addClass("editagain");
					rows.find("td[savename='name']").text(name);
					rows.find("td[savename='note']").text(code);

				},
			})
		}
	});
	
	tbarOpts.push({
		text: '选择检测类型',
		action: function() {
			var rows = $("#sampleDeteyionItemTable .selected");
			var length = rows.length;
			if(!length) {
				top.layer.msg(biolims.common.pleaseSelectData);
				return false;
			}
			top.layer.open({
				title: '选择检测类型',
				type: 2,
				area: ["650px", "400px"],
				btn: biolims.common.selected,
				content: [window.ctx + "/system/nextFlow/nextFlow/showAllQualityTestDialogList.action", ''],
				yes: function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#qualityTestNext .chosed").children("td").eq(0).text();
					var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#qualityTestNext .chosed").children("td").eq(1).text();
					top.layer.close(index);
			
					rows.addClass("editagain");
					rows.find("td[savename='nextFlow-id']").text(id);
					rows.find("td[savename='nextFlow-name']").attr("nextFlow-id",id).text(name);

				},
			})
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#sampleDeteyionItemTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveSampleDeteyionItem($("#sampleDeteyionItemTable"));
		}
	});
	
	}
	
	var sampleDeteyionItemOptions = table(true,
		id,
		'/system/detecyion/sampleDeteyion/showSampleDeteyionItemTableJson.action', colOpts, tbarOpts)
	sampleDeteyionItemTable = renderData($("#sampleDeteyionItemTable"), sampleDeteyionItemOptions);
	sampleDeteyionItemTable.on('draw', function() {
		oldsampleDeteyionItemChangeLog = sampleDeteyionItemTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveSampleDeteyionItem(ele) {
	var data = saveSampleDeteyionItemjson(ele);
	var ele=$("#sampleDeteyionItemTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/system/detecyion/sampleDeteyion/saveSampleDeteyionItemTable.action',
		data: {
			id: $("#sampleDeteyion_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveSampleDeteyionItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "nextFlow-name") {
				json["nextFlow-id"] = $(tds[j]).attr("nextFlow-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldsampleDeteyionItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
