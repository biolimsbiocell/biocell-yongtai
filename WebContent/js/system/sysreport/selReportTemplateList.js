var reportTemplateGrid;
$(function() {
	var cols = {};
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 120,
		hidden : true
	});

	cm.push({
		dataIndex : 'name',
		header : biolims.common.designation,
		width : 160,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.name,
		width : 200,
		sortable : true
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/sysmanage/report/selReportTemplateListJson.action";
	var opts = {};
	opts.height = 380;
	opts.width = 453;
	// opts.title = "报告模版";
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setReportFun(rec);
	};

	reportTemplateGrid = gridTable("report_template_grid_div", cols, loadParam, opts);

	$("#sel_search").click(function() {
		var data = {};
		data.name = $("#sel_search_name").val();
		data.note = $("#sel_search_note").val();
		reportTemplateGrid.getStore().baseParams.data = JSON.stringify(data);
		reportTemplateGrid.getStore().load();
	});
});