var reportTemplateGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'type',
		type : "string"
	});
	fields.push({
		name : 'attach-fileName',
		type : "string"
	});
	fields.push({
		name : 'attach-id',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 120,
		hidden : true
	});

	cm.push({
		dataIndex : 'name',
		header : biolims.common.designation,
		width : 100,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.path,
		width : 200,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var storetype = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [  [ '0', 'AA' ], [ '1', 'GA' ], [ '2', 'GG' ] ]
//	});
//	var type = new Ext.form.ComboBox({
//		store : storetype,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'type',
//		header:'模板类型',
//		width:20*6,
//		sortable:true,
//		renderer : Ext.util.Format.comboRenderer(type),
//		editor:type
//	});
	var unitstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'geneName'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx
					+ '/sample/dic/dicPriLibTypeCobJson.action',
			method : 'POST'
		})
	});
	unitstore.load();
	var unitcob = new Ext.form.ComboBox({
		store : unitstore,
		displayField : 'geneName',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'type',
		header : biolims.QPCR.genotype,
		width : 100,
		renderer : Ext.util.Format.comboRenderer(unitcob),
		editor : unitcob
	});
	var gridState = new Ext.form.ComboBox({
		transform : "grid_state",
		width : 50,
		triggerAction : 'all',
		lazyRender : true
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 100,
		sortable : true,
		editor : gridState,
		renderer : Ext.util.Format.comboRenderer(gridState)
	});
	var attach = new Ext.form.TextField({
		allowBlank : true
	});

	attach.on('focus', function() {
		var record = reportTemplateGrid.getSelectOneRecord();
		downFile(record.get("attach-id"));
	});
	cm.push({
		dataIndex : 'attach-fileName',
		header : biolims.common.attachment,
		width : 200,
		editor : attach
	});

	cm.push({
		dataIndex : 'attach-id',
		header : biolims.report.reportFileId,
		width : 200,
		hidden : true
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/sysmanage/report/showReportTemplateListJson.action";
	var opts = {};
	opts.height = document.body.clientHeight - 25;
	opts.title = biolims.report.reportTemplateManagement;
	// operParams = {};
	// operParams.isShowDefaultTbar = true;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ids = "'" + ids.join("','") + "'";
		ajax("post", "/sysmanage/report/delReportTemplateInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);

	};
	opts.tbar.push({
		text : biolims.common.fillDetail,
		iconCls : 'add',
		handler : function() {
			var ob = reportTemplateGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			p.set("state", "1");
			reportTemplateGrid.stopEditing();
			reportTemplateGrid.getStore().insert(0, p);
			reportTemplateGrid.startEditing(0, 0);
		}
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	
	opts.tbar.push({
		text : biolims.report.uploadReportTemplate,
		handler : function() {
			var isUpload = true;
			var record = reportTemplateGrid.getSelectOneRecord();
			load("/system/template/template/toSampeUpload.action", { // 是否修改
				fileId : record.get("attach-id"),
				isUpload : isUpload
			}, null, function() {
				$("#upload_file_div").data("callback", function(data) {
					record.set("attach-fileName", data.fileName);
					record.set("attach-id", data.fileId);
				});
			});
		}
	});
	
	opts.tbar.push({
		text : biolims.common.save,
		handler : function() {
			var result = getReportTemplateItemData();
			if (result.length > 0) {
				ajax("post", "/sysmanage/report/save.action", {
					itemDataJson : result
				}, function(data) {
					if (data.success) {
						message(biolims.common.saveSuccess);
						reportTemplateGrid.getStore().commitChanges();
						reportTemplateGrid.getStore().reload();
					} else {
						message(biolims.common.saveFailed);
					}
				}, null);
			}
		}
	});

	var getReportTemplateItemData = function() {
		var modifyRecord = reportTemplateGrid.getModifyRecord();
		var itemDataJson = [];
		if (modifyRecord && modifyRecord.length > 0) {
			$.each(modifyRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.name = obj.get("name");
				data.note = obj.get("note");
				data.state = obj.get("state");
				data.type = obj.get("type");
				data["attach-id"] = obj.get("attach-id");
				itemDataJson.push(data);
			});
			return JSON.stringify(itemDataJson);
		} else {
			return "";
		}
	};

	reportTemplateGrid = gridEditTable("report_template_grid_div", cols, loadParam, opts);
});
function downFile(id){
	window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
}