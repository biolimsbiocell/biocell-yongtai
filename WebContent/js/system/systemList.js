var systemListGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'tableName',
		type : "string"
	});
	fields.push({
		name : 'columnName',
		type : "string"
	});
	fields.push({
		name : 'columnComment',
		type : "string"
	});
	fields.push({
		name : 'isdisplay',
		type : "string"
	});
	fields.push({
		name : 'controltype',
		type : "string"
	});
	fields.push({
		name : 'isrequire',
		type : "string"
	});
	fields.push({
		name : 'colsort',
		type : "String"
	});
	fields.push({
		name : 'controlwidth',
		type : "String"
	});
	fields.push({
		name : 'isreadonly',
		type : "string"
	});
	fields.push({
		name : 'nullable',
		type : "string"
	});
	fields.push({
		name : 'dataPrecision',
		type : "string"
	});
	fields.push({
		name : 'dataScale',
		type : "string"
	});
	fields.push({
		name : 'dataLength',
		type : "string"
	});
	fields.push({
		name : 'dataType',
		type : "string"
	});
	fields.push({
		name : 'modeltype',
		type : "string"
	});
	fields.push({
		name : 'modeltypeclass',
		type : "string"
	});
	fields.push({
		name : 'fieldType',
		type : "string"
	});
	cols.fields = fields;
	var storeisdisplayCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'Y', 'Y' ], [ 'N', 'N' ] ]
	});
	var isdisplayCob = new Ext.form.ComboBox({
		store : storeisdisplayCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var storeisrequireCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'N', 'N' ], [ 'Y', 'Y' ] ]
	});
	var isrequireCob = new Ext.form.ComboBox({
		store : storeisrequireCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var storeisreadonlyCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'N', 'N' ], [ 'Y', 'Y' ] ]
	});
	var isreadonlyCob = new Ext.form.ComboBox({
		store : storeisreadonlyCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var storenullableCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'N', 'N' ], [ 'Y', 'Y' ] ]
	});
	var nullableCob = new Ext.form.ComboBox({
		store : storenullableCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var storedataTypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', 'A' ], [ '1', 'B' ], [ '2', 'C' ], [ '3', 'D' ], [ '4', 'E' ], [ '5', 'F' ] ]
	});
	var dataTypeCob = new Ext.form.ComboBox({
		store : storedataTypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var storemodeltypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'N', 'N' ], [ 'Y', 'Y' ] ]
	});
	var modeltypeCob = new Ext.form.ComboBox({
		store : storemodeltypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var storecontroltypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'text', biolims.common.textBox ], [ 'Date', biolims.common.dateTextBox ], [ 'Time', biolims.common.timeTextBox ], [ 'combox', biolims.common.combobox ], [ 'textArea', biolims.common.richTextBox ], [ 'storecombox', biolims.common.storeList ], [ 'hidden', biolims.common.hiddenDomain ], [ 'auto', biolims.common.automaticGeneration ] ,[ 'DicType', biolims.common.generalDictionarySelection ], 
		         [ 'DicUnit', biolims.common.selectionOfMeasurementUnits ], [ 'User', biolims.common.userSelection ], [ 'DicState', biolims.common.stateSeletion ], [ 'Department', biolims.common.departmentSelection ], [ 'UserAllSelect', biolims.common.multipleSelectionOfUsers ], [ 'TaskAllSelect', biolims.common.multipleSelectionForALaterStep ] ]
	});
	var controltypeCob = new Ext.form.ComboBox({
		store : storecontroltypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var storefieldTypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'String', 'String' ],  [ 'Date', 'Date' ], [ 'Double', 'Double' ], ]
	});
	var fieldTypeCob = new Ext.form.ComboBox({
		store : storefieldTypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.user.itemNo,
		width : 10 * 10,
		hidden : true,
		sortable : false
	});
	cm.push({
		dataIndex : 'tableName',
		header : biolims.common.tableName,
		width : 10 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'columnName',
		header : biolims.common.columnName,
		width : 10 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'columnComment',
		header : biolims.common.columnDescription,
		width : 10 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'fieldType',
		header : biolims.common.fieldType,
		width : 10 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'dataLength',
		header : biolims.common.databaseLength,
		width : 10 * 10,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'modeltype',
		header : biolims.common.entityName,
		width : 10 * 10,
		sortable : true,
		editor : modeltypeCob,
		renderer :Ext.util.Format.comboRenderer(modeltypeCob)
	});

	
	cm.push({
		dataIndex : 'controltype',
		header : biolims.common.buttonType,
		width : 10 * 10,
		sortable : true,
		editor : controltypeCob,
		renderer :Ext.util.Format.comboRenderer(controltypeCob)
	});
	
	cm.push({
		dataIndex : 'isdisplay',
		header : biolims.common.doesItShow,
		width : 10 * 10,
		editor : isdisplayCob,
		renderer :Ext.util.Format.comboRenderer(isdisplayCob)
	});
	
	cm.push({
		dataIndex : 'isreadonly',
		header : biolims.common.readOnly,
		width : 10 * 10,
		sortable : true,
		editor : isreadonlyCob,
		renderer :Ext.util.Format.comboRenderer(isreadonlyCob)
	});
	cm.push({
		dataIndex : 'isrequire',
		header : biolims.common.isrequire,
		width : 10 * 10,
		sortable : true,
		editor : isrequireCob,
		renderer :Ext.util.Format.comboRenderer(isrequireCob)
	});
	cm.push({
		dataIndex : 'colsort',
		header : biolims.common.sort,
		width : 10 * 10,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'modeltypeclass',
		header : biolims.common.functionName,
		width : 10 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'controlwidth',
		header : 'controlwidth',
		width : 10 * 10,
		sortable : true,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'nullable',
		header : 'nullable',
		width : 10 * 10,
		sortable : true,
		hidden : true,
		/*editor : nullableCob,
		renderer :Ext.util.Format.comboRenderer(nullableCob)*/
	});
	cm.push({
		dataIndex : 'dataPrecision',
		header : 'dataPrecision',
		width : 10 * 10,
		sortable : true,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'dataScale',
		header : 'dataScale',
		width : 10 * 10,
		sortable : true,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'dataType',
		header : 'dataType',
		width : 10 * 10,
		sortable : true,
		hidden : true
/*		editor : dataTypeCob,
		renderer :Ext.util.Format.comboRenderer(dataTypeCob)
*/	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/system/systemJoin.action";
	var opts = {};
	opts.title = biolims.common.automaticGeneration;
	opts.height = document.body.clientHeight - 34;
	opts.tbar = [];
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id,rec) {
		$('#selectId').val(id);
		edit();
	};
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.delSelect = function(ids) {
		
		ajax("post", "/system/delSystem.action", {
			ids : ids
			
		}, function(data) {
			if (data.success) {
				systemListGrid.getStore().commitChanges();
				systemListGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				systemListGrid.getStore().commitChanges();
				systemListGrid.getStore().reload();
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_system_div"),biolims.common.batchUpload,null,{
				"Confirm":function(){
					goInExcel1();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	var idTmr = "";
	function goInExcel1(){
		var file = document.getElementById("file-uploada").files[0];  
		var n = 0;
		var ob = systemListGrid.getStore().recordType;
		var ids = "";
		var reader = new FileReader();  
		//将文件以文本形式读入页面  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[1]){
                			var p = new ob({});
                			p.isNew = true;				
//                			p.set("id",this[0]);
                			p.set("tableName",this[1]);
							p.set("columnName",this[2]);
							p.set("columnComment",this[3]);
					
							p.set("fieldType",this[4]);
							p.set("dataLength",this[5]);//数字
							p.set("modeltype",this[6]);//下拉
							p.set("controltype",this[7]);//下拉
							p.set("isdisplay", this[8]);//是/否
							p.set("isreadonly",this[9]);//是否
							p.set("isrequire",this[10]);//是否
							p.set("colsort",this[11]);//数字
							p.set("modeltypeclass",this[16]);//录入
//							p.set("controlwidth",this[8]);//数字
//							p.set("nullable",this[10]);//是否
//							p.set("dataPrecision",this[11]);//数字
//							p.set("dataScale",this[12]);//数字
//							p.set("dataType",this[14]);//下拉框
//							ids = ids+"'"+this[0]+"',";
							systemListGrid.getStore().insert(0, p);
                		}
                	}
                    n = n +1;
               });
		}
	}
	opts.tbar.push({
		text : biolims.common.fillDetail,
		iconCls : 'add',
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		iconCls : 'add',
		handler : function() {
			var ob = systemListGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			p.set("tableName", $("#tableName").val());
			
			
				
				
			systemListGrid.stopEditing();
			systemListGrid.getStore().insert(0, p);
			systemListGrid.startEditing(0, 0);
		}
	});
	
	opts.tbar.push({
		text : biolims.common.save,
		handler : function() {
			var itemDataJson = [];
			var resulta = c();
				ajax("post", "/system/saveSystembById.action", {
					itemDataJson : resulta
				}, function(data){
					if (data.success) {
						message(biolims.common.saveSuccess);
						systemListGrid.getStore().commitChanges();
						systemListGrid.getStore().reload();
					} else{
						systemListGrid.getStore().commitChanges();
						systemListGrid.getStore().reload();
						message(biolims.common.saveFailed);
					}
				}, null);
		}	
	});

		
	var c = function() {
		var ModifyRecord = systemListGrid.getModifyRecord();
		var itemDataJson = [];
		if (ModifyRecord && ModifyRecord.length > 0) {
			$.each(ModifyRecord,function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.tableName = obj.get("tableName");
				data.columnName = obj.get("columnName");
				data.modeltype = obj.get("modeltype");
				data.fieldType = obj.get("fieldType");
				data.dataLength = obj.get("dataLength");
				data.controltype = obj.get("controltype");
				data.isdisplay = obj.get("isdisplay");
				data.isreadonly = obj.get("isreadonly");
				data.isrequire = obj.get("isrequire");
				data.colsort = obj.get("colsort");
				data.modeltypeclass = obj.get("modeltypeclass");				
				data.columnComment = obj.get("columnComment");
				data.controlwidth = obj.get("controlwidth");
				data.nullable = obj.get("nullable");
				data.dataPrecision = obj.get("dataPrecision");
				data.dataScale = obj.get("dataScale");
				data.dataType = obj.get("dataType");
				itemDataJson.push(data);
			});
			return JSON.stringify(itemDataJson);
		} else {
			return "";
		}
	};
	systemListGrid = gridEditTable("system_wait_grid_div", cols, loadParam, opts);
	$("#left").data("operWaitGrid", systemListGrid);
	$("#wait_qc").data("operWaitGrid", systemListGrid);
})
function selectCustomer(){
	
	
	
	
	
	var filter1 = function(record, id){
		   if (record.get("tableName")==$("#tableName").val()){
		      	return true;
		      }
		   else
		      return false;
		};
		var onStoreLoad1 = function(store, records, options){
		   store.filterBy(filter1);
		};
		
		systemListGrid.store.on("load", onStoreLoad1);
		systemListGrid.store.reload();
}