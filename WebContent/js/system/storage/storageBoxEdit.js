﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/system/storage/storageBox/editStorageBox.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/system/storage/storageBox/showStorageBoxList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#storageBox", {
					userId : userId,
					userName : userName,
					formId : $("#storageBox_id").val(),
					title : $("#storageBox_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#storageBox_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
//	 	var storageBoxItemDivData = storageBoxItemGrid;
//	 	if(!storageBoxItemDivData){
//	 		document.getElementById('storageBoxItemDivJson').value = commonGetModifyRecords(storageBoxItemDivData);
//	 	}
	 	
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/system/storage/storageBox/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/system/storage/storageBox/copyStorageBox.action?id=' + $("#storageBox_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#storageBox_id").val() + "&tableId=storageBox");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#storageBox_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#storageBox_state").val());
	nsc.push(biolims.common.stateIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.storageBox,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/system/storage/storageBox/showStorageBoxItemList.action", {
	id : $("#storageBox_id").val()
}, "#storageBoxPage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
		$(function() {
							 new Ext.form.ComboBox({
								transform : "storageBox_state",
								width : 80,
								hiddenId : "storageBox_state",
								hiddenName : "storageBox.state"
							});
					});

	var item = menu.add({
		text: '确认转移'
						});
	item.on('click', saveNewStorage);
	setTimeout(function() {
		if($("#storageBox_container_id").val()){
			load("/storage/container/showGridBoxDataDialog.action?=", {
			idStr : $("#storageBox_id").val()
		}, "#3d_image", null);
			
		}
	}, 700);	
	function saveNewStorage(){
		ajax("post", "/system/storage/storageBox/saveNewStorage.action", {
			id : $("#storageBox_id").val(),
			locationId:$("#storageBox_oldLocationId").val(),
			newLocationId:$("#storageBox_newLocationId").val()
			}, function(data) {
				if (data.success) {
					message("成功转移新储位！");
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
	}
	
	//选择位置
	function FrozenLocationFun(){
		 var win = Ext.getCmp('FrozenLocationFun');
		 if (win) {win.close();}
		 var ProjectFun= new Ext.Window({
		 id:'FrozenLocationFun',modal:true,title:biolims.sample.selectLocation,layout:'fit',width:900,height:660,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		 html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog.action?typeId="+$("#sampleIn_sampleType").val()+"&type=0' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: biolims.common.close,
		  handler: function(){
		  ProjectFun.close(); }  }]  });     ProjectFun.show(); }
	function setFrozenLocationFun(str,type){
		var ids = str.split("-");
		var str1="";
		var strId="";
		if(ids.length==3){
			if(ids[2].length==2){
				strId = ids[2].substr(0,1)+"0"+ids[2].substr(1);
			}else{
				strId = ids[2];
			}
			str1 = ids[0]+"-"+ids[1]+"-"+strId;
		}else if(ids.length==4){
			if(ids[3].length==2){
				strId = ids[3].substr(0,1)+"0"+ids[3].substr(1);
			}else{
				strId = ids[3];
			}
			str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+strId;
		}else if(ids.length==5){
			if(ids[4].length==2){
				strId = ids[4].substr(0,1)+"0"+ids[4].substr(1);
			}else{
				strId = ids[4];
			}
			str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+ids[3]+"-"+strId;
		}
		if(type==0){
			$("#storageBox_locationId").val(str1);
		}else{
			$("#storageBox_newLocationId").val(str1);
		}
		 var win = Ext.getCmp('FrozenLocationFun')
		 if(win){win.close();}
	 }	
	//选择位置
	function FrozenLocationFun1(){
		 var win = Ext.getCmp('FrozenLocationFun');
		 if (win) {win.close();}
		 var ProjectFun= new Ext.Window({
		 id:'FrozenLocationFun',modal:true,title:biolims.sample.selectLocation,layout:'fit',width:900,height:660,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		 html:"<iframe scrolling='yes' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog.action?typeId="+$("#sampleIn_sampleType").val()+"&type=1' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: biolims.common.close,
		  handler: function(){
		  ProjectFun.close(); }  }]  });     ProjectFun.show(); }
//	function setFrozenLocationFun(str,upStr){
//		var ids = str.split("-");
//		var str1="";
//		var strId="";
//		if(ids.length==3){
//			if(ids[2].length==2){
//				strId = ids[2].substr(0,1)+"0"+ids[2].substr(1);
//			}else{
//				strId = ids[2];
//			}
//			str1 = ids[0]+"-"+ids[1]+"-"+strId;
//		}else if(ids.length==4){
//			if(ids[3].length==2){
//				strId = ids[3].substr(0,1)+"0"+ids[3].substr(1);
//			}else{
//				strId = ids[3];
//			}
//			str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+strId;
//		}else if(ids.length==5){
//			if(ids[4].length==2){
//				strId = ids[4].substr(0,1)+"0"+ids[4].substr(1);
//			}else{
//				strId = ids[4];
//			}
//			str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+ids[3]+"-"+strId;
//		}
//		$("#storageBox_locationId").val(str1);
//		 var win = Ext.getCmp('FrozenLocationFun')
//		 if(win){win.close();}
//	 }	