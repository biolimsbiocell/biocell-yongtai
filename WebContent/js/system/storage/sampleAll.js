var sampleAllTab, oldChangeLog;
// 出库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
	})
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
	})
	colOpts.push({
		"data" : "sampleCode",
		"title" : biolims.common.sampleCode,
	});

	colOpts.push({
		"data" : "sampleType",
		"title" :biolims.common.originalSampleType,
	});
	colOpts.push({
		"data" : "storageBox-id",
		"title" : "盒子编号",
	});
	colOpts.push({
		"data" : "storageBox-name",
		"title" : "盒子名称",
	});
	colOpts.push({
		"data" : "boxLocation",
		"title" : "盒内位置",
	});
	
	var tbarOpts = [];
	var sampleAllOps = table(
			true,
			$("#storageBox_id").val(),
			"/system/storage/storageBox/selectSampleInfoInByBoxId.action",
			colOpts, tbarOpts);
	sampleAllTab = renderData($("#sampleAlldiv"),
			sampleAllOps);
});

