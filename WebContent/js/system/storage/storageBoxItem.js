﻿var storageBoxItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'locationId',
		type:"string"
	});
    fields.push({
		name:'locationName',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
		name:'changeUser',
		type:"string"
	});
    fields.push({
		name:'changeDate',
		type:"date",
		dateFormat:"Y-m-d"
    });
    fields.push({
		name:'storageBox-id',
		type:"string"
	});
    fields.push({
		name:'storageBox-name',
		type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:40*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6
	});	
	cm.push({
		dataIndex:'locationId',
		hidden : false,
		header:biolims.common.storageId,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'locationName',
		hidden : false,
		header:biolims.common.storageNote,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'changeUser',
		hidden : false,
		header:biolims.common.modifiedBy,
		width:20*6
	});
	cm.push({
		dataIndex:'changeDate',
		hidden : false,
		header:biolims.common.updateDate,
		renderer: formatDate,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:30*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storageBox-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:30*6
	});
	cm.push({
		dataIndex:'storageBox-name',
		hidden : false,
		header:biolims.common.relatedMainTableName,
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/storage/storageBox/showStorageBoxItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.common.storageHistory;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/system/storage/storageBox/delExperimentDnaGetItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				storageBoxItemGrid.getStore().commitChanges();
				storageBoxItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	storageBoxItemGrid=gridEditTable("storageBoxItemdiv",cols,loadParam,opts);
	$("#storageBoxItemdiv").data("storageBoxItemGrid", storageBoxItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
