var storageBoxGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'locationId',
		type:"string"
	});
	    fields.push({
			name:'newLocationId',
			type:"string"
		}); 
	    fields.push({
			name:'oldLocationId',
			type:"string"
		}); 
	    fields.push({
		name:'locationName',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
			name:'storageContainer-id',
			type:"string"
		});
	    fields.push({
			name:'storageContainer-name',
			type:"string"
		});
	    fields.push({
			name:'typeId',
			type:"string"
		});
	    fields.push({
			name:'type',
			type:"string"
		});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'locationId',
		header:biolims.common.storageId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'newLocationId',
		header:biolims.common.newCoordination,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'locationName',
		header:biolims.common.storageName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'storageContainer-id',
		header:biolims.storage.containerId,
		hidden :true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'storageContainer-name',
		header:biolims.storage.container,
		width:20*6,
		sortable:true
	});
	
	var statestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.master.valid], [ '0', biolims.master.invalid ]]
	});
	
	var stateComboxFun = new Ext.form.ComboBox({
		store : statestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(stateComboxFun),				
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/storage/storageBox/showStorageBoxListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.sample.storageBox;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
//	opts.tbar.push({
//		text : "填加明细",
//		handler : null
//	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
//	opts.tbar.push({
//		text : "批量上传(csv文件)",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_upload_div"), "批量上传", null, {
//				"确定" : function() {
//					goInExcel();
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		iconCls : 'add',
		handler : function() {
			var ob = storageBoxGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			p.set("state","1");
			storageBoxGrid.stopEditing();
			storageBoxGrid.getStore().insert(0,p);
			storageBoxGrid.startEditing(0, 0);
		}
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.storageName,
		handler : function(){
			var selRecords = storageBoxGrid.getSelectionModel().getSelections(); 
			if(selRecords.length>0){
				showsp();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});
	opts.tbar.push({
		text : biolims.storage.container,
		handler : function(){
			var selRecords = storageBoxGrid.getSelectionModel().getSelections(); 
			if(selRecords.length>0){
				selectstorageContainerFun();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});
//	 opts.tbar.push({
//			text : "批量替换架子",
//			handler : function() {
//				var options = {};
//				options.width = 400;
//				options.height = 300;
//				loadDialogPage($("#bat_box_div"), "批量替换架子", null, {
//					"Confirm" : function() {
//						var records = storageBoxGrid.getSelectRecord();
//						if (records && records.length > 0) {
//							var storageBox = $("#storageBox").val();
//							storageBoxGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								var str = obj.get("oldLocationId");
//								var proIds = "";
//								if(str!=null &&str!="" && str !=undefined){
//									var strs = str.split("-");
//									if(strs.length==3){
//										proIds = strs[2];
//									}else if(strs.length==4){
//										proIds = strs[3];
//									}else{
//										proIds = str.substr(str.lastIndexOf("-")+1);
//									}
//									obj.set("newLocationId", storageBox+"-"+proIds);
//									
//								}else{
//									message("请先给盒子放置位置！");
//								}
//							});
//							storageBoxGrid.startEditing(0, 0);
//						}
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}
//		});
	storageBoxGrid=gridEditTable("show_storageBox_div",cols,loadParam,opts);
	$("#show_storageBox_div").data("storageBoxGrid", storageBoxGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function add(){
		window.location=window.ctx+'/system/storage/storageBox/editStorageBox.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/storage/storageBox/editStorageBox.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/storage/storageBox/viewStorageBox.action?id=' + id;
}
function exportexcel() {
	storageBoxGrid.title = biolims.common.exportList;
	var vExportContent = storageBoxGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
function save(){	
	var itemJson = commonGetModifyRecords(storageBoxGrid);
	if(itemJson.length>0){
			ajax("post", "/system/storage/storageBox/saveStorageBoxGrid.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					storageBoxGrid.getStore().commitChanges();
					storageBoxGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
	
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(storageBoxGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
function goInExcel(){
	var file = document.getElementById("file-upload").files[0];  
	var reader = new FileReader();  
	//将文件以文本形式读入页面  
	reader.readAsText(file,'GB2312');  
	reader.onload=function(f){  
	var n = 0;	
	var ob = storageBoxGrid.getStore().recordType;
	var csv_data = $.simple_csv(this.result);
    $(csv_data).each(function() {
            	if(n>0){
            		if(this[0]){
            			var p = new ob({});
            			p.isNew = true;	
            			p.set("id",this[0]);
						p.set("name",this[1]);
						p.set("locationId",this[2]);
						p.set("newLocationId",this[3]);
						p.set("locationName",this[4]);
						storageBoxGrid.getStore().insert(0, p);
            		}
            	}
                n = n +1;
            });
		};
     }

//选择容器
function selectstorageContainerFun(){
	var win = Ext.getCmp('storageContainer');
	if (win) {win.close();}
	var storageContainer= new Ext.Window({
	id:'storageContainer',modal:true,title:biolims.storage.selectContainer,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/container/showContainerList.action?flag=storagef' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 storageContainer.close(); }  }]  });    
	storageContainer.show(); }

	function setstoragef(id,name){
		var gridGrid = storageBoxGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('storageContainer-id',id);
			obj.set('storageContainer-name',name);
		});
		var win = Ext.getCmp('storageContainer');
		if(win){
			win.close();
		}
	}
	
	//存储位置
	function showsp(){
		 var win = Ext.getCmp('FrozenLocationFun');
		 if (win) {win.close();}
		 var ProjectFun= new Ext.Window({
		 id:'FrozenLocationFun',modal:true,title:biolims.sample.selectLocation,layout:'fit',width:1000,height:660,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		 html:"<iframe scrolling='yes' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog.action?typeId="+$("#sampleIn_sampleType").val()+"&type=1' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: biolims.common.close,
		  handler: function(){
		  ProjectFun.close(); }  }]  });     ProjectFun.show(); }
	function setFrozenLocationFun(str,type){
		var ids = str.split("-");
		var str1="";
		var strId="";
		if(ids.length==3){
			if(ids[2].length==2){
				strId = ids[2].substr(0,1)+"0"+ids[2].substr(1);
			}else{
				strId = ids[2];
			}
			str1 = ids[0]+"-"+ids[1]+"-"+strId;
		}else if(ids.length==4){
			if(ids[3].length==2){
				strId = ids[3].substr(0,1)+"0"+ids[3].substr(1);
			}else{
				strId = ids[3];
			}
			str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+strId;
		}else if(ids.length==5){
			if(ids[4].length==2){
				strId = ids[4].substr(0,1)+"0"+ids[4].substr(1);
			}else{
				strId = ids[4];
			}
			str1 = ids[0]+"-"+ids[1]+"-"+ids[2]+"-"+ids[3]+"-"+strId;
		}
		var gridGrid = storageBoxGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('newLocationId',str1);
		});
		 var win = Ext.getCmp('FrozenLocationFun')
		 if(win){win.close();}
	 }	