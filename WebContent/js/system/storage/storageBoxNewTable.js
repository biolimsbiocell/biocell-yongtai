var storageBoxTab;
$(function() {
	var options = table(true, "",
			"/system/storage/storageBox/showStorageBoxTableListJson.action", [ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "name",
				"title" : biolims.common.name,
			}, {
				"data" : "newLocationId",
				"title" : biolims.common.newCoordination,
			}, {
				"data" : "locationName",
				"title" : biolims.common.storageName,
			}, {
				"data" : "storageContainer-id",
				"title" :biolims.storage.containerId,
			}, {
				"data" : "storageContainer-name",
				"title" : biolims.storage.container,
			}, {
				"data" : "state",
				"title" : biolims.common.state,
			} ], null)
			
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#storageBoxTablediv"));
		}
	});
	tbarOpts.push({
		text: biolims.storage.container,
		action: function() {
			selectContainer();
		}
	});
	
	
	tbarOpts.push({
		text : biolims.common.save,
		action : function() {
			saveStorageBoxTableTab();
		}
	})
	storageBoxTab = renderData($("#storageBoxTablediv"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(storageBoxTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/sample/storage/storageBox/toEditstorageBox.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/sample/storage/storageBox/toEditstorageBox.action?id=' + id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/dna/dnaTask/showDnaTaskSteps.action?id=" + id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" :biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" :biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.common.newCoordination,
		"type" : "input",
		"searchName" : "newLocationId",
	}, {
		"txt" : biolims.storageBox.applyUserId,
		"type" : "input",
		"searchName" : "locationName",
	}, {
		"txt" : biolims.storage.containerId,
		"type" : "input",
		"searchName" : "storageContainer-id",
	},, {
		"txt" : biolims.storage.container,
		"type" : "input",
		"searchName" : "storageContainer-name",
	}, {
		"txt" : biolims.common.state,
		"type" : "input",
		"searchName" : "state",
	},{
		"type" : "table",
		"table" : storageBoxTab
	} ];
}
