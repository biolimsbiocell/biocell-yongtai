//样本出库申请左侧待出库样本
var moveBoxTab;
var storageBox_id = $("#storageBox_id").val();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" :biolims.user.itemNo,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" :biolims.common.name,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data" : "locationId",
		"title" :biolims.common.storageId,
		"createdCell" : function(td) {
			$(td).attr("saveName", "locationId");
		}
	});
	colOpts.push({
		"data" : "locationName",
		"title" : biolims.common.storageNote,
		"createdCell" : function(td) {
			$(td).attr("saveName", "locationName");
		}
	});
	colOpts.push({
		"data" : "changeUser",
		"title" : biolims.common.modifiedBy,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "changeUser");
		},
	});

	colOpts.push({
		"data" : "changeDate",
		"title" : biolims.common.updateDate,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "changeDate");
		},
	});
	colOpts.push({
		"data" : "state",
		"title" : biolims.common.state,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "state");
		},
	});
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "note");
		},
	});
	

	colOpts.push({
		"data" : "storageBox-id",
		"title" : biolims.common.relatedMainTableId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "storageBox-id");
		},
	});
	colOpts.push({
		"data" : "storageBox-name",
		"title" : biolims.common.relatedMainTableName,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "storageBox-name");
		},
	});
	
	var tbarOpts = [];
//	tbarOpts.push({
//		text : '<i class="fa fa-ioxhost"></i>' + biolims.common.search,
//		action : function() {
//			search()
//		}
//	});
	var moveBoxOps = table(true, storageBox_id,
			"/system/storage/storageBox/showStorageBoxItemTableListJson.action",
			colOpts, tbarOpts);
	moveBoxTab = renderData($("#moveBoxdiv"),
			moveBoxOps);

	// 选择数据并提示
	moveBoxTab.on('draw', function() {
		var index = 0;
		$("#moveBoxdiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
});

// 弹框模糊查询参数
//function searchOptions() {
//	return [  {
//		"txt" : biolims.common.code,
//		"type" : "input",
//		"searchName" : "code",
//	}, {
//		"txt" : biolims.common.sampleCode,
//		"type" : "input",
//		"searchName" :"sampleCode",
//	}, {
//		"txt" : biolims.common.throughput,
//		"type" : "input",
//		"searchName" : "dataTraffic",
//	}, {
//		"txt" : biolims.common.productId,
//		"type" : "input",
//		"searchName" : "productId",
//	}, {
//		"txt" :biolims.common.testProject,
//		"type" : "input",
//		"searchName" : "productName",
//	}, {
//		"txt" : biolims.sample.samplingDate,
//		"type" : "input",
//		"searchName" : "samplingDate",
//	}, {
//		"txt" : biolims.common.location,
//		"type" : "input",
//		"searchName" : "location",
//	}, {
//		"txt" : biolims.common.sampleType,
//		"type" : "input",
//		"searchName" : "sampleType",
//	}, {
//		"txt" : biolims.QPCR.concentration,
//		"type" : "input",
//		"searchName" : "qpcrConcentration",
//	}, {
//		"txt" : 'index',
//		"type" : "input",
//		"searchName" : "indexa",	
//	}, {
//		"txt" : biolims.common.barCode,
//		"type" : "input",
//		"searchName" : "concentration",
//	}, {
//		"txt" : biolims.common.concentration,
//		"type" : "input",
//		"searchName" : "barCode",
//	}, {
//		"txt" :biolims.common.volume,
//		"type" : "input",
//		"searchName" : "volume",
//	}, {
//		"txt" :biolims.common.sumNum,
//		"type" : "input",
//		"searchName" : "sumTotal",
//	}, {
//		"txt" :biolims.sample.applyUserId,
//		"type" : "input",
//		"searchName" : "applyUser-id",
//	}, {
//		"txt" :biolims.sample.applyUserName,
//		"type" : "input",
//		"searchName" : "applyUser-name",
//	}, {
//		"txt" :biolims.common.orderId,
//		"type" : "input",
//		"searchName" : "taskId",
//	},{
//		"type" : "table",
//		"table" : moveBoxTab
//	} ];
//}


$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

