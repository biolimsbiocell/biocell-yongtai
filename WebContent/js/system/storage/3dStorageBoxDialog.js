$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编码",
	});
	colOpts.push({
		"data": "name",
		"title": "描述",
	});
	colOpts.push({
		"data": "storageContainer-name",
		"title": "容器名称",
	});
	colOpts.push({
		"data": "storageContainer-rowNum",
		"title": "行数",
	});
	colOpts.push({
		"data": "storageContainer-colNum",
		"title": "列数",
	});
	var tbarOpts = [];
	var addOptions = table(true, null,
		'/storage/newPosition/selNewBox.action',
		colOpts, tbarOpts)
	var storageBoxDialog = renderData($("#3dStorageBoxDialog"), addOptions);
	$("#3dStorageBoxDialog").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#3dStorageBoxDialog_wrapper .dt-buttons").empty();
			$('#3dStorageBoxDialog_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#3dStorageBoxDialog tbody tr");
			storageBoxDialog.ajax.reload();
//			storageBoxDialog.on('draw', function() {
//				trs = $("#3dStorageBoxDialog tbody tr");
//				trs.click(function() {
//					$(this).addClass("chosed").siblings("tr")
//						.removeClass("chosed");
//				});
//			});
//			trs.click(function() {
//				$(this).addClass("chosed").siblings("tr")
//					.removeClass("chosed");
//			});
		});
})