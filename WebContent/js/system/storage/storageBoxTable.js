//样本出库申请左侧待出库样本
var storageBoxTableTab,oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" :biolims.common.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" :biolims.common.name,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data" : "newLocationId",
		"title" :biolims.common.newCoordination,
		"createdCell" : function(td) {
			$(td).attr("saveName", "newLocationId");
		}
	});
	colOpts.push({
		"data" : "locationName",
		"title" : biolims.common.storageName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "locationName");
		}
	});
	colOpts.push({
		"data" : "storageContainer-id",
		"title" : biolims.storage.containerId,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "storageContainer-id");
		},
	});

	colOpts.push({
		"data" : "storageContainer-name",
		"title" : biolims.storage.container,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "storageContainer-name");
		},
	});
	colOpts.push({
		"data" : "state",
		"title" : biolims.common.state,
		"createdCell" : function(td, data) {
			$(td).attr("saveName", "state");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "新建";
			}
			if (data == "0") {
				return "完成";
			} else {
				return '';
			}
		}
		
	});
	
	
	var tbarOpts = [];
	tbarOpts.push({
		text : '<i class="fa fa-ioxhost"></i>' + biolims.common.search,
		action : function() {
			search()
		}
	});
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#storageBoxTablediv"));
		}
	});
	tbarOpts.push({
		text: biolims.storage.container,
		action: function() {
			selectContainer();
		}
	});
	
	tbarOpts.push({
		text : biolims.common.edit,
		action : function() {
			var rows = $("#storageBoxTablediv .selected");
			if(rows.length!=1){
				top.layer.msg("请选择一条记录");
				return false;
			}
			var id = rows.find("td[savename='id']").text();
			if (id == "" || id == undefined) {
				top.layer.msg(biolims.common.selectRecord);
				return false;
			}
			window.location = window.ctx
					+ '/system/storage/storageBox/editStorageBoxTable.action?id=' + id;
}
	})
	
	tbarOpts.push({
		text : biolims.common.save,
		action : function() {
			saveStorageBoxTableTab();
		}
	})

	var storageBoxTableOps = table(true, "",
			"/system/storage/storageBox/showStorageBoxTableListJson.action",
			colOpts, tbarOpts);
	storageBoxTableTab = renderData($("#storageBoxTablediv"),
			storageBoxTableOps);

	// 选择数据并提示
	storageBoxTableTab.on('draw', function() {
		var index = 0;
		$("#storageBoxTablediv .icheck").on(
				'ifChanged',
				function(event) {
					if ($(this).is(':checked')) {
						index++;
						$("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "2px solid #000");
					} else {
						var tt = $("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "1px solid gainsboro");
						index--;
					}
					top.layer.msg(biolims.common.Youselect + index
							+ biolims.common.data);
				});
		oldChangeLog = storageBoxTableTab.ajax.json();
	
	});
});


function selectContainer(){

	var rows = $("#storageBoxTablediv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	

	top.layer.open({
	title:biolims.storage.selectContainer,
	type:2,
	area:[document.body.clientWidth-300,document.body.clientHeight-100],
	btn: biolims.common.selected,
	content:[window.ctx+"/storage/container/showContainerTable.action",''],
	yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addContainer .chosed").children("td").eq(
			0).text();
		top.layer.close(index)
		rows.addClass("editagain");
		rows.find("td[savename='storageContainer-name']").attr("storageContainer-id", id).text(name);
		rows.find("td[savename='storageContainer-id']").text(id);
	},
})

}

//保存
function saveStorageBoxTableTab() {
	var ele = $("#storageBoxTablediv");
	var changeLog = "存储盒子:";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type : 'post',
		url : '/system/storage/storageBox/saveStorageBoxTableGrid.action',
		data : {
			dataJson : data,
			logInfo : changeLog
		},
		success : function(data) {
			var data = JSON.parse(data)
			if (data.success) {
				top.layer.closeAll();
				var url = "/system/storage/storageBox/showStorageBoxTableList.action";
				window.location.href = url;
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			}
			;
		}
	})
}

//获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for ( var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
			// if(k == "dicSampleTypeName") {
			// json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
			// continue;
			// }
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += 'ID为"' + id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

// 弹框模糊查询参数
function searchOptions() {
	return [  {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" :"name",
	}/*, {
		"txt" : biolims.common.storageId,
		"type" : "input",
		"searchName" : "locationId",
	}*/, {
		"txt" : biolims.common.newCoordination,
		"type" : "input",
		"searchName" : "newLocationId",
	}, {
		"txt" :biolims.common.storageName,
		"type" : "input",
		"searchName" : "locationName",
	}, {
		"txt" : biolims.storage.containerId,
		"type" : "input",
		"searchName" : "storageContainer-id",
	}, {
		"txt" : biolims.storage.container,
		"type" : "input",
		"searchName" : "storageContainer-name",
	}, {
		"txt" :biolims.common.state,
		"type" : "input",
		"searchName" : "state",
	},{
		"type" : "table",
		"table" : storageBoxTableTab
	} ];
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};






