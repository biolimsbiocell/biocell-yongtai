var expressCompanyTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.sysExpressCompany.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.sysExpressCompany.name,
	});
	
	    fields.push({
		"data":"phone",
		"title":biolims.sysExpressCompany.phone,
	});
	
	    fields.push({
		"data":"address",
		"title":biolims.sysExpressCompany.adress,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.sysExpressCompany.state,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.master.valid;
			}
			if(data == "0") {
				return biolims.master.invalid;
			} else {
				return "";
			}
		}
	});
	
	    fields.push({
		"data":"printPath",
		"title":biolims.sysExpressCompany.printPath,
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "ExpressCompany"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
				}
			}
		});

	var options = table(true, "","/system/express/expressCompany/showExpressCompanyTableJson.action",
	 fields, null)
	expressCompanyTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(expressCompanyTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/system/express/expressCompany/editExpressCompany.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/express/expressCompany/editExpressCompany.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/express/expressCompany/viewExpressCompany.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.sysExpressCompany.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.sysExpressCompany.name
		});
	   fields.push({
		    "searchName":"phone",
			"type":"input",
			"txt":biolims.sysExpressCompany.phone
		});
	   fields.push({
		    "searchName":"address",
			"type":"input",
			"txt":biolims.sysExpressCompany.adress
		});
	   fields.push({
		    "searchName":"state",
			"type": "select",
			"options": biolims.master.valid+"|"+biolims.master.invalid,
			"changeOpt": "1|0",
			"txt":biolims.sysExpressCompany.state
		});
	   fields.push({
		    "searchName":"printPath",
			"type":"input",
			"txt":biolims.sysExpressCompany.printPath
		});
	
	fields.push({
		"type":"table",
		"table":expressCompanyTable
	});
	return fields;
}
