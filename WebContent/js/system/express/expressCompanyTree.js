$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : biolims.sysExpressCompany.id,
		field : "id"
	});
	
	   
	fields.push({
		title : biolims.sysExpressCompany.name,
		field : "name"
	});
	
	   
	fields.push({
		title : biolims.sysExpressCompany.phone,,
		field : "phone"
	});
	
	   
	fields.push({
		title : biolims.sysExpressCompany.adress,,
		field : "address"
	});
	
	   
	fields.push({
		title : biolims.sysExpressCompany.state,
		field : "state"
	});
	
	   
	fields.push({
		title : biolims.sysExpressCompany.printPath,
		field : "printPath"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/system/express/expressCompany/showExpressCompanyListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/system/express/expressCompany/editExpressCompany.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/express/expressCompany/editExpressCompany.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/system/express/expressCompany/viewExpressCompany.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : biolims.sysExpressCompany.id,,
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.sysExpressCompany.name,
		"searchName" : 'name',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.sysExpressCompany.phone,,
		"searchName" : 'phone',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.sysExpressCompany.adress,,
		"searchName" : 'address',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.sysExpressCompany.state,,
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : biolims.sysExpressCompany.printPath,
		"searchName" : 'printPath',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

