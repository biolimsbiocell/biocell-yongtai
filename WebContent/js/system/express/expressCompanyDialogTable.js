$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编码",
	});
	colOpts.push({
		"data": "name",
		"title": "快递公司",
	});
	var tbarOpts = [];
	var expressCompanyOptions = table(false, null,
			'/system/express/expressCompany/showDialogExpressCompanyJson.action',colOpts , tbarOpts)
		var expressCompanyTable = renderData($("#addExpressCompany"), expressCompanyOptions);
	$("#addExpressCompany").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addExpressCompany_wrapper .dt-buttons").empty();
		$('#addExpressCompany_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addExpressCompany tbody tr");
	expressCompanyTable.ajax.reload();
	expressCompanyTable.on('draw', function() {
		trs = $("#addExpressCompany tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

