var expressCompanyTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.sysExpressCompany.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.sysExpressCompany.name,
	});
	
	    fields.push({
		"data":"phone",
		"title":biolims.sysExpressCompany.phone,
	});
	
	    fields.push({
		"data":"address",
		"title":biolims.sysExpressCompany.adress,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.sysExpressCompany.state,
	});
	
	    fields.push({
		"data":"printPath",
		"title":biolims.sysExpressCompany.printPath,
	});
	
	var options = table(false, "","/system/express/expressCompany/showDialogExpressCompanyJson.action",
	 fields, null)
	expressCompanyTable = renderData($("#addExpressCompanyTable"), options);
	$("#addExpressCompanyTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addExpressCompanyTable_wrapper .dt-buttons").empty();
				$('#addExpressCompanyTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addExpressCompanyTable tbody tr");
			expressCompanyTable.ajax.reload();
			expressCompanyTable.on('draw', function() {
				trs = $("#addExpressCompanyTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
});
