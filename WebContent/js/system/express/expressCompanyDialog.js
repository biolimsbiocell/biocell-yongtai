var expressCompanyDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'fileInfo-id',
		type:"string"
	});
	    fields.push({
		name:'fileInfo-name',
		type:"string"
	});
	    fields.push({
		name:'address',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'fileInfo-id',
//		header:biolims.common.templateId,
//		width:15*10,
//		sortable:true
//	});
//	cm.push({
//		dataIndex:'fileInfo-name',
//		header:biolims.common.templateName,
//		width:20*6,
//		sortable:true
//	});
	cm.push({
		dataIndex:'address',
		header:biolims.common.addr,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/express/expressCompany/showExpressCompanyListJson.action";
	var opts={};
	opts.title=biolims.sample.companyName;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setExpressCompanyFun(rec);
	};
	expressCompanyDialogGrid=gridTable("show_dialog_expressCompany_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(expressCompanyDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}


function xj(){
	window.open(window.ctx + '/system/express/expressCompany/editExpressCompany.action', '', '', '');
}
