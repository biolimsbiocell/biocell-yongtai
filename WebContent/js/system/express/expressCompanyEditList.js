var expressCompanyTable;
var oldexpressCompanyChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.sysExpressCompany.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.sysExpressCompany.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"phone",
		"title": biolims.sysExpressCompany.phone,
		"createdCell": function(td) {
			$(td).attr("saveName", "phone");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"address",
		"title": biolims.sysExpressCompany.adress,
		"createdCell": function(td) {
			$(td).attr("saveName", "address");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.sysExpressCompany.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"printPath",
		"title": biolims.sysExpressCompany.printPath,
		"createdCell": function(td) {
			$(td).attr("saveName", "printPath");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#expressCompanyTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#expressCompanyTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#expressCompanyTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#expressCompany_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/system/express/expressCompany/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 expressCompanyTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveExpressCompany($("#expressCompanyTable"));
		}
	});
	}
	
	var expressCompanyOptions = 
	table(true, "","/system/express/expressCompany/showExpressCompanyTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	expressCompanyTable = renderData($("#expressCompanyTable"), expressCompanyOptions);
	expressCompanyTable.on('draw', function() {
		oldexpressCompanyChangeLog = expressCompanyTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveExpressCompany(ele) {
	var data = saveExpressCompanyjson(ele);
	var ele=$("#expressCompanyTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/system/express/expressCompany/saveExpressCompanyTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveExpressCompanyjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldexpressCompanyChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
