var projectCheckGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		sortable:true
	});
	var state = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.master.valid
			}, {
				id : '0',
				name : biolims.master.invalid
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(state),
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/projectcheck/projectCheck/showProjectCheckListJson.action";
	var opts={};
	opts.title=biolims.common.projectCheck;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	projectCheckGrid=gridTable("show_projectCheck_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/system/projectcheck/projectCheck/editProjectCheck.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/projectcheck/projectCheck/editProjectCheck.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/projectcheck/projectCheck/viewProjectCheck.action?id=' + id;
}
function exportexcel() {
	projectCheckGrid.title = biolims.common.exportList;
	var vExportContent = projectCheckGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"),biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(projectCheckGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
