﻿var projectCheckItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'projectCheck-id',
		type:"string"
	});
	    fields.push({
		name:'projectCheck-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:50*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.sample.note,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'projectCheck-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'projectCheck-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/projectcheck/projectCheck/showProjectCheckItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.projectCheckDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/system/projectcheck/projectCheck/delProjectCheckItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				projectCheckItemGrid.getStore().commitChanges();
				projectCheckItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	opts.tbar.push({
			text : biolims.common.selectprojectCheckFun,
			handler : selectprojectCheckFun
		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = projectCheckItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							projectCheckItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	projectCheckItemGrid=gridEditTable("projectCheckItemdiv",cols,loadParam,opts);
	$("#projectCheckItemdiv").data("projectCheckItemGrid", projectCheckItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectinstrumentFun(){
	var win = Ext.getCmp('selectinstrument');
	if (win) {win.close();}
	var selectinstrument= new Ext.Window({
	id:'selectinstrument',modal:true,title:biolims.common.selectInstrument,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/instrument/instrumentSelect.action?flag=InstrumentFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectinstrument.close(); }  }]  });     selectinstrument.show(); }
function setInstrumentFun(rec){
	var gridGrid = $("#cosItemdiv").data("cosItemGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	if(selRecords.length>0){
		$.each(selRecords, function(i, obj) {
			obj.set('instrument-id',rec.get('id'));
			obj.set('instrument-note',rec.get('note'));
		});
	}else{
		message(biolims.common.selectAtLeastRecord);
	}
	var win = Ext.getCmp('selectinstrument');
	if(win){
		win.close();
	}
}
function selectprojectCheckFun(){
	var win = Ext.getCmp('selectprojectCheck');
	if (win) {win.close();}
	var selectprojectCheck= new Ext.Window({
	id:'selectprojectCheck',modal:true,title:biolims.common.selectprojectCheckFun,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeFullRecSelect.action?flag=checked' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectprojectCheck.close(); }  }]  });     selectprojectCheck.show(); }
	function setchecked(rec){
		var gridGrid = $("#projectCheckItemdiv").data("projectCheckItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('note',rec.get("note"));
			obj.set('name',rec.get("name"));
		});
		var win = Ext.getCmp('selectprojectCheck')
		if(win){
			win.close();
		}
	}
	
