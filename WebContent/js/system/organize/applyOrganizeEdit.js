﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/system/organize/applyOrganize/editApplyOrganize.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/system/organize/applyOrganize/showApplyOrganizeList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var flag=true;
	var reg = /^((\+?86)|(\(\+86\)))?1\d{10}$/;
	//var reg=/^(\+)?(86)?0?1\d{10}$/;
	for(var i=0;i<applyOrganizeAddressGrid.store.getCount();i++){
		if(applyOrganizeAddressGrid.store.getAt(i).get("transTime")!="" &&applyOrganizeAddressGrid.store.getAt(i).get("transTime")!=null &&applyOrganizeAddressGrid.store.getAt(i).get("transTime")!=undefined ){
			flag=true;
		}else{
			flag=false;
			message(biolims.common.transTimeIsEmpty);
			break;
		}
		if(reg.test(applyOrganizeAddressGrid.store.getAt(i).get("phone"))){
			flag=true;
		}else{
			flag=false;
			message(biolims.sample.pleaseInputPhone);
			break;
		}
	}
	if(flag){
		save();
	}
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#applyOrganize", {
					userId : userId,
					userName : userName,
					formId : $("#applyOrganize_id").val(),
					title : $("#applyOrganize_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#applyOrganize_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});





function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var applyOrganizeAddressDivData = $("#applyOrganizeAddressdiv").data("applyOrganizeAddressGrid");
		document.getElementById('applyOrganizeAddressJson').value = commonGetModifyRecords(applyOrganizeAddressDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/system/organize/applyOrganize/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/system/organize/applyOrganize/copyApplyOrganize.action?id=' + $("#applyOrganize_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#applyOrganize_id").val() + "&tableId=applyOrganize");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#applyOrganize_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#applyOrganize_person").val());
	nsc.push(biolims.master.personNameIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.provinces,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/system/organize/applyOrganize/showApplyOrganizeAddressList.action", {
				id : $("#applyOrganize_id").val()
			}, "#applyOrganizeAddresspage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	function selectMark(){
		 var win = Ext.getCmp('selectMark');
		 if (win) {win.close();}
		 var selectMark = new Ext.Window({
		 id:'selectMark',modal:true,title:biolims.common.selectMarkCodeName,layout:'fit',width:500,height:500,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=markCode' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: biolims.common.close,
		  handler: function(){
			  selectMark.close(); }  }]  });     selectMark.show(); }
		  function setmarkCode(id,name){
			 document.getElementById('applyOrganize_markCode').value=id;
			 document.getElementById('applyOrganize_markCode_name').value=name;
			 var win = Ext.getCmp('selectMark');
			 if(win){win.close();}
		 };