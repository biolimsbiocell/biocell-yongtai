﻿
var officeApplyItemGrid;

$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
   fields.push({
		name:'packCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		   name:'state',
		   type:"string"
	   });
	    fields.push({
		name:'officeApply-id',
		type:"string"
	});
	    fields.push({
		name:'officeApply-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.id,
		width:30*6
	});
	cm.push({
		dataIndex:'packCode',
		hidden : false,
		header:biolims.master.materialPackageId,
		width:30*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.designation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'officeApply-id',
		hidden : true,
		header:biolims.master.relatedTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'officeApply-name',
		hidden : true,
		header:biolims.master.relatedTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/organize/officeApply/showOfficeApplyItemListJson.action?flag="+$("#flag").val()+"&id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.applicationDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	if($("#officeApply_stateName").val()==biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/system/organize/officeApplyItem/delOfficeApplyItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				officeApplyItemGrid.getStore().commitChanges();
				officeApplyItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	/*opts.tbar.push({
			text : '选择相关表id',
			handler : selectofficeApplyFun
		});*/
	
	
	
	

	
	
	
	/*opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});*/
	//======
	if($("#flag").val()!="101"){
	opts.tbar.push({
		text : biolims.common.reusableSamples,
		handler : function() {
			var options = {};
			options.width = 900;
			options.height = 460;
			var url="/goods/mate/goodsMaterialsReady/goodsMaterialsReadyDeSelect.action?flag="+"100";
			loadDialogPage(null, biolims.common.selectedDetail, url, {
				"Confirm": function() {
					 var ids=goodsMaterialsReadyDetailsGrid1.getSelectionModel();
					 var simpleInfo=officeApplyItemGrid.store;
					 if(ids.getSelections().length > 0){
						 $.each(ids.getSelections(), function(i, obj) {
							 var isRepeat = false;
								for(var j=0;j<simpleInfo.getCount();j++){
									var oldv = simpleInfo.getAt(j).get("code");
									if(oldv == obj.get("code")){
										isRepeat = true;
										message(biolims.common.haveDuplicate);
										break;
									}
								}
							 if(!isRepeat){
								 var ob=officeApplyItemGrid.getStore().recordType;
								 officeApplyItemGrid.stopEditing();
								 var p=new ob({});
								p.isNew=true;
								p.set("code",obj.get("code"));
								p.set("packCode",obj.get("packCode"));
								officeApplyItemGrid.getStore().add(p);
								officeApplyItemGrid.startEditing(0,0);
							 }
						 });
					 }else{
								message(biolims.common.pleaseSelect);
								return;
					 }
					 options.close();
				}
			}, true, options);
		}
	});
	}
//====
	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = officeApplyItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							officeApplyItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
	officeApplyItemGrid=gridEditTable("officeApplyItemdiv",cols,loadParam,opts);
	$("#officeApplyItemdiv").data("officeApplyItemGrid", officeApplyItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectofficeApplyFun(){
	var win = Ext.getCmp('selectofficeApply');
	if (win) {win.close();}
	var selectofficeApply= new Ext.Window({
	id:'selectofficeApply',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/OfficeApplySelect.action?flag=officeApply' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectofficeApply.close(); }  }]  });     selectofficeApply.show(); }
	function setofficeApply(id,name){
		var gridGrid = $("#officeApplyItemdiv").data("officeApplyItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('officeApply-id',id);
			obj.set('officeApply-name',name);
		});
		var win = Ext.getCmp('selectofficeApply')
		if(win){
			win.close();
		}
	}
	
