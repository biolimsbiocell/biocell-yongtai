var applyOrganizeDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'person-id',
		type:"string"
	});
	    fields.push({
		name:'person-name',
		type:"string"
	});
	    fields.push({
			name:'markCode-id',
			type:"string"
		});
		    fields.push({
			name:'markCode-name',
			type:"string"
		});
	    fields.push({
		name:'dept',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'person-id',
		header:biolims.master.personId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'person-name',
		header:biolims.master.personName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'markCode-id',
		header:biolims.common.markCodeId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'markCode-name',
		header:biolims.common.markCodeName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'dept',
		header:biolims.common.dept,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/organize/applyOrganize/showApplyOrganizeListJson.action";
	var opts={};
	opts.title=biolims.common.provinces;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setApplyOrganizeFun(rec);
	};
	applyOrganizeDialogGrid=gridTable("show_dialog_applyOrganize_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(applyOrganizeDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
