var applyOrganizeGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'person-id',
		type:"string"
	});
	    fields.push({
		name:'person-name',
		type:"string"
	});
    fields.push({
		name:'markCode-id',
		type:"string"
	});
    fields.push({
		name:'markCode-name',
		type:"string"
	});
	    fields.push({
		name:'dept',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'person-id',
		hidden:true,
		header:biolims.master.personId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'person-name',
		header:biolims.master.personName,
		
		width:15*10,
		sortable:true
		});
		cm.push({
			dataIndex:'markCode-name',
			hidden:true,
			header:biolims.common.markCodeName,
			width:15*10,
			sortable:true
			});
			cm.push({
			dataIndex:'markCode-id',
			header:biolims.common.markCodeId,
			width:15*10,
			sortable:true
			});
	cm.push({
		dataIndex:'dept',
		header:biolims.common.dept,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/organize/applyOrganize/showApplyOrganizeListJson.action";
	var opts={};
	opts.title=biolims.common.provinces;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	applyOrganizeGrid=gridTable("show_applyOrganize_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/system/organize/applyOrganize/editApplyOrganize.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/organize/applyOrganize/editApplyOrganize.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/organize/applyOrganize/viewApplyOrganize.action?id=' + id;
}
function exportexcel() {
	applyOrganizeGrid.title = biolims.common.exportList;
	var vExportContent = applyOrganizeGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(applyOrganizeGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
