﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/system/organize/officeApply/editOfficeApply.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/system/organize/officeApply/showOfficeApplyList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#officeApply", {
					userId : userId,
					userName : userName,
					formId : $("#officeApply_id").val(),
					title : $("#officeApply_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#officeApply_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var officeApplyItemDivData = $("#officeApplyItemdiv").data("officeApplyItemGrid");
		document.getElementById('officeApplyItemJson').value = commonGetModifyRecords(officeApplyItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/system/organize/officeApply/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/system/organize/officeApply/copyOfficeApply.action?id=' + $("#officeApply_id").val();
}
/*function changeState() {
	commonChangeState("formId=" + $("#officeApply_id").val() + "&tableId=officeApply");
}*/
$("#toolbarbutton_status").click(function(){
	if ($("#officeApply_id").val()){
		commonChangeState("formId=" + $("#officeApply_id").val() + "&tableId=OfficeApply");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#officeApply_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.officeApplication,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/system/organize/officeApply/showOfficeApplyItemList.action", {
				id : $("#officeApply_id").val()
			}, "#officeApplyItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);