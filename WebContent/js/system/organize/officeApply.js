var officeApplyGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'statename',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.designation,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:biolims.wk.approverId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.wk.approver,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.wk.approverDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'statename',
		header:biolims.common.stateName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/organize/officeApply/showOfficeApplyListJson.action";
	var opts={};
	opts.title=biolims.common.officeApplication;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	officeApplyGrid=gridTable("show_officeApply_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/system/organize/officeApply/editOfficeApply.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/organize/officeApply/editOfficeApply.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/organize/officeApply/viewOfficeApply.action?id=' + id;
}
function exportexcel() {
	officeApplyGrid.title = biolims.common.exportList;
	var vExportContent = officeApplyGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startacceptDate").val() != undefined) && ($("#startacceptDate").val() != '')) {
					var startacceptDatestr = ">=##@@##" + $("#startacceptDate").val();
					$("#acceptDate1").val(startacceptDatestr);
				}
				if (($("#endacceptDate").val() != undefined) && ($("#endacceptDate").val() != '')) {
					var endacceptDatestr = "<=##@@##" + $("#endacceptDate").val();

					$("#acceptDate2").val(endacceptDatestr);

				}
				
				
				commonSearchAction(officeApplyGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
