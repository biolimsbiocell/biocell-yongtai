$(function(){

	Ext.onReady(function() {
			Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";

			var tree = new Ext.tree.TreePanel({
				renderTo : 'markup',
				title : biolims.sample.unusualName,
				height : document.body.clientHeight - 20,
				width : 500,
				useArrows : true,
				checked:true,
				autoScroll : true,
				animate : true,
				enableDD : true,
				containerScroll : true,
				rootVisible : false,
				root : {
					nodeType : 'async',
					checked:true
				},
				sorters : [ {
					property : 'id',
					direction : 'ASC'
				}, {
					property : 'text',
					direction : 'ASC'
				}  ],
				dataUrl :$("#abnomalTypeTreePath").val(),

				listeners : {
					'checkchange' : function(node, checked) {
						if (checked) {
							if (node.leaf == false) {

								var childnodes = node.childNodes;
								Ext.each(childnodes, function() { // 从节点中取出子节点依次遍历
									var nd = this;

									if (nd.hasChildNodes()) { // 判断子节点下是否存在子节点
										findchildnode(nd); // 如果存在子节点 递归

									} else {

										nd.ui.toggleCheck(checked);
										nd.attributes.checked = checked;

									}
								});

							}
						} else {

							if (node.leaf == false) {

								var childnodes = node.childNodes;
								Ext.each(childnodes, function() { // 从节点中取出子节点依次遍历
									var nd = this;

									if (nd.hasChildNodes()) { // 判断子节点下是否存在子节点
										findchildnode(nd); // 如果存在子节点 递归
									} else {

										nd.ui.toggleCheck(checked);
										nd.attributes.checked = checked;

									}
								});

							}

						}
					}
				},
				buttons : [
				{

					id : 'saveAllModify',
					text : biolims.common.select,
					handler : function() {
						var msgId = '',msgName='', selNodes = tree.getChecked();
						Ext.each(selNodes, function(node) {
							if (msgId != '') {
								msgId += ', ';
								msgName+= ', ';
								
							}
							if(!node.attributes.text){
								return;
							}else{
								msgId +=  node.id ;
								msgName +=  node.text ;
							}
						});
						
						setvalue(msgId,msgName);
					}
				}
			  ]
			});
//			tree.on("click", function(node) {
	//
//				if (node.attributes.roleRightsId == undefined) {
	//
//					return false;
//				}
//			});

			tree.getRootNode().expand(true);
		});
		
});




//Ext.onReady(function(){
//	Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
//		Ext.QuickTips.init();
//		function reloadtree() {
//			var node = treeGrid.getSelectionModel().getSelectedNode();
//			if(node==null){
//				treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
//					treeGrid.getRootNode().expand(true);
//				}, this);
//			}else{
//				var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
//				treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
//					treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
//						treeGrid.getSelectionModel().select(oLastNode);
//					});
//				}, this);
//			}
//		}
//		col = [ 
//		
//   
//{
//	header : '编号',
//	dataIndex : 'id',
//	width:20*6
//}, 
//
//   
//{
//	header : '描述',
//	dataIndex : 'name',
//	width:50*6,
//	hidden:false
//}, 
//
//    {
//	header : '父级',
//	dataIndex : 'parent-name',
//	width:50*6,
//	hidden:false
//}, 
//   
//{
//	header : '状态id',
//	dataIndex : 'state',
//	width:20*6,
//	hidden:true
//}, 
//
//   
//{
//	header : '工作流状态',
//	dataIndex : 'stateName',
//	width:20*6,
//	hidden:false
//}, 
//		{
//			header : '上级编码',
//			width : 160,
//			dataIndex : 'upId'
//		}];
//		var tbl = [];
//		var treeGrid = new Ext.ux.tree.TreeGrid({
//			id:'treeGrid',
//			width:parent.document.body.clientWidth-50,
//			height: parent.document.body.clientHeight-80,
//			renderTo: 'markup',
//			enableDD: true,
//		
//			columnLines:true,
//			columns:col,
//			root:new Ext.tree.AsyncTreeNode({  
//	            id:'0',  
//	            loader:new Ext.tree.TreeLoader({  
//	                 dataUrl: $("#abnomalTypeTreePath").val(),  
//	                 listeners:{  
//	                     "beforeload":function(treeloader,node)  
//	                     {  
//	                        treeloader.baseParams={  
//	                        treegrid_id:node.id,  
//	                        method:'POST'  
//	                        };  
//	                     }  
//	                 }    
//	            })  
//	        }),  
//            listeners: {
//            	dblclick: function(n) {
//                setvalue(n.attributes.id,n.attributes.name);
//            }
//        }
//	 });
//		Ext.getCmp('treeGrid').getRootNode().expand(true);
//
//});
        function searchSubmit(){
        	form1.action =  window.ctx+"/system/abnomal/abnomalType/showAbnomalTypeTreeSelect.action";
        	form1.submit();    
        }