var abnomalTypeGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
			name:'method',
			type:"string"
		});
	    fields.push({
		name:'parent-id',
		type:"string"
	});
	    fields.push({
		name:'parent-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'parent-id',
		hidden:true,
		header:biolims.master.parentId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'parent-name',
		header : biolims.master.parentName,
		
		width:50*10,
		sortable:true
		});
		var storemethodCob = new Ext.data.ArrayStore({
			fields : [ 'id', 'name' ],
			data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.refuse2Accept], [ '2', biolims.common.concessions2Receive]]
		});
		var methodCob = new Ext.form.ComboBox({
			store : storemethodCob,
			displayField : 'name',
			valueField : 'id',
			mode : 'local'
		});
		cm.push({
			dataIndex:'method',
			hidden : false,
			header:biolims.common.method,
			width:20*6,
			editor : methodCob,
			renderer : Ext.util.Format.comboRenderer(methodCob)
		});
		var storestateCob = new Ext.data.ArrayStore({
			fields : [ 'id', 'name' ],
			data : [ [ '1', biolims.master.valid ], [ '0', biolims.master.invalid ]]
		});
		var stateCob = new Ext.form.ComboBox({
			store : storestateCob,
			displayField : 'name',
			valueField : 'id',
			mode : 'local'
		});
		cm.push({
			dataIndex:'state',
			hidden : false,
			header:biolims.common.state,
			width:20*6,
			editor : stateCob,
			renderer : Ext.util.Format.comboRenderer(stateCob)
		});
//	cm.push({
//		dataIndex:'stateName',
//		header:'工作流状态',
//		width:20*6,
//		
//		sortable:true
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/abnomal/abnomalType/showAbnomalTypeListJson.action";
	var opts={};
	opts.title=biolims.sample.unusualName;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	abnomalTypeGrid=gridTable("show_abnomalType_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/system/abnomal/abnomalType/editAbnomalType.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/abnomal/abnomalType/editAbnomalType.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/abnomal/abnomalType/viewAbnomalType.action?id=' + id;
}
function exportexcel() {
	abnomalTypeGrid.title = biolims.common.exportList;
	var vExportContent = abnomalTypeGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(abnomalTypeGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.master.listModel
		});
	item.on('click', lbms);
	
	});
function lbms(){
	var url = ctx+"/system/abnomal/abnomalType/showAbnomalTypeList.action";
	location.href = url;
}
Ext.onReady(function(){
	var item1 = menu.add({
	    	text: biolims.master.treeModel
		});
	item1.on('click', szms);
	});
function szms(){
	var url = ctx+"/system/abnomal/abnomalType/showAbnomalTypeTree.action";
	location.href = url;
}
