var customFieldTable;
$(function() {
	var options = table(true, "",
		"/system/customfields/showCustomFieldListJson.action", [{
			"data": "id",
			"title": biolims.common.id,
		}, {
			"data": "note",
			"title": biolims.common.name,
		}, {
			"data": "productId",
			"title": biolims.crmDoctorItem.productId,
		}, {
			"data": "productName",
			"title": biolims.crmDoctorItem.productName,
		}, {
			"data": "createUser-id",
			"title": biolims.sampleOutApply.createUserId,
		}, {
			"data": "createUser-name",
			"title": biolims.sampleOutApply.createUserName,
		}, {
			"data": "createDate",
			"title": biolims.sampleOutApply.createDate,
			"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
		}, {
			"data": "state",
			"title": biolims.common.state,
			"name":biolims.master.valid+"|"+biolims.master.invalid,
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return biolims.master.valid;
				}else if(data == "3") {
					return biolims.master.invalid;
				}else {
					return "";
				}
			}
		}], null)
	customFieldTable = renderData($("#customField"), options);
	//恢复之前查询的状态
	$('#customField').on('init.dt', function() {
		recoverSearchContent(customFieldTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/system/customfields/editCustomFields.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/customfields/editCustomFields.action?id=' + id;
}

// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/customfields/viewCustomFields.action?id=' + id;
}

//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sampleOutApply.createUserName,
			"type": "input",
			"searchName": "createUser-name"
		},
		{
			"txt": biolims.sampleOutApply.createDate+"(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sampleOutApply.createDate+"(End)",
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"txt": biolims.common.state,
			"type": "select",
			"searchName": "state",
			"options": biolims.common.pleaseChoose+"|"+biolims.master.valid+"|"+biolims.master.invalid,
			"changeOpt": "''|1|3",
		},
		{
			"type": "table",
			"table": customFieldTable
		}
	];
}
