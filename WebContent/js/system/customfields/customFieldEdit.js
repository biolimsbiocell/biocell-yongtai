$(function() {
	//日期格式化
	$("#sampleOrder_birthDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});

	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}

});

/**
 * onchange（参数）
 * 
 * @param id
 */
function change(id) {
	$("#" + id).css({
		"background-color": "white",
		"color": "black"
	});
}

function add() {
	window.location = window.ctx +
		'/system/customfields/editCustomFields.action';
}

function list() {
	window.location = window.ctx +
		'/system/customfields/showFieldTable.action';
}

function newSave() {

	if(checkSubmit() == true) {
		var bool = false;
		var bool2 = false;
		//查询id是否重复
		Ext.Ajax.request({
			url: window.ctx + '/common/hasId.action',
			method: 'POST',
			params: {
				id: $("#fieldTemplate_id").val(),
				obj: 'FieldTemplate'
			},
			async: false,
			success: function(response) {
				var respText = Ext.util.JSON.decode(response.responseText);
				if(respText.message == '') {
					bool = true;
				} else {
					message(respText.message);
				}

			},
			failure: function(response) {}
		});

		//查询
		document.getElementById('fieldTemplateInfoJson').value = saveItemjson($("#customFieldsTable"));
		$.ajax({
			type: "post",
			url: window.ctx + "/system/customfields/checkFieldName.action",
			async: false,
			data: {
				fieldTemplateInfoJson: document.getElementById('fieldTemplateInfoJson').value,
				ModelValue: $("#fieldTemplate_subordinateModuleId").val()
			},
			success: function(data) {
				var obj = JSON.parse(data);
				if(obj.success) {
					if(obj.bool) {
						bool2 = true;
					} else {
						top.layer.msg(obj.msg);
					}
				} else {
					top.layer.msg(biolims.common.checkingFieldCodingFailure);
				}
			}
		});

		if(bool && bool2) {
			save();
		}
	}

}

function tjsp() {
	/*top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
		icon: 3,
		title: '提示'
	}, function(index) {
		top.layer.open({
			title: '审批流程',
			type: 2,
			anim: 2,
			area: ['800px', '500px'],
			btn: biolims.common.selected,
			content: window.ctx + "/workflow/processinstance/toStartView.action?formName=SampleOrder",
			yes: function(index, layer) {
				var datas = {
					userId: userId,
					userName: userName,
					formId: $("#sampleOrder_id").val(),
					title: $("#sampleOrder_name").val()
				}
				ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {
							callback(data);
						}
						dialogWin.dialog("close");
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
				top.layer.close(index);
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}

		});
		top.layer.close(index);
	});*/

}

function sp() {
	/*var taskId = $("#bpmTaskId").val();
	var formId = $("#sampleOrder_id").val();
	top.layer.open({
		title: '审批',
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toCompleteTaskView.action?taskId=" + taskId + "&formId=" + formId,
		yes: function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();
			if(!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if(operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;
				var reqData = {
					data: JSON.stringify(paramData),
					formId: formId,
					taskId: taskId,
					userId: window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {}
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
			}
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});*/
}

function save() {
	if(checkSubmit() == true) {
		var changeLog = "自定义字段-";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		if(changeLog != "自定义字段-"){
			 document.getElementById("changeLog").value = changeLog;
		}
		//子表日志
		var changeLogItem = "自定义字段明细：";
		var data = saveItemjson($("#customFieldsTable"));
		changeLogItem = getChangeLog(data, $("#customFieldsTable"), changeLogItem);
		if(changeLogItem != "自定义字段明细："){
		   document.getElementById("changeLogItem").value = changeLogItem;
		}
		
		//查询
		var bool = false;
		//必填验证
		var requiredField=requiredFilter();
			if(!requiredField){
				return false;
			}
		document.getElementById('fieldTemplateInfoJson').value = saveItemjson($("#customFieldsTable"));
		$.ajax({
			type: "post",
			url: window.ctx + "/system/customfields/checkFieldName.action",
			async: false,
			data: {
				fieldTemplateInfoJson: document.getElementById('fieldTemplateInfoJson').value,
				ModelValue: $("#fieldTemplate_subordinateModuleId").val()
			},
			success: function(data) {
				var obj = JSON.parse(data);
				if(obj.success) {
					if(obj.bool) {
						bool = true;
					} else {
						top.layer.msg(obj.msg);
					}
				} else {
					top.layer.msg(biolims.common.checkingFieldCodingFailure);
				}
			}
		});

		if(bool) {
			top.layer.load(4, {shade:0.3});
			form1.action = window.ctx +
				"/system/customfields/save.action?loadNumber=1";
			form1.submit();
			top.layer.closeAll();
		}
	}
}

function checkSubmit() {
	if($("#fieldTemplate_id").val() == null || $("#fieldTemplate_id").val() == "") {
		top.layer.msg(biolims.user.NoNull);
		return false;
	};
	if($("#fieldTemplate_note").val() == null || $("#fieldTemplate_note").val() == "") {
		top.layer.msg(biolims.common.nameNotEmpty);
		return false;
	};
	return true;
}

// 选择检查产品
function voucherProductFun() {
	top.layer.open({
		title: biolims.common.selectProducts,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/com/biolims/system/product/showProductSelTree.action",
		yes: function(index, layer) {
			var name = [],
				id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			$("#fieldTemplate_productId").val(id.join(","));
			$("#fieldTemplate_productName").val(name.join(","));
			top.layer.close(index)
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

function parseDate(str) {
	if(str.match(/^\d{4}[\-\/\s+]\d{1,2}[\-\/\s+]\d{1,2}$/)) {
		return new Date(str.replace(/[\-\/\s+]/i, '/'));
	} else if(str.match(/^\d{8}$/)) {
		return new Date(str.substring(0, 4) + '/' + str.substring(4, 6) + '/' +
			str.substring(6));
	}
};
function changeState(){
	var paraStr = "formId=" + $("#fieldTemplate_id").val() +
		"&tableId=FieldTemplate";
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: $("#fieldTemplate_id").val()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		}
	});

}

//选择所属模块
function vouchersubordinateModuleFun() {
		top.layer.open({
			title: biolims.common.pleaseChoose,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/applicationTypeTable/showDialogApplicationTypeTable.action?flag=1", ''],
			yes: function(index, layer) {
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addApplicationTypeTable .chosed").children("td").eq(0).text();
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addApplicationTypeTable .chosed").children("td").eq(1).text();
				top.layer.close(index)
				$("#fieldTemplate_subordinateModuleId").val(id)
				$("#fieldTemplate_subordinateModuleName").val(name)
			},
		})
};
