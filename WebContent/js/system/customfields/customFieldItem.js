/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
var oldChangeLog;
$(function() {
	// 加载子表
	var id = $("#fieldTemplate_id").val();

	var tbarOpts = [];

	var colOpts = [];

	colOpts.push({
		"data": "id",
		"title": "ID",
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "fieldName",
		"title": biolims.common.fieldCodedName,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "fieldName");
		}
	});

	colOpts.push({
		"data": "fieldType",
		"title": biolims.common.fieldType,
		"className": "select",
		"name": biolims.common.textBox+"|"+biolims.common.digitalBox+"|"+biolims.common.dateTextBox+"|"+biolims.common.radiobutton+"|"+biolims.common.checkBox,
		"createdCell": function(td) {
			$(td).attr("saveName", "fieldType");
			$(td).attr("selectOpt", biolims.common.textBox+"|"+biolims.common.digitalBox+"|"+biolims.common.dateTextBox+"|"+biolims.common.radiobutton+"|"+biolims.common.checkBox);
		},
		"render": function(data, type, full, meta) {
			if(data == "text") {
				return biolims.common.textBox;
			}
			if(data == "number") {
				return biolims.common.digitalBox;
			}
			if(data == "date") {
				return biolims.common.dateTextBox;
			}
			if(data == "radio") {
				return biolims.common.radiobutton;
			}
			if(data == "checkbox") {
				return biolims.common.checkBox;
			}
		}
	});

	colOpts.push({
		"data": "label",
		"title": biolims.common.fieldDisplayName,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "label");
		}
	});
	colOpts.push({
		"data": "orderNum",
		"title": biolims.user.sortingNumber,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderNum");
		}
	});
	colOpts.push({
		"data": "readOnly",
		"title": biolims.common.readOnly,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "readOnly");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "true") {
				return biolims.common.yes;
			}
			if(data == "false") {
				return biolims.common.no;
			} else {
				return "";
			}
		}
	});

	colOpts.push({
		"data": "fieldLength",
		"title": biolims.common.fieldLength,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "fieldLength");
		},
	});

	colOpts.push({
		"data": "defaultValue",
		"title": biolims.common.defaultValue,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "defaultValue");
		}
	});

	colOpts.push({
		"data": "typeContent",
		"title": biolims.common.fieldHints,
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "typeContent");
		},
	});

	colOpts.push({
		"data": "isRequired",
		"title": biolims.common.required,
		"className": "select",
		"name": biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isRequired");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "true") {
				return biolims.common.yes;
			}
			if(data == "false") {
				return biolims.common.no;
			} else {
				return "";
			}
		}
	});

	colOpts.push({
		"data": "singleOptionName",
		"title": biolims.common.optionName,
		"width": "180px",
		"createdCell": function(td, data,rowdata) {
			$(td).attr("saveName", "singleOptionName");
			$(td).attr("singleOptionId", rowdata['singleOptionId']);
		}
	});

	var handlemethod = $("#handlemethod").val();
	tbarOpts.push({
		text: biolims.common.fieldAddition,
		className: 'btn btn-sm btn-success addTemplateItem',
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editFieldLayer()
		}
	});
	if(handlemethod != "view"&&$("#fieldTemplate_stateName").val()!="Complete") {
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#customFieldsTable"),
					"/system/customfields/delCustomFields.action", "自定义字段删除：", $("#fieldTemplate_id").val());
			}
		});
	}
	var customFieldsOptions = table(true,id,
		'/system/customfields/showFieldTemplateItemTableJson.action', colOpts, tbarOpts)
	myTable = renderData($("#customFieldsTable"), customFieldsOptions);
	//日志 dwb 2018-05-12 13:10:35
	myTable.on('draw', function() {
		oldChangeLog = myTable.ajax.json();
	});
	//自定义字段
	addTemplateItem();
	// 上一步下一步
	stepViewChange();
	
});

//自定义字段添加
function addTemplateItem() {
	var options = [biolims.common.textBox, biolims.common.digitalBox, biolims.common.dateTextBox, biolims.common.radiobutton, biolims.common.checkBox];
	var lis = '';
	options.forEach(function(v, i) {
		lis += '<li val=' + i + '><a href="###">' + v + '</a></li>';
	});
	$("#customFieldsTable").on('init.dt', function() {
		$(".addTemplateItem").addClass("dropdown-toggle").attr("data-toggle", "dropdown").append('<span class="caret"></span>').wrap("<div class='btn-group'></div>").after('<ul class="dropdown-menu">' + lis + '</ul>');
		$(".addTemplateItem").next("ul").children("li").click(function() {
			var item = $(this).attr("val");
			var name = $(this).text();
			top.layer.open({
				title: biolims.common.addCustomFields,
				type: 1,
				area: [document.body.clientWidth - 300, "400px"],
				btn: biolims.common.selected,
				content: $("#layerItem").html(),
				success: function() {
					$(".layui-layer-content .datatype",parent.document).val(name);
					if(item == "0" || item == "1" || item == "2") { //文本框//数字框//日期框
						$(".layui-layer-content .input-group",parent.document).css("margin-top", "30px");
						$(".layui-layer-content #choseCheekBox",parent.document).remove();
					}
					if(item == "3") { //单选框
						$(".layui-layer-content #choseCheekBox",parent.document).show();
					}
					if(item == "4") { //多选框
						$(".layui-layer-content #choseCheekBox",parent.document).show();
						$(".layui-layer-content .addItemTembtn",parent.document).show().addClass("xxx");
					}
					if($(".addItemTembtn",parent.document).hasClass("xxx")) {
						$(".addItemTembtn",parent.document).click(function() {
							var data = $("#choseCheekBox .choseCheekBox",parent.document).eq(0).clone();
							data.find("input").val("").attr("placeholder", "");
							$(this).before(data);
						});
					}
				},
				yes: function(index, layer) {
					//清除没有内容选项
					$("#customFieldsTable").find(".dataTables_empty").parent("tr").remove();
					//获取添加的数据
					var inputs = $(".form-control",parent.document);
					var addItemLayerValue = [];
					var itemValue = [];
					var itemName = [];
					inputs.each(function(i, val) {
						if(i>4){
							if(i < 14) {
								addItemLayerValue.push($(val).val());
							} else {
								if(i % 2) {
									itemName.push($(val).val());
								} else {
									itemValue.push($(val).val());
								}
							}}
					});
					var itemValueStr = itemValue.join("/");
					var itemNameStr = itemName.join("/");
					//生成tr
					var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
					tr.height(32);
					var ths = $("#customFieldsTable").find("th");
					for(var i = 1; i < ths.length; i++) {
						var edit = $(ths[i]).attr("key");
						var saveName = $(ths[i]).attr("saveName");
						var width = $(ths[i]).width();
						if(edit == "select") {
							var selectOpt = $(ths[i]).attr("selectopt");
							tr.append("<td class=" + edit + " saveName=" + saveName + " selectopt=" + selectOpt + ">" + addItemLayerValue[i - 1] + "</td>");
						} else if(saveName == "singleOptionName") {
							tr.append("<td class=" + edit + " saveName=" + saveName + " singleOptionId='" + itemValueStr + "' style='overflow:hidden;text-overflow:ellipsis;max-width: " + width + "px'>" + itemNameStr + "</td>");
						} else {
							tr.append("<td class=" + edit + " saveName=" + saveName + " style='overflow:hidden;text-overflow:ellipsis;max-width: " + width + "px'>" + addItemLayerValue[i - 1] + "</td>");
						}
					}
					$("#customFieldsTable").find("tbody").prepend(tr);
					checkall($("#customFieldsTable"));
					top.layer.close(index);
				},
				cancel: function(index, layer) {
					top.layer.close(index)
				}
			});
		});
	});
}
//弹窗编辑
function editFieldLayer() {
			var rows = $("#customFieldsTable .selected");
			var length = rows.length;
			if(!length) {
				top.layer.msg(biolims.common.pleaseSelect);
				return false;
			}
			var item
			var fieldName
			var fieldType
			var label
			var readOnly
			var fieldLength
			var defaultValue
			var orderNum
			var typeContent
			var isRequired
			var singleOptionId
			var singleOptionIds=[];
			var singleOptionName
			var singleOptionNames=[];
			$.each(rows, function(i, k) {
				item=$(k).find("td[savename='fieldType']").text();
				fieldName=$(k).find("td[savename='fieldName']").text();
				fieldType=$(k).find("td[savename='fieldType']").text();
				label=$(k).find("td[savename='label']").text();
				readOnly=$(k).find("td[savename='readOnly']").text();
				fieldLength=$(k).find("td[savename='fieldLength']").text();
				orderNum=$(k).find("td[savename='orderNum']").text();
				defaultValue=$(k).find("td[savename='defaultValue']").text();
				typeContent=$(k).find("td[savename='typeContent']").text();
				isRequired=$(k).find("td[savename='isRequired']").text();
				singleOptionId=$(k).find("td[savename='singleOptionName']").attr("singleOptionId")
				singleOptionName=$(k).find("td[savename='singleOptionName']").text();
				singleOptionIds=singleOptionId.split("/")
				singleOptionNames=singleOptionName.split("/")
			});
			top.layer.open({
				title: biolims.common.addCustomFields,
				type: 1,
				area: [document.body.clientWidth - 300, "400px"],
				btn: biolims.common.selected,
				content: $("#layerItem").html(),
				success: function() {
					$(".layui-layer-content .input-group",parent.document).find("#fieldCodedName").val(fieldName);
					$(".layui-layer-content .input-group",parent.document).find("#fieldType").val(fieldType);
					$(".layui-layer-content .input-group",parent.document).find("#fieldDisplayName").val(label);
					$(".layui-layer-content .input-group",parent.document).find("#isReadOnly").val(readOnly);
					$(".layui-layer-content .input-group",parent.document).find("#orderNum").val(orderNum);
					$(".layui-layer-content .input-group",parent.document).find("#fieldLength").val(fieldLength);
					$(".layui-layer-content .input-group",parent.document).find("#defaultValue").val(defaultValue);
					$(".layui-layer-content .input-group",parent.document).find("#fieldHints").val(typeContent);
					$(".layui-layer-content .input-group",parent.document).find("#required").val(isRequired);
					if(item == biolims.common.textBox || item == biolims.common.dateTextBox || item == biolims.common.digitalBox) { //文本框//数字框//日期框
						$(".layui-layer-content .input-group",parent.document).css("margin-top", "30px");
						$(".layui-layer-content #choseCheekBox",parent.document).remove();
					}
					if(item == biolims.common.radiobutton) { //单选框
						$(".layui-layer-content #choseCheekBox",parent.document).show();
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(0).val(singleOptionIds[0])
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(1).val(singleOptionNames[0])
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(2).val(singleOptionIds[1])
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(3).val(singleOptionNames[1])
					}
					if(item == biolims.common.checkBox) { //多选框
						$(".layui-layer-content #choseCheekBox",parent.document).show();
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(0).val(singleOptionIds[0])
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(1).val(singleOptionNames[0])
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(2).val(singleOptionIds[1])
						$(".layui-layer-content #choseCheekBox",parent.document).find(".form-control").eq(3).val(singleOptionNames[1])
						$(".layui-layer-content .addItemTembtn",parent.document).show().addClass("xxx");
						for(var i=0;i<singleOptionIds.length;i++){
							if(i>1){
								var data = $("#choseCheekBox .choseCheekBox",parent.document).eq(0).clone();
								data.find("input").val(singleOptionIds[i]);
								data.find("input").eq(1).val(singleOptionNames[i]);
								$(".addItemTembtn",parent.document).before(data);
//								$(this).before(data);	
							}
						}
					}
					if($(".addItemTembtn",parent.document).hasClass("xxx")) {
						$(".addItemTembtn",parent.document).click(function() {
							var data = $("#choseCheekBox .choseCheekBox",parent.document).eq(0).clone();
							data.find("input").val("").attr("placeholder", "");
							$(this).before(data);
						});
					}
				},
				yes: function(index, layer) {
					//清除没有内容选项
					$("#customFieldsTable").find(".dataTables_empty").parent("tr").remove();
					//获取添加的数据
					var inputs = $(".form-control",parent.document);
					console.log(inputs)
					var addItemLayerValue = [];
					var itemValue = [];
					var itemName = [];
					inputs.each(function(i, val) {
						if(i>4){
						if(i < 14) {
							addItemLayerValue.push($(val).val());
						} else {
							if(i % 2) {
								itemName.push($(val).val());
							} else {
								itemValue.push($(val).val());
							}
						}}
					});
					console.log(addItemLayerValue)
					var itemValueStr = itemValue.join("/");
					var itemNameStr = itemName.join("/");
					$(rows[0]).find("td[savename='fieldName']").text(addItemLayerValue[0]);
					$(rows[0]).find("td[savename='fieldType']").text(addItemLayerValue[1]);
					$(rows[0]).find("td[savename='label']").text(addItemLayerValue[2]);
					$(rows[0]).find("td[savename='orderNum']").text(addItemLayerValue[3]);
					$(rows[0]).find("td[savename='readOnly']").text(addItemLayerValue[4]);
					$(rows[0]).find("td[savename='fieldLength']").text(addItemLayerValue[5]);
					$(rows[0]).find("td[savename='defaultValue']").text(addItemLayerValue[6]);
					$(rows[0]).find("td[savename='typeContent']").text(addItemLayerValue[7]);
					$(rows[0]).find("td[savename='isRequired']").text(addItemLayerValue[8]);
					$(rows[0]).find("td[savename='singleOptionName']").text(itemNameStr);
					$(rows[0]).find("td[savename='singleOptionName']").attr("singleOptionId",itemValueStr);
					checkall($("#customFieldsTable"));
					top.layer.close(index);
				},
				cancel: function(index, layer) {
					top.layer.close(index)
				}
			});
}
//dwb 日志功能 2018-05-12 13:13:42
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断选择框并转换为数据库字段
			if(k == "fieldType") {
				var type = $(tds[j]).text();
				if(type == biolims.common.textBox) {
					json[k] = "text";
				} else if(type == biolims.common.digitalBox) {
					json[k] = "number";
				} else if(type == biolims.common.dateTextBox) {
					json[k] = "date";
				} else if(type == biolims.common.radiobutton) {
					json[k] = "radio";
				} else if(type == biolims.common.checkBox) {
					json[k] = "checkbox";
				}
				continue;
			}
			if(k == "readOnly") {
				var readOnly = $(tds[j]).text();
				if(readOnly == biolims.common.yes) {
					json[k] = "true";
				} else if(readOnly == biolims.common.no) {
					json[k] = "false";
				}
				continue;
			}
			if(k == "isRequired") {
				var required = $(tds[j]).text();
				if(required == biolims.common.yes) {
					json[k] = "true";
				} else if(required == biolims.common.no) {
					json[k] = "false";
				}
				continue;
			}
			if(k == "singleOptionName") {
				var itemValue = $(tds[j]).attr("singleoptionId");
				var itemName = $(tds[j]).text();
				json.singleOptionName = itemName;
				json.singleOptionId= itemValue;
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	console.log(data);
	data=JSON.stringify(data);
	return data;
}

// 点击上一步下一步切换
function stepViewChange() {
	$("#next").unbind("click").click(
		function() {
			$("#pre").attr("disabled", false);
			var index = $(".wizard_steps .step").index(
				$(".wizard_steps .selected"));
			if(index == $(".wizard_steps .step").length - 1) {
				$(this).attr("disabled", true);
				return false;
			}
			$(".HideShowPanel").eq(index + 1).slideDown().siblings(
				'.HideShowPanel').slideUp();
			$(".wizard_steps .step").eq(index).removeClass("selected")
				.addClass("done");
			var nextStep = $(".wizard_steps .step").eq(index + 1);
			if(nextStep.hasClass("disabled")) {
				nextStep.removeClass("disabled").addClass("selected");
			} else {
				nextStep.removeClass("done").addClass("selected");
			}
			//table刷新和操作显示
			$(".box-tools").show();
			// 回到顶部
			$('body,html').animate({
				scrollTop: 0
			}, 500, function() {
				myTable.ajax.reload();
			});
		});
	$("#pre").click(
		function() {
			$("#next").attr("disabled", false);
			var index = $(".wizard_steps .step").index(
				$(".wizard_steps .selected"));
			if(index == 0) {
				$(this).attr("disabled", true);
				return false;
			}
			$(".HideShowPanel").eq(index - 1).slideDown().siblings(
				'.HideShowPanel').slideUp();
			$(".wizard_steps .step").eq(index).removeClass("selected")
				.addClass("done");
			$(".wizard_steps .step").eq(index - 1).removeClass("done")
				.addClass("selected");
			//table刷新和操作隐藏
			$(".box-tools").hide();

			$('body,html').animate({
				scrollTop: 0
			}, 500);
		});
	$(".step").click(
		function() {
			if($(this).hasClass("done")) {
				var index = $(".wizard_steps .step").index($(this));
				$(".wizard_steps .selected").removeClass("selected")
					.addClass("done");
				$(this).removeClass("done").addClass("selected");
				$(".HideShowPanel").eq(index).slideDown().siblings(
					'.HideShowPanel').slideUp();
				//table刷新和操作隐藏
				console.log(index);
				if(index == 0) {
					$(".box-tools").hide();
				} else {
					$(".box-tools").show();
				}

			} else if($(this).hasClass("disabled")) {
				top.layer.msg(biolims.order.finishPre);
			}
		});
}