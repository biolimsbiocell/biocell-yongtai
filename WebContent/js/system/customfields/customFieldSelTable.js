$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "note",
		"title": biolims.user.eduName1,
	});
	colOpts.push({
		"data": "subordinateModuleName",
		"title": biolims.common.subordinateModule,
	});
	var tbarOpts = [];
	var options = table(true, null,
			'/system/customfields/showCustomDialogJson.action',colOpts , tbarOpts)
		var customFieldSelTable = renderData($("#addcustomFieldTable"), options);
	$("#addcustomFieldTable").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addcustomFieldTable_wrapper .dt-buttons").empty();
		$('#addcustomFieldTable_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addcustomFieldTable tbody tr");
	customFieldSelTable.ajax.reload();
	customFieldSelTable.on('draw', function() {
		trs = $("#addcustomFieldTable tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

