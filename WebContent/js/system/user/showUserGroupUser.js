var showUserGroupUserGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'userGroup-id',
		type:"string"
	});
	    fields.push({
		name:'userGroup-name',
		type:"string"
	});
	    fields.push({
		name:'user-id',
		type:"string"
	});
	    fields.push({
		name:'user-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'user-id',
		header:biolims.common.testUserId,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'user-name',
		header:biolims.common.testUserName,
		width:20*6,
		hidden:false,
		sortable:true
		});
		cm.push({
		dataIndex:'userGroup-id',
		header:biolims.common.acceptUserId,
		width:20*6,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'userGroup-name',
		header:biolims.common.acceptUserName,
		width:20*10,
		hidden:false,
		sortable:true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/user/userGroupUser/showuserGroupUserSelectJson.action?gid="+$("#gid").val();
	var opts={};
	opts.title=biolims.common.acceptUserName;
	opts.width = 450;
	opts.height = 400;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
//		$('#selectId').val(id);
//		window.parent.setTechJkServiceTaskFun(rec);
		setUserGroupUser();
	};
	showUserGroupUserGrid=gridTable("showUserGroupUserDiv",cols,loadParam,opts);
	$("#showUserGroupUserDiv").data("showUserGroupUserGrid", showUserGroupUserGrid);
});