var codeMainGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
			name:'code',
			type:"string"
		});
	    fields.push({
		name:'namex',
		type:"string"
	});
	    fields.push({
		name:'namey',
		type:"string"
	});
	    fields.push({
		name:'codex',
		type:"string"
	});
	    fields.push({
		name:'codey',
		type:"string"
	});
	    fields.push({
		name:'qrx',
		type:"string"
	});
	    fields.push({
		name:'qry',
		type:"string"
	});
	    fields.push({
		name:'ip',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:10*8,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.sys.code,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'namex',
		header:biolims.sys.namex,
		width:10*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'namey',
		header:biolims.sys.namey,
		width:10*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'codex',
		header:biolims.sys.codex,
		width:10*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'codey',
		header:biolims.sys.codey,
		width:10*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'qrx',
		header:biolims.sys.qrx,
		width:10*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'qry',
		header:biolims.sys.qry,
		width:10*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'ip',
		header:biolims.sys.ip,
		width:20*6,
		
		sortable:true
	});
	var statestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.master.valid ], [ '0', biolims.master.invalid ]]
	});
	
	var stateComboxFun = new Ext.form.ComboBox({
		store : statestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:10*10,
		renderer: Ext.util.Format.comboRenderer(stateComboxFun),
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/syscode/codeMain/showCodeMainListJson.action";
	var opts={};
	opts.title=biolims.master.codeMain;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	codeMainGrid=gridTable("show_codeMain_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/system/syscode/codeMain/editCodeMain.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/system/syscode/codeMain/editCodeMain.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/system/syscode/codeMain/viewCodeMain.action?id=' + id;
}
function exportexcel() {
	codeMainGrid.title = biolims.common.exportList;
	var vExportContent = codeMainGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(codeMainGrid);
				$(this).dialog("close");

			},
			"Confirm": function() {
				form_reset();

			}
		}, true, option);
	});
});
