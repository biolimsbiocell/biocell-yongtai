var codeMainDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'namex',
		type:"string"
	});
	    fields.push({
		name:'namey',
		type:"string"
	});
	    fields.push({
		name:'codex',
		type:"string"
	});
	    fields.push({
		name:'codey',
		type:"string"
	});
	    fields.push({
		name:'qrx',
		type:"string"
	});
	    fields.push({
		name:'qry',
		type:"string"
	});
	    fields.push({
		name:'ip',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'namex',
		header:biolims.sys.namex,
		width:50*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'namey',
		header:biolims.sys.namey,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'codex',
		header:biolims.sys.codex,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'codey',
		header:biolims.sys.codey,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'qrx',
		header:biolims.sys.qrx,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'qry',
		header:biolims.sys.qry,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'ip',
		header:biolims.sys.ip,
		width:50*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:50*10,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/syscode/codeMain/showCodeMainListJson.action";
	var opts={};
	opts.title=biolims.master.codeMain;
	opts.height=document.body.clientHeight-140;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setCodeMainFun(rec);
	};
	codeMainDialogGrid=gridTable("show_dialog_codeMain_div",cols,loadParam,opts);
	$("#show_dialog_codeMain_div").data("codeMainDialogGrid", codeMainDialogGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(codeMainDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
