$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.name,
	});
	colOpts.push({
		"data": "ip",
		"title": "IP",
	});
	colOpts.push({
		"data": "code",
		"title": biolims.goods.printId,
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/system/syscode/codeMain/showDialogCodeMainTableJson.action',colOpts , tbarOpts)
		var codeMainTable = renderData($("#addCodeMain"), options);
	$("#addCodeMain").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addCodeMain_wrapper .dt-buttons").empty();
		$('#addCodeMain_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addCodeMain tbody tr");
	codeMainTable.ajax.reload();
	codeMainTable.on('draw', function() {
		trs = $("#addCodeMain tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

