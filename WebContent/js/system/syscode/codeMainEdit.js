﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/system/syscode/codeMain/editCodeMain.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/system/syscode/codeMain/showCodeMainList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#codeMain", {
					userId : userId,
					userName : userName,
					formId : $("#codeMain_id").val(),
					title : $("#codeMain_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#codeMain_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/system/syscode/codeMain/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/system/syscode/codeMain/copyCodeMain.action?id=' + $("#codeMain_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#codeMain_id").val() + "&tableId=codeMain");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#codeMain_id").val());
	nsc.push(biolims.common.IdEmpty);
//	fs.push($("#codeMain_namex").val());
//	nsc.push(biolims.sys.namexIsEmpty);
//	fs.push($("#codeMain_namey").val());
//	nsc.push(biolims.sys.nameyIsEmpty);
//	fs.push($("#codeMain_codex").val());
//	nsc.push(biolims.sys.codexIsEmpty);
//	fs.push($("#codeMain_codey").val());
//	nsc.push(biolims.sys.codeyIsEmpty);
//	fs.push($("#codeMain_qrx").val());
//	nsc.push(biolims.sys.qrxIsEmpty);
	fs.push($("#codeMain_state").val());
	nsc.push("状态不能为空！");
	fs.push($("#codeMain_ip").val());
	nsc.push(biolims.sys.ipIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.master.codeMain,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);