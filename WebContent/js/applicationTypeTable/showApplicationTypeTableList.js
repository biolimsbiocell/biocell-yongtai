$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'tableName',
		type : "string"
	});
	fields.push({
		name : 'entitypackage',
		type : "string"
	});
	fields.push({
		name : 'entityname',
		type : "string"
	});
	fields.push({
		name : 'type',
		type : "string"
	});
	fields.push({
		name : 'orderNumber',
		type : "Intrger"
	});
	fields.push({
		name : 'pathName',
		type : "string"
	});
	fields.push({
		name : 'applicationType-id',
		type : "string"
	});
	fields.push({
		name : 'classPath',
		type : "string"
	});
	fields.push({
		name : 'workflowUserColumn',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'upTable',
		type : "string"
	});
	cols.fields = fields;
	var storeTypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '', '请选择' ],[ '0', '主表' ], [ '1', '页签' ] ]
	});
	var typeCob = new Ext.form.ComboBox({
		store : storeTypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		header : '表单名称',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'tableName',
		header : '表名',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'entitypackage',
		header : '包名',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'entityname',
		header : '实体名',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'upTable',
		header : '上级表ID',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'type',
		header : '类型',
		width : 120,
		sortable : true,
		editor : typeCob,
		renderer :Ext.util.Format.comboRenderer(typeCob)
	});
	cm.push({
		dataIndex : 'code',
		header : '编码',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'orderNumber',
		header : '排序号',
		width : 120,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'pathName',
		header : '路径',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var appType = new Ext.form.TextField({
		allowBlank : true
	});
	appType.on('focus', function() {
		var option = {};
		option.width = 475;
		option.height = 525;
		var url = "/applicationType/showDialogApplicationTypeList.action";
		var selApplicationTypeTableDialog = loadDialogPage(null, "选择表单类型", url, {
			"确定" : function() {
				var selGrid=$("#show_dialog_applicationtype_grid_div").data("waitApplicationtypeApplyGrid");
				var scRecord = selGrid.getSelectRecord();
				if (scRecord && scRecord[0]) {
					applicationTypeReceiveGrid.stopEditing();
					var myRecord=applicationTypeReceiveGrid.getSelectRecord();
					myRecord[0].set("applicationType-id", scRecord[0].get('id'));
					applicationTypeReceiveGrid.startEditing(0, 0);
				}
				$(selApplicationTypeTableDialog).dialog("close");
			}
		}, true, option);
	});
	cm.push({
		dataIndex : 'applicationType-id',
		header : '所属应用类型',
		width : 120,
		sortable : true,
		editor : appType
	});
	cm.push({
		dataIndex : 'classPath',
		header : '表单对应实体的类路径',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'workflowUserColumn',
		header : 'workflow用户字段',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		header : '状态',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/applicationTypeTable/showApplicationTypeTableListJson.action";
	loadParam.limit = 10000;
	var opts = {};
	opts.title = "表单应用管理详细信息";
	opts.height = document.body.clientHeight - 20;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/applicationTypeTable/delApplicationTypeTable.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				applicationTypeReceiveGrid.getStore().commitChanges();
				applicationTypeReceiveGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '保存',
		handler : function() {
			var result = {};
			if (result) {
				ajax("post", "/applicationTypeTable/saveApplicationTypeTableList.action", {
					itemDataJson : getItemData()
				}, function(data) {
					if (data.success) {
						applicationTypeReceiveGrid.getStore().commitChanges();
						applicationTypeReceiveGrid.getStore().reload();
						message("保存成功！");
					} else {
						message("保存失败！");
					}
				}, null);
			}
		}
	});
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_typeTable_div99"),"批量上传",null,{
				"确定":function(){
					goInExcel99();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
var idTmr = "";
	function goInExcel99(){
		var file = document.getElementById("file-uploadaew99").files[0];  
		var n = 0;
		var ob = applicationTypeReceiveGrid.getStore().recordType;
		var reader = new FileReader();  
		//将文件以文本形式读入页面  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[1]){
                			var p = new ob({});
                			p.isNew = true;				
                			p.set("id",this[0]);
                			p.set("name",this[1]);
							p.set("tableName",this[2]);
							p.set("entitypackage",this[3]);
							p.set("entityname",this[4]);
							p.set("upTable",this[5]);
							
							p.set("type",this[6]);
							p.set("orderNumber",this[7]);
							p.set("pathName",this[8]);
							p.set("applicationType-id", this[9]);
							p.set("classPath",this[10]);
							p.set("workflowUserColumn",this[11]);
							p.set("state",this[12]);
							applicationTypeReceiveGrid.getStore().insert(0, p);
                		}
                	}
                    n = n +1;
               });
		}
	}

	applicationTypeReceiveGrid = gridEditTable("show_applicationtypetable_grid_div", cols, loadParam, opts);
	var getItemData = function() {
		var itemDataJson = [];
		var modifyRecord = applicationTypeReceiveGrid.getModifyRecord();
		if (modifyRecord && modifyRecord.length > 0) {
			$.each(modifyRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.name = obj.get("name");
				data.tableName = obj.get("tableName");
				data.entitypackage = obj.get("entitypackage");
				data.entityname = obj.get("entityname");
				data.type = obj.get("type");
				data.orderNumber = obj.get("orderNumber");
				data.pathName = obj.get("pathName");
				data['applicationType-id'] = obj.get("applicationType-id");
				data.classPath = obj.get("classPath");
				
				data.upTable = obj.get("upTable");
				
//				data.classPath = obj.get("classPath").replaceAll("．",".");
				data.workflowUserColumn = obj.get("workflowUserColumn");
				data.state = obj.get("state");
				itemDataJson.push(data);
			});
		}
		return itemDataJson.length > 0 ? JSON.stringify(itemDataJson) : "";
	};
});
