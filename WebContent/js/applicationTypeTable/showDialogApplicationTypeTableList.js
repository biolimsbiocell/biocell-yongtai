﻿$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'pathName',
		type : "string"
	});
	fields.push({
		name : 'applicationType-id',
		type : "string"
	});
	fields.push({
		name : 'classPath',
		type : "string"
	});
	fields.push({
		name : 'workflowUserColumn',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		header : '表单名称',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'pathName',
		header : '路径',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'applicationType-id',
		header : '所属应用类型',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		}) 
	});
	cm.push({
		dataIndex : 'classPath',
		header : '表单对应实体的类路径',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'workflowUserColumn',
		header : 'workflow用户字段',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		header : '状态',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/applicationTypeTable/showApplicationTypeTableListJson.action";
	var opts = {};
	opts.width = 500;
	opts.height = 430;
	applicationTypeGrid = gridTable("show_dialog_applicationtypetable_grid_div", cols, loadParam, opts);
	$("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid", applicationTypeGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
});
