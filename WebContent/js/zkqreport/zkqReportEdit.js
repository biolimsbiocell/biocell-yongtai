$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/zkqreport/zkqReport/editZkqReport.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/zkqreport/zkqReport/showZkqReportList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("ZkqReport", {
					userId : userId,
					userName : userName,
					formId : $("#zkqReport_id").val(),
					title : $("#zkqReport_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#zkqReport_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
	    var zkqReportItemDivData = $("#zkqReportItemdiv").data("zkqReportItemGrid");
		document.getElementById('zkqReportItemJson').value = commonGetModifyRecords(zkqReportItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/zkqreport/zkqReport/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/zkqreport/zkqReport/copyZkqReport.action?id=' + $("#zkqReport_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#zkqReport_id").val() + "&tableId=zkqReport");
//}
$("#toolbarbutton_status").click(function(){
	commonChangeState("formId=" + $("#zkqReport_id").val() + "&tableId=zkqReport");

});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#zkqReport_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'生成报告',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/zkqreport/zkqReport/showZkqReportItemList.action", {
				id : $("#zkqReport_id").val()
			}, "#zkqReportItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	
	 function SequencingFun(){
		 var SequencingFun=null;
		 var str="100";
		 var win = Ext.getCmp('SequencingFun');
		 if (win) {win.close();}
		 SequencingFun= new Ext.Window({
		 id:'SequencingFun',modal:true,title:"选择FC号",layout:'fit',width:500,height:500,closeAction:'close',
		 plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		 collapsible: true,maximizable: true,
		 items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		 html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/experiment/sequencing/sequencingSelect.action?flag=SequencingFun&code="+str+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		 buttons: [
		 { text: biolims.common.close,
		  handler: function(){
		  SequencingFun.close(); }  }]  });     
		 SequencingFun.show(); 
		 }
//function setSequencingFun(rec){
//		 document.getElementById('zkqReport_flowCode').value=rec.get('fcCode');
//		 alert(rec.get("fcCode"))
////		 $("#deSequencingTask_flowCode").val(rec.get('id'));
//		 var win = Ext.getCmp('SequencingFun');
//		 var id=rec.get('id');
//			ajax("post", "/experiment/sequencing/getSequencingList.action", {
//				code : id,
//				}, function(data) {
//					if (data.success) {	
//						var ob = zkqReportGrid.getStore().recordType;
//						zkqReportGrid.stopEditing();
//						$.each(data.data, function(i, obj) {
//							alert()
//							var p = new ob({});
//							p.isNew = true;
//							p.set("flowCode", obj.flowCell);
//							alert(obj.flowCell);
//							zkqReportGrid.getStore().add(p);							
//						});
//						
//						zkqReportGrid.startEditing(0, 0);		
//					} else {
//						message(biolims.common.anErrorOccurred);
//					}
//				}, null);
//		 if(win){win.close();}
//		 }

function setSequencingFun(rec){
	 document.getElementById("zkqReport_flowCode").value = rec.get('fcCode');
	 var win = Ext.getCmp('SequencingFun');
	 if(win){win.close();}
	 }

//
//function showfcFun(){
//	var win = Ext.getCmp('showfcFun');
//	if (win) {win.close();}
//	var showfcFun= new Ext.Window({
//	id:'showfcFun',modal:true,title:biolims.sequencing.selectFCCode,layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/analysis/desequencing/deSequencingTask/showDeSequencingTaskByState.action?flag=ShowfcFun3' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: biolims.common.close,
//	 handler: function(){
//		 showfcFun.close(); }  }]  });
//	showfcFun.show(); 
//}
//
//
//function setShowfcFun3(rec){
//	$("#zkqReport_flowCode").val(rec.get("flowCode")) ;
//	ajax("post", "/analysis/data/dataTask/selectOrAddByFC.action", {
//		fcId : rec.get("flowCode")
//	}, function(data) {
//		console.log(data);
//		for(var i=0;i<data.data.length;i++){
//
//			dataTaskTempGrid.getStore().add(p);
//		}
//	}, null);
//	
//	var win = Ext.getCmp('showfcFun');
//	if(win){win.close();
//	}
//}
//	