var zkqReportItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'project-id',
		type:"string"
	});
	   fields.push({
		name:'crmcustomer-id',
		type:"string"
	});
	   fields.push({
		name:'crmdoctor-id',
		type:"string"
	});
	   fields.push({
			name:'project-name',
			type:"string"
		});
		   fields.push({
			name:'crmcustomer-name',
			type:"string"
		});
		   fields.push({
			name:'crmdoctor-name',
			type:"string"
		});
	   fields.push({
		name:'tp',
		type:"string"
	});
	   fields.push({
		name:'genotype',
		type:"string"
	});
	    fields.push({
		name:'zkqreport-id',
		type:"string"
	});
	    fields.push({
		name:'zkqreport-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   
	    fields.push({
			name:'sellPerson-id',
			type:"string"
		});
		    fields.push({
			name:'sellPerson-name',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'报告编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'project-id',
		hidden : false,
		header:'项目编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'project-name',
		hidden : false,
		header:'项目名称',
		width:20*6,
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'crmcustomer-id',
		hidden : true,
		header:'客户编号',
		width:20*6,
	});
	
	cm.push({
		dataIndex:'crmcustomer-name',
		hidden : false,
		header:'客户名称',
		width:20*6,
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'crmdoctor-id',
		hidden : true,
		header:'医生编号',
		width:20*6,
	});
	
	cm.push({
		dataIndex:'crmdoctor-name',
		hidden : false,
		header:'医生名称',
		width:20*6,
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sellPerson-id',
		hidden : true,
		header:'实验员ID',
		width:20*6,
	});
	
	var sellPersonCob =new Ext.form.TextField({
        allowBlank: false
	});
	sellPersonCob.on('focus', function() {
		selectoperUserFun();
	});
	cm.push({
		dataIndex:'sellPerson-name',
		header:"实验员",
		width:20*6,
		hidden : false,
		editor : sellPersonCob
	});
	
//	var storeStateCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [  [ '1', "是" ],[ '0', '否' ]]
//	});
//	var stateCob = new Ext.form.ComboBox({
//		store : storeStateCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'tp',
//		hidden : false,
//		header:"数据是否已释放",
//		width:100,
//		editor : stateCob,
//		renderer :Ext.util.Format.comboRenderer(stateCob)
//	});
//	
	
//	var storegenotypeCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [  [ '1', '是' ], [ '0', '否' ] ]
//	});
//	var genotypeCob = new Ext.form.ComboBox({
//		store : storegenotypeCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'genotype',
//		hidden : false,
//		header:"是否已发送报告",
//		width:20*6,
//		editor : genotypeCob,
//		renderer : Ext.util.Format.comboRenderer(genotypeCob)
//	});
	cm.push({
		dataIndex:'zkqreport-id',
		hidden : true,
		header:'相关主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'zkqreport-name',
		hidden : true,
		header:'相关主表',
		width:50*10
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/zkqreport/zkqReport/showZkqReportItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="报告明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/zkqreport/zkqReport/delZkqReportItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
//				zkqReportItemGrid.getStore().commitChanges();
//				zkqReportItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : "选择项目",
		handler : selectProject
	});
	opts.tbar.push({
		text : '选择实验员',
		handler : selectoperUserFun
	});
	
//	opts.tbar.push({
//		text : "批量是否已释放数据",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_tp_div"), "是否已释放数据", null, {
//				"Confirm" : function() {
//					var records = zkqReportItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var tp = $("#tp").val();
//						zkqReportItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("tp", tp);
//						});
//						zkqReportItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "批量是否已发送报告",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_genotype_div"),"是否已发送报告", null, {
//				"Confirm" : function() {
//					var records = zkqReportItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var genotype = $("#genotype").val();
//						zkqReportItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("genotype", genotype);
//						});
//						zkqReportItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	
	
	zkqReportItemGrid=gridEditTable("zkqReportItemdiv",cols,loadParam,opts);
	$("#zkqReportItemdiv").data("zkqReportItemGrid", zkqReportItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})


//选择项目
var loadProject;
function selectProject() {
var options = {};
options.width = document.body.clientWidth-800;
options.height = document.body.clientHeight-40;
loadProject=loadDialogPage(null, "选择项目", "/crm/project/selProjectList.action", {
	"Confirm" : function() {
		var operGrid = $("#show_dialog_project_div").data("projectDialogGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		var records = zkqReportItemGrid.getSelectRecord();
		if (selectRecord.length > 0) {
//			$.each(records, function(i, obj) {
				$.each(selectRecord, function(a, b) {
					var ob = zkqReportItemGrid.getStore().recordType;
					zkqReportItemGrid.stopEditing();
					var p = new ob({});
					p.isNew = true;
					p.set("project-id",b.get("id"));//项目编号
					p.set("project-name",b.get("name"));//项目名称
					p.set("crmcustomer-id",b.get("mainProject-id"));//客户ID
					p.set("crmcustomer-name",b.get("mainProject-name"));//客户名称
					p.set("crmdoctor-id",b.get("crmDoctor-id"));//医生ID
					p.set("crmdoctor-name",b.get("crmDoctor-name"));//医生名称
					zkqReportItemGrid.getStore().add(p);
					zkqReportItemGrid.startEditing(0,0);
	
				});
//			});
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		$(this).dialog("close");
	}
}, true, options);
}

function setDicProject(){
	var operGrid = $("#show_dialog_project_div").data("projectDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
			$.each(selectRecord, function(a, b) {
				var ob = zkqReportItemGrid.getStore().recordType;
				zkqReportItemGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("project-id",b.get("id"));//项目编号
				p.set("project-name",b.get("name"));//项目名称
				p.set("crmcustomer-id",b.get("mainProject-id"));//客户ID
				p.set("crmcustomer-name",b.get("mainProject-name"));//客户名称
				p.set("crmdoctor-id",b.get("crmDoctor-id"));//医生ID
				p.set("crmdoctor-name",b.get("crmDoctor-name"));//医生名称
				zkqReportItemGrid.getStore().add(p);
				zkqReportItemGrid.startEditing(0,0);

			});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadProject.dialog("close");
}

//选择实验员

function selectoperUserFun(){
	var win = Ext.getCmp('selectoperUser');
	if (win) {win.close();}
	var selectoperUser= new Ext.Window({
	id:'selectoperUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=returnedPerson' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectoperUser.close(); }  }]  });    
	selectoperUser.show(); }

	function setreturnedPerson(id,name){
		var gridGrid = zkqReportItemGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('sellPerson-id',id);
			obj.set('sellPerson-name',name);
		});
		var win = Ext.getCmp('selectoperUser')
		if(win){
			win.close();
		}
	}
	


