var ReportTab;	
$(function() {
	var options = table(true, "",
			"/reports/showSampleReportListJson.action", [ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "note",
				"title" : biolims.common.name,
			}, {
				"data" : "sendUser-name",
				"title" : biolims.common.createUserName,
			}, {
				"data" : "sendDate",
				"title" : biolims.common.createDate,
			}, {
				"data" : "confirmDate",
				"title" : biolims.common.confirmDate,
			}, {
				"data" : "acceptUser-name",
				"title" : biolims.report.acceptUserName,
			}, {
				"data" : "qcUser-name",
				"title" : biolims.wk.confirmUserName,
			}, {
				"data" : "scopeName",
				"title" : biolims.purchase.costCenter,
			}, {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	ReportTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(ReportTab);
	})
});
function add() {
	window.location = window.ctx
			+ '/reports/toEditSampleReport.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/reports/toEditSampleReport.action?id=' + id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
//	$("#maincontentframe", window.parent.document)[0].src = window.ctx
//			+ "/reports/showBloodSplitSteps.action?id="
//			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"searchName" : "confirmDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "confirmDate##@@##2"
	}, {
		"type" : "table",
		"table" : ReportTab
	} ];
}