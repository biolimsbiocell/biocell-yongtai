$(function() {
	
	var id=$("#sr_state").val();
	if(id=="3"){ 
//		load("/report/showSampleReportTempList.action", {
//		 state : ''
//	 }, $("#left"), null);
		load("/reports/showSampleReportTempList.action", null, "#left");
		$("#markup").css("width","100%");
	}else{
		$("#left").remove();
	}
//	 load("/report/showSampleReportTempList.action", {
//	 state : ''
//	 }, $("#left"), null);

//	load("/report/toEditSampleReport.action", {
//		id : $("#report_id").val(),
//		bpmTaskId :$("#bpmTaskId").val()
//	}, $("#report_div"), null);

	load("/reports/showSampleReportItemList.action", null, $("#report_item_div"), null);

	var handlemethod = $("#handlemethod").val();
	if ('view' === handlemethod) {
		setTimeout(function() {
			settextreadonlyByAllJquery();
		}, 1000);
	} else if ('modify' === handlemethod) {
		setTimeout(function() {
			settextreadonlyJquery($("#sr_id"));
		}, 1000);
	}

	$("#toolbarbutton_add").click(function() {
		window.location = window.ctx + '/report/toMainframe.action?reqMethodType=add';
	});

	
	$("#toolbarbutton_tjsp").click(
			function() {
				submitWorkflow("SampleReport", {
					userId : userId,
					userName : userName,
					formId : $("#sr_id").val(),
					title : $("#sr_note").val()
				}, function() {
					window.location.reload();
				});
			});
	$("#toolbarbutton_sp").click(function() {
		//commonWorkflowLook("applicationTypeTableId=plasmaTask&formId=" + $("#task_id").val());
		completeTask($("#sr_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
	});
	$("#toolbarbutton_status").click(function(){
		commonChangeState("formId=" + $("#sr_id").val() + "&tableId=SampleReport");
	});
	
	$("#toolbarbutton_list").click(function() {
		window.location = window.ctx + '/reports/showSampleReportList.action';
	});
	$("#toolbarbutton_save").click(function() {
		var outId = $("#sr_id").val();
		if (!outId) {
			message(biolims.report.reportIdIsEmpty);
			return;
		}
		var reportgrid = $("#report_item_grid_div").data("reportgrid");
		//var getReportItemData = $("#report_item_grid_div").data("getReportItemData");
		var resultData = commonGetModifyRecords(reportgrid);
		//if (typeof getReportItemData == 'function') {
			//var resultData = getReportItemData();
			$("#item_data_json").val(resultData);
			//$("#toolbarbutton_save").hide();
			$("#report_form").attr("action", "/reports/save.action").submit();
		//}
	});
	new Ext.TabPanel({
		renderTo : 'maintab',
		height : document.body.clientHeight-29,
		autoWidth : true,
		activeTab : 0,
		margins : '0 0 0 0',
		items : [ {
			title : biolims.report.generateReport,
			autoWidth : true,
			contentEl : 'markup'

		} ]
	});
	
	
});