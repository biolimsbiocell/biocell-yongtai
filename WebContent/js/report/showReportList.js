var oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	})
	colOpts.push({
		"data": "patientId",
		"title": biolims.report.patientId,
		"createdCell": function(td) {
			$(td).attr("saveName", "patientId");
		}
	});
	colOpts.push({
		"data": "orderNum",
		"title": biolims.purchase.orderNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "orderNum");
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.master.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "completeDate",
		"title": biolims.report.completeDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "completeDate");
		}
	})
	colOpts.push({
		"data": "sendDate",
		"title":biolims.report.sendDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "sendDate");
		}
	})
	colOpts.push({
		"data": "report-id",
		"title": biolims.report.reportId,
		"createdCell": function(td) {
			$(td).attr("saveName", "report-id");
		}
	})
	colOpts.push({
		"data": "expressCompany",
		"title": biolims.sample.companyName,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "expressCompany");
		}
	})
	colOpts.push({
		"data": "emsId",
		"className":"edit",
		"title": biolims.sample.expreceCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "emsId");
		}
	})
	colOpts.push({
		"data": "arc",
		"title": biolims.report.ifSent,
		"name":biolims.common.yes+"|"+biolims.common.no,
		"className":"select",
		"createdCell": function(td) {
			$(td).attr("saveName", "arc");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			else if(data == "0") {
				return biolims.common.no;
			}else {
				return "";
			}
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-floppy-open"></i>' + biolims.common.uploadAttachment,
		action: function() {
			var id = $(".selected").find("input").val();
			if(!id) {
				top.layer.msg(biolims.common.pleaseSelect);
				return false;
			}else{
				$("#uploadFile").modal("show");
				$(".fileinput-remove").click();
				var csvFileInput = fileInput('1', 'SampleReportItem', id);
			}
		}
	});
	tbarOpts.push({
		text: biolims.report.checkReport,
		action: function() {
			var id = $(".selected").find("input").val();
			if(id){
				window.open(window.ctx + '/operfile/downloadByContentId.action?id='+id,'','');
			}
		}
	});
	tbarOpts.push({
		text: biolims.common.search,
		action: function() {
			search();
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem();
		}
	});
	var ops = table(true, null, "/sample/report/main/showSampleMainReportListJson.action", colOpts, tbarOpts);
	reportSendTab = renderData($("#reportSendDiv"), ops);
	reportSendTab.on('draw', function() {
		oldChangeLog = reportSendTab.ajax.json();
	});
	$('#reportSendDiv').on('init.dt', function() {
		recoverSearchContent(reportSendTab);
	})
});
// 保存
function saveItem() {
	var ele=$("#reportSendDiv");
	var changeLog = "报告发送：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="报告发送："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/sample/report/main/saveSampleReportItemList.action',
		data: {
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			if(k == "arc"){
				var arc = $(tds[j]).text();
				if(arc == biolims.common.no) {
					json[k] = "0";
				} else if(arc == biolims.common.yes) {
					json[k] = "1";
				}
				continue;	
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}


/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/09
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
//弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.sample.orderNum,
		"type" : "input",
		"searchName" : "orderNum",
	}, {
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	}, {
		"txt" : biolims.report.ifSent,
		"type": "select",
		"options": biolims.report.reportId+"|"+ biolims.common.yes+"|"+biolims.common.no,
		"changeOpt": "''|1|0|",
		"searchName": "arc"
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	},{
		"type" : "table",
		"table" : reportSendTab
	} ];
}