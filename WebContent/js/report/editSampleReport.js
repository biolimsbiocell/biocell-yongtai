var sampleReportTempTab;
$(function() {
	var colOpts = []
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": biolims.common.id,
	})
	colOpts.push({
		"data": "code",
		"visible":false,
		"title": biolims.common.code,
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
	})
	colOpts.push({
		"data": "orderNum",
		"title": biolims.sample.orderNum,
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
	})
	colOpts.push({
		"data": "testFroms",
		"title": biolims.common.platformInflow,
	})
	colOpts.push({
		"data": "testCode",
		"title": biolims.common.platformCode,
	})
	var tbarOpts = []
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>' + biolims.common.search,
		action: function() {
			search()
		}
	});
	var options = table(true, null, "/reports/showSampleReportTempListJson.action", colOpts, tbarOpts)
	//渲染样本的table
	sampleReportTempTab = renderData($("#reportTemp"), options);
	//提示样本选中数量
	var selectednum = $(".selectednum").text();
	if(!selectednum) {
		selectednum = 0;
	}
	sampleReportTempTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			$(".selectednum").text(index + parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	$('#reportTemp').on('init.dt', function() {
		recoverSearchContent(sampleReportTempTab);
	})
	$("#reportDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	save()
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//操作人的点击操作
	userHandler(selectednum);
	var handlemethod=$("#handlemethod").val();
	if(handlemethod == "view"||$("#report_state").text()=="Complete"){
		settextreadonly();
		$("#save").hide()
	}
});
//sop和实验员的点击操作
function userHandler(selectednum) {
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
		var img = $(this).children("img");
		if(img.hasClass("userChosed")) {
			img.removeClass("userChosed");
		} else {
			img.addClass("userChosed");
		}
	});
}
//保存操作
function save() {
	$("#save").click(function() {
		var changeLog = "";
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		//获得待处理样本的ID
		var sampleId = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});
		//获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			userId.push($(val).parent("li").attr("userid"));
		});
		changeLog = getChangeLog(userId, changeLog);
		//拼接主表的信息
		var main = {
			id: $("#report_id").text(),
			note: $("#report_note").val(),
			"sendUser-id": $("#report_sendUser").attr("userId"),
			sendDate: $("#report_sendDate").text(),
			reportDate:$("#reportDate").val(),
			state: $("#report_state").attr("state"),
			stateName: $("#report_state").text(),
		};
		var data = JSON.stringify(main);
		//拼接保存的data
		var info = {
			temp: sampleId,
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data,
			logInfo: changeLog
		}
		$.ajax({
			type: 'post',
			url: ctx + "/reports/save.action",
			data: info,
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					top.layer.msg(biolims.common.saveSuccess);
					window.location = window.ctx +
						'/reports/toEditSampleReport.action?id=' + data.id;
				}
			}
		});
	});
}
//下一步操作
$("#next").click(function() {
	var report_id = $("#report_id").text();
	if(report_id == "NEW") {
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/reports/showSampleReportItemList.action?id=" + report_id + "&bpmTaskId=" + $("#bpmTaskId").val();
});
//获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { //获取待处理样本的修改日志
	var sampleNew = [];
	$(".selected").each(function(i, val) {
		sampleNew.push($(val).children("td").eq(1).text());
	});
	changeLog += '"待出报告样本"新增有：' + sampleNew + ";";
	//获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '操作人:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}
//弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.sample.orderNum,
		"type" : "input",
		"searchName" : "orderNum",
	}, {
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	},{
		"type" : "table",
		"table" : sampleReportTempTab
	} ];
}