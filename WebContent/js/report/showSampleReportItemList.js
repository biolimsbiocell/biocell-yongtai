var sampleReportItemTab,oldChangeLog;
$(function() {
	var colOpts = []
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	})
	colOpts.push({
		"data": "orderNum",
		"title": biolims.sample.orderNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "orderNum");
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.master.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "reti-id",
		"title": biolims.common.templateId,
		"createdCell": function(td) {
			$(td).attr("saveName", "reti-id");
		}
	})
	colOpts.push({
		"data": "reti-name",
		"title": biolims.common.templateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "reti-name");
		}
	})
	colOpts.push({
		"data": "reti-note",
		"title": biolims.common.templatePath,
		"createdCell": function(td) {
			$(td).attr("saveName", "reti-note");
		}
	})
	colOpts.push({
		"data": "genotype",
		"title": biolims.report.ReportSending,
		"className":"select",
		"width":"120px",
		"name":biolims.report.reportBeenSent+"|"+biolims.report.reportBeenSentNoDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "genotype");
			$(td).attr("selectOpt", biolims.report.reportBeenSent+"|"+biolims.report.reportBeenSentNoDate);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.report.reportBeenSent;
			}else if(data == "0") {
				return biolims.report.reportBeenSentNoDate;
			}else{
				return "";
			}
			
		}
	})
	colOpts.push({
		"data": "noteState",
		"title": biolims.report.summaryState,
		"width":"90px",
		"className":"select",
		"name":biolims.report.standardAnalysis+"|"+biolims.report.personalizedAnalysis,
		"createdCell": function(td) {
			$(td).attr("saveName", "noteState");
			$(td).attr("selectOpt", biolims.report.standardAnalysis+"|"+biolims.report.personalizedAnalysis);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.report.standardAnalysis;
			}else if(data == "0") {
				return biolims.report.personalizedAnalysis;
			}else{
				return "";
			}
		}
	})
	colOpts.push({
		"data": "tp",
		"title": biolims.report.releaseRawData,
		"className":"select",
		"name":biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "tp");
			$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}else if(data == "0") {
				return biolims.common.no;
			}else{
				return "";
			}
		}
	})
	var tbarOpts = []
	var handlemethod=$("#handlemethod").val();
	if(handlemethod == "view"||$("#report_state").text()!="Complete"){
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#reportItem"),
				"/reports/delSampleReportItemList.action","报告生成删除样本：",$("#report_id").text());
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-floppy-open"></i>' + biolims.common.uploadAttachment,
		action: function() {
			var id = $(".selected").find("input").val();
			if(!id) {
				top.layer.msg(biolims.common.pleaseSelect);
				return false;
			}else{
				$("#uploadFile").modal("show");
				$(".fileinput-remove").click();
				var csvFileInput = fileInput('1', 'SampleReportItem', id);
			}
		}
	});}
	tbarOpts.push({
		text: biolims.report.checkReport,
		action: function() {
			var id = $(".selected").find("input").val();
			if(id){
				window.open(window.ctx + '/operfile/downloadByContentId.action?id='+id,'','');
			}
		}
	});
	var options = table(true, $("#report_id").text(), "/reports/showSampleReportItemListJson.action", colOpts, tbarOpts)
	//渲染样本的table
	sampleReportItemTab = renderData($("#reportItem"), options);
	//提示样本选中数量
	var selectednum = $(".selectednum").text();
	if(!selectednum) {
		selectednum = 0;
	}
	sampleReportItemTab.on('draw', function() {
		oldChangeLog = sampleReportItemTab.ajax.json();
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			$(".selectednum").text(index + parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleReportTempTab);
	})
	if(handlemethod == "view"||$("#report_state").text()=="Complete"){
		$("#tjsp").hide()
		$("#sp").hide()
		$("#save").hide()
		$("#changeState").hide()
	}
});
$("#pre").click(function() {
	var report_id = $("#report_id").text();
	if(report_id == "NEW") {
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/reports/toEditSampleReport.action?id=" + report_id + "&bpmTaskId=" + $("#bpmTaskId").val();
});
function saveItem() {
	var ele = $("#reportItem");
	var changeLog = "报告生成：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="报告生成："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/reports/saveSampleReportItemList.action',
		data: {
			id: $("#report_id").text(),
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})

};
//获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断男女并转换为数字
			if(k == "genotype") {
				var genotype = $(tds[j]).text();
				if(genotype == biolims.report.reportBeenSentNoDate) {
					json[k] = "0";
				} else if(genotype == biolims.report.reportBeenSent) {
					json[k] = "1";
				}
				continue;
			}
			if(k == "noteState"){
				var noteState = $(tds[j]).text();
				if(noteState == biolims.report.personalizedAnalysis) {
					json[k] = "0";
				} else if(noteState == biolims.report.standardAnalysis) {
					json[k] = "1";
				}
				continue;	
			}
			if(k == "tp"){
				var tp = $(tds[j]).text();
				if(tp == biolims.common.no) {
					json[k] = "0";
				} else if(tp == biolims.common.yes) {
					json[k] = "1";
				}
				continue;	
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//生成并下载报告文件
function createReport1(ids) {
	var selected=reportgrid.getSelectionModel().getSelections();
	var ids = [];
	var sampleids = [];
	if(selected.length>0){
		var flag=true;
		var bc=true;
		/*$.each(selected,function(i,b){
		if(b.get("reti-id")=="" || b.get("reti-id")==null
				|| b.get("reti-id")==undefined){
			flag=false;
		}else{
			flag=true;
		}
		});*/
		$.each(selected,function(i,b){
			if(b.get("id")=="" || b.get("id")==null
					|| b.get("id")==undefined){
				bc=false;
			}else{
				bc=true;
			}
		});
		if(flag){
			if(bc){
				for ( var i = 0; i < selected.length; i++) {
					if (!selected[i].isNew) {
						ids.push(selected[i].get("orderNum"));
					}
					if (!selected[i].isNew) {
						sampleids.push(selected[i].get("sampleCode"));
					}
				}
				ajax("post", "/reports/createReportFileWord.action", {
					ids : ids,
					sampleids : sampleids
				}, function(data) {
					if (data.success) {
						message(biolims.report.generateReportSuccess);
					} else {
						message(biolims.report.previewReportFailed);
					}
				}, null);
				reportgrid.getStore().commitChanges();
				reportgrid.getStore().reload();
				message(biolims.report.generateReportSuccess);
			}else{
				message(biolims.master.pleaseSaveBasic);
			}
		}else{
			message(biolims.report.templateSelect);
		}
	}else{
		message(biolims.report.selectPreview);
		return;
	}
}

function openW(id){
	window.open(window.ctx + '/sample/blood/main/toEditSampleMain.action?noButton=true&reqMethodType=view&id='+id,'','height=768,width=1366,scrollbars=yes,resizable=yes');
}
function downFile(id,bloodId){
	window.open(window.ctx+"/report/toReportEdit.action?id="+id+"&bloodId="+bloodId,'','height=768,width=1366,scrollbars=yes,resizable=yes');
}
function downFiles(id,bloodId){
	window.open(window.ctx+"/report/toReportEdits.action?id="+id+"&bloodId="+bloodId,'','height=768,width=1366,scrollbars=yes,resizable=yes');
}
function setWritingPOne(id,name){
	records = reportgrid.getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("writingPersonOne-id",id);
		obj.set("writingPersonOne-name",name);
	});
	var win = Ext.getCmp('getWritingPOne');
	if(win){win.close();}
}
function setWritingPTwo(id,name){
	records = reportgrid.getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("writingPersonTwo-id",id);
		obj.set("writingPersonTwo-name",name);
	});
	var win = Ext.getCmp('getWritingPTwo');
	if(win){win.close();}
}
function setAuditPerson(id,name){
	records = reportgrid.getSelectRecord();
	$.each(records, function(i, obj) {
		obj.set("auditPerson-id",id);
		obj.set("auditPerson-name",name);
	});
	var win = Ext.getCmp('getAuditPerson');
	if(win){win.close();}
}

//生成并下载报告文件
function createReport(ids) {
	var selected=reportgrid.getSelectionModel().getSelections();
	var ids = [];
	if(selected.length>0){
//		var flag=true;
//		$.each(selected,function(i,b){
//			if(b.get("template-id")=="" || b.get("template-id")==null
//					|| b.get("template-id")==undefined){
//				flag=false;
//				message("请选择报告模板！");
//			}else{
//				flag=true;
//			}
//		});
//		if(flag){
			for ( var i = 0; i < selected.length; i++) {
				if (!selected[i].isNew) {
					ids.push(selected[i].get("id"));
				}
			}
			ajax("post", "/reports/createReportFile.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					reportgrid.getStore().commitChanges();
					reportgrid.getStore().reload();
					message(biolims.report.generateReportSuccess);
					downFiles();
				} else {
					message(biolims.report.previewReportFailed);
				}
			}, null);
			
			
//		}
	}else{
		message(biolims.report.selectPreview);
		return;
	}
}

//下载文件
function downFiles(){
	var selectGrid=reportgrid.getSelectionModel().getSelections();
	if(selectGrid.length>0){
		$.each(selectGrid, function(i, obj) {
			var fileName="PDF"+obj.get("id");
			
			window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\form\\'+fileName+'.pdf','','');
		});
	}
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += '样本编号为"' + v.sampleCode + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function tjsp() {
	if(!$("#checkUser_id").val()){
		top.layer.open({
			title: biolims.sampleOut.acceptUserName,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
			yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
				$("#checkUser_name").text(name);
				$("#checkUser_id").val(id);
				saveItem();
				top.layer.close(index);
			},
			end:function(){
				splc();
			}
		})
	}else{
		splc();
	}
}
function splc(){
	top.layer.confirm(
			biolims.common.pleaseConfirmSaveBeforeSubmit,
			{
				icon : 3,
				title : biolims.common.prompt,
				btn:biolims.common.selected
			},
			function(index) {
				top.layer.open({
							title : biolims.common.approvalTask,
							type : 2,
							anim : 2,
							area : [ '800px', '500px' ],
							btn : biolims.common.selected,
							content : window.ctx
									+ "/workflow/processinstance/toStartView.action?formName=DnaTask",
							yes : function(index, layer) {
								var datas = {
									userId : userId,
									userName : userName,
									formId : dnaTask_id,
									title : $("#report_note").val(),
									formName : "SampleReport"
								}
								ajax(
										"post",
										"/workflow/processinstance/start.action",
										datas,
										function(data) {
											if (data.success) {
												top.layer.msg(biolims.common.submitSuccess);
												top.layer.close(index);
												if (typeof callback == 'function') {
													callback(data);
												}
											} else {
												top.layer
														.msg(biolims.common.submitFail);
											}
										}, null);
							}

						});
			});
}
function sp(){
	var taskId = $("#bpmTaskId").val();
	var formId = $("#report_id").text();

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}
	});
}
function changeState(){

    var	id=$("#report_id").text()
	var paraStr = "formId=" + id +
		"&tableId=SampleReport";
	top.layer.open({
		title: biolims.common.approvalProcess,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		}

	});

}