$(function() {
	
	$("#birtVersion_testingTime").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		language: 'zh-CN',
	}); 
	$("#birtVersion_validityPeriod").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		language: 'zh-CN',
	}); 
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#birtVersion_state").val()=="2") {
		settextreadonly();
	}
	if(handlemethod == "modify") {

		if($("#birtVersion_id_id").val()==null||$("#birtVersion_id_id").val()==""){
			$("#birtVersion_id").prop("readonly", false);
		}else{
			$("#birtVersion_id").prop("readonly", "readonly");
		}
	}else{
		if($("#birtVersion_id_id").val()==null||$("#birtVersion_id_id").val()==""){
			$("#birtVersion_id").prop("readonly", false);
		}else{
			$("#birtVersion_id").prop("readonly", "readonly");
		}
	}
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'birtVersion', $("#birtVersion_id").val());

});

/**
 * onchange（参数）
 * 
 * @param id
 */
function change(id) {
	$("#" + id).css({
		"background-color": "white",
		"color": "black"
	});
}
//列表
function list() {
	window.location = window.ctx +
		'/stamp/birtVersion/showBirtVersion.action';
}

function validId() {
	$.ajax({
		type: "post",
		url: window.ctx + '/common/hasId.action',
		async: false,
		data: {
			id: $("#birtVersion_id").val(),
			obj: 'BirtVersion'
		},
		success: function(data) {
			var obj = JSON.parse(data);
			if(obj.success) {
				if(obj.bool) {
					bool2 = true;
				} else {
					top.layer.msg(obj.msg);
				}
			} else {
				top.layer.msg(biolims.common.checkingFieldCodingFailure);
			}
		}
	});
}
function save() {
	var flag=true;
	var arr=[];
	var ttr=[];
	var length =$("#birtVersionItemTable tbody tr").length
	$("#birtVersionItemTable tbody tr").each(function(i,j,k){
		var startTime=$(this).find("td[savename='startTime']").text();
		var endTime=$(this).find("td[savename='endTime']").text();
//		var version =$(this).find("td[savename='version']").text();
//		arr.push(version);
		// 判断开始时间是否小于结束时间 
		var sDate = new Date(startTime.replace(/\-/g, "\/"));
		var eDate = new Date(endTime.replace(/\-/g, "\/"));
		if(sDate > eDate){
				top.layer.msg("结束日期不能小于开始日期！");
			
				flag=false;
			 }
	    })
	//去重
//	 for(var i=0; i<arr.length; i++){
//         for(var j=i+1; j<arr.length; j++){
//             if(arr[i]==arr[j]){         //第一个等同于第二个，splice方法删除第二个
//                 arr.splice(j,1);
//                 j--;
//             }
//         }
//     }
//	alert()
	
	if(flag==false){
		
		return false;
	}
	
	
	if($("#birtVersion_id").val()==null
			||$("#birtVersion_id").val()==""){
		top.layer.msg("报表版本编号不能为空！");
	}else{
		
//		if(arr.length!=length){
//			top.layer.msg("版本号有重复");
//			return false;
//		}
	 var trs = $("#birtVersionItemTable").find("tbody").children(".editagain");
     trs.each(function(i,j,k){
	 var version =$(this).find("td[savename='version']").text();
	 var state =$(this).find("td[savename='state']").text();
	 if(state==""){
		arr.push(version); 
	 }

	
    })
		$.ajax({
			type: "post",
			url: "/stamp/birtVersion/findC.action",
			data: {
				"arr" : arr,
				"birtVId":$("#birtVersion_id").val(),
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.reportN) {
					//
					//自定义字段
					//拼自定义字段儿（实验记录）
					var requiredField = requiredFilter();
					if(!requiredField) {
						return false;
					}
					var changeLog = "QA审核-";
					$('input[class="form-control"]').each(function(i, v) {
						var valnew = $(v).val();
						var val = $(v).attr("changelog");
						if(val !== valnew) {
							changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
						}
					});

					document.getElementById("changeLog").value = changeLog;
					
					//子表
					var e=$("#birtVersionItemTable");
					var dataJson = savejson(e);
					document.getElementById("itemJson").value = dataJson;
					var handlemethod = $("#handlemethod").val();
					if(handlemethod == "modify" && checkSubmit() == true) {
						top.layer.load(4, {
							shade: 0.3
						});
						$("#form1").attr("action", "/stamp/birtVersion/save.action");
						$("#form1").submit();
						top.layer.closeAll();
					} else {
						$.ajax({
							type: "post",
							url: ctx + '/common/hasId.action',
							data: {
								id: $("#birtVersion_id").val(),
								obj: 'BirtVersion'
							},
							success: function(data) {
								var data = JSON.parse(data);
								if(data.message) {
									top.layer.msg(data.message);
								} else {
									top.layer.load(4, {
										shade: 0.3
									});
									$("#form1").attr("action", "/stamp/birtVersion/save.action");
									$("#form1").submit();
									top.layer.closeAll();
								}
							}
						});
					}

				}else{
					top.layer.msg("版本号有重复");	
				}
			
			}
		});
	
	}

	
}

var changeId = $("#changeId").val();


function checkSubmit() {
	if($("#birtVersion_id").val() == null || $("#birtVersion_id").val() == "") {
		top.layer.msg(biolims.common.codeNotEmpty);
		return false;
	};
	return true;
}

function showModels(){
	top.layer.open({
		title: "选择模块",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=md", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#birtVersion_models_id").val(id);
			$("#birtVersion_models_name").val(name);
		},
	})
	
	
}