var birtVersionItem;
var oldbirtVersionItem;
$(function() {
	// 加载子表
	var id = document.getElementById('birtVersion_id').value;
	
	
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data":"id",
		"title": "编码 ",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false,	
		
	});
	colOpts.push({
		"data":"version",
		"title": "版本号",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "version");
	    },
	});
	colOpts.push({
		"data":"reportN",
		"title": "报告名称",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "reportN");
	    },
	});
	colOpts.push({
		"data":"startTime",
		"title": "开始时间",
		"className": "date",
		"createdCell": function(td) {
			$(td).attr("saveName", "startTime");
	    },
	});
	colOpts.push({
		"data":"endTime",
		"title": "结束时间",
		"className": "date",
		"createdCell": function(td) {
			$(td).attr("saveName", "endTime");
	    },
	});
	colOpts.push({
		"data" : "state",
		"title" : "是否作废",
		"className" : "select",
		"name" : biolims.common.yes + "|" + biolims.common.no,
		"createdCell" : function(td) {
			$(td).attr("saveName", "state");
			$(td).attr("selectOpt",
					biolims.common.yes + "|" + biolims.common.no);
		},
		"render" : function(data, type, full, meta) {
			if (data == biolims.common.yes) {
				return biolims.common.yes;
			}
			if (data == biolims.common.no) {
				return biolims.common.no;
			} else {
				return '否';
			}
		}
	})
	
	

	var handlemethod = $("#handlemethod").val();
//按钮
	if(handlemethod != "view") {
		tbarOpts.push({
			text: "添加明细",
			action: function() {
				addItem($("#birtVersionItemTable"))
			}
		});
		tbarOpts
		.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked(
						$("#birtVersionItemTable"),
						"/stamp/birtVersion/delbv.action",
						"删除文档：", id,birtVersionItem);
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#birtVersionItemTable"))
			}
		});
		
	}
	
	var birtVItem = table(true,
		id,
		'/stamp/birtVersion/showBirtVersionItemTableJson.action', colOpts, tbarOpts)
	birtVersionItem = renderData($("#birtVersionItemTable"), birtVItem);
	birtVersionItem.on('draw', function() {
		oldbirtVersionItem = birtVersionItem.ajax.json();
	});
});	
	
//json
function savejson(ele) {

		var trs = ele.find("tbody").children(".editagain");
		var data = [];
		var flag = true;

		var trss = ele.find("tbody tr");
		trss.each(function(i, val) {
			var json = {};
			var tds = $(val).children("td");
			json["id"] = $(tds[0]).find("input").val();
			for (var j = 1; j < tds.length; j++) {
				var k = $(tds[j]).attr("savename");
				json[k] = $(tds[j]).text();
				}
			data.push(json);
			})
			return JSON.stringify(data);
		}

//$("#birtVersionItemTable tbody tr").each(function(i,j,k){
//	alter($(this).find("td[savename='startTime']"));
//	
//	changeDateInput($(this).find("td[savename='startTime']"))
//	changeDateInput($(this).find("td[savename='endTime']"))
//	
//})
//行内input编辑
function edittable() {
	document.addEventListener('click', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);
				//console.log("123456")
			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}
	}, false)
	document.addEventListener('touchstart', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);

			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}

	}, false)
	//变普通input输入框
	function changeTextInput(ele) {
		$(ele).css({
			"padding": "0px"
		});
		var width = $(ele).css("width");
		var value = ele.innerText;
		//console.log(value);
		var ipt = $('<input type="text" id="edit">');
		ipt.css({
			"width": width,
			"height": "32px",
		});
		$(ele).html(ipt.val(value));
		ipt.focus();
		ipt.select();
		$(ipt).click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			var tdIndex = $(ele).parent("tr").children("td").index($(ele));
			if(e.keyCode == 38) { //上：38 下：40 左：39 右：37 回车 ：13
				var shangtr = $(ele).parent("tr").prev("tr");
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				if(shangtr.length) {
					var ele1 = shangtr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 40||e.keyCode==13) {
				var xiatr = $(ele).parent("tr").next("tr");
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				if(xiatr.length) {
					var ele1 = xiatr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 39) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var ele1 = $(ele).next("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.next("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}

			} else if(e.keyCode == 37) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var ele1 = $(ele).prev("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.prev("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}
			}
		}

		//		document.onkeydown = function(event) {
		//			var e = event || window.event || arguments.callee.caller.arguments[0];
		//			if(e.keyCode === 9) {
		//				var width = $("#edit").width();
		//				$("#edit").parent("td").css({
		//					"padding": "5px 0px 5px 5px",
		//					"overflow": "hidden",
		//					"text-overflow": "ellipsis",
		//					"max-width": width + "px",
		//					"box-shadow": "0px 0px 1px #17C697"
		//				});
		//				$("#edit").parents("tr").addClass("editagain");
		//				$("#edit").parent("td").html($("#edit").val());
		//				var next = $(ele).next("td");
		//				while(next.hasClass("undefined")) {
		//					next = next.next("td")
		//				}
		//				if(next.length) {
		//					next.click();
		//				} else {
		//					return false;
		//				}
		//
		//			}
		//
		//		};
	}
	//变普通Textarea输入框
	function changeTextTextarea(ele) {
		if($(ele).hasClass("edited")) {
			var width = $(ele).width() + "px";
		} else {
			var width = $(ele).width() + 10 + "px";
		}
		ele.style.padding = "0";
		$(ele).css({
			"min-width": width
		});
		var ipt = $('<textarea www=' + width + ' style="position: absolute;height:80px;width:200px" id="textarea">' + ele.innerText + '</textarea>');
		$(ele).html(ipt);
		$(ele).find("textarea").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var td = $("#textarea").parents("td");
				var width = $("#textarea").attr("www");
				td.css({
					"padding": "5px 0px",
					"max-width": width,
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"box-shadow": "0px 0px 1px #17C697",
				});
				td.addClass("edited");
				td.parent("tr").addClass("editagain");
				//td[0].title=$("#edit").val();
				td.html($("#textarea").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变下拉框
	function changeSelectInput(ele) {
		$(ele).css("padding", "0px");
		var width = $(ele).width();
		var selectOpt = $(ele).attr("selectopt");
		var selectOptArr = selectOpt.split("|");
		var ipt = $('<select id="select"></select>');
		var value = ele.innerText;
		selectOptArr.forEach(function(val, i) {
			if(value == val) {
				ipt.append("<option selected>" + val + "</option>");
			} else {
				ipt.append("<option>" + val + "</option>");
			}
		});
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
		//$(ele).find("select").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			var tdIndex = $(ele).parent("tr").children("td").index($(ele));
			if(e.keyCode == 38) { //上：38 下：40 左：39 右：37
				var shangtr = $(ele).parent("tr").prev("tr");
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				if(shangtr.length) {
					var ele1 = shangtr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 40||e.keyCode==13) {
				var xiatr = $(ele).parent("tr").next("tr");
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				if(xiatr.length) {
					var ele1 = xiatr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 39) {
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				var ele1 = $(ele).next("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.next("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}

			} else if(e.keyCode == 37) {
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				var ele1 = $(ele).prev("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.prev("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}
			}
		}
	}
	//变日期选择框
	function changeDateInput(ele) {
		$(ele).css("padding", "0px");
		var ipt = $('<input type="text" id="date" autofocus value=' + $(ele).text() + '>');
		var width = $(ele).width();
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
		ipt.datepicker({
			//language: "zh-TW",
			language: 'cn',
			autoclose: true, //选中之后自动隐藏日期选择框
			format: "yyyy-mm-dd" //日期格式，详见 
		});
		$(ele).find("input").click();
			document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			var tdIndex = $(ele).parent("tr").children("td").index($(ele));
			if(e.keyCode == 38) { //上：38 下：40 左：39 右：37
				var shangtr = $(ele).parent("tr").prev("tr");
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				if(shangtr.length) {
					var ele1 = shangtr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 40||e.keyCode==13) {
				var xiatr = $(ele).parent("tr").next("tr");
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				if(xiatr.length) {
					var ele1 = xiatr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 39) {
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				var ele1 = $(ele).next("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.next("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}

			} else if(e.keyCode == 37) {
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				var ele1 = $(ele).prev("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.prev("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}
			}
		}
	}
	document.onmouseup = function(event) {
		var evt = window.event || event;
		if(document.getElementById("textarea") && evt.target.id != "textarea") {
			var td = $("#textarea").parents("td");
			var width = $("#textarea").attr("www");
			td.css({
				"padding": "5px 0px",
				"max-width": width,
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"box-shadow": "0px 0px 1px #17C697",
			});
			td.addClass("edited");
			td.parent("tr").addClass("editagain");
			//td[0].title=$("#edit").val();
			td.html($("#textarea").val());
		}
		if(document.getElementById("edit") && evt.target.id != "edit") {
			var width = $("#edit").width();
			$("#edit").parent("td").css({
				"padding": "5px 0px 5px 5px",
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"max-width": width + "px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#edit").parents("tr").addClass("editagain");
			$("#edit").parent("td").html($("#edit").val());
		}
		if(document.getElementById("select") && evt.target.id != "select") {
			$("#select").parent("td").css({
				"padding": "5px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#select").parents("tr").addClass("editagain");
			$("#select").parent("td").html($("#select option:selected").val());
		}
		if(document.getElementById("date") && evt.target.id != "date") {
			if(!$(".datepicker").length) {
				$("#date").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
			}
		}
	}
}

