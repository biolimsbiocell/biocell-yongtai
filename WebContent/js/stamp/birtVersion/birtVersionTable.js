var birtVersionTable;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": "编号",
		}, {
			"data": "name",
			"title": "名称",
		},{
			"data": "models-name",
			"title": "模块",
		}];
		
	var options = table(true, "",
		"/stamp/birtVersion/showBirtVersionTableJson.action",colData , null)
	birtVersionTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(birtVersionTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/stamp/birtVersion/editBirtVersionTable.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/stamp/birtVersion/editBirtVersionTable.action?id=' + id ;
}
//查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/stamp/birtVersion/viewOrder.action?id=' + id + '&type=' + $("#order_type").val();
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "报表版本编号",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "名称",
			"type": "input",
			"searchName": "name"
		},
		{
			"type": "table",
			"table": birtVersionTable
		}
	];
}