﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#interpretationBloodDisease_state").val()!="3"){
		load("/interpret/interpretation/interpretationBloodDisease/showInterpretationTempList.action", {}, "#interpretationTemppage");
		$("#markup").css("width","75%");
	}
});	
function add() {
	window.location = window.ctx + "/interpret/interpretation/interpretationBloodDisease/editinterpretationBloodDisease.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/interpret/interpretation/interpretationBloodDisease/showinterpretationBloodDiseaseList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	if($("#interpretationBloodDisease_id").val()==""){
		message("编号不能为空!");
		return;
	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#interpretationBloodDisease", {
					userId : userId,
					userName : userName,
					formId : $("#interpretationBloodDisease_id").val(),
					title : $("#interpretationBloodDisease_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#interpretationBloodDisease_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var interpretationItemDivData = $("#interpretationItemdiv").data("interpretationItemGrid");
		document.getElementById('interpretationItemJson').value = commonGetModifyRecords(interpretationItemDivData);
	    var interpretationCourseDivData = $("#interpretationCoursediv").data("interpretationCourseGrid");
		document.getElementById('interpretationCourseJson').value = commonGetModifyRecords(interpretationCourseDivData);
//	    var interpretationBackDivData = $("#interpretationBackdiv").data("interpretationBackGrid");
//		document.getElementById('interpretationBackJson').value = commonGetModifyRecords(interpretationBackDivData);
//	    var interpretationInfoDivData = $("#interpretationInfodiv").data("interpretationInfoGrid");
//		document.getElementById('interpretationInfoJson').value = commonGetModifyRecords(interpretationInfoDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/interpret/interpretation/interpretationBloodDisease/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/interpret/interpretation/interpretationBloodDisease/copyinterpretationBloodDisease.action?id=' + $("#interpretationBloodDisease_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#interpretationBloodDisease_id").val() + "&tableId=interpretationBloodDisease");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'白血病解读',
	    	   contentEl:'markup'
	       } ]
	   });
});
//load("/interpret/interpretation/interpretationBloodDisease/showInterpretationItemList.action", {
//				id : $("#interpretationBloodDisease_id").val()
//			}, "#interpretationItempage");
//load("/interpret/interpretation/interpretationBloodDisease/showInterpretationCourseList.action", {
//				id : $("#interpretationBloodDisease_id").val()
//			}, "#interpretationCoursepage");
//load("/interpret/interpretation/interpretationBloodDisease/showInterpretationBackList.action", {
//				id : $("#interpretationBloodDisease_id").val()
//			}, "#interpretationBackpage");
//load("/interpret/interpretation/interpretationBloodDisease/showInterpretationInfoList.action", {
//				id : $("#interpretationBloodDisease_id").val()
//			}, "#interpretationInfopage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);

	
function AnalysisTaskFun(){
		var win = Ext.getCmp('AnalysisTaskFun');
		if (win) {win.close();}
		var AnalysisTaskFun= new Ext.Window({
		id:'AnalysisTaskFun',modal:true,title:'选择分析方案',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/analysis/analy/analysisTask/analysisTaskSelect.action?flag=AnalysisTaskFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 AnalysisTaskFun.close(); }  }]  });     AnalysisTaskFun.show(); 
}

