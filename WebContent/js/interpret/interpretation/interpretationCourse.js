﻿
var interpretationCourseGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'karyotype',
		type:"string"
	});
	   fields.push({
		name:'chrSite',
		type:"string"
	});
	   fields.push({
		name:'area',
		type:"string"
	});
//	   fields.push({
//		name:'size',
//		type:"string"
//	});
	   ///////////////////////
	   fields.push({
		name:'resultOne',
		type:"string"
	});
	   fields.push({
		name:'resultTwo',
		type:"string"
	});
	   fields.push({
		name:'cnv',
		type:"string"
	});
	   fields.push({
		name:'readsMb',
		type:"string"
	});
	   fields.push({
		name:'gcContent',
		type:"string"
	});
	   fields.push({
		name:'q30Ratio',
		type:"string"
	});
	   fields.push({
		name:'urRatio',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'alignRatio',
		type:"string"
	});
	   ///////////////////////////////

	   fields.push({
		name:'gainLoss',
		type:"string"
	});
	   fields.push({
		name:'syndrome',
		type:"string"
	});
	   fields.push({
		name:'gene',
		type:"string"
	});
	   fields.push({
		name:'dgv',
		type:"string"
	});
	   fields.push({
		name:'omim',
		type:"string"
	});
	   fields.push({
		name:'combined',
		type:"string"
	});
	   fields.push({
		name:'isgood',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'readingResult',
		type:"string"
	});
	    fields.push({
		name:'interpretationTask-id',
		type:"string"
	});
	    fields.push({
		name:'interpretationTask-name',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
////////////////////////////////////////////////
	cm.push({
		dataIndex:'resultOne',
		hidden : false,
		header:'result1',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'resultTwo',
		hidden : false,
		header:'result2',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'cnv',
		hidden : false,
		header:'cnv',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'readsMb',
		hidden : false,
		header:'reads_mb',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'gcContent',
		hidden : false,
		header:'gc_content',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'q30Ratio',
		hidden : false,
		header:'q30_ratio',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'alignRatio',
		hidden : false,
		header:'align_ratio',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'urRatio',
		hidden : false,
		header:'ur_ratio',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'信息分析结果',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	////////////////////////////////////////////////
	cm.push({
		dataIndex:'karyotype',
		hidden : false,
		header:'karyotype',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'chrSite',
		hidden : false,
		header:'chr:site',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'area',
		hidden : false,
		header:'area',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'size',
//		hidden : false,
//		header:'size',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'gainLoss',
		hidden : false,
		header:'gain/loss',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'syndrome',
		hidden : false,
		header:'syndrome',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'gene',
		hidden : false,
		header:'gene',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dgv',
		hidden : false,
		header:'dgv',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'omim',
		hidden : false,
		header:'omim',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'combined',
		hidden : false,
		header:'combined',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isgood',
		hidden : false,
		header:'是否作图',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
//	cm.push({
//		dataIndex:'isgood',
//		hidden : false,
//		header:'结果判定',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'method',
//		hidden : false,
//		header:'处理方式',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'readingResult',
		hidden : false,
		header:'解读结果',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'关联中间表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'interpretationTask-id',
		hidden : true,
		header:'关联主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'interpretationTask-name',
		hidden : true,
		header:'关联主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/interpretation/interpretationTask/showInterpretationCourseListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="解读过程";
	opts.height =  document.body.clientHeight-260;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/interpret/interpretation/interpretationTask/delInterpretationCourse.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//    
//	opts.tbar.push({
//			text : '选择关联主表',
//			handler : selectinterpretationTaskFun
//		});
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = interpretationCourseGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 12-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 13-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 14-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 15-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 16-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							interpretationCourseGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
//	opts.tbar.push({
//		iconCls : 'save',
//		text : '保存',
//		handler : save
//	});
	interpretationCourseGrid=gridEditTable("interpretationCoursediv",cols,loadParam,opts);
	$("#interpretationCoursediv").data("interpretationCourseGrid", interpretationCourseGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//保存
//function save(){	
//	var selectRecord = interpretationCourseGrid.getSelectionModel();
//	var inItemGrid = $("#interpretationCoursediv").data("interpretationCourseGrid");
//	var itemJson = commonGetModifyRecords(inItemGrid);
//	if(selectRecord.getSelections().length>0){
//		$.each(selectRecord.getSelections(), function(i, obj) {
//			ajax("post", "/interpret/interpretation/interpretationTask/saveInterpretationCourse.action", {
//				itemDataJson : itemJson
//			}, function(data) {
//				if (data.success) {
//					$("#interpretationCoursediv").data("interpretationCourseGrid").getStore().commitChanges();
//					$("#interpretationCoursediv").data("interpretationCourseGrid").getStore().reload();
//					message("保存成功！");
//				} else {
//					message("保存失败！");
//				}
//			}, null);			
//		});
//	}else{
//		message("没有需要保存的数据！");
//	}
//}
	
