﻿var interpretationItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'isgood',
		type:"string"
	});
	    fields.push({
		name:'method-id',
		type:"string"
	});
    fields.push({
		name:'method-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   ///////////////////////
	   fields.push({
			name:'testNum',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'wkCode',
			type:"string"
		});
	   fields.push({
			name:'sampleReceiveDate',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'reportStopDate',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'onTime',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'perOffTime',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'acceptCnvDate',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'readEndDate',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'acceptImgDate',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'reportSendDate',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'abnormalSample',
			type:"string"
		});
	   fields.push({
			name:'reportState',
			type:"string"
		});
	   fields.push({
			name:'reportPostDate',
			type:"date",
			dateFormat:'Y-m-d H:i:s'
		});
	   fields.push({
			name:'patientName',
			type:"string"
		});
	   fields.push({
			name:'idCard',
			type:"string"
		});
	   fields.push({
			name:'businessType-id',
			type:"string"
		});
//	   fields.push({
//			name:'businessType-name',
//			type:"string"
//		});
	   fields.push({
			name:'hospital',
			type:"string"
		});
	   
	   fields.push({
			name:'payType-id',
			type:"string"
		});	  
//	   fields.push({
//			name:'payType-name',
//			type:"string"
//		});
		   fields.push({
				name:'productId',
				type:"string"
			});
		   fields.push({
				name:'productName',
				type:"string"
			});
		   fields.push({
				name:'sampleTime',
				type:"string"
			});
		   fields.push({
				name:'spellName',
				type:"string"
			});
		   fields.push({
				name:'gender',
				type:"string"
			});
		   fields.push({
				name:'age',
				type:"string"
			});
		   fields.push({
				name:'birthAddress',
				type:"string"
			});
		   fields.push({
				name:'nation',
				type:"string"
			});
		   fields.push({
				name:'hight',
				type:"string"
			});
		   fields.push({
				name:'weight',
				type:"string"
			});
		   fields.push({
				name:'marriage',
				type:"string"
			});
		   fields.push({
				name:'cardType',
				type:"string"
			});
		   fields.push({
				name:'cardNumber',
				type:"string"
			});
		   fields.push({
				name:'tele',
				type:"string"
			});
		   fields.push({
				name:'email',
				type:"string"
			});
		   fields.push({
				name:'familyAddress',
				type:"string"
			});
		   fields.push({
				name:'isFee',
				type:"string"
			});
		   fields.push({
				name:'privilege',
				type:"string"
			});
		   fields.push({
				name:'referUser',
				type:"string"
			});
		   fields.push({
				name:'billNumber',
				type:"string"
			});
		   fields.push({
				name:'billTitle',
				type:"string"
			});
		   fields.push({
				name:'inputUser',
				type:"string"
			});
		   fields.push({
				name:'confirmUser1',
				type:"string"
			});
		   fields.push({
				name:'confirmUser2',
				type:"string"
			});
		   fields.push({
				name:'receiveDate',
				type:"string"
			});
		   fields.push({
				name:'area',
				type:"string"
			});
		   fields.push({
				name:'fee',
				type:"string"
			});
		   fields.push({
				name:'medicalCard',
				type:"string"
			});
		   fields.push({
				name:'inspectDate',
				type:"string"
			});
		   fields.push({
				name:'reportDate',
				type:"string"
			});
		   fields.push({
				name:'files-id',
				type:"string"
			});
//		   fields.push({
//				name:'files-name',
//				type:"string"
//			});
	   ///////////////////////
	    fields.push({
		name:'interpretationTask-id',
		type:"string"
	});
	    fields.push({
		name:'interpretationTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'isgood',
		hidden : false,
		header:'结果判定',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'method-id',
		hidden : true,
		header:'处理方式ID',
		width:20*10
	});
	cm.push({
		dataIndex:'method-name',
		hidden : false,
		header:'处理方式',
		width:20*10
	});
	////////////////////////////
	cm.push({
		dataIndex:'testNum',
		hidden : false,
		header:'实验期次',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:'文库编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleReceiveDate',
		hidden : false,
		header:'样本接收日期',
		width:20*6,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'reportStopDate',
		hidden : false,
		header:'报告截止日期',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'onTime',
		hidden : false,
		header:'上机时间',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'perOffTime',
		hidden : false,
		header:'预下机时间',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'acceptCnvDate',
		hidden : false,
		header:'收到CNV数据日期',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'readEndDate',
		hidden : false,
		header:'解读完成日期',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'acceptImgDate',
		hidden : false,
		header:'收到图片日期',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'reportSendDate',
		hidden : false,
		header:'报告发客服日期',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	cm.push({
		dataIndex:'abnormalSample',
		hidden : false,
		header:'异常样本',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportState',
		hidden : false,
		header:'报告状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportPostDate',
		hidden : false,
		header:'报告发出日期',
		width:20*6,
		
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d H:i:s'})
	});
	//////////////SampleInfo
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'病人姓名',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'idCard',
		hidden : false,
		header:'身份证号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'gender',
		hidden : false,
		header:'性别',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'phone',
		hidden : false,
		header:'手机号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'businessType-id',
		hidden : true,
		header:'业务类型ID',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'businessType-name',
		hidden : false,
		header:'业务类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hospital',
		hidden : false,
		header:'送检医院',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'price',
		hidden : false,
		header:'价格',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'payType-id',
		hidden : true,
		header:'付款方式ID',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'payType-name',
		hidden : false,
		header:'付款方式',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'项目编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'项目名称',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleTime',
		hidden : false,
		header:'取样时间',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'spellName',
		hidden : false,
		header:'姓名拼音',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'age',
		hidden : false,
		header:'年龄',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'birthAddress',
		hidden : false,
		header:'籍贯',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nation',
		hidden : false,
		header:'民族',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hight',
		hidden : false,
		header:'身高',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'weight',
		hidden : false,
		header:'体重',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'marriage',
		hidden : false,
		header:'婚姻状况',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cardType',
		hidden : false,
		header:'证件类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cardNumber',
		hidden : false,
		header:'证件号码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tele',
		hidden : false,
		header:'联系方式',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'email',
		hidden : false,
		header:'邮箱',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'familyAddress',
		hidden : false,
		header:'家庭住址',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isFee',
		hidden : false,
		header:'是否收费',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'privilege',
		hidden : false,
		header:'优惠类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'referUser',
		hidden : false,
		header:'推荐人',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'billNumber',
		hidden : false,
		header:'发票号',
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'billTitle',
		hidden : false,
		header:'发票抬头',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inputUser',
		hidden : false,
		header:'录入人',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'confirmUser1',
		hidden : false,
		header:'审核人1',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'confirmUser2',
		hidden : false,
		header:'审核人2',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'receiveDate',
		hidden : false,
		header:'接收日期',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'files-id',
		hidden : true,
		header:'上传文件ID',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'files-name',
//		hidden : false,
//		header:'上传文件',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'area',
		hidden : false,
		header:'地区',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fee',
		hidden : false,
		header:'金额',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'medicalCard',
		hidden : false,
		header:'病历号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : false,
		header:'送检日期',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	////////////////////////////
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'interpretationTask-id',
		hidden : true,
		header:'关联主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'interpretationTask-name',
		hidden : true,
		header:'关联主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/interpretation/interpretationTask/showInterpretationItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="样本明细";
	opts.height =  document.body.clientHeight-260;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/interpret/interpretation/interpretationTask/delInterpretationItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				interpretationItemGrid.getStore().commitChanges();
				interpretationItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择处理方式',
			handler : selectmethodFun
		});

//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = interpretationItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							interpretationItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
//	opts.tbar.push({
//		iconCls : 'save',
//		text : '保存',
//		handler : save
//	});
	interpretationItemGrid=gridEditTable("interpretationItemdiv",cols,loadParam,opts);
	$("#interpretationItemdiv").data("interpretationItemGrid", interpretationItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectmethodFun(){
	var win = Ext.getCmp('selectmethod');
	if (win) {win.close();}
	var selectmethod= new Ext.Window({
	id:'selectmethod',modal:true,title:'选择处理方式',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=dealType' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectmethod.close(); }  }]  });     selectmethod.show(); }
	function setdealType(id,name){
		var gridGrid = $("#interpretationItemdiv").data("interpretationItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('method-id',id);
			obj.set('method-name',name);
		});
		var win = Ext.getCmp('selectmethod');
		if(win){
			win.close();
		}
	}
//保存
//function save(){	
//	var selectRecord = interpretationItemGrid.getSelectionModel();
//	var inItemGrid = $("#interpretationItemdiv").data("interpretationItemGrid");
//	var itemJson = commonGetModifyRecords(inItemGrid);
//	if(selectRecord.getSelections().length>0){
//		$.each(selectRecord.getSelections(), function(i, obj) {
//			ajax("post", "/interpret/interpretation/interpretationTask/saveInterpretationItem.action", {
//				itemDataJson : itemJson
//			}, function(data) {
//				if (data.success) {
//					$("#interpretationItemdiv").data("interpretationItemGrid").getStore().commitChanges();
//					$("#interpretationItemdiv").data("interpretationItemGrid").getStore().reload();
//					message("保存成功！");
//				} else {
//					message("保存失败！");
//				}
//			}, null);			
//		});
//	}else{
//		message("没有需要保存的数据！");
//	}
//}
	
