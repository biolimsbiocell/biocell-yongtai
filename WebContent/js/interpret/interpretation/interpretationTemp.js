﻿
var interpretationTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
   fields.push({
		name:'id',
		type:"string"
	});
   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'resultOne',
		type:"string"
	});
	   fields.push({
		name:'resultTwo',
		type:"string"
	});
	   fields.push({
		name:'cnv',
		type:"string"
	});
	   fields.push({
		name:'readsMb',
		type:"string"
	});
	   fields.push({
		name:'gcContent',
		type:"string"
	});
	   fields.push({
		name:'q30Ratio',
		type:"string"
	});
	   fields.push({
		name:'alignRatio',
		type:"string"
	});
	   fields.push({
		name:'urRatio',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'analysisInfoId',
		type:"string"
	});
//	    fields.push({
//		name:'analysisInfo-name',
//		type:"string"
//	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'resultOne',
		hidden : false,
		header:'result1',
		width:20*6
	});
	cm.push({
		dataIndex:'resultTwo',
		hidden : false,
		header:'result2',
		width:20*6
	});
	cm.push({
		dataIndex:'cnv',
		hidden : false,
		header:'cnv',
		width:20*6
	});
	cm.push({
		dataIndex:'readsMb',
		hidden : false,
		header:'reads_mb',
		width:20*6
	});
	cm.push({
		dataIndex:'gcContent',
		hidden : false,
		header:'gc_content',
		width:20*6
	});
	cm.push({
		dataIndex:'q30Ratio',
		hidden : false,
		header:'q30_ratio',
		width:20*6
	});
	cm.push({
		dataIndex:'alignRatio',
		hidden : false,
		header:'align_ratio',
		width:20*6
	});
	cm.push({
		dataIndex:'urRatio',
		hidden : false,
		header:'ur_ratio',
		width:20*6
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'分析结果',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'analysisInfoId',
		hidden : true,
		header:'关联表ID',
		width:20*10
	});
//	cm.push({
//		dataIndex:'analysisInfo-name',
//		hidden : false,
//		header:'关联表',
//		width:20*10
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/interpretation/interpretationTask/showInterpretationTempListJson.action";
	var opts={};
	opts.title="结果明细";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];

	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '添加到数据解读',
		handler : addTest
	});
	interpretationTempGrid=gridEditTable("interpretationTempdiv",cols,loadParam,opts);
	$("#interpretationTempdiv").data("interpretationTempGrid", interpretationTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function addTest(){
	var selectRecord=interpretationTempGrid.getSelectionModel();
	var selRecord=interpretationCourseGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("sampleCode");
				if(oldv == obj.get("sampleCode")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;					
				}
			}
			if(!isRepeat){
				var ob = interpretationCourseGrid.getStore().recordType;
				interpretationCourseGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("sampleCode", obj.get("sampleCode"));
				p.set("resultOne", obj.get("resultOne"));
				p.set("resultTwo", obj.get("resultTwo"));
				p.set("cnv", obj.get("cnv"));
				p.set("readsMb", obj.get("readsMb"));
				p.set("gcContent", obj.get("gcContent"));
				p.set("q30Ratio", obj.get("q30Ratio"));
				p.set("alignRatio", obj.get("alignRatio"));
				p.set("urRatio", obj.get("urRatio"));
				p.set("result", obj.get("result"));
				p.set("tempId", obj.get("id"));
//				p.set("resultOne", "1");
//				p.set("resultTwo", "2");
//				p.set("cnv", "3");
//				p.set("readsMb", "4");
//				p.set("gcContent", "5");
//				p.set("q30Ratio","6");
//				p.set("alignRatio", "7");
//				p.set("urRatio", "8");
//				p.set("result", "9");
				interpretationCourseGrid.getStore().add(p);
				interpretationCourseGrid.startEditing(0, 0);
				var tId=obj.get("sampleCode");
				ajax("post", "/interpret/interpretation/interpretationTask/setSampleInfoList.action", {
					code : tId,
				}, function(data) {
					if (data.success) {
						var ob1 = interpretationItemGrid.getStore().recordType;
						interpretationItemGrid.stopEditing();
							
						$.each(data.data, function(i, obj) {
							var p1 = new ob1({});
							p1.isNew = true;
							
							p1.set("sampleCode",obj.sCode);
							p1.set("idCard",obj.sIdCard);
							p1.set("patientName",obj.sPatientName);
							p1.set("hospital",obj.sHospital);
							p1.set("productId",obj.sProductId);
							p1.set("productName",obj.sProductName);
							p1.set("sampleType",obj.sampleType);
							p1.set("gender",obj.sGender);
							p1.set("phone",obj.sPhone);
							p1.set("businessType-id",obj.businessTypeId);
							p1.set("businessType-name",obj.businessType);
							p1.set("price",obj.sPrice);
							p1.set("payType-id",obj.typeId);
							p1.set("payType-name",obj.typeId);
							p1.set("sampleTime",obj.sSampleTime);
							p1.set("spellName",obj.sSpellName);
							p1.set("age",obj.sAge);
							p1.set("birthAddress",obj.sBirthAddress);
							p1.set("nation",obj.sNation);
							p1.set("hight",obj.sHight);
							p1.set("weight",obj.sWeight);
							p1.set("marriage",obj.sMarriage);
							p1.set("cardType",obj.sCardType);
							p1.set("cardNumber",obj.sCardNumber);
							p1.set("tele",obj.sTele);
							p1.set("email",obj.sEmail);
							p1.set("familyAddress",obj.sFamilyAddress);
							p1.set("isFee",obj.sIsFee);
							p1.set("privilege",obj.sPrivilege);
							p1.set("referUser",obj.sReferUser);
							p1.set("billNumber",obj.sBillNumber);
							p1.set("billTitle",obj.sBillTitle);
							p1.set("inputUser",obj.sInputUser);
							p1.set("confirmUser1",obj.sConfirmUser1);
							p1.set("confirmUser2",obj.sConfirmUser2);
							p1.set("receiveDate",obj.sReceiveDate);
							p1.set("files-id",obj.upId);
							p1.set("files-name",obj.upName);
							p1.set("area",obj.sArea);
							p1.set("fee",obj.sFee);
							p1.set("medicalCard",obj.sMedicalCard);
							p1.set("inspectDate",obj.sInspectDate);
							p1.set("reportDate",obj.sReportDate);
							
							interpretationItemGrid.getStore().add(p1);
							interpretationItemGrid.startEditing(0, 0);
						});
					}else{
						message("获取明细数据时发生错误！");
					}
				});
			}
		});
		
	}else{
		message("请选择样本");
	}
}