﻿
var interpretationBackGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'cnv',
		type:"string"
	});
	   fields.push({
		name:'result1',
		type:"string"
	});
	   fields.push({
		name:'result2',
		type:"string"
	});
	   fields.push({
		name:'readingResult',
		type:"string"
	});
	   fields.push({
		name:'upload',
		type:"string"
	});
	   fields.push({
		name:'isgood',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	    fields.push({
		name:'interpretationTask-id',
		type:"string"
	});
	    fields.push({
		name:'interpretationTask-name',
		type:"string"
	});
	 // 上传信息录入图片
		fields.push({
			name : 'upLoadAccessory-fileName',
			type : "string"
		});
		fields.push({
			name : 'upLoadAccessory-id',
			type : "string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cnv',
		hidden : false,
		header:'cnv',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'result1',
		hidden : false,
		header:'result1',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'result2',
		hidden : false,
		header:'result2',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'readingResult',
		hidden : false,
		header:'解读结果',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		xtype : 'actioncolumn',
//		width : 80,
//		header : '上传图片',
//		items : [ {
//			icon : window.ctx + '/images/img_attach.gif',
//			tooltip : '附件',
//			handler : function(grid, rowIndex, colIndex) {
//				var rec = grid.getStore().getAt(rowIndex);
//				if(rec.get('id')&&rec.get('id')!='NEW'){
//					var win = Ext.getCmp('doc');
//					if (win) {win.close();}
//					var doc= new Ext.Window({
//					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
//					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//					collapsible: true,maximizable: true,
//					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
//					buttons: [
//					{ text: '关闭',
//					 handler: function(){
//					 doc.close(); }  }]  }); 
//					 doc.show();
//				}else{
//					message("请先保存记录。");
//				}
//			}
//		}]
//	});
	 //鼠标聚焦触发事件
	 var code = new Ext.form.TextField({
	 allowBlank : true
	 });
	 code.on('focus', function() {
		 var isUpload = true;
			var record = interpretationBackGrid.getSelectOneRecord();
			load("/system/template/template/toSampeUpload.action", {
				fileId : record.get("upLoadAccessory-id"),
				isUpload : isUpload
			}, null, function() {
				$("#upload_file_div").data("callback", function(data) {
					record.set("upLoadAccessory-fileName", data.fileName);
					record.set("upLoadAccessory-id", data.fileId);
				});
			});
	 });
		cm.push({
			dataIndex : 'upLoadAccessory-fileName',
			header : '上传图片',
			width : 150,
			hidden : false,
			editor : code
		});
		cm.push({
			dataIndex : 'upLoadAccessory-id',
			header : '图片Id',
			width : 150,
			hidden : true
			
		});
//	cm.push({
//		dataIndex:'upload',
//		hidden : false,
//		header:'是否上传图片',
//		width:20*6,
//		
//		editor : code
//	});
	cm.push({
		dataIndex:'isgood',
		hidden : false,
		header:'结果判定',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理方式',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'interpretationTask-id',
		hidden : true,
		header:'关联主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'interpretationTask-name',
		hidden : true,
		header:'关联主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/interpretation/interpretationTask/showInterpretationBackListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="反馈作图";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/interpret/interpretation/interpretationTask/delInterpretationBack.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择关联主表',
//			handler : selectinterpretationTaskFun
//		});
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = interpretationBackGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							interpretationBackGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
//	opts.tbar.push({
//		iconCls : 'save',
//		text : '保存',
//		handler : null
//	});
	interpretationBackGrid=gridEditTable("interpretationBackdiv",cols,loadParam,opts);
	$("#interpretationBackdiv").data("interpretationBackGrid", interpretationBackGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectinterpretationTaskFun(){
	var win = Ext.getCmp('selectinterpretationTask');
	if (win) {win.close();}
	var selectinterpretationTask= new Ext.Window({
	id:'selectinterpretationTask',modal:true,title:'选择关联主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/InterpretationTaskSelect.action?flag=interpretationTask' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectinterpretationTask.close(); }  }]  });     selectinterpretationTask.show(); }
	function setinterpretationTask(id,name){
		var gridGrid = $("#interpretationBackdiv").data("interpretationBackGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('interpretationTask-id',id);
			obj.set('interpretationTask-name',name);
		});
		var win = Ext.getCmp('selectinterpretationTask');
		if(win){
			win.close();
		}
	}
	
