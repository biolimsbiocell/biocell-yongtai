﻿
var interpretationInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'cnv',
		type:"string"
	});
	   fields.push({
		name:'readingResult',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'interpretationTask-id',
		type:"string"
	});
	    fields.push({
		name:'interpretationTask-name',
		type:"string"
	});
	   fields.push({
		name:'result1',
		type:"string"
	});
	    fields.push({
		name:'result2',
		type:"string"
	});
	 // 上传图片
	fields.push({
		name : 'upLoadAccessory-fileName',
		type : "string"
	});
	fields.push({
		name : 'upLoadAccessory-id',
		type : "string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:30*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'数据解读编号',
		width:30*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'cnv',
		hidden : false,
		header:'cnv',
		width:30*6
	});
	cm.push({
		dataIndex:'result1',
		hidden : false,
		header:'result1',
		width:30*6
	});
	cm.push({
		dataIndex:'result2',
		hidden : false,
		header:'result2',
		width:30*6
	});
	cm.push({
		dataIndex:'readingResult',
		hidden : false,
		header:'解读结果',
		width:40*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
//	cm.push({
//		dataIndex:'result',
//		hidden : true,
//		header:'结果',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '通过' ], [ '1', '待反馈' ]]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理方式',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
//	cm.push({
//		dataIndex:'method',
//		hidden : false,
//		header:'处理方式',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex : 'upLoadAccessory-fileName',
		header : '上传图片',
		width : 150,
		hidden : false,
	});
	cm.push({
		dataIndex : 'upLoadAccessory-id',
		header : '图片Id',
		width : 150,
		hidden : true		
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'interpretationTask-id',
		hidden : true,
		header:'关联主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'interpretationTask-name',
		hidden : true,
		header:'关联主表',
		width:20*10
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/interpretation/interpretationInfo/showInterpretationInfoListJson.action";
	var opts={};
	opts.title="解读结果";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = interpretationInfoGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							interpretationInfoGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	interpretationInfoGrid=gridEditTable("interpretationInfodiv",cols,loadParam,opts);
	$("#interpretationInfodiv").data("interpretationInfoGrid", interpretationInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//保存
function save(){	
	var selectRecord = interpretationInfoGrid.getSelectionModel();
	var inItemGrid = $("#interpretationInfodiv").data("interpretationInfoGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/interpret/interpretation/interpretationInfo/saveInterpretationInfo.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#interpretationInfodiv").data("interpretationInfoGrid").getStore().commitChanges();
					$("#interpretationInfodiv").data("interpretationInfoGrid").getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
		});
	}else{
		message("没有需要保存的数据！");
	}
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
		
			
			commonSearchAction(interpretationInfoGrid);
			$(this).dialog("close");

		},
		"清空" : function() {
			form_reset();

		}
	}, true, option);
}