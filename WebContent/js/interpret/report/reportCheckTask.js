var reportCheckTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'acceptUser',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'createUser',
		header:'下达人',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:'下达日期',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser',
		header:'报告员',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:'报告生成日期',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'工作流id',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/report/reportCheckTask/showReportCheckTaskListJson.action";
	var opts={};
	opts.title="报告审核";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	reportCheckTaskGrid=gridTable("show_reportCheckTask_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/interpret/report/reportCheckTask/editReportCheckTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/interpret/report/reportCheckTask/editReportCheckTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/interpret/report/reportCheckTask/viewReportCheckTask.action?id=' + id;
}
function exportexcel() {
	reportCheckTaskGrid.title = '导出列表';
	var vExportContent = reportCheckTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startacceptDate").val() != undefined) && ($("#startacceptDate").val() != '')) {
					var startacceptDatestr = ">=##@@##" + $("#startacceptDate").val();
					$("#acceptDate1").val(startacceptDatestr);
				}
				if (($("#endacceptDate").val() != undefined) && ($("#endacceptDate").val() != '')) {
					var endacceptDatestr = "<=##@@##" + $("#endacceptDate").val();

					$("#acceptDate2").val(endacceptDatestr);

				}
				
				
				commonSearchAction(reportCheckTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
