﻿var reportCheckItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'reportcheckTask-id',
		type:"string"
	});
	    fields.push({
		name:'reportcheckTask-name',
		type:"string"
	});
	   fields.push({
		name:'product',
		type:"string"
	});
	   fields.push({
		name:'status',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportcheckTask-id',
		hidden : true,
		header:'关联主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportcheckTask-name',
		hidden : false,
		header:'关联主表',
		width:20*10
	});
	cm.push({
		dataIndex:'product',
		hidden : false,
		header:'检测项目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'status',
		hidden : false,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/report/reportCheckTask/showReportCheckItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="报告明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/interpret/report/reportCheckTask/delReportCheckItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				reportCheckItemGrid.getStore().commitChanges();
				reportCheckItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择关联主表',
			handler : selectreportcheckTaskFun
		});
	
	
	
	
	
	
	
	
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = reportCheckItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							reportCheckItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	reportCheckItemGrid=gridEditTable("reportCheckItemdiv",cols,loadParam,opts);
	$("#reportCheckItemdiv").data("reportCheckItemGrid", reportCheckItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectreportcheckTaskFun(){
	var win = Ext.getCmp('selectreportcheckTask');
	if (win) {win.close();}
	var selectreportcheckTask= new Ext.Window({
	id:'selectreportcheckTask',modal:true,title:'选择关联主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/ReportCheckTaskSelect.action?flag=reportcheckTask' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectreportcheckTask.close(); }  }]  });     selectreportcheckTask.show(); }
	function setreportcheckTask(id,name){
		var gridGrid = $("#reportCheckItemdiv").data("reportCheckItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reportcheckTask-id',id);
			obj.set('reportcheckTask-name',name);
		});
		var win = Ext.getCmp('selectreportcheckTask')
		if(win){
			win.close();
		}
	}
	
