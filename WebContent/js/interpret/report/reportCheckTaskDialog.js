var reportCheckTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'acceptUser',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser',
		header:'下达人',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:'下达日期',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser',
		header:'报告员',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:'报告生成日期',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'工作流id',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/interpret/report/reportCheckTask/showReportCheckTaskListJson.action";
	var opts={};
	opts.title="报告审核";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setReportCheckTaskFun(rec);
	};
	reportCheckTaskDialogGrid=gridTable("show_dialog_reportCheckTask_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(reportCheckTaskDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
