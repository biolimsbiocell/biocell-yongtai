﻿
var sequencingTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'describe',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"String"
	});
	   fields.push({
		name:'endTime',
		type:"String"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'sequencing-id',
		type:"string"
	});
	    fields.push({
		name:'sequencing-name',
		type:"string"
	});
	    fields.push({
			name:'tItem',
			type:"string"
		});
		   fields.push({
			name:'testUser-id',
			type:"string"
		});
		   fields.push({
			name:'testUser-name',
			type:"string"
		});
		   fields.push({
			name:'state',
			type:"string"
		});
		   fields.push({
			name:'stateName',
			type:"string"
		});
		  fields.push({
				name:'sampleCodes',
				type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.stepDetail,
		width:100*6,
		editor : new Ext.form.HtmlEditor({
			readOnly : true
		})
	});
	cm.push({
		dataIndex:'describe',
		hidden : true,
		header:biolims.common.describe,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'testUser-id',
		hidden : true,
		header:biolims.common.testUserId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUser-name',
		hidden : true,
		header:biolims.common.testUserName,
		width:20*6,
		
		editor : testUser
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : true,
		header:biolims.common.relateSample,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:biolims.common.startTime,
		width:20*6,
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:biolims.common.endTime,
		width:20*6,
	});

	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:biolims.common.itemId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sequencingLife/showSequencingTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.executionStep;
	opts.height =  document.body.clientHeight*0.8;
	opts.tbar = [];
	var state = $("#sequencing_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sequencingLife/delSequencingTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : biolims.common.fillDetail,
		handler : templateSelectSq
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	}
	sequencingTemplateGrid=gridEditTable("sequencingTemplatediv",cols,loadParam,opts);
	$("#sequencingTemplatediv").data("sequencingTemplateGrid", sequencingTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//打印执行单
function stampOrder(){
	var id=$("#sequencing_template").val();
	if(id==""){
		message(biolims.common.pleaseSelectTemplate);
		return;
	}else{
		var url = '__report=SequenceTask.rptdesign&id=' + $("#sequencing_id").val();
		commonPrint(url);
	}
}

//查询实验员
function loadTestUser(){
	var win = Ext.getCmp('loadTestUser');
	if (win) {win.close();}
	var loadTestUser= new Ext.Window({
	id:'loadTestUser',modal:true,title:biolims.common.chooseTester,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=loadTestUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 loadTestUser.close(); }  }]  });     loadTestUser.show(); }
	function setloadTestUser(id,name){
		var gridGrid = $("#sequencingTemplatediv").data("sequencingTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('testUser-id',id);
			obj.set('testUser-name',name);
		});
		var win = Ext.getCmp('loadTestUser');
		if(win){
			win.close();
		}
	}
	
	
	//获取开始时的时间
	function getStartTime(){
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=sequencingTemplateGrid.getSelectionModel();
		var setNum = sequencingReagentGrid.store;
		var selectRecords=sequencingItemGrid.getSelectionModel();
		if(selectRecords.getSelections().length>0){
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("stepNum")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message(biolims.common.selectStepNum);
		}
		
		//将所选的样本，放到关联样本
		var selRecord=sequencingTemplateGrid.getSelectRecord();
		var codes = "";
			$.each(selectRecords.getSelections(), function(i, obj) {
				codes += obj.get("poolingCode")+",";
			});
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", codes);
			});
		}else{
			message(biolims.common.pleaseSelectSamples);
		}
		
	}
	
	
	//获取停止时的时间
	function getEndTime(){
			var setRecord=sequencingItemGrid.store;
			var d = new Date();
			
			var getIndex = sequencingTemplateGrid.store;
			var getIndexs = sequencingTemplateGrid.getSelectionModel().getSelections();
			
			var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
			var selectRecord=sequencingTemplateGrid.getSelectionModel();
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					obj.set("endTime",str);
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i=0; i<setRecord.getCount(); i++){
						for(var j=0; j<scode.length; j++){
							if(scode[j]==setRecord.getAt(i).get("poolingCode")){
								
								setRecord.getAt(i).set("stepNum",obj.get("stepNum"));
							}
						}
					}
					//将当前行的关联样本传到下一行
					getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
				});
				/*var codeIds = "";
				for(var i =0;i<scode.length;i++){
					codeIds += scode[i];
				}*/
			}
			else{
				message(biolims.common.selectStepNum);
			}
	}
	
	function addSuccess(){

		var num =$("#sequencing_template").val();
			if(num!=""){
				var setNum = sequencingReagentGrid.store;
				var selectRecords = sequencingItemGrid.store;
					for(var i=0;i<setNum.getCount();i++){
						setNum.getAt(i).set("sampleNum",selectRecords.getCount());
				}


		
		var getRecord1 = sequencingItemGrid.store;
//		for(var j=0;j<getRecord1.getCount();j++){
//			var productNum=getRecord1.getAt(j).get("dataDemand");
//			if(productNum==""){
//				message("请填写分割数量");
//				return;
//			}
//		}
		var getRecord = sequencingItemGrid.getSelectionModel().getSelections();
		var selectRecord = sequencingTemplateGrid.getSelectionModel();
		var selRecord = sequencingResultGrid.store;
		if(selectRecord.getSelections().length > 0){
			$.each(selectRecord.getSelections(), function(i, obj) {
				var isRepeat = false;
				var codes = obj.get("sampleCodes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<scode.length; i++){
					for(var j=0;j<selRecord.getCount();j++){
						var getv = scode[i];
						var setv = selRecord.getAt(j).get("poolingCode");
						if(getv == setv){
							isRepeat = true;
							message(biolims.common.haveDuplicate);
							break;					
						}
					}
				}
				if(!isRepeat){
					$.each(getRecord,function(a,b){

//						var productNum=b.get("dataDemand");
//							for(var k=1;k<=productNum;k++){
								var ob = sequencingResultGrid.getStore().recordType;
								sequencingResultGrid.stopEditing();
								var p = new ob({});
								p.isNew = true;
								p.set("poolingCode",b.get("poolingCode"));
								p.set("poolingId",b.get("poolingId"));
								p.set("laneCode",b.get("laneCode"));
								p.set("sampleCode",b.get("sampleCode"));
								p.set("code",b.get("code"));
								p.set("result","1");
								p.set("fcCode",$("#sequencing_fcCode").val());
								p.set("machineCode",$("#sequencing_deviceCode").val());
								
								sequencingResultGrid.getStore().add(p);
								sequencingResultGrid.startEditing(0,0);
//							}
							message(biolims.common.generateResultsSuccess);
					
					});
				
			}
			});
		}else{
			var selRecord = sequencingResultGrid.store;
			var flag;
			
			var getRecord = sequencingItemGrid.getAllRecord();
			
				flag = true;
				for(var i=0;i<getRecord.length;i++){
					for(var j1=0;j1<selRecord.getCount();j1++){
						var getv = getRecord[i].get("poolingCode");
						var setv = selRecord.getAt(j1).get("poolingCode");
						if(getv == setv){
							flag = false;
							message(biolims.common.haveDuplicate);
							break;					
						}
					}
				}
				
				if(flag==true){
					$.each(getRecord,function(a,b){
//					var productNum=b.get("dataDemand");
//						for(var k=1;k<=productNum;k++){
							var ob = sequencingResultGrid.getStore().recordType;
							sequencingResultGrid.stopEditing();
							var p = new ob({});
							p.isNew = true;
							p.set("poolingCode",b.get("poolingCode"));
							p.set("poolingId",b.get("poolingId"));
							p.set("laneCode",b.get("laneCode"));
							p.set("code",b.get("code"));
							p.set("sampleCode",b.get("sampleCode"));
							p.set("result","1");
							p.set("fcCode",$("#sequencing_fcCode").val());
							p.set("machineCode",$("#sequencing_deviceCode").val());
							
							sequencingResultGrid.getStore().add(p);
							sequencingResultGrid.startEditing(0,0);
//						}
						message(biolims.common.generateResultsSuccess);
					});
				}
		
		}

			}else{
				message(biolims.common.pleaseSelectTemplate);
			}
	}
	//选择实验步骤
	function templateSelectSq(){
		var option = {};
		option.width = 450;
		option.height = 500;
		loadDialogPage(null, biolims.common.chooseExperimentalSteps, "/experiment/sequencingLife/showSequencingTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
			"Confirm" : function() {
				var operGrid = $("#template_wait_grid_div").data("grid");
				var ob = sequencingTemplateGrid.getStore().recordType;
				sequencingTemplateGrid.stopEditing();
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
//						for(var i=0;i<arr.length-1;i++){
							var p = new ob({});
							p.isNew = true;
							p.set("stepNum", obj.get("stepNum"));
							p.set("stepName", obj.get("stepName"));
//							p.set("blood-sequencingName", obj.get("sequencingName"));
//							p.set("blood-reportMan-id", obj.get("reportMan-id"));	
//							p.set("blood-patientId-name", obj.get("patientId-name"));	
							sequencingTemplateGrid.getStore().add(p);
					});
					sequencingTemplateGrid.startEditing(0, 0);
					$(this).dialog("close");
					$(this).dialog("remove");
				} else {
					message(biolims.common.selectYouWant);
					return;
				}
			}
		}, true, option);

		
	}