﻿var sequencingAbnormalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
    fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	  
	   fields.push({
		name:'mixVolume',
		type:"string"
	});
	   fields.push({
		name:'fcCode',
		type:"string"
	});
	   fields.push({
		name:'laneCode',
		type:"string"
	});
	   fields.push({
			name:'machineNum',
			type:"string"
		});
	   fields.push({
		name:'sequencingDate',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'isExecute',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'sequencing-name',
		type:"string"
	});
	   fields.push({
		name:'sequencing-id',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:biolims.pooling.code,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'laneCode',
		hidden : false,
		header:biolims.pooling.lane,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.sampleName,
		width:20*6,
		
	});
	cm.push({
		dataIndex:'fcCode',
		hidden : false,
		header:biolims.sequencing.fcCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sequencingDate',
		hidden : false,
		header:biolims.sequencing.sequencingDate,
		width:20*6,
	});
	
	
	cm.push({
		dataIndex:'mixVolume',
		hidden : true,
		header:biolims.pooling.mixVolume,
		width:20*6,
	});
	cm.push({
		dataIndex:'machineNum',
		hidden : false,
		header:biolims.sequencing.machineNum,
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.fcPosition,
		width:20*6,
		hidden:false,
		sortable:true
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.disqualified
			},{
				id : '1',
				name : biolims.common.qualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),
//		editor: result
	});
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.sequencing.sequencingAgain ], [ '1', biolims.pooling.refund ] ,[ '2', biolims.common.qualified ],[ '3', biolims.pooling.toFeedback],[ '4', biolims.sequencing.offlineQC ]]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:biolims.common.method,
		width:20*6,
		sortable:true,
//		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	var storeisExecuteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var isExecuteCob = new Ext.form.ComboBox({
		store : storeisExecuteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:biolims.common.confirm2Execute,
		width:10*6,
		editor : isExecuteCob,
		renderer : Ext.util.Format.comboRenderer(isExecuteCob)
	});
	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*10,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		hidden:true,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};//showSequencingAbnormalListJson
	loadParam.url=ctx+"/experiment/sequencingLife/sequencingAbnormal/showSequencingAbnormalListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.sequencing.sequencingAbnormal;
	opts.height=document.body.clientHeight;
	
	opts.tbar.push({
		text : biolims.common.batchExecute,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), biolims.common.batchExecute, null, {
				"Confirm" : function() {
					var records = sequencingAbnormalGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute").val();
						sequencingAbnormalGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", isExecute);
						});
						sequencingAbnormalGrid.startEditing(0, 0);
					}else{
						message(biolims.common.pleaseSelect);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	sequencingAbnormalGrid=gridEditTable("sequencingAbnormaldiv",cols,loadParam,opts);
	$("#sequencingAbnormaldiv").data("sequencingAbnormalGrid", sequencingAbnormalGrid);
});


//保存
function save(){	
	var selectRecord = sequencingAbnormalGrid.getSelectionModel();
	var inItemGrid = $("#sequencingAbnormaldiv").data("sequencingAbnormalGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/sequencingLife/sequencingAbnormal/saveSequencingAbnormal.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#sequencingAbnormaldiv").data("sequencingAbnormalGrid").getStore().commitChanges();
					$("#sequencingAbnormaldiv").data("sequencingAbnormalGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		});
	}else{
		message(biolims.common.noData2Save);
	}
}

//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
		
			
			commonSearchAction(sequencingAbnormalGrid);
			$(this).dialog("close");

		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}