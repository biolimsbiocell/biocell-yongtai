var sequencingDialogGrid;
$(function() {
	var cols = {};
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'reciveUser-id',
		type : "string"
	});
	fields.push({
		name : 'reciveUser-name',
		type : "string"
	});
	fields.push({
		name : 'reciveDate',
		type : "string"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'fcCode',
		type : "string"
	});
	fields.push({
		name : 'acceptUser-id',
		type : "string"
	});
	fields.push({
		name : 'acceptUser-name',
		type : "string"
	});
	fields.push({
		name : 'template-id',
		type : "string"
	});
	fields.push({
		name : 'template-name',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'fcCode',
		header : "FC号",
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.name,
		width : 20 * 10,
		sortable : true
	});

	cm.push({
		dataIndex : 'reciveUser-id',
		header : biolims.common.testUserId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'reciveUser-name',
		header : biolims.common.testUserName,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'template-id',
		header : "模板ID",
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'template-name',
		header : "模板名称",
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'reciveDate',
		header : biolims.common.testTime,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-id',
		header : biolims.common.createUserId,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : biolims.common.createUserName,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.common.createDate,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : biolims.common.workFlowStateName,
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'acceptUser-id',
		header : "实验组ID",
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'acceptUser-name',
		header : "实验组",
		width : 20 * 10,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 20 * 10,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/experiment/sequencingLife/showSequencingListJson.action?flag="
			+ $("#flowCell").val();
	var opts = {};
	opts.title = biolims.sequencing.computerSequencing;
	opts.height = document.body.clientHeight - 34;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id, rec) {
		$('#selectId').val(id);
		window.parent.setSequencingFun(rec);
	};
	sequencingDialogGrid = gridTable("show_dialog_sequencing_div", cols,
			loadParam, opts);
});
function sc() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(sequencingDialogGrid);
			$(this).dialog("close");

		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}
