﻿
var sequencingCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'state',
			type:"string"
		});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'type-id',
			type:"string"
		});
	   fields.push({
			name:'type-name',
			type:"string"
		});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
		name:'sequencing-id',
		type:"string"
	});
	    fields.push({
		name:'sequencing-name',
		type:"string"
	});
	    fields.push({
			name:'temperature',
			type:"string"
		});
	   fields.push({
			name:'speed',
			type:"string"
		});
	   fields.push({
			name:'time',
			type:"string"
		});
	   fields.push({
			name:'itemId',
			type:"string"
		});
	   fields.push({
			name:'tCos',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.instrumentNo,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var instrumentName =new Ext.form.TextField({
        allowBlank: false
	});
	instrumentName.on('focus', function() {
		setinstrumentName();
	 });
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.instrumentName,
		width:20*6,
		editor:instrumentName
		
	});
	cm.push({
		dataIndex:'type-id',
		hidden : true,
		header:"设备类型ID",
		width:20*6
	});
	cm.push({
		dataIndex:'type-name',
		hidden : false,
		header:biolims.common.instrumentType,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.instrumentState,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'tCos',
		hidden : true,
		header:biolims.common.templateInstrumentId,
		width:20*6
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.templateStepNo,
		width:20*6
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isDetecting,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(isGood),editor: isGood
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sequencingLife/showSequencingCosListJson.action?id="+ $("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title=biolims.common.instrumentDetail;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#sequencing_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sequencingLife/delSequencingCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectsequencingFun
//		});
//	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = sequencingCosGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			
//						
//							
//							sequencingCosGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		text : biolims.common.fillDetail+"111",
//		handler : function (){
//			//获取选择的数据
//			var selectRcords=sequencingTemplateGrid.getSelectionModel().getSelections();
//			//获取全部数据
//			var allRcords=sequencingTemplateGrid.store;
//			//选中的数量
//			var length1=selectRcords.length;
//			//全部数据量
//			var length2=allRcords.getCount();
//			if(length2>0){
//				if(length1==1){
//					var code="";
//					$.each(selectRcords, function(i, obj) {
//						code=obj.get("stepNum");
//					});
//					if(code!=""){
//						var ob = sequencingCosGrid.getStore().recordType;
//						var p = new ob({});
//						p.isNew = true;
//						p.set("itemId", code);
//						sequencingCosGrid.stopEditing();
//						sequencingCosGrid.getStore().insert(0, p);
//						sequencingCosGrid.startEditing(0, 0);
//					}else{
//						message(biolims.common.addTemplateDetail);
//						return;
//					}								
//				}else if(length1>1){
//					message(biolims.common.onlyChooseOne);
//					return;
//				}else{
//					message(biolims.common.pleaseSelectData);
//					return;
//				}
//			}else{
//				message(biolims.common.theDataIsEmpty);
//				return;
//			}
//		}
//	});
	opts.tbar.push({
		text : biolims.common.copy,
		handler : function() {
			var ob = sequencingCosGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			var records = sequencingCosGrid.getSelectRecord();
			if (records.length==1) {
				$.each(records, function(i, obj) {
					p.set("code",obj.get("code"));
					p.set("name",obj.get("name"));
					p.set("temperature",obj.get("temperature"));
					p.set("speed",obj.get("speed"));
					p.set("time",obj.get("time"));
					p.set("isGood",obj.get("isGood"));
					p.set("itemId",obj.get("itemId"));
					p.set("tCos",obj.get("tCos"));
					p.set("type-id",obj.get("type-id"));
					p.set("type-name",obj.get("type-name"));
					p.set("sequencing-id",obj.get("sequencing-id"));
					p.set("sequencing-name",obj.get("sequencing-name"));
					p.set("note",obj.get("note"));
					sequencingCosGrid.stopEditing();
					sequencingCosGrid.getStore().add(p);
					sequencingCosGrid.startEditing(0, 0);
				});
			}else{
				message(biolims.common.pleaseChooseCopyData);
			}
		}
	});
	if($("#sequencing_id").val()&&$("#sequencing_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
	}
	}
	sequencingCosGrid=gridEditTable("sequencingCosdiv",cols,loadParam,opts);
	$("#sequencingCosdiv").data("sequencingCosGrid", sequencingCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectsequencingFun(){
	var win = Ext.getCmp('selectsequencing');
	if (win) {win.close();}
	var selectsequencing= new Ext.Window({
	id:'selectsequencing',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/SequencingSelect.action?flag=sequencing' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectsequencing.close(); }  }]  });     selectsequencing.show(); }
	function setsequencing(id,name){
		var gridGrid = $("#sequencingCosdiv").data("sequencingCosGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('sequencing-id',id);
			obj.set('sequencing-name',name);
		});
		var win = Ext.getCmp('selectsequencing')
		if(win){
			win.close();
		}
	}
	function setinstrumentName(){
		var record = sequencingCosGrid.getSelectionModel().getSelected();
		var options = {};
		 options.width = 800;
		 options.height = 500;
		 loadDialogPage(null, biolims.common.selectInstrument, "/equipment/repair/showInstrument.action", {
		 "Confirm" : function() {
		 var operGrid = $("#show_instrument_div").data("showInstrumentGrid");
		 var selectRecord = operGrid.getSelectionModel().getSelections();
		 if (selectRecord.length > 0) {
			 $.each(selectRecord, function(i, obj) {
				 record.set("code", obj.get("id"));
				 record.set("name", obj.get("name"));
				 record.set("state", obj.get("state-name"));
			 });
				
		 }else{
		 message(biolims.common.selectYouWant);
		 return;}
		 $(this).dialog("close");}
		 }, true, options);
	}
