var sequencingGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'fcCode',
		type:"string"
	});
	    fields.push({
		name:'refCode',
		type:"string"
	});
	    fields.push({
		name:'spec',
		type:"string"
	});
	    fields.push({
		name:'refName',
		type:"string"
	});
	    fields.push({
		name:'reagent',
		type:"string"
	});
//	    fields.push({
//		name:'expectDate',
//		type:"string"
//	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	    fields.push({
		name:'reciveDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'deviceCode-id',
		type:"string"
	});
	    fields.push({
		name:'deviceCode-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
			name:'confirmDate',
			type:"string"
	});
	    fields.push({
			name:'keySignal',
			type:"string"
	});
	    fields.push({
			name:'ispLoding',
			type:"string"
	});
	    fields.push({
			name:'totalReads',
			type:"string"
	});
	    fields.push({
			name:'polyclonal',
			type:"string"
	});
	    fields.push({
			name:'lowQuality',
			type:"string"
	});
	    fields.push({
			name:'readLength',
			type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'fcCode',
		header:biolims.sequencing.fcCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'refCode',
		header:biolims.sequencing.refCode,
		width:20*6,
		hidden:true,
		sortable:true
	});
		cm.push({
		dataIndex:'spec',
		header:biolims.common.reagentName,
		hidden:true,
		width:20*6,
		sortable:true
		});
	
		cm.push({
			dataIndex:'keySignal',
			header:"Key Signal（≥60)",
			hidden:true,
			width:20*6,
			sortable:true
		
		});
		
		cm.push({
			dataIndex:'ispLoding',
			header:'ISP Loading（≥70%）',
			hidden:true,
			width:20*6,
			sortable:true
		});
		
		cm.push({
			dataIndex:'totalReads',
			header:'Total Reads（≥70M)',
			hidden:true,
			width:20*6,
			sortable:true
		});
		
		cm.push({
			dataIndex:'polyclonal',
			header:'Polyclonal(≤30%)',
			hidden:true,
			width:20*6,
			sortable:true
		});
		
		cm.push({
			dataIndex:'lowQuality',
			header:'Low Quality≤25%)',
			hidden:true,
			width:20*6,
			sortable:true
		});
		
		
		cm.push({
			dataIndex:'readLength',
			hidden:true,
			header:'Read Length(100-170bp)',
			width:20*6,
			sortable:true
			});
			
	cm.push({
		dataIndex:'refName',
		header:biolims.sequencing.refName,
		width:20*6,
	
		sortable:true
	});
	cm.push({
		dataIndex:'reagent',
		header:biolims.sequencing.reagent,
		width:10*6,
		
		sortable:true
	});
//	cm.push({
//		dataIndex:'expectDate',
//		header:'预下机日期',
//		width:20*6,
//		
//		sortable:true
//	});
		cm.push({
		dataIndex:'reciveUser-id',
		hidden:true,
		header:biolims.common.testUserId,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'reciveUser-name',
		header:biolims.common.testUserName,
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'reciveDate',
		header:biolims.common.testTime,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:biolims.common.exeId,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'template-name',
		header:biolims.common.templateName,
		
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'deviceCode-id',
		hidden:true,
		header:biolims.common.instrumentId,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'deviceCode-name',
		header:biolims.common.instrumentNo,
		hidden:true,
		width:20*6,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.common.createUserId,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.common.confirmDate,
		width:20*7,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*10,
		
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:biolims.common.acceptUserId,
		width:20*6,
		sortable:true
	});
		cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.acceptUserName,
		
		width:20*6,
		sortable:true
	});
		
		
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sequencingLife/showSequencingListJson.action";
	var opts={};
	opts.title=biolims.sequencing.computerSequencing;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	sequencingGrid=gridTable("show_sequencing_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/experiment/sequencingLife/editSequencing.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/experiment/sequencingLife/editSequencing.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/experiment/sequencingLife/viewSequencing.action?id=' + id;
}
function exportexcel() {
	sequencingGrid.title = biolims.common.exportList;
	var vExportContent = sequencingGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startreciveDate").val() != undefined) && ($("#startreciveDate").val() != '')) {
					var startreciveDatestr = ">=##@@##" + $("#startreciveDate").val();
					$("#reciveDate1").val(startreciveDatestr);
				}
				if (($("#endreciveDate").val() != undefined) && ($("#endreciveDate").val() != '')) {
					var endreciveDatestr = "<=##@@##" + $("#endreciveDate").val();

					$("#reciveDate2").val(endreciveDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(sequencingGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});

//opts.tbar.push({
//text : "批量上传（csv文件）",
//handler : function() {
//	var options = {};
//	options.width = 350;
//	options.height = 200;
//	loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//		"确定":function(){
//			goInExcelcsv();
//			$(this).dialog("close");
//		}
//	},true,options);
//}
//});

//function goInExcelcsv(){
//var file = document.getElementById("file-uploadcsv").files[0];  
//var n = 0;
//var ob = sequencingResultGrid.getStore().recordType;
//var reader = new FileReader();  
//reader.readAsText(file,'GB2312');  
//reader.onload=function(f){  
//	var csv_data = $.simple_csv(this.result);
//	$(csv_data).each(function() {
//        	if(n>0){
//        		if(this[0]){
//        			var p = new ob({});
//        			p.isNew = true;				
//        			var o;
//        			o= 0-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 1-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 2-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 3-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 4-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 5-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 6-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 7-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 8-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 9-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//        			o= 10-1;
//        			p.set("po.fieldName",this[o]);
//        			
//				
//					
//					sequencingResultGrid.getStore().insert(0, p);
//        		}
//        	}
//             n = n +1;
//        	
//        });
//};
//}
//生成CSV
//opts.tbar.push({
//	text : '生成CSV',
//	handler : createCSV
//});

//function createCSV(){
//	ajax("post", "/experiment/sequencing/createExcel.action", {
//		taskId : $("#sequencing_id").val()
//	},function(data){
//		if(data.success){
//			if(data.fileId!=""){
//				message("生成成功！");
//			}else{
//				message("没有要生成的数据！");
//			}
//		}else{
//			message("另一个程序正在使用此文件，进程无法访问。");
//		}
//	},null);
//}

