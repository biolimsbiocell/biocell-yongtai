﻿var sequencingResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'fcCode',
		type:"string"
	});
	   fields.push({
		name:'laneCode',
		type:"string"
	});
	   fields.push({
		name:'machineCode',
		type:"string"
	});
//	   fields.push({
//		name:'molarity',
//		type:"string"
//	});
	   fields.push({
		name:'quantity',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'computerDate',
		type:"string"
	});
	   fields.push({
		name:'pfPercent',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date"
	});
	    fields.push({
		name:'idCard',
		type:"string"
	});
	    fields.push({
		name:'sequenceFun',
		type:"string"
	});
	    fields.push({
		name:'inspectDate',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date"
	});
	    fields.push({
		name:'expectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'sequencing-id',
		type:"string"
	});
	    fields.push({
		name:'sequencing-name',
		type:"string"
	});
    fields.push({
		name:'dataDemand',
		type:"string"
	});
    	fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'poolingId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'poolingId',
		hidden : true,
		header:biolims.wk.hhzh,
		width:20*6
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden :false,
		header:biolims.pooling.code,
		width:20*6
	});
	cm.push({
		dataIndex:'laneCode',
		hidden : false,
		header:biolims.pooling.lane,
		width:20*6
	});
	cm.push({
		dataIndex:'fcCode',
		hidden : false,
		header:biolims.sequencing.fcCode,
		width:20*6
	});
	cm.push({
		dataIndex:'machineCode',
		hidden : false,
		header:biolims.sequencing.machineNum,
		width:20*8
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.fcPosition,
		width:20*6,
		hidden:false,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'expectDate',
		hidden : false,
		header:biolims.sequencing.expectDate,
		width:20*6,
		sortable:true,
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
		
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.wk.sjfz,
		width:20*6
	});
	cm.push({
		dataIndex:'pfPercent',
		hidden : true,
		header:'Lane PF(%)',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'FC Q30(%)',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'productId',
		header:biolims.sequencing.fcData,
		hidden:true,
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

//	cm.push({
//		dataIndex:'molarity',
//		hidden : false,
//		header:'摩尔浓度',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'quantity',
		hidden : true,
		header:biolims.sequencing.quantity,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:biolims.sequencing.density,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'computerDate',
		hidden : true,
		header:biolims.sequencing.sequencingDate,
		width:20*6
		
	});

	
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.disqualified
			},{
				id : '1',
				name : biolims.common.qualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		header:biolims.common.result+'<font color="red">*</font>',
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'method',
	
		header:biolims.common.method,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportDate',
		header:biolims.common.reportDate,
		hidden:true,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.acceptDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		hidden:true,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'dataDemand',
		hidden : true,
		header:biolims.sequencing.dataRequest,
		width:20*10
	});
	
	cm.push({
		dataIndex:'contractId',
		header:biolims.common.contractId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:biolims.common.projectId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.common.orderType,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'techTaskId',
		header:biolims.common.taskId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sequencingLife/showSequencingResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sequencing.sequencingResult;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#sequencing_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sequencingLife/delSequencingResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.fcPosition,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_idCard_div"), "FC位置", null, {
				"Confirm" : function() {
					var records = sequencingResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var idCard = $("#idCard").val();
						sequencingResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("idCard", idCard);
						});
						sequencingResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), biolims.common.batchResult, null, {
				"Confirm" : function() {
					var records = sequencingResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						sequencingResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						sequencingResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	//生成CSV
//	opts.tbar.push({
//		text : '生成CSV',
//		handler : createCSV
//	});
	
//	function createCSV(){
//		ajax("post", "/experiment/sequencing/createExcel.action", {
//			taskId : $("#sequencing_id").val()
//		},function(data){
//			if(data.success){
//				if(data.fileId!=""){
//					message("生成成功！");
//				}else{
//					message("没有要生成的数据！");
//				}
//			}else{
//				message("另一个程序正在使用此文件，进程无法访问。");
//			}
//		},null);
//	}
	
//	
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		text : '查看文库信息',
//		handler : lookWkInfo
//	});
	}else{
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : "删除选中",
			handler : null
		});
//		opts.tbar.push({
//			text : '查看文库信息',
//			handler : lookWkInfo
//		});
	}
	sequencingResultGrid=gridEditTable("sequencingResultdiv",cols,loadParam,opts);
	$("#sequencingResultdiv").data("sequencingResultGrid", sequencingResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function lookWkInfo(){
	
	var records = sequencingResultGrid.getSelectRecord();
	var fjCode="";
	if (records.length>0) {
		$.each(records, function(i, obj) {
			fjCode+="'"+obj.get("poolingCode")+"',";
		});
		//var array=fjCode.split(",");
		var str=fjCode.substring(0, fjCode.length-1);
		var options = {};
		options.width = 940;
		options.height = 580;
		var a="sequencing";
		loadDialogPage(null,"查看文库信息", "/experiment/wkLife/showWKSplitInfoList.action?lx="+a+"&codes="+str+"&id="+ $("#sequencing_id").val(), {
			"确定" : function() {
				options.close();
			}
			}, true, options);
	}else{
		message("请选择要拆分的样本！");
	}
}
//复制添加明细
function addItem(){
//	alert(12);
	var selectRecord = sequencingResultGrid.getSelectionModel();
	if(selectRecord.getSelections().length > 0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			var ob = sequencingResultGrid.getStore().recordType;
			sequencingResultGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("fcCode",obj.get("fcCode"));
			p.set("poolingCode",obj.get("poolingCode"));
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("machineCode",obj.get("machineCode"));
			p.set("molarity",obj.get("molarity"));
			p.set("quantity",obj.get("quantity"));
			p.set("volume",obj.get("volume"));
			p.set("concentration",obj.get("concentration"));
			p.set("computerDate",obj.get("computerDate"));
			p.set("pfPercent",obj.get("pfPercent"));
			p.set("unit",obj.get("unit"));
			p.set("result",obj.get("result"));
			p.set("nextFlow",obj.get("nextFlow"));
			p.set("method",obj.get("method"));
			p.set("isExecute",obj.get("isExecute"));
			
			p.set("patientName",obj.get("patientName"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("inspectDate",obj.get("inspectDate"));
			p.set("acceptDate",obj.get("acceptDate"));
			p.set("idCard",obj.get("idCard"));
			p.set("phone",obj.get("phone"));
			p.set("orderId",obj.get("orderId"));
			p.set("sequenceFun",obj.get("sequenceFun"));
			p.set("reportDate",obj.get("reportDate"));
			p.set("note",obj.get("note"));
			p.set("state",obj.get("state"));
			p.set("classify",obj.get("classify"));
			
			sequencingResultGrid.getStore().add(p);
			sequencingResultGrid.startEditing(0,0);
		});
	}else{
		message(biolims.sequencing.pleaseSelectCopy);
	}
}
//function selectsequencingFun(){
//	var win = Ext.getCmp('selectsequencing');
//	if (win) {win.close();}
//	var selectsequencing= new Ext.Window({
//	id:'selectsequencing',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/SequencingSelect.action?flag=sequencing' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectsequencing.close(); }  }]  });     selectsequencing.show(); }
//	function setsequencing(id,name){
//		var gridGrid = $("#sequencingResultdiv").data("sequencingResultGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('sequencing-id',id);
//			obj.set('sequencing-name',name);
//		});
//		var win = Ext.getCmp('selectsequencing')
//		if(win){
//			win.close();
//		}
//	}
	
