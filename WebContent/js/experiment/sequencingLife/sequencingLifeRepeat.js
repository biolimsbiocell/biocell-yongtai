﻿var sequencingRepeatGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
			name:'state',
			type:"string"
		});
	    fields.push({
		name:'poolingCode',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'mixVolume',
		type:"string"
	});
	    fields.push({
		name:'fcCode',
		type:"string"
	});
	    fields.push({
		name:'laneCode',
		type:"string"
	});
	    fields.push({
		name:'machineNum',
		type:"string"
	});
	    fields.push({
		name:'sequencingDate',
		type:"string"
	});
	    fields.push({
		name:'density',
		type:"string"
	});
	    fields.push({
		name:'pf',
		type:"string"
	});
	    fields.push({
		name:'result',
		type:"string"
	});
	    fields.push({
		name:'method',
		type:"string"
	});
	    fields.push({
		name:'nextFlow',
		type:"string"
	});
	    fields.push({
		name:'isExcute',
		type:"string"
	});
	    fields.push({
		name:'reportDate',
		type:"date"
	});
	    fields.push({
		name:'idCard',
		type:"string"
	});
	    fields.push({
		name:'sequenceFun',
		type:"string"
	});
	    fields.push({
		name:'inspectDate',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'sequencing-id',
		type:"string"
	});
	    fields.push({
		name:'sequencing-name',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'poolingCode',
		header:biolims.pooling.code,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'mixVolume',
		header:biolims.common.volume,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'fcCode',
		header:biolims.sequencing.fcCode,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'laneCode',
		header:biolims.pooling.lane,
		width:20*6,
		
		sortable:true
	});
	
	cm.push({
		dataIndex:'machineNum',
		header:biolims.sequencing.machineNum,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sequencingDate',
		header:biolims.sequencing.sequencingDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'density',
		header:biolims.sequencing.density,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'pf',
		header:'PF%',
		width:20*6,
		
		sortable:true
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '0',
				name : biolims.common.disqualified
			},{
				id : '1',
				name : biolims.common.qualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.plasma.resultDecide,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: null
	});
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.plasma.buildLib ], [ '1', biolims.sequencing.sequencingAgain ],[ '2', biolims.plasma.putInLib ],[ '3', biolims.plasma.feedBack2Group ],[ '4', biolims.plasma.end ] ]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:biolims.common.nextFlow,
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.sequencing.sequencingAgain ], [ '5', biolims.common.pause ]]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:biolims.common.method,
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	
	var storeisExcuteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.yes ], [ '1', biolims.common.no ] ]
	});
	var isExcuteCob = new Ext.form.ComboBox({
		store : storeisExcuteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExcute',
		hidden : false,
		header:biolims.common.confirm2Execute,
		width:20*6,
		editor : isExcuteCob,
		renderer : Ext.util.Format.comboRenderer(isExcuteCob)
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		header:biolims.common.reportDate,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.idCard,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.acceptDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*10,
		
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};//showSequencingAbnormalListJson
	loadParam.url=ctx+"/experiment/sequencingLife/repeat/showSequencingRepeatListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.sequencing.sequencingAgain;
	opts.height=document.body.clientHeight;
	
	opts.tbar.push({
		text : biolims.common.batch2deal,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_method_div"), biolims.common.batch2deal, null, {
				"Confirm" : function() {
					var records = sequencingRepeatGrid.getSelectRecord();
					if (records && records.length > 0) {
						var method = $("#method").val();
						sequencingRepeatGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("method", method);
						});
						sequencingRepeatGrid.startEditing(0, 0);
					}else{
						message(biolims.common.pleaseSelect);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	sequencingRepeatGrid=gridEditTable("sequencingRepeatdiv",cols,loadParam,opts);
	$("#sequencingRepeatdiv").data("sequencingRepeatGrid", sequencingRepeatGrid);
});


//保存
function save(){	
	var selectRecord = sequencingRepeatGrid.getSelectionModel();
	var inItemGrid = $("#sequencingRepeatdiv").data("sequencingRepeatGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/sequencingLife/repeat/saveSequencingRepeat.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#sequencingRepeatdiv").data("sequencingRepeatGrid").getStore().commitChanges();
					$("#sequencingRepeatdiv").data("sequencingRepeatGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		});
	}else{
		message(biolims.common.noData2Save);
	}
}

//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
		
			
			commonSearchAction(sequencingRepeatGrid);
			$(this).dialog("close");

		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}