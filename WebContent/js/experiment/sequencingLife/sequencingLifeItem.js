﻿var sequencingItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
			name:'sequencingPlatform',
			type:"string"
		});
		   fields.push({
			name:'sequencingReadLong',
			type:"string"
		});
		   fields.push({
			name:'sequenceType',
			type:"string"
		});
	   fields.push({
		name:'laneCode',
		type:"string"
	});
	   
	   fields.push({
			name:'runCode',
			type:"string"
		});
	   
	   fields.push({
			name:'chipBarcode',
			type:"string"
		});
	   
	   fields.push({
			name:'protonNumber',
			type:"string"
		});
	   
	   fields.push({
			name:'protonState',
			type:"string"
		});
	   
	   fields.push({
			name:'protonClWash',
			type:"string"
		});
	   
	   fields.push({
			name:'protonWash',
			type:"string"
		});
	   
	   fields.push({
			name:'protonInit',
			type:"string"
		});
//	   fields.push({
//		name:'quantity',
//		type:"string"
//	});
	   fields.push({
		name:'density',
		type:"string"
	});
	   fields.push({
		name:'dataDemand',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date"
	});
	    fields.push({
		name:'idCard',
		type:"string"
	});
	    fields.push({
		name:'sequenceFun',
		type:"string"
	});
	    fields.push({
		name:'inspectDate',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	    fields.push({
		name:'sequencing-id',
		type:"string"
	});
	    fields.push({
		name:'sequencing-name',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'qpcrNd',
		type:"string"
	});
	   fields.push({
		name:'upNd',
		type:"string"
	});
	    fields.push({
		name:'yw',
		type:"string"
	});
	    fields.push({
		name:'prepareNd',
		type:"string"
	});
	   fields.push({
		name:'quantitativeMethod',
		type:"string"
	});
	   fields.push({
		name:'dilutionOne',
		type:"string"
	});
	   fields.push({
		name:'dilutionTwo',
		type:"string"
	});
	   fields.push({
		name:'dilutionThree',
		type:"string"
	});
	    fields.push({
		name:'dilutionFour',
		type:"string"
	});
	    fields.push({
		name:'bxybl',
		type:"string"
	});
	   fields.push({
		name:'hyb',
		type:"string"
	});
	   fields.push({
		name:'bxhnd',
		type:"string"
	});
	   fields.push({
		name:'naOhTake',
		type:"string"
	});
	   fields.push({
		name:'dnaTake',
		type:"string"
	});
	   fields.push({
		name:'hybTake',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'cut',
			type:"string"
		});
	   fields.push({
			name:'sample',
			type:"string"
		});
	   fields.push({
			name:'addWater',
			type:"string"
		});
	   fields.push({
			name:'toShift',
			type:"string"
		});
	   
	   fields.push({
			name:'raise',
			type:"string"
		});
	   fields.push({
			name:'dry',
			type:"string"
		});
	   fields.push({
			name:'yszlnd',
			type:"string"
		});
		 fields.push({
				name:'thzlnd',
				type:"string"
			});
		 fields.push({
				name:'ysmend',
				type:"string"
			});
		 fields.push({
				name:'thmend',
				type:"string"
			});
		 fields.push({
				name:'poolingId',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:"原始样本编号",
		width:20*6
	});
	cm.push({
		dataIndex:'poolingId',
		header:biolims.wk.hhzh,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:biolims.pooling.code,
		width:20*6
	});
	
	cm.push({
		dataIndex:'runCode',
		hidden : false,
		header:biolims.common.RunCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'chipBarcode',
		hidden : false,
		header:biolims.common.chipBar,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'protonNumber',
		hidden : false,
		header:biolims.common.protonCode,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'protonState',
		hidden : false,
		header:biolims.common.protonState,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'protonClWash',
		hidden : false,
		header:biolims.common.protonChlorine,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'protonWash',
		hidden : false,
		header:biolims.common.protonWater,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'protonInit',
		hidden : false,
		header:biolims.common.protonInit,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'laneCode',
		hidden : false,
		header:biolims.common.laneNum,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.common.code,
		width:20*6
		
	});
	cm.push({
		dataIndex:'yszlnd',
		hidden : false,
		header:biolims.common.phixAddtotal,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'upNd',
		hidden : false,
		header:biolims.sequencing.upNd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sample',
		hidden : true,
		header:"20pM文库取样体积(ul)",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addWater',
		hidden : true,
		header:"HT1补加体积(ul)",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:"描述",
		width:20*6
	});

	cm.push({
		dataIndex:'dataDemand',
		hidden : true,
		header:biolims.sequencing.dataDemand,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var storetypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '', biolims.common.pleaseChoose ], [ '0', 'PE' ],['1','SE'],[ '2', 'PE/SE' ]]
	});
	var typeCob = new Ext.form.ComboBox({
		store : storetypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'sequenceType',
		hidden : true,
		header:biolims.pooling.sequencingType,
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(typeCob)
	});
	var storereadLongCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '', biolims.common.pleaseChoose ], [ '0', '40+8' ],['1','50+8'],[ '2', '75+8' ],[ '3', '100+8' ],[ '4', '125+8' ],[ '5', '150+8' ],[ '6', '250+8' ],[ '7', '300+8' ],[ '8', biolims.pooling.others]]
	});
	var readLongCob = new Ext.form.ComboBox({
		store : storereadLongCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'sequencingReadLong',
		hidden : true,
		header:biolims.pooling.sequencingReadLong,
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(readLongCob)
	});
	var storeplatformCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '', biolims.common.pleaseChoose ], [ '0', 'Nextseq 500' ],['1','Nextseq 550AR'],[ '2', 'Miseq' ],[ '3', 'Hiseq 2500 V4' ],[ '4', 'Hiseq 2500 Rapid' ],[ '5', biolims.pooling.others]]
	});
	var platformCob = new Ext.form.ComboBox({
		store : storeplatformCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'sequencingPlatform',
		hidden : true,
		header:biolims.pooling.sequencingPlatform,
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(platformCob)
	});
	cm.push({
		dataIndex:'density',
		hidden : true,
		header:biolims.wk.density,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
//	var result = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '合格'
//			},{
//				id : '0',
//				name : '不合格'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'处理结果',
//		width:15*6,
//		renderer: Ext.util.Format.comboRenderer(result),editor: result
//	});
	
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:biolims.common.orderNumber,
		width:10*7,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'reason',
//		hidden : true,
//		header:'失败原因',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'template-name',
		header:biolims.common.templateName,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'template-id',
		header:biolims.common.templateId,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		header:biolims.common.reportDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.idCard,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.acceptDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequencing-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequencing-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.isPutInStorage,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'qpcrNd',
		hidden : true,
		header:biolims.QPCR.concentration,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cut',
		hidden : true,
		header:biolims.sequencing.cut,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'raise',
		hidden : true,
		header:biolims.sequencing.raise,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'dry',
		hidden : true,
		header:biolims.sequencing.dry,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'thzlnd',
		hidden : true,
		header:biolims.sequencing.tzhzlnd,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ysmend',
		hidden : true,
		header:biolims.sequencing.ysmnd,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'thmend',
		hidden : true,
		header:biolims.sequencing.tzhmnd,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'toShift',
		hidden : true,
		header:biolims.sequencing.toShift,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'yw',
		hidden : true,
		header:biolims.wk.yw,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'prepareNd',
		hidden : true,
		header:biolims.wk.density,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'quantitativeMethod',
		hidden : true,
		header:biolims.sequencing.quantitativeMethod,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dilutionOne',
		hidden : true,
		header:biolims.sequencing.dilutionOne,
		width:20*6
	});
	cm.push({
		dataIndex:'dilutionTwo',
		hidden : true,
		header:biolims.sequencing.dilutionTwo,
		width:20*6
	});
	cm.push({
		dataIndex:'dilutionThree',
		hidden : true,
		header:biolims.sequencing.dilutionThree,
		width:20*6
	});
	cm.push({
		dataIndex:'dilutionFour',
		hidden : true,
		header:biolims.sequencing.dilutionFour,
		width:20*6
	});
	cm.push({
		dataIndex:'bxhnd',
		hidden : true,
		header:biolims.sequencing.bxhnd,
		width:20*6
	});
	cm.push({
		dataIndex:'naOhTake',
		hidden : true,
		header:'NaOH',
		width:20*6
	});
	cm.push({
		dataIndex:'dnaTake',
		hidden : true,
		header:'DNA',
		width:20*6
	});
	cm.push({
		dataIndex:'hybTake',
		hidden : true,
		header:biolims.sequencing.hybTake,
		width:20*6
	});
	cm.push({
		dataIndex:'bxybl',
		hidden : true,
		header:biolims.sequencing.bxybl,
		width:20*6
	});
	cm.push({
		dataIndex:'hyb',
		hidden : true,
		header:'Hyb',
		width:20*6
	});
	
	
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderTypeName,
		width:20*6
	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sequencingLife/showSequencingItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sequencing.sequencingDetail;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#sequencing_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sequencingLife/delSequencingItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				sequencingTempGrid.getStore().commitChanges();
				sequencingTempGrid.getStore().reload();
				sequencingItemGrid.getStore().commitChanges();
				sequencingItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	/*opts.tbar.push({
		text : '开始试验',
		handler : addDetails
	});*/
	opts.tbar.push({
		text : biolims.common.batchInLib,
		handler : ruku
	});
	}
	sequencingItemGrid=gridEditTable("sequencingItemdiv",cols,loadParam,opts);
	$("#sequencingItemdiv").data("sequencingItemGrid", sequencingItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function ruku() {
	var operGrid = sequencingItemGrid;
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if(selectRecord.length>0){
	var records = [];
	for ( var i = 0; i < selectRecord.length; i++) {
		if(selectRecord[i].get("state")=="1"){
			message(biolims.wk.pleaseChooseAgain);
			return;
		}
		records.push(selectRecord[i].get("id"));
	}
	ajax("post", "/experiment/sequencingLife/rukuSequencingTaskItem.action", {
		ids : records
	}, function(data) {
		if (data.success) {
			sequencingItemGrid.getStore().commitChanges();
			sequencingItemGrid.getStore().reload();
			message(biolims.plasma.warehousingSuccess);
		} else {
			message(biolims.plasma.warehousingFailed);
		}
	}, null);
	}else{
		message(biolims.plasma.pleaseSelectWarehousing);
	}
}


//function selectsequencingFun(){
//	var win = Ext.getCmp('selectsequencing');
//	if (win) {win.close();}
//	var selectsequencing= new Ext.Window({
//	id:'selectsequencing',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/SequencingSelect.action?flag=sequencing' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectsequencing.close(); }  }]  });     selectsequencing.show(); }
//	function setsequencing(id,name){
//		var gridGrid = $("#sequencingItemdiv").data("sequencingItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('sequencing-id',id);
//			obj.set('sequencing-name',name);
//		});
//		var win = Ext.getCmp('selectsequencing')
//		if(win){
//			win.close();
//		}
//	}
//	


/*//开始试验
function addDetails(){
	var selectRecord=sequencingItemGrid.getSelectionModel();
	var selRecord=sequencingTemplateGrid.getSelectRecord();
	if (selectRecord.getSelections().length > 0) {
		var sampleCodes = "";
		$.each(selectRecord.getSelections(), function(i, obj) {
			sampleCodes += obj.get("code")+",";
		});
		if (!selRecord.length) {
			selRecord = sequencingTemplateGrid.getAllRecord();
		}
		if (selRecord&&selRecord.length>0) {
			sequencingTemplateGrid.stopEditing();
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", sampleCodes);
			});
			sequencingTemplateGrid.startEditing(0, 0);
		}
			
	}else{
		message("请选择实验样本");
	}

}*/