//上机测序左侧
var sequencingTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
    	name:'poolingCode',
    	type:"string"
    });
	    fields.push({
	    	name:'qpcrConcentration',
	    	type:"string"
	    });
	    
	    fields.push({
    	name:'code',
    	type:"string"
    });
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
    	name:'sampleCode',
    	type:"string"
    });
	    fields.push({
    	name:'mixDate',
    	type:"string"
    });
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
    	name:'state',
    	type:"string"
    });
	    fields.push({
    	name:'sequenceType',
    	type:"string"
    });
	    fields.push({
    	name:'template-id',
    	type:"string"
    });
	    fields.push({
    	name:'template-name',
    	type:"string"
    });
	    fields.push({
		name:'sequencingReadLong',
		type:"string"
	});
	    fields.push({
		name:'sequencingPlatform',
		type:"string"
	});
	    fields.push({
		name:'product-id',
		type:"string"
	});
	    fields.push({
		name:'product-name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	    fields.push({
		name:'reciveDate',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'reportDate',
		type:"date"
	});
	    fields.push({
		name:'idCard',
		type:"string"
	});
	    fields.push({
		name:'sequenceFun',
		type:"string"
	});
	    fields.push({
		name:'inspectDate',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'phone',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'density',
		type:"string"
	});
	    fields.push({
		name:'yw',
		type:"string"
	});
	    fields.push({
		name:'mixRatio',
		type:"string"
	});
	    
    fields.push({
		name:'totalAmount',
		type:"string"
	});
	    fields.push({
		name:'totalVolume',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'readsOne',
		type:"string"
	});
	    fields.push({
		name:'readsTwo',
		type:"string"
	});
	    fields.push({
		name:'index1',
		type:"string"
	});
	    fields.push({
		name:'index2',
		type:"string"
	});
	    
	    fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
	    fields.push({
		name:'orderType',
		type:"string"
	});
	    fields.push({
		name:'techTaskId',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	    fields.push({
			name:'lane',
			type:"string"
		});
	    fields.push({
			name:'poolingId',
			type:"string"
		});
	    fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-sequenceBillDate',
			type:"string"
		});
	  fields.push({
			name:'isZkp',
			type:"string"
		});
	  fields.push({
			name:'laneNum',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:"原始样本编号",
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'poolingId',
		header:biolims.pooling.code,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.wk.sjfz,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'poolingCode',
		header:"上机分组号",
		width:20*7,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'name',
		header:biolims.sequencing.tzhmnd,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'lane',
		header:biolims.wk.wkVolume,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'qpcrConcentration',
		header:biolims.QPCR.concentration,
		width:20*6,
		hidden:true,
		sortable:true
	});

	cm.push({
		dataIndex:'mixDate',
		header:biolims.sequencing.mixDate,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
//	var sequencingReadLong = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '0',
//				name : '40+8'
//			},{
//				id : '1',
//				name : '50+8'
//			},{
//				id : '2',
//				name : '75+8'
//			},{
//				id : '3',
//				name : '100+8'
//			},{
//				id : '4',
//				name : '125+8'
//			},{
//				id : '5',
//				name : '150+8'
//			},{
//				id : '6',
//				name : '250+8'
//			},{
//				id : '7',
//				name : '300+8'
//			},{
//				id : '8',
//				name : '其他'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
	cm.push({
		dataIndex:'sequencingReadLong',
		header:biolims.pooling.sequencingReadLong,
		width:20*6,
		hidden:true,
		sortable:true
//		renderer: Ext.util.Format.comboRenderer(sequencingReadLong)
	});
	
//	var sequencingType = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '0',
//				name : 'PE'
//			},{
//				id : '1',
//				name : 'SE'
//			},{
//				id : '2',
//				name : 'PE/SE'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
	cm.push({
		dataIndex:'sequenceType',
		header:biolims.pooling.sequencingType,
		width:20*6,
		hidden:true,
		sortable:true
//		renderer: Ext.util.Format.comboRenderer(sequencingType)
	});
	
//	var sequencingPlatform = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '0',
//				name : 'Nextseq 500'
//			},{
//				id : '1',
//				name : 'Nextseq 550AR'
//			},{
//				id : '2',
//				name : 'Miseq'
//			},{
//				id : '3',
//				name : 'Hiseq 2500 V4'
//			},{
//				id : '4',
//				name : 'Hiseq 2500 Rapid'
//			},{
//				id : '5',
//				name : '其他'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
	cm.push({
		dataIndex:'sequencingPlatform',
		header:biolims.pooling.sequencingPlatform,
		width:20*6,
		hidden:true,
		sortable:true
//		renderer: Ext.util.Format.comboRenderer(sequencingPlatform)
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'reciveUser-id',
		header:biolims.common.testUserId,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reciveUser-name',
		header:biolims.common.testUserName,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'reciveDate',
		header:biolims.common.testTime,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-id',
		header:biolims.common.createUserId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'template-name',
		header:biolims.common.templateName,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'template-id',
		header:biolims.common.templateId,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'product-id',
		header:biolims.wk.productId,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'product-name',
		header:biolims.wk.productName,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		header:biolims.common.reportDate,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.idCard,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		header:biolims.common.acceptDate,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.common.phone,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		header:biolims.common.patientName,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'density',
		header:biolims.wk.density,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'yw',
		header:biolims.wk.yw,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'mixRatio',
		header:biolims.wk.mixRatio,
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'totalAmount',
		header:biolims.sequencing.totalAmount,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'totalVolume',
		header:biolims.common.totalVolume,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-id',
		header:biolims.common.acceptUserId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.acceptUserName,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'readsOne',
		header:'ReadsOne',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'readsTwo',
		header:'ReadsTwo',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'index1',
		header:'Index1',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'index2',
		header:'Index2',
		hidden:true,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'contractId',
		header:biolims.common.contractId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:biolims.common.projectId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.common.orderType,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'techTaskId',
		header:biolims.common.taskId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'laneNum',
		hidden : false,
		header:biolims.pooling.laneNum,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequenceBillDate',
		hidden : true,
		header:"截止日期",
		width:30*6
	});

	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sequencingLife/showSequencingTempListJson.action";
	loadParam.limit=999;
	var opts={};
	opts.title=biolims.sequencing.forSequencingSamples;
	opts.height=document.body.clientHeight-30;
	opts.tbar=[];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = sequencingTempGrid.getAllRecord();
							var store = sequencingTempGrid.store;

							var isOper = true;
							var buf = [];
							sequencingTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("poolingCode")){
										buf.push(store.indexOfId(obj1.get("id")));
									}
								});
							});
							sequencingTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message(biolims.common.samplecodeComparison);
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							sequencingTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		text : biolims.common.scienceService,
		handler : techDNAService
	});
	opts.tbar.push({
		text : biolims.common.retrieve,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_js_div"), biolims.common.retrieve, null, {
				"Confirm": function() {
					commonSearchAction(sequencingTempGrid);
					$(this).dialog("close");
				}
			}, true, options);
			
		}
	});
	opts.tbar.push({
		text : biolims.common.sampleWindow,
		handler : changeWidth
	});
	sequencingTempGrid=gridEditTable("sequencingTempdiv",cols,loadParam,opts);
	$("#sequencingTempdiv").data("sequencingTempGrid", sequencingTempGrid);
});

$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(sequencingTempGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
function addItem(){
	var count=1;
	var max=0;
	var selectRecord=sequencingTempGrid.getSelectionModel();
	var selRecord=sequencingItemGrid.store;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;					
				}
			}
			var num=obj.get("laneNum");
			if(num!=""){
				for(var k=0;k<num;k++){
					if(!isRepeat){
						var ob = sequencingItemGrid.getStore().recordType;
						sequencingItemGrid.stopEditing();
						var p = new ob({});
						p.isNew = true;
						
						p.set("poolingCode",obj.get("poolingId"));
						p.set("laneCode",Number(max)+count);
						p.set("code",obj.get("code"));
						p.set("sampleCode",obj.get("sampleCode"));
						p.set("name",obj.get("name"));
						p.set("tempId",obj.get("id"));
						p.set("orderNumber",Number(max)+count);
						sequencingItemGrid.getStore().add(p);
						count++;
					}
				}
			}else{
				if(!isRepeat){
					var ob = sequencingItemGrid.getStore().recordType;
					sequencingItemGrid.stopEditing();
					var p = new ob({});
					p.isNew = true;
					
					p.set("poolingCode",obj.get("poolingId"));
					p.set("laneCode",Number(max)+count);
					p.set("code",obj.get("code"));
					p.set("sampleCode",obj.get("sampleCode"));
					p.set("name",obj.get("name"));
					p.set("tempId",obj.get("id"));
					p.set("orderNumber",Number(max)+count);
					sequencingItemGrid.getStore().add(p);
					count++;
				}
			}
			
			
		});
		sequencingItemGrid.startEditing(0, 0);
		}
}

//科技服务
function techDNAService(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
		loadDialogPage(null, biolims.common.scienceServiceTest, "/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogList.action", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				//var records = bloodSplitLeftPagedivGrid.store;
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						var id1=obj.get("id");
						$("#techJkServiceTask_id").val(id1);
						commonSearchAction(sequencingTempGrid);
//						ajax("post", "/experiment/sequencing/selTechJkServicTaskByid.action", {
//						id : id1
//						}, function(data) {
//							if(data.success){
////								alert(data.list);
//								$.each(data.list, function(a,b) {
//									var ob = sequencingItemGrid.getStore().recordType;
//									sequencingItemGrid.stopEditing();
//									var p= new ob({});
//									p.set("poolingCode",b.poolingCode);
//									p.set("poolingId",b.poolingId);
//									p.set("laneCode",b.lane);
//									p.set("code",b.code);
//									p.set("sampleCode",b.sampleCode);
//									p.set("name",b.name);
//									p.set("tempId",b.id);
//									p.set("orderNumber",a+1);
//									sequencingItemGrid.getStore().add(p);
//								});
//							}
//						});
					});
					
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function selectTemp(){
	if(window.location.href.indexOf("?")>0){
		location.href=window.location.href+"&method=window";
	}else{
		location.href=window.location.href+"?method=window";	
	}

}


