﻿function changeWidth(){
	
	$("#sequencingTemppage").css("width","40%");
	$("#markup").css("width","60%");
	sequencingTempGrid.getStore().reload();
	
	
}
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#sequencing_id").val();
	var state = $("#sequencing_state").val();
	var method = $("#sequencing_method").val();
	if(id=="" || state == "3"){
//		load("/experiment/sequencing/showSequencingTempList.action", { }, "#sequencingTemppage");
//		$("#markup").css("width","75%");

		if(method=='window'){
			$("#sequencingTemppage").remove();
			$("#markup").css("width","100%");
			selectTemp();
		}else{
			load("/experiment/sequencingLife/showSequencingTempList.action", { }, "#sequencingTemppage");
			$("#markup").css("width","75%");
		}
	}
	
	var mttf = $("#sequencing_template_templateFieldsCode").val();
	var mttfItem = $("#sequencing_template_templateFieldsItemCode").val();
	reloadSequencingItem(mttf, mttfItem);

});

function reloadSequencingItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = sequencingItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							sequencingItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							sequencingItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = sequencingResultGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							sequencingResultGrid.getColumnModel().setHidden(
									i, false);
						} else {
							sequencingResultGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}

function add() {
	window.location = window.ctx + "/experiment/sequencingLife/editSequencing.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/sequencingLife/showSequencingList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
	if($("#sequencing_testUserOneName").val()==""){
		message(biolims.sample.pleaseSelectExperimenter);
		return;
	}
				submitWorkflow("SequencingLifeTask", {
					userId : userId,
					userName : userName,
					formId : $("#sequencing_id").val(),
					title : $("#sequencing_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		if($("#sequencing_confirmUser_name").val()==""){
			message(biolims.sample.pleaseSelectReviewer);
			return;
		}
		if(sequencingResultGrid.getModifyRecord().length > 0){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
		var grid = sequencingResultGrid.store;
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("result")==""){
				message(biolims.sequencing.sequencingResultIsEmpty);
				return;
			}
		}
		completeTask($("#sequencing_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	//计算总原辅料量
//	var getRecord = sequencingReagentGrid.store;
//	var sum=0;
//	for(var j=0;j<getRecord.getCount();j++){
//		var num = getRecord.getAt(j).get("num");
//		sum+=Number(num);
//	}
//	document.getElementById('sequencing_reagent').value=sum;
	
	    var sequencingItemDivData = $("#sequencingItemdiv").data("sequencingItemGrid");
		document.getElementById('sequencingItemJson').value = commonGetModifyRecords(sequencingItemDivData);
	    var sequencingTemplateDivData = $("#sequencingTemplatediv").data("sequencingTemplateGrid");
		document.getElementById('sequencingTemplateJson').value = commonGetModifyRecords(sequencingTemplateDivData);
	    var sequencingReagentDivData = $("#sequencingReagentdiv").data("sequencingReagentGrid");
		document.getElementById('sequencingReagentJson').value = commonGetModifyRecords(sequencingReagentDivData);
	    var sequencingCosDivData = $("#sequencingCosdiv").data("sequencingCosGrid");
		document.getElementById('sequencingCosJson').value = commonGetModifyRecords(sequencingCosDivData);
	    var sequencingResultDivData = $("#sequencingResultdiv").data("sequencingResultGrid");
		document.getElementById('sequencingResultJson').value = commonGetModifyRecords(sequencingResultDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/sequencingLife/save.action";
		form1.submit();
	
		}
}	
function saveItem() {
	var id = $("#sequencing_id").val();
	if (!id) {
		return;
	}
	  var sequencingItemDivData = $("#sequencingItemdiv").data("sequencingItemGrid");
		var sequencingItemJson = commonGetModifyRecords(sequencingItemDivData);
	    var sequencingResultDivData = $("#sequencingResultdiv").data("sequencingResultGrid");
		var sequencingResultJson = commonGetModifyRecords(sequencingResultDivData);
		var sequencingTemplateDivData = $("#sequencingTemplatediv").data("sequencingTemplateGrid");
		var sequencingTemplateJson = commonGetModifyRecords(sequencingTemplateDivData);
		var sequencingReagentDivData = $("#sequencingReagentdiv").data("sequencingReagentGrid");
		var sequencingReagentJson = commonGetModifyRecords(sequencingReagentDivData);
		var sequencingCosDivData = $("#sequencingCosdiv").data("sequencingCosGrid");
		var sequencingCosJson = commonGetModifyRecords(sequencingCosDivData);
	
	
	ajax("post", "/experiment/sequencingLife/saveAjax.action", {
		sequencingItemJson:sequencingItemJson,
		sequencingResultJson:sequencingResultJson,
		sequencingTemplateJson:sequencingTemplateJson,
		sequencingReagentJson:sequencingReagentJson,
		sequencingCosJson:sequencingCosJson,
		id : id
	}, function(data) {
		if (data.success) {
			//message("保存成功！");
			if(data.equip!=0&&typeof(data.equip)!='undefined'){
				message(data.equip+biolims.common.cosDataSaveSuccess);
			}
			if(data.re!=0&&typeof(data.re)!='undefined'){
				message(data.re+biolims.common.reagentDataSaveSuccess);
			}
			sequencingItemDivData.getStore().reload();
			sequencingResultDivData.getStore().reload();
			sequencingTemplateDivData.getStore().reload();
			sequencingReagentDivData.getStore().reload();
			sequencingCosDivData.getStore().reload();
		} else {
			message(biolims.common.saveFailed);
		}
	}, null);
}
function editCopy() {
	window.location = window.ctx + '/experiment/sequencingLife/copySequencing.action?id=' + $("#sequencing_id").val();
}
/*function changeState() {
	commonChangeState("formId=" + $("#sequencing_id").val() + "&tableId=sequencing");
}*/
$("#toolbarbutton_status").click(function(){
	var flag=false;
	var grid1 = sequencingResultGrid.store;
	var grid2 = sequencingItemGrid.store;
	commonChangeState("formId=" + $("#sequencing_id").val() + "&tableId=SequencingLifeTask");
	//判断是否有不合格的pooling
//	for(var i=0; i<grid1.getCount(); i++){
//		var re = grid1.getAt(i).get("result");
//		if(re==1){
//			message("存在不合格的POOLING！");
//			flag=true;
//			break;
//		}
//	}
//	//判断是否存在未使用的实验材料
//	if(!flag){
//		if(grid1.getCount()!=grid2.getCount()){
//			message("存在未使用的POOLING！");
//		}else if ($("#sequencing_id").val()){
//			commonChangeState("formId=" + $("#sequencing_id").val() + "&tableId=Sequencing");
//		}	
//	}
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#sequencing_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#sequencing_template").val());
	nsc.push(biolims.common.templateEmpty);
//	fs.push($("#sequencing_acceptUser").val());
//	nsc.push(biolims.common.createUserEmpty);
//	fs.push($("#sequencing_fcCode").val());
//	nsc.push(biolims.sequencing.fcCodeIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sequencing.computerSequencing,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/sequencingLife/showSequencingItemList.action", {
				id : $("#sequencing_id").val()
			}, "#sequencingItempage");
load("/experiment/sequencingLife/showSequencingTemplateList.action", {
				id : $("#sequencing_id").val()
			}, "#sequencingTemplatepage");
load("/experiment/sequencingLife/showSequencingReagentList.action", {
				id : $("#sequencing_id").val()
			}, "#sequencingReagentpage");
load("/experiment/sequencingLife/showSequencingCosList.action", {
				id : $("#sequencing_id").val()
			}, "#sequencingCospage");
load("/experiment/sequencingLife/showSequencingResultList.action", {
				id : $("#sequencing_id").val()
			}, "#sequencingResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
//加载原辅料规格
function SpecFun(){
	var win = Ext.getCmp('SpecFun');
	if (win) {win.close();}
	var SpecFun= new Ext.Window({
	id:'SpecFun',modal:true,title:biolims.sequencing.selectREFCode,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/storage/showSpecSelectList.action?flag=SpecFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
	 SpecFun.close(); }  }]  }); 
	 SpecFun.show(); 
}
function setSpecFun(rec){
	
	document.getElementById('sequencing_refCode').value=rec.get('id');
	document.getElementById('sequencing_spec').value=rec.get('name');
	document.getElementById('sequencing_refName').value=rec.get('spec');
	var win = Ext.getCmp('SpecFun');
	if(win){
		win.close();
	}
}
//调用模板
function TemplateFun(){
	var type="dosequenceLife";
	var win = Ext.getCmp('TemplateFun');
	if (win) {win.close();}
	var TemplateFun= new Ext.Window({
	id:'TemplateFun',modal:true,title:biolims.common.selectTemplate,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
	 TemplateFun.close(); }  }]  }); 
	 TemplateFun.show(); 
}
function setTemplateFun(rec){
	if($("#sequencing_acceptUser_name").val()==""){
		document.getElementById('sequencing_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('sequencing_acceptUser_name').value=rec.get('acceptUser-name');
	}

	
	var code=$("#sequencing_template").val();
	if(code==""){
		var cid=rec.get('id');
//		ajax("post", "/system/template/template/findInstrument.action", {
//			code : cid
//		}, function(data) {
//			if (data.success) {
//				if (data.data) {
//					message("设备被占用！");
//					var win = Ext.getCmp('TemplateFun');
//	 				if(win){win.close();}
//					return;
//				}else{
					document.getElementById('sequencing_template').value=rec.get('id');
					document.getElementById('sequencing_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id,
					}, function(data) {
						if (data.success) {	
							var ob = sequencingTemplateGrid.getStore().recordType;
							sequencingTemplateGrid.stopEditing();								
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
//								p.set("tCos",obj.id);
//								p.set("type-id",obj.typeId);
//								p.set("type-name",obj.typeName);
//								p.set("isGood",obj.isGood);
//								p.set("itemId",obj.itemId);									
//								p.set("temperature",obj.temperature);
//								p.set("speed",obj.speed);
//								p.set("time",obj.time);
//								p.set("note",obj.note);
								p.set("tItem",obj.id);
								p.set("stepNum",obj.code);
								p.set("stepName",obj.name);
								sequencingTemplateGrid.getStore().add(p);							
							});			
							sequencingTemplateGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
//						if (data.success) {
//							var ob = sequencingTemplateGrid.getStore().recordType;
//							sequencingTemplateGrid.stopEditing();
//							$.each(data.data, function(i, obj) {
//								var p = new ob({});
//								p.isNew = true;
//								p.set("tItem",obj.id);
//								p.set("stepNum",obj.code);
//								p.set("stepName",obj.name);
//								p.set("note",obj.note);
//								sequencingTemplateGrid.getStore().add(p);							
//							});
//							sequencingTemplateGrid.startEditing(0, 0);		
//						} else {
//							message(biolims.common.anErrorOccurred);
//						}
//						
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id,
					}, function(data) {
						if (data.success) {	
							var ob = sequencingReagentGrid.getStore().recordType;
							sequencingReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								
								p.set("tReagent",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
								p.set("item",obj.itemId);
								
								p.set("oneNum",obj.num);
								sequencingReagentGrid.getStore().add(p);							
							});
							sequencingReagentGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id,
					}, function(data) {
						if (data.success) {	
							var ob = sequencingCosGrid.getStore().recordType;
							sequencingCosGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								
								p.set("tCos",obj.id);
								p.set("type-id",obj.typeId);
								p.set("type-name",obj.typeName);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								p.set("state",obj.state);
								p.set("temperature",obj.temperature);
								p.set("speed",obj.speed);
								p.set("time",obj.time);
								p.set("note",obj.note);
								sequencingCosGrid.getStore().add(p);							
							});			
							sequencingCosGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
//				}
//			}
//		}, null);
		
	}else{
		var flag = true;
		if(rec.get('id')==code){
			flag = confirm(biolims.common.whetherOrNot2Load);
			 }
		if(flag==true){
//			var cid=rec.get('id');
//			ajax("post", "/system/template/template/findInstrument.action", {
//				code : cid
//			}, function(data) {
//				if (data.success) {
//					if (data.data) {
//						message("设备被占用！");
//						var win = Ext.getCmp('TemplateFun');
//		 				if(win){win.close();}
//						return;
//					}else{
						var ob1 = sequencingTemplateGrid.store;
						if (ob1.getCount() > 0) {
						for(var j=0;j<ob1.getCount();j++){
							var oldv = ob1.getAt(j).get("id");
							//=====================2015-11-30 ly======================//
							//根据ID删除
							if(oldv!=null){
								ajax("post", "/experiment/sequencingLife/delSequencingTemplateOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null);
							}else{								
								sequencingTemplateGrid.store.removeAll();
							}
						}
						sequencingTemplateGrid.store.removeAll();
	 				}
	 				//===========================================//
					var ob2 = sequencingReagentGrid.store;
					if (ob2.getCount() > 0) {
						for(var j=0;j<ob2.getCount();j++){
							var oldv = ob2.getAt(j).get("id");
							//=====================2015-11-30 ly======================//
							//根据ID删除
							if(oldv!=null){
							ajax("post", "/experiment/sequencingLife/delSequencingReagentOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null); 
							}else{
								sequencingReagentGrid.store.removeAll();
							}
						}
						sequencingReagentGrid.store.removeAll();
	 				}
					//=========================================
					var ob3 = sequencingCosGrid.store;
					if (ob3.getCount() > 0) {
						for(var j=0;j<ob3.getCount();j++){
							var oldv = ob3.getAt(j).get("id");
							//=====================2015-11-30 ly======================//
							//根据ID删除
							if(oldv!=null){
								ajax("post", "/experiment/sequencingLife/delSequencingCosOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null); 
							}else{
								sequencingCosGrid.store.removeAll();
							}
						}
						sequencingCosGrid.store.removeAll();
	 				}
				
					document.getElementById('sequencing_template').value=rec.get('id');
	 				document.getElementById('sequencing_template_name').value=rec.get('name');
	 				var win = Ext.getCmp('TemplateFun');
	 				if(win){win.close();}
					var id = rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = sequencingTemplateGrid.getStore().recordType;
								sequencingTemplateGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("stepNum",obj.code);
									p.set("stepName",obj.name);
									sequencingTemplateGrid.getStore().add(p);							
								});
								
								sequencingTemplateGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = sequencingReagentGrid.getStore().recordType;
								sequencingReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									sequencingReagentGrid.getStore().add(p);							
								});
								sequencingReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = sequencingCosGrid.getStore().recordType;
								sequencingCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("state",obj.state);
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									sequencingCosGrid.getStore().add(p);							
								});			
								sequencingCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null);
					}
				}
//			}, null);
//		}	 				
//	}
	reloadSequencingItem(rec.get("templateFieldsCode"), rec
			.get("templateFieldsItemCode"));

}
//按条件加载原辅料
function showReagent(){
	//获取选择的数据
	var selectRcords=sequencingTemplateGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=sequencingTemplateGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#sequencing_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/sequencingLife/showSequencingReagentList.action", {
			id : $("#sequencing_id").val()
		}, "#sequencingReagentpage");
	}else if(length1==1){
//		$.each(selectRcords, function(i, obj) {
//			var code=obj.get("stepNum");
//			load("/experiment/sequencing/showSequencingReagentList.action", {
//				id : $("#sequencing_id").val(),
//				itemId : code
//			}, "#sequencingReagentpage");
//		});	
		sequencingReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("stepNum");
		ajax("post", "/experiment/sequencingLife/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = sequencingReagentGrid.getStore().recordType;
				sequencingReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					//p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("expireDate",obj.expireDate);
					p.set("sn",obj.sn);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("sequencing-id",obj.tId);
					p.set("sequencing-name",obj.tName);
					
					sequencingReagentGrid.getStore().add(p);							
				});
				sequencingReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.selectRecord);
		return;
	}
	
}
//选择设备	
function selectinstrument(){
	

	 var options = {};
	 options.width = 900;
	 options.height = 600;
	 loadDialogPage(null, "Selection instrument", "/equipment/main/instrumentSelect.action?p_type=1", {
	 "Confirm" : function() {
	 var operGrid = $("#show_dialog_instrument_div").data("instrumentDialogGrid");
	 var selectRecord = operGrid.getSelectionModel().getSelections();
	 if (selectRecord.length > 0) {
		 $.each(selectRecord, function(i, obj) {
			 document.getElementById("sequencing_deviceCode").value=obj.get("id");
			 document.getElementById("sequencing_deviceCode_name").value=obj.get("name");
		 });	
	 }else{
	 message(biolims.common.selectYouWant);
	 return;}
	 $(this).dialog("close");}
	 }, true, options);
//	 
//		var win = Ext.getCmp('selectinstrument');
//		if (win) {win.close();}
//		var selectinstrument= new Ext.Window({
//		id:'selectinstrument',modal:true,title:biolims.common.selectInstrument,layout:'fit',width:700,height:500,closeAction:'close',
//		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//		collapsible: true,maximizable: true,
//		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/equipment/main/instrumentSelect.action?p_type=1&flag=InstrumentFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
//		buttons: [
//		{ text: biolims.common.close,
//		 handler: function(){
//			 selectinstrument.close(); }  }]  });     selectinstrument.show(); 
}
//function setInstrumentFun(rec){
//		document.getElementById("sequencing_deviceCode").value=rec.get('id');
//		document.getElementById("sequencing_deviceCode_name").value=rec.get('note');
//		var win = Ext.getCmp('selectinstrument');
//		if(win){
//			win.close();
//		}
//}

//按条件加载设备
function showCos(){
	//获取选择的数据
	var selectRcords=sequencingTemplateGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=sequencingTemplateGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#sequencing_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/sequencingLife/showSequencingCosList.action", {
			id : $("#sequencing_id").val()
		}, "#sequencingCospage");
	}else if(length1==1){
//		var code="";
//		$.each(selectRcords, function(i, obj) {
//			code=obj.get("stepNum");
//			load("/experiment/sequencing/showSequencingCosList.action", {
//				id : $("#sequencing_id").val(),
//				itemId : code
//			}, "#sequencingCospage");
//		});
		sequencingCosGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("stepNum");
		ajax("post", "/experiment/sequencingLife/setCos.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = sequencingCosGrid.getStore().recordType;
				sequencingCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("type-id",obj.typeId);
					p.set("type-name",obj.typeName);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("note",obj.note);
					p.set("time",obj.time);
					p.set("state",obj.state);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tCos",obj.tCos);
					p.set("sequencing-id",obj.tId);
					p.set("sequencing-name",obj.tName);
					
					sequencingCosGrid.getStore().add(p);							
				});
				sequencingCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});	
	}else{
		message(biolims.common.selectRecord);
		return;
	}
	
}

var loadtestUser;
//选择实验组用户
function testUser(){
	var gid=$("#sequencing_acceptUser").val();
	if(gid!=""){
		var options = {};
		options.width = 500;
		options.height = 500;
		var confirm=biolims.common.confirm;
		loadtestUser=loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			confirm : function() {
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$("#sequencing_reciveUser").val(selectRecord[0].get("user-id"));
					$("#sequencing_reciveUser_name").val(selectRecord[0].get("user-name"));
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}else{
		message(biolims.common.pleaseSelectGroup);
	}
	
}
function setUserGroupUser(){
	var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
			$("#sequencing_reciveUser").val(selectRecord[0].get("user-id"));
			$("#sequencing_reciveUser_name").val(selectRecord[0].get("user-name"));
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadtestUser.dialog("close");
}


function selectTemp(){
	var options = {};
	options.width = document.body.clientWidth*0.9;
	options.height = document.body.clientHeight*0.9;
	var url = "/experiment/sequencingLife/showSequencingTempList.action";
	loadDialogPage(null, biolims.common.approvalTask, url, {}, true, options);
}


//选择实验组用户
function testUser(type){
	var gid=$("#sequencing_acceptUser").val();
	if(gid!=""){
		var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			"Confirm" : function() {
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					var id=[];
					var name=[];
					for(var i=0;i<selectRecord.length;i++){
						id.push(selectRecord[i].get("user-id"));
						name.push(selectRecord[i].get("user-name"));
					}
					if(type==1){//第一批实验员
						$("#sequencing_testUserOneId").val(id);
						$("#sequencing_testUserOneName").val(name);
					}else if(type==2){//第二批实验员
						$("#sequencing_testUserTwoId").val(id);
						$("#sequencing_testUserTwoName").val(name);
					}else if(type==3){//审核人
						$("#sequencing_confirmUser").val(id);
						$("#sequencing_confirmUser_name").val(name);
					}
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}else{
		message(biolims.common.pleaseSelectGroup);
	}
	
}