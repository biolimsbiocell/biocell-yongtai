var oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "sampleOrder-id",
		"title":"关联订单",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.master.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleType");
		}
	})
	colOpts.push({
		"data": "dicSampleType-id",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data": "dicSampleType-name",
		"title":biolims.common.dicSampleTypeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-name");
		}
	})
	colOpts.push({
		"data": "concentration",
		"title": biolims.common.concentration,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "concentration");
		}
	})
	colOpts.push({
		"data": "volume",
		"title": biolims.common.volume,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
		}
	})
	colOpts.push({
		"data": "sumTotal",
		"title": biolims.common.sumNum,
//		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "sumTotal");
		}
	})
	colOpts.push({
		"data": "rin",
		"title": "RIN",
		"className":"edit",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "rin");
		}
	})
	colOpts.push({
		"data": "sampleInfo-id",
		"title": "SampleInfo",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleInfo-id");
		}
	})
	colOpts.push({
		"data": "od230",
		"title": "OD260/OD230",
		"className":"edit",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "od230");
		}
	})
	colOpts.push({
		"data": "od260",
		"title": "OD260/OD280",
		"className":"edit",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "od260");
		}
	})
	colOpts.push({
		"data": "qbcontraction",
		"title": biolims.common.qbConcentration,
		"className":"edit",
		"visible":false,
		"createdCell": function(td) {
			$(td).attr("saveName", "qbcontraction");
		}
	})
	colOpts.push({
		"data": "result",
		"title": biolims.common.result+'<img src="/images/required.gif"/>',
		"className":"select",
		"name":biolims.common.qualified+"|"+biolims.common.disqualified,
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", biolims.common.qualified+"|"+biolims.common.disqualified);
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}
		}
	})
	colOpts.push({
		"data": "nextFlowId",
		"title": biolims.common.nextFlowId,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
		"data": "nextFlow",
		"title": biolims.common.nextFlow+'<img src="/images/required.gif"/>',
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlow");
		}
	})
	colOpts.push({
		"data": "submit",
		"title": biolims.common.toSubmit,
		"name":biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "submit");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			else if(data == "0") {
				return biolims.common.no;
			}else {
				return "";
			}
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"className":"edit",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "note");
			$(td).attr("sampleInfo-id", rowData['sampleInfo-id']);
		}
	})
	window.localStorage.clear();
	var visibleItem = $("#templateFieldsItemCode").val();
	if(visibleItem) {
		var visibleItemArr = visibleItem.split(",");
		visibleItemArr.forEach(function(v, i) {
			colOpts.forEach(function(vv, ii) {
				if(v == vv.data) {
					vv.visible=true;
				}
			});
		});
	}
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#dnaTask_state").text()!="Complete"){
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#dnaTaskResultdiv"),
				"/experiment/dna/dnaTask/delDnaTaskResult.action","核酸提取结果删除样本：",$("#dnaTask_id").text());
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-th"></i>'+biolims.common.applicationOper,
		action: function() {
			$.ajax({
				type: "post",
				data: {
					id:$("#dnaTask_id").text()
				},
				url: ctx + "/experiment/dna/dnaTask/bringResult.action",
				success: function(data) {
					var data = JSON.parse(data)
					if(data.success) {
						dnaTaskResultTab.ajax.reload();
					} else {
						top.layer.msg(biolims.purchase.failed)
					}
	
				}
			});
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-share-alt"></i>'+biolims.common.uploadResult,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#dnaTask_id").text(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/dna/dnaTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							dnaTaskResultTab.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.batchResult,
		className: 'btn btn-sm btn-success resultsBatchBtn',
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-floppy-open"></i>'+biolims.common.uploadAttachment,
		action: function() {
			$("#uploadFile").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput =fileInput('1', 'dnaTask', $("#dnaTask_id").text());
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-paypal"></i> '+biolims.common.nextFlow,
		action: function() {
			nextFlow();
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem();
		}
	});
	tbarOpts.push({
		text: biolims.common.submitSample,
		action: function() {
			submitSample();
		}
	});
	//添加浓度按钮
	tbarOpts.push({
		text: biolims.common.concentration,
		className: 'btn btn-sm btn-success btnConcentration',
		action: function() {
			submitConcentration();
		}
	});
	//添加体积按钮
	tbarOpts.push({
		text: biolims.common.volume,
		className: 'btn btn-sm btn-success btnVolume',
		action: function() {
			submitVolume();
		}
	});
	//添加下载Exel按钮
	tbarOpts.push({
		text : biolims.common.downloadCsvTemplet,
		action : function() {
			downLoadTemp();
		}
	});
	}
	var dnaTaskResultOps = table(true, $("#dnaTask_id").text(), "/experiment/dna/dnaTask/showDnaTaskResultTableJson.action", colOpts, tbarOpts);
	dnaTaskResultTab = renderData($("#dnaTaskResultdiv"), dnaTaskResultOps);
	dnaTaskResultTab.on('draw', function() {
		oldChangeLog = dnaTaskResultTab.ajax.json();
	});
	
	//批量结果
	btnChangeDropdown ($('#dnaTaskResultdiv'),$(".resultsBatchBtn"),[biolims.common.qualified,biolims.common.disqualified],"result");
	bpmTask($("#bpmTaskId").val());
	//上一步下一步操作
	preAndNext();
	if(handlemethod == "view"||$("#dnaTask_state").text()=="Complete"){
		settextreadonly();
		$("#save").hide()
		$("#sp").hide()
		$("#finish").hide()
	}
	
});
//下一步流向
function nextFlow() {
	var rows = $("#dnaTaskResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var productId = "";
	var sampleType="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		sampleType = $(k).find("td[savename='dicSampleType-id']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=DnaTask&productId="
					+ productIds+"&sampleType="+sampleType,''],
		yes: function(index, layer) {
			var name1 = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name1);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}
// 保存
function saveItem() {
	var ele=$("#dnaTaskResultdiv");
	var changeLog = "核酸提取-实验结果：";
	var data = saveItemjson(ele);
	if(!data){
		return false;
	}
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog != "核酸提取-实验结果："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3});
	$.ajax({
		type: 'post',
		url: '/experiment/dna/dnaTask/saveResult.action',
		data: {
			id: $("#dnaTask_id").text(),
			dataJson: data,
			logInfo: changeLogs,
			confirmUser:$("#confirmUser_id").val()
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag=true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			// 判断男女并转换为数字
			if(k == "result") {
				var result = $(tds[j]).text();
				if(result == biolims.common.disqualified) {
					json[k] = "0";
				} else if(result == biolims.common.qualified) {
					json[k] = "1";
				}
				continue;
			}
			if(k == "submit"){
				var submit = $(tds[j]).text();
				if(submit == biolims.common.no) {
					json[k] = "0";
				} else if(submit == biolims.common.yes) {
					json[k] = "1";
				}
				continue;	
			}
			if(k=="nextFlow"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.pleaseSelectNextFlow);
					return false;
				}
			}
			if(k=="result"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.chooseTaskResult);
					return false;
				}
			}
			if(k == "note") {
				json["sampleInfo-id"] = $(tds[j]).attr("sampleInfo-id");
			}
			
		}
		data.push(json);
	});
	if(flag){
		return JSON.stringify(data);
	}else{
		return false;
	}
}

//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/dna/dnaTask/showDnaTaskSteps.action?id=" + $("#dnaTask_id").text()+"&bpmTaskId="+$("#bpmTaskId").val();
	});
	//上一步操作
	$("#finish").click(function() {
	var paraStr = "formId=" + $("#dnaTask_id").text() +
		"&tableId=DnaTask";
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: $("#dnaTask_id").text()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload()
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		}

	});
});
	
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function submitSample(){
	var rows = $("#dnaTaskResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids=[];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val())
	});
	ajax("post", "/experiment/dna/dnaTask/submitSample.action", {
		ids: ids,
		id: $("#dnaTask_id").text()
	}, function(data) {
		if(data.success) {
			top.layer.msg(biolims.common.submitSuccess);
			tableRefresh();
		} else {
			top.layer.msg(biolims.common.submitFail);
		}
	}, null)
}
function sp() {
	if(!$("#confirmUser_id").val()){
		top.layer.open({
			title: biolims.sample.pleaseSelectReviewer,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
			yes: function(index, layer) {
				var name1 = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
				$("#confirmUser_name").text(name1);
				$("#confirmUser_id").val(id);
				saveItem();
				top.layer.close(index);
			},
			end:function(){
				splc();
			}
		})
	}else{
		splc();
	}
}
function splc(){

	var taskId = $("#bpmTaskId").val();
	var formId = $("#dnaTask_id").text();

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								window.open(window.ctx+"/main/toPortal.action",'_parent');
								top.layer.close(index);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
		}
	});
}
//赋值勾选的浓度
function submitConcentration(){
	var rows = $("#dnaTaskResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=concentration]").text());
	});
	//获取勾选的浓度得第一个值,最后要用的值
	var concentrationValue=concentrationList[0];
	$.each(rows, function(i, v) {
		$(v).find("td[savename=concentration]").text(concentrationValue);
		$(v).addClass("editagain");
	});
	return concentrationValue;
}

//赋值勾选的体积
function submitVolume(){
	var rows = $("#dnaTaskResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=volume]").text());
		//获取勾选的浓度得第一个值,最后要用的值
		var volumeValue=concentrationList[0];
		$(v).find("td[savename=volume]").text(volumeValue);
		$(v).addClass("editagain");
	});
	return volumeValue;
}
//downLoadTemp
function downLoadTemp(){
	var rows = $("#dnaTaskResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids = [];
	var codes = [];
	$.each(rows, function(i, k) {
		ids.push($(k).find("input[type=checkbox]").val());
		codes.push($(k).find("td[savename=code]").text());
	});
	if ("" == codes) {
		message(biolims.common.saveDownload);
		return;
	}
	window.open(window.ctx + '/experiment/dna/dnaTask/downLoadTemp.action?codes='+codes+'&ids='+ids);
}