﻿var massarrayExtensionTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'expCode',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	fields.push({
		name:'colCode',
		type:"string"
	});
    fields.push({
		name:'counts',
		type:"string"
	});
    fields.push({
		name:'sampleName',
		type:"string"
    });
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'sampleVolume',
		type:"string"
	});
    fields.push({
		name:'addVolume',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'indexs',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
    fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'concentration',
		type:"string"
	});
	fields.push({
		name:'result',
		type:"string"
	});
	fields.push({
		name:'reason',
		type:"string"
	});
	fields.push({
		name:'stepNum',
		type:"string"
	});
    fields.push({
	    name:'massarrayExtensionTask-id',
	    type:"string"
	});
	fields.push({
		name:'massarrayExtensionTask-name',
		type:"string"
	});
	fields.push({
		name:'orderNumber',
		type:"string"
	});
	fields.push({
		name:'projectId',
		type:"string"
	});
	fields.push({
		name:'contractId',
		type:"string"
	});
	fields.push({
		name:'orderType',
		type:"string"
	});
    fields.push({
		name:'taskId',
		type:"string"
	});  
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'productNum',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'sampleConsume',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    fields.push({
    	name:'oligoName',
    	type:"string"
    });
    fields.push({
    	name:'oligoMass',
    	type:"string"
    });
    fields.push({
    	name:'oligoConcentration',
    	type:"string"
    });
    fields.push({
    	name:'oligoConcentrationInPool',
    	type:"string"
    });
    fields.push({
    	name:'zuHe',
    	type:"string"
    });
    fields.push({
    	name:'oligoUl',
    	type:"string"
    });
    fields.push({
    	name:'yongShui',
    	type:"string"
    });
    fields.push({
    	name:'yongShuiUl',
    	type:"string"
    });
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'expCode',
		hidden : true,
		header:'实验编号',
		width:20*6
	});	
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'样本编号',
		sortable:true,
		width:20*7,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:'实验室样本号',
		width:20*6
	});
	//低糖名字
	cm.push({
		dataIndex:'oligoName',
		hidden : false,
		header:'oligoName',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//低糖的质量
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '高' ], 
		         [ '2', '中' ] , 
		         [ '3', '低' ], ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local',
	});
	cm.push({
		dataIndex:'oligoMass',
		hidden : false,
		header:'oligoMass',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	
	//低糖浓度
	
	cm.push({
		dataIndex:'oligoConcentration',
		hidden : false,
		header:'oligoConcentration',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//低聚糖浓度值
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '高（15pM)' ], 
		         [ '2', '中（10pM)' ] , 
		         [ '3', '低（5pM)' ], ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local',
	});
	cm.push({
		dataIndex:'oligoConcentrationInPool',
		hidden : false,
		header:'oligoConcentrationInPool',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	//组合总量
	cm.push({
		dataIndex:'zuHe',
		hidden : false,
		header:'组合总量',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'oligoUl',
		hidden : false,
		header:'oligo 用量（ul）',
		width:20*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'yongShui',
		hidden : false,
		header:'所用水',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'yongShuiUl',
		hidden : false,
		header:'所用水量',
		width:20*6
	});
//	cm.push({
//		dataIndex:'sampleType',
//		hidden : false,
//		header:'样本类型',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'indexs',
		hidden : true,
		header:'Index',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:'身份证号',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:'手机号',
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		hidden : true,
		width:20*6,
		sortable:true
	});
//	cm.push({
//		dataIndex:'productName',
//		header:'检测项目',
//		width:20*6,
//		hidden : false
//	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目ID',
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		hidden : true,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		hidden : true,
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:'中间产物类型编号',
		width:20*6
	});
	var testDicType =new Ext.form.TextField({
        allowBlank: false
	});
	testDicType.on('focus', function() {
		loadTestDicType();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:'中间产物类型',
		width:15*10,
		editor : testDicType
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'Name',
		hidden : true,
		header:'样本名称',
		width:20*6,
	})
	var testDicType =new Ext.form.TextField({
	        allowBlank: false
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : false,
		header:'样本用量',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:'取样体积（μl）',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : true,
		header:'补充体积（μl）',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : true,
		header:'总体积（μl）',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});	
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同编号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:'科技服务任务单',
		width:20*6
	});	
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务 ',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'浓度',
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:30*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:30*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:30*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:30*6
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cm.push({
		dataIndex:'massarrayExtensionTaskCode',
		hidden : true,
		header:'MassarrayExtension任务编号',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:'实验排序号',
		width:15*10
	});
	cm.push({
		dataIndex:'productNum',
		hidden : false,
		header:'中间产物数量',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:'接收备注',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title="MassarrayExtension任务明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		iconCls : 'add',
//		handler : function() {
//			var ob = sampleReceiveItemGrid.getStore().recordType;
//			var p = new ob({});
//			p.isNew = true;
//			p.set("sampleNum","1");
//			p.set("isGood","1");
//			p.set("method","1");
//			sampleReceiveItemGrid.stopEditing();
//			sampleReceiveItemGrid.getStore().add(p);
//			sampleReceiveItemGrid.startEditing(0, 0);
//		}
//	});
	var state=$("#massarrayExtensionTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/delMassarrayExtensionTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				massarrayExtensionTaskItemGrid.getStore().commitChanges();
				massarrayExtensionTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "批量数据",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_item_div"), "批量数据", null, {
				"确定" : function() {
					var records = massarrayExtensionTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var Consume = $("#Consume").val();
						var Volume = $("#Volume").val();
						var addVolume = $("#addVolume").val();
						massarrayExtensionTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("sampleConsume", Consume);
							obj.set("sampleVolume", Volume);
							obj.set("addVolume", addVolume);
						});
						massarrayExtensionTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "产物类型",
		handler : loadTestDicType
	});
	opts.tbar.push({
		text : "批量产物数量",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_productNum_div"), "批量产物数量", null, {
				"确定" : function() {
					var records = massarrayExtensionTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var productNum = $("#productNum").val();
						massarrayExtensionTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("productNum", productNum);
						});
						massarrayExtensionTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	//按钮
	opts.tbar.push({
			text : '计算oligo用量',
			handler : loadNums
		});
	opts.tbar.push({
		text : '计算所用水量',
		handler : yongshuiliang
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler :  function() {
			var ob = massarrayExtensionTaskItemGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			p.set("sampleNum","1");
			p.set("isGood","1");
			p.set("method","1");
			massarrayExtensionTaskItemGrid.stopEditing();
			massarrayExtensionTaskItemGrid.getStore().add(p);
			massarrayExtensionTaskItemGrid.startEditing(0, 0);
		}
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	massarrayExtensionTaskItemGrid=gridEditTable("massarrayExtensionTaskItemdiv",cols,loadParam,opts);
	$("#massarrayExtensionTaskItemdiv").data("massarrayExtensionTaskItemGrid", massarrayExtensionTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectprojectFun(){
	var win = Ext.getCmp('selectproject');
	if (win) {win.close();}
	var selectproject= new Ext.Window({
	id:'selectproject',modal:true,title:'选择检测项目',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/com/biolims/system/product/productSelect.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectproject.close(); }  }]  });     selectproject.show(); }
	function setProductFun1(rec){
		var gridGrid = $("#massarrayExtensionTaskItemdiv").data("massarrayExtensionTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('product-id',rec.get('id'));
			obj.set('product-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectproject');
		if(win){
			win.close();
		}
	}
	
function setRowSelected(rowIndex){
    //设置rowIndex行被选中   
	massarrayExtensionTaskTemplateItemGrid.getSelectionModel().selectRow(rowIndex);  
}  

var loadDicType;	
//查询样本类型
function loadTestDicType(){
	var options = {};
	options.width = document.body.clientWidth-800;
	options.height = document.body.clientHeight-40;
	loadDicType=loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
		"确定" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = massarrayExtensionTaskItemGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					$.each(records, function(a, b) {
						b.set("dicSampleType-id", obj.get("id"));
						b.set("dicSampleType-name", obj.get("name"));
					});
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}

function setDicType(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = massarrayExtensionTaskItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$.each(records, function(a, b) {
				b.set("dicSampleType-id", obj.get("id"));
				b.set("dicType-name", obj.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadDicType.dialog("close");
}
//自动计算
function loadNums(){
	var selectRecord=massarrayExtensionTaskItemGrid.getSelectionModel().getSelections();
//	alert(11);
	var selAll=massarrayExtensionTaskItemGrid.store;
	
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			if(obj.get("zuHe")!=null && obj.get("oligoConcentrationInPool")!=null && obj.get("oligoConcentration")!=null
					&& obj.get("oligoConcentration")!="0"){
				var a ;
			if(obj.get("oligoConcentrationInPool")=="1"){
				a="15";
			}else if(obj.get("oligoConcentrationInPool")=="2"){
				a="10";
			}else if(obj.get("oligoConcentrationInPool")=="3"){
				a="5";
			}else {
				a="";
			}
				var qm=parseFloat(obj.get("zuHe"))*a/ parseFloat(obj.get("oligoConcentration"));
				var qm1=qm.toFixed(2);
				obj.set("oligoUl",qm1);
			}else{
				message("请填写正确的数据！");
				return;
			}
		});
	}else{
		if(selAll.getCount()>0){
			for(var i=0;i<selAll.getCount();i++){
				if(selAll.getAt(i).get("zuHe")!=null && selAll.getAt(i).get("oligoConcentrationInPool")!=null && selAll.getAt(i).get("oligoConcentration")!=null
						&& selAll.getAt(i).get("oligoConcentration")!="0"){
					var a ;
					if(selAll.getAt(i).get("oligoConcentrationInPool")=="1"){
						a="15";
					}else if(selAll.getAt(i).get("oligoConcentrationInPool")=="2"){
						a="10";
					}else if(selAll.getAt(i).get("oligoConcentrationInPool")=="3"){
						a="5";
					}else {
						a="";
					}
					var qm=parseFloat(selAll.getAt(i).get("zuHe"))* a / parseFloat(selAll.getAt(i).get("oligoConcentration"));
					var qm1=qm.toFixed(2);
					selAll.getAt(i).set("oligoUl",qm1);
				}else{
					message("请填写正确的数据！");
					return;
				}
			}
		}
	}
}
//计算所用水量
function yongshuiliang(){
	var selectRecord=massarrayExtensionTaskItemGrid.getSelectionModel().getSelections();
	var selAll=massarrayExtensionTaskItemGrid.store;
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			if(obj.get("yongShui")!=null && obj.get("oligoUl")!=null){
				var qm=parseFloat(obj.get("yongShui")) - parseFloat(obj.get("oligoUl"));
				var qm1=qm.toFixed(2);
				obj.set("yongShuiUl",qm1);
			}else{
				message("请填写正确的数据！");
				return;
			}
		});
	}else{
		if(selAll.getCount()>0){
			for(var i=0;i<selAll.getCount();i++){
				if(selAll.getAt(i).get("yongShui")!=null && selAll.getAt(i).get("oligoUl")!=null){
					var qm=parseFloat(selAll.getAt(i).get("yongShui")) - parseFloat(selAll.getAt(i).get("oligoUl"));
					var qm1=qm.toFixed(2);
					selAll.getAt(i).set("yongShuiUl",qm1);
				}else{
					message("请填写正确的数据！");
					return;
				}
			}
		}
	}
}
