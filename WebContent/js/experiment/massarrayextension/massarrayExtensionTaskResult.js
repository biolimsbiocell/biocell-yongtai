﻿var massarrayExtensionTaskResultGrid1;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'volume',
		type:"string"
	});
	fields.push({
		name:'unit',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'massarrayExtensionTask-id',
		type:"string"
	});
	fields.push({
		name:'massarrayExtensionTask-name',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'od260',
		type:"string"
	});
    fields.push({
		name:'od280',
		type:"string"
	});
    fields.push({
		name:'rin',
		type:"string"
	});
    fields.push({
		name:'contraction',
		type:"string"
	});
    fields.push({
		name:'endDate',
		type:"string"
	});
    fields.push({
		name:'taskId',
		type:"string"
	});
    fields.push({
		name:'dicType-id',
		type:"string"
	});
    fields.push({
		name:'dicType-name',
		type:"string"
	});  
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'MassarrayExtension任务编号',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dicType-id',
		hidden:true,
		header:'样本类型编号',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'dicType-name',
		header:'样本类型名称',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'od260',
		hidden : false,
		header:'OD260/230',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od280',
		hidden : false,
		header:'OD260/280',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contraction',
		hidden : false,
		header:'浓度',
		width:20*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : false,
		header:'总量',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rin',
		hidden : false,
		header:'RIN',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '合格' ], [ '1', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果判定',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '文库构建' ], [ '1', '重新MassarrayExtension任务' ],[ '2', '入库' ],[ '3', '反馈至项目组' ],[ '4', '终止' ],['5','MassarrayExtension任务检测'] ]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:'下一步流向',
		width:20*6,
		editor : nextFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '文库构建' ], [ '1', '重MassarrayExtension任务' ],[ '2', '入库' ],[ '3', '终止' ] ]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:'处理意见',
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cm.push({
		dataIndex:'endDate',
		hidden : true,
		header:'截止日期',
		width:15*10
	});
	cm.push({
		dataIndex:'taskId',
		hidden : false,
		header:'科技服务任务单',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="MassarrayExtension任务结果1";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state=$("#massarrayExtensionTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/delMassarrayExtensionTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};    		
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	massarrayExtensionTaskResultGrid1=gridEditTable("massarrayExtensionTaskResultdiv1",cols,loadParam,opts);
	$("#massarrayExtensionTaskResultdiv1").data("massarrayExtensionTaskResultGrid1", massarrayExtensionTaskResultGrid1);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});