﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#massarrayExtensionTaskReceive_id").val();
	var state = $("#massarrayExtensionTaskReceive_state").val();
	if(id=="" || state == "3"){
		var type="2";
		load("/experiment/massarrayextension/massarrayExtensionTaskReceive/showMassarrayExtensionTaskReceiveItemListTo.action", {type:type}, "#MassarrayExtensionTaskReceviceLeftPage");
		$("#markup").css("width","75%");
	}
});

function add() {
	window.location = window.ctx + "/experiment/massarrayextension/massarrayExtensionTaskReceive/editMassarrayExtensionTaskReceive.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/massarrayextension/massarrayExtensionTaskReceive/showMassarrayExtensionTaskReceiveList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#massarrayExtensionTaskReceive", {
					userId : userId,
					userName : userName,
					formId : $("#massarrayExtensionTaskReceive_id").val(),
					title : $("#massarrayExtensionTaskReceive_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {
		completeTask($("#massarrayExtensionTaskReceive_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var massarrayExtensionTaskReceiveItemDivData = $("#massarrayExtensionTaskReceiveItemdiv").data("massarrayExtensionTaskReceiveItemGrid");
		document.getElementById('massarrayExtensionTaskReceiveItemJson').value = commonGetModifyRecords(massarrayExtensionTaskReceiveItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/massarrayextension/massarrayExtensionTaskReceive/save.action";
		form1.submit();	
		}
}	

function editCopy() {
	window.location = window.ctx + '/experiment/massarrayextension/massarrayExtensionTaskReceive/copyMassarrayExtensionTaskReceive.action?id=' + $("#massarrayExtensionTaskReceive_id").val();
}

$("#toolbarbutton_status").click(function(){
	var selRecord = massarrayExtensionTaskReceiveItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var result = selRecord.getAt(j).get("method");
		if(result==""){
			message("处理结果不能为空！");
			return;
		}
	}
	if ($("#massarrayExtensionTaskReceive_id").val()){
		commonChangeState("formId=" + $("#massarrayextensionTaskReceive_id").val() + "&tableId=MassarrayExtensionTaskReceive");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#massarrayExtensionTaskReceive_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#massarrayExtensionTaskReceive_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}

$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'样本接收',
	    	   contentEl:'markup'
	       } ]
	   });
});

load("/experiment/massarrayextension/massarrayExtensionTaskReceive/showMassarrayExtensionTaskReceiveItemList.action", {
				id : $("#massarrayExtensionTaskReceive_id").val()
			}, "#massarrayExtensionTaskReceiveItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);