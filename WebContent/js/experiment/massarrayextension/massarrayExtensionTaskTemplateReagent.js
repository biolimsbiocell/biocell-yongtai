﻿var massarrayExtensionTaskTemplateReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'tReagent',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'itemId',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'batch',
		type:"string"
	});
	fields.push({
		name:'isGood',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'massarrayExtensionTask-id',
		type:"string"
	});
	fields.push({
		name:'massarrayExtensionTask-name',
		type:"string"
	});
    fields.push({
		name:'oneNum',
		type:"string"
	});
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'num',
		type:"string"
	});
    fields.push({
		name:'sn',
		type:"string"
	});
    fields.push({
		name:'expireDate',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.reagentId,
		width:20*6
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:biolims.common.templateReagentId,
		width:20*6
	});	
	var codes =new Ext.form.TextField({
        allowBlank: false
	});
	codes.on('focus', function() {
		var selectRecord = dnaTemplateReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var tid = $("#experimentDnaGet_template").val();
				loadReagentItemByCode(tid);
			});
		}
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.common.reagentNo,
		width:20*6,
		editor : codes
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.templateStepNo,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.reagentName,
		width:50*6
	});
	cm.push({
		dataIndex:'sn',editor : new Ext.form.TextField({ allowBlank : true }),
		hidden : false,
		header:'sn',
		width:20*6
	});
	//鼠标单击触发事件 
	var batchs =new Ext.form.TextField({
            allowBlank: false
    });
	batchs.on('focus', function() {
		var selectRecord = dnaTemplateReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("code");
				loadStorageReagentBuy(code);
			});
		}
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6,
		editor:batchs
	});
	cm.push({
		dataIndex:'expireDate',
		hidden : false,
		header:'过期日期',
		width:20*6
	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no], [ '1', biolims.common.yes ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isGood,
		width:20*6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'factNum',
		hidden : false,
		header:"实际反应数",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : false,
		header:'单个用量',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.dose,
		width:20*6
	});
	cm.push({
		dataIndex:'reagentCode',
		hidden : true,
		header:'原辅料编码',
		width:20*6
	});
	var isRunout = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name :biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isRunout',
		hidden : false,
		header:'是否用完',
		width:20*5,
		renderer: Ext.util.Format.comboRenderer(isRunout),
		editor: isRunout
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'massarrayExtensionTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskTemplateReagentListJson.action?id="+$("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();	
	var opts={};
	opts.title="原辅料明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state=$("#massarrayExtensionTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/delmassarrayExtensionTaskTemplateReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
//	opts.tbar.push({
//			text : '填加明细',
//			handler : function (){
//				//获取选择的数据
//				var selectRcords=massarrayExtensionTaskTemplateItemGrid.getSelectionModel().getSelections();
//				//获取全部数据
//				var allRcords=massarrayExtensionTaskTemplateItemGrid.store;
//				//选中的数量
//				var length1=selectRcords.length;
//				//全部数据量
//				var length2=allRcords.getCount();
//				if(length2>0){
//					if(length1==1){
//						var code="";
//						$.each(selectRcords, function(i, obj) {
//							code=obj.get("code");
//						});
//						if(code!=""){
//							showStorageList(code);
//						}else{
//							message("请先添加模板明细数据！");
//							return;
//						}				
//					}else if(length1>1){
//						message("模板明细中只能选择一条数据！");
//						return;
//					}else{
//						message("请先选择模板明细中数据！");
//						return;
//					}
//				}else{
//					message("模板明细中数据为空！");
//					return;
//				}
//			}
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	})
	opts.tbar.push({
		text : biolims.common.copy,
		handler : function() {
			var ob = massarrayExtensionTaskTemplateReagentGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			var records = massarrayExtensionTaskTemplateReagentGrid.getSelectRecord();
			if (records.length==1) {
				$.each(records, function(i, obj) {
					p.set("code",obj.get("code"));
					p.set("name",obj.get("name"));
					p.set("batch",obj.get("batch"));
					p.set("oneNum",obj.get("oneNum"));
					p.set("expireDate",obj.get("expireDate"));
					p.set("sampleNum",obj.get("sampleNum"));
					p.set("isGood",obj.get("isGood"));
					p.set("itemId",obj.get("itemId"));
					p.set("tReagent",obj.get("tReagent"));
					p.set("massarrayExtensionTask-id",obj.get("massarrayExtensionTask-id"));
					p.set("massarrayExtensionTask-name",obj.get("massarrayExtensionTask-name"));
					p.set("note",obj.get("note"));
					massarrayExtensionTaskTemplateReagentGrid.stopEditing();
					massarrayExtensionTaskTemplateReagentGrid.getStore().add(p);
					massarrayExtensionTaskTemplateReagentGrid.startEditing(0, 0);
				});
			}else{
				message("请选择一条您要复制的数据！");
			}
		}
	});
	if($("#massarrayExtensionTask_id").val()&&$("#massarrayExtensionTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
	}
	}
	massarrayExtensionTaskTemplateReagentGrid=gridEditTable("massarrayExtensionTaskTemplateReagentdiv",cols,loadParam,opts);
	$("#massarrayExtensionTaskTemplateReagentdiv").data("massarrayExtensionTaskTemplateReagentGrid", massarrayExtensionTaskTemplateReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:'选择采购原辅料',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}

function setStorageReagentBuy(rec){	
	var gridGrid = $("#massarrayExtensionTaskTemplateReagentdiv").data("massarrayExtensionTaskTemplateReagentGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}

//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, "库存主数据", url, {
		"确定" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					massarrayExtensionTaskTemplateReagentGrid.stopEditing();
					var ob = massarrayExtensionTaskTemplateReagentGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;
					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("itemId",code);				
					massarrayExtensionTaskTemplateReagentGrid.getStore().add(p);	
				});
				massarrayExtensionTaskTemplateReagentGrid.startEditing(0, 0);
				options.close();
			}else{
				message("请选择数据！");
			}
		}
	}, true, options);
}

//根据原辅料编号查询原辅料明细
function loadReagentItemByCode(tid){
		var options = {};
		options.width = 900;
		options.height = 460;
		var url="/system/template/template/showReagentItemByCodeList.action?tid="+tid;
		loadDialogPage(null, "选择明细", url, {
			 "确定": function() {
				 selRecord = reagentItem1Grid.getSelectionModel();
					if (selRecord.getSelections().length > 0) {
						$.each(selRecord.getSelections(), function(i, obj) {
							massarrayExtensionTaskTemplateReagentGrid.stopEditing();
							var ob = massarrayExtensionTaskTemplateReagentGrid.getStore().recordType;
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.get("code"));
							p.set("name",obj.get("name"));
							p.set("batch",obj.get("batch"));
							p.set("isGood",obj.get("isGood"));
							p.set("itemId",obj.get("itemId"));							
							p.set("oneNum",obj.get("num"));
							p.set("note",obj.get("note"));
							massarrayExtensionTaskTemplateReagentGrid.getStore().add(p);	
						});
						massarrayExtensionTaskTemplateReagentGrid.startEditing(0, 0);
						options.close();
					}else{
						message("请选择数据！");
					}
				 options.close();
			}
		}, true, options);
}