﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state = $("#massarrayExtensionTask_state").val();
	var stateName = $("#massarrayExtensionTask_stateName").val();
//	if(state =="3"||stateName=="待修改"){
//		load("/experiment/massarrayExtension/massarrayExtensionTask/showMassarrayExtensionTaskTempList.action", null, "#massarrayExtensionTaskTempPage");
//		$("#markup").css("width","75%");
//	}else{
//		$("#massarrayExtensionTaskTempPage").remove();
//	}
	
	var mttf = $("#massarrayExtensionTask_template_templateFieldsCode").val();
	var mttfItem = $("#massarrayExtensionTask_template_templateFieldsItemCode").val();
	reloadMassarrayExtensionTaskItem(mttf, mttfItem);
});	

function reloadMassarrayExtensionTaskItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = massarrayExtensionTaskItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							massarrayExtensionTaskItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							massarrayExtensionTaskItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = massarrayExtensionTaskResultGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							massarrayExtensionTaskResultGrid.getColumnModel().setHidden(
									i, false);
						} else {
							massarrayExtensionTaskResultGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}

function add() {
	window.location = window.ctx + "/experiment/massarrayextension/massarrayExtensionTask/editMassarrayExtensionTask.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});	

$("#toolbarbutton_print").click(function(){
	var url = '__report=MassarrayExtensionTask.rptdesign&id=' + $("#massarrayExtensionTask_id").val();
	commonPrint(url);
});

setTimeout(function() {
	if($("#massarrayExtensionTask_template").val()){
		var maxNum = $("#massarrayExtensionTask_maxNum").val();
		if(maxNum>0){
			load("/storage/container/sampleContainerTest.action", {
				id : $("#massarrayExtensionTask_template").val(),
				type :$("#type").val(),
				maxNum : 0
			}, "#3d_image0", function(){
				if(maxNum>1){
					load("/storage/container/sampleContainerTest.action", {
						id : $("#massarrayExtensionTask_template").val(),
						type :$("#type").val(),
						maxNum : 1
					}, "#3d_image1", null);
				}
			});
		}
	}
}, 100);

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("MassarrayExtensionTask", {
					userId : userId,
					userName : userName,
					formId : $("#massarrayExtensionTask_id").val(),
					title : $("#massarrayExtensionTask_name").val()
				}, function() {
					window.location.reload();
				});				
});
$("#toolbarbutton_tjsp").click(function() {
	if($("#massarrayExtensionTask_testUserOneName").val()==""){
		message("实验员不能为空！");
		return;
	}
				submitWorkflow("MassarrayExtensionTask", {
					userId : userId,
					userName : userName,
					formId : $("#massarrayExtensionTask_id").val(),
					title : $("#massarrayExtensionTask_name").val()
				}, function() {
					window.location.reload();
				});				
});
$("#toolbarbutton_sp").click(function() {	
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#massarrayExtensionTask_id").val();	
	var options = {};
	options.width = 929;
	options.height = 534;	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}
	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, "审批任务", url, {
		"确定" : function() {			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();
				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};											
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);				
			}else if(operVal=="1"){
				if(taskName=="MassarrayExtension任务"){
				var codeList = new Array();
				var codeList1 = new Array();
				var selRecord1 = massarrayExtensionTaskItemGrid.store;
				var flag=true;
				var flag1=true;
				if (massarrayExtensionTaskResultGrid.getAllRecord().length > 0) {
					var selRecord = massarrayExtensionTaskResultGrid.store;
					for(var j=0;j<selRecord.getCount();j++){
						var oldv = selRecord.getAt(j).get("submit");
						codeList.push(selRecord.getAt(j).get("massarrayExtensionTaskCode"));
						if(oldv==""){
							flag=false;
							message("有样本未提交！");
							return;
						}
						if(selRecord.getAt(j).get("nextFlowId")==""){
							message("有下一步未填写！");
							return;
						}
					}
					for(var j=0;j<selRecord1.getCount();j++){						
						if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
							codeList1.push(selRecord1.getAt(j).get("code"));
							flag1=false;
							message("有样本未完成实验！");
						};
					}
					if(massarrayExtensionTaskResultGrid.getModifyRecord().length > 0){
						message("请先保存记录！");
						return;
					}
					if(flag1){
							var myMask1 = new Ext.LoadMask(Ext.getBody(), {
								msg : '请等待...'
							});
							myMask1.show();
							Ext.MessageBox.confirm("确认", "请确认保存修改项后进行办理!", function(button, text) {
								if (button == "yes") {
									var paramData =  {};
									paramData.oper = $("#oper").val();
									paramData.info = $("#opinion").val();
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									};																		
									_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
										location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
									}, dialogWin);
																										}
							});
							myMask1.hide();
					}else{
						message("有样本未完成实验！样本有："+codeList1);
					}
				}else{
					message("请填加任务明细并保存！");
					return;
				}
				}else{
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();
					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};										
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}
			}				
		},
		"查看流程图" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });

	    var massarrayExtensionTaskItemDivData = $("#massarrayExtensionTaskItemdiv").data("massarrayExtensionTaskItemGrid");
		document.getElementById('massarrayExtensionTaskItemJson').value = commonGetModifyRecords(massarrayExtensionTaskItemDivData);
	    var massarrayExtensionTaskResultDivData = $("#massarrayExtensionTaskResultdiv").data("massarrayExtensionTaskResultGrid");
		document.getElementById('massarrayExtensionTaskResultJson').value = commonGetModifyRecords(massarrayExtensionTaskResultDivData);
		var massarrayExtensionTaskTemplateItemDivData = $("#massarrayExtensionTaskTemplateItemdiv").data("massarrayExtensionTaskTemplateItemGrid");
		document.getElementById('massarrayExtensionTaskTemplateItemJson').value = commonGetModifyRecords(massarrayExtensionTaskTemplateItemDivData);
		var massarrayExtensionTaskTemplateReagentDivData = $("#massarrayExtensionTaskTemplateReagentdiv").data("massarrayExtensionTaskTemplateReagentGrid");
		document.getElementById('massarrayExtensionTaskTemplateReagentJson').value = commonGetModifyRecords(massarrayExtensionTaskTemplateReagentDivData);
		var massarrayExtensionTaskTemplateCosDivData = $("#massarrayExtensionTaskTemplateCosdiv").data("massarrayExtensionTaskTemplateCosGrid");
		document.getElementById('massarrayExtensionTaskTemplateCosJson').value = commonGetModifyRecords(massarrayExtensionTaskTemplateCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/massarrayextension/massarrayExtensionTask/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();	
		}
}	

function saveItem() {
	var id = $("#massarrayExtensionTask_id").val();
	if (!id) {
		return;
	}
	
	// Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	 var massarrayExtensionTaskItemDivData = $("#massarrayExtensionTaskItemdiv").data("massarrayExtensionTaskItemGrid");
		massarrayExtensionTaskItemJson = commonGetModifyRecords(massarrayExtensionTaskItemDivData);
	    var massarrayExtensionTaskResultDivData = $("#massarrayExtensionTaskResultdiv").data("massarrayExtensionTaskResultGrid");
		massarrayExtensionTaskResultJson= commonGetModifyRecords(massarrayExtensionTaskResultDivData);
		var massarrayExtensionTaskTemplateItemDivData = $("#massarrayExtensionTaskTemplateItemdiv").data("massarrayExtensionTaskTemplateItemGrid");
		massarrayExtensionTaskTemplateItemJson= commonGetModifyRecords(massarrayExtensionTaskTemplateItemDivData);
		var massarrayExtensionTaskTemplateReagentDivData = $("#massarrayExtensionTaskTemplateReagentdiv").data("massarrayExtensionTaskTemplateReagentGrid");
		massarrayExtensionTaskTemplateReagentJson= commonGetModifyRecords(massarrayExtensionTaskTemplateReagentDivData);
		var massarrayExtensionTaskTemplateCosDivData = $("#massarrayExtensionTaskTemplateCosdiv").data("massarrayExtensionTaskTemplateCosGrid");
		massarrayExtensionTaskTemplateCosJson = commonGetModifyRecords(massarrayExtensionTaskTemplateCosDivData);
	
	ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/saveAjax.action", {
		massarrayExtensionTaskItemJson:massarrayExtensionTaskItemJson,
		massarrayExtensionTaskResultJson:massarrayExtensionTaskResultJson,
		massarrayExtensionTaskTemplateItemJson:massarrayExtensionTaskTemplateItemJson,
		massarrayExtensionTaskTemplateReagentJson:massarrayExtensionTaskTemplateReagentJson,
		massarrayExtensionTaskTemplateCosJson:massarrayExtensionTaskTemplateCosJson,
		id : id
	}, function(data) {
		if (data.success) {
			//message("保存成功！");
			if(data.equip!=0&&typeof(data.equip)!='undefined'){
				message(data.equip+"条设备数据保存成功！");
			}
			if(data.re!=0&&typeof(data.re)!='undefined'){
				message(data.re+"条原辅料数据保存成功！");
			}
			massarrayExtensionTaskItemDivData.getStore().reload();
			massarrayExtensionTaskResultDivData.getStore().reload();
			massarrayExtensionTaskTemplateItemDivData.getStore().reload();
			massarrayExtensionTaskTemplateReagentDivData.getStore().reload();
			massarrayExtensionTaskTemplateCosDivData.getStore().reload();
		} else {
			message(biolims.common.saveFailed);
		}
	}, null);
}

function editCopy() {
	window.location = window.ctx + '/experiment/massarrayextension/massarrayExtensionTask/copyMassarrayExtensionTask.action?id=' + $("#massarrayExtensionTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	var MassarrayExtensionTaskInfoGrid = massarrayExtensionTaskResultGrid.getStore();
	var MassarrayExtensionTaskItemGrid = massarrayExtensionTaskItemGrid.getStore();
	var num = 0;
	for(var i= 0; i<MassarrayExtensionTaskItemGrid.getCount();i++){		
		if(MassarrayExtensionTaskInfoGrid.getCount()==0){
			message("请完成实验！");
			return;
		}
		for(var j=0;j<MassarrayExtensionTaskInfoGrid.getCount();j++){
			if(MassarrayExtensionTaskItemGrid.getAt(i).get("yCode")==MassarrayExtensionTaskInfoGrid.getAt(j).get("yCode")){
				j=MassarrayExtensionTaskInfoGrid.getCount();
			}else{
				num=num+1;
				if(num==MassarrayExtensionTaskInfoGrid.getCount()){
					num=0;
					message("请完成实验！");
					return;
				}
			}
		}		
	}
	commonChangeState("formId=" + $("#massarrayExtensionTask_id").val() + "&tableId=MassarrayExtensionTask");
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#massarrayExtensionTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#massarrayExtensionTask_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#massarrayExtensionTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'MassarrayExtension任务',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskItemList.action", {
				id : $("#massarrayExtensionTask_id").val()
			}, "#massarrayExtensionTaskItempage");
load("/experiment/massarrayextension/massarrayExtensionTask/showTemplateItemList.action", {
	id:$("#massarrayExtensionTask_id").val()
}, "#massarrayExtensionTaskTemplateItempage");
load("/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskTemplateReagentList.action",{
				id:$("#massarrayExtensionTask_id").val(),
			}, "#massarrayExtensionTaskTemplateReagentpage");
load("/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskTemplateCosList.action", {
	id:$("#massarrayExtensionTask_id").val()
}, "#massarrayExtensionTaskTemplateCospage");
load("/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskInfoList.action", {
				id : $("#massarrayExtensionTask_id").val()
			}, "#massarrayExtensionTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
//调用模板
function TemplateFun(){
			var type="doMassarrayExtensionTask";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}

//双击加载模板数据
function setTemplateFun(rec){	
	if($('#massarrayExtensionTask_acceptUser_name').val()==""){
		document.getElementById('massarrayExtensionTask_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('massarrayExtensionTask_acceptUser_name').value=rec.get('acceptUser-name');
	}		
	var itemGrid=massarrayExtensionTaskItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
			itemGrid.getAt(i).set("sampleConsume",rec.get('sampleNum'));
		}
	}
		var code=$("#massarrayExtensionTask_template").val();
		if(code==""){
			var cid=rec.get('id');
						document.getElementById('massarrayExtensionTask_template').value=rec.get('id');
						document.getElementById('massarrayExtensionTask_template_name').value=rec.get('name');
						var win = Ext.getCmp('TemplateFun');
						if(win){win.close();}
						var id=rec.get('id');
						//加载模板明细
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {
									var ob = massarrayExtensionTaskTemplateItemGrid.getStore().recordType;
									massarrayExtensionTaskTemplateItemGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										massarrayExtensionTaskTemplateItemGrid.getStore().add(p);							
									});									
									massarrayExtensionTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						//加载原辅料明细
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = massarrayExtensionTaskTemplateReagentGrid.getStore().recordType;
								massarrayExtensionTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									massarrayExtensionTaskTemplateReagentGrid.getStore().add(p);							
								});								
								massarrayExtensionTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						//加载实验设备明细
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = massarrayExtensionTaskTemplateCosGrid.getStore().recordType;
								massarrayExtensionTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									massarrayExtensionTaskTemplateCosGrid.getStore().add(p);							
								});			
								massarrayExtensionTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null);
		}else{
			var flag = true;
			if(rec.get('id')==code){
				flag = confirm("是否重新加载实验步骤，应有实验步骤将被删除？");
 			 }
			if(flag==true){
 				//判断设备是否占用
 				var cid=rec.get('id');
						var ob1 = massarrayExtensionTaskTemplateItemGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id");				
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/delTemplateItemOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									massarrayExtensionTaskTemplateItemGrid.store.removeAll();
								}
							}
							massarrayExtensionTaskTemplateItemGrid.store.removeAll();
		 				}		 				
						var ob2 = massarrayExtensionTaskTemplateReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");								
								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/delTemplateReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
								}else{
									massarrayExtensionTaskTemplateReagentGrid.store.removeAll();
								}
							}
							massarrayExtensionTaskTemplateReagentGrid.store.removeAll();
		 				}
						var ob3 = massarrayExtensionTaskTemplateCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");							
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/delTemplateCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
								}else{
									massarrayExtensionTaskTemplateCosGrid.store.removeAll();
								}
							}
							massarrayExtensionTaskTemplateCosGrid.store.removeAll();
		 				}
						document.getElementById('massarrayExtensionTask_template').value=rec.get('id');
		 				document.getElementById('massarrayExtensionTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	
									var ob = massarrayExtensionTaskTemplateItemGrid.getStore().recordType;
									massarrayExtensionTaskTemplateItemGrid.stopEditing();									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										massarrayExtensionTaskTemplateItemGrid.getStore().add(p);							
									});									
									massarrayExtensionTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = massarrayExtensionTaskTemplateReagentGrid.getStore().recordType;
								massarrayExtensionTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									massarrayExtensionTaskTemplateReagentGrid.getStore().add(p);							
								});								
								massarrayExtensionTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = massarrayExtensionTaskTemplateCosGrid.getStore().recordType;
								massarrayExtensionTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									massarrayExtensionTaskTemplateCosGrid.getStore().add(p);							
								});			
								massarrayExtensionTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
					}			
	}	
		reloadMassarrayExtensionTaskItem(rec.get("templateFieldsCode"), rec
				.get("templateFieldsItemCode"));
}
	
//按条件加载原辅料
function showReagent(){
	//获取全部数据
	var allRcords=massarrayExtensionTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=massarrayExtensionTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=massarrayExtensionTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#massarrayExtensionTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskTemplateReagentList.action", {
			id : $("#massarrayExtensionTask_id").val()
		}, "#massarrayExtensionTaskTemplateReagentpage");
	}else if(length1==1){
		massarrayExtensionTaskTemplateReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = massarrayExtensionTaskTemplateReagentGrid.getStore().recordType;
				massarrayExtensionTaskTemplateReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("expireDate",obj.expireDate);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("sn",obj.sn);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("massarrayExtensionTask-id",obj.tId);
					p.set("massarrayExtensionTask-name",obj.tName);					
					massarrayExtensionTaskTemplateReagentGrid.getStore().add(p);							
				});
				massarrayExtensionTaskTemplateReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}	
}

//按条件加载设备
function showCos(){
	var allRcords=massarrayExtensionTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=massarrayExtensionTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=massarrayExtensionTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#massarrayExtensionTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/massarrayextension/massarrayExtensionTask/showMassarrayExtensionTaskTemplateCosList.action", {
			id : $("#massarrayExtensionTask_id").val()
		}, "#massarrayExtensionTaskTemplateCospage");
	}else if(length1==1){
		massarrayExtensionTaskTemplateCosGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/massarrayextension/massarrayExtensionTask/setCos.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = massarrayExtensionTaskTemplateCosGrid.getStore().recordType;
				massarrayExtensionTaskTemplateCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("note",obj.note);
					p.set("time",obj.time);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("type-id",obj.typeId);
					p.set("type-name",obj.typeName);
					p.set("state",obj.state);
					p.set("tCos",obj.tCos);
					p.set("massarrayExtensionTask-id",obj.tId);
					p.set("massarrayExtensionTask-name",obj.tName);					
					massarrayExtensionTaskTemplateCosGrid.getStore().add(p);							
				});
				massarrayExtensionTaskTemplateCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}
}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	
	function ckcrk(){		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = massarrayExtensionTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					}
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "MassarrayExtensionTask",id : $("#massarrayExtensionTask_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	
}
//	var loadtestUser;
	//选择实验组用户
	function testUser(type){
		var gid=$("#massarrayExtensionTask_acceptUser").val();
		if(gid!=""){
			var options = {};
			options.width = 500;
			options.height = 500;
			loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				"Confirm" : function() {
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						var id=[];
						var name=[];
						for(var i=0;i<selectRecord.length;i++){
							id.push(selectRecord[i].get("user-id"));
							name.push(selectRecord[i].get("user-name"));
						}
						if(type==1){//第一批实验员
							$("#massarrayExtensionTask_testUserOneId").val(id);
							$("#massarrayExtensionTask_testUserOneName").val(name);
						}else if(type==2){//第二批实验员
							$("#massarrayExtensionTask_testUserTwoId").val(id);
							$("#massarrayExtensionTask_testUserTwoName").val(name);
						}else if(type==3){//审核人
							$("#massarrayExtensionTask_confirmUser").val(id);
							$("#massarrayExtensionTask_confirmUser_name").val(name);
						}
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}else{
			message(biolims.common.pleaseSelectGroup);
		}
		
	}



