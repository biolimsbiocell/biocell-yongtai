//QPCR实验明细
var qtTaskItemManagerGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
    	
        name:'id',
        type:"string"
    });
    fields.push({
	    name:'expCode',
        type:"string"
    });
    fields.push({
	    name:'code',
        type:"string"
    });
    fields.push({
	    name:'sampleCode',
        type:"string"
    });
    fields.push({
	    name:'sampleName',
        type:"string"
    });
    fields.push({
	    name:'sampleNum',
        type:"string"
    });
    fields.push({
	    name:'sampleVolume',
        type:"string"
    });
    fields.push({
	    name:'addVolume',
        type:"string"
    });
    fields.push({
	    name:'sumVolume',
        type:"string"
    });
    fields.push({
	    name:'indexs',
        type:"string"
    });
    fields.push({
	    name:'note',
        type:"string"
    });
    fields.push({
	    name:'nextFlow',
        type:"string"
    });
    fields.push({
	    name:'sampleId',
        type:"string"
    });
    fields.push({
        name:'state',
        type:"string"
    });
    fields.push({
	    name:'stateName',
        type:"string"
    });
    fields.push({
        name:'concentration',
        type:"string"
    });
    fields.push({
        name:'result',
        type:"string"
    });
    fields.push({
        name:'reason',
        type:"string"
    });
    fields.push({
        name:'stepNum',
        type:"string"
    });
    fields.push({
	    name:'patientName',
        type:"string"
    });
    fields.push({
	    name:'sequenceFun',
        type:"string"
    });   
    fields.push({
	    name:'productName',
        type:"string"
    });
    fields.push({
	    name:'productId',
        type:"string"
    });
    fields.push({
	    name:'inspectDate',
        type:"string"
    });
    fields.push({
	    name : 'orderId',
        type : "string"
    });
//    fields.push({
//	    name : 'idCard',
//        type : "string"
//    });
//    fields.push({
//	    name : 'phone',
//        type : "string"
//    });
    fields.push({
	    name : 'reportDate',
        type : "string"
    });
    fields.push({
	    name:'classify',
        type:"string"
    });
    fields.push({
	    name:'dicSampleType-id',
        type:"string"
    });
    fields.push({
	    name:'dicSampleType-name',
        type:"string"
    });
    fields.push({
	    name:'sampleCode',
        type:"string"
    });
    fields.push({
	    name:'sampleType',
        type:"string"
    });
    fields.push({
	    name:'sampleConsume',
         type:"string"
    });
    fields.push({
	    name:'labCode',
        type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6
	});
	cm.push({
		dataIndex:'expCode',
		hidden : true,
		header:biolims.common.expCode,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		header:biolims.common.phone,
//		hidden:true,
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.dicSampleTypeId,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleId',
		hidden : true,
		header:biolims.common.sampleId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleName',
		hidden : true,
		header:biolims.common.sampleName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : false,
		header:biolims.common.sampleConsume,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:biolims.common.sampleVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : true,
		header:biolims.common.addVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : true,
		header:biolims.common.totalVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexs',
		hidden : true,
		header:'Index',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:biolims.common.concentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			},{
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:biolims.common.result,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTaskManage/showQtTaskItemManagerListJson.action";
	var opts={};
	opts.title=biolims.QPCR.testDetail;
	opts.height =  document.body.clientHeight-240;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.batchInLib,
		handler : ruku
	});
	opts.tbar.push({
		text : biolims.QPCR.test2Do,
		handler : tiqu
	});
	qtTaskItemManagerGrid=gridEditTable("qtTaskItemManagerdiv",cols,loadParam,opts);
	$("#qtTaskItemManagerdiv").data("qtTaskItemManagerGrid", qtTaskItemManagerGrid);
});

//保存Grid
function save1(){	
	var itemJson = commonGetModifyRecords(qtTaskItemManagerGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/qt/qtTaskManage/saveQtTaskItemManager.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					qtTaskItemManagerGrid.getStore().commitChanges();
					qtTaskItemManagerGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
}

function selectInfo(){
	commonSearchAction(qtTaskItemManagerGrid);
	$("#qtTaskItemManagerGrid_code").val("");
	$("#qtTaskItemManagerGrid_Code").val("");
}

function ruku(){
	var selectRcords=qtTaskItemManagerGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		var ids="";
		$.each(selectRcords,function(i,obj){
			ids+=obj.get("id")+",";
		});
		ajax("post", "/experiment/qt/qtTaskManage/qtTaskManageItemRuku.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qtTaskItemManagerGrid.getStore().reload();
				message(biolims.plasma.warehousingSuccess);
			} else{
				message(biolims.plasma.warehousingFailed);
			}
		}, null);
	}else{
		message(biolims.plasma.pleaseSelectWarehousing);
	}
}

function tiqu(){
	var selectRcords=qtTaskItemManagerGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		for(var i=0;i<selectRcords.length;i++){
			ajax("post", "/experiment/qt/qtTaskManage/qtTaskManageItemTiqu.action", {
				id : selectRcords[i].get("id")
			}, function(data) {
				if (data.success) {
					qtTaskItemManagerGrid.getStore().reload();
					message(biolims.common.backlogSuccess);
				} else{
					message(biolims.common.backlogFailed);
				}
			}, null);
		}
	}else{
		message(biolims.common.pleaseSelectBacklog);
	}
}
