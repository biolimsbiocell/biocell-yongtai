var qtTaskTemplateItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({    	  
	    name:'id',
	    type:"string"
    });
    fields.push({
	    name:'code',
	    type:"string"
    });
    fields.push({
		name:'testUserId',
		type:"string"
	});
    fields.push({
		name:'testUserName',
		type:"string"
	});
    fields.push({
		name:'tItem',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
	    name:'startTime',
		type:"string"
	});
	fields.push({
		name:'endTime',
		type:"string"
	});
    fields.push({
		name:'sampleCodes',
		type:"string"
	});
    fields.push({
	    name:'state',
	    type:"string"
	});
    fields.push({
	    name:'qtTask-id',
	    type:"string"
	});
	fields.push({
		name:'qtTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.processId,
		width:20*6
	});	
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:biolims.common.templateProcessId,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.stepDetail,
		width:100*6,		
		editor : new Ext.form.HtmlEditor({
			readOnly : true
		})
	});
	cm.push({
		dataIndex:'testUserId',
		hidden : true,
		header:biolims.common.testUserId,
		width:20*6		
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUserName',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*6,		
		editor : testUser
	});	
	
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:biolims.common.startTime,
		width:30*6
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:biolims.common.endTime,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:biolims.common.relateSample,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.relateSample,
		width:20*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTask/showTemplateItemListJson.action?id="+$("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.commonTemplate;
	opts.height =  document.body.clientHeight*0.8;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qt/qtTask/delTemplateItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qtTaskTemplateItemGrid.getStore().commitChanges();
				qtTaskTemplateItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : biolims.common.fillDetail,
		handler : templateSelect
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		text : biolims.common.testUserName,
		handler : loadTestUser
	});
	if($("#qtTask_id").val()&&$("#qtTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	}
	qtTaskTemplateItemGrid=gridEditTable("qtTaskTemplateItemdiv",cols,loadParam,opts);
	$("#qtTaskTemplateItemdiv").data("qtTaskTemplateItemGrid",qtTaskTemplateItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");	
});

//打印执行单
function stampOrder(){
}

function addSuccess(){
	var num =$("#qtTask_template_name").val();
	if(num!=""){
		var setNum = qtTaskTemplateReagentGrid.store;
		var selectRecords = qtTaskTemplateReagentGrid.store;
			for(var i=0;i<setNum.getCount();i++){
				setNum.getAt(i).set("sampleNum",selectRecords.getCount());
		}


	
	var getRecord = qtTaskItemGrid.getAllRecord();
	var selRecord = qtTaskResultGrid.store;
	if(selRecord.getCount()>0){
		var flag=true;
		$.each(getRecord,function(a,b){
			for(var i=0;i<selRecord.getCount();i++){
				if(b.get("sampleCode")==selRecord.getAt(i).get("sampleCode")){
					message(biolims.common.dataRepeat);
					flag=false;
				}
			}
			if(flag){
				for(var i=0;i<selRecord.getCount();i++){
					if(b.get("sampleCode")!=selRecord.getAt(i).get("sampleCode")){
						var ob = qtTaskResultGrid.getStore().recordType;
						qtTaskResultGrid.stopEditing();
						var p = new ob({});
						p.isNew = true;
						p.set("code",b.get("code"));
						p.set("sampleCode",b.get("sampleCode"));
						p.set("chromosomalLocation",b.get("chromosomalLocation"));
						p.set("fragmentLength",b.get("fragmentLength"));
						p.set("result",b.get("result"));
						
						p.set("tempId",b.get("tempId"));
						p.set("rowCode",b.get("rowCode"));
						p.set("colCode",b.get("colCode"));
						p.set("counts",b.get("counts"));
						p.set("barCode",b.get("barCode"));
						p.set("dataTraffic",b.get("dataTraffic"));
					
						p.set("dicSampleType-id",b.get("dicSampleType-id"));
						p.set("dicSampleType-name",b.get("dicSampleType-name"));
					
						p.set("concentration",b.get("concentration"));
						p.set("patientName",b.get("patientName"));
						p.set("idCard",b.get("idCard"));
						p.set("phone",b.get("phone"));
						p.set("sequenceFun",b.get("sequenceFun"));
						p.set("productName",b.get("productName"));
						p.set("productId",b.get("productId"));
						p.set("inspectDate",b.get("inspectDate"));
						p.set("reportDate",b.get("reportDate"));
						p.set("orderId",b.get("orderId"));
						p.set("result","1");												
						p.set("projectId",b.get("projectId"));//项目编号
						p.set("contractId",b.get("contractId"));//合同编号
						p.set("orderType",b.get("orderType"));//任务单类型
						p.set("taskId",b.get("taskId"));//科技服务任务单
						p.set("classify",b.get("classify"));//科技服务任务单
						p.set("sampleType",b.get("sampleType"));
						p.set("labCode",b.get("labCode"));
						
						if(b.get("productId")==""||b.get("productId")==null){
							
						}else{
							ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
								model : "QtTask",productId:b.get("productId")
							}, function(data) {
								p.set("nextFlowId",data.dnextId);
								p.set("nextFlow",data.dnextName);
							}, null);
						}
						p.set("submit",b.get("submit"));
						p.set("qtTask-id",b.get("qtTask-id"));
						p.set("qtTask-name",b.get("qtTask-name"));
						qtTaskResultGrid.getStore().add(p);
						qtTaskResultGrid.startEditing(0,0);
					}
				}
			}else{
				message(biolims.common.dataRepeat);
			}
		});
	}else{
		$.each(getRecord,function(a,b){
			var ob = qtTaskResultGrid.getStore().recordType;
			qtTaskResultGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("code",b.get("code"));
			p.set("sampleCode",b.get("sampleCode"));
			p.set("chromosomalLocation",b.get("chromosomalLocation"));
			p.set("fragmentLength",b.get("fragmentLength"));
			p.set("result",b.get("result"));
			p.set("rowCode",b.get("rowCode"));
			p.set("colCode",b.get("colCode"));
			p.set("counts",b.get("counts"));
			p.set("barCode",b.get("barCode"));
			p.set("dataTraffic",b.get("dataTraffic"));
			
			p.set("dicSampleType-id",b.get("dicSampleType-id"));
			p.set("dicSampleType-name",b.get("dicSampleType-name"));
			p.set("tempId",b.get("tempId"));
			p.set("concentration",b.get("concentration"));
			p.set("patientName",b.get("patientName"));
			p.set("idCard",b.get("idCard"));
			p.set("phone",b.get("phone"));
			p.set("sequenceFun",b.get("sequenceFun"));
			p.set("productName",b.get("productName"));
			p.set("productId",b.get("productId"));
			p.set("inspectDate",b.get("inspectDate"));
			p.set("reportDate",b.get("reportDate"));
			p.set("orderId",b.get("orderId"));
			p.set("result","1");												
			p.set("projectId",b.get("projectId"));//项目编号
			p.set("contractId",b.get("contractId"));//合同编号
			p.set("orderType",b.get("orderType"));//任务单类型
			p.set("taskId",b.get("taskId"));//科技服务任务单
			p.set("classify",b.get("classify"));//科技服务任务单
			p.set("sampleType",b.get("sampleType"));
			p.set("labCode",b.get("labCode"));
			if(b.get("productId")==""||b.get("productId")==null){
				
			}else{
				ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
					model : "QtTask",productId:b.get("productId")
				}, function(data) {
					p.set("nextFlowId",data.dnextId);
					p.set("nextFlow",data.dnextName);
				}, null);
			}
			
			p.set("nextFlow",b.get("nextFlow"));
			p.set("submit",b.get("submit"));
			p.set("qtTask-id",b.get("qtTask-id"));
			p.set("qtTask-name",b.get("qtTask-name"));
			qtTaskResultGrid.getStore().add(p);
			qtTaskResultGrid.startEditing(0,0);
		});
	}
	}else{
		message(biolims.common.pleaseSelectTemplate);
	}
}

function addSuccess1(){
	var getRecord = qtTaskItemGrid.getAllRecord();
	var selectRecord = qtTaskTemplateItemGrid.getSelectionModel().getSelections();
	var selRecord = qtTaskResultGrid.store;
	if(selectRecord.length>0){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("Codes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<selRecord.getCount();j1++){
							var getv = scode[i1];
							var setv = selRecord.getAt(j1).get("qtTaskCode");
							if(getv == setv){
								isRepeat = false;
								message(biolims.common.haveDuplicate);
								break;					
							}
						}
							if(isRepeat){
								$.each(getRecord,function(a,b){
									if(b.get("code")==scode[i1]){
									var productNum=b.get("productNum");
									if(b.get("dicType-name")==null||b.get("dicType-name")==""){
										message(biolims.common.pleaseFillDic);
										return;
									}
										if(productNum==null||productNum==""||productNum==0){
											message(biolims.common.pleaseFillDicNum);
										}else{
											for(var k=1;k<=productNum;k++){
												var ob = qtTaskResultGrid.getStore().recordType;
												qtTaskResultGrid.stopEditing();
												var p = new ob({});
												p.isNew = true;
												p.set("tempId",b.get("tempId"));
												p.set("sampleCode",b.get("sampleCode"));
												p.set("code",b.get("code"));
												p.set("dicSampleType-id",b.get("dicSampleType-id"));
												p.set("dicSampleType-name",b.get("dicSampleType-name"));
												p.set("tempId",b.get("tempId"));
												p.set("concentration",b.get("concentration"));
												p.set("patientName",b.get("patientName"));
												p.set("idCard",b.get("idCard"));
												p.set("phone",b.get("phone"));
												p.set("sequenceFun",b.get("sequenceFun"));
												p.set("productName",b.get("productName"));
												p.set("productId",b.get("productId"));
												p.set("inspectDate",b.get("inspectDate"));
												p.set("reportDate",b.get("reportDate"));
												p.set("orderId",b.get("orderId"));
												p.set("result","1");												
												p.set("projectId",b.get("projectId"));//项目编号
												p.set("contractId",b.get("contractId"));//合同编号
												p.set("orderType",b.get("orderType"));//任务单类型
												p.set("taskId",b.get("taskId"));//科技服务任务单
												p.set("classify",b.get("classify"));//科技服务任务单
												p.set("sampleType",b.get("sampleType"));
												p.set("labCode",b.get("labCode"));
												ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
													model : "QtTask",productId:b.get("productId")
												}, function(data) {
													p.set("nextFlowId",data.dnextId);
													p.set("nextFlow",data.dnextName);
												}, null);
												message(biolims.common.generateResultsSuccess);
												qtTaskResultGrid.getStore().add(p);
												qtTaskResultGrid.startEditing(0,0);
											}
										}
									}
								});								
							}													
					}										
				});			
	}else{
		var selRecord = qtTaskResultGrid.store;
		var flag;		
		var getRecord = qtTaskItemGrid.getAllRecord();
		$.each(getRecord,function(a,b){
			flag = true;
			for(var j1=0;j1<selRecord.getCount();j1++){
				var getv = b.get("code");
				var setv = selRecord.getAt(j1).get("qtTaskCode");
				if(getv == setv){
					flag = false;
					message(biolims.common.generateResultsSuccess);
					break;					
				}
			}
			if(flag==true){
			var productNum=b.get("productNum");
			if(b.get("dicSampleType-name")==null||b.get("dicSampleType-name")==""){
				message(biolims.common.pleaseFillDic);
				return;
			}
				if(productNum==null||productNum==""||productNum==0){
					message(biolims.common.pleaseFillDicNum);
				}else{
					for(var k=1;k<=productNum;k++){
						var ob = qtTaskResultGrid.getStore().recordType;
						qtTaskResultGrid.stopEditing();
						var p = new ob({});
						p.isNew = true;
						p.set("tempId",b.get("tempId"));
						p.set("sampleCode",b.get("sampleCode"));
						p.set("code",b.get("code"));
						p.set("dicSampleType-id",b.get("dicSampleType-id"));
						p.set("dicSampleType-name",b.get("dicSampleType-name"));
						p.set("tempId",b.get("tempId"));
						p.set("concentration",b.get("concentration"));
						p.set("patientName",b.get("patientName"));
						p.set("idCard",b.get("idCard"));
						p.set("phone",b.get("phone"));
						p.set("sequenceFun",b.get("sequenceFun"));
						p.set("productName",b.get("productName"));
						p.set("productId",b.get("productId"));
						p.set("inspectDate",b.get("inspectDate"));
						p.set("reportDate",b.get("reportDate"));
						p.set("orderId",b.get("orderId"));
						p.set("result","1");						
						p.set("projectId",b.get("projectId"));//项目编号
						p.set("contractId",b.get("contractId"));//合同编号
						p.set("orderType",b.get("orderType"));//任务单类型
						p.set("taskId",b.get("taskId"));//科技服务任务单
						p.set("classify",b.get("classify"));//科技服务任务单
						p.set("sampleType",b.get("sampleType"));
						p.set("labCode",b.get("labCode"));
						ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
							model : "QtTask",productId:b.get("productId")
						}, function(data) {
							p.set("nextFlowId",data.dnextId);
							p.set("nextFlow",data.dnextName);
						}, null);
						message(biolims.common.generateResultsSuccess);
						qtTaskResultGrid.getStore().add(p);
						qtTaskResultGrid.startEditing(0,0);
					}
				}
			}
		});		
	}
}

//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=qtTaskTemplateItemGrid.getSelectionModel();
	var setNum = qtTaskTemplateReagentGrid.store;
	var selectRecords = qtTaskItemGrid.getSelectionModel();
	if(selectRecords.getSelections().length>0){
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			obj.set("startTime",str);
			//将所选样本的数量，放到原辅料样本数量处
			for(var i=0; i<setNum.getCount();i++){
				var num = setNum.getAt(i).get("itemId");
				if(num==obj.get("code")){
					setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
				}
			}
		});		
	}else{
		message(biolims.common.selectStepNum);
	}
	var selectRecord=qtTaskItemGrid.getSelectionModel();
	var selRecord=qtTaskTemplateItemGrid.getSelectRecord();
	var sampleCodes = "";
	$.each(selectRecord.getSelections(), function(i, obj) {
		sampleCodes += obj.get("code")+",";
	});
	$.each(selRecord, function(i, obj) {
		obj.set("Codes", Codes);
	});
	}else{
	    message(biolims.common.pleaseSelectSamples);
    }
}

//获取停止时的时间
function getEndTime(){
		var setRecord=qtTaskItemGrid.store;
		var d = new Date();
		var getIndex= qtTaskTemplateItemGrid.store;
		var getIndexs = qtTaskTemplateItemGrid.getSelectionModel().getSelections();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=qtTaskTemplateItemGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("endTime",str);
				var codes = obj.get("Codes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("Code")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("Codes",codes);
			});
		}
		else{
			message(biolims.common.selectStepNum);
		}
}

//选择实验步骤
function templateSelect(){
	var option = {};
	option.width = 605;
	option.height = 558;
	loadDialogPage(null, biolims.common.chooseExperimentalSteps, "/experiment/qt/qtTask/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
		"Confirm" : function() {
			var operGrid = $("#template_wait_grid_div").data("grid");
			var ob = qtTaskTemplateItemGrid.getStore().recordType;
			qtTaskTemplateItemGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code", obj.get("code"));
						p.set("name", obj.get("name"));	
						qtTaskTemplateItemGrid.getStore().add(p);
				});
				qtTaskTemplateItemGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);	
}

//查询实验员
function loadTestUser(){
		var gid=$("#qtTask_acceptUser").val();
		var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			"Confirm" : function() {
				var gridGrid = $("#qtTaskTemplateItemdiv").data("qtTaskTemplateItemGrid");
				var selRecords = gridGrid.getSelectionModel().getSelections(); 
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					var id=[];
					var name=[];
					for(var i=0;i<selectRecord.length;i++){
						id.push(selectRecord[i].get("user-id"));
						name.push(selectRecord[i].get("user-name"));
					}
					for(var j=0;j<selRecords.length;j++){
						selRecords[j].set("testUserId",id.toString());
						selRecords[j].set("testUserName",name.toString());}
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
