﻿var qtflag = 0;
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state = $("#qtTask_state").val();
	var stateName = $("#qtTask_stateName").val();
	var method = $("#qtTask_method").val();
	if(state =="3"||stateName==biolims.common.toModify){
//		load("/experiment/qt/qtTask/showQtTaskTempList.action", null, "#qtTaskTempPage");
//		$("#markup").css("width","75%");
		
		if(method=='window'){
			$("#qtTaskTempPage").remove();
			$("#markup").css("width","100%");
			selectTemp();
		}else{
			load("/experiment/qt/qtTask/showQtTaskTempList.action", null, "#qtTaskTempPage");
			$("#markup").css("width","75%");
		}
	}else{
		$("#qtTaskTempPage").remove();
	}
	
	var mttf = $("#qtTask_template_templateFieldsCode").val();
	var mttfItem = $("#qtTask_template_templateFieldsItemCode").val();
	reloadQtTaskItem(mttf, mttfItem);

});	





//清空检测项目
function qkxm(){
	var stateName = $("#sampleReceive_stateName").val();
	  if(stateName != "完成"){
		  $("#qtTask_productId").val("");
			$("#qtTask_productName").val("");
			message("检测项目已清空，请保存！");
	  }else{
		  message("任务单已完成，无法清空科研项目！");
		  return;
	  }
}

function reloadQtTaskItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = qtTaskItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							qtTaskItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							qtTaskItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = qtTaskResultGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							qtTaskResultGrid.getColumnModel().setHidden(
									i, false);
						} else {
							qtTaskResultGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}


function changeWidth(){
	if(qtflag == 0){
		$("#qtTaskTempPage").css("width","50%");
		$("#markup").css("width","50%");
		qtflag = 1;
	}else{
		$("#qtTaskTempPage").css("width","25%");
		$("#markup").css("width","75%");
		qtflag = 0;
	}
	qtTaskTempGrid.getStore().reload();
	
	
}


function add() {
	window.location = window.ctx + "/experiment/qt/qtTask/editQtTask.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/qt/qtTask/showQtTaskList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});	

$("#toolbarbutton_print").click(function(){
	var url = '__report=QtTask.rptdesign&ID=' + $("#qtTask_id").val();
	commonPrint(url);
});

setTimeout(function() {
	if($("#qtTask_template").val()){
		var maxNum = $("#qtTask_maxNum").val();
		if(maxNum>0){
			load("/storage/container/sampleContainerTest.action", {
				id : $("#qtTask_template").val(),
				type :$("#type").val(),
				maxNum : 0
			}, "#3d_image0", function(){
				if(maxNum>0){
					load("/storage/container/sampleContainerTest.action", {
						id : $("#qtTask_template").val(),
						type :"qtTaskInfo",
						maxNum : 1
					}, "#3d_image1", null);
				}
			});
		}
	}
}, 100);

$("#toolbarbutton_tjsp").click(function() {
	
	var countItem = qtTaskItemGrid.store.getCount();
	if (countItem == 0 ) {
		message("请填写明细！");
		return;
	}
			submitWorkflow("QtTask", {
					userId : userId,
					userName : userName,
					formId : $("#qtTask_id").val(),
					title : $("#qtTask_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {
	var countResult = qtTaskResultGrid.store.getCount();
	if (countResult == 0 ) {
		message("请填写实验结果！");
		return;
	}
		
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#qtTask_id").val();	
	var options = {};
	options.width = 929;
	options.height = 534;	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}
	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null,biolims.common.approvalTask, url, {
		"Confirm" : function() {
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();
				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};											
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);				
			}else if(operVal=="1"){
				if(taskName=="QPCR实验"){
				var codeList = new Array();
				var codeList1 = new Array();
				var selRecord1 = qtTaskItemGrid.store;
				var flag=true;
				var flag1=true;
				if (qtTaskResultGrid.getAllRecord().length > 0) {
					var selRecord = qtTaskResultGrid.store;
					for(var j=0;j<selRecord.getCount();j++){
						var oldv = selRecord.getAt(j).get("submit");
						var a = selRecord.getAt(j).get("code");
						codeList.push(selRecord.getAt(j).get("code"));
//						if(oldv==""){
//							flag=false;
//							message(biolims.common.sampleUncommited);
//							return;
//						}
						if(selRecord.getAt(j).get("nextFlowId")==""&&selRecord.getAt(j).get("sampleCode")!=""){
							message(biolims.common.notFillNextStep);
							return;
						}
						
					}
					
					for(var j=0;j<selRecord1.getCount();j++){
						if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
							codeList1.push(selRecord1.getAt(j).get("code"));
							flag1=false;
							message(biolims.common.unfinishedExperiments);
						};
					}
					if(qtTaskResultGrid.getModifyRecord().length > 0){
						message(biolims.common.pleaseSaveRecord);
						return;
					}
					if(flag1){
							var myMask1 = new Ext.LoadMask(Ext.getBody(), {
								msg :biolims.common.pleaseWait
							});
							myMask1.show();
							Ext.MessageBox.confirm(biolims.common.makeSure, biolims.common.pleaseMakeSure2Deal, function(button, text) {
								if (button == "yes") {
									var paramData =  {};
									paramData.oper = $("#oper").val();
									paramData.info = $("#opinion").val();
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									};																		
									_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
										location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
									}, dialogWin);
																										}
							});
							myMask1.hide();
					}else{
						message(biolims.common.sampleUnfinished+codeList1);
					}
				}else{
					message(biolims.common.addAndSave);
					return;
				}
				}else{
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();
					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};										
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}
			}				
		},
		"查看流程图(Check flow chart)" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
});
function abc() {
	var operVal = $("#oper").val();
	if(operVal=="0"){
		var paramData = {};
		paramData.oper = $("#oper").val();
		paramData.info = $("#opinion").val();
		var reqData = {
			data : JSON.stringify(paramData),
			formId : formId,
			taskId : taskId,
			userId : window.userId
		};											
		_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
		location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
	}, dialogWin);				
	}else if(operVal=="1"){
		if(taskName==biolims.QPCR.test){
		var codeList = new Array();
		var codeList1 = new Array();
		var selRecord1 = qtTaskItemGrid.store;
		var flag=true;
		var flag1=true;
		if (qtTaskResultGrid.getAllRecord().length > 0) {
			var selRecord = qtTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("submit");
				codeList.push(selRecord.getAt(j).get("qtTaskCode"));
//				if(oldv==""){
//					flag=false;
//					message(biolims.common.sampleUncommited);
//					return;
//				}
				if(selRecord.getAt(j).get("nextFlowId")==""){
					message(biolims.common.notFillNextStep);
					return;
				}
			}
			for(var j=0;j<selRecord1.getCount();j++){						
				if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
					codeList1.push(selRecord1.getAt(j).get("code"));
					flag1=false;
					message(biolims.common.unfinishedExperiments);
				};
			}
			if(qtTaskResultGrid.getModifyRecord().length > 0){
				message(biolims.common.pleaseSaveRecord);
				return;
			}
			if(flag1){
					var myMask1 = new Ext.LoadMask(Ext.getBody(), {
						msg :biolims.common.pleaseWait
					});
					myMask1.show();
					Ext.MessageBox.confirm(biolims.common.makeSure, biolims.common.pleaseMakeSure2Deal, function(button, text) {
						if (button == "yes") {
							var paramData =  {};
							paramData.oper = $("#oper").val();
							paramData.info = $("#opinion").val();
							var reqData = {
								data : JSON.stringify(paramData),
								formId : formId,
								taskId : taskId,
								userId : window.userId
							};																		
							_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
								location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
							}, dialogWin);
																								}
					});
					myMask1.hide();
			}else{
				message(biolims.common.sampleUnfinished+codeList1);
			}
		}else{
			message(biolims.common.addAndSave);
			return;
		}
		}else{
			var paramData = {};
			paramData.oper = $("#oper").val();
			paramData.info = $("#opinion").val();
			var reqData = {
				data : JSON.stringify(paramData),
				formId : formId,
				taskId : taskId,
				userId : window.userId
			};										
			_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		}, dialogWin);
		}
	}				
}
function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
		var selRecord = qtTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			var type = selRecord.getAt(j).get("chromosomalLocation");
			var famCtMean = selRecord.getAt(j).get("famCtMean");
			var vicCtMean = selRecord.getAt(j).get("vicCtMean");
			if(famCtMean=="undetermined"){
				famCtMean = 37;
			}else if(vicCtMean=="undetermined"){
				vicCtMean = 37;
			}
			
			if(type=="CYP2C9*3"){
				if(Number(famCtMean)<=36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","AC");
				}else if(Number(famCtMean)<=36 && Number(vicCtMean)>36){
					selRecord.getAt(j).set("genotype","AA");
				}else if(Number(famCtMean)>36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","CC");
				}
			}else if(type=="VKORC1"){
				if(Number(famCtMean)<=36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","AG");
				}else if(Number(famCtMean)<=36 && Number(vicCtMean)>36){
					selRecord.getAt(j).set("genotype","GG");
				}else if(Number(famCtMean)>36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","AA");
				}
			}
			
		}
        var qtTaskItemDivData = qtTaskItemGrid;
		document.getElementById('qtTaskItemJson').value = commonGetModifyRecords(qtTaskItemDivData);
	    var qtTaskResultDivData = qtTaskResultGrid;
		document.getElementById('qtTaskResultJson').value = commonGetModifyRecords(qtTaskResultDivData);
		var qtTaskTemplateItemDivData = qtTaskTemplateItemGrid;
		document.getElementById('qtTaskTemplateItemJson').value = commonGetModifyRecords(qtTaskTemplateItemDivData);
		var qtTaskTemplateReagentDivData = qtTaskTemplateReagentGrid;
		document.getElementById('qtTaskTemplateReagentJson').value = commonGetModifyRecords(qtTaskTemplateReagentDivData);
		var qtTaskTemplateCosDivData = qtTaskTemplateCosGrid;
		document.getElementById('qtTaskTemplateCosJson').value = commonGetModifyRecords(qtTaskTemplateCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/qt/qtTask/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();	
		}
}	


function saveItem() {
	var id = $("#qtTask_id").val();
	if (!id) {
		return;
	}
	var sf = true;
	var selRecords =qtTaskItemGrid.store;
	for(var h=0;h<selRecords.getCount();h++){
		var z=0;
		for(var l=0;l<selRecords.getCount();l++){
			if(selRecords.getAt(h).get("orderNumber")==selRecords.getAt(l).get("orderNumber")){
				z++;
			}
		}
		if(z!=1){
			sf=false;
		}
		
	}
	if(sf){
		var selRecord = qtTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			var type = selRecord.getAt(j).get("chromosomalLocation");
			var famCtMean = selRecord.getAt(j).get("famCtMean");
			var vicCtMean = selRecord.getAt(j).get("vicCtMean");
			if(famCtMean=="undetermined"){
				famCtMean = 37;
			}else if(vicCtMean=="undetermined"){
				vicCtMean = 37;
			}
			
			if(type=="CYP2C9*3"){
				if(Number(famCtMean)<=36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","AC");
				}else if(Number(famCtMean)<=36 && Number(vicCtMean)>36){
					selRecord.getAt(j).set("genotype","AA");
				}else if(Number(famCtMean)>36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","CC");
				}
			}else if(type=="VKORC1"){
				if(Number(famCtMean)<=36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","AG");
				}else if(Number(famCtMean)<=36 && Number(vicCtMean)>36){
					selRecord.getAt(j).set("genotype","GG");
				}else if(Number(famCtMean)>36 && Number(vicCtMean)<=36){
					selRecord.getAt(j).set("genotype","AA");
				}
			}
			
		}
		
			var qtTaskItemDivData = qtTaskItemGrid;
			var qtTaskItemJson = commonGetModifyRecords(qtTaskItemDivData);
		    var qtTaskResultDivData = qtTaskResultGrid;
			var qtTaskResultJson = commonGetModifyRecords(qtTaskResultDivData);
			var qtTaskTemplateItemDivData = qtTaskTemplateItemGrid;
			var qtTaskTemplateItemJson = commonGetModifyRecords(qtTaskTemplateItemDivData);
			var qtTaskTemplateReagentDivData = qtTaskTemplateReagentGrid;
			var qtTaskTemplateReagentJson = commonGetModifyRecords(qtTaskTemplateReagentDivData);
			var qtTaskTemplateCosDivData = qtTaskTemplateCosGrid;
			var qtTaskTemplateCosJson = commonGetModifyRecords(qtTaskTemplateCosDivData);
		
		ajax("post", "/experiment/qt/qtTask/saveAjax.action", {
			qtTaskItemJson:qtTaskItemJson,
			qtTaskResultJson:qtTaskResultJson,
			qtTaskTemplateItemJson:qtTaskTemplateItemJson,
			qtTaskTemplateReagentJson:qtTaskTemplateReagentJson,
			qtTaskTemplateCosJson:qtTaskTemplateCosJson,
			id : id
		}, function(data) {
			if (data.success) {
				//message("保存成功！");
				if (data.equip != 0 && typeof (data.equip) != 'undefined') {
					message(data.equip + "条设备数据保存成功！");
				}
				if (data.re != 0 && typeof (data.re) != 'undefined') {
					message(data.re + "条原辅料数据保存成功！");
				}
				qtTaskItemDivData.getStore().reload();
				qtTaskResultDivData.getStore().reload();
				qtTaskTemplateItemDivData.getStore().reload();
				qtTaskTemplateReagentDivData.getStore().reload();
				qtTaskTemplateCosDivData.getStore().reload();
				gridContainerTestGrid.getStore().reload();
			} else {
				message(biolims.common.saveFailed);
			}
		}, null);
	}else{
		message("实验明细存在相同排序号！");
	}
	
	
}


function editCopy() {
	window.location = window.ctx + '/experiment/qt/qtTask/copyQtTask.action?id=' + $("#qtTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	var QtTaskInfoGrid = qtTaskResultGrid.getStore();
	var QtTaskItemGrid = qtTaskItemGrid.getStore();
	var num = 0;
	for(var i= 0; i<QtTaskItemGrid.getCount();i++){		
		if(QtTaskInfoGrid.getCount()==0){
			message(biolims.common.pleaseComplete);
			return;
		}
		for(var j=0;j<QtTaskInfoGrid.getCount();j++){
			if(QtTaskItemGrid.getAt(i).get("yCode")==QtTaskInfoGrid.getAt(j).get("yCode")){
				j=QtTaskInfoGrid.getCount();
			}else{
				num=num+1;
				if(num==QtTaskInfoGrid.getCount()){
					num=0;
					message(biolims.common.pleaseComplete);
					return;
				}
			}
		}		
	}
	commonChangeState("formId=" + $("#qtTask_id").val() + "&tableId=QtTask");
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	var t = "";
	fs.push($("#qtTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	var sf = true;
	var selRecords =qtTaskItemGrid.store;
	for(var h=0;h<selRecords.getCount();h++){
		var z=0;
		for(var l=0;l<selRecords.getCount();l++){
			if(selRecords.getAt(h).get("orderNumber")==selRecords.getAt(l).get("orderNumber")){
				z++;
			}
		}
		if(z!=1){
			sf=false;
		}
		
	}
	if(sf){
		
	}else{
		fs.push($("#kong").val());
		nsc.push("实验明细存在相同排序号！");
	}
	/**
	 * by 吴迪 2017-09-19 添加限制条件
	 */
	//描述
	fs.push($("#qtTask_name").val());
	nsc.push(biolims.common.describeEmpty);
	//模板
	fs.push($("#qtTask_template").val());
	nsc.push(biolims.common.templateEmpty);
	//第一实验员
	fs.push($("#qtTask_testUserOneId").val());
	nsc.push(biolims.common.testUserEmptyEmpty);
	//审核人
	fs.push($("#qtTask_confirmUser").val());
	nsc.push(biolims.common.confirmUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.QPCR.test,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/qt/qtTask/showQtTaskItemList.action", {
				id : $("#qtTask_id").val()
			}, "#qtTaskItempage");
load("/experiment/qt/qtTask/showTemplateItemList.action", {
	id:$("#qtTask_id").val()
}, "#qtTaskTemplateItempage");
load("/experiment/qt/qtTask/showQtTaskTemplateReagentList.action",{
				id:$("#qtTask_id").val(),
			}, "#qtTaskTemplateReagentpage");
load("/experiment/qt/qtTask/showQtTaskTemplateCosList.action", {
	id:$("#qtTask_id").val()
}, "#qtTaskTemplateCospage");
load("/experiment/qt/qtTask/showQtTaskInfoList.action", {
				id : $("#qtTask_id").val()
			}, "#qtTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text:biolims.common.copy
						});
	item.on('click', editCopy);

// 选择检查项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : biolims.QPCR.selectProject,
				layout : 'fit',
				width : 600,
				height : 600,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/system/primers/mappingPrimersLibrary/mappingPrimersLibrarySelect.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}
function setPrimersLibraryValue(record){
	var str = document.getElementById("qtTask_productId").value;
	for ( var i = 0; i < record.length; i++) {
		str  = str + record[i].get("id") +",";
	}
	document.getElementById("qtTask_productId").value = str;
	document.getElementById("qtTask_productName").value = str;
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}

function setPrimersLibraryFun(rec) {
//	var productName = "";
//	ajax(
//			"post",
//			"/com/biolims/system/product/findProductToSample.action",
//			{
//				code : id
//			},
//			function(data) {
//				if (data.success) {
//					$.each(data.data, function(i, obj) {
//						productName += obj.name;
//					});
//					document.getElementById("qtTask_productId").value = id;
//					document.getElementById("qtTask_productName").value = productName;
//				}
//			}, null);
	document.getElementById("qtTask_productId").value = rec.get("id");
	document.getElementById("qtTask_productName").value = rec.get("name");
	
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}

//调用模板
function TemplateFun(){
			var type="doQpcr";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:biolims.common.pleaseSelectTemplate,layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text:biolims.common.close,
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}

//双击加载模板数据
function setTemplateFun(rec){	
	if($('#qtTask_acceptUser_name').val()==""){
		document.getElementById('qtTask_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('qtTask_acceptUser_name').value=rec.get('acceptUser-name');
	}		
	var itemGrid=qtTaskItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
			itemGrid.getAt(i).set("sampleConsume",rec.get('sampleNum'));
		}
	}
		var code=$("#qtTask_template").val();
		if(code==""){
			var cid=rec.get('id');
						document.getElementById('qtTask_template').value=rec.get('id');
						document.getElementById('qtTask_template_name').value=rec.get('name');
						var win = Ext.getCmp('TemplateFun');
						if(win){win.close();}
						var id=rec.get('id');
						//加载模板明细
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {
									var ob = qtTaskTemplateItemGrid.getStore().recordType;
									qtTaskTemplateItemGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										qtTaskTemplateItemGrid.getStore().add(p);							
									});									
									qtTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
						//加载原辅料明细
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qtTaskTemplateReagentGrid.getStore().recordType;
								qtTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									qtTaskTemplateReagentGrid.getStore().add(p);							
								});								
								qtTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						//加载实验设备明细
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qtTaskTemplateCosGrid.getStore().recordType;
								qtTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id", obj.typeId);
									p.set("type-name", obj.typeName);
									p.set("state",obj.state);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									qtTaskTemplateCosGrid.getStore().add(p);							
								});			
								qtTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null);
		}else{
			var flag = true;
			if(rec.get('id')==code){
				flag = confirm(biolims.common.whetherOrNot2Load);
 			 }
			if(flag==true){
 				//判断设备是否占用
 				var cid=rec.get('id');
						var ob1 = qtTaskTemplateItemGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id");				
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/qt/qtTask/delTemplateItemOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null);
								}else{								
									qtTaskTemplateItemGrid.store.removeAll();
								}
							}
							qtTaskTemplateItemGrid.store.removeAll();
		 				}		 				
						var ob2 = qtTaskTemplateReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");								
								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/qt/qtTask/delTemplateReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null); 
								}else{
									qtTaskTemplateReagentGrid.store.removeAll();
								}
							}
							qtTaskTemplateReagentGrid.store.removeAll();
		 				}
						var ob3 = qtTaskTemplateCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");							
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/qt/qtTask/delTemplateCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null); 
								}else{
									qtTaskTemplateCosGrid.store.removeAll();
								}
							}
							qtTaskTemplateCosGrid.store.removeAll();
		 				}
						document.getElementById('qtTask_template').value=rec.get('id');
		 				document.getElementById('qtTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	
									var ob = qtTaskTemplateItemGrid.getStore().recordType;
									qtTaskTemplateItemGrid.stopEditing();									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										qtTaskTemplateItemGrid.getStore().add(p);							
									});									
									qtTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qtTaskTemplateReagentGrid.getStore().recordType;
								qtTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									qtTaskTemplateReagentGrid.getStore().add(p);							
								});								
								qtTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qtTaskTemplateCosGrid.getStore().recordType;
								qtTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id", obj.typeId);
									p.set("type-name", obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("state",obj.state);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									qtTaskTemplateCosGrid.getStore().add(p);							
								});			
								qtTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
					}			
	}	
		reloadQtTaskItem(rec.get("templateFieldsCode"), rec
				.get("templateFieldsItemCode"));

}
	
//按条件加载原辅料
function showReagent(){
	//获取全部数据
	var allRcords=qtTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=qtTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=qtTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#qtTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/qt/qtTask/showQtTaskTemplateReagentList.action", {
			id : $("#qtTask_id").val()
		}, "#qtTaskTemplateReagentpage");
	}else if(length1==1){
		qtTaskTemplateReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/qt/qtTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = qtTaskTemplateReagentGrid.getStore().recordType;
				qtTaskTemplateReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("expireDate",obj.expireDate);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("qtTask-id",obj.tId);
					p.set("qtTask-name",obj.tName);					
					qtTaskTemplateReagentGrid.getStore().add(p);							
				});
				qtTaskTemplateReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.selectRecord);
		return;
	}	
}

//按条件加载设备
function showCos(){
	var allRcords=qtTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=qtTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=qtTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#qtTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/qt/qtTask/showQtTaskTemplateCosList.action", {
			id : $("#qtTask_id").val()
		}, "#qtTaskTemplateCospage");
	}else if(length1==1){
		qtTaskTemplateCosGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/qt/qtTask/setCos.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = qtTaskTemplateCosGrid.getStore().recordType;
				qtTaskTemplateCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("type-id", obj.typeId);
					p.set("type-name", obj.typeName);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("state",obj.state);
					p.set("note",obj.note);
					p.set("time",obj.time);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tCos",obj.tCos);
					p.set("qtTask-id",obj.tId);
					p.set("qtTask-name",obj.tName);					
					qtTaskTemplateCosGrid.getStore().add(p);							
				});
				qtTaskTemplateCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.selectRecord);
		return;
	}
}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.rollback
			});
		item.on('click', ckcrk);
		
		});
	
	function ckcrk(){		
		Ext.MessageBox.confirm(biolims.common.prompt, biolims.common.initTaskList, function(button, text) {
			if (button == "yes") {
				var selRecord = qtTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message(biolims.common.canNotInit);
						return;
					}
				}
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message(biolims.common.backSuccess);
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message(biolims.common.rollbackFailed);
							}
						}, null);
					}
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.save
			});
		item.on('click', ckcrk2);		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.handleRollback
			});
		item.on('click', ckcrk3);		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: biolims.common.handlingRollback, progressText:biolims.common.handling, width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "QtTask",id : $("#qtTask_id").val()
		}, function(data) {
			if (data.success) {	
				message(biolims.common.handleRollbackSuccess);
			} else {
				message(biolims.common.handleRollbackFailed);
			}
		}, null);
	
}
		



//	var loadtestUser;
	//选择实验组用户
	function testUser(type){
		var gid=$("#qtTask_acceptUser").val();
		if(gid!=""){
			var options = {};
			options.width = 500;
			options.height = 500;
			loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				"Confirm" : function() {
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						var id=[];
						var name=[];
						for(var i=0;i<selectRecord.length;i++){
							id.push(selectRecord[i].get("user-id"));
							name.push(selectRecord[i].get("user-name"));
						}
						if(type==1){//第一批实验员
							$("#qtTask_testUserOneId").val(id);
							$("#qtTask_testUserOneName").val(name);
						}else if(type==2){//第二批实验员
							$("#qtTask_testUserTwoId").val(id);
							$("#qtTask_testUserTwoName").val(name);
						}else if(type==3){//审核人
							$("#qtTask_confirmUser").val(id);
							$("#qtTask_confirmUser_name").val(name);
						}
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}else{
			message(biolims.common.pleaseSelectGroup);
		}
		
	}
//	function setUserGroupUser(){
//		var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
//		var selectRecord = operGrid.getSelectionModel().getSelections();
//		if (selectRecord.length > 0) {
//				$("#qtTask_testUser").val(selectRecord[0].get("user-id"));
//				$("#qtTask_testUser_name").val(selectRecord[0].get("user-name"));
//		}else{
//			message(biolims.common.selectYouWant);
//			return;
//		}
//		loadtestUser.dialog("close");
//	}
	
	
	function selectTemp(){
		var options = {};
		options.width = document.body.clientWidth*0.9;
		options.height = document.body.clientHeight*0.9;
		var url = "/experiment/qt/qtTask/showQtTaskTempList.action";
		loadDialogPage(null, biolims.common.approvalTask, url, {}, true, options);
	}
