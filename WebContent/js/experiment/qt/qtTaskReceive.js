var qtTaskReceiveGrid;
$(function(){
	var cols={};
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'receiveUser-id',
		type:"string"
	});
	fields.push({
		name:'receiveUser-name',
		type:"string"
	});
	fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
		name:'receiverDate',
		type:"string"
	});
	fields.push({
		name:'method',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'nextFlow',
		type:"string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
		name:'advice',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'isExecute',
		type:"string"
	});
	fields.push({
		name:'feedbackTime',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'receiveUser-id',
		hidden:true,
		header:biolims.common.receiverId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiveUser-name',
		header:biolims.common.receiverName,
		hidden:false,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiverDate',
		header:biolims.common.receiveDate,
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'location',
		header:biolims.common.location,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowStateName,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*6,		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTaskReceive/showQtTaskReceiveListJson.action";
	var opts={};
	opts.title=biolims.common.sampleReceive;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	qtTaskReceiveGrid=gridTable("show_qtTaskReceive_div",cols,loadParam,opts);
})

function add(){
		window.location=window.ctx+'/experiment/qt/qtTaskReceive/editQtTaskReceive.action';
	}

function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/experiment/qt/qtTaskReceive/editQtTaskReceive.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/experiment/qt/qtTaskReceive/viewQtTaskReceive.action?id=' + id;
}

function exportexcel() {
	qtTaskReceiveGrid.title = biolims.common.exportList;
	var vExportContent = qtTaskReceiveGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {			
				if (($("#startreceiverDate").val() != undefined) && ($("#startreceiverDate").val() != '')) {
					var startreceiverDatestr = ">=##@@##" + $("#startreceiverDate").val();
					$("#receiverDate1").val(startreceiverDatestr);
				}
				if (($("#endreceiverDate").val() != undefined) && ($("#endreceiverDate").val() != '')) {
					var endreceiverDatestr = "<=##@@##" + $("#endreceiverDate").val();
					$("#receiverDate2").val(endreceiverDatestr);
				}
				commonSearchAction(qtTaskReceiveGrid);
				$(this).dialog("close");
			},
			"清空(Empty)" : function() {
				form_reset();
			}
		}, true, option);
	});
});
