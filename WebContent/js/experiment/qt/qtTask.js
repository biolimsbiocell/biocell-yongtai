var qtTaskGrid;
$(function(){
	var cols={};
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'createUser-id',
		type:"string"
	});
	fields.push({
		name:'createUser-name',
		type:"string"
	});
	fields.push({
		name:'createDate',
		type:"string"
	});
	fields.push({
		name:'testUser-id',
		type:"string"
	});
	fields.push({
		name:'testUser-name',
		type:"string"
	});
	fields.push({
		name:'receiveDate',
		type:"string"
	});
	 
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
		name:'maxNum',
		type:"string"
	});
	fields.push({
		name:'template-id',
		type:"string"
	});
	fields.push({
		name:'template-name',
		type:"string"
	});
	fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:30*6,		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.common.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDateName,
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'testUser-id',
		hidden:true,
		header:biolims.common.testUserId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testUser-name',
		header:biolims.common.testUserName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:biolims.common.templateId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'template-name',
		header:biolims.common.templateName,		
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiveDate',
		header:biolims.common.acceptDate,
		width:20*6,
		hidden:true,
		sortable:true
	});	
	cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:biolims.common.acceptUserId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.acceptUserId,		
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'maxNum',
		header:biolims.common.containerNum,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskListJson.action";
	var opts={};
	opts.title=biolims.QPCR.test;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	qtTaskGrid=gridTable("show_qtTask_div",cols,loadParam,opts);
});

function add(){
		window.location=window.ctx+'/experiment/qt/qtTask/editQtTask.action';
}

function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/experiment/qt/qtTask/editQtTask.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/experiment/qt/qtTask/viewQtTask.action?id=' + id;
}

function exportexcel() {
	qtTaskGrid.title = biolims.common.exportList;
	var vExportContent = qtTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {			
				if (($("#startreciveDate").val() != undefined) && ($("#startreciveDate").val() != '')) {
					var startreciveDatestr = ">=##@@##" + $("#startreciveDate").val();
					$("#reciveDate1").val(startreciveDatestr);
				}
				if (($("#endreciveDate").val() != undefined) && ($("#endreciveDate").val() != '')) {
					var endreciveDatestr = "<=##@@##" + $("#endreciveDate").val();
					$("#reciveDate2").val(endreciveDatestr);
				}				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();
					$("#createDate2").val(endcreateDatestr);
				}								
				commonSearchAction(qtTaskGrid);
				$(this).dialog("close");
			},
			"清空(Empty)" : function() {
				form_reset();
			}
		}, true, option);
	});
});
