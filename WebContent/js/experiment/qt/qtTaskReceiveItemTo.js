﻿var qtTaskReceiveItemTogrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	   });
	   
	   fields.push({
		   name:'code',
		   type:"string"
	   });
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	   fields.push({
			name:'name',
			type:"string"
		});
	   fields.push({
			name:'patientName',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'productId',
			type:"string"
		});
	   fields.push({
			name:'productName',
			type:"string"
		});
		   
	   fields.push({
			name:'inspectDate',
			type:"string"
		});
	   fields.push({
			name:'acceptDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'unit',
			type:"string"
		});
	   fields.push({
			name:'state',
			type:"string"
		});
	   fields.push({
			name:'note',
			type:"string"
		});
	   fields.push({
			name:'concentration',
			type:"string"
		});
				   
	   fields.push({
			name:'result',
			type:"string"
		});
	   fields.push({
			name:'reason',
			type:"string"
		});
//	   fields.push({
//			name:'idCard',
//			type:"string"
//		});
	   fields.push({
			name:'sequencingFun',
			type:"string"
		});
	   fields.push({
			name:'reportDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'orderId',
			type:"string"
		});
//	   fields.push({
//			name:'phone',
//			type:"string"
//		});
	   
	   fields.push({
			name:'classify',
			type:"string"
		});
	   fields.push({
			name:'sampleNum',
			type:"string"
		});
	   fields.push({
			name : 'labCode',
			type : "string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:20*10,
		sortable:true,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'sequencingFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6
	});
	
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:20*10,
		sortable:true,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:biolims.common.concentration,
		width:20*10
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:biolims.common.result,
		width:20*10
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:20*6
//	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6
	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6
//	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTaskReceive/showQtTaskReceiveItemListToJson.action";
	var opts={};
	opts.title=biolims.common.toReceiveSample;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
    
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = qtTaskReceiveItemTogrid.getAllRecord();
							var store = qtTaskReceiveItemTogrid.store;

							var isOper = true;
							var buf = [];
							qtTaskReceiveItemTogrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("sampleCode")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							qtTaskReceiveItemTogrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message(biolims.common.samplecodeComparison);
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							qtTaskReceiveItemTogrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	qtTaskReceiveItemTogrid=gridEditTable("qtTaskReceiveItemTodiv",cols,loadParam,opts);
	$("#qtTaskReceiveItemTodiv").data("qtTaskReceiveItemTogrid", qtTaskReceiveItemTogrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});



	//从左边添加到右边的明细中
	function addItem(){
		var selRecord = qtTaskReceiveItemTogrid.getSelectionModel().getSelections();//从左边获取数据
		var getRecord = qtTaskReceiveItemGrid.store;//填充到当前的明细中
		if(selRecord.length >0){
			$.each(selRecord,function(i, obj){
				var isRepeat = false;
				for(var j=0; j<getRecord.getCount();j++){
					var getData = getRecord.getAt(j).get("tempId");
					if(getData==obj.get("id")){
						isRepeat = true;
						message(biolims.common.haveDuplicate);
						break;
					}
				}
				if(!isRepeat){
					var ob = qtTaskReceiveItemGrid.getStore().recordType;
					qtTaskReceiveItemGrid.stopEditing();
					var p= new ob({});
					p.set("tempId",obj.get("id"));
					p.set("code", obj.get("code"));
					p.set("sampleCode", obj.get("sampleCode"));
					p.set("name", obj.get("name"));
					p.set("patientName", obj.get("patientName"));
					p.set("productId", obj.get("productId"));
					p.set("productName", obj.get("productName"));
					p.set("inspectDate", obj.get("inspectDate"));
					p.set("idCard", obj.get("idCard"));
					p.set("sequencingFun", obj.get("sequencingFun"));
					p.set("reportDate", obj.get("reportDate"));
					p.set("orderId", obj.get("orderId"));
					p.set("phone", obj.get("phone"));
					
					p.set("note", obj.get("note"));
					p.set("state", obj.get("state"));
					p.set("stateName", obj.get("stateName"));
					p.set("storage-id", obj.get("storage-id"));
					p.set("storage-name", obj.get("storage-name"));
					p.set("result", obj.get("result"));
					p.set("reason", obj.get("reason"));
					p.set("acceptDate", obj.get("acceptDate"));
					p.set("volume", obj.get("volume"));
					p.set("unit", obj.get("unit"));
					p.set("concentration", obj.get("concentration"));
					p.set("sampleType", obj.get("sampleType"));
					p.set("sampleNum", obj.get("sampleNum"));
					p.set("labCode", obj.get("labCode"));
					qtTaskReceiveItemGrid.getStore().add(p);
					
				}
					
					
			});
			
			qtTaskReceiveItemGrid.startEditing(0,0);
		}else{
			message(biolims.common.pleaseChooseSamples);
		}
	}