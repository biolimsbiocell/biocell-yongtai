﻿var qtTaskTemplateReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'tReagent',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'itemId',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'batch',
		type:"string"
	});
	fields.push({
		name:'isGood',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'qtTask-id',
		type:"string"
	});
	fields.push({
		name:'qtTask-name',
		type:"string"
	});
    fields.push({
		name:'oneNum',
		type:"string"
	});
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'num',
		type:"string"
	});
    fields.push({
		name:'sn',
		type:"string"
	});
    fields.push({
		name:'expireDate',
		type:"string"
	});
    fields.push({
		name:'reagentCode',
		type:"string"
	});
    fields.push({
		name:'isRunout',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.common.reagentId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tReagent',
		hidden : true,
		header : biolims.common.templateReagentId,
		width : 20 * 6
	});

	var codes = new Ext.form.TextField({
		allowBlank : false
	});
	codes.on('focus', function() {
		var selectRecord = bloodSplitReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				// var code=obj.get("code");
				var tid = $("#bloodSplit_template").val();
				loadReagentItemByCode(tid);
			});
		}
	});
	cm.push({
		dataIndex : 'code',
		hidden : true,
		header : biolims.common.reagentNo,
		width : 20 * 6,
		editor : codes
	});
	cm.push({
		dataIndex : 'itemId',
		hidden : true,
		header : biolims.common.templateStepNo,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'name',
		hidden : false,
		header : biolims.common.reagentName,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sn',
		hidden : false,
		header : 'sn',
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		}),
	});

	// 鼠标单击触发事件
	var batchs = new Ext.form.TextField({
		allowBlank : false
	});
	batchs.on('focus', function() {
		var selectRecord = bloodSplitReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code = obj.get("code");
				loadStorageReagentBuy(code);
			});
		}
	});
	cm.push({
		dataIndex : 'batch',
		hidden : false,
		header : biolims.common.batch,
		width : 20 * 6,
		editor : batchs
	});
	// cm.push({
	// dataIndex:'isGood',
	// hidden : false,
	// header:'是否通过检验',
	// width:20*6,
	//		
	// editor : new Ext.form.TextField({
	// allowBlank : true
	// })
	// });
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isGood',
		hidden : false,
		header : biolims.common.isGood,
		width : 20 * 6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});

	cm.push({
		dataIndex : 'count',
		hidden : false,
		header : biolims.common.count,
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'oneNum',
		hidden : true,
		header : biolims.common.singleDose,// 单个用量
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleNum',
		hidden : false,
		header : biolims.common.sampleNum,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'num',
		hidden : true,
		header : biolims.common.dose,// 用量
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'expireDate',
		hidden : false,
		header : biolims.common.expirationDate,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'reagentCode',
		hidden : false,
		header :biolims.common.reagentNo,
		width : 20 * 6
	});
	var isRunout = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			}, {
				id : '0',
				name : biolims.common.no
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'isRunout',
		hidden : false,
		header : biolims.common.doYouUseIt,
		width : 20 * 5,
		renderer : Ext.util.Format.comboRenderer(isRunout),
		editor : isRunout
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : biolims.common.note,
		width : 20 * 6
	});
	cm.push({
		dataIndex:'qtTask-id',
		hidden : true,
		header:biolims.common.note,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskTemplateReagentListJson.action?id="+$("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();	
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qt/qtTask/delqtTaskTemplateReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qtTaskTemplateItemGrid.getStore().commitChanges();
				qtTaskTemplateItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//			text : biolims.common.fillDetail,
//			handler : function (){
//				//获取选择的数据
//				var selectRcords=qtTaskTemplateItemGrid.getSelectionModel().getSelections();
//				//获取全部数据
//				var allRcords=qtTaskTemplateItemGrid.store;
//				//选中的数量
//				var length1=selectRcords.length;
//				//全部数据量
//				var length2=allRcords.getCount();
//				if(length2>0){
//					if(length1==1){
//						var code="";
//						$.each(selectRcords, function(i, obj) {
//							code=obj.get("code");
//						});
//						if(code!=""){
//							showStorageList(code);
//						}else{
//							message(biolims.common.addTemplateDetail);
//							return;
//						}				
//					}else if(length1>1){
//						message(biolims.common.onlyChooseOne);
//						return;
//					}else{
//						message(biolims.common.pleaseSelectData);
//						return;
//					}
//				}else{
//					message(biolims.common.theDataIsEmpty);
//					return;
//				}
//			}
//	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.copy,
		handler : function() {
			var ob = qtTaskTemplateReagentGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			var records = qtTaskTemplateReagentGrid.getSelectRecord();
			if (records.length==1) {
				$.each(records, function(i, obj) {
					p.set("code",obj.get("code"));
					p.set("name",obj.get("name"));
					p.set("batch",obj.get("batch"));
					p.set("oneNum",obj.get("oneNum"));
					p.set("expireDate",obj.get("expireDate"));
					p.set("sampleNum",obj.get("sampleNum"));
					p.set("isGood",obj.get("isGood"));
					p.set("itemId",obj.get("itemId"));
					p.set("tReagent",obj.get("tReagent"));
					p.set("qtTask-id",obj.get("qtTask-id"));
					p.set("qtTask-name",obj.get("qtTask-name"));
					p.set("note",obj.get("note"));
					qtTaskTemplateReagentGrid.stopEditing();
					qtTaskTemplateReagentGrid.getStore().add(p);
					qtTaskTemplateReagentGrid.startEditing(0, 0);
				});
			}else{
				message(biolims.common.pleaseChooseCopyData);
			}
		}
	});
	if($("#qtTask_id").val()&&$("#qtTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	}
	qtTaskTemplateReagentGrid=gridEditTable("qtTaskTemplateReagentdiv",cols,loadParam,opts);
	$("#qtTaskTemplateReagentdiv").data("qtTaskTemplateReagentGrid", qtTaskTemplateReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:biolims.common.chosePurchasingReagents,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}

function setStorageReagentBuy(rec){	
	var gridGrid = $("#qtTaskTemplateReagentdiv").data("qtTaskTemplateReagentGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}

//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, biolims.common.theInventoryMasterData, url, {
		"Confirm" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					qtTaskTemplateReagentGrid.stopEditing();
					var ob = qtTaskTemplateReagentGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;
					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("itemId",code);				
					qtTaskTemplateReagentGrid.getStore().add(p);	
				});
				qtTaskTemplateReagentGrid.startEditing(0, 0);
				options.close();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	}, true, options);
}

//根据原辅料编号查询原辅料明细
function loadReagentItemByCode(tid){
		var options = {};
		options.width = 900;
		options.height = 460;
		var url="/system/template/template/showReagentItemByCodeList.action?tid="+tid;
		loadDialogPage(null, biolims.common.selectedDetail, url, {
			 "Confirm": function() {
				 selRecord = reagentItem1Grid.getSelectionModel();
					if (selRecord.getSelections().length > 0) {
						$.each(selRecord.getSelections(), function(i, obj) {
							qtTaskTemplateReagentGrid.stopEditing();
							var ob = qtTaskTemplateReagentGrid.getStore().recordType;
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.get("code"));
							p.set("name",obj.get("name"));
							p.set("batch",obj.get("batch"));
							p.set("isGood",obj.get("isGood"));
							p.set("itemId",obj.get("itemId"));							
							p.set("oneNum",obj.get("num"));
							p.set("note",obj.get("note"));
							qtTaskTemplateReagentGrid.getStore().add(p);	
						});
						qtTaskTemplateReagentGrid.startEditing(0, 0);
						options.close();
					}else{
						message(biolims.common.pleaseSelect);
					}
				 options.close();
			}
		}, true, options);
}