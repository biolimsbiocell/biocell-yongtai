var qtTaskRepeatGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
    fields.push({
		name:'productName',
		type:"string"
	});   
	fields.push({
		name:'sequenceFun',
		type:"string"
	});
    fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name:'idCard',
		type:"string"
	});
    fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'sampleType',
		type : "string"
	});
    fields.push({
		name:'sampleCondition',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'isExecute',
		type:"string"
	});
    fields.push({
		name:'feedbackTime',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
		name:'name',
		type:"string"
	});
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6,		
		sortable:true
	});	
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:biolims.common.productId,
		width:30*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:biolims.common.idCard,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		hidden : false,
		header:biolims.common.phone,
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:biolims.common.orderId,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'Condition',
		header:biolims.common.sampleCondition,
		width:20*6,
		sortable:true
	});
	var storeresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified], [ '0', biolims.common.disqualified ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'method',
		header:biolims.common.method,
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes], [ '0',biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isExecute',
		header : biolims.common.isExecute,
		width : 20 * 6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'feedbackTime',
		hidden : false,
		header:biolims.common.backDate,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*6,
		
		sortable:true
	});	
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:biolims.common.classify,
		width:20*6,		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/showRepeatQtTaskListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.QPCR.testRepeat;
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.batchDealOpinion,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_next_div"), biolims.common.batchDealOpinion, null, {
				"Confirm" : function() {
					var records = qtTaskRepeatGrid.getSelectRecord();
					if (records && records.length > 0) {
						var nextFlow = $("#method").val();
						qtTaskRepeatGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("method", nextFlow);
						});
						qtTaskRepeatGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	qtTaskRepeatGrid=gridEditTable("show_qtTaskRepeat_div",cols,loadParam,opts);
	$("#show_qtTaskRepeat_div").data("qtTaskRepeatGrid", qtTaskRepeatGrid);
});

//保存
function save(){
	var itemJson = commonGetModifyRecords(qtTaskRepeatGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/qtTask/saveQtTaskRepeat.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					qtTaskRepeatGrid.getStore().commitChanges();
					qtTaskRepeatGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);					
	}else{
		message(biolims.common.noData2Save);
	}
}

//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(qtTaskRepeatGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();
		}
	}, true, option);
}
