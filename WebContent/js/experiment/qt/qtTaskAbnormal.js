var qtTaskAbnormalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
    fields.push({	   
	    name:'sampleCode',
		type:"string"
    });
    fields.push({
	    name:'code',
	    type:"string"
    }); 
    fields.push({
		name:'patientName',
		type:"string"
    });
    fields.push({
		name:'productName',
		type:"string"
    });
    fields.push({
		name:'productId',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
    fields.push({
		name:'inspectDate',
		type:"string"
	});
//    fields.push({
//		name : 'idCard',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	}); 
    fields.push({
		name : 'orderId',
		type : "string"
	});
    fields.push({
	    name:'sampleType',
	    type:"string"
	});
    fields.push({
		name:'sampleCondition',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'isExecute',
		type:"string"
	});
	fields.push({
		name:'feedbackTime',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'classify',
		type:"string"
	});
	fields.push({
		name:'sampleNum',
		type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.id,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});	
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		hidden : false,
		width:30*6,
		sortable:true
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:30*6
//	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		hidden : true,
		width:30*6
	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6
//	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:biolims.common.orderId,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'Condition',
		hidden : true,
		header:biolims.common.sampleCondition,
		width:20*6
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			},{
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),
		editor: result
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:biolims.common.method,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeconfirmCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var confirmCob = new Ext.form.ComboBox({
		store : storeconfirmCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:biolims.common.confirm2Execute,
		width:20*6,
		hidden : false,
		editor : confirmCob,
		renderer : Ext.util.Format.comboRenderer(confirmCob)
	});	
	cm.push({
		dataIndex:'feedbackTime',
		hidden : false,
		header:biolims.common.abnormalFeedbackTime,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTaskAbnormal/showQtTaskAbnormalListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.QPCR.testAbnormal;
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"),biolims.common.batchResult, null, {
				"Confirm" : function() {
					var records = qtTaskAbnormalGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						qtTaskAbnormalGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						qtTaskAbnormalGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : function() {
			var records = qtTaskAbnormalGrid.getSelectRecord();
			if(records.length>0){
				if(records.length>2){
					var productId = new Array();
					$.each(records, function(j, k) {
						productId[j]=k.get("productId");
					});
					for(var i=0;i<records.length;i++){
						if(i!=0&&productId[i]!=productId[i-1]){
							message(biolims.common.testDifferent);
							return;
						}
					}
					loadTestNextFlowCob();
				}else{
					loadTestNextFlowCob();
				}				
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.batchExecute,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), biolims.common.batchExecute, null, {
				"Confirm" : function() {
					var records = qtTaskAbnormalGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute").val();
						qtTaskAbnormalGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", isExecute);
						});
						qtTaskAbnormalGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	qtTaskAbnormalGrid=gridEditTable("show_qtTaskAbnormal_div",cols,loadParam,opts);
	$("#show_qtTaskAbnormal_div").data("qtTaskAbnormalGrid", qtTaskAbnormalGrid);
});

//保存
function save(){
	var itemJson = commonGetModifyRecords(qtTaskAbnormalGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/qt/qtTaskAbnormal/saveQtTaskAbnormal.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					qtTaskAbnormalGrid.getStore().commitChanges();
					qtTaskAbnormalGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);					
	}else{
		message(biolims.common.noData2Save);
	}
}

//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"),biolims.common.saarch, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(qtTaskAbnormalGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();
		}
	}, true, option);
}

function selectInfo(){
	qtTaskAbnormalGrid.store.removeAll();
	var code=$("#qtTaskAbnormal_code").val();
	var Code=$("#qtTaskAbnormal_Code").val();
	ajax("post", "/experiment/qt/qtTaskAbnormal/selectAbnormal.action", {
		code : code,Code:Code
	}, function(data) {
		if (data.success) {	
			var ob = qtTaskAbnormalGrid.getStore().recordType;
			qtTaskAbnormalGrid.stopEditing();
			$.each(data.data, function(i, obj) {
				var p = new ob({});
				p.isNew = true;				
				p.set("id", obj.id);
				p.set("code", obj.code);
				p.set("Code", obj.Code);
				p.set("Type", obj.Type);
				p.set("method", obj.method);
				p.set("nextFlow", obj.nextFlow);
				p.set("note", obj.note);
				p.set("methods", obj.methods);
				p.set("feedbackTime", obj.feedbackTime);
				p.set("isExecute", obj.isExecute);
				qtTaskAbnormalGrid.getStore().add(p);							
			});
			qtTaskAbnormalGrid.startEditing(0, 0);		
		} else {
			message(biolims.common.anErrorOccurred);
		}
	}, null);
	$("#qtTaskAbnormal_code").val("");
	$("#qtTaskAbnormal_Code").val("");
}

//下一步流向
function loadTestNextFlowCob(){
	var records1 = qtTaskAbnormalGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=QtTask&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qtTaskAbnormalGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}