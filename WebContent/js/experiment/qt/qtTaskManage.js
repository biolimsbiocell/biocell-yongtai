var qtTaskManageGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'volume',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'result',
		type:"string"
	});
	fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
    fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name:'classify',
		type:"string"
	});
	fields.push({
		name:'dicType-id',
		type:"string"
	});
    fields.push({
		name:'dicType-name',
		type:"string"
	});
	
	fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
		name:'labCode',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.labCode,
		width:20*6
	});
	
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:30*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		header:biolims.common.phone,
//		hidden:true,
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dicType-id',
		hidden:true,
		header:biolims.common.dicSampleTypeId,
		width:15*10,
		sortable:true
	});
	var testDicType2 =new Ext.form.TextField({
        allowBlank: false
	});
	testDicType2.on('focus', function() {
		loadTestDicType2();
	});
	cm.push({
		dataIndex:'dicType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		sortable:true,
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:30*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden:false,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'volume',
		header:biolims.common.volume,
		hidden:true,
		width:20*6
	});

	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		sortable:true,		
	});
	var storeresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified], [ '0', biolims.common.disqualified ]]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:biolims.common.result,
		width:20*6,		
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*6,		
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTaskManage/showQtTaskManageListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.QPCR.testResultManage;
	opts.height=document.body.clientHeight-200;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	qtTaskManageGrid=gridEditTable("show_qtTaskManage_div",cols,loadParam,opts);
	$("#show_qtTaskManage_div").data("qtTaskManageGrid", qtTaskManageGrid);

});

//保存
function save(){	
	var itemJson = commonGetModifyRecords(qtTaskManageGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/qt/qtTaskManage/saveQPCR实验Manage.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {			
					qtTaskManageGrid.getStore().commitChanges();
					qtTaskManageGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
}

function selectQtTaskInfo(){
	commonSearchAction(qtTaskManageGrid);
	$("#qtTaskManage_code").val("");
	$("#qtTaskManage_Code").val("");
}

//下一步流向
function loadTestNextFlowCob(){
	var records1 = qtTaskManageGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=QtTask&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qtTaskManageGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}