﻿var qtTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'chromosomalLocation',
		type:"string"
	});
	fields.push({
	    name:'patientName',
	    type:"string"
	});
	fields.push({
	    name:'genotype',
	    type:"string"
	});
	fields.push({
		name:'fragmentLength',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
    fields.push({
	    name:'location',
	    type:"string"
	});
    fields.push({
		name:'qbcontraction',
		type:"string"
	});	   
	fields.push({
		name:'volume',
		type:"string"
	});
	fields.push({
		name:'unit',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'projectId',
		type:"string"
	});
	fields.push({
		name:'contractId',
		type:"string"
	});
	fields.push({
		name:'orderType',
		type:"string"
	});
	fields.push({
		name:'taskId',
		type:"string"
	});
    fields.push({
		name:'submit',
		type:"string"
	});
	fields.push({
		name:'qtTask-id',
		type:"string"
	});
	fields.push({
		name:'qtTask-name',
		type:"string"
	});
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'isToProject',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'advice',
		type:"string"
	});
    
    fields.push({
		name:'eighteens',
		type:"string"
	});
    fields.push({
		name:'facility',
		type:"string"
	}); 
    fields.push({
		name:'reaction',
		type:"string"
	});
    fields.push({
		name:'condition',
		type:"string"
	}); 
    fields.push({
		name:'ct',
		type:"string"
	});
    fields.push({
		name:'fold',
		type:"string"
	});
    fields.push({
		name:'unitGroup-id',
		type:"string"
	});
	fields.push({
		name:'unitGroup-name',
		type:"string"
	});
    
    fields.push({
		name:'od260',
		type:"string"
	});
    fields.push({
		name:'od280',
		type:"string"
	});
    fields.push({
		name:'rin',
		type:"string"
	});
    fields.push({
		name:'contraction',
		type:"string"
	});
    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
	    name:'classify',
	    type:"string"
	});
    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'dataBits',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    fields.push({
		name:'techJkServiceTask-id',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-name',
		type:"string"
	});
  fields.push({
		name:'rowCode',
		type:"string"
	});
	fields.push({
		name:'colCode',
		type:"string"
	});
fields.push({
		name:'counts',
		type:"string"
	});
/************新增字段**************/
	fields.push({
		name : 'referenceGene',
		type : "string"
	});
	fields.push({
		name : 'cnPredicted',
		type : "string"
	});
	fields.push({
		name : 'confidence',
		type : "string"
	});
	fields.push({
		name : 'replicateCount',
		type : "string"
	});
	fields.push({
		name : 'famCtMean',
		type : "string"
	});
	fields.push({
		name : 'vicCtMean',
		type : "string"
	});
	fields.push({
		name : 'zScore',
		type : "string"
	});
	fields.push({
		name : 'batchNumber',
		type : "string"
	});
	fields.push({
		name:'barCode',
		type:"string"
	});
	fields.push({
		name:'dataTraffic',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex : 'referenceGene',
		hidden : true,
		header : '参考基因',
		sortable : true,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'cnPredicted',
		hidden : true,
		header : '预测拷贝数',
		sortable : true,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'confidence',
		hidden : true,
		header : '可信度',
		sortable : true,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'zScore',
		hidden : true,
		header : 'Z-Score',
		sortable : true,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'replicateCount',
		hidden : true,
		header : '平行重复',
		sortable : true,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'famCtMean',
		hidden : false,
		header : 'FAM Ct Mean',
		sortable : true,
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'vicCtMean',
		hidden : false,
		header : 'VIC Ct Mean',
		sortable : true,
		width : 20 * 6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'batchNumber',
		hidden : true,
		header : 'Batch number',
		sortable : true,
		width : 20 * 6
	});
	
	var storegenotypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ 'AA', 'AA' ], [ 'AC', 'AC' ], [ 'AG', 'AG' ],[ 'CC', 'CC' ],['GG','GG'] ]
	});
	var genotypeCob = new Ext.form.ComboBox({
		store : storegenotypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'genotype',
		hidden : false,
		header:biolims.QPCR.genotype,
		width:20*6,
		editor : genotypeCob,
		renderer : Ext.util.Format.comboRenderer(genotypeCob)
	});
/*	var unitstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {
			name : 'id'
		}, {
			name : 'geneName'
		} ],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx
					+ '/sample/dic/dicPriLibTypeCobJson.action',
			method : 'POST'
		})
	});
	unitstore.load();
	var unitcob = new Ext.form.ComboBox({
		store : unitstore,
		displayField : 'geneName',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'genotype',
		header : biolims.QPCR.genotype,
		width : 100,
		renderer : Ext.util.Format.comboRenderer(unitcob),
		editor : unitcob,
		hidden:false
	});*/
	cm.push({
		dataIndex:'chromosomalLocation',
		hidden : false,
		header:biolims.sanger.chromosomalLocation,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'qtTaskCode',
		hidden : true,
		header:biolims.QPCR.testCode,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'fragmentLength',
		hidden : true,
		header:biolims.pooling.pdLength,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		header:biolims.common.phone,
//		hidden:true,
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.testProject,
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden:true,
		header:biolims.common.dicSampleTypeId,
		width:15*10,
		sortable:true
	});
	var testDicType2 =new Ext.form.TextField({
        allowBlank: false
	});
	testDicType2.on('focus', function() {
		loadTestDicType2();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		editor : testDicType2,
		hidden : true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积(μL)',
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'contraction',
		hidden : true,
		header:'浓度(ng/ul)',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:'总量',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:"单位组ID",
		width:15*10,
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: "单位组",
		width:15*10,
		hidden : true
//		sortable:true,
//		editor : testUnitGroup
	});

	cm.push({
		dataIndex:'eighteens',
		hidden : true,
		header:'28s:18s',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'facility',
		hidden : true,
		header:'实验设备',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'reaction',
		hidden : true,
		header:'RT-PCR反应体系',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'condition',
		hidden : true,
		header:'RT-PCR反应条件',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'ct',
		hidden : true,
		header:'Ct值',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'fold',
		hidden : true,
		header:'Fold change',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	
	
	cm.push({
		dataIndex:'od280',
		hidden : true,
		header:'OD260/280',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'od260',
		hidden : true,
		header:'OD260/230',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'qbcontraction',
		hidden : true,
		header:biolims.common.qbConcentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'rin',
		hidden : true,
		header:'RIN',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.disqualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		editor : nextFlowCob,
		hidden : false
	});
	var storePutCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0',biolims.common.no ] ]
	});
	var putCob = new Ext.form.ComboBox({
		store : storePutCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : true,
		header:biolims.common.toSubmit,
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(putCob)
	});
	var storeDataBits = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.dna.germline ], [ '0', biolims.dna.variation ] ]
	});
	var DataBits = new Ext.form.ComboBox({
		store : storeDataBits,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'dataBits',
		hidden : true,
		header:biolims.dna.dataBits,
		width:20*6,
		editor : DataBits,
		renderer : Ext.util.Format.comboRenderer(DataBits)
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:40*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:30*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:30*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:biolims.common.counts,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'qtTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:biolims.common.classify,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:biolims.common.sampleInfoNote,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskInfoListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.QPCR.testResult;
	opts.height =  document.body.clientHeight*0.8;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qt/qtTask/delQtTaskInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qtTaskResultGrid.getStore().commitChanges();
				qtTaskResultGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.productType,
		handler : loadTestDicType2
	});
	opts.tbar.push({
		text :biolims.common.batchData,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_infos_div"), biolims.common.batchData, null, {
				"Confirm" : function() {
					var records = qtTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var od230 = $("#od230").val();
						var od260 = $("#od260").val();
						var contraction = $("#contraction").val();
						var volume = $("#volume").val();
						var rin = $("#rin").val();
						var qbcontraction = $("#qbcontraction").val();
						qtTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("od260", od230);
							obj.set("od280", od260);
							obj.set("contraction", contraction);
							obj.set("volume", volume);
							obj.set("rin", rin);
							obj.set("qbcontraction", qbcontraction);
						});
						qtTaskResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
		
	 opts.tbar.push({
			text : biolims.common.batchimport,
			handler : function() {				
				$(".jquery-ui-warning").html(biolims.common.pleasePaste);
				$("#many_bat_text").val("");
				$("#many_bat_text").attr("style", "width:465px;height: 339px");
				var options = {};
				options.width = 494;
				options.height = 508;
				loadDialogPage($("#many_bat_div"), biolims.common.batchimport, null, {
					"Confirm" : function() {
						var positions = $("#many_bat_text").val();
						//alert(positions);
						if (!positions) {
							message(biolims.common.pleaseFillInfo);
							return;
						}
						var posiObj = {};
						var posiObj1 = {};
						var posiObj2 = {};
						var posiObj3 = {};
						var posiObj4 = {};
						var posiObj5 = {};
						var posiObj6 = {};
						var array = formatData(positions.split("\n"));
						$.each(array, function(i, obj) {
							var tem = obj.split("\t");
							posiObj[tem[0]] = tem[1];
							posiObj1[tem[0]] = tem[2];
							posiObj2[tem[0]] = tem[3];
							posiObj3[tem[0]] = tem[4];
							posiObj4[tem[0]] = tem[5];
							posiObj5[tem[0]] = tem[6];
							posiObj6[tem[0]] = tem[7];
						});
						var records = qtTaskResultGrid.getAllRecord();
						qtTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							if (posiObj[obj.get("code")]) {
								obj.set("result", posiObj[obj.get("code")]);
							}
							if (posiObj1[obj.get("code")]) {
								obj.set("volume", posiObj1[obj.get("code")]);
							}
							if (posiObj2[obj.get("code")]) {
								obj.set("contraction", posiObj2[obj.get("code")]);
							}
							if (posiObj3[obj.get("code")]) {
								obj.set("od260", posiObj3[obj.get("code")]);
							}
							if (posiObj4[obj.get("code")]) {
								obj.set("od280", posiObj4[obj.get("code")]);
							}
							if (posiObj5[obj.get("code")]) {
								obj.set("qbcontraction", posiObj5[obj.get("code")]);
							}
							if (posiObj6[obj.get("code")]) {
								obj.set("rin", posiObj6[obj.get("code")]);
							}																
						});
						qtTaskResultGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}
		});			 	 
	 opts.tbar.push({
			text : biolims.common.batchResult,
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_result_div"), biolims.common.batchResult, null, {
					"Confirm" : function() {
						var records = qtTaskResultGrid.getSelectRecord();
						if (records && records.length > 0) {
							var result = $("#result").val();
							qtTaskResultGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("result", result);
							});
							qtTaskResultGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : loadTestNextFlowCob
	});

	opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexcel
	});
	
	if($("#qtTask_id").val()&&$("#qtTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submit
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.downloadCsvTemplet,
		handler : function() {
			var selected = qtTaskResultGrid.getSelectionModel().getSelections();
			var ids = [];
			var code = [];
			if (selected.length > 0) {
				$.each(selected, function(i, obj) {
					ids.push(obj.get("id"));
					code.push(obj.get("code"));
				});
			}
			var countItem = qtTaskResultGrid.store.getCount();
			if (countItem == 0) {
				message(biolims.common.resultsDownload);
				return;
			}
			if ("" == code) {
				message(biolims.common.saveDownload);
				return;
			}
			window.open(window.ctx + '/experiment/qt/qtTask/downLoadTemp.action?code='+code+'&ids='+ids);
		}
	});
	opts.tbar.push({
		text :biolims.common.uploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_svnuploadcsv_div"), biolims.common.batchUpload, null, {
				"确定" : function() {
					goInExcelcsvSvn();
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	function goInExcelcsvSvn() {
		var file = document.getElementById("file-svnuploadcsv1").files[0];
		var n = 0;
		var ob = qtTaskResultGrid.getStore().recordType;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_datas = $.simple_csv(this.result);
			$(csv_datas)
					.each(
							function() {
								if (n > 0) {
									if (this[0]) {
										var p = new ob({});
										p.isNew = true;
//										p.set("code", this[0]);// 
//										p.set("sampleCode", this[1]);// 
//										p.set("barCode", this[2]);// 
//										p.set("famCtMean", this[3]);// 
//										p.set("vicCtMean", this[4]);//
//										p.set("genotype", this[5]);// 
//										p.set("chromosomalLocation", this[6]);
//										p.set("result", this[7]);// 
//										p.set("nextFlow", this[8]);// 
//										p.set("note", this[9]);// 
										ajax("post","/experiment/qt/qtTask/updateForUpload.action",
												{
											code : this[0],
											famCtMean : this[3],
											vicCtMean : this[4],
											genotype : this[5],
											chromosomalLocation : this[6],
											result : this[7],
												},
												function(data) {
													if (data.success) {
														message(biolims.common.uploadSuccess);
													}else {
														message(biolims.common.uploadFailed+"<a style ='color:red'>"+biolims.common.pleaseFillInfo+"</a>");
													}
												}, null);
									}
								}
								n = n + 1;

							});
			qtTaskResultGrid.getStore().reload();
		};
	}
	}
	qtTaskResultGrid=gridEditTable("qtTaskResultdiv",cols,loadParam,opts);
	$("#qtTaskResultdiv").data("qtTaskResultGrid", qtTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function exportexcel() {
	qtTaskResultGrid.title = biolims.common.exportList;
	var vExportContent = qtTaskResultGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

var loadDicType2;	
//查询样本类型
function loadTestDicType2(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDicType2=loadDialogPage(null, biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action?a=2", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qtTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("dicSampleType-id", b.get("id"));
							obj.set("dicSampleType-name", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setDicType2(){
	var operGrid = $("#show_dialog_dicType_div").data("dicTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qtTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$.each(records, function(a, b) {
				b.set("dicType-id", obj.get("id"));
				b.set("dicType-name", obj.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadDicType2.dialog("close");

}

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = qtTaskResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=QtTask&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qtTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qtTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}	
	
//提交样本
function submit(){
	var id=$("#qtTask_id").val();  
	if(qtTaskResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var record = qtTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("nextFlowId")==""){
			message(biolims.common.notFillNextStep);
			return;
		}
	}
	var grid=qtTaskResultGrid.store;
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();				
		var records = [];						
		for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
		}		
		ajax("post", "/experiment/qt/qtTask/submit.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				qtTaskResultGrid.getStore().commitChanges();
				qtTaskResultGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);
	}else{
		message(biolims.common.noData2Submit);
	}
}
