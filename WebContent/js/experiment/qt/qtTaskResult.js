﻿var qtTaskResultGrid1;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'volume',
		type:"string"
	});
	fields.push({
		name:'unit',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'qtTask-id',
		type:"string"
	});
	fields.push({
		name:'qtTask-name',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'od260',
		type:"string"
	});
    fields.push({
		name:'od280',
		type:"string"
	});
    fields.push({
		name:'rin',
		type:"string"
	});
    fields.push({
		name:'contraction',
		type:"string"
	});
    fields.push({
		name:'endDate',
		type:"string"
	});
    fields.push({
		name:'taskId',
		type:"string"
	});
    fields.push({
		name:'dicType-id',
		type:"string"
	});
    fields.push({
		name:'dicType-name',
		type:"string"
	});  
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.QPCR.testCode,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dicType-id',
		hidden:true,
		header:biolims.common.sampleTypeId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'dicType-name',
		header:biolims.common.sampleTypeName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'od260',
		hidden : false,
		header:'OD260/230',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od280',
		hidden : false,
		header:'OD260/280',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contraction',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rin',
		hidden : false,
		header:'RIN',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.qualified], [ '1', biolims.common.disqualified] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.plasma.buildLib], [ '1', biolims.QPCR.testRepeat ],[ '2', biolims.plasma.putInLib ],[ '3', biolims.plasma.feedBack2Group ],[ '4', biolims.plasma.end ],['5',biolims.QPCR.testDecetion] ]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:biolims.common.nextFlow,
		width:20*6,
		editor : nextFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.plasma.buildLib ], [ '1', biolims.QPCR.testRepeat],[ '2', biolims.plasma.putInLib ],[ '3', biolims.plasma.end ] ]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:biolims.common.method,
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cm.push({
		dataIndex:'endDate',
		hidden : false,
		header:biolims.common.endDate,
		width:15*10
	});
	cm.push({
		dataIndex:'taskId',
		hidden : false,
		header:biolims.common.taskId,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.QPCR.testResult1;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qt/qtTask/delQtTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};    		
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	if($("#qtTask_id").val()&&$("#qtTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	}
	qtTaskResultGrid1=gridEditTable("qtTaskResultdiv1",cols,loadParam,opts);
	$("#qtTaskResultdiv1").data("qtTaskResultGrid1", qtTaskResultGrid1);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});