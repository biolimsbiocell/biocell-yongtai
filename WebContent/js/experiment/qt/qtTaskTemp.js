﻿var qtTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name:'sampleName',
		type:"string"
	});
    fields.push({
		name:'sampleId',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'reason',
		type:"string"
    });
    fields.push({
		name:'note',
		type:"string"
    });
	fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
	    name : 'classify',
	    type : "string"
    });
	fields.push({
		name : 'sampleType',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	fields.push({
		name:'labCode',
		type:"string"
	});
	fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
	fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
	   fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-sequenceBillDate',
			type:"string"
		});
	  fields.push({
			name:'barCode',
			type:"string"
		});
	  fields.push({
		  name:'dataTraffic',
		  type:"string"
	  });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		sortable:true,
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleId',
		hidden : true,
		header:biolims.common.sampleId,
		width:30*6
	});
	cm.push({
		dataIndex:'Name',
		hidden : true,
		header:biolims.common.sampleName,
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:biolims.common.storageLocalName,
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:biolims.common.orderId,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:biolims.common.sampleInfoNote,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : false,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequenceBillDate',
		hidden : true,
		header:"截止日期1",
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	if($("#id").val()=="1"){
		loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskTempJson.action?id="+$("#id").val();
	}else{
		loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskTempJson.action";
	}
	loadParam.limit=10000;
	var opts={};
	opts.title=biolims.dna.forExperimental;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];       
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		text :biolims.master.clinicalSample,
		handler : function clinicalSample(){
			load("/experiment/qt/qtTask/showQtTaskTempList.action", 
					{id : "1"}, "#qtTaskTempPage");
		}
	});
	opts.tbar.push({
		text : biolims.common.sampleWindow,
		handler : changeWidth
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$(".jquery-ui-warning").html(biolims.common.pleaseInputSampleNum);
			$("#many_batTemp_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_batTemp_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_batTemp_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = qtTaskTempGrid.getAllRecord();
							var store = qtTaskTempGrid.store;
							var isOper = true;
							var buf = [];
							qtTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {																
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));										
									}									
								});
							});
							qtTaskTempGrid.getSelectionModel().selectRows(buf);																																			
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}							
							if(isOper==false){								
								//message("样本号核对不符，请检查！");								
							}else{								
								//message("样本号核对完毕！");
								addItem();
							}
							qtTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);
		}
	});
	
	
	opts.tbar.push({
		text : biolims.common.scienceService,
		handler : techDNAService
	});
	opts.tbar.push({
		text : biolims.common.retrieve,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_js_div"), biolims.common.retrieve, null, {
				"Confirm": function() {
					commonSearchAction(qtTaskTempGrid);
					$(this).dialog("close");
				}
			}, true, options);
			
		}
	});
	opts.tbar.push({
		text : biolims.wk.addQuality,
		handler : addQuality
	});
	
	qtTaskTempGrid=gridEditTable("qtTaskTempdiv",cols,loadParam,opts);
	$("#qtTaskTempdiv").data("qtTaskTempGrid", qtTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});



//添加质控品
function addQuality(){
	var options = {};
	options.width = 900;
	options.height = 460;
		var url="/system/quality/qualityProduct/showSelectQualityList.action";
		loadDialogPage(null, biolims.common.selectedDetail, url, {
			"Confirm": function() {
				var getQuality = selectQualityGrid.getSelectionModel().getSelections();
				var count=1;
				if(getQuality.length > 0){
					$.each(getQuality, function(i, obj) {
						if(obj.get("num")==""||obj.get("num")==0){
							message(biolims.common.pleaseInputNum);
						}else{
							for(var i=0; i<obj.get("num");i++){
								var ob = qtTaskTempGrid.getStore().recordType;
								qtTaskTempGrid.stopEditing();
								var p = new ob({});
								p.isNew = true;
								p.set("code",obj.get("id")+count);
								p.set("name",obj.get("name"));
								p.set("state",'1');
								p.set("isZkp",'1');
								qtTaskTempGrid.getStore().add(p);
								count++;
							}
						}
					});
					
					qtTaskTempGrid.startEditing(0, 0);
					
				}else{
					message(biolims.common.pleaseSelectQuality);
				}
				
				options.close();
			}
		}, true, options);
}
function addItem(){
	var isRepeat = true;
	var records = qtTaskTempGrid.getSelectRecord();
	var selectRecord=qtTaskTempGrid.getSelectionModel();
	var selRecord=qtTaskItemGrid.store;
	//获取页面主表的检测项目
	var productId=$('#qtTask_productId').val();
	var max=0;
	var count = 1;
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(Number(a)>Number(max)){
			max=a;
		}
	}
	
	if(selectRecord.getSelections().length > 0){
		for(var z=0;z<selectRecord.getSelections().length;z++){
			var productName=records[z].get("productName");
			var productId=records[z].get("productId");
			if(productId==undefined){
				var obj = selectRecord.getSelections()[z];
				
				var ob = qtTaskItemGrid.getStore().recordType;
				qtTaskItemGrid.stopEditing();
				//$.each(data.data, function(k, obj1) {
				var p = new ob({});
				p.isNew = true;
				
				p.set("tempId",obj.get("id"));
				p.set("code",obj.get("code"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("barCode",obj.get("barCode"));
				p.set("dataTraffic",obj.get("dataTraffic"));
				p.set("sampleName",obj.get("sampleName"));
				p.set("patientName",obj.get("patientName"));
				p.set("productId",obj.get("productId"));
				p.set("productName",obj.get("productName"));
				p.set("sequenceFun",obj.get("sequenceFun"));
				p.set("phone",obj.get("phone"));
				p.set("idCard",obj.get("idCard"));
				p.set("inspectDate",obj.get("inspectDate"));
				p.set("orderId",obj.get("orderId"));
				p.set("reportDate",obj.get("reportDate"));
				p.set("location",obj.get("binLocation"));
				p.set("classify",obj.get("classify"));
				p.set("sampleType",obj.get("sampleType"));
				p.set("sampleNum",obj.get("sampleNum"));
				p.set("state","1");
				p.set("num","1");
				p.set("labCode",obj.get("labCode"));
				p.set("orderNumber",Number(max)+count);
				
				p.set("chromosomalLocation",obj.get("plCode"));
				p.set("primerNumber",obj.get("ywCode"));
				
				p.set("groupId",obj.get("groupId"));
				p.set("userId",obj.get("userId"));
				//p.set("leftPrimer",obj1.leftPrimer);
				//p.set("rightPrimer",obj1.rightPrimer);
				//p.set("ampliconid",obj1.ampliconid);
				
				qtTaskItemGrid.getStore().add(p);							
				qtTaskItemGrid.startEditing(0, 0);
				count++;
			}else{
				ajax("post","/experiment/qt/qtTask/selectMappingPrimersLibrary.action",{
					productId : productId
				}, function(data) {
					if (data.success) {
						$.each(data.data, function(j,b) {
							var obj = selectRecord.getSelections()[z];
							
							var ob = qtTaskItemGrid.getStore().recordType;
							qtTaskItemGrid.stopEditing();
							//$.each(data.data, function(k, obj1) {
							var p = new ob({});
							p.isNew = true;
							
							p.set("tempId",obj.get("id"));
							p.set("code",obj.get("code"));
							p.set("sampleCode",obj.get("sampleCode"));
							p.set("barCode",obj.get("barCode"));
							p.set("dataTraffic",obj.get("dataTraffic"));
							p.set("sampleName",obj.get("sampleName"));
							p.set("patientName",obj.get("patientName"));
							p.set("productId",obj.get("productId"));
							p.set("productName",obj.get("productName"));
							p.set("sequenceFun",obj.get("sequenceFun"));
							p.set("phone",obj.get("phone"));
							p.set("idCard",obj.get("idCard"));
							p.set("inspectDate",obj.get("inspectDate"));
							p.set("orderId",obj.get("orderId"));
							p.set("reportDate",obj.get("reportDate"));
							p.set("location",obj.get("binLocation"));
							p.set("classify",obj.get("classify"));
							p.set("sampleType",obj.get("sampleType"));
							p.set("sampleNum",obj.get("sampleNum"));
							p.set("state","1");
							p.set("num","1");
							p.set("labCode",obj.get("labCode"));
							p.set("orderNumber",Number(max)+count);
							
							p.set("chromosomalLocation",b.chromosomalLocation);
							p.set("primerNumber",b.plId);
							
							p.set("groupId",obj.get("groupId"));
							p.set("userId",obj.get("userId"));
							//p.set("leftPrimer",obj1.leftPrimer);
							//p.set("rightPrimer",obj1.rightPrimer);
							//p.set("ampliconid",obj1.ampliconid);
							
							qtTaskItemGrid.getStore().add(p);							
							qtTaskItemGrid.startEditing(0, 0);
							count++;
						});
					}else{
						var obj = selectRecord.getSelections()[z];
						
						var ob = qtTaskItemGrid.getStore().recordType;
						qtTaskItemGrid.stopEditing();
						//$.each(data.data, function(k, obj1) {
						var p = new ob({});
						p.isNew = true;
						
						p.set("tempId",obj.get("id"));
						p.set("code",obj.get("code"));
						p.set("sampleCode",obj.get("sampleCode"));
						p.set("barCode",obj.get("barCode"));
						p.set("dataTraffic",obj.get("dataTraffic"));
						p.set("sampleName",obj.get("sampleName"));
						p.set("patientName",obj.get("patientName"));
						p.set("productId",obj.get("productId"));
						p.set("productName",obj.get("productName"));
						p.set("sequenceFun",obj.get("sequenceFun"));
						p.set("phone",obj.get("phone"));
						p.set("idCard",obj.get("idCard"));
						p.set("inspectDate",obj.get("inspectDate"));
						p.set("orderId",obj.get("orderId"));
						p.set("reportDate",obj.get("reportDate"));
						p.set("location",obj.get("binLocation"));
						p.set("classify",obj.get("classify"));
						p.set("sampleType",obj.get("sampleType"));
						p.set("sampleNum",obj.get("sampleNum"));
						p.set("state","1");
						p.set("num","1");
						p.set("labCode",obj.get("labCode"));
						p.set("orderNumber",Number(max)+count);
						
						p.set("chromosomalLocation",obj.get("plCode"));
						p.set("primerNumber",obj.get("ywCode"));
						
						p.set("groupId",obj.get("groupId"));
						p.set("userId",obj.get("userId"));
						//p.set("leftPrimer",obj1.leftPrimer);
						//p.set("rightPrimer",obj1.rightPrimer);
						//p.set("ampliconid",obj1.ampliconid);
						
						qtTaskItemGrid.getStore().add(p);							
						qtTaskItemGrid.startEditing(0, 0);
						count++;
					}
				});
			}
		
		}
	}
}

////点击
//function addItem(){
//	
//
//	var isRepeat = true;
//	var selectRecord=qtTaskTempGrid.getSelectionModel();
//	var selRecord=qtTaskItemGrid.store;
//	var productId=$('#qtTask_productId').val();
//	var count = 1;
//	var max=0;
//	for(var i=0; i<selRecord.getCount();i++){
//		var a=selRecord.getAt(i).get("orderNumber");
//		if(a>max){
//			max=a;
//		}
//	}
//	if(selectRecord.getSelections().length > 0){
//		if(productId==""){
//			message(biolims.common.pleaseSelectProduct);
//		}else{
//			ajax("post","/experiment/qt/qtTask/selectMappingPrimersLibrary.action",{
//				productId : productId,
//			}, function(data) {
//				if (data.success) {
//					if(selRecord.getCount()>0){
//						$.each(selectRecord.getSelections(), function(i, obj) {
//							for(var j=0;j<selRecord.getCount();j++){
//								var oldv = selRecord.getAt(j).get("tempId");
//								if(oldv == obj.get("id")){
//									isRepeat = false;
//									message(biolims.common.haveDuplicate);
//									return;					
//								}
//							}
//						});
//					}
//					
//					if(isRepeat){
//						$.each(data.data, function(j,b) {
//									$.each(selectRecord.getSelections(), function(k, obj) {
//									var ob = qtTaskItemGrid.getStore().recordType;
//									qtTaskItemGrid.stopEditing();
//									var p = new ob({});
//									p.isNew = true;
//									
//									p.set("tempId",obj.get("id"));
//									p.set("code",obj.get("code"));
//									p.set("sampleCode",obj.get("sampleCode"));
//									p.set("chromosomalLocation",b.chromosomalLocation);
//									p.set("primerNumber",b.plId);
//									p.set("leftPrimer",b.leftPrimer);
//									p.set("rightPrimer",b.rightPrimer);
//									p.set("ampliconid",b.ampliconid);
//									
//									p.set("barCode",obj.get("barCode"));
//									p.set("sampleName",obj.get("sampleName"));
//									p.set("patientName",obj.get("patientName"));
//									p.set("productId",obj.get("productId"));
//									p.set("productName",obj.get("productName"));
//									p.set("sequenceFun",obj.get("sequenceFun"));
//									p.set("phone",obj.get("phone"));
//									p.set("idCard",obj.get("idCard"));
//									p.set("inspectDate",obj.get("inspectDate"));
//									p.set("orderId",obj.get("orderId"));
//									p.set("reportDate",obj.get("reportDate"));
//									p.set("location",obj.get("binLocation"));
//									p.set("classify",obj.get("classify"));
//									p.set("sampleType",obj.get("sampleType"));
//									p.set("sampleNum",obj.get("sampleNum"));
//									p.set("state","1");
//									p.set("num","1");
//									p.set("labCode",obj.get("labCode"));
//									p.set("orderNumber",Number(max)+count);
//									qtTaskItemGrid.getStore().add(p);
//									qtTaskItemGrid.startEditing(0, 0);
//									});
//						});
//					}
//				}
//			});
//		}
//	}else{
//		message(biolims.common.pleaseChooseSamples);
//	}	
//
//	
//}

//点击
function addItem1(){
	var isRepeat = true;
	var selectRecord=qtTaskTempGrid.getSelectionModel();
	var selRecord=qtTaskItemGrid.store;
	var productId=$('#qtTask_productId').val();
	var count = 1;
	var max=0;
	// 没经过这
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			//alert(10);
			max=a;
			//alert(11);
		}
	}
	if(selectRecord.getSelections().length > 0){
		if(productId==""){
			message(biolims.common.pleaseSelectProduct);
		}else{
			ajax("post","/experiment/qt/qtTask/selectMappingPrimersLibrary.action",{
				productId : productId,
			}, function(data) {
				if (data.success) {
					// 没经过这
					if(selRecord.getCount()>0){
						$.each(selectRecord.getSelections(), function(i, obj) {
							for(var j=0;j<selRecord.getCount();j++){
								var oldv = selRecord.getAt(j).get("tempId");
								if(oldv == obj.get("id")){
									isRepeat = false;
									message(biolims.common.haveDuplicate);
									return;	
								}
							}
						});
						if(isRepeat){
							$.each(data.data, function(j,b) {
								$.each(selectRecord.getSelections(), function(k, obj) {
								var ob = qtTaskItemGrid.getStore().recordType;
								qtTaskItemGrid.stopEditing();
								var p = new ob({});
								p.isNew = true;		
								p.set("orderNumber",Number(max)+count);
								p.set("sampleName",obj.get("sampleName"));
								p.set("barCode",obj.get("barCode"));
								p.set("dataTraffic",obj.get("dataTraffic"));
								p.set("patientName",obj.get("patientName"));
								p.set("productId",obj.get("productId"));
								p.set("productName",obj.get("productName"));
								p.set("sequenceFun",obj.get("sequenceFun"));
								p.set("phone",obj.get("phone"));
								p.set("idCard",obj.get("idCard"));
								p.set("inspectDate",obj.get("inspectDate"));
								p.set("orderId",obj.get("orderId"));
								p.set("reportDate",obj.get("reportDate"));
								p.set("location",obj.get("binLocation"));
								p.set("classify",obj.get("classify"));
								p.set("sampleType",obj.get("sampleType"));
								p.set("sampleNum",obj.get("sampleNum"));
								p.set("state","1");
								p.set("labCode",obj.get("labCode"));								
								p.set("tempId",obj.get("id"));
								p.set("code",obj.get("code"));
								p.set("sampleCode",obj.get("sampleCode"));
								p.set("chromosomalLocation",b.chromosomalLocation);
								p.set("primerNumber",b.plId);
								p.set("leftPrimer",b.leftPrimer);
								p.set("rightPrimer",b.rightPrimer);
								p.set("ampliconid",b.ampliconid);
								qtTaskItemGrid.getStore().add(p);
								qtTaskItemGrid.startEditing(0, 0);
								});
							});
						}
					}else{
						$.each(data.data, function(j,b) {
							$.each(selectRecord.getSelections(), function(k, obj) {
							var ob = qtTaskItemGrid.getStore().recordType;
							qtTaskItemGrid.stopEditing();
							var p = new ob({});
							p.isNew = true;	
							p.set("orderNumber",Number(max)+count);
							p.set("sampleName",obj.get("sampleName"));
							p.set("patientName",obj.get("patientName"));
							p.set("productId",obj.get("productId"));
							p.set("productName",obj.get("productName"));
							p.set("sequenceFun",obj.get("sequenceFun"));
							p.set("phone",obj.get("phone"));
							p.set("idCard",obj.get("idCard"));
							p.set("inspectDate",obj.get("inspectDate"));
							p.set("orderId",obj.get("orderId"));
							p.set("reportDate",obj.get("reportDate"));
							p.set("location",obj.get("binLocation"));
							p.set("classify",obj.get("classify"));
							p.set("sampleType",obj.get("sampleType"));
							p.set("sampleNum",obj.get("sampleNum"));
							p.set("state","1");
							p.set("labCode",obj.get("labCode"));								
							p.set("tempId",obj.get("id"));
							p.set("code",obj.get("code"));
							p.set("sampleCode",obj.get("sampleCode"));
							p.set("chromosomalLocation",b.chromosomalLocation);
							p.set("primerNumber",b.plId);
							p.set("leftPrimer",b.leftPrimer);
							p.set("rightPrimer",b.rightPrimer);
							p.set("ampliconid",b.ampliconid);
							qtTaskItemGrid.getStore().add(p);
							qtTaskItemGrid.startEditing(0, 0);
							});
						});
					}
					
				}
			});
		}
	}else{
		message(biolims.common.pleaseChooseSamples);
	}	
}

//科技服务
function techDNAService(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
		loadDialogPage(null, biolims.common.scienceServiceTest, "/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogList.action", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				//var records = bloodSplitLeftPagedivGrid.store;
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						var id1=obj.get("id");
						$("#techJkServiceTask_id").val(id1);
						commonSearchAction(qtTaskTempGrid);
						ajax("post", "/experiment/qt/qtTask/selTechJkServicTaskByid.action", {
						id : id1
						}, function(data) {
							if(data.success){
//								alert(data.list);
								$.each(data.list, function(a,b) {
									var ob = qtTaskItemGrid.getStore().recordType;
									qtTaskItemGrid.stopEditing();
									var p= new ob({});
									p.set("tempId",b.id);
									p.set("code",b.code);
									p.set("sampleCode",b.sampleCode);
									p.set("chromosomalLocation",b.chromosomalLocation);
									p.set("primerNumber",b.primerNumber);
									p.set("leftPrimer",b.leftPrimer);
									p.set("rightPrimer",b.rightPrimer);
									p.set("ampliconid",b.ampliconid);
									p.set("sampleName",b.sampleName);
									p.set("patientName",b.patientName);
									p.set("productId",b.productId);
									p.set("productName",b.productName);
									p.set("sequenceFun",b.sequenceFun);
									p.set("phone",b.phone);
									p.set("idCard",b.idCard);
									p.set("inspectDate",b.inspectDate);
									p.set("orderId",b.orderId);
									p.set("reportDate",b.reportDate);
									p.set("location",b.binLocation);
									p.set("classify",b.classify);
									p.set("sampleType",b.sampleType);
									p.set("sampleNum",b.sampleNum);
									p.set("state","1");
									p.set("labCode",b.labCode);
									p.set("orderNumber",a+1);
									if(b.sampleInfo!=null){
										p.set("sampleInfo-id",b.sampleInfo.id);
										p.set("sampleInfo-note",b.sampleInfo.note);
//										p.set("sampleInfo-receiveDate",b.sampleInfo.receiveDate);
//										p.set("sampleInfo-reportDate",b.sampleInfo.reportDate);
									}
									if(b.techJkServiceTask!=null){
										p.set("techJkServiceTask-id",b.techJkServiceTask.id);
										p.set("techJkServiceTask-name",b.techJkServiceTask.name);
									}
									qtTaskItemGrid.getStore().add(p);
								});
							}
						});
					});
					
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function selectTemp(){
	if(window.location.href.indexOf("?")>0){
		location.href=window.location.href+"&method=window";
	}else{
		location.href=window.location.href+"?method=window";	
	}

}