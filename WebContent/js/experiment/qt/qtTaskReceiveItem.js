﻿var qtTaskReceiveItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
    fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'reportDate',
		type:"string"
	});
    fields.push({
		name:'inspectDate',
		type:"string"
	});
//    fields.push({
//		name:'phone',
//		type:"string"
//	});
//    fields.push({
//		name:'idCard',
//		type:"string"
//	});
    fields.push({
		name:'orderId',
		type:"string"
	});
    fields.push({
		name:'sampleName',
		type:"string"
	});
    fields.push({
		name:'sampleId',
		type:"string"
	});
    fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'stateName',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'reason',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'qtTaskReceive-id',
		type:"string"
	});
    fields.push({
		name:'qtTaskReceive-name',
		type:"string"
	});

	fields.push({
		name:'classify',
		type:"string"
	});
	fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
		name:'sampleNum',
		type:"string"
	});
	fields.push({
		name:'tempId',
		type:"string"
	});
	fields.push({
		name:'labCode',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:biolims.common.labCode,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:30*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6
	});
	var method = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			},{
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:biolims.common.method,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(method),editor: method
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:30*6
//	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:30*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:30*6
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:biolims.common.storageLocalName,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:30*6
	});	
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:30*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});	
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.reason,
		width:30*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTaskReceive-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTaskReceive-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTaskReceive/showQtTaskReceiveItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.common.sampleReceiveDetail;
	opts.height =  document.body.clientHeight-140;
	opts.tbar = [];
	if($("#qtTaskReceive_stateName").val()!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qt/qtTaskReceive/delQtTaskReceiveItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qtTaskReceiveItemGrid.getStore().commitChanges();
				qtTaskReceiveItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_batItem_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_batItem_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_batItem_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = qtTaskReceiveItemGrid.getAllRecord();
							var store = qtTaskReceiveItemGrid.store;
							var count = 0;
							var isOper = true;
							var buf = [];
							var buf1 = [];
							qtTaskReceiveItemGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										isOper==true;
									}else{
										
									}
									
								});
							});
							if(isOper){
								message("编码核对完成！");
							}
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}
							qtTaskReceiveItemGrid.getSelectionModel().selectRows(buf);
							qtTaskReceiveItemGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_method_div"), biolims.common.batchResult, null, {
				"Confirm" : function() {
					var records = qtTaskReceiveItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var method = $("#method").val();
						qtTaskReceiveItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("method", method);
						});
						qtTaskReceiveItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});	
	}
	qtTaskReceiveItemGrid=gridEditTable("qtTaskReceiveItemdiv",cols,loadParam,opts);
	$("#qtTaskReceiveItemdiv").data("qtTaskReceiveItemGrid", qtTaskReceiveItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});