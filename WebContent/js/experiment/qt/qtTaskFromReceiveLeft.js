﻿//QPCR实验左侧
var qtTaskFromReceiveLeftgrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'sampleName',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'testProject',
		type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'qtTaskReceive-id',
		type:"string"
	});
	fields.push({
	    name:'qtTaskReceive-name',
		type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.patientName,
		sortable:true,
		width:50*6
	});
	cm.push({
		dataIndex:'Name',
		hidden : false,
		header:biolims.common.sampleName,
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:biolims.common.storageLocalName,
		width:20*6
	});
	cm.push({
		dataIndex:'testProject',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6
	});
	cm.push({
		dataIndex:'qtTaskReceive-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*6
	});
	cm.push({
		dataIndex:'qtTaskReceive-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskFromReceiveListJson.action";
	var opts={};
	opts.title=biolims.common.sampleReceiveDetail;
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = qtTaskFromReceiveLeftgrid.getAllRecord();
							var store = qtTaskFromReceiveLeftgrid.store;
							var isOper = true;
							var buf = [];
							qtTaskFromReceiveLeftgrid.stopEditing();
							$.each(array,function(i, obj) {						
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));										
									}									
								})
							});
							qtTaskFromReceiveLeftgrid.getSelectionModel().selectRows(buf);							
							if(isOper==false){								
								message(biolims.common.samplecodeComparison);								
							}else{								
								addItem();
							}
							qtTaskFromReceiveLeftgrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);
		}
	});
	qtTaskFromReceiveLeftgrid=gridEditTable("qtTaskFromReceiveLeftdiv",cols,loadParam,opts);
	$("#qtTaskFromReceiveLeftdiv").data("qtTaskFromReceiveLeftgrid", qtTaskFromReceiveLeftgrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//从左边添加到右边的明细中
function addItem(){
	var selRecord = qtTaskFromReceiveLeftgrid.getSelectionModel().getSelections();//从左边获取数据
	var getRecord = qtTaskItemGrid.store;//填充到当前的明细中
	var count = 0;
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("Code");
				if(getData==obj.get("code")){
					message(biolims.common.haveDuplicate);
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
				var ob = qtTaskItemGrid.getStore().recordType;
				qtTaskItemGrid.stopEditing();
				var p= new ob({});				
				var str=obj.get("code");
				var s1=str.substring(0,1);
				var s2=str.substring(2);
				var s3=s1+"D"+s2;
				ajax("post", "/experiment/qt/qtTask/selectCodeCount.action", {
					code : str
				}, function(data) {
					if (data.success) {
							if(data.data==0){
								p.set("qtTaskCode",s3+"A");
							}else if(data.data==1){
								p.set("qtTaskCode",s3+"B");
							}else if(data.data==2){
								p.set("qtTaskCode",s3+"C");
							}else if(data.data==3){
								p.set("qtTaskCode",s3+"D");
							}else{
								message(biolims.common.pleaseResample);
								return;
							}
							p.set("Code",obj.get("code"));
							p.set("sampleName",obj.get("sampleName"));
							p.set("patient",obj.get("name"));
							p.set("state",obj.get("state"));
							p.set("testProject",obj.get("businessType"));
							p.set("location",obj.get("binLocation"));						
							var index = $("#qtTask_indexs").val();
							if(index!=""||index!=0){								
								p.set("indexs", Number(index)+count);								
							}else{
								p.set("indexs", count+1);								
							}
							count++;							
					}
					qtTaskItemGrid.getStore().add(p);
				});
			}
		});		
		qtTaskItemGrid.startEditing(0,0);
	}else{
		message(biolims.common.pleaseAddSample);
	}
}

