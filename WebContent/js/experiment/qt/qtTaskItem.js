﻿var qtTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'expCode',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'chromosomalLocation',
		type:"string"
	});
    fields.push({
		name:'primerNumber',
		type:"string"
	});
    fields.push({
		name:'leftPrimer',
		type:"string"
	});
    fields.push({
		name:'rightPrimer',
		type:"string"
	});
    fields.push({
		name:'ampliconid',
		type:"string"
	});  
    fields.push({
		name:'rowCode',
		type:"string"
	});
	fields.push({
		name:'colCode',
		type:"string"
	});
    fields.push({
		name:'counts',
		type:"string"
	});
    fields.push({
		name:'sampleName',
		type:"string"
    });
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'sampleVolume',
		type:"string"
	});
    fields.push({
		name:'addVolume',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'indexs',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
    fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'concentration',
		type:"string"
	});
	fields.push({
		name:'result',
		type:"string"
	});
	fields.push({
		name:'reason',
		type:"string"
	});
	fields.push({
		name:'stepNum',
		type:"string"
	});
    fields.push({
	    name:'qtTask-id',
	    type:"string"
	});
	fields.push({
		name:'qtTask-name',
		type:"string"
	});
	fields.push({
		name:'orderNumber',
		type:"string"
	});
	fields.push({
		name:'projectId',
		type:"string"
	});
	fields.push({
		name:'contractId',
		type:"string"
	});
	fields.push({
		name:'orderType',
		type:"string"
	});
    fields.push({
		name:'taskId',
		type:"string"
	});  
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'productNum',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'sampleConsume',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    fields.push({
		name:'techJkServiceTask-id',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-name',
		type:"string"
	});
  fields.push({
		name:'blx',
		type:"string"
	});
  fields.push({
		name:'sampleSyNum',
		type:"string"
	});
  fields.push({
		name:'num',
		type:"string"
	});

  fields.push({
	name:'unitGroup-id',
	type:"string"
});
fields.push({
	name:'unitGroup-name',
	type:"string"
});
/********新增字段*********/
fields.push({
	name:'dnaConcentration',
	type:"string"
});
fields.push({
	name:'qpcrLineNum',
	type:"string"
});

fields.push({
	name:'qpcrColNum',
	type:"string"
});
fields.push({
	name:'barCode',
	type:"string"
});
fields.push({
	name:'dataTraffic',
	type:"string"
});

	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.orderNumber,
		width:90,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6
	});
	cm.push({
		dataIndex:'expCode',
		hidden : true,
		header:biolims.common.expCode,
		width:20*6
	});	
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'barCode',
		hidden : false,
		header:biolims.common.barCode,
		width:20*6
	});
	cm.push({
		dataIndex:'dataTraffic',
		hidden : false,
		header:biolims.common.throughput,
		width:20*6
	});
	cm.push({
		dataIndex:'dnaConcentration',
		hidden : false,
		header:biolims.common.dnaConcentration,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'qpcrLineNum',
		hidden : false,
		header:biolims.common.qpcrReactionHoleLineNumber,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'qpcrColNum',
		hidden : false,
		header:biolims.common.qpcrReactionHoleNumber,
		sortable:true,
		width:20*6
	});
	
	cm.push({
		dataIndex:'chromosomalLocation',
		hidden : false,
		header:biolims.sanger.chromosomalLocation,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'primerNumber',
		hidden : true,
		header:biolims.sanger.primerNumber,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'leftPrimer',
		hidden : true,
		header:'leftPrimer',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'rightPrimer',
		hidden : true,
		header:'rightPrimer',
		sortable:true,
		width:20*6
	});
	
	cm.push({
		dataIndex:'ampliconid',
		hidden : true,
		header:'ampliconid',
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'indexs',
		hidden : true,
		header:'Index',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden : true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:biolims.common.concentration,
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : true,
		header:biolims.common.totalVolume,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});	
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:"单位组ID",
		width:15*10
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: "单位组",
		width:15*10,
		hidden:true
//		sortable:true,
//		editor : testUnitGroup
	});

	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:biolims.common.sampleNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : true,
		header:biolims.common.sampleConsume,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:"样本用量（体积ul）",
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'blx',
		hidden : true,
		header:"变量X",
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : true,
		header:"补水/RSB/TE体积",
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleSyNum',
		hidden : true,
		header:"样本剩余总量（ng）",
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.common.inspectDate,
		hidden : true,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		header:biolims.common.reportDate,
		hidden : true,
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.common.orderId,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.dicSampleTypeId,
		width:20*6
	});
	var testDicType =new Ext.form.TextField({
        allowBlank: false
	});
	testDicType.on('focus', function() {
		loadTestDicType();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		editor : testDicType,
		hidden : true
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'Name',
		hidden : true,
		header:biolims.common.sampleName,
		width:20*6
	});
	
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6
	});	
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*6
	});

	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			},{
				id : '0',
				name :biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:biolims.common.result,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:30*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : false,
		header:biolims.common.rowCode,
		width:30*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'colCode',
		hidden : false,
		header:biolims.common.colCode,
		width:30*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'counts',
		hidden : false,
		header:biolims.common.counts,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:"排板个数",
		width:10*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cm.push({
		dataIndex:'qtTaskCode',
		hidden : true,
		header:biolims.QPCR.testCode,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'productNum',
		hidden : true,
		header:biolims.common.productNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:biolims.common.sampleInfoNote,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qt/qtTask/showQtTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.QPCR.testDetail;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qt/qtTask/delQtTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qtTaskTempGrid.getStore().commitChanges();
				qtTaskTempGrid.getStore().reload();
				qtTaskItemGrid.getStore().commitChanges();
				qtTaskItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$(".jquery-ui-warning").html(biolims.common.pleaseInputSampleNum);
			$("#many_batItem_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_batItem_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_batItem_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = qtTaskItemGrid.getAllRecord();
							var store = qtTaskItemGrid.store;
							var isOper = true;
							var buf = [];
							qtTaskItemGrid.stopEditing();
							$.each(array,function(i, obj) {																
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										isOper = true;
									}									
								});
							});
							if(isOper){
								message("编码核对完毕！");
							}
							qtTaskItemGrid.getSelectionModel().selectRows(buf);																																			
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}							
							
							qtTaskItemGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchData,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_item_div"), biolims.common.batchData, null, {
				"Confirm" : function() {
					var records = qtTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var Consume = $("#Consume").val();
						var Volume = $("#Volume").val();
						var addVolume = $("#addVolume").val();
						qtTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("sampleConsume", Consume);
							obj.set("sampleVolume", Volume);
							obj.set("addVolume", addVolume);
						});
						qtTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.productType,
		handler : loadTestDicType
	});
	opts.tbar.push({
		text : biolims.common.productBatchNumber,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_productNum_div"), biolims.common.productBatchNumber, null, {
				"Confirm" : function() {
					var records = qtTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var productNum = $("#productNum").val();
						qtTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("productNum", productNum);
						});
						qtTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchPanelNum,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_counts_div"),biolims.common.batchPanelNum, null, {
				"Confirm" : function() {
					var records = qtTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var counts = $("#counts").val();
						qtTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("counts", counts);
						});
						qtTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	
	if($("#qtTask_id").val()&&$("#qtTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	/*opts.tbar.push({
		text : '请选择移动的位置',
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_item1_div"), "选择移动的位置", null, {
				"Confirm" : function() {
					var records = qtTaskItemGrid.getSelectRecord();
					var ids="";
					var st="";
					if (records && records.length > 0) {
						$.each(records, function(i, obj) {
							ids+=obj.get("id")+",";
							st+="'"+obj.get("id")+"',";
						});
						st=st.substring(0, st.length-1);
						var local = $("#local").val();
						var order1 = $("#order1").val();
						saveItem1(local,ids,st,order1);
					}else{
						message("请选择质控品！");
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});*/
	opts.tbar.push({
		text :biolims.common.verticalOrdering,
		handler :function() {
			sortTwo("1");
		}
	});
	opts.tbar.push({
		text : biolims.common.horizontalOrdering,
		handler : function() {
			sortTwo("0");
		}
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	
	
//	opts.tbar.push({
//		text : "单位组",
//		handler : function() {
//			var records = qtTaskItemGrid.getSelectRecord();
//			if(records.length>0){
//				loadUnitGroup();
//			}else{
//				message("请选择样本");
//			}
//			
//		}
//	});

	}
	qtTaskItemGrid=gridEditTable("qtTaskItemdiv",cols,loadParam,opts);
	$("#qtTaskItemdiv").data("qtTaskItemGrid", qtTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
/**
 * 选定排序位置排质控品
 */
function saveItem1(local,ids,st,order1){
	var id=$("#qtTask_id").val();
	if(id!="" && id !=null){
		ajax("post", "/experiment/qt/qtTask/saveItem.action", {
			id : id,
			local:local,
			ids : ids, 
			st : st,
			order:order1
		}, function(data) {
			if (data.success) {					
				qtTaskItemGrid.getStore().commitChanges();
				qtTaskItemGrid.getStore().reload();
				setTimeout(function() {
					if($("#qtTask_template").val()){
						var maxNum = $("#qtTask_maxNum").val();
						if(maxNum>0){
							load("/storage/container/sampleContainerTest.action", {
								id : $("#qtTask_template").val(),
								type :$("#type").val(),
								maxNum : 0
							}, "#3d_image0", null);
						}
					}
				}, 100);
				message(biolims.common.sortSuccess);
			} else {
				message(biolims.common.sortFailure);
			}
		}, null);
	}
}
/**
 * 横向或竖向排列96孔板
 */
function sortTwo(order){
	var id=$("#qtTask_id").val();
	if(id!="" && id !=null){
		$("#qtTask_orders").val(order);
		ajax("post", "/experiment/qt/qtTask/sortTwo.action", {
			id : id,
			order:order
		}, function(data) {
			if (data.success) {					
				qtTaskItemGrid.getStore().commitChanges();
				qtTaskItemGrid.getStore().reload();
				setTimeout(function() {
					if($("#qtTask_template").val()){
						var maxNum = $("#qtTask_maxNum").val();
						if(maxNum>0){
							load("/storage/container/sampleContainerTest.action", {
								id : $("#qtTask_template").val(),
								type :$("#type").val(),
								maxNum : 0
							}, "#3d_image0",null);
						}
					}
				}, 100);
				message(biolims.common.sortSuccess);
			} else {
				message(biolims.common.sortFailure);
			}
		}, null);
	}
}
function selectprojectFun(){
	var win = Ext.getCmp('selectproject');
	if (win) {win.close();}
	var selectproject= new Ext.Window({
	id:'selectproject',modal:true,title:biolims.common.selectProduct,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/com/biolims/system/product/productSelect.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectproject.close(); }  }]  });     selectproject.show(); }
	function setProductFun1(rec){
		var gridGrid = $("#qtTaskItemdiv").data("qtTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('product-id',rec.get('id'));
			obj.set('product-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectproject');
		if(win){
			win.close();
		}
	}
	
function setRowSelected(rowIndex){
    //设置rowIndex行被选中   
	qtTaskTemplateItemGrid.getSelectionModel().selectRow(rowIndex);  
}  

var loadDicType;	
//查询样本类型
function loadTestDicType(){
	var options = {};
	options.width = document.body.clientWidth-800;
	options.height = document.body.clientHeight-40;
	loadDicType=loadDialogPage(null, biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action", {
		"Confirm" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = qtTaskItemGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					$.each(records, function(a, b) {
						b.set("dicSampleType-id", obj.get("id"));
						b.set("dicSampleType-name", obj.get("name"));
					});
				});
			}else{
				message(biolims.common.selectYouWant);
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}

function setDicType(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qtTaskItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$.each(records, function(a, b) {
				b.set("dicSampleType-id", obj.get("id"));
				b.set("dicType-name", obj.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadDicType.dialog("close");
}
//查询单位组
function loadUnitGroup(){
var win = Ext.getCmp('loadUnitGroup');
if (win) {win.close();}
var loadUnitGroup= new Ext.Window({
id:'loadUnitGroup',modal:true,title:'选择单位组',layout:'fit',width:600,height:600,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center',
html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=unitGroup' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: '关闭',
 handler: function(){
	 loadUnitGroup.close(); }  }]  });   
loadUnitGroup.show(); }


function setunitGroup(id,name){
	var selRecords = qtTaskItemGrid.getSelectionModel().getSelections(); 
	if(selRecords.length>0){
		$.each(selRecords, function(i, obj) {
			obj.set('unitGroup-id',id);
			obj.set('unitGroup-name',name);
		});
	}
	var win = Ext.getCmp('loadUnitGroup');
	if(win){
		win.close();
	}
}
