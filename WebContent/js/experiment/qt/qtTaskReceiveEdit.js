﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#qtTaskReceive_id").val();
	var state = $("#qtTaskReceive_state").val();
	if(id=="" || state == "3"){
		var type="2";
		load("/experiment/qt/qtTaskReceive/showQtTaskReceiveItemListTo.action", {type:type}, "#QtTaskReceviceLeftPage");
		$("#markup").css("width","75%");
	}
});

function add() {
	window.location = window.ctx + "/experiment/qt/qtTaskReceive/editQtTaskReceive.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/qt/qtTaskReceive/showQtTaskReceiveList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#qtTaskReceive", {
					userId : userId,
					userName : userName,
					formId : $("#qtTaskReceive_id").val(),
					title : $("#qtTaskReceive_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {
		completeTask($("#qtTaskReceive_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg:biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var qtTaskReceiveItemDivData = $("#qtTaskReceiveItemdiv").data("qtTaskReceiveItemGrid");
		document.getElementById('qtTaskReceiveItemJson').value = commonGetModifyRecords(qtTaskReceiveItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/qt/qtTaskReceive/save.action";
		form1.submit();	
		}
}	

function editCopy() {
	window.location = window.ctx + '/experiment/qt/qtTaskReceive/copyQtTaskReceive.action?id=' + $("#qtTaskReceive_id").val();
}

$("#toolbarbutton_status").click(function(){
	var selRecord = qtTaskReceiveItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var result = selRecord.getAt(j).get("method");
		if(result==""){
			message(biolims.common.resultsIsEmpty);
			return;
		}
	}
	if ($("#qtTaskReceive_id").val()){
		commonChangeState("formId=" + $("#qtTaskReceive_id").val() + "&tableId=QtTaskReceive");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#qtTaskReceive_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#qtTaskReceive_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}

$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.sampleReceive,
	    	   contentEl:'markup'
	       } ]
	   });
});

load("/experiment/qt/qtTaskReceive/showQtTaskReceiveItemList.action", {
				id : $("#qtTaskReceive_id").val()
			}, "#qtTaskReceiveItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);