﻿
var generationAbnormalTaskGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
	   name:'code',
	   type:"string"
   });
	   fields.push({
	   name:'sampleCode',
	   type:"string"
   });
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'bulk',
		type:"string"
	});
	   fields.push({
		name:'chorma',
		type:"string"
	});
	   fields.push({
		name:'resultDecide',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'handleIdea',
		type:"string"
	});
	   fields.push({
		name:'isExecute',
		type:"string"
	});
	   fields.push({
		name:'backTime',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'generationAbnormalTask-id',
		type:"string"
	});
	    fields.push({
		name:'generationAbnormalTask-name',
		type:"string"
	});
	    fields.push({
	    name:'method',
	    type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
		
	});

	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'文库编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:30*6
		
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6
		
	});
	cm.push({
		dataIndex:'bulk',
		hidden : false,
		header:'体积',
		width:20*6
		
	});
	cm.push({
		dataIndex:'chorma',
		hidden : true,
		header:'浓度',
		width:20*6
	});
	
	var resultDecideCob = new Ext.form.ComboBox({
		transform : "resultDecide",
		width : 50,
		triggerAction : 'all',
		lazyRender : true
	});
	cm.push({
		dataIndex : 'resultDecide',
		header : '结果判定',
		width : 80,
//		editor : resultDecideCob,
		renderer : function(value, metadata, record, rowIndex,
				colIndex, store) {
			if (value == "0") {
				return "<span style='color:red'>合格</span>";
			} else if (value == "1") {
				return "不合格";
			} 
		}
	});

	/*var storeresultDecideCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '合格' ], [ '1', '不合格' ] ]
	});
	var resultDecideCob = new Ext.form.ComboBox({
		store : storeresultDecideCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'resultDecide',
		hidden : false,
		header:'结果判定',
		width:20*6,
		editor : resultDecideCob,
		renderer : Ext.util.Format.comboRenderer(resultDecideCob)
	});*/
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storenextFlowCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '1', 'QPCR质控' ],[ '2', '重建库' ],['3','入库'] ,['4','异常反馈至项目管理']]
	});
	var nextFlowCob = new Ext.form.ComboBox({
		store : storenextFlowCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:'下一步流向',
		width:20*6,
		editor : nextFlowCob,
		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
	});
	cm.push({
		dataIndex:'handleIdea',
		hidden : false,
		header:'处理意见',
		width:50*6
	});
//	cm.push({
//		dataIndex:'isExecute',
//		hidden : false,
//		header:'确认执行',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storeisExecuteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '是' ], [ '1', '否' ] ]
	});
	var isExecuteCob = new Ext.form.ComboBox({
		store : storeisExecuteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:'是否执行',
		width:20*6,
		editor : isExecuteCob,
		renderer : Ext.util.Format.comboRenderer(isExecuteCob)
	});
	cm.push({
		dataIndex:'backTime',
		hidden : false,
		header:'反馈时间',
		width:20*6,
		
		renderer: formatDate
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	
	var storemethodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '重抽血' ], [ '1', '退费' ] ,[ '2', '合格' ],[ '3', '待反馈']]
	});
	var methodCob = new Ext.form.ComboBox({
		store : storemethodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'method',
		header:'处理结果',
		width:20*6,
		sortable:true,
		editor : methodCob,
		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/generation/generationAbnormalTask/showGenerationAbnormalTaskListJson.action";
	var opts={};
	opts.title="测序异常反馈";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	generationAbnormalTaskGrid=gridEditTable("generationAbnormalTaskdiv",cols,loadParam,opts);
	$("#generationAbnormalTaskdiv").data("generationAbnormalTaskGrid", generationAbnormalTaskGrid);
});
//保存
function save(){	
	var selectRecord = generationAbnormalTaskGrid.getSelectionModel();
	var inItemGrid = $("#generationAbnormalTaskdiv").data("generationAbnormalTaskGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/generation/generationAbnormalTask/saveGenerationAbnormalTask.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#generationAbnormalTaskdiv").data("generationAbnormalTaskGrid").getStore().commitChanges();
					$("#generationAbnormalTaskdiv").data("generationAbnormalTaskGrid").getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
		});
	}else{
		message("没有需要保存的数据！");
	}
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
			commonSearchAction(generationAbnormalTaskGrid);
//			alert(wkAbnormalBackGrid);
			$(this).dialog("close");
		},
		"清空" : function() {
			form_reset();

		}
	}, true, option);
}


	
