﻿var generationTestDetailGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'reciveDate',
		type:"string",
	});
	   fields.push({
		name:'project',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'testQualife',
		type:"string"
	});
	   fields.push({
		name:'failReasons',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'generationTest-id',
		type:"string"
	});
	    fields.push({
		name:'generationTest-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*10
	});
	cm.push({
		dataIndex:'reciveDate',
		hidden : false,
		header:'实验时间',
		width:20*6
	});	
	cm.push({
		dataIndex:'project',
		hidden : false,
		header:'检测项目',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	var testQualife = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '是'
			},{
				id : '0',
				name : '否'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'testQualife',
		hidden : false,
		header:'是否合格',
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(testQualife),editor: testQualife
	});
	/*cm.push({
		dataIndex:'testQualife',
		hidden : false,
		header:'是否合格',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'failReasons',
		hidden : false,
		header:'失败原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generationTest-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		
	});
	cm.push({
		dataIndex:'generationTest-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/generation/showGenerationTestDetailListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="测序样本";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#generationTest_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/generation/delGenerationTestDetail.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择实验员',
			handler : selectreciveUserFun
		});
	opts.tbar.push({
		text : '开始实验',
		handler : addDetails
	});
	/*opts.tbar.push({
			text : '选择相关主表',
			handler : selectgenerationTestFun
		});
	*/
	
	
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = generationTestDetailGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							generationTestDetailGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	}
	generationTestDetailGrid=gridEditTable("generationTestDetaildiv",cols,loadParam,opts);
	$("#generationTestDetaildiv").data("generationTestDetailGrid", generationTestDetailGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//开始实验
function addDetails(){
	var selectRecord=generationTestDetailGrid.getSelectionModel();
	var selRecord=generationTestStepGrid.getSelectRecord();
	//setRowSelected(1);
	if (selectRecord.getSelections().length > 0) {
		var sampleCodes = "";
		$.each(selectRecord.getSelections(), function(i, obj) {
			sampleCodes += obj.get("sampleCode")+",";
		});
		if (!selRecord.length) {
			selRecord = generationTestStepGrid.getAllRecord();
		}
		if (selRecord&&selRecord.length>0) {
			generationTestStepGrid.stopEditing();
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", sampleCodes);
			});
			generationTestStepGrid.startEditing(0, 0);
		}
			
	}

}
function selectreciveUserFun(){
	var win = Ext.getCmp('selectreciveUser');
	if (win) {win.close();}
	var selectreciveUser= new Ext.Window({
	id:'selectreciveUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectreciveUser.close(); }  }]  });     selectreciveUser.show(); }
	function setreciveUser(id,name){
		var gridGrid = $("#generationTestDetaildiv").data("generationTestDetailGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',id);
			obj.set('reciveUser-name',name);
		});
		var win = Ext.getCmp('selectreciveUser')
		if(win){
			win.close();
		}
	}
	
function selectgenerationTestFun(){
	var win = Ext.getCmp('selectgenerationTest');
	if (win) {win.close();}
	var selectgenerationTest= new Ext.Window({
	id:'selectgenerationTest',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GenerationTestSelect.action?flag=generationTest' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectgenerationTest.close(); }  }]  });     selectgenerationTest.show(); }
	function setgenerationTest(id,name){
		var gridGrid = $("#generationTestDetaildiv").data("generationTestDetailGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('generationTest-id',id);
			obj.set('generationTest-name',name);
		});
		var win = Ext.getCmp('selectgenerationTest')
		if(win){
			win.close();
		}
	}
	
