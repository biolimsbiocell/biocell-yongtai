﻿var generationTestStepGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'describe',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string",
	});
	   fields.push({
		name:'endTime',
		type:"string",
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'generationTesk-id',
		type:"string"
	});
	    fields.push({
		name:'generationTesk-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'stepNum',
		hidden : false,
		header:'步骤编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:'步骤名称',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'describe',
		hidden : false,
		header:'步骤描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:20*6
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:'关联样本',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generationTesk-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generationTesk-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/generation/showGenerationTestStepListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="执行步骤";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#generationTest_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/generation/delGenerationTestStep.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	/*pts.tbar.push({
			text : '选择相关主表',
			handler : selectgenerationTestFun
		});*/
	
	/*opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
*/
	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = generationTestStepGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							generationTestStepGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '打印执行单',
		handler : printOrder
	});
	opts.tbar.push({
		text : '开始执行',
		handler : getStartTime
	});
	opts.tbar.push({
		text : '执行结束',
		handler : getEndTime
	});
	opts.tbar.push({
		text : '生成结果明细',
		handler : addSuccess
	});
	}
	generationTestStepGrid=gridEditTable("generationTestStepdiv",cols,loadParam,opts);
	$("#generationTestStepdiv").data("generationTestStepGrid", generationTestStepGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
//生成结果明细
function addSuccess(){
	var selectRecord=generationTestDetailGrid.getSelectionModel();
	var selRecord=generationTestResultGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("sampleNum");
				if(oldv == obj.get("sampleCode")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;					
				}
			}
			if(!isRepeat){
				var ob = generationTestResultGrid.getStore().recordType;
				generationTestResultGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				
				p.set("sampleNum",obj.get("sampleCode"));
				
				generationTestResultGrid.getStore().add(p);
			}
			
		});
	}else{
		message("请先选择数据！");
	}
}
//打印执行单
function printOrder(){
	var id=$("#generationTest_template").val();
	if(id==""){
		message("请先选择模板!");
		return;
	}else{
		window.open(window.ctx+"/system/template/template/openTemplate.action?id="+id,'','height=768,width=1366,scrollbars=yes,resizable=yes');
	}
}
//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=generationTestStepGrid.getSelectionModel();
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			obj.set("startTime",str);
		});
	}else{
		message("请先选择数据！");
	}
}
//获取停止时的时间
function getEndTime(){
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=generationTestStepGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("endTime",str);
		});
	}
}
function selectgenerationTestFun(){
	var win = Ext.getCmp('selectgenerationTest');
	if (win) {win.close();}
	var selectgenerationTest= new Ext.Window({
	id:'selectgenerationTest',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GenerationTestSelect.action?flag=generationTest' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectgenerationTest.close(); }  }]  });     selectgenerationTest.show(); }
	function setgenerationTest(id,name){
		var gridGrid = $("#generationTestStepdiv").data("generationTestStepGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('generationTest-id',id);
			obj.set('generationTest-name',name);
		});
		var win = Ext.getCmp('selectgenerationTest')
		if(win){
			win.close();
		}
	}
	
