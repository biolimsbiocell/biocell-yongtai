﻿
var generationReceiveTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'code',
			type:"string"
		});
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'原始样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:50*6
	});
	cm.push({
		dataIndex:'unit',
		hidden : false,
		header:'单位',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/generation/generationReceiveTask/showGenerationReceiveTempListJson.action";
	var opts={};
	opts.title="待实验样本";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								//alert(12);
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = generationReceiveTempGrid.getAllRecord();
							var store = generationReceiveTempGrid.store;

							var isOper = true;
							var buf = [];
							generationReceiveTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								})
							});
							generationReceiveTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message("样本号核对不符，请检查！");
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							generationReceiveTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	generationReceiveTempGrid=gridEditTable("generationReceiveTempdiv",cols,loadParam,opts);
});

function addItem(){
	var selectRecord=generationReceiveTempGrid.getSelectionModel();
	var selRecord=generationReceiveInfoGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("code");
				if(oldv == obj.get("code")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;					
				}
			}
			if(!isRepeat){
			var ob = generationReceiveInfoGrid.getStore().recordType;
			generationReceiveInfoGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("state",'1');
			
			generationReceiveInfoGrid.getStore().add(p);
		}
			
		});
		generationReceiveInfoGrid.startEditing(0, 0);
		}
	
}
