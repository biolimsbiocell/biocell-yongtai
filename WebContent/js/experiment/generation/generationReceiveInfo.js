﻿
var generationReceiveInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sname',
		type:"string"
	});
	   fields.push({
		name:'testPro',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'storageLocal-id',
		type:"string"
	});
	    fields.push({
		name:'storageLocal-name',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'generationReceiveTask-id',
		type:"string"
	});
	    fields.push({
		name:'generationReceiveTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:30*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sname',
		hidden : false,
		header:'姓名',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'testPro',
		hidden : false,
		header:'检测项目',
		width:30*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	var method = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '通过'
			}, {
				id : '1',
				name : '不通过'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理方式',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(method),editor: method
	});
	/*cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理方式',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storageLocal-id',
		hidden : true,
		header:'储位ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storageLocal-name',
		hidden : true,
		header:'储位',
		width:20*10
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generationReceiveTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generationReceiveTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/generation/generationReceiveTask/showGenerationReceiveInfoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="样本接收明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#generationReceiveTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/generation/generationReceiveTask/delGenerationReceiveInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择储位',
			handler : selectstorageLocalFun
		});
    
//	opts.tbar.push({
//			text : '选择关联主表',
//			handler : selectwKReceiveFun
//		});
	
	
	
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = generationReceiveInfoGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							generationReceiveInfoGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	generationReceiveInfoGrid=gridEditTable("generationReceiveInfodiv",cols,loadParam,opts);
	$("#generationReceiveInfodiv").data("generationReceiveInfoGrid", generationReceiveInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


//选择储位selectstorageLocalFun
function selectstorageLocalFun(){
	var win = Ext.getCmp('selectstorageFun');
	if (win) {win.close();}
	var selectstorageFun= new Ext.Window({
	id:'selectstorageFun',modal:true,title:'选择储位',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTree1.action?flag=SaveLocationFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectstorageFun.close(); }  }]  });     selectstorageFun.show(); }
	function setSaveLocationFun(rec){
		var gridGrid = $("#generationReceiveInfodiv").data("generationReceiveInfoGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('storage-id',rec.get('id'));
			obj.set('storage-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectstorageFun');
		if(win){
			win.close();
		}
	}
	
function selectgenerationReceiveTaskFun(){
	var win = Ext.getCmp('selectgenerationReceiveTask');
	if (win) {win.close();}
	var selectgenerationReceiveTask= new Ext.Window({
	id:'selectgenerationReceiveTask',modal:true,title:'选择关联主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GenerationReceiveTaskSelect.action?flag=wKReceive' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectgenerationReceive.close(); }  }]  });     selectgenerationReceiveTask.show(); }
	function setgenerationReceiveTask(id,name){
		var gridGrid = $("#generationReceiveInfodiv").data("generationReceiveInfoGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('generationReceiveTask-id',id);
			obj.set('generationReceiveTask-name',name);
		});
		var win = Ext.getCmp('selectgenerationReceiveTask');
		if(win){
			win.close();
		}
	}
	
