﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#generationTest_id").val();
	if(id==""){
		load("/experiment/generation/showGenerationTestTempList.action", { }, "#generationTestTemppage");
		$("#markup").css("width","75%");
	}
});	
function add() {
	window.location = window.ctx + "/experiment/generation/editGenerationTest.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/generation/showGenerationTestList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#generationTest", {
					userId : userId,
					userName : userName,
					formId : $("#generationTest_id").val(),
					title : $("#generationTest_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#generationTest_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var generationTestDetailDivData = $("#generationTestDetaildiv").data("generationTestDetailGrid");
		document.getElementById('generationTestDetailJson').value = commonGetModifyRecords(generationTestDetailDivData);
	    var generationTestStepDivData = $("#generationTestStepdiv").data("generationTestStepGrid");
		document.getElementById('generationTestStepJson').value = commonGetModifyRecords(generationTestStepDivData);
	    var generationTestItemDivData = $("#generationTestItemdiv").data("generationTestItemGrid");
		document.getElementById('generationTestItemJson').value = commonGetModifyRecords(generationTestItemDivData);
	    var generationTestParticuarsDivData = $("#generationTestParticuarsdiv").data("generationTestParticuarsGrid");
		document.getElementById('generationTestParticuarsJson').value = commonGetModifyRecords(generationTestParticuarsDivData);
	    var generationTestResultDivData = $("#generationTestResultdiv").data("generationTestResultGrid");
		document.getElementById('generationTestResultJson').value = commonGetModifyRecords(generationTestResultDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/generation/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/generation/copyGenerationTest.action?id=' + $("#generationTest_id").val();
}
/*function changeState() {
	commonChangeState("formId=" + $("#generationTest_id").val() + "&tableId=generationTest");
}*/
$("#toolbarbutton_status").click(function(){
	if ($("#generationTest_id").val()){
		commonChangeState("formId=" + $("#generationTest_id").val() + "&tableId=GenerationTest");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#generationTest_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#generationTest_createDate").val());
	nsc.push("下达时间不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'测序页面',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/generation/showGenerationTestDetailList.action", {
				id : $("#generationTest_id").val()
			}, "#generationTestDetailpage");
load("/experiment/generation/showGenerationTestStepList.action", {
				id : $("#generationTest_id").val()
			}, "#generationTestSteppage");
load("/experiment/generation/showGenerationTestItemList.action", {
				id : $("#generationTest_id").val()
			}, "#generationTestItempage");
load("/experiment/generation/showGenerationTestParticuarsList.action", {
				id : $("#generationTest_id").val()
			}, "#generationTestParticuarspage");
load("/experiment/generation/showGenerationTestResultList.action", {
				id : $("#generationTest_id").val()
			}, "#generationTestResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
	text: '复制'
	});
item.on('click', editCopy);
    //调用模板
   function TemplateFun(){
			var type="doGenera";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}
	function setTemplateFun(rec){
			var code=$("#generationTest_template").val();
			if(code==""){
				document.getElementById('generationTest_template').value=rec.get('id');
				document.getElementById('generationTest_template_name').value=rec.get('name');
				var win = Ext.getCmp('TemplateFun');
				if(win){win.close();}
				var id=rec.get('id');
				ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id,
					}, function(data) {
						if (data.success) {
							var ob = generationTestStepGrid.getStore().recordType;
							generationTestStepGrid.stopEditing();
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								//p.set("id",obj.id);
								p.set("stepNum",obj.code);
								p.set("stepName",obj.name);
								generationTestStepGrid.getStore().add(p);							
							});
							
							generationTestStepGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = generationTestItemGrid.getStore().recordType;
							generationTestItemGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								//p.set("id",obj.id);
								p.set("reagentId",obj.code);
								p.set("designation",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
//								p.set("itemId",obj.itemId);
								generationTestItemGrid.getStore().add(p);							
							});
							
							generationTestItemGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = generationTestParticuarsGrid.getStore().recordType;
							generationTestParticuarsGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								//p.set("id",obj.id);
								p.set("deviceId",obj.code);
								p.set("designation",obj.name);
								p.set("isGood",obj.isGood);
								generationTestParticuarsGrid.getStore().add(p);							
							});			
							generationTestParticuarsGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null);
			}else{
				if(rec.get('id')==code){
	 				var win = Ext.getCmp('TemplateFun');
	 				if(win){win.close();}
	 			 }else{
	 				var ob1 = generationTestStepGrid.store.getCount();
					var ob2 = generationTestItemGrid.store.getCount();
					var ob3 = generationTestParticuarsGrid.store.getCount();
	 				 if(ob1>0){
	 					message("请先删除明细中原有数据！");
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
		 				return; 
	 				 }else if(ob2>0){
	 					message("请先删除原辅料明细中原有数据！");
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
		 				return;
	 				 }else if(ob3>0){
	 					message("请先删除设备明细中原有数据！");
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
		 				return;
	 				 }else{
	 					document.getElementById('generationTest_template').value=rec.get('id');
		 				document.getElementById('generationTest_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = generationTestStepGrid.getStore().recordType;
									generationTestStepGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										//p.set("id",obj.id);
										p.set("stepNum",obj.code);
										p.set("stepName",obj.name);
										generationTestStepGrid.getStore().add(p);							
									});
									
									generationTestStepGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = generationTestItemGrid.getStore().recordType;
									generationTestItemGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										//p.set("id",obj.id);
										p.set("reagentId",obj.code);
										p.set("designation",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										generationTestItemGrid.getStore().add(p);							
									});
									
									generationTestItemGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = generationTestParticuarsGrid.getStore().recordType;
									generationTestParticuarsGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										//p.set("id",obj.id);
										p.set("deviceCode",obj.code);
										p.set("designation",obj.name);
										p.set("isGood",obj.isGood);
//										p.set("itemId",obj.itemId);
										generationTestParticuarsGrid.getStore().add(p);							
									});			
									generationTestParticuarsGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
	 				 }	 				
 				generationTestParticuarsGrid.store.removeAll();
				 }
				
	 		}
}