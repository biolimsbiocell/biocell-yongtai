﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#generationReceiveTask_state").val();
	if(id=="3"){
		load("/experiment/generation/generationReceiveTask/showGenerationReceiveTempList.action", { }, "#generationReceiveTempPage");
		$("#markup").css("width","75%");
	}
});
function add() {
	window.location = window.ctx + "/experiment/generation/generationReceiveTask/editGenerationReceiveTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/generation/generationReceiveTask/showGenerationReceiveTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#generationReceiveTask", {
					userId : userId,
					userName : userName,
					formId : $("#generationReceiveTask_id").val(),
					title : $("#generationReceiveTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#generationReceiveTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var generationReceiveInfoDivData = $("#generationReceiveInfodiv").data("generationReceiveInfoGrid");
		document.getElementById('generationReceiveInfoJson').value = commonGetModifyRecords(generationReceiveInfoDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/generation/generationReceiveTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/generation/generationReceiveTask/copyGenerationReceiveTask.action?id=' + $("#generationReceiveTask_id").val();
}
/*function changeState() {
	commonChangeState("formId=" + $("#generationReceiveTask_id").val() + "&tableId=generationReceiveTask");
}*/
$("#toolbarbutton_status").click(function(){
	if ($("#generationReceiveTask_id").val()){
		commonChangeState("formId=" + $("#generationReceiveTask_id").val() + "&tableId=GenerationReceiveTask");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#generationReceiveTask_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'样本接收',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/generation/generationReceiveTask/showGenerationReceiveInfoList.action", {
				id : $("#generationReceiveTask_id").val()
			}, "#generationReceiveInfopage");
//load("/experiment/generation/generationReceiveInfo/showGenerationReceiveInfoList.action",{
//	id : $("#generationReceiveTask_id").val()
//}, "#generationReceive");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);