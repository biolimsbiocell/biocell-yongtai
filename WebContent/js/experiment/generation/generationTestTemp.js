
var generationTestTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'dnaCode',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'experimentDnaGet-id',
		type:"string"
	});
	    fields.push({
		name:'experimentDnaGet-name',
		type:"string"
	});
	    //
	    fields.push({
			name:'sampleName',
			type:"string"
	   });
	   fields.push({
			name:'sampleNum',
			type:"string"
		});
	   fields.push({
			name:'sampleVolume',
			type:"string"
		});
	   fields.push({
			name:'addVolume',
			type:"string"
		});
	   fields.push({
			name:'sumVolume',
			type:"string"
		});
	   fields.push({
			name:'indexs',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dnaCode',
		hidden : false,
		header:'DNA编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'接收样本号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:'DNA储位',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'unit',
		hidden : false,
		header:'单位',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleName',
		hidden : false,
		header:'样本名称',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本用量（μg）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : false,
		header:'取样体积（μl）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : false,
		header:'补充体积（μl）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : false,
		header:'总体积（μl）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexs',
		hidden : false,
		header:'Index',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'说明',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'experimentDnaGet-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'experimentDnaGet-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/generation/showGenerationTestTempListJson.action";
	var opts={};                              
	opts.title="待实验样本";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '添加到任务',
		handler : addItem
	});
	generationTestTempGrid=gridEditTable("generationTestTempdiv",cols,loadParam,opts);
});
//添加到一代测序
function addItem(){
	var selRecord = generationTestTempGrid.getSelectionModel().getSelections();
	var getRecord = generationTestDetailGrid.store;
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("sampleCode");
				if(getData==obj.get("sampleCode")){
					message("有重复的数据，请重新选择！");
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
				var ob = generationTestDetailGrid.getStore().recordType;
				generationTestDetailGrid.stopEditing();
				var p= new ob({});
				
				var d = new Date();
				var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
				p.set("tempId", obj.get("id"));
				p.set("sampleCode", obj.get("sampleCode"));
				p.set("reciveUser-id", $("#generationTest_createUser").val());
				p.set("reciveUser-name", $("#generationTest_createUser_name").val());
				p.set("reciveDate", str);
				
				generationTestDetailGrid.getStore().add(p);
				generationTestDetailGrid.startEditing(0,0);
			}
		});
		
		
	}
}