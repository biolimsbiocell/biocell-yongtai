﻿
var generationTestResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'textNum',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'flow',
		type:"string"
	});
	   fields.push({
		name:'idea',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'generationTest-id',
		type:"string"
	});
	    fields.push({
		name:'generationTest-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'textNum',
		hidden : false,
		header:'测序编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'unit',
		hidden : false,
		header:'单位',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果判定',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '合格'
			}, {
				id : '1',
				name : '异常'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果判定',
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	/*cm.push({
		dataIndex:'flow',
		hidden : false,
		header:'下一步流向',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	var flow = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '质控'
			}, {
				id : '1',
				name : '文库'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'flow',
		hidden : false,
		header:'下一步流向',
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(flow),editor: flow
	});
	
	cm.push({
		dataIndex:'idea',
		hidden : false,
		header:'处理意见',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generationTest-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'generationTest-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/generation/showGenerationTestResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="测序结果";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#generationTest_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/generation/delGenerationTestResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	/*opts.tbar.push({
			text : '选择相关主表',
			handler : selectgenerationTestFun
		});*/
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = generationTestResultGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							generationTestResultGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	generationTestResultGrid=gridEditTable("generationTestResultdiv",cols,loadParam,opts);
	$("#generationTestResultdiv").data("generationTestResultGrid", generationTestResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectgenerationTestFun(){
	var win = Ext.getCmp('selectgenerationTest');
	if (win) {win.close();}
	var selectgenerationTest= new Ext.Window({
	id:'selectgenerationTest',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/GenerationTestSelect.action?flag=generationTest' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectgenerationTest.close(); }  }]  });     selectgenerationTest.show(); }
	function setgenerationTest(id,name){
		var gridGrid = $("#generationTestResultdiv").data("generationTestResultGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('generationTest-id',id);
			obj.set('generationTest-name',name);
		});
		var win = Ext.getCmp('selectgenerationTest')
		if(win){
			win.close();
		}
	}
	
