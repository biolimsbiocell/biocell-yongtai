$(function() {
	
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	var mainFileInput=fileInput ('1','ufTask',$("#ufTask_id").val());
	
	
})	
function change(id) {
	$("#" + id).css({
		"background-color" : "white",
		"color" : "black"
	});
}
function add() {
	window.location = window.ctx + "/experiment/uf/ufTask/editUfTask.action";
}

function list() {
	window.location = window.ctx + '/experiment/uf/ufTask/showUfTaskList.action';
}
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
function tjsp() {
			top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {icon: 3, title:biolims.common.prompt}, function(index){
				top.layer.open({
					  title: biolims.common.submit,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toStartView.action?formName=UfTask",
					  yes: function(index, layer) {
						 var datas={
									userId : userId,
									userName : userName,
									formId : $("#ufTask_id").val(),
									title : $("#ufTask_name").val(),
									formName : 'UfTask'
								}
							ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
								if (data.success) {
									top.layer.msg(biolims.common.submitSuccess);
									if (typeof callback == 'function') {
										callback(data);
									}
									dialogWin.dialog("close");
								} else {
									top.layer.msg(biolims.common.submitFail);
								}
							}, null);
							top.layer.close(index);
						},
						cancel: function(index, layer) {
							top.layer.close(index)
						}
				
				});     
				  top.layer.close(index);
				});
}
function sp(){
	
		var taskId = $("#bpmTaskId").val();
		var formId = $("#ufTask_id").val();
		
					top.layer.open({
						  title: biolims.common.handle,
						  type:2,
						  anim: 2,
						  area: ['800px','500px']
						  ,btn: biolims.common.selected,
						  content: window.ctx+"/workflow/processinstance/toCompleteTaskView.action?taskId="+taskId+"&formId="+formId,
						  yes: function(index, layer) {
							  var operVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
							  var opinionVal =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();
							  var opinion =  $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinion").val();
								if(!operVal){
									top.layer.msg(biolims.common.pleaseSelectOper);
									return false;
								}
								if (operVal == "2") {
									_trunTodoTask(taskId, callback, dialogWin);
								} else {
									var paramData = {};
									paramData.oper = operVal;
									paramData.info = opinion;
					
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									}
									ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
										if (data.success) {
											top.layer.msg(biolims.common.submitSuccess);
											if (typeof callback == 'function') {
											}
										} else {
											top.layer.msg(biolims.common.submitFail);
										}
									}, null);
								}
								window.open(window.ctx+"/main/toPortal.action",'_parent');
							},
							cancel: function(index, layer) {
								top.layer.close(index)
							}
					
					});     
}

function ck(){
			top.layer.open({
					  title: biolims.common.checkFlowChart,
					  type:2,
					  anim: 2,
					  area: ['800px','500px']
					  ,btn: biolims.common.selected,
					  content: window.ctx+"/workflow/processinstance/toTraceProcessInstanceView.action?formId=" + $("#ufTask_id").val(),
					  yes: function(index, layer) {
							top.layer.close(index)
					   },
						cancel: function(index, layer) {
							top.layer.close(index)
						}
				});     
}

function save() {
if(checkSubmit()==true){
		document.getElementById('ufTaskCosJson').value = saveUfTaskCosjson($("#ufTaskCosTable"));
		console.log($("#ufTaskCosJson").val());
		form1.action = window.ctx
				+ "/experiment/uf/ufTask/save.action?loadNumber=1";
		form1.submit();
	}
}

function save() {
if(checkSubmit()==true){
		
		
		var jsonStr = JSON.stringify($("#form1").serializeObject());  
		$.ajax({
			url: ctx + '/experiment/uf/ufTask/save.action',
			dataType: 'json',
			type: 'post',
			data: {
				dataValue: jsonStr,
				ufTaskCosJson : saveUfTaskCosjson($("#ufTaskCosTable")),
				
				bpmTaskId : $("#bpmTaskId").val()
			},
			success: function(data) {
				//关闭加载层
				//top.layer.close(index);
				
				if(data.bool) {
					var url = "/experiment/uf/ufTask/editUfTask.action?id="+data.id;
					
					if(data.bpmTaskId){
						url = url +"&bpmTaskId="+data.bpmTaskId;
					}
					
					window.location.href=url;
					
					
				}else{
					top.layer.msg(data.msg);
				
				}
				}
			
		});
		
	}
}		

$.fn.serializeObject = function() {
	   var o = {};  
	    var a = this.serializeArray();  
	    $.each(a, function() {  
	        if (o[this.name]) {  
	            if (!o[this.name].push) {  
	                o[this.name] = [ o[this.name] ];  
	            }  
	            o[this.name].push(this.value || '');  
	        } else {  
	            o[this.name] = this.value || '';  
	        }  
	    });  
	    return o;  
};



function editCopy() {
	window.location = window.ctx + '/experiment/uf/ufTask/copyUfTask.action?id=' + $("#ufTask_id").val();
}
function changeState() {
	var paraStr="formId=" + $("#sampleOrder_id").val()
	+ "&tableId=sampleOrder";
	top.layer.open({
		  title: biolims.common.changeState,
		  type:2,
		  anim: 2,
		  area: ['400px','400px']
		  ,btn: biolims.common.selected,
		  content: window.ctx
			+ "/applicationTypeAction/applicationTypeActionLook.action?" + paraStr
			+ "&flag=changeState'",
		  yes: function(index, layer) {
			  top.layer.confirm(biolims.common.toSubmit, {icon: 3, title:biolims.common.prompt}, function(index){
				 ajax("post","/applicationTypeAction/exeFun.action",{
					 applicationTypeActionId:ufTask,
					 formId:$("#ufTask_id").val()
				 },function(response){
					 var respText = response.message;
					 if (respText == '') {
							window.location.reload();
						} else {
							top.layer.msg(respText);
						}
				 },null)
				 top.layer.close(index);
			 })
			},
			cancel: function(index, layer) {
				top.layer.close(index)
			}
	
	});   
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#ufTask_id").val());
	nsc.push("编号"+biolims.common.describeEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			top.layer.msg(mess);
			return false;
		}
		return true;
}
function fileUp(){
	$("#uploadFile").modal("show");
}
function fileView(){
	top.layer.open({
		title:biolims.common.attachment,
		type:2,
		skin: 'layui-top.layer-lan',
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		content:window.ctx+"/operfile/initFileList.action?flag=1&modelType=ufTask&id="+$("#ufTask_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
function settextreadonly() {
    jQuery(":text, textarea").each(function() {
	var _vId = jQuery(this).attr('id');
	jQuery(this).css("background-color","#B4BAB5").attr("readonly", "readOnly");
	if (_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}
function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}


function showreciveUser(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"//userSelectTable.action",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#ufTask_reciveUser").val(id)
		$("#ufTask_reciveUser_name").val(name)
		},
	})
}
function showcreateUser(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"//userSelectTable.action",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#ufTask_createUser").val(id)
		$("#ufTask_createUser_name").val(name)
		},
	})
}
function showtemplate(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"//templateSelectTable.action",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplateTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplateTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#ufTask_template").val(id)
		$("#ufTask_template_name").val(name)
		},
	})
}
function showacceptUser(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"//userGroupSelectTable.action",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroupTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroupTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#ufTask_acceptUser").val(id)
		$("#ufTask_acceptUser_name").val(name)
		},
	})
}
function showparent(){
		top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"//ufTaskSelectTable.action",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUfTaskTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUfTaskTable .chosed").children("td").eq(0).text();
		top.layer.close(index)
		$("#ufTask_parent").val(id)
		$("#ufTask_parent_name").val(name)
		},
	})
}
