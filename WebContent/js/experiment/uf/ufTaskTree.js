$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : "编号",
		field : "id"
	});
	
	   
	fields.push({
		title : "描述",
		field : "name"
	});
	
	  fields.push({
		title : "实验员",
		field : "reciveUser.name"
	});
	   
	fields.push({
		title : "实验时间",
		field : "reciveDate"
	});
	
	  fields.push({
		title : "下达人",
		field : "createUser.name"
	});
	   
	fields.push({
		title : "下达时间",
		field : "createDate"
	});
	
	  fields.push({
		title : "模板",
		field : "template.name"
	});
	   
	fields.push({
		title : "index",
		field : "indexa"
	});
	
	   
	fields.push({
		title : "状态",
		field : "state"
	});
	
	   
	fields.push({
		title : "状态名称",
		field : "stateName"
	});
	
	   
	fields.push({
		title : "备注",
		field : "note"
	});
	
	  fields.push({
		title : "实验组",
		field : "acceptUser.name"
	});
	   
	fields.push({
		title : "容器数量",
		field : "maxNum"
	});
	
	   
	fields.push({
		title : "指控品数量",
		field : "qcNum"
	});
	
	  fields.push({
		title : "父级",
		field : "parent.name"
	});
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/experiment/uf/ufTask/showUfTaskTreeJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮 
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/experiment/uf/ufTask/editUfTask.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/experiment/uf/ufTask/editUfTask.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/experiment/uf/ufTask/viewUfTask.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : "编号",
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "描述",
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : '实验员',
		"searchName" : 'reciveUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "实验时间",
		"searchName" : 'reciveDate',
		"type" : "input"
	});
	
	   fields.push({
		header : '下达人',
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "下达时间",
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   fields.push({
		header : '模板',
		"searchName" : 'template.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "index",
		"searchName" : 'indexa',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态",
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态名称",
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "备注",
		"searchName" : 'note',
		"type" : "input"
	});
	
	   fields.push({
		header : '实验组',
		"searchName" : 'acceptUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "容器数量",
		"searchName" : 'maxNum',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "指控品数量",
		"searchName" : 'qcNum',
		"type" : "input"
	});
	
	   fields.push({
		header : '父级',
		"searchName" : 'parent.name',
		"type" : "input"
	});

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

