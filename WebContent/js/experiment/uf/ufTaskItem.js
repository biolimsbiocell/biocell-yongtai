var ufTaskItemTable;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#ufTask_id").val();
	var tbarOpts = [];
	var colOpts = [];
	
	   colOpts.push({
		"data":"id",
		"title": "编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
				
	    },
		
		"visible": false,	
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"name",
		"title": "描述",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
				
	    },
		
		
		
		"className": "textarea"
		
	});
	   colOpts.push({
		"data":"orderNumber",
		"title": "序号",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderNumber");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"code",
		"title": "样本编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"sampleCode",
		"title": "原始样本编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"indexa",
		"title": "index",
		"createdCell": function(td) {
			$(td).attr("saveName", "indexa");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"concentration",
		"title": "浓度",
		"createdCell": function(td) {
			$(td).attr("saveName", "concentration");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"result",
		"title": "处理结果",
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"reason",
		"title": "失败原因",
		"createdCell": function(td) {
			$(td).attr("saveName", "reason");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"stepNum",
		"title": "步骤编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "stepNum");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
				
	    },
		
		
		
		"className": "textarea"
		
	});
	   colOpts.push({
		"data":"patientName",
		"title": "患者姓名",
		"createdCell": function(td) {
			$(td).attr("saveName", "patientName");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"productId",
		"title": "检测项目编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"productName",
		"title": "检测项目名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"inspectDate",
		"title": "取样日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "inspectDate");
				
	    },
		
		
		"className": "date"

	});
	   colOpts.push({
		"data":"acceptDate",
		"title": "接收日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptDate");
				
	    },
		
		
		"className": "date"

	});
	   colOpts.push({
		"data":"idCard",
		"title": "身份证",
		"createdCell": function(td) {
			$(td).attr("saveName", "idCard");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"phone",
		"title": "手机号",
		"createdCell": function(td) {
			$(td).attr("saveName", "phone");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"orderId",
		"title": "关联任务单",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderId");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"sequenceFun",
		"title": "检测方法",
		"createdCell": function(td) {
			$(td).attr("saveName", "sequenceFun");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"reportDate",
		"title": "应出报告日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "reportDate");
				
	    },
		
		
		"className": "date"

	});
	   colOpts.push({
		"data":"state",
		"title": "状态",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
				
	    },
		
		
		
		"className": "edit"
		
	});

	
		colOpts.push( {
		"data": "ufTask-id",
		"title": "相关主表ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "ufTask-id");
		}
	});
	
	colOpts.push( {
		"data": "ufTask-name",
		"title": "相关主表",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "ufTask-name");
			$(td).attr("ufTask-id", rowData['ufTaskr-id']);
		}
	});
	
	    
	   colOpts.push({
		"data":"rowCode",
		"title": "行号",
		"createdCell": function(td) {
			$(td).attr("saveName", "rowCode");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"colCode",
		"title": "列号",
		"createdCell": function(td) {
			$(td).attr("saveName", "colCode");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"volume",
		"title": "体积",
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"sampleNum",
		"title": "样本用量",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleNum");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"addVolume",
		"title": "补充体积",
		"createdCell": function(td) {
			$(td).attr("saveName", "addVolume");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"sumVolume",
		"title": "总体积",
		"createdCell": function(td) {
			$(td).attr("saveName", "sumVolume");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"counts",
		"title": "板号",
		"createdCell": function(td) {
			$(td).attr("saveName", "counts");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"unit",
		"title": "单位",
		"createdCell": function(td) {
			$(td).attr("saveName", "unit");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"projectId",
		"title": "项目编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "projectId");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"contractId",
		"title": "合同编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "contractId");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"orderType",
		"title": "任务单类型",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderType");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"jkTaskId",
		"title": "建库任务单编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "jkTaskId");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"classify",
		"title": "区分临床还是科技服务 0 临床 1 科技服务",
		"createdCell": function(td) {
			$(td).attr("saveName", "classify");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"dataType",
		"title": "数据类型",
		"createdCell": function(td) {
			$(td).attr("saveName", "dataType");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"dataNum",
		"title": "数据量",
		"createdCell": function(td) {
			$(td).attr("saveName", "dataNum");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"productNum",
		"title": "中间产物数量",
		"createdCell": function(td) {
			$(td).attr("saveName", "productNum");
				
	    },
		
		
		
		"className": "edit"
		
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: '添加明细',
		action: function() {
			addItem($("#ufTaskItemTable"))
	}
	});
	tbarOpts.push({
		text: '弹框添加',
		action: function() {
			addItemLayer($("#ufTaskItemTable"))
		}
	});
	tbarOpts.push({
		text: '弹框编辑',
		action: function() {
			editItemLayer($("#ufTaskItemTable"))
		}
	});
	tbarOpts.push({
		text: '批量上传',
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#ufTaskItem_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/uf/ufTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 ufTaskItemTable.ajax.reload();
						} else {
							top.layer.msg("上传失败！")
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: '保存',
		action: function() {
			saveUfTaskItem($("#ufTaskItemTable"));
		}
	});
	}
	
	var ufTaskItemOptions = table(true,
		id,
		'/experiment/uf/ufTask/showUfTaskItemTableJson.action', colOpts, tbarOpts)
	ufTaskItemTable = renderData($("#ufTaskItemTable"), ufTaskItemOptions);

	// 上一步下一步，子表打开注释一次
	stepViewChange();
});

// 保存
function saveUfTaskItem(ele) {
	var data = saveUfTaskItemjson(ele);
	$.ajax({
		type: 'post',
		url: '/experiment/uf/ufTask/saveUfTaskItemTable.action',
		data: {
			id: $("#ufTask_id").val(),
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg("保存成功");
				tableRefresh();
			} else {
				top.layer.msg("保存失败")
			};
		}
	})
}
// 获得保存时的json数据
function saveUfTaskItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "ufTask-name") {
				json["ufTask-id"] = $(tds[j]).attr("ufTask-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
