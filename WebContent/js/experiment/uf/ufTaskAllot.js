var ufTaskAllotTab;
$(function() {
	var colOpts=[]
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": biolims.common.id,
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
	})
	var tbarOpts=[]
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	var options = table(true,null,"/experiment/uf/ufTask/showUfTaskTempTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	ufTaskAllotTab=renderData($("#main"),options);
	
	
	//提示样本选中数量
	var selectednum=$(".selectednum").text();
		if(!selectednum){
			selectednum=0;
	}
	ufTaskAllotTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			$(".selectednum").text(index+parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//sop和实验员的点击操作
	sopAndUserHandler(selectednum);
	//保存操作
	allotSave();
});
//sop和实验员的点击操作
function sopAndUserHandler(selectednum) {
	//执行sop点击选择操作
	$(".sopLi").click(function() {
		var length = $(".selected").length;
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		$(".label-warning").removeClass("selectednum").html("");
		$(this).children(".label-warning").addClass("selectednum").text(length+parseInt(selectednum));
		if($(this).children(".label-primary").text()) {
			if($("#maxNum").val() == "0") {
				$("#maxNum").val("1");
			}
		} else {
			$("#maxNum").val("0");
		}
	});
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
			var img = $(this).children("img");
			if(img.hasClass("userChosed")) {
				img.removeClass("userChosed");
			} else {
				img.addClass("userChosed");
			}		
	});
}
//保存操作
function allotSave() {
	$("#save").click(function() {
		var changeLog = "";
		if(!$(".sopChosed").length) {
			top.layer.msg(biolims.common.selectTaskModel);
			return false;
		}
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		//获得选中样本的ID
		var sampleId = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});
		//获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			userId.push($(val).parent("li").attr("userid"));
		});
		changeLog = getChangeLog(userId, changeLog);
		
		
		//拼接主表的信息
		var main = {
			id: $("#ufTask_id").text(),
			name: $("#ufTask_name").val(),
			"createUser-id": $("#ufTask_createUser").attr("userId"),
			createDate: $("#ufTask_createDate").text(),
			state: $("#ufTask_state").attr("state"),
			stateName: $("#ufTask_state").text(),
			sampleNum:$(".selectednum").text(),
			maxNum:$("#maxNum").val()
		};
		var data = JSON.stringify(main);
		//拼接保存的data
		var info = {
			temp: sampleId,
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data,
			logInfo: changeLog
		}
		$.ajax({
		type : 'post',
		url:ctx+"/experiment/uf/ufTask/saveAllot.action",
		data:info,
		success:function(data){
			var data = JSON.parse(data);
			if(data.success){				
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
			+ '/experiment/uf/ufTask/editUfTask.action?id=' + data.id;
			}
		}
		})
	});
}
//下一步操作
$("#next").click(function () {
	var ufTask_id=$("#ufTask_id").text();
	if(ufTask_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/uf/ufTask/showUfTaskItemTable.action?id="+ufTask_id+"&bpmTaskId="+$("#bpmTaskId").val();
});
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"type" : "table",
		"table" : ufTaskAllotTab
	} ];
}
//biolims.common.selected
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layer) {
			var arr = $("#checkCode",parent.document).val().split("\n");
			ufTaskAllotTab.settings()[0].ajax.data = {"codes":arr};
			ufTaskAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
//获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { //获取待处理样本的修改日志
	var sampleNew = [];
	$(".selected").each(function(i, val) {
		sampleNew.push($(val).children("td").eq(1).text());
	});
	changeLog += '"待处理样本"新增有：' + sampleNew + ";";
	//获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//获取模板sop的修改日志
	if($(".sopChosed").attr("sopid") != $("#sopChangelogId").val()) {
		changeLog += '实验模板:由"' + $("#sopChangelogName").val() + '"变为"' + $(".sopChosed").children('.text').text() + '";';
		changeLog += '样本数量:由"' + $("#sopChangelogSampleNum").val() + '"变为"' + $(".sopChosed").children('.badge').text() + '";';
	}
	//获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '实验员:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}
