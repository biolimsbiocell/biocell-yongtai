var ufTaskTempTable;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#ufTask_id").val();
	var tbarOpts = [];
	var colOpts = [];
	
	   colOpts.push({
		"data":"id",
		"title": "编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		
		"visible": false,	
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"name",
		"title": "描述",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"code",
		"title": "样本编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"sampleCode",
		"title": "原始样本编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"patientName",
		"title": "患者姓名",
		"createdCell": function(td) {
			$(td).attr("saveName", "patientName");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"productId",
		"title": "检测项目编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"productName",
		"title": "检测项目名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"inspectDate",
		"title": "取样日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "inspectDate");
	    },
		
		
		"className": "date",
			"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	   colOpts.push({
		"data":"acceptDate",
		"title": "接收日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptDate");
	    },
		
		
		"className": "date",
			"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	   colOpts.push({
		"data":"volume",
		"title": "体积",
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"unit",
		"title": "单位",
		"createdCell": function(td) {
			$(td).attr("saveName", "unit");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"idCard",
		"title": "身份证",
		"createdCell": function(td) {
			$(td).attr("saveName", "idCard");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"sequenceFun",
		"title": "检测方法",
		"createdCell": function(td) {
			$(td).attr("saveName", "sequenceFun");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"reportDate",
		"title": "应出报告日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "reportDate");
	    },
		
		
		"className": "date",
			"render": function(data) {
			if(data) {
				return parent.moment(data).format('YYYY-MM-DD');
			} else {
				return "";
			}
		}
	});
	   colOpts.push({
		"data":"phone",
		"title": "手机号",
		"createdCell": function(td) {
			$(td).attr("saveName", "phone");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"orderId",
		"title": "任务单",
		"createdCell": function(td) {
			$(td).attr("saveName", "orderId");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"state",
		"title": "状态",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"note ",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note ");
	    },
		
		
		"className": "edit",
	});
	   colOpts.push({
		"data":"classify",
		"title": "区分临床还是科技服务 0临床 1科技服务",
		"createdCell": function(td) {
			$(td).attr("saveName", "classify");
	    },
		
		
		"className": "edit",
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: '添加明细',
		action: function() {
			addItem($("#ufTaskTempTable"))
	}
	});
	tbarOpts.push({
		text: '弹框添加',
		action: function() {
			addItemLayer($("#ufTaskTempTable"))
		}
	});
	tbarOpts.push({
		text: '弹框编辑',
		action: function() {
			editItemLayer($("#ufTaskTempTable"))
		}
	});
	tbarOpts.push({
		text: '批量上传',
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#ufTaskTemp_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/uf/ufTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 ufTaskTempTable.ajax.reload();
						} else {
							top.layer.msg("上传失败！")
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: '保存',
		action: function() {
			saveUfTaskTemp($("#ufTaskTempTable"));
		}
	});
	}
	
	var ufTaskTempOptions = table(true,
		id,
		'/experiment/uf/ufTask/showUfTaskTempTableJson.action', colOpts, tbarOpts)
	ufTaskTempTable = renderData($("#ufTaskTempTable"), ufTaskTempOptions);

	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveUfTaskTemp(ele) {
	var data = saveUfTaskTempjson(ele);
	$.ajax({
		type: 'post',
		url: '/experiment/uf/ufTask/saveUfTaskTempTable.action',
		data: {
			id: $("#ufTask_id").val(),
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg("保存成功");
				tableRefresh();
			} else {
				top.layer.msg("保存失败")
			};
		}
	})
}
// 获得保存时的json数据
function saveUfTaskTempjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
