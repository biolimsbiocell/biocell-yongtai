var ufTaskTable;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.ufTask.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false,	
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.ufTask.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "reciveUser-id",
		"title": "实验员ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "reciveUser-id");
		}
	});
	colOpts.push( {
		"data": "reciveUser-name",
		"title": biolims.ufTask.reciveUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "reciveUser-name");
			$(td).attr("reciveUser-id", rowData['reciveUserr-id']);
		}
	});
	   colOpts.push({
		"data":"reciveDate",
		"title": biolims.ufTask.reciveDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "reciveDate");
	    },
		"className": "date"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "下达人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.ufTask.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUserr-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.ufTask.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	colOpts.push( {
		"data": "template-id",
		"title": "模板ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "template-id");
		}
	});
	colOpts.push( {
		"data": "template-name",
		"title": biolims.ufTask.template,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "template-name");
			$(td).attr("template-id", rowData['template-id']);
		}
	});
	   colOpts.push({
		"data":"indexa",
		"title": biolims.ufTask.indexa,
		"createdCell": function(td) {
			$(td).attr("saveName", "indexa");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.ufTask.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.ufTask.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.ufTask.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "acceptUser-id",
		"title": "实验组ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptUser-id");
		}
	});
	colOpts.push( {
		"data": "acceptUser-name",
		"title": biolims.ufTask.acceptUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "acceptUser-name");
			$(td).attr("acceptUser-id", rowData['acceptUserr-id']);
		}
	});
	   colOpts.push({
		"data":"maxNum",
		"title": biolims.ufTask.maxNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "maxNum");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"qcNum",
		"title": biolims.ufTask.qcNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "qcNum");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "parent-id",
		"title": "父级ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "parent-id");
		}
	});
	colOpts.push( {
		"data": "parent-name",
		"title": biolims.ufTask.parent,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "parent-name");
			$(td).attr("parent-id", rowData['parentr-id']);
		}
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#ufTaskTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#ufTaskTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#ufTaskTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#ufTask_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/uf/ufTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 ufTaskTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveUfTask($("#ufTaskTable"));
		}
	});
	}
	
	var ufTaskOptions = 
	table(true, "","/experiment/uf/ufTask/showUfTaskTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	ufTaskTable = renderData($("#ufTaskTable"), ufTaskOptions);

	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveUfTask(ele) {
	var data = saveUfTaskjson(ele);
	$.ajax({
		type: 'post',
		url: '/experiment/uf/ufTask/saveUfTaskTable.action',
		data: {
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveUfTaskjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "reciveUser-name") {
				json["reciveUser-id"] = $(tds[j]).attr("reciveUser-id");
				continue;
			}
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			
			if(k == "template-name") {
				json["template-id"] = $(tds[j]).attr("template-id");
				continue;
			}
			
			if(k == "acceptUser-name") {
				json["acceptUser-id"] = $(tds[j]).attr("acceptUser-id");
				continue;
			}
			
			if(k == "parent-name") {
				json["parent-id"] = $(tds[j]).attr("parent-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
