var ufTaskTemplateTable;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#ufTask_id").val();
	var tbarOpts = [];
	var colOpts = [];
	
	   colOpts.push({
		"data":"id",
		"title": "编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
				
	    },
		
		"visible": false,	
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"name",
		"title": "描述",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
				
	    },
		
		
		
		"className": "textarea"
		
	});

	
		colOpts.push( {
		"data": "reciveUser-id",
		"title": "实验员ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "reciveUser-id");
		}
	});
	
	colOpts.push( {
		"data": "reciveUser-name",
		"title": "实验员",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "reciveUser-name");
			$(td).attr("reciveUser-id", rowData['reciveUserr-id']);
		}
	});
	
	    
	   colOpts.push({
		"data":"tItem",
		"title": "模板步骤编id",
		"createdCell": function(td) {
			$(td).attr("saveName", "tItem");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"code",
		"title": "步骤编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"stepName",
		"title": "步骤名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "stepName");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"startTime",
		"title": "开始时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "startTime");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"endTime ",
		"title": "结束时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "endTime ");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"state",
		"title": "状态",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
				
	    },
		
		
		
		"className": "textarea"
		
	});
	   colOpts.push({
		"data":"sampleCodes",
		"title": "关联样本",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCodes");
				
	    },
		
		
		
		"className": "edit"
		
	});

	
		colOpts.push( {
		"data": "ufTask-id",
		"title": "相关主表ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "ufTask-id");
		}
	});
	
	colOpts.push( {
		"data": "ufTask-name",
		"title": "相关主表",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "ufTask-name");
			$(td).attr("ufTask-id", rowData['ufTaskr-id']);
		}
	});
	
	    
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: '添加明细',
		action: function() {
			addItem($("#ufTaskTemplateTable"))
	}
	});
	tbarOpts.push({
		text: '弹框添加',
		action: function() {
			addItemLayer($("#ufTaskTemplateTable"))
		}
	});
	tbarOpts.push({
		text: '弹框编辑',
		action: function() {
			editItemLayer($("#ufTaskTemplateTable"))
		}
	});
	tbarOpts.push({
		text: '批量上传',
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#ufTaskTemplate_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/uf/ufTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 ufTaskTemplateTable.ajax.reload();
						} else {
							top.layer.msg("上传失败！")
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: '保存',
		action: function() {
			saveUfTaskTemplate($("#ufTaskTemplateTable"));
		}
	});
	}
	
	var ufTaskTemplateOptions = table(true,
		id,
		'/experiment/uf/ufTask/showUfTaskTemplateTableJson.action', colOpts, tbarOpts)
	ufTaskTemplateTable = renderData($("#ufTaskTemplateTable"), ufTaskTemplateOptions);

	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveUfTaskTemplate(ele) {
	var data = saveUfTaskTemplatejson(ele);
	$.ajax({
		type: 'post',
		url: '/experiment/uf/ufTask/saveUfTaskTemplateTable.action',
		data: {
			id: $("#ufTask_id").val(),
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg("保存成功");
				tableRefresh();
			} else {
				top.layer.msg("保存失败")
			};
		}
	})
}
// 获得保存时的json数据
function saveUfTaskTemplatejson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "reciveUser-name") {
				json["reciveUser-id"] = $(tds[j]).attr("reciveUser-id");
				continue;
			}
			
			if(k == "ufTask-name") {
				json["ufTask-id"] = $(tds[j]).attr("ufTask-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
