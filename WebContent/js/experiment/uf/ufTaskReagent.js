var ufTaskReagentTable;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#ufTask_id").val();
	var tbarOpts = [];
	var colOpts = [];
	
	   colOpts.push({
		"data":"id",
		"title": "编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
				
	    },
		
		"visible": false,	
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"name",
		"title": "描述",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
				
	    },
		
		
		
		"className": "textarea"
		
	});
	   colOpts.push({
		"data":"code",
		"title": "原辅料编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"batch",
		"title": "批次",
		"createdCell": function(td) {
			$(td).attr("saveName", "batch");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"count",
		"title": "数量",
		"createdCell": function(td) {
			$(td).attr("saveName", "count");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"oneNum",
		"title": "单个用量",
		"createdCell": function(td) {
			$(td).attr("saveName", "oneNum");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"sampleNum",
		"title": "样本数量",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleNum");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"num",
		"title": "用量",
		"createdCell": function(td) {
			$(td).attr("saveName", "num");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"isGood",
		"title": "是否通过检验",
		"createdCell": function(td) {
			$(td).attr("saveName", "isGood");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
				
	    },
		
		
		
		"className": "textarea"
		
	});

	
		colOpts.push( {
		"data": "ufTask-id",
		"title": "相关主表ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "ufTask-id");
		}
	});
	
	colOpts.push( {
		"data": "ufTask-name",
		"title": "相关主表",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "ufTask-name");
			$(td).attr("ufTask-id", rowData['ufTaskr-id']);
		}
	});
	
	    
	   colOpts.push({
		"data":"itemId",
		"title": "关联步骤id",
		"createdCell": function(td) {
			$(td).attr("saveName", "itemId");
				
	    },
		
		
		
		"className": "edit"
		
	});
	   colOpts.push({
		"data":"tReagent",
		"title": "模板原辅料id",
		"createdCell": function(td) {
			$(td).attr("saveName", "tReagent");
				
	    },
		
		
		
		"className": "edit"
		
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: '添加明细',
		action: function() {
			addItem($("#ufTaskReagentTable"))
	}
	});
	tbarOpts.push({
		text: '弹框添加',
		action: function() {
			addItemLayer($("#ufTaskReagentTable"))
		}
	});
	tbarOpts.push({
		text: '弹框编辑',
		action: function() {
			editItemLayer($("#ufTaskReagentTable"))
		}
	});
	tbarOpts.push({
		text: '批量上传',
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#ufTaskReagent_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/uf/ufTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 ufTaskReagentTable.ajax.reload();
						} else {
							top.layer.msg("上传失败！")
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: '保存',
		action: function() {
			saveUfTaskReagent($("#ufTaskReagentTable"));
		}
	});
	}
	
	var ufTaskReagentOptions = table(true,
		id,
		'/experiment/uf/ufTask/showUfTaskReagentTableJson.action', colOpts, tbarOpts)
	ufTaskReagentTable = renderData($("#ufTaskReagentTable"), ufTaskReagentOptions);

	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveUfTaskReagent(ele) {
	var data = saveUfTaskReagentjson(ele);
	$.ajax({
		type: 'post',
		url: '/experiment/uf/ufTask/saveUfTaskReagentTable.action',
		data: {
			id: $("#ufTask_id").val(),
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg("保存成功");
				tableRefresh();
			} else {
				top.layer.msg("保存失败")
			};
		}
	})
}
// 获得保存时的json数据
function saveUfTaskReagentjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "ufTask-name") {
				json["ufTask-id"] = $(tds[j]).attr("ufTask-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
