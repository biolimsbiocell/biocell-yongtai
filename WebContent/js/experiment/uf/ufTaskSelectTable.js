var ufTaskTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.ufTask.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.ufTask.name,
	});
	
	    fields.push({
		"data":"reciveUser-id",
		"title":"实验员ID"
	});
	    fields.push({
		"data":"reciveUser-name",
		"title":biolims.ufTask.reciveUser
	});
	
	    fields.push({
		"data":"reciveDate",
		"title":biolims.ufTask.reciveDate,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":"下达人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.ufTask.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.ufTask.createDate,
	});
	
	    fields.push({
		"data":"template-id",
		"title":"模板ID"
	});
	    fields.push({
		"data":"template-name",
		"title":biolims.ufTask.template
	});
	
	    fields.push({
		"data":"indexa",
		"title":biolims.ufTask.indexa,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.ufTask.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.ufTask.stateName,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.ufTask.note,
	});
	
	    fields.push({
		"data":"acceptUser-id",
		"title":"实验组ID"
	});
	    fields.push({
		"data":"acceptUser-name",
		"title":biolims.ufTask.acceptUser
	});
	
	    fields.push({
		"data":"maxNum",
		"title":biolims.ufTask.maxNum,
	});
	
	    fields.push({
		"data":"qcNum",
		"title":biolims.ufTask.qcNum,
	});
	
	    fields.push({
		"data":"parent-id",
		"title":"父级ID"
	});
	    fields.push({
		"data":"parent-name",
		"title":biolims.ufTask.parent
	});
	
	var options = table(true, "","/experiment/uf/ufTask/showUfTaskTableJson.action",
	 fields, null)
	ufTaskTable = renderData($("#addUfTaskTable"), options);
	$('#addUfTaskTable').on('init.dt', function() {
		recoverSearchContent(ufTaskTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.ufTask.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.ufTask.name
		});
	fields.push({
	    "type":"input",
		"searchName":"reciveUser.id",
		"txt":"实验员ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"reciveUser.name",
		"txt":biolims.ufTask.reciveUser
	});
	   fields.push({
		    "searchName":"reciveDate",
			"type":"input",
			"txt":biolims.ufTask.reciveDate
		});
	fields.push({
			"txt": "实验时间(Start)",
			"type": "dataTime",
			"searchName": "reciveDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "实验时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "reciveDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"下达人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.ufTask.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.ufTask.createDate
		});
	fields.push({
			"txt": "下达时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "下达时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	fields.push({
	    "type":"input",
		"searchName":"template.id",
		"txt":"模板ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"template.name",
		"txt":biolims.ufTask.template
	});
	   fields.push({
		    "searchName":"indexa",
			"type":"input",
			"txt":biolims.ufTask.indexa
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.ufTask.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.ufTask.stateName
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.ufTask.note
		});
	fields.push({
	    "type":"input",
		"searchName":"acceptUser.id",
		"txt":"实验组ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"acceptUser.name",
		"txt":biolims.ufTask.acceptUser
	});
	   fields.push({
		    "searchName":"maxNum",
			"type":"input",
			"txt":biolims.ufTask.maxNum
		});
	   fields.push({
		    "searchName":"qcNum",
			"type":"input",
			"txt":biolims.ufTask.qcNum
		});
	fields.push({
	    "type":"input",
		"searchName":"parent.id",
		"txt":"父级ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"parent.name",
		"txt":biolims.ufTask.parent
	});
	
	fields.push({
		"type":"table",
		"table":ufTaskTable
	});
	return fields;
}
