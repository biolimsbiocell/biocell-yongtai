var oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "dicSampleType-id",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data": "dicSampleType-name",
		"title":biolims.common.dicSampleTypeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-name");
		}
	})
	colOpts.push({
		"data": "concentration",
		"title": biolims.common.concentration,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "concentration");
		}
	})
	colOpts.push({
		"data": "volume",
		"title": biolims.common.volume,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
		}
	})
	colOpts.push({
		"data": "result",
		"title": biolims.common.result,
		"className":"select",
		"name":biolims.common.qualified+"|"+biolims.common.disqualified,
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", biolims.common.qualified+"|"+biolims.common.disqualified);
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}
		}
	})
	colOpts.push({
		"data": "nextFlowId",
		"title": biolims.common.nextFlowId,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
		"data": "nextFlow",
		"title": biolims.common.nextFlow,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlow");
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#ufTaskResultdiv"),
				"/experiment/uf/ufTask/delUfTaskResult.action","样本处理结果删除样本：",$("#ufTask_id").text());
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-th"></i>'+biolims.common.applicationOper,
		action: function() {
			$.ajax({
				type: "post",
				data: {
					id:$("#ufTask_id").text()
				},
				url: ctx + "/experiment/uf/ufTask/bringResult.action",
				success: function(data) {
					var data = JSON.parse(data)
					if(data.success) {
						ufTaskResultTab.ajax.reload();
					} else {
						top.layer.msg(biolims.purchase.failed)
					}
	
				}
			});
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-share-alt"></i>'+biolims.common.uploadResult,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#ufTask_id").text(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/uf/ufTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							ufTaskResultTab.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.batchResult,
		className: 'btn btn-sm btn-success resultsBatchBtn',
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-floppy-open"></i>'+biolims.common.uploadAttachment,
		action: function() {
			$("#uploadFile").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput =fileInput('1', 'ufTask', $("#ufTask_id").text());
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-paypal"></i> '+biolims.common.selectNextFlow,
		action: function() {
			nextFlow();
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem($("#ufTaskResultdiv"));
		}
	});
	var ufTaskResultOps = table(true, $("#ufTask_id").text(), "/experiment/uf/ufTask/showUfTaskResultTableJson.action", colOpts, tbarOpts);
	ufTaskResultTab = renderData($("#ufTaskResultdiv"), ufTaskResultOps);
	ufTaskResultTab.on('draw', function() {
		oldChangeLog = ufTaskResultTab.ajax.json();
	});
	
	//批量结果
	btnChangeDropdown ($('#ufTaskResultdiv'),$(".resultsBatchBtn"),[biolims.common.qualified,biolims.common.disqualified],"result");
	bpmTask($("#bpmTaskId").val());
	//上一步下一步操作
	preAndNext();
});
//下一步流向
function nextFlow() {
	var rows = $("#ufTaskResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var productId = "";
	var sampleType="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		sampleType = $(k).find("td[savename='dicSampleType-id']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=UfTask&productId="
					+ productIds+"&sampleType="+sampleType,''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}
// 保存
function saveItem(ele) {
	var changeLog = "实验结果：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/experiment/uf/ufTask/saveResult.action',
		data: {
			id: $("#ufTask_id").text(),
			dataJson: data,
			logInfo: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断男女并转换为数字
			if(k == "result") {
				var result = $(tds[j]).text();
				if(result == biolims.common.disqualified) {
					json[k] = "0";
				} else if(result == biolims.common.qualified) {
					json[k] = "1";
				}
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/uf/ufTask/showUfTaskSteps.action?id=" + $("#ufTask_id").text()+"&bpmTaskId="+$("#bpmTaskId").val();
	});
	//上一步操作
	$("#finish").click(function() {
	var paraStr = "formId=" + $("#ufTask_id").text() +
		"&tableId=UfTask";
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: '13',
					formId: $("#ufTask_id").text()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.close(index);
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});
});
	
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

