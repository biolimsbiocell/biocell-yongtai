var ufTaskCosTable;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#ufTask_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.ufTaskCos.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false,	
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.ufTaskCos.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"code",
		"title": biolims.ufTaskCos.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"temperature",
		"title": biolims.ufTaskCos.temperature,
		"createdCell": function(td) {
			$(td).attr("saveName", "temperature");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"speed",
		"title": biolims.ufTaskCos.speed,
		"createdCell": function(td) {
			$(td).attr("saveName", "speed");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"time",
		"title": biolims.ufTaskCos.time,
		"createdCell": function(td) {
			$(td).attr("saveName", "time");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.ufTaskCos.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"isGood",
		"title": biolims.ufTaskCos.isGood,
		"createdCell": function(td) {
			$(td).attr("saveName", "isGood");
	    },
		"className": "edit"
	});
	colOpts.push( {
		"data": "ufTask-id",
		"title": "相关主表ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "ufTask-id");
		}
	});
	colOpts.push( {
		"data": "ufTask-name",
		"title": biolims.ufTaskCos.ufTask,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "ufTask-name");
			$(td).attr("ufTask-id", rowData['ufTaskr-id']);
		}
	});
	   colOpts.push({
		"data":"itemId",
		"title": biolims.ufTaskCos.itemId,
		"createdCell": function(td) {
			$(td).attr("saveName", "itemId");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"tCos",
		"title": biolims.ufTaskCos.tCos,
		"createdCell": function(td) {
			$(td).attr("saveName", "tCos");
	    },
		"className": "edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#ufTaskCosTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#ufTaskCosTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#ufTaskCosTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#ufTaskCos_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/uf/ufTask/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 ufTaskCosTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveUfTaskCos($("#ufTaskCosTable"));
		}
	});
	}
	
	var ufTaskCosOptions = table(true,
		id,
		'/experiment/uf/ufTask/showUfTaskCosTableJson.action', colOpts, tbarOpts)
	ufTaskCosTable = renderData($("#ufTaskCosTable"), ufTaskCosOptions);

	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveUfTaskCos(ele) {
	var data = saveUfTaskCosjson(ele);
	$.ajax({
		type: 'post',
		url: '/experiment/uf/ufTask/saveUfTaskCosTable.action',
		data: {
			id: $("#ufTask_id").val(),
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveUfTaskCosjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "ufTask-name") {
				json["ufTask-id"] = $(tds[j]).attr("ufTask-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
