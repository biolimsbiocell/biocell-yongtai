var blendTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'blendTask-id',
		type:"string"
	});
	    fields.push({
		name:'blendTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'步骤编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:'步骤名称',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*10
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤编id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:'关联样本',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'blendTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'blendTask-name',
		hidden : false,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/blend/blendTask/showBlendTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="执行步骤";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/blend/blendTask/delBlendTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
			text : '选择实验员',
				handler : selectreciveUserFun
		});
	opts.tbar.push({
			text : '选择相关主表',
				handler : selectblendTaskDialogFun
		});
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = blendTaskTemplateGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
							blendTaskTemplateGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	blendTaskTemplateGrid=gridEditTable("blendTaskTemplatediv",cols,loadParam,opts);
	$("#blendTaskTemplatediv").data("blendTaskTemplateGrid", blendTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectreciveUserFun(){
	var win = Ext.getCmp('selectreciveUser');
	if (win) {win.close();}
	var selectreciveUser= new Ext.Window({
	id:'selectreciveUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectreciveUser.close(); }  }]  });  
    selectreciveUser.show(); }
	function setreciveUser(rec){
		var gridGrid = $("#blendTaskTemplatediv").data("blendTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',rec.get('id'));
			obj.set('reciveUser-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectreciveUser')
		if(win){
			win.close();
		}
	}
	function selectreciveUserDialogFun(){
			var title = '';
			var url = '';
			title = "选择实验员";
			url = ctx + "/UserSelect.action?flag=reciveUser";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selreciveUserVal(this);
				}
			}, true, option);
		}
	var selreciveUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#blendTaskTemplatediv").data("blendTaskTemplateGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('reciveUser-id',rec.get('id'));
				obj.set('reciveUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
function selectblendTaskFun(){
	var win = Ext.getCmp('selectblendTask');
	if (win) {win.close();}
	var selectblendTask= new Ext.Window({
	id:'selectblendTask',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectblendTask.close(); }  }]  }) });  
    selectblendTask.show(); }
	function setblendTask(rec){
		var gridGrid = $("#blendTaskTemplatediv").data("blendTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('blendTask-id',rec.get('id'));
			obj.set('blendTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectblendTask')
		if(win){
			win.close();
		}
	}
	function selectblendTaskDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/BlendTaskSelect.action?flag=blendTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selblendTaskVal(this);
				}
			}, true, option);
		}
	var selblendTaskVal = function(win) {
		var operGrid = blendTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#blendTaskTemplatediv").data("blendTaskTemplateGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('blendTask-id',rec.get('id'));
				obj.set('blendTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
