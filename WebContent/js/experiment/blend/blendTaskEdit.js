$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#blendTask_state").val();
	if(id!="1"){
		load("/experiment/blend/blendTask/showBlendTaskTempList.action", null, "#blendTaskTempPage");
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#cfdnaTaskTempPage").remove();
	}
});	
function add() {
	window.location = window.ctx + "/experiment/blend/blendTask/editBlendTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/blend/blendTask/showBlendTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#blendTask", {
					userId : userId,
					userName : userName,
					formId : $("#blendTask_id").val(),
					title : $("#blendTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#blendTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var blendTaskItemDivData = $("#blendTaskItemdiv").data("blendTaskItemGrid");
		document.getElementById('blendTaskItemJson').value = commonGetModifyRecords(blendTaskItemDivData);
	    var blendTaskTemplateDivData = $("#blendTaskTemplatediv").data("blendTaskTemplateGrid");
		document.getElementById('blendTaskTemplateJson').value = commonGetModifyRecords(blendTaskTemplateDivData);
	    var blendTaskReagentDivData = $("#blendTaskReagentdiv").data("blendTaskReagentGrid");
		document.getElementById('blendTaskReagentJson').value = commonGetModifyRecords(blendTaskReagentDivData);
	    var blendTaskCosDivData = $("#blendTaskCosdiv").data("blendTaskCosGrid");
		document.getElementById('blendTaskCosJson').value = commonGetModifyRecords(blendTaskCosDivData);
	    var blendTaskResultDivData = $("#blendTaskResultdiv").data("blendTaskResultGrid");
		document.getElementById('blendTaskResultJson').value = commonGetModifyRecords(blendTaskResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/blend/blendTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/blend/blendTask/copyBlendTask.action?id=' + $("#blendTask_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#blendTask_id").val() + "&tableId=blendTask");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#blendTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#blendTask_lane").val());
	nsc.push("lane号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'混合实验',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/blend/blendTask/showBlendTaskItemList.action", {
				id : $("#blendTask_id").val()
			}, "#blendTaskItempage");
load("/experiment/blend/blendTask/showBlendTaskTemplateList.action", {
				id : $("#blendTask_id").val()
			}, "#blendTaskTemplatepage");
load("/experiment/blend/blendTask/showBlendTaskReagentList.action", {
				id : $("#blendTask_id").val()
			}, "#blendTaskReagentpage");
load("/experiment/blend/blendTask/showBlendTaskCosList.action", {
				id : $("#blendTask_id").val()
			}, "#blendTaskCospage");
load("/experiment/blend/blendTask/showBlendTaskResultList.action", {
				id : $("#blendTask_id").val()
			}, "#blendTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);