var blendTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'lane',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	    fields.push({
		name:'reciveDate',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'lane',
		header:biolims.pooling.lane,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'reciveUser-id',
		hidden:true,
		header:biolims.common.testUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'reciveUser-name',
		header:biolims.common.testUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'reciveDate',
		header:biolims.common.testTime,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.common.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:biolims.common.templateId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:biolims.common.templateName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:biolims.common.acceptUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.acceptUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态名称',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/blend/blendTask/showBlendTaskListJson.action";
	var opts={};
	opts.title="混合实验";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	blendTaskGrid=gridTable("show_blendTask_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/experiment/blend/blendTask/editBlendTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/blend/blendTask/editBlendTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/blend/blendTask/viewBlendTask.action?id=' + id;
}
function exportexcel() {
	blendTaskGrid.title = '导出列表';
	var vExportContent = blendTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startreciveDate").val() != undefined) && ($("#startreciveDate").val() != '')) {
					var startreciveDatestr = ">=##@@##" + $("#startreciveDate").val();
					$("#reciveDate1").val(startreciveDatestr);
				}
				if (($("#endreciveDate").val() != undefined) && ($("#endreciveDate").val() != '')) {
					var endreciveDatestr = "<=##@@##" + $("#endreciveDate").val();

					$("#reciveDate2").val(endreciveDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(blendTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
