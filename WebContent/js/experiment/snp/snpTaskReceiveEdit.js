﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#snpTaskReceive_id").val();
	var state = $("#snpTaskReceive_state").val();
	if(id=="" || state == "3"){
		var type="2";
		load("/experiment/snp/snpTaskReceive/showSnpTaskReceiveItemListTo.action", {type:type}, "#SnpTaskReceviceLeftPage");
		$("#markup").css("width","75%");
	}
});

function add() {
	window.location = window.ctx + "/experiment/snp/snpTaskReceive/editSnpTaskReceive.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/snp/snpTaskReceive/showSnpTaskReceiveList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#snpTaskReceive", {
					userId : userId,
					userName : userName,
					formId : $("#snpTaskReceive_id").val(),
					title : $("#snpTaskReceive_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {
		completeTask($("#snpTaskReceive_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var snpTaskReceiveItemDivData = $("#snpTaskReceiveItemdiv").data("snpTaskReceiveItemGrid");
		document.getElementById('snpTaskReceiveItemJson').value = commonGetModifyRecords(snpTaskReceiveItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/snp/snpTaskReceive/save.action";
		form1.submit();	
		}
}	

function editCopy() {
	window.location = window.ctx + '/experiment/snp/snpTaskReceive/copySnpTaskReceive.action?id=' + $("#snpTaskReceive_id").val();
}

$("#toolbarbutton_status").click(function(){
	var selRecord = snpTaskReceiveItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var result = selRecord.getAt(j).get("method");
		if(result==""){
			message("处理结果不能为空！");
			return;
		}
	}
	if ($("#snpTaskReceive_id").val()){
		commonChangeState("formId=" + $("#snpTaskReceive_id").val() + "&tableId=SnpTaskReceive");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#snpTaskReceive_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#snpTaskReceive_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}

$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'样本接收',
	    	   contentEl:'markup'
	       } ]
	   });
});

load("/experiment/snp/snpTaskReceive/showSnpTaskReceiveItemList.action", {
				id : $("#snpTaskReceive_id").val()
			}, "#snpTaskReceiveItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);