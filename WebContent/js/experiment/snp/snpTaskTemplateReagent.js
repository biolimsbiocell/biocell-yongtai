﻿var snpTaskTemplateReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'tReagent',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'itemId',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'batch',
		type:"string"
	});
	fields.push({
		name:'isGood',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'snpTask-id',
		type:"string"
	});
	fields.push({
		name:'snpTask-name',
		type:"string"
	});
    fields.push({
		name:'oneNum',
		type:"string"
	});
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'num',
		type:"string"
	});
    fields.push({
		name:'sn',
		type:"string"
	});
    fields.push({
		name:'expireDate',
		type:"string"
	});
    fields.push({
		name:'reagentCode',
		type:"string"
	});
    fields.push({
		name:'isRunout',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'原辅料id',
		width:20*6
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:'模板原辅料Id',
		width:20*6
	});	
	var codes =new Ext.form.TextField({
        allowBlank: false
	});
	codes.on('focus', function() {
		var selectRecord = snpTaskTemplateReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var tid = $("#snpTask_template").val();
				loadReagentItemByCode(tid);
			});
		}
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'原辅料编号',
		width:20*6,
		editor : codes
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'模板步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'原辅料名称',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//鼠标单击触发事件 
	var batchs =new Ext.form.TextField({
            allowBlank: false
    });
	batchs.on('focus', function() {
		var selectRecord = snpTaskTemplateReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("code");
				loadStorageReagentBuy(code);
			});
		}
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:'批次',
		width:20*6,
		editor:batchs
	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '否' ], [ '1', '是' ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:'是否通过检验',
		width:20*6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : true,
		header:'单个用量(μl)',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:'用量(μl)',
		width:20*6
	});
	cm.push({
		dataIndex:'sn',editor : new Ext.form.TextField({ allowBlank : true }),
		hidden : false,
		header:'sn',
		width:20*6
	});
	cm.push({
		dataIndex:'expireDate',
		hidden : false,
		header:'过期日期',
		width:20*6
	});
	cm.push({
		dataIndex:'reagentCode',
		hidden : false,
		header:'原辅料编码',
		width:20*6
	});
	var isRunout = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name :biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isRunout',
		hidden : false,
		header:'是否用完',
		width:20*5,
		renderer: Ext.util.Format.comboRenderer(isRunout),
		editor: isRunout
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snp/snpTask/showSnpTaskTemplateReagentListJson.action?id="+$("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();	
	var opts={};
	opts.title="原辅料明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state=$("#snpTask_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snp/snpTask/delsnpTaskTemplateReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
			text : '填加明细',
			handler : function (){
				//获取选择的数据
				var selectRcords=snpTaskTemplateItemGrid.getSelectionModel().getSelections();
				//获取全部数据
				var allRcords=snpTaskTemplateItemGrid.store;
				//选中的数量
				var length1=selectRcords.length;
				//全部数据量
				var length2=allRcords.getCount();
				if(length2>0){
					if(length1==1){
						var code="";
						$.each(selectRcords, function(i, obj) {
							code=obj.get("code");
						});
						if(code!=""){
							showStorageList(code);
						}else{
							message("请先添加模板明细数据！");
							return;
						}				
					}else if(length1>1){
						message("模板明细中只能选择一条数据！");
						return;
					}else{
						message("请先选择模板明细中数据！");
						return;
					}
				}else{
					message("模板明细中数据为空！");
					return;
				}
			}
	});
	}
	snpTaskTemplateReagentGrid=gridEditTable("snpTaskTemplateReagentdiv",cols,loadParam,opts);
	$("#snpTaskTemplateReagentdiv").data("snpTaskTemplateReagentGrid", snpTaskTemplateReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:'选择采购原辅料',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}

function setStorageReagentBuy(rec){	
	var gridGrid = $("#snpTaskTemplateReagentdiv").data("snpTaskTemplateReagentGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}

//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, "库存主数据", url, {
		"确定" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					snpTaskTemplateReagentGrid.stopEditing();
					var ob = snpTaskTemplateReagentGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;
					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("itemId",code);				
					snpTaskTemplateReagentGrid.getStore().add(p);	
				});
				snpTaskTemplateReagentGrid.startEditing(0, 0);
				options.close();
			}else{
				message("请选择数据！");
			}
		}
	}, true, options);
}

//根据原辅料编号查询原辅料明细
function loadReagentItemByCode(tid){
		var options = {};
		options.width = 900;
		options.height = 460;
		var url="/system/template/template/showReagentItemByCodeList.action?tid="+tid;
		loadDialogPage(null, "选择明细", url, {
			 "确定": function() {
				 selRecord = reagentItem1Grid.getSelectionModel();
					if (selRecord.getSelections().length > 0) {
						$.each(selRecord.getSelections(), function(i, obj) {
							snpTaskTemplateReagentGrid.stopEditing();
							var ob = snpTaskTemplateReagentGrid.getStore().recordType;
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.get("code"));
							p.set("name",obj.get("name"));
							p.set("batch",obj.get("batch"));
							p.set("isGood",obj.get("isGood"));
							p.set("itemId",obj.get("itemId"));							
							p.set("oneNum",obj.get("num"));
							p.set("note",obj.get("note"));
							snpTaskTemplateReagentGrid.getStore().add(p);	
						});
						snpTaskTemplateReagentGrid.startEditing(0, 0);
						options.close();
					}else{
						message("请选择数据！");
					}
				 options.close();
			}
		}, true, options);
}