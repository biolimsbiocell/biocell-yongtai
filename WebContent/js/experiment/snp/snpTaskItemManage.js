//fluidigm实验明细
var snpTaskItemManagerGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
    	
        name:'id',
        type:"string"
    });
    fields.push({
	    name:'expCode',
        type:"string"
    });
    fields.push({
	    name:'code',
        type:"string"
    });
    fields.push({
	    name:'sampleCode',
        type:"string"
    });
    fields.push({
	    name:'sampleName',
        type:"string"
    });
    fields.push({
	    name:'sampleNum',
        type:"string"
    });
    fields.push({
	    name:'sampleVolume',
        type:"string"
    });
    fields.push({
	    name:'addVolume',
        type:"string"
    });
    fields.push({
	    name:'sumVolume',
        type:"string"
    });
    fields.push({
	    name:'indexs',
        type:"string"
    });
    fields.push({
	    name:'note',
        type:"string"
    });
    fields.push({
	    name:'nextFlow',
        type:"string"
    });
    fields.push({
	    name:'sampleId',
        type:"string"
    });
    fields.push({
        name:'state',
        type:"string"
    });
    fields.push({
	    name:'stateName',
        type:"string"
    });
    fields.push({
        name:'concentration',
        type:"string"
    });
    fields.push({
        name:'result',
        type:"string"
    });
    fields.push({
        name:'reason',
        type:"string"
    });
    fields.push({
        name:'stepNum',
        type:"string"
    });
    fields.push({
	    name:'patientName',
        type:"string"
    });
    fields.push({
	    name:'sequenceFun',
        type:"string"
    });   
    fields.push({
	    name:'productName',
        type:"string"
    });
    fields.push({
	    name:'productId',
        type:"string"
    });
    fields.push({
	    name:'inspectDate',
        type:"string"
    });
    fields.push({
	    name : 'orderId',
        type : "string"
    });
    fields.push({
	    name : 'idCard',
        type : "string"
    });
    fields.push({
	    name : 'phone',
        type : "string"
    });
    fields.push({
	    name : 'reportDate',
        type : "string"
    });
    fields.push({
	    name:'classify',
        type:"string"
    });
    fields.push({
	    name:'dicSampleType-id',
        type:"string"
    });
    fields.push({
	    name:'dicSampleType-name',
        type:"string"
    });
    fields.push({
	    name:'sampleCode',
        type:"string"
    });
    fields.push({
	    name:'sampleType',
        type:"string"
    });
    fields.push({
	    name:'sampleConsume',
         type:"string"
    });
    fields.push({
	    name:'labCode',
        type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'expCode',
		hidden : true,
		header:'实验编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		header:'手机号',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:'中间产物类型编号',
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:'中间产物类型',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'关联任务单',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleId',
		hidden : true,
		header:'样本Id',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleName',
		hidden : true,
		header:'样本名称',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : false,
		header:'样本用量',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:'取样体积',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : true,
		header:'补充体积（μl）',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : true,
		header:'总体积（μl）',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexs',
		hidden : true,
		header:'Index',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'浓度',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snp/snpTaskManage/showSnpTaskItemManagerListJson.action";
	var opts={};
	opts.title="fluidigm实验明细";
	opts.height =  document.body.clientHeight-240;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '批量入库',
		handler : ruku
	});
	opts.tbar.push({
		text : 'fluidigm实验待办',
		handler : tiqu
	});
	snpTaskItemManagerGrid=gridEditTable("snpTaskItemManagerdiv",cols,loadParam,opts);
	$("#snpTaskItemManagerdiv").data("snpTaskItemManagerGrid", snpTaskItemManagerGrid);
});

//保存Grid
function save1(){	
	var itemJson = commonGetModifyRecords(snpTaskItemManagerGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/snp/snpTaskManage/saveSnpTaskItemManager.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					snpTaskItemManagerGrid.getStore().commitChanges();
					snpTaskItemManagerGrid.getStore().reload();
					message("保存成功！");
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}

function selectInfo(){
	commonSearchAction(snpTaskItemManagerGrid);
	$("#snpTaskItemManagerGrid_code").val("");
	$("#snpTaskItemManagerGrid_Code").val("");
}

function ruku(){
	var selectRcords=snpTaskItemManagerGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		var ids="";
		$.each(selectRcords,function(i,obj){
			ids+=obj.get("id")+",";
		});
		ajax("post", "/experiment/snp/snpTaskManage/snpTaskManageItemRuku.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				snpTaskItemManagerGrid.getStore().reload();
				message("入库成功！");
			} else{
				message("入库失败！");
			}
		}, null);
	}else{
		message("请选择入库数据！");
	}
}

function tiqu(){
	var selectRcords=snpTaskItemManagerGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		for(var i=0;i<selectRcords.length;i++){
			ajax("post", "/experiment/snp/snpTaskManage/snpTaskManageItemTiqu.action", {
				id : selectRcords[i].get("id")
			}, function(data) {
				if (data.success) {
					snpTaskItemManagerGrid.getStore().reload();
					message("待办成功！");
				} else{
					message("待办失败！");
				}
			}, null);
		}
	}else{
		message("请选择待办数据！");
	}
}
