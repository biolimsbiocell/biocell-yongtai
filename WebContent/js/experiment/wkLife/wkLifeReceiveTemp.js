﻿
var wkReceiveTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
//   fields.push({
//		name:'idCard',
//		type:"string"
//	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});  
//	   fields.push({
//		name:'phone',
//		type:"string"
//	});
	   fields.push({
		name:'orderId',
		type:"string"
	});    
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
	    name:'unit',
	    type:"string"
    });
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'classify',
		type:"string"
	});   
	   fields.push({
			name:'sampleType',
			type:"string"
		});  
	   fields.push({
			name:'sampleNum',
			type:"string"
		}); 
	   fields.push({
			name:'labCode',
			type:"string"
		}); 
	   fields.push({
			name:'sampleInfo-id',
			type:"string"
		}); 
	   fields.push({
			name:'sampleInfo-note',
			type:"string"
		}); 
	   fields.push({
			name:'sampleInfo-receiveDate',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-reportDate',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6,
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:30*6,
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:30*6,
//	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : true,
		header:biolims.common.testProject,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:30*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:30*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:30*6,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6,
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:biolims.common.classify,
		width:20*6,
		hidden : true
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		header:biolims.common.openBoxId,
		width:20*6,
		hidden : true
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:biolims.common.sampleInfoReceiveDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:biolims.common.sampleInfoReportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		header:biolims.common.openBox,
		width:20*6,
		hidden : false
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wKReceive/showWkReceiveTempListJson.action";
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.dna.forExperimental;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = wkReceiveTempGrid.getAllRecord();
							var store = wkReceiveTempGrid.store;

							var isOper = true;
							var buf = [];
							wkReceiveTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								})
							});
							wkReceiveTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message(biolims.common.samplecodeComparison);
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							wkReceiveTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	wkReceiveTempGrid=gridEditTable("wkReceiveTempdiv",cols,loadParam,opts);
});

function addItem(){
	var selectRecord=wkReceiveTempGrid.getSelectionModel();
	var selRecord=wKReceiveItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;					
				}
			}
			if(!isRepeat){
			var ob = wKReceiveItemGrid.getStore().recordType;
			wKReceiveItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",obj.get("id"));
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("patientName",obj.get("patientName"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("inspectDate",obj.get("inspectDate"));
			p.set("acceptDate",obj.get("acceptDate"));
			p.set("idCard",obj.get("idCard"));
			p.set("sequenceFun",obj.get("sequenceFun"));
			p.set("reportDate",obj.get("reportDate"));
			p.set("phone",obj.get("phone"));
			p.set("orderId",obj.get("orderId"));
			p.set("note",obj.get("note"));
			p.set("state",'1');
			p.set("classify",obj.get("classify"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("sampleNum",obj.get("sampleNum"));
			p.set("labCode",obj.get("labCode"));
			p.set("sampleInfo-id",obj.get("sampleInfo-id"));
			p.set("sampleInfo-note",obj.get("sampleInfo-note"));
			p.set("sampleInfo-receiveDate",obj.get("sampleInfo-receiveDate"));
			p.set("sampleInfo-reportDate",obj.get("sampleInfo-reportDate"));
			wKReceiveItemGrid.getStore().add(p);
		}
			
		});
		wKReceiveItemGrid.startEditing(0, 0);
		}
	
}
