﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#wkReceive_stateName").val();
	if(id!=biolims.common.finish){
		load("/experiment/wkLife/wKReceive/showWkReceiveTempList.action", { }, "#wkReceiveTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#doclinks_img").css("display","none");
	}
});
function add() {
	window.location = window.ctx + "/experiment/wkLife/wKReceive/editWKReceive.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/wkLife/wKReceive/showWKReceiveList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#wKReceive", {
					userId : userId,
					userName : userName,
					formId : $("#wkReceive_id").val(),
					title : $("#wkReceive_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#wkReceive_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var wKReceiveItemDivData = $("#wKReceiveItemdiv").data("wKReceiveItemGrid");
		document.getElementById('wKReceiveItemJson').value = commonGetModifyRecords(wKReceiveItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/wkLife/wKReceive/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/wkLife/wKReceive/copyWKReceive.action?id=' + $("#wkReceive_id").val();
}
/*function changeState() {
	commonChangeState("formId=" + $("#wkReceive_id").val() + "&tableId=wKReceive");
}*/
$("#toolbarbutton_status").click(function(){
	var selRecord = wKReceiveItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var method=selRecord.getAt(j).get("method");
		if(method==""||method==null){
			message(biolims.common.resultsIsEmpty);
			return;
		}
	}
	if ($("#wkReceive_id").val()){
		commonChangeState("formId=" + $("#wkReceive_id").val() + "&tableId=WKReceive");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wkReceive_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.sampleReceive,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/wkLife/wKReceive/showWKReceiveItemList.action", {
				id : $("#wkReceive_id").val()
			}, "#wKReceiveItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	
	
	
	