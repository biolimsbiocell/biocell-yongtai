﻿var wKTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   	fields.push({
		name:'testUserId',
		type:"string"
	});
	    fields.push({
		name:'testUserName',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string",
		
	});
	   fields.push({
		name:'endTime',
		type:"string",
		
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.stepDetail,
		width:100*6,
		editor : new Ext.form.HtmlEditor({
			readOnly : true
		})
	});
	cm.push({
		dataIndex:'testUserId',
		hidden : true,
		header:biolims.common.testUserId,
		width:20*10
		
	});
	//鼠标聚焦时触发事件 
	var reciveUser =new Ext.form.TextField({
            allowBlank: false
    });
	reciveUser.on('focus', function() {
		
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUserName',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*6,

		editor : reciveUser
	});
	/*cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*10
	});*/
	
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:biolims.common.templateProcessId,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:biolims.common.startTime,
		width:20*6,
		
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:biolims.common.endTime,
		width:20*6,
		
	});
	
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:biolims.common.relateSample,
		width:20*20,
		
	});
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/showWKTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.executionStep;
	opts.height =  document.body.clientHeight*0.8;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/wkLife/delWKTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : biolims.common.fillDetail,
		handler : templateSelect
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		text : biolims.common.testUserName,
		handler : loadTestUser
	});
	
	if($("#wk_id").val()&&$("#wk_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	
	
	}
	wKTemplateGrid=gridEditTable("wKTemplatediv",cols,loadParam,opts);
	$("#wKTemplatediv").data("wKTemplateGrid", wKTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//打印执行单
function stampOrder(){
	var id=$("#wk_template").val();
	if(id==""){
		message(biolims.common.pleaseSelectTemplate);
		return;
	}else{
		var url = '__report=WkTask.rptdesign&id=' + $("#wk_id").val();
		commonPrint(url);}
}


function addSuccess(){
	var isCF=false;
	var getRecord = wKItemGrid.store;
	var selectRecord = wKTemplateGrid.getSelectionModel().getSelections();
	var selRecord = wKSampleInfoGrid.store;
	if(selRecord.getCount()>0){
		for(var i=0; i<getRecord.getCount(); i++){
			for(var j=0;j<selRecord.getCount();j++){
				var itemCode = getRecord.getAt(i).get("splitCode");
				var infoCode = selRecord.getAt(j).get("splitCode");
				if(itemCode == infoCode){
					isCF = true;
					message("数据重复，请删除结果，重新生成！");
					break;					
				}
			}
		}
	   if(isCF==false){
		   for(var i=0;i<getRecord.getCount();i++){
			   if(getRecord.getAt(i).get("dicSampleType-name")==null ||
					   getRecord.getAt(i).get("dicSampleType-name")==""){
					message("请填写产物类型，并保存！");
					return;
				}
				var ob = wKSampleInfoGrid.getStore().recordType;
				wKSampleInfoGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("tempId",getRecord.getAt(i).get("tempId"));
				p.set("name",getRecord.getAt(i).get("name"));
				p.set("sysCode",getRecord.getAt(i).get("sysCode"));
				p.set("sampleCode",getRecord.getAt(i).get("sampleCode"));
				p.set("splitCode",getRecord.getAt(i).get("splitCode"));
				p.set("note",getRecord.getAt(i).get("note"));
				p.set("patientName",getRecord.getAt(i).get("patientName"));
				p.set("productId",getRecord.getAt(i).get("productId"));
				p.set("productName",getRecord.getAt(i).get("productName"));
				p.set("inspectDate",getRecord.getAt(i).get("inspectDate"));
				p.set("acceptDate",getRecord.getAt(i).get("acceptDate"));
				p.set("idCard",getRecord.getAt(i).get("idCard"));
				p.set("phone",getRecord.getAt(i).get("phone"));
				p.set("orderId",getRecord.getAt(i).get("orderId"));
				p.set("sequenceFun",getRecord.getAt(i).get("sequenceFun"));
				p.set("reportDate",getRecord.getAt(i).get("reportDate"));
				p.set("unit",getRecord.getAt(i).get("unit"));
				p.set("sampleType",getRecord.getAt(i).get("sampleType"));
				p.set("dicSampleType-id",getRecord.getAt(i).get("dicSampleType-id"));
				p.set("dicSampleType-name",getRecord.getAt(i).get("dicSampleType-name"));
				p.set("indexa",getRecord.getAt(i).get("indexa"));
				p.set("i5",getRecord.getAt(i).get("i5"));
				p.set("i7",getRecord.getAt(i).get("i7"));
				p.set("indexConcentration",getRecord.getAt(i).get("concentration"));
				
				p.set("labCode",getRecord.getAt(i).get("labCode"));
				p.set("sampleInfo-id",getRecord.getAt(i).get("sampleInfo-id"));
				p.set("sampleInfo-note",getRecord.getAt(i).get("sampleInfo-note"));
				p.set("sampleInfo-receiveDate",getRecord.getAt(i).get("sampleInfo-receiveDate"));
				p.set("sampleInfo-reportDate",getRecord.getAt(i).get("sampleInfo-reportDate"));
				p.set("techJkServiceTask-id",getRecord.getAt(i).get("techJkServiceTask-id"));
				p.set("techJkServiceTask-name",getRecord.getAt(i).get("techJkServiceTask-name"));
				p.set("result","1");
				p.set("rowCode",getRecord.getAt(i).get("rowCode"));
				p.set("colCode",getRecord.getAt(i).get("colCode"));
				p.set("counts",getRecord.getAt(i).get("counts"));
				p.set("isZkp",getRecord.getAt(i).get("isZkp"));
				p.set("inwardCode",getRecord.getAt(i).get("inwardCode"));
				p.set("tjItem-id",getRecord.getAt(i).get("tjItem-id"));
				p.set("tjItem-inwardCode",getRecord.getAt(i).get("tjItem-inwardCode"));
				p.set("species",getRecord.getAt(i).get("tjItem-species"));
				p.set("wkCode",getRecord.getAt(i).get("code"));
				ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
					model : "WkLifeTask",productId:getRecord.getAt(i).get("productId")
				}, function(data) {
					p.set("nextFlowId",data.dnextId);
					p.set("nextFlow",data.dnextName);
				}, null);
				message(biolims.common.generateResultsSuccess);
				wKSampleInfoGrid.getStore().add(p);
				wKSampleInfoGrid.startEditing(0,0);
					}
		   }
	}
	
	else{
		 for(var i=0;i<getRecord.getCount();i++){
			 if(getRecord.getAt(i).get("dicSampleType-name")==null ||
					   getRecord.getAt(i).get("dicSampleType-name")==""){
					message(biolims.common.pleaseFillSampleTypeSave);
					return;
				}
				var ob = wKSampleInfoGrid.getStore().recordType;
				wKSampleInfoGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("tempId",getRecord.getAt(i).get("tempId"));
				p.set("name",getRecord.getAt(i).get("name"));
				p.set("sysCode",getRecord.getAt(i).get("sysCode"));
				p.set("sampleCode",getRecord.getAt(i).get("sampleCode"));
				p.set("splitCode",getRecord.getAt(i).get("splitCode"));
				p.set("note",getRecord.getAt(i).get("note"));
				p.set("patientName",getRecord.getAt(i).get("patientName"));
				p.set("productId",getRecord.getAt(i).get("productId"));
				p.set("productName",getRecord.getAt(i).get("productName"));
				p.set("inspectDate",getRecord.getAt(i).get("inspectDate"));
				p.set("acceptDate",getRecord.getAt(i).get("acceptDate"));
				p.set("idCard",getRecord.getAt(i).get("idCard"));
				p.set("phone",getRecord.getAt(i).get("phone"));
				p.set("orderId",getRecord.getAt(i).get("orderId"));
				p.set("sequenceFun",getRecord.getAt(i).get("sequenceFun"));
				p.set("reportDate",getRecord.getAt(i).get("reportDate"));
				p.set("unit",getRecord.getAt(i).get("unit"));
				p.set("sampleType",getRecord.getAt(i).get("sampleType"));
				p.set("dicSampleType-id",getRecord.getAt(i).get("dicSampleType-id"));
				p.set("dicSampleType-name",getRecord.getAt(i).get("dicSampleType-name"));
				p.set("indexa",getRecord.getAt(i).get("indexa"));
				p.set("i5",getRecord.getAt(i).get("i5"));
				p.set("i7",getRecord.getAt(i).get("i7"));
				p.set("indexConcentration",getRecord.getAt(i).get("concentration"));
				
				p.set("labCode",getRecord.getAt(i).get("labCode"));
				p.set("sampleInfo-id",getRecord.getAt(i).get("sampleInfo-id"));
				p.set("sampleInfo-note",getRecord.getAt(i).get("sampleInfo-note"));
				p.set("sampleInfo-receiveDate",getRecord.getAt(i).get("sampleInfo-receiveDate"));
				p.set("sampleInfo-reportDate",getRecord.getAt(i).get("sampleInfo-reportDate"));
				p.set("techJkServiceTask-id",getRecord.getAt(i).get("techJkServiceTask-id"));
				p.set("techJkServiceTask-name",getRecord.getAt(i).get("techJkServiceTask-name"));
				p.set("result","1");
				p.set("rowCode",getRecord.getAt(i).get("rowCode"));
				p.set("colCode",getRecord.getAt(i).get("colCode"));
				p.set("counts",getRecord.getAt(i).get("counts"));
				p.set("isZkp",getRecord.getAt(i).get("isZkp"));
				p.set("inwardCode",getRecord.getAt(i).get("inwardCode"));
				p.set("tjItem-id",getRecord.getAt(i).get("tjItem-id"));
				p.set("tjItem-inwardCode",getRecord.getAt(i).get("tjItem-inwardCode"));
				p.set("species",getRecord.getAt(i).get("tjItem-species"));
				p.set("wkCode",getRecord.getAt(i).get("code"));
				ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
					model : "WkLifeTask",productId:getRecord.getAt(i).get("productId")
				}, function(data) {
					p.set("nextFlowId",data.dnextId);
					p.set("nextFlow",data.dnextName);
				}, null);
				message(biolims.common.generateResultsSuccess);
				wKSampleInfoGrid.getStore().add(p);
				wKSampleInfoGrid.startEditing(0,0);
					}
		   }
	}
//生成结果明细
function addSuccess1(){	
	
	var num =$("#wk_template_name").val();
	if(num!=""){
		var setNum = wKReagentGrid.store;
		var selectRecords = wKItemGrid.store;
			for(var i=0;i<setNum.getCount();i++){
				setNum.getAt(i).set("sampleNum",selectRecords.getCount());
		}


	var getRecord = wKItemGrid.getAllRecord();
	var selectRecord = wKTemplateGrid.getSelectionModel().getSelections();
	var selRecord = wKSampleInfoGrid.store;
			if(selectRecord.length>0){
				$.each(selectRecord, function(i, obj) {
//					var isRepeat = true;
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					
					if($("#wk_type").val()=="0"){//FFPE，血液
						for(var i1=0; i1<scode.length; i1++){
							for(var j1=0;j1<selRecord.getCount();j1++){
								var getv = scode[i1];
								var setv = selRecord.getAt(j1).get("tempId");
								var a=setv.indexOf(getv);
								if(a>-1){
									message(biolims.common.haveDuplicate);
									return;					
								}
							}
						}
						var blendCode=[];
						var n=[];
						for(var j=0;j<getRecord.length;j++){
							if(getRecord[j].get("blendCode")!=""){
								blendCode.push(getRecord[j].get("blendCode"));
							}
						}
						for(var j=0;j<getRecord.length;j++){
							var a=blendCode.indexOf(getRecord[j].get("blendCode"));
							var flg=true;
							for(var k=0;k<n.length;k++){
								if(a==n[k]){
									flg=false;
								}
							}
							if(flg){
								n.push(a);
							}
						}
						if(blendCode.length>0){//混合
							for(var i=0;i<n.length;i++){
								if(getRecord[n[i]].get("dicSampleType-name")==null||getRecord[n[i]].get("dicSampleType-name")==""){
									message(biolims.common.pleaseFillDic);
									return;
								}
								var productNum=getRecord[n[i]].get("productNum");
								if(productNum==null||productNum==""||productNum==0){
									message(biolims.common.pleaseFillDicNum);
								}else{
									for(var i1=0; i1<scode.length; i1++){
									for(var k=0;k<productNum;k++){
										if(getRecord[k].get("code")==scode[i1]){
											var ob = wKSampleInfoGrid.getStore().recordType;
											wKSampleInfoGrid.stopEditing();
											var p = new ob({});
											p.isNew = true;
											var sampleCode=[];
											var code=[];
											var expectNum=0;
											for(var j=0;j<getRecord.length;j++){
												if(getRecord[j].get("blendCode")==blendCode[n[i]]){
													sampleCode.push(getRecord[j].get("sampleCode"));
													code.push(getRecord[j].get("code"));
													expectNum+=Number(getRecord[j].get("expectNum"));
												}
											}
											p.set("sampleCode",sampleCode.toString());
											p.set("expectNum",expectNum);
											if(getRecord[n[i]].get("isZkp")==1){
												p.set("code",getRecord[n[i]].get("code"));
											}
											p.set("tempId",code.toString());
											p.set("name",getRecord[n[i]].get("name"));
											p.set("note",getRecord[n[i]].get("note"));
											p.set("patientName",getRecord[n[i]].get("patientName"));
											p.set("productId",getRecord[n[i]].get("productId"));
											p.set("productName",getRecord[n[i]].get("productName"));
											p.set("inspectDate",getRecord[n[i]].get("inspectDate"));
											p.set("acceptDate",getRecord[n[i]].get("acceptDate"));
											p.set("idCard",getRecord[n[i]].get("idCard"));
											p.set("phone",getRecord[n[i]].get("phone"));
											p.set("orderId",getRecord[n[i]].get("orderId"));
											p.set("sequenceFun",getRecord[n[i]].get("sequenceFun"));
											p.set("reportDate",getRecord[n[i]].get("reportDate"));
											p.set("unit",getRecord[n[i]].get("unit"));
											p.set("sampleType",getRecord[n[i]].get("sampleType"));
											p.set("indexa",getRecord[n[i]].get("indexa"));
											p.set("i5",getRecord[n[i]].get("i5"));
											p.set("i7",getRecord[n[i]].get("i7"));
											p.set("indexConcentration",getRecord[n[i]].get("concentration"));
											p.set("dicSampleType-id",getRecord[n[i]].get("dicSampleType-id"));
											p.set("dicSampleType-name",getRecord[n[i]].get("dicSampleType-name"));
											p.set("labCode",getRecord[n[i]].get("labCode"));
											p.set("sampleInfo-id",getRecord[n[i]].get("sampleInfo-id"));
											p.set("sampleInfo-note",getRecord[n[i]].get("sampleInfo-note"));
											p.set("sampleInfo-receiveDate",getRecord[n[i]].get("sampleInfo-receiveDate"));
											p.set("sampleInfo-reportDate",getRecord[n[i]].get("sampleInfo-reportDate"));
											p.set("techJkServiceTask-id",getRecord[n[i]].get("techJkServiceTask-id"));
											p.set("techJkServiceTask-name",getRecord[n[i]].get("techJkServiceTask-name"));
											p.set("result","1");
											p.set("rowCode",getRecord[n[i]].get("rowCode"));
											p.set("colCode",getRecord[n[i]].get("colCode"));
											p.set("counts",getRecord[n[i]].get("counts"));
											p.set("isZkp",getRecord[n[i]].get("isZkp"));
											
											p.set("techJkServiceTask-libraryType",getRecord[n[i]].get("techJkServiceTask-libraryType"));
											p.set("techJkServiceTask-insertSize",getRecord[n[i]].get("techJkServiceTask-insertSize"));
											p.set("libNum",getRecord[n[i]].get("libNum"));
											p.set("sampleNowNum",getRecord[n[i]].get("sampleNowNum"));
											p.set("takeNum",getRecord[n[i]].get("takeNum"));
											p.set("sampleVolume",getRecord[n[i]].get("sampleVolume"));
											p.set("breakNum",getRecord[n[i]].get("breakNum"));
											p.set("teOrH20num",getRecord[n[i]].get("teOrH20num"));
											p.set("storageLocation",getRecord[n[i]].get("storageLocation"));
											p.set("techJkServiceTask-sequenceType",getRecord[n[i]].get("techJkServiceTask-sequenceType"));
											p.set("primer",getRecord[n[i]].get("primer"));
											p.set("species",getRecord[n[i]].get("species"));
											p.set("inwardCode",getRecord[n[i]].get("inwardCode"));
											p.set("tjItem-id",getRecord[n[i]].get("tjItem-id"));
											p.set("tjItem-inwardCode",getRecord[n[i]].get("tjItem-inwardCode"));
											p.set("species",getRecord[n[i]].get("tjItem-species"));
											ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
												model : "WkTask",productId:getRecord[n[i]].get("productId")
											}, function(data) {
												p.set("nextFlowId",data.dnextId);
												p.set("nextFlow",data.dnextName);
											}, null);
											message(biolims.common.generateResultsSuccess);
											wKSampleInfoGrid.getStore().add(p);
											wKSampleInfoGrid.startEditing(0,0);
											
											}
										}}
									}
							}
						}else{//不混
							$.each(getRecord,function(a,b){
								if(b.get("dicSampleType-name")==null||b.get("dicSampleType-name")==""){
									message(biolims.common.pleaseFillDic);
									return;
								}
								var productNum=b.get("productNum");
								if(productNum==null||productNum==""||productNum==0){
									message(biolims.common.pleaseFillDicNum);
								}else{
									for(var i1=0; i1<scode.length; i1++){
									for(var k=1;k<=productNum;k++){
										if(b.get("code")==scode[i1]){
											var ob = wKSampleInfoGrid.getStore().recordType;
											wKSampleInfoGrid.stopEditing();
											var p = new ob({});
											p.isNew = true;
											
											
											if(b.get("isZkp")==1){
												p.set("code",b.get("code"));
											}
											p.set("tempId",b.get("tempId"));
											p.set("name",b.get("name"));
											p.set("sampleCode",b.get("sampleCode"));
											p.set("note",b.get("note"));
											p.set("patientName",b.get("patientName"));
											p.set("productId",b.get("productId"));
											p.set("productName",b.get("productName"));
											p.set("inspectDate",b.get("inspectDate"));
											p.set("acceptDate",b.get("acceptDate"));
											p.set("idCard",b.get("idCard"));
											p.set("phone",b.get("phone"));
											p.set("orderId",b.get("orderId"));
											p.set("sequenceFun",b.get("sequenceFun"));
											p.set("reportDate",b.get("reportDate"));
											p.set("unit",b.get("unit"));
											p.set("sampleType",b.get("sampleType"));
											p.set("dicSampleType-id",b.get("dicSampleType-id"));
											p.set("dicSampleType-name",b.get("dicSampleType-name"));
											p.set("indexa",b.get("indexa"));
											p.set("i5",b.get("i5"));
											p.set("i7",b.get("i7"));
											p.set("indexConcentration",b.get("concentration"));
											p.set("expectNum",b.get("expectNum"));
											p.set("labCode",b.get("labCode"));
											p.set("sampleInfo-id",b.get("sampleInfo-id"));
											p.set("sampleInfo-note",b.get("sampleInfo-note"));
											p.set("sampleInfo-receiveDate",b.get("sampleInfo-receiveDate"));
											p.set("sampleInfo-reportDate",b.get("sampleInfo-reportDate"));
											p.set("techJkServiceTask-id",b.get("techJkServiceTask-id"));
											p.set("techJkServiceTask-name",b.get("techJkServiceTask-name"));
											p.set("result","1");
											p.set("rowCode",b.get("rowCode"));
											p.set("colCode",b.get("colCode"));
											p.set("counts",b.get("counts"));
											p.set("isZkp",b.get("isZkp"));
											p.set("techJkServiceTask-libraryType",b.get("techJkServiceTask-libraryType"));
											p.set("techJkServiceTask-insertSize",b.get("techJkServiceTask-insertSize"));
											p.set("libNum",b.get("libNum"));
											p.set("sampleNowNum",b.get("sampleNowNum"));
											p.set("takeNum",b.get("takeNum"));
											p.set("sampleVolume",b.get("sampleVolume"));
											p.set("breakNum",b.get("breakNum"));
											p.set("teOrH20num",b.get("teOrH20num"));
											p.set("storageLocation",b.get("storageLocation"));
											p.set("techJkServiceTask-sequenceType",b.get("techJkServiceTask-sequenceType"));
											p.set("primer",b.get("primer"));
											p.set("species",b.get("species"));
											p.set("inwardCode",b.get("inwardCode"));
											p.set("tjItem-id",b.get("tjItem-id"));
											p.set("tjItem-inwardCode",b.get("tjItem-inwardCode"));
											p.set("species",b.get("tjItem-species"));
											ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
												model : "WkTask",productId:b.get("productId")
											}, function(data) {
												p.set("nextFlowId",data.dnextId);
												p.set("nextFlow",data.dnextName);
											}, null);
											message(biolims.common.generateResultsSuccess);
											wKSampleInfoGrid.getStore().add(p);
											wKSampleInfoGrid.startEditing(0,0);
										}
									}}
								}
							});
					
					}
						}
					
				});
				
			}else{
				var selRecord1 = wKSampleInfoGrid.store;
				var getRecord1 = wKItemGrid.getAllRecord();
				if($("#wk_type").val()=="0"){//FFPE，血液
					var blendCode=[];
					var n=[];
					for(var j=0;j<getRecord.length;j++){
						if(getRecord[j].get("blendCode")!=""){
							blendCode.push(getRecord[j].get("blendCode"));
						}
					}
					for(var j=0;j<getRecord.length;j++){
						var a=blendCode.indexOf(getRecord[j].get("blendCode"));
						var flg=true;
						for(var k=0;k<n.length;k++){
							if(a==n[k]){
								flg=false;
							}
						}
						if(flg){
							n.push(a);
						}
					}
					for(var i=0;i<getRecord.length;i++){
						for(var j1=0;j1<selRecord1.getCount();j1++){
							var getv = getRecord1[i].get("code");
							var setv = selRecord1.getAt(j1).get("tempId");
							var a=setv.indexOf(getv);
							if(a>-1){
								message(biolims.common.haveDuplicate);
								return;					
							}
						}
					}
					if(blendCode.length>0){//混合
						for(var i=0;i<n.length;i++){
							if(getRecord[n[i]].get("dicSampleType-name")==null||getRecord[n[i]].get("dicSampleType-name")==""){
								message(biolims.common.pleaseFillDic);
								return;
							}
							var productNum=getRecord1[n[i]].get("productNum");
							if(productNum==null||productNum==""||productNum==0){
								message(biolims.common.pleaseFillDicNum);
							}else{
								for(var k=1;k<=productNum;k++){
							var ob = wKSampleInfoGrid.getStore().recordType;
							wKSampleInfoGrid.stopEditing();
							var p = new ob({});
							p.isNew = true;
							var sampleCode=[];
							var code=[];
							var expectNum=0;
							for(var j=0;j<getRecord.length;j++){
								if(getRecord[j].get("blendCode")==blendCode[n[i]]){
									sampleCode.push(getRecord[j].get("sampleCode"));
									code.push(getRecord[j].get("code"));
									expectNum+=Number(getRecord[j].get("expectNum"));
								}
							}
							p.set("sampleCode",sampleCode.toString());
							p.set("expectNum",expectNum);
							if(getRecord1[n[i]].get("isZkp")==1){
								p.set("code",getRecord1[n[i]].get("code"));
							}
							p.set("tempId",code.toString());
							p.set("name",getRecord1[n[i]].get("name"));
							
							p.set("note",getRecord1[n[i]].get("note"));
							p.set("patientName",getRecord1[n[i]].get("patientName"));
							p.set("productId",getRecord1[n[i]].get("productId"));
							p.set("productName",getRecord1[n[i]].get("productName"));
							p.set("inspectDate",getRecord1[n[i]].get("inspectDate"));
							p.set("acceptDate",getRecord1[n[i]].get("acceptDate"));
							p.set("idCard",getRecord1[n[i]].get("idCard"));
							p.set("phone",getRecord1[n[i]].get("phone"));
							p.set("orderId",getRecord1[n[i]].get("orderId"));
							p.set("sequenceFun",getRecord1[n[i]].get("sequenceFun"));
							p.set("reportDate",getRecord1[n[i]].get("reportDate"));
							p.set("unit",getRecord1[n[i]].get("unit"));
							p.set("sampleType",getRecord1[n[i]].get("sampleType"));
							p.set("dicSampleType-id",getRecord1[n[i]].get("dicSampleType-id"));
							p.set("dicSampleType-name",getRecord1[n[i]].get("dicSampleType-name"));
							p.set("indexa",getRecord1[n[i]].get("indexa"));
							p.set("i5",getRecord1[n[i]].get("i5"));
							p.set("i7",getRecord1[n[i]].get("i7"));
							p.set("indexConcentration",getRecord1[n[i]].get("concentration"));
							
							p.set("labCode",getRecord1[n[i]].get("labCode"));
							p.set("sampleInfo-id",getRecord1[n[i]].get("sampleInfo-id"));
							p.set("sampleInfo-note",getRecord1[n[i]].get("sampleInfo-note"));
							p.set("sampleInfo-receiveDate",getRecord1[n[i]].get("sampleInfo-receiveDate"));
							p.set("sampleInfo-reportDate",getRecord1[n[i]].get("sampleInfo-reportDate"));
							p.set("techJkServiceTask-id",getRecord1[n[i]].get("techJkServiceTask-id"));
							p.set("techJkServiceTask-name",getRecord1[n[i]].get("techJkServiceTask-name"));
							p.set("result","1");
							p.set("rowCode",getRecord1[n[i]].get("rowCode"));
							p.set("colCode",getRecord1[n[i]].get("colCode"));
							p.set("counts",getRecord1[n[i]].get("counts"));
							p.set("isZkp",getRecord1[n[i]].get("isZkp"));
							p.set("inwardCode",getRecord1[n[i]].get("inwardCode"));
							p.set("tjItem-id",getRecord1[n[i]].get("tjItem-id"));
							p.set("tjItem-inwardCode",getRecord1[n[i]].get("tjItem-inwardCode"));
							p.set("species",getRecord[n[i]].get("tjItem-species"));
							ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
								model : "WkTask",productId:getRecord1[n[i]].get("productId")
							}, function(data) {
								p.set("nextFlowId",data.dnextId);
								p.set("nextFlow",data.dnextName);
							}, null);
							message(biolims.common.generateResultsSuccess);
							wKSampleInfoGrid.getStore().add(p);
							wKSampleInfoGrid.startEditing(0,0);
								}}
						
						}
					}else{//不混
						$.each(getRecord1,function(a,b){
							if(b.get("dicSampleType-name")==null||b.get("dicSampleType-name")==""){
								message(biolims.common.pleaseFillDic);
								return;
							}
							var productNum=b.get("productNum");
							if(productNum==null||productNum==""||productNum==0){
								message(biolims.common.pleaseFillDicNum);
							}else{
								for(var k=1;k<=productNum;k++){
									var ob = wKSampleInfoGrid.getStore().recordType;
									wKSampleInfoGrid.stopEditing();
									var p = new ob({});
									p.isNew = true;
									if(b.get("isZkp")==1){
										p.set("code",b.get("code"));
									}
									p.set("tempId",b.get("code"));
									p.set("name",b.get("name"));
									p.set("sampleCode",b.get("sampleCode"));
									p.set("note",b.get("note"));
									p.set("patientName",b.get("patientName"));
									p.set("productId",b.get("productId"));
									p.set("productName",b.get("productName"));
									p.set("inspectDate",b.get("inspectDate"));
									p.set("acceptDate",b.get("acceptDate"));
									p.set("idCard",b.get("idCard"));
									p.set("phone",b.get("phone"));
									p.set("orderId",b.get("orderId"));
									p.set("sequenceFun",b.get("sequenceFun"));
									p.set("reportDate",b.get("reportDate"));
									p.set("unit",b.get("unit"));
									p.set("sampleType",b.get("sampleType"));
									p.set("dicSampleType-id",b.get("dicSampleType-id"));
									p.set("dicSampleType-name",b.get("dicSampleType-name"));
									p.set("indexa",b.get("indexa"));
									p.set("i5",b.get("i5"));
									p.set("i7",b.get("i7"));
									p.set("indexConcentration",b.get("concentration"));
									p.set("expectNum",b.get("expectNum"));
									p.set("labCode",b.get("labCode"));
									p.set("sampleInfo-id",b.get("sampleInfo-id"));
									p.set("sampleInfo-note",b.get("sampleInfo-note"));
									p.set("sampleInfo-receiveDate",b.get("sampleInfo-receiveDate"));
									p.set("sampleInfo-reportDate",b.get("sampleInfo-reportDate"));
									p.set("techJkServiceTask-id",b.get("techJkServiceTask-id"));
									p.set("techJkServiceTask-name",b.get("techJkServiceTask-name"));
									p.set("result","1");
									p.set("rowCode",b.get("rowCode"));
									p.set("colCode",b.get("colCode"));
									p.set("counts",b.get("counts"));
									p.set("isZkp",b.get("isZkp"));
									p.set("inwardCode",b.get("inwardCode"));
									p.set("tjItem-id",b.get("tjItem-id"));
									p.set("tjItem-inwardCode",b.get("tjItem-inwardCode"));
									p.set("species",b.get("tjItem-species"));
									ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
										model : "WkTask",productId:b.get("productId")
									}, function(data) {
										p.set("nextFlowId",data.dnextId);
										p.set("nextFlow",data.dnextName);
									}, null);
									message(biolims.common.generateResultsSuccess);
									wKSampleInfoGrid.getStore().add(p);
									wKSampleInfoGrid.startEditing(0,0);
									}
								}
						});
					}
				}
			}
	}else{
		message(biolims.common.pleaseSelectTemplate);
	}

}

//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=wKTemplateGrid.getSelectionModel();
	var setNum = wKReagentGrid.store;
	var selectRecords=wKItemGrid.getSelectionModel();
	if(selectRecords.getSelections().length>0){
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			obj.set("startTime",str);
			//将所选样本的数量，放到原辅料样本数量处
			for(var i=0; i<setNum.getCount();i++){
				var num = setNum.getAt(i).get("itemId");
				if(num==obj.get("code")){
					setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
				}
			}
		});
	}else{
		message(biolims.common.pleaseSelect);
	}
	
	//将所选的样本，放到关联样本
	var selRecord=wKTemplateGrid.getSelectRecord();
	var codes = "";
		$.each(selectRecords.getSelections(), function(i, obj) {
			codes += obj.get("code")+",";
		});
		$.each(selRecord, function(i, obj) {
			obj.set("sampleCodes", codes);
		});
	
}else{
	message(biolims.common.pleaseSelectSamples);
}
}
//获取停止时的时间
function getEndTime(){
		var setRecord=wKItemGrid.store;
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=wKTemplateGrid.getSelectionModel();
		var getIndex = wKTemplateGrid.store;
		var getIndexs = wKTemplateGrid.getSelectionModel().getSelections();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("endTime",str);
				//将步骤编号，赋值到文库明细表
				var codes = obj.get("sampleCodes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("code")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				//将当前行的关联样本传到下一行
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
			});
			
		}
		
		
}
function selectwkFun(){
	var win = Ext.getCmp('selectwk');
	if (win) {win.close();}
	var selectwk= new Ext.Window({
	id:'selectwk',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKSelect.action?flag=wk' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectwk.close(); }  }]  });     selectwk.show(); }
	function setwk(id,name){
		var gridGrid = $("#wKTemplatediv").data("wKTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wk-id',id);
			obj.set('wk-name',name);
		});
		var win = Ext.getCmp('selectwk')
		if(win){
			win.close();
		}
	}
	//选择实验步骤
	function templateSelect(){
		var option = {};
		option.width = 605;
		option.height = 558;
		loadDialogPage(null,biolims.common.chooseExperimentalSteps, "/experiment/wkLife/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
			"Confirm" : function() {
				var operGrid = $("#template_wait_grid_div").data("grid");
				var ob = wKTemplateGrid.getStore().recordType;
				wKTemplateGrid.stopEditing();
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
//						for(var i=0;i<arr.length-1;i++){
							var p = new ob({});
							p.isNew = true;
							p.set("code", obj.get("code"));
							p.set("name", obj.get("name"));
//							p.set("blood-sequencingName", obj.get("sequencingName"));
//							p.set("blood-reportMan-id", obj.get("reportMan-id"));	
//							p.set("blood-patientId-name", obj.get("patientId-name"));	
							wKTemplateGrid.getStore().add(p);
					});
					wKTemplateGrid.startEditing(0, 0);
					$(this).dialog("close");
					$(this).dialog("remove");
				} else {
					message(biolims.common.selectYouWant);
					return;
				}
			}
		}, true, option);

		
	}
	//查询实验员
	function loadTestUser(){
			var gid=$("#wk_acceptUser").val();
			var options = {};
			options.width = 500;
			options.height = 500;
			loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				"Confirm" : function() {
					var gridGrid = $("#wKTemplatediv").data("wKTemplateGrid");
					var selRecords = gridGrid.getSelectionModel().getSelections(); 
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						var id=[];
						var name=[];
						for(var i=0;i<selectRecord.length;i++){
							id.push(selectRecord[i].get("user-id"));
							name.push(selectRecord[i].get("user-name"));
						}
						for(var j=0;j<selRecords.length;j++){
							selRecords[j].set("testUserId",id.toString());
							selRecords[j].set("testUserName",name.toString());}
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
	}
