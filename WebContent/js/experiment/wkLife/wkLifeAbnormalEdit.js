﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});
function add() {
	window.location = window.ctx + "/experiment/wkLife/wkAbnormal/editWkAbnormal.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/wkLife/wkAbnormal/showWkAbnormalList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#wkAbnormal", {
					userId : userId,
					userName : userName,
					formId : $("#wkAbnormal_id").val(),
					title : $("#wkAbnormal_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#wkAbnormal_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var plasmaAbnormalBackDivData = $("#plasmaAbnormalBackdiv").data("plasmaAbnormalBackGrid");
		document.getElementById('plasmaAbnormalBackJson').value = commonGetModifyRecords(plasmaAbnormalBackDivData);
	    var wkAbnormalBackDivData = $("#wkAbnormalBackdiv").data("wkAbnormalBackGrid");
		document.getElementById('wkAbnormalBackJson').value = commonGetModifyRecords(wkAbnormalBackDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/wkLife/wkAbnormal/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/wkLife/wkAbnormal/copyWkAbnormal.action?id=' + $("#wkAbnormal_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#wkAbnormal_id").val() + "&tableId=wkAbnormal");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wkAbnormal_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.wk.wkAbnormal,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/wkLife/wkAbnormal/showPlasmaAbnormalBackList.action", {
				id : $("#wkAbnormal_id").val()
			}, "#plasmaAbnormalBackpage");
load("/experiment/wkLife/wkAbnormal/showWkAbnormalBackList.action", {
				id : $("#wkAbnormal_id").val()
			}, "#wkAbnormalBackpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);