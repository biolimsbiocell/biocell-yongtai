﻿var wKReceiveItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
//	   fields.push({
//		name:'idCard',
//		type:"string"
//	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
//	   fields.push({
//		name:'phone',
//		type:"string"
//	});
   		fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'storageLocal-id',
		type:"string"
	});
	    fields.push({
		name:'storageLocal-name',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'wkReceive-id',
		type:"string"
	});
	    fields.push({
		name:'wkReceive-name',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	    fields.push({
			name:'sampleType',
			type:"string"
		});
	    fields.push({
			name:'sampleNum',
			type:"string"
		});
	    fields.push({
			name:'tempId',
			type:"string"
		});
	    fields.push({
			name:'labCode',
			type:"string"
		}); 
	    fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-note',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-receiveDate',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-reportDate',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6,
	});
	var method = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : biolims.common.disqualified
			}, {
				id : '1',
				name : biolims.common.qualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:biolims.common.method,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(method),editor: method
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:20*6,
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6,
//	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6,
	});

	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.reason,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'storageLocal-id',
		hidden : true,
		header:biolims.common.storageId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storageLocal-name',
		hidden : true,
		header:biolims.common.location,
		width:20*10
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'wkReceive-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkReceive-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sampleInfo-id',
		header :biolims.common.openBoxId,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:biolims.common.sampleInfoReceiveDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:biolims.common.sampleInfoReportDate,
		width:20*6
	});
	cm.push({
		dataIndex : 'sampleInfo-note',
		header : biolims.common.openBox,
		width : 20 * 6,
		hidden : false,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wKReceive/showWKReceiveItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.common.sampleReceiveDetail;
	opts.height =  document.body.clientHeight-145;
	opts.tbar = [];
	var state = $("#wkReceive_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/wkLife/wKReceive/delWKReceiveItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wKReceiveItemGrid.getStore().commitChanges();
				wKReceiveItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_batItem_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_batItem_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_batItem_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = wKReceiveItemGrid.getAllRecord();
							var store = wKReceiveItemGrid.store;
							var count = 0;
							var isOper = true;
							var buf = [];
							var buf1 = [];
							wKReceiveItemGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										isOper==true;
									}else{
									}
									
								});
							});
							if(isOper){
								message("编码核对完成！");
							}
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}
							wKReceiveItemGrid.getSelectionModel().selectRows(buf);
							wKReceiveItemGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
//	opts.tbar.push({
//			text : biolims.common.pleaseSelectLocation,
//			handler : selectstorageLocalFun
//		});
    
	
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), biolims.common.batchResult, null, {
				"Confirm" : function() {
					var records = wKReceiveItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						wKReceiveItemGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("method", result);
						});
						wKReceiveItemGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
	wKReceiveItemGrid=gridEditTable("wKReceiveItemdiv",cols,loadParam,opts);
	$("#wKReceiveItemdiv").data("wKReceiveItemGrid", wKReceiveItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


//选择储位selectstorageLocalFun
function selectstorageLocalFun(){
	var win = Ext.getCmp('selectstorageFun');
	if (win) {win.close();}
	var selectstorageFun= new Ext.Window({
	id:'selectstorageFun',modal:true,title:biolims.common.pleaseSelectLocation,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTree1.action?flag=SaveLocationFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectstorageFun.close(); }  }]  });     selectstorageFun.show(); }
	function setSaveLocationFun(rec){
		var gridGrid = $("#wKReceiveItemdiv").data("wKReceiveItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('storage-id',rec.get('id'));
			obj.set('storage-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectstorageFun');
		if(win){
			win.close();
		}
	}
	
//function selectwKReceiveFun(){
//	var win = Ext.getCmp('selectwKReceive');
//	if (win) {win.close();}
//	var selectwKReceive= new Ext.Window({
//	id:'selectwKReceive',modal:true,title:'选择关联主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKReceiveSelect.action?flag=wKReceive' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectwKReceive.close(); }  }]  });     selectwKReceive.show(); }
//	function setwKReceive(id,name){
//		var gridGrid = $("#wKReceiveItemdiv").data("wKReceiveItemGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('wKReceive-id',id);
//			obj.set('wKReceive-name',name);
//		});
//		var win = Ext.getCmp('selectwKReceive')
//		if(win){
//			win.close();
//		}
//	}
	
